.class public Lorg/slf4j/impl/Log4jLoggerFactory;
.super Ljava/lang/Object;
.source "Log4jLoggerFactory.java"

# interfaces
.implements Lorg/slf4j/ILoggerFactory;


# instance fields
.field loggerMap:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/slf4j/Logger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/slf4j/impl/Log4jLoggerFactory;->loggerMap:Ljava/util/concurrent/ConcurrentMap;

    .line 50
    return-void
.end method


# virtual methods
.method public getLogger(Ljava/lang/String;)Lorg/slf4j/Logger;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v4, p0, Lorg/slf4j/impl/Log4jLoggerFactory;->loggerMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/slf4j/Logger;

    .line 59
    .local v3, "slf4jLogger":Lorg/slf4j/Logger;
    if-eqz v3, :cond_0

    .line 70
    .end local v3    # "slf4jLogger":Lorg/slf4j/Logger;
    :goto_0
    return-object v3

    .line 63
    .restart local v3    # "slf4jLogger":Lorg/slf4j/Logger;
    :cond_0
    const-string v4, "ROOT"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 64
    invoke-static {}, Lorg/apache/log4j/LogManager;->getRootLogger()Lorg/apache/log4j/Logger;

    move-result-object v0

    .line 68
    .local v0, "log4jLogger":Lorg/apache/log4j/Logger;
    :goto_1
    new-instance v1, Lorg/slf4j/impl/Log4jLoggerAdapter;

    invoke-direct {v1, v0}, Lorg/slf4j/impl/Log4jLoggerAdapter;-><init>(Lorg/apache/log4j/Logger;)V

    .line 69
    .local v1, "newInstance":Lorg/slf4j/Logger;
    iget-object v4, p0, Lorg/slf4j/impl/Log4jLoggerFactory;->loggerMap:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/slf4j/Logger;

    .line 70
    .local v2, "oldInstance":Lorg/slf4j/Logger;
    if-nez v2, :cond_2

    .end local v1    # "newInstance":Lorg/slf4j/Logger;
    :goto_2
    move-object v3, v1

    goto :goto_0

    .line 66
    .end local v0    # "log4jLogger":Lorg/apache/log4j/Logger;
    .end local v2    # "oldInstance":Lorg/slf4j/Logger;
    :cond_1
    invoke-static {p1}, Lorg/apache/log4j/LogManager;->getLogger(Ljava/lang/String;)Lorg/apache/log4j/Logger;

    move-result-object v0

    .restart local v0    # "log4jLogger":Lorg/apache/log4j/Logger;
    goto :goto_1

    .restart local v1    # "newInstance":Lorg/slf4j/Logger;
    .restart local v2    # "oldInstance":Lorg/slf4j/Logger;
    :cond_2
    move-object v1, v2

    .line 70
    goto :goto_2
.end method
