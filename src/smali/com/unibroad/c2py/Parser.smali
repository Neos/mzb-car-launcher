.class public Lcom/unibroad/c2py/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# static fields
.field private static sInitialized:Z = false

.field private static final sName:Ljava/lang/String; = "unicode_to_hanyu_pinyin.txt"

.field protected static sProp:Ljava/util/Properties;

.field public static specialHanzi:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-boolean v0, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    .line 201
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    .line 206
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u8584"

    const-string v2, "\u640f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u79d8"

    const-string v2, "\u95ed"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u7987"

    const-string v2, "\u695a"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u91cd"

    const-string v2, "\u5d07"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u76d6"

    const-string v2, "\u8238"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u8d3e"

    const-string v2, "\u7532"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u89e3"

    const-string v2, "\u8c22"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u7f2a"

    const-string v2, "\u5999"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u90a3"

    const-string v2, "\u7eb3"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u533a"

    const-string v2, "\u6b27"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u8983"

    const-string v2, "\u52e4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u77bf"

    const-string v2, "\u6b0b"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u4ec7"

    const-string v2, "\u7403"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u76db"

    const-string v2, "\u5269"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u77f3"

    const-string v2, "\u65f6"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u6c88"

    const-string v2, "\u5ba1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u6298"

    const-string v2, "\u86c7"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u5355"

    const-string v2, "\u5584"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u5854"

    const-string v2, "\u5b83"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u590f"

    const-string v2, "\u4e0b"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u51bc"

    const-string v2, "\u9669"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u4fde"

    const-string v2, "\u4e8e"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u65bc"

    const-string v2, "\u6de4"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u66fe"

    const-string v2, "\u589e"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u67e5"

    const-string v2, "\u5412"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u85cf"

    const-string v2, "\u846c"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u7fdf"

    const-string v2, "\u5b85"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u4e07\u4fdf"

    const-string v2, "\u83ab\u5947"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    const-string v1, "\u5c09\u8fdf"

    const-string v2, "\u7389\u8fdf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getAllPinyin(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    .line 130
    if-eqz p0, :cond_0

    const-string v0, ""

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    :cond_0
    const-string v0, "#"

    .line 134
    :goto_0
    return-object v0

    .line 133
    :cond_1
    invoke-static {p0}, Lcom/unibroad/c2py/Parser;->isPolyphone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 134
    invoke-static {p0}, Lcom/unibroad/c2py/Parser;->getPinyin(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final getPinyin(C)Ljava/lang/String;
    .locals 6
    .param p0, "ch"    # C

    .prologue
    .line 73
    if-ltz p0, :cond_0

    const/16 v3, 0x100

    if-ge p0, v3, :cond_0

    .line 74
    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 92
    :goto_0
    return-object v2

    .line 76
    :cond_0
    sget-boolean v3, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    if-nez v3, :cond_1

    .line 78
    :try_start_0
    invoke-static {}, Lcom/unibroad/c2py/Parser;->initialize()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :cond_1
    :goto_1
    const/4 v2, 0x0

    .line 85
    .local v2, "result":Ljava/lang/String;
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "key":Ljava/lang/String;
    sget-object v3, Lcom/unibroad/c2py/Parser;->sProp:Ljava/util/Properties;

    invoke-virtual {v3, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    if-nez v2, :cond_2

    .line 88
    const-string v2, "#"

    .line 89
    goto :goto_0

    .line 79
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PinyinParser initialize error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 90
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "key":Ljava/lang/String;
    .restart local v2    # "result":Ljava/lang/String;
    :cond_2
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v2, v3, v4

    goto :goto_0
.end method

.method public static final getPinyin(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    .line 111
    if-eqz p0, :cond_0

    const-string v5, ""

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 112
    :cond_0
    const-string v5, "#"

    .line 122
    :goto_0
    return-object v5

    .line 114
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 115
    .local v2, "chs":[C
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .local v0, "builder":Ljava/lang/StringBuilder;
    array-length v4, v2

    .line 118
    .local v4, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v4, :cond_2

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 119
    :cond_2
    aget-char v1, v2, v3

    .line 120
    .local v1, "ch":C
    invoke-static {v1}, Lcom/unibroad/c2py/Parser;->getPinyin(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static final getPinyinAllFirstLetters(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    .line 187
    if-eqz p0, :cond_0

    const-string v5, ""

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 188
    :cond_0
    const-string v5, "#"

    .line 198
    :goto_0
    return-object v5

    .line 190
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 191
    .local v2, "chs":[C
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .local v0, "builder":Ljava/lang/StringBuilder;
    array-length v4, v2

    .line 194
    .local v4, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v4, :cond_2

    .line 198
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 195
    :cond_2
    aget-char v1, v2, v3

    .line 196
    .local v1, "ch":C
    invoke-static {v1}, Lcom/unibroad/c2py/Parser;->getPinyinFirstLetter(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static final getPinyinFirstLetter(C)Ljava/lang/String;
    .locals 3
    .param p0, "ch"    # C

    .prologue
    .line 101
    invoke-static {p0}, Lcom/unibroad/c2py/Parser;->getPinyin(C)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 102
    .local v0, "py":C
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final getPinyinFirstLetter(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    .line 156
    if-eqz p0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    :cond_0
    const-string v1, "#"

    .line 160
    :goto_0
    return-object v1

    .line 159
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 160
    .local v0, "ch":C
    invoke-static {v0}, Lcom/unibroad/c2py/Parser;->getPinyinFirstLetter(C)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static final getPinyinFirstWord(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    .line 143
    if-eqz p0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    :cond_0
    const-string v1, "#"

    .line 147
    :goto_0
    return-object v1

    .line 146
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 147
    .local v0, "ch":C
    invoke-static {v0}, Lcom/unibroad/c2py/Parser;->getPinyin(C)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static final getPinyinSecondWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 169
    if-eqz p0, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 170
    :cond_0
    const-string v1, "#"

    .line 178
    :cond_1
    :goto_0
    return-object v1

    .line 172
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 173
    .local v0, "ch":C
    invoke-static {v0}, Lcom/unibroad/c2py/Parser;->getPinyin(C)Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "s1":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 175
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 176
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/unibroad/c2py/Parser;->getPinyin(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static initialize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    sget-boolean v2, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    if-nez v2, :cond_2

    .line 47
    const-class v3, Lcom/unibroad/c2py/Parser;

    monitor-enter v3

    .line 48
    :try_start_0
    sget-boolean v2, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    if-nez v2, :cond_1

    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "is":Ljava/io/InputStream;
    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    sput-object v2, Lcom/unibroad/c2py/Parser;->sProp:Ljava/util/Properties;

    .line 51
    const-class v2, Lcom/unibroad/c2py/Parser;

    const-string v4, "unicode_to_hanyu_pinyin.txt"

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 53
    :try_start_1
    sget-object v2, Lcom/unibroad/c2py/Parser;->sProp:Ljava/util/Properties;

    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    if-eqz v1, :cond_0

    .line 58
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 61
    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    .line 47
    :cond_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 65
    :cond_2
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 56
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 57
    if-eqz v1, :cond_3

    .line 58
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 60
    :cond_3
    throw v2

    .line 47
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2
.end method

.method private static isPolyphone(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "chinese"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 263
    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "firstName":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_2

    .line 265
    invoke-virtual {p0, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "firstName1":Ljava/lang/String;
    const-string v4, "\u4e07\u4fdf"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "\u5c09\u8fdf"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 267
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 268
    .local v3, "lastName1":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 276
    .end local v1    # "firstName1":Ljava/lang/String;
    .end local v3    # "lastName1":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p0

    .line 272
    :cond_2
    sget-object v4, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 273
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 274
    .local v2, "lastName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/unibroad/c2py/Parser;->specialHanzi:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static final release()V
    .locals 2

    .prologue
    .line 32
    sget-boolean v0, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    if-eqz v0, :cond_2

    .line 33
    const-class v1, Lcom/unibroad/c2py/Parser;

    monitor-enter v1

    .line 34
    :try_start_0
    sget-boolean v0, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    if-eqz v0, :cond_1

    .line 35
    sget-object v0, Lcom/unibroad/c2py/Parser;->sProp:Ljava/util/Properties;

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Lcom/unibroad/c2py/Parser;->sProp:Ljava/util/Properties;

    invoke-virtual {v0}, Ljava/util/Properties;->clear()V

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/unibroad/c2py/Parser;->sProp:Ljava/util/Properties;

    .line 39
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/unibroad/c2py/Parser;->sInitialized:Z

    .line 33
    :cond_1
    monitor-exit v1

    .line 43
    :cond_2
    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
