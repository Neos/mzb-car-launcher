.class public Lcom/touchus/publicutils/sysconst/BMWModel;
.super Ljava/lang/Object;
.source "BMWModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;,
        Lcom/touchus/publicutils/sysconst/BMWModel$EBMWTpye;
    }
.end annotation


# static fields
.field public static ACTION_BMW_ORIGINAL:Ljava/lang/String;

.field public static ACTION_BMW_SIZE:Ljava/lang/String;

.field public static ACTION_BMW_TYPE:Ljava/lang/String;

.field public static KEY:Ljava/lang/String;

.field public static ORIGINAL_KEY:Ljava/lang/String;

.field public static SIZE_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "com.touchus.factorytest.bmwtype"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel;->ACTION_BMW_TYPE:Ljava/lang/String;

    .line 12
    const-string v0, "com.touchus.factorytest.bmworiginal"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel;->ACTION_BMW_ORIGINAL:Ljava/lang/String;

    .line 13
    const-string v0, "com.touchus.factorytest.bmwsize"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel;->ACTION_BMW_SIZE:Ljava/lang/String;

    .line 14
    const-string v0, "bmwType"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel;->KEY:Ljava/lang/String;

    .line 15
    const-string v0, "bmwOriginal"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel;->ORIGINAL_KEY:Ljava/lang/String;

    .line 16
    const-string v0, "bmwSize"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel;->SIZE_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
