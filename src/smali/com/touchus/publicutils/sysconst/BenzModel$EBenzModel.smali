.class public final enum Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;
.super Ljava/lang/Enum;
.source "BenzModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/sysconst/BenzModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBenzModel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BENZ_A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_E:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field public static final enum BENZ_ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

.field private static final synthetic ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 66
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_A"

    const-string v2, "BENZ_A"

    invoke-direct {v0, v1, v4, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_B"

    const-string v2, "BENZ_B"

    invoke-direct {v0, v1, v5, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_C"

    const-string v2, "BENZ_C"

    invoke-direct {v0, v1, v6, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_GLC"

    const-string v2, "BENZ_GLC"

    invoke-direct {v0, v1, v7, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_E"

    const-string v2, "BENZ_E"

    invoke-direct {v0, v1, v8, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_E:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    .line 67
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_GLE"

    const/4 v2, 0x5

    const-string v3, "BENZ_GLE"

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_GLA"

    const/4 v2, 0x6

    const-string v3, "BENZ_GLA"

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_GLK"

    const/4 v2, 0x7

    const-string v3, "BENZ_GLK"

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    const-string v1, "BENZ_ML"

    const/16 v2, 0x8

    const-string v3, "BENZ_ML"

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    .line 65
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v1, v0, v7

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_E:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    aput-object v2, v0, v1

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput-object p3, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->name:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    return-object v0
.end method

.method public static values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->name:Ljava/lang/String;

    return-object v0
.end method
