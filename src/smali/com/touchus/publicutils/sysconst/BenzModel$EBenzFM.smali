.class public final enum Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;
.super Ljava/lang/Enum;
.source "BenzModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/sysconst/BenzModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBenzFM"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

.field public static final enum NAV_AUX:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

.field public static final enum NAV_BT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

.field public static final enum NONAV_AUX:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

.field public static final enum NONAV_BT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;


# instance fields
.field private name:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 217
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    const-string v1, "NAV_BT"

    invoke-direct {v0, v1, v2, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NAV_BT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    .line 218
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    const-string v1, "NAV_AUX"

    invoke-direct {v0, v1, v3, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NAV_AUX:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    .line 219
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    const-string v1, "NONAV_BT"

    invoke-direct {v0, v1, v4, v4}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NONAV_BT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    .line 220
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    const-string v1, "NONAV_AUX"

    invoke-direct {v0, v1, v5, v5}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NONAV_AUX:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    .line 216
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NAV_BT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    aput-object v1, v0, v2

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NAV_AUX:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    aput-object v1, v0, v3

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NONAV_BT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    aput-object v1, v0, v4

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->NONAV_AUX:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    aput-object v1, v0, v5

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "name"    # I

    .prologue
    .line 224
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 225
    iput p3, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->name:I

    .line 226
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    return-object v0
.end method

.method public static values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getName()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;->name:I

    return v0
.end method
