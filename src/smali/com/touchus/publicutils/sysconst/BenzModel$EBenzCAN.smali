.class public final enum Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;
.super Ljava/lang/Enum;
.source "BenzModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/sysconst/BenzModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBenzCAN"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

.field public static final enum WCL:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

.field public static final enum XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

.field public static final enum ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

.field public static final enum ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 197
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    const-string v1, "ZMYT"

    const-string v2, "ZMYT"

    invoke-direct {v0, v1, v3, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    const-string v1, "ZHTD"

    const-string v2, "ZHTD"

    invoke-direct {v0, v1, v4, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    const-string v1, "XBS"

    const-string v2, "XBS"

    invoke-direct {v0, v1, v5, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    const-string v1, "WCL"

    const-string v2, "WCL"

    invoke-direct {v0, v1, v6, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->WCL:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    .line 196
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    aput-object v1, v0, v3

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    aput-object v1, v0, v4

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    aput-object v1, v0, v5

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->WCL:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    aput-object v1, v0, v6

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 202
    iput-object p3, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->name:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    return-object v0
.end method

.method public static values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->name:Ljava/lang/String;

    return-object v0
.end method
