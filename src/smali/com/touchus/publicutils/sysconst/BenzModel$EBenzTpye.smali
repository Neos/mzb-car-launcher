.class public final enum Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
.super Ljava/lang/Enum;
.source "BenzModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/sysconst/BenzModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBenzTpye"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field private static final synthetic ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum E_10:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum E_11:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum E_13:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum E_15:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

.field public static final enum ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "A"

    invoke-direct {v0, v1, v4, v4}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 23
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "B"

    invoke-direct {v0, v1, v5, v5}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 24
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "C"

    invoke-direct {v0, v1, v6, v6}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 25
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "E_10"

    invoke-direct {v0, v1, v7, v7}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_10:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 26
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "E_11"

    invoke-direct {v0, v1, v8, v8}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_11:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 27
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "E_13"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_13:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 28
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "E_15"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_15:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 29
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "GLA"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 30
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "GLC"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 31
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "GLE"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 32
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "GLK"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 33
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    const-string v1, "ML"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 21
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v1, v0, v4

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v1, v0, v5

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v1, v0, v6

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_10:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v1, v0, v7

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_11:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_13:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_15:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    aput-object v2, v0, v1

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->code:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    return-object v0
.end method

.method public static values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->code:I

    int-to-byte v0, v0

    return v0
.end method
