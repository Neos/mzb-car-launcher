.class public final enum Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;
.super Ljava/lang/Enum;
.source "BenzModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/sysconst/BenzModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBenzSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

.field public static final enum size1280_480:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

.field public static final enum size480_800:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    const-string v1, "size480_800"

    invoke-direct {v0, v1, v2, v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->size480_800:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    .line 47
    new-instance v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    const-string v1, "size1280_480"

    invoke-direct {v0, v1, v3, v3}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->size1280_480:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->size480_800:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    aput-object v1, v0, v2

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->size1280_480:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    aput-object v1, v0, v3

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->code:I

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    return-object v0
.end method

.method public static values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->code:I

    int-to-byte v0, v0

    return v0
.end method
