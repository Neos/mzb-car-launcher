.class public Lcom/touchus/publicutils/sysconst/VoiceAssistantConst;
.super Ljava/lang/Object;
.source "VoiceAssistantConst.java"


# static fields
.field public static final EVENT_ADDRESS_BOOK_CHANGE:Ljava/lang/String; = "com.unibroad.voiceassistant.contactupdate"

.field public static final EVENT_ANALYTIC_CMD:Ljava/lang/String; = "com.unibroad.voiceassistant.analyticCmd"

.field public static final EVENT_START_VOICE_ASSISTANT:Ljava/lang/String; = "com.unibroad.voiceassistant.start"

.field public static final EVENT_START_VOICE_ASSISTANT_FROM_OTHER_APP:Ljava/lang/String; = "com.unibroad.voiceassistant.start.other.app"

.field public static final EVENT_STOP_VOICE_ASSISTANT_FROM_OTHER_APP:Ljava/lang/String; = "com.unibroad.voiceassistant.stop_other_app"

.field public static final FLAG_ANALYTIC_CMD:Ljava/lang/String; = "analyticCmd"

.field public static final FLAG_ARG1_STRING:Ljava/lang/String; = "stringArg1"

.field public static final FLAG_ARG2_STRING:Ljava/lang/String; = "stringArg2"

.field public static final FLAG_ARG3_STRING:Ljava/lang/String; = "stringArg3"

.field public static final FLAG_MAIN_APP:Ljava/lang/String; = "main_app"

.field public static final FLAG_MAIN_CARINFO:Ljava/lang/String; = "main_carinfo"

.field public static final FLAG_MAIN_CLOSE:Ljava/lang/String; = "main_close"

.field public static final FLAG_MAIN_CMD:Ljava/lang/String; = "pos"

.field public static final FLAG_MAIN_ECAR:Ljava/lang/String; = "main_ecar"

.field public static final FLAG_MAIN_INSTRUMENT:Ljava/lang/String; = "main_instrument"

.field public static final FLAG_MAIN_MEDIA:Ljava/lang/String; = "main_media"

.field public static final FLAG_MAIN_NAVI:Ljava/lang/String; = "main_navi"

.field public static final FLAG_MAIN_ORIGINAL:Ljava/lang/String; = "main_original"

.field public static final FLAG_MAIN_PHONE:Ljava/lang/String; = "main_phone"

.field public static final FLAG_MAIN_RADIO:Ljava/lang/String; = "main_radio"

.field public static final FLAG_MAIN_RECORD:Ljava/lang/String; = "main_recorder"

.field public static final FLAG_MAIN_SETTING:Ljava/lang/String; = "main_setting"

.field public static final FLAG_MAIN_SMARTCONNECTED:Ljava/lang/String; = "main_smartconnected"

.field public static final FLAG_MAIN_VOICE:Ljava/lang/String; = "main_voice"

.field public static final PERMISSION_ADDRESS_BOOK_CHANGE:Ljava/lang/String; = "com.unibroad.voiceassistant.ADDRESS_BOOK_CHANGE"

.field public static final PERMISSION_ANALYTIC_CMD:Ljava/lang/String; = "com.unibroad.voiceassistant.ANALYTIC_CMD"

.field public static final PERMISSION_START_VOICE_ASSISTANT:Ljava/lang/String; = "com.unibroad.voiceassistant.START"

.field static final PERMISSION_START_VOICE_ASSISTANT_FROM_OTHER_APP:Ljava/lang/String; = "com.unibroad.voiceassistant.start.other.APP"

.field public static final VALUE_APP_BOOK:I = 0x7d3

.field public static final VALUE_APP_LOCAL_MUSIC:I = 0x7d1

.field public static final VALUE_APP_NETRADIO:I = 0x7d4

.field public static final VALUE_APP_NEWINFO:I = 0x7d5

.field public static final VALUE_APP_ONLINE_MUSIC:I = 0x7d2

.field public static final VALUE_CMD_OF_BLUETOOTH_ANWER_CALL:I = 0x3f7

.field public static final VALUE_CMD_OF_BLUETOOTH_CALL:I = 0x3f3

.field public static final VALUE_CMD_OF_BLUETOOTH_CALL_VIEW:I = 0x3f6

.field public static final VALUE_CMD_OF_BLUETOOTH_MISSED_CALL:I = 0x3f4

.field public static final VALUE_CMD_OF_BLUETOOTH_RECEIVE_CALL:I = 0x3f5

.field public static final VALUE_CMD_OF_FAVORITE:I = 0x400

.field public static final VALUE_CMD_OF_NEXT:I = 0x3eb

.field public static final VALUE_CMD_OF_PALY:I = 0x3e9

.field public static final VALUE_CMD_OF_PAUSE:I = 0x3ea

.field public static final VALUE_CMD_OF_PLAY_BY_NAME:I = 0x3ed

.field public static final VALUE_CMD_OF_PLAY_BY_SINGER:I = 0x3ee

.field public static final VALUE_CMD_OF_PLAY_BY_SINGER_AND_NAME:I = 0x3ef

.field public static final VALUE_CMD_OF_PLAY_TYPE_LOOP:I = 0x3fe

.field public static final VALUE_CMD_OF_PLAY_TYPE_RANDOM:I = 0x3ff

.field public static final VALUE_CMD_OF_PLAY_TYPE_SINGLE:I = 0x3fd

.field public static final VALUE_CMD_OF_PREVIOUS:I = 0x3ec

.field public static final VALUE_CMD_OF_RADIO_PLAY_BY_BAND:I = 0x3f2

.field public static final VALUE_CMD_OF_RADIO_PLAY_BY_NAME:I = 0x3f1

.field public static final VALUE_CMD_OF_START_APP:I = 0x3f0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
