.class public Lcom/touchus/publicutils/sysconst/BenzModel;
.super Ljava/lang/Object;
.source "BenzModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;,
        Lcom/touchus/publicutils/sysconst/BenzModel$EBenzFM;,
        Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;,
        Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;,
        Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    }
.end annotation


# static fields
.field public static KEY:Ljava/lang/String;

.field public static SIZE_KEY:Ljava/lang/String;

.field public static benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

.field public static benzSize:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

.field public static benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "benzType"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->KEY:Ljava/lang/String;

    .line 12
    const-string v0, "benzSize"

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->SIZE_KEY:Ljava/lang/String;

    .line 80
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 81
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->size480_800:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzSize:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    .line 174
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static benzName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    .line 153
    :cond_0
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzB()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 155
    :cond_1
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzC()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_2
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_E:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 159
    :cond_3
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLA()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 161
    :cond_4
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 162
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_5
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLC()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 164
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_6
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLE()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 166
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 167
    :cond_7
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzML()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 168
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 170
    :cond_8
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->BENZ_C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;

    invoke-virtual {v0}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzModel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isBenzA()Z
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzB()Z
    .locals 2

    .prologue
    .line 90
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x1

    .line 93
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzC()Z
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzE()Z
    .locals 2

    .prologue
    .line 116
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_10:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_11:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_13:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_15:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    :cond_0
    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzGLA()Z
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzGLC()Z
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x1

    .line 106
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzGLE()Z
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzGLK()Z
    .locals 2

    .prologue
    .line 132
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x1

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBenzML()Z
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    .line 112
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSUV()Z
    .locals 2

    .prologue
    .line 181
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-eq v0, v1, :cond_0

    .line 182
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-eq v0, v1, :cond_0

    .line 183
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-eq v0, v1, :cond_0

    .line 184
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-ne v0, v1, :cond_1

    .line 185
    :cond_0
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
