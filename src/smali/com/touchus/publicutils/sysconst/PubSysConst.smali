.class public Lcom/touchus/publicutils/sysconst/PubSysConst;
.super Ljava/lang/Object;
.source "PubSysConst.java"


# static fields
.field public static ACTION_FACTORY_BREAKSET:Ljava/lang/String;

.field public static DEBUG:Z

.field public static GT9XX_INT_TRIGGER:Ljava/lang/String;

.field public static KEY_BREAKPOS:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    sput-boolean v0, Lcom/touchus/publicutils/sysconst/PubSysConst;->DEBUG:Z

    .line 7
    const-string v0, "com.touchus.factorytest.breakpos"

    sput-object v0, Lcom/touchus/publicutils/sysconst/PubSysConst;->ACTION_FACTORY_BREAKSET:Ljava/lang/String;

    .line 8
    const-string v0, "breakpos"

    sput-object v0, Lcom/touchus/publicutils/sysconst/PubSysConst;->KEY_BREAKPOS:Ljava/lang/String;

    .line 9
    const-string v0, "/sys/bus/i2c/drivers/gt9xx/gt9xx_int_trigger"

    sput-object v0, Lcom/touchus/publicutils/sysconst/PubSysConst;->GT9XX_INT_TRIGGER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
