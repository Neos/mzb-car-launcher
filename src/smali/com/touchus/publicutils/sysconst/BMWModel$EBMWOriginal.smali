.class public final enum Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;
.super Ljava/lang/Enum;
.source "BMWModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/sysconst/BMWModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBMWOriginal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

.field public static final enum EXIST:Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

.field public static final enum UNEXIST:Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    const-string v1, "EXIST"

    invoke-direct {v0, v1, v2, v2}, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->EXIST:Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    .line 41
    new-instance v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    const-string v1, "UNEXIST"

    invoke-direct {v0, v1, v3, v3}, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->UNEXIST:Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->EXIST:Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    aput-object v1, v0, v2

    sget-object v1, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->UNEXIST:Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    aput-object v1, v0, v3

    sput-object v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->code:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    return-object v0
.end method

.method public static values()[Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->ENUM$VALUES:[Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/touchus/publicutils/sysconst/BMWModel$EBMWOriginal;->code:I

    int-to-byte v0, v0

    return v0
.end method
