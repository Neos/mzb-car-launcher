.class public Lcom/touchus/publicutils/bean/MediaBean;
.super Ljava/lang/Object;
.source "MediaBean.java"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private data:Ljava/lang/String;

.field private display_name:Ljava/lang/String;

.field private duration:J

.field private id:J

.field private mime_type:Ljava/lang/String;

.field private size:J

.field private thumbnail_path:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplay_name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->display_name:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->duration:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->id:J

    return-wide v0
.end method

.method public getMime_type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->mime_type:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->size:J

    return-wide v0
.end method

.method public getThumbnail_path()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->thumbnail_path:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/touchus/publicutils/bean/MediaBean;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->bitmap:Landroid/graphics/Bitmap;

    .line 28
    return-void
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->data:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setDisplay_name(Ljava/lang/String;)V
    .locals 0
    .param p1, "display_name"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->display_name:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->duration:J

    .line 84
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 35
    iput-wide p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->id:J

    .line 36
    return-void
.end method

.method public setMime_type(Ljava/lang/String;)V
    .locals 0
    .param p1, "mime_type"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->mime_type:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setSize(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->size:J

    .line 60
    return-void
.end method

.method public setThumbnail_path(Ljava/lang/String;)V
    .locals 0
    .param p1, "thumbnail_path"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->thumbnail_path:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/touchus/publicutils/bean/MediaBean;->title:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "MediaBean [id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-wide v2, p0, Lcom/touchus/publicutils/bean/MediaBean;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 99
    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    iget-object v1, p0, Lcom/touchus/publicutils/bean/MediaBean;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget-object v1, p0, Lcom/touchus/publicutils/bean/MediaBean;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    iget-wide v2, p0, Lcom/touchus/publicutils/bean/MediaBean;->size:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 105
    const-string v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iget-wide v2, p0, Lcom/touchus/publicutils/bean/MediaBean;->duration:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-object v1, p0, Lcom/touchus/publicutils/bean/MediaBean;->data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, ", mime_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    iget-object v1, p0, Lcom/touchus/publicutils/bean/MediaBean;->mime_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string v1, ", bitmap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget-object v1, p0, Lcom/touchus/publicutils/bean/MediaBean;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    const-string v1, ", thumbnail_path="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget-object v1, p0, Lcom/touchus/publicutils/bean/MediaBean;->thumbnail_path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
