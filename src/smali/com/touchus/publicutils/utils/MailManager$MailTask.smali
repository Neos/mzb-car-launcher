.class Lcom/touchus/publicutils/utils/MailManager$MailTask;
.super Landroid/os/AsyncTask;
.source "MailManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/publicutils/utils/MailManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mimeMessage:Ljavax/mail/internet/MimeMessage;

.field final synthetic this$0:Lcom/touchus/publicutils/utils/MailManager;


# direct methods
.method public constructor <init>(Lcom/touchus/publicutils/utils/MailManager;Ljavax/mail/internet/MimeMessage;)V
    .locals 0
    .param p2, "mimeMessage"    # Ljavax/mail/internet/MimeMessage;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/touchus/publicutils/utils/MailManager$MailTask;->this$0:Lcom/touchus/publicutils/utils/MailManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 63
    iput-object p2, p0, Lcom/touchus/publicutils/utils/MailManager$MailTask;->mimeMessage:Ljavax/mail/internet/MimeMessage;

    .line 64
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 5
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 70
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    iget-object v3, p0, Lcom/touchus/publicutils/utils/MailManager$MailTask;->mimeMessage:Ljavax/mail/internet/MimeMessage;

    invoke-static {v3}, Ljavax/mail/Transport;->send(Ljavax/mail/Message;)V

    .line 71
    const-string v3, "FEEDBACK_RESULT"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 73
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "com.unibroad.mail"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 75
    invoke-static {}, Lcom/touchus/publicutils/utils/MailManager;->access$1()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 76
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljavax/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-object v3

    .line 77
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljavax/mail/MessagingException;
    invoke-virtual {v1}, Ljavax/mail/MessagingException;->printStackTrace()V

    .line 79
    const-string v3, "FEEDBACK_RESULT"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 80
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 81
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v3, "com.unibroad.mail"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 83
    invoke-static {}, Lcom/touchus/publicutils/utils/MailManager;->access$1()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 84
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/touchus/publicutils/utils/MailManager$MailTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
