.class public Lcom/touchus/publicutils/utils/MailManager;
.super Ljava/lang/Object;
.source "MailManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/publicutils/utils/MailManager$InstanceHolder;,
        Lcom/touchus/publicutils/utils/MailManager$MailTask;
    }
.end annotation


# static fields
.field private static final KEY_MAIL_AUTH:Ljava/lang/String; = "mail.smtp.auth"

.field private static final KEY_MAIL_HOST:Ljava/lang/String; = "mail.smtp.host"

.field private static final SENDER_NAME:Ljava/lang/String; = "bug@unibroad.com"

.field private static final SENDER_PASS:Ljava/lang/String; = "BUGzero0"

.field private static final VALUE_MAIL_AUTH:Ljava/lang/String; = "true"

.field private static final VALUE_MAIL_HOST:Ljava/lang/String; = "smtp.exmail.qq.com"

.field private static mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/touchus/publicutils/utils/MailManager;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/touchus/publicutils/utils/MailManager;-><init>()V

    return-void
.end method

.method static synthetic access$1()Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/touchus/publicutils/utils/MailManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private appendFile(Ljavax/mail/internet/MimeMessage;Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljavax/mail/internet/MimeMessage;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 167
    :try_start_0
    invoke-virtual {p1}, Ljavax/mail/internet/MimeMessage;->getContent()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljavax/mail/Multipart;

    .line 168
    .local v2, "multipart":Ljavax/mail/Multipart;
    new-instance v1, Ljavax/mail/internet/MimeBodyPart;

    invoke-direct {v1}, Ljavax/mail/internet/MimeBodyPart;-><init>()V

    .line 169
    .local v1, "filePart":Ljavax/mail/internet/MimeBodyPart;
    invoke-virtual {v1, p2}, Ljavax/mail/internet/MimeBodyPart;->attachFile(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v2, v1}, Ljavax/mail/Multipart;->addBodyPart(Ljavax/mail/BodyPart;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 176
    .end local v1    # "filePart":Ljavax/mail/internet/MimeBodyPart;
    .end local v2    # "multipart":Ljavax/mail/Multipart;
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 173
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 174
    .local v0, "e":Ljavax/mail/MessagingException;
    invoke-virtual {v0}, Ljavax/mail/MessagingException;->printStackTrace()V

    goto :goto_0
.end method

.method private appendMultiFile(Ljavax/mail/internet/MimeMessage;Ljava/util/List;)V
    .locals 6
    .param p1, "message"    # Ljavax/mail/internet/MimeMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/mail/internet/MimeMessage;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p2, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {p1}, Ljavax/mail/internet/MimeMessage;->getContent()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljavax/mail/Multipart;

    .line 181
    .local v2, "multipart":Ljavax/mail/Multipart;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 191
    .end local v2    # "multipart":Ljavax/mail/Multipart;
    :goto_1
    return-void

    .line 181
    .restart local v2    # "multipart":Ljavax/mail/Multipart;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 182
    .local v3, "path":Ljava/lang/String;
    new-instance v1, Ljavax/mail/internet/MimeBodyPart;

    invoke-direct {v1}, Ljavax/mail/internet/MimeBodyPart;-><init>()V

    .line 183
    .local v1, "filePart":Ljavax/mail/internet/MimeBodyPart;
    invoke-virtual {v1, v3}, Ljavax/mail/internet/MimeBodyPart;->attachFile(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v2, v1}, Ljavax/mail/Multipart;->addBodyPart(Ljavax/mail/BodyPart;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 186
    .end local v1    # "filePart":Ljavax/mail/internet/MimeBodyPart;
    .end local v2    # "multipart":Ljavax/mail/Multipart;
    .end local v3    # "path":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 188
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 189
    .local v0, "e":Ljavax/mail/MessagingException;
    invoke-virtual {v0}, Ljavax/mail/MessagingException;->printStackTrace()V

    goto :goto_1
.end method

.method private createMessage(Ljava/lang/String;Ljava/lang/String;)Ljavax/mail/internet/MimeMessage;
    .locals 10
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v4

    .line 137
    .local v4, "properties":Ljava/util/Properties;
    const-string v7, "mail.smtp.host"

    const-string v8, "smtp.exmail.qq.com"

    invoke-virtual {v4, v7, v8}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v7, "mail.smtp.auth"

    const-string v8, "true"

    invoke-virtual {v4, v7, v8}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-direct {p0}, Lcom/touchus/publicutils/utils/MailManager;->getAuthenticator()Ljavax/mail/Authenticator;

    move-result-object v7

    invoke-static {v4, v7}, Ljavax/mail/Session;->getInstance(Ljava/util/Properties;Ljavax/mail/Authenticator;)Ljavax/mail/Session;

    move-result-object v5

    .line 140
    .local v5, "session":Ljavax/mail/Session;
    new-instance v2, Ljavax/mail/internet/MimeMessage;

    invoke-direct {v2, v5}, Ljavax/mail/internet/MimeMessage;-><init>(Ljavax/mail/Session;)V

    .line 143
    .local v2, "mimeMessage":Ljavax/mail/internet/MimeMessage;
    :try_start_0
    new-instance v7, Ljavax/mail/internet/InternetAddress;

    const-string v8, "bug@unibroad.com"

    invoke-direct {v7, v8}, Ljavax/mail/internet/InternetAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljavax/mail/internet/MimeMessage;->setFrom(Ljavax/mail/Address;)V

    .line 145
    const/4 v7, 0x1

    new-array v0, v7, [Ljavax/mail/internet/InternetAddress;

    const/4 v7, 0x0

    new-instance v8, Ljavax/mail/internet/InternetAddress;

    const-string v9, "bug@unibroad.com"

    invoke-direct {v8, v9}, Ljavax/mail/internet/InternetAddress;-><init>(Ljava/lang/String;)V

    aput-object v8, v0, v7

    .line 146
    .local v0, "addresses":[Ljavax/mail/internet/InternetAddress;
    sget-object v7, Ljavax/mail/Message$RecipientType;->TO:Ljavax/mail/Message$RecipientType;

    invoke-virtual {v2, v7, v0}, Ljavax/mail/internet/MimeMessage;->setRecipients(Ljavax/mail/Message$RecipientType;[Ljavax/mail/Address;)V

    .line 148
    invoke-virtual {v2, p1}, Ljavax/mail/internet/MimeMessage;->setSubject(Ljava/lang/String;)V

    .line 149
    new-instance v6, Ljavax/mail/internet/MimeBodyPart;

    invoke-direct {v6}, Ljavax/mail/internet/MimeBodyPart;-><init>()V

    .line 151
    .local v6, "textPart":Ljavax/mail/internet/MimeBodyPart;
    const-string v7, "text/html"

    invoke-virtual {v6, p2, v7}, Ljavax/mail/internet/MimeBodyPart;->setContent(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    const-string v7, "UTF-8"

    invoke-virtual {v6, p2, v7}, Ljavax/mail/internet/MimeBodyPart;->setText(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    new-instance v3, Ljavax/mail/internet/MimeMultipart;

    invoke-direct {v3}, Ljavax/mail/internet/MimeMultipart;-><init>()V

    .line 154
    .local v3, "multipart":Ljavax/mail/Multipart;
    invoke-virtual {v3, v6}, Ljavax/mail/Multipart;->addBodyPart(Ljavax/mail/BodyPart;)V

    .line 155
    invoke-virtual {v2, v3}, Ljavax/mail/internet/MimeMessage;->setContent(Ljavax/mail/Multipart;)V

    .line 157
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v7}, Ljavax/mail/internet/MimeMessage;->setSentDate(Ljava/util/Date;)V
    :try_end_0
    .catch Ljavax/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v0    # "addresses":[Ljavax/mail/internet/InternetAddress;
    .end local v3    # "multipart":Ljavax/mail/Multipart;
    .end local v6    # "textPart":Ljavax/mail/internet/MimeBodyPart;
    :goto_0
    return-object v2

    .line 159
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Ljavax/mail/MessagingException;
    invoke-virtual {v1}, Ljavax/mail/MessagingException;->printStackTrace()V

    goto :goto_0
.end method

.method private getAuthenticator()Ljavax/mail/Authenticator;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/touchus/publicutils/utils/MailManager$1;

    invoke-direct {v0, p0}, Lcom/touchus/publicutils/utils/MailManager$1;-><init>(Lcom/touchus/publicutils/utils/MailManager;)V

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/touchus/publicutils/utils/MailManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sput-object p0, Lcom/touchus/publicutils/utils/MailManager;->mContext:Landroid/content/Context;

    .line 48
    invoke-static {}, Lcom/touchus/publicutils/utils/MailManager$InstanceHolder;->access$0()Lcom/touchus/publicutils/utils/MailManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public sendMail(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/touchus/publicutils/utils/MailManager;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljavax/mail/internet/MimeMessage;

    move-result-object v1

    .line 96
    .local v1, "mimeMessage":Ljavax/mail/internet/MimeMessage;
    new-instance v0, Lcom/touchus/publicutils/utils/MailManager$MailTask;

    invoke-direct {v0, p0, v1}, Lcom/touchus/publicutils/utils/MailManager$MailTask;-><init>(Lcom/touchus/publicutils/utils/MailManager;Ljavax/mail/internet/MimeMessage;)V

    .line 97
    .local v0, "mailTask":Lcom/touchus/publicutils/utils/MailManager$MailTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/touchus/publicutils/utils/MailManager$MailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 98
    return-void
.end method

.method public sendMailWithFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "filePath"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/touchus/publicutils/utils/MailManager;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljavax/mail/internet/MimeMessage;

    move-result-object v1

    .line 108
    .local v1, "mimeMessage":Ljavax/mail/internet/MimeMessage;
    invoke-direct {p0, v1, p3}, Lcom/touchus/publicutils/utils/MailManager;->appendFile(Ljavax/mail/internet/MimeMessage;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/touchus/publicutils/utils/MailManager$MailTask;

    invoke-direct {v0, p0, v1}, Lcom/touchus/publicutils/utils/MailManager$MailTask;-><init>(Lcom/touchus/publicutils/utils/MailManager;Ljavax/mail/internet/MimeMessage;)V

    .line 110
    .local v0, "mailTask":Lcom/touchus/publicutils/utils/MailManager$MailTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/touchus/publicutils/utils/MailManager$MailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    return-void
.end method

.method public sendMailWithMultiFile(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p3, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/touchus/publicutils/utils/MailManager;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljavax/mail/internet/MimeMessage;

    move-result-object v1

    .line 121
    .local v1, "mimeMessage":Ljavax/mail/internet/MimeMessage;
    invoke-direct {p0, v1, p3}, Lcom/touchus/publicutils/utils/MailManager;->appendMultiFile(Ljavax/mail/internet/MimeMessage;Ljava/util/List;)V

    .line 122
    new-instance v0, Lcom/touchus/publicutils/utils/MailManager$MailTask;

    invoke-direct {v0, p0, v1}, Lcom/touchus/publicutils/utils/MailManager$MailTask;-><init>(Lcom/touchus/publicutils/utils/MailManager;Ljavax/mail/internet/MimeMessage;)V

    .line 123
    .local v0, "mailTask":Lcom/touchus/publicutils/utils/MailManager$MailTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/touchus/publicutils/utils/MailManager$MailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 124
    return-void
.end method
