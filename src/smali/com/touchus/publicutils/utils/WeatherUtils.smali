.class public Lcom/touchus/publicutils/utils/WeatherUtils;
.super Ljava/lang/Object;
.source "WeatherUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getResId(Landroid/content/Context;Ljava/lang/String;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "weather"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0xb

    .line 20
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 21
    const-string p1, "weather_yin"

    .line 23
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 24
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x6

    if-le v3, v4, :cond_1

    .line 25
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0x12

    .line 24
    if-ge v3, v4, :cond_1

    const/4 v2, 0x1

    .line 26
    .local v2, "isday":Z
    :goto_0
    const-string v3, "\u6674"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 27
    if-eqz v2, :cond_2

    .line 28
    const-string p1, "weather_qing_baitian"

    .line 77
    :goto_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "drawable"

    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 77
    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 81
    :goto_2
    return v3

    .line 24
    .end local v2    # "isday":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 30
    .restart local v2    # "isday":Z
    :cond_2
    const-string p1, "weather_qing_yejian"

    .line 32
    goto :goto_1

    :cond_3
    const-string v3, "\u591a\u4e91"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 33
    if-eqz v2, :cond_4

    .line 34
    const-string p1, "weather_duoyun_baitian"

    .line 35
    goto :goto_1

    .line 36
    :cond_4
    const-string p1, "weather_duoyun_yejian"

    .line 38
    goto :goto_1

    :cond_5
    const-string v3, "\u9634"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 39
    const-string p1, "weather_yin"

    .line 40
    goto :goto_1

    :cond_6
    const-string v3, "\u96fe"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 41
    const-string p1, "weather_wu"

    .line 42
    goto :goto_1

    :cond_7
    const-string v3, "\u5927\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "\u66b4\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 43
    :cond_8
    const-string p1, "weather_dayubaoyu"

    .line 44
    goto :goto_1

    :cond_9
    const-string v3, "\u5c0f\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "\u4e2d\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 45
    :cond_a
    const-string p1, "weather_xiaoyuzhongyu"

    .line 46
    goto :goto_1

    :cond_b
    const-string v3, "\u96f7\u9635\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 47
    const-string p1, "weather_leizhenyu"

    .line 48
    goto :goto_1

    :cond_c
    const-string v3, "\u9635\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 49
    if-eqz v2, :cond_d

    .line 50
    const-string p1, "weather_zhenyu_baitian"

    .line 51
    goto :goto_1

    .line 52
    :cond_d
    const-string p1, "weather_zhenyu_yejian"

    .line 54
    goto :goto_1

    :cond_e
    const-string v3, "\u51bb\u96e8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "\u51b0\u96f9"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 55
    :cond_f
    const-string p1, "weather_dongyu"

    .line 56
    goto/16 :goto_1

    :cond_10
    const-string v3, "\u96e8\u5939\u96ea"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 57
    const-string p1, "weather_yujiaxue"

    .line 58
    goto/16 :goto_1

    :cond_11
    const-string v3, "\u5927\u96ea"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string v3, "\u66b4\u96ea"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 59
    :cond_12
    const-string p1, "weather_daxuebaoxue"

    .line 60
    goto/16 :goto_1

    :cond_13
    const-string v3, "\u5c0f\u96ea"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string v3, "\u4e2d\u96ea"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 61
    :cond_14
    const-string p1, "weather_xiaoxuezhongxue"

    .line 62
    goto/16 :goto_1

    :cond_15
    const-string v3, "\u9635\u96ea"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 63
    if-eqz v2, :cond_16

    .line 64
    const-string p1, "weather_zhenxue_baitian"

    .line 65
    goto/16 :goto_1

    .line 66
    :cond_16
    const-string p1, "weather_zhenxue_yejian"

    .line 68
    goto/16 :goto_1

    :cond_17
    const-string v3, "\u6d6e\u5c18"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string v3, "\u626c\u6c99"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 69
    const-string v3, "\u973e"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 70
    :cond_18
    const-string p1, "weather_mai"

    .line 71
    goto/16 :goto_1

    :cond_19
    const-string v3, "\u6c99\u5c18\u66b4"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 72
    const-string p1, "weather_shachenbao"

    .line 73
    goto/16 :goto_1

    .line 74
    :cond_1a
    const-string p1, "weather_yin"

    goto/16 :goto_1

    .line 79
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 81
    sget v3, Lcom/touchus/publicutils/R$drawable;->weather_yin:I

    goto/16 :goto_2
.end method
