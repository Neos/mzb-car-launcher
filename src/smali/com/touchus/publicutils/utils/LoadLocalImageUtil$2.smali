.class Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;
.super Ljava/lang/Thread;
.source "LoadLocalImageUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->loadDrawable(Lcom/touchus/publicutils/bean/MediaBean;ILandroid/content/Context;Ljava/lang/String;Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$handler:Landroid/os/Handler;

.field private final synthetic val$tag:Ljava/lang/String;

.field private final synthetic val$type:I


# direct methods
.method constructor <init>(Lcom/touchus/publicutils/utils/LoadLocalImageUtil;ILandroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->this$0:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    iput p2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$type:I

    iput-object p3, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$tag:Ljava/lang/String;

    iput-object p5, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$handler:Landroid/os/Handler;

    .line 62
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 66
    iget v2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$type:I

    sget v3, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->MUSIC_TYPE:I

    if-ne v2, v3, :cond_0

    .line 67
    iget-object v2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->access$0(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    .local v0, "bitmip":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->this$0:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    invoke-static {v2}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->access$2(Lcom/touchus/publicutils/utils/LoadLocalImageUtil;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 73
    .local v1, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 74
    return-void

    .line 69
    .end local v0    # "bitmip":Landroid/graphics/Bitmap;
    .end local v1    # "message":Landroid/os/Message;
    :cond_0
    iget-object v2, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->access$1(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "bitmip":Landroid/graphics/Bitmap;
    goto :goto_0
.end method
