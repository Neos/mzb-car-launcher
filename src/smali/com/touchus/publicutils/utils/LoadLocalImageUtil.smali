.class public Lcom/touchus/publicutils/utils/LoadLocalImageUtil;
.super Ljava/lang/Object;
.source "LoadLocalImageUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;
    }
.end annotation


# static fields
.field public static MUSIC_TYPE:I

.field public static VIDEO_TYPE:I

.field private static instance:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

.field private static mediaBean:Lcom/touchus/publicutils/bean/MediaBean;


# instance fields
.field private imageCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x1

    sput v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->MUSIC_TYPE:I

    .line 25
    const/4 v0, 0x2

    sput v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->VIDEO_TYPE:I

    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->instance:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    .line 27
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->imageCache:Ljava/util/HashMap;

    .line 39
    return-void
.end method

.method static synthetic access$0(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 102
    invoke-static {p0}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->getMusicPic(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->getVideoPic(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/publicutils/utils/LoadLocalImageUtil;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->imageCache:Ljava/util/HashMap;

    return-object v0
.end method

.method private static createAlbumArt(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 113
    const/4 v0, 0x0

    .line 115
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 117
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4, p0}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->getEmbeddedPicture()[B

    move-result-object v3

    .line 119
    .local v3, "embedPic":[B
    const/4 v5, 0x0

    array-length v6, v3

    invoke-static {v3, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 124
    :try_start_1
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 129
    .end local v3    # "embedPic":[B
    :goto_0
    return-object v0

    .line 120
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 124
    :try_start_3
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 125
    :catch_1
    move-exception v2

    .line 126
    .local v2, "e2":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 122
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e2":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 124
    :try_start_4
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 128
    :goto_1
    throw v5

    .line 125
    :catch_2
    move-exception v2

    .line 126
    .restart local v2    # "e2":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 125
    .end local v2    # "e2":Ljava/lang/Exception;
    .restart local v3    # "embedPic":[B
    :catch_3
    move-exception v2

    .line 126
    .restart local v2    # "e2":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance()Lcom/touchus/publicutils/utils/LoadLocalImageUtil;
    .locals 2

    .prologue
    .line 29
    const-class v1, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->instance:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    invoke-direct {v0}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;-><init>()V

    sput-object v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->instance:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    .line 32
    :cond_0
    sget-object v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->instance:Lcom/touchus/publicutils/utils/LoadLocalImageUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getMusicPic(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    sget-object v1, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->createAlbumArt(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 104
    .local v0, "bm":Landroid/graphics/Bitmap;
    sget-object v1, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1, v0}, Lcom/touchus/publicutils/bean/MediaBean;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 105
    return-object v0
.end method

.method private static getVideoPic(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 88
    sget-object v2, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v2}, Lcom/touchus/publicutils/bean/MediaBean;->getId()J

    move-result-wide v2

    .line 89
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 86
    invoke-static {v1, v2, v3, v4, v5}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    sget-object v1, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1, v0}, Lcom/touchus/publicutils/bean/MediaBean;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 91
    return-object v0
.end method


# virtual methods
.method public loadDrawable(Lcom/touchus/publicutils/bean/MediaBean;ILandroid/content/Context;Ljava/lang/String;Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "bean"    # Lcom/touchus/publicutils/bean/MediaBean;
    .param p2, "type"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "tag"    # Ljava/lang/String;
    .param p5, "imageCallback"    # Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;

    .prologue
    .line 43
    sput-object p1, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    .line 44
    iget-object v0, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->imageCache:Ljava/util/HashMap;

    invoke-virtual {v0, p4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->imageCache:Ljava/util/HashMap;

    invoke-virtual {v0, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/ref/SoftReference;

    .line 46
    .local v7, "softReference":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    invoke-virtual {v7}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 47
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_1

    .line 48
    sget v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->MUSIC_TYPE:I

    if-ne p2, v0, :cond_0

    .line 49
    sget-object v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v0, v6}, Lcom/touchus/publicutils/bean/MediaBean;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 76
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "softReference":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    :goto_0
    return-object v6

    .line 51
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v7    # "softReference":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    :cond_0
    sget-object v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->mediaBean:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v0, v6}, Lcom/touchus/publicutils/bean/MediaBean;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 56
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    .end local v7    # "softReference":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    :cond_1
    new-instance v5, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$1;

    invoke-direct {v5, p0, p5, p4}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$1;-><init>(Lcom/touchus/publicutils/utils/LoadLocalImageUtil;Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;Ljava/lang/String;)V

    .line 62
    .local v5, "handler":Landroid/os/Handler;
    new-instance v0, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;

    move-object v1, p0

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;-><init>(Lcom/touchus/publicutils/utils/LoadLocalImageUtil;ILandroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V

    .line 75
    invoke-virtual {v0}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil$2;->start()V

    .line 76
    const/4 v6, 0x0

    goto :goto_0
.end method
