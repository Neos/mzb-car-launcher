.class Lcom/touchus/benchilauncher/MainService$13;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 7
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 1493
    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    .line 1494
    :try_start_0
    move-object v0, p2

    check-cast v0, Landroid/bluetooth/BluetoothHeadset;

    move-object v3, v0

    .line 1495
    .local v3, "headset":Landroid/bluetooth/BluetoothHeadset;
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4, v3}, Lcom/touchus/benchilauncher/MainService;->access$28(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothHeadset;)V

    .line 1510
    .end local v3    # "headset":Landroid/bluetooth/BluetoothHeadset;
    :cond_0
    :goto_0
    return-void

    .line 1497
    :cond_1
    const/4 v4, 0x2

    if-ne p1, v4, :cond_0

    .line 1498
    move-object v0, p2

    check-cast v0, Landroid/bluetooth/BluetoothA2dp;

    move-object v1, v0

    .line 1499
    .local v1, "a2dp":Landroid/bluetooth/BluetoothA2dp;
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4, v1}, Lcom/touchus/benchilauncher/MainService;->access$29(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothA2dp;)V

    .line 1500
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1501
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "btstate a2dp connect = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v6}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1502
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$27(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothA2dp;

    move-result-object v4

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$13;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothA2dp;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1506
    .end local v1    # "a2dp":Landroid/bluetooth/BluetoothA2dp;
    :catch_0
    move-exception v2

    .line 1507
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "debug"

    .line 1508
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "uuid BOND_ BluetoothProfile error"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1507
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 0
    .param p1, "profile"    # I

    .prologue
    .line 1488
    return-void
.end method
