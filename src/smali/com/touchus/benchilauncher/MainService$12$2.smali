.class Lcom/touchus/benchilauncher/MainService$12$2;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/MainService$12;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/touchus/benchilauncher/MainService$12;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService$12;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$12$2;->this$1:Lcom/touchus/benchilauncher/MainService$12;

    .line 1320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$12$2;->this$1:Lcom/touchus/benchilauncher/MainService$12;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService$12;->access$0(Lcom/touchus/benchilauncher/MainService$12;)Lcom/touchus/benchilauncher/MainService;

    move-result-object v0

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$12$2;->this$1:Lcom/touchus/benchilauncher/MainService$12;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService$12;->access$0(Lcom/touchus/benchilauncher/MainService$12;)Lcom/touchus/benchilauncher/MainService;

    move-result-object v0

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1325
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "btstate a2dp connect = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$12$2;->this$1:Lcom/touchus/benchilauncher/MainService$12;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService$12;->access$0(Lcom/touchus/benchilauncher/MainService$12;)Lcom/touchus/benchilauncher/MainService;

    move-result-object v2

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$12$2;->this$1:Lcom/touchus/benchilauncher/MainService$12;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService$12;->access$0(Lcom/touchus/benchilauncher/MainService$12;)Lcom/touchus/benchilauncher/MainService;

    move-result-object v0

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$27(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothA2dp;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$12$2;->this$1:Lcom/touchus/benchilauncher/MainService$12;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService$12;->access$0(Lcom/touchus/benchilauncher/MainService$12;)Lcom/touchus/benchilauncher/MainService;

    move-result-object v1

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothA2dp;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    .line 1328
    :cond_0
    return-void
.end method
