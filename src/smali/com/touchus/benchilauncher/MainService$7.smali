.class Lcom/touchus/benchilauncher/MainService$7;
.super Landroid/content/BroadcastReceiver;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 981
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 985
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const-string v3, "cityName"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->currentCityName:Ljava/lang/String;

    .line 986
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const-string v3, "cityWeather"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->weatherTempDes:Ljava/lang/String;

    .line 987
    const-string v2, "highTemperature"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 988
    .local v0, "hi":Ljava/lang/String;
    const-string v2, "lowTemperature"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 990
    .local v1, "low":Ljava/lang/String;
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\u2103"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->weatherTemp:Ljava/lang/String;

    .line 991
    const-string v2, "voice"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Weather app.weatherTempDes = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->weatherTempDes:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 992
    const-string v4, ",app.weatherTemp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->weatherTemp:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 991
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$7;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/16 v3, 0x3f2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 994
    return-void
.end method
