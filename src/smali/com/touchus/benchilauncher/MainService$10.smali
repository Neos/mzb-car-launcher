.class Lcom/touchus/benchilauncher/MainService$10;
.super Landroid/content/BroadcastReceiver;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1081
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 1085
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1086
    .local v0, "action":Ljava/lang/String;
    const-string v1, "net.easyconn.bt.checkstatus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1087
    const-string v1, "easyConnect"

    const-string v2, "easyConnect checkstatus"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyView:Z

    .line 1089
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v1, :cond_1

    .line 1090
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    const-string v2, "net.easyconn.bt.connected"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->sendEasyConnectBroadcast(Ljava/lang/String;)V

    .line 1091
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestMusicAudioFocus()V

    .line 1111
    :cond_0
    :goto_0
    return-void

    .line 1093
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->openOrCloseBluetooth(Z)V

    .line 1094
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    const-string v2, "net.easyconn.bt.opened"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->sendEasyConnectBroadcast(Ljava/lang/String;)V

    .line 1095
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    const-string v2, "net.easyconn.bt.unconnected"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->sendEasyConnectBroadcast(Ljava/lang/String;)V

    goto :goto_0

    .line 1097
    :cond_2
    const-string v1, "net.easyconn.a2dp.acquire"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1098
    const-string v1, "easyConnect"

    const-string v2, "easyConnect a2dp acquire"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v1, :cond_0

    .line 1100
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestMusicAudioFocus()V

    goto :goto_0

    .line 1102
    :cond_3
    const-string v1, "net.easyconn.a2dp.release"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1103
    const-string v1, "easyConnect"

    const-string v2, "easyConnect a2dp release"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->stopBTMusic()V

    goto :goto_0

    .line 1105
    :cond_4
    const-string v1, "net.easyconn.app.quit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1106
    const-string v1, "easyConnect"

    const-string v2, "easyConnect quit"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$10;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyView:Z

    goto :goto_0

    .line 1109
    :cond_5
    const-string v1, "easyConnect"

    const-string v2, "easyConnect no"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
