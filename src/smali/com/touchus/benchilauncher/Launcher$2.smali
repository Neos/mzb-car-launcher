.class Lcom/touchus/benchilauncher/Launcher$2;
.super Ljava/lang/Object;
.source "Launcher.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/Launcher;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/Launcher;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/Launcher$2;->this$0:Lcom/touchus/benchilauncher/Launcher;

    .line 541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher$2;->this$0:Lcom/touchus/benchilauncher/Launcher;

    invoke-static {v0}, Lcom/touchus/benchilauncher/Launcher;->access$0(Lcom/touchus/benchilauncher/Launcher;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    check-cast p2, Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;

    .line 545
    .end local p2    # "binder":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;->getService()Lcom/touchus/benchilauncher/service/BluetoothService;

    move-result-object v1

    .line 544
    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 546
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher$2;->this$0:Lcom/touchus/benchilauncher/Launcher;

    invoke-static {v0}, Lcom/touchus/benchilauncher/Launcher;->access$1(Lcom/touchus/benchilauncher/Launcher;)V

    .line 547
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 551
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher$2;->this$0:Lcom/touchus/benchilauncher/Launcher;

    invoke-static {v0}, Lcom/touchus/benchilauncher/Launcher;->access$0(Lcom/touchus/benchilauncher/Launcher;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 552
    return-void
.end method
