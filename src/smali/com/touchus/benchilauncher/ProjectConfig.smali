.class public Lcom/touchus/benchilauncher/ProjectConfig;
.super Ljava/lang/Object;
.source "ProjectConfig.java"


# static fields
.field public static APPSoft:Ljava/lang/String; = null

.field public static CANSoft:Ljava/lang/String; = null

.field public static final CONFIG_URL:Ljava/lang/String; = "http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh"

.field public static FactoryPwd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static MCUSoft:Ljava/lang/String;

.field public static projectName:Ljava/lang/String;

.field public static systemSoft:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    const-string v0, "TYPE\uff1aXHD_"

    sput-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->projectName:Ljava/lang/String;

    .line 9
    const-string v0, "c200_en-ota-user.smartpie.zip"

    sput-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->systemSoft:Ljava/lang/String;

    .line 10
    const-string v0, "BENZ_MCU.BIN"

    sput-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->MCUSoft:Ljava/lang/String;

    .line 11
    const-string v0, "can_app.bin"

    sput-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->CANSoft:Ljava/lang/String;

    .line 12
    const-string v0, "C200_EN_BCLauncher.apk"

    sput-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->APPSoft:Ljava/lang/String;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->FactoryPwd:Ljava/util/List;

    .line 16
    sget-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->FactoryPwd:Ljava/util/List;

    const-string v1, "111111"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    sget-object v0, Lcom/touchus/benchilauncher/ProjectConfig;->FactoryPwd:Ljava/util/List;

    const-string v1, "2109"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
