.class public final enum Lcom/touchus/benchilauncher/SysConst$DataTpye;
.super Ljava/lang/Enum;
.source "SysConst.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/SysConst;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DataTpye"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/benchilauncher/SysConst$DataTpye;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BT_PHONE_VOLUME:Lcom/touchus/benchilauncher/SysConst$DataTpye;

.field private static final synthetic ENUM$VALUES:[Lcom/touchus/benchilauncher/SysConst$DataTpye;

.field public static final enum LANGUAGE:Lcom/touchus/benchilauncher/SysConst$DataTpye;

.field public static final enum NAVI_VOLUME:Lcom/touchus/benchilauncher/SysConst$DataTpye;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 272
    new-instance v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;

    const-string v1, "NAVI_VOLUME"

    invoke-direct {v0, v1, v4, v2}, Lcom/touchus/benchilauncher/SysConst$DataTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->NAVI_VOLUME:Lcom/touchus/benchilauncher/SysConst$DataTpye;

    new-instance v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;

    const-string v1, "BT_PHONE_VOLUME"

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/SysConst$DataTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->BT_PHONE_VOLUME:Lcom/touchus/benchilauncher/SysConst$DataTpye;

    new-instance v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;

    const-string v1, "LANGUAGE"

    invoke-direct {v0, v1, v3, v5}, Lcom/touchus/benchilauncher/SysConst$DataTpye;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->LANGUAGE:Lcom/touchus/benchilauncher/SysConst$DataTpye;

    .line 271
    new-array v0, v5, [Lcom/touchus/benchilauncher/SysConst$DataTpye;

    sget-object v1, Lcom/touchus/benchilauncher/SysConst$DataTpye;->NAVI_VOLUME:Lcom/touchus/benchilauncher/SysConst$DataTpye;

    aput-object v1, v0, v4

    sget-object v1, Lcom/touchus/benchilauncher/SysConst$DataTpye;->BT_PHONE_VOLUME:Lcom/touchus/benchilauncher/SysConst$DataTpye;

    aput-object v1, v0, v2

    sget-object v1, Lcom/touchus/benchilauncher/SysConst$DataTpye;->LANGUAGE:Lcom/touchus/benchilauncher/SysConst$DataTpye;

    aput-object v1, v0, v3

    sput-object v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->ENUM$VALUES:[Lcom/touchus/benchilauncher/SysConst$DataTpye;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 276
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 277
    iput p3, p0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->code:I

    .line 278
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/benchilauncher/SysConst$DataTpye;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;

    return-object v0
.end method

.method public static values()[Lcom/touchus/benchilauncher/SysConst$DataTpye;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->ENUM$VALUES:[Lcom/touchus/benchilauncher/SysConst$DataTpye;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/benchilauncher/SysConst$DataTpye;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lcom/touchus/benchilauncher/SysConst$DataTpye;->code:I

    int-to-byte v0, v0

    return v0
.end method
