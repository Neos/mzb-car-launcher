.class public Lcom/touchus/benchilauncher/bean/SmsInfo;
.super Ljava/lang/Object;
.source "SmsInfo.java"


# static fields
.field public static TYPE_AD:I

.field public static TYPE_ALL:I

.field public static TYPE_LOCATE:I

.field public static TYPE_SMS:I


# instance fields
.field private data1:Ljava/lang/String;

.field private data2:Ljava/lang/String;

.field private hadRead:Z

.field private id:Ljava/lang/String;

.field private inEdit:Z

.field private name:Ljava/lang/String;

.field private sender:Ljava/lang/String;

.field private time:J

.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x1

    sput v0, Lcom/touchus/benchilauncher/bean/SmsInfo;->TYPE_LOCATE:I

    .line 7
    const/4 v0, 0x2

    sput v0, Lcom/touchus/benchilauncher/bean/SmsInfo;->TYPE_AD:I

    .line 9
    const/4 v0, 0x3

    sput v0, Lcom/touchus/benchilauncher/bean/SmsInfo;->TYPE_SMS:I

    .line 11
    const/4 v0, 0x4

    sput v0, Lcom/touchus/benchilauncher/bean/SmsInfo;->TYPE_ALL:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->hadRead:Z

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->inEdit:Z

    .line 3
    return-void
.end method


# virtual methods
.method public getData1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->data1:Ljava/lang/String;

    return-object v0
.end method

.method public getData2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->data2:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->sender:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->time:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->type:I

    return v0
.end method

.method public isHadRead()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->hadRead:Z

    return v0
.end method

.method public isInEdit()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->inEdit:Z

    return v0
.end method

.method public setData1(Ljava/lang/String;)V
    .locals 0
    .param p1, "data1"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->data1:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setData2(Ljava/lang/String;)V
    .locals 0
    .param p1, "data2"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->data2:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setHadRead(Z)V
    .locals 0
    .param p1, "hadRead"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->hadRead:Z

    .line 54
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->id:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setInEdit(Z)V
    .locals 0
    .param p1, "inEdit"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->inEdit:Z

    .line 60
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->name:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setSender(Ljava/lang/String;)V
    .locals 0
    .param p1, "sender"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->sender:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->time:J

    .line 48
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/touchus/benchilauncher/bean/SmsInfo;->type:I

    .line 66
    return-void
.end method
