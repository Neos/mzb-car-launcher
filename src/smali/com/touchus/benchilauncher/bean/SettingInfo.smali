.class public Lcom/touchus/benchilauncher/bean/SettingInfo;
.super Ljava/lang/Object;
.source "SettingInfo.java"


# instance fields
.field private checked:Z

.field private isCheckItem:Z

.field private item:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "item"    # Ljava/lang/String;
    .param p2, "isCheckItem"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->item:Ljava/lang/String;

    .line 22
    iput-boolean p2, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->isCheckItem:Z

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "item"    # Ljava/lang/String;
    .param p2, "isCheckItem"    # Z
    .param p3, "checked"    # Z

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->item:Ljava/lang/String;

    .line 27
    iput-boolean p2, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->isCheckItem:Z

    .line 28
    iput-boolean p3, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->checked:Z

    .line 29
    return-void
.end method


# virtual methods
.method public getItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->item:Ljava/lang/String;

    return-object v0
.end method

.method public isCheckItem()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->isCheckItem:Z

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->checked:Z

    return v0
.end method

.method public setCheckItem(Z)V
    .locals 0
    .param p1, "isCheckItem"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->isCheckItem:Z

    .line 18
    return-void
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->checked:Z

    .line 35
    return-void
.end method

.method public setItem(Ljava/lang/String;)V
    .locals 0
    .param p1, "item"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/SettingInfo;->item:Ljava/lang/String;

    .line 12
    return-void
.end method
