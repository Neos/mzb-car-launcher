.class public Lcom/touchus/benchilauncher/bean/BeanWifi;
.super Ljava/lang/Object;
.source "BeanWifi.java"


# static fields
.field public static final CONNECT:I = 0x1

.field public static final CONNECTING:I = 0x2

.field public static final UNCONNECT:I = 0x3


# instance fields
.field private capabilities:Ljava/lang/String;

.field private level:I

.field private name:Ljava/lang/String;

.field private pwd:Z

.field private saveInlocal:Z

.field private state:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCapabilities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->capabilities:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->level:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->state:I

    return v0
.end method

.method public isPwd()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->pwd:Z

    return v0
.end method

.method public isSaveInlocal()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->saveInlocal:Z

    return v0
.end method

.method public setCapabilities(Ljava/lang/String;)V
    .locals 0
    .param p1, "capabilities"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->capabilities:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->level:I

    .line 89
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->name:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setPwd(Z)V
    .locals 0
    .param p1, "pwd"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->pwd:Z

    .line 61
    return-void
.end method

.method public setSaveInlocal(Z)V
    .locals 0
    .param p1, "saveInlocal"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->saveInlocal:Z

    .line 33
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/touchus/benchilauncher/bean/BeanWifi;->state:I

    .line 75
    return-void
.end method
