.class public Lcom/touchus/benchilauncher/bean/MenuInfo;
.super Ljava/lang/Object;
.source "MenuInfo.java"


# instance fields
.field private bigIconRes:I

.field private nameRes:I

.field private smallIconRes:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "nameRes"    # I
    .param p2, "bigIconRes"    # I
    .param p3, "smallIconRes"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->nameRes:I

    .line 41
    iput p2, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->bigIconRes:I

    .line 42
    iput p3, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->smallIconRes:I

    .line 43
    return-void
.end method


# virtual methods
.method public getBigIconRes()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->bigIconRes:I

    return v0
.end method

.method public getNameRes()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->nameRes:I

    return v0
.end method

.method public getSmallIconRes()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->smallIconRes:I

    return v0
.end method

.method public setBigIconRes(I)V
    .locals 0
    .param p1, "bigIconRes"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->bigIconRes:I

    .line 28
    return-void
.end method

.method public setNameRes(I)V
    .locals 0
    .param p1, "nameRes"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->nameRes:I

    .line 20
    return-void
.end method

.method public setSmallIconRes(I)V
    .locals 0
    .param p1, "smallIconRes"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/touchus/benchilauncher/bean/MenuInfo;->smallIconRes:I

    .line 36
    return-void
.end method
