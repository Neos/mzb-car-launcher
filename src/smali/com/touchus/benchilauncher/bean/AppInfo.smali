.class public Lcom/touchus/benchilauncher/bean/AppInfo;
.super Ljava/lang/Object;
.source "AppInfo.java"


# instance fields
.field private appIcon:Landroid/graphics/drawable/Drawable;

.field private appName:Ljava/lang/String;

.field private iCheck:Z

.field private packageName:Ljava/lang/String;

.field private versionCode:I

.field private versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->appName:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->packageName:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->versionName:Ljava/lang/String;

    .line 9
    iput v1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->versionCode:I

    .line 10
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->iCheck:Z

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->appIcon:Landroid/graphics/drawable/Drawable;

    .line 5
    return-void
.end method


# virtual methods
.method public getAppIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->appIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->versionCode:I

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method public isiCheck()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->iCheck:Z

    return v0
.end method

.method public setAppIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "appIcon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->appIcon:Landroid/graphics/drawable/Drawable;

    .line 41
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->appName:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->packageName:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setVersionCode(I)V
    .locals 0
    .param p1, "versionCode"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->versionCode:I

    .line 35
    return-void
.end method

.method public setVersionName(Ljava/lang/String;)V
    .locals 0
    .param p1, "versionName"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->versionName:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setiCheck(Z)V
    .locals 0
    .param p1, "iCheck"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/bean/AppInfo;->iCheck:Z

    .line 47
    return-void
.end method
