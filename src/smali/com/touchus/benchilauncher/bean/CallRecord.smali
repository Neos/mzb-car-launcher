.class public Lcom/touchus/benchilauncher/bean/CallRecord;
.super Ljava/lang/Object;
.source "CallRecord.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private callName:Ljava/lang/String;

.field private callState:I

.field private callTime:Ljava/lang/String;

.field private callphoneNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCallName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callName:Ljava/lang/String;

    return-object v0
.end method

.method public getCallState()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callState:I

    return v0
.end method

.method public getCallTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callTime:Ljava/lang/String;

    return-object v0
.end method

.method public getCallphoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callphoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public setCallName(Ljava/lang/String;)V
    .locals 0
    .param p1, "callName"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callName:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setCallState(I)V
    .locals 0
    .param p1, "callState"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callState:I

    .line 60
    return-void
.end method

.method public setCallTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "callTime"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callTime:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setCallphoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "callphoneNumber"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/CallRecord;->callphoneNumber:Ljava/lang/String;

    .line 44
    return-void
.end method
