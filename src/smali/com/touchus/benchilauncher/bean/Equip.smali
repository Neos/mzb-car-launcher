.class public Lcom/touchus/benchilauncher/bean/Equip;
.super Ljava/lang/Object;
.source "Equip.java"


# instance fields
.field private address:Ljava/lang/String;

.field private index:I

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/touchus/benchilauncher/bean/Equip;->index:I

    .line 15
    iput-object p2, p0, Lcom/touchus/benchilauncher/bean/Equip;->name:Ljava/lang/String;

    .line 16
    iput-object p3, p0, Lcom/touchus/benchilauncher/bean/Equip;->address:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Equip;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/touchus/benchilauncher/bean/Equip;->index:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Equip;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Equip;->address:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/touchus/benchilauncher/bean/Equip;->index:I

    .line 25
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Equip;->name:Ljava/lang/String;

    .line 33
    return-void
.end method
