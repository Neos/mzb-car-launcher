.class public Lcom/touchus/benchilauncher/bean/Music;
.super Ljava/lang/Object;
.source "Music.java"


# instance fields
.field private data:Ljava/lang/String;

.field private display_name:Ljava/lang/String;

.field private id:I

.field private mime_type:Ljava/lang/String;

.field private size:I

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Music;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplay_name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Music;->display_name:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/touchus/benchilauncher/bean/Music;->id:I

    return v0
.end method

.method public getMime_type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Music;->mime_type:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/touchus/benchilauncher/bean/Music;->size:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Music;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Music;->data:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setDisplay_name(Ljava/lang/String;)V
    .locals 0
    .param p1, "display_name"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Music;->display_name:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/touchus/benchilauncher/bean/Music;->id:I

    .line 23
    return-void
.end method

.method public setMime_type(Ljava/lang/String;)V
    .locals 0
    .param p1, "mime_type"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Music;->mime_type:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/touchus/benchilauncher/bean/Music;->size:I

    .line 41
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Music;->title:Ljava/lang/String;

    .line 35
    return-void
.end method
