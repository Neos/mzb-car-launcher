.class public Lcom/touchus/benchilauncher/bean/Person;
.super Ljava/lang/Object;
.source "Person.java"


# instance fields
.field private flag:I

.field private id:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private phone:Ljava/lang/String;

.field private remark:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "phone"    # Ljava/lang/String;
    .param p4, "flag"    # I
    .param p5, "remark"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Person;->id:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/touchus/benchilauncher/bean/Person;->name:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/touchus/benchilauncher/bean/Person;->phone:Ljava/lang/String;

    .line 19
    iput p4, p0, Lcom/touchus/benchilauncher/bean/Person;->flag:I

    .line 20
    iput-object p5, p0, Lcom/touchus/benchilauncher/bean/Person;->remark:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getFlag()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/touchus/benchilauncher/bean/Person;->flag:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Person;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Person;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Person;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public getRemark()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/bean/Person;->remark:Ljava/lang/String;

    return-object v0
.end method

.method public setFlag(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/touchus/benchilauncher/bean/Person;->flag:I

    .line 53
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Person;->id:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Person;->name:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setPhone(Ljava/lang/String;)V
    .locals 0
    .param p1, "phone"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Person;->phone:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setRemark(Ljava/lang/String;)V
    .locals 0
    .param p1, "remark"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/touchus/benchilauncher/bean/Person;->remark:Ljava/lang/String;

    .line 61
    return-void
.end method
