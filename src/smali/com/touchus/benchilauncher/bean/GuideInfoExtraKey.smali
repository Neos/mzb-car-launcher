.class public Lcom/touchus/benchilauncher/bean/GuideInfoExtraKey;
.super Ljava/lang/Object;
.source "GuideInfoExtraKey.java"


# static fields
.field public static final CAMERA_DIST:Ljava/lang/String; = "CAMERA_DIST"

.field public static final CAMERA_INDEX:Ljava/lang/String; = "CAMERA_INDEX"

.field public static final CAMERA_SPEED:Ljava/lang/String; = "CAMERA_SPEED"

.field public static final CAMERA_TYPE:Ljava/lang/String; = "CAMERA_TYPE"

.field public static final CAR_DIRECTION:Ljava/lang/String; = "CAR_DIRECTION"

.field public static final CAR_LATITUDE:Ljava/lang/String; = "CAR_LATITUDE"

.field public static final CAR_LONGITUDE:Ljava/lang/String; = "CAR_LONGITUDE"

.field public static final CUR_POINT_NUM:Ljava/lang/String; = "CUR_POINT_NUM"

.field public static final CUR_ROAD_NAME:Ljava/lang/String; = "CUR_ROAD_NAME"

.field public static final CUR_SEG_NUM:Ljava/lang/String; = "CUR_SEG_NUM"

.field public static final CUR_SPEED:Ljava/lang/String; = "CUR_SPEED"

.field public static final ICON:Ljava/lang/String; = "ICON"

.field public static final LIMITED_SPEED:Ljava/lang/String; = "LIMITED_SPEED"

.field public static final NEXT_ROAD_NAME:Ljava/lang/String; = "NEXT_ROAD_NAME"

.field public static final ROAD_TYPE:Ljava/lang/String; = "ROAD_TYPE"

.field public static final ROUND_ABOUT_NUM:Ljava/lang/String; = "ROUNG_ABOUT_NUM"

.field public static final ROUND_ALL_NUM:Ljava/lang/String; = "ROUND_ALL_NUM"

.field public static final ROUTE_ALL_DIS:Ljava/lang/String; = "ROUTE_ALL_DIS"

.field public static final ROUTE_ALL_TIME:Ljava/lang/String; = "ROUTE_ALL_TIME"

.field public static final ROUTE_REMAIN_DIS:Ljava/lang/String; = "ROUTE_REMAIN_DIS"

.field public static final ROUTE_REMAIN_TIME:Ljava/lang/String; = "ROUTE_REMAIN_TIME"

.field public static final SAPA_DIST:Ljava/lang/String; = "SAPA_DIST"

.field public static final SAPA_NAME:Ljava/lang/String; = "SAPA_NAME"

.field public static final SAPA_NUM:Ljava/lang/String; = "SAPA_NUM"

.field public static final SAPA_TYPE:Ljava/lang/String; = "SAPA_TYPE"

.field public static final SEG_REMAIN_DIS:Ljava/lang/String; = "SEG_REMAIN_DIS"

.field public static final SEG_REMAIN_TIME:Ljava/lang/String; = "SEG_REMAIN_TIME"

.field public static final TRAFFIC_LIGHT_NUM:Ljava/lang/String; = "TRAFFIC_LIGHT_NUM"

.field public static final TYPE:Ljava/lang/String; = "TYPE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
