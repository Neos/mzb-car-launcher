.class Lcom/touchus/benchilauncher/LauncherApplication$1;
.super Landroid/os/Handler;
.source "LauncherApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/LauncherApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/LauncherApplication;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/LauncherApplication;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 154
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 158
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FloatSystemCallDialog handleMessage  msg.what:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 159
    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 163
    :sswitch_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->interAndroidView()V

    .line 164
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 165
    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v2, 0x584

    if-ne v1, v2, :cond_0

    .line 167
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    .line 168
    .local v0, "floatCallingDialog":Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getShowStatus()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    move-result-object v1

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->INCOMING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    if-ne v1, v2, :cond_0

    .line 169
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 171
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getCallingPhonenumber()Ljava/lang/String;

    move-result-object v2

    .line 170
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 171
    if-nez v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setCallingPhonenumber(Ljava/lang/String;)V

    .line 173
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->clearView()V

    .line 174
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->enterSystemFloatCallView()V

    goto :goto_0

    .line 179
    .end local v0    # "floatCallingDialog":Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
    :sswitch_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 180
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->updateTalkStatus()V

    goto :goto_0

    .line 183
    :sswitch_2
    sget-boolean v1, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 188
    :goto_1
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getShowStatus()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    move-result-object v1

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    if-ne v1, v2, :cond_0

    .line 189
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->freshCallingTime()V

    goto :goto_0

    .line 186
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v4, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    goto :goto_1

    .line 193
    :sswitch_3
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v4, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    .line 194
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v4, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 195
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/backaudio/android/driver/Mainboard;->openOrCloseRelay(Z)V

    goto :goto_0

    .line 198
    :sswitch_4
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyView:Z

    if-eqz v1, :cond_2

    .line 199
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyBtConnect:Z

    if-nez v1, :cond_3

    .line 200
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyBtConnect:Z

    .line 201
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const-string v2, "net.easyconn.bt.connected"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->sendEasyConnectBroadcast(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestMusicAudioFocus()V

    .line 211
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v4, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 212
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isDestory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 213
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->dissShow()V

    .line 214
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->clearView()V

    goto/16 :goto_0

    .line 203
    :cond_3
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyBtConnect:Z

    if-eqz v1, :cond_2

    .line 204
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v4, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyBtConnect:Z

    .line 205
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->openOrCloseBluetooth(Z)V

    .line 206
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const-string v2, "net.easyconn.bt.opened"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->sendEasyConnectBroadcast(Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const-string v2, "net.easyconn.bt.unconnected"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->sendEasyConnectBroadcast(Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->stopBTMusic()V

    goto :goto_2

    .line 219
    :sswitch_5
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v1, v2, :cond_4

    .line 220
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v1, v2, :cond_4

    .line 221
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_0

    .line 222
    :cond_4
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    if-nez v1, :cond_5

    .line 223
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication$1;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->enterSystemFloatCallView()V

    goto/16 :goto_0

    .line 225
    :cond_5
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->clearView()V

    goto/16 :goto_0

    .line 160
    nop

    :sswitch_data_0
    .sparse-switch
        0x583 -> :sswitch_1
        0x587 -> :sswitch_0
        0x588 -> :sswitch_3
        0x589 -> :sswitch_2
        0x58e -> :sswitch_5
        0x1772 -> :sswitch_4
    .end sparse-switch
.end method
