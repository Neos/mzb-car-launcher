.class Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;
.super Ljava/lang/Object;
.source "FragmentVideoPlay.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    .line 910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 912
    packed-switch p1, :pswitch_data_0

    .line 947
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 917
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 919
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$5(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V

    .line 920
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$6(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 925
    :pswitch_2
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$7(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v0, :cond_2

    .line 926
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    sget v2, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 928
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 930
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$5(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V

    .line 931
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$6(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 937
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$7(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 938
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    sget v2, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 939
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$8(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 940
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 941
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$6(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 942
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0, v3}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$5(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V

    goto/16 :goto_0

    .line 912
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
