.class Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "FragmentYingyong.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/fragment/FragmentYingyong;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppPagerAdapter"
.end annotation


# instance fields
.field private listData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/GridView;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentYingyong;


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/GridView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p3, "_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/GridView;>;"
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentYingyong;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 326
    iput-object p2, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->mContext:Landroid/content/Context;

    .line 327
    iput-object p3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->listData:Ljava/util/ArrayList;

    .line 328
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 342
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 343
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 347
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 348
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 337
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
