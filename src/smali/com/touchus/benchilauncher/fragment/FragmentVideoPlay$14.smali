.class Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;
.super Ljava/lang/Object;
.source "FragmentVideoPlay.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 394
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 388
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 377
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v3}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 378
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 379
    .local v1, "progress":I
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v3}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 380
    .local v0, "playerMax":I
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    .line 381
    .local v2, "seekBarMax":I
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    mul-int v4, v0, v1

    div-int/2addr v4, v2

    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$13(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V

    .line 382
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v3}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v3

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v4}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$12(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 384
    .end local v0    # "playerMax":I
    .end local v1    # "progress":I
    .end local v2    # "seekBarMax":I
    :cond_0
    return-void
.end method
