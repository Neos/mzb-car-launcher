.class final Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;
.super Ljava/lang/Object;
.source "FragmentVideoPlay.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SurfaceViewCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;


# direct methods
.method private constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;)V
    .locals 0

    .prologue
    .line 555
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 560
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 564
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v1, v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    if-nez v1, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v1, v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v2

    if-le v1, v2, :cond_0

    .line 568
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v1, v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 569
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v1, v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 570
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 571
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$8(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 572
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$10(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V

    .line 576
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$12(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 577
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$12(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0

    .line 574
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$11(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/view/SurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_1
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 585
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 586
    const-string v0, "videoshow"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediaPlayer isPlaying= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$13(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V

    .line 589
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$14(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    invoke-static {}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$15()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$12(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 590
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$14(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    invoke-static {}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$16()Ljava/lang/String;

    move-result-object v2

    .line 591
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v0, v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v3}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/bean/MediaBean;

    .line 592
    invoke-virtual {v0}, Lcom/touchus/publicutils/bean/MediaBean;->getDisplay_name()Ljava/lang/String;

    move-result-object v0

    .line 590
    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 596
    :cond_0
    return-void
.end method
