.class final Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;
.super Ljava/lang/Object;
.source "FragmentVideoPlay.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PrepareListener"
.end annotation


# instance fields
.field private position:I

.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V
    .locals 0
    .param p2, "position"    # I

    .prologue
    .line 658
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 659
    iput p2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->position:I

    .line 661
    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 665
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$17(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V

    .line 666
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 667
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$6(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    iget-object v0, v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$3(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;->post(Ljava/lang/Runnable;)Z

    .line 669
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->position:I

    if-lez v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;->position:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 672
    :cond_0
    return-void
.end method
