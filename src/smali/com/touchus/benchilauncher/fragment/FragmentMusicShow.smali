.class public Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentMusicShow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;
    }
.end annotation


# static fields
.field private static POS:Ljava/lang/String;

.field private static mContext:Lcom/touchus/benchilauncher/Launcher;


# instance fields
.field private arr:[Landroid/widget/ImageView;

.field private count:I

.field private curIndexTview:Landroid/widget/TextView;

.field private loopType:I

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mBofang:Landroid/widget/ImageView;

.field private mIDRIVERENUM:B

.field private mMTitle:Landroid/widget/TextView;

.field private mMenuImg:Landroid/widget/ImageView;

.field public mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

.field private mNowTime:Landroid/widget/TextView;

.field private mRootView:Landroid/view/View;

.field private mShang:Landroid/widget/ImageView;

.field private mSingleLoopImg:Landroid/widget/ImageView;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private mXiayiqu:Landroid/widget/ImageView;

.field private mZongTime:Landroid/widget/TextView;

.field private musicPic:Landroid/widget/ImageView;

.field private needPlayIndex:I

.field private seekBar:Landroid/widget/SeekBar;

.field private totalRecordTview:Landroid/widget/TextView;

.field private updatePlayTimeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "pos"

    sput-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->POS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 39
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 58
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    .line 63
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->needPlayIndex:I

    .line 64
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    .line 285
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->updatePlayTimeRunnable:Ljava/lang/Runnable;

    .line 356
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->updatePlayTimeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$10(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mShang:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$11(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mXiayiqu:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mNowTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->musicPic:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;I)V
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;I)V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$7()Lcom/touchus/benchilauncher/Launcher;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mContext:Lcom/touchus/benchilauncher/Launcher;

    return-object v0
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    return v0
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;I)V
    .locals 0

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->pressItem(I)V

    return-void
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMenuImg:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$3;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$4;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$4;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mShang:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$5;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$6;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mXiayiqu:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$7;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$7;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$8;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$8;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 283
    return-void
.end method

.method private pressItem(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/16 v4, 0x3eb

    const/16 v1, 0x3e9

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 302
    packed-switch p1, :pswitch_data_0

    .line 342
    :goto_0
    return-void

    .line 304
    :pswitch_0
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->handleBackAction()V

    goto :goto_0

    .line 307
    :pswitch_1
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    if-ne v0, v1, :cond_0

    .line 308
    iput v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    .line 314
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "musicloop"

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 315
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setLoopType()V

    goto :goto_0

    .line 309
    :cond_0
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    if-ne v0, v4, :cond_1

    .line 310
    const/16 v0, 0x3ea

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    goto :goto_1

    .line 312
    :cond_1
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    goto :goto_1

    .line 318
    :pswitch_2
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->prev()V

    .line 319
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setImageSeclect(I)V

    goto :goto_0

    .line 322
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020002

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 324
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    .line 325
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->pauseMusic()V

    .line 326
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V

    goto :goto_0

    .line 328
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 329
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->replayMusic()V

    .line 330
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    .line 331
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0, v3}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V

    goto :goto_0

    .line 335
    :pswitch_4
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->Next()V

    .line 336
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setImageSeclect(I)V

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 410
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setPlayBtnStatus(I)V

    .line 411
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 418
    return-void

    .line 412
    :cond_0
    if-ne p1, v0, :cond_1

    .line 413
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 411
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1
.end method

.method private setCurrentPlayInfo()V
    .locals 10

    .prologue
    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget-object v1, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->currentMusic:Lcom/touchus/publicutils/bean/MediaBean;

    .line 161
    .local v1, "musicInfo":Lcom/touchus/publicutils/bean/MediaBean;
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getTitle()Ljava/lang/String;

    move-result-object v8

    .line 165
    .local v8, "title":Ljava/lang/String;
    const-string v0, "\\."

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 166
    .local v6, "splitAddress":[Ljava/lang/String;
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMTitle:Landroid/widget/TextView;

    const/4 v2, 0x0

    aget-object v2, v6, v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getDuration()J

    move-result-wide v2

    long-to-int v0, v2

    div-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v9

    .line 168
    .local v9, "zongTime":Ljava/lang/String;
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getDuration()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 169
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mZongTime:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mNowTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->getPosition()I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget v7, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 173
    .local v7, "temp":I
    if-ltz v7, :cond_2

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->curIndexTview:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->getMusicList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->totalRecordTview:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->getMusicList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 179
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :goto_2
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_4

    .line 185
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->musicPic:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 186
    invoke-static {}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->getInstance()Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    move-result-object v0

    .line 187
    sget v2, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->MUSIC_TYPE:I

    sget-object v3, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 188
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$2;

    invoke-direct {v5, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$2;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    .line 186
    invoke-virtual/range {v0 .. v5}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->loadDrawable(Lcom/touchus/publicutils/bean/MediaBean;ILandroid/content/Context;Ljava/lang/String;Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;)Landroid/graphics/Bitmap;

    .line 202
    :goto_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setLoopType()V

    goto/16 :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->curIndexTview:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 182
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->totalRecordTview:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 200
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->musicPic:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3
.end method

.method private setImageSeclect(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    const-wide/16 v2, 0x12c

    .line 437
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mShang:Landroid/widget/ImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 439
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$9;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$9;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mXiayiqu:Landroid/widget/ImageView;

    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 447
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$10;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$10;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private setLoopType()V
    .locals 2

    .prologue
    const/high16 v1, 0x7f020000

    .line 206
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    packed-switch v0, :pswitch_data_0

    .line 217
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 220
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setPlayLoopType(I)V

    .line 221
    return-void

    .line 208
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 211
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 214
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    const v1, 0x7f020006

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private setPlayBtnStatus(I)V
    .locals 2
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 421
    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020017

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 434
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 429
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 431
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020002

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public Next()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    .line 345
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playNextMusic()V

    .line 346
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setCurrentPlayInfo()V

    .line 347
    return-void
.end method

.method public handlerMsgx(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 375
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_7

    .line 376
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 377
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mIDRIVERENUM:B

    .line 380
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_1

    .line 381
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->pressItem(I)V

    .line 407
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 382
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_1
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 383
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_2

    .line 384
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_4

    .line 385
    :cond_2
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_3

    .line 386
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    .line 388
    :cond_3
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seclectTrue(I)V

    goto :goto_0

    .line 389
    :cond_4
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 390
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_5

    .line 391
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_0

    .line 392
    :cond_5
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    if-lez v1, :cond_6

    .line 393
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    .line 395
    :cond_6
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seclectTrue(I)V

    goto :goto_0

    .line 397
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_7
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1b59

    if-ne v1, v2, :cond_9

    .line 398
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8

    .line 399
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v2, 0x7f020017

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 403
    :goto_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setCurrentPlayInfo()V

    goto :goto_0

    .line 401
    :cond_8
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    const v2, 0x7f020016

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 404
    :cond_9
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1b5b

    if-ne v1, v2, :cond_0

    .line 405
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setPlayBtnStatus(I)V

    goto :goto_0
.end method

.method public onBack()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 155
    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 156
    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 71
    const v0, 0x7f030023

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    .line 72
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    sput-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 73
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 74
    if-eqz p3, :cond_0

    .line 75
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->POS:Ljava/lang/String;

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "musicloop"

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->loopType:I

    .line 78
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 79
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/MyTextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMTitle:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->musicPic:Landroid/widget/ImageView;

    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mNowTime:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mZongTime:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->curIndexTview:Landroid/widget/TextView;

    .line 85
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    .line 86
    const v1, 0x7f0b00c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 85
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->totalRecordTview:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seekBar:Landroid/widget/SeekBar;

    .line 89
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mShang:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    .line 91
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mXiayiqu:Landroid/widget/ImageView;

    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMenuImg:Landroid/widget/ImageView;

    .line 94
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    .line 95
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMenuImg:Landroid/widget/ImageView;

    aput-object v1, v0, v3

    .line 96
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mSingleLoopImg:Landroid/widget/ImageView;

    aput-object v1, v0, v4

    .line 97
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mShang:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 98
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mBofang:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->arr:[Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mXiayiqu:Landroid/widget/ImageView;

    aput-object v1, v0, v5

    .line 100
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->needPlayIndex:I

    .line 101
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 102
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->initListener()V

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-nez v0, :cond_1

    .line 104
    invoke-static {v5}, Lcom/touchus/publicutils/utils/UtilTools;->sendKeyeventToSystem(I)V

    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seekBar:Landroid/widget/SeekBar;

    .line 150
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 151
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->POS:Ljava/lang/String;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 113
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/touchus/publicutils/utils/UtilTools;->sendKeyeventToSystem(I)V

    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    .line 118
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->needPlayIndex:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget v1, v1, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    if-ne v0, v1, :cond_2

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    .line 126
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->updatePlayTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;->post(Ljava/lang/Runnable;)Z

    .line 127
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setCurrentPlayInfo()V

    .line 128
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->count:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->seclectTrue(I)V

    .line 129
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 130
    return-void

    .line 122
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->needPlayIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 123
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->needPlayIndex:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playMusic(I)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 135
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->updatePlayTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 139
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 140
    return-void
.end method

.method public prev()V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    .line 351
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playPreviousMusic()V

    .line 352
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setCurrentPlayInfo()V

    .line 353
    return-void
.end method
