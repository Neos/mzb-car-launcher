.class Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;
.super Ljava/lang/Object;
.source "FragmentMusicLanYa.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 156
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0, v2}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$1(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;I)V

    .line 157
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0, v2}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$2(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;I)V

    .line 158
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$5(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$6(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Landroid/widget/ImageView;

    move-result-object v0

    const v3, 0x7f020016

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$5(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$7(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;Z)V

    .line 161
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    .line 162
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    .line 169
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 160
    goto :goto_0

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$6(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Landroid/widget/ImageView;

    move-result-object v0

    const v3, 0x7f020002

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 165
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$5(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v3, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$7(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;Z)V

    .line 166
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    .line 167
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->access$4(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    goto :goto_1

    :cond_2
    move v0, v2

    .line 165
    goto :goto_2
.end method
