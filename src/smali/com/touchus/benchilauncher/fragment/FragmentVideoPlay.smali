.class public Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentVideoPlay.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;,
        Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;,
        Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;
    }
.end annotation


# static fields
.field private static LAST_VIDEO_PLAY_NAME:Ljava/lang/String;

.field private static LAST_VIDEO_PLAY_POSITION:Ljava/lang/String;


# instance fields
.field private PasueFlag:Z

.field private arr:[Landroid/widget/ImageButton;

.field private autoSettingHideOrShowRunnable:Ljava/lang/Runnable;

.field changeProgress:I

.field private countSufaceVieew:I

.field curProgress:I

.field private currentPlayIndex:I

.field private currentPosition:I

.field private iIsPlaySeekbarShow:Z

.field private iNeedHeartThread:Z

.field private indexCount:I

.field private info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

.field private isBigView:Z

.field private isOpenSound:Z

.field private isPrepare:Z

.field private lp:Landroid/view/ViewGroup$LayoutParams;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mBoTime:Landroid/widget/TextView;

.field mContext:Landroid/content/Context;

.field private mDibuCaidan:Landroid/widget/LinearLayout;

.field private mDibucandan:Landroid/view/ViewGroup$LayoutParams;

.field private mFangsuo:Landroid/widget/ImageButton;

.field private mFlsurfaceView:Landroid/widget/FrameLayout;

.field private mIDRIVERENUM:B

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field private mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

.field private mPaomadeng:Landroid/widget/LinearLayout;

.field private mRootView:Landroid/view/View;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBeijing:Landroid/widget/LinearLayout;

.field private mSound:Landroid/widget/ImageButton;

.field private mToolBar:Landroid/widget/LinearLayout;

.field private mVedioBofang:Landroid/widget/ImageButton;

.field private mVedioCaidan:Landroid/widget/ImageButton;

.field private mVedioName:Lcom/touchus/benchilauncher/views/MyTextView;

.field private mVedioShnag:Landroid/widget/ImageButton;

.field private mVedioXia:Landroid/widget/ImageButton;

.field private mYoushengdao:Landroid/widget/ImageButton;

.field private mZongTime:Landroid/widget/TextView;

.field private mZuoshengdao:Landroid/widget/ImageButton;

.field public mediaBeans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field private mediaPlayer:Landroid/media/MediaPlayer;

.field moveX:I

.field private share:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field startX:I

.field private surfaceView:Landroid/view/SurfaceView;

.field private surfaceViewCallBack:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;

.field private updateRunnable:Ljava/lang/Runnable;

.field private vedio_pause:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "lastVideoPlayName"

    sput-object v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_NAME:Ljava/lang/String;

    .line 49
    const-string v0, "lastVideoPlayPosition"

    sput-object v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_POSITION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    .line 60
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    .line 61
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isOpenSound:Z

    .line 68
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    .line 91
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iIsPlaySeekbarShow:Z

    .line 99
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iNeedHeartThread:Z

    .line 184
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$1;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->autoSettingHideOrShowRunnable:Ljava/lang/Runnable;

    .line 282
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    .line 599
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isPrepare:Z

    .line 676
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$2;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->updateRunnable:Ljava/lang/Runnable;

    .line 704
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    .line 910
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$3;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 46
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 3

    .prologue
    .line 901
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 908
    :goto_0
    return-void

    .line 904
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 905
    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 904
    check-cast v0, Landroid/media/AudioManager;

    .line 906
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->hidePlayControlBar()V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iNeedHeartThread:Z

    return v0
.end method

.method static synthetic access$10(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V
    .locals 0

    .prologue
    .line 600
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->play(I)V

    return-void
.end method

.method static synthetic access$11(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    return v0
.end method

.method static synthetic access$13(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    return-void
.end method

.method static synthetic access$14(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Lcom/touchus/benchilauncher/utils/SpUtilK;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    return-object v0
.end method

.method static synthetic access$15()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_POSITION:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$16()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$17(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V
    .locals 0

    .prologue
    .line 599
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isPrepare:Z

    return-void
.end method

.method static synthetic access$18(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$19(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V
    .locals 0

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->updateCurrentPlayInfo()V

    return-void
.end method

.method static synthetic access$20(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    return v0
.end method

.method static synthetic access$21(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V
    .locals 0

    .prologue
    .line 802
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->press(I)V

    return-void
.end method

.method static synthetic access$22(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    return v0
.end method

.method static synthetic access$23(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V
    .locals 0

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    return-void
.end method

.method static synthetic access$24(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Z
    .locals 1

    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isPrepare:Z

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->updateRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Z)V
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    return v0
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    return v0
.end method

.method private hidePlayControlBar()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 496
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 497
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 499
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBeijing:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 500
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mPaomadeng:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 501
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    .line 503
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->hideTop()V

    .line 508
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iIsPlaySeekbarShow:Z

    .line 509
    return-void

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 506
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->showTop()V

    goto :goto_0
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$4;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$4;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSound:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$5;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mZuoshengdao:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$6;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$7;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$7;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioCaidan:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$8;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$8;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFlsurfaceView:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$9;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$9;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, p0}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 337
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$10;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$10;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioShnag:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$11;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$11;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioXia:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$12;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$12;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 365
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$13;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$13;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$14;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 396
    return-void
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 512
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 514
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 515
    const v1, 0x7f0b00de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 514
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBar:Landroid/widget/SeekBar;

    .line 516
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 517
    const v1, 0x7f0b00da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 516
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    .line 518
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 519
    const v1, 0x7f0b00dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 518
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mZongTime:Landroid/widget/TextView;

    .line 520
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 521
    const v1, 0x7f0b00df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 520
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mBoTime:Landroid/widget/TextView;

    .line 522
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    .line 523
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioShnag:Landroid/widget/ImageButton;

    .line 524
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioXia:Landroid/widget/ImageButton;

    .line 525
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    .line 526
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioCaidan:Landroid/widget/ImageButton;

    .line 527
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    .line 528
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    .line 529
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 530
    const v1, 0x7f0b00e6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 529
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFlsurfaceView:Landroid/widget/FrameLayout;

    .line 531
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00e0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSound:Landroid/widget/ImageButton;

    .line 532
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mZuoshengdao:Landroid/widget/ImageButton;

    .line 533
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    .line 535
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/MyTextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioName:Lcom/touchus/benchilauncher/views/MyTextView;

    .line 536
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mPaomadeng:Landroid/widget/LinearLayout;

    .line 537
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 538
    const v1, 0x7f0b00ed

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 537
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBeijing:Landroid/widget/LinearLayout;

    .line 540
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSound:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 541
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mZuoshengdao:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 542
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioShnag:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 543
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 544
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioXia:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 545
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 546
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioCaidan:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    .line 548
    return-void
.end method

.method private play(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 601
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->updateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 602
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 648
    :goto_0
    return-void

    .line 605
    :cond_0
    sput p1, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    .line 606
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v4, "videoPos"

    invoke-virtual {v3, v4, p1}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 608
    :try_start_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->reset()V

    .line 609
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v3}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 610
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 611
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isPrepare:Z

    .line 612
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 613
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v4, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$15;

    invoke-direct {v4, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$15;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 621
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 622
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/touchus/publicutils/bean/MediaBean;

    .line 623
    .local v2, "temp":Lcom/touchus/publicutils/bean/MediaBean;
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v4, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_NAME:Ljava/lang/String;

    const-string v5, "-"

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 624
    .local v1, "lastName":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/touchus/publicutils/bean/MediaBean;->getDisplay_name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v4, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_POSITION:Ljava/lang/String;

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    .line 632
    .end local v1    # "lastName":Ljava/lang/String;
    .end local v2    # "temp":Lcom/touchus/publicutils/bean/MediaBean;
    :goto_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v4, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;

    iget v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    invoke-direct {v4, p0, v5}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$PrepareListener;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;I)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 633
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioName:Lcom/touchus/benchilauncher/views/MyTextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v3}, Lcom/touchus/publicutils/bean/MediaBean;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/touchus/benchilauncher/views/MyTextView;->setText(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v4, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$16;

    invoke-direct {v4, p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$16;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 645
    :catch_0
    move-exception v0

    .line 646
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 627
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "lastName":Ljava/lang/String;
    .restart local v2    # "temp":Lcom/touchus/publicutils/bean/MediaBean;
    :cond_1
    const/4 v3, -0x1

    :try_start_1
    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    goto :goto_1

    .line 630
    .end local v1    # "lastName":Ljava/lang/String;
    .end local v2    # "temp":Lcom/touchus/publicutils/bean/MediaBean;
    :cond_2
    const/4 v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private press(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const v3, 0x7f02017c

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 803
    packed-switch p1, :pswitch_data_0

    .line 868
    :goto_0
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->seclectTrue(I)V

    .line 869
    return-void

    .line 805
    :pswitch_0
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isOpenSound:Z

    if-eqz v2, :cond_1

    .line 806
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSound:Landroid/widget/ImageButton;

    const v3, 0x7f020182

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 807
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isOpenSound:Z

    if-eqz v2, :cond_0

    :goto_1
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isOpenSound:Z

    .line 808
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v5, v5}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 807
    goto :goto_1

    .line 810
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSound:Landroid/widget/ImageButton;

    const v3, 0x7f020180

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 811
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isOpenSound:Z

    if-eqz v2, :cond_2

    :goto_2
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isOpenSound:Z

    .line 812
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v4, v4}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 811
    goto :goto_2

    .line 816
    :pswitch_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 817
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->resetPositon()V

    .line 818
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    .line 819
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 820
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0

    .line 823
    :pswitch_2
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->resetPositon()V

    .line 824
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    if-lez v0, :cond_3

    .line 825
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    .line 826
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->play(I)V

    goto :goto_0

    .line 828
    :cond_3
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->play(I)V

    goto :goto_0

    .line 832
    :pswitch_3
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-eqz v2, :cond_5

    .line 833
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    .line 834
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 835
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    .line 836
    const v3, 0x7f020186

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 837
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-eqz v2, :cond_4

    :goto_3
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    .line 838
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->requestAudioFocus()V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 837
    goto :goto_3

    .line 840
    :cond_5
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->pause()V

    .line 841
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 842
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    .line 843
    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 844
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-eqz v2, :cond_6

    :goto_4
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_4

    .line 848
    :pswitch_4
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->resetPositon()V

    .line 849
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    .line 850
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    .line 851
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->play(I)V

    goto/16 :goto_0

    .line 853
    :cond_7
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->play(I)V

    goto/16 :goto_0

    .line 858
    :pswitch_5
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    .line 859
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showOrFangSuo()V

    goto/16 :goto_0

    .line 862
    :pswitch_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->handleBackAction()V

    goto/16 :goto_0

    .line 803
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private requestAudioFocus()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 882
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 898
    :goto_0
    return-void

    .line 888
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput v4, v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 889
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 890
    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 889
    check-cast v1, Landroid/media/AudioManager;

    .line 891
    .local v1, "mAudioManager":Landroid/media/AudioManager;
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 892
    const/4 v3, 0x1

    .line 891
    invoke-virtual {v1, v2, v4, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 893
    .local v0, "flag":I
    const-string v2, "requestAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flag"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    goto :goto_0
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 872
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 879
    return-void

    .line 873
    :cond_0
    if-ne p1, v0, :cond_1

    .line 874
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 872
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 876
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1
.end method

.method private setAutoYincanMuen()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->autoSettingHideOrShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 181
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->autoSettingHideOrShowRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 182
    return-void
.end method

.method private showOrFangSuo()V
    .locals 5

    .prologue
    const v4, 0x7f02017a

    const v3, 0x7f020179

    const/16 v2, 0x1e0

    .line 434
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v1, v1, 0x3

    if-nez v1, :cond_2

    .line 435
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    if-eqz v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 437
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 438
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v1, 0x500

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 439
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 440
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 473
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 443
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 444
    .restart local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v1, 0x320

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 445
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 446
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 449
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v1, v1, 0x3

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 451
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    const v2, 0x7f02017b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 452
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 454
    .restart local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v1, 0x7d0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 455
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 456
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 457
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v1, v1, 0x3

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 458
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    if-eqz v1, :cond_4

    .line 459
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 460
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 461
    .restart local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v1, 0x424

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 462
    const/16 v1, 0x12c

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 463
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 465
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 466
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 467
    .restart local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v1, 0x21c

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 468
    const/16 v1, 0x107

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 469
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private showOrHidePlaySeekbar()V
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iIsPlaySeekbarShow:Z

    if-eqz v0, :cond_0

    .line 477
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->hidePlayControlBar()V

    .line 481
    :goto_0
    return-void

    .line 479
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showPlayControlBar()V

    goto :goto_0
.end method

.method private showPlayControlBar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 484
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 485
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 486
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 487
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBeijing:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 488
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mPaomadeng:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 489
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 490
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->showTop()V

    .line 491
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->setAutoYincanMuen()V

    .line 492
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iIsPlaySeekbarShow:Z

    .line 493
    return-void
.end method

.method private updateCurrentPlayInfo()V
    .locals 6

    .prologue
    .line 692
    :try_start_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    .line 693
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    .line 694
    .local v1, "mMax":I
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    .line 695
    .local v2, "sMax":I
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    mul-int/2addr v4, v2

    div-int/2addr v4, v1

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 696
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mBoTime:Landroid/widget/TextView;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 697
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mZongTime:Landroid/widget/TextView;

    div-int/lit16 v4, v1, 0x3e8

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 701
    .end local v1    # "mMax":I
    .end local v2    # "sMax":I
    :goto_0
    return-void

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public fangdaVedio()V
    .locals 8

    .prologue
    const/16 v7, 0x500

    const/16 v6, 0x1e0

    const/4 v5, 0x0

    .line 203
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFlsurfaceView:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    .line 204
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 205
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 206
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFlsurfaceView:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 208
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    .line 209
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    iput v7, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 210
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 211
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    const v4, 0x7f020077

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 215
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 217
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_1

    .line 218
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    const v4, 0x7f02017a

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 219
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 220
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v7, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 221
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 222
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBeijing:Landroid/widget/LinearLayout;

    .line 232
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 231
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 233
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0x166

    const/16 v4, 0x14a

    invoke-virtual {v0, v5, v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 235
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mPaomadeng:Landroid/widget/LinearLayout;

    .line 236
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 235
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 237
    .local v1, "layoutParams1":Landroid/widget/RelativeLayout$LayoutParams;
    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 238
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioName:Lcom/touchus/benchilauncher/views/MyTextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/views/MyTextView;->setGravity(I)V

    .line 239
    const/16 v3, 0x190

    const/16 v4, 0xa

    invoke-virtual {v1, v3, v5, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 240
    return-void

    .line 223
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "layoutParams1":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v3, v3, 0x3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 224
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    const v4, 0x7f020179

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 225
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 226
    .restart local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v3, 0x424

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 227
    const/16 v3, 0x12c

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 228
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public handlerMsg(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 725
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x1771

    if-ne v3, v4, :cond_b

    .line 727
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 728
    .local v0, "temp":Landroid/os/Bundle;
    const-string v3, "idriver_enum"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    iput-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    .line 730
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_2

    .line 731
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showPlayControlBar()V

    .line 732
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 733
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 734
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 736
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->arr:[Landroid/widget/ImageButton;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 737
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    .line 739
    :cond_0
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->seclectTrue(I)V

    .line 741
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->setAutoYincanMuen()V

    .line 800
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 743
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_3

    .line 744
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showPlayControlBar()V

    .line 745
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->press(I)V

    goto :goto_0

    .line 746
    :cond_3
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 747
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_5

    .line 748
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showPlayControlBar()V

    .line 749
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 750
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 751
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 753
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    if-lez v1, :cond_4

    .line 754
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    .line 756
    :cond_4
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->indexCount:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->seclectTrue(I)V

    .line 757
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->setAutoYincanMuen()V

    goto :goto_0

    .line 759
    :cond_5
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_6

    .line 760
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    if-nez v1, :cond_1

    .line 761
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    .line 762
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->fangdaVedio()V

    goto :goto_0

    .line 764
    :cond_6
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_7

    .line 765
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    if-eqz v2, :cond_1

    .line 766
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->isBigView:Z

    .line 767
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->suoXiaoVedio()V

    goto :goto_0

    .line 769
    :cond_7
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_9

    .line 770
    iget-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-eqz v3, :cond_1

    .line 771
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->start()V

    .line 772
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    .line 773
    const v4, 0x7f020186

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 774
    iget-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-eqz v3, :cond_8

    :goto_1
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    .line 775
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 774
    goto :goto_1

    .line 777
    :cond_9
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_1

    .line 778
    iget-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-nez v3, :cond_1

    .line 779
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->pause()V

    .line 780
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioBofang:Landroid/widget/ImageButton;

    .line 781
    const v4, 0x7f02017c

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 782
    iget-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    if-eqz v3, :cond_a

    move v2, v1

    :cond_a
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->PasueFlag:Z

    .line 783
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->vedio_pause:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 786
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_b
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x3ef

    if-ne v1, v2, :cond_1

    .line 787
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    iput-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .line 788
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiFlash()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 791
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiLeftBackOpen()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiLeftFrontOpen()Z

    move-result v1

    if-nez v1, :cond_c

    .line 792
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiRightBackOpen()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->info:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiRightFrontOpen()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 793
    :cond_c
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->suoXiaoVedio()V

    .line 794
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showOrFangSuo()V

    .line 795
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showPlayControlBar()V

    goto/16 :goto_0

    .line 797
    :cond_d
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->setAutoYincanMuen()V

    goto/16 :goto_0
.end method

.method public next()V
    .locals 2

    .prologue
    .line 193
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    .line 194
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    if-gt v0, v1, :cond_0

    .line 195
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    .line 197
    :cond_0
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->play(I)V

    .line 199
    return-void
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, -0x1

    .line 106
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mContext:Landroid/content/Context;

    .line 107
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/Launcher;

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 108
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 109
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 111
    .local v2, "start":J
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "videoshow = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    new-instance v4, Lcom/touchus/benchilauncher/utils/SpUtilK;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 113
    const v4, 0x7f030026

    invoke-virtual {p1, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 114
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->initView()V

    .line 116
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 117
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "position"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    .line 119
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 120
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    iget v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPlayIndex:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    .line 121
    .local v1, "temp":Lcom/touchus/publicutils/bean/MediaBean;
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v5, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_NAME:Ljava/lang/String;

    const-string v6, "-"

    invoke-virtual {v4, v5, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "lastName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getDisplay_name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v5, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_POSITION:Ljava/lang/String;

    invoke-virtual {v4, v5, v7}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    .line 132
    .end local v0    # "lastName":Ljava/lang/String;
    .end local v1    # "temp":Lcom/touchus/publicutils/bean/MediaBean;
    :goto_0
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    .line 133
    const v5, 0x7f0b00e7

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/SurfaceView;

    .line 132
    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    .line 137
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    const/16 v5, 0xb0

    const/16 v6, 0x90

    invoke-interface {v4, v5, v6}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 138
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    invoke-interface {v4, v8}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    .line 139
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v4, v8}, Landroid/view/SurfaceView;->setClickable(Z)V

    .line 140
    new-instance v4, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;

    invoke-direct {v4, p0, v9}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;)V

    iput-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceViewCallBack:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;

    .line 141
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceViewCallBack:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$SurfaceViewCallBack;

    invoke-interface {v4, v5}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 143
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->initListener()V

    .line 144
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "videoshow = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->setAutoYincanMuen()V

    .line 146
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->seclectTrue(I)V

    .line 147
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->requestAudioFocus()V

    .line 149
    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mRootView:Landroid/view/View;

    return-object v4

    .line 125
    .restart local v0    # "lastName":Ljava/lang/String;
    .restart local v1    # "temp":Lcom/touchus/publicutils/bean/MediaBean;
    :cond_0
    iput v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    goto :goto_0

    .line 129
    .end local v0    # "lastName":Ljava/lang/String;
    .end local v1    # "temp":Lcom/touchus/publicutils/bean/MediaBean;
    :cond_1
    iput v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->abandonAudioFocus()V

    .line 169
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 173
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iNeedHeartThread:Z

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->autoSettingHideOrShowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 175
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 176
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 177
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iNeedHeartThread:Z

    .line 155
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 158
    :cond_0
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 159
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 164
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x5

    const/4 v4, -0x5

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 402
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 403
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->startX:I

    .line 404
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->curProgress:I

    .line 405
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    .line 431
    :cond_0
    :goto_0
    return v0

    .line 407
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 408
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->startX:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    .line 409
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->curProgress:I

    rsub-int/lit8 v3, v3, 0x64

    mul-int/lit8 v3, v3, 0xa

    if-lt v2, v3, :cond_4

    .line 410
    const/16 v1, 0x64

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->changeProgress:I

    .line 416
    :goto_1
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    if-ge v1, v5, :cond_2

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    if-gt v1, v4, :cond_3

    .line 418
    :cond_2
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->iIsPlaySeekbarShow:Z

    if-eqz v1, :cond_3

    .line 419
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->changeProgress:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x64

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    .line 420
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->currentPosition:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 421
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->updateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay$MusicLanyaHandler;->post(Ljava/lang/Runnable;)Z

    .line 423
    :cond_3
    const-string v1, "mVideo"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onTouch moveX = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 411
    :cond_4
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->curProgress:I

    neg-int v3, v3

    mul-int/lit8 v3, v3, 0xa

    if-gt v2, v3, :cond_5

    .line 412
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->changeProgress:I

    goto :goto_1

    .line 414
    :cond_5
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->curProgress:I

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    div-int/lit8 v2, v2, 0xa

    add-int/2addr v1, v2

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->changeProgress:I

    goto :goto_1

    .line 425
    :cond_6
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_7

    .line 426
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    if-ge v1, v5, :cond_0

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->moveX:I

    if-le v1, v4, :cond_0

    .line 427
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->showOrHidePlaySeekbar()V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 431
    goto/16 :goto_0
.end method

.method public resetPositon()V
    .locals 3

    .prologue
    .line 651
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_POSITION:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 652
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->share:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->LAST_VIDEO_PLAY_NAME:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    return-void
.end method

.method public suoXiaoVedio()V
    .locals 9

    .prologue
    const/16 v8, 0x320

    const/4 v7, 0x0

    const/16 v6, 0x1e0

    .line 243
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFlsurfaceView:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    .line 244
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    iput v8, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 245
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 246
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFlsurfaceView:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->lp:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    .line 249
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    iput v8, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 250
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 251
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibuCaidan:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mDibucandan:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 252
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mFangsuo:Landroid/widget/ImageButton;

    const v4, 0x7f020076

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 255
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mToolBar:Landroid/widget/LinearLayout;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 257
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mSeekBeijing:Landroid/widget/LinearLayout;

    .line 258
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 257
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 259
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0x36

    const/16 v4, 0x166

    const/16 v5, 0x4a

    invoke-virtual {v0, v3, v4, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 261
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mPaomadeng:Landroid/widget/LinearLayout;

    .line 262
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 261
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 263
    .local v1, "layoutParams1":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mVedioName:Lcom/touchus/benchilauncher/views/MyTextView;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/views/MyTextView;->setGravity(I)V

    .line 264
    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 265
    const/16 v3, 0x1ea

    const/16 v4, 0xa

    invoke-virtual {v1, v3, v7, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 267
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_1

    .line 268
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    const v4, 0x7f02017a

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 269
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 270
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v8, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 271
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 272
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 280
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->countSufaceVieew:I

    rem-int/lit8 v3, v3, 0x3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 274
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mYoushengdao:Landroid/widget/ImageButton;

    const v4, 0x7f020179

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 275
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 276
    .restart local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v3, 0x21c

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 277
    const/16 v3, 0x107

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 278
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3, v2}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
