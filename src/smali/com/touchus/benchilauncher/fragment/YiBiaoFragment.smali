.class public Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "YiBiaoFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;
    }
.end annotation


# instance fields
.field private car_body:Landroid/widget/ImageView;

.field private car_bonnet:Landroid/widget/ImageView;

.field private car_brake:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

.field private car_headlamp:Landroid/widget/ImageView;

.field private car_lamplet:Landroid/widget/ImageView;

.field private car_lb_door:Landroid/widget/ImageView;

.field private car_lf_door:Landroid/widget/ImageView;

.field private car_outsidetemp:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

.field private car_rb_door:Landroid/widget/ImageView;

.field private car_rearbox:Landroid/widget/ImageView;

.field private car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

.field private car_rf_door:Landroid/widget/ImageView;

.field private car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

.field private car_stoplight:Landroid/widget/ImageView;

.field private iUpdate:Z

.field private launcher:Lcom/touchus/benchilauncher/Launcher;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mContext:Landroid/content/Context;

.field private mView:Landroid/view/View;

.field private manager:Landroid/app/FragmentManager;

.field private meterHandler:Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;

.field private rSpeed:F

.field private speed:F

.field private suduAngle:F

.field private suduYiBiao:Lcom/touchus/benchilauncher/views/YiBiaoView;

.field private zhuanSAngle:F

.field private zhuanSuYiBiao:Lcom/touchus/benchilauncher/views/YiBiaoView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 46
    const/high16 v0, -0x3cff0000    # -129.0f

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->suduAngle:F

    .line 47
    const/high16 v0, -0x3cf10000    # -143.0f

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->zhuanSAngle:F

    .line 48
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->speed:F

    .line 49
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->rSpeed:F

    .line 55
    new-instance v0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;-><init>(Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->meterHandler:Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 76
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->iUpdate:Z

    if-nez v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3f0

    if-ne v2, v3, :cond_2

    .line 80
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 81
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "runningState"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/backaudio/android/driver/beans/CarRunInfo;

    .line 82
    .local v1, "info":Lcom/backaudio/android/driver/beans/CarRunInfo;
    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getCurSpeed()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->speed:F

    .line 86
    invoke-virtual {v1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getRevolutions()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->rSpeed:F

    .line 87
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->speedToAngle()V

    .line 88
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setYiBiaoAngle()V

    .line 89
    invoke-direct {p0, v1, v4}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    goto :goto_0

    .line 90
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "info":Lcom/backaudio/android/driver/beans/CarRunInfo;
    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3ef

    if-ne v2, v3, :cond_0

    .line 91
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 92
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v2, "carBaseInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .line 93
    .local v1, "info":Lcom/backaudio/android/driver/beans/CarBaseInfo;
    invoke-direct {p0, v4, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    goto :goto_0
.end method

.method private initData()V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setYiBiaoAngle()V

    .line 209
    return-void
.end method

.method private initView()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 154
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/YiBiaoView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->suduYiBiao:Lcom/touchus/benchilauncher/views/YiBiaoView;

    .line 155
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    .line 156
    const v1, 0x7f0b00f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/YiBiaoView;

    .line 155
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->zhuanSuYiBiao:Lcom/touchus/benchilauncher/views/YiBiaoView;

    .line 158
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_body:Landroid/widget/ImageView;

    .line 159
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00a7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_headlamp:Landroid/widget/ImageView;

    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lamplet:Landroid/widget/ImageView;

    .line 161
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_bonnet:Landroid/widget/ImageView;

    .line 162
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lf_door:Landroid/widget/ImageView;

    .line 163
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rf_door:Landroid/widget/ImageView;

    .line 164
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lb_door:Landroid/widget/ImageView;

    .line 165
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rb_door:Landroid/widget/ImageView;

    .line 166
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rearbox:Landroid/widget/ImageView;

    .line 167
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_stoplight:Landroid/widget/ImageView;

    .line 169
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00b1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    .line 170
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    .line 171
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_outsidetemp:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_brake:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200ab

    .line 175
    const v2, 0x7f070075

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-virtual {v0, v5, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_brake:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200a0

    const v2, 0x7f070071

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v6, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200ae

    const v2, 0x7f070074

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v5, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_headlamp:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 181
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lamplet:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 182
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_bonnet:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 183
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lf_door:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 184
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rf_door:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 185
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lb_door:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 186
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rb_door:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 187
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rearbox:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 188
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_stoplight:Landroid/widget/ImageView;

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 190
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-eq v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    invoke-virtual {v0, v5}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    invoke-virtual {v0, v7}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setVisibility(I)V

    .line 197
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    invoke-virtual {v0, v7}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    invoke-virtual {v0, v5}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setBaseinfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V
    .locals 5
    .param p1, "baseInfo"    # Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 245
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiBrake()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_brake:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200a0

    const v2, 0x7f070071

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    .line 251
    :goto_0
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiSafetyBelt()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200ad

    const v2, 0x7f070074

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    .line 256
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_headlamp:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiDistantLightOpen()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 257
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lamplet:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiNearLightOpen()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 258
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_bonnet:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiFront()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 259
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lf_door:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiLeftFrontOpen()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 260
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rf_door:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiRightFrontOpen()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 261
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_lb_door:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiLeftBackOpen()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 262
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rb_door:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiRightBackOpen()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 263
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_rearbox:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiBack()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setImageState(Landroid/widget/ImageView;Z)V

    .line 265
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_brake:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200a1

    const v2, 0x7f070072

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_safety_belt:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    const v1, 0x7f0200ae

    const v2, 0x7f070073

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    goto :goto_1
.end method

.method private setImageState(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "isopen"    # Z

    .prologue
    .line 200
    if-eqz p2, :cond_0

    .line 201
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setRuningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V
    .locals 6
    .param p1, "runInfo"    # Lcom/backaudio/android/driver/beans/CarRunInfo;

    .prologue
    const v3, 0x7f070075

    const v5, 0x7f0200ab

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 269
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getMileage()I

    move-result v0

    const/16 v1, 0x7ff

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getMileage()I

    move-result v0

    if-gtz v0, :cond_1

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "----"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v5, v1}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    .line 275
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_remainkon:Lcom/touchus/benchilauncher/views/CarWarnInfoView;

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getMileage()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v5, v1}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->setCarInfo(ZILjava/lang/String;)V

    goto :goto_0
.end method

.method private setSpeed()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->speed:I

    int-to-float v0, v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->speed:F

    .line 224
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->rSpeed:I

    int-to-float v0, v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->rSpeed:F

    .line 225
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->speedToAngle()V

    .line 226
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setYiBiaoAngle()V

    .line 227
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->carRunInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    .line 228
    return-void
.end method

.method private setYiBiaoAngle()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->suduYiBiao:Lcom/touchus/benchilauncher/views/YiBiaoView;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->suduAngle:F

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/YiBiaoView;->setAngle(F)V

    .line 282
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->zhuanSuYiBiao:Lcom/touchus/benchilauncher/views/YiBiaoView;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->zhuanSAngle:F

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/YiBiaoView;->setAngle(F)V

    .line 283
    return-void
.end method

.method private settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;Lcom/backaudio/android/driver/beans/CarBaseInfo;)V
    .locals 1
    .param p1, "runInfo"    # Lcom/backaudio/android/driver/beans/CarRunInfo;
    .param p2, "baseInfo"    # Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    if-eqz p1, :cond_2

    .line 237
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setRuningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    .line 239
    :cond_2
    if-eqz p2, :cond_0

    .line 240
    invoke-direct {p0, p2}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setBaseinfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    goto :goto_0
.end method

.method private speedToAngle()V
    .locals 2

    .prologue
    .line 289
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->speed:F

    const/high16 v1, 0x43020000    # 130.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->suduAngle:F

    .line 290
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->rSpeed:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->rSpeed:F

    const/high16 v1, 0x45fa0000    # 8000.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 291
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->rSpeed:F

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    const v1, 0x461c4000    # 10000.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x43100000    # 144.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->zhuanSAngle:F

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    const/high16 v0, -0x3cf10000    # -143.0f

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->zhuanSAngle:F

    goto :goto_0
.end method


# virtual methods
.method public changeFragment(ILandroid/app/Fragment;)V
    .locals 2
    .param p1, "rootID"    # I
    .param p2, "to"    # Landroid/app/Fragment;

    .prologue
    .line 113
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/Launcher;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->manager:Landroid/app/FragmentManager;

    .line 114
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->manager:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 115
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, p1, p2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 116
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 117
    return-void
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mContext:Landroid/content/Context;

    .line 102
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 104
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " onCreateView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const v0, 0x7f030028

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    .line 124
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->initView()V

    .line 126
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 213
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 214
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 3
    .param p1, "hidden"    # Z

    .prologue
    .line 304
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " onHiddenChanged = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    if-eqz p1, :cond_0

    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->iUpdate:Z

    .line 307
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->meterHandler:Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 308
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->setSpeed()V

    .line 318
    :goto_0
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onHiddenChanged(Z)V

    .line 319
    return-void

    .line 310
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->iUpdate:Z

    .line 311
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->meterHandler:Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 312
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_body:Landroid/widget/ImageView;

    const v1, 0x7f02019f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_body:Landroid/widget/ImageView;

    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 148
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " onResume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onResume()V

    .line 150
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->initData()V

    .line 151
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 131
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " onStart "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_body:Landroid/widget/ImageView;

    const v1, 0x7f02019f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 137
    :goto_0
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 138
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;->car_body:Landroid/widget/ImageView;

    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 142
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " onStop = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 144
    return-void
.end method
