.class public Lcom/touchus/benchilauncher/fragment/FragmentPicShow;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentPicShow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;
    }
.end annotation


# instance fields
.field private arr:[Landroid/widget/ImageView;

.field private bitmap:Landroid/graphics/Bitmap;

.field private count:I

.field private fScale:F

.field private fangda:Landroid/widget/ImageView;

.field handler:Landroid/os/Handler;

.field private indexCount:I

.field private isBofang:Z

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mBofangPic:Landroid/widget/ImageView;

.field private mIDRIVERENUM:B

.field private mMBitmapda:Landroid/graphics/Bitmap;

.field private mMFl:Landroid/widget/FrameLayout;

.field private mMFliV:Landroid/widget/ImageView;

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field private mMuenPic:Landroid/widget/LinearLayout;

.field public mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;

.field private mPicName:Landroid/widget/TextView;

.field private mPosition:I

.field private mRootView:Landroid/view/View;

.field private mShangPic:Landroid/widget/ImageView;

.field private mXiaPic:Landroid/widget/ImageView;

.field private nBitmapWidth:I

.field private picCaidan:Landroid/widget/ImageView;

.field public picList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field private suoxiao:Landroid/widget/ImageView;

.field task:Ljava/util/TimerTask;

.field timer:Ljava/util/Timer;

.field private turnRotate:I

.field private url:Ljava/lang/String;

.field private youxuan:Landroid/widget/ImageView;

.field private zuoxuan:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 45
    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->count:I

    .line 55
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->isBofang:Z

    .line 71
    const/16 v0, 0x5a

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->turnRotate:I

    .line 72
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    .line 75
    const/16 v0, 0x8

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    .line 232
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$1;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->handler:Landroid/os/Handler;

    .line 246
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->timer:Ljava/util/Timer;

    .line 247
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$2;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    .line 333
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;I)V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->pressItem(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;I)V
    .locals 0

    .prologue
    .line 76
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;I)V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->count:I

    return v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;I)V
    .locals 0

    .prologue
    .line 45
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->count:I

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMuenPic:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 5
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 294
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 295
    .local v1, "picheight":I
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 297
    .local v2, "picwidth":I
    move v3, v1

    .line 298
    .local v3, "targetheight":I
    move v4, v2

    .line 299
    .local v4, "targetwidth":I
    const/4 v0, 0x1

    .line 300
    .local v0, "inSampleSize":I
    if-gt v3, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 301
    :cond_0
    :goto_0
    if-lt v3, p2, :cond_1

    if-ge v4, p1, :cond_2

    .line 309
    :cond_1
    return v0

    .line 303
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 304
    div-int v3, v1, v0

    .line 305
    div-int v4, v2, v0

    goto :goto_0
.end method

.method private dealScale()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 224
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 226
    .local v5, "matrix":Landroid/graphics/Matrix;
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 228
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 229
    .local v7, "bitmap2":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    return-void
.end method

.method public static decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 284
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 285
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 286
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 287
    invoke-static {v0, p1, p2}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 288
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 289
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private hideFangDa()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 137
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method private initData()V
    .locals 4

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "position"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    .line 123
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->url:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "picName":Ljava/lang/String;
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->url:Ljava/lang/String;

    const/16 v2, 0x31f

    const/16 v3, 0x18f

    invoke-static {v1, v2, v3}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    .line 129
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 130
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPicName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picCaidan:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$3;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->suoxiao:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$4;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$4;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fangda:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$5;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mBofangPic:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$6;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mXiaPic:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$7;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$7;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mShangPic:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$8;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$8;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFl:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$9;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$9;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    return-void
.end method

.method private pressItem(I)V
    .locals 14
    .param p1, "indexCount"    # I

    .prologue
    .line 386
    packed-switch p1, :pswitch_data_0

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 388
    :pswitch_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->hideFangDa()V

    .line 389
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 392
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    const v1, 0x3e4ccccd    # 0.2f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    .line 393
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 394
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    .line 396
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->nBitmapWidth:I

    if-gtz v0, :cond_2

    .line 398
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->nBitmapWidth:I

    .line 400
    :cond_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->dealScale()V

    goto :goto_0

    .line 403
    :pswitch_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->hideFangDa()V

    .line 404
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 407
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    const v1, 0x3e4ccccd    # 0.2f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    .line 408
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    float-to-double v0, v0

    const-wide v2, 0x3fc999999999999aL    # 0.2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    .line 409
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fScale:F

    .line 411
    :cond_3
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->nBitmapWidth:I

    if-gtz v0, :cond_4

    .line 413
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->nBitmapWidth:I

    .line 416
    :cond_4
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->dealScale()V

    goto :goto_0

    .line 419
    :pswitch_2
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    if-lez v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v0}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v12

    .line 423
    .local v12, "urll":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 427
    .local v8, "file":Ljava/io/File;
    const/16 v0, 0x31f

    const/16 v1, 0x18f

    invoke-static {v12, v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    .line 428
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 429
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v0}, Lcom/touchus/publicutils/bean/MediaBean;->getTitle()Ljava/lang/String;

    move-result-object v10

    .line 430
    .local v10, "picName":Ljava/lang/String;
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPicName:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    goto/16 :goto_0

    .line 434
    .end local v8    # "file":Ljava/io/File;
    .end local v10    # "picName":Ljava/lang/String;
    .end local v12    # "urll":Ljava/lang/String;
    :pswitch_3
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->isBofang:Z

    if-nez v0, :cond_7

    .line 435
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    if-eqz v0, :cond_5

    .line 436
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 437
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    .line 438
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$10;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow$10;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentPicShow;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    .line 447
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x7d0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 448
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->isBofang:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->isBofang:Z

    .line 449
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mBofangPic:Landroid/widget/ImageView;

    const v1, 0x7f0201d5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 448
    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    .line 451
    :cond_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 452
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->isBofang:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->isBofang:Z

    .line 453
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mBofangPic:Landroid/widget/ImageView;

    const v1, 0x7f0201cb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 452
    :cond_8
    const/4 v0, 0x1

    goto :goto_2

    .line 457
    :pswitch_4
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 460
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v0}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v13

    .line 461
    .local v13, "urlll":Ljava/lang/String;
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v0}, Lcom/touchus/publicutils/bean/MediaBean;->getTitle()Ljava/lang/String;

    move-result-object v11

    .line 462
    .local v11, "picName1":Ljava/lang/String;
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPicName:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    const/16 v0, 0x31f

    const/16 v1, 0x18f

    invoke-static {v13, v0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    .line 465
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 467
    .local v9, "file1":Ljava/io/File;
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 468
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPosition:I

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    goto/16 :goto_0

    .line 471
    .end local v9    # "file1":Ljava/io/File;
    .end local v11    # "picName1":Ljava/lang/String;
    .end local v13    # "urlll":Ljava/lang/String;
    :pswitch_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 474
    const/16 v0, -0x5a

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->turnRotate:I

    .line 475
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->toturn(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 476
    .local v6, "bitmap1":Landroid/graphics/Bitmap;
    iput-object v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    .line 477
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 481
    .end local v6    # "bitmap1":Landroid/graphics/Bitmap;
    :pswitch_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 484
    const/16 v0, 0x5a

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->turnRotate:I

    .line 485
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->toturn(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 486
    .local v7, "bitmap2":Landroid/graphics/Bitmap;
    iput-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    .line 487
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 490
    .end local v7    # "bitmap2":Landroid/graphics/Bitmap;
    :pswitch_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->handleBackAction()V

    goto/16 :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 314
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 321
    return-void

    .line 315
    :cond_0
    if-ne p1, v0, :cond_1

    .line 316
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 314
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 353
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_2

    .line 354
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 355
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    .line 358
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_0

    .line 359
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_3

    .line 360
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMuenPic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 361
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    const/4 v2, 0x7

    if-ge v1, v2, :cond_1

    .line 362
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    .line 364
    :cond_1
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->seclectTrue(I)V

    .line 383
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_0
    return-void

    .line 366
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_3
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_4

    .line 367
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->pressItem(I)V

    goto :goto_0

    .line 368
    :cond_4
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_2

    .line 370
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_2

    .line 372
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_5

    .line 373
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 374
    :cond_5
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMuenPic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 375
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    if-lez v1, :cond_6

    .line 376
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    .line 378
    :cond_6
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->seclectTrue(I)V

    goto :goto_0
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 272
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b00d4

    if-ne v0, v1, :cond_1

    .line 273
    const/4 v0, 0x5

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    .line 277
    :cond_0
    :goto_0
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->seclectTrue(I)V

    .line 278
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->pressItem(I)V

    .line 279
    return-void

    .line 274
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b00d5

    if-ne v0, v1, :cond_0

    .line 275
    const/4 v0, 0x6

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->indexCount:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    const v0, 0x7f030024

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    .line 86
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 87
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 89
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFl:Landroid/widget/FrameLayout;

    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mShangPic:Landroid/widget/ImageView;

    .line 91
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mXiaPic:Landroid/widget/ImageView;

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mBofangPic:Landroid/widget/ImageView;

    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMuenPic:Landroid/widget/LinearLayout;

    .line 94
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->zuoxuan:Landroid/widget/ImageView;

    .line 95
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->youxuan:Landroid/widget/ImageView;

    .line 96
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fangda:Landroid/widget/ImageView;

    .line 97
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->suoxiao:Landroid/widget/ImageView;

    .line 98
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picCaidan:Landroid/widget/ImageView;

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMFliV:Landroid/widget/ImageView;

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mPicName:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->fangda:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->suoxiao:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 104
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mShangPic:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mBofangPic:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 106
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mXiaPic:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 107
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->zuoxuan:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 108
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->youxuan:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 109
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picCaidan:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 111
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->initData()V

    .line 112
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->initListener()V

    .line 113
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->hideFangDa()V

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->zuoxuan:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->youxuan:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentPicShow$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 326
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->task:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 329
    :cond_0
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 330
    return-void
.end method

.method public toturn(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "img"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 259
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 260
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 261
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->turnRotate:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 262
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 263
    .local v3, "width":I
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 264
    .local v4, "height":I
    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 266
    return-object p1
.end method
