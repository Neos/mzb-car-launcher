.class public Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentMusicOutUSB.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/touchus/benchilauncher/base/BaseFragment;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field isUSB1:Z

.field private mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mContext:Lcom/touchus/benchilauncher/Launcher;

.field private mCount:I

.field private mIDRIVERENUM:B

.field private mListView:Landroid/widget/ListView;

.field public mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;

.field private mRootView:Landroid/view/View;

.field private mediaBeans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field private noDataTv:Landroid/widget/TextView;

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->isUSB1:Z

    .line 43
    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    .line 44
    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    .line 116
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;

    .line 36
    return-void
.end method

.method private artMusic()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 273
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-nez v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v2, Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/touchus/benchilauncher/service/MusicPlayControl;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 258
    :cond_1
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 259
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 260
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 259
    invoke-static {v1}, Lcom/touchus/benchilauncher/service/MusicLoader;->instance(Landroid/content/ContentResolver;)Lcom/touchus/benchilauncher/service/MusicLoader;

    move-result-object v0

    .line 261
    .local v0, "musicLoader":Lcom/touchus/benchilauncher/service/MusicLoader;
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicLoader;->getMusicList()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    .line 263
    .end local v0    # "musicLoader":Lcom/touchus/benchilauncher/service/MusicLoader;
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setPlayList(Ljava/util/List;)V

    .line 264
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 265
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 266
    sget v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 267
    sput v4, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    .line 268
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v2, "musicPos"

    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 270
    :cond_3
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    sget v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->setSeclectIndex(I)V

    .line 271
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    sget v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 272
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged(I)V

    goto :goto_0
.end method

.method private artPic()V
    .locals 2

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 279
    :cond_0
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 280
    new-instance v0, Lcom/touchus/benchilauncher/service/PictrueUtil;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/service/PictrueUtil;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/PictrueUtil;->getPicList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 283
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 284
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    sget v1, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->setSeclectIndex(I)V

    .line 285
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 286
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged(I)V

    goto :goto_0
.end method

.method private artVedio()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 290
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 293
    :cond_0
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 294
    new-instance v0, Lcom/touchus/benchilauncher/service/VideoService;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/service/VideoService;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/VideoService;->getVideoList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 297
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 299
    sput v2, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    .line 300
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "videoPos"

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    sget v1, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->setSeclectIndex(I)V

    .line 303
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 304
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    sget v1, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 305
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged(I)V

    goto :goto_0
.end method

.method private changeToPicShowiew()V
    .locals 4

    .prologue
    .line 348
    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 357
    :goto_0
    return-void

    .line 351
    :cond_0
    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;-><init>()V

    .line 352
    .local v1, "fragmentShow":Lcom/touchus/benchilauncher/fragment/FragmentPicShow;
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 353
    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    iput-object v2, v1, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->picList:Ljava/util/List;

    .line 354
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 355
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "position"

    sget v3, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 356
    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/fragment/FragmentPicShow;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private changeToPlayMusicView()V
    .locals 4

    .prologue
    .line 325
    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 333
    :goto_0
    return-void

    .line 328
    :cond_0
    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;-><init>()V

    .line 329
    .local v1, "fragmentShow":Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 330
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 331
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "position"

    sget v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 332
    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private changeToPlayVideoView()V
    .locals 4

    .prologue
    .line 336
    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 345
    :goto_0
    return-void

    .line 339
    :cond_0
    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;-><init>()V

    .line 340
    .local v1, "fragmentShow":Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    iput-object v2, v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->mediaBeans:Ljava/util/List;

    .line 341
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 342
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "position"

    sget v3, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 343
    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;->setArguments(Landroid/os/Bundle;)V

    .line 344
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/Launcher;->changeToPlayVideo(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V

    goto :goto_0
.end method

.method private initList()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 78
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    if-ne v0, v2, :cond_1

    .line 79
    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    .line 80
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->artVedio()V

    .line 91
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->noDataTv:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    :goto_1
    return-void

    .line 81
    :cond_1
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    if-ne v0, v3, :cond_2

    .line 82
    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    .line 83
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->artPic()V

    goto :goto_0

    .line 85
    :cond_2
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    .line 86
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->artMusic()V

    .line 87
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    if-eqz v0, :cond_0

    .line 88
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPlayMusicView()V

    goto :goto_0

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->noDataTv:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mRootView:Landroid/view/View;

    const v1, 0x7f0b008c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->noDataTv:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->noDataTv:Landroid/widget/TextView;

    const v1, 0x7f070078

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 107
    return-void
.end method


# virtual methods
.method public down(I)V
    .locals 5
    .param p1, "seclectIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 229
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    .line 230
    .local v1, "lastVisiblePosition":I
    if-nez p1, :cond_2

    .line 231
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 232
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 234
    :cond_0
    iget p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 235
    add-int/lit8 v2, v1, -0x1

    if-le p1, v2, :cond_1

    .line 236
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, p1, v4}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 246
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->setSeclectIndex(I)V

    .line 247
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged(I)V

    .line 248
    return-void

    .line 239
    :cond_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 240
    .local v0, "firstVisiblePosition":I
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 241
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_3

    .line 242
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 244
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    add-int v3, v0, p1

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    goto :goto_0
.end method

.method public handlerMsg(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 135
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x1771

    if-ne v3, v4, :cond_d

    .line 136
    const/4 v1, 0x0

    .line 137
    .local v1, "seclectIndex":I
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-nez v3, :cond_4

    .line 138
    sget v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 145
    .local v2, "temp":Landroid/os/Bundle;
    const-string v3, "idriver_enum"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    iput-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    .line 146
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v3, v4, :cond_1

    .line 147
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_6

    .line 148
    :cond_1
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->down(I)V

    .line 165
    :cond_2
    :goto_1
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-nez v3, :cond_b

    .line 166
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    sput v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    .line 203
    .end local v1    # "seclectIndex":I
    .end local v2    # "temp":Landroid/os/Bundle;
    :cond_3
    :goto_2
    return-void

    .line 139
    .restart local v1    # "seclectIndex":I
    :cond_4
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v3, v5, :cond_5

    .line 140
    sget v3, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    goto :goto_0

    .line 141
    :cond_5
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v3, v6, :cond_0

    .line 142
    sget v3, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    goto :goto_0

    .line 149
    .restart local v2    # "temp":Landroid/os/Bundle;
    :cond_6
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 150
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v3, v4, :cond_7

    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_8

    .line 151
    :cond_7
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->up(I)V

    goto :goto_1

    .line 152
    :cond_8
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v3, v4, :cond_2

    .line 154
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v3, v4, :cond_2

    .line 156
    iget-byte v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v3, v4, :cond_2

    .line 157
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-nez v3, :cond_9

    .line 158
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPlayMusicView()V

    goto :goto_1

    .line 159
    :cond_9
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v3, v5, :cond_a

    .line 160
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPlayVideoView()V

    goto :goto_1

    .line 161
    :cond_a
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v3, v6, :cond_2

    .line 162
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPicShowiew()V

    goto :goto_1

    .line 167
    :cond_b
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v3, v5, :cond_c

    .line 168
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    sput v3, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    goto :goto_2

    .line 169
    :cond_c
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v3, v6, :cond_3

    .line 170
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    sput v3, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    goto :goto_2

    .line 172
    .end local v1    # "seclectIndex":I
    .end local v2    # "temp":Landroid/os/Bundle;
    :cond_d
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x1b5a

    if-ne v3, v4, :cond_10

    .line 173
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 174
    .restart local v2    # "temp":Landroid/os/Bundle;
    const-string v3, "usblistener"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 177
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 179
    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 181
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    .line 183
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mediaBeans:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 184
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged()V

    goto/16 :goto_2

    .line 185
    :cond_e
    const-string v3, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 187
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->serviceHandler:Landroid/os/Handler;

    if-eqz v3, :cond_3

    .line 190
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const v4, 0x7f07006f

    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/MainService;->createLoadingFloatView(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 191
    :cond_f
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 193
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 195
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    .line 197
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->initList()V

    goto/16 :goto_2

    .line 199
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "temp":Landroid/os/Bundle;
    :cond_10
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x407

    if-ne v3, v4, :cond_3

    .line 200
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->initList()V

    goto/16 :goto_2
.end method

.method public onBack()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 111
    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 112
    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    const v0, 0x7f030022

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mRootView:Landroid/view/View;

    .line 57
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 58
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 60
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->initView()V

    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 367
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 368
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x1

    .line 311
    invoke-virtual {p2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 312
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-nez v0, :cond_0

    .line 313
    sput p3, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    .line 314
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPlayMusicView()V

    .line 322
    :goto_0
    return-void

    .line 315
    :cond_0
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    if-ne v0, v1, :cond_1

    .line 316
    sput p3, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    .line 317
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPlayVideoView()V

    goto :goto_0

    .line 319
    :cond_1
    sput p3, Lcom/touchus/benchilauncher/LauncherApplication;->imageIndex:I

    .line 320
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->changeToPicShowiew()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 68
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->checkUSBExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isScanner:Z

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const v1, 0x7f07006f

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService;->createLoadingFloatView(Ljava/lang/String;)V

    .line 73
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->initList()V

    .line 74
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 75
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 362
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 363
    return-void
.end method

.method public up(I)V
    .locals 5
    .param p1, "seclectIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 206
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 207
    .local v0, "firstVisiblePosition":I
    if-nez p1, :cond_2

    .line 208
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    if-lez v2, :cond_0

    .line 209
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 211
    :cond_0
    iget p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 212
    add-int/lit8 v2, v0, 0x1

    if-ge p1, v2, :cond_1

    .line 213
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    add-int/lit8 v3, p1, -0x3

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 224
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->setSeclectIndex(I)V

    .line 225
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mAdapter:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->type:I

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged(I)V

    .line 226
    return-void

    .line 216
    :cond_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    .line 217
    .local v1, "lastVisiblePosition":I
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 218
    iget v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    if-gez v2, :cond_3

    .line 219
    iput v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mCount:I

    .line 221
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;->mListView:Landroid/widget/ListView;

    add-int v3, v1, p1

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    goto :goto_0
.end method
