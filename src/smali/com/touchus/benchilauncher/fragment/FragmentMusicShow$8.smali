.class Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$8;
.super Ljava/lang/Object;
.source "FragmentMusicShow.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->initListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$8;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 272
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 275
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$8;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    invoke-static {v0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->access$1(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 280
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 279
    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->changeCurrentPlayProgress(I)V

    .line 281
    return-void
.end method
