.class Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;
.super Ljava/lang/Object;
.source "FragmentMusicShow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 289
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    iget-object v2, v2, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->mMusicHandlerx:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    invoke-static {v3}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->access$0(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$MusicHandlerx;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 291
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->access$1(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->getPosition()I

    move-result v0

    .line 292
    .local v0, "curPro":I
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->access$2(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 293
    div-int/lit16 v2, v0, 0x3e8

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "temp":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow$1;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;

    invoke-static {v2}, Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;->access$3(Lcom/touchus/benchilauncher/fragment/FragmentMusicShow;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
