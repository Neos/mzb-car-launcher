.class public Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentMusicLanYa.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;
    }
.end annotation


# instance fields
.field private arr:[Landroid/widget/ImageView;

.field private count:I

.field private isNoXuanZhuan:Z

.field private isPlaying:Z

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mBofang:Landroid/widget/ImageView;

.field private mContext:Lcom/touchus/benchilauncher/Launcher;

.field private mIDRIVERENUM:B

.field private mMTitle:Landroid/widget/TextView;

.field public mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

.field private mRootView:Landroid/view/View;

.field private mShang:Landroid/widget/ImageView;

.field private mXiayiqu:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 24
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 25
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    .line 198
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

    .line 281
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isNoXuanZhuan:Z

    .line 24
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;Z)V
    .locals 0

    .prologue
    .line 281
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isNoXuanZhuan:Z

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;I)V
    .locals 0

    .prologue
    .line 43
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;I)V
    .locals 0

    .prologue
    .line 283
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;I)V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->setImageSeclect(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    return v0
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;Z)V
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    return-void
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    return-object v0
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$1;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$2;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$3;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    return-void
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/MyTextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    .line 176
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    .line 178
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    .line 179
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 180
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 181
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    .line 183
    return-void
.end method

.method private seclectTrue(I)V
    .locals 7
    .param p1, "seclectCuttun"    # I

    .prologue
    const v6, 0x7f020017

    const v5, 0x7f020016

    const v4, 0x7f020002

    const/4 v3, 0x1

    .line 284
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 319
    return-void

    .line 285
    :cond_0
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isNoXuanZhuan:Z

    if-eqz v1, :cond_3

    .line 286
    if-eq p1, v3, :cond_1

    .line 287
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 288
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 313
    :goto_1
    if-ne p1, v0, :cond_7

    .line 314
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 284
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    :cond_1
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    if-eqz v1, :cond_2

    .line 291
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 293
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 297
    :cond_3
    if-eq p1, v3, :cond_5

    .line 298
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    if-eqz v1, :cond_4

    .line 299
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 301
    :cond_4
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    const v2, 0x7f020003

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 304
    :cond_5
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    if-eqz v1, :cond_6

    .line 305
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 307
    :cond_6
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 316
    :cond_7
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->arr:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_2
.end method

.method private setImageSeclect(I)V
    .locals 4
    .param p1, "postion"    # I

    .prologue
    const-wide/16 v2, 0x1f4

    .line 322
    if-nez p1, :cond_1

    .line 323
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 324
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$4;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$4;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 332
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$5;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private settingPlayName()V
    .locals 7

    .prologue
    const v2, 0x7f070032

    const v6, 0x7f020003

    const v5, 0x7f020002

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 84
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    const v2, 0x7f07003e

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 87
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mShang:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mXiayiqu:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 96
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    if-ne v0, v3, :cond_2

    .line 97
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 98
    :cond_2
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    const/16 v1, 0x58a

    if-ne v0, v1, :cond_5

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v1, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_3
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v4, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    .line 104
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-ne v0, v3, :cond_4

    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 107
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    const v1, 0x7f020017

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 109
    :cond_5
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    const/16 v1, 0x58b

    if-ne v0, v1, :cond_7

    .line 110
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    const v2, 0x7f07003d

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iput-boolean v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 113
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-ne v0, v3, :cond_6

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 116
    :cond_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 118
    :cond_7
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    const/16 v1, 0x58c

    if-ne v0, v1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iput-boolean v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 122
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-ne v0, v3, :cond_8

    .line 123
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 125
    :cond_8
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 217
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-nez v1, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 231
    :goto_1
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x1771

    if-ne v1, v4, :cond_0

    .line 232
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 233
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mIDRIVERENUM:B

    .line 235
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v1, v4, :cond_2

    .line 236
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_5

    .line 237
    :cond_2
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isNoXuanZhuan:Z

    .line 239
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-ge v1, v5, :cond_3

    .line 240
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    .line 242
    :cond_3
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->seclectTrue(I)V

    goto :goto_0

    .line 222
    .end local v0    # "temp":Landroid/os/Bundle;
    :pswitch_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v1, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 223
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v4, v4, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :cond_4
    :pswitch_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->settingPlayName()V

    goto :goto_1

    .line 244
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_5
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 245
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v1, v4, :cond_6

    .line 246
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_8

    .line 248
    :cond_6
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isNoXuanZhuan:Z

    .line 249
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-lez v1, :cond_7

    .line 250
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    .line 252
    :cond_7
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->seclectTrue(I)V

    goto :goto_0

    .line 254
    :cond_8
    iget-byte v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mIDRIVERENUM:B

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_0

    .line 255
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-nez v1, :cond_a

    .line 256
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 257
    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->setImageSeclect(I)V

    .line 258
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->playPrev()V

    .line 276
    :cond_9
    :goto_2
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->seclectTrue(I)V

    goto/16 :goto_0

    .line 259
    :cond_a
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-ne v1, v3, :cond_e

    .line 260
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    if-nez v1, :cond_c

    .line 261
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    const v4, 0x7f020016

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 262
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    if-eqz v1, :cond_b

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 263
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    .line 264
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    goto :goto_2

    :cond_b
    move v1, v3

    .line 262
    goto :goto_3

    .line 266
    :cond_c
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mBofang:Landroid/widget/ImageView;

    const v4, 0x7f020002

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 267
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    if-eqz v1, :cond_d

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 268
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    .line 269
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    goto :goto_2

    :cond_d
    move v1, v3

    .line 267
    goto :goto_4

    .line 271
    :cond_e
    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->count:I

    if-ne v1, v5, :cond_9

    .line 272
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->isPlaying:Z

    .line 273
    invoke-direct {p0, v5}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->setImageSeclect(I)V

    .line 274
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->playNext()V

    goto :goto_2

    .line 220
    :pswitch_data_0
    .packed-switch 0x58a
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBack()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 193
    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 194
    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 50
    const v0, 0x7f030021

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mRootView:Landroid/view/View;

    .line 52
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 53
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 54
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->initView()V

    .line 56
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->initListener()V

    .line 58
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 188
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 189
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestMusicAudioFocus()V

    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->readMediaStatus()V

    .line 67
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 68
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->settingPlayName()V

    .line 69
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_0

    .line 70
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    const/16 v1, 0x58a

    if-ne v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->btMusicConnect()V

    .line 73
    :cond_0
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 74
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;->mMusicHandler:Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa$MusicLanyaHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 79
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 80
    return-void
.end method
