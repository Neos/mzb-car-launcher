.class Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "FragmentYingyong.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/fragment/FragmentYingyong;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppsGridAdapter"
.end annotation


# instance fields
.field private currentPage:I

.field private listData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/touchus/benchilauncher/fragment/FragmentYingyong;


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "page"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 358
    .local p3, "_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentYingyong;

    .line 357
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 355
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->currentPage:I

    .line 359
    iput-object p2, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->mContext:Landroid/content/Context;

    .line 360
    iput-object p3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->listData:Ljava/util/List;

    .line 361
    iput p4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->currentPage:I

    .line 362
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->listData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 404
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->listData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 408
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 370
    if-nez p2, :cond_0

    .line 371
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->mContext:Landroid/content/Context;

    const v6, 0x7f03002a

    .line 372
    const/4 v7, 0x0

    .line 371
    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 375
    :cond_0
    const v5, 0x7f0b00f4

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 374
    check-cast v2, Landroid/widget/ImageView;

    .line 377
    .local v2, "mIv":Landroid/widget/ImageView;
    const v5, 0x7f0b00f5

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 376
    check-cast v3, Landroid/widget/TextView;

    .line 378
    .local v3, "mTv":Landroid/widget/TextView;
    const v5, 0x7f0b0011

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 379
    .local v4, "pp":Landroid/widget/LinearLayout;
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->listData:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 380
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v5, :cond_1

    iget v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->currentPage:I

    if-nez v5, :cond_1

    if-nez p1, :cond_1

    .line 381
    const v5, 0x7f0200d9

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 382
    const v5, 0x7f070051

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 389
    :goto_0
    iget v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->currentPage:I

    mul-int/lit8 v5, v5, 0x8

    add-int v0, v5, p1

    .line 391
    .local v0, "index":I
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->this$0:Lcom/touchus/benchilauncher/fragment/FragmentYingyong;

    invoke-static {v5}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->access$0(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)I

    move-result v5

    if-ne v5, v0, :cond_2

    .line 392
    const v5, 0x7f02007d

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 396
    :goto_1
    return-object p2

    .line 384
    .end local v0    # "index":I
    :cond_1
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->mContext:Landroid/content/Context;

    .line 385
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 384
    invoke-virtual {v5, v6}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 386
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 387
    iget-object v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 386
    invoke-virtual {v5, v6}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 387
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 386
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 394
    .restart local v0    # "index":I
    :cond_2
    const v5, 0x7f02007c

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public setData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 365
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->listData:Ljava/util/List;

    .line 366
    return-void
.end method
