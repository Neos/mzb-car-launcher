.class public Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentMediaMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;
    }
.end annotation


# instance fields
.field private currentPlayInfoTview:Landroid/widget/TextView;

.field currentShowData:[Ljava/lang/String;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field mContext:Lcom/touchus/benchilauncher/Launcher;

.field public mHandler:Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;

.field private mIvMenu:Landroid/widget/ImageView;

.field private mKey:Ljava/lang/String;

.field private mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

.field private mRootView:Landroid/view/View;

.field rotateData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field texts1:[Ljava/lang/String;

.field texts2:[Ljava/lang/String;

.field texts3:[Ljava/lang/String;

.field texts4:[Ljava/lang/String;

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    .line 240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->rotateData:Ljava/util/Map;

    .line 271
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mHandler:Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;I)V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->settingCurrentShowData(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->handleClick()V

    return-void
.end method

.method private handleClick()V
    .locals 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    const v1, 0x7f070041

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 189
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    const v1, 0x7f070042

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    const v1, 0x7f070043

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 195
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 196
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    const v1, 0x7f070040

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method private initData()V
    .locals 5

    .prologue
    .line 233
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x5

    if-lt v0, v2, :cond_0

    .line 238
    return-void

    .line 234
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 235
    const v3, 0x7f03002f

    const/4 v4, 0x0

    .line 234
    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 236
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->addView(Landroid/view/View;)V

    .line 233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private initLinstener()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mIvMenu:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 144
    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$1;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setOnItemSelectedListener(Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;)V

    .line 152
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$2;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setOnItemClickListener(Lcom/touchus/benchilauncher/inface/OnItemClickListener;)V

    .line 158
    return-void
.end method

.method private initLoopRotarySwitchView()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->width:I

    div-int/lit8 v1, v1, 0x5

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setR(F)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 207
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setChildViewText([Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method private initRotateData()V
    .locals 8

    .prologue
    const v7, 0x7f070043

    const v6, 0x7f070042

    const v5, 0x7f070041

    const v4, 0x7f070040

    const/4 v3, 0x0

    .line 244
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1, v5}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 245
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 246
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 247
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 248
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v4}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 244
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts1:[Ljava/lang/String;

    .line 249
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1, v6}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 250
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 251
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v4}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 252
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v4}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 253
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v5}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 249
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts2:[Ljava/lang/String;

    .line 254
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1, v7}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 255
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v4}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 256
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v5}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 257
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v5}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 258
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 254
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts3:[Ljava/lang/String;

    .line 259
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v1, v4}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 260
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v5}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 261
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 262
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 263
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 259
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts4:[Ljava/lang/String;

    .line 264
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->rotateData:Ljava/util/Map;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts1:[Ljava/lang/String;

    aget-object v1, v1, v3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts1:[Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->rotateData:Ljava/util/Map;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts2:[Ljava/lang/String;

    aget-object v1, v1, v3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts2:[Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->rotateData:Ljava/util/Map;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts3:[Ljava/lang/String;

    aget-object v1, v1, v3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts3:[Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->rotateData:Ljava/util/Map;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts4:[Ljava/lang/String;

    aget-object v1, v1, v3

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts4:[Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts1:[Ljava/lang/String;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    .line 269
    return-void
.end method

.method private initView()V
    .locals 4

    .prologue
    .line 215
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mRootView:Landroid/view/View;

    .line 216
    const v3, 0x7f0b00b6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 215
    iput-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 217
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mRootView:Landroid/view/View;

    .line 218
    const v3, 0x7f0b00b8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 217
    iput-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentPlayInfoTview:Landroid/widget/TextView;

    .line 219
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mRootView:Landroid/view/View;

    const v3, 0x7f0b00b7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mIvMenu:Landroid/widget/ImageView;

    .line 221
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 222
    .local v0, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 223
    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 222
    check-cast v1, Landroid/view/WindowManager;

    .line 224
    .local v1, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 225
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->width:I

    .line 227
    return-void
.end method

.method private selectStateChange(Ljava/lang/String;)V
    .locals 2
    .param p1, "temp"    # Ljava/lang/String;

    .prologue
    .line 167
    const v0, 0x7f070041

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mIvMenu:Landroid/widget/ImageView;

    const v1, 0x7f0201d8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 169
    const/4 v0, 0x0

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    const v0, 0x7f070042

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mIvMenu:Landroid/widget/ImageView;

    const v1, 0x7f0200c0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 172
    const/4 v0, 0x1

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    goto :goto_0

    .line 173
    :cond_2
    const v0, 0x7f070043

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mIvMenu:Landroid/widget/ImageView;

    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 175
    const/4 v0, 0x2

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    goto :goto_0

    .line 176
    :cond_3
    const v0, 0x7f070040

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mIvMenu:Landroid/widget/ImageView;

    const v1, 0x7f020094

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    const/4 v0, 0x3

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    goto :goto_0
.end method

.method private settingCurrentShowData(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_0

    .line 138
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    aget-object v3, v3, p1

    iput-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    .line 125
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->rotateData:Ljava/util/Map;

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 126
    .local v2, "tempData":[Ljava/lang/String;
    const/4 v3, 0x5

    new-array v1, v3, [Ljava/lang/String;

    .line 127
    .local v1, "newData":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v2

    if-lt v0, v3, :cond_1

    .line 134
    iput-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    .line 135
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-virtual {v3, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setChildViewText([Ljava/lang/String;)V

    .line 136
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setTextViewText()V

    .line 137
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mKey:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->selectStateChange(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_1
    if-ge v0, p1, :cond_2

    .line 129
    add-int/lit8 v3, p1, -0x1

    sub-int/2addr v3, v0

    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    aget-object v4, v2, v4

    aput-object v4, v1, v3

    .line 127
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 132
    :cond_2
    sub-int v3, v0, p1

    aget-object v3, v2, v3

    aput-object v3, v1, v0

    goto :goto_2
.end method


# virtual methods
.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_0

    .line 294
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 295
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 297
    .local v0, "code":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_2

    .line 298
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->handleClick()V

    goto :goto_0

    .line 299
    :cond_2
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_3

    .line 300
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_4

    .line 301
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 302
    sget-object v3, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setAutoScrollDirection(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 303
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->doRotain()V

    goto :goto_0

    .line 304
    :cond_4
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_5

    .line 305
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_0

    .line 306
    :cond_5
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 307
    sget-object v3, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setAutoScrollDirection(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 308
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->doRotain()V

    goto :goto_0
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 115
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b00b7

    if-ne v0, v1, :cond_0

    .line 116
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->handleClick()V

    .line 118
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    const v0, 0x7f030020

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mRootView:Landroid/view/View;

    .line 55
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 58
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->initView()V

    .line 59
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->initData()V

    .line 60
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->initRotateData()V

    .line 61
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->initLoopRotarySwitchView()V

    .line 62
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->initLinstener()V

    .line 63
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    if-eqz v0, :cond_0

    .line 64
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicOutUSB;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/fragment/FragmentMusicLanYa;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 106
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mHandler:Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 100
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onPause()V

    .line 101
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mHandler:Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu$USBMusicHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 94
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onResume()V

    .line 95
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 75
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 76
    const/4 v0, 0x0

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    .line 78
    :cond_0
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    if-nez v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts1:[Ljava/lang/String;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    .line 87
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->mLoopRotarySwitchView:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setChildViewText([Ljava/lang/String;)V

    .line 88
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 89
    return-void

    .line 80
    :cond_2
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 81
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts2:[Ljava/lang/String;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    goto :goto_0

    .line 82
    :cond_3
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 83
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts3:[Ljava/lang/String;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    goto :goto_0

    .line 84
    :cond_4
    sget v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 85
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->texts4:[Ljava/lang/String;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;->currentShowData:[Ljava/lang/String;

    goto :goto_0
.end method
