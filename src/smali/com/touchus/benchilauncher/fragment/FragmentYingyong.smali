.class public Lcom/touchus/benchilauncher/fragment/FragmentYingyong;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "FragmentYingyong.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;,
        Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;,
        Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/touchus/benchilauncher/base/BaseFragment;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;"
    }
.end annotation


# instance fields
.field appBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private currentPage:I

.field private dot_container:Landroid/widget/LinearLayout;

.field private lastPage:I

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mAppHandler:Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;

.field private mContext:Lcom/touchus/benchilauncher/Launcher;

.field private mNeedShowApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRootView:Landroid/view/View;

.field private pageCount:I

.field private selectIndex:I

.field private viewPageListData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/GridView;",
            ">;"
        }
    .end annotation
.end field

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    .line 65
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 67
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    .line 68
    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->lastPage:I

    .line 413
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;

    invoke-direct {v0, p0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mAppHandler:Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;

    .line 507
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$1;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->appBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 59
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    return v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->clearAllDotState()V

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;I)V
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    return v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;I)V
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->lastPage:I

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->lastPage:I

    return v0
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;I)V
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    return-void
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addAppReceiver()V
    .locals 3

    .prologue
    .line 496
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 497
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 498
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 499
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 500
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 501
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->appBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 502
    return-void
.end method

.method private clearAllDotState()V
    .locals 3

    .prologue
    .line 220
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 223
    return-void

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private initDot()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 226
    iput v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->pageCount:I

    .line 227
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 228
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    div-int/lit8 v3, v3, 0x8

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->pageCount:I

    .line 230
    :cond_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 231
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 233
    :cond_1
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 236
    .local v2, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->pageCount:I

    if-lt v0, v3, :cond_2

    .line 250
    return-void

    .line 237
    :cond_2
    new-instance v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 238
    .local v1, "img":Landroid/widget/ImageView;
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .line 240
    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 238
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    const v3, 0x7f02007b

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 242
    if-eqz v0, :cond_3

    .line 243
    const/16 v3, 0x14

    invoke-virtual {v2, v3, v5, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 244
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    :goto_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_3
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_1
.end method

.method private initViewPageData()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    .line 253
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    .line 254
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->pageCount:I

    if-lt v1, v7, :cond_0

    .line 278
    return-void

    .line 255
    :cond_0
    new-instance v4, Landroid/widget/GridView;

    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v4, v7}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 256
    .local v4, "mGridView":Landroid/widget/GridView;
    new-instance v7, Landroid/view/ViewGroup$LayoutParams;

    .line 258
    invoke-direct {v7, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 256
    invoke-virtual {v4, v7}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 259
    const/4 v7, 0x4

    invoke-virtual {v4, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 260
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 261
    .local v5, "mLists1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    mul-int/lit8 v6, v1, 0x8

    .line 262
    .local v6, "star":I
    add-int/lit8 v7, v1, 0x1

    mul-int/lit8 v0, v7, 0x8

    .line 263
    .local v0, "end":I
    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v0, v7, :cond_1

    .line 264
    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    .line 266
    :cond_1
    move v2, v6

    .local v2, "j":I
    :goto_1
    if-lt v2, v0, :cond_2

    .line 270
    new-instance v3, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;

    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v3, p0, v7, v5, v1}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 272
    .local v3, "mAppsAdapter":Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;
    invoke-virtual {v4, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 273
    new-instance v7, Landroid/graphics/drawable/ColorDrawable;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v4, v7}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 274
    invoke-virtual {v4, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 275
    invoke-virtual {v4, p0}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 276
    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 267
    .end local v3    # "mAppsAdapter":Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;
    :cond_2
    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private loadApps()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 281
    new-instance v4, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    const/4 v9, 0x0

    invoke-direct {v4, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 282
    .local v4, "mainIntent":Landroid/content/Intent;
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    iget-object v8, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v8}, Lcom/touchus/benchilauncher/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 284
    invoke-virtual {v8, v4, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 285
    .local v6, "temps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v8, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 286
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-lt v1, v8, :cond_0

    .line 314
    sget-boolean v8, Lcom/touchus/benchilauncher/SysConst;->DV_CUSTOM:Z

    .line 317
    iget-object v8, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    new-instance v9, Landroid/content/pm/ResolveInfo;

    invoke-direct {v9}, Landroid/content/pm/ResolveInfo;-><init>()V

    invoke-interface {v8, v7, v9}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 318
    return-void

    .line 287
    :cond_0
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    .line 288
    .local v5, "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v2, 0x0

    .line 289
    .local v2, "iIsSystem":Z
    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 291
    .local v0, "curPkg":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v8}, Lcom/touchus/benchilauncher/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 292
    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 293
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    iget v8, v3, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v8, v8, 0x1

    if-lez v8, :cond_4

    const/4 v2, 0x1

    .line 297
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    :goto_1
    if-nez v2, :cond_1

    const-string v8, "com.touchus.factorytest"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 298
    const-string v8, "com.touchus.benchilauncher"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 299
    :cond_1
    const-string v8, "com.mediatek.filemanager"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 300
    const-string v8, "com.autonavi.amapauto"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 301
    const-string v8, "com.unibroad.voiceassistant"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 302
    const-string v8, "com.uc.browser.hd"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 303
    const-string v8, "com.android.vending"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 304
    const-string v8, "com.android.chrome"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 305
    const-string v8, "com.google.android.apps.maps"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 306
    const-string v8, "com.google.android.youtube"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 307
    const-string v8, "com.google.android.gms"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 308
    const-string v8, "com.papago.s1OBU"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 309
    const-string v8, "com.tima.carnet.vt"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 311
    :cond_2
    iget-object v8, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .restart local v3    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_4
    move v2, v7

    .line 293
    goto :goto_1

    .line 294
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v8

    goto :goto_1
.end method

.method private startApp()V
    .locals 7

    .prologue
    .line 146
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    iget v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 147
    .local v3, "info":Landroid/content/pm/ResolveInfo;
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v5, :cond_0

    iget v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    if-nez v5, :cond_0

    .line 152
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 153
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/MainService;->createNoTouchScreens()V

    .line 154
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v5

    sget-object v6, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->RECORDER:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v5, v6}, Lcom/backaudio/android/driver/Mainboard;->showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    .line 169
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 160
    .local v4, "pkg":Ljava/lang/String;
    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 162
    .local v0, "cls":Ljava/lang/String;
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .local v1, "componet":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 165
    .local v2, "i":Landroid/content/Intent;
    const/high16 v5, 0x10000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 166
    const/high16 v5, 0x200000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 167
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 168
    iget-object v5, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v5, v2}, Lcom/touchus/benchilauncher/Launcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private unAppReceiver()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->appBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 505
    return-void
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 432
    iget v3, p1, Landroid/os/Message;->what:I

    const/16 v4, 0x1771

    if-ne v3, v4, :cond_3

    .line 433
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 434
    .local v2, "temp":Landroid/os/Bundle;
    const-string v3, "idriver_enum"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 435
    .local v0, "code":B
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-eq v0, v3, :cond_0

    .line 436
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-ne v0, v3, :cond_4

    .line 437
    :cond_0
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 438
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 439
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 441
    :cond_1
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    div-int/lit8 v1, v3, 0x8

    .line 442
    .local v1, "cur":I
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    if-eq v1, v3, :cond_2

    .line 443
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v1, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 445
    :cond_2
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 446
    invoke-virtual {v3}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 445
    check-cast v3, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;

    .line 446
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->notifyDataSetChanged()V

    .line 493
    .end local v0    # "code":B
    .end local v1    # "cur":I
    .end local v2    # "temp":Landroid/os/Bundle;
    :cond_3
    :goto_0
    return-void

    .line 447
    .restart local v0    # "code":B
    .restart local v2    # "temp":Landroid/os/Bundle;
    :cond_4
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-eq v0, v3, :cond_5

    .line 448
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-ne v0, v3, :cond_8

    .line 449
    :cond_5
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 450
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    if-gtz v3, :cond_6

    .line 451
    iput v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 453
    :cond_6
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    div-int/lit8 v1, v3, 0x8

    .line 454
    .restart local v1    # "cur":I
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    if-eq v1, v3, :cond_7

    .line 455
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v1, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 457
    :cond_7
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 458
    invoke-virtual {v3}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 457
    check-cast v3, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;

    .line 458
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 459
    .end local v1    # "cur":I
    :cond_8
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-ne v0, v3, :cond_b

    .line 461
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    if-eqz v3, :cond_3

    .line 464
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    add-int/lit8 v3, v3, -0x4

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 465
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    if-gtz v3, :cond_9

    .line 466
    iput v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 468
    :cond_9
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    div-int/lit8 v1, v3, 0x8

    .line 469
    .restart local v1    # "cur":I
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    if-eq v1, v3, :cond_a

    .line 470
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v1, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 472
    :cond_a
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 473
    invoke-virtual {v3}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 472
    check-cast v3, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;

    .line 473
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 474
    .end local v1    # "cur":I
    :cond_b
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-ne v0, v3, :cond_e

    .line 476
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v3, v4, :cond_3

    .line 479
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    add-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 480
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    iget-object v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v3, v4, :cond_c

    .line 481
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 483
    :cond_c
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    div-int/lit8 v1, v3, 0x8

    .line 484
    .restart local v1    # "cur":I
    iget v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    if-eq v1, v3, :cond_d

    .line 485
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v1, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 487
    :cond_d
    iget-object v3, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    iget v4, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    .line 488
    invoke-virtual {v3}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 487
    check-cast v3, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;

    .line 488
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 489
    .end local v1    # "cur":I
    :cond_e
    sget-object v3, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    if-ne v0, v3, :cond_3

    .line 490
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->startApp()V

    goto/16 :goto_0
.end method

.method public initData()V
    .locals 3

    .prologue
    .line 178
    const/4 v1, 0x0

    iput v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    .line 179
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->loadApps()V

    .line 180
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->initDot()V

    .line 181
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->initViewPageData()V

    .line 182
    new-instance v0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 183
    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    .line 182
    invoke-direct {v0, p0, v1, v2}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 184
    .local v0, "appPagerAdapter":Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppPagerAdapter;
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 185
    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPageListData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 186
    return-void
.end method

.method public initEvent()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$2;-><init>(Lcom/touchus/benchilauncher/fragment/FragmentYingyong;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 217
    return-void
.end method

.method public initView()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mRootView:Landroid/view/View;

    .line 173
    const v1, 0x7f0b00f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 172
    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->dot_container:Landroid/widget/LinearLayout;

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mRootView:Landroid/view/View;

    const v1, 0x7f0b00f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 175
    return-void
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    const v0, 0x7f030029

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mRootView:Landroid/view/View;

    .line 78
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 79
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->initView()V

    .line 80
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->initData()V

    .line 81
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->initEvent()V

    .line 82
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->addAppReceiver()V

    .line 83
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->unAppReceiver()V

    .line 103
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 104
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    mul-int/lit8 v0, v0, 0x8

    add-int/2addr v0, p3

    iput v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->selectIndex:I

    .line 109
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppsGridAdapter;->notifyDataSetChanged()V

    .line 110
    invoke-direct {p0}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->startApp()V

    .line 111
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 9
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x0

    .line 116
    new-instance v1, Lcom/touchus/benchilauncher/views/UnInstallDialog;

    iget-object v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    const v7, 0x7f080006

    invoke-direct {v1, v6, v7}, Lcom/touchus/benchilauncher/views/UnInstallDialog;-><init>(Landroid/content/Context;I)V

    .line 117
    .local v1, "dialog":Lcom/touchus/benchilauncher/views/UnInstallDialog;
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 118
    .local v5, "w":Landroid/view/Window;
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 120
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    const/16 v6, 0x104

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 121
    invoke-virtual {v5, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 123
    iget-object v6, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mNeedShowApps:Ljava/util/List;

    iget v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->currentPage:I

    mul-int/lit8 v7, v7, 0x8

    add-int/2addr v7, p3

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 125
    .local v2, "info":Landroid/content/pm/ResolveInfo;
    iget-object v6, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v6, :cond_0

    .line 136
    :goto_0
    return v8

    .line 129
    :cond_0
    iget-object v6, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 131
    .local v4, "pkg":Ljava/lang/String;
    iget-object v6, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 132
    iget-object v7, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v7}, Lcom/touchus/benchilauncher/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 131
    invoke-virtual {v6, v7}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 132
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "cls":Ljava/lang/String;
    invoke-virtual {v1, v4, v0}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->setPkgName(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->show()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mAppHandler:Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 97
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onPause()V

    .line 98
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mContext:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;->mAppHandler:Lcom/touchus/benchilauncher/fragment/FragmentYingyong$AppHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 91
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onResume()V

    .line 92
    return-void
.end method
