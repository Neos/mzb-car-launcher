.class public final Lcom/touchus/benchilauncher/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final LoopRotarySwitchView:[I

.field public static final LoopRotarySwitchView_autoRotation:I = 0x1

.field public static final LoopRotarySwitchView_direction:I = 0x3

.field public static final LoopRotarySwitchView_orientation:I = 0x0

.field public static final LoopRotarySwitchView_r:I = 0x2

.field public static final MyClockStyleable:[I

.field public static final MyClockStyleable_center_clock:I = 0x4

.field public static final MyClockStyleable_clock:I = 0x0

.field public static final MyClockStyleable_hour:I = 0x1

.field public static final MyClockStyleable_minute:I = 0x2

.field public static final MyClockStyleable_second:I = 0x3

.field public static final MyMenuView:[I

.field public static final MyMenuView_icon:I = 0x1

.field public static final MyMenuView_iselect:I = 0x0

.field public static final MyMenuView_text:I = 0x2

.field public static final MySoundSet:[I

.field public static final MySoundSet_soundvalue:I = 0x0

.field public static final MyYibiaoStyleable:[I

.field public static final MyYibiaoStyleable_bgimage:I = 0x0

.field public static final MyYibiaoStyleable_centerimage:I = 0x1

.field public static final MyYibiaoStyleable_point:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1464
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/touchus/benchilauncher/R$styleable;->LoopRotarySwitchView:[I

    .line 1561
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/touchus/benchilauncher/R$styleable;->MyClockStyleable:[I

    .line 1629
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/touchus/benchilauncher/R$styleable;->MyMenuView:[I

    .line 1676
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 1677
    const v2, 0x7f01000c

    aput v2, v0, v1

    .line 1676
    sput-object v0, Lcom/touchus/benchilauncher/R$styleable;->MySoundSet:[I

    .line 1707
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/touchus/benchilauncher/R$styleable;->MyYibiaoStyleable:[I

    .line 1739
    return-void

    .line 1464
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
    .end array-data

    .line 1561
    :array_1
    .array-data 4
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
    .end array-data

    .line 1629
    :array_2
    .array-data 4
        0x7f01000d
        0x7f01000e
        0x7f01000f
    .end array-data

    .line 1707
    :array_3
    .array-data 4
        0x7f010009
        0x7f01000a
        0x7f01000b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
