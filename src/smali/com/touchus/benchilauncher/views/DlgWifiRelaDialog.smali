.class public Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;
.super Landroid/app/Dialog;
.source "DlgWifiRelaDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

.field public height:I

.field private joinBtn:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field public parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

.field private pwdEdit:Landroid/widget/EditText;

.field private pwdTextWatcher:Landroid/text/TextWatcher;

.field public showMessage:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 23
    const/16 v0, 0x19f

    iput v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->width:I

    .line 24
    const/16 v0, 0x96

    iput v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->height:I

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->showMessage:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->title:Ljava/lang/String;

    .line 131
    new-instance v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;-><init>(Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->pwdTextWatcher:Landroid/text/TextWatcher;

    .line 36
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeId"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 23
    const/16 v0, 0x19f

    iput v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->width:I

    .line 24
    const/16 v0, 0x96

    iput v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->height:I

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->showMessage:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->title:Ljava/lang/String;

    .line 131
    new-instance v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;-><init>(Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->pwdTextWatcher:Landroid/text/TextWatcher;

    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->joinBtn:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 83
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0b0063

    if-ne v4, v5, :cond_3

    .line 84
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v4

    if-ne v4, v6, :cond_1

    .line 85
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->disConnectWifi(Landroid/content/Context;)V

    .line 86
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->updateWifiList()V

    .line 127
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->dismiss()V

    .line 128
    :goto_1
    return-void

    .line 88
    :cond_1
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->getHadSaveWifiConfig(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 89
    .local v0, "config":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_2

    .line 90
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->connectToHadSaveConfigWifi(Landroid/content/Context;I)V

    .line 91
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->disConnectCurWifi()V

    .line 92
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->connectingToSpecifyWifi(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f070081

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    .end local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0b0083

    if-ne v4, v5, :cond_4

    .line 98
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->getHadSaveWifiConfig(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 99
    .restart local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 100
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->removeWifiConfig(Landroid/content/Context;I)V

    .line 101
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v4

    if-ne v4, v6, :cond_0

    .line 102
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->updateWifiList()V

    goto :goto_0

    .line 106
    .end local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0b0081

    if-ne v4, v5, :cond_0

    .line 107
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->pwdEdit:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "pwd":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 109
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f070087

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 112
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x8

    if-ge v4, v5, :cond_6

    .line 113
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f07008d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 117
    :cond_6
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->connectingToSpecifyWifi(Ljava/lang/String;)V

    .line 118
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getCapabilities()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->getCurrentWifiPwdType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "type":Ljava/lang/String;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2, v3}, Lcom/touchus/benchilauncher/utils/WifiTool;->CreateWifiInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 120
    .restart local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    invoke-static {v4, v0}, Lcom/touchus/benchilauncher/utils/WifiTool;->connectToSpecifyWifi(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v1

    .line 121
    .local v1, "flag":Z
    if-nez v1, :cond_0

    .line 122
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    .line 123
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v7, 0x7f070082

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v6}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 122
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 124
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->updateWifiList()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    .line 46
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 47
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v6}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/touchus/benchilauncher/utils/WifiTool;->getHadSaveWifiConfig(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    .line 48
    .local v1, "config":Landroid/net/wifi/WifiConfiguration;
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v5

    if-ne v5, v8, :cond_1

    if-nez v1, :cond_1

    .line 49
    const v5, 0x7f030018

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->setContentView(I)V

    .line 50
    const v5, 0x7f0b007f

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 51
    .local v2, "showInfo":Landroid/widget/TextView;
    const v5, 0x7f0b0081

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->joinBtn:Landroid/widget/Button;

    .line 52
    const v5, 0x7f0b0080

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->pwdEdit:Landroid/widget/EditText;

    .line 54
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->joinBtn:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->joinBtn:Landroid/widget/Button;

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 56
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->joinBtn:Landroid/widget/Button;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v7, 0x7f070089

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->showMessage:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 58
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f070088

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->showMessage:Ljava/lang/String;

    .line 60
    :cond_0
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->showMessage:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    .end local v2    # "showInfo":Landroid/widget/TextView;
    :goto_0
    const v5, 0x7f0b007e

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 76
    .local v4, "titleName":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    iget v6, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->width:I

    iget v7, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->height:I

    invoke-virtual {v5, v6, v7}, Landroid/view/Window;->setLayout(II)V

    .line 79
    return-void

    .line 62
    .end local v4    # "titleName":Landroid/widget/TextView;
    :cond_1
    const v5, 0x7f030019

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->setContentView(I)V

    .line 63
    const v5, 0x7f0b0083

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 64
    .local v0, "canceBtn":Landroid/widget/Button;
    const v5, 0x7f0b0063

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 65
    .local v3, "submitBtn":Landroid/widget/Button;
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v5

    if-ne v5, v7, :cond_3

    .line 68
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f07008b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f07008c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->height:I

    int-to-double v6, v5

    const-wide v8, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v6, v8

    double-to-int v5, v6

    iput v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->height:I

    goto :goto_0

    .line 69
    :cond_3
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v5

    if-ne v5, v8, :cond_2

    .line 70
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f07008a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
