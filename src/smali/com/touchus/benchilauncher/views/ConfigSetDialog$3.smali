.class Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;
.super Ljava/lang/Object;
.source "ConfigSetDialog.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/ConfigSetDialog;->initSetup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    .line 145
    packed-switch p2, :pswitch_data_0

    .line 155
    :goto_0
    sget-object v0, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 156
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$4(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    const-string v1, "naviExist"

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 157
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendStoreDataToMcu([B)V

    .line 158
    return-void

    .line 147
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    goto :goto_0

    .line 150
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    goto :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x7f0b0038
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
