.class public Lcom/touchus/benchilauncher/views/ReverseDialog;
.super Landroid/app/Dialog;
.source "ReverseDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;
    }
.end annotation


# instance fields
.field private addreverse360:Landroid/widget/RadioButton;

.field private context:Landroid/content/Context;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mHandler:Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

.field private mIDRIVERENUM:B

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private mute:Landroid/widget/RadioButton;

.field private newaddreverse:Landroid/widget/RadioButton;

.field private normal:Landroid/widget/RadioButton;

.field private originalreverse:Landroid/widget/RadioButton;

.field private reversePos:I

.field private reverseType:Landroid/widget/RadioGroup;

.field private selectPos:I

.field private type:I

.field private voiceSet:Landroid/widget/RadioGroup;

.field private voicesetPos:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 25
    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    .line 26
    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    .line 177
    new-instance v0, Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/ReverseDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    .line 195
    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 25
    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    .line 26
    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    .line 177
    new-instance v0, Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/ReverseDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    .line 195
    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    .line 50
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->context:Landroid/content/Context;

    .line 51
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/ReverseDialog;I)V
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/ReverseDialog;I)V
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/ReverseDialog;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/ReverseDialog;I)V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/ReverseDialog;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/ReverseDialog;)V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->press()V

    return-void
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/ReverseDialog;I)V
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/ReverseDialog;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    return v0
.end method

.method private initData()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 141
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->context:Landroid/content/Context;

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 142
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 143
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "reversingType"

    invoke-virtual {v0, v1, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    .line 144
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "voiceType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    .line 145
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    if-ne v0, v3, :cond_0

    .line 146
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 151
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    goto :goto_0
.end method

.method private initSetup()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 75
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->originalreverse:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->newaddreverse:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->addreverse360:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->normal:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mute:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 81
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/ReverseDialog$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ReverseDialog$1;-><init>(Lcom/touchus/benchilauncher/views/ReverseDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 104
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/ReverseDialog$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ReverseDialog$2;-><init>(Lcom/touchus/benchilauncher/views/ReverseDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 123
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->seclectTrue(I)V

    .line 125
    return-void
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 128
    const v0, 0x7f0b0042

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    .line 129
    const v0, 0x7f0b0046

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    .line 131
    const v0, 0x7f0b0043

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->originalreverse:Landroid/widget/RadioButton;

    .line 132
    const v0, 0x7f0b0044

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->newaddreverse:Landroid/widget/RadioButton;

    .line 133
    const v0, 0x7f0b0045

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->addreverse360:Landroid/widget/RadioButton;

    .line 135
    const v0, 0x7f0b0048

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->normal:Landroid/widget/RadioButton;

    .line 136
    const v0, 0x7f0b0047

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mute:Landroid/widget/RadioButton;

    .line 138
    return-void
.end method

.method private press()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 245
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v0, v4, :cond_1

    .line 246
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "voiceType"

    iget v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 247
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    if-ne v0, v3, :cond_0

    .line 248
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 249
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/backaudio/android/driver/Mainboard;->sendReverseMediaSetToMcu(I)V

    .line 254
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 260
    :goto_1
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 252
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendReverseMediaSetToMcu(I)V

    goto :goto_0

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "reversingType"

    iget v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 257
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendReverseSetToMcu(I)V

    .line 258
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private seclectTrue(I)V
    .locals 5
    .param p1, "seclectCuttun"    # I

    .prologue
    const/4 v4, 0x0

    .line 264
    iget v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 265
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    .line 266
    .local v1, "posGroup":Landroid/widget/RadioGroup;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    iget v3, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    .line 271
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    if-lt v0, v2, :cond_1

    .line 278
    return-void

    .line 268
    .end local v0    # "i":I
    .end local v1    # "posGroup":Landroid/widget/RadioGroup;
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    .line 269
    .restart local v1    # "posGroup":Landroid/widget/RadioGroup;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    iget v3, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 272
    .restart local v0    # "i":I
    :cond_1
    if-ne p1, v0, :cond_2

    .line 273
    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    .line 271
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 275
    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2
.end method


# virtual methods
.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 198
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_2

    .line 199
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 200
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    .line 201
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_0

    .line 202
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_5

    .line 203
    :cond_0
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v1, v3, :cond_3

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_3

    .line 204
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    .line 205
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    .line 216
    :cond_1
    :goto_0
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/ReverseDialog;->seclectTrue(I)V

    .line 242
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_1
    return-void

    .line 206
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_3
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v1, v3, :cond_4

    .line 207
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_4

    .line 208
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    .line 209
    const/4 v1, 0x0

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    .line 210
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    goto :goto_0

    .line 211
    :cond_4
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v1, v4, :cond_1

    .line 212
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voiceSet:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 213
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    .line 214
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    goto :goto_0

    .line 217
    :cond_5
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 218
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_6

    .line 219
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_a

    .line 220
    :cond_6
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v1, v4, :cond_8

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    if-lez v1, :cond_8

    .line 221
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    .line 222
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    .line 231
    :cond_7
    :goto_2
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/ReverseDialog;->seclectTrue(I)V

    goto :goto_1

    .line 223
    :cond_8
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v1, v4, :cond_9

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    if-nez v1, :cond_9

    .line 224
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    .line 225
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reverseType:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    .line 226
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    goto :goto_2

    .line 227
    :cond_9
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    if-ne v1, v3, :cond_7

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    if-lez v1, :cond_7

    .line 228
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    .line 229
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->selectPos:I

    goto :goto_2

    .line 233
    :cond_a
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_b

    .line 234
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->press()V

    goto/16 :goto_1

    .line 235
    :cond_b
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_c

    .line 236
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_c

    .line 237
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 238
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    .line 237
    if-ne v1, v2, :cond_2

    .line 239
    :cond_c
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->dismiss()V

    goto/16 :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 155
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 171
    :goto_0
    :pswitch_0
    return-void

    .line 159
    :pswitch_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    .line 160
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->reversePos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->seclectTrue(I)V

    goto :goto_0

    .line 164
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->type:I

    .line 165
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->voicesetPos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->seclectTrue(I)V

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x7f0b0043
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->setContentView(I)V

    .line 57
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 58
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->initView()V

    .line 59
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->initData()V

    .line 60
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->initSetup()V

    .line 61
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 65
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 66
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 71
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 72
    return-void
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 175
    return-void
.end method
