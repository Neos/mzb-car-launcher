.class public Lcom/touchus/benchilauncher/views/UpdateDialog;
.super Landroid/app/Dialog;
.source "UpdateDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;
    }
.end annotation


# static fields
.field public static final APP_UPDATE:I = 0x4

.field public static final CANBOX_UPDATE:I = 0x3

.field public static final MCU_UPDATE:I = 0x2

.field public static final SN_UPDATE:I = 0x5

.field public static final SYSTEM_UPDATE:I = 0x1


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private cancelBtn:Landroid/widget/Button;

.field private context:Landroid/content/Context;

.field private curInfoTview:Landroid/widget/TextView;

.field private curTypeTview:Landroid/widget/TextView;

.field public currentType:I

.field private currentUpdatePath:Ljava/lang/String;

.field private iFinishUpdate:Z

.field private iInCancelState:Z

.field public mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

.field private onlineDownloadReceiver:Landroid/content/BroadcastReceiver;

.field private submitBtn:Landroid/widget/Button;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 68
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 51
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iInCancelState:Z

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iFinishUpdate:Z

    .line 63
    iput v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    .line 64
    const-string v0, "http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh"

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->url:Ljava/lang/String;

    .line 193
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$1;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->onlineDownloadReceiver:Landroid/content/BroadcastReceiver;

    .line 416
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    .line 69
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    const/4 v1, 0x1

    .line 73
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 51
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iInCancelState:Z

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iFinishUpdate:Z

    .line 63
    iput v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    .line 64
    const-string v0, "http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh"

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->url:Ljava/lang/String;

    .line 193
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$1;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->onlineDownloadReceiver:Landroid/content/BroadcastReceiver;

    .line 416
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    .line 74
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    .line 75
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/UpdateDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentUpdatePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/UpdateDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/UpdateDialog;)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->androidUpdate()V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/UpdateDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 308
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/UpdateDialog;->scanUsbAndSdcard(Ljava/lang/String;)V

    return-void
.end method

.method private addOnlineDownloadReceiver()V
    .locals 3

    .prologue
    .line 189
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 190
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->onlineDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 192
    return-void
.end method

.method private androidUpdate()V
    .locals 2

    .prologue
    .line 380
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog$9;

    const-string v1, "Reboot"

    invoke-direct {v0, p0, v1}, Lcom/touchus/benchilauncher/views/UpdateDialog$9;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;Ljava/lang/String;)V

    .line 391
    .local v0, "thr":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 392
    return-void
.end method

.method private checkUpdateFile()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 153
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 154
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startSystemCheckThread()V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 156
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startCanboxCheckThread()V

    goto :goto_0

    .line 157
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 158
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startMcuCheckThread()V

    goto :goto_0

    .line 159
    :cond_3
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 160
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startAPPCheckThread()V

    goto :goto_0

    .line 161
    :cond_4
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startSNCheckThread()V

    goto :goto_0
.end method

.method private deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 395
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$10;

    invoke-direct {v1, p0, p1}, Lcom/touchus/benchilauncher/views/UpdateDialog$10;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 413
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 414
    return-void
.end method

.method private getFile()Z
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/io/File;

    const-string v1, "update"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 206
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    goto :goto_0
.end method

.method private getOnlineConfig()V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v7, 0x0

    .line 168
    invoke-virtual {p0, v7}, Lcom/touchus/benchilauncher/views/UpdateDialog;->setCanceledOnTouchOutside(Z)V

    .line 169
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 171
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 172
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v6, 0x7f07006e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v7, v4, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    .line 175
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->addOnlineDownloadReceiver()V

    .line 177
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const-string v5, "download"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    .line 179
    .local v2, "downloadManager":Landroid/app/DownloadManager;
    new-instance v3, Landroid/app/DownloadManager$Request;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->url:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 181
    .local v3, "request":Landroid/app/DownloadManager$Request;
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->getFile()Z

    .line 183
    const-string v4, "update"

    const-string v5, "update_config.sh"

    invoke-virtual {v3, v4, v5}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 185
    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    .line 186
    .local v0, "downloadId":J
    return-void
.end method

.method private scanUsbAndSdcard(Ljava/lang/String;)V
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 309
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v3, 0x7f07000e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, p1}, Lcom/touchus/benchilauncher/views/UpdateDialog;->scanUsbDiskUpdateFile(Ljava/io/File;Ljava/lang/String;)Z

    move-result v0

    .line 310
    .local v0, "hasFile":Z
    if-nez v0, :cond_0

    .line 311
    new-instance v1, Ljava/io/File;

    const-string v2, "mnt/usbotg"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, p1}, Lcom/touchus/benchilauncher/views/UpdateDialog;->scanUsbDiskUpdateFile(Ljava/io/File;Ljava/lang/String;)Z

    .line 318
    :cond_0
    return-void
.end method

.method private scanUsbDiskUpdateFile(Ljava/io/File;Ljava/lang/String;)Z
    .locals 9
    .param p1, "folder"    # Ljava/io/File;
    .param p2, "flag"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 327
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 328
    .local v3, "filenames":[Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 330
    array-length v7, v3

    move v6, v5

    :goto_0
    if-lt v6, v7, :cond_1

    .line 347
    :cond_0
    :goto_1
    return v5

    .line 330
    :cond_1
    aget-object v4, v3, v6

    .line 332
    .local v4, "name":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 334
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 336
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 337
    .local v2, "filePath":Ljava/lang/String;
    invoke-virtual {v2, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 338
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentUpdatePath:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    const/4 v5, 0x1

    goto :goto_1

    .line 342
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 330
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method private startAPPCheckThread()V
    .locals 2

    .prologue
    .line 298
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$8;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$8;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 305
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 306
    return-void
.end method

.method private startCanboxCheckThread()V
    .locals 2

    .prologue
    .line 288
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$7;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$7;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 295
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 296
    return-void
.end method

.method private startMcuCheckThread()V
    .locals 2

    .prologue
    .line 277
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$6;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 284
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 285
    return-void
.end method

.method private startSNCheckThread()V
    .locals 2

    .prologue
    .line 256
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$4;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$4;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 262
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 263
    return-void
.end method

.method private startSystemCheckThread()V
    .locals 2

    .prologue
    .line 266
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$5;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 273
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 274
    return-void
.end method

.method private startUpdate()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 209
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentUpdatePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/UpdateDialog;->setCanceledOnTouchOutside(Z)V

    .line 213
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v2, 0x7f070063

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    .line 218
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 219
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$2;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 230
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 231
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 232
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentUpdatePath:Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v2, 0x7f070007

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/UpdateDialog;->copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    .line 233
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v1, 0x7f070008

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->deleteFile(Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->enterIntoUpgradeCanboxCheck()V

    goto :goto_0

    .line 235
    :cond_3
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 236
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentUpdatePath:Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v2, 0x7f070009

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/UpdateDialog;->copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    .line 237
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v1, 0x7f07000a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->deleteFile(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->enterIntoUpgradeMcuCheck()V

    goto :goto_0

    .line 239
    :cond_4
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 240
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/UpdateDialog$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/UpdateDialog$3;-><init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 251
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method


# virtual methods
.method public copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 13
    .param p1, "oldPath"    # Ljava/lang/String;
    .param p2, "newPath"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 352
    const/4 v2, 0x0

    .line 353
    .local v2, "byteread":I
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 354
    .local v6, "oldfile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 355
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 356
    .local v5, "inStream":Ljava/io/InputStream;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 357
    .local v4, "fs":Ljava/io/FileOutputStream;
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 358
    .local v0, "begin":I
    const/16 v8, 0x2000

    new-array v1, v8, [B

    .line 359
    .local v1, "buffer":[B
    :cond_0
    :goto_0
    invoke-virtual {v5, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v8, -0x1

    if-ne v2, v8, :cond_1

    .line 367
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 368
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 369
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 376
    .end local v0    # "begin":I
    .end local v1    # "buffer":[B
    .end local v4    # "fs":Ljava/io/FileOutputStream;
    .end local v5    # "inStream":Ljava/io/InputStream;
    .end local v6    # "oldfile":Ljava/io/File;
    :goto_1
    return-object v8

    .line 361
    .restart local v0    # "begin":I
    .restart local v1    # "buffer":[B
    .restart local v4    # "fs":Ljava/io/FileOutputStream;
    .restart local v5    # "inStream":Ljava/io/InputStream;
    .restart local v6    # "oldfile":Ljava/io/File;
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {v4, v1, v8, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 362
    invoke-virtual {v5}, Ljava/io/InputStream;->available()I

    move-result v8

    int-to-double v8, v8

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v8, v10

    int-to-double v10, v0

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v8, v10

    double-to-int v7, v8

    .line 363
    .local v7, "per":I
    const/16 v8, 0x64

    if-gt v7, v8, :cond_0

    if-ltz v7, :cond_0

    .line 364
    iget-object v8, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    const/16 v9, 0x458

    rsub-int/lit8 v10, v7, 0x64

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 373
    .end local v0    # "begin":I
    .end local v1    # "buffer":[B
    .end local v4    # "fs":Ljava/io/FileOutputStream;
    .end local v5    # "inStream":Ljava/io/InputStream;
    .end local v6    # "oldfile":Ljava/io/File;
    .end local v7    # "per":I
    :catch_0
    move-exception v3

    .line 374
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 376
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_1

    .line 371
    .restart local v6    # "oldfile":Ljava/io/File;
    :cond_2
    :try_start_1
    const-string v8, ""

    const-string v9, "file not  exist..........."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    .line 129
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    .line 130
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 131
    return-void
.end method

.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v6, 0x7f070062

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 435
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x1771

    if-ne v4, v5, :cond_9

    .line 436
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 437
    .local v3, "temp":Landroid/os/Bundle;
    const-string v4, "idriver_enum"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    .line 438
    .local v1, "idriverCode":B
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v1, v4, :cond_0

    .line 439
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_2

    .line 440
    :cond_0
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iInCancelState:Z

    .line 513
    .end local v1    # "idriverCode":B
    .end local v3    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 441
    .restart local v1    # "idriverCode":B
    .restart local v3    # "temp":Landroid/os/Bundle;
    :cond_2
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v1, v4, :cond_3

    .line 442
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_4

    .line 443
    :cond_3
    iput-boolean v8, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iInCancelState:Z

    goto :goto_0

    .line 444
    :cond_4
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_7

    .line 445
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iInCancelState:Z

    if-eqz v4, :cond_5

    .line 446
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->dismiss()V

    goto :goto_0

    .line 448
    :cond_5
    iget v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_6

    .line 449
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->getOnlineConfig()V

    goto :goto_0

    .line 451
    :cond_6
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startUpdate()V

    goto :goto_0

    .line 455
    :cond_7
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v1, v4, :cond_8

    .line 456
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v1, v4, :cond_8

    .line 457
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v1, v4, :cond_1

    .line 458
    :cond_8
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->dismiss()V

    goto :goto_0

    .line 460
    .end local v1    # "idriverCode":B
    .end local v3    # "temp":Landroid/os/Bundle;
    :cond_9
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x457

    if-ne v4, v5, :cond_b

    .line 461
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentUpdatePath:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 462
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v6, 0x7f070061

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 464
    :cond_a
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 467
    :cond_b
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x464

    if-ne v4, v5, :cond_c

    .line 468
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    .line 469
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 468
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 470
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 471
    :cond_c
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x468

    if-ne v4, v5, :cond_d

    .line 472
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v6, 0x7f070060

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 473
    invoke-virtual {p0, v7}, Lcom/touchus/benchilauncher/views/UpdateDialog;->setCanceledOnTouchOutside(Z)V

    .line 474
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 475
    :cond_d
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x469

    if-ne v4, v5, :cond_e

    .line 476
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v6, 0x7f07005f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    invoke-virtual {p0, v7}, Lcom/touchus/benchilauncher/views/UpdateDialog;->setCanceledOnTouchOutside(Z)V

    .line 478
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 479
    :cond_e
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x46b

    if-eq v4, v5, :cond_f

    .line 480
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x461

    if-ne v4, v5, :cond_12

    .line 481
    :cond_f
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v7, v4, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    .line 482
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iFinishUpdate:Z

    .line 483
    invoke-virtual {p0, v7}, Lcom/touchus/benchilauncher/views/UpdateDialog;->setCanceledOnTouchOutside(Z)V

    .line 484
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 485
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 486
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 487
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    const v5, 0x7f07005b

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 488
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 489
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v6, 0x7f070065

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    iget v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_11

    .line 491
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v4

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard;->getMCUVersionNumber()V

    .line 492
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v5, 0x7f070009

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/touchus/benchilauncher/views/UpdateDialog;->deleteFile(Ljava/lang/String;)V

    .line 499
    :cond_10
    :goto_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->dismiss()V

    goto/16 :goto_0

    .line 493
    :cond_11
    iget v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_10

    .line 495
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v4

    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->CANBOX_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    invoke-virtual {v4, v5}, Lcom/backaudio/android/driver/Mainboard;->getModeInfo(Lcom/backaudio/android/driver/Mainboard$EModeInfo;)V

    .line 497
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v5, 0x7f070007

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/touchus/benchilauncher/views/UpdateDialog;->deleteFile(Ljava/lang/String;)V

    goto :goto_1

    .line 500
    :cond_12
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x458

    if-ne v4, v5, :cond_13

    .line 502
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v5, 0x7f070063

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 503
    .local v3, "temp":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 504
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 506
    .end local v3    # "temp":Ljava/lang/String;
    :cond_13
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x459

    if-ne v4, v5, :cond_14

    .line 507
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 508
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "upgradePer"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 509
    .local v2, "per":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    const v6, 0x7f070064

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 510
    .restart local v3    # "temp":Ljava/lang/String;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 511
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "per":Ljava/lang/String;
    .end local v3    # "temp":Ljava/lang/String;
    :cond_14
    iget v4, p1, Landroid/os/Message;->what:I

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b004d

    if-ne v0, v1, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->dismiss()V

    .line 148
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->iFinishUpdate:Z

    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->dismiss()V

    goto :goto_0

    .line 141
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 142
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->getOnlineConfig()V

    goto :goto_0

    .line 144
    :cond_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->startUpdate()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 79
    const v2, 0x7f030016

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;->setContentView(I)V

    .line 80
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 81
    const v2, 0x7f0b007a

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curInfoTview:Landroid/widget/TextView;

    .line 82
    const v2, 0x7f0b0079

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curTypeTview:Landroid/widget/TextView;

    .line 83
    const v2, 0x7f0b004c

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    .line 84
    const v2, 0x7f0b004d

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    .line 87
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 91
    .local v0, "dialogWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 93
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x190

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 94
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 95
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 97
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 98
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 99
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->checkUpdateFile()V

    .line 105
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 106
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curTypeTview:Landroid/widget/TextView;

    const-string v1, "SYSTEM UPDATE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 117
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 118
    return-void

    .line 107
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 108
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curTypeTview:Landroid/widget/TextView;

    const-string v1, "CANBOX UPDATE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 109
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 110
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curTypeTview:Landroid/widget/TextView;

    const-string v1, "MCU UPDATE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 111
    :cond_3
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curTypeTview:Landroid/widget/TextView;

    const-string v1, "APP UPDATE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 113
    :cond_4
    iget v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->curTypeTview:Landroid/widget/TextView;

    const-string v1, "ONLINE CONFIG UPDATE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog;->mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 123
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 124
    return-void
.end method
