.class Lcom/touchus/benchilauncher/views/LanguageDialog$1;
.super Ljava/lang/Object;
.source "LanguageDialog.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/LanguageDialog;->initSetup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/LanguageDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    .line 70
    packed-switch p2, :pswitch_data_0

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$1(Lcom/touchus/benchilauncher/views/LanguageDialog;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$2(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$3(Lcom/touchus/benchilauncher/views/LanguageDialog;)V

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->dismiss()V

    .line 101
    return-void

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 75
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 78
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 81
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 84
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 87
    :pswitch_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 90
    :pswitch_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 93
    :pswitch_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog$1;->this$0:Lcom/touchus/benchilauncher/views/LanguageDialog;

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b004f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
