.class public Lcom/touchus/benchilauncher/views/LeftStateSlide;
.super Lcom/touchus/benchilauncher/base/AbstraSlide;
.source "LeftStateSlide.java"


# static fields
.field public static mChildWith:I

.field public static mPage:I


# instance fields
.field public iHandleTouchEvent:Z

.field public mDownX:F

.field public mDownY:F

.field private mScrolling:Z

.field public mSpace:I

.field public mStartX:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/16 v0, 0x1db

    sput v0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/touchus/benchilauncher/base/AbstraSlide;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/16 v0, 0x14

    iput v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->iHandleTouchEvent:Z

    .line 23
    return-void
.end method


# virtual methods
.method public changeToCurrentView()V
    .locals 2

    .prologue
    .line 120
    sget v0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    sget v1, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    mul-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->scrollTo(II)V

    .line 121
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mScrolling:Z

    return v0

    .line 100
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mStartX:F

    .line 101
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mDownX:F

    .line 102
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mScrolling:Z

    goto :goto_0

    .line 105
    :pswitch_1
    iget v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mStartX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 106
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 105
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mScrolling:Z

    goto :goto_0

    .line 109
    :cond_0
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mScrolling:Z

    goto :goto_0

    .line 113
    :pswitch_2
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mScrolling:Z

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v11, -0x14

    const/16 v10, 0x12c

    const/16 v9, 0x64

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 29
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->iHandleTouchEvent:Z

    if-nez v4, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v8

    .line 32
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 34
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mStartX:F

    .line 35
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mDownX:F

    .line 36
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mDownY:F

    goto :goto_0

    .line 39
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 40
    .local v3, "moveX":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mDownX:F

    sub-float/2addr v4, v3

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 41
    .local v0, "diffX":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->getScrollX()I

    move-result v4

    add-int v2, v4, v0

    .line 42
    .local v2, "finalX":I
    const-string v4, "ACTION_MOVE"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ACTION_ moveX = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",diffX = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",finalX = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, La_vcard/android/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    if-ge v2, v11, :cond_2

    .line 44
    invoke-virtual {p0, v11, v7}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->scrollTo(II)V

    goto :goto_0

    .line 47
    :cond_2
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    sget v5, Lcom/touchus/benchilauncher/LauncherApplication;->pageCount:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x14

    if-le v2, v4, :cond_3

    .line 48
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    sget v5, Lcom/touchus/benchilauncher/LauncherApplication;->pageCount:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x14

    invoke-virtual {p0, v4, v7}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->scrollTo(II)V

    goto :goto_0

    .line 52
    :cond_3
    invoke-virtual {p0, v0, v7}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->scrollBy(II)V

    .line 53
    iput v3, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mDownX:F

    goto :goto_0

    .line 56
    .end local v0    # "diffX":I
    .end local v2    # "finalX":I
    .end local v3    # "moveX":F
    :pswitch_2
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mDownX:F

    iget v5, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mStartX:F

    sub-float v1, v4, v5

    .line 57
    .local v1, "distance":F
    const-string v4, "ACTION_UP"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ACTION_ distance = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",mPage = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, La_vcard/android/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    packed-switch v4, :pswitch_data_1

    goto/16 :goto_0

    .line 60
    :pswitch_3
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-lez v4, :cond_4

    .line 61
    invoke-virtual {p0, v7, v9}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    goto/16 :goto_0

    .line 62
    :cond_4
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 63
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    mul-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4, v10}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    .line 65
    sput v8, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    goto/16 :goto_0

    .line 69
    :pswitch_4
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-lez v4, :cond_5

    .line 70
    invoke-virtual {p0, v7, v10}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    .line 71
    sput v7, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    goto/16 :goto_0

    .line 72
    :cond_5
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_6

    .line 73
    const/4 v4, 0x2

    sput v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    .line 74
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    mul-int/lit8 v4, v4, 0x2

    invoke-virtual {p0, v4, v9}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    goto/16 :goto_0

    .line 75
    :cond_6
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 76
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    mul-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4, v9}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    goto/16 :goto_0

    .line 80
    :pswitch_5
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-lez v4, :cond_7

    .line 81
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    mul-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4, v10}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    .line 82
    sput v8, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mPage:I

    goto/16 :goto_0

    .line 83
    :cond_7
    iget v4, p0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mSpace:I

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 84
    sget v4, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    mul-int/lit8 v4, v4, 0x2

    invoke-virtual {p0, v4, v9}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->smoothScrollXTo(II)V

    goto/16 :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 58
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
