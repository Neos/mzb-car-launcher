.class public Lcom/touchus/benchilauncher/views/FactorySetDialog;
.super Landroid/app/Dialog;
.source "FactorySetDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;
    }
.end annotation


# instance fields
.field public height:I

.field private joinBtn:Landroid/widget/Button;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mContext:Landroid/content/Context;

.field public mHandler:Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

.field private pwdEdit:Landroid/widget/EditText;

.field public showMessage:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 30
    const/16 v0, 0x19f

    iput v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->width:I

    .line 31
    const/16 v0, 0x96

    iput v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->height:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->showMessage:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->title:Ljava/lang/String;

    .line 106
    new-instance v0, Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/FactorySetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mHandler:Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeId"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 30
    const/16 v0, 0x19f

    iput v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->width:I

    .line 31
    const/16 v0, 0x96

    iput v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->height:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->showMessage:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->title:Ljava/lang/String;

    .line 106
    new-instance v0, Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/FactorySetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mHandler:Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

    .line 46
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 125
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_0

    .line 126
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 127
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 128
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_1

    .line 129
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->joinBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->performClick()Z

    .line 136
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 130
    .restart local v0    # "idriverCode":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_1
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_2

    .line 131
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_2

    .line 132
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_0

    .line 133
    :cond_2
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->dismiss()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0b0081

    if-ne v3, v4, :cond_2

    .line 73
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->pwdEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, "pwd":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f070087

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 94
    .end local v2    # "pwd":Ljava/lang/String;
    :goto_0
    return-void

    .line 78
    .restart local v2    # "pwd":Ljava/lang/String;
    :cond_0
    sget-object v3, Lcom/touchus/benchilauncher/ProjectConfig;->FactoryPwd:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 79
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f070090

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :cond_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 83
    .local v1, "pkgManger":Landroid/content/pm/PackageManager;
    const-string v3, "com.touchus.factorytest"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 84
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 85
    const/high16 v3, 0x10400000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 87
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->KEY:Ljava/lang/String;

    sget-object v4, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v4}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 88
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->SIZE_KEY:Ljava/lang/String;

    sget-object v4, Lcom/touchus/publicutils/sysconst/BenzModel;->benzSize:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    invoke-virtual {v4}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->getCode()B

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 89
    sget-object v3, Lcom/touchus/publicutils/sysconst/PubSysConst;->KEY_BREAKPOS:Ljava/lang/String;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->breakpos:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 90
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 93
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pkgManger":Landroid/content/pm/PackageManager;
    .end local v2    # "pwd":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->dismiss()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v2, 0x7f030018

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->setContentView(I)V

    .line 53
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 54
    const v2, 0x7f0b007f

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 55
    .local v0, "showInfo":Landroid/widget/TextView;
    const v2, 0x7f0b0081

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->joinBtn:Landroid/widget/Button;

    .line 56
    const v2, 0x7f0b0080

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->pwdEdit:Landroid/widget/EditText;

    .line 57
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->joinBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->joinBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 59
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->joinBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f07002d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->showMessage:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f07008f

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->showMessage:Ljava/lang/String;

    .line 63
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->showMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const v2, 0x7f0b007e

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 65
    .local v1, "titleName":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f07008e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget v3, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->width:I

    iget v4, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->height:I

    invoke-virtual {v2, v3, v4}, Landroid/view/Window;->setLayout(II)V

    .line 68
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mHandler:Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 99
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 100
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FactorySetDialog;->mHandler:Lcom/touchus/benchilauncher/views/FactorySetDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 104
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 105
    return-void
.end method
