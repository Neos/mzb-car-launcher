.class public Lcom/touchus/benchilauncher/views/SoundSetView;
.super Landroid/widget/RelativeLayout;
.source "SoundSetView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static MIXPRO:I

.field public static SOUND:I


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private baseType:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private mSoundSeekBar:Landroid/widget/SeekBar;

.field private mSoundTv:Landroid/widget/TextView;

.field private max:I

.field private num:I

.field private onSoundClickListener:Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

.field private sound_add:Landroid/widget/ImageView;

.field private sound_jianjian:Landroid/widget/ImageView;

.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput v0, Lcom/touchus/benchilauncher/views/SoundSetView;->SOUND:I

    .line 29
    const/4 v0, 0x1

    sput v0, Lcom/touchus/benchilauncher/views/SoundSetView;->MIXPRO:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/SoundSetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v1, 0xa

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->max:I

    .line 26
    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    .line 27
    const/4 v1, 0x0

    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->type:I

    .line 82
    const-string v1, "\u97f3\u91cf"

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->baseType:Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->context:Landroid/content/Context;

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 39
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 38
    check-cast v0, Landroid/view/LayoutInflater;

    .line 40
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030040

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 41
    const v1, 0x7f0b006b

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundTv:Landroid/widget/TextView;

    .line 42
    const v1, 0x7f0b006c

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->sound_jianjian:Landroid/widget/ImageView;

    .line 43
    const v1, 0x7f0b006d

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundSeekBar:Landroid/widget/SeekBar;

    .line 44
    const v1, 0x7f0b006e

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->sound_add:Landroid/widget/ImageView;

    .line 46
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->sound_jianjian:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->sound_add:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->max:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 50
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundSeekBar:Landroid/widget/SeekBar;

    .line 51
    new-instance v2, Lcom/touchus/benchilauncher/views/SoundSetView$1;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/SoundSetView$1;-><init>(Lcom/touchus/benchilauncher/views/SoundSetView;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 80
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/SoundSetView;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/SoundSetView;I)V
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/SoundSetView;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->type:I

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/SoundSetView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundTv:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/SoundSetView;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    return v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/SoundSetView;)Lcom/touchus/benchilauncher/inface/OnSoundClickListener;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->onSoundClickListener:Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

    return-object v0
.end method


# virtual methods
.method public addSoundValue()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->sound_add:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 119
    return-void
.end method

.method public decSoundValue()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->sound_jianjian:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 123
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 97
    :cond_0
    :goto_0
    :pswitch_0
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->type:I

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSoundValue(II)V

    .line 98
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->onSoundClickListener:Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    invoke-interface {v0, v1}, Lcom/touchus/benchilauncher/inface/OnSoundClickListener;->click(I)V

    .line 99
    return-void

    .line 87
    :pswitch_1
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 88
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    goto :goto_0

    .line 92
    :pswitch_2
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->max:I

    if-ge v0, v1, :cond_0

    .line 93
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b006c
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setOnSoundClickListener(Lcom/touchus/benchilauncher/inface/OnSoundClickListener;)V
    .locals 0
    .param p1, "onSoundClickListener"    # Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->onSoundClickListener:Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

    .line 130
    return-void
.end method

.method public setSoundValue(II)V
    .locals 3
    .param p1, "soundValue"    # I
    .param p2, "type"    # I

    .prologue
    .line 102
    iput p2, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->type:I

    .line 103
    iput p1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    .line 104
    sget v0, Lcom/touchus/benchilauncher/views/SoundSetView;->SOUND:I

    if-ne p2, v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundTv:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 111
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->mSoundTv:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->num:I

    mul-int/lit8 v2, v2, 0xa

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSpeakText(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundSetView;->baseType:Ljava/lang/String;

    .line 115
    return-void
.end method
