.class public Lcom/touchus/benchilauncher/views/SettingDialog$Builder;
.super Ljava/lang/Object;
.source "SettingDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/SettingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;
    }
.end annotation


# instance fields
.field private benzconfig:Landroid/widget/TextView;

.field private clickCount:I

.field private context:Landroid/content/Context;

.field private currentSelectIndex:I

.field private mAdapter:Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mDialog:Lcom/touchus/benchilauncher/views/SettingDialog;

.field private mLauncher:Lcom/touchus/benchilauncher/Launcher;

.field public mSettingHandler:Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

.field private mlistView:Landroid/widget/ListView;

.field resetClickCountThread:Ljava/lang/Thread;

.field private strings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 60
    iput v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->clickCount:I

    .line 139
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$1;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->resetClickCountThread:Ljava/lang/Thread;

    .line 146
    new-instance v0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

    .line 63
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    .line 65
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;I)V
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->clickCount:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;I)V
    .locals 0

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->clickItem(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->clickCount:I

    return v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artConfig()V

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method private artAppset()V
    .locals 3

    .prologue
    .line 384
    new-instance v0, Lcom/touchus/benchilauncher/views/AppSettingDialog;

    .line 385
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v2, 0x7f080006

    .line 384
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/AppSettingDialog;-><init>(Landroid/content/Context;I)V

    .line 386
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/AppSettingDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 387
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 388
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->show()V

    .line 389
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 390
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$11;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$11;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 397
    return-void
.end method

.method private artConfig()V
    .locals 3

    .prologue
    .line 320
    new-instance v0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    .line 321
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v2, 0x7f080006

    .line 320
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;-><init>(Landroid/content/Context;I)V

    .line 322
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/ConfigSetDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 323
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 324
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->show()V

    .line 325
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 326
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$7;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$7;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 333
    return-void
.end method

.method private artDaoche()V
    .locals 3

    .prologue
    .line 336
    new-instance v0, Lcom/touchus/benchilauncher/views/ReverseDialog;

    .line 337
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v2, 0x7f080006

    .line 336
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/ReverseDialog;-><init>(Landroid/content/Context;I)V

    .line 338
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/ReverseDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 339
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 340
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/ReverseDialog;->show()V

    .line 341
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 342
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$8;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$8;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/ReverseDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 349
    return-void
.end method

.method private artLanguage()V
    .locals 3

    .prologue
    .line 288
    new-instance v0, Lcom/touchus/benchilauncher/views/LanguageDialog;

    .line 289
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v2, 0x7f080006

    .line 288
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/LanguageDialog;-><init>(Landroid/content/Context;I)V

    .line 290
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/LanguageDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 291
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 292
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->show()V

    .line 293
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 294
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$5;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 302
    return-void
.end method

.method private artNetwork()V
    .locals 3

    .prologue
    .line 368
    new-instance v0, Lcom/touchus/benchilauncher/views/NetworkDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    .line 369
    const v2, 0x7f080006

    .line 368
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/NetworkDialog;-><init>(Landroid/content/Context;I)V

    .line 370
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/NetworkDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 371
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 372
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->show()V

    .line 373
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 374
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$10;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$10;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 381
    return-void
.end method

.method private artSound()V
    .locals 4

    .prologue
    .line 271
    new-instance v1, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 272
    .local v1, "builder":Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->create()Lcom/touchus/benchilauncher/views/SoundSetDialog;

    move-result-object v0

    .line 273
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/SoundSetDialog;
    const/16 v2, 0x12c

    const/16 v3, -0x14

    invoke-static {v0, v2, v3}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 274
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/SoundSetDialog;->show()V

    .line 275
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v2, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 276
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 277
    new-instance v2, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$4;

    invoke-direct {v2, p0, v1}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$4;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/views/SoundSetDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 285
    return-void
.end method

.method private artSystem()V
    .locals 3

    .prologue
    .line 305
    new-instance v0, Lcom/touchus/benchilauncher/views/SystemSetDialog;

    .line 306
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v2, 0x7f080006

    .line 305
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/SystemSetDialog;-><init>(Landroid/content/Context;I)V

    .line 307
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/SystemSetDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 308
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 309
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->show()V

    .line 310
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 311
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$6;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 318
    return-void
.end method

.method private artVersion()V
    .locals 3

    .prologue
    .line 352
    new-instance v0, Lcom/touchus/benchilauncher/views/XitongDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    .line 353
    const v2, 0x7f080006

    .line 352
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/XitongDialog;-><init>(Landroid/content/Context;I)V

    .line 354
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/XitongDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 355
    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 356
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/XitongDialog;->show()V

    .line 357
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->unRigisterHandler()V

    .line 358
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$9;

    invoke-direct {v1, p0, v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$9;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;Lcom/touchus/benchilauncher/views/XitongDialog;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/XitongDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 366
    return-void
.end method

.method private clickItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 249
    iput p1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 250
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->seclectTrue()V

    .line 251
    iget v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->pressBTItem(I)V

    .line 253
    return-void
.end method

.method private pressAMPItem(I)V
    .locals 1
    .param p1, "currentSelectIndex"    # I

    .prologue
    .line 242
    if-eqz p1, :cond_0

    .line 245
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->pressBTItem(I)V

    .line 247
    :cond_0
    return-void
.end method

.method private pressBTItem(I)V
    .locals 1
    .param p1, "currentSelectIndex"    # I

    .prologue
    .line 209
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 212
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 214
    :pswitch_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artAppset()V

    goto :goto_0

    .line 217
    :pswitch_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artSound()V

    goto :goto_0

    .line 220
    :pswitch_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artLanguage()V

    goto :goto_0

    .line 223
    :pswitch_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artDaoche()V

    goto :goto_0

    .line 229
    :pswitch_4
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artNetwork()V

    goto :goto_0

    .line 232
    :pswitch_5
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artSystem()V

    goto :goto_0

    .line 235
    :pswitch_6
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->artVersion()V

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private seclectTrue()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mAdapter:Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

    iget v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->setSeclectIndex(I)V

    .line 256
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mAdapter:Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->notifyDataSetChanged()V

    .line 257
    return-void
.end method


# virtual methods
.method public RigisterHandler()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 263
    :cond_0
    return-void
.end method

.method public create()Lcom/touchus/benchilauncher/views/SettingDialog;
    .locals 6

    .prologue
    .line 68
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    check-cast v2, Lcom/touchus/benchilauncher/Launcher;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mLauncher:Lcom/touchus/benchilauncher/Launcher;

    .line 69
    new-instance v2, Lcom/touchus/benchilauncher/views/SettingDialog;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f080006

    invoke-direct {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SettingDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SettingDialog;

    .line 70
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    .line 71
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 70
    check-cast v0, Landroid/view/LayoutInflater;

    .line 72
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030012

    .line 73
    const/4 v3, 0x0

    .line 72
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 74
    .local v1, "layout":Landroid/view/View;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SettingDialog;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .line 75
    const/4 v4, -0x1

    .line 76
    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 74
    invoke-virtual {v2, v1, v3}, Lcom/touchus/benchilauncher/views/SettingDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    const v2, 0x7f0b001c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mlistView:Landroid/widget/ListView;

    .line 78
    const v2, 0x7f0b006a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->benzconfig:Landroid/widget/TextView;

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    .line 85
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f070044

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f070049

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f070045

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f070046

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f070048

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f07004a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f070047

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v2, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    invoke-direct {v2, v3, v4}, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mAdapter:Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

    .line 94
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mAdapter:Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

    iget v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->setSeclectIndex(I)V

    .line 95
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mlistView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mAdapter:Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mlistView:Landroid/widget/ListView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$2;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$2;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 106
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->benzconfig:Landroid/widget/TextView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;-><init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const/4 v2, 0x0

    iput v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 130
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->seclectTrue()V

    .line 131
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mLauncher:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 132
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->RigisterHandler()V

    .line 134
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SettingDialog;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/views/SettingDialog;->setContentView(Landroid/view/View;)V

    .line 136
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SettingDialog;

    return-object v2
.end method

.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 165
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_1

    .line 166
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 167
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 169
    .local v0, "code":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_2

    .line 171
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 172
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 174
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->seclectTrue()V

    .line 206
    .end local v0    # "code":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 175
    .restart local v0    # "code":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_2
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_4

    .line 176
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    if-lez v2, :cond_3

    .line 177
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 179
    :cond_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->seclectTrue()V

    goto :goto_0

    .line 181
    :cond_4
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_5

    .line 182
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->pressBTItem(I)V

    goto :goto_0

    .line 187
    :cond_5
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_6

    .line 188
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_6

    .line 189
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_7

    .line 190
    :cond_6
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SettingDialog;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/SettingDialog;->dismiss()V

    goto :goto_0

    .line 191
    :cond_7
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_9

    .line 193
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->strings:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_8

    .line 194
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 196
    :cond_8
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->seclectTrue()V

    goto :goto_0

    .line 198
    :cond_9
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_1

    .line 199
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    if-lez v2, :cond_a

    .line 200
    iget v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->currentSelectIndex:I

    .line 202
    :cond_a
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->seclectTrue()V

    goto :goto_0
.end method

.method public unRigisterHandler()V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 269
    :cond_0
    return-void
.end method
