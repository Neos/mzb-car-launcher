.class Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;
.super Landroid/os/Handler;
.source "NetworkDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/NetworkDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/views/NetworkDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/views/NetworkDialog;)V
    .locals 1
    .param p1, "activity"    # Lcom/touchus/benchilauncher/views/NetworkDialog;

    .prologue
    .line 586
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 587
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->target:Ljava/lang/ref/WeakReference;

    .line 588
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 596
    :goto_0
    return-void

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->handlerMsg(Landroid/os/Message;)V

    goto :goto_0
.end method
