.class Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;
.super Ljava/lang/Object;
.source "LoopRotarySwitchView.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;


# direct methods
.method private constructor <init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    return-void
.end method


# virtual methods
.method public compare(Landroid/view/View;Landroid/view/View;)I
    .locals 4
    .param p1, "lhs"    # Landroid/view/View;
    .param p2, "rhs"    # Landroid/view/View;

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 239
    const/4 v0, 0x0

    .line 241
    .local v0, "result":I
    :try_start_0
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v1

    mul-float/2addr v1, v3

    invoke-virtual {p2}, Landroid/view/View;->getScaleX()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v0, v1

    .line 244
    :goto_0
    return v0

    .line 242
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;->compare(Landroid/view/View;Landroid/view/View;)I

    move-result v0

    return v0
.end method
