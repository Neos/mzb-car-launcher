.class public Lcom/touchus/benchilauncher/views/ConfigSetDialog;
.super Landroid/app/Dialog;
.source "ConfigSetDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas:[I


# instance fields
.field private configure_model:Landroid/widget/Spinner;

.field private connect_AUX:Landroid/widget/RadioButton;

.field private connect_BT:Landroid/widget/RadioButton;

.field private connect_type:Landroid/widget/RadioGroup;

.field private context:Landroid/content/Context;

.field private help:Landroid/widget/TextView;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

.field private navi_have:Landroid/widget/RadioButton;

.field private navi_no:Landroid/widget/RadioButton;

.field private original_navi:Landroid/widget/RadioGroup;

.field private radioset:Landroid/widget/RadioGroup;

.field private screenType:[Ljava/lang/String;

.field private square:Landroid/widget/RadioButton;

.field private square_layout:Landroid/widget/RelativeLayout;

.field private stripe:Landroid/widget/RadioButton;

.field private stripe_layout:Landroid/widget/LinearLayout;

.field private type:I

.field private usbNum:[Ljava/lang/String;

.field private usb_configure:Landroid/widget/Spinner;


# direct methods
.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas()[I
    .locals 3

    .prologue
    .line 46
    sget-object v0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->values()[Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ACTIVATING:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->FAILED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->SUCCEED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 76
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 67
    iput v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->type:I

    .line 72
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "2"

    aput-object v1, v0, v3

    const-string v1, "3"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "10"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->screenType:[Ljava/lang/String;

    .line 73
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "2"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usbNum:[Ljava/lang/String;

    .line 380
    new-instance v0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 80
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 67
    iput v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->type:I

    .line 72
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "2"

    aput-object v1, v0, v3

    const-string v1, "3"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "10"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->screenType:[Ljava/lang/String;

    .line 73
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v4

    const-string v1, "2"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usbNum:[Ljava/lang/String;

    .line 380
    new-instance v0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    .line 81
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    .line 82
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/ConfigSetDialog;I)V
    .locals 0

    .prologue
    .line 67
    iput p1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->type:I

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/ConfigSetDialog;I)V
    .locals 0

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/utils/SpUtilK;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/ConfigSetDialog;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 314
    invoke-direct/range {p0 .. p5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->setConnectType()V

    return-void
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/views/ConfigSetDialog;Lcom/touchus/benchilauncher/views/MyCustomDialog;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    return-void
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/views/MyCustomDialog;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    return-object v0
.end method

.method private initData()V
    .locals 6

    .prologue
    const v4, 0x7f030041

    const/4 v5, 0x0

    .line 261
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    check-cast v2, Lcom/touchus/benchilauncher/Launcher;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 262
    new-instance v2, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 265
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    .line 266
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->screenType:[Ljava/lang/String;

    .line 265
    invoke-direct {v0, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 267
    .local v0, "screenAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 268
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->configure_model:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 270
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    .line 271
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usbNum:[Ljava/lang/String;

    .line 270
    invoke-direct {v1, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 272
    .local v1, "usbNumAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 273
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usb_configure:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 275
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v4, "radioType"

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    .line 276
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v4, "naviExist"

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    .line 277
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v4, "usbNum"

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    .line 278
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v4, "connectType"

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 279
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v4, "screenType"

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    .line 280
    return-void
.end method

.method private initSetup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 107
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->stripe:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->square:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    const-string v1, "com.unibroad.benzuserguide"

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/utils/Utiltools;->isAvilible(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->help:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->help:Landroid/widget/TextView;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$1;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->radioset:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$2;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 141
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->original_navi:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$3;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->connect_type:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 189
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->configure_model:Landroid/widget/Spinner;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$5;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 206
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usb_configure:Landroid/widget/Spinner;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$6;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 221
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->seclectTrue(I)V

    .line 222
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    if-le v0, v3, :cond_2

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput v4, v0, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->original_navi:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 226
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    if-le v0, v3, :cond_4

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput v4, v0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 229
    :cond_4
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connect_type.size = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->connect_type:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->connect_type:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 232
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usbNum:[Ljava/lang/String;

    array-length v1, v1

    if-le v0, v1, :cond_6

    .line 233
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput v4, v0, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    .line 235
    :cond_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usb_configure:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 236
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->configure_model:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 237
    return-void
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 240
    const v0, 0x7f0b0032

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->help:Landroid/widget/TextView;

    .line 241
    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->radioset:Landroid/widget/RadioGroup;

    .line 242
    const v0, 0x7f0b0037

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->original_navi:Landroid/widget/RadioGroup;

    .line 243
    const v0, 0x7f0b003a

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->connect_type:Landroid/widget/RadioGroup;

    .line 245
    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->stripe:Landroid/widget/RadioButton;

    .line 246
    const v0, 0x7f0b0035

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->square:Landroid/widget/RadioButton;

    .line 247
    const v0, 0x7f0b0038

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->navi_have:Landroid/widget/RadioButton;

    .line 248
    const v0, 0x7f0b0039

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->navi_no:Landroid/widget/RadioButton;

    .line 249
    const v0, 0x7f0b003b

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->connect_BT:Landroid/widget/RadioButton;

    .line 250
    const v0, 0x7f0b003c

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->connect_AUX:Landroid/widget/RadioButton;

    .line 252
    const v0, 0x7f0b0036

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->stripe_layout:Landroid/widget/LinearLayout;

    .line 253
    const v0, 0x7f0b003d

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->square_layout:Landroid/widget/RelativeLayout;

    .line 255
    const v0, 0x7f0b0041

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->configure_model:Landroid/widget/Spinner;

    .line 256
    const v0, 0x7f0b003f

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->usb_configure:Landroid/widget/Spinner;

    .line 258
    return-void
.end method

.method private pressItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 374
    return-void
.end method

.method private seclectTrue(I)V
    .locals 4
    .param p1, "seclectCuttun"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 451
    iget v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->type:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 453
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    if-nez v0, :cond_3

    .line 454
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->square_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 455
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->stripe_layout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 460
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->radioset:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    .line 463
    :cond_1
    sget-object v0, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 464
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SysConst.storeData = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendStoreDataToMcu([B)V

    .line 466
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "radioType"

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 467
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->radioset:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 469
    :cond_2
    return-void

    .line 457
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->square_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->stripe_layout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setConnectType()V
    .locals 3

    .prologue
    .line 297
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x1

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 298
    const/4 v0, 0x0

    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    .line 299
    sget-object v0, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 300
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "connectType"

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 301
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendStoreDataToMcu([B)V

    .line 302
    return-void
.end method

.method private setDialogLocation(Landroid/app/Dialog;)V
    .locals 3
    .param p1, "myCustomDialog"    # Landroid/app/Dialog;

    .prologue
    .line 365
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 366
    .local v0, "dialogWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 367
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x100

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 368
    const/16 v2, -0x1e

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 369
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 370
    return-void
.end method

.method private showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "Messsage"    # Ljava/lang/String;
    .param p2, "nextMessage"    # Ljava/lang/String;
    .param p3, "btnPosiText"    # Ljava/lang/String;
    .param p4, "btnNegaText"    # Ljava/lang/String;
    .param p5, "flag"    # Z

    .prologue
    .line 316
    new-instance v0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 317
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    .line 316
    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 318
    .local v0, "builder":Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 319
    invoke-virtual {v0, p2}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setNextMessage(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 321
    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$7;

    invoke-direct {v1, p0, p5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$7;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;Z)V

    .line 320
    invoke-virtual {v0, p3, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 336
    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$8;

    invoke-direct {v1, p0, p5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$8;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;Z)V

    .line 335
    invoke-virtual {v0, p4, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 347
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 350
    :cond_0
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->create()Lcom/touchus/benchilauncher/views/MyCustomDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    .line 351
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    .line 352
    new-instance v2, Lcom/touchus/benchilauncher/views/ConfigSetDialog$9;

    invoke-direct {v2, p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$9;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)V

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 360
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->setDialogLocation(Landroid/app/Dialog;)V

    .line 361
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->show()V

    .line 362
    return-void
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 399
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x1771

    if-ne v0, v1, :cond_2

    .line 400
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    .line 401
    .local v8, "temp":Landroid/os/Bundle;
    const-string v0, "idriver_enum"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v7

    .line 402
    .local v7, "idriverCode":B
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v0

    if-eq v7, v0, :cond_0

    .line 403
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v0

    if-eq v7, v0, :cond_0

    .line 404
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v0

    if-ne v7, v0, :cond_1

    .line 405
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->dismiss()V

    .line 448
    .end local v7    # "idriverCode":B
    .end local v8    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 407
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x418

    if-ne v0, v1, :cond_1

    .line 408
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    .line 409
    .restart local v8    # "temp":Landroid/os/Bundle;
    const-string v0, "aux_activate_stutas"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    .line 410
    .local v6, "eStutas":Lcom/backaudio/android/driver/Mainboard$EAUXStutas;
    invoke-static {}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas()[I

    move-result-object v0

    invoke-virtual {v6}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 412
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_3

    .line 413
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 416
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    const v1, 0x7f0700b9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 417
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    const v2, 0x7f0700ba

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v4, v3

    .line 415
    invoke-direct/range {v0 .. v5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 421
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_4

    .line 422
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 425
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    const v1, 0x7f0700bb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 426
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    const v2, 0x7f0700bc

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v4, v3

    .line 424
    invoke-direct/range {v0 .. v5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 428
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    new-instance v1, Lcom/touchus/benchilauncher/views/ConfigSetDialog$10;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$10;-><init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V

    .line 436
    const-wide/16 v2, 0x7d0

    .line 428
    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 439
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_5

    .line 440
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 443
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    const v1, 0x7f0700bd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    .line 444
    const/4 v5, 0x1

    move-object v0, p0

    move-object v4, v3

    .line 442
    invoke-direct/range {v0 .. v5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 410
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 284
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 294
    :goto_0
    return-void

    .line 287
    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->type:I

    .line 288
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->seclectTrue(I)V

    goto :goto_0

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0034
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 87
    const v0, 0x7f03000b

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->setContentView(I)V

    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 89
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->initView()V

    .line 90
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->initData()V

    .line 91
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->initSetup()V

    .line 92
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 97
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 98
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 103
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 104
    return-void
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 378
    return-void
.end method
