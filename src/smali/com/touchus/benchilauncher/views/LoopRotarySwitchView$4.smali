.class Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;
.super Ljava/lang/Object;
.source "LoopRotarySwitchView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->checkChildView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iput p2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->val$position:I

    .line 454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 457
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$10(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v0

    .line 459
    .local v0, "calculateItem":I
    iget v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->val$position:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v2, v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$11(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 460
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->val$position:I

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setSelectItem(I)V

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$12(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$13(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/inface/OnItemClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 463
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$13(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/inface/OnItemClickListener;

    move-result-object v2

    iget v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->val$position:I

    .line 464
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$7(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Ljava/util/List;

    move-result-object v1

    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;->val$position:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 463
    invoke-interface {v2, v3, v1}, Lcom/touchus/benchilauncher/inface/OnItemClickListener;->onItemClick(ILandroid/view/View;)V

    goto :goto_0
.end method
