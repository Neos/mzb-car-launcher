.class public Lcom/touchus/benchilauncher/views/ClockView;
.super Landroid/view/View;
.source "ClockView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# instance fields
.field private clockDrawable:Landroid/graphics/drawable/Drawable;

.field private clockThread:Ljava/lang/Thread;

.field private hour:I

.field private hourDrawable:Landroid/graphics/drawable/Drawable;

.field private isChange:Z

.field private isSetTime:Z

.field private minute:I

.field private minuteDrawable:Landroid/graphics/drawable/Drawable;

.field private paint:Landroid/graphics/Paint;

.field private time:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/ClockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/touchus/benchilauncher/views/ClockView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/ClockView;->isSetTime:Z

    .line 52
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ClockView;->mContext:Landroid/content/Context;

    .line 57
    sget-object v1, Lcom/touchus/benchilauncher/R$styleable;->MyClockStyleable:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 58
    .local v0, "ta":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->clockDrawable:Landroid/graphics/drawable/Drawable;

    .line 60
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->hourDrawable:Landroid/graphics/drawable/Drawable;

    .line 61
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->minuteDrawable:Landroid/graphics/drawable/Drawable;

    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 66
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->paint:Landroid/graphics/Paint;

    .line 67
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->paint:Landroid/graphics/Paint;

    const-string v2, "#000000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 69
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 70
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 72
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    .line 73
    new-instance v1, Lcom/touchus/benchilauncher/views/ClockView$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ClockView$1;-><init>(Lcom/touchus/benchilauncher/views/ClockView;)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/ClockView;->clockThread:Ljava/lang/Thread;

    .line 85
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/ClockView;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/ClockView;->isChange:Z

    return v0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/ClockView;->isChange:Z

    .line 192
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ClockView;->clockThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 193
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/ClockView;->isChange:Z

    .line 199
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v13, 0x43b40000    # 360.0f

    const/high16 v12, 0x42700000    # 60.0f

    .line 98
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 100
    iget-boolean v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->isSetTime:Z

    if-eqz v8, :cond_0

    .line 101
    iget-object v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    iget v9, p0, Lcom/touchus/benchilauncher/views/ClockView;->hour:I

    iput v9, v8, Landroid/text/format/Time;->hour:I

    .line 102
    iget-object v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    iget v9, p0, Lcom/touchus/benchilauncher/views/ClockView;->minute:I

    iput v9, v8, Landroid/text/format/Time;->minute:I

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v5, v8, 0x2

    .line 106
    .local v5, "viewCenterX":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getBottom()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v6, v8, 0x2

    .line 108
    .local v6, "viewCenterY":I
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ClockView;->clockDrawable:Landroid/graphics/drawable/Drawable;

    .line 109
    .local v0, "dial":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 110
    .local v1, "h":I
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 112
    .local v7, "w":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    if-lt v8, v7, :cond_1

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getBottom()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    if-ge v8, v1, :cond_2

    .line 113
    :cond_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    int-to-float v9, v7

    div-float/2addr v8, v9

    .line 114
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getBottom()I

    move-result v9

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ClockView;->getTop()I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v9, v9

    int-to-float v10, v1

    div-float/2addr v9, v10

    .line 113
    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 115
    .local v4, "scale":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 116
    int-to-float v8, v5

    int-to-float v9, v6

    invoke-virtual {p1, v4, v4, v8, v9}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 118
    .end local v4    # "scale":F
    :cond_2
    iget-boolean v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->isChange:Z

    if-eqz v8, :cond_3

    .line 119
    div-int/lit8 v8, v7, 0x2

    sub-int v8, v5, v8

    .line 120
    div-int/lit8 v9, v1, 0x2

    sub-int v9, v6, v9

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v5

    .line 121
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v6

    .line 119
    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 123
    :cond_3
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 124
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 137
    iget-object v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    iget v8, v8, Landroid/text/format/Time;->hour:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    iget v9, v9, Landroid/text/format/Time;->minute:I

    int-to-float v9, v9

    div-float/2addr v9, v12

    add-float/2addr v8, v9

    const/high16 v9, 0x41400000    # 12.0f

    div-float/2addr v8, v9

    mul-float/2addr v8, v13

    int-to-float v9, v5

    int-to-float v10, v6

    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 138
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ClockView;->hourDrawable:Landroid/graphics/drawable/Drawable;

    .line 139
    .local v2, "mHour":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 140
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 141
    iget-boolean v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->isChange:Z

    if-eqz v8, :cond_4

    .line 142
    div-int/lit8 v8, v7, 0x2

    sub-int v8, v5, v8

    .line 143
    div-int/lit8 v9, v1, 0x2

    sub-int v9, v6, v9

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v5

    .line 144
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v6

    .line 142
    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 146
    :cond_4
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 147
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 148
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 151
    iget-object v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    iget v8, v8, Landroid/text/format/Time;->minute:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/touchus/benchilauncher/views/ClockView;->time:Landroid/text/format/Time;

    iget v9, v9, Landroid/text/format/Time;->second:I

    int-to-float v9, v9

    div-float/2addr v9, v12

    add-float/2addr v8, v9

    div-float/2addr v8, v12

    mul-float/2addr v8, v13

    int-to-float v9, v5

    int-to-float v10, v6

    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 152
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ClockView;->minuteDrawable:Landroid/graphics/drawable/Drawable;

    .line 153
    .local v3, "mMinute":Landroid/graphics/drawable/Drawable;
    iget-boolean v8, p0, Lcom/touchus/benchilauncher/views/ClockView;->isChange:Z

    if-eqz v8, :cond_5

    .line 154
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 155
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 156
    div-int/lit8 v8, v7, 0x2

    sub-int v8, v5, v8

    div-int/lit8 v9, v1, 0x2

    sub-int v9, v6, v9

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v5

    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v6

    invoke-virtual {v3, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 158
    :cond_5
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 159
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 160
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 186
    return-void
.end method

.method public setTime(ZII)V
    .locals 0
    .param p1, "isSetTime"    # Z
    .param p2, "h"    # I
    .param p3, "m"    # I

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/views/ClockView;->isSetTime:Z

    .line 92
    iput p2, p0, Lcom/touchus/benchilauncher/views/ClockView;->hour:I

    .line 93
    iput p3, p0, Lcom/touchus/benchilauncher/views/ClockView;->minute:I

    .line 94
    return-void
.end method
