.class Lcom/touchus/benchilauncher/views/UpdateDialog$3;
.super Ljava/lang/Object;
.source "UpdateDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/UpdateDialog;->startUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/UpdateDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/UpdateDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog$3;->this$0:Lcom/touchus/benchilauncher/views/UpdateDialog;

    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 243
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog$3;->this$0:Lcom/touchus/benchilauncher/views/UpdateDialog;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UpdateDialog$3;->this$0:Lcom/touchus/benchilauncher/views/UpdateDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;->access$0(Lcom/touchus/benchilauncher/views/UpdateDialog;)Ljava/lang/String;

    move-result-object v2

    .line 244
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/UpdateDialog$3;->this$0:Lcom/touchus/benchilauncher/views/UpdateDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/UpdateDialog;->access$1(Lcom/touchus/benchilauncher/views/UpdateDialog;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f07000c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 243
    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/views/UpdateDialog;->copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 245
    .local v0, "iUpdate":Z
    if-eqz v0, :cond_0

    .line 246
    invoke-static {}, Lcom/touchus/benchilauncher/utils/Utiltools;->updateLauncherAPK()V

    .line 250
    :goto_0
    return-void

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UpdateDialog$3;->this$0:Lcom/touchus/benchilauncher/views/UpdateDialog;

    iget-object v1, v1, Lcom/touchus/benchilauncher/views/UpdateDialog;->mHandler:Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;

    const/16 v2, 0x468

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog$MHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method
