.class public Lcom/touchus/benchilauncher/views/DesktopLayout;
.super Landroid/widget/LinearLayout;
.source "DesktopLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private back:Landroid/widget/ImageView;

.field context:Landroid/content/Context;

.field hideBgRun:Ljava/lang/Runnable;

.field hideBtnRun:Ljava/lang/Runnable;

.field private home:Landroid/widget/ImageView;

.field isHide:Z

.field mHandler:Landroid/os/Handler;

.field private right_push:Landroid/widget/ImageView;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, -0x2

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->mHandler:Landroid/os/Handler;

    .line 83
    new-instance v0, Lcom/touchus/benchilauncher/views/DesktopLayout$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/DesktopLayout$1;-><init>(Lcom/touchus/benchilauncher/views/DesktopLayout;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBtnRun:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/touchus/benchilauncher/views/DesktopLayout$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/DesktopLayout$2;-><init>(Lcom/touchus/benchilauncher/views/DesktopLayout;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBgRun:Ljava/lang/Runnable;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->isHide:Z

    .line 27
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->context:Landroid/content/Context;

    .line 28
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 29
    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 28
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 31
    const v1, 0x7f030005

    const/4 v2, 0x0

    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->view:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->view:Landroid/view/View;

    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->right_push:Landroid/widget/ImageView;

    .line 33
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->view:Landroid/view/View;

    const v1, 0x7f0b0016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->home:Landroid/widget/ImageView;

    .line 34
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->view:Landroid/view/View;

    const v1, 0x7f0b0017

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->back:Landroid/widget/ImageView;

    .line 35
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->right_push:Landroid/widget/ImageView;

    new-instance v1, Lcom/touchus/benchilauncher/views/DesktopLayout$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/DesktopLayout$3;-><init>(Lcom/touchus/benchilauncher/views/DesktopLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 53
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->right_push:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->home:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->back:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->setBtnhide()V

    .line 57
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->view:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->addView(Landroid/view/View;)V

    .line 58
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->view:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->right_push:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->home:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->back:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/DesktopLayout;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->setBtnhide()V

    return-void
.end method

.method private setBtnhide()V
    .locals 4

    .prologue
    const-wide/16 v2, 0xbb8

    .line 77
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBgRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBtnRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 79
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBgRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBtnRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 81
    return-void
.end method


# virtual methods
.method public hideBtn()Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x0

    .line 96
    const-string v1, "DesktopLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isHide = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->isHide:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->isHide:Z

    if-eqz v1, :cond_0

    .line 105
    :goto_0
    return v0

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->hideBtnRun:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 101
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->right_push:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->home:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->back:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->isHide:Z

    .line 105
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout;->isHide:Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 75
    :goto_0
    return-void

    .line 63
    :pswitch_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->sendKeyeventToSystem(I)V

    .line 65
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->setBtnhide()V

    goto :goto_0

    .line 68
    :pswitch_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->sendKeyeventToSystem(I)V

    .line 70
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->setBtnhide()V

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x7f0b0016
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public sendKeyeventToSystem(I)V
    .locals 3
    .param p1, "keyeventCode"    # I

    .prologue
    .line 113
    if-gez p1, :cond_0

    .line 124
    :goto_0
    return-void

    .line 116
    :cond_0
    move v0, p1

    .line 117
    .local v0, "keycode":I
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/touchus/benchilauncher/views/DesktopLayout$4;

    invoke-direct {v2, p0, v0}, Lcom/touchus/benchilauncher/views/DesktopLayout$4;-><init>(Lcom/touchus/benchilauncher/views/DesktopLayout;I)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 123
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
