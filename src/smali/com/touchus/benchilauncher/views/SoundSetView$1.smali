.class Lcom/touchus/benchilauncher/views/SoundSetView$1;
.super Ljava/lang/Object;
.source "SoundSetView.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/SoundSetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/SoundSetView;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/SoundSetView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v1, 0x1

    .line 55
    if-ge p2, v1, :cond_1

    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$0(Lcom/touchus/benchilauncher/views/SoundSetView;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0, p2}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$1(Lcom/touchus/benchilauncher/views/SoundSetView;I)V

    .line 60
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$2(Lcom/touchus/benchilauncher/views/SoundSetView;)I

    move-result v0

    sget v1, Lcom/touchus/benchilauncher/views/SoundSetView;->SOUND:I

    if-ne v0, v1, :cond_2

    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$3(Lcom/touchus/benchilauncher/views/SoundSetView;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$4(Lcom/touchus/benchilauncher/views/SoundSetView;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$5(Lcom/touchus/benchilauncher/views/SoundSetView;)Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$5(Lcom/touchus/benchilauncher/views/SoundSetView;)Lcom/touchus/benchilauncher/inface/OnSoundClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$4(Lcom/touchus/benchilauncher/views/SoundSetView;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/touchus/benchilauncher/inface/OnSoundClickListener;->click(I)V

    goto :goto_0

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$3(Lcom/touchus/benchilauncher/views/SoundSetView;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetView$1;->this$0:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/SoundSetView;->access$4(Lcom/touchus/benchilauncher/views/SoundSetView;)I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 73
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 78
    return-void
.end method
