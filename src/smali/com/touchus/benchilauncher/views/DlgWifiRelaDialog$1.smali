.class Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;
.super Ljava/lang/Object;
.source "DlgWifiRelaDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;->this$0:Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 148
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 143
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 135
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;->this$0:Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->access$0(Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog$1;->this$0:Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->access$0(Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 138
    :cond_0
    return-void
.end method
