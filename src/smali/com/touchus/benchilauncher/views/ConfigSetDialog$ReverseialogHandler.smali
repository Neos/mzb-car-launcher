.class Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;
.super Landroid/os/Handler;
.source "ConfigSetDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/ConfigSetDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ReverseialogHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/views/ConfigSetDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V
    .locals 1
    .param p1, "activity"    # Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    .prologue
    .line 385
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 386
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;->target:Ljava/lang/ref/WeakReference;

    .line 387
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 395
    :goto_0
    return-void

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$ReverseialogHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->handlerMsg(Landroid/os/Message;)V

    goto :goto_0
.end method
