.class public Lcom/touchus/benchilauncher/views/SystemOperateDialog;
.super Landroid/app/Dialog;
.source "SystemOperateDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;
    }
.end annotation


# instance fields
.field private canceBtn:Landroid/widget/Button;

.field public height:I

.field private itemString:Ljava/lang/String;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mContext:Landroid/content/Context;

.field public mHandler:Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

.field public showMessage:Ljava/lang/String;

.field private submitBtn:Landroid/widget/Button;

.field public title:Ljava/lang/String;

.field private titleName:Landroid/widget/TextView;

.field public width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 25
    const/16 v0, 0x19f

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->width:I

    .line 26
    const/16 v0, 0x96

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->height:I

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->showMessage:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->title:Ljava/lang/String;

    .line 110
    new-instance v0, Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/SystemOperateDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

    .line 38
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeId"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 25
    const/16 v0, 0x19f

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->width:I

    .line 26
    const/16 v0, 0x96

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->height:I

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->showMessage:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->title:Ljava/lang/String;

    .line 110
    new-instance v0, Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/SystemOperateDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

    .line 43
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 129
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_1

    .line 130
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 131
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 132
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_0

    .line 133
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_2

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setSelected(Z)V

    .line 135
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setSelected(Z)V

    .line 152
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 136
    .restart local v0    # "idriverCode":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_2
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_3

    .line 137
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_4

    .line 138
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setSelected(Z)V

    .line 139
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_0

    .line 140
    :cond_4
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_6

    .line 141
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 142
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->performClick()Z

    goto :goto_0

    .line 144
    :cond_5
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->performClick()Z

    goto :goto_0

    .line 146
    :cond_6
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_7

    .line 147
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_7

    .line 148
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_1

    .line 149
    :cond_7
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->dismiss()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0b0063

    if-ne v1, v2, :cond_2

    .line 95
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->itemString:Ljava/lang/String;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f0700a4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 98
    .local v0, "pManager":Landroid/os/PowerManager;
    const-string v1, "reboot"

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 107
    .end local v0    # "pManager":Landroid/os/PowerManager;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->dismiss()V

    .line 108
    return-void

    .line 99
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->itemString:Ljava/lang/String;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f0700a5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.touchus.factorytest.recovery"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 104
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->setContentView(I)V

    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 56
    const v0, 0x7f0b0083

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    .line 57
    const v0, 0x7f0b0063

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    .line 58
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f070058

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f07005a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->height:I

    int-to-double v0, v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->height:I

    .line 65
    const v0, 0x7f0b007e

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->titleName:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->itemString:Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0700a4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->titleName:Landroid/widget/TextView;

    const v1, 0x7f0700a6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->width:I

    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->height:I

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 77
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->canceBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->submitBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 79
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->itemString:Ljava/lang/String;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0700a5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->titleName:Landroid/widget/TextView;

    const v1, 0x7f0700a7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 84
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 85
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemOperateDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 89
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 90
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "itemString"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->itemString:Ljava/lang/String;

    .line 48
    return-void
.end method
