.class public Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;
.super Ljava/lang/Object;
.source "SoundSetDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/SoundSetDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;
    }
.end annotation


# instance fields
.field private arr:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field builder:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

.field private context:Landroid/content/Context;

.field private count:I

.field private dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

.field private dv_tv:Landroid/widget/TextView;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mDialog:Lcom/touchus/benchilauncher/views/SoundSetDialog;

.field private mIDRIVERENUM:B

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field public mSettingHandler:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

.field private media_tv:Landroid/widget/TextView;

.field private mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

.field private mix_tv:Landroid/widget/TextView;

.field private naviSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

.field private navi_tv:Landroid/widget/TextView;

.field private size:I

.field private soundViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/views/SoundSetView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->builder:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    .line 45
    const/4 v0, 0x4

    iput v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->size:I

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    .line 217
    new-instance v0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;

    .line 51
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    .line 53
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;I)V
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    return v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;I)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)Lcom/touchus/benchilauncher/utils/SpUtilK;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    return-object v0
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 264
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 271
    return-void

    .line 265
    :cond_0
    if-ne p1, v0, :cond_1

    .line 266
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 264
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public create()Lcom/touchus/benchilauncher/views/SoundSetDialog;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 56
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    check-cast v2, Lcom/touchus/benchilauncher/Launcher;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 57
    new-instance v2, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 58
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 59
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 61
    new-instance v2, Lcom/touchus/benchilauncher/views/SoundSetDialog;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f080006

    invoke-direct {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SoundSetDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundSetDialog;

    .line 62
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    .line 63
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 62
    check-cast v0, Landroid/view/LayoutInflater;

    .line 64
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030014

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    .local v1, "layout":Landroid/view/View;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundSetDialog;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .line 66
    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 65
    invoke-virtual {v2, v1, v3}, Lcom/touchus/benchilauncher/views/SoundSetDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    const v2, 0x7f0b006f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->navi_tv:Landroid/widget/TextView;

    .line 69
    const v2, 0x7f0b0071

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->media_tv:Landroid/widget/TextView;

    .line 70
    const v2, 0x7f0b0075

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dv_tv:Landroid/widget/TextView;

    .line 71
    const v2, 0x7f0b0073

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mix_tv:Landroid/widget/TextView;

    .line 73
    const v2, 0x7f0b0070

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/views/SoundSetView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->naviSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    .line 74
    const v2, 0x7f0b0072

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/views/SoundSetView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    .line 75
    const v2, 0x7f0b0076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/views/SoundSetView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    .line 76
    const v2, 0x7f0b0074

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/views/SoundSetView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    .line 78
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->navi_tv:Landroid/widget/TextView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->naviSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v2

    if-nez v2, :cond_2

    .line 81
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->media_tv:Landroid/widget/TextView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->media_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    .line 85
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v2, :cond_1

    .line 86
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mix_tv:Landroid/widget/TextView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mix_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    .line 100
    :goto_0
    sget-boolean v2, Lcom/touchus/benchilauncher/SysConst;->DV_CUSTOM:Z

    if-eqz v2, :cond_3

    .line 101
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dv_tv:Landroid/widget/TextView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dv_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    .line 110
    :goto_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->navi_tv:Landroid/widget/TextView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$1;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$1;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->media_tv:Landroid/widget/TextView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$2;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$2;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mix_tv:Landroid/widget/TextView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$3;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$3;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dv_tv:Landroid/widget/TextView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$4;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$4;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->naviSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$5;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$5;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setOnSoundClickListener(Lcom/touchus/benchilauncher/inface/OnSoundClickListener;)V

    .line 154
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setOnSoundClickListener(Lcom/touchus/benchilauncher/inface/OnSoundClickListener;)V

    .line 169
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$7;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$7;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setOnSoundClickListener(Lcom/touchus/benchilauncher/inface/OnSoundClickListener;)V

    .line 181
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$8;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$8;-><init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setOnSoundClickListener(Lcom/touchus/benchilauncher/inface/OnSoundClickListener;)V

    .line 192
    sget-boolean v2, Lcom/touchus/benchilauncher/SysConst;->DV_CUSTOM:Z

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v4, Lcom/touchus/benchilauncher/SysConst;->dvVoice:Ljava/lang/String;

    invoke-virtual {v3, v4, v8}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 194
    sget v4, Lcom/touchus/benchilauncher/views/SoundSetView;->SOUND:I

    .line 193
    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSoundValue(II)V

    .line 196
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v4, Lcom/touchus/benchilauncher/SysConst;->mediaVoice:Ljava/lang/String;

    invoke-virtual {v3, v4, v8}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    sget v4, Lcom/touchus/benchilauncher/views/SoundSetView;->SOUND:I

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSoundValue(II)V

    .line 197
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f0700ab

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSpeakText(Ljava/lang/String;)V

    .line 199
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v4, Lcom/touchus/benchilauncher/SysConst;->mixPro:Ljava/lang/String;

    invoke-virtual {v3, v4, v8}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 200
    sget v4, Lcom/touchus/benchilauncher/views/SoundSetView;->MIXPRO:I

    .line 199
    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSoundValue(II)V

    .line 201
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f0700aa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSpeakText(Ljava/lang/String;)V

    .line 203
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->naviSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v4, Lcom/touchus/benchilauncher/SysConst;->naviVoice:Ljava/lang/String;

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 204
    sget v4, Lcom/touchus/benchilauncher/views/SoundSetView;->SOUND:I

    .line 203
    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSoundValue(II)V

    .line 205
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->naviSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f0700a8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/SoundSetView;->setSpeakText(Ljava/lang/String;)V

    .line 207
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundSetDialog;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog;->setContentView(Landroid/view/View;)V

    .line 209
    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->seclectTrue(I)V

    .line 210
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundSetDialog;

    return-object v2

    .line 91
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mix_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    goto/16 :goto_0

    .line 95
    :cond_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->media_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mediaSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    .line 97
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mix_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mixSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    goto/16 :goto_0

    .line 106
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dv_tv:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->dvSetView:Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v2, v6}, Lcom/touchus/benchilauncher/views/SoundSetView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 237
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_1

    .line 238
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 239
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    .line 240
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 241
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->arr:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 242
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    .line 244
    :cond_0
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->seclectTrue(I)V

    .line 261
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 245
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_4

    .line 246
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    if-lez v1, :cond_3

    .line 247
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    .line 249
    :cond_3
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->seclectTrue(I)V

    goto :goto_0

    .line 250
    :cond_4
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_5

    .line 251
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->addSoundValue()V

    goto :goto_0

    .line 252
    :cond_5
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_6

    .line 253
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->soundViews:Ljava/util/List;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->count:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/views/SoundSetView;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/SoundSetView;->decSoundValue()V

    goto :goto_0

    .line 254
    :cond_6
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_7

    .line 255
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_7

    .line 256
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_7

    .line 257
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_1

    .line 258
    :cond_7
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundSetDialog;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog;->dismiss()V

    goto :goto_0
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$YuyingDialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 215
    return-void
.end method
