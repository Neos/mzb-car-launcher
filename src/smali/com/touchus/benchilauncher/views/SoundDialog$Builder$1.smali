.class Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;
.super Ljava/lang/Object;
.source "SoundDialog.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->create(I)Lcom/touchus/benchilauncher/views/SoundDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v3, 0x0

    .line 89
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0, p2}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$0(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;I)V

    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$1(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$3(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$4(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->naviVoice:Ljava/lang/String;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$4(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->callVoice:Ljava/lang/String;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 95
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->callnum:[I

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v4}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v4

    aget v2, v2, v4

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 103
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 108
    return-void
.end method
