.class public Lcom/touchus/benchilauncher/views/CarWarnInfoView;
.super Landroid/widget/LinearLayout;
.source "CarWarnInfoView.java"


# instance fields
.field private infoImg:Landroid/widget/ImageView;

.field private infoText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030004

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 25
    const v0, 0x7f0b0013

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoImg:Landroid/widget/ImageView;

    .line 26
    const v0, 0x7f0b0014

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoText:Landroid/widget/TextView;

    .line 27
    return-void
.end method


# virtual methods
.method public setCarInfo(ZILjava/lang/String;)V
    .locals 2
    .param p1, "isWarning"    # Z
    .param p2, "imgres"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 30
    if-eqz p1, :cond_0

    .line 31
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoImg:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 32
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoText:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 33
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoText:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoImg:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 36
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoText:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 37
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CarWarnInfoView;->infoText:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
