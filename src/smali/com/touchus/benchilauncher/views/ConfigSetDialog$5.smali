.class Lcom/touchus/benchilauncher/views/ConfigSetDialog$5;
.super Ljava/lang/Object;
.source "ConfigSetDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/ConfigSetDialog;->initSetup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$5;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/backaudio/android/driver/Mainboard;->setBenzSize(I)V

    .line 195
    const/4 v0, 0x6

    if-ne p3, v0, :cond_0

    .line 196
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->setOldCBTAudio()V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$5;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$4(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    const-string v1, "screenType"

    invoke-virtual {v0, v1, p3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 199
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
