.class public Lcom/touchus/benchilauncher/views/FeedbackDialog;
.super Landroid/app/Dialog;
.source "FeedbackDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private cancelBtn:Landroid/widget/Button;

.field private context:Landroid/content/Context;

.field private iInCancelState:Z

.field public mHandler:Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

.field private phoneNum:Landroid/widget/EditText;

.field private submitBtn:Landroid/widget/Button;

.field private suggest:Landroid/widget/EditText;

.field private suggestStr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->iInCancelState:Z

    .line 157
    new-instance v0, Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/FeedbackDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->mHandler:Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

    .line 53
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->iInCancelState:Z

    .line 157
    new-instance v0, Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/FeedbackDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->mHandler:Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

    .line 58
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    .line 59
    return-void
.end method

.method private submit()V
    .locals 9

    .prologue
    .line 125
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "path":Ljava/lang/String;
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "path = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 129
    .local v1, "files":[Ljava/io/File;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v4, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v1

    if-lt v2, v5, :cond_0

    .line 134
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/data/anr"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 135
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "path = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 138
    const/4 v2, 0x0

    :goto_1
    array-length v5, v1

    if-lt v2, v5, :cond_1

    .line 147
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_3

    .line 155
    :goto_2
    return-void

    .line 131
    :cond_0
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "files[i].getAbsolutePath() = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v1, v2

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    :cond_1
    const-string v5, ""

    .line 140
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "files[i].getAbsolutePath() = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 141
    aget-object v7, v1, v2

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 140
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 139
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 138
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 145
    :cond_2
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 151
    :cond_3
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/touchus/publicutils/utils/MailManager;->getInstance(Landroid/content/Context;)Lcom/touchus/publicutils/utils/MailManager;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->benzName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "\u5954\u9a70\u53cd\u9988\u6536\u96c6"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/touchus/publicutils/utils/TimeUtils;->getSimpleDate()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\u53cd\u9988\u610f\u89c1\uff1a"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->suggestStr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n\u8054\u7cfb\u53f7\u7801\uff1a"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 152
    iget-object v8, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->phoneNum:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 153
    const-string v8, "\u5730\u5740\uff1a"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v8, v8, Lcom/touchus/benchilauncher/LauncherApplication;->address:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 151
    invoke-virtual {v5, v6, v7, v4}, Lcom/touchus/publicutils/utils/MailManager;->sendMailWithMultiFile(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 154
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->dismiss()V

    goto/16 :goto_2
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    .line 103
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 104
    return-void
.end method

.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 176
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_9

    .line 177
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 178
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 179
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_0

    .line 180
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_2

    .line 181
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->iInCancelState:Z

    .line 205
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 182
    .restart local v0    # "idriverCode":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_2
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_3

    .line 183
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_4

    .line 184
    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->iInCancelState:Z

    goto :goto_0

    .line 185
    :cond_4
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_6

    .line 186
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->iInCancelState:Z

    if-eqz v2, :cond_5

    .line 187
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->dismiss()V

    goto :goto_0

    .line 189
    :cond_5
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->submit()V

    goto :goto_0

    .line 191
    :cond_6
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_7

    .line 192
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->dismiss()V

    goto :goto_0

    .line 193
    :cond_7
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_8

    .line 194
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_1

    .line 195
    :cond_8
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->dismiss()V

    goto :goto_0

    .line 197
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_9
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x40f

    if-ne v2, v3, :cond_1

    .line 198
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 199
    .restart local v1    # "temp":Landroid/os/Bundle;
    const-string v2, "FEEDBACK_RESULT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 200
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    const-string v3, "send successful"

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/utils/ToastTool;->showShortToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :cond_a
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    const-string v3, "send failed"

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/utils/ToastTool;->showShortToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 108
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b004d

    if-ne v0, v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->dismiss()V

    .line 122
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->suggest:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->suggestStr:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->suggestStr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 113
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/touchus/publicutils/utils/UtilTools;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->submit()V

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    const v2, 0x7f070096

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/utils/ToastTool;->showShortToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->suggest:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    const v2, 0x7f070093

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const v2, 0x7f03000d

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->setContentView(I)V

    .line 65
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 66
    const v2, 0x7f0b004a

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->suggest:Landroid/widget/EditText;

    .line 67
    const v2, 0x7f0b004b

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->phoneNum:Landroid/widget/EditText;

    .line 68
    const v2, 0x7f0b004c

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->submitBtn:Landroid/widget/Button;

    .line 69
    const v2, 0x7f0b004d

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->cancelBtn:Landroid/widget/Button;

    .line 72
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 76
    .local v0, "dialogWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 78
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x190

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 79
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 80
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 82
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 83
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->cancelBtn:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 84
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->submitBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 86
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->mHandler:Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 91
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 92
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FeedbackDialog;->mHandler:Lcom/touchus/benchilauncher/views/FeedbackDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 97
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 98
    return-void
.end method
