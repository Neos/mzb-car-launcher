.class public final enum Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;
.super Ljava/lang/Enum;
.source "LoopRotarySwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AutoScrollDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

.field public static final enum left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

.field public static final enum right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    new-instance v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    const-string v1, "left"

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    new-instance v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    const-string v1, "right"

    invoke-direct {v0, v1, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    .line 96
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    sget-object v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ENUM$VALUES:[Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    return-object v0
.end method

.method public static values()[Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ENUM$VALUES:[Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
