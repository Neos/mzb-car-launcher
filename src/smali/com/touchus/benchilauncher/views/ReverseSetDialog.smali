.class public Lcom/touchus/benchilauncher/views/ReverseSetDialog;
.super Landroid/app/Dialog;
.source "ReverseSetDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private english:Landroid/widget/RadioButton;

.field private languageType:Landroid/widget/RadioGroup;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mHandler:Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

.field private mIDRIVERENUM:B

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private selectPos:I

.field private simplified:Landroid/widget/RadioButton;

.field private traditional:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    .line 114
    new-instance v0, Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/ReverseSetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    .line 114
    new-instance v0, Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/ReverseSetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->context:Landroid/content/Context;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/ReverseSetDialog;I)V
    .locals 0

    .prologue
    .line 22
    iput p1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/ReverseSetDialog;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    return v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/ReverseSetDialog;I)V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/ReverseSetDialog;)V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->press()V

    return-void
.end method

.method private initData()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->context:Landroid/content/Context;

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 100
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 101
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "reversingType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->simplified:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->context:Landroid/content/Context;

    const v2, 0x7f07004b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->traditional:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->context:Landroid/content/Context;

    const v2, 0x7f07004c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->english:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->context:Landroid/content/Context;

    const v2, 0x7f07004d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 108
    return-void
.end method

.method private initSetup()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/ReverseSetDialog$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog$1;-><init>(Lcom/touchus/benchilauncher/views/ReverseSetDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 87
    iget v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->seclectTrue(I)V

    .line 89
    return-void
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 92
    const v0, 0x7f0b004e

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    .line 93
    const v0, 0x7f0b004f

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->simplified:Landroid/widget/RadioButton;

    .line 94
    const v0, 0x7f0b0050

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->traditional:Landroid/widget/RadioButton;

    .line 95
    const v0, 0x7f0b0051

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->english:Landroid/widget/RadioButton;

    .line 96
    return-void
.end method

.method private press()V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iLanguageType:I

    .line 163
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "reversingType"

    iget v2, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 164
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendReverseSetToMcu(I)V

    .line 165
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->dismiss()V

    .line 166
    return-void
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 169
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 176
    return-void

    .line 170
    :cond_0
    if-ne p1, v0, :cond_1

    .line 171
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 169
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 133
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_2

    .line 134
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 135
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    .line 136
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_0

    .line 137
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_3

    .line 138
    :cond_0
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 139
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    .line 141
    :cond_1
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->seclectTrue(I)V

    .line 159
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_0
    return-void

    .line 142
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_3
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 143
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_4

    .line 144
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_6

    .line 145
    :cond_4
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    if-lez v1, :cond_5

    .line 146
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    .line 148
    :cond_5
    iget v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->selectPos:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->seclectTrue(I)V

    goto :goto_0

    .line 150
    :cond_6
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_7

    .line 151
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->press()V

    goto :goto_0

    .line 152
    :cond_7
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_8

    .line 153
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_8

    .line 154
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 155
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    .line 154
    if-ne v1, v2, :cond_2

    .line 156
    :cond_8
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->dismiss()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->setContentView(I)V

    .line 48
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 49
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->initView()V

    .line 50
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->initSetup()V

    .line 51
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->initData()V

    .line 52
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 56
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 57
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 62
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 63
    return-void
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ReverseSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/ReverseSetDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 112
    return-void
.end method
