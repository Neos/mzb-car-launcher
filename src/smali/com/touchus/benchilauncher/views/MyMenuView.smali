.class public Lcom/touchus/benchilauncher/views/MyMenuView;
.super Landroid/widget/RelativeLayout;
.source "MyMenuView.java"


# instance fields
.field private iconDrawable:Landroid/graphics/drawable/Drawable;

.field private isBig:Z

.field layoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field private menuIcon:Landroid/widget/ImageView;

.field private menuName:Ljava/lang/String;

.field private menuTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/MyMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, -0x2

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->layoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 38
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 37
    check-cast v0, Landroid/view/LayoutInflater;

    .line 39
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03003b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 41
    const v1, 0x7f0b0159

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/MyMenuView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuIcon:Landroid/widget/ImageView;

    .line 42
    const v1, 0x7f0b015a

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/MyMenuView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuTv:Landroid/widget/TextView;

    .line 66
    return-void
.end method


# virtual methods
.method public setImageBitmap(IIZ)V
    .locals 3
    .param p1, "menuName"    # I
    .param p2, "icon"    # I
    .param p3, "iBig"    # Z

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 70
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuTv:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 71
    if-eqz p3, :cond_0

    .line 72
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuTv:Landroid/widget/TextView;

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 73
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->layoutParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0xa0

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 74
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuTv:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->layoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuTv:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 77
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->layoutParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x61

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->menuTv:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/MyMenuView;->layoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
