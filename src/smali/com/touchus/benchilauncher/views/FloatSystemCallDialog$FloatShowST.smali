.class public final enum Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;
.super Ljava/lang/Enum;
.source "FloatSystemCallDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FloatShowST"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

.field private static final synthetic ENUM$VALUES:[Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

.field public static final enum HIDEN:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

.field public static final enum INCOMING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

.field public static final enum OUTCALLING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

.field public static final enum TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    const-string v1, "INCOMING"

    invoke-direct {v0, v1, v3}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->INCOMING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    const-string v1, "TALKING"

    invoke-direct {v0, v1, v4}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    const-string v1, "OUTCALLING"

    invoke-direct {v0, v1, v5}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->OUTCALLING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    const-string v1, "HIDEN"

    invoke-direct {v0, v1, v6}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->HIDEN:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    aput-object v1, v0, v2

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->INCOMING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    aput-object v1, v0, v3

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    aput-object v1, v0, v4

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->OUTCALLING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    aput-object v1, v0, v5

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->HIDEN:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    aput-object v1, v0, v6

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ENUM$VALUES:[Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    return-object v0
.end method

.method public static values()[Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ENUM$VALUES:[Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    array-length v1, v0

    new-array v2, v1, [Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
