.class public Lcom/touchus/benchilauncher/views/CycleWheelView;
.super Landroid/widget/ListView;
.source "CycleWheelView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;,
        Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewException;,
        Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;
    }
.end annotation


# static fields
.field private static final COLOR_DIVIDER_DEFALUT:I

.field private static final COLOR_SOLID_DEFAULT:I

.field private static final COLOR_SOLID_SELET_DEFAULT:I

.field private static final HEIGHT_DIVIDER_DEFAULT:I = 0x2

.field public static final TAG:Ljava/lang/String;

.field private static final WHEEL_SIZE_DEFAULT:I = 0x3


# instance fields
.field private cylceEnable:Z

.field private dividerColor:I

.field private dividerHeight:I

.field private mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

.field private mAlphaGradual:F

.field private mCurrentPositon:I

.field private mHandler:Landroid/os/Handler;

.field private mItemHeight:I

.field private mItemLabelTvId:I

.field private mItemLayoutId:I

.field private mItemSelectedListener:Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;

.field private mLabelColor:I

.field private mLabelSelectColor:I

.field private mLabels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWheelSize:I

.field private seletedSolidColor:I

.field private solidColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->TAG:Ljava/lang/String;

    .line 46
    const-string v0, "#747474"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 45
    sput v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_DIVIDER_DEFALUT:I

    .line 48
    const-string v0, "#3e4043"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_DEFAULT:I

    .line 50
    const-string v0, "#323335"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 49
    sput v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_SELET_DEFAULT:I

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelSelectColor:I

    .line 70
    const v0, -0x777778

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelColor:I

    .line 75
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAlphaGradual:F

    .line 80
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_DIVIDER_DEFALUT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerColor:I

    .line 85
    const/4 v0, 0x2

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerHeight:I

    .line 90
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_SELET_DEFAULT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->seletedSolidColor:I

    .line 95
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_DEFAULT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->solidColor:I

    .line 100
    const/4 v0, 0x3

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelSelectColor:I

    .line 70
    const v0, -0x777778

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelColor:I

    .line 75
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAlphaGradual:F

    .line 80
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_DIVIDER_DEFALUT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerColor:I

    .line 85
    const/4 v0, 0x2

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerHeight:I

    .line 90
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_SELET_DEFAULT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->seletedSolidColor:I

    .line 95
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_DEFAULT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->solidColor:I

    .line 100
    const/4 v0, 0x3

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    .line 129
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->init()V

    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 124
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelSelectColor:I

    .line 70
    const v0, -0x777778

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelColor:I

    .line 75
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAlphaGradual:F

    .line 80
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_DIVIDER_DEFALUT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerColor:I

    .line 85
    const/4 v0, 0x2

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerHeight:I

    .line 90
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_SELET_DEFAULT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->seletedSolidColor:I

    .line 95
    sget v0, Lcom/touchus/benchilauncher/views/CycleWheelView;->COLOR_SOLID_DEFAULT:I

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->solidColor:I

    .line 100
    const/4 v0, 0x3

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    .line 125
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/CycleWheelView;)Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->cylceEnable:Z

    return v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    return v0
.end method

.method static synthetic access$10(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerHeight:I

    return v0
.end method

.method static synthetic access$11(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->seletedSolidColor:I

    return v0
.end method

.method static synthetic access$12(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->solidColor:I

    return v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLayoutId:I

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLabelTvId:I

    return v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemHeight:I

    return v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/CycleWheelView;F)I
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getDistance(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/CycleWheelView;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->refreshItems()V

    return-void
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/views/CycleWheelView;I)I
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/views/CycleWheelView;I)V
    .locals 0

    .prologue
    .line 1
    invoke-super {p0, p1}, Landroid/widget/ListView;->setSelection(I)V

    return-void
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/views/CycleWheelView;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerColor:I

    return v0
.end method

.method private getDistance(F)I
    .locals 2
    .param p1, "scrollDistance"    # F

    .prologue
    .line 177
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 178
    float-to-int v0, p1

    .line 182
    :goto_0
    return v0

    .line 179
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x41400000    # 12.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 180
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, -0x2

    goto :goto_0

    .line 182
    :cond_2
    const/high16 v0, 0x40c00000    # 6.0f

    div-float v0, p1, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method private getPosition(I)I
    .locals 3
    .param p1, "positon"    # I

    .prologue
    .line 288
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 289
    :cond_0
    const/4 p1, 0x0

    .line 295
    .end local p1    # "positon":I
    :cond_1
    :goto_0
    return p1

    .line 291
    .restart local p1    # "positon":I
    :cond_2
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->cylceEnable:Z

    if-eqz v1, :cond_1

    .line 292
    const v1, 0x3fffffff    # 1.9999999f

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    div-int v0, v1, v2

    .line 293
    .local v0, "d":I
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/2addr v1, v0

    add-int/2addr p1, v1

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 137
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mHandler:Landroid/os/Handler;

    .line 138
    const v0, 0x7f03002c

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLayoutId:I

    .line 139
    const v0, 0x7f0b00fb

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLabelTvId:I

    .line 140
    new-instance v0, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;-><init>(Lcom/touchus/benchilauncher/views/CycleWheelView;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    .line 141
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setVerticalScrollBarEnabled(Z)V

    .line 142
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setScrollingCacheEnabled(Z)V

    .line 143
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setCacheColorHint(I)V

    .line 144
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setFadingEdgeLength(I)V

    .line 145
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setOverScrollMode(I)V

    .line 146
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setDividerHeight(I)V

    .line 147
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 148
    new-instance v0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/CycleWheelView$1;-><init>(Lcom/touchus/benchilauncher/views/CycleWheelView;)V

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 174
    return-void
.end method

.method private initView()V
    .locals 4

    .prologue
    .line 418
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->measureHeight()I

    move-result v2

    iput v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemHeight:I

    .line 419
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 420
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemHeight:I

    iget v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    mul-int/2addr v2, v3

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 421
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;->setData(Ljava/util/List;)V

    .line 422
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;->notifyDataSetChanged()V

    .line 423
    new-instance v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/CycleWheelView$3;-><init>(Lcom/touchus/benchilauncher/views/CycleWheelView;)V

    .line 460
    .local v0, "backgroud":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 461
    return-void
.end method

.method private measureHeight()I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 464
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 465
    iget v5, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLayoutId:I

    const/4 v6, 0x0

    .line 464
    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 466
    .local v2, "itemView":Landroid/view/View;
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    .line 467
    const/4 v5, -0x1

    .line 468
    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 466
    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 469
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 471
    .local v3, "w":I
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 473
    .local v0, "h":I
    invoke-virtual {v2, v3, v0}, Landroid/view/View;->measure(II)V

    .line 474
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 476
    .local v1, "height":I
    return v1
.end method

.method private refreshItems()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 187
    iget v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    div-int/lit8 v1, v3, 0x2

    .line 188
    .local v1, "offset":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getFirstVisiblePosition()I

    move-result v0

    .line 189
    .local v0, "firstPosition":I
    const/4 v2, 0x0

    .line 190
    .local v2, "position":I
    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemHeight:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_3

    .line 194
    add-int v2, v0, v1

    .line 198
    :goto_1
    iget v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    if-eq v2, v3, :cond_0

    .line 201
    iput v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    .line 202
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemSelectedListener:Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;

    if-eqz v3, :cond_2

    .line 203
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemSelectedListener:Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getSelection()I

    move-result v4

    .line 204
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getSelectLabel()Ljava/lang/String;

    move-result-object v5

    .line 203
    invoke-interface {v3, v4, v5}, Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;->onItemSelected(ILjava/lang/String;)V

    .line 206
    :cond_2
    invoke-direct {p0, v0, v2, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->resetItems(III)V

    goto :goto_0

    .line 196
    :cond_3
    add-int v3, v0, v1

    add-int/lit8 v2, v3, 0x1

    goto :goto_1
.end method

.method private resetItems(III)V
    .locals 10
    .param p1, "firstPosition"    # I
    .param p2, "position"    # I
    .param p3, "offset"    # I

    .prologue
    .line 210
    sub-int v6, p2, p3

    add-int/lit8 v3, v6, -0x1

    .local v3, "i":I
    :goto_0
    add-int v6, p2, p3

    add-int/lit8 v6, v6, 0x1

    if-lt v3, v6, :cond_0

    .line 226
    return-void

    .line 211
    :cond_0
    sub-int v6, v3, p1

    invoke-virtual {p0, v6}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 212
    .local v4, "itemView":Landroid/view/View;
    if-nez v4, :cond_1

    .line 210
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 215
    :cond_1
    iget v6, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLabelTvId:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 216
    .local v5, "labelTv":Landroid/widget/TextView;
    if-ne p2, v3, :cond_2

    .line 217
    iget v6, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelSelectColor:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 218
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 220
    :cond_2
    iget v6, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelColor:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    sub-int v6, v3, p2

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 222
    .local v2, "delta":I
    iget v6, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAlphaGradual:F

    float-to-double v6, v6

    int-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 223
    .local v0, "alpha":D
    double-to-float v6, v0

    invoke-virtual {v4, v6}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method


# virtual methods
.method public getLabels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    return-object v0
.end method

.method public getSelectLabel()Ljava/lang/String;
    .locals 3

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getSelection()I

    move-result v1

    .line 317
    .local v1, "position":I
    if-gez v1, :cond_0

    const/4 v1, 0x0

    .line 319
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :goto_0
    return-object v2

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    goto :goto_0
.end method

.method public getSelection()I
    .locals 2

    .prologue
    .line 304
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    if-nez v0, :cond_0

    .line 305
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    .line 307
    :cond_0
    iget v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    iget v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    return v0
.end method

.method public setAlphaGradual(F)V
    .locals 3
    .param p1, "alphaGradual"    # F

    .prologue
    .line 368
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAlphaGradual:F

    .line 369
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getFirstVisiblePosition()I

    move-result v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    iget v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->resetItems(III)V

    .line 370
    return-void
.end method

.method public setCycleEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->cylceEnable:Z

    if-eq v0, p1, :cond_0

    .line 268
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->cylceEnable:Z

    .line 269
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;->notifyDataSetChanged()V

    .line 270
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getSelection()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setSelection(I)V

    .line 272
    :cond_0
    return-void
.end method

.method public setDivider(II)V
    .locals 0
    .param p1, "dividerColor"    # I
    .param p2, "dividerHeight"    # I

    .prologue
    .line 412
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerColor:I

    .line 413
    iput p2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->dividerHeight:I

    .line 414
    return-void
.end method

.method public setLabelColor(I)V
    .locals 3
    .param p1, "labelColor"    # I

    .prologue
    .line 348
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelColor:I

    .line 349
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getFirstVisiblePosition()I

    move-result v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    iget v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->resetItems(III)V

    .line 350
    return-void
.end method

.method public setLabelSelectColor(I)V
    .locals 3
    .param p1, "labelSelectColor"    # I

    .prologue
    .line 358
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabelSelectColor:I

    .line 359
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getFirstVisiblePosition()I

    move-result v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mCurrentPositon:I

    iget v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->resetItems(III)V

    .line 360
    return-void
.end method

.method public setLabels(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234
    .local p1, "labels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    .line 235
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;->setData(Ljava/util/List;)V

    .line 236
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;->notifyDataSetChanged()V

    .line 237
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->initView()V

    .line 238
    return-void
.end method

.method public setOnWheelItemSelectedListener(Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;)V
    .locals 0
    .param p1, "mItemSelectedListener"    # Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemSelectedListener:Lcom/touchus/benchilauncher/views/CycleWheelView$WheelItemSelectedListener;

    .line 248
    return-void
.end method

.method public setSelection(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 279
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/touchus/benchilauncher/views/CycleWheelView$2;

    invoke-direct {v1, p0, p1}, Lcom/touchus/benchilauncher/views/CycleWheelView$2;-><init>(Lcom/touchus/benchilauncher/views/CycleWheelView;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 285
    return-void
.end method

.method public setSolid(II)V
    .locals 0
    .param p1, "unselectedSolidColor"    # I
    .param p2, "selectedSolidColor"    # I

    .prologue
    .line 398
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->solidColor:I

    .line 399
    iput p2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->seletedSolidColor:I

    .line 400
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->initView()V

    .line 401
    return-void
.end method

.method public setWheelItemLayout(II)V
    .locals 2
    .param p1, "itemResId"    # I
    .param p2, "labelTvId"    # I

    .prologue
    .line 334
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLayoutId:I

    .line 335
    iput p2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mItemLabelTvId:I

    .line 336
    new-instance v0, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;-><init>(Lcom/touchus/benchilauncher/views/CycleWheelView;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    .line 337
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mLabels:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;->setData(Ljava/util/List;)V

    .line 338
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mAdapter:Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewAdapter;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 339
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->initView()V

    .line 340
    return-void
.end method

.method public setWheelSize(I)V
    .locals 2
    .param p1, "wheelSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewException;
        }
    .end annotation

    .prologue
    .line 380
    const/4 v0, 0x3

    if-lt p1, v0, :cond_0

    rem-int/lit8 v0, p1, 0x2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 381
    :cond_0
    new-instance v0, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewException;

    .line 382
    const-string v1, "Wheel Size Error , Must Be 3,5,7,9..."

    .line 381
    invoke-direct {v0, p0, v1}, Lcom/touchus/benchilauncher/views/CycleWheelView$CycleWheelViewException;-><init>(Lcom/touchus/benchilauncher/views/CycleWheelView;Ljava/lang/String;)V

    throw v0

    .line 384
    :cond_1
    iput p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView;->mWheelSize:I

    .line 385
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->initView()V

    .line 387
    return-void
.end method
