.class public Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
.super Landroid/widget/RelativeLayout;
.source "LoopRotarySwitchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;,
        Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection:[I = null

.field private static final LoopR:I = 0xc8

.field private static final RADIO_DEFAULT_CHILD_DIMENSION:F = 0.25f

.field private static final horizontal:I = 0x1

.field private static final vertical:I


# instance fields
.field private RADIO_DEFAULT_CENTERITEM_DIMENSION:F

.field private angle:F

.field private autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

.field private autoRotation:Z

.field protected clickRotation:Z

.field private distance:F

.field private isCanClickListener:Z

.field private isFirst:Z

.field private last_angle:F

.field private limitX:F

.field loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

.field private loopRotationX:I

.field private loopRotationZ:I

.field private mContext:Landroid/content/Context;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mOrientation:I

.field private multiple:F

.field private onItemClickListener:Lcom/touchus/benchilauncher/inface/OnItemClickListener;

.field private onItemSelectedListener:Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

.field private onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

.field private r:F

.field private rAnimation:Landroid/animation/ValueAnimator;

.field private restAnimator:Landroid/animation/ValueAnimator;

.field private selectItem:I

.field private size:I

.field private textViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private texts:[Ljava/lang/String;

.field private touching:Z

.field private views:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private x:F

.field private xAnimation:Landroid/animation/ValueAnimator;

.field private zAnimation:Landroid/animation/ValueAnimator;


# direct methods
.method static synthetic $SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection()[I
    .locals 3

    .prologue
    .line 30
    sget-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->values()[Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/high16 v8, 0x43480000    # 200.0f

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 128
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mOrientation:I

    .line 42
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    .line 44
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    .line 46
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    .line 48
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    .line 50
    iput v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationX:I

    iput v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    .line 52
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 54
    iput v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->selectItem:I

    .line 56
    const/4 v2, 0x5

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    .line 59
    iput v8, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    .line 61
    const/high16 v2, 0x40000000    # 2.0f

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->multiple:F

    .line 63
    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->multiple:F

    iget v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->distance:F

    .line 66
    iput v7, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    .line 68
    iput v7, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    .line 70
    iput-boolean v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->clickRotation:Z

    .line 72
    iput-boolean v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    .line 74
    iput-boolean v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->touching:Z

    .line 76
    iput-boolean v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isFirst:Z

    .line 78
    sget-object v2, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    .line 80
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    .line 82
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemSelectedListener:Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    .line 84
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    .line 86
    iput-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemClickListener:Lcom/touchus/benchilauncher/inface/OnItemClickListener;

    .line 88
    iput-boolean v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener:Z

    .line 92
    const/high16 v2, 0x41f00000    # 30.0f

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->limitX:F

    .line 94
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->RADIO_DEFAULT_CENTERITEM_DIMENSION:F

    .line 157
    new-instance v2, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;

    .line 158
    const/16 v3, 0xbb8

    invoke-direct {v2, p0, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    .line 385
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    .line 129
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mContext:Landroid/content/Context;

    .line 131
    sget-object v2, Lcom/touchus/benchilauncher/R$styleable;->LoopRotarySwitchView:[I

    .line 130
    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 132
    .local v1, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v4, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mOrientation:I

    .line 134
    invoke-virtual {v1, v6, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    .line 136
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    .line 138
    const/4 v2, 0x3

    .line 137
    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 139
    .local v0, "direction":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 140
    new-instance v2, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getGeomeryController()Landroid/view/GestureDetector$SimpleOnGestureListener;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 141
    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mOrientation:I

    if-ne v2, v6, :cond_0

    .line 142
    iput v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    .line 146
    :goto_0
    if-nez v0, :cond_1

    .line 147
    sget-object v2, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    .line 151
    :goto_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    iget-boolean v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->setLoop(Z)V

    .line 152
    return-void

    .line 144
    :cond_0
    const/16 v2, 0x5a

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    goto :goto_0

    .line 149
    :cond_1
    sget-object v2, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    goto :goto_1
.end method

.method private AnimRotationTo(FLjava/lang/Runnable;)V
    .locals 4
    .param p1, "finall"    # F
    .param p2, "complete"    # Ljava/lang/Runnable;

    .prologue
    .line 530
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 533
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    aput v2, v0, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    .line 534
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 535
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 537
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    .line 538
    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$5;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$5;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 547
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 584
    if-eqz p2, :cond_1

    .line 585
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$7;

    invoke-direct {v1, p0, p2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$7;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 607
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    return v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    return-object v0
.end method

.method static synthetic access$10(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I
    .locals 1

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->calculateItem()I

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)I
    .locals 1

    .prologue
    .line 473
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->calculateItemPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener:Z

    return v0
.end method

.method static synthetic access$13(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/inface/OnItemClickListener;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemClickListener:Lcom/touchus/benchilauncher/inface/OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$14(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->touching:Z

    return v0
.end method

.method static synthetic access$15(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->selectItem:I

    return-void
.end method

.method static synthetic access$16(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->selectItem:I

    return v0
.end method

.method static synthetic access$17(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemSelectedListener:Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$18(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationX:I

    return-void
.end method

.method static synthetic access$19(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)F
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;F)V
    .locals 0

    .prologue
    .line 66
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;FLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 529
    invoke-direct {p0, p1, p2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->AnimRotationTo(FLjava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    return v0
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->initView()V

    return-void
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;F)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    return-void
.end method

.method private calculateItem()I
    .locals 3

    .prologue
    .line 616
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    const/16 v1, 0x168

    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    rem-int/2addr v0, v1

    return v0
.end method

.method private calculateItemPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 474
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->calculateItem()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 490
    :goto_0
    return p1

    .line 476
    :pswitch_0
    const/4 p1, 0x4

    .line 477
    goto :goto_0

    .line 479
    :pswitch_1
    const/4 p1, 0x3

    .line 480
    goto :goto_0

    .line 482
    :pswitch_2
    const/4 p1, 0x2

    .line 483
    goto :goto_0

    .line 485
    :pswitch_3
    const/4 p1, 0x1

    .line 486
    goto :goto_0

    .line 474
    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private initView()V
    .locals 14

    .prologue
    .line 269
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lt v1, v10, :cond_0

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v0, "arrayViewList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 289
    const/4 v1, 0x0

    :goto_1
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lt v1, v10, :cond_1

    .line 292
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->sortList(Ljava/util/List;)V

    .line 293
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->postInvalidate()V

    .line 294
    return-void

    .line 270
    .end local v0    # "arrayViewList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_0
    iget v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    const/high16 v11, 0x43340000    # 180.0f

    add-float/2addr v10, v11

    mul-int/lit16 v11, v1, 0x168

    iget v12, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int/2addr v11, v12

    int-to-float v11, v11

    sub-float/2addr v10, v11

    float-to-double v2, v10

    .line 271
    .local v2, "radians":D
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v10, v10

    iget v11, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    mul-float v8, v10, v11

    .line 272
    .local v8, "x0":F
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v10, v10

    iget v11, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    mul-float v9, v10, v11

    .line 273
    .local v9, "y0":F
    iget v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->distance:F

    sub-float/2addr v10, v9

    iget v11, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->distance:F

    iget v12, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    add-float/2addr v11, v12

    div-float v7, v10, v11

    .line 275
    .local v7, "scale0":F
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, v7}, Landroid/view/View;->setScaleX(F)V

    .line 276
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-virtual {v10, v7}, Landroid/view/View;->setScaleY(F)V

    .line 277
    iget v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationX:I

    int-to-double v10, v10

    .line 278
    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    .line 277
    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v10, v10

    .line 279
    iget v11, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    .line 277
    mul-float v4, v10, v11

    .line 281
    .local v4, "rotationX_y":F
    iget v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    neg-int v10, v10

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    .line 280
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v10, v10

    neg-float v10, v10

    mul-float v6, v10, v8

    .line 283
    .local v6, "rotationZ_y":F
    iget v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    neg-int v10, v10

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    .line 282
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v10, v10

    mul-float/2addr v10, v8

    sub-float v5, v10, v8

    .line 284
    .local v5, "rotationZ_x":F
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    add-float v11, v8, v5

    invoke-virtual {v10, v11}, Landroid/view/View;->setTranslationX(F)V

    .line 285
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    add-float v11, v4, v6

    invoke-virtual {v10, v11}, Landroid/view/View;->setTranslationY(F)V

    .line 269
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 290
    .end local v2    # "radians":D
    .end local v4    # "rotationX_y":F
    .end local v5    # "rotationZ_x":F
    .end local v6    # "rotationZ_y":F
    .end local v7    # "scale0":F
    .end local v8    # "x0":F
    .end local v9    # "y0":F
    .restart local v0    # "arrayViewList":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_1
    iget-object v10, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method private onTouch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 626
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 627
    iget v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iput v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    .line 628
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->touching:Z

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 631
    .local v0, "sc":Z
    if-eqz v0, :cond_1

    .line 632
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 634
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v3, :cond_2

    .line 635
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 636
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->touching:Z

    .line 637
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restPosition()V

    .line 640
    :cond_3
    return v3
.end method

.method private sortList(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$SortComparator;)V

    .line 219
    .local v1, "comparator":Ljava/util/Comparator;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 221
    .local v0, "array":[Ljava/lang/Object;
    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 222
    const/4 v2, 0x0

    .line 223
    .local v2, "i":I
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v4

    .line 224
    .local v4, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<TT;>;"
    :goto_0
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 228
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-lt v5, v6, :cond_1

    .line 231
    return-void

    .line 225
    .end local v5    # "j":I
    :cond_0
    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 226
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aget-object v6, v0, v2

    invoke-interface {v4, v6}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 229
    .restart local v5    # "j":I
    :cond_1
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->bringToFront()V

    .line 228
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method


# virtual methods
.method public RAnimation()V
    .locals 2

    .prologue
    .line 348
    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->RAnimation(FF)V

    .line 349
    return-void
.end method

.method public RAnimation(FF)V
    .locals 4
    .param p1, "from"    # F
    .param p2, "to"    # F

    .prologue
    .line 360
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    .line 361
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    .line 362
    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$3;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 369
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 370
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 371
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 372
    return-void
.end method

.method public RAnimation(Z)V
    .locals 2
    .param p1, "fromZeroToLoopR"    # Z

    .prologue
    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 352
    if-eqz p1, :cond_0

    .line 353
    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->RAnimation(FF)V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->RAnimation(FF)V

    goto :goto_0
.end method

.method public checkChildView()V
    .locals 6

    .prologue
    .line 436
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_0

    .line 439
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 440
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getChildCount()I

    move-result v0

    .line 441
    .local v0, "count":I
    iput v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    .line 442
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v0, :cond_1

    .line 471
    return-void

    .line 437
    .end local v0    # "count":I
    :cond_0
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 436
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 443
    .restart local v0    # "count":I
    :cond_1
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 444
    .local v4, "view":Landroid/view/View;
    const/high16 v5, 0x43250000    # 165.0f

    invoke-virtual {v4, v5}, Landroid/view/View;->setPivotY(F)V

    .line 445
    const/high16 v5, 0x42c80000    # 100.0f

    invoke-virtual {v4, v5}, Landroid/view/View;->setPivotX(F)V

    .line 446
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    array-length v5, v5

    if-ne v5, v0, :cond_2

    .line 447
    const v5, 0x7f0b0105

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 448
    .local v3, "textView":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    .end local v3    # "textView":Landroid/widget/TextView;
    :cond_2
    move v2, v1

    .line 453
    .local v2, "position":I
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    new-instance v5, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;

    invoke-direct {v5, p0, v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$4;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 442
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public createXAnimation(IIZ)V
    .locals 4
    .param p1, "from"    # I
    .param p2, "to"    # I
    .param p3, "start"    # Z

    .prologue
    .line 958
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 959
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 961
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    .line 962
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    .line 963
    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$8;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$8;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 970
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 971
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 972
    if-eqz p3, :cond_1

    .line 973
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 974
    :cond_1
    return-void
.end method

.method public createZAnimation(IIZ)Landroid/animation/ValueAnimator;
    .locals 4
    .param p1, "from"    # I
    .param p2, "to"    # I
    .param p3, "start"    # Z

    .prologue
    .line 977
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 980
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    .line 981
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    .line 982
    new-instance v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$9;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$9;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 989
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 990
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 991
    if-eqz p3, :cond_1

    .line 992
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 993
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 663
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onTouch(Landroid/view/MotionEvent;)Z

    .line 664
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    invoke-interface {v0, p1}, Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;->onTouch(Landroid/view/MotionEvent;)V

    .line 667
    :cond_0
    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener(Landroid/view/MotionEvent;)V

    .line 668
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public doRotain()V
    .locals 4

    .prologue
    .line 184
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->clickRotation:Z

    if-eqz v2, :cond_1

    .line 185
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->clickRotation:Z

    .line 187
    :try_start_0
    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    if-eqz v2, :cond_1

    .line 188
    const/4 v1, 0x0

    .line 189
    .local v1, "perAngle":I
    invoke-static {}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection()[I

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 197
    :goto_0
    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    const/high16 v3, 0x43b40000    # 360.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 198
    const/4 v2, 0x0

    iput v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    .line 200
    :cond_0
    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    int-to-float v3, v1

    add-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->AnimRotationTo(FLjava/lang/Runnable;)V

    .line 207
    .end local v1    # "perAngle":I
    :cond_1
    :goto_1
    return-void

    .line 191
    .restart local v1    # "perAngle":I
    :pswitch_0
    const/16 v2, 0x168

    iget v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v1, v2, v3

    .line 192
    goto :goto_0

    .line 194
    :pswitch_1
    const/16 v2, -0x168

    iget v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v1, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 202
    .end local v1    # "perAngle":I
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 204
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->clickRotation:Z

    goto :goto_1

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getAngle()F
    .locals 1

    .prologue
    .line 718
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    return v0
.end method

.method public getAutoRotationTime()J
    .locals 2

    .prologue
    .line 917
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    iget-wide v0, v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->loopTime:J

    return-wide v0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 736
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->distance:F

    return v0
.end method

.method public getGeomeryController()Landroid/view/GestureDetector$SimpleOnGestureListener;
    .locals 1

    .prologue
    .line 254
    new-instance v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;-><init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    return-object v0
.end method

.method public getLoopRotationX()I
    .locals 1

    .prologue
    .line 1036
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationX:I

    return v0
.end method

.method public getLoopRotationZ()I
    .locals 1

    .prologue
    .line 1040
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    return v0
.end method

.method public getR()F
    .locals 1

    .prologue
    .line 754
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    return v0
.end method

.method public getRestAnimator()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->restAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method public getSelectItem()I
    .locals 1

    .prologue
    .line 763
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->selectItem:I

    return v0
.end method

.method public getTextViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    return-object v0
.end method

.method public getViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    return-object v0
.end method

.method public getrAnimation()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->rAnimation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method public getxAnimation()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method public getzAnimation()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method public isAutoRotation()Z
    .locals 1

    .prologue
    .line 936
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    return v0
.end method

.method public isCanClickListener(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x3e8

    .line 677
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 701
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 679
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->x:F

    .line 680
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    invoke-virtual {v0, v4}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->removeMessages(I)V

    goto :goto_0

    .line 688
    :pswitch_2
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    if-eqz v0, :cond_1

    .line 689
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    .line 692
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    iget-wide v2, v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->loopTime:J

    .line 690
    invoke-virtual {v0, v4, v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 694
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->x:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->limitX:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->limitX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 695
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener:Z

    goto :goto_0

    .line 697
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener:Z

    goto :goto_0

    .line 677
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v1, 0x1

    .line 308
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 309
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isFirst:Z

    if-nez v0, :cond_1

    .line 310
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isFirst:Z

    .line 311
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->checkChildView()V

    .line 312
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemSelectedListener:Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 313
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener:Z

    .line 314
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemSelectedListener:Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    iget v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->selectItem:I

    .line 315
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    iget v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->selectItem:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 314
    invoke-interface {v1, v2, v0}, Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;->selected(ILandroid/view/View;)V

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->RAnimation()V

    .line 321
    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 298
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 299
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->initView()V

    .line 300
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    .line 302
    const/16 v1, 0x3e8

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    iget-wide v2, v2, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->loopTime:J

    .line 301
    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 304
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 651
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    invoke-interface {v0, p1}, Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;->onTouch(Landroid/view/MotionEvent;)V

    .line 654
    :cond_0
    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->isCanClickListener(Landroid/view/MotionEvent;)V

    .line 655
    const/4 v0, 0x1

    return v0
.end method

.method public removeview(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 428
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 429
    return-void
.end method

.method public restPosition()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 497
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    if-nez v4, :cond_0

    .line 521
    :goto_0
    return-void

    .line 500
    :cond_0
    const/4 v0, 0x0

    .line 501
    .local v0, "finall":F
    const/16 v4, 0x168

    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int/2addr v4, v5

    int-to-float v3, v4

    .line 502
    .local v3, "part":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_1

    .line 503
    neg-float v3, v3

    .line 505
    :cond_1
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    div-float/2addr v4, v3

    float-to-int v4, v4

    int-to-float v4, v4

    mul-float v2, v4, v3

    .line 506
    .local v2, "minvalue":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    div-float/2addr v4, v3

    float-to-int v4, v4

    int-to-float v4, v4

    mul-float/2addr v4, v3

    add-float v1, v4, v3

    .line 507
    .local v1, "maxvalue":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    cmpl-float v4, v4, v6

    if-ltz v4, :cond_3

    .line 508
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    sub-float/2addr v4, v5

    cmpl-float v4, v4, v6

    if-lez v4, :cond_2

    .line 509
    move v0, v1

    .line 520
    :goto_1
    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->AnimRotationTo(FLjava/lang/Runnable;)V

    goto :goto_0

    .line 511
    :cond_2
    move v0, v2

    .line 513
    goto :goto_1

    .line 514
    :cond_3
    iget v4, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    sub-float/2addr v4, v5

    cmpg-float v4, v4, v6

    if-gez v4, :cond_4

    .line 515
    move v0, v1

    .line 516
    goto :goto_1

    .line 517
    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public setAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 727
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    .line 728
    return-void
.end method

.method public setAutoRotation(Z)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 1
    .param p1, "autoRotation"    # Z

    .prologue
    .line 906
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotation:Z

    .line 907
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->setLoop(Z)V

    .line 908
    return-object p0
.end method

.method public setAutoRotationTime(J)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 1
    .param p1, "autoRotationTime"    # J

    .prologue
    .line 926
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopHandler:Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;

    invoke-virtual {v0, p1, p2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;->setLoopTime(J)V

    .line 927
    return-object p0
.end method

.method public setAutoScrollDirection(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 0
    .param p1, "mAutoScrollDirection"    # Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    .prologue
    .line 953
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->autoRotatinDirection:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    .line 954
    return-object p0
.end method

.method public setChildViewText([Ljava/lang/String;)V
    .locals 0
    .param p1, "texts"    # [Ljava/lang/String;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    .line 383
    return-void
.end method

.method public setDistance(F)V
    .locals 0
    .param p1, "distance"    # F

    .prologue
    .line 745
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->distance:F

    .line 746
    return-void
.end method

.method public setHorizontal(ZZ)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 4
    .param p1, "horizontal"    # Z
    .param p2, "anim"    # Z

    .prologue
    const/16 v3, 0x5a

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1008
    if-eqz p2, :cond_1

    .line 1009
    if-eqz p1, :cond_0

    .line 1010
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getLoopRotationZ()I

    move-result v0

    invoke-virtual {p0, v0, v1, v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->createZAnimation(IIZ)Landroid/animation/ValueAnimator;

    .line 1022
    :goto_0
    return-object p0

    .line 1012
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getLoopRotationZ()I

    move-result v0

    invoke-virtual {p0, v0, v3, v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->createZAnimation(IIZ)Landroid/animation/ValueAnimator;

    goto :goto_0

    .line 1015
    :cond_1
    if-eqz p1, :cond_2

    .line 1016
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setLoopRotationZ(I)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 1020
    :goto_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->initView()V

    goto :goto_0

    .line 1018
    :cond_2
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setLoopRotationZ(I)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    goto :goto_1
.end method

.method public setLoopRotationX(I)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 0
    .param p1, "loopRotationX"    # I

    .prologue
    .line 1026
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationX:I

    .line 1027
    return-object p0
.end method

.method public setLoopRotationZ(I)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 0
    .param p1, "loopRotationZ"    # I

    .prologue
    .line 1031
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->loopRotationZ:I

    .line 1032
    return-object p0
.end method

.method public setMultiple(F)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 0
    .param p1, "mMultiple"    # F

    .prologue
    .line 947
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->multiple:F

    .line 948
    return-object p0
.end method

.method public setOnItemClickListener(Lcom/touchus/benchilauncher/inface/OnItemClickListener;)V
    .locals 0
    .param p1, "onItemClickListener"    # Lcom/touchus/benchilauncher/inface/OnItemClickListener;

    .prologue
    .line 887
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemClickListener:Lcom/touchus/benchilauncher/inface/OnItemClickListener;

    .line 888
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;)V
    .locals 0
    .param p1, "onItemSelectedListener"    # Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    .prologue
    .line 878
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onItemSelectedListener:Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    .line 879
    return-void
.end method

.method public setOnLoopViewTouchListener(Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;)V
    .locals 0
    .param p1, "onLoopViewTouchListener"    # Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    .prologue
    .line 897
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->onLoopViewTouchListener:Lcom/touchus/benchilauncher/inface/OnLoopViewTouchListener;

    .line 898
    return-void
.end method

.method public setOrientation(I)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 2
    .param p1, "mOrientation"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1003
    if-ne p1, v0, :cond_0

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->setHorizontal(ZZ)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 1004
    return-object p0

    :cond_0
    move v0, v1

    .line 1003
    goto :goto_0
.end method

.method public setR(F)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
    .locals 1
    .param p1, "r"    # F

    .prologue
    .line 866
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->r:F

    .line 867
    iget v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->multiple:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->distance:F

    .line 868
    return-object p0
.end method

.method public setSelectItem(I)V
    .locals 10
    .param p1, "selectItem"    # I

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x168

    .line 807
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getSelectItem()I

    move-result v5

    if-ne p1, v5, :cond_1

    .line 858
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    if-ltz p1, :cond_0

    .line 811
    const/4 v1, 0x0

    .line 812
    .local v1, "jiaodu":F
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getSelectItem()I

    move-result v5

    if-nez v5, :cond_4

    .line 813
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne p1, v5, :cond_3

    .line 814
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    int-to-float v6, v6

    sub-float v1, v5, v6

    .line 815
    const-string v5, "iii"

    new-instance v6, Ljava/lang/StringBuilder;

    iget v7, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    :goto_1
    const/4 v0, 0x0

    .line 835
    .local v0, "finall":F
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v5, v8, v5

    int-to-float v4, v5

    .line 836
    .local v4, "part":F
    cmpg-float v5, v1, v9

    if-gez v5, :cond_2

    .line 837
    neg-float v4, v4

    .line 839
    :cond_2
    div-float v5, v1, v4

    float-to-int v5, v5

    int-to-float v5, v5

    mul-float v3, v5, v4

    .line 840
    .local v3, "minvalue":F
    div-float v5, v1, v4

    float-to-int v5, v5

    int-to-float v5, v5

    mul-float v2, v5, v4

    .line 841
    .local v2, "maxvalue":F
    cmpl-float v5, v1, v9

    if-ltz v5, :cond_9

    .line 842
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    sub-float v5, v1, v5

    cmpl-float v5, v5, v9

    if-lez v5, :cond_8

    .line 843
    move v0, v2

    .line 855
    :goto_2
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    if-lez v5, :cond_0

    .line 856
    const/4 v5, 0x0

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->AnimRotationTo(FLjava/lang/Runnable;)V

    goto :goto_0

    .line 817
    .end local v0    # "finall":F
    .end local v2    # "maxvalue":F
    .end local v3    # "minvalue":F
    .end local v4    # "part":F
    :cond_3
    const-string v5, "iii"

    new-instance v6, Ljava/lang/StringBuilder;

    iget v7, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    int-to-float v6, v6

    add-float v1, v5, v6

    .line 820
    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getSelectItem()I

    move-result v5

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->views:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne v5, v6, :cond_6

    .line 821
    if-nez p1, :cond_5

    .line 822
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    int-to-float v6, v6

    add-float v1, v5, v6

    .line 823
    goto :goto_1

    .line 824
    :cond_5
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    int-to-float v6, v6

    sub-float v1, v5, v6

    .line 826
    goto :goto_1

    .line 827
    :cond_6
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getSelectItem()I

    move-result v5

    if-le p1, v5, :cond_7

    .line 828
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    int-to-float v6, v6

    add-float v1, v5, v6

    .line 829
    goto :goto_1

    .line 830
    :cond_7
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    int-to-float v6, v6

    sub-float v1, v5, v6

    goto/16 :goto_1

    .line 845
    .restart local v0    # "finall":F
    .restart local v2    # "maxvalue":F
    .restart local v3    # "minvalue":F
    .restart local v4    # "part":F
    :cond_8
    move v0, v3

    .line 847
    goto :goto_2

    .line 848
    :cond_9
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    sub-float v5, v1, v5

    cmpg-float v5, v5, v9

    if-gez v5, :cond_a

    .line 849
    move v0, v2

    .line 850
    goto :goto_2

    .line 851
    :cond_a
    move v0, v3

    goto :goto_2
.end method

.method public setSelectItemm(I)V
    .locals 9
    .param p1, "selectItem"    # I

    .prologue
    const/16 v8, 0x168

    const/4 v7, 0x0

    .line 776
    if-ltz p1, :cond_1

    .line 777
    const/4 v1, 0x0

    .line 778
    .local v1, "jiaodu":F
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->angle:F

    iget v6, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v6, v8, v6

    mul-int/2addr v6, p1

    int-to-float v6, v6

    add-float v1, v5, v6

    .line 780
    const/4 v0, 0x0

    .line 781
    .local v0, "finall":F
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    div-int v5, v8, v5

    int-to-float v4, v5

    .line 782
    .local v4, "part":F
    cmpg-float v5, v1, v7

    if-gez v5, :cond_0

    .line 783
    neg-float v4, v4

    .line 785
    :cond_0
    div-float v5, v1, v4

    float-to-int v5, v5

    int-to-float v5, v5

    mul-float v3, v5, v4

    .line 786
    .local v3, "minvalue":F
    div-float v5, v1, v4

    float-to-int v5, v5

    int-to-float v5, v5

    mul-float v2, v5, v4

    .line 787
    .local v2, "maxvalue":F
    cmpl-float v5, v1, v7

    if-ltz v5, :cond_3

    .line 788
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    sub-float v5, v1, v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_2

    .line 789
    move v0, v2

    .line 801
    :goto_0
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    if-lez v5, :cond_1

    .line 802
    const/4 v5, 0x0

    invoke-direct {p0, v0, v5}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->AnimRotationTo(FLjava/lang/Runnable;)V

    .line 804
    .end local v0    # "finall":F
    .end local v1    # "jiaodu":F
    .end local v2    # "maxvalue":F
    .end local v3    # "minvalue":F
    .end local v4    # "part":F
    :cond_1
    return-void

    .line 791
    .restart local v0    # "finall":F
    .restart local v1    # "jiaodu":F
    .restart local v2    # "maxvalue":F
    .restart local v3    # "minvalue":F
    .restart local v4    # "part":F
    :cond_2
    move v0, v3

    .line 793
    goto :goto_0

    .line 794
    :cond_3
    iget v5, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->last_angle:F

    sub-float v5, v1, v5

    cmpg-float v5, v5, v7

    if-gez v5, :cond_4

    .line 795
    move v0, v2

    .line 796
    goto :goto_0

    .line 797
    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size1"    # I

    .prologue
    .line 772
    iput p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->size:I

    .line 773
    return-void
.end method

.method public setTextViewText()V
    .locals 4

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getChildCount()I

    move-result v0

    .line 393
    .local v0, "count":I
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    if-nez v2, :cond_1

    .line 425
    :cond_0
    return-void

    .line 396
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    array-length v2, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 399
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 400
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    array-length v2, v2

    if-ne v2, v0, :cond_2

    .line 401
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 404
    packed-switch v1, :pswitch_data_0

    .line 399
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 406
    :pswitch_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 409
    :pswitch_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 412
    :pswitch_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 415
    :pswitch_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 418
    :pswitch_4
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->textViews:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->texts:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setxAnimation(Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p1, "xAnimation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 1060
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->xAnimation:Landroid/animation/ValueAnimator;

    .line 1061
    return-void
.end method

.method public setzAnimation(Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p1, "zAnimation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 1052
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->zAnimation:Landroid/animation/ValueAnimator;

    .line 1053
    return-void
.end method
