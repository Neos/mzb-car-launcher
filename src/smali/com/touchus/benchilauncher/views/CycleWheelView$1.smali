.class Lcom/touchus/benchilauncher/views/CycleWheelView$1;
.super Ljava/lang/Object;
.source "CycleWheelView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/CycleWheelView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/CycleWheelView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$6(Lcom/touchus/benchilauncher/views/CycleWheelView;)V

    .line 172
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 6
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    const/16 v5, 0x32

    .line 151
    if-nez p2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 153
    .local v1, "itemView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 154
    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v0

    .line 155
    .local v0, "deltaY":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-nez v2, :cond_1

    .line 166
    .end local v0    # "deltaY":F
    .end local v1    # "itemView":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 158
    .restart local v0    # "deltaY":F
    .restart local v1    # "itemView":Landroid/view/View;
    :cond_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 159
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v3, v0}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$5(Lcom/touchus/benchilauncher/views/CycleWheelView;F)I

    move-result v3

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/views/CycleWheelView;->smoothScrollBy(II)V

    goto :goto_0

    .line 161
    :cond_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$1;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v4}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v0

    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$5(Lcom/touchus/benchilauncher/views/CycleWheelView;F)I

    move-result v3

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/views/CycleWheelView;->smoothScrollBy(II)V

    goto :goto_0
.end method
