.class public Lcom/touchus/benchilauncher/views/AppSettingDialog;
.super Landroid/app/Dialog;
.source "AppSettingDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Dialog;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private appInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field currentSelectIndex:I

.field private mAdapter:Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;

.field public mHandler:Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    .line 119
    new-instance v0, Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/AppSettingDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mHandler:Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    .line 119
    new-instance v0, Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/AppSettingDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mHandler:Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    .line 46
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    .line 47
    return-void
.end method

.method private initData()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 64
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 65
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 66
    invoke-virtual {v6, v8}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v4

    .line 68
    .local v4, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lt v1, v6, :cond_0

    .line 85
    const/4 v1, 0x0

    :goto_1
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v1, v6, :cond_2

    .line 90
    return-void

    .line 69
    :cond_0
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 70
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 71
    .local v0, "curPkg":Ljava/lang/String;
    invoke-static {v0}, Lcom/touchus/publicutils/utils/APPSettings;->isNavgation(Ljava/lang/String;)Z

    move-result v2

    .line 72
    .local v2, "navgation":Z
    if-eqz v2, :cond_1

    .line 73
    new-instance v5, Lcom/touchus/benchilauncher/bean/AppInfo;

    invoke-direct {v5}, Lcom/touchus/benchilauncher/bean/AppInfo;-><init>()V

    .line 74
    .local v5, "tmpInfo":Lcom/touchus/benchilauncher/bean/AppInfo;
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 75
    iget-object v7, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 74
    invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 75
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 74
    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->setAppName(Ljava/lang/String;)V

    .line 76
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 77
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->setVersionName(Ljava/lang/String;)V

    .line 78
    iget v6, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->setVersionCode(I)V

    .line 79
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    .line 80
    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 79
    invoke-virtual {v6, v7}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->setAppIcon(Landroid/graphics/drawable/Drawable;)V

    .line 81
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v6}, Lcom/touchus/benchilauncher/LauncherApplication;->getNaviAPP()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->setiCheck(Z)V

    .line 82
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    .end local v5    # "tmpInfo":Lcom/touchus/benchilauncher/bean/AppInfo;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "curPkg":Ljava/lang/String;
    .end local v2    # "navgation":Z
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/touchus/benchilauncher/bean/AppInfo;

    invoke-virtual {v6}, Lcom/touchus/benchilauncher/bean/AppInfo;->isiCheck()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 87
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-static {v6, v8, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 85
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private pressItem(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    .line 115
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/bean/AppInfo;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/bean/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->setNaviAPP(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mAdapter:Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->setSeclectIndex(I)V

    .line 117
    return-void
.end method

.method private seclectTrue(I)V
    .locals 1
    .param p1, "currentSelectIndex"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mAdapter:Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->setSeclectIndex(I)V

    .line 165
    return-void
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 138
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_2

    .line 139
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 140
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 141
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_0

    .line 142
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_3

    .line 143
    :cond_0
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    .line 144
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    .line 146
    :cond_1
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->seclectTrue(I)V

    .line 161
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_0
    return-void

    .line 147
    .restart local v0    # "idriverCode":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_3
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_4

    .line 148
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_6

    .line 149
    :cond_4
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    if-lez v2, :cond_5

    .line 150
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    .line 152
    :cond_5
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->seclectTrue(I)V

    goto :goto_0

    .line 153
    :cond_6
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_7

    .line 154
    iget v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->pressItem(I)V

    goto :goto_0

    .line 155
    :cond_7
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_8

    .line 156
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_8

    .line 157
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_2

    .line 158
    :cond_8
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->dismiss()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->setContentView(I)V

    .line 52
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 53
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->initData()V

    .line 54
    const v0, 0x7f0b001c

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mListView:Landroid/widget/ListView;

    .line 55
    new-instance v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->appInfos:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;-><init>(Lcom/touchus/benchilauncher/LauncherApplication;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mAdapter:Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;

    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mAdapter:Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 58
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 60
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0, p3}, Lcom/touchus/benchilauncher/views/AppSettingDialog;->pressItem(I)V

    .line 111
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mHandler:Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 95
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 96
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mHandler:Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 101
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/AppSettingDialog;->mHandler:Lcom/touchus/benchilauncher/views/AppSettingDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 102
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 103
    return-void
.end method
