.class Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;
.super Ljava/lang/Object;
.source "ConfigSetDialog.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/ConfigSetDialog;->initSetup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 7
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    const/4 v5, 0x1

    .line 164
    packed-switch p2, :pswitch_data_0

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 166
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 167
    sput-boolean v5, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    .line 168
    sget-object v0, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 169
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$4(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    const-string v1, "connectType"

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 170
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendStoreDataToMcu([B)V

    goto :goto_0

    .line 173
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$1(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    if-eq v0, v5, :cond_0

    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$0(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0700b4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 178
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$0(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0700b5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 179
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$0(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0700af

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 180
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/ConfigSetDialog$4;->this$0:Lcom/touchus/benchilauncher/views/ConfigSetDialog;

    invoke-static {v4}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$0(Lcom/touchus/benchilauncher/views/ConfigSetDialog;)Landroid/content/Context;

    move-result-object v4

    const v6, 0x7f0700b0

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 177
    invoke-static/range {v0 .. v5}, Lcom/touchus/benchilauncher/views/ConfigSetDialog;->access$5(Lcom/touchus/benchilauncher/views/ConfigSetDialog;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b003b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
