.class public Lcom/touchus/benchilauncher/views/SoundDialog$Builder;
.super Ljava/lang/Object;
.source "SoundDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/SoundDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mDialog:Lcom/touchus/benchilauncher/views/SoundDialog;

.field private mIDRIVERENUM:B

.field private mIDRIVERSTATEENUM:B

.field public mSettingHandler:Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;

.field private mSoundAdd:Landroid/widget/ImageView;

.field private mSoundJianjian:Landroid/widget/ImageView;

.field private mSoundSeekBar:Landroid/widget/SeekBar;

.field private mSoundTv:Landroid/widget/TextView;

.field private max:I

.field private mun:I

.field private spUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private type:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    .line 47
    const/16 v0, 0xa

    iput v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->max:I

    .line 179
    new-instance v0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;-><init>(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;

    .line 50
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->context:Landroid/content/Context;

    .line 52
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;I)V
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundTv:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    return v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->type:I

    return v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Lcom/touchus/benchilauncher/utils/SpUtilK;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->spUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->max:I

    return v0
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method


# virtual methods
.method public create(I)Lcom/touchus/benchilauncher/views/SoundDialog;
    .locals 6
    .param p1, "voltype"    # I

    .prologue
    const/4 v4, 0x5

    .line 56
    new-instance v2, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->spUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 57
    iput p1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->type:I

    .line 58
    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->type:I

    if-nez v2, :cond_1

    .line 59
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->spUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v3, "mun"

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    .line 64
    :goto_0
    new-instance v2, Lcom/touchus/benchilauncher/views/SoundDialog;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f080006

    invoke-direct {v2, v3, v4}, Lcom/touchus/benchilauncher/views/SoundDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundDialog;

    .line 65
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->context:Landroid/content/Context;

    .line 66
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 65
    check-cast v0, Landroid/view/LayoutInflater;

    .line 67
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030013

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 69
    .local v1, "layout":Landroid/view/View;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundDialog;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .line 70
    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 69
    invoke-virtual {v2, v1, v3}, Lcom/touchus/benchilauncher/views/SoundDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    const v2, 0x7f0b006d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    .line 73
    const v2, 0x7f0b006e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundAdd:Landroid/widget/ImageView;

    .line 74
    const v2, 0x7f0b006c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundJianjian:Landroid/widget/ImageView;

    .line 75
    const v2, 0x7f0b006b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundTv:Landroid/widget/TextView;

    .line 76
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundDialog;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/views/SoundDialog;->setContentView(Landroid/view/View;)V

    .line 77
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    iget v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 78
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    iget v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->max:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 79
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundTv:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 82
    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->type:I

    if-nez v2, :cond_0

    .line 83
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 86
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$1;-><init>(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 111
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundAdd:Landroid/widget/ImageView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$2;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$2;-><init>(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundJianjian:Landroid/widget/ImageView;

    new-instance v3, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;-><init>(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundDialog;

    return-object v2

    .line 61
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "layout":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->spUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v3, "callmun"

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    goto/16 :goto_0
.end method

.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 199
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_2

    .line 200
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 201
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    .line 202
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERSTATEENUM:B

    .line 203
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_0

    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_3

    .line 204
    :cond_0
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->max:I

    if-ge v1, v2, :cond_1

    .line 205
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundTv:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 221
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_0
    return-void

    .line 209
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_3
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_4

    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_6

    .line 210
    :cond_4
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    if-lez v1, :cond_5

    .line 211
    iget v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    .line 213
    :cond_5
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundTv:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSoundSeekBar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mun:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 215
    :cond_6
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_7

    .line 216
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_7

    .line 217
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 218
    :cond_7
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mDialog:Lcom/touchus/benchilauncher/views/SoundDialog;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/SoundDialog;->dismiss()V

    goto :goto_0
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 175
    iget v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->type:I

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SoundDialog$Builder$DaohangDialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 178
    :cond_0
    return-void
.end method
