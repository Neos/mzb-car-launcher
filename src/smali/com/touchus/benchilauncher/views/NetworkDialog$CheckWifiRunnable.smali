.class Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;
.super Ljava/lang/Object;
.source "NetworkDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/NetworkDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckWifiRunnable"
.end annotation


# instance fields
.field private ConnectingWifiSsid:Ljava/lang/String;

.field final synthetic this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/views/NetworkDialog;Ljava/lang/String;)V
    .locals 0
    .param p2, "ssid"    # Ljava/lang/String;

    .prologue
    .line 484
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    iput-object p2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->ConnectingWifiSsid:Ljava/lang/String;

    .line 486
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 490
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_1

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->ConnectingWifiSsid:Ljava/lang/String;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$4(Lcom/touchus/benchilauncher/views/NetworkDialog;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 495
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v3

    .line 496
    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 495
    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 497
    .local v2, "mWifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 498
    .local v1, "info":Landroid/net/wifi/WifiInfo;
    const-string v3, "launcherlog"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CheckWifiRunnable1 "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_3

    .line 501
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$4(Lcom/touchus/benchilauncher/views/NetworkDialog;)Ljava/lang/String;

    move-result-object v4

    .line 500
    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->getHadSaveWifiConfig(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 502
    .local v0, "config":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_0

    .line 505
    const-string v3, "launcherlog"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CheckWifiRunnable1 "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->status:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 506
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 505
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$5(Lcom/touchus/benchilauncher/views/NetworkDialog;)V

    .line 508
    iget v3, v0, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 511
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v3

    .line 512
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f070082

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 513
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$4(Lcom/touchus/benchilauncher/views/NetworkDialog;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 512
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 510
    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 514
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v3

    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->removeWifiConfig(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 516
    :cond_2
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v3

    .line 517
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    invoke-static {v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f070083

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 518
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->ConnectingWifiSsid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 517
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 516
    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 521
    .end local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    :cond_3
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    iget-object v3, v3, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    new-instance v4, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->this$0:Lcom/touchus/benchilauncher/views/NetworkDialog;

    .line 522
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;->ConnectingWifiSsid:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;Ljava/lang/String;)V

    const-wide/16 v6, 0x7d0

    .line 521
    invoke-virtual {v3, v4, v6, v7}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
