.class public Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
.super Ljava/lang/Object;
.source "MyCustomDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/MyCustomDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private button_layout:Landroid/widget/LinearLayout;

.field private contentView:Landroid/view/View;

.field private context:Landroid/content/Context;

.field private dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

.field private mHandler:Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;

.field private message:Ljava/lang/String;

.field private messageNext:Ljava/lang/String;

.field private negativeButton:Landroid/widget/Button;

.field private negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private negativeButtonText:Ljava/lang/String;

.field private positiveButton:Landroid/widget/Button;

.field private positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private positiveButtonText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v1, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;-><init>(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->mHandler:Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;

    .line 58
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->context:Landroid/content/Context;

    .line 59
    instance-of v1, p1, Lcom/touchus/benchilauncher/Launcher;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    .line 61
    .local v0, "launcher":Lcom/touchus/benchilauncher/Launcher;
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 63
    .end local v0    # "launcher":Lcom/touchus/benchilauncher/Launcher;
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v1, :cond_1

    .line 64
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->mHandler:Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 66
    :cond_1
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)Lcom/touchus/benchilauncher/views/MyCustomDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 88
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_0

    .line 90
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 91
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->right()V

    .line 100
    :cond_1
    :goto_0
    return-void

    .line 92
    :cond_2
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_3

    .line 93
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_4

    .line 94
    :cond_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->left()V

    goto :goto_0

    .line 95
    :cond_4
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_1

    .line 96
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->press()V

    goto :goto_0
.end method

.method private left()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 104
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 106
    :cond_0
    return-void
.end method

.method private press()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    .line 127
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->unRegisterHandler()V

    .line 128
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    goto :goto_0

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    goto :goto_0
.end method

.method private right()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 110
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 112
    :cond_0
    return-void
.end method


# virtual methods
.method public create()Lcom/touchus/benchilauncher/views/MyCustomDialog;
    .locals 9

    .prologue
    const v8, 0x7f0b0067

    const v7, 0x7f0b001e

    const/4 v6, -0x1

    const/16 v5, 0x8

    .line 201
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->context:Landroid/content/Context;

    .line 202
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 201
    check-cast v0, Landroid/view/LayoutInflater;

    .line 204
    .local v0, "inflater":Landroid/view/LayoutInflater;
    new-instance v2, Lcom/touchus/benchilauncher/views/MyCustomDialog;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->context:Landroid/content/Context;

    const v4, 0x7f080006

    invoke-direct {v2, v3, v4}, Lcom/touchus/benchilauncher/views/MyCustomDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    .line 205
    const v2, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 206
    .local v1, "layout":Landroid/view/View;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .line 207
    const/4 v4, -0x2

    invoke-direct {v3, v6, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 206
    invoke-virtual {v2, v1, v3}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 208
    const v2, 0x7f0b001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    .line 209
    const v2, 0x7f0b0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    .line 210
    const v2, 0x7f0b0069

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->button_layout:Landroid/widget/LinearLayout;

    .line 212
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 214
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonText:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 215
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->button_layout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 216
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v2, :cond_0

    .line 218
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    new-instance v3, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$1;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$1;-><init>(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonText:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 234
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v2, :cond_1

    .line 236
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    new-instance v3, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$2;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$2;-><init>(Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->message:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 251
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->message:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 252
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    :cond_2
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->messageNext:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 256
    const v2, 0x7f0b0068

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->messageNext:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v2, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->setContentView(Landroid/view/View;)V

    .line 265
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->dialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    return-object v2

    .line 229
    :cond_4
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->button_layout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 230
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 247
    :cond_5
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 258
    :cond_6
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->contentView:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 259
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 260
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 261
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 262
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->contentView:Landroid/view/View;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public setContentView(Landroid/view/View;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->contentView:Landroid/view/View;

    .line 158
    return-object p0
.end method

.method public setMessage(I)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 1
    .param p1, "message"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->message:Ljava/lang/String;

    .line 148
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->message:Ljava/lang/String;

    .line 138
    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 1
    .param p1, "negativeButtonText"    # I
    .param p2, "listener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->context:Landroid/content/Context;

    .line 185
    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 184
    iput-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonText:Ljava/lang/String;

    .line 186
    iput-object p2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 187
    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 0
    .param p1, "negativeButtonText"    # Ljava/lang/String;
    .param p2, "listener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonText:Ljava/lang/String;

    .line 193
    iput-object p2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 194
    return-object p0
.end method

.method public setNextMessage(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 0
    .param p1, "messageNext"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->messageNext:Ljava/lang/String;

    .line 153
    return-object p0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 1
    .param p1, "positiveButtonText"    # I
    .param p2, "listener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->context:Landroid/content/Context;

    .line 170
    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 169
    iput-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonText:Ljava/lang/String;

    .line 171
    iput-object p2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 172
    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    .locals 0
    .param p1, "positiveButtonText"    # Ljava/lang/String;
    .param p2, "listener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonText:Ljava/lang/String;

    .line 178
    iput-object p2, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 179
    return-object p0
.end method

.method public unRegisterHandler()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->mHandler:Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder$MyCustomHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 134
    :cond_0
    return-void
.end method
