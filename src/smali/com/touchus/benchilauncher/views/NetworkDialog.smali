.class public Lcom/touchus/benchilauncher/views/NetworkDialog;
.super Landroid/app/Dialog;
.source "NetworkDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;,
        Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;,
        Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Dialog;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;"
    }
.end annotation


# instance fields
.field private CHECK_WIFI_SPACE:I

.field private adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private clickCount:I

.field private context:Landroid/content/Context;

.field private currentApName:Ljava/lang/String;

.field private currentConnectingWifiSsid:Ljava/lang/String;

.field private currentPwd:Ljava/lang/String;

.field private data:Landroid/widget/TextView;

.field private dataTview:Landroid/widget/TextView;

.field private data_switch:Landroid/widget/ImageView;

.field private hadPwdCbox:Landroid/widget/CheckBox;

.field private iDataOpen:Z

.field private iWifiOpen:Z

.field private iWlanOpen:Z

.field private list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/BeanWifi;",
            ">;"
        }
    .end annotation
.end field

.field public mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

.field private mListView:Landroid/widget/ListView;

.field private noListDataTview:Landroid/widget/TextView;

.field private refreshWifiListRunnable:Ljava/lang/Runnable;

.field private submitBtn:Landroid/widget/Button;

.field private wifiReceiver:Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;

.field private wifi_ap:Landroid/widget/TextView;

.field private wifi_ap_switch:Landroid/widget/ImageView;

.field private wifi_apset_rl:Landroid/widget/RelativeLayout;

.field private wifi_name_et:Landroid/widget/EditText;

.field private wifi_pwd:Landroid/widget/TextView;

.field private wifi_pwd_et:Landroid/widget/EditText;

.field private wlan:Landroid/widget/TextView;

.field private wlan_switch:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 51
    iput v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->clickCount:I

    .line 69
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    .line 70
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    .line 71
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    .line 402
    new-instance v0, Lcom/touchus/benchilauncher/views/NetworkDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/NetworkDialog$1;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListRunnable:Ljava/lang/Runnable;

    .line 477
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->CHECK_WIFI_SPACE:I

    .line 581
    new-instance v0, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    .line 78
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 51
    iput v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->clickCount:I

    .line 69
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    .line 70
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    .line 71
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    .line 402
    new-instance v0, Lcom/touchus/benchilauncher/views/NetworkDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/NetworkDialog$1;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListRunnable:Ljava/lang/Runnable;

    .line 477
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->CHECK_WIFI_SPACE:I

    .line 581
    new-instance v0, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    .line 83
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 84
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/NetworkDialog;)V
    .locals 0

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListViewByConnected()V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/NetworkDialog;)Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    return v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/NetworkDialog;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/NetworkDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/NetworkDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->currentConnectingWifiSsid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/NetworkDialog;)V
    .locals 0

    .prologue
    .line 443
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListViewByDisconnect()V

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/views/NetworkDialog;)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiSettingView()V

    return-void
.end method

.method private enterToWifiSettingView()V
    .locals 4

    .prologue
    .line 398
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 399
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 400
    return-void
.end method

.method private refreshWifiListViewByConnected()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 421
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/touchus/benchilauncher/utils/WifiTool;->getAllCanConectWifi(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 422
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->notifyDataSetChanged()V

    .line 425
    :cond_0
    return-void
.end method

.method private refreshWifiListViewByConnecting(Ljava/lang/String;)V
    .locals 6
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 428
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 429
    .local v0, "oldIndex":I
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->getListData()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    .line 430
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 437
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->notifyDataSetInvalidated()V

    .line 438
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    new-instance v3, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;

    invoke-direct {v3, p0, p1}, Lcom/touchus/benchilauncher/views/NetworkDialog$CheckWifiRunnable;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;Ljava/lang/String;)V

    iget v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->CHECK_WIFI_SPACE:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 439
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->currentConnectingWifiSsid:Ljava/lang/String;

    .line 440
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 441
    return-void

    .line 430
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/bean/BeanWifi;

    .line 431
    .local v1, "temp":Lcom/touchus/benchilauncher/bean/BeanWifi;
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 432
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->setState(I)V

    goto :goto_0

    .line 434
    :cond_1
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->setState(I)V

    goto :goto_0
.end method

.method private refreshWifiListViewByDisconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 444
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 445
    .local v0, "oldIndex":I
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->getListData()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    .line 446
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 451
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->notifyDataSetInvalidated()V

    .line 452
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 453
    return-void

    .line 446
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/bean/BeanWifi;

    .line 447
    .local v1, "wifi":Lcom/touchus/benchilauncher/bean/BeanWifi;
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 448
    invoke-virtual {v1, v4}, Lcom/touchus/benchilauncher/bean/BeanWifi;->setState(I)V

    goto :goto_0
.end method

.method private refreshWifiSettingView()V
    .locals 5

    .prologue
    .line 456
    iget-boolean v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 461
    .local v2, "temp":Ljava/lang/Object;
    instance-of v3, v2, Lcom/touchus/benchilauncher/bean/BeanWifi;

    if-eqz v3, :cond_2

    .line 463
    :try_start_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 464
    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 463
    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 465
    .local v0, "mWiFiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v1

    .line 466
    .local v1, "netId":I
    if-gez v1, :cond_3

    .line 467
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListViewByDisconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    .end local v0    # "mWiFiManager":Landroid/net/wifi/WifiManager;
    .end local v1    # "netId":I
    :cond_2
    :goto_1
    const-string v3, ""

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->currentConnectingWifiSsid:Ljava/lang/String;

    goto :goto_0

    .line 469
    .restart local v0    # "mWiFiManager":Landroid/net/wifi/WifiManager;
    .restart local v1    # "netId":I
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListViewByConnected()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 471
    .end local v0    # "mWiFiManager":Landroid/net/wifi/WifiManager;
    .end local v1    # "netId":I
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private registerWifiReceiver()V
    .locals 3

    .prologue
    .line 370
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 371
    .local v0, "wifiFilter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 372
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 374
    new-instance v1, Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;-><init>(Lcom/touchus/benchilauncher/views/NetworkDialog;)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifiReceiver:Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;

    .line 375
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifiReceiver:Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 376
    return-void
.end method

.method private setSwitchImage(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "isOpen"    # Z

    .prologue
    .line 181
    if-eqz p2, :cond_0

    .line 182
    const v0, 0x7f0200fa

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    const v0, 0x7f0200f9

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private showWifiConfigDialog(Lcom/touchus/benchilauncher/bean/BeanWifi;)V
    .locals 3
    .param p1, "wifi"    # Lcom/touchus/benchilauncher/bean/BeanWifi;

    .prologue
    .line 572
    new-instance v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 573
    const v2, 0x7f080006

    .line 572
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;-><init>(Landroid/content/Context;I)V

    .line 574
    .local v0, "wifiialog":Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;
    iput-object p0, v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->parent:Lcom/touchus/benchilauncher/views/NetworkDialog;

    .line 575
    const/16 v1, 0x1f4

    iput v1, v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->width:I

    .line 576
    const/16 v1, 0xc8

    iput v1, v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->height:I

    .line 577
    iput-object p1, v0, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->currentWifiInfo:Lcom/touchus/benchilauncher/bean/BeanWifi;

    .line 578
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/DlgWifiRelaDialog;->show()V

    .line 579
    return-void
.end method

.method private unregisterWifiReceiver()V
    .locals 2

    .prologue
    .line 380
    :try_start_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifiReceiver:Lcom/touchus/benchilauncher/views/NetworkDialog$WifiBroadCastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :goto_0
    return-void

    .line 381
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public connectingToSpecifyWifi(Ljava/lang/String;)V
    .locals 0
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 394
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListViewByConnecting(Ljava/lang/String;)V

    .line 395
    return-void
.end method

.method public disConnectCurWifi()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/touchus/benchilauncher/utils/WifiTool;->disConnectWifi(Landroid/content/Context;)V

    .line 391
    return-void
.end method

.method public handlerMsg(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 600
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_0

    .line 601
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 602
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 603
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_1

    .line 604
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->dismiss()V

    .line 610
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 605
    .restart local v0    # "idriverCode":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_1
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_2

    .line 606
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_0

    .line 607
    :cond_2
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->dismiss()V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 351
    if-eqz p2, :cond_0

    .line 352
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 353
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->currentPwd:Ljava/lang/String;

    .line 354
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 355
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 366
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 362
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 363
    const v2, 0x7f070084

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 362
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->currentPwd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f07009a

    const v8, 0x7f070099

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 347
    :goto_0
    :pswitch_0
    return-void

    .line 206
    :pswitch_1
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 207
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    if-eqz v4, :cond_0

    .line 208
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    :goto_1
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 213
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 214
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 210
    :cond_0
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 217
    :pswitch_2
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    if-eqz v4, :cond_1

    .line 218
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 219
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    :goto_2
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 226
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 221
    :cond_1
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 223
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 229
    :pswitch_3
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    if-eqz v4, :cond_2

    .line 230
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 231
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    :goto_3
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 238
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 233
    :cond_2
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 234
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 241
    :pswitch_4
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 242
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    if-eqz v4, :cond_3

    .line 243
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    .line 244
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    :goto_4
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/MobileDataSwitcher;->setMobileData(Landroid/content/Context;Z)V

    .line 250
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->data_switch:Landroid/widget/ImageView;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    invoke-direct {p0, v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    .line 251
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 252
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 253
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 246
    :cond_3
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    .line 247
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-virtual {v5, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 257
    :pswitch_5
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    if-eqz v4, :cond_4

    .line 258
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    .line 259
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 260
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->unregisterWifiReceiver()V

    .line 271
    :goto_5
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->setWifiState(Landroid/content/Context;Z)V

    .line 272
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->enterToWifiSettingView()V

    .line 273
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 274
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan_switch:Landroid/widget/ImageView;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    invoke-direct {p0, v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    .line 276
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap_switch:Landroid/widget/ImageView;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    invoke-direct {p0, v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    goto/16 :goto_0

    .line 264
    :cond_4
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    .line 265
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    .line 266
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 267
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->registerWifiReceiver()V

    .line 269
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setWifiApState(Z)V

    goto :goto_5

    .line 279
    :pswitch_6
    iget-boolean v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    if-eqz v4, :cond_5

    .line 280
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    .line 281
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->closeWifiAp(Landroid/content/Context;)V

    .line 282
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 283
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 296
    :goto_6
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setWifiApState(Z)V

    .line 297
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 298
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan_switch:Landroid/widget/ImageView;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    invoke-direct {p0, v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    .line 300
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap_switch:Landroid/widget/ImageView;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    invoke-direct {p0, v4, v5}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    goto/16 :goto_0

    .line 286
    :cond_5
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    .line 287
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    .line 288
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->unregisterWifiReceiver()V

    .line 289
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 290
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->getCurrentWifiApConfig(Landroid/content/Context;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 293
    .local v0, "apConfig":Landroid/net/wifi/WifiConfiguration;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4, v0}, Lcom/touchus/benchilauncher/utils/WifiTool;->openWifiAp(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)V

    .line 294
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->setWifiState(Landroid/content/Context;Z)V

    goto :goto_6

    .line 303
    .end local v0    # "apConfig":Landroid/net/wifi/WifiConfiguration;
    :pswitch_7
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_name_et:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 304
    .local v2, "name":Ljava/lang/String;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 305
    .local v3, "pwd":Ljava/lang/String;
    invoke-static {v2}, Lcom/touchus/publicutils/utils/UtilTools;->stringFilter(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 306
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 307
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v6, 0x7f07007f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 306
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 310
    :cond_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 311
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 312
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v6, 0x7f070080

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 311
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 316
    :cond_7
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->hadPwdCbox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v6, :cond_8

    .line 317
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 318
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v6, 0x7f07008d

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 317
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 321
    :cond_8
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->hadPwdCbox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 322
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v4, v7}, Lcom/touchus/benchilauncher/LauncherApplication;->setWifiApHasPwd(Z)V

    .line 323
    const-string v3, ""

    .line 328
    :goto_7
    const/4 v4, 0x2

    new-array v1, v4, [Ljava/lang/String;

    aput-object v2, v1, v5

    aput-object v3, v1, v7

    .line 329
    .local v1, "idAndKey":[Ljava/lang/String;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v4, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->setWifiApIdAndKey([Ljava/lang/String;)V

    .line 331
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->iWifiApIsOpen(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 332
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->closeWifiAp(Landroid/content/Context;)V

    .line 333
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4, v2, v3}, Lcom/touchus/benchilauncher/utils/WifiTool;->updateWifiAp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 335
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->getCurrentWifiApConfig(Landroid/content/Context;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v5

    .line 334
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/WifiTool;->openWifiAp(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)V

    .line 340
    :goto_8
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 341
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v6, 0x7f07007e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 340
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showLongToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 325
    .end local v1    # "idAndKey":[Ljava/lang/String;
    :cond_9
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setWifiApHasPwd(Z)V

    goto :goto_7

    .line 337
    .restart local v1    # "idAndKey":[Ljava/lang/String;
    :cond_a
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4, v2, v3}, Lcom/touchus/benchilauncher/utils/WifiTool;->updateWifiAp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 204
    :pswitch_data_0
    .packed-switch 0x7f0b0057
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 88
    const v1, 0x7f030010

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setContentView(I)V

    .line 89
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 90
    const v1, 0x7f0b0059

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan:Landroid/widget/TextView;

    .line 91
    const v1, 0x7f0b005b

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap:Landroid/widget/TextView;

    .line 92
    const v1, 0x7f0b0057

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->data:Landroid/widget/TextView;

    .line 93
    const v1, 0x7f0b005a

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan_switch:Landroid/widget/ImageView;

    .line 94
    const v1, 0x7f0b005c

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap_switch:Landroid/widget/ImageView;

    .line 95
    const v1, 0x7f0b0058

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->data_switch:Landroid/widget/ImageView;

    .line 96
    const v1, 0x7f0b005d

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    .line 97
    const v1, 0x7f0b005f

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_name_et:Landroid/widget/EditText;

    .line 98
    const v1, 0x7f0b0060

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd:Landroid/widget/TextView;

    .line 99
    const v1, 0x7f0b0061

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    .line 100
    const v1, 0x7f0b0062

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->hadPwdCbox:Landroid/widget/CheckBox;

    .line 101
    const v1, 0x7f0b0063

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->submitBtn:Landroid/widget/Button;

    .line 103
    const v1, 0x7f0b0064

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    .line 104
    const v1, 0x7f0b0065

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    .line 105
    const v1, 0x7f0b0066

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    .line 107
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->data:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan_switch:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap_switch:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->data_switch:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_name_et:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->hadPwdCbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 117
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->submitBtn:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/touchus/benchilauncher/utils/MobileDataSwitcher;->getMobileDataState(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    .line 120
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v3, 0x7f07009a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->data_switch:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    invoke-direct {p0, v1, v2}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    .line 127
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getWifiApIdAndKey()[Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "idAndKey":[Ljava/lang/String;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_name_et:Landroid/widget/EditText;

    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    aget-object v2, v0, v6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/touchus/benchilauncher/utils/WifiTool;->iWifiState(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    .line 132
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setWifiApState(Z)V

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getWifiApState()Z

    move-result v1

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    .line 136
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wlan_switch:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    invoke-direct {p0, v1, v2}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    .line 137
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_ap_switch:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    invoke-direct {p0, v1, v2}, Lcom/touchus/benchilauncher/views/NetworkDialog;->setSwitchImage(Landroid/widget/ImageView;Z)V

    .line 139
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWlanOpen:Z

    if-eqz v1, :cond_3

    .line 141
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 142
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 143
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getWifiApHasPwd()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 157
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->hadPwdCbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 158
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 165
    :goto_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 167
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    .line 168
    new-instance v1, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->list:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    .line 169
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->adapter:Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->registerWifiReceiver()V

    .line 171
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->enterToWifiSettingView()V

    .line 174
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 175
    const/16 v2, 0x12

    .line 174
    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 177
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 178
    return-void

    .line 123
    .end local v0    # "idAndKey":[Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v3, 0x7f070099

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 145
    .restart local v0    # "idAndKey":[Ljava/lang/String;
    :cond_3
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iWifiOpen:Z

    if-eqz v1, :cond_4

    .line 146
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 147
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 148
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 150
    :cond_4
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->iDataOpen:Z

    if-eqz v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_apset_rl:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 152
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 153
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->noListDataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->dataTview:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 161
    :cond_5
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->hadPwdCbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 162
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->wifi_pwd_et:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 545
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/BeanWifi;

    .line 546
    .local v3, "wifi":Lcom/touchus/benchilauncher/bean/BeanWifi;
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 549
    :cond_1
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 552
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->isPwd()Z

    move-result v4

    if-nez v4, :cond_3

    .line 553
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->disConnectCurWifi()V

    .line 555
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getCapabilities()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/WifiTool;->getCurrentWifiPwdType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 556
    .local v2, "type":Ljava/lang/String;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 557
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    .line 556
    invoke-static {v4, v5, v6, v2}, Lcom/touchus/benchilauncher/utils/WifiTool;->CreateWifiInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 558
    .local v0, "config":Landroid/net/wifi/WifiConfiguration;
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    invoke-static {v4, v0}, Lcom/touchus/benchilauncher/utils/WifiTool;->connectToSpecifyWifi(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)Z

    move-result v1

    .line 559
    .local v1, "flag":Z
    if-nez v1, :cond_2

    .line 560
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    .line 561
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->context:Landroid/content/Context;

    const v7, 0x7f070086

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 562
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 561
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 560
    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showBigShortToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 564
    :cond_2
    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/views/NetworkDialog;->connectingToSpecifyWifi(Ljava/lang/String;)V

    goto :goto_0

    .line 567
    .end local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    .end local v1    # "flag":Z
    .end local v2    # "type":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v3}, Lcom/touchus/benchilauncher/views/NetworkDialog;->showWifiConfigDialog(Lcom/touchus/benchilauncher/bean/BeanWifi;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 191
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 192
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/NetworkDialog;->mHandler:Lcom/touchus/benchilauncher/views/NetworkDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 198
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->unregisterWifiReceiver()V

    .line 199
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 200
    return-void
.end method

.method public updateWifiList()V
    .locals 0

    .prologue
    .line 416
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/NetworkDialog;->refreshWifiListViewByDisconnect()V

    .line 417
    return-void
.end method
