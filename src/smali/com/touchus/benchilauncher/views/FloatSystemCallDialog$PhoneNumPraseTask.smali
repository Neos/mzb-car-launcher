.class Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;
.super Landroid/os/AsyncTask;
.source "FloatSystemCallDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhoneNumPraseTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 349
    :try_start_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$0(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/touchus/publicutils/utils/UtilTools;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 350
    const-string v0, ""

    .line 351
    .local v0, "city":Ljava/lang/String;
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$1()Lorg/slf4j/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FloatSystemCallDialog PhoneNumPraseTask city: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 352
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 351
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 361
    .end local v0    # "city":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 355
    :cond_0
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$1()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "FloatSystemCallDialog PhoneNumPraseTask not net error ! "

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$0(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070034

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 356
    :catch_0
    move-exception v1

    .line 357
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 358
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$1()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "FloatSystemCallDialog get city error !"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 359
    const-string v0, ""

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 366
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 367
    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v2, 0x584

    if-eq v1, v2, :cond_0

    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v2, 0x585

    if-ne v1, v2, :cond_1

    .line 369
    :cond_0
    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_2

    .line 370
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$2(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$0(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/content/Context;

    move-result-object v3

    .line 371
    const v4, 0x7f070035

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 372
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 373
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 370
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    :cond_1
    :goto_0
    return-void

    .line 374
    :cond_2
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_1

    .line 375
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$2(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;->this$0:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->access$0(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/content/Context;

    move-result-object v3

    .line 376
    const v4, 0x7f070036

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 377
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 378
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 375
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
