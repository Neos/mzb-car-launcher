.class Lcom/touchus/benchilauncher/views/CycleWheelView$3;
.super Landroid/graphics/drawable/Drawable;
.source "CycleWheelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/CycleWheelView;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/CycleWheelView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    .line 423
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 20
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 426
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->getWidth()I

    move-result v19

    .line 427
    .local v19, "viewWidth":I
    new-instance v18, Landroid/graphics/Paint;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    .line 428
    .local v18, "dividerPaint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$9(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 429
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$10(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 430
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 431
    .local v12, "seletedSolidPaint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$11(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    invoke-virtual {v12, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 432
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 433
    .local v6, "solidPaint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$12(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 434
    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, v19

    int-to-float v4, v0

    .line 435
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v5}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    mul-int/2addr v1, v5

    int-to-float v5, v1

    move-object/from16 v1, p1

    .line 434
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 436
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v1, v3

    int-to-float v3, v1

    .line 437
    move/from16 v0, v19

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v5}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v5

    mul-int/2addr v1, v5

    int-to-float v5, v1

    move-object/from16 v1, p1

    .line 436
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 438
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    mul-int/2addr v1, v2

    int-to-float v9, v1

    move/from16 v0, v19

    int-to-float v10, v0

    .line 439
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    int-to-float v11, v1

    move-object/from16 v7, p1

    .line 438
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 440
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    mul-int/2addr v1, v2

    int-to-float v15, v1

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v16, v0

    .line 441
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    mul-int/2addr v1, v2

    int-to-float v0, v1

    move/from16 v17, v0

    move-object/from16 v13, p1

    .line 440
    invoke-virtual/range {v13 .. v18}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 442
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    int-to-float v15, v1

    .line 443
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$4(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/views/CycleWheelView$3;->this$0:Lcom/touchus/benchilauncher/views/CycleWheelView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/CycleWheelView;->access$1(Lcom/touchus/benchilauncher/views/CycleWheelView;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    int-to-float v0, v1

    move/from16 v17, v0

    move-object/from16 v13, p1

    .line 442
    invoke-virtual/range {v13 .. v18}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 445
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 457
    const/4 v0, 0x0

    return v0
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .prologue
    .line 449
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 453
    return-void
.end method
