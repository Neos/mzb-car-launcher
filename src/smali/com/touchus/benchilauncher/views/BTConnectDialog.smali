.class public Lcom/touchus/benchilauncher/views/BTConnectDialog;
.super Ljava/lang/Object;
.source "BTConnectDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;
    }
.end annotation


# static fields
.field private static instance:Lcom/touchus/benchilauncher/views/BTConnectDialog;

.field private static isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static logger:Lorg/slf4j/Logger;


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private contentView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

.field private message:Ljava/lang/String;

.field private negativeButton:Landroid/widget/Button;

.field private negativeButtonText:Ljava/lang/String;

.field private outside:Landroid/view/View;

.field private params:Landroid/view/WindowManager$LayoutParams;

.field private positiveButton:Landroid/widget/Button;

.field private positiveButtonText:Ljava/lang/String;

.field time:I

.field timer:Ljava/util/Timer;

.field update:Ljava/lang/Runnable;

.field private wm:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    const-class v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 41
    sput-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->logger:Lorg/slf4j/Logger;

    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->instance:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    .line 60
    const/16 v0, 0x8

    iput v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    .line 61
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->timer:Ljava/util/Timer;

    .line 165
    new-instance v0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;-><init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->update:Ljava/lang/Runnable;

    .line 206
    new-instance v0, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;-><init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mHandler:Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    .line 67
    sget-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->logger:Lorg/slf4j/Logger;

    const-string v1, "BTConnectDialog BTConnectDialog()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method static synthetic access$0()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->logger:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mHandler:Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/BTConnectDialog;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    return-object v0
.end method

.method private autoConnect()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mHandler:Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->update:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->post(Ljava/lang/Runnable;)Z

    .line 163
    return-void
.end method

.method public static getInstance()Lcom/touchus/benchilauncher/views/BTConnectDialog;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->instance:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->instance:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    .line 74
    :cond_0
    sget-object v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->instance:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    return-object v0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 230
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 231
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 232
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    .line 231
    if-eq v1, v2, :cond_0

    .line 233
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 234
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    .line 233
    if-ne v1, v2, :cond_2

    .line 235
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->right()V

    .line 247
    :cond_1
    :goto_0
    return-void

    .line 236
    :cond_2
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 237
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_3

    .line 238
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 239
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    .line 238
    if-ne v1, v2, :cond_4

    .line 240
    :cond_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->left()V

    goto :goto_0

    .line 241
    :cond_4
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 242
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_5

    .line 243
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->press()V

    goto :goto_0

    .line 244
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButtonText:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private left()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 252
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 254
    :cond_0
    return-void
.end method

.method private press()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    .line 269
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->unRegisterHandler()V

    .line 270
    return-void

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->performClick()Z

    goto :goto_0
.end method

.method private right()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 259
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 261
    :cond_0
    return-void
.end method


# virtual methods
.method public dissShow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 186
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 204
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btConnectDialog:Z

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btConnectDialog:Z

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 193
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->unRegisterHandler()V

    .line 195
    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 197
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_2
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    .line 203
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public showBTConnectView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 78
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 79
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->logger:Lorg/slf4j/Logger;

    const-string v2, "BTConnectDialog show  contentView != null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 159
    :goto_0
    return-void

    .line 83
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mContext:Landroid/content/Context;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 86
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f070070

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->message:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f070097

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButtonText:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f070098

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButtonText:Ljava/lang/String;

    .line 89
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->timer:Ljava/util/Timer;

    .line 90
    const/16 v1, 0x8

    iput v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    .line 91
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mContext:Landroid/content/Context;

    .line 92
    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 91
    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->wm:Landroid/view/WindowManager;

    .line 93
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->params:Landroid/view/WindowManager$LayoutParams;

    .line 95
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 96
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x28

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 99
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x7d3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 101
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 102
    const v2, 0x7f030008

    const/4 v3, 0x0

    .line 101
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    .line 103
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    .line 104
    const v2, 0x7f0b001d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 103
    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->outside:Landroid/view/View;

    .line 105
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    .line 106
    const v2, 0x7f0b001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 105
    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    .line 107
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    .line 108
    const v2, 0x7f0b0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 107
    iput-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    .line 109
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 111
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButtonText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButtonText:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->positiveButton:Landroid/widget/Button;

    new-instance v2, Lcom/touchus/benchilauncher/views/BTConnectDialog$2;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog$2;-><init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButtonText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->negativeButton:Landroid/widget/Button;

    new-instance v2, Lcom/touchus/benchilauncher/views/BTConnectDialog$3;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog$3;-><init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    const v2, 0x7f0b001e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 135
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->contentView:Landroid/view/View;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->params:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->outside:Landroid/view/View;

    new-instance v2, Lcom/touchus/benchilauncher/views/BTConnectDialog$4;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog$4;-><init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 152
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->autoConnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mHandler:Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 158
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v6, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btConnectDialog:Z

    goto/16 :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->logger:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BTConnectDialog Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public unRegisterHandler()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog;->mHandler:Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 276
    :cond_0
    return-void
.end method
