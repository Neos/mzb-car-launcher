.class Lcom/touchus/benchilauncher/views/BTConnectDialog$1;
.super Ljava/lang/Object;
.source "BTConnectDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/BTConnectDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 169
    invoke-static {}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$0()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BTConnectDialog time:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    iget v3, v3, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    iget v1, v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    if-nez v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$1(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btConnectDialog:Z

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$1(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/MainService;->btMusicConnect()V

    .line 175
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->dissShow()V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    iget v2, v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/touchus/benchilauncher/views/BTConnectDialog;->time:I

    .line 179
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$2(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 180
    .local v0, "message":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 181
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$2(Lcom/touchus/benchilauncher/views/BTConnectDialog;)Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$1;->this$0:Lcom/touchus/benchilauncher/views/BTConnectDialog;

    iget-object v2, v2, Lcom/touchus/benchilauncher/views/BTConnectDialog;->update:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
