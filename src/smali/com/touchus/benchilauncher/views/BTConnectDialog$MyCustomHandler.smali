.class Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;
.super Landroid/os/Handler;
.source "BTConnectDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/BTConnectDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MyCustomHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/views/BTConnectDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/views/BTConnectDialog;)V
    .locals 1
    .param p1, "instance"    # Lcom/touchus/benchilauncher/views/BTConnectDialog;

    .prologue
    .line 211
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 212
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->target:Ljava/lang/ref/WeakReference;

    .line 213
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 217
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 218
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x1771

    if-ne v0, v1, :cond_2

    .line 222
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v0, p1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$3(Lcom/touchus/benchilauncher/views/BTConnectDialog;Landroid/os/Message;)V

    goto :goto_0

    .line 223
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/BTConnectDialog$MyCustomHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/BTConnectDialog;

    invoke-static {v0, p1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->access$3(Lcom/touchus/benchilauncher/views/BTConnectDialog;Landroid/os/Message;)V

    goto :goto_0
.end method
