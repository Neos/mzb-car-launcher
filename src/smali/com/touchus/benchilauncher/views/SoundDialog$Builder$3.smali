.class Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;
.super Ljava/lang/Object;
.source "SoundDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->create(I)Lcom/touchus/benchilauncher/views/SoundDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v0

    if-lez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$0(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;I)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$1(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$6(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 131
    return-void
.end method
