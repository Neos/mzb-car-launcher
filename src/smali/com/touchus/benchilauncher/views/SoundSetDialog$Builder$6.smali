.class Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;
.super Ljava/lang/Object;
.source "SoundSetDialog.java"

# interfaces
.implements Lcom/touchus/benchilauncher/inface/OnSoundClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->create()Lcom/touchus/benchilauncher/views/SoundSetDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;->this$1:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;->this$1:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->access$0(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;I)V

    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;->this$1:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;->this$1:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->access$1(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->access$2(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;I)V

    .line 161
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder$6;->this$1:Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;->access$3(Lcom/touchus/benchilauncher/views/SoundSetDialog$Builder;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->mediaVoice:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 162
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    .line 163
    sget v1, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    .line 164
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    aget v2, v2, p1

    .line 165
    const/4 v3, 0x0

    sget-object v4, Lcom/touchus/benchilauncher/SysConst;->num:[I

    aget v4, v4, p1

    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->num:[I

    aget v5, v5, p1

    .line 162
    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 166
    return-void
.end method
