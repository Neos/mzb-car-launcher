.class Lcom/touchus/benchilauncher/views/DesktopLayout$3;
.super Ljava/lang/Object;
.source "DesktopLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/DesktopLayout;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/DesktopLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 39
    const-string v0, "DesktopLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MotionEvent event = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->access$0(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0200cd

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 42
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->access$1(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 43
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->access$2(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->access$3(Lcom/touchus/benchilauncher/views/DesktopLayout;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 45
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/views/DesktopLayout;->isHide:Z

    .line 46
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/DesktopLayout$3;->this$0:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/DesktopLayout;->access$4(Lcom/touchus/benchilauncher/views/DesktopLayout;)V

    .line 49
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 47
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    goto :goto_0
.end method
