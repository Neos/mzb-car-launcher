.class public Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;
.super Landroid/app/Dialog;
.source "CallPhoneKeyDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field lastview:Landroid/view/View;

.field private number0Img:Landroid/widget/ImageView;

.field private number1Img:Landroid/widget/ImageView;

.field private number2Img:Landroid/widget/ImageView;

.field private number3Img:Landroid/widget/ImageView;

.field private number4Img:Landroid/widget/ImageView;

.field private number5Img:Landroid/widget/ImageView;

.field private number6Img:Landroid/widget/ImageView;

.field private number7Img:Landroid/widget/ImageView;

.field private number8Img:Landroid/widget/ImageView;

.field private number9Img:Landroid/widget/ImageView;

.field private numberOtherImg:Landroid/widget/ImageView;

.field private numberStarImg:Landroid/widget/ImageView;

.field public parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->lastview:Landroid/view/View;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->lastview:Landroid/view/View;

    .line 33
    return-void
.end method

.method private initListener()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number0Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number1Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number2Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number3Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number4Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number5Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number6Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number7Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number8Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number9Img:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->numberStarImg:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->numberOtherImg:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 95
    const v0, 0x7f0b0026

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number1Img:Landroid/widget/ImageView;

    .line 96
    const v0, 0x7f0b0027

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number2Img:Landroid/widget/ImageView;

    .line 97
    const v0, 0x7f0b0028

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number3Img:Landroid/widget/ImageView;

    .line 98
    const v0, 0x7f0b0029

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number4Img:Landroid/widget/ImageView;

    .line 99
    const v0, 0x7f0b002a

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number5Img:Landroid/widget/ImageView;

    .line 100
    const v0, 0x7f0b002b

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number6Img:Landroid/widget/ImageView;

    .line 101
    const v0, 0x7f0b002c

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number7Img:Landroid/widget/ImageView;

    .line 102
    const v0, 0x7f0b002d

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number8Img:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f0b002e

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number9Img:Landroid/widget/ImageView;

    .line 104
    const v0, 0x7f0b0030

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->number0Img:Landroid/widget/ImageView;

    .line 105
    const v0, 0x7f0b002f

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->numberStarImg:Landroid/widget/ImageView;

    .line 106
    const v0, 0x7f0b0031

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->numberOtherImg:Landroid/widget/ImageView;

    .line 107
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 59
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    if-nez v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->lastview:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->lastview:Landroid/view/View;

    if-eq v0, p1, :cond_2

    .line 63
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->lastview:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 65
    :cond_2
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->lastview:Landroid/view/View;

    .line 66
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0030

    if-ne v0, v1, :cond_3

    .line 68
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 69
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0026

    if-ne v0, v1, :cond_4

    .line 70
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 71
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0027

    if-ne v0, v1, :cond_5

    .line 72
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 73
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0028

    if-ne v0, v1, :cond_6

    .line 74
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 75
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0029

    if-ne v0, v1, :cond_7

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 77
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b002a

    if-ne v0, v1, :cond_8

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 79
    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b002b

    if-ne v0, v1, :cond_9

    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0

    .line 81
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b002c

    if-ne v0, v1, :cond_a

    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0

    .line 83
    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b002d

    if-ne v0, v1, :cond_b

    .line 84
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0

    .line 85
    :cond_b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b002e

    if-ne v0, v1, :cond_c

    .line 86
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0

    .line 87
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b002f

    if-ne v0, v1, :cond_d

    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0

    .line 89
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0031

    if-ne v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v2, 0x7f03000a

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->setContentView(I)V

    .line 40
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->initView()V

    .line 41
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->initListener()V

    .line 43
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 44
    .local v0, "dialogWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 45
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x13

    invoke-virtual {v0, v2}, Landroid/view/Window;->setGravity(I)V

    .line 46
    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 47
    const/16 v2, -0x32

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 48
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 49
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 55
    return-void
.end method
