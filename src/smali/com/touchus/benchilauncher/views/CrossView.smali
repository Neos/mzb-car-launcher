.class public Lcom/touchus/benchilauncher/views/CrossView;
.super Landroid/view/View;
.source "CrossView.java"


# instance fields
.field private cp:Landroid/graphics/Point;

.field private drawLine:Z

.field private isCilkshizi:Ljava/lang/Boolean;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mLineA:[F

.field private mLineB:[F

.field private paint:Landroid/graphics/Paint;

.field public parantX:F

.field public parantY:F

.field sen:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->drawLine:Z

    .line 25
    const/16 v0, 0x32

    iput v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->sen:I

    .line 30
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->isCilkshizi:Ljava/lang/Boolean;

    .line 35
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CrossView;->mContext:Landroid/content/Context;

    .line 37
    invoke-virtual {p0, p2}, Lcom/touchus/benchilauncher/views/CrossView;->init(Landroid/view/View;)V

    .line 38
    return-void
.end method


# virtual methods
.method public getPravX()F
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    return v0
.end method

.method public getPravY()F
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    return v0
.end method

.method public init(Landroid/view/View;)V
    .locals 8
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v3, 0x40000000    # 2.0f

    .line 41
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    if-nez v0, :cond_1

    .line 42
    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    .line 43
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    .line 45
    :cond_1
    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/views/CrossView;->setFocusable(Z)V

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [F

    aput v5, v0, v7

    iget v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    div-float/2addr v1, v3

    aput v1, v0, v4

    const/4 v1, 0x2

    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    div-float/2addr v2, v3

    aput v2, v0, v1

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    .line 50
    const/4 v0, 0x4

    new-array v0, v0, [F

    iget v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    div-float/2addr v1, v3

    aput v1, v0, v7

    iget v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    aput v1, v0, v4

    const/4 v1, 0x2

    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v5, v0, v1

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    .line 52
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iget v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    div-float/2addr v1, v3

    add-float/2addr v1, v6

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 53
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iget v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    div-float/2addr v1, v3

    add-float/2addr v1, v6

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 55
    return-void
.end method

.method public onCilkCrossView(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f000000    # 0.5f

    .line 153
    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    div-float/2addr v2, v5

    mul-int/lit8 v3, p1, 0x7

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v0, v2

    .line 154
    .local v0, "X":I
    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    div-float/2addr v2, v5

    mul-int/lit8 v3, p2, 0x9

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v1, v2

    .line 155
    .local v1, "Y":I
    int-to-float v2, v0

    iget v3, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 156
    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    add-float/2addr v2, v4

    float-to-int v0, v2

    .line 161
    :cond_0
    :goto_0
    int-to-float v2, v1

    iget v3, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 162
    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    add-float/2addr v2, v4

    float-to-int v1, v2

    .line 168
    :cond_1
    :goto_1
    iput-boolean v6, p0, Lcom/touchus/benchilauncher/views/CrossView;->drawLine:Z

    .line 169
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iput v0, v2, Landroid/graphics/Point;->x:I

    .line 170
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iput v1, v2, Landroid/graphics/Point;->y:I

    .line 172
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    int-to-float v3, v1

    aput v3, v2, v6

    .line 173
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    const/4 v3, 0x3

    int-to-float v4, v1

    aput v4, v2, v3

    .line 174
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    const/4 v3, 0x0

    int-to-float v4, v0

    aput v4, v2, v3

    .line 175
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    const/4 v3, 0x2

    int-to-float v4, v0

    aput v4, v2, v3

    .line 176
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CrossView;->invalidate()V

    .line 177
    return-void

    .line 157
    :cond_2
    if-gtz v0, :cond_0

    .line 158
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :cond_3
    if-gtz v1, :cond_1

    .line 164
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 62
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 63
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setDither(Z)V

    .line 64
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 68
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->drawLine:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    aget v1, v0, v8

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    aget v2, v0, v6

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    aget v3, v0, v9

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    aget v4, v0, v10

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 74
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    aget v1, v0, v8

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    aget v2, v0, v6

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    aget v3, v0, v9

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    aget v4, v0, v10

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->isCilkshizi:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CrossView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 80
    const v1, 0x7f020213

    .line 79
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mBitmap:Landroid/graphics/Bitmap;

    .line 86
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    add-int/lit8 v1, v1, -0x10

    int-to-float v1, v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-int/lit8 v2, v2, -0x11

    int-to-float v2, v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/CrossView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 87
    return-void

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CrossView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 84
    const v1, 0x7f020214

    .line 83
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/CrossView;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 92
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 93
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    add-float/2addr v2, v3

    float-to-int v0, v2

    .line 99
    .local v0, "X":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    add-float/2addr v2, v3

    float-to-int v1, v2

    .line 101
    .local v1, "Y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 138
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/CrossView;->invalidate()V

    .line 139
    return v5

    .line 105
    :pswitch_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->isCilkshizi:Ljava/lang/Boolean;

    .line 107
    const-string v2, "XY: "

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "X"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-----"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Y"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    int-to-float v2, v0

    iget v3, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 109
    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantX:F

    float-to-int v0, v2

    .line 114
    :cond_0
    :goto_1
    int-to-float v2, v1

    iget v3, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 115
    iget v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->parantY:F

    float-to-int v1, v2

    .line 121
    :cond_1
    :goto_2
    iput-boolean v6, p0, Lcom/touchus/benchilauncher/views/CrossView;->drawLine:Z

    .line 122
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iput v0, v2, Landroid/graphics/Point;->x:I

    .line 123
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->cp:Landroid/graphics/Point;

    iput v1, v2, Landroid/graphics/Point;->y:I

    .line 125
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    int-to-float v3, v1

    aput v3, v2, v5

    .line 126
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineA:[F

    const/4 v3, 0x3

    int-to-float v4, v1

    aput v4, v2, v3

    .line 127
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    int-to-float v3, v0

    aput v3, v2, v6

    .line 128
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->mLineB:[F

    const/4 v3, 0x2

    int-to-float v4, v0

    aput v4, v2, v3

    goto :goto_0

    .line 110
    :cond_2
    if-gtz v0, :cond_0

    .line 111
    const/4 v0, 0x0

    goto :goto_1

    .line 116
    :cond_3
    if-gtz v1, :cond_1

    .line 117
    const/4 v1, 0x0

    goto :goto_2

    .line 133
    :pswitch_1
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/views/CrossView;->drawLine:Z

    .line 134
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->isCilkshizi:Ljava/lang/Boolean;

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setIsCilkshizi(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/CrossView;->isCilkshizi:Ljava/lang/Boolean;

    .line 181
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/CrossView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    .line 182
    .local v0, "spUtilK":Lcom/touchus/benchilauncher/utils/SpUtilK;
    const-string v1, "isCilkshizi"

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/CrossView;->isCilkshizi:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 183
    return-void
.end method
