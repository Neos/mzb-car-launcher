.class public Lcom/touchus/benchilauncher/views/YiBiaoView;
.super Landroid/view/View;
.source "YiBiaoView.java"


# instance fields
.field private angle:F

.field private bgImage:Landroid/graphics/drawable/Drawable;

.field private isChange:Z

.field private neiYuanView:Landroid/graphics/drawable/Drawable;

.field private paint:Landroid/graphics/Paint;

.field private pointView:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/YiBiaoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/touchus/benchilauncher/views/YiBiaoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->mContext:Landroid/content/Context;

    .line 48
    sget-object v1, Lcom/touchus/benchilauncher/R$styleable;->MyYibiaoStyleable:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    .local v0, "ta":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->bgImage:Landroid/graphics/drawable/Drawable;

    .line 50
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->pointView:Landroid/graphics/drawable/Drawable;

    .line 51
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->neiYuanView:Landroid/graphics/drawable/Drawable;

    .line 52
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 53
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->paint:Landroid/graphics/Paint;

    .line 54
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->paint:Landroid/graphics/Paint;

    const-string v2, "#000000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 56
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 57
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 59
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->isChange:Z

    .line 119
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->isChange:Z

    .line 125
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 68
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v5, v8, 0x2

    .line 69
    .local v5, "viewCenterX":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getBottom()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v6, v8, 0x2

    .line 71
    .local v6, "viewCenterY":I
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->bgImage:Landroid/graphics/drawable/Drawable;

    .line 72
    .local v0, "dial":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 73
    .local v1, "h":I
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 75
    .local v7, "w":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    if-lt v8, v7, :cond_0

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getBottom()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    if-ge v8, v1, :cond_1

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    int-to-float v8, v8

    int-to-float v9, v7

    div-float/2addr v8, v9

    .line 77
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getBottom()I

    move-result v9

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->getTop()I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v9, v9

    int-to-float v10, v1

    div-float/2addr v9, v10

    .line 76
    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 78
    .local v4, "scale":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 79
    int-to-float v8, v5

    int-to-float v9, v6

    invoke-virtual {p1, v4, v4, v8, v9}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 81
    .end local v4    # "scale":F
    :cond_1
    iget-boolean v8, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->isChange:Z

    if-eqz v8, :cond_2

    .line 82
    div-int/lit8 v8, v7, 0x2

    sub-int v8, v5, v8

    .line 83
    div-int/lit8 v9, v1, 0x2

    sub-int v9, v6, v9

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v5

    .line 84
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v6

    .line 82
    invoke-virtual {v0, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 86
    :cond_2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 90
    iget v8, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->angle:F

    int-to-float v9, v5

    int-to-float v10, v6

    invoke-virtual {p1, v8, v9, v10}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 91
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->pointView:Landroid/graphics/drawable/Drawable;

    .line 92
    .local v2, "mHour":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 93
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 94
    iget-boolean v8, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->isChange:Z

    if-eqz v8, :cond_3

    .line 95
    div-int/lit8 v8, v7, 0x2

    sub-int v8, v5, v8

    .line 96
    div-int/lit8 v9, v1, 0x2

    sub-int v9, v6, v9

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v5

    .line 97
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v6

    .line 95
    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 99
    :cond_3
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 100
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 101
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 103
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->neiYuanView:Landroid/graphics/drawable/Drawable;

    .line 104
    .local v3, "neiYuan":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 105
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 106
    div-int/lit8 v8, v7, 0x2

    sub-int v8, v5, v8

    .line 107
    div-int/lit8 v9, v1, 0x2

    sub-int v9, v6, v9

    div-int/lit8 v10, v7, 0x2

    add-int/2addr v10, v5

    .line 108
    div-int/lit8 v11, v1, 0x2

    add-int/2addr v11, v6

    .line 106
    invoke-virtual {v3, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 109
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 110
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 111
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 113
    return-void
.end method

.method public setAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 61
    iput p1, p0, Lcom/touchus/benchilauncher/views/YiBiaoView;->angle:F

    .line 62
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/YiBiaoView;->postInvalidate()V

    .line 63
    return-void
.end method
