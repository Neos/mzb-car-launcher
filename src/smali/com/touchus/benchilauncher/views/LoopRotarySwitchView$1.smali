.class Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;
.super Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;
.source "LoopRotarySwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection:[I


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection()[I
    .locals 3

    .prologue
    .line 157
    sget-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->values()[Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->left:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->right:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V
    .locals 0
    .param p2, "$anonymous0"    # I

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 157
    invoke-direct {p0, p2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchViewHandler;-><init>(I)V

    return-void
.end method


# virtual methods
.method public doScroll()V
    .locals 5

    .prologue
    .line 162
    :try_start_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$0(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    const/4 v1, 0x0

    .line 164
    .local v1, "perAngle":I
    invoke-static {}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection()[I

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$1(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$AutoScrollDirection;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 172
    :goto_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$2(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)F

    move-result v2

    const/high16 v3, 0x43b40000    # 360.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 173
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$3(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;F)V

    .line 175
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$2(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)F

    move-result v3

    int-to-float v4, v1

    add-float/2addr v3, v4

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$4(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;FLjava/lang/Runnable;)V

    .line 180
    .end local v1    # "perAngle":I
    :cond_1
    :goto_1
    return-void

    .line 166
    .restart local v1    # "perAngle":I
    :pswitch_0
    const/16 v2, 0x168

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$0(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v3

    div-int v1, v2, v3

    .line 167
    goto :goto_0

    .line 169
    :pswitch_1
    const/16 v2, -0x168

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$1;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$0(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v3

    div-int v1, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    .end local v1    # "perAngle":I
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
