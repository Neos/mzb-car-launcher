.class public Lcom/touchus/benchilauncher/views/SystemSetDialog;
.super Landroid/app/Dialog;
.source "SystemSetDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Dialog;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field currentSelectIndex:I

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mHandler:Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

.field private mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

.field private mSetListView:Landroid/widget/ListView;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

.field private mSysListView:Landroid/widget/ListView;

.field private setInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/SettingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private sysInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/SettingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private type:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    .line 194
    new-instance v0, Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/SystemSetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    .line 194
    new-instance v0, Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/SystemSetDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    .line 46
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 47
    return-void
.end method

.method private initData()V
    .locals 8

    .prologue
    const v7, 0x7f0700a3

    const v3, 0x7f0700a2

    const v6, 0x7f0700a1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 77
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 78
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 79
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 78
    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 81
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    .line 82
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SystemSetDialog mApp.ismix = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 83
    const-string v2, ",mApp.isAirhide = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    new-instance v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 86
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    invoke-direct {v1, v2, v5, v3}, Lcom/touchus/benchilauncher/bean/SettingInfo;-><init>(Ljava/lang/String;ZZ)V

    .line 85
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    new-instance v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 89
    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 90
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    invoke-direct {v1, v2, v5, v3}, Lcom/touchus/benchilauncher/bean/SettingInfo;-><init>(Ljava/lang/String;ZZ)V

    .line 88
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    new-instance v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    invoke-direct {v1, v2, v5, v3}, Lcom/touchus/benchilauncher/bean/SettingInfo;-><init>(Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    new-instance v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 96
    const v3, 0x7f07008e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/touchus/benchilauncher/bean/SettingInfo;-><init>(Ljava/lang/String;Z)V

    .line 95
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    new-instance v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 98
    const v3, 0x7f0700a4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/touchus/benchilauncher/bean/SettingInfo;-><init>(Ljava/lang/String;Z)V

    .line 97
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    new-instance v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 100
    const v3, 0x7f0700a5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/touchus/benchilauncher/bean/SettingInfo;-><init>(Ljava/lang/String;Z)V

    .line 99
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    return-void
.end method

.method private initView()V
    .locals 3

    .prologue
    .line 61
    const v0, 0x7f0b0077

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetListView:Landroid/widget/ListView;

    .line 62
    new-instance v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    .line 63
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 66
    const v0, 0x7f0b0078

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysListView:Landroid/widget/ListView;

    .line 67
    new-instance v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    .line 68
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 72
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 73
    return-void
.end method

.method private pressItem(I)V
    .locals 9
    .param p1, "position"    # I

    .prologue
    const/4 v8, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 135
    iput p1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    .line 137
    const-string v3, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SystemSetDialog type = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",mApp.isOriginalKeyOpen = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 138
    iget-object v7, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v7, v7, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 137
    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v3, v5, :cond_a

    .line 140
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/SettingInfo;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/SettingInfo;->getItem()Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "itemString":Ljava/lang/String;
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    invoke-virtual {v3, v1, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 142
    .local v0, "iCheck":Z
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 143
    const v6, 0x7f0700a1

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 142
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 143
    if-eqz v3, :cond_3

    .line 144
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_1

    move v3, v4

    :goto_0
    iput-boolean v3, v6, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    .line 145
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v6, v6, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    invoke-virtual {v3, v1, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 146
    sget-object v6, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    if-eqz v3, :cond_2

    move v3, v5

    .line 147
    :goto_1
    int-to-byte v3, v3

    .line 146
    aput-byte v3, v6, v4

    .line 158
    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/SettingInfo;

    if-eqz v0, :cond_9

    :goto_3
    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/bean/SettingInfo;->setChecked(Z)V

    .line 159
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v3

    sget-object v4, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v3, v4}, Lcom/backaudio/android/driver/Mainboard;->sendStoreDataToMcu([B)V

    .line 160
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v3, p1}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 161
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v3, v8}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 185
    .end local v0    # "iCheck":Z
    :goto_4
    return-void

    .restart local v0    # "iCheck":Z
    :cond_1
    move v3, v5

    .line 144
    goto :goto_0

    :cond_2
    move v3, v4

    .line 147
    goto :goto_1

    .line 148
    :cond_3
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 149
    const v6, 0x7f0700a2

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 148
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 149
    if-eqz v3, :cond_6

    .line 150
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_4

    move v3, v4

    :goto_5
    iput-boolean v3, v6, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 151
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v6, v6, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    invoke-virtual {v3, v1, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 152
    sget-object v6, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v7, 0x3

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v3, :cond_5

    move v3, v5

    :goto_6
    int-to-byte v3, v3

    aput-byte v3, v6, v7

    goto :goto_2

    :cond_4
    move v3, v5

    .line 150
    goto :goto_5

    :cond_5
    move v3, v4

    .line 152
    goto :goto_6

    .line 153
    :cond_6
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    const v6, 0x7f0700a3

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 154
    iget-object v6, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    if-eqz v0, :cond_7

    move v3, v4

    :goto_7
    iput-boolean v3, v6, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    .line 155
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v6, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v6, v6, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    invoke-virtual {v3, v1, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 156
    sget-object v6, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/16 v7, 0x8

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    if-eqz v3, :cond_8

    move v3, v5

    :goto_8
    int-to-byte v3, v3

    aput-byte v3, v6, v7

    goto/16 :goto_2

    :cond_7
    move v3, v5

    .line 154
    goto :goto_7

    :cond_8
    move v3, v4

    .line 156
    goto :goto_8

    :cond_9
    move v4, v5

    .line 158
    goto :goto_3

    .line 163
    .end local v0    # "iCheck":Z
    .end local v1    # "itemString":Ljava/lang/String;
    :cond_a
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/SettingInfo;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/SettingInfo;->getItem()Ljava/lang/String;

    move-result-object v1

    .line 164
    .restart local v1    # "itemString":Ljava/lang/String;
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 165
    const v4, 0x7f0700a4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 164
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 165
    if-eqz v3, :cond_c

    .line 167
    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->showRecoveryDlg(Ljava/lang/String;)V

    .line 181
    :cond_b
    :goto_9
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v3, v8}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 182
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v3, p1}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    goto/16 :goto_4

    .line 168
    :cond_c
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 169
    const v4, 0x7f0700a5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 168
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 169
    if-eqz v3, :cond_d

    .line 171
    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->showRecoveryDlg(Ljava/lang/String;)V

    goto :goto_9

    .line 172
    :cond_d
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 173
    const v4, 0x7f07008e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 172
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 173
    if-eqz v3, :cond_b

    .line 175
    new-instance v2, Lcom/touchus/benchilauncher/views/FactorySetDialog;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 176
    const v4, 0x7f080006

    .line 175
    invoke-direct {v2, v3, v4}, Lcom/touchus/benchilauncher/views/FactorySetDialog;-><init>(Landroid/content/Context;I)V

    .line 177
    .local v2, "wifiialog":Lcom/touchus/benchilauncher/views/FactorySetDialog;
    const/16 v3, 0x1f4

    iput v3, v2, Lcom/touchus/benchilauncher/views/FactorySetDialog;->width:I

    .line 178
    const/16 v3, 0xc8

    iput v3, v2, Lcom/touchus/benchilauncher/views/FactorySetDialog;->height:I

    .line 179
    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/FactorySetDialog;->show()V

    goto :goto_9
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "currentSelectIndex"    # I

    .prologue
    const/4 v2, -0x1

    .line 252
    iget v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 253
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 254
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSetAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    .line 257
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSysAdapter:Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->setSeclectIndex(I)V

    goto :goto_0
.end method

.method private showRecoveryDlg(Ljava/lang/String;)V
    .locals 3
    .param p1, "itemString"    # Ljava/lang/String;

    .prologue
    .line 188
    new-instance v0, Lcom/touchus/benchilauncher/views/SystemOperateDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    .line 189
    const v2, 0x7f080006

    .line 188
    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;-><init>(Landroid/content/Context;I)V

    .line 190
    .local v0, "dialog":Lcom/touchus/benchilauncher/views/SystemOperateDialog;
    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->setTitle(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/SystemOperateDialog;->show()V

    .line 192
    return-void
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 213
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_2

    .line 214
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 215
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 216
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_0

    .line 217
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_5

    .line 218
    :cond_0
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v2, v4, :cond_3

    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    .line 219
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    .line 228
    :cond_1
    :goto_0
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->seclectTrue(I)V

    .line 249
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_1
    return-void

    .line 220
    .restart local v0    # "idriverCode":B
    .restart local v1    # "temp":Landroid/os/Bundle;
    :cond_3
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v2, v4, :cond_4

    .line 221
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_4

    .line 222
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    .line 223
    const/4 v2, 0x0

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    goto :goto_0

    .line 224
    :cond_4
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v2, v5, :cond_1

    .line 225
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->sysInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    .line 226
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    goto :goto_0

    .line 229
    :cond_5
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 230
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_6

    .line 231
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_a

    .line 232
    :cond_6
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v2, v5, :cond_8

    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    if-lez v2, :cond_8

    .line 233
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    .line 240
    :cond_7
    :goto_2
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->seclectTrue(I)V

    goto :goto_1

    .line 234
    :cond_8
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v2, v5, :cond_9

    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    if-nez v2, :cond_9

    .line 235
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    .line 236
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    goto :goto_2

    .line 237
    :cond_9
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    if-ne v2, v4, :cond_7

    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    if-lez v2, :cond_7

    .line 238
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    goto :goto_2

    .line 241
    :cond_a
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_b

    .line 242
    iget v2, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->currentSelectIndex:I

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->pressItem(I)V

    goto/16 :goto_1

    .line 243
    :cond_b
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_c

    .line 244
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_c

    .line 245
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_2

    .line 246
    :cond_c
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->dismiss()V

    goto/16 :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->setContentView(I)V

    .line 52
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 53
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 54
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->initData()V

    .line 55
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->initView()V

    .line 57
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 58
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 132
    :goto_0
    return-void

    .line 123
    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    .line 124
    invoke-direct {p0, p3}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->pressItem(I)V

    goto :goto_0

    .line 127
    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->type:I

    .line 128
    invoke-direct {p0, p3}, Lcom/touchus/benchilauncher/views/SystemSetDialog;->pressItem(I)V

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x7f0b0077
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 106
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 107
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SystemSetDialog;->mHandler:Lcom/touchus/benchilauncher/views/SystemSetDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 113
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 114
    return-void
.end method
