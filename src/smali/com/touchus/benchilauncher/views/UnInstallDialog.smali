.class public Lcom/touchus/benchilauncher/views/UnInstallDialog;
.super Landroid/app/Dialog;
.source "UnInstallDialog.java"


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private appName:Ljava/lang/String;

.field private btnCancel:Landroid/widget/Button;

.field private btnConfirm:Landroid/widget/Button;

.field public height:I

.field private mContext:Landroid/content/Context;

.field private onClickListener:Landroid/view/View$OnClickListener;

.field private pkgName:Ljava/lang/String;

.field public width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 19
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->width:I

    .line 20
    const/16 v0, 0xc8

    iput v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->height:I

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->pkgName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->appName:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/touchus/benchilauncher/views/UnInstallDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/UnInstallDialog$1;-><init>(Lcom/touchus/benchilauncher/views/UnInstallDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->onClickListener:Landroid/view/View$OnClickListener;

    .line 32
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->mContext:Landroid/content/Context;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 19
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->width:I

    .line 20
    const/16 v0, 0xc8

    iput v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->height:I

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->pkgName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->appName:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/touchus/benchilauncher/views/UnInstallDialog$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/UnInstallDialog$1;-><init>(Lcom/touchus/benchilauncher/views/UnInstallDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->onClickListener:Landroid/view/View$OnClickListener;

    .line 38
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->mContext:Landroid/content/Context;

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 40
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/UnInstallDialog;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->clickEvent(Landroid/view/View;)V

    return-void
.end method

.method private clickEvent(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 88
    :goto_0
    return-void

    .line 75
    :sswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnCancel:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnConfirm:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 77
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->unstallApp()V

    .line 78
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->dismiss()V

    goto :goto_0

    .line 81
    :sswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnCancel:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnConfirm:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->dismiss()V

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0063 -> :sswitch_0
        0x7f0b0083 -> :sswitch_1
    .end sparse-switch
.end method

.method private unstallApp()V
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->pkgName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    const-string v1, "uninstall"

    const-string v2, "pkgName is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 100
    .local v0, "uninstall_intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.DELETE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 102
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 108
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 47
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v1, 0x7f030019

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->setContentView(I)V

    .line 49
    const v1, 0x7f0b007e

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 50
    .local v0, "titleName":Landroid/widget/TextView;
    const v1, 0x7f0b0063

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnConfirm:Landroid/widget/Button;

    .line 51
    const v1, 0x7f0b0083

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnCancel:Landroid/widget/Button;

    .line 52
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnConfirm:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnCancel:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnConfirm:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f07005e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->appName:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 56
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnCancel:Landroid/widget/Button;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f07005a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->btnCancel:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setSelected(Z)V

    .line 61
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/UnInstallDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget v2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->width:I

    iget v3, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->height:I

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    .line 62
    return-void
.end method

.method public setPkgName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->pkgName:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/touchus/benchilauncher/views/UnInstallDialog;->appName:Ljava/lang/String;

    .line 28
    return-void
.end method
