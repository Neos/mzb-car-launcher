.class public Lcom/touchus/benchilauncher/views/LanguageDialog;
.super Landroid/app/Dialog;
.source "LanguageDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private languageType:Landroid/widget/RadioGroup;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mHandler:Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

.field private mIDRIVERENUM:B

.field private mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private selectPos:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    .line 123
    new-instance v0, Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/LanguageDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mHandler:Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    .line 123
    new-instance v0, Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;-><init>(Lcom/touchus/benchilauncher/views/LanguageDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mHandler:Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    .line 41
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->context:Landroid/content/Context;

    .line 42
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V
    .locals 0

    .prologue
    .line 25
    iput p1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/LanguageDialog;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    return v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/LanguageDialog;I)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->seclectTrue(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/LanguageDialog;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->press()V

    return-void
.end method

.method private initData()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->context:Landroid/content/Context;

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 113
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mMMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "languageType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 117
    return-void
.end method

.method private initSetup()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/touchus/benchilauncher/views/LanguageDialog$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/LanguageDialog$1;-><init>(Lcom/touchus/benchilauncher/views/LanguageDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 103
    iget v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->seclectTrue(I)V

    .line 105
    return-void
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 108
    const v0, 0x7f0b004e

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    .line 109
    return-void
.end method

.method private press()V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iLanguageType:I

    .line 173
    iget v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    packed-switch v0, :pswitch_data_0

    .line 201
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "languageType"

    iget v2, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 202
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->sendLanguageSetToMcu(I)V

    .line 203
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->getStoreDataFromMcu()V

    .line 204
    return-void

    .line 175
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 178
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 181
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 184
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Ljava/util/Locale;

    const-string v2, "de"

    const-string v3, "DE"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 187
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Ljava/util/Locale;

    const-string v2, "pl"

    const-string v3, "PL"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 190
    :pswitch_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Ljava/util/Locale;

    const-string v2, "sr"

    const-string v3, "RS"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 193
    :pswitch_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Ljava/util/Locale;

    const-string v2, "tr"

    const-string v3, "TR"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 196
    :pswitch_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    new-instance v1, Ljava/util/Locale;

    const-string v2, "sv"

    const-string v3, "SE"

    invoke-direct {v1, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->updateLanguage(Ljava/util/Locale;)V

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private seclectTrue(I)V
    .locals 3
    .param p1, "seclectCuttun"    # I

    .prologue
    .line 207
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 214
    return-void

    .line 208
    :cond_0
    if-ne p1, v0, :cond_1

    .line 209
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 207
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 142
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x1771

    if-ne v1, v2, :cond_2

    .line 143
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 144
    .local v0, "temp":Landroid/os/Bundle;
    const-string v1, "idriver_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    iput-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    .line 145
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_0

    .line 146
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_3

    .line 147
    :cond_0
    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->languageType:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 148
    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    .line 150
    :cond_1
    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->seclectTrue(I)V

    .line 169
    .end local v0    # "temp":Landroid/os/Bundle;
    :cond_2
    :goto_0
    return-void

    .line 151
    .restart local v0    # "temp":Landroid/os/Bundle;
    :cond_3
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 152
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_4

    .line 153
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_6

    .line 154
    :cond_4
    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    if-lez v1, :cond_5

    .line 155
    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    .line 157
    :cond_5
    iget v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->selectPos:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/views/LanguageDialog;->seclectTrue(I)V

    goto :goto_0

    .line 159
    :cond_6
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_7

    .line 160
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->press()V

    .line 161
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->dismiss()V

    goto :goto_0

    .line 162
    :cond_7
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_8

    .line 163
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_8

    .line 164
    iget-byte v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mIDRIVERENUM:B

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 165
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    .line 164
    if-ne v1, v2, :cond_2

    .line 166
    :cond_8
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->dismiss()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->setContentView(I)V

    .line 48
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 49
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->initView()V

    .line 50
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->initSetup()V

    .line 51
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/LanguageDialog;->initData()V

    .line 52
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mHandler:Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 56
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 57
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mHandler:Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 62
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 63
    return-void
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LanguageDialog;->mHandler:Lcom/touchus/benchilauncher/views/LanguageDialog$ReverseialogHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 121
    return-void
.end method
