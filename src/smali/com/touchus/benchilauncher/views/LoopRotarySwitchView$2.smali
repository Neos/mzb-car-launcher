.class Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "LoopRotarySwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->getGeomeryController()Landroid/view/GestureDetector$SimpleOnGestureListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 254
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 258
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$2(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)F

    move-result v1

    float-to-double v2, v1

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$6(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v1

    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 259
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$7(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    div-float v1, p3, v1

    float-to-double v6, v1

    .line 258
    mul-double/2addr v4, v6

    .line 260
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$6(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v1

    int-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    .line 261
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$7(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-float v1, v1

    div-float v1, p4, v1

    float-to-double v8, v1

    .line 260
    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v1, v2

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$3(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;F)V

    .line 262
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$2;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$8(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V

    .line 263
    const/4 v0, 0x1

    return v0
.end method
