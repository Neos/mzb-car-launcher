.class public Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
.super Ljava/lang/Object;
.source "FloatSystemCallDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;,
        Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;,
        Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$PhoneNumPraseTask;
    }
.end annotation


# static fields
.field private static instance:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

.field private static isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static logger:Lorg/slf4j/Logger;

.field private static showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private callingPhoneNumber:Ljava/lang/String;

.field private leftButton:Landroid/widget/ImageButton;

.field private mCallFloatView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

.field private params:Landroid/view/WindowManager$LayoutParams;

.field private phoneLayout:Landroid/widget/LinearLayout;

.field private rightButton:Landroid/widget/ImageButton;

.field private talkingPhoneNumber:Landroid/widget/TextView;

.field private talkingphoneStatus:Landroid/widget/TextView;

.field private wm:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-class v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 39
    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->instance:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 53
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v1

    .line 52
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 59
    const-string v0, "-1"

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->callingPhoneNumber:Ljava/lang/String;

    .line 480
    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;-><init>(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mHandler:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    .line 72
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    const-string v1, "FloatSystemCallDialog FloatSystemCallDialog()"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingphoneStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method public static getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->instance:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->instance:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    .line 79
    :cond_0
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->instance:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    return-object v0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 441
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 442
    .local v0, "bundle":Landroid/os/Bundle;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 444
    :pswitch_0
    const-string v2, "idriver_enum"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    .line 445
    .local v1, "mIDRIVERENUM":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_1

    .line 446
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v2, v3, :cond_0

    .line 447
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->performClick()Z

    goto :goto_0

    .line 449
    :cond_1
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HANG_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_3

    .line 450
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v2, v3, :cond_2

    .line 451
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v2, v3, :cond_2

    .line 452
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v2, v3, :cond_0

    .line 453
    :cond_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->performClick()Z

    goto :goto_0

    .line 455
    :cond_3
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 456
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_4

    .line 457
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_5

    .line 458
    :cond_4
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 459
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 460
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 462
    :cond_5
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 463
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_6

    .line 464
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_7

    .line 465
    :cond_6
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 466
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 467
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 469
    :cond_7
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_0

    .line 470
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 471
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->performClick()Z

    goto/16 :goto_0

    .line 472
    :cond_8
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 473
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->performClick()Z

    goto/16 :goto_0

    .line 442
    :pswitch_data_0
    .packed-switch 0x1771
        :pswitch_0
    .end packed-switch
.end method

.method private modifyName()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 219
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingPhoneNumber:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setButtonState(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 182
    if-eqz p1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 184
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 185
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 186
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 194
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 189
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 190
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 191
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0
.end method


# virtual methods
.method public clearView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 222
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 235
    :goto_0
    return-void

    .line 226
    :cond_0
    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 228
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :cond_1
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 234
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public dissShow()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 238
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 274
    :goto_0
    return-void

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mHandler:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 245
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    .line 246
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->HIDEN:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 247
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    const-string v2, "FloatSystemCallDialog HIDEN  return"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 249
    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 250
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 251
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_2
    :goto_1
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 257
    iput-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    goto :goto_0

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 260
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 261
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->histalkflag:Z

    .line 262
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->accFlag:Z

    .line 263
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->exitsource()V

    .line 265
    :try_start_1
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 266
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 267
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 272
    :cond_4
    :goto_2
    iput-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 273
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0

    .line 269
    :catch_1
    move-exception v0

    .line 270
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public freshCallingTime()V
    .locals 6

    .prologue
    .line 200
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 203
    :cond_0
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TIME_FLAG"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 203
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingphoneStatus:Landroid/widget/TextView;

    .line 206
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f070033

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 207
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget v4, v4, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/touchus/publicutils/utils/TimeUtils;->secToTimeString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 205
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->updateTalkStatus()V

    goto :goto_0
.end method

.method public getCallingPhonenumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->callingPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getShowStatus()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;
    .locals 2

    .prologue
    .line 83
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->values()[Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hiden()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 277
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v1, :cond_2

    .line 294
    :cond_1
    :goto_0
    return-void

    .line 283
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mHandler:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 284
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    .line 285
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->HIDEN:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 287
    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 289
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isDestory()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 338
    const/4 v0, 0x1

    .line 340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 389
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 433
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 391
    :pswitch_1
    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v2, 0x584

    if-ne v1, v2, :cond_0

    .line 392
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->answerCalling()V

    .line 393
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 398
    :pswitch_2
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->mContext:Landroid/content/Context;

    .line 399
    const-string v2, "com.touchus.benchilauncher.Launcher"

    .line 398
    invoke-static {v1, v2}, Lcom/touchus/publicutils/utils/UtilTools;->isForeground(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 399
    if-nez v1, :cond_1

    .line 400
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$1;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$1;-><init>(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 407
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 409
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/16 v2, 0x3f5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 414
    :pswitch_3
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v1

    if-nez v1, :cond_0

    .line 416
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    const-string v2, "FloatSystemCallDialog cutdownCurrentCalling"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 418
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->cutdownCurrentCalling()V

    .line 419
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->dissShow()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$2;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$2;-><init>(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;)V

    .line 429
    const-wide/16 v4, 0x3e8

    .line 424
    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0021
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized setCallingPhonenumber(Ljava/lang/String;)V
    .locals 1
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->callingPhoneNumber:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setShowStatus(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;)V
    .locals 3
    .param p1, "value"    # Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    .prologue
    .line 87
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FloatSystemCallDialog setShowStatus showingStatus: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 90
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FloatSystemCallDialog setShowStatus showingStatus: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getShowStatus()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 297
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    const-string v2, "FloatSystemCallDialog show()"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 299
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    const-string v2, "FloatSystemCallDialog show  mCallFloatView = null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mHandler:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 303
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    .line 304
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->updateTalkStatus()V

    .line 306
    :try_start_0
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_1

    .line 307
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->modifyName()V

    goto :goto_0

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public showFloatCallView(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 104
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 106
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FloatSystemCallDialog showFloatCallView()  PhoneNumber="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 107
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 106
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 108
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    .line 109
    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 108
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    .line 110
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    .line 111
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 112
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 113
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0xeb

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 114
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x2bc

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 115
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/4 v3, 0x1

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 116
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x28

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 119
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d2

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 120
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v2, :cond_0

    .line 122
    :try_start_1
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 123
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 127
    :goto_0
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 129
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 130
    const v3, 0x7f030009

    const/4 v4, 0x0

    .line 129
    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 132
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 133
    const v3, 0x7f0b0024

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 132
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingPhoneNumber:Landroid/widget/TextView;

    .line 134
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 135
    const v3, 0x7f0b0023

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 134
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingphoneStatus:Landroid/widget/TextView;

    .line 136
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 137
    const v3, 0x7f0b0025

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 136
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    .line 138
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 139
    const v3, 0x7f0b0022

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 138
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    .line 140
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    .line 141
    const v3, 0x7f0b0021

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 140
    iput-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->phoneLayout:Landroid/widget/LinearLayout;

    .line 142
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_1

    .line 143
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->params:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isAdded:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 146
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 147
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingPhoneNumber:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    .line 148
    const v4, 0x7f070038

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 147
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :goto_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->leftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->rightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->phoneLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 161
    .end local v1    # "metrics":Landroid/util/DisplayMetrics;
    :goto_2
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v2, v3, :cond_5

    .line 162
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingphoneStatus:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    .line 163
    const v5, 0x7f070035

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 162
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    :cond_2
    :goto_3
    sget v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v3, 0x584

    if-ne v2, v3, :cond_6

    .line 171
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 172
    sget-boolean v2, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setButtonState(Z)V

    .line 176
    :cond_3
    :goto_4
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mHandler:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 177
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v6, v2, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    .line 178
    return-void

    .line 124
    .restart local v1    # "metrics":Landroid/util/DisplayMetrics;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 155
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "metrics":Landroid/util/DisplayMetrics;
    :catch_1
    move-exception v0

    .line 156
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FloatSystemCallDialog Exception:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 158
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v3, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_2

    .line 150
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "metrics":Landroid/util/DisplayMetrics;
    :cond_4
    :try_start_4
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingPhoneNumber:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    .line 164
    .end local v1    # "metrics":Landroid/util/DisplayMetrics;
    :cond_5
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v2, v3, :cond_2

    .line 165
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->talkingphoneStatus:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mContext:Landroid/content/Context;

    .line 166
    const v5, 0x7f070036

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 165
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    invoke-direct {p0, v6}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setButtonState(Z)V

    goto :goto_3

    .line 173
    :cond_6
    sget v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v3, 0x585

    if-ne v2, v3, :cond_3

    .line 174
    invoke-direct {p0, v6}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setButtonState(Z)V

    goto :goto_4
.end method

.method public updateTalkStatus()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->mCallFloatView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 334
    :goto_0
    return-void

    .line 320
    :cond_0
    sget-boolean v0, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-eqz v0, :cond_1

    .line 321
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setButtonState(Z)V

    .line 322
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 325
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setButtonState(Z)V

    .line 326
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v1, 0x584

    if-ne v0, v1, :cond_2

    .line 327
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->INCOMING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 328
    :cond_2
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v1, 0x585

    if-ne v0, v1, :cond_3

    .line 329
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->OUTCALLING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0

    .line 331
    :cond_3
    sget-object v0, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showingStatus:Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->DEFAULT:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_0
.end method
