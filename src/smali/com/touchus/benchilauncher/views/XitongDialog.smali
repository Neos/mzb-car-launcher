.class public Lcom/touchus/benchilauncher/views/XitongDialog;
.super Landroid/app/Dialog;
.source "XitongDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;
    }
.end annotation


# instance fields
.field private androidTview:Landroid/widget/TextView;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private canboxTview:Landroid/widget/TextView;

.field private clickCount:I

.field private clickLayout:Landroid/widget/LinearLayout;

.field private context:Landroid/content/Context;

.field public mHandler:Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

.field private mcuTview:Landroid/widget/TextView;

.field resetClickCountThread:Ljava/lang/Thread;

.field private snTview:Landroid/widget/TextView;

.field private systemTview:Landroid/widget/TextView;

.field private typeTview:Landroid/widget/TextView;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->url:Ljava/lang/String;

    .line 163
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/XitongDialog$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/XitongDialog$1;-><init>(Lcom/touchus/benchilauncher/views/XitongDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->resetClickCountThread:Ljava/lang/Thread;

    .line 195
    new-instance v0, Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/XitongDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mHandler:Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

    .line 43
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeResId"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->url:Ljava/lang/String;

    .line 163
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/XitongDialog$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/XitongDialog$1;-><init>(Lcom/touchus/benchilauncher/views/XitongDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->resetClickCountThread:Ljava/lang/Thread;

    .line 195
    new-instance v0, Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;-><init>(Lcom/touchus/benchilauncher/views/XitongDialog;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mHandler:Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

    .line 48
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/XitongDialog;I)V
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/views/XitongDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/views/XitongDialog;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/views/XitongDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->url:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/views/XitongDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->url:Ljava/lang/String;

    return-object v0
.end method

.method private showDialog()V
    .locals 3

    .prologue
    .line 184
    new-instance v0, Lcom/touchus/benchilauncher/views/FeedbackDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    const v2, 0x7f080006

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/FeedbackDialog;-><init>(Landroid/content/Context;I)V

    .line 185
    .local v0, "dialog":Lcom/touchus/benchilauncher/views/FeedbackDialog;
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    .line 186
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FeedbackDialog;->show()V

    .line 187
    return-void
.end method

.method private showDialog(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 171
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    const v2, 0x7f080006

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;-><init>(Landroid/content/Context;I)V

    .line 172
    .local v0, "dialog":Lcom/touchus/benchilauncher/views/UpdateDialog;
    iput p1, v0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    .line 173
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    .line 174
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->show()V

    .line 175
    return-void
.end method

.method private showDialog(ILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 177
    new-instance v0, Lcom/touchus/benchilauncher/views/UpdateDialog;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    const v2, 0x7f080006

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/UpdateDialog;-><init>(Landroid/content/Context;I)V

    .line 178
    .local v0, "dialog":Lcom/touchus/benchilauncher/views/UpdateDialog;
    iput p1, v0, Lcom/touchus/benchilauncher/views/UpdateDialog;->currentType:I

    .line 179
    iput-object p2, v0, Lcom/touchus/benchilauncher/views/UpdateDialog;->url:Ljava/lang/String;

    .line 180
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    .line 181
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/UpdateDialog;->show()V

    .line 182
    return-void
.end method


# virtual methods
.method public getVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SYSTEM\uff1aV"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "androidV":Ljava/lang/String;
    return-object v0
.end method

.method public handlerMsgUSB(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 214
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_2

    .line 215
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 216
    .local v1, "temp":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 217
    .local v0, "idriverCode":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_1

    .line 218
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_1

    .line 220
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_1

    .line 221
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_1

    .line 223
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_0

    .line 224
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v0, v2, :cond_0

    .line 225
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v0, v2, :cond_1

    .line 226
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/XitongDialog;->dismiss()V

    .line 236
    .end local v0    # "idriverCode":B
    .end local v1    # "temp":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return-void

    .line 228
    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x46b

    if-ne v2, v3, :cond_3

    .line 229
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mcuTview:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MCU\uff1a"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->curMcuVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 230
    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x461

    if-eq v2, v3, :cond_4

    .line 231
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x467

    if-ne v2, v3, :cond_5

    .line 232
    :cond_4
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->canboxTview:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CAN\uff1a"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->curCanboxVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 233
    :cond_5
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x465

    if-ne v2, v3, :cond_1

    .line 234
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->url:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->showDialog(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 108
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0084

    if-ne v0, v1, :cond_1

    .line 109
    iput v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 110
    invoke-direct {p0}, Lcom/touchus/benchilauncher/views/XitongDialog;->showDialog()V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0085

    if-ne v0, v1, :cond_2

    .line 112
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/views/XitongDialog$2;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/XitongDialog$2;-><init>(Lcom/touchus/benchilauncher/views/XitongDialog;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 127
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0049

    if-eq v0, v1, :cond_0

    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0086

    if-ne v0, v1, :cond_3

    .line 149
    iput v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 150
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/XitongDialog;->showDialog(I)V

    goto :goto_0

    .line 151
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0087

    if-ne v0, v1, :cond_4

    .line 152
    iput v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 153
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/XitongDialog;->showDialog(I)V

    goto :goto_0

    .line 154
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0088

    if-ne v0, v1, :cond_5

    .line 155
    iput v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 156
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/XitongDialog;->showDialog(I)V

    goto :goto_0

    .line 157
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0b0089

    if-ne v0, v1, :cond_0

    .line 158
    iput v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 159
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/views/XitongDialog;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    const v3, 0x7f03001a

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->setContentView(I)V

    .line 54
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 55
    const v3, 0x7f0b0049

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 56
    .local v1, "clickLayout":Landroid/widget/LinearLayout;
    const v3, 0x7f0b0084

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->typeTview:Landroid/widget/TextView;

    .line 57
    const v3, 0x7f0b0085

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->snTview:Landroid/widget/TextView;

    .line 58
    const v3, 0x7f0b0086

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->androidTview:Landroid/widget/TextView;

    .line 59
    const v3, 0x7f0b0087

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->systemTview:Landroid/widget/TextView;

    .line 60
    const v3, 0x7f0b0088

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mcuTview:Landroid/widget/TextView;

    .line 61
    const v3, 0x7f0b0089

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/views/XitongDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->canboxTview:Landroid/widget/TextView;

    .line 63
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->typeTview:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/touchus/benchilauncher/ProjectConfig;->projectName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->benzName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/touchus/publicutils/utils/UtilTools;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "SN":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->snTview:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    :cond_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->snTview:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SN\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->androidTview:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "APP\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/touchus/publicutils/utils/UtilTools;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/XitongDialog;->getVersion()Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "version":Ljava/lang/String;
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->systemTview:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mcuTview:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MCU\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->curMcuVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->canboxTview:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CAN\uff1a"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->curCanboxVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->typeTview:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->snTview:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->androidTview:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->systemTview:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mcuTview:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->canboxTview:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v3, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mHandler:Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 84
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v3

    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard;->getMCUVersionNumber()V

    .line 85
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->CANBOX_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    invoke-virtual {v3, v4}, Lcom/backaudio/android/driver/Mainboard;->getModeInfo(Lcom/backaudio/android/driver/Mainboard$EModeInfo;)V

    .line 86
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->clickCount:I

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mcuTview:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MCU\uff1a"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->curMcuVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->canboxTview:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CAN\uff1a"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->curCanboxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 95
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 100
    return-void
.end method

.method public unregisterHandlerr()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/XitongDialog;->mHandler:Lcom/touchus/benchilauncher/views/XitongDialog$MHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 104
    return-void
.end method
