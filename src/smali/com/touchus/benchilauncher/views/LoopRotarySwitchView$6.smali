.class Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;
.super Ljava/lang/Object;
.source "LoopRotarySwitchView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->AnimRotationTo(FLjava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    .line 547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 576
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$14(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 556
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$10(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$15(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V

    .line 557
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$16(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v0

    if-gez v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$0(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v2}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$16(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$15(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;I)V

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$17(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 561
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$7(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 571
    :goto_0
    return-void

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$17(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;

    move-result-object v1

    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$16(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v2

    .line 566
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$7(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    invoke-static {v3}, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->access$16(Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;)I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 565
    invoke-interface {v1, v2, v0}, Lcom/touchus/benchilauncher/inface/OnItemSelectedListener;->selected(ILandroid/view/View;)V

    .line 570
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView$6;->this$0:Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/views/LoopRotarySwitchView;->clickRotation:Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 581
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 551
    return-void
.end method
