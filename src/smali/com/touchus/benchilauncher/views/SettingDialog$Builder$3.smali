.class Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;
.super Ljava/lang/Object;
.source "SettingDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->create()Lcom/touchus/benchilauncher/views/SettingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 110
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-eq v0, v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v0

    if-nez v0, :cond_3

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-static {v0, v2}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$0(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;I)V

    .line 122
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$4(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    iget-object v0, v0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    iget-object v1, v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->resetClickCountThread:Ljava/lang/Thread;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 124
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    iget-object v0, v0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->mSettingHandler:Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    iget-object v1, v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->resetClickCountThread:Ljava/lang/Thread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$SettingHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 116
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$4(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$0(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;I)V

    .line 117
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$4(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    .line 118
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-static {v0, v2}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$0(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;I)V

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/SettingDialog$Builder$3;->this$1:Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-static {v0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->access$5(Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    goto :goto_1
.end method
