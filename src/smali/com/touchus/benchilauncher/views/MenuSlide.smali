.class public Lcom/touchus/benchilauncher/views/MenuSlide;
.super Lcom/touchus/benchilauncher/base/AbstraSlide;
.source "MenuSlide.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;
    }
.end annotation


# static fields
.field public static final STATE_DOWN0:I = 0x0

.field public static final STATE_DOWN1:I = 0x1

.field public static final STATE_HIDE_NEXT5V:I = 0x8

.field public static final STATE_UP020:I = 0x2

.field public static final STATE_UP021:I = 0x3

.field public static final STATE_UP120:I = 0x4

.field public static final STATE_UP121:I = 0x5

.field public static final STATE_UP122:I = 0x9

.field public static final STATE_UP221:I = 0xa

.field public static final STATE_UP222:I = 0xb

.field public static final STATE_V421:I = 0x6

.field public static final STATE_V520:I = 0x7

.field public static final mChildWithB:I = 0xe6

.field public static final mChildWithS:I = 0x8a


# instance fields
.field childNum:I

.field public mDownX:F

.field public mDownY:F

.field public mPage:I

.field mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

.field public mStartX:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/touchus/benchilauncher/base/AbstraSlide;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/16 v0, 0xa

    iput v0, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->childNum:I

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    .line 44
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 48
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 66
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v4, 0x0

    :goto_1
    return v4

    .line 50
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mStartX:F

    .line 51
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mDownX:F

    .line 52
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mDownY:F

    goto :goto_0

    .line 55
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 56
    .local v2, "moveX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 57
    .local v3, "moveY":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mDownX:F

    sub-float v0, v2, v4

    .line 58
    .local v0, "diffX":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mDownY:F

    sub-float v1, v3, v4

    .line 59
    .local v1, "diffY":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 60
    const/4 v4, 0x1

    goto :goto_1

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v10, 0x2b2

    const/16 v9, 0x1f4

    const/16 v8, 0x64

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 72
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    return v6

    .line 74
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 75
    .local v3, "moveX":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mDownX:F

    sub-float/2addr v4, v3

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v0, v4

    .line 76
    .local v0, "diffX":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/MenuSlide;->getScrollX()I

    move-result v4

    add-int v2, v4, v0

    .line 77
    .local v2, "finalX":I
    const/16 v4, 0x2c6

    if-le v2, v4, :cond_1

    .line 78
    const/16 v4, 0x2c6

    invoke-virtual {p0, v4, v7}, Lcom/touchus/benchilauncher/views/MenuSlide;->scrollTo(II)V

    goto :goto_0

    .line 80
    :cond_1
    const/16 v4, -0x14

    if-ge v2, v4, :cond_2

    .line 81
    const/16 v4, -0x14

    invoke-virtual {p0, v4, v7}, Lcom/touchus/benchilauncher/views/MenuSlide;->scrollTo(II)V

    goto :goto_0

    .line 84
    :cond_2
    invoke-virtual {p0, v0, v7}, Lcom/touchus/benchilauncher/views/MenuSlide;->scrollBy(II)V

    .line 85
    iput v3, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mDownX:F

    goto :goto_0

    .line 89
    .end local v0    # "diffX":I
    .end local v2    # "finalX":I
    .end local v3    # "moveX":F
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mStartX:F

    sub-float v1, v4, v5

    .line 90
    .local v1, "distance":F
    iget v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 92
    :pswitch_2
    const/high16 v4, -0x3d600000    # -80.0f

    cmpg-float v4, v1, v4

    if-gez v4, :cond_4

    .line 93
    invoke-virtual {p0, v10, v9}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    .line 94
    iput v6, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    .line 95
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    if-eqz v4, :cond_3

    .line 96
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;->lesten(I)V

    .line 98
    :cond_3
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    if-eqz v4, :cond_0

    .line 99
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    invoke-interface {v4, v6}, Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;->pageListener(I)V

    goto :goto_0

    .line 101
    :cond_4
    invoke-virtual {p0, v7, v8}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    .line 102
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    if-eqz v4, :cond_0

    .line 103
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;->lesten(I)V

    goto :goto_0

    .line 108
    :pswitch_3
    const/high16 v4, 0x42a00000    # 80.0f

    cmpl-float v4, v1, v4

    if-lez v4, :cond_6

    .line 109
    invoke-virtual {p0, v7, v9}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    .line 110
    iput v7, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    .line 111
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    if-eqz v4, :cond_5

    .line 112
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    const/4 v5, 0x4

    invoke-interface {v4, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;->lesten(I)V

    .line 114
    :cond_5
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    if-eqz v4, :cond_0

    .line 115
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    invoke-interface {v4, v7}, Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;->pageListener(I)V

    goto/16 :goto_0

    .line 125
    :cond_6
    invoke-virtual {p0, v10, v8}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    .line 126
    iput v6, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    .line 127
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    if-eqz v4, :cond_0

    .line 128
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    const/4 v5, 0x5

    invoke-interface {v4, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;->lesten(I)V

    goto/16 :goto_0

    .line 133
    :pswitch_4
    const/high16 v4, 0x42a00000    # 80.0f

    cmpl-float v4, v1, v4

    if-lez v4, :cond_8

    .line 134
    invoke-virtual {p0, v10, v9}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    .line 135
    iput v6, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    .line 136
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    if-eqz v4, :cond_7

    .line 137
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    const/16 v5, 0xa

    invoke-interface {v4, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;->lesten(I)V

    .line 139
    :cond_7
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    if-eqz v4, :cond_0

    .line 140
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    invoke-interface {v4, v6}, Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;->pageListener(I)V

    goto/16 :goto_0

    .line 142
    :cond_8
    const/16 v4, 0x564

    invoke-virtual {p0, v4, v8}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    .line 143
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    if-eqz v4, :cond_0

    .line 144
    iget-object v4, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    const/16 v5, 0xb

    invoke-interface {v4, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;->lesten(I)V

    goto/16 :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 90
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setPageListener(Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPageListener:Lcom/touchus/benchilauncher/views/MenuSlide$PageListener;

    .line 25
    return-void
.end method

.method public slide2Page(II)V
    .locals 1
    .param p1, "page"    # I
    .param p2, "duration"    # I

    .prologue
    .line 154
    packed-switch p1, :pswitch_data_0

    .line 165
    :goto_0
    iput p1, p0, Lcom/touchus/benchilauncher/views/MenuSlide;->mPage:I

    .line 166
    return-void

    .line 156
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    goto :goto_0

    .line 159
    :pswitch_1
    const/16 v0, 0x2b2

    invoke-virtual {p0, v0, p2}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    goto :goto_0

    .line 162
    :pswitch_2
    const/16 v0, 0x564

    invoke-virtual {p0, v0, p2}, Lcom/touchus/benchilauncher/views/MenuSlide;->smoothScrollXTo(II)V

    goto :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
