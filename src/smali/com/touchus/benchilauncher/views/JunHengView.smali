.class public Lcom/touchus/benchilauncher/views/JunHengView;
.super Landroid/view/View;
.source "JunHengView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# instance fields
.field private clockDrawable:Landroid/graphics/drawable/Drawable;

.field private clockThread:Ljava/lang/Thread;

.field private hourDrawable:Landroid/graphics/drawable/Drawable;

.field private isChange:Z

.field public istuodong:Z

.field private mViewCenterX:I

.field private mViewCenterY:I

.field mun:I

.field private paint:Landroid/graphics/Paint;

.field private time:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/views/JunHengView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/touchus/benchilauncher/views/JunHengView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/views/JunHengView;->istuodong:Z

    .line 22
    iput v2, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mun:I

    .line 59
    sget-object v1, Lcom/touchus/benchilauncher/R$styleable;->MyClockStyleable:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    .local v0, "ta":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->clockDrawable:Landroid/graphics/drawable/Drawable;

    .line 63
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->hourDrawable:Landroid/graphics/drawable/Drawable;

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->paint:Landroid/graphics/Paint;

    .line 68
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->paint:Landroid/graphics/Paint;

    const-string v2, "#000000"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 70
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 71
    iget-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 73
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->time:Landroid/text/format/Time;

    .line 74
    new-instance v1, Lcom/touchus/benchilauncher/views/JunHengView$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/JunHengView$1;-><init>(Lcom/touchus/benchilauncher/views/JunHengView;)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->clockThread:Ljava/lang/Thread;

    .line 88
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/views/JunHengView;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->isChange:Z

    return v0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->isChange:Z

    .line 144
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->clockThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 145
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->isChange:Z

    .line 151
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 94
    iget-object v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->time:Landroid/text/format/Time;

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    .line 96
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    .line 97
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getBottom()I

    move-result v5

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    .line 98
    iget-object v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->clockDrawable:Landroid/graphics/drawable/Drawable;

    .line 100
    .local v0, "dial":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 101
    .local v1, "h":I
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 102
    .local v4, "w":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    if-lt v5, v4, :cond_0

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getBottom()I

    move-result v5

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    if-ge v5, v1, :cond_1

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    int-to-float v6, v4

    div-float/2addr v5, v6

    .line 104
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getBottom()I

    move-result v6

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getTop()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    int-to-float v7, v1

    div-float/2addr v6, v7

    .line 103
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 105
    .local v3, "scale":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 106
    iget v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    int-to-float v5, v5

    iget v6, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    int-to-float v6, v6

    invoke-virtual {p1, v3, v3, v5, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 108
    .end local v3    # "scale":F
    :cond_1
    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->isChange:Z

    if-eqz v5, :cond_2

    .line 110
    iget v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    div-int/lit8 v6, v4, 0x2

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    .line 111
    div-int/lit8 v7, v1, 0x2

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    div-int/lit8 v8, v4, 0x2

    add-int/2addr v7, v8

    .line 112
    iget v8, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    div-int/lit8 v9, v1, 0x2

    add-int/2addr v8, v9

    .line 110
    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 114
    :cond_2
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 116
    const/high16 v5, 0x41700000    # 15.0f

    iget v6, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mun:I

    invoke-virtual {p0, v6}, Lcom/touchus/benchilauncher/views/JunHengView;->setMun(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    int-to-float v6, v6

    iget v7, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    int-to-float v7, v7

    invoke-virtual {p1, v5, v6, v7}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 117
    iget-object v2, p0, Lcom/touchus/benchilauncher/views/JunHengView;->hourDrawable:Landroid/graphics/drawable/Drawable;

    .line 118
    .local v2, "mHour":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 119
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 120
    iget-boolean v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->isChange:Z

    if-eqz v5, :cond_3

    .line 121
    iget v5, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    div-int/lit8 v6, v4, 0x2

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    .line 122
    sub-int/2addr v6, v1

    add-int/lit8 v6, v6, 0x46

    iget v7, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterX:I

    div-int/lit8 v8, v4, 0x2

    add-int/2addr v7, v8

    .line 123
    iget v8, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mViewCenterY:I

    add-int/lit8 v8, v8, 0x46

    .line 121
    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 125
    :cond_3
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 126
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 127
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 128
    return-void
.end method

.method public setMun(I)I
    .locals 1
    .param p1, "mun1"    # I

    .prologue
    .line 131
    if-ltz p1, :cond_1

    const/16 v0, 0xa

    if-gt p1, v0, :cond_1

    .line 132
    iput p1, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mun:I

    .line 136
    :cond_0
    :goto_0
    iget v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mun:I

    return v0

    .line 133
    :cond_1
    const/16 v0, -0xa

    if-lt p1, v0, :cond_0

    const/4 v0, -0x1

    if-gt p1, v0, :cond_0

    .line 134
    add-int/lit8 v0, p1, 0x18

    iput v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->mun:I

    goto :goto_0
.end method

.method public settingCurrentClockPic(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 46
    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->clockDrawable:Landroid/graphics/drawable/Drawable;

    .line 48
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->hourDrawable:Landroid/graphics/drawable/Drawable;

    .line 53
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->clockDrawable:Landroid/graphics/drawable/Drawable;

    .line 51
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/views/JunHengView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/views/JunHengView;->hourDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
