.class Lcom/touchus/benchilauncher/MainService$9;
.super Landroid/content/BroadcastReceiver;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1027
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 1031
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1032
    .local v0, "action":Ljava/lang/String;
    const-string v5, "com.touchus.factorytest.recovery"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1033
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    const-string v6, "com.unibroad.usbcardvr"

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/MainService;->forceStopApp(Ljava/lang/String;)V

    .line 1034
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1078
    :cond_0
    :goto_0
    return-void

    .line 1035
    :cond_1
    const-string v5, "com.touchus.factorytest.sdcardreset"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1036
    invoke-static {}, Lcom/touchus/benchilauncher/utils/Utiltools;->resetSDCard()V

    goto :goto_0

    .line 1037
    :cond_2
    const-string v5, "com.touchus.factorytest.getMcu"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1038
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1039
    .local v1, "getMcu":Landroid/content/Intent;
    const-string v5, "com.touchus.factorytest.Mcu"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1040
    const-string v5, "mcu"

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v6}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v6

    iget-object v6, v6, Lcom/touchus/benchilauncher/LauncherApplication;->curMcuVersion:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1041
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v5, v1}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1042
    .end local v1    # "getMcu":Landroid/content/Intent;
    :cond_3
    sget-object v5, Lcom/touchus/publicutils/sysconst/PubSysConst;->ACTION_FACTORY_BREAKSET:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1043
    sget-object v5, Lcom/touchus/publicutils/sysconst/PubSysConst;->KEY_BREAKPOS:Ljava/lang/String;

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1044
    .local v2, "pos":I
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v5

    sget-object v6, Lcom/touchus/publicutils/sysconst/PubSysConst;->KEY_BREAKPOS:Ljava/lang/String;

    invoke-virtual {v5, v6, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 1045
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iput v2, v5, Lcom/touchus/benchilauncher/LauncherApplication;->breakpos:I

    .line 1046
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/16 v6, 0x9

    int-to-byte v7, v2

    aput-byte v7, v5, v6

    .line 1047
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SysConst.storeData = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v5

    sget-object v6, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-virtual {v5, v6}, Lcom/backaudio/android/driver/Mainboard;->sendStoreDataToMcu([B)V

    .line 1049
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/touchus/publicutils/sysconst/PubSysConst;->GT9XX_INT_TRIGGER:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/touchus/publicutils/utils/UtilTools;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 1050
    .end local v2    # "pos":I
    :cond_4
    const-string v5, "com.touchus.factorytest.benzetype"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1052
    sget-object v5, Lcom/touchus/publicutils/sysconst/BenzModel;->KEY:Ljava/lang/String;

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    int-to-byte v3, v5

    .line 1053
    .local v3, "type":B
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    move-result-object v7

    array-length v8, v7

    move v5, v6

    :goto_1
    if-lt v5, v8, :cond_6

    .line 1059
    :goto_2
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isSUV()Z

    move-result v7

    iput-boolean v7, v5, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    .line 1060
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v5

    sget-object v7, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v5, v7}, Lcom/backaudio/android/driver/Mainboard;->setBenzType(Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;)V

    .line 1061
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1062
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iput-boolean v6, v5, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 1064
    :cond_5
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$9;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v5

    sget-object v6, Lcom/touchus/publicutils/sysconst/BenzModel;->KEY:Ljava/lang/String;

    sget-object v7, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v7}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1053
    :cond_6
    aget-object v4, v7, v5

    .line 1054
    .local v4, "value":Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    invoke-virtual {v4}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v9

    if-ne v3, v9, :cond_7

    .line 1055
    sput-object v4, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    goto :goto_2

    .line 1053
    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method
