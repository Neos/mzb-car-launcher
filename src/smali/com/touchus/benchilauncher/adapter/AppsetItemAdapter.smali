.class public Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppsetItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private context:Landroid/content/Context;

.field private listData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private seclectIndex:I


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/LauncherApplication;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "app"    # Lcom/touchus/benchilauncher/LauncherApplication;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/touchus/benchilauncher/LauncherApplication;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p3, "listData":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/bean/AppInfo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 44
    iput-object p2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->context:Landroid/content/Context;

    .line 45
    iput-object p3, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->listData:Ljava/util/List;

    .line 46
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 60
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 66
    if-nez p2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->context:Landroid/content/Context;

    const v3, 0x7f030006

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 68
    new-instance v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;-><init>()V

    .line 69
    .local v0, "holder":Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;
    const v2, 0x7f0b0019

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;->appIcon:Landroid/widget/ImageView;

    .line 70
    const v2, 0x7f0b001a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;->appName:Landroid/widget/TextView;

    .line 71
    const v2, 0x7f0b001b

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;->appICheck:Landroid/widget/CheckBox;

    .line 72
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 76
    :goto_0
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;->appIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/bean/AppInfo;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/AppInfo;->getAppIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;->appName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/bean/AppInfo;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/AppInfo;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;->appICheck:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/LauncherApplication;->getNaviAPP()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/bean/AppInfo;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 79
    const v2, 0x7f0b0018

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 80
    .local v1, "itemView":Landroid/widget/RelativeLayout;
    iget v2, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->seclectIndex:I

    if-ne v2, p1, :cond_1

    .line 81
    const v2, 0x7f02016a

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 85
    :goto_1
    return-object p2

    .line 74
    .end local v0    # "holder":Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;
    .end local v1    # "itemView":Landroid/widget/RelativeLayout;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter$ViewHolder;
    goto :goto_0

    .line 83
    .restart local v1    # "itemView":Landroid/widget/RelativeLayout;
    :cond_1
    const v2, 0xffffff

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public setSeclectIndex(I)V
    .locals 0
    .param p1, "seclectIndex"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->seclectIndex:I

    .line 96
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/adapter/AppsetItemAdapter;->notifyDataSetChanged()V

    .line 97
    return-void
.end method
