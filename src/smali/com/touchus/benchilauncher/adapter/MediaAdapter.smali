.class public Lcom/touchus/benchilauncher/adapter/MediaAdapter;
.super Landroid/widget/BaseAdapter;
.source "MediaAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private mediaBeans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field private seclectIndex:I

.field private type:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p2, "mediaBeans":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/publicutils/bean/MediaBean;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->type:I

    .line 36
    iput-object p2, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->mediaBeans:Ljava/util/List;

    .line 37
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->context:Landroid/content/Context;

    .line 38
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 39
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->mediaBeans:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->mediaBeans:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 53
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 61
    if-nez p2, :cond_0

    .line 62
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03003a

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->mediaBeans:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    .line 65
    .local v1, "mediaBean":Lcom/touchus/publicutils/bean/MediaBean;
    const v0, 0x7f0b0157

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 66
    .local v7, "imageView":Landroid/widget/ImageView;
    const v0, 0x7f0b0158

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 69
    .local v9, "textView":Landroid/widget/TextView;
    iget v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->type:I

    if-nez v0, :cond_1

    .line 70
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 94
    :goto_0
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    const v0, 0x7f0b0018

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    .line 97
    .local v8, "itemView":Landroid/widget/RelativeLayout;
    iget v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->seclectIndex:I

    if-ne v0, p1, :cond_4

    .line 98
    const v0, 0x7f020109

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 102
    :goto_1
    return-object p2

    .line 71
    .end local v8    # "itemView":Landroid/widget/RelativeLayout;
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->type:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 72
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 73
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 74
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    .line 75
    invoke-static {}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->getInstance()Lcom/touchus/publicutils/utils/LoadLocalImageUtil;

    move-result-object v0

    .line 76
    sget v2, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->VIDEO_TYPE:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->context:Landroid/content/Context;

    .line 77
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;

    invoke-direct {v5, p0, v7}, Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;-><init>(Lcom/touchus/benchilauncher/adapter/MediaAdapter;Landroid/widget/ImageView;)V

    .line 75
    invoke-virtual/range {v0 .. v5}, Lcom/touchus/publicutils/utils/LoadLocalImageUtil;->loadDrawable(Lcom/touchus/publicutils/bean/MediaBean;ILandroid/content/Context;Ljava/lang/String;Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;)Landroid/graphics/Bitmap;

    goto :goto_0

    .line 87
    :cond_2
    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 90
    :cond_3
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    new-instance v6, Ljava/io/File;

    invoke-virtual {v1}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    .local v6, "file":Ljava/io/File;
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 100
    .end local v6    # "file":Ljava/io/File;
    .restart local v8    # "itemView":Landroid/widget/RelativeLayout;
    :cond_4
    const v0, 0xffffff

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public notifyDataSetChanged(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->type:I

    .line 111
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->notifyDataSetChanged()V

    .line 112
    return-void
.end method

.method public setSeclectIndex(I)V
    .locals 0
    .param p1, "seclectIndex"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter;->seclectIndex:I

    .line 107
    return-void
.end method
