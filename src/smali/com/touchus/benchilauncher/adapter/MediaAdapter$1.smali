.class Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;
.super Ljava/lang/Object;
.source "MediaAdapter.java"

# interfaces
.implements Lcom/touchus/publicutils/utils/LoadLocalImageUtil$ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/adapter/MediaAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

.field private final synthetic val$imageView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/adapter/MediaAdapter;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;->this$0:Lcom/touchus/benchilauncher/adapter/MediaAdapter;

    iput-object p2, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;->val$imageView:Landroid/widget/ImageView;

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public imageLoaded(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 1
    .param p1, "bitmip"    # Landroid/graphics/Bitmap;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/MediaAdapter$1;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 84
    :cond_0
    return-void
.end method
