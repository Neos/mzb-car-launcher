.class public Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "DialogItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private listData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private seclectIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "listData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->context:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->listData:Ljava/util/List;

    .line 40
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 54
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 60
    if-nez p2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->context:Landroid/content/Context;

    const v3, 0x7f03000e

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 62
    new-instance v0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;-><init>()V

    .line 63
    .local v0, "holder":Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;
    const v2, 0x7f0b001a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;->nameTview:Landroid/widget/TextView;

    .line 64
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 68
    :goto_0
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;->nameTview:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const v2, 0x7f0b0018

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 70
    .local v1, "itemView":Landroid/widget/LinearLayout;
    const-string v3, "systemtime"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v2, " seclectIndex == position   "

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->seclectIndex:I

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget v2, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->seclectIndex:I

    if-ne v2, p1, :cond_2

    .line 72
    const v2, 0x7f02016a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 73
    const-string v2, "systemtime"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " getView   "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :goto_2
    return-object p2

    .line 66
    .end local v0    # "holder":Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;
    .end local v1    # "itemView":Landroid/widget/LinearLayout;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/touchus/benchilauncher/adapter/DialogItemAdapter$ViewHolder;
    goto :goto_0

    .line 70
    .restart local v1    # "itemView":Landroid/widget/LinearLayout;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 75
    :cond_2
    const v2, 0xffffff

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto :goto_2
.end method

.method public setSeclectIndex(I)V
    .locals 0
    .param p1, "seclectIndex"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/touchus/benchilauncher/adapter/DialogItemAdapter;->seclectIndex:I

    .line 86
    return-void
.end method
