.class public Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "SystemSetItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private listData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/SettingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private seclectIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/SettingInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p2, "listData":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/bean/SettingInfo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->context:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->listData:Ljava/util/List;

    .line 46
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    invoke-direct {v0, p1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 47
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 61
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v6, 0x8

    .line 67
    if-nez p2, :cond_0

    .line 68
    iget-object v3, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->context:Landroid/content/Context;

    const v4, 0x7f030006

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 69
    new-instance v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;-><init>()V

    .line 70
    .local v0, "holder":Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;
    const v3, 0x7f0b0019

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appIcon:Landroid/widget/ImageView;

    .line 71
    const v3, 0x7f0b001a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appName:Landroid/widget/TextView;

    .line 72
    const v3, 0x7f0b001b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appICheck:Landroid/widget/CheckBox;

    .line 73
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 77
    :goto_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->listData:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/bean/SettingInfo;

    .line 79
    .local v1, "info":Lcom/touchus/benchilauncher/bean/SettingInfo;
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appName:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/SettingInfo;->getItem()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "en"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 82
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appName:Landroid/widget/TextView;

    const/high16 v4, 0x41b00000    # 22.0f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 86
    :goto_1
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/SettingInfo;->isCheckItem()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 87
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appICheck:Landroid/widget/CheckBox;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 88
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appICheck:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/SettingInfo;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 92
    :goto_2
    const v3, 0x7f0b0018

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 93
    .local v2, "itemView":Landroid/widget/RelativeLayout;
    iget v3, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->seclectIndex:I

    if-ne v3, p1, :cond_3

    .line 94
    const v3, 0x7f02016a

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 98
    :goto_3
    return-object p2

    .line 75
    .end local v0    # "holder":Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;
    .end local v1    # "info":Lcom/touchus/benchilauncher/bean/SettingInfo;
    .end local v2    # "itemView":Landroid/widget/RelativeLayout;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;
    goto :goto_0

    .line 84
    .restart local v1    # "info":Lcom/touchus/benchilauncher/bean/SettingInfo;
    :cond_1
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appName:Landroid/widget/TextView;

    const/high16 v4, 0x41e00000    # 28.0f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_1

    .line 90
    :cond_2
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter$ViewHolder;->appICheck:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_2

    .line 96
    .restart local v2    # "itemView":Landroid/widget/RelativeLayout;
    :cond_3
    const v3, 0xffffff

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_3
.end method

.method public setSeclectIndex(I)V
    .locals 0
    .param p1, "seclectIndex"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->seclectIndex:I

    .line 109
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/adapter/SystemSetItemAdapter;->notifyDataSetChanged()V

    .line 110
    return-void
.end method
