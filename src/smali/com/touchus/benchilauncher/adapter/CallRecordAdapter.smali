.class public Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;
.super Landroid/widget/BaseAdapter;
.source "CallRecordAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private callRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private selectPosition:I

.field private type:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;-><init>(Landroid/content/Context;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 30
    iput v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->selectPosition:I

    .line 31
    iput v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->type:I

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->callRecords:Ljava/util/List;

    .line 39
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->context:Landroid/content/Context;

    .line 40
    iput p2, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->type:I

    .line 41
    return-void
.end method

.method private initData(Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;Lcom/touchus/benchilauncher/bean/Person;)V
    .locals 2
    .param p1, "viewHolder"    # Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;
    .param p2, "callRecord"    # Lcom/touchus/benchilauncher/bean/Person;

    .prologue
    .line 94
    invoke-virtual {p2}, Lcom/touchus/benchilauncher/bean/Person;->getFlag()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setCallState(Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;I)V

    .line 95
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_yunyinshang:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/touchus/benchilauncher/bean/Person;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_phone_number:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->type:I

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_call_time:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/touchus/benchilauncher/bean/Person;->getRemark()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_call_time:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setCallState(Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;
    .param p2, "callState"    # I

    .prologue
    .line 109
    packed-switch p2, :pswitch_data_0

    .line 120
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->img_phone_state:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    :goto_0
    return-void

    .line 111
    :pswitch_0
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->img_phone_state:Landroid/widget/ImageView;

    const v1, 0x7f0201fe

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 114
    :pswitch_1
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->img_phone_state:Landroid/widget/ImageView;

    const v1, 0x7f0201ff

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 117
    :pswitch_2
    iget-object v0, p1, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->img_phone_state:Landroid/widget/ImageView;

    const v1, 0x7f020200

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x584
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->callRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 57
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 62
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 68
    if-nez p2, :cond_0

    .line 69
    iget-object v1, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->context:Landroid/content/Context;

    const v2, 0x7f03002b

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 70
    new-instance v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;

    invoke-direct {v0, p0, v3}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;-><init>(Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;)V

    .line 71
    .local v0, "viewHolder":Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;
    const v1, 0x7f0b00f7

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->img_phone_state:Landroid/widget/ImageView;

    .line 72
    const v1, 0x7f0b00f8

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_yunyinshang:Landroid/widget/TextView;

    .line 73
    const v1, 0x7f0b00f9

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_phone_number:Landroid/widget/TextView;

    .line 74
    const v1, 0x7f0b00fa

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;->tv_call_time:Landroid/widget/TextView;

    .line 75
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 79
    :goto_0
    iget v1, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->selectPosition:I

    if-ne p1, v1, :cond_1

    .line 80
    const v1, 0x7f020109

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 84
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->callRecords:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/bean/Person;

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->initData(Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;Lcom/touchus/benchilauncher/bean/Person;)V

    .line 85
    return-object p2

    .line 77
    .end local v0    # "viewHolder":Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;

    .restart local v0    # "viewHolder":Lcom/touchus/benchilauncher/adapter/CallRecordAdapter$ViewHolder;
    goto :goto_0

    .line 82
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "callRecords":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/bean/Person;>;"
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->callRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->callRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->callRecords:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 48
    return-void
.end method

.method public setSelectItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->selectPosition:I

    .line 130
    return-void
.end method
