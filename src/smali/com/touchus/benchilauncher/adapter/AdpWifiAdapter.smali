.class public Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;
.super Landroid/widget/BaseAdapter;
.source "AdpWifiAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private listData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/BeanWifi;",
            ">;"
        }
    .end annotation
.end field

.field public mLauncher:Lcom/touchus/benchilauncher/Launcher;

.field public rowCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/BeanWifi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "listData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/BeanWifi;>;"
    const/4 v0, 0x5

    .line 46
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 43
    iput v0, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->rowCount:I

    .line 47
    iput-object p1, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->context:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->listData:Ljava/util/ArrayList;

    .line 49
    iput v0, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->rowCount:I

    .line 50
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 64
    int-to-long v0, p1

    return-wide v0
.end method

.method public getListData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/BeanWifi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->listData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 70
    if-nez p2, :cond_0

    .line 71
    iget-object v3, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03002d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 72
    new-instance v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;-><init>()V

    .line 73
    .local v0, "holder":Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;
    const v3, 0x7f0b00ff

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->nameTview:Landroid/widget/TextView;

    .line 74
    const v3, 0x7f0b00fc

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Space;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->layoutSpace:Landroid/widget/Space;

    .line 75
    const v3, 0x7f0b0100

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->signImg:Landroid/widget/ImageView;

    .line 76
    const v3, 0x7f0b00fd

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->selectImg:Landroid/widget/ImageView;

    .line 77
    const v3, 0x7f0b00fe

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->loadingImg:Landroid/widget/ImageView;

    .line 78
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 82
    :goto_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->listData:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/bean/BeanWifi;

    .line 83
    .local v2, "temp":Lcom/touchus/benchilauncher/bean/BeanWifi;
    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getLevel()I

    move-result v3

    if-lez v3, :cond_1

    .line 84
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->signImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->signImg:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getLevel()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageLevel(I)V

    .line 89
    :goto_1
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->nameTview:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter;->rowCount:I

    div-int/2addr v3, v4

    invoke-direct {v1, v7, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 91
    .local v1, "param":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->layoutSpace:Landroid/widget/Space;

    invoke-virtual {v3, v1}, Landroid/widget/Space;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 93
    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v3

    if-ne v3, v7, :cond_2

    .line 94
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->selectImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->loadingImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    :goto_2
    return-object p2

    .line 80
    .end local v0    # "holder":Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;
    .end local v1    # "param":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "temp":Lcom/touchus/benchilauncher/bean/BeanWifi;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;
    goto :goto_0

    .line 87
    .restart local v2    # "temp":Lcom/touchus/benchilauncher/bean/BeanWifi;
    :cond_1
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->signImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 98
    .restart local v1    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :cond_2
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->selectImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    invoke-virtual {v2}, Lcom/touchus/benchilauncher/bean/BeanWifi;->getState()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 100
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->loadingImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 103
    :cond_3
    iget-object v3, v0, Lcom/touchus/benchilauncher/adapter/AdpWifiAdapter$ViewHolder;->loadingImg:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method
