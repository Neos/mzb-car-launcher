.class Lcom/touchus/benchilauncher/service/BluetoothService$3;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/service/BluetoothService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/service/BluetoothService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 1421
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, -0x1

    .line 1426
    :try_start_0
    const-string v4, "analyticCmd"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1427
    .local v0, "cmdId":I
    const-string v4, "stringArg1"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1428
    .local v2, "strParam1":Ljava/lang/String;
    const-string v4, "stringArg2"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1429
    .local v3, "strParam2":Ljava/lang/String;
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "assistantReceiver  cmdId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , strParam2: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1430
    if-ne v0, v7, :cond_1

    .line 1477
    .end local v0    # "cmdId":I
    .end local v2    # "strParam1":Ljava/lang/String;
    .end local v3    # "strParam2":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1433
    .restart local v0    # "cmdId":I
    .restart local v2    # "strParam1":Ljava/lang/String;
    .restart local v3    # "strParam2":Ljava/lang/String;
    :cond_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1436
    :pswitch_0
    const/16 v4, 0x585

    sput v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 1438
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v5, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v4, v5, :cond_0

    .line 1440
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v5, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v4, v5, :cond_4

    .line 1442
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_2

    .line 1443
    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1444
    :cond_2
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$3(Lcom/touchus/benchilauncher/service/BluetoothService;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f070037

    invoke-static {v4, v5}, Lcom/touchus/benchilauncher/utils/ToastTool;->showBigShortToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1475
    .end local v0    # "cmdId":I
    .end local v2    # "strParam1":Ljava/lang/String;
    .end local v3    # "strParam2":Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_0

    .line 1447
    .restart local v0    # "cmdId":I
    .restart local v2    # "strParam1":Ljava/lang/String;
    .restart local v3    # "strParam2":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iput-object v3, v4, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 1448
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v4, v4, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    iget-object v5, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget-object v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->call(Ljava/lang/String;)V

    goto :goto_0

    .line 1450
    :cond_4
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->launcherHandler:Landroid/os/Handler;

    .line 1451
    const/16 v5, 0x40c

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1450
    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1452
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 1461
    .end local v1    # "msg":Landroid/os/Message;
    :pswitch_1
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iget-object v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->launcherHandler:Landroid/os/Handler;

    .line 1462
    const/16 v5, 0x40c

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1461
    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 1463
    .restart local v1    # "msg":Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 1466
    .end local v1    # "msg":Landroid/os/Message;
    :pswitch_2
    const-string v4, "1"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1467
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->answerCalling()V

    goto/16 :goto_0

    .line 1469
    :cond_5
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$3;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->cutdownCurrentCalling()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1433
    :pswitch_data_0
    .packed-switch 0x3f3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
