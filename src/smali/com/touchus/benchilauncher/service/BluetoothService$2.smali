.class Lcom/touchus/benchilauncher/service/BluetoothService$2;
.super Ljava/lang/Object;
.source "BluetoothService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/service/BluetoothService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/service/BluetoothService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 1271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 7
    .param p1, "focusChange"    # I

    .prologue
    const/16 v6, 0x58a

    const/4 v5, 0x1

    const/high16 v4, 0x42c80000    # 100.0f

    const/4 v3, 0x0

    .line 1273
    packed-switch p1, :pswitch_data_0

    .line 1306
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 1307
    return-void

    .line 1275
    :pswitch_1
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "btmusic AUDIOFOCUS_LOSS  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1276
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->stopBTMusic()V

    goto :goto_0

    .line 1279
    :pswitch_2
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestAudioFocus\tbtmusic AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1280
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v0, :cond_2

    .line 1281
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->setBTVolume(I)V

    .line 1287
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    goto :goto_0

    .line 1283
    :cond_2
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    if-ne v0, v6, :cond_1

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    if-eqz v0, :cond_1

    .line 1284
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    goto :goto_1

    .line 1290
    :pswitch_3
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestAudioFocus\tbtmusic AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1291
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    if-ne v0, v6, :cond_3

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    if-eqz v0, :cond_3

    .line 1292
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    .line 1294
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    goto/16 :goto_0

    .line 1297
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v5, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    .line 1298
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestAudioFocus\tbtmusic AUDIOFOCUS_GAIN"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1299
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->setBTVolume(I)V

    .line 1300
    const/16 v0, 0x58b

    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    if-nez v0, :cond_4

    .line 1301
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v5}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    .line 1303
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$2;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    goto/16 :goto_0

    .line 1273
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
