.class public Lcom/touchus/benchilauncher/service/MusicLoader;
.super Ljava/lang/Object;
.source "MusicLoader.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "com.cwf.app.musicplay"

.field private static contentResolver:Landroid/content/ContentResolver;

.field private static musicList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field private static musicLoader:Lcom/touchus/benchilauncher/service/MusicLoader;


# instance fields
.field private contentUri:Landroid/net/Uri;

.field private projection:[Ljava/lang/String;

.field private sortOrder:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicLoader;->contentUri:Landroid/net/Uri;

    .line 30
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 31
    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 32
    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 33
    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 34
    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 35
    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 36
    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 37
    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 38
    const-string v2, "_size"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicLoader;->projection:[Ljava/lang/String;

    .line 41
    const-string v0, "_size"

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicLoader;->sortOrder:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public static instance(Landroid/content/ContentResolver;)Lcom/touchus/benchilauncher/service/MusicLoader;
    .locals 1
    .param p0, "pContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 45
    sget-object v0, Lcom/touchus/benchilauncher/service/MusicLoader;->musicLoader:Lcom/touchus/benchilauncher/service/MusicLoader;

    if-nez v0, :cond_0

    .line 46
    sput-object p0, Lcom/touchus/benchilauncher/service/MusicLoader;->contentResolver:Landroid/content/ContentResolver;

    .line 47
    new-instance v0, Lcom/touchus/benchilauncher/service/MusicLoader;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/service/MusicLoader;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/service/MusicLoader;->musicLoader:Lcom/touchus/benchilauncher/service/MusicLoader;

    .line 49
    :cond_0
    sget-object v0, Lcom/touchus/benchilauncher/service/MusicLoader;->musicLoader:Lcom/touchus/benchilauncher/service/MusicLoader;

    return-object v0
.end method

.method private queryMusicList()V
    .locals 27

    .prologue
    .line 56
    sget-object v4, Lcom/touchus/benchilauncher/service/MusicLoader;->contentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/service/MusicLoader;->contentUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/touchus/benchilauncher/service/MusicLoader;->projection:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 57
    .local v10, "cursor":Landroid/database/Cursor;
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v21, "musicNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v10, :cond_0

    .line 92
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 63
    const-string v4, "_display_name"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 64
    .local v14, "displayNameCol":I
    const-string v4, "title"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 65
    .local v26, "titleCol":I
    const-string v4, "_id"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 66
    .local v17, "idCol":I
    const-string v4, "duration"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 67
    .local v16, "durationCol":I
    const-string v4, "_size"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 68
    .local v24, "sizeCol":I
    const-string v4, "_data"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 70
    .local v12, "dataCol":I
    :cond_1
    move/from16 v0, v26

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 71
    .local v25, "title":Ljava/lang/String;
    invoke-interface {v10, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 72
    .local v13, "displayName":Ljava/lang/String;
    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 73
    .local v18, "id":J
    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 74
    .local v15, "duration":I
    move/from16 v0, v24

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 75
    .local v22, "size":J
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 77
    .local v11, "data":Ljava/lang/String;
    new-instance v20, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-direct/range {v20 .. v20}, Lcom/touchus/publicutils/bean/MediaBean;-><init>()V

    .line 78
    .local v20, "musicInfo":Lcom/touchus/publicutils/bean/MediaBean;
    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/touchus/publicutils/bean/MediaBean;->setId(J)V

    .line 79
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/touchus/publicutils/bean/MediaBean;->setTitle(Ljava/lang/String;)V

    .line 80
    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lcom/touchus/publicutils/bean/MediaBean;->setDisplay_name(Ljava/lang/String;)V

    .line 81
    int-to-long v4, v15

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Lcom/touchus/publicutils/bean/MediaBean;->setDuration(J)V

    .line 82
    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/touchus/publicutils/bean/MediaBean;->setSize(J)V

    .line 83
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/touchus/publicutils/bean/MediaBean;->setData(Ljava/lang/String;)V

    .line 84
    move-object/from16 v0, v21

    invoke-interface {v0, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v15, :cond_2

    .line 85
    move-object/from16 v0, v21

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v4, Lcom/touchus/benchilauncher/service/MusicLoader;->musicList:Ljava/util/ArrayList;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 91
    .end local v11    # "data":Ljava/lang/String;
    .end local v12    # "dataCol":I
    .end local v13    # "displayName":Ljava/lang/String;
    .end local v14    # "displayNameCol":I
    .end local v15    # "duration":I
    .end local v16    # "durationCol":I
    .end local v17    # "idCol":I
    .end local v18    # "id":J
    .end local v20    # "musicInfo":Lcom/touchus/publicutils/bean/MediaBean;
    .end local v22    # "size":J
    .end local v24    # "sizeCol":I
    .end local v25    # "title":Ljava/lang/String;
    .end local v26    # "titleCol":I
    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method


# virtual methods
.method public getMusicList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/service/MusicLoader;->musicList:Ljava/util/ArrayList;

    .line 96
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/MusicLoader;->queryMusicList()V

    .line 97
    sget-object v0, Lcom/touchus/benchilauncher/service/MusicLoader;->musicList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMusicUriById(J)Landroid/net/Uri;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 101
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicLoader;->contentUri:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 102
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method
