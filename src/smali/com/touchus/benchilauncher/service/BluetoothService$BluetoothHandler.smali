.class Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;
.super Ljava/lang/Object;
.source "BluetoothService.java"

# interfaces
.implements Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/service/BluetoothService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BluetoothHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/service/BluetoothService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnswerPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;)V
    .locals 0
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;

    .prologue
    .line 311
    return-void
.end method

.method public onCallOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;)V
    .locals 0
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;

    .prologue
    .line 315
    return-void
.end method

.method public onConnectedDevice(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;)V
    .locals 5
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;

    .prologue
    .line 620
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->getDeviceAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 621
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const-string v3, ""

    iput-object v3, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    .line 623
    :cond_0
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->getDeviceAddress()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    .line 624
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    .line 625
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onConnectedDevice: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 626
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 627
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 636
    .end local v1    # "i":I
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$14(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/16 v3, 0x57e

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 639
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_Per.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$15(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;)V

    .line 640
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_His.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$15(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;)V

    .line 641
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$16(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;)V

    .line 642
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->iNeedToChangeLocalContacts(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 644
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->setLastDeviceName(Ljava/lang/String;)V

    .line 645
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBookchanged()V

    .line 647
    :cond_2
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onConnectedDevice: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 648
    return-void

    .line 628
    .restart local v1    # "i":I
    :cond_3
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/bean/Equip;

    .line 629
    .local v0, "equip":Lcom/touchus/benchilauncher/bean/Equip;
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/bean/Equip;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 630
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/bean/Equip;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    goto/16 :goto_1

    .line 627
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public onDeviceName(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V
    .locals 3
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    .prologue
    .line 682
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 683
    return-void
.end method

.method public onDeviceRemoved(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;)V
    .locals 0
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;

    .prologue
    .line 652
    return-void
.end method

.method public onFinishDownloadPhoneBook()V
    .locals 6

    .prologue
    .line 739
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onFinishDownloadPhoneBook "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v5, v5, Lcom/touchus/benchilauncher/service/BluetoothService;->bookList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 741
    :try_start_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->bookList:Ljava/util/ArrayList;

    new-instance v4, Lcom/touchus/benchilauncher/utils/PinyinComparator;

    invoke-direct {v4}, Lcom/touchus/benchilauncher/utils/PinyinComparator;-><init>()V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 742
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v4, v4, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_Per.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 743
    .local v1, "file":Ljava/io/File;
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 744
    .local v2, "outStream":Ljava/io/FileOutputStream;
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->bookList:Ljava/util/ArrayList;

    invoke-static {v3, v2}, Lcom/touchus/benchilauncher/utils/PersonParse;->save(Ljava/util/ArrayList;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 749
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "outStream":Ljava/io/FileOutputStream;
    :goto_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v4, 0x2

    iput v4, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 750
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadType:I

    if-nez v3, :cond_0

    .line 751
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$18(Lcom/touchus/benchilauncher/service/BluetoothService;I)V

    .line 752
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBookchanged()V

    .line 753
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    const/16 v4, 0x581

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 754
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$7(Lcom/touchus/benchilauncher/service/BluetoothService;J)V

    .line 756
    :cond_0
    return-void

    .line 746
    :catch_0
    move-exception v0

    .line 747
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onHangUpPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;)V
    .locals 3
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;

    .prologue
    .line 319
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "bluetoothlog onHangUpPhone----"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 320
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->unrequestAudioFocus()V

    .line 321
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    .line 322
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x588

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 323
    return-void
.end method

.method public onIncomingCall(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;)V
    .locals 6
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 328
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FloatSystemCallDialog onIncomingCall arg0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 329
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;->getPhone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 328
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->closeOrWakeupScreen(Z)V

    .line 333
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$4(Lcom/touchus/benchilauncher/service/BluetoothService;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    .line 334
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 335
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    .line 336
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->INCOMING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    .line 335
    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setShowStatus(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;)V

    .line 337
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;->getPhone()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 338
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->getbookName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0, v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$1(Lcom/touchus/benchilauncher/service/BluetoothService;Z)V

    .line 343
    const/16 v0, 0x584

    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 344
    sput-boolean v5, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    .line 345
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v5, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    .line 346
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x587

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 347
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$5(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    .line 350
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    if-nez v0, :cond_2

    .line 351
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    if-eqz v0, :cond_3

    .line 352
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->interAndroidView()V

    .line 353
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v4, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 354
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v4, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 355
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v4, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 358
    :cond_3
    return-void
.end method

.method public onMediaInfo(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;)V
    .locals 4
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    .prologue
    .line 362
    const/4 v1, 0x1

    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 364
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    .line 365
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->getArtist()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    .line 366
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v1, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x58d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 370
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bluetoothprotocal onMediaInfo"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "singer::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v3, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :goto_0
    return-void

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
    .locals 3
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;

    .prologue
    .line 379
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bluetoothprotocal MediaPlayStatus"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->getMediaPlayStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 380
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->getMediaPlayStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PLAYING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    if-ne v0, v1, :cond_0

    .line 381
    const/4 v0, 0x1

    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 382
    const/16 v0, 0x58a

    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    .line 383
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->readMediaInfo()V

    .line 395
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 396
    return-void

    .line 386
    :cond_0
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->getMediaPlayStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    if-ne v0, v1, :cond_1

    .line 387
    const/16 v0, 0x58b

    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    goto :goto_0

    .line 390
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const-string v1, " "

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    .line 391
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const-string v1, " "

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    .line 392
    const/16 v0, 0x58c

    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    goto :goto_0
.end method

.method public onMediaStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;)V
    .locals 0
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;

    .prologue
    .line 400
    return-void
.end method

.method public onPairedDevice(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceAddress"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 760
    return-void
.end method

.method public onPairingList(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;)V
    .locals 7
    .param p1, "list"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;

    .prologue
    .line 404
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onPairingList "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;->getUnits()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 405
    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 406
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;->getUnits()Ljava/util/List;

    move-result-object v2

    .line 407
    .local v2, "tempList":Ljava/util/List;, "Ljava/util/List<Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 414
    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 415
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "#############  equipList.isEmpty()"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 417
    const-wide/16 v4, 0x7d0

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 418
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->readPairingList()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :goto_1
    return-void

    .line 408
    :cond_0
    new-instance v1, Lcom/touchus/benchilauncher/bean/Equip;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/bean/Equip;-><init>()V

    .line 409
    .local v1, "temp":Lcom/touchus/benchilauncher/bean/Equip;
    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/bean/Equip;->setIndex(I)V

    .line 410
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/bean/Equip;->setName(Ljava/lang/String;)V

    .line 411
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;->getDeviceAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/bean/Equip;->setAddress(Ljava/lang/String;)V

    .line 412
    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    .end local v1    # "temp":Lcom/touchus/benchilauncher/bean/Equip;
    :cond_1
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "addr : "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/Equip;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/Equip;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 419
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public onPairingModeEnd()V
    .locals 3

    .prologue
    .line 771
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->onPairingResult:Z

    .line 772
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x57a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 773
    return-void
.end method

.method public onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V
    .locals 3
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;

    .prologue
    .line 764
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;->isSuccess()Z

    move-result v1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->onPairingResult:Z

    .line 765
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPairingModeResult "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->onPairingResult:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x579

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 767
    return-void
.end method

.method public onPhoneBook(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 715
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$17(Lcom/touchus/benchilauncher/service/BluetoothService;)I

    move-result v2

    if-ge v2, v5, :cond_0

    .line 716
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bookList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 717
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/16 v3, 0x57f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 719
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v5, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 721
    new-instance v0, Lcom/touchus/benchilauncher/bean/Person;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/bean/Person;-><init>()V

    .line 722
    .local v0, "addr":Lcom/touchus/benchilauncher/bean/Person;
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$17(Lcom/touchus/benchilauncher/service/BluetoothService;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/bean/Person;->setId(Ljava/lang/String;)V

    .line 723
    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/bean/Person;->setName(Ljava/lang/String;)V

    .line 724
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/bean/Person;->setFlag(I)V

    .line 725
    const-string v2, " "

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 726
    const-string v2, "-"

    const-string v3, ""

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/bean/Person;->setPhone(Ljava/lang/String;)V

    .line 727
    invoke-static {p1}, Lcom/unibroad/c2py/Parser;->getAllPinyin(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 728
    .local v1, "sortString":Ljava/lang/String;
    const-string v2, "\u6211\u7684\u7f16\u53f7"

    invoke-virtual {v2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 729
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0000"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 735
    :goto_0
    return-void

    .line 732
    :cond_1
    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/bean/Person;->setRemark(Ljava/lang/String;)V

    .line 733
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$17(Lcom/touchus/benchilauncher/service/BluetoothService;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$18(Lcom/touchus/benchilauncher/service/BluetoothService;I)V

    .line 734
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bookList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onPhoneBookCtrlStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;)V
    .locals 5
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;

    .prologue
    const/4 v4, 0x0

    .line 429
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->getPhoneBookCtrlStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    if-ne v0, v1, :cond_2

    .line 430
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$6(Lcom/touchus/benchilauncher/service/BluetoothService;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 432
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x1

    iput v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 433
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadType:I

    if-nez v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadPhoneBook()V

    .line 438
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x57f

    invoke-virtual {v0, v1, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 444
    :cond_0
    :goto_1
    return-void

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadRecordList()V

    goto :goto_0

    .line 439
    :cond_2
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->getPhoneBookCtrlStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    if-ne v0, v1, :cond_0

    .line 441
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, -0x1

    iput v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 442
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x580

    invoke-virtual {v0, v1, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_1
.end method

.method public onPhoneBookList(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;)V
    .locals 14
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .prologue
    .line 449
    iget-object v11, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$7(Lcom/touchus/benchilauncher/service/BluetoothService;J)V

    .line 451
    :try_start_0
    new-instance v6, Lcom/touchus/benchilauncher/utils/LibVcard;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getPlayload()[B

    move-result-object v11

    invoke-direct {v6, v11}, Lcom/touchus/benchilauncher/utils/LibVcard;-><init>([B)V

    .line 452
    .local v6, "libVcard":Lcom/touchus/benchilauncher/utils/LibVcard;
    invoke-virtual {v6}, Lcom/touchus/benchilauncher/utils/LibVcard;->getList()Ljava/util/List;

    move-result-object v7

    .line 453
    .local v7, "list":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/utils/VcardUnit;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 454
    .local v1, "bookList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    iget-object v11, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget v11, v11, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadType:I

    if-nez v11, :cond_1

    .line 455
    new-instance v3, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    iget-object v12, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v12, v12, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 456
    const-string v12, "_Per.xml"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 455
    invoke-direct {v3, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 457
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 458
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 460
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 461
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-lt v4, v11, :cond_2

    .line 479
    new-instance v11, Lcom/touchus/benchilauncher/utils/PinyinComparator;

    invoke-direct {v11}, Lcom/touchus/benchilauncher/utils/PinyinComparator;-><init>()V

    invoke-static {v1, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 480
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 481
    .local v8, "outStream":Ljava/io/FileOutputStream;
    invoke-static {v1, v8}, Lcom/touchus/benchilauncher/utils/PersonParse;->save(Ljava/util/ArrayList;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    .end local v1    # "bookList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "i":I
    .end local v6    # "libVcard":Lcom/touchus/benchilauncher/utils/LibVcard;
    .end local v7    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/utils/VcardUnit;>;"
    .end local v8    # "outStream":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    iget-object v11, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v12, 0x2

    iput v12, v11, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 490
    iget-object v11, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v11}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v11

    const/16 v12, 0x581

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 491
    iget-object v11, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v11}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBookchanged()V

    .line 492
    iget-object v11, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$7(Lcom/touchus/benchilauncher/service/BluetoothService;J)V

    .line 493
    return-void

    .line 462
    .restart local v1    # "bookList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "i":I
    .restart local v6    # "libVcard":Lcom/touchus/benchilauncher/utils/LibVcard;
    .restart local v7    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/utils/VcardUnit;>;"
    :cond_2
    :try_start_1
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v11}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getNumbers()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v9

    .line 463
    .local v9, "phonesize":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-lt v5, v9, :cond_3

    .line 461
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 464
    :cond_3
    new-instance v0, Lcom/touchus/benchilauncher/bean/Person;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/bean/Person;-><init>()V

    .line 465
    .local v0, "addr":Lcom/touchus/benchilauncher/bean/Person;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "0000"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/touchus/benchilauncher/bean/Person;->setId(Ljava/lang/String;)V

    .line 466
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v11}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/touchus/benchilauncher/bean/Person;->setName(Ljava/lang/String;)V

    .line 467
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Lcom/touchus/benchilauncher/bean/Person;->setFlag(I)V

    .line 468
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v11}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getNumbers()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v0, v11}, Lcom/touchus/benchilauncher/bean/Person;->setPhone(Ljava/lang/String;)V

    .line 470
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v11}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getName()Ljava/lang/String;

    move-result-object v11

    .line 469
    invoke-static {v11}, Lcom/unibroad/c2py/Parser;->getAllPinyin(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 470
    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 471
    .local v10, "sortString":Ljava/lang/String;
    const-string v12, "\u6211\u7684\u7f16\u53f7"

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v11}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 472
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "0000"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 463
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 475
    :cond_4
    invoke-virtual {v0, v10}, Lcom/touchus/benchilauncher/bean/Person;->setRemark(Ljava/lang/String;)V

    .line 476
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 484
    .end local v0    # "addr":Lcom/touchus/benchilauncher/bean/Person;
    .end local v1    # "bookList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "i":I
    .end local v5    # "j":I
    .end local v6    # "libVcard":Lcom/touchus/benchilauncher/utils/LibVcard;
    .end local v7    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/benchilauncher/utils/VcardUnit;>;"
    .end local v9    # "phonesize":I
    .end local v10    # "sortString":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 485
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 486
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "parse error "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onPhoneCallingOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;)V
    .locals 5
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;

    .prologue
    const/16 v4, 0x585

    const/4 v3, 0x0

    .line 687
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 688
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FloatSystemCallDialog  onPhoneCallingOut...."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 689
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 688
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 690
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 691
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    .line 692
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->OUTCALLING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    .line 691
    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setShowStatus(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;)V

    .line 693
    sput v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 694
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    .line 695
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x587

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 697
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->getbookName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    .line 698
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->setHistoryList(I)V

    .line 700
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x58e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 701
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 702
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestAudioFocus()V

    .line 704
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    if-nez v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    if-eqz v0, :cond_2

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->interAndroidView()V

    .line 707
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 708
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 709
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 711
    :cond_2
    return-void
.end method

.method public onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V
    .locals 11
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    .prologue
    const/16 v10, 0x583

    const/16 v9, 0x584

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 497
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    if-nez v0, :cond_1

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 501
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPhoneStatus: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 502
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_2

    .line 503
    sput-boolean v7, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 504
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: CALLING_OUT"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->accFlag:Z

    .line 506
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    .line 507
    const/16 v0, 0x585

    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 508
    sput-boolean v7, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    goto :goto_0

    .line 509
    :cond_2
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_6

    .line 510
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: CONNECTED"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 511
    sput-boolean v7, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 512
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    .line 513
    sget-boolean v0, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-eqz v0, :cond_3

    .line 514
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    .line 515
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    .line 517
    :cond_3
    sget-boolean v0, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    if-eqz v0, :cond_5

    .line 518
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v1, 0x585

    if-eq v0, v1, :cond_4

    .line 521
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->histalkflag:Z

    if-nez v0, :cond_4

    .line 525
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 526
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/16 v1, 0x586

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->setHistoryList(I)V

    .line 530
    :cond_4
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    .line 531
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->histalkflag:Z

    .line 532
    sput v3, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 533
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "----------------app.phoneNumber = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",app.phoneName ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 534
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 535
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    .line 537
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$8(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    .line 538
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$9(Lcom/touchus/benchilauncher/service/BluetoothService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "addr"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    .line 539
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$9(Lcom/touchus/benchilauncher/service/BluetoothService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "name"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    .line 540
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x1772

    invoke-virtual {v0, v1, v8}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 541
    :cond_6
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_7

    .line 542
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: UNCONNECT"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 543
    sput-boolean v3, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 544
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 545
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    .line 546
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    .line 547
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastindex:I

    .line 548
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->lasthisindex:I

    .line 549
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, -0x1

    iput v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 550
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    .line 551
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    .line 552
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const-string v1, " "

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    .line 553
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const-string v1, " "

    iput-object v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    .line 555
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->cutdownCurrentCalling()V

    .line 556
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x58d

    invoke-virtual {v0, v1, v8}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 558
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x1772

    invoke-virtual {v0, v1, v8}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 559
    :cond_7
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_b

    .line 560
    sput-boolean v7, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 561
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: TALKING"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 562
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 563
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->callnum:[I

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$10(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v4

    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->callVoice:Ljava/lang/String;

    const/4 v6, 0x5

    invoke-virtual {v4, v5, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget v2, v2, v4

    move v4, v3

    move v5, v3

    .line 562
    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 564
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    if-nez v0, :cond_8

    .line 565
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: NoCallTALKING"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 568
    :cond_8
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    .line 569
    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->TALKING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    .line 568
    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setShowStatus(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;)V

    .line 570
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    .line 572
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    .line 573
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->histalkflag:Z

    .line 574
    sget-boolean v0, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-nez v0, :cond_9

    .line 575
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    .line 576
    sput-boolean v7, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    .line 577
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0, v10, v8}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 578
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-direct {v2, v3, v8}, Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$11(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/Thread;)V

    .line 579
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$12(Lcom/touchus/benchilauncher/service/BluetoothService;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 581
    :cond_9
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestAudioFocus()V

    .line 582
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$13(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    .line 583
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "bluetoothlog  TALKING..........."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 585
    sget v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    if-ne v0, v9, :cond_a

    .line 586
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0, v9}, Lcom/touchus/benchilauncher/service/BluetoothService;->setHistoryList(I)V

    .line 588
    :cond_a
    sput v10, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 589
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->topIsBlueMainFragment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isDestory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x58e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 596
    :cond_b
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_c

    .line 597
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: CONNECTING"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 598
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    .line 599
    sput-boolean v3, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    goto/16 :goto_0

    .line 600
    :cond_c
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_0

    .line 601
    sput-boolean v7, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 602
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FloatSystemCallDialog onPhoneStatus: INCOMING_CALL"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 603
    sput-boolean v7, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    .line 604
    sput v9, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 605
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    .line 606
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->accFlag:Z

    goto/16 :goto_0
.end method

.method public onSetPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;)V
    .locals 0
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;

    .prologue
    .line 612
    return-void
.end method

.method public onVersion(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;)V
    .locals 0
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;

    .prologue
    .line 616
    return-void
.end method

.method public ondeviceSwitchedProtocol(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V
    .locals 3
    .param p1, "arg0"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;

    .prologue
    .line 656
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FloatSystemCallDialog  [sound] ondeviceSwitchedProtocol: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 657
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;->getConnectedDevice()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 656
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 658
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;->getConnectedDevice()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;->LOCAL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    if-ne v0, v1, :cond_0

    .line 659
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    .line 664
    :goto_0
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bluetooth ondeviceSwitchedProtocol requestAudioFocus isAfterInOrOutCall : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 678
    return-void

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    goto :goto_0
.end method
