.class public Lcom/touchus/benchilauncher/service/BluetoothService;
.super Landroid/app/Service;
.source "BluetoothService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;,
        Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;,
        Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;
    }
.end annotation


# static fields
.field public static final BOOK_LIST_CANCEL_LOAD:I = 0x580

.field public static final BOOK_LIST_START_LOAD:I = 0x57f

.field public static final CALL_TALKING:I = 0x583

.field public static final CALL_TYPE_IN:I = 0x584

.field public static final CALL_TYPE_MISS:I = 0x586

.field public static final CALL_TYPE_OUT:I = 0x585

.field public static final DEVICE_SWITCH:I = 0x582

.field public static final DRIVER_BOOK_LIST:I = 0x581

.field public static final END_PAIRMODE:I = 0x57a

.field public static final HANGUP_PHONE:I = 0x588

.field public static final MEDIAINFO:I = 0x58d

.field public static final ONGOING_NOTIFICATION:I = 0x578

.field public static final OUTCALL_FLOAT:I = 0x58e

.field public static final PLAY_STATE_PAUSE:I = 0x58b

.field public static final PLAY_STATE_PLAYING:I = 0x58a

.field public static final PLAY_STATE_STOP:I = 0x58c

.field public static final START_PAIRMODE:I = 0x579

.field public static final TIME_FLAG:I = 0x589

.field public static final UPDATE_NAME:I = 0x57e

.field public static final UPDATE_PHONENUM:I = 0x587

.field public static bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static currentCallingType:I

.field public static currentEquipAddress:Ljava/lang/String;

.field public static deviceName:Ljava/lang/String;

.field public static equipList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Equip;",
            ">;"
        }
    .end annotation
.end field

.field public static iIsCallingInButNotalkingState:Z

.field private static logger:Lorg/slf4j/Logger;

.field public static mediaPlayState:I

.field public static talkingflag:Z


# instance fields
.field public Apppath:Ljava/lang/String;

.field public accFlag:Z

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private assistantReceiver:Landroid/content/BroadcastReceiver;

.field public blueMusicFocus:Z

.field public bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

.field bookList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation
.end field

.field private booklistIndex:I

.field public calltime:I

.field public changeAudioResult:Z

.field public currentEquipName:Ljava/lang/String;

.field public downloadType:I

.field public downloadflag:I

.field public histalkflag:Z

.field private iHoldFocus:Z

.field public iSoundInPhone:Z

.field public ipause:Z

.field public isAfterInOrOutCall:Z

.field isDownloading:Z

.field private lastCloseCallingViewTime:J

.field private lastPhoneBookDownloadTime:J

.field public lasthisindex:I

.field public lastindex:I

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public mAudioManager:Landroid/media/AudioManager;

.field public mBinder:Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;

.field private mBlueMusicFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mContext:Landroid/content/Context;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field public music:Ljava/lang/String;

.field public onPairingResult:Z

.field private serviceHandler:Landroid/os/Handler;

.field private shared_device:Landroid/content/SharedPreferences;

.field public showDialogflag:Z

.field public singer:Ljava/lang/String;

.field private timeThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    const-class v0, Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 87
    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    .line 124
    const/4 v0, 0x1

    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    .line 138
    sput-boolean v1, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->equipList:Ljava/util/ArrayList;

    .line 148
    sput-boolean v1, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    .line 152
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 168
    const-string v0, "Benz"

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->deviceName:Ljava/lang/String;

    .line 169
    const-string v0, ""

    sput-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 136
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->accFlag:Z

    .line 140
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->histalkflag:Z

    .line 142
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    .line 146
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->isAfterInOrOutCall:Z

    .line 150
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    .line 154
    iput v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    .line 156
    iput v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastindex:I

    .line 158
    iput v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lasthisindex:I

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 164
    iput v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadType:I

    .line 166
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastPhoneBookDownloadTime:J

    .line 170
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    .line 171
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->music:Ljava/lang/String;

    .line 172
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->singer:Ljava/lang/String;

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bookList:Ljava/util/ArrayList;

    .line 174
    iput v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->booklistIndex:I

    .line 175
    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    .line 176
    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->timeThread:Ljava/lang/Thread;

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 180
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/unibroad/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    .line 181
    new-instance v0, Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mBinder:Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;

    .line 184
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->onPairingResult:Z

    .line 185
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->serviceHandler:Landroid/os/Handler;

    .line 186
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->changeAudioResult:Z

    .line 187
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    .line 246
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iHoldFocus:Z

    .line 271
    new-instance v0, Lcom/touchus/benchilauncher/service/BluetoothService$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$1;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 305
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastCloseCallingViewTime:J

    .line 836
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->isDownloading:Z

    .line 837
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    .line 1271
    new-instance v0, Lcom/touchus/benchilauncher/service/BluetoothService$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$2;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mBlueMusicFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1421
    new-instance v0, Lcom/touchus/benchilauncher/service/BluetoothService$3;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$3;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->assistantReceiver:Landroid/content/BroadcastReceiver;

    .line 86
    return-void
.end method

.method private CreateFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 986
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 987
    .local v1, "file1":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 988
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 994
    .end local v1    # "file1":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 990
    :catch_0
    move-exception v0

    .line 991
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 992
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "create file error : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private CreatePerFile(Ljava/lang/String;)V
    .locals 5
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 1015
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/_Per.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1016
    .local v1, "filetxt":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1017
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 1019
    :cond_0
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 1020
    .local v2, "writer":Ljava/io/BufferedWriter;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "_Per.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1021
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1026
    .end local v1    # "filetxt":Ljava/io/File;
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    :goto_0
    return-void

    .line 1023
    :catch_0
    move-exception v0

    .line 1024
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/service/BluetoothService;Z)V
    .locals 0

    .prologue
    .line 246
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iHoldFocus:Z

    return-void
.end method

.method static synthetic access$10(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/utils/SpUtilK;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    return-object v0
.end method

.method static synthetic access$11(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/Thread;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->timeThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$12(Lcom/touchus/benchilauncher/service/BluetoothService;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->timeThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$13(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 1403
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendVoiceAssistantInTalkingEvent()V

    return-void
.end method

.method static synthetic access$14(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 973
    invoke-direct {p0, p1, p2}, Lcom/touchus/benchilauncher/service/BluetoothService;->putDeviceInfo(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$15(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 984
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/service/BluetoothService;->CreateFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$16(Lcom/touchus/benchilauncher/service/BluetoothService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1013
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/service/BluetoothService;->CreatePerFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$17(Lcom/touchus/benchilauncher/service/BluetoothService;)I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->booklistIndex:I

    return v0
.end method

.method static synthetic access$18(Lcom/touchus/benchilauncher/service/BluetoothService;I)V
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->booklistIndex:I

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/service/BluetoothService;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/service/BluetoothService;)J
    .locals 2

    .prologue
    .line 305
    iget-wide v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastCloseCallingViewTime:J

    return-wide v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 1392
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendVoiceAssistantStartEvent()V

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/service/BluetoothService;)J
    .locals 2

    .prologue
    .line 166
    iget-wide v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastPhoneBookDownloadTime:J

    return-wide v0
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/service/BluetoothService;J)V
    .locals 1

    .prologue
    .line 166
    iput-wide p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastPhoneBookDownloadTime:J

    return-void
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 1412
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendVoiceAssistantCallEndEvent()V

    return-void
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/service/BluetoothService;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->shared_device:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private putDeviceInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "addr"    # Ljava/lang/String;

    .prologue
    .line 974
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 981
    :goto_0
    return-void

    .line 977
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->shared_device:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 978
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "name"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 979
    const-string v1, "addr"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 980
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private sendVoiceAssistantCallEndEvent()V
    .locals 3

    .prologue
    .line 1413
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1414
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.voiceassistant.start.other.app"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1415
    const-string v1, "iNeedEnterIncall"

    const-string v2, "end"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1416
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1417
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1418
    return-void
.end method

.method private sendVoiceAssistantInTalkingEvent()V
    .locals 3

    .prologue
    .line 1404
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1405
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.voiceassistant.start.other.app"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1406
    const-string v1, "iNeedEnterIncall"

    const-string v2, "talking"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1407
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1408
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1409
    return-void
.end method

.method private sendVoiceAssistantStartEvent()V
    .locals 3

    .prologue
    .line 1393
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1394
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.voiceassistant.start.other.app"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1395
    const-string v1, "iNeedEnterIncall"

    const-string v2, "incall"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1396
    const-string v1, "name"

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1397
    const-string v1, "number"

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1398
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1399
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1400
    return-void
.end method


# virtual methods
.method public CallOut(Ljava/lang/String;)V
    .locals 3
    .param p1, "phonenum"    # Ljava/lang/String;

    .prologue
    .line 942
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_0

    const-string v1, "*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 943
    const-string v1, "#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 945
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mContext:Landroid/content/Context;

    .line 946
    const v2, 0x7f070037

    .line 945
    invoke-static {v1, v2}, Lcom/touchus/benchilauncher/utils/ToastTool;->showBigShortToast(Landroid/content/Context;I)V

    .line 960
    :goto_0
    return-void

    .line 949
    :cond_1
    const/16 v1, 0x585

    sput v1, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    .line 950
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sput-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 951
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object p1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 952
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->clearView()V

    .line 953
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    .line 954
    sget-object v2, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->OUTCALLING:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    .line 953
    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setShowStatus(Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;)V

    .line 955
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->call(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 957
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public answerCalling()V
    .locals 2

    .prologue
    .line 1062
    :try_start_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestAudioFocus()V

    .line 1063
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->answerThePhone()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1067
    :goto_0
    return-void

    .line 1064
    :catch_0
    move-exception v0

    .line 1065
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public cancelDownloadPhoneBook()V
    .locals 2

    .prologue
    .line 1185
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->cancelDownloadPhoneBook()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1189
    :goto_0
    return-void

    .line 1186
    :catch_0
    move-exception v0

    .line 1187
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public connectDevice(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1099
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->connectDeviceSync(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1103
    :goto_0
    return-void

    .line 1100
    :catch_0
    move-exception v0

    .line 1101
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public cutdownCurrentCalling()V
    .locals 4

    .prologue
    .line 1030
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastCloseCallingViewTime:J

    .line 1031
    sget v1, Lcom/touchus/benchilauncher/service/BluetoothService;->currentCallingType:I

    const/16 v2, 0x585

    if-eq v1, v2, :cond_0

    .line 1034
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->histalkflag:Z

    if-nez v1, :cond_0

    .line 1038
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1039
    const/16 v1, 0x586

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->setHistoryList(I)V

    .line 1043
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v2, ""

    iput-object v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 1044
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v2, ""

    iput-object v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    .line 1045
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 1046
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->unrequestAudioFocus()V

    .line 1048
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->hangUpThePhone()V

    .line 1050
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    .line 1051
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1056
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendVoiceAssistantCallEndEvent()V

    .line 1057
    return-void

    .line 1053
    :catch_0
    move-exception v0

    .line 1054
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public decVolume()V
    .locals 1

    .prologue
    .line 1007
    :try_start_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->decVolume()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1010
    :goto_0
    return-void

    .line 1008
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disconnectCurDevice()V
    .locals 2

    .prologue
    .line 1090
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->disconnectCurrentDevice()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1094
    :goto_0
    return-void

    .line 1091
    :catch_0
    move-exception v0

    .line 1092
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public downloadPhoneBook()V
    .locals 3

    .prologue
    .line 1167
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->downloadPhoneBookSync(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1171
    :goto_0
    return-void

    .line 1168
    :catch_0
    move-exception v0

    .line 1169
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public downloadRecordList()V
    .locals 3

    .prologue
    .line 1176
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->downloadPhoneBookSync(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1180
    :goto_0
    return-void

    .line 1177
    :catch_0
    move-exception v0

    .line 1178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public enterPairingMode()V
    .locals 2

    .prologue
    .line 1072
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->enterPairingModeSync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1076
    :goto_0
    return-void

    .line 1073
    :catch_0
    move-exception v0

    .line 1074
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public enterSystemFloatCallView()V
    .locals 5

    .prologue
    .line 1361
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    const-string v3, "FloatSystemCallDialog >>>>>>enterSystemFloatCallView"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1362
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/LauncherApplication;->topIsBlueMainFragment()Z

    move-result v0

    .line 1363
    .local v0, "flag":Z
    if-eqz v0, :cond_0

    .line 1374
    :goto_0
    return-void

    .line 1367
    :cond_0
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    .line 1368
    .local v1, "floatCallDialog":Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isDestory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1369
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->showFloatCallView(Landroid/content/Context;)V

    .line 1373
    :goto_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/16 v3, 0x410

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1371
    :cond_1
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->show()V

    goto :goto_1
.end method

.method public exitsource()V
    .locals 3

    .prologue
    .line 965
    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/touchus/benchilauncher/service/BluetoothService;->iIsCallingInButNotalkingState:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 969
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->unrequestAudioFocus()V

    .line 970
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v1

    const-string v2, "-1"

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->setCallingPhonenumber(Ljava/lang/String;)V

    .line 971
    return-void

    .line 966
    :catch_0
    move-exception v0

    .line 967
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getBookList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation

    .prologue
    .line 841
    const/4 v2, 0x0

    .line 842
    .local v2, "input":Ljava/io/FileInputStream;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 843
    .local v4, "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    sget-boolean v6, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-nez v6, :cond_0

    move-object v5, v4

    .line 871
    .end local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .local v5, "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :goto_0
    return-object v5

    .line 847
    .end local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 848
    const-string v7, "_Per.xml"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 847
    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 849
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 850
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 852
    :cond_1
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 853
    .end local v2    # "input":Ljava/io/FileInputStream;
    .local v3, "input":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {v3}, Lcom/touchus/benchilauncher/utils/PersonParse;->getPersons(Ljava/io/InputStream;)Ljava/util/ArrayList;

    move-result-object v4

    .line 854
    if-nez v4, :cond_2

    .line 855
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 856
    .end local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .local v5, "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :try_start_2
    sget-object v6, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onPhoneBookList::getBookList::"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v4, v5

    .line 858
    .end local v5    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :cond_2
    :try_start_3
    sget-object v6, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onPhoneBookList::getBookList::"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 863
    if-eqz v3, :cond_3

    .line 865
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_3
    :goto_1
    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    move-object v5, v4

    .line 859
    .local v5, "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    goto/16 :goto_0

    .line 866
    .end local v2    # "input":Ljava/io/FileInputStream;
    .end local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 867
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 860
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 861
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 863
    if-eqz v2, :cond_4

    .line 865
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_3
    move-object v5, v4

    .line 871
    .restart local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    goto/16 :goto_0

    .line 866
    .end local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 867
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 862
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 863
    :goto_4
    if-eqz v2, :cond_5

    .line 865
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 870
    :cond_5
    :goto_5
    throw v6

    .line 866
    :catch_3
    move-exception v0

    .line 867
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 862
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v2    # "input":Ljava/io/FileInputStream;
    .end local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v3    # "input":Ljava/io/FileInputStream;
    .local v5, "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :catchall_2
    move-exception v6

    move-object v4, v5

    .end local v5    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_4

    .line 860
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v2    # "input":Ljava/io/FileInputStream;
    .end local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v3    # "input":Ljava/io/FileInputStream;
    .restart local v5    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :catch_5
    move-exception v0

    move-object v4, v5

    .end local v5    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1377
    sget-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getHistoryList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation

    .prologue
    .line 876
    const/4 v2, 0x0

    .line 877
    .local v2, "input":Ljava/io/FileInputStream;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 878
    .local v4, "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    sget-boolean v6, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-nez v6, :cond_0

    move-object v5, v4

    .line 900
    .end local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .local v5, "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :goto_0
    return-object v5

    .line 882
    .end local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v4    # "per":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 883
    const-string v7, "_His.xml"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 882
    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 884
    .local v1, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 885
    .end local v2    # "input":Ljava/io/FileInputStream;
    .local v3, "input":Ljava/io/FileInputStream;
    :try_start_1
    invoke-static {v3}, Lcom/touchus/benchilauncher/utils/PersonParse;->getPersons(Ljava/io/InputStream;)Ljava/util/ArrayList;

    move-result-object v4

    .line 886
    sget-object v6, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onPhoneBookList::getHistoryList::"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 892
    if-eqz v3, :cond_1

    .line 894
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_1
    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    move-object v5, v4

    .line 887
    .restart local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    goto :goto_0

    .line 895
    .end local v2    # "input":Ljava/io/FileInputStream;
    .end local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 896
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 888
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 889
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v6, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    const-string v7, "onPhoneBookList::getHistoryList::Exception"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 890
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 892
    if-eqz v2, :cond_2

    .line 894
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_3
    move-object v5, v4

    .line 900
    .restart local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    goto :goto_0

    .line 895
    .end local v5    # "per":Ljava/lang/Object;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 896
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 891
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 892
    :goto_4
    if-eqz v2, :cond_3

    .line 894
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 899
    :cond_3
    :goto_5
    throw v6

    .line 895
    :catch_3
    move-exception v0

    .line 896
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 891
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_4

    .line 888
    .end local v2    # "input":Ljava/io/FileInputStream;
    .restart local v3    # "input":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "input":Ljava/io/FileInputStream;
    .restart local v2    # "input":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public getbookName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "curNum"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0xa

    .line 816
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->getBookList()Ljava/util/ArrayList;

    move-result-object v3

    .line 817
    .local v3, "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    const-string v2, ""

    .line 819
    .local v2, "name":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-lt v1, v4, :cond_0

    .line 833
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .end local p1    # "curNum":Ljava/lang/String;
    :goto_2
    return-object p1

    .line 820
    .restart local p1    # "curNum":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 821
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/Person;->getName()Ljava/lang/String;

    move-result-object v2

    .line 819
    :cond_1
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 822
    :cond_2
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 823
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 824
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_1

    .line 825
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v5, :cond_1

    .line 826
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/bean/Person;->getName()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_3

    .line 829
    :catch_0
    move-exception v0

    .line 830
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 831
    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "get name error e:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    move-object p1, v2

    .line 833
    goto :goto_2
.end method

.method public incVolume()V
    .locals 1

    .prologue
    .line 999
    :try_start_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->incVolume()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1002
    :goto_0
    return-void

    .line 1000
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public leavePairingMode()V
    .locals 2

    .prologue
    .line 1081
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->leavePairingModeSync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1085
    :goto_0
    return-void

    .line 1082
    :catch_0
    move-exception v0

    .line 1083
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mBinder:Lcom/touchus/benchilauncher/service/BluetoothService$PlayerBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 198
    const-string v3, ""

    const-string v4, "BluetoothService ==== "

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :try_start_0
    iput-object p0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mContext:Landroid/content/Context;

    .line 201
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->getInstance()Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    move-result-object v3

    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    .line 202
    const-string v3, "device_name"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/touchus/benchilauncher/service/BluetoothService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->shared_device:Landroid/content/SharedPreferences;

    .line 203
    new-instance v3, Lcom/touchus/benchilauncher/utils/SpUtilK;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 204
    const-string v3, "audio"

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/service/BluetoothService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    .line 205
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    new-instance v4, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;

    invoke-direct {v4, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    invoke-virtual {v3, v4}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->addEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V

    .line 206
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v3

    new-instance v4, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;

    invoke-direct {v4, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$BluetoothHandler;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    invoke-virtual {v3, v4}, Lcom/backaudio/android/driver/Mainboard;->addEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V

    .line 207
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 209
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 211
    :cond_0
    invoke-static {}, Lcom/unibroad/c2py/Parser;->initialize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    .end local v2    # "file":Ljava/io/File;
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 217
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object p0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 219
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/LauncherApplication;->getNeedToEnterPairMode()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->openOrCloseBluetooth(Z)V

    .line 220
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->serviceHandler:Landroid/os/Handler;

    new-instance v4, Lcom/touchus/benchilauncher/service/BluetoothService$4;

    invoke-direct {v4, p0}, Lcom/touchus/benchilauncher/service/BluetoothService$4;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    .line 232
    const-wide/16 v6, 0x5dc

    .line 220
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 235
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 236
    .local v0, "assiFilter":Landroid/content/IntentFilter;
    const-string v3, "com.unibroad.voiceassistant.analyticCmd"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 237
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->assistantReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 238
    return-void

    .line 212
    .end local v0    # "assiFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 292
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 293
    sput v0, Lcom/touchus/benchilauncher/service/BluetoothService;->mediaPlayState:I

    .line 294
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->unrequestAudioFocus()V

    .line 295
    invoke-static {}, Lcom/unibroad/c2py/Parser;->release()V

    .line 296
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->stopForeground(Z)V

    .line 297
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 242
    const/4 p2, 0x1

    .line 243
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public pauseForce()V
    .locals 2

    .prologue
    .line 1249
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->pauseSync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1253
    :goto_0
    return-void

    .line 1250
    :catch_0
    move-exception v0

    .line 1251
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public pausePlaySync(Z)V
    .locals 2
    .param p1, "iPlay"    # Z

    .prologue
    .line 1241
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->pausePlaySync(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1245
    :goto_0
    return-void

    .line 1242
    :catch_0
    move-exception v0

    .line 1243
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public playNext()V
    .locals 2

    .prologue
    .line 1203
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->playNext()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1207
    :goto_0
    return-void

    .line 1204
    :catch_0
    move-exception v0

    .line 1205
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public playPrev()V
    .locals 2

    .prologue
    .line 1212
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->playPrev()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1216
    :goto_0
    return-void

    .line 1213
    :catch_0
    move-exception v0

    .line 1214
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V
    .locals 2
    .param p1, "button"    # Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    .prologue
    .line 1158
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1162
    :goto_0
    return-void

    .line 1159
    :catch_0
    move-exception v0

    .line 1160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public readDeviceAddr()V
    .locals 2

    .prologue
    .line 1127
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->readDeviceAddr()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1131
    :goto_0
    return-void

    .line 1128
    :catch_0
    move-exception v0

    .line 1129
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public readMediaInfo()V
    .locals 2

    .prologue
    .line 1229
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->readMediaInfo()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1233
    :goto_0
    return-void

    .line 1230
    :catch_0
    move-exception v0

    .line 1231
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public readMediaStatus()V
    .locals 2

    .prologue
    .line 1221
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->readMediaStatus()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1225
    :goto_0
    return-void

    .line 1222
    :catch_0
    move-exception v0

    .line 1223
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public readPairingList()V
    .locals 2

    .prologue
    .line 1136
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->readPairingListSync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1140
    :goto_0
    return-void

    .line 1137
    :catch_0
    move-exception v0

    .line 1138
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public readPhoneStatus()V
    .locals 2

    .prologue
    .line 1118
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->readPhoneStatusSync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1122
    :goto_0
    return-void

    .line 1119
    :catch_0
    move-exception v0

    .line 1120
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public removeDevice(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 1108
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p2}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->removeDevice(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1113
    :goto_0
    return-void

    .line 1109
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public requestAudioFocus()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    .line 250
    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    const-string v4, "bluetooth btservice requestAudioFocus"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 251
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v3, v4, v6, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 254
    .local v1, "flag":I
    const/4 v0, 0x0

    .line 255
    .local v0, "count":I
    :goto_0
    if-eq v1, v2, :cond_0

    if-lt v0, v5, :cond_1

    .line 261
    :cond_0
    if-ne v1, v2, :cond_2

    :goto_1
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iHoldFocus:Z

    .line 263
    return-void

    .line 256
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 257
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v3, v4, v6, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    goto :goto_0

    .line 261
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public requestMusicAudioFocus()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1320
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v3, 0x2

    iput v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 1321
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mBlueMusicFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1322
    const/4 v4, 0x3

    .line 1321
    invoke-virtual {v1, v3, v4, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 1324
    .local v0, "flag":I
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    if-ne v0, v2, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, v3, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    .line 1325
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bluetooth btservice requestMusicAudioFocus"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1326
    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->setBTVolume(I)V

    .line 1327
    sput-boolean v2, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 1328
    return-void

    .line 1324
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public savePhoneBookList(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 784
    .local p1, "newBookList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 785
    const-string v4, "_Per.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 784
    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 786
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 787
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 789
    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 790
    .local v2, "outStream":Ljava/io/FileOutputStream;
    invoke-static {p1, v2}, Lcom/touchus/benchilauncher/utils/PersonParse;->save(Ljava/util/ArrayList;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 795
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "outStream":Ljava/io/FileOutputStream;
    :goto_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/16 v4, 0x581

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 796
    return-void

    .line 791
    :catch_0
    move-exception v0

    .line 792
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 793
    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "save error "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendBookchanged()V
    .locals 2

    .prologue
    .line 1386
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1387
    .local v0, "intent1":Landroid/content/Intent;
    const-string v1, "com.unibroad.voiceassistant.contactupdate"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1388
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1389
    return-void
.end method

.method public setBTEnterACC(Z)V
    .locals 2
    .param p1, "isAccOff"    # Z

    .prologue
    .line 1351
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->setBTEnterACC(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1355
    :goto_0
    return-void

    .line 1352
    :catch_0
    move-exception v0

    .line 1353
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setBTVolume(I)V
    .locals 2
    .param p1, "volume"    # I

    .prologue
    .line 1265
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1, p1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->setBTVolume(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1269
    :goto_0
    return-void

    .line 1266
    :catch_0
    move-exception v0

    .line 1267
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setDeviceName(Landroid/content/Context;)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 1339
    invoke-static {p0}, Lcom/touchus/publicutils/utils/UtilTools;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "000000000000000"

    .line 1342
    .local v1, "imeiString":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->deviceName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->deviceName:Ljava/lang/String;

    .line 1343
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    sget-object v3, Lcom/touchus/benchilauncher/service/BluetoothService;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->setDeviceName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347
    :goto_1
    return-void

    .line 1340
    .end local v1    # "imeiString":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/touchus/publicutils/utils/UtilTools;->getIMEI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1344
    .restart local v1    # "imeiString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1345
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setHistoryList(I)V
    .locals 14
    .param p1, "flag"    # I

    .prologue
    .line 905
    const/4 v12, 0x0

    .line 907
    .local v12, "xml":Ljava/io/InputStream;
    :try_start_0
    new-instance v9, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->Apppath:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 908
    const-string v2, "_His.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 907
    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 909
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 910
    invoke-virtual {v9}, Ljava/io/File;->createNewFile()Z

    .line 912
    :cond_0
    new-instance v13, Ljava/io/FileInputStream;

    invoke-direct {v13, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 913
    .end local v12    # "xml":Ljava/io/InputStream;
    .local v13, "xml":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v13}, Lcom/touchus/benchilauncher/utils/PersonParse;->getPersons(Ljava/io/InputStream;)Ljava/util/ArrayList;

    move-result-object v11

    .line 914
    .local v11, "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0x1e

    if-le v1, v2, :cond_1

    .line 915
    const/16 v1, 0x1d

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 917
    :cond_1
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "app.phoneNumber = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",app.phoneName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 918
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 919
    .local v6, "curtime":J
    new-instance v0, Lcom/touchus/benchilauncher/bean/Person;

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 920
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 921
    :goto_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-static {}, Lcom/touchus/publicutils/utils/TimeUtils;->getSimpleCallDate()Ljava/lang/String;

    move-result-object v5

    move v4, p1

    .line 919
    invoke-direct/range {v0 .. v5}, Lcom/touchus/benchilauncher/bean/Person;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 922
    .local v0, "per":Lcom/touchus/benchilauncher/bean/Person;
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 923
    const/4 v1, 0x0

    invoke-virtual {v11, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 924
    :cond_2
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 925
    .local v10, "outStream":Ljava/io/FileOutputStream;
    invoke-static {v11, v10}, Lcom/touchus/benchilauncher/utils/PersonParse;->save(Ljava/util/ArrayList;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 929
    if-eqz v13, :cond_6

    .line 931
    :try_start_2
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v12, v13

    .line 937
    .end local v0    # "per":Lcom/touchus/benchilauncher/bean/Person;
    .end local v6    # "curtime":J
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "outStream":Ljava/io/FileOutputStream;
    .end local v11    # "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .end local v13    # "xml":Ljava/io/InputStream;
    .restart local v12    # "xml":Ljava/io/InputStream;
    :cond_3
    :goto_1
    return-void

    .line 920
    .end local v12    # "xml":Ljava/io/InputStream;
    .restart local v6    # "curtime":J
    .restart local v9    # "file":Ljava/io/File;
    .restart local v11    # "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v13    # "xml":Ljava/io/InputStream;
    :cond_4
    :try_start_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 926
    .end local v6    # "curtime":J
    .end local v9    # "file":Ljava/io/File;
    .end local v11    # "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .end local v13    # "xml":Ljava/io/InputStream;
    .restart local v12    # "xml":Ljava/io/InputStream;
    :catch_0
    move-exception v8

    .line 927
    .local v8, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 929
    if-eqz v12, :cond_3

    .line 931
    :try_start_5
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 932
    :catch_1
    move-exception v8

    .line 933
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 928
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 929
    :goto_3
    if-eqz v12, :cond_5

    .line 931
    :try_start_6
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 936
    :cond_5
    :goto_4
    throw v1

    .line 932
    :catch_2
    move-exception v8

    .line 933
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 932
    .end local v8    # "e":Ljava/io/IOException;
    .end local v12    # "xml":Ljava/io/InputStream;
    .restart local v0    # "per":Lcom/touchus/benchilauncher/bean/Person;
    .restart local v6    # "curtime":J
    .restart local v9    # "file":Ljava/io/File;
    .restart local v10    # "outStream":Ljava/io/FileOutputStream;
    .restart local v11    # "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .restart local v13    # "xml":Ljava/io/InputStream;
    :catch_3
    move-exception v8

    .line 933
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .end local v8    # "e":Ljava/io/IOException;
    :cond_6
    move-object v12, v13

    .end local v13    # "xml":Ljava/io/InputStream;
    .restart local v12    # "xml":Ljava/io/InputStream;
    goto :goto_1

    .line 928
    .end local v0    # "per":Lcom/touchus/benchilauncher/bean/Person;
    .end local v6    # "curtime":J
    .end local v10    # "outStream":Ljava/io/FileOutputStream;
    .end local v11    # "pers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    .end local v12    # "xml":Ljava/io/InputStream;
    .restart local v13    # "xml":Ljava/io/InputStream;
    :catchall_1
    move-exception v1

    move-object v12, v13

    .end local v13    # "xml":Ljava/io/InputStream;
    .restart local v12    # "xml":Ljava/io/InputStream;
    goto :goto_3

    .line 926
    .end local v12    # "xml":Ljava/io/InputStream;
    .restart local v13    # "xml":Ljava/io/InputStream;
    :catch_4
    move-exception v8

    move-object v12, v13

    .end local v13    # "xml":Ljava/io/InputStream;
    .restart local v12    # "xml":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public stopBTMusic()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1311
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    .line 1312
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->stopMusic()V

    .line 1313
    iput-boolean v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->ipause:Z

    .line 1314
    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 1315
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/BluetoothService;->unrequestMusicAudioFocus()V

    .line 1316
    return-void
.end method

.method public stopMusic()V
    .locals 2

    .prologue
    .line 1257
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->stopPlay()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1261
    :goto_0
    return-void

    .line 1258
    :catch_0
    move-exception v0

    .line 1259
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public switchDevice()V
    .locals 4

    .prologue
    .line 1146
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    .line 1147
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FloatSystemCallDialog iSoundInPhone="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1149
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    iget-boolean v2, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    invoke-virtual {v1, v2}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->switchDevice(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1153
    :goto_1
    return-void

    .line 1146
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 1150
    :catch_0
    move-exception v0

    .line 1151
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public tryToDownloadPhoneBook()V
    .locals 2

    .prologue
    .line 1194
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetooth:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->tryToDownloadPhoneBook()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1198
    :goto_0
    return-void

    .line 1195
    :catch_0
    move-exception v0

    .line 1196
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public unrequestAudioFocus()V
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    const-string v1, "mAudioFocusListener btservice unrequestAudioFocus"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 269
    return-void
.end method

.method public unrequestMusicAudioFocus()V
    .locals 3

    .prologue
    .line 1332
    sget-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->logger:Lorg/slf4j/Logger;

    const-string v1, "mAudioFocusListener btservice unrequestMusicAudioFocus"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1333
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->mBlueMusicFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1334
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0xc8

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1335
    return-void
.end method
