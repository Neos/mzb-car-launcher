.class Lcom/touchus/benchilauncher/service/BluetoothService$1;
.super Ljava/lang/Object;
.source "BluetoothService.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/service/BluetoothService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/service/BluetoothService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$1;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 273
    packed-switch p1, :pswitch_data_0

    .line 287
    :goto_0
    :pswitch_0
    return-void

    .line 277
    :pswitch_1
    invoke-static {}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$0()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAudioFocusListener bluetooth  AUDIOFOCUS_LOSS ,talkingflag:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v2, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 278
    sget-boolean v0, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$1;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->requestAudioFocus()V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$1;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$1(Lcom/touchus/benchilauncher/service/BluetoothService;Z)V

    goto :goto_0

    .line 284
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/BluetoothService$1;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$1(Lcom/touchus/benchilauncher/service/BluetoothService;Z)V

    goto :goto_0

    .line 273
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
