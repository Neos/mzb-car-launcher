.class public Lcom/touchus/benchilauncher/service/VideoService;
.super Ljava/lang/Object;
.source "VideoService.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static context:Landroid/content/Context;

.field private static cursor:Landroid/database/Cursor;

.field private static mediaBeans:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "VideoService"

    sput-object v0, Lcom/touchus/benchilauncher/service/VideoService;->TAG:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sput-object p1, Lcom/touchus/benchilauncher/service/VideoService;->context:Landroid/content/Context;

    .line 26
    return-void
.end method

.method private GetFiles(Ljava/lang/String;Z)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "isIterative"    # Z

    .prologue
    .line 116
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 117
    .local v1, "files":[Ljava/io/File;
    const/4 v4, 0x0

    .line 118
    .local v4, "num":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v1

    if-lt v2, v6, :cond_1

    .line 153
    :cond_0
    return-void

    .line 119
    :cond_1
    aget-object v0, v1, v2

    .line 120
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 121
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 122
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 121
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 124
    .local v5, "videoSuffix":Ljava/lang/String;
    invoke-static {v5}, Lcom/touchus/benchilauncher/utils/VideoType;->isVideo(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 129
    new-instance v3, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-direct {v3}, Lcom/touchus/publicutils/bean/MediaBean;-><init>()V

    .line 144
    .local v3, "mediaBean":Lcom/touchus/publicutils/bean/MediaBean;
    sget-object v6, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    .end local v3    # "mediaBean":Lcom/touchus/publicutils/bean/MediaBean;
    :cond_2
    if-eqz p2, :cond_0

    .line 118
    .end local v5    # "videoSuffix":Ljava/lang/String;
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 149
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "/."

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_3

    .line 150
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, p2}, Lcom/touchus/benchilauncher/service/VideoService;->GetFiles(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method private getFilesBySystem()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 50
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 51
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 52
    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 53
    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    .line 54
    const-string v5, "_display_name"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    .line 55
    const-string v5, "title"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    .line 56
    const-string v5, "_size"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    .line 57
    const-string v5, "_data"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    .line 58
    const-string v5, "duration"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    .line 59
    const-string v5, "mime_type"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    .line 50
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 60
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 88
    :goto_0
    return-void

    .line 64
    :cond_0
    new-instance v6, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-direct {v6}, Lcom/touchus/publicutils/bean/MediaBean;-><init>()V

    .line 65
    .local v6, "mediaBean":Lcom/touchus/publicutils/bean/MediaBean;
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 66
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 65
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/touchus/publicutils/bean/MediaBean;->setId(J)V

    .line 68
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 69
    const-string v2, "_display_name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 68
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/touchus/publicutils/bean/MediaBean;->setDisplay_name(Ljava/lang/String;)V

    .line 70
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 71
    const-string v2, "title"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 70
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/touchus/publicutils/bean/MediaBean;->setTitle(Ljava/lang/String;)V

    .line 72
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 73
    const-string v2, "mime_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 72
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/touchus/publicutils/bean/MediaBean;->setMime_type(Ljava/lang/String;)V

    .line 74
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 75
    const-string v2, "_size"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 74
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lcom/touchus/publicutils/bean/MediaBean;->setSize(J)V

    .line 76
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    .line 77
    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 76
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/touchus/publicutils/bean/MediaBean;->setData(Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    .end local v6    # "mediaBean":Lcom/touchus/publicutils/bean/MediaBean;
    :cond_1
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getVideo(I)Lcom/touchus/publicutils/bean/MediaBean;
    .locals 1
    .param p0, "id"    # I

    .prologue
    .line 103
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/publicutils/bean/MediaBean;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 96
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 97
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 96
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 99
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public getVideoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    .line 44
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/VideoService;->getFilesBySystem()V

    .line 45
    sget-object v0, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    return-object v0
.end method

.method public getVideoList(II)Ljava/util/List;
    .locals 2
    .param p1, "pageNow"    # I
    .param p2, "pageSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    .line 37
    add-int/lit8 v1, p1, -0x1

    mul-int v0, v1, p2

    .line 38
    .local v0, "offset":I
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/VideoService;->getFilesBySystem()V

    .line 39
    sget-object v1, Lcom/touchus/benchilauncher/service/VideoService;->mediaBeans:Ljava/util/List;

    return-object v1
.end method
