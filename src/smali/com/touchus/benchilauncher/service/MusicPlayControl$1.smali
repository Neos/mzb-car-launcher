.class Lcom/touchus/benchilauncher/service/MusicPlayControl$1;
.super Ljava/lang/Object;
.source "MusicPlayControl.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/service/MusicPlayControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/service/MusicPlayControl;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 312
    packed-switch p1, :pswitch_data_0

    .line 348
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 314
    :pswitch_1
    const-string v0, "requestAudioFocus"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music AudioManager.AUDIOFOCUS_LOSS   iHoldAudioFocus:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldAudioFocus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->stopLocalMusic()V

    goto :goto_0

    .line 319
    :pswitch_2
    const-string v0, "requestAudioFocus"

    const-string v1, "music  AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0, v2, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$0(Lcom/touchus/benchilauncher/service/MusicPlayControl;ZZ)V

    .line 321
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$1(Lcom/touchus/benchilauncher/service/MusicPlayControl;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v0, :cond_2

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$2(Lcom/touchus/benchilauncher/service/MusicPlayControl;)Landroid/media/MediaPlayer;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    sget v2, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 324
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->pauseMusic()V

    goto :goto_0

    .line 330
    :pswitch_3
    const-string v0, "requestAudioFocus"

    const-string v1, "music  AudioManager.AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0, v2, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$0(Lcom/touchus/benchilauncher/service/MusicPlayControl;ZZ)V

    .line 332
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->pauseMusic()V

    goto :goto_0

    .line 339
    :pswitch_4
    const-string v0, "requestAudioFocus"

    const-string v1, "music  AudioManager.AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$2(Lcom/touchus/benchilauncher/service/MusicPlayControl;)Landroid/media/MediaPlayer;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    sget v2, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 341
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0, v3, v3}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$0(Lcom/touchus/benchilauncher/service/MusicPlayControl;ZZ)V

    .line 342
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-static {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->access$1(Lcom/touchus/benchilauncher/service/MusicPlayControl;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput v3, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 343
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    if-nez v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;->this$0:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->replayMusic()V

    goto/16 :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
