.class Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;
.super Ljava/lang/Object;
.source "BluetoothService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/service/BluetoothService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/service/BluetoothService;


# direct methods
.method private constructor <init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V
    .locals 0

    .prologue
    .line 798
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/touchus/benchilauncher/service/BluetoothService;Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;)V
    .locals 0

    .prologue
    .line 798
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;-><init>(Lcom/touchus/benchilauncher/service/BluetoothService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 802
    :goto_0
    sget-boolean v1, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-nez v1, :cond_0

    .line 811
    return-void

    .line 804
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->calltime:I

    .line 805
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/BluetoothService$TimeThread;->this$0:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->access$2(Lcom/touchus/benchilauncher/service/BluetoothService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x589

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 806
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 807
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
