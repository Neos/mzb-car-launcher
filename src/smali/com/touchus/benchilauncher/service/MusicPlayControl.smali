.class public Lcom/touchus/benchilauncher/service/MusicPlayControl;
.super Ljava/lang/Object;
.source "MusicPlayControl.java"


# static fields
.field public static final PLAY_LOOP_LOOP:I = 0x3e9

.field public static final PLAY_LOOP_RANDOM:I = 0x3eb

.field public static final PLAY_SINGE_LOOP:I = 0x3ea


# instance fields
.field private context:Landroid/content/Context;

.field public currentMusic:Lcom/touchus/publicutils/bean/MediaBean;

.field public currentPlayPath:Ljava/lang/String;

.field public iHoldAudioFocus:Z

.field public iHoldLongAudioFocus:Z

.field public iIsManualPause:Z

.field public iNeedUpsetPlayOrder:Z

.field public ipause:Z

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public mAudioManager:Landroid/media/AudioManager;

.field public mHandler:Landroid/os/Handler;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field public musicIndex:I

.field private musicList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field private musicPlayer:Landroid/media/MediaPlayer;

.field private playLoopType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "cont"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    .line 30
    iput-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 36
    iput-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->currentMusic:Lcom/touchus/publicutils/bean/MediaBean;

    .line 38
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    .line 41
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iIsManualPause:Z

    .line 43
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldAudioFocus:Z

    .line 45
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldLongAudioFocus:Z

    .line 47
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    .line 112
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iNeedUpsetPlayOrder:Z

    .line 310
    new-instance v0, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl$1;-><init>(Lcom/touchus/benchilauncher/service/MusicPlayControl;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 54
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->context:Landroid/content/Context;

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 56
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    .line 57
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->context:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioManager:Landroid/media/AudioManager;

    .line 58
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/service/MusicPlayControl;ZZ)V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setHoldAudioFocus(ZZ)V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/service/MusicPlayControl;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/service/MusicPlayControl;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/service/MusicPlayControl;)I
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->getNextIndex()I

    move-result v0

    return v0
.end method

.method private getNextIndex()I
    .locals 6

    .prologue
    .line 211
    iget v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 212
    .local v1, "index":I
    iget v4, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    const/16 v5, 0x3e9

    if-ne v4, v5, :cond_2

    .line 213
    add-int/lit8 v1, v1, 0x1

    .line 214
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 228
    :cond_0
    :goto_0
    return v1

    .line 214
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 215
    :cond_2
    iget v4, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    const/16 v5, 0x3eb

    if-ne v4, v5, :cond_0

    .line 216
    iget-object v4, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 217
    .local v3, "size":I
    const/4 v4, 0x1

    if-gt v3, v4, :cond_3

    .line 218
    const/4 v1, 0x0

    .line 219
    goto :goto_0

    .line 220
    :cond_3
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 221
    .local v2, "rdm":Ljava/util/Random;
    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 222
    .local v0, "currentIndex":I
    :goto_1
    if-eq v0, v1, :cond_4

    .line 225
    move v1, v0

    goto :goto_0

    .line 223
    :cond_4
    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    goto :goto_1
.end method

.method private play(I)V
    .locals 4
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 233
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/publicutils/bean/MediaBean;

    iput-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->currentMusic:Lcom/touchus/publicutils/bean/MediaBean;

    .line 234
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V

    .line 235
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 237
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 238
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    .line 239
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->currentMusic:Lcom/touchus/publicutils/bean/MediaBean;

    invoke-virtual {v2}, Lcom/touchus/publicutils/bean/MediaBean;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 242
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 243
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 244
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 245
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/touchus/benchilauncher/service/MusicPlayControl$2;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl$2;-><init>(Lcom/touchus/benchilauncher/service/MusicPlayControl;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 252
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/touchus/benchilauncher/service/MusicPlayControl$3;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl$3;-><init>(Lcom/touchus/benchilauncher/service/MusicPlayControl;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 259
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/touchus/benchilauncher/service/MusicPlayControl$4;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl$4;-><init>(Lcom/touchus/benchilauncher/service/MusicPlayControl;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 268
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x1b59

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 270
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 272
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method

.method private randomMusic()V
    .locals 4

    .prologue
    .line 141
    iget-object v3, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 142
    .local v2, "size":I
    const/4 v3, 0x1

    if-gt v2, v3, :cond_0

    .line 143
    const/4 v3, 0x0

    iput v3, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 152
    :goto_0
    return-void

    .line 145
    :cond_0
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 146
    .local v1, "rdm":Ljava/util/Random;
    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 147
    .local v0, "currentIndex":I
    :goto_1
    iget v3, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    if-eq v0, v3, :cond_1

    .line 150
    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    goto :goto_0

    .line 148
    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    goto :goto_1
.end method

.method private saveMusicDate()V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method private declared-synchronized setHoldAudioFocus(ZZ)V
    .locals 1
    .param p1, "flag"    # Z
    .param p2, "iSetLongFocusValue"    # Z

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldAudioFocus:Z

    .line 277
    if-eqz p2, :cond_0

    .line 278
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldLongAudioFocus:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    :cond_0
    monitor-exit p0

    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public changeCurrentPlayProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 195
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->unrequestMusicAudioFocus()V

    .line 65
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setHoldAudioFocus(ZZ)V

    .line 66
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 68
    return-void
.end method

.method public getMusicList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    return-object v0
.end method

.method public getPlayLoopType()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    return v0
.end method

.method public getPosition()I
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 77
    .local v0, "temp":I
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 80
    :goto_0
    return v0

    .line 78
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public isMusicPlaying()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    return v0
.end method

.method public pauseMusic()V
    .locals 2

    .prologue
    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    .line 157
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public declared-synchronized playMusic(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gtz v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 96
    :cond_1
    const/4 v1, -0x1

    if-eq p1, v1, :cond_2

    :try_start_1
    iget v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    if-eq v1, p1, :cond_2

    .line 97
    iput p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 99
    :cond_2
    iget v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 100
    const/4 v1, 0x0

    iput v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 102
    :cond_3
    iget v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    sput v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    .line 103
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v2, "musicPos"

    iget v3, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 104
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->play(I)V

    .line 105
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->requestAudioFocus()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public playNextMusic()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_2

    .line 119
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->randomMusic()V

    .line 124
    :goto_1
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playMusic(I)V

    goto :goto_0

    .line 121
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 122
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    :goto_2
    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public playPreviousMusic()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_2

    .line 132
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->randomMusic()V

    .line 137
    :goto_1
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playMusic(I)V

    goto :goto_0

    .line 134
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    .line 135
    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    if-ltz v0, :cond_3

    iget v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    :goto_2
    iput v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicIndex:I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public replayMusic()V
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldAudioFocus:Z

    if-nez v0, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->pauseMusic()V

    .line 183
    :goto_0
    return-void

    .line 178
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 179
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->requestAudioFocus()V

    .line 180
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public requestAudioFocus()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 288
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iput v4, v1, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 289
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2, v5, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 291
    .local v0, "flag":I
    const-string v1, "Music requestAudioFocus"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flag"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    if-ne v0, v4, :cond_0

    .line 293
    invoke-direct {p0, v4, v4}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setHoldAudioFocus(ZZ)V

    .line 303
    :goto_0
    sput-boolean v4, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 304
    return-void

    .line 295
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v1, v2, v5, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 297
    if-ne v0, v4, :cond_1

    .line 298
    invoke-direct {p0, v4, v4}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setHoldAudioFocus(ZZ)V

    goto :goto_0

    .line 300
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1, v4}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setHoldAudioFocus(ZZ)V

    goto :goto_0
.end method

.method public declared-synchronized setHoldAudioFocus(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldAudioFocus:Z

    .line 284
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->iHoldLongAudioFocus:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setPlayList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/touchus/publicutils/bean/MediaBean;>;"
    iput-object p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicList:Ljava/util/List;

    .line 85
    return-void
.end method

.method public setPlayLoopType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 190
    iput p1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playLoopType:I

    .line 191
    return-void
.end method

.method public settingPlayState(Z)V
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 199
    if-eqz p1, :cond_0

    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    if-nez v1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    if-nez v1, :cond_2

    .line 208
    :cond_1
    :goto_0
    return-void

    .line 202
    :cond_2
    sput-boolean p1, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    .line 204
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 205
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x1b5b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 206
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public stopLocalMusic()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 352
    const/4 v0, 0x1

    invoke-direct {p0, v2, v0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->setHoldAudioFocus(ZZ)V

    .line 353
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->isMusicPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->pauseMusic()V

    .line 356
    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V

    .line 357
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->destroy()V

    .line 358
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->ipause:Z

    .line 359
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 360
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->saveMusicDate()V

    .line 361
    sput-boolean v2, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 363
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->unrequestMusicAudioFocus()V

    .line 364
    return-void
.end method

.method public stopMusic()V
    .locals 2

    .prologue
    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->musicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 166
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->settingPlayState(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public unrequestMusicAudioFocus()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/touchus/benchilauncher/service/MusicPlayControl;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 308
    return-void
.end method
