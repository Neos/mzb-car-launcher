.class public Lcom/touchus/benchilauncher/service/PictrueUtil;
.super Ljava/lang/Object;
.source "PictrueUtil.java"


# static fields
.field private static context:Landroid/content/Context;

.field private static cursor:Landroid/database/Cursor;

.field private static picList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sput-object p1, Lcom/touchus/benchilauncher/service/PictrueUtil;->context:Landroid/content/Context;

    .line 21
    return-void
.end method

.method private getFilesBySystem()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 31
    sget-object v11, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 32
    .local v11, "uri":Landroid/net/Uri;
    if-nez v11, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 37
    const-string v1, "mounted"

    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 37
    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 40
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 39
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    .line 46
    :goto_1
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 63
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 48
    :cond_2
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    .line 49
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 48
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 50
    .local v6, "id":J
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    .line 51
    const-string v2, "_display_name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 50
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 53
    .local v9, "name":Ljava/lang/String;
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    .line 54
    const-string v2, "description"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 53
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 55
    .local v8, "info":Ljava/lang/String;
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    sget-object v1, Lcom/touchus/benchilauncher/service/PictrueUtil;->cursor:Landroid/database/Cursor;

    .line 56
    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 55
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 57
    .local v12, "url":Ljava/lang/String;
    new-instance v10, Lcom/touchus/publicutils/bean/MediaBean;

    invoke-direct {v10}, Lcom/touchus/publicutils/bean/MediaBean;-><init>()V

    .line 58
    .local v10, "picBean":Lcom/touchus/publicutils/bean/MediaBean;
    invoke-virtual {v10, v6, v7}, Lcom/touchus/publicutils/bean/MediaBean;->setId(J)V

    .line 59
    invoke-virtual {v10, v9}, Lcom/touchus/publicutils/bean/MediaBean;->setTitle(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v10, v12}, Lcom/touchus/publicutils/bean/MediaBean;->setData(Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->picList:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public getPicList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->picList:Ljava/util/List;

    .line 25
    invoke-direct {p0}, Lcom/touchus/benchilauncher/service/PictrueUtil;->getFilesBySystem()V

    .line 26
    sget-object v0, Lcom/touchus/benchilauncher/service/PictrueUtil;->picList:Ljava/util/List;

    return-object v0
.end method
