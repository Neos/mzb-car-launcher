.class public Lcom/touchus/benchilauncher/activity/main/StateFrag;
.super Landroid/support/v4/app/Fragment;
.source "StateFrag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;,
        Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;,
        Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;
    }
.end annotation


# instance fields
.field activity:Lcom/touchus/benchilauncher/Launcher;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private checkWifiSignRunnable:Ljava/lang/Runnable;

.field mBt:Landroid/widget/ImageView;

.field mDate:Landroid/widget/TextView;

.field mGps:Landroid/widget/ImageView;

.field mSdcard:Landroid/widget/ImageView;

.field mSignal:Landroid/widget/ImageView;

.field mStateHandler:Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

.field mTime:Landroid/widget/TextView;

.field mUsb3:Landroid/widget/ImageView;

.field mUsbReceiver:Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;

.field mWifi:Landroid/widget/ImageView;

.field private telManager:Landroid/telephony/TelephonyManager;

.field view:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 322
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/StateFrag$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag$1;-><init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkWifiSignRunnable:Ljava/lang/Runnable;

    .line 438
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mStateHandler:Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/StateFrag;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkWifiSignRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkWifiSign()V

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/widget/ImageView;I)V
    .locals 0

    .prologue
    .line 238
    invoke-direct {p0, p1, p2}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    return-void
.end method

.method private checkWifiSign()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, -0x32

    const/16 v6, -0x46

    const/16 v5, -0x50

    .line 299
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->isWifiConnect(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 300
    invoke-virtual {p0, v8}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setWifi(I)V

    .line 320
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 305
    const-string v4, "wifi"

    .line 304
    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 306
    .local v2, "manager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 307
    .local v0, "info":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v1

    .line 308
    .local v1, "level":I
    if-gtz v1, :cond_1

    if-lt v1, v7, :cond_1

    .line 309
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setWifi(I)V

    goto :goto_0

    .line 310
    :cond_1
    if-ge v1, v7, :cond_2

    if-lt v1, v6, :cond_2

    .line 311
    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setWifi(I)V

    goto :goto_0

    .line 312
    :cond_2
    if-ge v1, v6, :cond_3

    if-lt v1, v5, :cond_3

    .line 313
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setWifi(I)V

    goto :goto_0

    .line 314
    :cond_3
    if-ge v1, v5, :cond_4

    const/16 v3, -0x64

    if-lt v1, v3, :cond_4

    .line 315
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setWifi(I)V

    goto :goto_0

    .line 317
    :cond_4
    invoke-virtual {p0, v8}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setWifi(I)V

    goto :goto_0
.end method

.method private findID()V
    .locals 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->activity:Lcom/touchus/benchilauncher/Launcher;

    .line 111
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b0142

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSignal:Landroid/widget/ImageView;

    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b0143

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mWifi:Landroid/widget/ImageView;

    .line 113
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b0148

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mBt:Landroid/widget/ImageView;

    .line 114
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b0149

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mGps:Landroid/widget/ImageView;

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSdcard:Landroid/widget/ImageView;

    .line 117
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b0146

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsb3:Landroid/widget/ImageView;

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mTime:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    const v1, 0x7f0b014c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mDate:Landroid/widget/TextView;

    .line 122
    return-void
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 392
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 436
    :goto_0
    return-void

    .line 424
    :sswitch_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setGPS()V

    goto :goto_0

    .line 427
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setTimeInfo(Landroid/os/Bundle;)V

    goto :goto_0

    .line 430
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 431
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "isRed"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setTimeColor(Z)V

    goto :goto_0

    .line 392
    nop

    :sswitch_data_0
    .sparse-switch
        0x3f1 -> :sswitch_1
        0x40e -> :sswitch_0
        0x414 -> :sswitch_2
    .end sparse-switch
.end method

.method private initDate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSignal:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateSignal(Landroid/widget/ImageView;I)V

    .line 81
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mWifi:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateWifi(Landroid/widget/ImageView;I)V

    .line 82
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setBt()V

    .line 83
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkUsb3()V

    .line 84
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setGPS()V

    .line 85
    return-void
.end method

.method public static isWifiConnect(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 294
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 295
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 296
    .local v1, "mWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    return v2
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 460
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 461
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 462
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 463
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 464
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;-><init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsbReceiver:Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;

    .line 465
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsbReceiver:Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 466
    return-void
.end method

.method private setTimeInfo(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    const/16 v13, 0xa

    .line 255
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v9, v10, Lcom/touchus/benchilauncher/LauncherApplication;->year:I

    .line 256
    .local v9, "year":I
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v10, Lcom/touchus/benchilauncher/LauncherApplication;->month:I

    .line 257
    .local v2, "month":I
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v0, v10, Lcom/touchus/benchilauncher/LauncherApplication;->day:I

    .line 258
    .local v0, "day":I
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v7, v10, Lcom/touchus/benchilauncher/LauncherApplication;->hour:I

    .line 259
    .local v7, "time":I
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v10, Lcom/touchus/benchilauncher/LauncherApplication;->min:I

    .line 260
    .local v1, "min":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 261
    .local v5, "tMonth":Ljava/lang/String;
    if-ge v2, v13, :cond_0

    .line 262
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "0"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 264
    :cond_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 265
    .local v3, "tDay":Ljava/lang/String;
    if-ge v0, v13, :cond_1

    .line 266
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "0"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 268
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 269
    .local v4, "tMin":Ljava/lang/String;
    if-ge v1, v13, :cond_2

    .line 270
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "0"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 273
    :cond_2
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mDate:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v8, v10, Lcom/touchus/benchilauncher/LauncherApplication;->timeFormat:I

    .line 275
    .local v8, "timeFormat":I
    const/16 v10, 0x18

    if-ne v8, v10, :cond_4

    .line 276
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 277
    .local v6, "tTime":Ljava/lang/String;
    if-ge v7, v13, :cond_3

    .line 278
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "0"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 280
    :cond_3
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mTime:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :goto_0
    return-void

    .line 282
    .end local v6    # "tTime":Ljava/lang/String;
    :cond_4
    const-string v6, ""

    .line 283
    .restart local v6    # "tTime":Ljava/lang/String;
    const/16 v10, 0xc

    if-gt v7, v10, :cond_5

    .line 284
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 289
    :goto_1
    iget-object v10, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mTime:Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 286
    :cond_5
    add-int/lit8 v7, v7, -0xc

    .line 287
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method private stateImg(Landroid/widget/ImageView;I)V
    .locals 1
    .param p1, "imgV"    # Landroid/widget/ImageView;
    .param p2, "state"    # I

    .prologue
    .line 239
    packed-switch p2, :pswitch_data_0

    .line 252
    :goto_0
    return-void

    .line 241
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 247
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setSelected(Z)V

    goto :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private unregisterReceiver()V
    .locals 2

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsbReceiver:Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 470
    return-void
.end method


# virtual methods
.method public checkUsb3()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 473
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/usbotg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 474
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 475
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_4

    .line 476
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsb3:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    .line 481
    :cond_1
    :goto_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    const-string v1, "storage/sdcard1"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 482
    .restart local v0    # "file":Ljava/io/File;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 483
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_5

    .line 484
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSdcard:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v2}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    .line 489
    :cond_3
    :goto_1
    return-void

    .line 477
    :cond_4
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_1

    .line 478
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsb3:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    goto :goto_0

    .line 485
    :cond_5
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_3

    .line 486
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSdcard:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    const v1, 0x7f030038

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    .line 67
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 68
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->findID()V

    .line 69
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->initDate()V

    .line 70
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 71
    const-string v2, "phone"

    .line 70
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->telManager:Landroid/telephony/TelephonyManager;

    .line 72
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;-><init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V

    .line 73
    .local v0, "lister":Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->telManager:Landroid/telephony/TelephonyManager;

    const/16 v2, 0x101

    invoke-virtual {v1, v0, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 76
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->view:Landroid/view/View;

    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 107
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 89
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 90
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mStateHandler:Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 91
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mStateHandler:Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkWifiSignRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 92
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->registerReceiver()V

    .line 93
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 98
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->unregisterReceiver()V

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mStateHandler:Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mStateHandler:Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkWifiSignRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 101
    return-void
.end method

.method public setBt()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 147
    :cond_0
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mBt:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mBt:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    goto :goto_0
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 1
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    return-void
.end method

.method public setGPS()V
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 157
    :cond_0
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isGPSLocation:Z

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mGps:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mGps:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateImg(Landroid/widget/ImageView;I)V

    goto :goto_0
.end method

.method public setSignal(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSignal:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateSignal(Landroid/widget/ImageView;I)V

    .line 137
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 1
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    return-void
.end method

.method public setTimeColor(Z)V
    .locals 2
    .param p1, "isRed"    # Z

    .prologue
    .line 125
    sget-boolean v0, Lcom/touchus/benchilauncher/SysConst;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mTime:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mTime:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setWifi(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mWifi:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->stateWifi(Landroid/widget/ImageView;I)V

    .line 141
    return-void
.end method

.method public stateSignal(Landroid/widget/ImageView;I)V
    .locals 2
    .param p1, "imgV"    # Landroid/widget/ImageView;
    .param p2, "state"    # I

    .prologue
    const v1, 0x7f0201ae

    .line 178
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag;->activity:Lcom/touchus/benchilauncher/Launcher;

    invoke-static {v0}, Lcom/touchus/benchilauncher/utils/MobileDataSwitcher;->getMobileDataState(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    packed-switch p2, :pswitch_data_0

    .line 215
    :goto_0
    return-void

    .line 181
    :pswitch_0
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 184
    :pswitch_1
    const v0, 0x7f0201af

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 187
    :pswitch_2
    const v0, 0x7f0201b1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 190
    :pswitch_3
    const v0, 0x7f0201b3

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 193
    :pswitch_4
    const v0, 0x7f0201b5

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 197
    :cond_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 199
    :pswitch_5
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 202
    :pswitch_6
    const v0, 0x7f0201b0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 205
    :pswitch_7
    const v0, 0x7f0201b2

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 208
    :pswitch_8
    const v0, 0x7f0201b4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 211
    :pswitch_9
    const v0, 0x7f0201b6

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 197
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public stateWifi(Landroid/widget/ImageView;I)V
    .locals 1
    .param p1, "imgV"    # Landroid/widget/ImageView;
    .param p2, "state"    # I

    .prologue
    .line 219
    packed-switch p2, :pswitch_data_0

    .line 236
    :goto_0
    return-void

    .line 221
    :pswitch_0
    const v0, 0x7f0201c6

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 224
    :pswitch_1
    const v0, 0x7f0201c7

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 227
    :pswitch_2
    const v0, 0x7f0201c8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 230
    :pswitch_3
    const v0, 0x7f0201c9

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 233
    :pswitch_4
    const v0, 0x7f0201ca

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
