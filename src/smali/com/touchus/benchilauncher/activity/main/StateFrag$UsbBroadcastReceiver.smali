.class Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StateFrag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/activity/main/StateFrag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UsbBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 496
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 497
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/usbotg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 498
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 499
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 500
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, v2, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsb3:Landroid/widget/ImageView;

    invoke-static {v1, v2, v4}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->access$3(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/widget/ImageView;I)V

    .line 502
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    const-string v1, "storage/sdcard1"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 503
    .restart local v0    # "file":Ljava/io/File;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 504
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_1

    .line 505
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, v2, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSdcard:Landroid/widget/ImageView;

    invoke-static {v1, v2, v4}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->access$3(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/widget/ImageView;I)V

    .line 522
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 507
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 508
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/usbotg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 509
    .restart local v0    # "file":Ljava/io/File;
    if-eqz v0, :cond_3

    .line 510
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 511
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_4

    .line 513
    :cond_3
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, v2, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mUsb3:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->access$3(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/widget/ImageView;I)V

    .line 515
    :cond_4
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    const-string v1, "storage/sdcard1"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 516
    .restart local v0    # "file":Ljava/io/File;
    if-eqz v0, :cond_5

    .line 517
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 518
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_1

    .line 519
    :cond_5
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$UsbBroadcastReceiver;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    iget-object v2, v2, Lcom/touchus/benchilauncher/activity/main/StateFrag;->mSdcard:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->access$3(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/widget/ImageView;I)V

    goto :goto_0
.end method
