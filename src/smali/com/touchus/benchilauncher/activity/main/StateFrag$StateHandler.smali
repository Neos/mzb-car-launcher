.class Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;
.super Landroid/os/Handler;
.source "StateFrag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/activity/main/StateFrag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StateHandler"
.end annotation


# instance fields
.field public mBundle:Landroid/os/Bundle;

.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/activity/main/StateFrag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V
    .locals 1
    .param p1, "instance"    # Lcom/touchus/benchilauncher/activity/main/StateFrag;

    .prologue
    .line 444
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 445
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;->target:Ljava/lang/ref/WeakReference;

    .line 446
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 454
    :goto_0
    return-void

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$StateHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-static {v0, p1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->access$2(Lcom/touchus/benchilauncher/activity/main/StateFrag;Landroid/os/Message;)V

    goto :goto_0
.end method
