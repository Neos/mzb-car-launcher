.class public Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;
.super Landroid/support/v4/app/Fragment;
.source "LeftClockFrag.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private clickCount:I

.field private clockView:Lcom/touchus/benchilauncher/views/ClockView;

.field mHandler:Landroid/os/Handler;

.field resetClickCountThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    .line 71
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag$1;-><init>(Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->resetClickCountThread:Ljava/lang/Thread;

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->mHandler:Landroid/os/Handler;

    .line 24
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;I)V
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 39
    const-string v0, "test"

    const-string v1, "click"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v0

    if-nez v0, :cond_2

    .line 41
    iput v2, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    .line 57
    :cond_0
    :goto_0
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->resetClickCountThread:Ljava/lang/Thread;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 59
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->resetClickCountThread:Ljava/lang/Thread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 67
    :cond_1
    return-void

    .line 43
    :cond_2
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    .line 44
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 45
    iput v2, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clickCount:I

    .line 46
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    if-nez v0, :cond_3

    .line 47
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "test open"

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/utils/ToastTool;->showShortToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    .line 49
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "test"

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 51
    :cond_3
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "test close"

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/utils/ToastTool;->showShortToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    .line 53
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "test"

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    const v1, 0x7f03001f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "mView":Landroid/view/View;
    const v1, 0x7f0b00b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/views/ClockView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clockView:Lcom/touchus/benchilauncher/views/ClockView;

    .line 33
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clockView:Lcom/touchus/benchilauncher/views/ClockView;

    invoke-virtual {v1, p0}, Lcom/touchus/benchilauncher/views/ClockView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 35
    return-object v0
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clockView:Lcom/touchus/benchilauncher/views/ClockView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->hour:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->min:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/views/ClockView;->setTime(ZII)V

    .line 89
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 90
    return-void
.end method

.method public updateCurrentTime()V
    .locals 4

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->clockView:Lcom/touchus/benchilauncher/views/ClockView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->hour:I

    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->min:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/views/ClockView;->setTime(ZII)V

    .line 84
    :cond_0
    return-void
.end method
