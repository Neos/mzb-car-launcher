.class public Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;
.super Landroid/os/Handler;
.source "RunningFrag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RunningHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;)V
    .locals 1
    .param p1, "instance"    # Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;

    .prologue
    .line 153
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 154
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;->target:Ljava/lang/ref/WeakReference;

    .line 155
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 159
    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3f0

    if-ne v2, v3, :cond_0

    .line 164
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 165
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "runningState"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/backaudio/android/driver/beans/CarRunInfo;

    .line 166
    .local v1, "info":Lcom/backaudio/android/driver/beans/CarRunInfo;
    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;

    invoke-static {v2, v1}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->access$0(Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    goto :goto_0
.end method
