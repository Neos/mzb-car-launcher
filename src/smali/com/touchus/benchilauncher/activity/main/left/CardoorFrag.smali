.class public Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;
.super Landroid/support/v4/app/Fragment;
.source "CardoorFrag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field public mBehind:Landroid/widget/ImageView;

.field public mDoorDes:Landroid/widget/TextView;

.field public mDoorsDes:Landroid/widget/TextView;

.field public mFront:Landroid/widget/ImageView;

.field public mHandler:Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;

.field public mImgState:Landroid/widget/ImageView;

.field public mLeftBehind:Landroid/widget/ImageView;

.field public mLeftFront:Landroid/widget/ImageView;

.field public mNormal:Landroid/widget/ImageView;

.field public mRightBehind:Landroid/widget/ImageView;

.field public mRightFront:Landroid/widget/ImageView;

.field public mRlOverlook:Landroid/widget/RelativeLayout;

.field public mRlState:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;

    .line 27
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->handleMesage(Landroid/os/Message;)V

    return-void
.end method

.method private handleMesage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3ef

    if-ne v2, v3, :cond_0

    .line 254
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 256
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "carBaseInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .line 257
    .local v1, "info":Lcom/backaudio/android/driver/beans/CarBaseInfo;
    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->settingDoorView(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    goto :goto_0
.end method

.method private hideAll()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 237
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mNormal:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mBehind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mFront:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 240
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mLeftBehind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mLeftFront:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRightBehind:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRightFront:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorsDes:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    return-void
.end method

.method private initDoorView()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    if-nez v0, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->hideAll()V

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->settingDoorView(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    goto :goto_0
.end method

.method private openDoor(I)V
    .locals 6
    .param p1, "door"    # I

    .prologue
    const v5, 0x7f070022

    const v4, 0x7f070021

    const v3, 0x7f070020

    const v2, 0x7f07001f

    const/4 v1, 0x0

    .line 139
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_1

    .line 145
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 151
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0201a0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 152
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 147
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f02019c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    const v1, 0x7f070024

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 155
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f02019d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 159
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f02019e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 163
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0201a1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 164
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    const v1, 0x7f070023

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 167
    :pswitch_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0201a2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 168
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 172
    :cond_1
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    .line 178
    :pswitch_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0200b8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 179
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 174
    :pswitch_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0200b1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 175
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    const v1, 0x7f070024

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 182
    :pswitch_8
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0200b3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 183
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 186
    :pswitch_9
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0200b5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 187
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 190
    :pswitch_a
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0200ba

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 191
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    const v1, 0x7f070023

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 194
    :pswitch_b
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    const v1, 0x7f0200bc

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 195
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 172
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private openDoors(I)V
    .locals 3
    .param p1, "door"    # I

    .prologue
    const/4 v2, 0x0

    .line 202
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mNormal:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mNormal:Landroid/widget/ImageView;

    const v1, 0x7f02019f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 208
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 234
    :goto_1
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mNormal:Landroid/widget/ImageView;

    const v1, 0x7f0200b6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 210
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mBehind:Landroid/widget/ImageView;

    const v1, 0x7f02019b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 215
    :goto_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mBehind:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mBehind:Landroid/widget/ImageView;

    const v1, 0x7f0200b0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 218
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mFront:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 221
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mLeftBehind:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 224
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mLeftFront:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 227
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRightBehind:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 230
    :pswitch_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRightFront:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 208
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private resetLeftShowInfo()V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/inface/IDoorStateParent;

    .line 135
    .local v0, "parent":Lcom/touchus/benchilauncher/inface/IDoorStateParent;
    invoke-interface {v0}, Lcom/touchus/benchilauncher/inface/IDoorStateParent;->resetLeftLayoutToRightState()V

    goto :goto_0
.end method

.method private settingDoorView(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V
    .locals 5
    .param p1, "info"    # Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 88
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v1, "openState":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiLeftBackOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiLeftFrontOpen()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_1
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiRightBackOpen()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_2
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiRightFrontOpen()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 99
    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_3
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiFront()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 102
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    :cond_4
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiBack()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 105
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_5
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->hideAll()V

    .line 108
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_7

    .line 109
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->resetLeftShowInfo()V

    .line 120
    :cond_6
    :goto_0
    return-void

    .line 110
    :cond_7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v4, :cond_8

    .line 111
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->openDoor(I)V

    .line 112
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->showMe()V

    goto :goto_0

    .line 113
    :cond_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v4, :cond_6

    .line 114
    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorsDes:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_9

    .line 118
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->showMe()V

    goto :goto_0

    .line 116
    :cond_9
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->openDoors(I)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private showMe()V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/inface/IDoorStateParent;

    .line 127
    .local v0, "parent":Lcom/touchus/benchilauncher/inface/IDoorStateParent;
    invoke-interface {v0}, Lcom/touchus/benchilauncher/inface/IDoorStateParent;->showCardoor()V

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const v1, 0x7f030031

    .line 48
    const/4 v2, 0x0

    .line 47
    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 49
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0b011a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRlState:Landroid/widget/RelativeLayout;

    .line 50
    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mImgState:Landroid/widget/ImageView;

    .line 51
    const v1, 0x7f0b0111

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRlOverlook:Landroid/widget/RelativeLayout;

    .line 52
    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRightBehind:Landroid/widget/ImageView;

    .line 53
    const v1, 0x7f0b0118

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mRightFront:Landroid/widget/ImageView;

    .line 54
    const v1, 0x7f0b0117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mLeftBehind:Landroid/widget/ImageView;

    .line 55
    const v1, 0x7f0b0116

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mLeftFront:Landroid/widget/ImageView;

    .line 56
    const v1, 0x7f0b0115

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mBehind:Landroid/widget/ImageView;

    .line 57
    const v1, 0x7f0b0114

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mFront:Landroid/widget/ImageView;

    .line 58
    const v1, 0x7f0b0112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mNormal:Landroid/widget/ImageView;

    .line 59
    const v1, 0x7f0b011c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorDes:Landroid/widget/TextView;

    .line 60
    const v1, 0x7f0b0113

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mDoorsDes:Landroid/widget/TextView;

    .line 61
    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 67
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 68
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->initDoorView()V

    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 70
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 75
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag$CardoorHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 77
    return-void
.end method
