.class public Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;
.super Landroid/support/v4/app/Fragment;
.source "RunningFrag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;
    }
.end annotation


# instance fields
.field private curSpeed_ll:Landroid/widget/LinearLayout;

.field private mApp:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mHandler:Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;

.field private mRemain:Landroid/widget/TextView;

.field private mSpeed:Landroid/widget/TextView;

.field private mSpeedLayout:Landroid/widget/LinearLayout;

.field private mSum:Landroid/widget/TextView;

.field private mTemperature:Landroid/widget/TextView;

.field private mileage_ll:Landroid/widget/LinearLayout;

.field private mzhuansu:Landroid/widget/TextView;

.field private running_speed_none:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 148
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;

    .line 28
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;Lcom/backaudio/android/driver/beans/CarRunInfo;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    return-void
.end method

.method private findID(Landroid/view/View;)V
    .locals 2
    .param p1, "mView"    # Landroid/view/View;

    .prologue
    .line 62
    const v0, 0x7f0b012e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeed:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0b0127

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->running_speed_none:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0b0129

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mRemain:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0b012a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSum:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0b012c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mTemperature:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0b012b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mzhuansu:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0b012d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeedLayout:Landroid/widget/LinearLayout;

    .line 69
    const v0, 0x7f0b0126

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->curSpeed_ll:Landroid/widget/LinearLayout;

    .line 70
    const v0, 0x7f0b0128

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mileage_ll:Landroid/widget/LinearLayout;

    .line 73
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeedLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 74
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 80
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 81
    return-void
.end method

.method private settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V
    .locals 8
    .param p1, "info"    # Lcom/backaudio/android/driver/beans/CarRunInfo;

    .prologue
    const-wide/16 v6, 0xa

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 85
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BenzModel.benzTpye = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-eq v0, v1, :cond_0

    .line 87
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mileage_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->curSpeed_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 94
    :goto_0
    if-nez p1, :cond_1

    .line 97
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->running_speed_none:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mRemain:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSum:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mTemperature:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    :goto_1
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mileage_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->curSpeed_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 105
    :cond_1
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    if-eq v0, v1, :cond_4

    .line 106
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getMileage()I

    move-result v0

    const/16 v1, 0x7ff

    if-ge v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getMileage()I

    move-result v0

    if-gtz v0, :cond_3

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->running_speed_none:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mRemain:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSum:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mzhuansu:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mTemperature:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 117
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeed:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getCurSpeed()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mRemain:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getMileage()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mzhuansu:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getRevolutions()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " rpm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSum:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getTotalMileage()J

    move-result-wide v2

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mTemperature:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getOutsideTemp()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " \u2103"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 125
    :cond_4
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getCurSpeed()I

    move-result v0

    if-ltz v0, :cond_5

    .line 126
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeedLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->running_speed_none:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeed:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getCurSpeed()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_2
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getOutsideTemp()D

    move-result-wide v0

    const-wide/high16 v2, -0x3fbc000000000000L    # -40.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_6

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getOutsideTemp()D

    move-result-wide v0

    const-wide v2, 0x4055e00000000000L    # 87.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_6

    .line 135
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mTemperature:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getOutsideTemp()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " \u2103"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    :goto_3
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getTotalMileage()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    .line 140
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSum:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getTotalMileage()J

    move-result-wide v2

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 130
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSpeedLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->running_speed_none:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->running_speed_none:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 137
    :cond_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mTemperature:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 142
    :cond_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mSum:Landroid/widget/TextView;

    const-string v1, "----"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    const v1, 0x7f030034

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 43
    .local v0, "mView":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->findID(Landroid/view/View;)V

    .line 44
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->init()V

    .line 46
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 58
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 59
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->mApp:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->carRunInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;->settingRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    .line 52
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 53
    return-void
.end method
