.class public Lcom/touchus/benchilauncher/activity/main/left/LightFrag;
.super Landroid/support/v4/app/Fragment;
.source "LightFrag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private mHandler:Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;

.field private mImgCarLight:Landroid/widget/ImageView;

.field private mLightDesc:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 47
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/left/LightFrag;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;

    .line 21
    return-void
.end method

.method private settingLightInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    const v1, 0x7f0201a4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 80
    :goto_0
    if-nez p1, :cond_1

    .line 100
    :goto_1
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    const v1, 0x7f0200be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiDistantLightOpen()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_2

    .line 85
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    const v1, 0x7f0201a3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 89
    :goto_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mLightDesc:Landroid/widget/TextView;

    const v1, 0x7f070068

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    const v1, 0x7f0200bd

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 90
    :cond_3
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->isiNearLightOpen()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 91
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    if-eqz v0, :cond_4

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    const v1, 0x7f0201a5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    :goto_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mLightDesc:Landroid/widget/TextView;

    const v1, 0x7f070067

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 94
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    const v1, 0x7f0200bf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 98
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mLightDesc:Landroid/widget/TextView;

    const v1, 0x7f070066

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public handMsg(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3ef

    if-ne v2, v3, :cond_0

    .line 68
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 69
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "carBaseInfo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .line 70
    .local v1, "info":Lcom/backaudio/android/driver/beans/CarBaseInfo;
    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->settingLightInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    const v1, 0x7f030032

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 29
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0b011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mImgCarLight:Landroid/widget/ImageView;

    .line 30
    const v1, 0x7f0b011e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mLightDesc:Landroid/widget/TextView;

    .line 31
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 37
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 38
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->settingLightInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    .line 39
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 40
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;->mHandler:Lcom/touchus/benchilauncher/activity/main/left/LightFrag$LightHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 45
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 46
    return-void
.end method
