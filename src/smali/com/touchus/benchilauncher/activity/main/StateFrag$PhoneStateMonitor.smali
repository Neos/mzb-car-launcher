.class public Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;
.super Landroid/telephony/PhoneStateListener;
.source "StateFrag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/activity/main/StateFrag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PhoneStateMonitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/activity/main/StateFrag;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 364
    invoke-super {p0, p1}, Landroid/telephony/PhoneStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V

    .line 365
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    const/4 v0, 0x5

    .line 369
    .local v0, "level":I
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 385
    :goto_1
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 386
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 371
    :pswitch_0
    const/4 v0, 0x5

    .line 372
    goto :goto_1

    .line 374
    :pswitch_1
    const/4 v0, 0x3

    .line 375
    goto :goto_1

    .line 377
    :pswitch_2
    const/4 v0, 0x5

    .line 378
    goto :goto_1

    .line 380
    :pswitch_3
    const/4 v0, 0x5

    .line 381
    goto :goto_1

    .line 369
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 8
    .param p1, "signalStrength"    # Landroid/telephony/SignalStrength;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 334
    invoke-super {p0, p1}, Landroid/telephony/PhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V

    .line 335
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_0

    .line 361
    :goto_0
    return-void

    .line 339
    :cond_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 340
    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 339
    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 341
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    .line 342
    .local v1, "networkType":I
    if-eq v1, v5, :cond_1

    .line 343
    if-ne v1, v6, :cond_2

    .line 344
    :cond_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3, v5}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 345
    :cond_2
    if-nez v1, :cond_3

    .line 346
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 348
    :cond_3
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrengthDbm()I

    move-result v0

    .line 349
    .local v0, "dbm":I
    const/16 v3, -0x4b

    if-lt v0, v3, :cond_4

    .line 350
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3, v7}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 351
    :cond_4
    const/16 v3, -0x55

    if-lt v0, v3, :cond_5

    .line 352
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3, v7}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 353
    :cond_5
    const/16 v3, -0x5f

    if-lt v0, v3, :cond_6

    .line 354
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 355
    :cond_6
    const/16 v3, -0x64

    if-lt v0, v3, :cond_7

    .line 356
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3, v6}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0

    .line 358
    :cond_7
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/StateFrag$PhoneStateMonitor;->this$0:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v3, v5}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setSignal(I)V

    goto :goto_0
.end method
