.class public Lcom/touchus/benchilauncher/activity/main/ButtomBar;
.super Lcom/touchus/benchilauncher/base/BaseHandlerFragment;
.source "ButtomBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field air_state:Landroid/widget/TextView;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private backIbtn:Landroid/widget/ImageButton;

.field private hideButtomBarRunnabler:Ljava/lang/Runnable;

.field private homeIbtn:Landroid/widget/ImageButton;

.field mAc:Landroid/widget/ImageView;

.field mAirfan:Landroid/widget/ImageView;

.field mAuto:Landroid/widget/ImageView;

.field mMainActivity:Lcom/touchus/benchilauncher/Launcher;

.field mMax:Landroid/widget/ImageView;

.field mTempL:Landroid/widget/TextView;

.field mTempR:Landroid/widget/TextView;

.field private screenControlIbtn:Landroid/widget/ImageButton;

.field private tempLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;-><init>()V

    .line 289
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/ButtomBar$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar$1;-><init>(Lcom/touchus/benchilauncher/activity/main/ButtomBar;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->hideButtomBarRunnabler:Ljava/lang/Runnable;

    .line 26
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/ButtomBar;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->tempLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private handleAirInfo(Lcom/backaudio/android/driver/beans/AirInfo;)V
    .locals 7
    .param p1, "airInfo"    # Lcom/backaudio/android/driver/beans/AirInfo;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 222
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_2

    .line 226
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->air_state:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 229
    :cond_2
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiAirOpen()Z

    move-result v0

    if-nez v0, :cond_3

    .line 230
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->air_state:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    goto :goto_0

    .line 234
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->air_state:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->getLeftTemp()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->setTempL(D)V

    .line 238
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiACOpen()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->setAc(Z)V

    .line 239
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->getLevel()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->setAirfan(D)V

    .line 240
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->getRightTemp()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->setTempR(D)V

    .line 241
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiFlatWind()Z

    move-result v0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiDownWind()Z

    move-result v1

    .line 242
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiMaxFrontWind()Z

    move-result v2

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiFrontWind()Z

    move-result v3

    .line 241
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->setAuto(ZZZZ)V

    .line 243
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiAuto1()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 244
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f02011b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 246
    :cond_4
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiAuto2()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 247
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 249
    :cond_5
    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->isiMaxFrontWind()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 250
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMax:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAc:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempL:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempR:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    :goto_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    goto/16 :goto_0

    .line 257
    :cond_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMax:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAc:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempL:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempR:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private handleBackAction()V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    invoke-static {}, Lcom/touchus/benchilauncher/LauncherApplication;->getInstance()Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->handleBackAction()V

    goto :goto_0
.end method

.method private handleHomeAction()V
    .locals 4

    .prologue
    .line 212
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    if-nez v0, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-static {}, Lcom/touchus/benchilauncher/LauncherApplication;->getInstance()Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    if-eqz v0, :cond_0

    .line 216
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Menu start time = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->handHomeAction()V

    goto :goto_0
.end method

.method private setAc(Z)V
    .locals 2
    .param p1, "ac"    # Z

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    .line 138
    if-eqz p1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAc:Landroid/widget/ImageView;

    const v1, 0x7f02008a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAc:Landroid/widget/ImageView;

    const v1, 0x7f020089

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setAirfan(D)V
    .locals 3
    .param p1, "airfan"    # D

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    .line 147
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f02011b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f02010c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 151
    :cond_2
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_3

    .line 152
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f02010d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 153
    :cond_3
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_4

    .line 154
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f02010e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 155
    :cond_4
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_5

    .line 156
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f02010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 157
    :cond_5
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_6

    .line 158
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020110

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 159
    :cond_6
    const-wide/high16 v0, 0x4008000000000000L    # 3.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_7

    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 161
    :cond_7
    const-wide/high16 v0, 0x400c000000000000L    # 3.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_8

    .line 162
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020112

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 163
    :cond_8
    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_9

    .line 164
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020113

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 165
    :cond_9
    const-wide/high16 v0, 0x4012000000000000L    # 4.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_a

    .line 166
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020114

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 167
    :cond_a
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_b

    .line 168
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020115

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 169
    :cond_b
    const-wide/high16 v0, 0x4016000000000000L    # 5.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_c

    .line 170
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020116

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 171
    :cond_c
    const-wide/high16 v0, 0x4018000000000000L    # 6.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_d

    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020117

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 173
    :cond_d
    const-wide/high16 v0, 0x401a000000000000L    # 6.5

    cmpl-double v0, p1, v0

    if-nez v0, :cond_e

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020118

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 175
    :cond_e
    const-wide/high16 v0, 0x401c000000000000L    # 7.0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    const v1, 0x7f020119

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method private setAuto(ZZZZ)V
    .locals 2
    .param p1, "iFlatWind"    # Z
    .param p2, "iDownWind"    # Z
    .param p3, "iMaxFrontWind"    # Z
    .param p4, "iFrontWind"    # Z

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    .line 114
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f020098

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-nez p4, :cond_2

    .line 117
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f02009a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 118
    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    if-nez p4, :cond_3

    .line 119
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f02009b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 120
    :cond_3
    if-nez p1, :cond_4

    if-eqz p2, :cond_4

    if-nez p4, :cond_4

    .line 121
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f020095

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 122
    :cond_4
    if-nez p1, :cond_5

    if-eqz p2, :cond_5

    if-eqz p4, :cond_5

    .line 123
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f020097

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 127
    :cond_5
    if-eqz p1, :cond_6

    if-nez p2, :cond_6

    if-eqz p4, :cond_6

    .line 128
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f020099

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 129
    :cond_6
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-eqz p4, :cond_0

    .line 130
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    const v1, 0x7f020096

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setTempL(D)V
    .locals 3
    .param p1, "tempL"    # D

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    .line 182
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempL:Landroid/widget/TextView;

    const-string v1, "LO"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    :goto_0
    return-void

    .line 184
    :cond_0
    const-wide/high16 v0, 0x403c000000000000L    # 28.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempL:Landroid/widget/TextView;

    const-string v1, "HI"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempL:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\u2103"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setTempR(D)V
    .locals 3
    .param p1, "tempR"    # D

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->stateChanged()V

    .line 193
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempR:Landroid/widget/TextView;

    const-string v1, "LO"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    :goto_0
    return-void

    .line 195
    :cond_0
    const-wide/high16 v0, 0x403c000000000000L    # 28.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempR:Landroid/widget/TextView;

    const-string v1, "HI"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempR:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\u2103"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public handlerMsg(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 269
    iget v4, p1, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 271
    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 273
    .local v1, "airInfoBundle":Landroid/os/Bundle;
    const-string v4, "airInfo"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/beans/AirInfo;

    .line 274
    .local v0, "airInfo":Lcom/backaudio/android/driver/beans/AirInfo;
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->handleAirInfo(Lcom/backaudio/android/driver/beans/AirInfo;)V

    goto :goto_0

    .line 277
    .end local v0    # "airInfo":Lcom/backaudio/android/driver/beans/AirInfo;
    .end local v1    # "airInfoBundle":Landroid/os/Bundle;
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 278
    .local v3, "idriverData":Landroid/os/Bundle;
    const-string v4, "idriver_enum"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v2

    .line 279
    .local v2, "btnCode":B
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v2, v4, :cond_1

    .line 280
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->handleBackAction()V

    goto :goto_0

    .line 281
    :cond_1
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v2, v4, :cond_2

    .line 282
    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-ne v2, v4, :cond_0

    .line 283
    :cond_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->handleHomeAction()V

    goto :goto_0

    .line 269
    :sswitch_data_0
    .sparse-switch
        0x3f4 -> :sswitch_0
        0x1771 -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 79
    :pswitch_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->handleHomeAction()V

    goto :goto_0

    .line 82
    :pswitch_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->handleBackAction()V

    goto :goto_0

    .line 85
    :pswitch_2
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->closeOrWakeupScreen(Z)V

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0106
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/Launcher;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    .line 44
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    const v2, 0x7f030030

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "mView":Landroid/view/View;
    const v1, 0x7f0b0106

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->homeIbtn:Landroid/widget/ImageButton;

    .line 46
    const v1, 0x7f0b0107

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->backIbtn:Landroid/widget/ImageButton;

    .line 48
    const v1, 0x7f0b0108

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 47
    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->screenControlIbtn:Landroid/widget/ImageButton;

    .line 49
    const v1, 0x7f0b010c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAuto:Landroid/widget/ImageView;

    .line 50
    const v1, 0x7f0b010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAc:Landroid/widget/ImageView;

    .line 51
    const v1, 0x7f0b010e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mAirfan:Landroid/widget/ImageView;

    .line 52
    const v1, 0x7f0b010f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMax:Landroid/widget/ImageView;

    .line 54
    const v1, 0x7f0b0109

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->air_state:Landroid/widget/TextView;

    .line 55
    const v1, 0x7f0b010b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempL:Landroid/widget/TextView;

    .line 56
    const v1, 0x7f0b0110

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mTempR:Landroid/widget/TextView;

    .line 58
    const v1, 0x7f0b010a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->tempLayout:Landroid/widget/LinearLayout;

    .line 60
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 62
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->homeIbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->backIbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->screenControlIbtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-object v0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->airInfo:Lcom/backaudio/android/driver/beans/AirInfo;

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->handleAirInfo(Lcom/backaudio/android/driver/beans/AirInfo;)V

    .line 72
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->onStart()V

    .line 73
    return-void
.end method

.method public settingTempLayoutHideOrShow(Z)V
    .locals 6
    .param p1, "iShow"    # Z

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 96
    :cond_0
    if-nez p1, :cond_2

    .line 97
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 98
    .local v0, "value":I
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->tempLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 97
    .end local v0    # "value":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_1

    .line 100
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    new-instance v2, Lcom/touchus/benchilauncher/activity/main/ButtomBar$2;

    invoke-direct {v2, p0, p1}, Lcom/touchus/benchilauncher/activity/main/ButtomBar$2;-><init>(Lcom/touchus/benchilauncher/activity/main/ButtomBar;Z)V

    .line 107
    const-wide/16 v4, 0x1f4

    .line 100
    invoke-virtual {v1, v2, v4, v5}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public stateChanged()V
    .locals 4

    .prologue
    .line 298
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    if-nez v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    iget-object v0, v0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMainActivity:Lcom/touchus/benchilauncher/Launcher;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->hideButtomBarRunnabler:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 305
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;->hideButtomBarRunnabler:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
