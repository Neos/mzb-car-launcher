.class public Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "CallPhoneFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus:[I = null

.field public static final DOWN_AREA:I = 0x2

.field public static final UP_AREA:I = 0x1

.field public static mSelectArea:I

.field public static mSelectedIndexInDown:I


# instance fields
.field private TAG:Ljava/lang/String;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private bt_callphone_xia_bluetooth:Landroid/widget/Button;

.field private bt_callphone_xia_connect:Landroid/widget/Button;

.field private bt_callphone_xia_record:Landroid/widget/Button;

.field private callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

.field private fragmentView:Landroid/view/View;

.field isAlreadyOut:Z

.field private isLogToggle:Z

.field private isMargin:Z

.field isSendNum:Z

.field private iv_number0:Landroid/widget/ImageButton;

.field private iv_number1:Landroid/widget/ImageButton;

.field private iv_number2:Landroid/widget/ImageButton;

.field private iv_number3:Landroid/widget/ImageButton;

.field private iv_number4:Landroid/widget/ImageButton;

.field private iv_number5:Landroid/widget/ImageButton;

.field private iv_number6:Landroid/widget/ImageButton;

.field private iv_number7:Landroid/widget/ImageButton;

.field private iv_number8:Landroid/widget/ImageButton;

.field private iv_number9:Landroid/widget/ImageButton;

.field private iv_number_clear:Landroid/widget/ImageButton;

.field private iv_number_end:Landroid/widget/ImageButton;

.field private iv_number_jia:Landroid/widget/ImageButton;

.field private iv_number_jin:Landroid/widget/ImageButton;

.field private iv_number_send:Landroid/widget/ImageButton;

.field private iv_number_xing:Landroid/widget/ImageButton;

.field private ll_callphone_number:Landroid/widget/LinearLayout;

.field private mContext:Lcom/touchus/benchilauncher/Launcher;

.field private mDownBtns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;

.field private mSelectedIndexInUp:I

.field private mUpBtns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field

.field private myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

.field private soundswitch:Landroid/widget/ImageView;

.field private tv_call_connect_state:Landroid/widget/TextView;

.field private tv_callphone_bluetooth_state:Landroid/widget/TextView;

.field private tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

.field private tv_callphone_number_details:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus()[I
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->values()[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INITIALIZING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->MULTI_TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x1

    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    const/4 v0, 0x0

    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->TAG:Ljava/lang/String;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isLogToggle:Z

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    .line 114
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isMargin:Z

    .line 122
    iput v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    .line 127
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;

    .line 854
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isSendNum:Z

    .line 1048
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isAlreadyOut:Z

    .line 48
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;)V
    .locals 0

    .prologue
    .line 1235
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bluetoothState()V

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_connect:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_bluetooth:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;Lcom/touchus/benchilauncher/views/MyCustomDialog;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    return-void
.end method

.method private afterSendBeforeConnectBtnState()V
    .locals 4

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1069
    const v3, 0x7f07002c

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1068
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1071
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1072
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1073
    const v2, 0x7f07003b

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1072
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1074
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1075
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllUpBtnUnclickable()V

    .line 1076
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnClickable(I)V

    .line 1077
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllDownBtnUnclickable()V

    .line 1078
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->autoChangeUpSelectBtn()V

    .line 1079
    return-void
.end method

.method private answerBeforeConnectBtnState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1087
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1088
    const v3, 0x7f07002c

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1087
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1089
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1090
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1091
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1092
    const v2, 0x7f07003a

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1091
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1093
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1094
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllUpBtnUnclickable()V

    .line 1095
    invoke-direct {p0, v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnClickable(I)V

    .line 1096
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnClickable(I)V

    .line 1097
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllDownBtnUnclickable()V

    .line 1098
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->autoChangeUpSelectBtn()V

    .line 1099
    return-void
.end method

.method private autoChangeUpSelectBtn()V
    .locals 2

    .prologue
    .line 992
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downHaveBtnCanSelect(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 993
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downSelectBtn(I)V

    .line 997
    :cond_0
    :goto_0
    return-void

    .line 994
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upHaveBtnCanSelect(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    goto :goto_0
.end method

.method private bluetoothState()V
    .locals 0

    .prologue
    .line 1236
    return-void
.end method

.method private btConnetAndHavePhoneNumBtnState()V
    .locals 4

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1055
    const v3, 0x7f07002c

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1054
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1056
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1057
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1058
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1059
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllUpBtnClickable()V

    .line 1060
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllDownBtnUnclickable()V

    .line 1061
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->autoChangeUpSelectBtn()V

    .line 1062
    return-void
.end method

.method private btConnetAndNoPhoneNumBtnState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1021
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1022
    const v3, 0x7f07002c

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1021
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1023
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1024
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1026
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_connect:Landroid/widget/Button;

    const v1, 0x7f07002a

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1027
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1028
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllUpBtnClickable()V

    .line 1029
    invoke-direct {p0, v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnUnclickable(I)V

    .line 1030
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnUnclickable(I)V

    .line 1031
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnUnclickable(I)V

    .line 1032
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllDownBtnClickable()V

    .line 1033
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->autoChangeUpSelectBtn()V

    .line 1034
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    if-eqz v0, :cond_1

    .line 1035
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 1036
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->afterSendBeforeConnectBtnState()V

    .line 1037
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1038
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isAlreadyOut:Z

    if-eqz v0, :cond_0

    .line 1039
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->sendPhoneNumFromRecord()V

    .line 1040
    iput-boolean v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isAlreadyOut:Z

    .line 1046
    :cond_0
    :goto_0
    return-void

    .line 1043
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    goto :goto_0
.end method

.method private btUnconnectBtnState()V
    .locals 3

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1004
    const v2, 0x7f07002b

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1003
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1005
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    const v1, 0x7f020086

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1007
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1008
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1009
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_connect:Landroid/widget/Button;

    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 1010
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1011
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 1012
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllUpBtnUnclickable()V

    .line 1013
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllDownBtnClickable()V

    .line 1014
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->autoChangeUpSelectBtn()V

    .line 1015
    return-void
.end method

.method private clearNumber()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1139
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1141
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private clickDownBtn(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 255
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 260
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-nez v0, :cond_0

    .line 261
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_2

    .line 262
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->disconnectCurDevice()V

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    const v1, 0x7f07002e

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    .line 267
    const/4 v5, 0x1

    move-object v0, p0

    move-object v4, v3

    .line 264
    invoke-direct/range {v0 .. v5}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 272
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 281
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private dismissNumberKeyDialog()V
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->dismiss()V

    .line 1208
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    .line 1209
    return-void
.end method

.method private downAllDisselect()V
    .locals 3

    .prologue
    .line 592
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 595
    return-void

    .line 593
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setSelected(Z)V

    .line 592
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private downListenSelect(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 1244
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downSelectBtn(I)V

    .line 1245
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->clickDownBtn(I)V

    .line 1246
    return-void
.end method

.method private downSelectBtn(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 579
    const/4 v0, 0x2

    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    .line 580
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upAllDisselect()V

    .line 581
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downAllDisselect()V

    .line 582
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 583
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isMargin:Z

    .line 584
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setMaringL()V

    .line 585
    sput p1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    .line 586
    return-void
.end method

.method private getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    goto :goto_0
.end method

.method private handlerCallConnState()V
    .locals 2

    .prologue
    .line 644
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_1

    .line 645
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndNoPhoneNumBtnState()V

    .line 646
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 653
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 654
    return-void

    .line 651
    :cond_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btUnconnectBtnState()V

    goto :goto_0
.end method

.method private handlerHangup()V
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 627
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 628
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_0

    .line 629
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndNoPhoneNumBtnState()V

    .line 633
    :goto_0
    return-void

    .line 631
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btUnconnectBtnState()V

    goto :goto_0
.end method

.method private handlerIdriver(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x0

    .line 211
    const-string v1, "idriver_enum"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 212
    .local v0, "mIDRIVERENUM":B
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_1

    .line 213
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateTurnRight()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_2

    .line 215
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateTurnLeft()V

    goto :goto_0

    .line 216
    :cond_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_3

    .line 217
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateRight()V

    goto :goto_0

    .line 218
    :cond_3
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_4

    .line 219
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateLeft()V

    goto :goto_0

    .line 220
    :cond_4
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_5

    .line 221
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operatePress()V

    goto :goto_0

    .line 222
    :cond_5
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_6

    .line 223
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateUp()V

    goto :goto_0

    .line 224
    :cond_6
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_7

    .line 225
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateDown()V

    goto :goto_0

    .line 226
    :cond_7
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_9

    .line 227
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_8

    .line 228
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 229
    :cond_8
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_0

    .line 230
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 232
    :cond_9
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HANG_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_0

    .line 233
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v1, v2, :cond_a

    .line 234
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v1, v2, :cond_a

    .line 235
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_b

    .line 236
    :cond_a
    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto/16 :goto_0

    .line 237
    :cond_b
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_0

    .line 238
    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->clickUpBtn(I)V

    goto/16 :goto_0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 168
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 169
    .local v0, "bundle":Landroid/os/Bundle;
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 203
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 171
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerIdriver(Landroid/os/Bundle;)V

    goto :goto_0

    .line 175
    :sswitch_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->phoneConnectingBtnState()V

    goto :goto_0

    .line 180
    :sswitch_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerCallConnState()V

    goto :goto_0

    .line 183
    :sswitch_4
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_1

    .line 184
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerPhoneNowCallout()V

    goto :goto_0

    .line 185
    :cond_1
    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v1, v2, :cond_0

    .line 186
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerPhoneNowCallin()V

    goto :goto_0

    .line 190
    :sswitch_5
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    if-eqz v1, :cond_2

    .line 191
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    .line 193
    :cond_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerHangup()V

    goto :goto_0

    .line 196
    :sswitch_6
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    if-eqz v1, :cond_3

    .line 197
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const v2, 0x7f02006b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 199
    :cond_3
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const v2, 0x7f020055

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 169
    nop

    :sswitch_data_0
    .sparse-switch
        0x582 -> :sswitch_6
        0x583 -> :sswitch_2
        0x584 -> :sswitch_0
        0x587 -> :sswitch_4
        0x588 -> :sswitch_5
        0x1771 -> :sswitch_1
        0x1772 -> :sswitch_3
    .end sparse-switch
.end method

.method private handlerPhoneNowCallin()V
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 615
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->answerBeforeConnectBtnState()V

    .line 617
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    .line 618
    return-void
.end method

.method private handlerPhoneNowCallout()V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 604
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->afterSendBeforeConnectBtnState()V

    .line 606
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    .line 607
    return-void
.end method

.method private initEvent()V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_send:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 873
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number0:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 874
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number1:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 875
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number2:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 876
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number3:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 877
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number4:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 878
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number5:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 879
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number6:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 880
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number7:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 881
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number8:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 882
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number9:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 883
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_xing:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 884
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_jin:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 885
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_jia:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 886
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_clear:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 887
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_end:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 889
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 891
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_connect:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 892
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_record:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 893
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_bluetooth:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 895
    return-void
.end method

.method private initStae()V
    .locals 3

    .prologue
    .line 794
    invoke-static {}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus()[I

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 852
    :goto_0
    :pswitch_0
    return-void

    .line 802
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isComeFromRecord:Z

    if-eqz v0, :cond_1

    .line 811
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isAlreadyOut:Z

    .line 812
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->getbookName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    .line 813
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->afterSendBeforeConnectBtnState()V

    .line 814
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isComeFromRecord:Z

    .line 815
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 816
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->sendPhoneNumFromRecord()V

    .line 823
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0

    .line 819
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 820
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 821
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndNoPhoneNumBtnState()V

    goto :goto_1

    .line 835
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 836
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->afterSendBeforeConnectBtnState()V

    goto :goto_0

    .line 839
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 840
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->phoneConnectingBtnState()V

    goto :goto_0

    .line 843
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 844
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->handlerPhoneNowCallin()V

    goto :goto_0

    .line 847
    :pswitch_5
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btUnconnectBtnState()V

    goto :goto_0

    .line 794
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 734
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 735
    const v1, 0x7f0b0095

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 734
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number0:Landroid/widget/ImageButton;

    .line 736
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 737
    const v1, 0x7f0b0096

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 736
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number1:Landroid/widget/ImageButton;

    .line 738
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 739
    const v1, 0x7f0b0097

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 738
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number2:Landroid/widget/ImageButton;

    .line 740
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 741
    const v1, 0x7f0b0098

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 740
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number3:Landroid/widget/ImageButton;

    .line 742
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 743
    const v1, 0x7f0b0099

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 742
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number4:Landroid/widget/ImageButton;

    .line 744
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 745
    const v1, 0x7f0b009a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 744
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number5:Landroid/widget/ImageButton;

    .line 746
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 747
    const v1, 0x7f0b009b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 746
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number6:Landroid/widget/ImageButton;

    .line 748
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 749
    const v1, 0x7f0b009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 748
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number7:Landroid/widget/ImageButton;

    .line 750
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 751
    const v1, 0x7f0b009d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 750
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number8:Landroid/widget/ImageButton;

    .line 752
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 753
    const v1, 0x7f0b009e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 752
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number9:Landroid/widget/ImageButton;

    .line 754
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 755
    const v1, 0x7f0b0093

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 754
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_send:Landroid/widget/ImageButton;

    .line 756
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 757
    const v1, 0x7f0b009f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 756
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_xing:Landroid/widget/ImageButton;

    .line 758
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 759
    const v1, 0x7f0b00a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 758
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_jin:Landroid/widget/ImageButton;

    .line 760
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 761
    const v1, 0x7f0b00a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 760
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_jia:Landroid/widget/ImageButton;

    .line 762
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 763
    const v1, 0x7f0b00a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 762
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_clear:Landroid/widget/ImageButton;

    .line 764
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 765
    const v1, 0x7f0b00a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 764
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_end:Landroid/widget/ImageButton;

    .line 767
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 768
    const v1, 0x7f0b0094

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 767
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->ll_callphone_number:Landroid/widget/LinearLayout;

    .line 770
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 771
    const v1, 0x7f0b00a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 770
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_connect:Landroid/widget/Button;

    .line 772
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 773
    const v1, 0x7f0b00a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 772
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_record:Landroid/widget/Button;

    .line 774
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 775
    const v1, 0x7f0b00a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 774
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_bluetooth:Landroid/widget/Button;

    .line 777
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 778
    const v1, 0x7f0b0091

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 777
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    .line 780
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 781
    const v1, 0x7f0b008f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 780
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    .line 782
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 783
    const v1, 0x7f0b008e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 782
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    .line 784
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 785
    const v1, 0x7f0b0090

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 784
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    .line 787
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 788
    const v1, 0x7f0b0092

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 787
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    .line 790
    return-void
.end method

.method private leftInDown()V
    .locals 2

    .prologue
    .line 493
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    .line 494
    .local v0, "tmpIndex":I
    :cond_0
    if-gtz v0, :cond_1

    .line 502
    :goto_0
    return-void

    .line 495
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 496
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    .line 498
    sget v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downSelectBtn(I)V

    goto :goto_0
.end method

.method private leftInUp()V
    .locals 2

    .prologue
    .line 508
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    .line 509
    .local v0, "tmpIndex":I
    :cond_0
    if-gtz v0, :cond_1

    .line 517
    :goto_0
    return-void

    .line 510
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 511
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    iput v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    .line 513
    iget v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    goto :goto_0
.end method

.method private phoneConnectingBtnState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1104
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_state:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1105
    const v3, 0x7f07002c

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/service/BluetoothService;->currentEquipName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1104
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1106
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_bluetooth_stateimg:Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1107
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1108
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_call_connect_state:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1109
    const v2, 0x7f07003c

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1108
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1110
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1111
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllUpBtnClickable()V

    .line 1112
    invoke-direct {p0, v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnUnclickable(I)V

    .line 1113
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnUnclickable(I)V

    .line 1114
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setAllDownBtnUnclickable()V

    .line 1115
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->autoChangeUpSelectBtn()V

    .line 1116
    return-void
.end method

.method private rightInDown()V
    .locals 2

    .prologue
    .line 523
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    .line 524
    .local v0, "tmpIndex":I
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 532
    :goto_0
    return-void

    .line 525
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 526
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    .line 528
    sget v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downSelectBtn(I)V

    goto :goto_0
.end method

.method private rightInUp()V
    .locals 2

    .prologue
    .line 538
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    .line 539
    .local v0, "tmpIndex":I
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 547
    :goto_0
    return-void

    .line 540
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 541
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542
    iput v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    .line 543
    iget v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    goto :goto_0
.end method

.method private sendPhoneNumFromRecord()V
    .locals 2

    .prologue
    .line 859
    const-string v0, "sendPhone"

    const-string v1, ":--\tsendPhoneNumFromRecord"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isSendNum:Z

    if-eqz v0, :cond_0

    .line 868
    :goto_0
    return-void

    .line 863
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isSendNum:Z

    .line 866
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 867
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->CallOut(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setAllDownBtnClickable()V
    .locals 2

    .prologue
    .line 975
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 978
    return-void

    .line 976
    :cond_0
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setDownBtnClickable(I)V

    .line 975
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setAllDownBtnUnclickable()V
    .locals 2

    .prologue
    .line 986
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 989
    return-void

    .line 987
    :cond_0
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setDownBtnUnclickable(I)V

    .line 986
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setAllUpBtnClickable()V
    .locals 2

    .prologue
    .line 931
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 934
    return-void

    .line 932
    :cond_0
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnClickable(I)V

    .line 931
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setAllUpBtnUnclickable()V
    .locals 2

    .prologue
    .line 942
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 945
    return-void

    .line 943
    :cond_0
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setUpBtnUnclickable(I)V

    .line 942
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setDialogLocation(Landroid/app/Dialog;)V
    .locals 3
    .param p1, "myCustomDialog"    # Landroid/app/Dialog;

    .prologue
    .line 1217
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1218
    .local v0, "dialogWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1219
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x100

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1220
    const/16 v2, -0x1e

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1221
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1222
    return-void
.end method

.method private setDownBtnClickable(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 964
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 965
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    .line 966
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 967
    return-void
.end method

.method private setDownBtnUnclickable(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 953
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 954
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    .line 955
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 956
    return-void
.end method

.method private setMaringL()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1122
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->ll_callphone_number:Landroid/widget/LinearLayout;

    .line 1123
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1122
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1124
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isMargin:Z

    if-eqz v1, :cond_0

    .line 1125
    const/16 v1, 0x7b

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1129
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->ll_callphone_number:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1130
    return-void

    .line 1127
    :cond_0
    const/16 v1, 0xaa

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method

.method private setUpBtnClickable(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 920
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 921
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 922
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 923
    return-void
.end method

.method private setUpBtnUnclickable(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 908
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 909
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 910
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 911
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 912
    return-void
.end method

.method private showBluetoothDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "Messsage"    # Ljava/lang/String;
    .param p2, "nextMessage"    # Ljava/lang/String;
    .param p3, "btnPosiText"    # Ljava/lang/String;
    .param p4, "btnNegaText"    # Ljava/lang/String;
    .param p5, "flag"    # Z

    .prologue
    .line 1150
    new-instance v0, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 1151
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 1150
    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1152
    .local v0, "builder":Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 1153
    invoke-virtual {v0, p2}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setNextMessage(Ljava/lang/String;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 1155
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$1;

    invoke-direct {v1, p0, p5}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$1;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;Z)V

    .line 1154
    invoke-virtual {v0, p3, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 1168
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$2;

    invoke-direct {v1, p0, p5}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$2;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;Z)V

    .line 1167
    invoke-virtual {v0, p4, v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;

    .line 1178
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;->create()Lcom/touchus/benchilauncher/views/MyCustomDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    .line 1179
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-nez v1, :cond_0

    if-eqz p5, :cond_0

    .line 1180
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->enterPairingMode()V

    .line 1182
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    .line 1183
    new-instance v2, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$3;

    invoke-direct {v2, p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$3;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;Lcom/touchus/benchilauncher/views/MyCustomDialog$Builder;)V

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1191
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setDialogLocation(Landroid/app/Dialog;)V

    .line 1192
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->show()V

    .line 1193
    return-void
.end method

.method private showNumberKeyDialog()V
    .locals 3

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    if-nez v0, :cond_0

    .line 1198
    new-instance v0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f080014

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    iput-object p0, v0, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->parent:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    .line 1201
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->callPhoneKeyDialog:Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/CallPhoneKeyDialog;->show()V

    .line 1202
    return-void
.end method

.method private swithSound()V
    .locals 2

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->switchDevice()V

    .line 1319
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->iSoundInPhone:Z

    if-eqz v0, :cond_0

    .line 1320
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const v1, 0x7f02006b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1324
    :goto_0
    return-void

    .line 1322
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->soundswitch:Landroid/widget/ImageView;

    const v1, 0x7f020055

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private upAllDisselect()V
    .locals 3

    .prologue
    .line 570
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 573
    return-void

    .line 571
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 570
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private upClick2AddChar(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 288
    sget-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_0

    .line 289
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->phoneConnectingBtnState()V

    .line 293
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    return-void

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndHavePhoneNumBtnState()V

    goto :goto_0
.end method

.method private upSelectBtn(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x1

    .line 553
    sput v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    .line 554
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downAllDisselect()V

    .line 555
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upAllDisselect()V

    .line 556
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 557
    if-ge p1, v1, :cond_0

    .line 558
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isMargin:Z

    .line 562
    :goto_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->setMaringL()V

    .line 563
    iput p1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    .line 564
    return-void

    .line 560
    :cond_0
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->isMargin:Z

    goto :goto_0
.end method


# virtual methods
.method public clickUpBtn(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 301
    const-string v0, "bluetooth"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    :goto_0
    return-void

    .line 305
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 307
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 308
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    const v1, 0x7f070031

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/utils/ToastTool;->showBigShortToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 311
    :cond_1
    sget-object v0, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-ne v0, v1, :cond_2

    .line 312
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->answerCalling()V

    goto :goto_0

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->CallOut(Ljava/lang/String;)V

    .line 316
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->afterSendBeforeConnectBtnState()V

    goto :goto_0

    .line 320
    :pswitch_1
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ZERO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto :goto_0

    .line 324
    :pswitch_2
    const-string v0, "1"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ONE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto :goto_0

    .line 328
    :pswitch_3
    const-string v0, "2"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->TWO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto :goto_0

    .line 332
    :pswitch_4
    const-string v0, "3"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->THREE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto :goto_0

    .line 336
    :pswitch_5
    const-string v0, "4"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FOUR:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto :goto_0

    .line 340
    :pswitch_6
    const-string v0, "5"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 341
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FIVE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto/16 :goto_0

    .line 344
    :pswitch_7
    const-string v0, "6"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SIX:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    .line 346
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndHavePhoneNumBtnState()V

    goto/16 :goto_0

    .line 349
    :pswitch_8
    const-string v0, "7"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SEVEN:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto/16 :goto_0

    .line 353
    :pswitch_9
    const-string v0, "8"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->EIGHT:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto/16 :goto_0

    .line 357
    :pswitch_a
    const-string v0, "9"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 358
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->NINE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto/16 :goto_0

    .line 361
    :pswitch_b
    const-string v0, "*"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 362
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ASTERISK:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto/16 :goto_0

    .line 365
    :pswitch_c
    const-string v0, "#"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->WELL:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V

    goto/16 :goto_0

    .line 369
    :pswitch_d
    const-string v0, "+"

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upClick2AddChar(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 372
    :pswitch_e
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->clearNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 373
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 374
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndNoPhoneNumBtnState()V

    .line 378
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 376
    :cond_3
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndHavePhoneNumBtnState()V

    goto :goto_1

    .line 381
    :pswitch_f
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v1, ""

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 382
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->tv_callphone_number_details:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->btConnetAndNoPhoneNumBtnState()V

    .line 385
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    if-eqz v0, :cond_4

    .line 386
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    .line 388
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->cutdownCurrentCalling()V

    goto/16 :goto_0

    .line 305
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public connectBluetooth()V
    .locals 1

    .prologue
    .line 1229
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->enterPairingMode()V

    .line 1230
    return-void
.end method

.method public downHaveBtnCanSelect(I)Z
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x1

    .line 438
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 444
    add-int/lit8 v0, p1, -0x1

    :goto_1
    const/4 v1, -0x1

    if-gt v0, v1, :cond_2

    .line 450
    const/4 v1, 0x0

    :goto_2
    return v1

    .line 439
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 440
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    move v1, v2

    .line 441
    goto :goto_2

    .line 438
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 445
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 446
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    move v1, v2

    .line 447
    goto :goto_2

    .line 444
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public initDataFromLauncher()V
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 160
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 676
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 678
    return-void
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 133
    const/4 v0, 0x1

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1251
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1315
    :goto_0
    :pswitch_0
    return-void

    .line 1253
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1256
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1259
    :pswitch_3
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1262
    :pswitch_4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1265
    :pswitch_5
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1268
    :pswitch_6
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1271
    :pswitch_7
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1274
    :pswitch_8
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1277
    :pswitch_9
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1280
    :pswitch_a
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1283
    :pswitch_b
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1286
    :pswitch_c
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1289
    :pswitch_d
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1292
    :pswitch_e
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1295
    :pswitch_f
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1298
    :pswitch_10
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upListenSelectBtn(I)V

    goto :goto_0

    .line 1301
    :pswitch_11
    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downListenSelect(I)V

    goto :goto_0

    .line 1304
    :pswitch_12
    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downListenSelect(I)V

    goto :goto_0

    .line 1307
    :pswitch_13
    invoke-direct {p0, v3}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downListenSelect(I)V

    goto :goto_0

    .line 1310
    :pswitch_14
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->swithSound()V

    goto :goto_0

    .line 1251
    :pswitch_data_0
    .packed-switch 0x7f0b0090
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 658
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 659
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mContext:Lcom/touchus/benchilauncher/Launcher;

    .line 660
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 668
    const v0, 0x7f03001c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    .line 669
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->initView()V

    .line 671
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->fragmentView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 899
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 900
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 717
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onPause()V

    .line 719
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 720
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->istop:Z

    .line 721
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    if-eqz v0, :cond_1

    .line 722
    sget-boolean v0, Lcom/touchus/benchilauncher/service/BluetoothService;->talkingflag:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->showDialogflag:Z

    if-eqz v0, :cond_1

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->topIsBlueMainFragment()Z

    move-result v0

    if-nez v0, :cond_1

    .line 724
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->enterSystemFloatCallView()V

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 727
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->myCustomDialog:Lcom/touchus/benchilauncher/views/MyCustomDialog;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/MyCustomDialog;->dismiss()V

    .line 729
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    .line 731
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 682
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onResume()V

    .line 683
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->istop:Z

    .line 684
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->isDestory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 685
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getShowStatus()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    move-result-object v0

    sget-object v1, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;->HIDEN:Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$FloatShowST;

    if-eq v0, v1, :cond_0

    .line 686
    invoke-static {}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->getInstance()Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog;->hiden()V

    .line 688
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->initEvent()V

    .line 689
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_send:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 690
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number0:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 691
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number1:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 692
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number2:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 693
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number3:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number4:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 695
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number5:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 696
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number6:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 697
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number7:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 698
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number8:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 699
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number9:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_xing:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 701
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_jin:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 702
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_jia:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 703
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_clear:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 704
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->iv_number_end:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 706
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_connect:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 707
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_record:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 708
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mDownBtns:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->bt_callphone_xia_bluetooth:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 710
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->initStae()V

    .line 711
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 713
    return-void
.end method

.method public operateDown()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 412
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    if-ne v0, v1, :cond_1

    .line 419
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downHaveBtnCanSelect(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    sput v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    .line 417
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->downSelectBtn(I)V

    goto :goto_0
.end method

.method public operateLeft()V
    .locals 2

    .prologue
    .line 457
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 458
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->leftInUp()V

    .line 462
    :goto_0
    return-void

    .line 460
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->leftInDown()V

    goto :goto_0
.end method

.method public operatePress()V
    .locals 2

    .prologue
    .line 247
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 248
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->clickUpBtn(I)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 250
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInDown:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->clickDownBtn(I)V

    goto :goto_0
.end method

.method public operateRight()V
    .locals 2

    .prologue
    .line 468
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->rightInUp()V

    .line 473
    :goto_0
    return-void

    .line 471
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->rightInDown()V

    goto :goto_0
.end method

.method public operateTurnLeft()V
    .locals 0

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateLeft()V

    .line 480
    return-void
.end method

.method public operateTurnRight()V
    .locals 0

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->operateRight()V

    .line 487
    return-void
.end method

.method public operateUp()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 399
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    if-ne v0, v1, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upHaveBtnCanSelect(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    sput v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectArea:I

    .line 404
    iget v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    goto :goto_0
.end method

.method public upHaveBtnCanSelect(I)Z
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x1

    .line 422
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 428
    add-int/lit8 v0, p1, -0x1

    :goto_1
    const/4 v1, -0x1

    if-gt v0, v1, :cond_2

    .line 434
    const/4 v1, 0x0

    :goto_2
    return v1

    .line 423
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 424
    iput v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    move v1, v2

    .line 425
    goto :goto_2

    .line 422
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 429
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mUpBtns:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 430
    iput v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->mSelectedIndexInUp:I

    move v1, v2

    .line 431
    goto :goto_2

    .line 428
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public upListenSelectBtn(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 1239
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->upSelectBtn(I)V

    .line 1240
    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->clickUpBtn(I)V

    .line 1241
    return-void
.end method
