.class public Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "CallRecordFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

.field private callRecordView:Landroid/view/View;

.field private callRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private currentPosition:I

.field private isLogToggle:Z

.field private isLongPress:Z

.field private launcher:Lcom/touchus/benchilauncher/Launcher;

.field private listView_record:Landroid/widget/ListView;

.field private mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->TAG:Ljava/lang/String;

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLogToggle:Z

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecords:Ljava/util/List;

    .line 58
    iput v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    .line 60
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    .line 62
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;

    .line 31
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;)Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecords:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;)Lcom/touchus/benchilauncher/Launcher;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    return-object v0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 82
    const/16 v0, 0x1771

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_0

    .line 83
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->handlerMsgIdr(Landroid/os/Message;)V

    .line 85
    :cond_0
    return-void
.end method

.method private handlerMsgIdr(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 89
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 90
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_LONG_PRESS:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 91
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v2

    .line 90
    if-ne v1, v2, :cond_0

    .line 92
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    .line 104
    :goto_0
    return-void

    .line 94
    :cond_0
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 95
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_1

    .line 96
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    .line 97
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->pressUpDown(Landroid/os/Bundle;)V

    goto :goto_0

    .line 98
    :cond_1
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_UP:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 99
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 100
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    goto :goto_0

    .line 102
    :cond_2
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    goto :goto_0
.end method

.method private initData()V
    .locals 3

    .prologue
    .line 221
    new-instance v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    .line 222
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecords:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->getHistoryList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 223
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecords:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setData(Ljava/util/List;)V

    .line 224
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 225
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordView:Landroid/view/View;

    const v2, 0x7f0b008c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 226
    return-void
.end method

.method private initEvent()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    .line 231
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$1;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 247
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordView:Landroid/view/View;

    .line 216
    const v1, 0x7f0b008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 215
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    .line 217
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 187
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->context:Landroid/content/Context;

    .line 188
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    .line 189
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 190
    return-void
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 181
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 182
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordView:Landroid/view/View;

    .line 196
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->initView()V

    .line 197
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->initData()V

    .line 198
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->initEvent()V

    .line 199
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 256
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 258
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 205
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 206
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment$RecordHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 211
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 212
    return-void
.end method

.method public pressUpDown(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 108
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 112
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 111
    if-eq v3, v4, :cond_2

    .line 113
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 114
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 113
    if-ne v3, v4, :cond_5

    .line 115
    :cond_2
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    .line 116
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 117
    iput v5, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    .line 118
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    .line 120
    :cond_3
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 121
    .local v0, "firstVisiblePosition":I
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    if-ge v3, v0, :cond_4

    .line 122
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    add-int/lit8 v4, v4, -0x5

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 175
    .end local v0    # "firstVisiblePosition":I
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setSelectItem(I)V

    .line 176
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 124
    :cond_5
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 125
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v3, v4, :cond_6

    .line 126
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 127
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 126
    if-ne v3, v4, :cond_8

    .line 128
    :cond_6
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    .line 129
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    iget-object v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->getCount()I

    move-result v4

    if-ne v3, v4, :cond_7

    .line 130
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    .line 131
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->isLongPress:Z

    .line 133
    :cond_7
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 134
    .local v1, "lastVisiblePosition":I
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    if-le v3, v1, :cond_4

    .line 135
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->listView_record:Landroid/widget/ListView;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    goto :goto_1

    .line 161
    .end local v1    # "lastVisiblePosition":I
    :cond_8
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 162
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 161
    if-ne v3, v4, :cond_0

    .line 164
    sget-boolean v3, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v3, :cond_4

    .line 165
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->callRecords:Ljava/util/List;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->currentPosition:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v2

    .line 167
    .local v2, "s":Ljava/lang/String;
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v6, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isComeFromRecord:Z

    .line 168
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v6, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    .line 169
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, v3, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    .line 170
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallRecordFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    new-instance v4, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-direct {v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1
.end method
