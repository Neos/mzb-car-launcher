.class Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;
.super Ljava/lang/Object;
.source "CallAddressBookFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->initEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x1

    .line 278
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$1(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setSelectItem(I)V

    .line 279
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0, p3}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$2(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;I)V

    .line 280
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$1(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->notifyDataSetChanged()V

    .line 281
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$3(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isComeFromRecord:Z

    .line 283
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$3(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    .line 284
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$3(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$4(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/bean/Person;

    .line 285
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v0

    .line 284
    iput-object v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    .line 286
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;->this$0:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$5(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/Launcher;

    move-result-object v0

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 288
    :cond_0
    return-void
.end method
