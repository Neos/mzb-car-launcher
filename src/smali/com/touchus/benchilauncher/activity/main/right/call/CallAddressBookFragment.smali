.class public Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;
.super Lcom/touchus/benchilauncher/base/BaseFragment;
.source "CallAddressBookFragment.java"

# interfaces
.implements Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

.field private callRecordView:Landroid/view/View;

.field private callRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private currentPosition:I

.field private isLogToggle:Z

.field private isLongPress:Z

.field private launcher:Lcom/touchus/benchilauncher/Launcher;

.field private layout:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private listView_record:Landroid/widget/ListView;

.field private mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;

.field private noDataTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;-><init>()V

    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->TAG:Ljava/lang/String;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLogToggle:Z

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    .line 65
    iput v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    .line 67
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    .line 69
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;

    .line 35
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;I)V
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)Lcom/touchus/benchilauncher/Launcher;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    return-object v0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 89
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 114
    :goto_0
    return-void

    .line 91
    :sswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService;->createLoadingFloatView(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 95
    :sswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    goto :goto_0

    .line 98
    :sswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    .line 99
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->getBookList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 100
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->noDataTv:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setData(Ljava/util/List;)V

    .line 106
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->notifyDataSetChanged()V

    .line 107
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->notifyDataSetInvalidated()V

    .line 108
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->layout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->noDataTv:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 111
    :sswitch_3
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->handlerMsgIdr(Landroid/os/Message;)V

    goto :goto_0

    .line 89
    nop

    :sswitch_data_0
    .sparse-switch
        0x57f -> :sswitch_0
        0x580 -> :sswitch_1
        0x581 -> :sswitch_2
        0x1771 -> :sswitch_3
    .end sparse-switch
.end method

.method private handlerMsgIdr(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 118
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 119
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_LONG_PRESS:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 120
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v2

    .line 119
    if-ne v1, v2, :cond_1

    .line 121
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 124
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 125
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    .line 126
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 127
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->pressUpDown(Landroid/os/Bundle;)V

    goto :goto_0

    .line 129
    :cond_2
    const-string v1, "idriver_state_enum"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_UP:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 130
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_3

    .line 131
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    goto :goto_0

    .line 133
    :cond_3
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    goto :goto_0
.end method

.method private initData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 251
    new-instance v0, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->context:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    .line 252
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->getBookList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 255
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastindex:I

    .line 256
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v3, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadType:I

    .line 257
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->tryToDownloadPhoneBook()V

    .line 261
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setData(Ljava/util/List;)V

    .line 262
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 263
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->noDataTv:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    :goto_1
    return-void

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->getBookList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->noDataTv:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private initEvent()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    .line 274
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$1;-><init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 290
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->layout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 291
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordView:Landroid/view/View;

    .line 243
    const v1, 0x7f0b008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 242
    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    .line 244
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordView:Landroid/view/View;

    const v1, 0x7f0b008a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->layout:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 245
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordView:Landroid/view/View;

    const v1, 0x7f0b008c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->noDataTv:Landroid/widget/TextView;

    .line 246
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->noDataTv:Landroid/widget/TextView;

    const v1, 0x7f070077

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 213
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 214
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->context:Landroid/content/Context;

    .line 215
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/Launcher;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    .line 216
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 217
    return-void
.end method

.method public onBack()Z
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 208
    invoke-super {p0, p1}, Lcom/touchus/benchilauncher/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 209
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 222
    const v0, 0x7f03001b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordView:Landroid/view/View;

    .line 223
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->initView()V

    .line 224
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->initData()V

    .line 225
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->initEvent()V

    .line 226
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 301
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onDestroy()V

    .line 303
    return-void
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 307
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadflag:I

    .line 309
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->lastindex:I

    .line 310
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iput v1, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->downloadType:I

    .line 311
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->tryToDownloadPhoneBook()V

    .line 313
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 232
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStart()V

    .line 233
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->mHandler:Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 238
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onStop()V

    .line 239
    return-void
.end method

.method public pressUpDown(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 139
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 143
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 142
    if-eq v3, v4, :cond_2

    .line 144
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 145
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 144
    if-ne v3, v4, :cond_5

    .line 146
    :cond_2
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    .line 147
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 148
    iput v5, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    .line 149
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    .line 151
    :cond_3
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 152
    .local v0, "firstVisiblePosition":I
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    if-ge v3, v0, :cond_4

    .line 153
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    add-int/lit8 v4, v4, -0x5

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    .line 202
    .end local v0    # "firstVisiblePosition":I
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->setSelectItem(I)V

    .line 203
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 155
    :cond_5
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 156
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    if-eq v3, v4, :cond_6

    .line 157
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 158
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 157
    if-ne v3, v4, :cond_8

    .line 159
    :cond_6
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    .line 160
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    iget-object v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v4}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->getCount()I

    move-result v4

    if-ne v3, v4, :cond_7

    .line 161
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecordAdapter:Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/adapter/CallRecordAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    .line 162
    iput-boolean v5, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->isLongPress:Z

    .line 164
    :cond_7
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 165
    .local v1, "lastVisiblePosition":I
    iget v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    if-le v3, v1, :cond_4

    .line 166
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->listView_record:Landroid/widget/ListView;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    goto :goto_1

    .line 188
    .end local v1    # "lastVisiblePosition":I
    :cond_8
    const-string v3, "idriver_enum"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v3

    sget-object v4, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 189
    invoke-virtual {v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v4

    .line 188
    if-ne v3, v4, :cond_0

    .line 191
    sget-boolean v3, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    if-eqz v3, :cond_4

    .line 192
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->callRecords:Ljava/util/List;

    iget v4, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->currentPosition:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/touchus/benchilauncher/bean/Person;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v2

    .line 194
    .local v2, "phonenum":Ljava/lang/String;
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v6, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isComeFromRecord:Z

    .line 195
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v6, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    .line 196
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v2, v3, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    .line 197
    iget-object v3, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->launcher:Lcom/touchus/benchilauncher/Launcher;

    new-instance v4, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-direct {v4}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1
.end method
