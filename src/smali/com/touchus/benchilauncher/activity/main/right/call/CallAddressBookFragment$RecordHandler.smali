.class Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;
.super Landroid/os/Handler;
.source "CallAddressBookFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RecordHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;)V
    .locals 1
    .param p1, "instance"    # Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;->target:Ljava/lang/ref/WeakReference;

    .line 76
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 81
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment$RecordHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;

    invoke-static {v0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;->access$0(Lcom/touchus/benchilauncher/activity/main/right/call/CallAddressBookFragment;Landroid/os/Message;)V

    goto :goto_0
.end method
