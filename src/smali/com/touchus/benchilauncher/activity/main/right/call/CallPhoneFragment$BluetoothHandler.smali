.class Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;
.super Landroid/os/Handler;
.source "CallPhoneFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BluetoothHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;)V
    .locals 1
    .param p1, "instance"    # Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    .prologue
    .line 141
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 142
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;->target:Ljava/lang/ref/WeakReference;

    .line 143
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 148
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment$BluetoothHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-static {v0, p1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;->access$0(Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;Landroid/os/Message;)V

    goto :goto_0
.end method
