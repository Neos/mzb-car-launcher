.class public Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;
.super Lcom/touchus/benchilauncher/base/BaseHandlerFragment;
.source "MenuFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;
    }
.end annotation


# static fields
.field public static mCurIndex:I

.field public static mCurPage:I


# instance fields
.field mImgPoint0:Landroid/widget/ImageView;

.field mImgPoint1:Landroid/widget/ImageView;

.field mImgPoint2:Landroid/widget/ImageView;

.field mLaucher:Lcom/touchus/benchilauncher/Launcher;

.field mMenuContainer:Landroid/widget/LinearLayout;

.field mMenuSlide:Lcom/touchus/benchilauncher/views/MenuSlide;

.field mParams:Landroid/widget/LinearLayout$LayoutParams;

.field mSlideRunnable:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;

.field mSpaArrBigMenu:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mSpaArrPoint:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mSpaArrSmallMenu:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;-><init>()V

    .line 43
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrPoint:Landroid/util/SparseArray;

    .line 44
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    .line 45
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrSmallMenu:Landroid/util/SparseArray;

    .line 208
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;-><init>(Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSlideRunnable:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;

    .line 35
    return-void
.end method

.method private init()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 220
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->MENU_LIST:[I

    array-length v5, v5

    if-lt v3, v5, :cond_0

    .line 230
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrPoint:Landroid/util/SparseArray;

    iget-object v6, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mImgPoint0:Landroid/widget/ImageView;

    invoke-virtual {v5, v8, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 231
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrPoint:Landroid/util/SparseArray;

    iget-object v6, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mImgPoint1:Landroid/widget/ImageView;

    invoke-virtual {v5, v7, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 233
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->MENU_LIST:[I

    array-length v0, v5

    .line 234
    .local v0, "btnNum":I
    const/4 v3, 0x0

    :goto_1
    if-lt v3, v0, :cond_1

    .line 258
    sget v5, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->selectMenu(I)V

    .line 262
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuSlide:Lcom/touchus/benchilauncher/views/MenuSlide;

    new-instance v6, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$3;

    invoke-direct {v6, p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$3;-><init>(Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;)V

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/views/MenuSlide;->setOnSlideListener(Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;)V

    .line 281
    return-void

    .line 221
    .end local v0    # "btnNum":I
    :cond_0
    new-instance v4, Lcom/touchus/benchilauncher/views/MyMenuView;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/touchus/benchilauncher/views/MyMenuView;-><init>(Landroid/content/Context;)V

    .line 222
    .local v4, "imgV":Lcom/touchus/benchilauncher/views/MyMenuView;
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->MENU_LIST:[I

    aget v5, v5, v3

    .line 223
    sget-object v6, Lcom/touchus/benchilauncher/SysConst;->MENU_SMALL_ICON:[I

    aget v6, v6, v3

    .line 222
    invoke-virtual {v4, v5, v6, v8}, Lcom/touchus/benchilauncher/views/MyMenuView;->setImageBitmap(IIZ)V

    .line 224
    invoke-virtual {p0, v4, v8}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->addView(Lcom/touchus/benchilauncher/views/MyMenuView;I)V

    .line 225
    new-instance v4, Lcom/touchus/benchilauncher/views/MyMenuView;

    .end local v4    # "imgV":Lcom/touchus/benchilauncher/views/MyMenuView;
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/touchus/benchilauncher/views/MyMenuView;-><init>(Landroid/content/Context;)V

    .line 226
    .restart local v4    # "imgV":Lcom/touchus/benchilauncher/views/MyMenuView;
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->MENU_LIST:[I

    aget v5, v5, v3

    .line 227
    sget-object v6, Lcom/touchus/benchilauncher/SysConst;->MENU_BIG_ICON:[I

    aget v6, v6, v3

    .line 226
    invoke-virtual {v4, v5, v6, v7}, Lcom/touchus/benchilauncher/views/MyMenuView;->setImageBitmap(IIZ)V

    .line 228
    invoke-virtual {p0, v4, v7}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->addView(Lcom/touchus/benchilauncher/views/MyMenuView;I)V

    .line 220
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 235
    .end local v4    # "imgV":Lcom/touchus/benchilauncher/views/MyMenuView;
    .restart local v0    # "btnNum":I
    :cond_1
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrSmallMenu:Landroid/util/SparseArray;

    iget-object v6, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuContainer:Landroid/widget/LinearLayout;

    mul-int/lit8 v7, v3, 0x2

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 236
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrSmallMenu:Landroid/util/SparseArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 237
    move v1, v3

    .line 238
    .local v1, "finalI":I
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuContainer:Landroid/widget/LinearLayout;

    mul-int/lit8 v6, v3, 0x2

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 239
    new-instance v6, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$1;

    invoke-direct {v6, p0, v1}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$1;-><init>(Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;I)V

    .line 238
    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    iget-object v6, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuContainer:Landroid/widget/LinearLayout;

    mul-int/lit8 v7, v3, 0x2

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 247
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 248
    move v2, v3

    .line 249
    .local v2, "finalI1":I
    iget-object v5, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 250
    new-instance v6, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$2;

    invoke-direct {v6, p0, v2}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$2;-><init>(Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;I)V

    .line 249
    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method public addView(Lcom/touchus/benchilauncher/views/MyMenuView;I)V
    .locals 4
    .param p1, "imgV"    # Lcom/touchus/benchilauncher/views/MyMenuView;
    .param p2, "state"    # I

    .prologue
    const/4 v3, 0x0

    .line 318
    if-nez p2, :cond_0

    .line 319
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x8a

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 320
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0xa3

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 325
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 326
    return-void

    .line 322
    :cond_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0xe6

    const/16 v2, 0xfa

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 323
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x77

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method

.method public createBmp(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 19
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "rID"    # I

    .prologue
    .line 345
    sget-object v17, Lcom/touchus/benchilauncher/LauncherApplication;->mContext:Landroid/content/Context;

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 344
    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 346
    .local v8, "imgMarker":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 347
    .local v16, "width":I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 348
    .local v7, "height":I
    sget-object v17, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v7, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 349
    .local v9, "imgTemp":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 350
    .local v4, "canvas":Landroid/graphics/Canvas;
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 351
    .local v13, "paint":Landroid/graphics/Paint;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 352
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 353
    new-instance v14, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-direct {v14, v0, v1, v2, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 354
    .local v14, "src":Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-direct {v5, v0, v1, v2, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 355
    .local v5, "dst":Landroid/graphics/Rect;
    invoke-virtual {v4, v8, v14, v5, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 357
    new-instance v15, Landroid/graphics/Paint;

    const/16 v17, 0x101

    move/from16 v0, v17

    invoke-direct {v15, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 359
    .local v15, "textPaint":Landroid/graphics/Paint;
    const v17, -0x41c7ae14    # -0.18f

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 360
    const/high16 v17, 0x41f00000    # 30.0f

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 361
    sget-object v17, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 362
    const/16 v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 363
    const/4 v10, 0x0

    .line 364
    .local v10, "index":I
    const/4 v12, 0x0

    .line 365
    .local v12, "offsetX":I
    invoke-virtual/range {p0 .. p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v11, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 366
    .local v11, "local":Ljava/util/Locale;
    invoke-virtual {v11}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v17

    const-string v18, "UK"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    invoke-virtual {v11}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v17

    const-string v18, "US"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 367
    :cond_0
    const/16 v17, 0x7

    move/from16 v0, v17

    new-array v6, v0, [I

    fill-array-data v6, :array_0

    .line 368
    .local v6, "earr":[I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x3

    array-length v0, v6

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_1

    array-length v0, v6

    move/from16 v17, v0

    add-int/lit8 v10, v17, -0x1

    .line 370
    :goto_0
    aget v12, v6, v10

    .line 376
    .end local v6    # "earr":[I
    :goto_1
    int-to-float v0, v12

    move/from16 v17, v0

    const/high16 v18, 0x43430000    # 195.0f

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v4, v0, v1, v2, v15}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 377
    return-object v9

    .line 369
    .restart local v6    # "earr":[I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v10, v17, -0x3

    goto :goto_0

    .line 372
    .end local v6    # "earr":[I
    :cond_2
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v3, v0, [I

    fill-array-data v3, :array_1

    .line 373
    .local v3, "arr":[I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v10, v17, -0x2

    .line 374
    aget v12, v3, v10

    goto :goto_1

    .line 367
    :array_0
    .array-data 4
        0x64
        0x5a
        0x50
        0x3c
        0x3c
        0x3c
        0x28
    .end array-data

    .line 372
    :array_1
    .array-data 4
        0x5a
        0x4b
        0x3c
        0x46
    .end array-data
.end method

.method public createBmp1(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 19
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "rID"    # I

    .prologue
    .line 395
    sget-object v17, Lcom/touchus/benchilauncher/LauncherApplication;->mContext:Landroid/content/Context;

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 394
    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 396
    .local v8, "imgMarker":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    .line 397
    .local v16, "width":I
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 398
    .local v7, "height":I
    sget-object v17, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v7, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 399
    .local v9, "imgTemp":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 400
    .local v4, "canvas":Landroid/graphics/Canvas;
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 401
    .local v13, "paint":Landroid/graphics/Paint;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 402
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 403
    new-instance v14, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-direct {v14, v0, v1, v2, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 404
    .local v14, "src":Landroid/graphics/Rect;
    new-instance v5, Landroid/graphics/Rect;

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-direct {v5, v0, v1, v2, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 405
    .local v5, "dst":Landroid/graphics/Rect;
    invoke-virtual {v4, v8, v14, v5, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 407
    new-instance v15, Landroid/graphics/Paint;

    const/16 v17, 0x101

    move/from16 v0, v17

    invoke-direct {v15, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 409
    .local v15, "textPaint":Landroid/graphics/Paint;
    const v17, -0x41c7ae14    # -0.18f

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 410
    const v17, 0x418e147b    # 17.76f

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 411
    sget-object v17, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 412
    const/16 v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 414
    const/4 v10, 0x0

    .line 415
    .local v10, "index":I
    const/4 v12, 0x0

    .line 416
    .local v12, "offsetX":I
    invoke-virtual/range {p0 .. p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v11, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 417
    .local v11, "local":Ljava/util/Locale;
    invoke-virtual {v11}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v17

    const-string v18, "UK"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    invoke-virtual {v11}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v17

    const-string v18, "US"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 418
    :cond_0
    const/16 v17, 0x7

    move/from16 v0, v17

    new-array v6, v0, [I

    fill-array-data v6, :array_0

    .line 419
    .local v6, "earr":[I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x3

    array-length v0, v6

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_1

    array-length v0, v6

    move/from16 v17, v0

    add-int/lit8 v10, v17, -0x1

    .line 421
    :goto_0
    aget v12, v6, v10

    .line 427
    .end local v6    # "earr":[I
    :goto_1
    int-to-float v0, v12

    move/from16 v17, v0

    const v18, 0x3f178d50    # 0.592f

    mul-float v17, v17, v18

    const v18, 0x42e6e148    # 115.44f

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v4, v0, v1, v2, v15}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 429
    return-object v9

    .line 420
    .restart local v6    # "earr":[I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v10, v17, -0x3

    goto :goto_0

    .line 423
    .end local v6    # "earr":[I
    :cond_2
    const/16 v17, 0x4

    move/from16 v0, v17

    new-array v3, v0, [I

    fill-array-data v3, :array_1

    .line 424
    .local v3, "arr":[I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v10, v17, -0x2

    .line 425
    aget v12, v3, v10

    goto :goto_1

    .line 418
    nop

    :array_0
    .array-data 4
        0x5f
        0x55
        0x4b
        0x3c
        0x3c
        0x3c
        0x28
    .end array-data

    .line 423
    :array_1
    .array-data 4
        0x5a
        0x4b
        0x3c
        0x46
    .end array-data
.end method

.method public handlerMsg(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 88
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 90
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 91
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v1

    .line 92
    .local v1, "mIDRIVERENUM":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_1

    .line 93
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_2

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->next()V

    goto :goto_0

    .line 95
    :cond_2
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 96
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-eq v1, v2, :cond_3

    .line 97
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_4

    .line 98
    :cond_3
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->pre()V

    goto :goto_0

    .line 99
    :cond_4
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v2

    if-ne v1, v2, :cond_0

    .line 100
    iget-object v2, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mLaucher:Lcom/touchus/benchilauncher/Launcher;

    sget v3, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/Launcher;->goTo(I)V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1771
        :pswitch_0
    .end packed-switch
.end method

.method public next()V
    .locals 4

    .prologue
    .line 192
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->select(I)V

    .line 195
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSlideRunnable:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    .line 55
    const v1, 0x7f030036

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0b0139

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/views/MenuSlide;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuSlide:Lcom/touchus/benchilauncher/views/MenuSlide;

    .line 58
    const v1, 0x7f0b013a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 57
    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuContainer:Landroid/widget/LinearLayout;

    .line 59
    const v1, 0x7f0b013b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mImgPoint0:Landroid/widget/ImageView;

    .line 60
    const v1, 0x7f0b013c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mImgPoint1:Landroid/widget/ImageView;

    .line 61
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mImgPoint2:Landroid/widget/ImageView;

    .line 62
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/Launcher;

    iput-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mLaucher:Lcom/touchus/benchilauncher/Launcher;

    .line 63
    invoke-direct {p0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->init()V

    .line 64
    const-string v1, ""

    const-string v2, "onCreateView"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mLaucher:Lcom/touchus/benchilauncher/Launcher;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/Launcher;->mMenuShow:Z

    .line 66
    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->onDestroyView()V

    .line 72
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mLaucher:Lcom/touchus/benchilauncher/Launcher;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/Launcher;->mMenuShow:Z

    .line 73
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->onStart()V

    .line 78
    return-void
.end method

.method public pre()V
    .locals 4

    .prologue
    .line 182
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    if-nez v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->select(I)V

    .line 185
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSlideRunnable:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$SlideRunnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public press()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mLaucher:Lcom/touchus/benchilauncher/Launcher;

    sget v1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/Launcher;->goTo(I)V

    .line 203
    return-void
.end method

.method public declared-synchronized select(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 114
    monitor-enter p0

    const/4 v0, 0x5

    if-ge p1, v0, :cond_1

    .line 115
    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I

    .line 122
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->showBig(I)V

    .line 123
    sput p1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    .line 124
    sget v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->selectPoint(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 116
    :cond_1
    const/16 v0, 0xa

    if-ge p1, v0, :cond_2

    .line 117
    const/4 v0, 0x1

    :try_start_1
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 118
    :cond_2
    const/16 v0, 0xf

    if-ge p1, v0, :cond_0

    .line 119
    const/4 v0, 0x2

    :try_start_2
    sput v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public selectDelay(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 301
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment$4;-><init>(Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;I)V

    .line 306
    const-wide/16 v2, 0x190

    .line 301
    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 307
    return-void
.end method

.method public selectMenu(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 285
    const/4 v0, 0x5

    if-ge p1, v0, :cond_1

    .line 286
    sput v2, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I

    .line 291
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuSlide:Lcom/touchus/benchilauncher/views/MenuSlide;

    sget v1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/views/MenuSlide;->slide2Page(II)V

    .line 292
    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->select(I)V

    .line 293
    return-void

    .line 287
    :cond_1
    const/16 v0, 0xa

    if-ge p1, v0, :cond_2

    .line 288
    const/4 v0, 0x1

    sput v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I

    goto :goto_0

    .line 289
    :cond_2
    const/16 v0, 0xf

    if-ge p1, v0, :cond_0

    .line 290
    const/4 v0, 0x2

    sput v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurPage:I

    goto :goto_0
.end method

.method public selectPoint(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 160
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrPoint:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 164
    return-void

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrPoint:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public showBig(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 143
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 152
    return-void

    .line 144
    :cond_0
    if-ne v0, p1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrSmallMenu:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 143
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v1, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrSmallMenu:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public showSmall(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrBigMenu:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mSpaArrSmallMenu:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 135
    return-void
.end method

.method public slideMenu(I)V
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mMenuSlide:Lcom/touchus/benchilauncher/views/MenuSlide;

    const/16 v1, 0x1f4

    invoke-virtual {v0, p1, v1}, Lcom/touchus/benchilauncher/views/MenuSlide;->slide2Page(II)V

    .line 173
    return-void
.end method
