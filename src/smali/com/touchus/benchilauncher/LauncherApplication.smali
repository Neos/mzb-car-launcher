.class public Lcom/touchus/benchilauncher/LauncherApplication;
.super Landroid/app/Application;
.source "LauncherApplication.java"


# static fields
.field private static FLAG_CITY_CODE:Ljava/lang/String;

.field private static FLAG_DEVICE_NAME:Ljava/lang/String;

.field private static FLAG_LAST_ENTER_PAIR_STATE:Ljava/lang/String;

.field private static FLAG_NAVI_APP:Ljava/lang/String;

.field private static FLAG_NEED_AUTO_START_NAVI:Ljava/lang/String;

.field private static FLAG_WIFI_AP_ID_KEY:Ljava/lang/String;

.field private static FLAG_WIFI_AP_STATE:Ljava/lang/String;

.field private static FLAG_WIFI_HAS_PWD:Ljava/lang/String;

.field public static iAccOff:Z

.field public static iPlaying:Z

.field public static iPlayingAuto:Z

.field public static imageIndex:I

.field public static imageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field public static isBT:Z

.field public static isBlueConnectState:Z

.field public static isGPSLocation:Z

.field public static mContext:Landroid/content/Context;

.field public static mInstance:Lcom/touchus/benchilauncher/LauncherApplication;

.field public static mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field public static menuSelectType:I

.field public static musicIndex:I

.field public static musicList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field

.field public static pageCount:I

.field public static shutDoorNeedShowYibiao:Z

.field public static videoIndex:I

.field public static videoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/publicutils/bean/MediaBean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final BluetoothMusicType:I

.field public address:Ljava/lang/String;

.field public airInfo:Lcom/backaudio/android/driver/beans/AirInfo;

.field public breakpos:I

.field public btConnectDialog:Z

.field public btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

.field public carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

.field public carRunInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

.field public connectPos:I

.field private countMusic:I

.field private countMusicBluetooth:I

.field private countRing:I

.field public curCanboxVersion:Ljava/lang/String;

.field public curMcuVersion:Ljava/lang/String;

.field public currentCityName:Ljava/lang/String;

.field public currentDialog1:Landroid/app/Dialog;

.field public currentDialog2:Landroid/app/Dialog;

.field public currentDialog3:Landroid/app/Dialog;

.field public currentDialog4:Landroid/app/Dialog;

.field public day:I

.field public eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

.field public gpsCityName:Ljava/lang/String;

.field public hour:I

.field public iCurrentInLauncher:Z

.field public iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public iIsYibiaoShowing:Z

.field public iLanguageType:I

.field public iMedeaDeviceClose:Z

.field public isAirhide:Z

.field private isBluetoothMusicMute:Z

.field public isCallDialog:Z

.field public isCalling:Z

.field public isComeFromRecord:Z

.field public isEasyBtConnect:Z

.field public isEasyView:Z

.field public isOriginalKeyOpen:Z

.field public isPhoneNumFromRecord:Z

.field public isSUV:Z

.field public isScanner:Z

.field private isStreamMusicMute:Z

.field private isStreamRingMute:Z

.field public isTestOpen:Z

.field public ismix:Z

.field public istop:Z

.field public kwapi:Lcn/kuwo/autosdk/api/KWAPI;

.field public launcherHandler:Landroid/os/Handler;

.field logger:Lorg/slf4j/Logger;

.field private mDVAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public mHandler:Landroid/os/Handler;

.field private mHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public mHomeAndBackEnable:Z

.field private mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public mOriginalViewHandler:Landroid/os/Handler;

.field public min:I

.field public month:I

.field public musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

.field public musicType:I

.field public naviPos:I

.field public phoneName:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public rSpeed:I

.field public radioPos:I

.field public recorPhoneNumber:Ljava/lang/String;

.field public screenPos:I

.field public second:I

.field public service:Lcom/touchus/benchilauncher/MainService;

.field public serviceHandler:Landroid/os/Handler;

.field private share:Landroid/content/SharedPreferences;

.field public speed:I

.field public timeFormat:I

.field public usbPos:I

.field public weatherTemp:Ljava/lang/String;

.field public weatherTempDes:Ljava/lang/String;

.field public year:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->shutDoorNeedShowYibiao:Z

    .line 66
    const-string v0, "cityCode"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_CITY_CODE:Ljava/lang/String;

    .line 68
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBlueConnectState:Z

    .line 69
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isGPSLocation:Z

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    .line 88
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iPlaying:Z

    .line 89
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iPlayingAuto:Z

    .line 94
    const/4 v0, -0x1

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->menuSelectType:I

    .line 108
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    .line 118
    const/4 v0, 0x1

    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    .line 132
    const/4 v0, 0x3

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->pageCount:I

    .line 537
    const-string v0, "needEnterPair"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_LAST_ENTER_PAIR_STATE:Ljava/lang/String;

    .line 561
    const-string v0, "needAutoStartNavi"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_NEED_AUTO_START_NAVI:Ljava/lang/String;

    .line 574
    const-string v0, "naviapp"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_NAVI_APP:Ljava/lang/String;

    .line 587
    const-string v0, "wifiApState"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_AP_STATE:Ljava/lang/String;

    .line 600
    const-string v0, "wifiApIdAndKey"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_AP_ID_KEY:Ljava/lang/String;

    .line 601
    const-string v0, "hadpwd"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_HAS_PWD:Ljava/lang/String;

    .line 629
    const-string v0, "lastDevice"

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_DEVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 64
    const-class v0, Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->logger:Lorg/slf4j/Logger;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneNumber:Ljava/lang/String;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->phoneName:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->recorPhoneNumber:Ljava/lang/String;

    .line 77
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isPhoneNumFromRecord:Z

    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isComeFromRecord:Z

    .line 79
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iCurrentInLauncher:Z

    .line 95
    iput-boolean v3, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->curMcuVersion:Ljava/lang/String;

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->curCanboxVersion:Ljava/lang/String;

    .line 100
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iLanguageType:I

    .line 102
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 104
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    .line 105
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    .line 106
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    .line 109
    const/16 v0, 0x7e1

    iput v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->year:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->month:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->day:I

    const/16 v0, 0x18

    iput v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->timeFormat:I

    .line 111
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    .line 112
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    .line 113
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    .line 114
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 115
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    .line 116
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->breakpos:I

    .line 120
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    .line 122
    iput-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog1:Landroid/app/Dialog;

    .line 124
    iput-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    .line 126
    iput-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    .line 128
    iput-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    .line 130
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->btConnectDialog:Z

    .line 144
    iput-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    .line 146
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    .line 148
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyBtConnect:Z

    .line 150
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isEasyView:Z

    .line 152
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    .line 154
    new-instance v0, Lcom/touchus/benchilauncher/LauncherApplication$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/LauncherApplication$1;-><init>(Lcom/touchus/benchilauncher/LauncherApplication;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    .line 232
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->istop:Z

    .line 346
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isScanner:Z

    .line 350
    iput-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    .line 353
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 369
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 371
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    .line 373
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 374
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iMedeaDeviceClose:Z

    .line 649
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->weatherTempDes:Ljava/lang/String;

    .line 650
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->weatherTemp:Ljava/lang/String;

    .line 697
    const-string v0, ""

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentCityName:Ljava/lang/String;

    .line 717
    new-instance v0, Lcom/touchus/benchilauncher/LauncherApplication$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/LauncherApplication$2;-><init>(Lcom/touchus/benchilauncher/LauncherApplication;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 763
    new-instance v0, Lcom/touchus/benchilauncher/LauncherApplication$3;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/LauncherApplication$3;-><init>(Lcom/touchus/benchilauncher/LauncherApplication;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mDVAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 785
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isBluetoothMusicMute:Z

    .line 786
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isStreamMusicMute:Z

    .line 787
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isStreamRingMute:Z

    .line 789
    const/16 v0, 0xc8

    iput v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->BluetoothMusicType:I

    .line 791
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusic:I

    .line 792
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusicBluetooth:I

    .line 793
    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countRing:I

    .line 63
    return-void
.end method

.method private closeScreen()V
    .locals 4

    .prologue
    .line 471
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    const/4 v0, 0x1

    .line 473
    .local v0, "iCloseScreen":Z
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/backaudio/android/driver/Mainboard;->closeOrOpenScreen(Z)V

    .line 475
    .end local v0    # "iCloseScreen":Z
    :cond_0
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 341
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mInstance:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method private initData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 274
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "musicPos"

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    .line 275
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v1, "videoPos"

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    .line 276
    const-string v0, "LauncherApplication"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "musicIndex = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",videoIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/touchus/benchilauncher/LauncherApplication;->videoIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    return-void
.end method

.method private initLogger()V
    .locals 6

    .prologue
    .line 317
    :try_start_0
    new-instance v0, Lde/mindpipe/android/logging/log4j/LogConfigurator;

    invoke-direct {v0}, Lde/mindpipe/android/logging/log4j/LogConfigurator;-><init>()V

    .line 318
    .local v0, "configurator":Lde/mindpipe/android/logging/log4j/LogConfigurator;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 319
    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "log.txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 318
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 320
    .local v3, "logfilename":Ljava/lang/String;
    invoke-virtual {v0, v3}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setFileName(Ljava/lang/String;)V

    .line 321
    sget-object v4, Lorg/apache/log4j/Level;->DEBUG:Lorg/apache/log4j/Level;

    invoke-virtual {v0, v4}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setRootLevel(Lorg/apache/log4j/Level;)V

    .line 322
    const-string v4, "org.apache"

    sget-object v5, Lorg/apache/log4j/Level;->ERROR:Lorg/apache/log4j/Level;

    invoke-virtual {v0, v4, v5}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setLevel(Ljava/lang/String;Lorg/apache/log4j/Level;)V

    .line 323
    const-string v4, "%d - %F:%L - %m  [%t]%n"

    invoke-virtual {v0, v4}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setFilePattern(Ljava/lang/String;)V

    .line 324
    const-wide/32 v4, 0x200000

    invoke-virtual {v0, v4, v5}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setMaxFileSize(J)V

    .line 325
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setMaxBackupSize(I)V

    .line 328
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->setImmediateFlush(Z)V

    .line 329
    invoke-virtual {v0}, Lde/mindpipe/android/logging/log4j/LogConfigurator;->configure()V

    .line 332
    const-class v4, Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-static {v4}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v2

    .line 333
    .local v2, "log":Lorg/slf4j/Logger;
    const-string v4, "init log4j"

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    .end local v0    # "configurator":Lde/mindpipe/android/logging/log4j/LogConfigurator;
    .end local v2    # "log":Lorg/slf4j/Logger;
    .end local v3    # "logfilename":Ljava/lang/String;
    :goto_0
    return-void

    .line 334
    :catch_0
    move-exception v1

    .line 335
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private wakeupScreen()V
    .locals 4

    .prologue
    .line 478
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 479
    const/4 v0, 0x0

    .line 480
    .local v0, "iCloseScreen":Z
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/backaudio/android/driver/Mainboard;->closeOrOpenScreen(Z)V

    .line 482
    .end local v0    # "iCloseScreen":Z
    :cond_0
    return-void
.end method


# virtual methods
.method public abandonDVAudioFocus()V
    .locals 3

    .prologue
    .line 756
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 757
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mDVAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 758
    iget v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 759
    const/4 v1, 0x0

    iput v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 761
    :cond_0
    return-void
.end method

.method public abandonMainAudioFocus()V
    .locals 2

    .prologue
    .line 713
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 714
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 715
    return-void
.end method

.method public changeAirplane(Z)V
    .locals 4
    .param p1, "iEnterAirplane"    # Z

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 514
    const-string v3, "airplane_mode_on"

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 513
    :goto_0
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 516
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 517
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 519
    sget-object v1, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 520
    return-void

    .line 514
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized closeOrWakeupScreen(Z)V
    .locals 1
    .param p1, "iClose"    # Z

    .prologue
    .line 464
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 465
    :try_start_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->closeScreen()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    :goto_0
    monitor-exit p0

    return-void

    .line 467
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->wakeupScreen()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog3:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 687
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 689
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog1:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog1:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 690
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog1:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 692
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 695
    :cond_3
    return-void
.end method

.method public getLanguage()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 450
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 451
    .local v0, "language":Ljava/lang/String;
    const-string v1, "zh_CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v2, "languageType"

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 453
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/backaudio/android/driver/Mainboard;->sendLanguageSetToMcu(I)V

    .line 461
    :goto_0
    return-void

    .line 454
    :cond_0
    const-string v1, "zh_TW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 455
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v2, "languageType"

    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 456
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/backaudio/android/driver/Mainboard;->sendLanguageSetToMcu(I)V

    goto :goto_0

    .line 458
    :cond_1
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    const-string v2, "languageType"

    invoke-virtual {v1, v2, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 459
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/backaudio/android/driver/Mainboard;->sendLanguageSetToMcu(I)V

    goto :goto_0
.end method

.method public getNaviAPP()Ljava/lang/String;
    .locals 4

    .prologue
    .line 583
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_NAVI_APP:Ljava/lang/String;

    const v3, 0x7f070004

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 584
    .local v0, "value":Ljava/lang/String;
    return-object v0
.end method

.method public getNeedStartNaviPkgs()Z
    .locals 4

    .prologue
    .line 570
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_NEED_AUTO_START_NAVI:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 571
    .local v0, "value":Z
    return v0
.end method

.method public getNeedToEnterPairMode()Z
    .locals 4

    .prologue
    .line 546
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_LAST_ENTER_PAIR_STATE:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 547
    .local v0, "flag":Z
    return v0
.end method

.method public getWifiApHasPwd()Z
    .locals 3

    .prologue
    .line 626
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_HAS_PWD:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getWifiApIdAndKey()[Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 604
    iget-object v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    sget-object v6, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_AP_ID_KEY:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "11111111;"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 605
    const v8, 0x7f07001c

    invoke-virtual {p0, v8}, Lcom/touchus/benchilauncher/LauncherApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 604
    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 606
    .local v4, "value":Ljava/lang/String;
    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 607
    .local v1, "index":I
    invoke-virtual {v4, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 608
    .local v2, "key":Ljava/lang/String;
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 609
    .local v3, "name":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    aput-object v3, v0, v9

    const/4 v5, 0x1

    aput-object v2, v0, v5

    .line 610
    .local v0, "idAndKey":[Ljava/lang/String;
    return-object v0
.end method

.method public getWifiApState()Z
    .locals 4

    .prologue
    .line 596
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_AP_STATE:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 597
    .local v0, "value":Z
    return v0
.end method

.method public iNeedToChangeLocalContacts(Ljava/lang/String;)Z
    .locals 4
    .param p1, "equipAddress"    # Ljava/lang/String;

    .prologue
    .line 633
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 634
    .local v1, "sp":Landroid/content/SharedPreferences;
    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_DEVICE_NAME:Ljava/lang/String;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 635
    .local v0, "deviceName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 636
    const/4 v2, 0x1

    .line 638
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public initModel()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 279
    sget-object v4, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v5, Lcom/touchus/publicutils/sysconst/BenzModel;->KEY:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 280
    .local v0, "code":I
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_0
    if-lt v4, v6, :cond_2

    .line 286
    :goto_1
    sget-object v4, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    sget-object v5, Lcom/touchus/publicutils/sysconst/BenzModel;->SIZE_KEY:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 287
    .local v2, "size":I
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    move-result-object v4

    array-length v5, v4

    :goto_2
    if-lt v3, v5, :cond_4

    .line 293
    :goto_3
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isSUV()Z

    move-result v3

    iput-boolean v3, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isSUV:Z

    .line 294
    const-string v3, "benz"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 295
    const-string v3, "ajbenz"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 296
    const-string v3, "c200_jly"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 297
    const-string v3, "c200_jly_tw"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 298
    const-string v3, "c200_psr"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 299
    :cond_0
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sput-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    .line 301
    :cond_1
    return-void

    .line 280
    .end local v2    # "size":I
    :cond_2
    aget-object v1, v5, v4

    .line 281
    .local v1, "eBenzTpye":Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v7

    if-ne v0, v7, :cond_3

    .line 282
    sput-object v1, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    goto :goto_1

    .line 280
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 287
    .end local v1    # "eBenzTpye":Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    .restart local v2    # "size":I
    :cond_4
    aget-object v1, v4, v3

    .line 288
    .local v1, "eBenzTpye":Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;
    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;->getCode()B

    move-result v6

    if-ne v2, v6, :cond_5

    .line 289
    sput-object v1, Lcom/touchus/publicutils/sysconst/BenzModel;->benzSize:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzSize;

    goto :goto_3

    .line 287
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public interAndroidView()V
    .locals 2

    .prologue
    .line 678
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    .line 679
    return-void
.end method

.method public interBTConnectView()V
    .locals 2

    .prologue
    .line 674
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    .line 675
    return-void
.end method

.method public interOriginalView()V
    .locals 2

    .prologue
    .line 669
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    .line 670
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/touchus/publicutils/utils/CrashHandler;->getInstance(Landroid/content/Context;)Lcom/touchus/publicutils/utils/CrashHandler;

    .line 249
    invoke-direct {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->initLogger()V

    .line 253
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mContext:Landroid/content/Context;

    .line 258
    sget-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mInstance:Lcom/touchus/benchilauncher/LauncherApplication;

    if-nez v0, :cond_0

    .line 259
    sput-object p0, Lcom/touchus/benchilauncher/LauncherApplication;->mInstance:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 262
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    invoke-static {}, Lcom/touchus/benchilauncher/LauncherApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/touchus/benchilauncher/LauncherApplication;->mSpUtils:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 263
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    .line 264
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->initModel()V

    .line 265
    const-string v0, "auto"

    invoke-static {p0, v0}, Lcn/kuwo/autosdk/api/KWAPI;->createKWAPI(Landroid/content/Context;Ljava/lang/String;)Lcn/kuwo/autosdk/api/KWAPI;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    .line 266
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getWifiApIdAndKey()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const v1, 0x7f07001c

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getWifiApIdAndKey()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const-string v1, "11111111"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 268
    invoke-static {p0}, Lcom/touchus/benchilauncher/utils/WifiTool;->setDefaultWifiAp(Landroid/content/Context;)V

    .line 270
    :cond_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->initData()V

    .line 271
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 272
    return-void
.end method

.method public onTerminate()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 306
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 307
    return-void
.end method

.method public openOrCloseBluetooth(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 551
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 554
    :cond_1
    if-eqz p1, :cond_2

    .line 555
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->enterPairingMode()V

    goto :goto_0

    .line 557
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/service/BluetoothService;->leavePairingMode()V

    goto :goto_0
.end method

.method public registerHandler(Landroid/os/Handler;)V
    .locals 3
    .param p1, "mHandler"    # Landroid/os/Handler;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    :cond_0
    const-string v0, "launcherlog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerHandler ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    return-void
.end method

.method public requestDVAudioFocus()V
    .locals 5

    .prologue
    .line 748
    const/4 v2, 0x4

    iput v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 749
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 750
    .local v1, "mAudioManager":Landroid/media/AudioManager;
    iget-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mDVAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 751
    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 750
    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 752
    .local v0, "flag":I
    const-string v2, "Main requestDVAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flag"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    return-void
.end method

.method public requestMainAudioFocus()V
    .locals 5

    .prologue
    .line 705
    const/4 v2, 0x0

    iput v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 706
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 707
    .local v1, "mAudioManager":Landroid/media/AudioManager;
    iget-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mMainAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 708
    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 707
    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 709
    .local v0, "flag":I
    const-string v2, "Main requestAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flag"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    return-void
.end method

.method public sendHideOrShowLocateViewEvent(Z)V
    .locals 2
    .param p1, "iShow"    # Z

    .prologue
    .line 530
    if-eqz p1, :cond_0

    const-string v0, "com.touchus.showlocatefloat"

    .line 532
    .local v0, "action":Ljava/lang/String;
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 533
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->sendBroadcast(Landroid/content/Intent;)V

    .line 535
    return-void

    .line 531
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v0, "com.touchus.hidelocatefloat"

    goto :goto_0
.end method

.method public declared-synchronized sendHideOrShowNavigationBarEvent(Z)V
    .locals 3
    .param p1, "iShow"    # Z

    .prologue
    .line 523
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    const-string v0, "SHOW_NAVIGATION_BAR"

    .line 524
    .local v0, "action":Ljava/lang/String;
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 525
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    monitor-exit p0

    return-void

    .line 523
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :try_start_1
    const-string v0, "HIDE_NAVIGATION_BAR"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public sendMessage(ILandroid/os/Bundle;)V
    .locals 8
    .param p1, "what"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 391
    sget-boolean v5, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    if-eqz v5, :cond_1

    const/16 v5, 0x3ef

    if-eq p1, v5, :cond_1

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const/4 v3, 0x0

    .line 396
    .local v3, "msg":Landroid/os/Message;
    const-string v5, "templog"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mHandlers.size="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    const/16 v5, 0x1771

    if-ne p1, v5, :cond_7

    iget-object v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_7

    .line 398
    const-string v5, "idriver_enum"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 399
    .local v0, "code":B
    iget-object v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 400
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_2

    .line 401
    invoke-direct {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->wakeupScreen()V

    goto :goto_0

    .line 404
    :cond_2
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 405
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 406
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 407
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 408
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NAVI:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 409
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RADIO:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 410
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CARSET:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 411
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MEDIA:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 412
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HANG_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 413
    sget-object v5, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v5

    if-eq v0, v5, :cond_7

    .line 415
    iget-boolean v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    if-nez v5, :cond_0

    .line 419
    const/4 v4, 0x0

    .line 420
    .local v4, "temp":Landroid/os/Handler;
    iget-object v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_1
    if-gez v1, :cond_4

    .line 433
    :cond_3
    const-string v5, "templog"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mHandlers.size="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    if-eqz v4, :cond_7

    .line 435
    invoke-virtual {v4, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 436
    invoke-virtual {v3, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 437
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 421
    :cond_4
    iget-object v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "temp":Landroid/os/Handler;
    check-cast v4, Landroid/os/Handler;

    .line 422
    .restart local v4    # "temp":Landroid/os/Handler;
    iget-boolean v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    if-eqz v5, :cond_6

    .line 423
    instance-of v5, v4, Lcom/touchus/benchilauncher/views/FloatSystemCallDialog$BluetoothHandler;

    if-nez v5, :cond_6

    .line 420
    :cond_5
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 426
    :cond_6
    instance-of v5, v4, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag$RunningHandler;

    if-nez v5, :cond_5

    .line 427
    instance-of v5, v4, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment$MeterHandler;

    if-eqz v5, :cond_3

    goto :goto_2

    .line 442
    .end local v0    # "code":B
    .end local v1    # "i":I
    .end local v4    # "temp":Landroid/os/Handler;
    :cond_7
    iget-object v5, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    .line 443
    .local v2, "mHandler":Landroid/os/Handler;
    invoke-virtual {v2, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 444
    invoke-virtual {v3, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 445
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto :goto_3
.end method

.method public setLastDeviceName(Ljava/lang/String;)V
    .locals 3
    .param p1, "equipAddress"    # Ljava/lang/String;

    .prologue
    .line 643
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 644
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 645
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_DEVICE_NAME:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 646
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 647
    return-void
.end method

.method public setNaviAPP(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 577
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 578
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_NAVI_APP:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 579
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 580
    return-void
.end method

.method public setNeedStartNaviPkg(Z)V
    .locals 2
    .param p1, "isAuto"    # Z

    .prologue
    .line 564
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 565
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_NEED_AUTO_START_NAVI:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 566
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 567
    return-void
.end method

.method public setNeedToEnterPairMode(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 540
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 541
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_LAST_ENTER_PAIR_STATE:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 542
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 543
    return-void
.end method

.method public declared-synchronized setTypeMute(IZ)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "state"    # Z

    .prologue
    .line 803
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setTypeMute type"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  state=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 804
    const-string v2, "setTypeMute"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "type"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  state=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 808
    .local v0, "audioManager":Landroid/media/AudioManager;
    sparse-switch p1, :sswitch_data_0

    .line 845
    :cond_0
    :goto_0
    const-string v2, "setTypeMute"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "singular is mute : countMusic=="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusic:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 846
    const-string v4, " countMusicBluetooth=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusicBluetooth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 847
    const-string v4, " countRing=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countRing:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 845
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 848
    monitor-exit p0

    return-void

    .line 810
    :sswitch_0
    :try_start_1
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isStreamMusicMute:Z

    if-eq v2, p2, :cond_0

    .line 811
    iput-boolean p2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isStreamMusicMute:Z

    .line 812
    if-eqz p2, :cond_1

    .line 813
    const/16 v2, 0x1e

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 817
    :goto_1
    iget v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusic:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusic:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 803
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 815
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_1
    const/16 v2, 0x1e

    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    goto :goto_1

    .line 821
    :sswitch_1
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isStreamRingMute:Z

    if-eq v2, p2, :cond_0

    .line 822
    iput-boolean p2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isStreamRingMute:Z

    .line 823
    if-eqz p2, :cond_2

    .line 824
    const/16 v2, 0x1f

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 825
    const/16 v2, 0x20

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 830
    :goto_2
    iget v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countRing:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countRing:I

    goto :goto_0

    .line 827
    :cond_2
    const/16 v2, 0x1f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 828
    const/16 v2, 0x20

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    goto :goto_2

    .line 834
    :sswitch_2
    iget-boolean v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isBluetoothMusicMute:Z

    if-eq v2, p2, :cond_0

    .line 835
    iput-boolean p2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->isBluetoothMusicMute:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 837
    :try_start_3
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->getInstance()Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->setBluetoothMusicMute(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 841
    :goto_3
    :try_start_4
    iget v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusicBluetooth:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/touchus/benchilauncher/LauncherApplication;->countMusicBluetooth:I

    goto/16 :goto_0

    .line 838
    :catch_0
    move-exception v1

    .line 839
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 808
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0xc8 -> :sswitch_2
    .end sparse-switch
.end method

.method public setWifiApHasPwd(Z)V
    .locals 2
    .param p1, "haspwd"    # Z

    .prologue
    .line 620
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 621
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_HAS_PWD:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 622
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 623
    return-void
.end method

.method public setWifiApIdAndKey([Ljava/lang/String;)V
    .locals 4
    .param p1, "idAndKey"    # [Ljava/lang/String;

    .prologue
    .line 614
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 615
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_AP_ID_KEY:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 616
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 617
    return-void
.end method

.method public setWifiApState(Z)V
    .locals 2
    .param p1, "iOpen"    # Z

    .prologue
    .line 590
    iget-object v1, p0, Lcom/touchus/benchilauncher/LauncherApplication;->share:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 591
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/touchus/benchilauncher/LauncherApplication;->FLAG_WIFI_AP_STATE:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 592
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 593
    return-void
.end method

.method public startAppByPkg(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 498
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 509
    :cond_0
    :goto_0
    return v2

    .line 501
    :cond_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 502
    .local v1, "pkgManger":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 503
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 504
    const/high16 v2, 0x10400000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 506
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->startActivity(Landroid/content/Intent;)V

    .line 507
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public startNavi()V
    .locals 4

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getNaviAPP()Ljava/lang/String;

    move-result-object v1

    .line 486
    .local v1, "pkg":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 495
    :cond_0
    :goto_0
    return-void

    .line 489
    :cond_1
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 490
    .local v2, "pkgManger":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 491
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 492
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 493
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public topIsBlueMainFragment()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->istop:Z

    return v0
.end method

.method public unregisterHandler(Landroid/os/Handler;)V
    .locals 3
    .param p1, "mHandler"    # Landroid/os/Handler;

    .prologue
    .line 383
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication;->mHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 385
    const-string v0, "launcherlog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unregisterHandler ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_0
    return-void
.end method

.method public updateLanguage(Ljava/util/Locale;)V
    .locals 5
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 653
    const-string v3, "ANDROID_LAB"

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    .line 657
    .local v2, "objActMagNative":Landroid/app/IActivityManager;
    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 658
    .local v0, "config":Landroid/content/res/Configuration;
    iput-object p1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 659
    invoke-interface {v2, v0}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    .line 660
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 661
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 660
    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v2    # "objActMagNative":Landroid/app/IActivityManager;
    :goto_0
    return-void

    .line 662
    :catch_0
    move-exception v1

    .line 663
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
