.class public Lcom/touchus/benchilauncher/receiver/AdressInfoReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AdressInfoReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 15
    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    .line 17
    .local v0, "app":Lcom/touchus/benchilauncher/LauncherApplication;
    if-eqz v0, :cond_0

    .line 24
    :goto_0
    return-void

    .line 20
    :cond_0
    const-string v1, "addr"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->address:Ljava/lang/String;

    .line 21
    const-string v1, "cityName"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->gpsCityName:Ljava/lang/String;

    .line 22
    invoke-static {p1}, Lcom/touchus/publicutils/utils/CrashHandler;->getInstance(Landroid/content/Context;)Lcom/touchus/publicutils/utils/CrashHandler;

    move-result-object v1

    iget-object v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->address:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/touchus/publicutils/utils/CrashHandler;->setAddress(Ljava/lang/String;)V

    .line 23
    const-string v1, "AdressInfoReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "address  = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/touchus/benchilauncher/LauncherApplication;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",city = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/touchus/benchilauncher/LauncherApplication;->gpsCityName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
