.class public Lcom/touchus/benchilauncher/receiver/UsbBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UsbBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 19
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 21
    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    .line 24
    .local v1, "app":Lcom/touchus/benchilauncher/LauncherApplication;
    const-string v3, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 26
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 28
    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 30
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 31
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 32
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 51
    :cond_0
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v3, "usblistener"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const/16 v3, 0x1b5a

    invoke-virtual {v1, v3, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 59
    return-void

    .line 33
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_1
    const-string v3, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 35
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isScanner:Z

    goto :goto_0

    .line 36
    :cond_2
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 37
    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isScanner:Z

    goto :goto_0

    .line 38
    :cond_3
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 39
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 40
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 41
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 43
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->musicList:Ljava/util/List;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 43
    invoke-static {v4}, Lcom/touchus/benchilauncher/service/MusicLoader;->instance(Landroid/content/ContentResolver;)Lcom/touchus/benchilauncher/service/MusicLoader;

    move-result-object v4

    .line 44
    invoke-virtual {v4}, Lcom/touchus/benchilauncher/service/MusicLoader;->getMusicList()Ljava/util/ArrayList;

    move-result-object v4

    .line 43
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 45
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->videoList:Ljava/util/List;

    new-instance v4, Lcom/touchus/benchilauncher/service/VideoService;

    invoke-direct {v4, p1}, Lcom/touchus/benchilauncher/service/VideoService;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-virtual {v4}, Lcom/touchus/benchilauncher/service/VideoService;->getVideoList()Ljava/util/List;

    move-result-object v4

    .line 45
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 47
    sget-object v3, Lcom/touchus/benchilauncher/LauncherApplication;->imageList:Ljava/util/List;

    new-instance v4, Lcom/touchus/benchilauncher/service/PictrueUtil;

    invoke-direct {v4, p1}, Lcom/touchus/benchilauncher/service/PictrueUtil;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {v4}, Lcom/touchus/benchilauncher/service/PictrueUtil;->getPicList()Ljava/util/List;

    move-result-object v4

    .line 47
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isScanner:Z

    goto :goto_0
.end method
