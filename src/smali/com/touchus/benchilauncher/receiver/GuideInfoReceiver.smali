.class public Lcom/touchus/benchilauncher/receiver/GuideInfoReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GuideInfoReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0x417

    const/4 v7, 0x5

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 17
    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    .line 19
    .local v0, "app":Lcom/touchus/benchilauncher/LauncherApplication;
    if-eqz v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const-string v2, "GuideInfoReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "guideInfo coming "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "KEY_TYPE"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 23
    const-string v4, ",intent.getExtras = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 22
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const-string v2, "KEY_TYPE"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0x2711

    if-ne v2, v3, :cond_2

    .line 50
    const-string v2, "GuideInfoReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "guideInfo coming "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    sput v7, Lcom/touchus/benchilauncher/LauncherApplication;->pageCount:I

    .line 82
    const/16 v2, 0x415

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 83
    :cond_2
    const-string v2, "KEY_TYPE"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0x2723

    if-ne v2, v3, :cond_0

    .line 84
    const-string v2, "EXTRA_STATE"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 85
    .local v1, "type":I
    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 89
    :sswitch_0
    sput v7, Lcom/touchus/benchilauncher/LauncherApplication;->pageCount:I

    .line 90
    const/16 v2, 0x416

    invoke-virtual {v0, v2, v6}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 95
    :sswitch_1
    const/4 v2, 0x4

    sput v2, Lcom/touchus/benchilauncher/LauncherApplication;->pageCount:I

    .line 96
    invoke-virtual {v0, v8, v6}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 100
    :sswitch_2
    invoke-virtual {v0, v8, v6}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 85
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x9 -> :sswitch_1
        0xa -> :sswitch_0
        0xc -> :sswitch_1
        0x27 -> :sswitch_2
    .end sparse-switch
.end method
