.class public Lcom/touchus/benchilauncher/receiver/MusicControlReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MusicControlReceiver.java"


# instance fields
.field count:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/touchus/benchilauncher/receiver/MusicControlReceiver;->count:I

    .line 15
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 21
    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    .line 24
    .local v0, "app":Lcom/touchus/benchilauncher/LauncherApplication;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "intentAction":Ljava/lang/String;
    const-string v5, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/view/KeyEvent;

    .line 29
    .local v4, "keyEvent":Landroid/view/KeyEvent;
    const-string v5, "musicControl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Action ---->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 30
    const-string v7, "  KeyEvent----->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/view/KeyEvent;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 29
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    const-string v5, "musicControl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "app.musicType ---->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    const-string v5, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 34
    invoke-virtual {v4}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 36
    .local v3, "keyCode":I
    invoke-virtual {v4}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    .line 38
    .local v2, "keyAction":I
    if-eqz v2, :cond_1

    .line 122
    .end local v2    # "keyAction":I
    .end local v3    # "keyCode":I
    :cond_0
    :goto_0
    return-void

    .line 41
    .restart local v2    # "keyAction":I
    .restart local v3    # "keyCode":I
    :cond_1
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    if-eqz v5, :cond_0

    .line 49
    const/16 v5, 0x57

    if-ne v5, v3, :cond_4

    .line 50
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v8, :cond_2

    .line 51
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v5, :cond_0

    .line 54
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playNextMusic()V

    goto :goto_0

    .line 55
    :cond_2
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v9, :cond_3

    .line 56
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/BluetoothService;->playNext()V

    goto :goto_0

    .line 57
    :cond_3
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v10, :cond_0

    .line 58
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    invoke-virtual {v5, p1}, Lcn/kuwo/autosdk/api/KWAPI;->isKuwoRunning(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 59
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    sget-object v6, Lcn/kuwo/autosdk/api/PlayState;->STATE_NEXT:Lcn/kuwo/autosdk/api/PlayState;

    invoke-virtual {v5, p1, v6}, Lcn/kuwo/autosdk/api/KWAPI;->setPlayState(Landroid/content/Context;Lcn/kuwo/autosdk/api/PlayState;)V

    goto :goto_0

    .line 62
    :cond_4
    const/16 v5, 0x7f

    if-ne v5, v3, :cond_7

    .line 63
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v8, :cond_5

    .line 64
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v5, :cond_0

    .line 67
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->pauseMusic()V

    goto :goto_0

    .line 68
    :cond_5
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v9, :cond_6

    .line 69
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    goto :goto_0

    .line 70
    :cond_6
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v10, :cond_0

    .line 71
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    invoke-virtual {v5, p1}, Lcn/kuwo/autosdk/api/KWAPI;->isKuwoRunning(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 72
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    sget-object v6, Lcn/kuwo/autosdk/api/PlayState;->STATE_PAUSE:Lcn/kuwo/autosdk/api/PlayState;

    invoke-virtual {v5, p1, v6}, Lcn/kuwo/autosdk/api/KWAPI;->setPlayState(Landroid/content/Context;Lcn/kuwo/autosdk/api/PlayState;)V

    goto :goto_0

    .line 75
    :cond_7
    const/16 v5, 0x7e

    if-ne v5, v3, :cond_b

    .line 76
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v8, :cond_8

    .line 77
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v5, :cond_0

    .line 80
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->replayMusic()V

    goto :goto_0

    .line 81
    :cond_8
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v9, :cond_9

    .line 82
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v5, v8}, Lcom/touchus/benchilauncher/service/BluetoothService;->pausePlaySync(Z)V

    goto/16 :goto_0

    .line 83
    :cond_9
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v10, :cond_0

    .line 84
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    invoke-virtual {v5, p1}, Lcn/kuwo/autosdk/api/KWAPI;->isKuwoRunning(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 85
    iget v5, p0, Lcom/touchus/benchilauncher/receiver/MusicControlReceiver;->count:I

    rem-int/lit8 v5, v5, 0x2

    if-ne v5, v8, :cond_a

    .line 86
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    .line 87
    sget-object v6, Lcn/kuwo/autosdk/api/PlayState;->STATE_PAUSE:Lcn/kuwo/autosdk/api/PlayState;

    .line 86
    invoke-virtual {v5, p1, v6}, Lcn/kuwo/autosdk/api/KWAPI;->setPlayState(Landroid/content/Context;Lcn/kuwo/autosdk/api/PlayState;)V

    goto/16 :goto_0

    .line 89
    :cond_a
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    .line 90
    sget-object v6, Lcn/kuwo/autosdk/api/PlayState;->STATE_PLAY:Lcn/kuwo/autosdk/api/PlayState;

    .line 89
    invoke-virtual {v5, p1, v6}, Lcn/kuwo/autosdk/api/KWAPI;->setPlayState(Landroid/content/Context;Lcn/kuwo/autosdk/api/PlayState;)V

    goto/16 :goto_0

    .line 94
    :cond_b
    const/16 v5, 0x58

    if-ne v5, v3, :cond_e

    .line 95
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v8, :cond_c

    .line 96
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v5, :cond_0

    .line 99
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->playPreviousMusic()V

    goto/16 :goto_0

    .line 100
    :cond_c
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v9, :cond_d

    .line 101
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/BluetoothService;->playPrev()V

    goto/16 :goto_0

    .line 102
    :cond_d
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v10, :cond_0

    .line 103
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    invoke-virtual {v5, p1}, Lcn/kuwo/autosdk/api/KWAPI;->isKuwoRunning(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 104
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    sget-object v6, Lcn/kuwo/autosdk/api/PlayState;->STATE_PRE:Lcn/kuwo/autosdk/api/PlayState;

    invoke-virtual {v5, p1, v6}, Lcn/kuwo/autosdk/api/KWAPI;->setPlayState(Landroid/content/Context;Lcn/kuwo/autosdk/api/PlayState;)V

    goto/16 :goto_0

    .line 107
    :cond_e
    const/16 v5, 0x56

    if-ne v5, v3, :cond_0

    .line 108
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v8, :cond_f

    .line 109
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    if-eqz v5, :cond_0

    .line 112
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicPlayControl:Lcom/touchus/benchilauncher/service/MusicPlayControl;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/MusicPlayControl;->stopMusic()V

    goto/16 :goto_0

    .line 113
    :cond_f
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v9, :cond_10

    .line 114
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-virtual {v5}, Lcom/touchus/benchilauncher/service/BluetoothService;->stopMusic()V

    goto/16 :goto_0

    .line 115
    :cond_10
    iget v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-ne v5, v10, :cond_0

    .line 116
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    invoke-virtual {v5, p1}, Lcn/kuwo/autosdk/api/KWAPI;->isKuwoRunning(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 117
    iget-object v5, v0, Lcom/touchus/benchilauncher/LauncherApplication;->kwapi:Lcn/kuwo/autosdk/api/KWAPI;

    sget-object v6, Lcn/kuwo/autosdk/api/PlayState;->STATE_PAUSE:Lcn/kuwo/autosdk/api/PlayState;

    invoke-virtual {v5, p1, v6}, Lcn/kuwo/autosdk/api/KWAPI;->setPlayState(Landroid/content/Context;Lcn/kuwo/autosdk/api/PlayState;)V

    goto/16 :goto_0
.end method
