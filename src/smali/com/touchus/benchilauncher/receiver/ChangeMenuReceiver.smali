.class public Lcom/touchus/benchilauncher/receiver/ChangeMenuReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ChangeMenuReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 17
    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    .line 19
    .local v0, "app":Lcom/touchus/benchilauncher/LauncherApplication;
    iget-object v4, v0, Lcom/touchus/benchilauncher/LauncherApplication;->launcherHandler:Landroid/os/Handler;

    if-eqz v4, :cond_0

    .line 54
    :goto_0
    return-void

    .line 22
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.unibroad.AudioFocus.PAPAGOGAIN"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 23
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.unibroad.AudioFocus.PAPAGOLOSS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 24
    :cond_1
    const-string v4, "unibroadAudioFocus"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "action = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 27
    :cond_2
    const-string v4, "pos"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28
    .local v3, "type":Ljava/lang/String;
    const/4 v2, 0x0

    .line 29
    .local v2, "pos":I
    const-string v4, "main_navi"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 30
    const/4 v2, 0x0

    .line 51
    :cond_3
    :goto_1
    iget-object v4, v0, Lcom/touchus/benchilauncher/LauncherApplication;->serviceHandler:Landroid/os/Handler;

    .line 52
    const/16 v5, 0x40c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 51
    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 53
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 31
    .end local v1    # "msg":Landroid/os/Message;
    :cond_4
    const-string v4, "main_radio"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 32
    const-string v4, "main_original"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 33
    :cond_5
    const/4 v2, 0x1

    .line 34
    goto :goto_1

    :cond_6
    const-string v4, "main_media"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 35
    const/4 v2, 0x2

    .line 36
    goto :goto_1

    :cond_7
    const-string v4, "main_phone"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 37
    const/4 v2, 0x3

    .line 38
    goto :goto_1

    :cond_8
    const-string v4, "main_voice"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 39
    const/4 v2, 0x5

    .line 40
    goto :goto_1

    :cond_9
    const-string v4, "main_instrument"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 41
    const/4 v2, 0x4

    .line 42
    goto :goto_1

    :cond_a
    const-string v4, "main_recorder"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 43
    const/4 v2, 0x6

    .line 44
    goto :goto_1

    :cond_b
    const-string v4, "main_app"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 45
    const/4 v2, 0x7

    .line 46
    goto :goto_1

    :cond_c
    const-string v4, "main_setting"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 47
    const/16 v2, 0x8

    .line 48
    goto :goto_1

    :cond_d
    const-string v4, "main_close"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 49
    const/16 v2, 0x9

    goto :goto_1
.end method
