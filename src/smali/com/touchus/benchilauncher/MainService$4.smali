.class Lcom/touchus/benchilauncher/MainService$4;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$11(Lcom/touchus/benchilauncher/MainService;)Landroid/widget/FrameLayout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    .line 586
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v1, v1, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 587
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 591
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " removeLoading isAUX = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$14(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 592
    const-string v2, ",getBTA2dpState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 591
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 594
    invoke-static {}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->getInstance()Lcom/touchus/benchilauncher/views/BTConnectDialog;

    move-result-object v0

    .line 595
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 594
    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->showBTConnectView(Landroid/content/Context;)V

    goto :goto_0

    .line 597
    :cond_3
    invoke-static {}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->getInstance()Lcom/touchus/benchilauncher/views/BTConnectDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->dissShow()V

    .line 598
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$4;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$16(Lcom/touchus/benchilauncher/MainService;)V

    goto :goto_0
.end method
