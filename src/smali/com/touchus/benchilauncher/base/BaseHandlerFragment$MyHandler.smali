.class public Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;
.super Landroid/os/Handler;
.source "BaseHandlerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/base/BaseHandlerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyHandler"
.end annotation


# instance fields
.field private target:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/touchus/benchilauncher/base/BaseHandlerFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/touchus/benchilauncher/base/BaseHandlerFragment;)V
    .locals 1
    .param p1, "fragment"    # Lcom/touchus/benchilauncher/base/BaseHandlerFragment;

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->target:Ljava/lang/ref/WeakReference;

    .line 50
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->target:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;->target:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;

    invoke-virtual {v0, p1}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->handlerMsg(Landroid/os/Message;)V

    goto :goto_0
.end method
