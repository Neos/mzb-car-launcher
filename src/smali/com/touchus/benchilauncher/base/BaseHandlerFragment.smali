.class public abstract Lcom/touchus/benchilauncher/base/BaseHandlerFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseHandlerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;
    }
.end annotation


# instance fields
.field public mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 18
    new-instance v0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;-><init>(Lcom/touchus/benchilauncher/base/BaseHandlerFragment;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    .line 17
    return-void
.end method


# virtual methods
.method public abstract handlerMsg(Landroid/os/Message;)V
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 41
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 29
    invoke-static {}, Lcom/touchus/benchilauncher/LauncherApplication;->getInstance()Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 30
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 35
    invoke-static {}, Lcom/touchus/benchilauncher/LauncherApplication;->getInstance()Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/base/BaseHandlerFragment;->mMyHandler:Lcom/touchus/benchilauncher/base/BaseHandlerFragment$MyHandler;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 36
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 24
    return-void
.end method
