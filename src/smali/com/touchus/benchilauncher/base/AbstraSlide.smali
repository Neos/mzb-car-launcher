.class public Lcom/touchus/benchilauncher/base/AbstraSlide;
.super Landroid/view/ViewGroup;
.source "AbstraSlide.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;
    }
.end annotation


# instance fields
.field public mChild:Landroid/view/View;

.field public mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

.field public mScroller:Landroid/widget/Scroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mScroller:Landroid/widget/Scroller;

    .line 20
    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/base/AbstraSlide;->scrollTo(II)V

    .line 70
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/base/AbstraSlide;->invalidate()V

    .line 73
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v3, 0x0

    .line 42
    iget-object v0, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mChild:Landroid/view/View;

    iget-object v1, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mChild:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mChild:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 43
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 26
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/base/AbstraSlide;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mChild:Landroid/view/View;

    .line 27
    iget-object v5, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mChild:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 28
    .local v2, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 29
    .local v3, "width":I
    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 31
    .local v0, "hight":I
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 32
    .local v4, "widthSpec":I
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 34
    .local v1, "hightSpec":I
    iget-object v5, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mChild:Landroid/view/View;

    invoke-virtual {v5, v4, v1}, Landroid/view/View;->measure(II)V

    .line 36
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/touchus/benchilauncher/base/AbstraSlide;->setMeasuredDimension(II)V

    .line 38
    return-void
.end method

.method public setOnSlideListener(Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mOnSlipeListener:Lcom/touchus/benchilauncher/base/AbstraSlide$OnSlipeListener;

    .line 82
    return-void
.end method

.method public smoothScrollTo(III)V
    .locals 6
    .param p1, "endX"    # I
    .param p2, "endY"    # I
    .param p3, "dutation"    # I

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/base/AbstraSlide;->getScrollX()I

    move-result v1

    .line 56
    .local v1, "startX":I
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/base/AbstraSlide;->getScrollY()I

    move-result v2

    .line 57
    .local v2, "startY":I
    sub-int v3, p1, v1

    .line 58
    .local v3, "dx":I
    sub-int v4, p2, v2

    .line 59
    .local v4, "dy":I
    iget-object v0, p0, Lcom/touchus/benchilauncher/base/AbstraSlide;->mScroller:Landroid/widget/Scroller;

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 60
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/base/AbstraSlide;->invalidate()V

    .line 61
    return-void
.end method

.method public smoothScrollXTo(II)V
    .locals 1
    .param p1, "endX"    # I
    .param p2, "duration"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/touchus/benchilauncher/base/AbstraSlide;->smoothScrollTo(III)V

    .line 47
    return-void
.end method

.method public smoothScrollYTo(II)V
    .locals 1
    .param p1, "endY"    # I
    .param p2, "duration"    # I

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/touchus/benchilauncher/base/AbstraSlide;->smoothScrollTo(III)V

    .line 51
    return-void
.end method
