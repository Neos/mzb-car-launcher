.class public Lcom/touchus/benchilauncher/SysConst;
.super Ljava/lang/Object;
.source "SysConst.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/SysConst$Cardoor;,
        Lcom/touchus/benchilauncher/SysConst$DataTpye;
    }
.end annotation


# static fields
.field public static final AGILITY_SETTING:I = 0x1773

.field public static final AIR_INFO:I = 0x3f4

.field public static final AUDIO_SOURCE:I = 0x3e9

.field public static final AUX_ACTIVATE_STUTAS:I = 0x418

.field public static final BACKLIGHT:I = 0x3fb

.field public static final BLUETOOTH_CONNECT:I = 0x3ea

.field public static final BLUETOOTH_PHONE_STATE:I = 0x1772

.field public static final BLUE_TOOTH_ISOPNESTATE:I = 0x1775

.field public static final BT_A2DP_STATUS:I = 0x411

.field public static final BT_HEADSET_STATUS:I = 0x412

.field public static final CALL_FLOAT:I = 0x410

.field public static final CANBOX_INFO_UPDATE:I = 0x467

.field public static final CAR_BASE_INFO:I = 0x3ef

.field public static final CD_MUSIC_PLAY:I = 0x3ed

.field public static final CHANGE_TO_HOME:I = 0x400

.field public static final CLOSE_VOICE_APP:I = 0x40a

.field public static final COPY_ERROR:I = 0x468

.field public static final CURENTRADIO:I = 0x3f3

.field public static DEBUG:Z = false

.field public static DV_CUSTOM:Z = false

.field public static final EVENT_EASYCONN_A2DP_ACQUIRE:Ljava/lang/String; = "net.easyconn.a2dp.acquire"

.field public static final EVENT_EASYCONN_A2DP_ACQUIRE_FAIL:Ljava/lang/String; = "net.easyconn.a2dp.acquire.fail"

.field public static final EVENT_EASYCONN_A2DP_ACQUIRE_OK:Ljava/lang/String; = "net.easyconn.a2dp.acquire.ok"

.field public static final EVENT_EASYCONN_A2DP_RELEASE:Ljava/lang/String; = "net.easyconn.a2dp.release"

.field public static final EVENT_EASYCONN_A2DP_RELEASE_FAIL:Ljava/lang/String; = "net.easyconn.a2dp.release.fail"

.field public static final EVENT_EASYCONN_A2DP_RELEASE_OK:Ljava/lang/String; = "net.easyconn.a2dp.release.ok "

.field public static final EVENT_EASYCONN_APP_QUIT:Ljava/lang/String; = "net.easyconn.app.quit"

.field public static final EVENT_EASYCONN_BT_CHECKSTATUS:Ljava/lang/String; = "net.easyconn.bt.checkstatus"

.field public static final EVENT_EASYCONN_BT_CONNECT:Ljava/lang/String; = "net.easyconn.bt.connect"

.field public static final EVENT_EASYCONN_BT_CONNECTED:Ljava/lang/String; = "net.easyconn.bt.connected"

.field public static final EVENT_EASYCONN_BT_OPENED:Ljava/lang/String; = "net.easyconn.bt.opened"

.field public static final EVENT_EASYCONN_BT_UNCONNECTED:Ljava/lang/String; = "net.easyconn.bt.unconnected"

.field public static final EVENT_MCU_ENTER_STANDBY:Ljava/lang/String; = "com.unibroad.mcu.enterStandbyMode"

.field public static final EVENT_MCU_WAKE_UP:Ljava/lang/String; = "com.unibroad.mcu.wakeUp"

.field public static final EVENT_UNIBROAD_AUDIO_FOCUS:Ljava/lang/String; = "com.unibroad.AudioFocus"

.field public static final EVENT_UNIBROAD_AUDIO_REGAIN:Ljava/lang/String; = "com.unibroad.AudioFocus.REGAIN"

.field public static final EVENT_UNIBROAD_OPEN_ORIGINALCAR:Ljava/lang/String; = "com.unibroad.sys.originalcar"

.field public static final EVENT_UNIBROAD_PAPAGOGAIN:Ljava/lang/String; = "com.unibroad.AudioFocus.PAPAGOGAIN"

.field public static final EVENT_UNIBROAD_PAPAGOLOSS:Ljava/lang/String; = "com.unibroad.AudioFocus.PAPAGOLOSS"

.field public static final EVENT_UNIBROAD_WEATHERDATA:Ljava/lang/String; = "com.unibroad.weatherdata"

.field public static final FEEDBACK_RESULT:I = 0x40f

.field public static final FLAG_AIR_INFO:Ljava/lang/String; = "airInfo"

.field public static final FLAG_AUDIO_SOURCE:Ljava/lang/String; = "audioSource"

.field public static final FLAG_AUX_ACTIVATE_STUTAS:Ljava/lang/String; = "aux_activate_stutas"

.field public static final FLAG_CAR_BASE_INFO:Ljava/lang/String; = "carBaseInfo"

.field public static final FLAG_CONFIG_CONNECT:Ljava/lang/String; = "connectType"

.field public static final FLAG_CONFIG_NAVI:Ljava/lang/String; = "naviExist"

.field public static final FLAG_CONFIG_RADIO:Ljava/lang/String; = "radioType"

.field public static final FLAG_CONFIG_SCREEN:Ljava/lang/String; = "screenType"

.field public static final FLAG_CONFIG_USB_NUM:Ljava/lang/String; = "usbNum"

.field public static final FLAG_LANGUAGE_TYPE:Ljava/lang/String; = "languageType"

.field public static final FLAG_MUSIC_LOOP:Ljava/lang/String; = "musicloop"

.field public static final FLAG_MUSIC_POS:Ljava/lang/String; = "musicPos"

.field public static final FLAG_REVERSING_TYPE:Ljava/lang/String; = "reversingType"

.field public static final FLAG_REVERSING_VOICE:Ljava/lang/String; = "voiceType"

.field public static final FLAG_RUNNING_CURRSPEED:Ljava/lang/String; = "flag_running_currspeed"

.field public static final FLAG_RUNNING_STATE:Ljava/lang/String; = "runningState"

.field public static final FLAG_RUNNING_ZHUANSU:Ljava/lang/String; = "flag_running_zhuansu"

.field public static final FLAG_UPGRADE_PER:Ljava/lang/String; = "upgradePer"

.field public static final FLAG_USB_LISTENER:Ljava/lang/String; = "usblistener"

.field public static final FLAG_VIDEO_POS:Ljava/lang/String; = "videoPos"

.field public static final FORCE_MAIN_VIEW:I = 0x406

.field public static final GET_CURRENT_CHANNEL:I = 0x40d

.field public static final GOTO_BT_VIEW:I = 0x3f5

.field public static final GPS_STATUS:I = 0x40e

.field public static final HIDE_ORIGINAL_VIEW:I = 0x40b

.field public static final IDRIVER_ENUM:Ljava/lang/String; = "idriver_enum"

.field public static final IDRIVER_STATE_ENUM:Ljava/lang/String; = "idriver_state_enum"

.field public static final IDRVIER_STATE:I = 0x1771

.field public static final IS_CILK_SHIZI:Ljava/lang/String; = "isCilkshizi"

.field public static final LISTITEM_UPDATE:I = 0x407

.field public static final MCU_SHOW_UPGRADE_DIALOG:I = 0x46c

.field public static final MCU_UPGRADE_CUR_DATA:I = 0x46d

.field public static final MCU_UPGRADE_FINISH:I = 0x46b

.field public static final MCU_UPGRADE_HEADER:I = 0x46a

.field public static final MENU_BIG_ICON:[I

.field public static final MENU_LIST:[I

.field public static final MENU_SMALL_ICON:[I

.field public static final MUSIC_PLAY:I = 0x3ec

.field public static final ONLINE_CONFIG_DATA:I = 0x464

.field public static final ONLINE_CONFIG_FINISH:I = 0x466

.field public static final ONLINE_CONFIG_VALID:I = 0x465

.field public static final ORIGINAL_FLAG:I = 0x414

.field public static final PRESS_NEXT:I = 0x3f6

.field public static final RADAR_BACK:I = 0x3f8

.field public static final RADAR_FRONT:I = 0x3f7

.field public static final RADIOLIST:I = 0x401

.field public static final REVERSING:I = 0x3fa

.field public static final REVERSING_FINISH:I = 0x402

.field public static final RUNNING_STATE:I = 0x3f0

.field public static final SHOW_LOADING_VIEW:I = 0x7d1

.field public static final SHOW_UPGRADE_DIALOG:I = 0x462

.field public static final SP_SETTING_TIME:Ljava/lang/String; = "sp_setting_time"

.field public static final SP_SETTING_YEAR:Ljava/lang/String; = "sp_setting_year"

.field public static final START_VOICE_APP:I = 0x409

.field public static final SYSTEM_SETTING:I = 0x3eb

.field public static final TIME_CANDLER:I = 0x3f1

.field public static final UPDATE_CUR_MUSIC_PLAY_INFO:I = 0x1b59

.field public static final UPDATE_MUSIC_PLAY_STATE:I = 0x1b5b

.field public static final UPDATE_NAVI:I = 0x415

.field public static final UPDATE_NAVI_END:I = 0x417

.field public static final UPDATE_NAVI_START:I = 0x416

.field public static final UPDATE_TIME_AUTO:I = 0x7d3

.field public static final UPDATE_WEATHER:I = 0x3f2

.field public static final UPGRADE_CHECK_FINISH:I = 0x457

.field public static final UPGRADE_COPY_PER:I = 0x458

.field public static final UPGRADE_CUR_DATA:I = 0x463

.field public static final UPGRADE_ERROR:I = 0x469

.field public static final UPGRADE_FINISH:I = 0x461

.field public static final UPGRADE_HEADER:I = 0x460

.field public static final UPGRADE_UPGRADE_PER:I = 0x459

.field public static final USB_CD_MUSIC_PLAY:I = 0x3ee

.field public static final USB_LISTENER:I = 0x1b5a

.field public static final VOICE_WAKEUP_MENU:I = 0x40c

.field public static final WHEEL_TURN:I = 0x3f9

.field public static final YOUHAO_STATE:I = 0x1774

.field public static basicNum:I

.field public static callVoice:Ljava/lang/String;

.field public static callnum:[I

.field public static dvVoice:Ljava/lang/String;

.field public static mediaBasicNum:I

.field public static mediaVoice:Ljava/lang/String;

.field public static mixPro:Ljava/lang/String;

.field public static musicDecVolume:F

.field public static musicNorVolume:F

.field public static naviVoice:Ljava/lang/String;

.field public static num:[I

.field public static storeData:[B


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x9

    const/4 v5, 0x6

    const/4 v4, 0x3

    const/16 v3, 0xa

    .line 6
    sput-boolean v0, Lcom/touchus/benchilauncher/SysConst;->DEBUG:Z

    .line 7
    sput-boolean v0, Lcom/touchus/benchilauncher/SysConst;->DV_CUSTOM:Z

    .line 50
    const v0, 0x3e99999a    # 0.3f

    sput v0, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/touchus/benchilauncher/SysConst;->musicNorVolume:F

    .line 53
    const/16 v0, 0x17

    sput v0, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 54
    sput v3, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    .line 55
    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v1, 0x1

    aput v4, v0, v1

    const/4 v1, 0x2

    aput v5, v0, v1

    aput v6, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0xc

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xf

    aput v2, v0, v1

    const/16 v1, 0x12

    aput v1, v0, v5

    const/4 v1, 0x7

    const/16 v2, 0x15

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x18

    aput v2, v0, v1

    const/16 v1, 0x1b

    aput v1, v0, v6

    const/16 v1, 0x1e

    aput v1, v0, v3

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->num:[I

    .line 56
    const/16 v0, 0xb

    new-array v0, v0, [I

    const/4 v1, 0x1

    aput v4, v0, v1

    const/4 v1, 0x2

    aput v5, v0, v1

    aput v6, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0xc

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xf

    aput v2, v0, v1

    const/16 v1, 0x12

    aput v1, v0, v5

    const/4 v1, 0x7

    const/16 v2, 0x15

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x18

    aput v2, v0, v1

    const/16 v1, 0x1b

    aput v1, v0, v6

    const/16 v1, 0x1e

    aput v1, v0, v3

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->callnum:[I

    .line 58
    const-string v0, "mixpro"

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->mixPro:Ljava/lang/String;

    .line 59
    const-string v0, "naviVoice"

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->naviVoice:Ljava/lang/String;

    .line 60
    const-string v0, "mediaVoice"

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->mediaVoice:Ljava/lang/String;

    .line 61
    const-string v0, "dvVoice"

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->dvVoice:Ljava/lang/String;

    .line 62
    const-string v0, "callVoice"

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->callVoice:Ljava/lang/String;

    .line 252
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->MENU_LIST:[I

    .line 258
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->MENU_BIG_ICON:[I

    .line 264
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->MENU_SMALL_ICON:[I

    .line 285
    new-array v0, v3, [B

    sput-object v0, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    .line 286
    return-void

    .line 252
    :array_0
    .array-data 4
        0x7f07004e
        0x7f07001a
        0x7f070050
        0x7f07004f
        0x7f070056
        0x7f070052
        0x7f070053
        0x7f070054
        0x7f070055
        0x7f07005c
    .end array-data

    .line 258
    :array_1
    .array-data 4
        0x7f020145
        0x7f02013f
        0x7f02013b
        0x7f02013d
        0x7f0201fc
        0x7f020187
        0x7f02010a
        0x7f020131
        0x7f020141
        0x7f0200d1
    .end array-data

    .line 264
    :array_2
    .array-data 4
        0x7f020146
        0x7f020140
        0x7f02013c
        0x7f02013e
        0x7f0201fd
        0x7f020188
        0x7f02010b
        0x7f020132
        0x7f020142
        0x7f0200d2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isBT()Z
    .locals 1

    .prologue
    .line 247
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    return v0
.end method
