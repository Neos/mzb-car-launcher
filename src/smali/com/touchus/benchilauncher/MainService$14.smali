.class Lcom/touchus/benchilauncher/MainService$14;
.super Landroid/content/BroadcastReceiver;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1602
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1606
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    .line 1607
    .local v14, "action":Ljava/lang/String;
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "action = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1608
    const-string v2, "pkgName"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 1609
    .local v19, "pkgName":Ljava/lang/String;
    const-string v2, "durationHint"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 1610
    .local v15, "durationHint":I
    const-string v2, "status"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    .line 1611
    .local v20, "status":I
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.unibroad.AudioFocus.PAPAGO"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1612
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1794
    :goto_0
    return-void

    .line 1616
    :cond_0
    const/16 v17, 0x0

    .line 1618
    .local v17, "iOpen":Z
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->mediaVoice:Ljava/lang/String;

    .line 1619
    const/4 v6, 0x5

    .line 1618
    invoke-virtual {v3, v5, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    aget v18, v2, v3

    .line 1620
    .local v18, "mediaVolume":I
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->dvVoice:Ljava/lang/String;

    const/4 v6, 0x5

    invoke-virtual {v3, v5, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    aget v7, v2, v3

    .line 1621
    .local v7, "dvVolume":I
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    .line 1622
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->naviVoice:Ljava/lang/String;

    const/4 v6, 0x3

    invoke-virtual {v3, v5, v6}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 1621
    aget v4, v2, v3

    .line 1625
    .local v4, "naviVolume":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$14(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$25(Lcom/touchus/benchilauncher/MainService;)Ljava/util/List;

    move-result-object v2

    .line 1626
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1627
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/MainService;->access$30(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 1629
    :cond_1
    const-string v2, "com.unibroad.AudioFocus.PAPAGOGAIN"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1630
    const/16 v17, 0x1

    .line 1631
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v2

    .line 1632
    sget v3, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1631
    invoke-virtual/range {v2 .. v7}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1633
    .end local v7    # "dvVolume":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1636
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    .line 1790
    :cond_2
    :goto_1
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "openOrCloseRelay: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1791
    const-string v5, ",isCalling : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget-boolean v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1790
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/touchus/benchilauncher/MainService;->access$33(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 1793
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/touchus/benchilauncher/MainService;->access$31(Lcom/touchus/benchilauncher/MainService;Z)V

    goto/16 :goto_0

    .line 1638
    .restart local v7    # "dvVolume":I
    :cond_3
    const-string v2, "com.unibroad.AudioFocus.PAPAGOLOSS"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1639
    const/16 v17, 0x0

    .line 1640
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1642
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v2

    .line 1643
    sget v3, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1642
    invoke-virtual/range {v2 .. v7}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1645
    .end local v7    # "dvVolume":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1646
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1647
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$16(Lcom/touchus/benchilauncher/MainService;)V

    goto :goto_1

    .line 1649
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto :goto_1

    .line 1651
    .restart local v7    # "dvVolume":I
    :cond_6
    const-string v2, "com.unibroad.AudioFocus.REGAIN"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1652
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "AUDIO_REGAIN pkgName:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1653
    const-string v5, " , openOrCloseRelay: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ",isCalling : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1654
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget-boolean v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ",app.musicType = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1652
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1655
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    if-eqz v2, :cond_7

    .line 1656
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v5

    sget v6, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 1657
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 1656
    invoke-virtual/range {v5 .. v10}, Lcom/backaudio/android/driver/Mainboard;->setDVSoundValue(IIIII)V

    .line 1658
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, " DV openOrCloseRelay: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1659
    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1658
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1660
    const/16 v17, 0x1

    .line 1661
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/touchus/benchilauncher/MainService;->access$31(Lcom/touchus/benchilauncher/MainService;Z)V

    goto/16 :goto_0

    .line 1665
    :cond_7
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1667
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1668
    sget v9, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v10, v4

    .line 1667
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1694
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/MainService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-nez v2, :cond_a

    .line 1697
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_1

    .line 1671
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v2, :cond_9

    .line 1672
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1674
    sget v2, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    div-int/lit8 v9, v2, 0x2

    .line 1675
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v10, v4

    .line 1673
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1677
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1682
    :cond_9
    :goto_3
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1683
    sget v2, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    div-int/lit8 v9, v2, 0x2

    const/4 v11, 0x0

    .line 1684
    div-int/lit8 v12, v18, 0x2

    div-int/lit8 v13, v18, 0x2

    move v10, v4

    .line 1682
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1686
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1690
    :goto_4
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1691
    sget v9, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    const/4 v11, 0x0

    move v10, v4

    move/from16 v12, v18

    move/from16 v13, v18

    .line 1690
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    goto :goto_2

    .line 1678
    :catch_0
    move-exception v16

    .line 1679
    .local v16, "e":Ljava/lang/InterruptedException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 1687
    .end local v16    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v16

    .line 1688
    .restart local v16    # "e":Ljava/lang/InterruptedException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4

    .line 1698
    .end local v16    # "e":Ljava/lang/InterruptedException;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1699
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$16(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_1

    .line 1701
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_1

    .line 1705
    :cond_c
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "requestAudiofocus action : "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1706
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " audiofocus pkgName:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1707
    const-string v5, " , durationHint: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ",status : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1708
    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", app.isCalling : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget-boolean v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1709
    const-string v5, ",app.musicType = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1705
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1711
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->isCalling:Z

    if-eqz v2, :cond_d

    .line 1712
    const/16 v17, 0x1

    .line 1714
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1715
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_1

    .line 1716
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1717
    const-string v2, "com.amap.navi.demo"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1718
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/LauncherApplication;->getNaviAPP()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1719
    invoke-static/range {v19 .. v19}, Lcom/touchus/publicutils/utils/APPSettings;->isNavgation(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1721
    :cond_e
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1722
    const/16 v17, 0x1

    .line 1723
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1724
    sget v9, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v10, v4

    .line 1723
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1745
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1746
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1748
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_1

    .line 1726
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v2, :cond_10

    .line 1727
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1728
    sget v9, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    const/4 v11, 0x0

    move v10, v4

    move/from16 v12, v18

    move/from16 v13, v18

    .line 1727
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    goto :goto_5

    .line 1731
    :cond_10
    const/16 v17, 0x1

    .line 1732
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    .line 1733
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    const/4 v9, 0x0

    .line 1734
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v10, v4

    .line 1733
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1736
    const-wide/16 v2, 0x64

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1740
    :goto_6
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1741
    sget v9, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v10, v4

    .line 1740
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    goto :goto_5

    .line 1737
    :catch_2
    move-exception v16

    .line 1738
    .restart local v16    # "e":Ljava/lang/InterruptedException;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_6

    .line 1750
    .end local v16    # "e":Ljava/lang/InterruptedException;
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/MainService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1751
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    if-nez v2, :cond_12

    .line 1753
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_1

    .line 1755
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/MainService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 1756
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x5

    iput v3, v2, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    .line 1758
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    if-eqz v2, :cond_14

    .line 1759
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v5

    .line 1760
    sget v6, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 1759
    invoke-virtual/range {v5 .. v10}, Lcom/backaudio/android/driver/Mainboard;->setDVSoundValue(IIIII)V

    .line 1761
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, " DV openOrCloseRelay: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1762
    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1761
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1763
    const/16 v17, 0x1

    .line 1764
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/touchus/benchilauncher/MainService;->access$31(Lcom/touchus/benchilauncher/MainService;Z)V

    goto/16 :goto_0

    .line 1768
    :cond_14
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1770
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1771
    sget v9, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v10, v4

    .line 1770
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 1778
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1779
    const-string v2, "unibroadAudioFocus"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "iAUX = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$14(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1780
    const-string v5, ",app.musicType = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    iget v5, v5, Lcom/touchus/benchilauncher/LauncherApplication;->musicType:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1779
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1781
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$32(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1782
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$16(Lcom/touchus/benchilauncher/MainService;)V

    .line 1783
    const-string v2, "unibroadAudioFocus"

    const-string v3, "MusicConnect"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1774
    :cond_15
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v8

    .line 1775
    sget v9, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    const/4 v11, 0x0

    move v10, v4

    move/from16 v12, v18

    move/from16 v13, v18

    .line 1774
    invoke-virtual/range {v8 .. v13}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    goto :goto_7

    .line 1785
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/touchus/benchilauncher/MainService$14;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v2}, Lcom/touchus/benchilauncher/MainService;->btMusicConnect()V

    .line 1786
    const-string v2, "unibroadAudioFocus"

    const-string v3, "MusicUnConnect"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
