.class Lcom/touchus/benchilauncher/MainService$2;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const v7, 0x7f0700a2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 224
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1, v5}, Lcom/touchus/benchilauncher/MainService;->access$3(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 225
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1, v6}, Lcom/touchus/benchilauncher/MainService;->access$4(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 226
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getLanguage()V

    .line 228
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    .line 229
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v4, 0x7f0700a1

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 228
    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    .line 230
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    .line 231
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v3, v7}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 230
    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 232
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    const-string v3, "test"

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    .line 234
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    const-string v3, "radioType"

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    .line 235
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    const-string v3, "naviExist"

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    .line 236
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    const-string v3, "usbNum"

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    .line 237
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    const-string v3, "connectType"

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 238
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v2

    const-string v3, "screenType"

    invoke-virtual {v2, v3, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    .line 240
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "app.radioPos = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",app.naviPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 241
    const-string v3, ",app.usbPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",app.connectPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 242
    const-string v3, ",app.screenPos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 240
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    const-string v2, "voiceType"

    invoke-virtual {v1, v2, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 245
    .local v0, "voicesetPos":I
    if-ne v0, v6, :cond_4

    .line 246
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 252
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    if-nez v1, :cond_5

    .line 253
    sput-boolean v6, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    .line 257
    :goto_1
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$7(Lcom/touchus/benchilauncher/MainService;)V

    .line 260
    :cond_0
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 261
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput-boolean v5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 262
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v2, v7}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 263
    const v1, 0x3e99999a    # 0.3f

    sput v1, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    .line 265
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-eqz v1, :cond_2

    .line 266
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    .line 267
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->mixPro:Ljava/lang/String;

    const/4 v3, 0x5

    .line 266
    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v1

    int-to-double v2, v1

    .line 267
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 266
    div-double/2addr v2, v4

    double-to-float v1, v2

    sput v1, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    .line 269
    :cond_2
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 270
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$8(Lcom/touchus/benchilauncher/MainService;)V

    .line 273
    :cond_3
    return-void

    .line 248
    :cond_4
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$2;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    iput-object v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    goto :goto_0

    .line 255
    :cond_5
    sput-boolean v5, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    goto :goto_1
.end method
