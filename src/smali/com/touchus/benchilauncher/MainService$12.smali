.class Lcom/touchus/benchilauncher/MainService$12;
.super Landroid/content/BroadcastReceiver;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1218
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/MainService$12;)Lcom/touchus/benchilauncher/MainService;
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0xfa0

    const/4 v8, 0x0

    .line 1222
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1223
    .local v0, "aString":Ljava/lang/String;
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "btstate Action = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1224
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1378
    :cond_0
    :goto_0
    return-void

    .line 1227
    :cond_1
    const-string v5, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1229
    const-string v5, "android.bluetooth.profile.extra.STATE"

    .line 1228
    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1230
    .local v1, "btA2dpStatus":I
    packed-switch v1, :pswitch_data_0

    .line 1262
    :cond_2
    :goto_1
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 1263
    .local v4, "message":Landroid/os/Message;
    const/16 v5, 0x411

    iput v5, v4, Landroid/os/Message;->what:I

    .line 1264
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1265
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/touchus/benchilauncher/MainService$Mhandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1232
    .end local v4    # "message":Landroid/os/Message;
    :pswitch_0
    const-string v5, ""

    const-string v6, "btstate a2dp STATE_DISCONNECTED"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5, v8}, Lcom/touchus/benchilauncher/MainService;->access$20(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 1234
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto :goto_1

    .line 1237
    :pswitch_1
    const-string v5, ""

    const-string v6, "btstate a2dp STATE_CONNECTING"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5, v8}, Lcom/touchus/benchilauncher/MainService;->access$20(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 1239
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    .line 1240
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1241
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    new-instance v6, Lcom/touchus/benchilauncher/MainService$12$1;

    invoke-direct {v6, p0}, Lcom/touchus/benchilauncher/MainService$12$1;-><init>(Lcom/touchus/benchilauncher/MainService$12;)V

    invoke-virtual {v5, v6, v10, v11}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 1251
    :pswitch_2
    const-string v5, ""

    const-string v6, "btstate a2dp STATE_CONNECTED"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1252
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1253
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v6, v6, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Lcom/touchus/benchilauncher/MainService$Mhandler;->post(Ljava/lang/Runnable;)Z

    .line 1255
    :cond_3
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/touchus/benchilauncher/MainService;->access$20(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 1256
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$16(Lcom/touchus/benchilauncher/MainService;)V

    goto :goto_1

    .line 1266
    .end local v1    # "btA2dpStatus":I
    :cond_4
    const-string v5, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    .line 1267
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1269
    const-string v5, "android.bluetooth.profile.extra.STATE"

    .line 1268
    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1270
    .local v2, "btHeadsetStatus":I
    packed-switch v2, :pswitch_data_1

    .line 1284
    :goto_2
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 1285
    .restart local v4    # "message":Landroid/os/Message;
    const/16 v5, 0x412

    iput v5, v4, Landroid/os/Message;->what:I

    .line 1286
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1287
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/touchus/benchilauncher/MainService$Mhandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1272
    .end local v4    # "message":Landroid/os/Message;
    :pswitch_3
    const-string v5, ""

    const-string v6, "btstate headset STATE_DISCONNECTED"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1275
    :pswitch_4
    const-string v5, ""

    const-string v6, "btstate headset STATE_CONNECTING"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1279
    :pswitch_5
    const-string v5, ""

    const-string v6, "btstate headset STATE_CONNECTED"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1288
    .end local v2    # "btHeadsetStatus":I
    :cond_5
    const-string v5, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1290
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 1291
    .local v3, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, ""

    .line 1292
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "btstate ACTION_FOUND device.getName = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1293
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1292
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1291
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1294
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$22(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1295
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5, v3}, Lcom/touchus/benchilauncher/MainService;->access$23(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothDevice;)V

    .line 1296
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v5, v5, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    goto/16 :goto_0

    .line 1298
    .end local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_6
    const-string v5, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    .line 1299
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1300
    const-string v5, ""

    const-string v6, "btstate ACTION_DISCOVERY_FINISHED"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1301
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1302
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    goto/16 :goto_0

    .line 1304
    :cond_7
    const-string v5, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    .line 1305
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1307
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 1308
    .restart local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v3, :cond_8

    .line 1309
    const-string v5, ""

    const-string v6, "btstate bond failed "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1311
    :cond_8
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$22(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1312
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$25(Lcom/touchus/benchilauncher/MainService;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1313
    :cond_9
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5, v3}, Lcom/touchus/benchilauncher/MainService;->access$23(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothDevice;)V

    .line 1314
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "btstate bond state:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto/16 :goto_0

    .line 1333
    :pswitch_6
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1334
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    new-instance v6, Lcom/touchus/benchilauncher/MainService$12$3;

    invoke-direct {v6, p0}, Lcom/touchus/benchilauncher/MainService$12$3;-><init>(Lcom/touchus/benchilauncher/MainService$12;)V

    invoke-virtual {v5, v6, v10, v11}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1317
    :pswitch_7
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$26(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothHeadset;

    move-result-object v5

    invoke-virtual {v5, v3, v8}, Landroid/bluetooth/BluetoothHeadset;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    .line 1319
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v5, v5, Lcom/touchus/benchilauncher/MainService;->iExist:Z

    if-nez v5, :cond_0

    .line 1320
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    new-instance v6, Lcom/touchus/benchilauncher/MainService$12$2;

    invoke-direct {v6, p0}, Lcom/touchus/benchilauncher/MainService$12$2;-><init>(Lcom/touchus/benchilauncher/MainService$12;)V

    .line 1329
    const-wide/16 v8, 0x7d0

    .line 1320
    invoke-virtual {v5, v6, v8, v9}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1353
    .end local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_a
    const-string v5, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1355
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    .line 1356
    .restart local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, ""

    const-string v6, "btstate pairing request"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    if-nez v3, :cond_b

    .line 1358
    const-string v5, ""

    const-string v6, "pairing failed "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1360
    :cond_b
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$22(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 1361
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$25(Lcom/touchus/benchilauncher/MainService;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1362
    :cond_c
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$12;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v5

    new-instance v6, Lcom/touchus/benchilauncher/MainService$12$4;

    invoke-direct {v6, p0}, Lcom/touchus/benchilauncher/MainService$12$4;-><init>(Lcom/touchus/benchilauncher/MainService$12;)V

    .line 1368
    const-wide/16 v8, 0x5dc

    .line 1362
    invoke-virtual {v5, v6, v8, v9}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1374
    .end local v3    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_d
    const-string v5, "android.bluetooth.device.action.PAIRING_CANCEL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1375
    const-string v5, ""

    const-string v6, "btstate pairing cancel"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1230
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1270
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1315
    :pswitch_data_2
    .packed-switch 0xb
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
