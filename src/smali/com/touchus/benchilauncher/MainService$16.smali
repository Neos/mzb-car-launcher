.class Lcom/touchus/benchilauncher/MainService$16;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/MainService;->initMcuAndCanbox()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$16;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 189
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    .line 190
    new-instance v1, Lcom/touchus/benchilauncher/MainService$MainListenner;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$16;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-direct {v1, v2}, Lcom/touchus/benchilauncher/MainService$MainListenner;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    .line 189
    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->setMainboardEventLisenner(Lcom/backaudio/android/driver/IMainboardEventLisenner;)V

    .line 191
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->readyToWork()V

    .line 192
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->getMCUVersionNumber()V

    .line 193
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->getReverseSetFromMcu()V

    .line 194
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->getStoreDataFromMcu()V

    .line 195
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->getReverseMediaSetFromMcu()V

    .line 196
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->getBenzType()V

    .line 197
    const-string v0, ""

    .line 198
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "listenlog QUERY CarLayer = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$16;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-wide v4, v4, Lcom/touchus/benchilauncher/MainService;->startTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$16;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/touchus/benchilauncher/MainService;->startTime:J

    .line 201
    return-void
.end method
