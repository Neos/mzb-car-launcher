.class Lcom/touchus/benchilauncher/MainService$15;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Landroid/location/GpsStatus$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$15;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGpsStatusChanged(I)V
    .locals 10
    .param p1, "arg0"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1817
    const/4 v5, 0x2

    if-ne p1, v5, :cond_2

    .line 1818
    sget-boolean v5, Lcom/touchus/benchilauncher/LauncherApplication;->isGPSLocation:Z

    if-eqz v5, :cond_0

    .line 1819
    sput-boolean v6, Lcom/touchus/benchilauncher/LauncherApplication;->isGPSLocation:Z

    .line 1821
    :cond_0
    sget-boolean v5, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    if-nez v5, :cond_1

    .line 1822
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$15;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5, v7}, Lcom/touchus/benchilauncher/MainService;->access$4(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 1846
    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$15;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v5

    const/16 v6, 0x40e

    invoke-virtual {v5, v6, v9}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 1847
    return-void

    .line 1824
    :cond_2
    if-ne p1, v8, :cond_1

    .line 1825
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService$15;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v5}, Lcom/touchus/benchilauncher/MainService;->access$34(Lcom/touchus/benchilauncher/MainService;)Landroid/location/LocationManager;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v1

    .line 1827
    .local v1, "gpsStatus":Landroid/location/GpsStatus;
    invoke-virtual {v1}, Landroid/location/GpsStatus;->getMaxSatellites()I

    move-result v3

    .line 1829
    .local v3, "maxSatellites":I
    invoke-virtual {v1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v5

    .line 1830
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1831
    .local v2, "iters":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/location/GpsSatellite;>;"
    const/4 v0, 0x0

    .line 1832
    .local v0, "count":I
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    if-le v0, v3, :cond_5

    .line 1840
    :cond_4
    if-le v0, v8, :cond_6

    .line 1841
    sput-boolean v7, Lcom/touchus/benchilauncher/LauncherApplication;->isGPSLocation:Z

    goto :goto_0

    .line 1833
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/location/GpsSatellite;

    .line 1834
    .local v4, "s":Landroid/location/GpsSatellite;
    invoke-virtual {v4}, Landroid/location/GpsSatellite;->usedInFix()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1836
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1843
    .end local v4    # "s":Landroid/location/GpsSatellite;
    :cond_6
    sput-boolean v6, Lcom/touchus/benchilauncher/LauncherApplication;->isGPSLocation:Z

    goto :goto_0
.end method
