.class Lcom/touchus/benchilauncher/MainService$11;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 1132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1136
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/MainService;->checkNavigationRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v2, v4, v5}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1137
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    if-eqz v1, :cond_0

    .line 1157
    :goto_0
    return-void

    .line 1140
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$19(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v0

    .line 1141
    .local v0, "topPkg":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/MainService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1142
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v2, 0x7f070003

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1143
    const-string v1, "com.unibroad.notifyreceiverservice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1144
    const-string v1, "com.unibroad.benzuserguide"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1145
    const-string v1, "com.touchus.amaplocation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1146
    const-string v1, "com.android.packageinstaller"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1147
    const-string v1, "com.papago.s1OBU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1148
    const-string v1, "com.tima.carnet.vt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1149
    const-string v1, "cld.navi.kgomap"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1150
    const-string v1, "com.baidu.navi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1151
    const-string v1, "net.easyconn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1152
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendHideOrShowNavigationBarEvent(Z)V

    .line 1153
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/MainService;->createDeskFloat()V

    goto :goto_0

    .line 1155
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$11;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendHideOrShowNavigationBarEvent(Z)V

    goto/16 :goto_0
.end method
