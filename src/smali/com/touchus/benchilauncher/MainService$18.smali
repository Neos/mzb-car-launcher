.class Lcom/touchus/benchilauncher/MainService$18;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/touchus/benchilauncher/MainService;->enterIntoUpgradeMcuCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 348
    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v11, 0x7f070009

    invoke-virtual {v10, v11}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 349
    .local v8, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 350
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    .line 390
    :goto_0
    return-void

    .line 353
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    .line 354
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    long-to-int v10, v10

    .line 353
    invoke-direct {v0, v10}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 355
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    const/4 v5, 0x0

    .line 357
    .local v5, "in":Ljava/io/BufferedInputStream;
    :try_start_0
    new-instance v6, Ljava/io/BufferedInputStream;

    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v10}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    .end local v5    # "in":Ljava/io/BufferedInputStream;
    .local v6, "in":Ljava/io/BufferedInputStream;
    const/16 v1, 0x400

    .line 359
    .local v1, "buf_size":I
    :try_start_1
    new-array v2, v1, [B

    .line 360
    .local v2, "buffer":[B
    const/4 v7, 0x0

    .line 361
    .local v7, "len":I
    const-wide/16 v10, 0x2800

    invoke-virtual {v6, v10, v11}, Ljava/io/BufferedInputStream;->skip(J)J

    .line 362
    :goto_1
    const/4 v10, -0x1

    const/4 v11, 0x0

    invoke-virtual {v6, v2, v11, v1}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    if-ne v10, v7, :cond_1

    .line 365
    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v11

    iput-object v11, v10, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 370
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 375
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    move-object v5, v6

    .line 380
    .end local v1    # "buf_size":I
    .end local v2    # "buffer":[B
    .end local v6    # "in":Ljava/io/BufferedInputStream;
    .end local v7    # "len":I
    .restart local v5    # "in":Ljava/io/BufferedInputStream;
    :goto_3
    const-string v10, "driverlog"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "mcu:upgradeByteData.length:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 381
    iget-object v12, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v12, v12, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    array-length v12, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 380
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v11, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v11, v11, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    array-length v11, v11

    add-int/lit8 v11, v11, 0x7f

    div-int/lit16 v11, v11, 0x80

    invoke-static {v10, v11}, Lcom/touchus/benchilauncher/MainService;->access$46(Lcom/touchus/benchilauncher/MainService;I)V

    .line 385
    const-string v10, "driverlog"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "mcu:totalDataIndex:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v12}, Lcom/touchus/benchilauncher/MainService;->access$38(Lcom/touchus/benchilauncher/MainService;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 387
    .local v9, "tempBuffer":Ljava/io/ByteArrayOutputStream;
    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService$18;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v10, v10, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    const/4 v11, 0x4

    invoke-virtual {v9, v10, v13, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 388
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v10

    .line 389
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v11

    .line 388
    invoke-virtual {v10, v11}, Lcom/backaudio/android/driver/Mainboard;->requestUpgradeMcu([B)V

    goto/16 :goto_0

    .line 363
    .end local v5    # "in":Ljava/io/BufferedInputStream;
    .end local v9    # "tempBuffer":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buf_size":I
    .restart local v2    # "buffer":[B
    .restart local v6    # "in":Ljava/io/BufferedInputStream;
    .restart local v7    # "len":I
    :cond_1
    const/4 v10, 0x0

    :try_start_4
    invoke-virtual {v0, v2, v10, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 366
    .end local v2    # "buffer":[B
    .end local v7    # "len":I
    :catch_0
    move-exception v3

    move-object v5, v6

    .line 367
    .end local v1    # "buf_size":I
    .end local v6    # "in":Ljava/io/BufferedInputStream;
    .local v3, "e":Ljava/io/IOException;
    .restart local v5    # "in":Ljava/io/BufferedInputStream;
    :goto_4
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 370
    :try_start_6
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 375
    :goto_5
    :try_start_7
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    .line 376
    :catch_1
    move-exception v3

    .line 377
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 371
    :catch_2
    move-exception v3

    .line 372
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 368
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 370
    :goto_6
    :try_start_8
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 375
    :goto_7
    :try_start_9
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 379
    :goto_8
    throw v10

    .line 371
    :catch_3
    move-exception v3

    .line 372
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 376
    .end local v3    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 377
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 371
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "in":Ljava/io/BufferedInputStream;
    .restart local v1    # "buf_size":I
    .restart local v2    # "buffer":[B
    .restart local v6    # "in":Ljava/io/BufferedInputStream;
    .restart local v7    # "len":I
    :catch_5
    move-exception v3

    .line 372
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 376
    .end local v3    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v3

    .line 377
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "in":Ljava/io/BufferedInputStream;
    .restart local v5    # "in":Ljava/io/BufferedInputStream;
    goto/16 :goto_3

    .line 368
    .end local v2    # "buffer":[B
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "in":Ljava/io/BufferedInputStream;
    .end local v7    # "len":I
    .restart local v6    # "in":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "in":Ljava/io/BufferedInputStream;
    .restart local v5    # "in":Ljava/io/BufferedInputStream;
    goto :goto_6

    .line 366
    .end local v1    # "buf_size":I
    :catch_7
    move-exception v3

    goto :goto_4
.end method
