.class public Lcom/touchus/benchilauncher/utils/QuotedPrintable;
.super Ljava/lang/Object;
.source "QuotedPrintable.java"


# static fields
.field private static final CR:B = 0xdt

.field private static final EQUALS:B = 0x3dt

.field private static final LF:B = 0xat

.field private static final LIT_END:B = 0x7et

.field private static final LIT_START:B = 0x21t

.field private static final MAX_LINE_LENGTH:I = 0x4c

.field private static final TAB:B = 0x9t

.field private static mCurrentLineLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->mCurrentLineLength:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkLineLength(ILjava/lang/StringBuilder;)V
    .locals 2
    .param p0, "required"    # I
    .param p1, "out"    # Ljava/lang/StringBuilder;

    .prologue
    .line 154
    sget v0, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->mCurrentLineLength:I

    add-int/2addr v0, p0

    const/16 v1, 0x4b

    if-le v0, v1, :cond_0

    .line 155
    const-string v0, "=/r/n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    sput p0, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->mCurrentLineLength:I

    .line 159
    :goto_0
    return-void

    .line 158
    :cond_0
    sget v0, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->mCurrentLineLength:I

    add-int/2addr v0, p0

    sput v0, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->mCurrentLineLength:I

    goto :goto_0
.end method

.method public static decode([B)I
    .locals 9
    .param p0, "qp"    # [B

    .prologue
    const/16 v8, 0xd

    const/16 v7, 0xa

    .line 30
    array-length v1, p0

    .line 31
    .local v1, "qplen":I
    const/4 v2, 0x0

    .line 33
    .local v2, "retlen":I
    const/4 v0, 0x0

    .local v0, "i":I
    move v3, v2

    .end local v2    # "retlen":I
    .local v3, "retlen":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 70
    return v3

    .line 35
    :cond_0
    aget-byte v4, p0, v0

    const/16 v5, 0x3d

    if-ne v4, v5, :cond_3

    .line 36
    sub-int v4, v1, v0

    const/4 v5, 0x2

    if-le v4, v5, :cond_3

    .line 38
    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p0, v4

    if-ne v4, v8, :cond_1

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, p0, v4

    if-ne v4, v7, :cond_1

    .line 40
    add-int/lit8 v0, v0, 0x2

    move v2, v3

    .line 33
    .end local v3    # "retlen":I
    .restart local v2    # "retlen":I
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    .end local v2    # "retlen":I
    .restart local v3    # "retlen":I
    goto :goto_0

    .line 43
    :cond_1
    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p0, v4

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->isHexDigit(B)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, p0, v4

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->isHexDigit(B)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 46
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "retlen":I
    .restart local v2    # "retlen":I
    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p0, v4

    invoke-static {v4}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->getHexValue(B)B

    move-result v4

    mul-int/lit8 v4, v4, 0x10

    add-int/lit8 v5, v0, 0x2

    aget-byte v5, p0, v5

    invoke-static {v5}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->getHexValue(B)B

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p0, v3

    .line 48
    add-int/lit8 v0, v0, 0x2

    .line 49
    goto :goto_1

    .line 52
    .end local v2    # "retlen":I
    .restart local v3    # "retlen":I
    :cond_2
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "decode: Invalid sequence = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 53
    add-int/lit8 v6, v0, 0x1

    aget-byte v6, p0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x2

    aget-byte v6, p0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 52
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 64
    :cond_3
    aget-byte v4, p0, v0

    const/16 v5, 0x20

    if-lt v4, v5, :cond_4

    aget-byte v4, p0, v0

    const/16 v5, 0x7f

    if-le v4, v5, :cond_5

    :cond_4
    aget-byte v4, p0, v0

    const/16 v5, 0x9

    if-eq v4, v5, :cond_5

    aget-byte v4, p0, v0

    if-eq v4, v8, :cond_5

    .line 65
    aget-byte v4, p0, v0

    if-ne v4, v7, :cond_6

    .line 66
    :cond_5
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "retlen":I
    .restart local v2    # "retlen":I
    aget-byte v4, p0, v0

    aput-byte v4, p0, v3

    goto :goto_1

    .end local v2    # "retlen":I
    .restart local v3    # "retlen":I
    :cond_6
    move v2, v3

    .end local v3    # "retlen":I
    .restart local v2    # "retlen":I
    goto :goto_1
.end method

.method public static decode([BLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "qp"    # [B
    .param p1, "enc"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 90
    invoke-static {p0}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->decode([B)I

    move-result v1

    .line 92
    .local v1, "len":I
    :try_start_0
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3, v1, p1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-object v2

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p0, v4, v1}, Ljava/lang/String;-><init>([BII)V

    goto :goto_0
.end method

.method public static encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "content"    # Ljava/lang/String;
    .param p1, "enc"    # Ljava/lang/String;

    .prologue
    .line 108
    if-nez p0, :cond_0

    .line 109
    const/4 v2, 0x0

    .line 117
    :goto_0
    return-object v2

    .line 111
    :cond_0
    const/4 v1, 0x0

    .line 113
    .local v1, "str":[B
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 117
    :goto_1
    invoke-static {v1}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->encode([B)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    goto :goto_1
.end method

.method public static encode([B)Ljava/lang/String;
    .locals 9
    .param p0, "content"    # [B

    .prologue
    const/16 v8, 0x3d

    const/4 v7, 0x0

    .line 128
    if-nez p0, :cond_0

    .line 129
    const/4 v4, 0x0

    .line 150
    :goto_0
    return-object v4

    .line 131
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    .local v2, "out":Ljava/lang/StringBuilder;
    sput v7, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->mCurrentLineLength:I

    .line 134
    const/4 v3, 0x0

    .line 136
    .local v3, "requiredLength":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    array-length v4, p0

    if-lt v1, v4, :cond_1

    .line 150
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 137
    :cond_1
    aget-byte v0, p0, v1

    .line 139
    .local v0, "c":B
    const/16 v4, 0x21

    if-lt v0, v4, :cond_2

    const/16 v4, 0x7e

    if-gt v0, v4, :cond_2

    if-eq v0, v8, :cond_2

    .line 140
    const/4 v3, 0x1

    .line 141
    invoke-static {v3, v2}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->checkLineLength(ILjava/lang/StringBuilder;)V

    .line 142
    int-to-char v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 144
    :cond_2
    const/4 v3, 0x3

    .line 145
    invoke-static {v3, v2}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->checkLineLength(ILjava/lang/StringBuilder;)V

    .line 146
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 147
    const-string v4, "%02X"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private static getHexValue(B)B
    .locals 2
    .param p0, "b"    # B

    .prologue
    .line 78
    int-to-char v0, p0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Character;->digit(CI)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method private static isHexDigit(B)Z
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 74
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-le p0, v0, :cond_2

    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-le p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
