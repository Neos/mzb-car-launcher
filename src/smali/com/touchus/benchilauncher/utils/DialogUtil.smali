.class public Lcom/touchus/benchilauncher/utils/DialogUtil;
.super Ljava/lang/Object;
.source "DialogUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setDialogLocation(Landroid/app/Dialog;II)V
    .locals 3
    .param p0, "dialog"    # Landroid/app/Dialog;
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 22
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 23
    .local v0, "dialogWindow":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 24
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x11

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 25
    iput p1, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 26
    iput p2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 27
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 28
    return-void
.end method
