.class public Lcom/touchus/benchilauncher/utils/MobileDataSwitcher;
.super Ljava/lang/Object;
.source "MobileDataSwitcher.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMobileDataState(Landroid/content/Context;)Z
    .locals 4
    .param p0, "pContext"    # Landroid/content/Context;

    .prologue
    .line 24
    .line 25
    :try_start_0
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 24
    check-cast v2, Landroid/net/ConnectivityManager;

    .line 26
    .local v2, "mConnectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 27
    .local v1, "isOpen":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 29
    .end local v1    # "isOpen":Ljava/lang/Boolean;
    .end local v2    # "mConnectivityManager":Landroid/net/ConnectivityManager;
    :goto_0
    return v3

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static setMobileData(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "pContext"    # Landroid/content/Context;
    .param p1, "pBoolean"    # Z

    .prologue
    .line 9
    .line 10
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 9
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 11
    .local v0, "mConnectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    .line 12
    return-void
.end method
