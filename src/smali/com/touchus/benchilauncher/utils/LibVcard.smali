.class public Lcom/touchus/benchilauncher/utils/LibVcard;
.super Ljava/lang/Object;
.source "LibVcard.java"


# instance fields
.field isgetend:Z

.field isgethead:Z

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/utils/VcardUnit;",
            ">;"
        }
    .end annotation
.end field

.field private unit:Lcom/touchus/benchilauncher/utils/VcardUnit;


# direct methods
.method public constructor <init>([B)V
    .locals 8
    .param p1, "vcardData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/touchus/benchilauncher/utils/LibVcard;->list:Ljava/util/List;

    .line 45
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    .line 46
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgethead:Z

    .line 47
    iput-boolean v7, p0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgetend:Z

    .line 17
    if-nez p1, :cond_1

    .line 43
    :cond_0
    return-void

    .line 21
    :cond_1
    const/4 v3, 0x0

    .line 22
    .local v3, "startIndex":I
    const/4 v0, 0x0

    .line 23
    .local v0, "endIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, p1

    if-ge v1, v6, :cond_0

    .line 24
    aget-byte v6, p1, v1

    const/16 v7, 0xa

    if-ne v6, v7, :cond_3

    .line 26
    add-int/lit8 v6, v1, -0x1

    if-ltz v6, :cond_3

    add-int/lit8 v6, v1, -0x1

    array-length v7, p1

    if-ge v6, v7, :cond_3

    .line 27
    add-int/lit8 v6, v1, -0x1

    aget-byte v6, p1, v6

    const/16 v7, 0xd

    if-ne v6, v7, :cond_3

    .line 29
    add-int/lit8 v6, v1, -0x2

    if-ltz v6, :cond_3

    add-int/lit8 v6, v1, -0x2

    array-length v7, p1

    if-ge v6, v7, :cond_3

    .line 30
    add-int/lit8 v6, v1, -0x2

    aget-byte v6, p1, v6

    const/16 v7, 0x3d

    if-eq v6, v7, :cond_3

    .line 31
    add-int/lit8 v0, v1, -0x2

    .line 32
    sub-int v6, v0, v3

    add-int/lit8 v6, v6, 0x1

    new-array v2, v6, [B

    .line 33
    .local v2, "line":[B
    move v4, v3

    .local v4, "t":I
    const/4 v5, 0x0

    .local v5, "tt":I
    :goto_1
    if-gt v4, v0, :cond_2

    .line 34
    array-length v6, v2

    .line 33
    if-lt v5, v6, :cond_4

    .line 37
    :cond_2
    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/utils/LibVcard;->processLine([B)V

    .line 38
    add-int/lit8 v3, v0, 0x3

    .line 23
    .end local v2    # "line":[B
    .end local v4    # "t":I
    .end local v5    # "tt":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    .restart local v2    # "line":[B
    .restart local v4    # "t":I
    .restart local v5    # "tt":I
    :cond_4
    aget-byte v6, p1, v4

    aput-byte v6, v2, v5

    .line 34
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private processLine([B)V
    .locals 17
    .param p1, "line"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v3, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    .line 58
    .local v3, "lineStr":Ljava/lang/String;
    const-string v13, "VcardUnit"

    invoke-static {v13, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgethead:Z

    if-nez v13, :cond_1

    .line 60
    const-string v13, "BEGIN:VCARD"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 61
    new-instance v13, Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-direct {v13}, Lcom/touchus/benchilauncher/utils/VcardUnit;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    .line 62
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgethead:Z

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgetend:Z

    if-nez v13, :cond_0

    .line 66
    const-string v13, "END:VCARD"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 67
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v13}, Lcom/touchus/benchilauncher/utils/VcardUnit;->isvalid()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 68
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->list:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_2
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgethead:Z

    .line 71
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->isgetend:Z

    goto :goto_0

    .line 74
    :cond_3
    const-string v13, "N"

    invoke-virtual {v3, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_4

    const-string v13, "FN"

    invoke-virtual {v3, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 80
    :cond_4
    const-string v13, "CHARSET=([0-9A-Za-z\\-]+)"

    invoke-static {v13}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    .line 81
    .local v9, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 82
    .local v4, "m":Ljava/util/regex/Matcher;
    const-string v2, "UTF-8"

    .line 83
    .local v2, "encoding":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 84
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 86
    :cond_5
    new-instance v7, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 87
    .local v7, "newStr":Ljava/lang/String;
    const-string v13, ":"

    invoke-virtual {v7, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v7, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 89
    .local v5, "name":Ljava/lang/String;
    const-string v13, "ENCODING=QUOTED-PRINTABLE"

    invoke-virtual {v7, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_b

    .line 90
    const-string v13, ";"

    invoke-virtual {v5, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 91
    .local v6, "namesp":[Ljava/lang/String;
    if-eqz v6, :cond_6

    array-length v13, v6

    const/4 v14, 0x3

    if-lt v13, v14, :cond_6

    .line 92
    new-instance v13, Ljava/lang/StringBuilder;

    const/4 v14, 0x0

    aget-object v14, v6, v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v14, 0x2

    aget-object v14, v6, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v6, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 94
    :cond_6
    const-string v13, ";"

    const-string v14, ""

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 95
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-static {v13, v2}, Lcom/touchus/benchilauncher/utils/QuotedPrintable;->decode([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 105
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v13, v5}, Lcom/touchus/benchilauncher/utils/VcardUnit;->setName(Ljava/lang/String;)V

    .line 107
    .end local v2    # "encoding":Ljava/lang/String;
    .end local v4    # "m":Ljava/util/regex/Matcher;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "namesp":[Ljava/lang/String;
    .end local v7    # "newStr":Ljava/lang/String;
    .end local v9    # "p":Ljava/util/regex/Pattern;
    :cond_7
    const-string v13, "TEL"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_a

    .line 110
    const-string v13, "TEL[;a-zA-Z\\=\\-]*:([0-9\\-\\+ ]+)"

    invoke-static {v13}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    .line 111
    .restart local v9    # "p":Ljava/util/regex/Pattern;
    invoke-virtual {v9, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 112
    .restart local v4    # "m":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 113
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 114
    .local v8, "number":Ljava/lang/String;
    const-string v13, "-"

    const-string v14, ""

    invoke-virtual {v8, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 115
    const-string v13, " "

    const-string v14, ""

    invoke-virtual {v8, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 116
    const-string v13, "+86"

    invoke-virtual {v8, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 117
    const/4 v13, 0x3

    invoke-virtual {v8, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 119
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v13, v8}, Lcom/touchus/benchilauncher/utils/VcardUnit;->appendNumber(Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v13}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getName()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_9

    .line 121
    const-string v13, ""

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v14}, Lcom/touchus/benchilauncher/utils/VcardUnit;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 122
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    invoke-virtual {v13, v8}, Lcom/touchus/benchilauncher/utils/VcardUnit;->setName(Ljava/lang/String;)V

    .line 126
    .end local v4    # "m":Ljava/util/regex/Matcher;
    .end local v8    # "number":Ljava/lang/String;
    .end local v9    # "p":Ljava/util/regex/Pattern;
    :cond_a
    const-string v13, "X-IRMC-CALL-DATETIME"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_0

    .line 130
    const-string v13, ";"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    const-string v14, ":"

    invoke-virtual {v3, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    .line 129
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 131
    .local v12, "type":Ljava/lang/String;
    const-string v13, "MISSED"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 132
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    const/16 v14, 0x586

    invoke-virtual {v13, v14}, Lcom/touchus/benchilauncher/utils/VcardUnit;->setFlag(I)V

    .line 139
    :goto_2
    const-string v13, ":"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    .line 140
    const-string v14, "T"

    invoke-virtual {v3, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v14

    .line 138
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "date":Ljava/lang/String;
    const-string v13, "T"

    invoke-virtual {v3, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    .line 141
    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 143
    .local v11, "time":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .local v10, "stringBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    const/4 v14, 0x0

    const/4 v15, 0x4

    invoke-virtual {v1, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 145
    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 146
    const/4 v15, 0x4

    const/16 v16, 0x6

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 147
    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 148
    const/4 v15, 0x6

    invoke-virtual {v1, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 149
    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 150
    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 151
    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 152
    const/4 v15, 0x2

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 153
    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 154
    const/4 v15, 0x4

    invoke-virtual {v11, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 144
    invoke-virtual {v13, v14}, Lcom/touchus/benchilauncher/utils/VcardUnit;->setRemark(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 99
    .end local v1    # "date":Ljava/lang/String;
    .end local v10    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v11    # "time":Ljava/lang/String;
    .end local v12    # "type":Ljava/lang/String;
    .restart local v2    # "encoding":Ljava/lang/String;
    .restart local v4    # "m":Ljava/util/regex/Matcher;
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v7    # "newStr":Ljava/lang/String;
    .restart local v9    # "p":Ljava/util/regex/Pattern;
    :cond_b
    const-string v13, ";"

    invoke-virtual {v5, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 100
    .restart local v6    # "namesp":[Ljava/lang/String;
    if-eqz v6, :cond_c

    array-length v13, v6

    const/4 v14, 0x3

    if-lt v13, v14, :cond_c

    .line 101
    new-instance v13, Ljava/lang/StringBuilder;

    const/4 v14, 0x0

    aget-object v14, v6, v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v14, 0x2

    aget-object v14, v6, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    aget-object v14, v6, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 103
    :cond_c
    const-string v13, ";"

    const-string v14, ""

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 133
    .end local v2    # "encoding":Ljava/lang/String;
    .end local v4    # "m":Ljava/util/regex/Matcher;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "namesp":[Ljava/lang/String;
    .end local v7    # "newStr":Ljava/lang/String;
    .end local v9    # "p":Ljava/util/regex/Pattern;
    .restart local v12    # "type":Ljava/lang/String;
    :cond_d
    const-string v13, "DIALED"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 134
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    const/16 v14, 0x585

    invoke-virtual {v13, v14}, Lcom/touchus/benchilauncher/utils/VcardUnit;->setFlag(I)V

    goto/16 :goto_2

    .line 136
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/touchus/benchilauncher/utils/LibVcard;->unit:Lcom/touchus/benchilauncher/utils/VcardUnit;

    const/16 v14, 0x584

    invoke-virtual {v13, v14}, Lcom/touchus/benchilauncher/utils/VcardUnit;->setFlag(I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/touchus/benchilauncher/utils/VcardUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/touchus/benchilauncher/utils/LibVcard;->list:Ljava/util/List;

    return-object v0
.end method
