.class public Lcom/touchus/benchilauncher/utils/PersonParse;
.super Ljava/lang/Object;
.source "PersonParse.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 23
    const-string v1, ""

    .line 25
    .local v1, "content":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    .local v3, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 28
    .local v5, "instream":Ljava/io/InputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    .end local v5    # "instream":Ljava/io/InputStream;
    .local v6, "instream":Ljava/io/InputStream;
    if-eqz v6, :cond_0

    .line 30
    :try_start_1
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 31
    .local v4, "inputreader":Ljava/io/InputStreamReader;
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 34
    .local v0, "buffreader":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .local v7, "line":Ljava/lang/String;
    if-nez v7, :cond_2

    .line 37
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 42
    .end local v0    # "buffreader":Ljava/io/BufferedReader;
    .end local v4    # "inputreader":Ljava/io/InputStreamReader;
    .end local v7    # "line":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 44
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_1
    :goto_1
    move-object v5, v6

    .end local v6    # "instream":Ljava/io/InputStream;
    .restart local v5    # "instream":Ljava/io/InputStream;
    move-object v8, v1

    .line 50
    :goto_2
    return-object v8

    .line 35
    .end local v5    # "instream":Ljava/io/InputStream;
    .restart local v0    # "buffreader":Ljava/io/BufferedReader;
    .restart local v4    # "inputreader":Ljava/io/InputStreamReader;
    .restart local v6    # "instream":Ljava/io/InputStream;
    .restart local v7    # "line":Ljava/lang/String;
    :cond_2
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    goto :goto_0

    .line 39
    .end local v0    # "buffreader":Ljava/io/BufferedReader;
    .end local v4    # "inputreader":Ljava/io/InputStreamReader;
    .end local v6    # "instream":Ljava/io/InputStream;
    .end local v7    # "line":Ljava/lang/String;
    .restart local v5    # "instream":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 42
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    if-eqz v5, :cond_3

    .line 44
    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 40
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_4
    const/4 v8, 0x0

    goto :goto_2

    .line 45
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 46
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 41
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 42
    :goto_5
    if-eqz v5, :cond_4

    .line 44
    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 49
    :cond_4
    :goto_6
    throw v8

    .line 45
    :catch_2
    move-exception v2

    .line 46
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 45
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "instream":Ljava/io/InputStream;
    .restart local v6    # "instream":Ljava/io/InputStream;
    :catch_3
    move-exception v2

    .line 46
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 41
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "instream":Ljava/io/InputStream;
    .restart local v5    # "instream":Ljava/io/InputStream;
    goto :goto_5

    .line 39
    .end local v5    # "instream":Ljava/io/InputStream;
    .restart local v6    # "instream":Ljava/io/InputStream;
    :catch_4
    move-exception v2

    move-object v5, v6

    .end local v6    # "instream":Ljava/io/InputStream;
    .restart local v5    # "instream":Ljava/io/InputStream;
    goto :goto_3
.end method

.method public static getPersons(Ljava/io/InputStream;)Ljava/util/ArrayList;
    .locals 13
    .param p0, "xml"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    const/4 v6, 0x0

    .line 56
    .local v6, "persons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    const/4 v5, 0x0

    .line 57
    .local v5, "person":Lcom/touchus/benchilauncher/bean/Person;
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v8

    .line 58
    .local v8, "pullParser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v10, "UTF-8"

    invoke-interface {v8, p0, v10}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 59
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 60
    .local v0, "event":I
    :goto_0
    const/4 v10, 0x1

    if-ne v0, v10, :cond_0

    .line 100
    const-string v10, "onPhoneBookList::"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "getHistoryList::"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    return-object v6

    .line 61
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 98
    :cond_1
    :goto_1
    :pswitch_0
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    goto :goto_0

    .line 63
    :pswitch_1
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "persons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .restart local v6    # "persons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    goto :goto_1

    .line 67
    :pswitch_2
    const-string v10, "person"

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 68
    const/4 v10, 0x0

    invoke-interface {v8, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 69
    .local v2, "id":J
    new-instance v5, Lcom/touchus/benchilauncher/bean/Person;

    .end local v5    # "person":Lcom/touchus/benchilauncher/bean/Person;
    invoke-direct {v5}, Lcom/touchus/benchilauncher/bean/Person;-><init>()V

    .line 70
    .restart local v5    # "person":Lcom/touchus/benchilauncher/bean/Person;
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Lcom/touchus/benchilauncher/bean/Person;->setId(Ljava/lang/String;)V

    .line 72
    .end local v2    # "id":J
    :cond_2
    const-string v10, "name"

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 73
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v5, v4}, Lcom/touchus/benchilauncher/bean/Person;->setName(Ljava/lang/String;)V

    .line 76
    .end local v4    # "name":Ljava/lang/String;
    :cond_3
    const-string v10, "phone"

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 77
    new-instance v7, Ljava/lang/String;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 78
    .local v7, "phone":Ljava/lang/String;
    invoke-virtual {v5, v7}, Lcom/touchus/benchilauncher/bean/Person;->setPhone(Ljava/lang/String;)V

    .line 80
    .end local v7    # "phone":Ljava/lang/String;
    :cond_4
    const-string v10, "flag"

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 81
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 82
    .local v1, "flag":I
    invoke-virtual {v5, v1}, Lcom/touchus/benchilauncher/bean/Person;->setFlag(I)V

    .line 84
    .end local v1    # "flag":I
    :cond_5
    const-string v10, "remark"

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 85
    new-instance v9, Ljava/lang/String;

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 86
    .local v9, "remark":Ljava/lang/String;
    invoke-virtual {v5, v9}, Lcom/touchus/benchilauncher/bean/Person;->setRemark(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 92
    .end local v9    # "remark":Ljava/lang/String;
    :pswitch_3
    const-string v10, "person"

    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 93
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 61
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static save(Ljava/util/ArrayList;Ljava/io/OutputStream;)V
    .locals 7
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/touchus/benchilauncher/bean/Person;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 107
    .local p0, "persons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;"
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v2

    .line 108
    .local v2, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    const-string v3, "UTF-8"

    invoke-interface {v2, p1, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 109
    const-string v3, "UTF-8"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 110
    const/4 v3, 0x0

    const-string v4, "persons"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 111
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 134
    const/4 v3, 0x0

    const-string v4, "persons"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 135
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 136
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    if-eqz p1, :cond_0

    .line 142
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 146
    .end local v2    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :cond_0
    :goto_1
    return-void

    .line 111
    .restart local v2    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/bean/Person;

    .line 112
    .local v1, "person":Lcom/touchus/benchilauncher/bean/Person;
    const-string v4, "onPhoneBookList"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onPhoneBookList persons ===== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const/4 v4, 0x0

    const-string v5, "person"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 114
    const/4 v4, 0x0

    const-string v5, "id"

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/Person;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v4, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 116
    const/4 v4, 0x0

    const-string v5, "name"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 117
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/Person;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 118
    const/4 v4, 0x0

    const-string v5, "name"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 120
    const/4 v4, 0x0

    const-string v5, "phone"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 121
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/Person;->getPhone()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 122
    const/4 v4, 0x0

    const-string v5, "phone"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 124
    const/4 v4, 0x0

    const-string v5, "flag"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 125
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/Person;->getFlag()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 126
    const/4 v4, 0x0

    const-string v5, "flag"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 128
    const/4 v4, 0x0

    const-string v5, "remark"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 129
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/bean/Person;->getRemark()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 130
    const/4 v4, 0x0

    const-string v5, "remark"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 132
    const/4 v4, 0x0

    const-string v5, "person"

    invoke-interface {v2, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 137
    .end local v1    # "person":Lcom/touchus/benchilauncher/bean/Person;
    .end local v2    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    if-eqz p1, :cond_0

    .line 142
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    goto/16 :goto_1

    .line 140
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 141
    if-eqz p1, :cond_2

    .line 142
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 144
    :cond_2
    throw v3
.end method
