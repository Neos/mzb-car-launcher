.class public Lcom/touchus/benchilauncher/utils/ToastTool;
.super Ljava/lang/Object;
.source "ToastTool.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static showBigShortToast(Landroid/content/Context;I)V
    .locals 8
    .param p0, "cont"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    const/4 v7, 0x0

    .line 47
    const/4 v0, 0x0

    .line 48
    .local v0, "toast":Landroid/widget/Toast;
    const-string v3, ""

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 49
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030042

    .line 50
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v3

    const v6, 0x7f0b0161

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 49
    invoke-virtual {v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 51
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0b0162

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 52
    .local v1, "txtView_Context":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    new-instance v0, Landroid/widget/Toast;

    .end local v0    # "toast":Landroid/widget/Toast;
    invoke-direct {v0, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 54
    .restart local v0    # "toast":Landroid/widget/Toast;
    const/16 v3, 0x11

    const/16 v4, 0xc8

    const/16 v5, 0xa

    invoke-virtual {v0, v3, v4, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 55
    invoke-virtual {v0, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 56
    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 57
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 58
    return-void
.end method

.method public static showBigShortToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p0, "cont"    # Landroid/content/Context;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 33
    const/4 v0, 0x0

    .line 34
    .local v0, "toast":Landroid/widget/Toast;
    const-string v3, ""

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 35
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030042

    .line 36
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v3

    const v6, 0x7f0b0161

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 35
    invoke-virtual {v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 37
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0b0162

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 38
    .local v1, "txtView_Context":Landroid/widget/TextView;
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    new-instance v0, Landroid/widget/Toast;

    .end local v0    # "toast":Landroid/widget/Toast;
    invoke-direct {v0, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 40
    .restart local v0    # "toast":Landroid/widget/Toast;
    const/16 v3, 0x11

    const/16 v4, 0xc8

    const/16 v5, 0xa

    invoke-virtual {v0, v3, v4, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 41
    invoke-virtual {v0, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 42
    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 43
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 44
    return-void
.end method

.method public static showLongToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "cont"    # Landroid/content/Context;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 16
    const/4 v1, 0x1

    :try_start_0
    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 17
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    const/4 v2, 0x0

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 18
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    .end local v0    # "toast":Landroid/widget/Toast;
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static showShortToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "cont"    # Landroid/content/Context;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 25
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 26
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    const/4 v2, 0x0

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 27
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .end local v0    # "toast":Landroid/widget/Toast;
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v1

    goto :goto_0
.end method
