.class public Lcom/touchus/benchilauncher/utils/VideoType;
.super Ljava/lang/Object;
.source "VideoType.java"


# static fields
.field private static videoTpye:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".rm"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ".rmvb"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ".avi"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ".pmeg"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ".wmv"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ".3gp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ".mp4"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ".dat"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ".vob"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ".flv"

    aput-object v2, v0, v1

    sput-object v0, Lcom/touchus/benchilauncher/utils/VideoType;->videoTpye:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isVideo(Ljava/lang/String;)Z
    .locals 6
    .param p0, "video"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 12
    sget-object v3, Lcom/touchus/benchilauncher/utils/VideoType;->videoTpye:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 17
    :goto_1
    return v1

    .line 12
    :cond_0
    aget-object v0, v3, v2

    .line 13
    .local v0, "v":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 14
    const/4 v1, 0x1

    goto :goto_1

    .line 12
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
