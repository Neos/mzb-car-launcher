.class Lcom/touchus/benchilauncher/LauncherApplication$3;
.super Ljava/lang/Object;
.source "LauncherApplication.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/LauncherApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/LauncherApplication;


# direct methods
.method constructor <init>(Lcom/touchus/benchilauncher/LauncherApplication;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/touchus/benchilauncher/LauncherApplication$3;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 765
    packed-switch p1, :pswitch_data_0

    .line 781
    :goto_0
    :pswitch_0
    return-void

    .line 767
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication$3;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->logger:Lorg/slf4j/Logger;

    const-string v1, "requestAudioFocus DV LOSS"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 773
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication$3;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->logger:Lorg/slf4j/Logger;

    const-string v1, "requestAudioFocus DV CAN_DUCK"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 777
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/LauncherApplication$3;->this$0:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->logger:Lorg/slf4j/Logger;

    const-string v1, "requestAudioFocus DV GAIN"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 765
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
