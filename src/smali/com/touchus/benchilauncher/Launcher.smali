.class public Lcom/touchus/benchilauncher/Launcher;
.super Landroid/support/v4/app/FragmentActivity;
.source "Launcher.java"

# interfaces
.implements Lcom/touchus/benchilauncher/inface/IDoorStateParent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/Launcher$InitBluetoothThread;,
        Lcom/touchus/benchilauncher/Launcher$Mhandler;
    }
.end annotation


# instance fields
.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field private backList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final blueconn:Landroid/content/ServiceConnection;

.field private conn:Landroid/content/ServiceConnection;

.field public lastChangeTimne:J

.field public lastChangeView:B

.field mButtomFrag:Lcom/touchus/benchilauncher/activity/main/ButtomBar;

.field public mButtomSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

.field private mHandler:Lcom/touchus/benchilauncher/Launcher$Mhandler;

.field public mIsButtomShowed:Z

.field public mIsTopShowed:Z

.field public mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

.field public mMenuShow:Z

.field mStateFra:Lcom/touchus/benchilauncher/activity/main/StateFrag;

.field public mTopSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

.field public manager:Landroid/support/v4/app/FragmentManager;

.field public menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

.field public playVideoLayout:Landroid/widget/FrameLayout;

.field private rightLayout:Landroid/widget/FrameLayout;

.field private yibiaoFragment:Landroid/support/v4/app/Fragment;

.field private yibiaoLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 51
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 56
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->manager:Landroid/support/v4/app/FragmentManager;

    .line 66
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/Launcher;->mIsButtomShowed:Z

    .line 67
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/Launcher;->mIsTopShowed:Z

    .line 69
    iput-boolean v1, p0, Lcom/touchus/benchilauncher/Launcher;->mMenuShow:Z

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/fragment/YiBiaoFragment;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    .line 175
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->MENU:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard$EControlSource;->getCode()B

    move-result v0

    iput-byte v0, p0, Lcom/touchus/benchilauncher/Launcher;->lastChangeView:B

    .line 176
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/touchus/benchilauncher/Launcher;->lastChangeTimne:J

    .line 529
    new-instance v0, Lcom/touchus/benchilauncher/Launcher$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/Launcher$1;-><init>(Lcom/touchus/benchilauncher/Launcher;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->conn:Landroid/content/ServiceConnection;

    .line 541
    new-instance v0, Lcom/touchus/benchilauncher/Launcher$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/Launcher$2;-><init>(Lcom/touchus/benchilauncher/Launcher;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->blueconn:Landroid/content/ServiceConnection;

    .line 674
    new-instance v0, Lcom/touchus/benchilauncher/Launcher$Mhandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/Launcher$Mhandler;-><init>(Lcom/touchus/benchilauncher/Launcher;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mHandler:Lcom/touchus/benchilauncher/Launcher$Mhandler;

    .line 51
    return-void
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/Launcher;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/Launcher;)V
    .locals 0

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->initBluetooth()V

    return-void
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/Launcher;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 614
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/Launcher;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method private artSound(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 657
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    .line 672
    :goto_0
    return-void

    .line 660
    :cond_0
    new-instance v1, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 661
    .local v1, "builder":Lcom/touchus/benchilauncher/views/SoundDialog$Builder;
    invoke-virtual {v1, p1}, Lcom/touchus/benchilauncher/views/SoundDialog$Builder;->create(I)Lcom/touchus/benchilauncher/views/SoundDialog;

    move-result-object v0

    .line 662
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/SoundDialog;
    const/4 v2, 0x0

    const/16 v3, -0xb4

    invoke-static {v0, v2, v3}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 663
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/SoundDialog;->show()V

    .line 664
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v2, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog4:Landroid/app/Dialog;

    .line 665
    new-instance v2, Lcom/touchus/benchilauncher/Launcher$4;

    invoke-direct {v2, p0, v1}, Lcom/touchus/benchilauncher/Launcher$4;-><init>(Lcom/touchus/benchilauncher/Launcher;Lcom/touchus/benchilauncher/views/SoundDialog$Builder;)V

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/views/SoundDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0
.end method

.method private changeRightLayoutFragmentByHistory(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "to"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 156
    const v0, 0x7f0b0003

    invoke-virtual {p0, v0, p1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 157
    return-void
.end method

.method private changeViewFromIdriverCar(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 588
    const-string v1, "idriver_enum"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v0

    .line 589
    .local v0, "code":B
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-eq v0, v1, :cond_0

    .line 590
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_1

    .line 591
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->dismissDialog()V

    .line 593
    :cond_1
    iget-byte v1, p0, Lcom/touchus/benchilauncher/Launcher;->lastChangeView:B

    if-ne v1, v0, :cond_3

    .line 594
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/touchus/benchilauncher/Launcher;->lastChangeTimne:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 612
    :cond_2
    :goto_0
    return-void

    .line 597
    :cond_3
    iput-byte v0, p0, Lcom/touchus/benchilauncher/Launcher;->lastChangeView:B

    .line 598
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/touchus/benchilauncher/Launcher;->lastChangeTimne:J

    .line 600
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-eq v0, v1, :cond_4

    .line 601
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_6

    .line 602
    :cond_4
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    if-eqz v1, :cond_5

    .line 603
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->isCallDialog:Z

    if-nez v1, :cond_2

    .line 604
    :cond_5
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 605
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->dismissDialog()V

    goto :goto_0

    .line 607
    :cond_6
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_7

    .line 608
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->handHomeAction()V

    goto :goto_0

    .line 609
    :cond_7
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NAVI:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v1

    if-ne v0, v1, :cond_2

    .line 610
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->startNavi()V

    goto :goto_0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    .line 615
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 654
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    const-string v2, "msg.what"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1771

    if-ne v2, v3, :cond_2

    .line 620
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 621
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-direct {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->changeViewFromIdriverCar(Landroid/os/Bundle;)V

    goto :goto_0

    .line 622
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x1772

    if-ne v2, v3, :cond_3

    .line 623
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 624
    const v3, 0x7f0b000f

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 623
    check-cast v1, Lcom/touchus/benchilauncher/activity/main/StateFrag;

    .line 625
    .local v1, "stateFrag":Lcom/touchus/benchilauncher/activity/main/StateFrag;
    if-eqz v1, :cond_0

    .line 626
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->setBt()V

    goto :goto_0

    .line 628
    .end local v1    # "stateFrag":Lcom/touchus/benchilauncher/activity/main/StateFrag;
    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3f5

    if-ne v2, v3, :cond_4

    .line 629
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    instance-of v2, v2, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    if-nez v2, :cond_0

    .line 630
    new-instance v2, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-direct {v2}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 632
    :cond_4
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3f1

    if-ne v2, v3, :cond_5

    .line 633
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->setLeftClockTime(Landroid/os/Bundle;)V

    goto :goto_0

    .line 634
    :cond_5
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x461

    if-eq v2, v3, :cond_6

    .line 635
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x46b

    if-ne v2, v3, :cond_7

    .line 636
    :cond_6
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v5, v2, Lcom/touchus/benchilauncher/LauncherApplication;->mHomeAndBackEnable:Z

    .line 637
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->handleBackAction()V

    .line 638
    const v2, 0x7f070065

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 639
    const/4 v3, 0x0

    .line 638
    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 639
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 640
    :cond_7
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x400

    if-ne v2, v3, :cond_8

    .line 641
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->handHomeAction()V

    goto/16 :goto_0

    .line 642
    :cond_8
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x406

    if-ne v2, v3, :cond_9

    .line 644
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->handHomeAction()V

    .line 645
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-virtual {v2, v5}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->selectMenu(I)V

    goto/16 :goto_0

    .line 646
    :cond_9
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x40c

    if-ne v2, v3, :cond_a

    .line 647
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/touchus/publicutils/utils/UtilTools;->sendKeyeventToSystem(I)V

    .line 648
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->goTo(I)V

    goto/16 :goto_0

    .line 649
    :cond_a
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x410

    if-ne v2, v3, :cond_b

    .line 650
    invoke-direct {p0, v5}, Lcom/touchus/benchilauncher/Launcher;->artSound(I)V

    goto/16 :goto_0

    .line 651
    :cond_b
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x416

    if-ne v2, v3, :cond_0

    .line 652
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showNaviView()V

    goto/16 :goto_0
.end method

.method private inflate()V
    .locals 2

    .prologue
    .line 99
    const v0, 0x7f0b000b

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 100
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 101
    const v0, 0x7f0b0006

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/left/RunningFrag;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 102
    const v0, 0x7f0b0007

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 103
    const v0, 0x7f0b0008

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/left/LightFrag;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 104
    const v0, 0x7f0b0009

    new-instance v1, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;

    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/left/CardoorFrag;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 106
    const v0, 0x7f0b000f

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->mStateFra:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 108
    const v0, 0x7f0b000d

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->mButtomFrag:Lcom/touchus/benchilauncher/activity/main/ButtomBar;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 110
    const v0, 0x7f0b0003

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    .line 111
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    .line 112
    return-void
.end method

.method private initBluetooth()V
    .locals 3

    .prologue
    .line 580
    :try_start_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/touchus/benchilauncher/Launcher$InitBluetoothThread;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/Launcher$InitBluetoothThread;-><init>(Lcom/touchus/benchilauncher/Launcher;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 581
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/touchus/benchilauncher/service/BluetoothService;->accFlag:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 585
    :goto_0
    return-void

    .line 582
    :catch_0
    move-exception v0

    .line 583
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initObject()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 77
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    .line 80
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mStateFra:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    .line 82
    new-instance v0, Lcom/touchus/benchilauncher/activity/main/ButtomBar;

    invoke-direct {v0}, Lcom/touchus/benchilauncher/activity/main/ButtomBar;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mButtomFrag:Lcom/touchus/benchilauncher/activity/main/ButtomBar;

    .line 84
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/ButtomSlide;

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mButtomSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

    .line 85
    const v0, 0x7f0b000e

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/ButtomSlide;

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mTopSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

    .line 87
    const v0, 0x7f0b0005

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/views/LeftStateSlide;

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    .line 88
    const v0, 0x7f0b0003

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    .line 90
    const v0, 0x7f0b000b

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    .line 91
    const v0, 0x7f0b000a

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    .line 92
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 94
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 95
    return-void
.end method

.method private removePlayVideo()V
    .locals 5

    .prologue
    .line 161
    :try_start_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 162
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showTop()V

    .line 163
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 164
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 165
    const v3, 0x7f0b000a

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 164
    check-cast v1, Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    .line 166
    .local v1, "showFrag":Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 168
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 170
    :cond_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/16 v3, 0x407

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    .end local v1    # "showFrag":Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private setLeftClockTime(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 524
    .local v0, "temp":Landroid/support/v4/app/Fragment;
    instance-of v1, v0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;

    if-eqz v1, :cond_0

    .line 525
    check-cast v0, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;

    .end local v0    # "temp":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/activity/main/left/LeftClockFrag;->updateCurrentTime()V

    .line 527
    :cond_0
    return-void
.end method

.method private startBluetoothService()V
    .locals 3

    .prologue
    .line 560
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    if-nez v1, :cond_0

    .line 561
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/touchus/benchilauncher/service/BluetoothService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 562
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 563
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->blueconn:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/touchus/benchilauncher/Launcher;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 567
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 565
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->initBluetooth()V

    goto :goto_0
.end method


# virtual methods
.method public changeCenterTo()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 443
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 444
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 445
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 446
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 447
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 448
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v0, v3}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 449
    return-void
.end method

.method public changeFragment(ILandroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "rootID"    # I
    .param p2, "to"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->manager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 292
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 293
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public changeRightTo(Landroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1, "to"    # Landroid/support/v4/app/Fragment;

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 120
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 123
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->removePlayVideo()V

    .line 124
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 129
    :cond_1
    instance-of v0, p1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    if-nez v0, :cond_7

    .line 130
    instance-of v0, p1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    if-eqz v0, :cond_4

    .line 131
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 134
    :cond_2
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    if-eqz v0, :cond_4

    .line 153
    :cond_3
    :goto_0
    return-void

    .line 138
    :cond_4
    instance-of v0, p1, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;

    if-eqz v0, :cond_6

    .line 139
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 140
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 142
    :cond_5
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;

    if-nez v0, :cond_3

    .line 146
    :cond_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showButtom()V

    .line 152
    :goto_1
    const v0, 0x7f0b0003

    invoke-virtual {p0, v0, p1}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 149
    :cond_7
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 150
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    goto :goto_1
.end method

.method public changeToPlayVideo(Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;)V
    .locals 5
    .param p1, "fragmentShow"    # Lcom/touchus/benchilauncher/fragment/FragmentVideoPlay;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 452
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 453
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f0b000a

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 454
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 455
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 456
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v1, v2}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 457
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 458
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 459
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 460
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 264
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    if-eqz v1, :cond_0

    .line 284
    :goto_0
    return v0

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    if-eqz v1, :cond_1

    .line 267
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v1, v1, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    if-eqz v1, :cond_1

    .line 268
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->iIsScreenClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->closeOrWakeupScreen(Z)V

    goto :goto_0

    .line 284
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getCurrentFragment()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0b0003

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 180
    .local v0, "temp":Landroid/support/v4/app/Fragment;
    return-object v0
.end method

.method public goTo(I)V
    .locals 5
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x1

    .line 315
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 366
    :goto_1
    const/4 v3, -0x1

    if-gt p1, v3, :cond_2

    const/16 v3, 0xb

    if-ge p1, v3, :cond_0

    .line 367
    :cond_2
    sput p1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    goto :goto_0

    .line 320
    :pswitch_0
    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/LauncherApplication;->startNavi()V

    goto :goto_1

    .line 323
    :pswitch_1
    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v3}, Lcom/touchus/benchilauncher/LauncherApplication;->interOriginalView()V

    goto :goto_1

    .line 327
    :pswitch_2
    new-instance v3, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;

    invoke-direct {v3}, Lcom/touchus/benchilauncher/fragment/FragmentMediaMenu;-><init>()V

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 330
    :pswitch_3
    new-instance v3, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    invoke-direct {v3}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 333
    :pswitch_4
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->changeCenterTo()V

    .line 334
    sput-boolean v4, Lcom/touchus/benchilauncher/LauncherApplication;->shutDoorNeedShowYibiao:Z

    goto :goto_1

    .line 337
    :pswitch_5
    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v4, "net.easyconn"

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->startAppByPkg(Ljava/lang/String;)Z

    goto :goto_1

    .line 340
    :pswitch_6
    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const-string v4, "com.android.chrome"

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->startAppByPkg(Ljava/lang/String;)Z

    goto :goto_1

    .line 343
    :pswitch_7
    new-instance v2, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;

    invoke-direct {v2}, Lcom/touchus/benchilauncher/fragment/FragmentYingyong;-><init>()V

    .line 344
    .local v2, "fragmentYingyong":Lcom/touchus/benchilauncher/fragment/FragmentYingyong;
    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 347
    .end local v2    # "fragmentYingyong":Lcom/touchus/benchilauncher/fragment/FragmentYingyong;
    :pswitch_8
    new-instance v1, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 349
    .local v1, "builder":Lcom/touchus/benchilauncher/views/SettingDialog$Builder;
    invoke-virtual {v1}, Lcom/touchus/benchilauncher/views/SettingDialog$Builder;->create()Lcom/touchus/benchilauncher/views/SettingDialog;

    move-result-object v0

    .line 350
    .local v0, "SettingDialog":Lcom/touchus/benchilauncher/views/SettingDialog;
    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->currentDialog1:Landroid/app/Dialog;

    .line 351
    const/16 v3, 0x64

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/touchus/benchilauncher/utils/DialogUtil;->setDialogLocation(Landroid/app/Dialog;II)V

    .line 352
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/SettingDialog;->show()V

    .line 354
    new-instance v3, Lcom/touchus/benchilauncher/Launcher$3;

    invoke-direct {v3, p0, v1}, Lcom/touchus/benchilauncher/Launcher$3;-><init>(Lcom/touchus/benchilauncher/Launcher;Lcom/touchus/benchilauncher/views/SettingDialog$Builder;)V

    invoke-virtual {v0, v3}, Lcom/touchus/benchilauncher/views/SettingDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_1

    .line 363
    .end local v0    # "SettingDialog":Lcom/touchus/benchilauncher/views/SettingDialog;
    .end local v1    # "builder":Lcom/touchus/benchilauncher/views/SettingDialog$Builder;
    :pswitch_9
    invoke-static {}, Lcom/touchus/benchilauncher/LauncherApplication;->getInstance()Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->closeOrWakeupScreen(Z)V

    goto :goto_1

    .line 318
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public handHomeAction()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 371
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->dismissDialog()V

    .line 372
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showTop()V

    .line 373
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendHideOrShowLocateViewEvent(Z)V

    .line 374
    sput-boolean v3, Lcom/touchus/benchilauncher/LauncherApplication;->shutDoorNeedShowYibiao:Z

    .line 375
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 377
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 378
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 379
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 381
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v1, v2}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 382
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 384
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 385
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->removePlayVideo()V

    .line 386
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 388
    :cond_1
    instance-of v1, v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    if-eqz v1, :cond_2

    sget v1, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->mCurIndex:I

    if-eqz v1, :cond_2

    .line 389
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;->selectMenu(I)V

    .line 394
    :goto_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    .line 395
    return-void

    .line 391
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 392
    const v1, 0x7f0b0003

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-virtual {p0, v1, v2}, Lcom/touchus/benchilauncher/Launcher;->changeFragment(ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public handleBackAction()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 398
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->showTop()V

    .line 399
    sput-boolean v4, Lcom/touchus/benchilauncher/LauncherApplication;->shutDoorNeedShowYibiao:Z

    .line 400
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 402
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v2, v0, Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 403
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 404
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    .line 440
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    :goto_0
    return-void

    .line 407
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 408
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 409
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v2, v4}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 410
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v2, v3}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 412
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v4, v2, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 413
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    goto :goto_0

    .line 416
    :cond_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 417
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->removePlayVideo()V

    .line 418
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 419
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v2, v4}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 420
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->playVideoLayout:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 423
    :cond_3
    instance-of v2, v0, Lcom/touchus/benchilauncher/base/BaseFragment;

    if-eqz v2, :cond_0

    .line 426
    check-cast v0, Lcom/touchus/benchilauncher/base/BaseFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/touchus/benchilauncher/base/BaseFragment;->onBack()Z

    move-result v2

    if-nez v2, :cond_0

    .line 427
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    .line 428
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 429
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 430
    .local v1, "needto":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    if-eqz v2, :cond_4

    .line 431
    new-instance v1, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;

    .end local v1    # "needto":Landroid/support/v4/app/Fragment;
    invoke-direct {v1}, Lcom/touchus/benchilauncher/activity/main/right/call/CallPhoneFragment;-><init>()V

    .line 433
    .restart local v1    # "needto":Landroid/support/v4/app/Fragment;
    :cond_4
    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/Launcher;->changeRightLayoutFragmentByHistory(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_0

    .line 435
    .end local v1    # "needto":Landroid/support/v4/app/Fragment;
    :cond_5
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->backList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 436
    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->menuFragment:Lcom/touchus/benchilauncher/activity/main/right/Menu/MenuFragment;

    invoke-virtual {p0, v2}, Lcom/touchus/benchilauncher/Launcher;->changeRightTo(Landroid/support/v4/app/Fragment;)V

    .line 437
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->hideButtom()V

    goto/16 :goto_0
.end method

.method public hideButtom()V
    .locals 3

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsButtomShowed:Z

    if-nez v0, :cond_0

    .line 520
    :goto_0
    return-void

    .line 518
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsButtomShowed:Z

    .line 519
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mButtomSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

    const/16 v1, -0x3c

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/views/ButtomSlide;->smoothScrollYTo(II)V

    goto :goto_0
.end method

.method public hideTop()V
    .locals 3

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsTopShowed:Z

    if-nez v0, :cond_0

    .line 506
    :goto_0
    return-void

    .line 504
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsTopShowed:Z

    .line 505
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mTopSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

    const/16 v1, 0x28

    const/16 v2, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/views/ButtomSlide;->smoothScrollYTo(II)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 242
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 185
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 186
    const-string v1, ""

    const-string v2, "NAVIGATION_BAR----------------onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual {p0, v3}, Lcom/touchus/benchilauncher/Launcher;->requestWindowFeature(I)Z

    .line 188
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/Launcher;->setContentView(I)V

    .line 189
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 190
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->mHandler:Lcom/touchus/benchilauncher/Launcher$Mhandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->registerHandler(Landroid/os/Handler;)V

    .line 191
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->mHandler:Lcom/touchus/benchilauncher/Launcher$Mhandler;

    iput-object v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->launcherHandler:Landroid/os/Handler;

    .line 193
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/touchus/benchilauncher/MainService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 194
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/Launcher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 195
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    if-nez v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->conn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v3}, Lcom/touchus/benchilauncher/Launcher;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->startBluetoothService()V

    .line 200
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->initObject()V

    .line 201
    invoke-direct {p0}, Lcom/touchus/benchilauncher/Launcher;->inflate()V

    .line 202
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 253
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, p0, Lcom/touchus/benchilauncher/Launcher;->mHandler:Lcom/touchus/benchilauncher/Launcher$Mhandler;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->unregisterHandler(Landroid/os/Handler;)V

    .line 255
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->conn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/Launcher;->unbindService(Landroid/content/ServiceConnection;)V

    .line 256
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->blueconn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/Launcher;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iCurrentInLauncher:Z

    .line 226
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->openNaviThread()V

    .line 229
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 230
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 213
    const-string v0, ""

    const-string v1, "NAVIGATION_BAR----------------onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->service:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->closeNaviThread()V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->sendHideOrShowNavigationBarEvent(Z)V

    .line 218
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iCurrentInLauncher:Z

    .line 219
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mStateFra:Lcom/touchus/benchilauncher/activity/main/StateFrag;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/activity/main/StateFrag;->checkUsb3()V

    .line 220
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 221
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 248
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 206
    const-string v0, ""

    const-string v1, "NAVIGATION_BAR----------------onStart== "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 209
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->dismissDialog()V

    .line 235
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 236
    return-void
.end method

.method public resetLeftLayoutToRightState()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 482
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->iHandleTouchEvent:Z

    .line 483
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->changeToCurrentView()V

    .line 484
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->shutDoorNeedShowYibiao:Z

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 486
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 487
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v0, v3}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 488
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 489
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    .line 491
    :cond_0
    return-void
.end method

.method public showButtom()V
    .locals 3

    .prologue
    .line 508
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsButtomShowed:Z

    if-eqz v0, :cond_0

    .line 512
    :goto_0
    return-void

    .line 510
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsButtomShowed:Z

    .line 511
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mButtomSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

    const/4 v1, 0x0

    const/16 v2, 0x12c

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/views/ButtomSlide;->smoothScrollYTo(II)V

    goto :goto_0
.end method

.method public showCardoor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 466
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/views/LeftStateSlide;->iHandleTouchEvent:Z

    .line 470
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    sget v1, Lcom/touchus/benchilauncher/views/LeftStateSlide;->mChildWith:I

    mul-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->scrollTo(II)V

    .line 471
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 473
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->yibiaoFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0, v1}, Lcom/touchus/benchilauncher/Launcher;->showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V

    .line 474
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->rightLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mLeftStateSlide:Lcom/touchus/benchilauncher/views/LeftStateSlide;

    invoke-virtual {v0, v2}, Lcom/touchus/benchilauncher/views/LeftStateSlide;->setVisibility(I)V

    .line 476
    const/4 v0, 0x1

    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->shutDoorNeedShowYibiao:Z

    .line 477
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->iIsYibiaoShowing:Z

    goto :goto_0
.end method

.method public showFragment(Ljava/lang/Boolean;Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "ishow"    # Ljava/lang/Boolean;
    .param p2, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/Launcher;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 308
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/Launcher;->manager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 302
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 303
    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 307
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 305
    :cond_1
    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method public showNaviView()V
    .locals 0

    .prologue
    .line 464
    return-void
.end method

.method public showTop()V
    .locals 3

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsTopShowed:Z

    if-eqz v0, :cond_0

    .line 498
    :goto_0
    return-void

    .line 496
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/Launcher;->mIsTopShowed:Z

    .line 497
    iget-object v0, p0, Lcom/touchus/benchilauncher/Launcher;->mTopSlide:Lcom/touchus/benchilauncher/views/ButtomSlide;

    const/4 v1, 0x0

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/views/ButtomSlide;->smoothScrollYTo(II)V

    goto :goto_0
.end method
