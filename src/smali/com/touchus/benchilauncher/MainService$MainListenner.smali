.class Lcom/touchus/benchilauncher/MainService$MainListenner;
.super Ljava/lang/Object;
.source "MainService.java"

# interfaces
.implements Lcom/backaudio/android/driver/IMainboardEventLisenner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/touchus/benchilauncher/MainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MainListenner"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer:[I

.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye:[I

.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet:[I


# instance fields
.field final synthetic this$0:Lcom/touchus/benchilauncher/MainService;


# direct methods
.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer()[I
    .locals 3

    .prologue
    .line 2019
    sget-object v0, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->values()[Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->BT_CONNECT:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->DV:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_3
    :try_start_3
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_4
    :try_start_4
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    :try_start_5
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->QUERY:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_6
    :try_start_6
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->RECORDER:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    :try_start_7
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_8
    :try_start_8
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->REVERSE_360:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_9
    :try_start_9
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->SCREEN_OFF:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_a
    sput-object v0, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_9

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_7

    :catch_4
    move-exception v1

    goto :goto_6

    :catch_5
    move-exception v1

    goto :goto_5

    :catch_6
    move-exception v1

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye()[I
    .locals 3

    .prologue
    .line 2019
    sget-object v0, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->values()[Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ADD_REVERSE:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->REVERSE_360:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet()[I
    .locals 3

    .prologue
    .line 2019
    sget-object v0, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->values()[Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->FLAT:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->QUERY:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 2019
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/MainService$MainListenner;)Lcom/touchus/benchilauncher/MainService;
    .locals 1

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    return-object v0
.end method


# virtual methods
.method public logcatCanbox(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 2455
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2456
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.logcatcanbox"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2457
    const-string v1, "canbox"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2458
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2459
    return-void
.end method

.method public obtainBenzSize(I)V
    .locals 3
    .param p1, "benzType"    # I

    .prologue
    .line 2574
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput p1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    .line 2575
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    const-string v1, "screenType"

    invoke-virtual {v0, v1, p1}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2576
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<<<\tobtainBenzSize:\ttype--"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2577
    return-void
.end method

.method public obtainBenzType(Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;)V
    .locals 4
    .param p1, "eBenzTpye"    # Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .prologue
    .line 2543
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "obtainBenzType before benzTpye = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2544
    sput-object p1, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 2545
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel;->KEY:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2546
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<<<\tobtainBenzType:\tEBenzTpye--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2547
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2548
    .local v0, "bun":Landroid/os/Bundle;
    const-string v1, "runningState"

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->carRunInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2549
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x3f0

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2550
    return-void
.end method

.method public obtainBrightness(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 2592
    return-void
.end method

.method public obtainLanguageMediaSet(Lcom/backaudio/android/driver/Mainboard$ELanguage;)V
    .locals 3
    .param p1, "eLanguage"    # Lcom/backaudio/android/driver/Mainboard$ELanguage;

    .prologue
    .line 2597
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<<<\tobtainLanguageMediaSet: eLanguage--"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2598
    return-void
.end method

.method public obtainReverseMediaSet(Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;)V
    .locals 3
    .param p1, "eMediaSet"    # Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .prologue
    .line 2554
    invoke-static {}, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2567
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-object p1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 2568
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<<<\tobtainReverseMediaSet:\tEReverserMediaSet--"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2569
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2568
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2570
    return-void

    .line 2556
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    const-string v1, "voiceType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 2561
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    const-string v1, "voiceType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 2554
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public obtainReverseType(Lcom/backaudio/android/driver/Mainboard$EReverseTpye;)V
    .locals 4
    .param p1, "eReverseTpye"    # Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    .prologue
    .line 2524
    const/4 v0, 0x1

    .line 2525
    .local v0, "type":I
    invoke-static {}, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2536
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    const-string v2, "reversingType"

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2537
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<<<\tobtainReverseType:\tEReverseTpye--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2538
    const-string v3, "type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2537
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2539
    return-void

    .line 2527
    :pswitch_0
    const/4 v0, 0x0

    .line 2528
    goto :goto_0

    .line 2530
    :pswitch_1
    const/4 v0, 0x1

    .line 2531
    goto :goto_0

    .line 2533
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 2525
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public obtainStoreData(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Byte;>;"
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x6

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2463
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<<<\tobtainStoreData:\tdata--"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2464
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    if-ne v0, v1, :cond_0

    .line 2465
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    .line 2469
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v4, 0x7f0700a1

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2470
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iget-boolean v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    .line 2469
    invoke-virtual {v0, v3, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 2471
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v2

    .line 2473
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v1

    .line 2474
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v4, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v4

    .line 2476
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    if-ne v0, v1, :cond_1

    .line 2477
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 2481
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v4, 0x7f0700a2

    invoke-virtual {v3, v4}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    iget-boolean v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    invoke-virtual {v0, v3, v4}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 2482
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v4, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v4

    .line 2484
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v6

    .line 2485
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    const-string v4, "radioType"

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2486
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    iput v0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    .line 2488
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v7

    .line 2489
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    const-string v4, "naviExist"

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2490
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    iput v0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    .line 2492
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v5

    .line 2493
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    const-string v4, "connectType"

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2494
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    iput v0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->connectPos:I

    .line 2495
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    if-ne v0, v1, :cond_2

    .line 2496
    sput-boolean v2, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    .line 2501
    :goto_2
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/4 v4, 0x7

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v4

    .line 2502
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v3

    const-string v4, "usbNum"

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2503
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    iput v0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    .line 2505
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    if-nez v0, :cond_4

    .line 2506
    sget-object v3, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/16 v4, 0x8

    const/16 v0, 0x8

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v3, v4

    .line 2507
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v3

    const/16 v0, 0x8

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, v3, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    .line 2508
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v2, 0x7f0700a3

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2509
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->isAirhide:Z

    .line 2508
    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 2516
    :goto_4
    sget-object v1, Lcom/touchus/benchilauncher/SysConst;->storeData:[B

    const/16 v2, 0x9

    const/16 v0, 0x9

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v1, v2

    .line 2517
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    sget-object v2, Lcom/touchus/publicutils/sysconst/PubSysConst;->KEY_BREAKPOS:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putInt(Ljava/lang/String;I)V

    .line 2518
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v0, 0x9

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    iput v0, v1, Lcom/touchus/benchilauncher/LauncherApplication;->breakpos:I

    .line 2519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->breakpos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/touchus/publicutils/sysconst/PubSysConst;->GT9XX_INT_TRIGGER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/touchus/publicutils/utils/UtilTools;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2520
    return-void

    .line 2467
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isOriginalKeyOpen:Z

    goto/16 :goto_0

    .line 2479
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    goto/16 :goto_1

    .line 2498
    :cond_2
    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 2507
    goto/16 :goto_3

    .line 2511
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-boolean v2, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    .line 2512
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const v3, 0x7f0700a2

    invoke-virtual {v1, v3}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/utils/SpUtilK;->putBoolean(Ljava/lang/String;Z)V

    .line 2513
    const v0, 0x3e99999a    # 0.3f

    sput v0, Lcom/touchus/benchilauncher/SysConst;->musicDecVolume:F

    goto/16 :goto_4
.end method

.method public obtainVersionDate(Ljava/lang/String;)V
    .locals 3
    .param p1, "versionDate"    # Ljava/lang/String;

    .prologue
    .line 2037
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<<<obtainVersionDate\t\tversionDate--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2038
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2037
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2039
    return-void
.end method

.method public obtainVersionNumber(Ljava/lang/String;)V
    .locals 3
    .param p1, "versionNumber"    # Ljava/lang/String;

    .prologue
    .line 2043
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<<<obtainVersionNumber\t\tversionNumber--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2044
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2043
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2045
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-object p1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->curMcuVersion:Ljava/lang/String;

    .line 2046
    return-void
.end method

.method public onAUXActivateStutas(Lcom/backaudio/android/driver/Mainboard$EAUXStutas;)V
    .locals 4
    .param p1, "eStutas"    # Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    .prologue
    .line 2582
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2583
    .local v0, "bun":Landroid/os/Bundle;
    const-string v1, "aux_activate_stutas"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2584
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x418

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2586
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<<<\tonAUXActivateStutas: eStutas--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2587
    return-void
.end method

.method public onAirInfo(Lcom/backaudio/android/driver/beans/AirInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/backaudio/android/driver/beans/AirInfo;

    .prologue
    .line 2229
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput-object p1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->airInfo:Lcom/backaudio/android/driver/beans/AirInfo;

    .line 2230
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2231
    .local v0, "bun":Landroid/os/Bundle;
    const-string v1, "airInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2232
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x3f4

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2233
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "<<<onAirInfo\t\tAirInfo--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/AirInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2234
    return-void
.end method

.method public onCanboxInfo(Ljava/lang/String;)V
    .locals 3
    .param p1, "info"    # Ljava/lang/String;

    .prologue
    .line 2117
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<<<onCanboxInfo\t\tinfo--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2118
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iput-object p1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->curCanboxVersion:Ljava/lang/String;

    .line 2119
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/MainService;->access$39(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 2120
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->curCanboxVersion:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2141
    :goto_0
    return-void

    .line 2123
    :cond_0
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCanboxInfo before BenzModel.benzTpye = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2124
    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2123
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2125
    const-string v0, "BEZ"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-eq v0, v1, :cond_2

    .line 2126
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->XBS:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    .line 2140
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    const/16 v1, 0x467

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 2128
    :cond_2
    const-string v0, "YT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "YX"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2129
    :cond_3
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-eq v0, v1, :cond_4

    .line 2130
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    goto :goto_1

    .line 2132
    :cond_4
    const-string v0, "Benz"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-eq v0, v1, :cond_1

    .line 2134
    sget-object v0, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sput-object v0, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    goto :goto_1
.end method

.method public onCanboxUpgradeForGetDataByIndex(I)V
    .locals 8
    .param p1, "nextIndex"    # I

    .prologue
    .line 2069
    const-string v4, "driverlog"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "canbox:nextIndex:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, p1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2070
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v4

    const/16 v5, 0x463

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/MainService$Mhandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 2071
    .local v1, "msg":Landroid/os/Message;
    add-int/lit8 v4, p1, 0x1

    iput v4, v1, Landroid/os/Message;->arg1:I

    .line 2072
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 2073
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v6}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2074
    const-string v6, "<<<onCanboxUpgradeForGetDataByIndex\t\tnextIndex--"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 2075
    add-int/lit8 v6, p1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2073
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2076
    int-to-double v4, p1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v6

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v6}, Lcom/touchus/benchilauncher/MainService;->access$10(Lcom/touchus/benchilauncher/MainService;)I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 2077
    .local v2, "per":I
    const/16 v4, 0x64

    if-gt v2, v4, :cond_0

    .line 2078
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2079
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2080
    .local v3, "per_s":Ljava/lang/String;
    const-string v4, "upgradePer"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2081
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    const/16 v5, 0x459

    invoke-virtual {v4, v5, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2083
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "per_s":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCanboxUpgradeState(Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;)V
    .locals 5
    .param p1, "info"    # Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2051
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->READY_UPGRADING:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    if-ne p1, v1, :cond_0

    .line 2052
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v2, v2, Lcom/touchus/benchilauncher/MainService;->canboxtThread:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2053
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/touchus/benchilauncher/MainService;->access$37(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 2054
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v1

    const/16 v2, 0x463

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService$Mhandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 2055
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->arg1:I

    .line 2056
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2063
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "<<<onCanboxUpgradeState\t\tEUpgrade--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2064
    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2063
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2065
    return-void

    .line 2057
    :cond_0
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->FINISH:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    if-ne p1, v1, :cond_1

    .line 2058
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x461

    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2059
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1, v3}, Lcom/touchus/benchilauncher/MainService;->access$37(Lcom/touchus/benchilauncher/MainService;Z)V

    goto :goto_0

    .line 2061
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x469

    invoke-virtual {v1, v2, v4}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onCarBaseInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .prologue
    .line 2238
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput-object p1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->carBaseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .line 2239
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2240
    .local v0, "bun":Landroid/os/Bundle;
    const-string v1, "carBaseInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2241
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x3ef

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2243
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<<<onCarBaseInfo\t\tCarBaseInfo--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2244
    return-void
.end method

.method public onCarRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V
    .locals 4
    .param p1, "info"    # Lcom/backaudio/android/driver/beans/CarRunInfo;

    .prologue
    .line 2305
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput-object p1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->carRunInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    .line 2306
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2307
    .local v0, "bun":Landroid/os/Bundle;
    const-string v1, "runningState"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2308
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getCurSpeed()I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->speed:I

    .line 2309
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->getRevolutions()I

    move-result v2

    iput v2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->rSpeed:I

    .line 2310
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x3f0

    invoke-virtual {v1, v2, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2311
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<<<\tonCarRunningInfo:\tCarRunInfo--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2312
    return-void
.end method

.method public onEnterStandbyMode()V
    .locals 3

    .prologue
    .line 2023
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<<<onEnterStandbyMode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2024
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/MainService;->access$36(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 2025
    return-void
.end method

.method public onHandleIdriver(Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;)V
    .locals 8
    .param p1, "value"    # Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
    .param p2, "state"    # Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .prologue
    const v7, 0x7f070003

    const/16 v6, 0x1771

    .line 2146
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$11(Lcom/touchus/benchilauncher/MainService;)Landroid/widget/FrameLayout;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2225
    :cond_0
    :goto_0
    return-void

    .line 2150
    :cond_1
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    if-ne p2, v2, :cond_0

    .line 2153
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "<<<onHandleIdriver\t\tEIdriverEnum--"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2154
    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";\tEBtnStateEnum--"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2153
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2155
    const-string v2, "systemtime"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " recv "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2157
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->SPEECH:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_2

    .line 2158
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v2

    sget-object v3, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v2, v3}, Lcom/backaudio/android/driver/Mainboard;->showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    goto :goto_0

    .line 2160
    :cond_2
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->SPEAKOFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_3

    .line 2161
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v2

    sget-object v3, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-virtual {v2, v3}, Lcom/backaudio/android/driver/Mainboard;->showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    goto :goto_0

    .line 2165
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    if-eqz v2, :cond_0

    .line 2169
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    iget-boolean v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->iCurrentInLauncher:Z

    if-nez v2, :cond_11

    .line 2170
    const/4 v1, -0x1

    .line 2172
    .local v1, "code":I
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_5

    .line 2173
    const/16 v1, 0x15

    .line 2216
    :cond_4
    :goto_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2217
    invoke-static {v1}, Lcom/touchus/publicutils/utils/UtilTools;->sendKeyeventToSystem(I)V

    goto/16 :goto_0

    .line 2174
    :cond_5
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_6

    .line 2175
    const/16 v1, 0x15

    .line 2176
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$19(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    .line 2177
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v3, v7}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2176
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2177
    if-eqz v2, :cond_4

    .line 2178
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/MainService;->access$40(Lcom/touchus/benchilauncher/MainService;Z)V

    goto :goto_1

    .line 2180
    :cond_6
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_7

    .line 2181
    const/16 v1, 0x16

    .line 2182
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$19(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    .line 2183
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v3, v7}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2182
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2183
    if-eqz v2, :cond_4

    .line 2184
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/touchus/benchilauncher/MainService;->access$40(Lcom/touchus/benchilauncher/MainService;Z)V

    goto :goto_1

    .line 2186
    :cond_7
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_8

    .line 2187
    const/16 v1, 0x16

    .line 2188
    goto :goto_1

    :cond_8
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_9

    .line 2189
    const/16 v1, 0x13

    .line 2190
    goto :goto_1

    :cond_9
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_a

    .line 2191
    const/16 v1, 0x14

    .line 2192
    goto :goto_1

    :cond_a
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_b

    .line 2193
    invoke-static {}, Lcom/touchus/publicutils/utils/UtilTools;->isFastDoubleClick()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2196
    const/16 v1, 0x17

    .line 2197
    goto :goto_1

    :cond_b
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_c

    .line 2198
    const/4 v1, 0x4

    .line 2199
    goto :goto_1

    :cond_c
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_d

    .line 2200
    const/4 v1, 0x3

    .line 2201
    goto :goto_1

    :cond_d
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_e

    .line 2202
    const/4 v1, 0x3

    .line 2203
    goto :goto_1

    :cond_e
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NAVI:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_f

    .line 2204
    const/4 v1, 0x3

    .line 2205
    goto :goto_1

    :cond_f
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-eq p1, v2, :cond_10

    .line 2206
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    if-ne p1, v2, :cond_4

    .line 2207
    :cond_10
    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->bluetoothStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    if-eq v2, v3, :cond_0

    .line 2210
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/touchus/publicutils/utils/UtilTools;->sendKeyeventToSystem(I)V

    .line 2211
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2212
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 2213
    const-string v2, "idriver_state_enum"

    invoke-virtual {p2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 2214
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    invoke-virtual {v2, v6, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 2220
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "code":I
    :cond_11
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2221
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v2, "idriver_enum"

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 2222
    const-string v2, "idriver_state_enum"

    invoke-virtual {p2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->getCode()B

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 2223
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v2

    invoke-virtual {v2, v6, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public onHornSoundValue(IIII)V
    .locals 0
    .param p1, "fLeft"    # I
    .param p2, "fRight"    # I
    .param p3, "rLeft"    # I
    .param p4, "rRight"    # I

    .prologue
    .line 2451
    return-void
.end method

.method public onMcuUpgradeForGetDataByIndex(I)V
    .locals 8
    .param p1, "nextIndex"    # I

    .prologue
    .line 2101
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "driverlog"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mcu:nextIndex:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2102
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v4

    const/16 v5, 0x46d

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/MainService$Mhandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 2103
    .local v1, "msg":Landroid/os/Message;
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 2104
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 2106
    int-to-double v4, p1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v6

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v6}, Lcom/touchus/benchilauncher/MainService;->access$38(Lcom/touchus/benchilauncher/MainService;)I

    move-result v6

    int-to-double v6, v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 2107
    .local v2, "per":I
    const/16 v4, 0x64

    if-gt v2, v4, :cond_0

    .line 2108
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2109
    .local v0, "bundle":Landroid/os/Bundle;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2110
    .local v3, "per_s":Ljava/lang/String;
    const-string v4, "upgradePer"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v4

    const/16 v5, 0x459

    invoke-virtual {v4, v5, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 2113
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "per_s":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onMcuUpgradeState(Lcom/backaudio/android/driver/Mainboard$EUpgrade;)V
    .locals 4
    .param p1, "info"    # Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    .prologue
    const/4 v3, 0x0

    .line 2087
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->GET_HERDER:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    if-ne p1, v1, :cond_1

    .line 2088
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v1

    .line 2089
    const/16 v2, 0x46a

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService$Mhandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 2090
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2097
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 2091
    :cond_1
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->FINISH:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    if-ne p1, v1, :cond_2

    .line 2092
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x46b

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 2093
    :cond_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    if-ne p1, v1, :cond_0

    .line 2094
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x469

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onOriginalCarView(Lcom/backaudio/android/driver/Mainboard$EControlSource;Z)V
    .locals 5
    .param p1, "source"    # Lcom/backaudio/android/driver/Mainboard$EControlSource;
    .param p2, "iOriginal"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2272
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "<<<onOriginalCarView:\tsource--"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2273
    const-string v4, ";\tiOriginal--"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2274
    const-string v4, ",source == EControlSource.AUX = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2275
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->AUX:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    if-ne p1, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2272
    invoke-interface {v3, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2276
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->AUX:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    if-ne p1, v0, :cond_4

    .line 2277
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " isAUX = true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2278
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$14(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2279
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/MainService;->access$30(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 2280
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$32(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    if-nez v0, :cond_0

    .line 2281
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->interAndroidView()V

    .line 2284
    :cond_0
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel;->isBenzGLK()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2285
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$32(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2286
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v1, v1, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService$Mhandler;->post(Ljava/lang/Runnable;)Z

    .line 2301
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 2275
    goto :goto_0

    .line 2289
    :cond_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v0

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-object v1, v1, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService$Mhandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 2293
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$14(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2294
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$32(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->isTestOpen:Z

    if-nez v0, :cond_5

    .line 2295
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->interOriginalView()V

    .line 2297
    :cond_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0, v2}, Lcom/touchus/benchilauncher/MainService;->access$30(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 2299
    :cond_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " isAUX = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$14(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onShowOrHideCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V
    .locals 11
    .param p1, "eCarLayer"    # Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    .prologue
    const/16 v10, 0x17

    const/4 v9, 0x3

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 2316
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v0

    .line 2317
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<<<\tonShowOrHideCarLayer:\tECarLayer--"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2318
    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",onNoTouchLayout = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2319
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$41(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/views/MyLinearlayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2317
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2316
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2320
    sget-object v0, Lcom/touchus/benchilauncher/SysConst;->num:[I

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v1

    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->mediaVoice:Ljava/lang/String;

    invoke-virtual {v1, v2, v8}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v1

    aget v6, v0, v1

    .line 2322
    .local v6, "mediaVolume":I
    invoke-static {}, Lcom/touchus/benchilauncher/MainService$MainListenner;->$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2412
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    if-nez v0, :cond_1

    sget v0, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    if-eq v0, v10, :cond_1

    .line 2413
    sput v10, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 2414
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$42(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->ismix:Z

    if-nez v0, :cond_3

    .line 2415
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    .line 2417
    sget v1, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 2418
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v4

    .line 2419
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->naviVoice:Ljava/lang/String;

    .line 2418
    invoke-virtual {v4, v5, v9}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget v2, v2, v4

    move v4, v6

    move v5, v6

    .line 2416
    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 2429
    :goto_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$15(Lcom/touchus/benchilauncher/MainService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2430
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$16(Lcom/touchus/benchilauncher/MainService;)V

    .line 2433
    :cond_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    if-eqz v0, :cond_4

    .line 2434
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/MainService;->removeNoTouchScreens()V

    .line 2435
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->abandonDVAudioFocus()V

    .line 2445
    :goto_2
    return-void

    .line 2324
    :pswitch_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2325
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2326
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2327
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2328
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2329
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 2330
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    goto :goto_0

    .line 2333
    :pswitch_1
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2334
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2335
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2336
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2337
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2338
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 2339
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    goto/16 :goto_0

    .line 2342
    :pswitch_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2343
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2344
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2345
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    if-nez v0, :cond_2

    .line 2346
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    sget v1, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 2347
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v4

    const-string v5, "dvmun"

    invoke-virtual {v4, v5, v8}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget v2, v2, v4

    move v4, v3

    move v5, v3

    .line 2346
    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setDVSoundValue(IIIII)V

    .line 2349
    :cond_2
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2350
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2351
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 2352
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    goto/16 :goto_0

    .line 2355
    :pswitch_3
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2356
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2357
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2358
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2359
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2360
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 2361
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    goto/16 :goto_0

    .line 2374
    :pswitch_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2375
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2376
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2377
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2378
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2379
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 2380
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    .line 2381
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    if-eq v0, v1, :cond_0

    .line 2382
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v0

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->eMediaSet:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    if-ne v0, v1, :cond_0

    .line 2383
    sput v3, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 2384
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    .line 2385
    sget v1, Lcom/touchus/benchilauncher/SysConst;->basicNum:I

    .line 2386
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v4

    const-string v5, "mun"

    invoke-virtual {v4, v5, v8}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget v2, v2, v4

    move v4, v6

    move v5, v6

    .line 2384
    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    .line 2388
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$21(Lcom/touchus/benchilauncher/MainService;)V

    goto/16 :goto_0

    .line 2392
    :pswitch_5
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2393
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2394
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2395
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2396
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2397
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    .line 2398
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    goto/16 :goto_0

    .line 2401
    :pswitch_6
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 2402
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 2403
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 2404
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 2405
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 2406
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v7, v0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 2407
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    iput-boolean v3, v0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    goto/16 :goto_0

    .line 2422
    :cond_3
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    .line 2424
    sget v1, Lcom/touchus/benchilauncher/SysConst;->mediaBasicNum:I

    .line 2425
    sget-object v2, Lcom/touchus/benchilauncher/SysConst;->num:[I

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v4}, Lcom/touchus/benchilauncher/MainService;->access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;

    move-result-object v4

    .line 2426
    sget-object v5, Lcom/touchus/benchilauncher/SysConst;->naviVoice:Ljava/lang/String;

    .line 2425
    invoke-virtual {v4, v5, v9}, Lcom/touchus/benchilauncher/utils/SpUtilK;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget v2, v2, v4

    move v4, v6

    move v5, v6

    .line 2423
    invoke-virtual/range {v0 .. v5}, Lcom/backaudio/android/driver/Mainboard;->setAllHornSoundValue(IIIII)V

    goto/16 :goto_1

    .line 2437
    :cond_4
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;

    move-result-object v0

    new-instance v1, Lcom/touchus/benchilauncher/MainService$MainListenner$1;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$MainListenner$1;-><init>(Lcom/touchus/benchilauncher/MainService$MainListenner;)V

    .line 2443
    const-wide/16 v2, 0xc8

    .line 2437
    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 2322
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method public onTime(IIIIIII)V
    .locals 7
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "day"    # I
    .param p4, "timeFormat"    # I
    .param p5, "time"    # I
    .param p6, "min"    # I
    .param p7, "sec"    # I

    .prologue
    .line 2249
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v3}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "<<<settingSystemOfTime:\tyear--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2250
    const-string v3, ";\tmonth--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\tday--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\thour--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2251
    const-string v3, ";\tmin--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\tsecond--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\ttimeFormat--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2252
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2249
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2253
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->min:I

    if-ne v1, p6, :cond_0

    .line 2268
    :goto_0
    return-void

    .line 2257
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput p1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->year:I

    .line 2258
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput p2, v1, Lcom/touchus/benchilauncher/LauncherApplication;->month:I

    .line 2259
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput p3, v1, Lcom/touchus/benchilauncher/LauncherApplication;->day:I

    .line 2260
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput p5, v1, Lcom/touchus/benchilauncher/LauncherApplication;->hour:I

    .line 2261
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput p6, v1, Lcom/touchus/benchilauncher/LauncherApplication;->min:I

    .line 2262
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    iput p7, v1, Lcom/touchus/benchilauncher/LauncherApplication;->second:I

    .line 2263
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2264
    .local v0, "cal":Ljava/util/Calendar;
    add-int/lit8 v2, p2, -0x1

    move v1, p1

    move v3, p3

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 2265
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    .line 2267
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v1}, Lcom/touchus/benchilauncher/MainService;->access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;

    move-result-object v1

    const/16 v2, 0x3f1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onWakeUp(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V
    .locals 3
    .param p1, "eCarLayer"    # Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    .prologue
    .line 2029
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/touchus/benchilauncher/MainService;->access$36(Lcom/touchus/benchilauncher/MainService;Z)V

    .line 2030
    invoke-virtual {p0, p1}, Lcom/touchus/benchilauncher/MainService$MainListenner;->onShowOrHideCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    .line 2031
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lcom/touchus/benchilauncher/MainService;->access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService$MainListenner;->this$0:Lcom/touchus/benchilauncher/MainService;

    invoke-static {v2}, Lcom/touchus/benchilauncher/MainService;->access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "<<<onWakeUp ECarLayer = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2032
    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2031
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2033
    return-void
.end method
