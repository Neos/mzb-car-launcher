.class public Lcom/touchus/benchilauncher/MainService;
.super Landroid/app/Service;
.source "MainService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/touchus/benchilauncher/MainService$GpsLocationListener;,
        Lcom/touchus/benchilauncher/MainService$MainBinder;,
        Lcom/touchus/benchilauncher/MainService$MainListenner;,
        Lcom/touchus/benchilauncher/MainService$Mhandler;
    }
.end annotation


# static fields
.field public static curChannel:Ljava/lang/String;


# instance fields
.field private MYLOG:Ljava/lang/String;

.field public adapter:Landroid/bluetooth/BluetoothAdapter;

.field private airplaneRunnable:Ljava/lang/Runnable;

.field private animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

.field private app:Lcom/touchus/benchilauncher/LauncherApplication;

.field public audioManager:Landroid/media/AudioManager;

.field private benzA2dp:Landroid/bluetooth/BluetoothA2dp;

.field private benzDevice:Landroid/bluetooth/BluetoothDevice;

.field private benzhHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private bluetoothReceiver:Landroid/content/BroadcastReceiver;

.field canboxtThread:Ljava/lang/Thread;

.field private carBTname:Ljava/lang/String;

.field public checkNavigationRunnable:Ljava/lang/Runnable;

.field private easyConnectReceiver:Landroid/content/BroadcastReceiver;

.field endTime:J

.field private getConnectCanboxState:Ljava/lang/Runnable;

.field private getWeatherReceiver:Landroid/content/BroadcastReceiver;

.field private getWeatherRunnabale:Ljava/lang/Runnable;

.field private gpsLocatListener:Lcom/touchus/benchilauncher/MainService$GpsLocationListener;

.field private gpsManager:Landroid/location/LocationManager;

.field private gpsStateListener:Landroid/location/GpsStatus$Listener;

.field private iAUX:Z

.field private iBenzA2dpConnected:Z

.field private iConnectCanbox:Z

.field iExist:Z

.field iGetWeather:Z

.field private iInAccState:Z

.field public iIsBTConnect:Z

.field public iIsDV:Z

.field private iIsEnterAirplaneMode:Z

.field public iIsInAndroid:Z

.field public iIsInOriginal:Z

.field public iIsInRecorder:Z

.field public iIsInReversing:Z

.field public iIsScrennOff:Z

.field private iNaviSound:Z

.field private iReadyUpdate:Z

.field private lastAirplaneTime:J

.field private loadingFloatLayout:Landroid/widget/FrameLayout;

.field private logger:Lorg/slf4j/Logger;

.field mActivityManager:Landroid/app/ActivityManager;

.field public mBinder:Lcom/touchus/benchilauncher/MainService$MainBinder;

.field private mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

.field private mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

.field private mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field public mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

.field private mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mcuTotalDataIndex:I

.field public mcuUpgradeByteData:[B

.field private networkReceiver:Landroid/content/BroadcastReceiver;

.field private noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

.field private recoveryReceiver:Landroid/content/BroadcastReceiver;

.field public removeLoadingRunable:Ljava/lang/Runnable;

.field private spaceTime:I

.field private startSettingRunnable:Ljava/lang/Runnable;

.field startTime:J

.field private testBTname:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private totalDataIndex:I

.field private unibroadAudioFocusReceiver:Landroid/content/BroadcastReceiver;

.field private upgradeByteData:[B


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, -0x3e7

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 102
    const-class v0, Lcom/touchus/benchilauncher/MainService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->logger:Lorg/slf4j/Logger;

    .line 105
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iInAccState:Z

    .line 110
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    .line 111
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iConnectCanbox:Z

    .line 208
    new-instance v0, Lcom/touchus/benchilauncher/MainService$1;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$1;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->getConnectCanboxState:Ljava/lang/Runnable;

    .line 220
    new-instance v0, Lcom/touchus/benchilauncher/MainService$2;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$2;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->startSettingRunnable:Ljava/lang/Runnable;

    .line 280
    iput-object v3, p0, Lcom/touchus/benchilauncher/MainService;->upgradeByteData:[B

    .line 327
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iReadyUpdate:Z

    .line 329
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/MainService$3;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$3;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->canboxtThread:Ljava/lang/Thread;

    .line 341
    iput-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    .line 342
    iput v4, p0, Lcom/touchus/benchilauncher/MainService;->mcuTotalDataIndex:I

    .line 579
    new-instance v0, Lcom/touchus/benchilauncher/MainService$4;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$4;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    .line 668
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/touchus/benchilauncher/MainService;->lastAirplaneTime:J

    .line 669
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsEnterAirplaneMode:Z

    .line 670
    const/16 v0, 0x2ee0

    iput v0, p0, Lcom/touchus/benchilauncher/MainService;->spaceTime:I

    .line 671
    new-instance v0, Lcom/touchus/benchilauncher/MainService$5;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$5;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->airplaneRunnable:Ljava/lang/Runnable;

    .line 962
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iGetWeather:Z

    .line 963
    new-instance v0, Lcom/touchus/benchilauncher/MainService$6;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$6;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    .line 981
    new-instance v0, Lcom/touchus/benchilauncher/MainService$7;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$7;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherReceiver:Landroid/content/BroadcastReceiver;

    .line 997
    new-instance v0, Lcom/touchus/benchilauncher/MainService$8;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$8;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->networkReceiver:Landroid/content/BroadcastReceiver;

    .line 1027
    new-instance v0, Lcom/touchus/benchilauncher/MainService$9;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$9;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->recoveryReceiver:Landroid/content/BroadcastReceiver;

    .line 1081
    new-instance v0, Lcom/touchus/benchilauncher/MainService$10;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$10;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->easyConnectReceiver:Landroid/content/BroadcastReceiver;

    .line 1132
    new-instance v0, Lcom/touchus/benchilauncher/MainService$11;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$11;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->checkNavigationRunnable:Ljava/lang/Runnable;

    .line 1218
    new-instance v0, Lcom/touchus/benchilauncher/MainService$12;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$12;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    .line 1381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->testBTname:Ljava/util/List;

    .line 1382
    const-string v0, "MB Bluetooth"

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->carBTname:Ljava/lang/String;

    .line 1387
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iBenzA2dpConnected:Z

    .line 1426
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iExist:Z

    .line 1483
    new-instance v0, Lcom/touchus/benchilauncher/MainService$13;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$13;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 1549
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iNaviSound:Z

    .line 1602
    new-instance v0, Lcom/touchus/benchilauncher/MainService$14;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$14;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->unibroadAudioFocusReceiver:Landroid/content/BroadcastReceiver;

    .line 1811
    new-instance v0, Lcom/touchus/benchilauncher/MainService$GpsLocationListener;

    invoke-direct {v0, p0, v3}, Lcom/touchus/benchilauncher/MainService$GpsLocationListener;-><init>(Lcom/touchus/benchilauncher/MainService;Lcom/touchus/benchilauncher/MainService$GpsLocationListener;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsLocatListener:Lcom/touchus/benchilauncher/MainService$GpsLocationListener;

    .line 1812
    new-instance v0, Lcom/touchus/benchilauncher/MainService$15;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$15;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsStateListener:Landroid/location/GpsStatus$Listener;

    .line 1884
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iIsInAndroid:Z

    .line 1885
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsInReversing:Z

    .line 1886
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    .line 1887
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsInRecorder:Z

    .line 1888
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsDV:Z

    .line 1889
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsBTConnect:Z

    .line 1890
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    .line 1995
    iput v4, p0, Lcom/touchus/benchilauncher/MainService;->totalDataIndex:I

    .line 1997
    new-instance v0, Lcom/touchus/benchilauncher/MainService$Mhandler;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$Mhandler;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    .line 2017
    const-string v0, "listenlogread"

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->MYLOG:Ljava/lang/String;

    .line 2610
    new-instance v0, Lcom/touchus/benchilauncher/MainService$MainBinder;

    invoke-direct {v0, p0}, Lcom/touchus/benchilauncher/MainService$MainBinder;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mBinder:Lcom/touchus/benchilauncher/MainService$MainBinder;

    .line 100
    return-void
.end method

.method private GLK_BTConnect()V
    .locals 5

    .prologue
    .line 1429
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getIsBTState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1449
    :cond_0
    :goto_0
    return-void

    .line 1432
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    .line 1433
    .local v1, "listSet":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1445
    :goto_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 1446
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iExist:Z

    .line 1447
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    goto :goto_0

    .line 1433
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1434
    .local v0, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->carBTname:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1435
    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    .line 1436
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "btstate benzDevice111 = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1437
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getBTA2dpState()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1438
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "btstate a2dp connect = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothA2dp;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    .line 1441
    :cond_4
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iExist:Z

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/touchus/benchilauncher/MainService;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iConnectCanbox:Z

    return v0
.end method

.method static synthetic access$1(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;
    .locals 1

    .prologue
    .line 1997
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    return-object v0
.end method

.method static synthetic access$10(Lcom/touchus/benchilauncher/MainService;)I
    .locals 1

    .prologue
    .line 1995
    iget v0, p0, Lcom/touchus/benchilauncher/MainService;->totalDataIndex:I

    return v0
.end method

.method static synthetic access$11(Lcom/touchus/benchilauncher/MainService;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$12(Lcom/touchus/benchilauncher/MainService;)Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->logger:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$13(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2017
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->MYLOG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$14(Lcom/touchus/benchilauncher/MainService;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    return v0
.end method

.method static synthetic access$15(Lcom/touchus/benchilauncher/MainService;)Z
    .locals 1

    .prologue
    .line 1451
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getBTA2dpState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$16(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1797
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->setConnectedMusicVoice()V

    return-void
.end method

.method static synthetic access$17(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 694
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->dealwithAirplaneMode()V

    return-void
.end method

.method static synthetic access$18(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 963
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$19(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 952
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getCurrentTopAppPkg()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->getConnectCanboxState:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$20(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 1387
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/MainService;->iBenzA2dpConnected:Z

    return-void
.end method

.method static synthetic access$21(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1805
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->setMusicMute()V

    return-void
.end method

.method static synthetic access$22(Lcom/touchus/benchilauncher/MainService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1382
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->carBTname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$23(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 1383
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    return-void
.end method

.method static synthetic access$24(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 1383
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$25(Lcom/touchus/benchilauncher/MainService;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1381
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->testBTname:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$26(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothHeadset;
    .locals 1

    .prologue
    .line 1385
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzhHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object v0
.end method

.method static synthetic access$27(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothA2dp;
    .locals 1

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    return-object v0
.end method

.method static synthetic access$28(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothHeadset;)V
    .locals 0

    .prologue
    .line 1385
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService;->benzhHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-void
.end method

.method static synthetic access$29(Lcom/touchus/benchilauncher/MainService;Landroid/bluetooth/BluetoothA2dp;)V
    .locals 0

    .prologue
    .line 1384
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    return-void
.end method

.method static synthetic access$3(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 679
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->setAirplaneMode(Z)V

    return-void
.end method

.method static synthetic access$30(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    return-void
.end method

.method static synthetic access$31(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 1582
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->openOrCloseRelay(Z)V

    return-void
.end method

.method static synthetic access$32(Lcom/touchus/benchilauncher/MainService;)Z
    .locals 1

    .prologue
    .line 1471
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getIsBTState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$33(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 1549
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/MainService;->iNaviSound:Z

    return-void
.end method

.method static synthetic access$34(Lcom/touchus/benchilauncher/MainService;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 1810
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$35(Lcom/touchus/benchilauncher/MainService;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 1893
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->handlerMsg(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$36(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 725
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->inOrOutAcc(Z)V

    return-void
.end method

.method static synthetic access$37(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 327
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/MainService;->iReadyUpdate:Z

    return-void
.end method

.method static synthetic access$38(Lcom/touchus/benchilauncher/MainService;)I
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lcom/touchus/benchilauncher/MainService;->mcuTotalDataIndex:I

    return v0
.end method

.method static synthetic access$39(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/MainService;->iConnectCanbox:Z

    return-void
.end method

.method static synthetic access$4(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 706
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->setGpsStatus(Z)V

    return-void
.end method

.method static synthetic access$40(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 2601
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->decMap(Z)V

    return-void
.end method

.method static synthetic access$41(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/views/MyLinearlayout;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    return-object v0
.end method

.method static synthetic access$42(Lcom/touchus/benchilauncher/MainService;)Z
    .locals 1

    .prologue
    .line 1549
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iNaviSound:Z

    return v0
.end method

.method static synthetic access$43(Lcom/touchus/benchilauncher/MainService;[B)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService;->upgradeByteData:[B

    return-void
.end method

.method static synthetic access$44(Lcom/touchus/benchilauncher/MainService;)[B
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->upgradeByteData:[B

    return-object v0
.end method

.method static synthetic access$45(Lcom/touchus/benchilauncher/MainService;I)V
    .locals 0

    .prologue
    .line 1995
    iput p1, p0, Lcom/touchus/benchilauncher/MainService;->totalDataIndex:I

    return-void
.end method

.method static synthetic access$46(Lcom/touchus/benchilauncher/MainService;I)V
    .locals 0

    .prologue
    .line 342
    iput p1, p0, Lcom/touchus/benchilauncher/MainService;->mcuTotalDataIndex:I

    return-void
.end method

.method static synthetic access$47(Lcom/touchus/benchilauncher/MainService;Landroid/graphics/drawable/AnimationDrawable;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/touchus/benchilauncher/MainService;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    return-void
.end method

.method static synthetic access$48(Lcom/touchus/benchilauncher/MainService;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method

.method static synthetic access$49(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/views/DesktopLayout;
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$50(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 809
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->autoStartApp()V

    return-void
.end method

.method static synthetic access$51(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1428
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->GLK_BTConnect()V

    return-void
.end method

.method static synthetic access$52(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1551
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->openRelay()V

    return-void
.end method

.method static synthetic access$53(Lcom/touchus/benchilauncher/MainService;Z)V
    .locals 0

    .prologue
    .line 1568
    invoke-direct {p0, p1}, Lcom/touchus/benchilauncher/MainService;->closeRelay(Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    return-object v0
.end method

.method static synthetic access$7(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->addBluetoothReceiver()V

    return-void
.end method

.method static synthetic access$8(Lcom/touchus/benchilauncher/MainService;)V
    .locals 0

    .prologue
    .line 1161
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->startLocalBT()V

    return-void
.end method

.method static synthetic access$9(Lcom/touchus/benchilauncher/MainService;)Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iReadyUpdate:Z

    return v0
.end method

.method private addBluetoothReceiver()V
    .locals 2

    .prologue
    .line 931
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 932
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 933
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 934
    const-string v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 935
    const-string v1, "android.bluetooth.device.action.PAIRING_CANCEL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 936
    const-string v1, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 937
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 938
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 939
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/MainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 940
    return-void
.end method

.method private addEasyConnectReceiver()V
    .locals 2

    .prologue
    .line 943
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 944
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "net.easyconn.bt.checkstatus"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 945
    const-string v1, "net.easyconn.a2dp.acquire"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 946
    const-string v1, "net.easyconn.a2dp.release"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 947
    const-string v1, "net.easyconn.app.quit"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 949
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->easyConnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/MainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 950
    return-void
.end method

.method private addNetworkReceiver()V
    .locals 2

    .prologue
    .line 905
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 906
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 907
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 908
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->networkReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/MainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 909
    return-void
.end method

.method private addRecoveryReceiver()V
    .locals 2

    .prologue
    .line 921
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 922
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.touchus.factorytest.recovery"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 923
    const-string v1, "com.touchus.factorytest.sdcardreset"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 924
    const-string v1, "com.touchus.factorytest.getMcu"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 925
    const-string v1, "com.touchus.factorytest.benzetype"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 926
    sget-object v1, Lcom/touchus/publicutils/sysconst/PubSysConst;->ACTION_FACTORY_BREAKSET:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 927
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->recoveryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/MainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 928
    return-void
.end method

.method private addUnibroadAudioFocusReceiver()V
    .locals 2

    .prologue
    .line 912
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 913
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.unibroad.AudioFocus"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 914
    const-string v1, "com.unibroad.AudioFocus.REGAIN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 917
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->unibroadAudioFocusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/MainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 918
    return-void
.end method

.method private addWeatherReceiver()V
    .locals 2

    .prologue
    .line 899
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 900
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.unibroad.weatherdata"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 901
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/touchus/benchilauncher/MainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 902
    return-void
.end method

.method private autoStartApp()V
    .locals 3

    .prologue
    .line 810
    const v1, 0x7f070004

    invoke-virtual {p0, v1}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 811
    .local v0, "tempPkgs":Ljava/lang/String;
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->startAppByPkg(Ljava/lang/String;)Z

    .line 812
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setNeedStartNaviPkg(Z)V

    .line 813
    return-void
.end method

.method private autoStartNaviAPP()V
    .locals 6

    .prologue
    .line 794
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->getNeedStartNaviPkgs()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 796
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    new-instance v2, Lcom/touchus/benchilauncher/MainService$23;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/MainService$23;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    .line 802
    const-wide/16 v4, 0x1f4

    .line 796
    invoke-virtual {v1, v2, v4, v5}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 807
    :cond_0
    :goto_0
    return-void

    .line 803
    :catch_0
    move-exception v0

    .line 804
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private broadcastEnterstandbyMode()V
    .locals 2

    .prologue
    .line 720
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 721
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.mcu.enterStandbyMode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 722
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 723
    return-void
.end method

.method private broadcastWakeUpEvent()V
    .locals 2

    .prologue
    .line 712
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 713
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.mcu.wakeUp"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 714
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 715
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 717
    return-void
.end method

.method private closeRelay(Z)V
    .locals 3
    .param p1, "iForceClose"    # Z

    .prologue
    .line 1570
    if-nez p1, :cond_0

    .line 1571
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/MainService;->iNaviSound:Z

    if-eqz v1, :cond_0

    .line 1580
    :goto_0
    return-void

    .line 1576
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/backaudio/android/driver/Mainboard;->openOrCloseRelay(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1577
    :catch_0
    move-exception v0

    .line 1578
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private connect(Landroid/bluetooth/BluetoothA2dp;)V
    .locals 2
    .param p1, "a2dp"    # Landroid/bluetooth/BluetoothA2dp;

    .prologue
    .line 1515
    if-nez p1, :cond_1

    .line 1516
    const-string v0, ""

    const-string v1, "btstate a2dp null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1531
    :cond_0
    :goto_0
    return-void

    .line 1528
    :cond_1
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iIsInOriginal:Z

    if-nez v0, :cond_0

    .line 1529
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->changeAudioSource()V

    goto :goto_0
.end method

.method private dealwithAirplaneMode()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 695
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/touchus/benchilauncher/MainService;->lastAirplaneTime:J

    .line 696
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 697
    const-string v4, "airplane_mode_on"

    .line 696
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 698
    .local v1, "isAirplaneMode":I
    if-ne v1, v0, :cond_2

    .line 699
    .local v0, "find":Z
    :goto_0
    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsEnterAirplaneMode:Z

    if-nez v2, :cond_1

    :cond_0
    if-nez v0, :cond_3

    iget-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iIsEnterAirplaneMode:Z

    if-nez v2, :cond_3

    .line 703
    :cond_1
    :goto_1
    return-void

    .end local v0    # "find":Z
    :cond_2
    move v0, v2

    .line 698
    goto :goto_0

    .line 702
    .restart local v0    # "find":Z
    :cond_3
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-boolean v3, p0, Lcom/touchus/benchilauncher/MainService;->iIsEnterAirplaneMode:Z

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/LauncherApplication;->changeAirplane(Z)V

    goto :goto_1
.end method

.method private decMap(Z)V
    .locals 4
    .param p1, "isdec"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2602
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2603
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "AUTONAVI_STANDARD_BROADCAST_RECV"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2604
    const-string v2, "KEY_TYPE"

    const/16 v3, 0x272b

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2605
    const-string v2, "EXTRA_TYPE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2606
    const-string v2, "EXTRA_OPERA"

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2607
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 2608
    return-void

    .line 2606
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private enterAcc()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 735
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard;->readyToStandby()V

    .line 736
    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    if-eqz v1, :cond_1

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 739
    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    .line 741
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iNaviSound:Z

    .line 742
    iput-boolean v2, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    .line 743
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->dismissDialog()V

    .line 745
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-boolean v2, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/service/BluetoothService;->setBTEnterACC(Z)V

    .line 746
    const-string v1, "ENTERACC"

    const-string v2, "enterAcc"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_2

    .line 749
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->removeLoadingFloatView()V

    .line 751
    :cond_2
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 753
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Lcom/touchus/benchilauncher/LauncherApplication;->requestMainAudioFocus()V

    .line 756
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->broadcastEnterstandbyMode()V

    .line 758
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->checkHadNaviRunning()Z

    .line 760
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->stopOtherAPP()V

    .line 762
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->launcherHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 763
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->launcherHandler:Landroid/os/Handler;

    .line 764
    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 765
    .local v0, "mes":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method private getBTA2dpState()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1452
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->logger:Lorg/slf4j/Logger;

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "btstate SysConst.isBT = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",iAUX = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1453
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1454
    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    .line 1461
    :cond_0
    :goto_0
    return v0

    .line 1456
    :cond_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v1, :cond_0

    .line 1459
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "btstate iBenzA2dpConnected = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/touchus/benchilauncher/MainService;->iBenzA2dpConnected:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1460
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "btstate  A2dpState = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothA2dp;->getState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1461
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/MainService;->iBenzA2dpConnected:Z

    .line 1462
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothA2dp;->getState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const/4 v0, 0x1

    .line 1461
    :cond_2
    or-int/2addr v0, v1

    goto :goto_0
.end method

.method private getBTHeadsetState()I
    .locals 4

    .prologue
    .line 1477
    const-string v0, ""

    .line 1478
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "btstate  HeadsetState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1479
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->benzhHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1478
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1477
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzhHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0
.end method

.method private getCurrentTopAppPkg()Ljava/lang/String;
    .locals 4

    .prologue
    .line 953
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mActivityManager:Landroid/app/ActivityManager;

    .line 954
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 955
    .local v0, "runningTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 956
    :cond_0
    const-string v1, ""

    .line 959
    :goto_0
    return-object v1

    .line 958
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 959
    .local v1, "topPkg":Ljava/lang/String;
    goto :goto_0
.end method

.method private getIsBTState()Z
    .locals 1

    .prologue
    .line 1473
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getBTA2dpState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handlerMsg(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v11, 0x80

    .line 1894
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x7d1

    if-ne v9, v10, :cond_1

    .line 1895
    const v9, 0x7f070070

    invoke-virtual {p0, v9}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/touchus/benchilauncher/MainService;->createLoadingFloatView(Ljava/lang/String;)V

    .line 1896
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    mul-int/lit16 v8, v9, 0x3e8

    .line 1897
    .local v8, "time":I
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    int-to-long v12, v8

    invoke-virtual {v9, v10, v12, v13}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1993
    .end local v8    # "time":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1898
    :cond_1
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x1771

    if-ne v9, v10, :cond_3

    .line 1899
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 1900
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v9, "idriver_enum"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v2

    .line 1901
    .local v2, "code":B
    const-string v9, "idriver_state_enum"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v6

    .line 1902
    .local v6, "state":B
    sget-object v9, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v9}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v9

    if-ne v2, v9, :cond_0

    .line 1903
    sget-object v9, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-virtual {v9}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v9

    if-ne v6, v9, :cond_0

    .line 1904
    iget-boolean v9, p0, Lcom/touchus/benchilauncher/MainService;->iInAccState:Z

    if-nez v9, :cond_2

    .line 1905
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/touchus/benchilauncher/MainService;->iInAccState:Z

    .line 1906
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v9

    invoke-virtual {v9}, Lcom/backaudio/android/driver/Mainboard;->enterIntoAcc()V

    goto :goto_0

    .line 1908
    :cond_2
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/touchus/benchilauncher/MainService;->iInAccState:Z

    .line 1909
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v9

    invoke-virtual {v9}, Lcom/backaudio/android/driver/Mainboard;->wakeupMcu()V

    goto :goto_0

    .line 1912
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "code":B
    .end local v6    # "state":B
    :cond_3
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x463

    if-ne v9, v10, :cond_6

    .line 1913
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 1914
    .local v3, "index":I
    mul-int/lit16 v4, v3, 0x80

    .line 1915
    .local v4, "offset":I
    iget v9, p0, Lcom/touchus/benchilauncher/MainService;->totalDataIndex:I

    if-lt v3, v9, :cond_4

    .line 1916
    const/4 v3, -0x1

    .line 1917
    goto :goto_0

    .line 1919
    :cond_4
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1920
    .local v7, "tempBuffer":Ljava/io/ByteArrayOutputStream;
    iget v9, p0, Lcom/touchus/benchilauncher/MainService;->totalDataIndex:I

    add-int/lit8 v9, v9, -0x1

    if-ge v3, v9, :cond_5

    .line 1921
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->upgradeByteData:[B

    invoke-virtual {v7, v9, v4, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1926
    :goto_1
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v9

    .line 1927
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 1926
    invoke-virtual {v9, v3, v10}, Lcom/backaudio/android/driver/Mainboard;->sendUpdateCanboxInfoByIndex(I[B)V

    goto :goto_0

    .line 1923
    :cond_5
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->upgradeByteData:[B

    .line 1924
    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService;->upgradeByteData:[B

    array-length v10, v10

    sub-int/2addr v10, v4

    .line 1923
    invoke-virtual {v7, v9, v4, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 1928
    .end local v3    # "index":I
    .end local v4    # "offset":I
    .end local v7    # "tempBuffer":Ljava/io/ByteArrayOutputStream;
    :cond_6
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x46a

    if-ne v9, v10, :cond_7

    .line 1929
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v9

    iget v10, p0, Lcom/touchus/benchilauncher/MainService;->mcuTotalDataIndex:I

    invoke-virtual {v9, v10}, Lcom/backaudio/android/driver/Mainboard;->sendUpgradeMcuHeaderInfo(I)V

    goto/16 :goto_0

    .line 1930
    :cond_7
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x46d

    if-ne v9, v10, :cond_a

    .line 1931
    iget v3, p1, Landroid/os/Message;->arg1:I

    .line 1932
    .restart local v3    # "index":I
    mul-int/lit16 v4, v3, 0x80

    .line 1933
    .restart local v4    # "offset":I
    iget v9, p0, Lcom/touchus/benchilauncher/MainService;->mcuTotalDataIndex:I

    if-lt v3, v9, :cond_8

    .line 1934
    const/4 v3, 0x0

    .line 1935
    goto/16 :goto_0

    .line 1937
    :cond_8
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1938
    .restart local v7    # "tempBuffer":Ljava/io/ByteArrayOutputStream;
    iget v9, p0, Lcom/touchus/benchilauncher/MainService;->mcuTotalDataIndex:I

    add-int/lit8 v9, v9, -0x1

    if-ge v3, v9, :cond_9

    .line 1939
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    invoke-virtual {v7, v9, v4, v11}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1944
    :goto_2
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v9

    .line 1945
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 1944
    invoke-virtual {v9, v3, v10}, Lcom/backaudio/android/driver/Mainboard;->sendUpdateMcuInfoByIndex(I[B)V

    goto/16 :goto_0

    .line 1941
    :cond_9
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    .line 1942
    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService;->mcuUpgradeByteData:[B

    array-length v10, v10

    sub-int/2addr v10, v4

    .line 1941
    invoke-virtual {v7, v9, v4, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2

    .line 1946
    .end local v3    # "index":I
    .end local v4    # "offset":I
    .end local v7    # "tempBuffer":Ljava/io/ByteArrayOutputStream;
    :cond_a
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x409

    if-eq v9, v10, :cond_0

    .line 1947
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x40c

    if-ne v9, v10, :cond_b

    .line 1948
    const/4 v9, 0x3

    invoke-static {v9}, Lcom/touchus/publicutils/utils/UtilTools;->sendKeyeventToSystem(I)V

    .line 1949
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1950
    .local v5, "pos":I
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    new-instance v10, Lcom/touchus/benchilauncher/MainService$27;

    invoke-direct {v10, p0, v5}, Lcom/touchus/benchilauncher/MainService$27;-><init>(Lcom/touchus/benchilauncher/MainService;I)V

    .line 1958
    const-wide/16 v12, 0xc8

    .line 1950
    invoke-virtual {v9, v10, v12, v13}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1960
    .end local v5    # "pos":I
    :cond_b
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x411

    if-ne v9, v10, :cond_d

    .line 1961
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v9, :cond_0

    .line 1964
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "btstate a2dp iBenzA2dpConnected = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v11, p0, Lcom/touchus/benchilauncher/MainService;->iBenzA2dpConnected:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1965
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getBTA2dpState()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1966
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v10, p0, Lcom/touchus/benchilauncher/MainService;->removeLoadingRunable:Ljava/lang/Runnable;

    invoke-virtual {v9, v10}, Lcom/touchus/benchilauncher/MainService$Mhandler;->post(Ljava/lang/Runnable;)Z

    .line 1967
    invoke-static {}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->getInstance()Lcom/touchus/benchilauncher/views/BTConnectDialog;

    move-result-object v9

    invoke-virtual {v9}, Lcom/touchus/benchilauncher/views/BTConnectDialog;->dissShow()V

    .line 1968
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->setConnectedMusicVoice()V

    goto/16 :goto_0

    .line 1971
    :cond_c
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->setMusicMute()V

    goto/16 :goto_0

    .line 1974
    :cond_d
    iget v9, p1, Landroid/os/Message;->what:I

    const/16 v10, 0x412

    if-ne v9, v10, :cond_0

    .line 1975
    iget-object v9, p0, Lcom/touchus/benchilauncher/MainService;->benzhHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v9, :cond_0

    .line 1978
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getBTHeadsetState()I

    move-result v0

    .line 1979
    .local v0, "btHeadsetStatus":I
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 1982
    :pswitch_1
    const-string v9, ""

    const-string v10, "btstate headset RECONNECTED"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1979
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized inOrOutAcc(Z)V
    .locals 3
    .param p1, "iEnterAcc"    # Z

    .prologue
    .line 726
    monitor-enter p0

    :try_start_0
    const-string v0, "mcu"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reciev:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    if-eqz p1, :cond_0

    .line 728
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->enterAcc()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 732
    :goto_0
    monitor-exit p0

    return-void

    .line 730
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->wakeUpFromAcc()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 726
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private initMcuAndCanbox()V
    .locals 4

    .prologue
    .line 186
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/MainService$16;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$16;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 202
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/touchus/benchilauncher/MainService;->iConnectCanbox:Z

    .line 205
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getConnectCanboxState:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 206
    return-void
.end method

.method private openOrCloseRelay(Z)V
    .locals 4
    .param p1, "iOpen"    # Z

    .prologue
    .line 1583
    if-eqz p1, :cond_0

    .line 1584
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    new-instance v1, Lcom/touchus/benchilauncher/MainService$25;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$25;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    .line 1590
    const-wide/16 v2, 0xc8

    .line 1584
    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1600
    :goto_0
    return-void

    .line 1592
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    new-instance v1, Lcom/touchus/benchilauncher/MainService$26;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$26;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    .line 1598
    const-wide/16 v2, 0x64

    .line 1592
    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private openRelay()V
    .locals 3

    .prologue
    .line 1552
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/MainService;->iIsScrennOff:Z

    if-eqz v1, :cond_1

    .line 1566
    :cond_0
    :goto_0
    return-void

    .line 1556
    :cond_1
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/MainService;->iNaviSound:Z

    if-eqz v1, :cond_0

    .line 1562
    :try_start_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/backaudio/android/driver/Mainboard;->openOrCloseRelay(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1563
    :catch_0
    move-exception v0

    .line 1564
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private registerGPSListener()V
    .locals 6

    .prologue
    .line 1851
    .line 1852
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 1851
    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsManager:Landroid/location/LocationManager;

    .line 1853
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x7d0

    .line 1854
    const/high16 v4, 0x40c00000    # 6.0f

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->gpsLocatListener:Lcom/touchus/benchilauncher/MainService$GpsLocationListener;

    .line 1853
    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 1855
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->gpsStateListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    .line 1856
    return-void
.end method

.method private requestBtAudio(Z)V
    .locals 7
    .param p1, "isForce"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1212
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    .line 1213
    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel;->benzTpye:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v3, v3, Lcom/touchus/benchilauncher/LauncherApplication;->radioPos:I

    if-nez v3, :cond_1

    move v3, v1

    .line 1214
    :goto_0
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v4, v4, Lcom/touchus/benchilauncher/LauncherApplication;->naviPos:I

    if-nez v4, :cond_2

    move v4, v5

    :goto_1
    sget-boolean v6, Lcom/touchus/benchilauncher/LauncherApplication;->isBT:Z

    if-eqz v6, :cond_0

    move v5, v1

    .line 1215
    :cond_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->usbPos:I

    add-int/lit8 v6, v1, 0x1

    move v1, p1

    .line 1212
    invoke-virtual/range {v0 .. v6}, Lcom/backaudio/android/driver/Mainboard;->forceRequestBTAudio(ZLcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;ZZZI)V

    .line 1216
    return-void

    :cond_1
    move v3, v5

    .line 1213
    goto :goto_0

    :cond_2
    move v4, v1

    .line 1214
    goto :goto_1
.end method

.method private declared-synchronized setAirplaneMode(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 680
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/touchus/benchilauncher/MainService;->iIsEnterAirplaneMode:Z

    .line 681
    iget-wide v2, p0, Lcom/touchus/benchilauncher/MainService;->lastAirplaneTime:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 682
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->dealwithAirplaneMode()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 692
    :goto_0
    monitor-exit p0

    return-void

    .line 684
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/touchus/benchilauncher/MainService;->lastAirplaneTime:J

    sub-long v0, v2, v4

    .line 685
    .local v0, "temp":J
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->airplaneRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 686
    iget v2, p0, Lcom/touchus/benchilauncher/MainService;->spaceTime:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 687
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->dealwithAirplaneMode()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 680
    .end local v0    # "temp":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 689
    .restart local v0    # "temp":J
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->airplaneRunnable:Ljava/lang/Runnable;

    iget v4, p0, Lcom/touchus/benchilauncher/MainService;->spaceTime:I

    int-to-long v4, v4

    sub-long/2addr v4, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private setConnectedMusicVoice()V
    .locals 4

    .prologue
    const/16 v3, 0xc8

    const/4 v2, 0x0

    .line 1798
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1799
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    iget-boolean v0, v0, Lcom/touchus/benchilauncher/service/BluetoothService;->blueMusicFocus:Z

    if-eqz v0, :cond_0

    .line 1800
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v3, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1804
    :goto_0
    return-void

    .line 1802
    :cond_0
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    goto :goto_0
.end method

.method private setGpsStatus(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 708
    const-string v1, "gps"

    .line 707
    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 709
    return-void
.end method

.method private setMusicMute()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1806
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1807
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 1808
    return-void
.end method

.method private startLocalBT()V
    .locals 6

    .prologue
    .line 1162
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v2, :cond_0

    .line 1189
    :goto_0
    return-void

    .line 1165
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1166
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1167
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    .line 1169
    :cond_1
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 v3, 0x17

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(I)Z

    .line 1170
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "btstate isEnabled = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    .line 1172
    .local v1, "listSet":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1185
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 1186
    const/4 v4, 0x2

    .line 1185
    invoke-virtual {v2, p0, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 1187
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 1188
    const/4 v4, 0x1

    .line 1187
    invoke-virtual {v2, p0, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    goto :goto_0

    .line 1172
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1176
    .local v0, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->carBTname:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1177
    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    .line 1178
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "btstate benzDevice = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1180
    :cond_4
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->testBTname:Ljava/util/List;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1181
    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    .line 1182
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "btstate benzDevice = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->benzDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private unregisterGPSListener()V
    .locals 2

    .prologue
    .line 1859
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->gpsStateListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 1860
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->gpsManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->gpsLocatListener:Lcom/touchus/benchilauncher/MainService$GpsLocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 1861
    return-void
.end method

.method private wakeUpFromAcc()V
    .locals 4

    .prologue
    .line 770
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0}, Lcom/backaudio/android/driver/Mainboard;->readyToWork()V

    .line 771
    sget-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    if-nez v0, :cond_0

    .line 788
    :goto_0
    return-void

    .line 774
    :cond_0
    const-string v0, "ENTERACC"

    const-string v1, "wakeUpFromAcc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    const/4 v0, 0x0

    sput-boolean v0, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    .line 777
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v0, v0, Lcom/touchus/benchilauncher/LauncherApplication;->btservice:Lcom/touchus/benchilauncher/service/BluetoothService;

    sget-boolean v1, Lcom/touchus/benchilauncher/LauncherApplication;->iAccOff:Z

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/service/BluetoothService;->setBTEnterACC(Z)V

    .line 778
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->initModel()V

    .line 779
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->startSettingRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 780
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->initMcuAndCanbox()V

    .line 781
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 782
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    const-wide/32 v2, 0x9c40

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 784
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->autoStartNaviAPP()V

    .line 785
    const-string v0, "ENTERACC"

    const-string v1, "wakeUpFromAcc end"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->broadcastWakeUpEvent()V

    goto :goto_0
.end method


# virtual methods
.method public addMusicReceiver()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 866
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    if-eqz v2, :cond_0

    .line 886
    :goto_0
    return-void

    .line 869
    :cond_0
    new-instance v2, Landroid/content/ComponentName;

    .line 870
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/touchus/benchilauncher/receiver/MusicControlReceiver;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    iput-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    .line 871
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "musicControl addMusicReceiver "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 879
    .local v0, "mediaButtonIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 880
    invoke-static {p0, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 885
    .local v1, "pi":Landroid/app/PendingIntent;
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->audioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    invoke-virtual {v2, v1, v3}, Landroid/media/AudioManager;->registerMediaButtonIntent(Landroid/app/PendingIntent;Landroid/content/ComponentName;)V

    goto :goto_0
.end method

.method public btMusicConnect()V
    .locals 6

    .prologue
    .line 1394
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->screenPos:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 1424
    :cond_0
    :goto_0
    return-void

    .line 1397
    :cond_1
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1398
    const-string v1, ""

    const-string v2, "Audio type is BT"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1399
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1400
    :cond_2
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->startLocalBT()V

    .line 1405
    :goto_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    new-instance v2, Lcom/touchus/benchilauncher/MainService$24;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/MainService$24;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    .line 1411
    const-wide/16 v4, 0x7530

    .line 1405
    invoke-virtual {v1, v2, v4, v5}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1412
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->benzA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/MainService;->connect(Landroid/bluetooth/BluetoothA2dp;)V

    goto :goto_0

    .line 1402
    :cond_3
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->adapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(I)Z

    goto :goto_1

    .line 1414
    :cond_4
    const-string v1, ""

    const-string v2, "Audio type is AUX"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1415
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    if-nez v1, :cond_0

    .line 1416
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1417
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0x7d1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1418
    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1419
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/MainService$Mhandler;->sendMessage(Landroid/os/Message;)Z

    .line 1420
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/MainService;->requestBtAudio(Z)V

    goto :goto_0
.end method

.method public changeAudioSource()V
    .locals 4

    .prologue
    const/16 v3, 0x28

    .line 1192
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1193
    .local v0, "message":Landroid/os/Message;
    const/16 v1, 0x7d1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1194
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->getBTA2dpState()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1195
    iget-boolean v1, p0, Lcom/touchus/benchilauncher/MainService;->iAUX:Z

    if-nez v1, :cond_0

    .line 1196
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v1, v2, :cond_1

    .line 1197
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1201
    :goto_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/MainService$Mhandler;->sendMessage(Landroid/os/Message;)Z

    .line 1202
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/MainService;->requestBtAudio(Z)V

    .line 1209
    :cond_0
    :goto_1
    return-void

    .line 1199
    :cond_1
    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 1205
    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1206
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    invoke-virtual {v1, v0}, Lcom/touchus/benchilauncher/MainService$Mhandler;->sendMessage(Landroid/os/Message;)Z

    .line 1207
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/touchus/benchilauncher/MainService;->requestBtAudio(Z)V

    goto :goto_1
.end method

.method public checkHadNaviRunning()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 846
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->mActivityManager:Landroid/app/ActivityManager;

    .line 847
    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 848
    .local v3, "runningTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v3, :cond_0

    move v1, v4

    .line 859
    :goto_0
    return v1

    .line 851
    :cond_0
    const/4 v0, 0x0

    .line 852
    .local v0, "cmpNameTemp":Ljava/lang/String;
    const v5, 0x7f070004

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 853
    .local v2, "navi":Ljava/lang/String;
    const/4 v1, 0x0

    .line 854
    .local v1, "isHasNavi":Z
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v0

    .line 855
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 856
    const/4 v1, 0x1

    .line 858
    :cond_1
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v4, v1}, Lcom/touchus/benchilauncher/LauncherApplication;->setNeedStartNaviPkg(Z)V

    goto :goto_0
.end method

.method public closeNaviThread()V
    .locals 2

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->checkNavigationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1125
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->removeDesk()V

    .line 1126
    return-void
.end method

.method public createDeskFloat()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 609
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    if-eqz v2, :cond_0

    .line 649
    :goto_0
    return-void

    .line 612
    :cond_0
    new-instance v2, Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-direct {v2, p0}, Lcom/touchus/benchilauncher/views/DesktopLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    .line 614
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 615
    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 614
    iput-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    .line 617
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 619
    .local v1, "mLayout":Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x7d3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 622
    const v2, 0x40028

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 626
    const/4 v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 628
    const/16 v2, 0x53

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 630
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 631
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 632
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    new-instance v3, Lcom/touchus/benchilauncher/MainService$22;

    invoke-direct {v3, p0}, Lcom/touchus/benchilauncher/MainService$22;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-virtual {v2, v3}, Lcom/touchus/benchilauncher/views/DesktopLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 644
    :try_start_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 645
    :catch_0
    move-exception v0

    .line 646
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "launcherlog"

    .line 647
    const-string v3, "canbox:error:mWindowManager.addView(mDesktopLayout)"

    .line 646
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public createLoadingFloatView(Ljava/lang/String;)V
    .locals 10
    .param p1, "showText"    # Ljava/lang/String;

    .prologue
    .line 406
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    if-eqz v5, :cond_0

    .line 467
    :goto_0
    return-void

    .line 409
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 410
    const v5, 0x7f07006e

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 412
    :cond_1
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    if-nez v5, :cond_2

    .line 413
    const-string v5, "window"

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/MainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    iput-object v5, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    .line 415
    :cond_2
    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v4}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 417
    .local v4, "wmParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x7d3

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 419
    const/4 v5, 0x1

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 420
    const/high16 v5, 0x20000

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 422
    const/16 v5, 0x33

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 424
    const/16 v5, 0x2a5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 425
    const/16 v5, 0xa0

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 427
    const/16 v5, 0x19f

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 428
    const/16 v5, 0x93

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 430
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getApplication()Landroid/app/Application;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 433
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f03003f

    const/4 v6, 0x0

    .line 432
    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    iput-object v5, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    .line 434
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    .line 435
    const v6, 0x7f0b015d

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 434
    check-cast v3, Landroid/widget/TextView;

    .line 436
    .local v3, "showTview":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    .line 437
    const v6, 0x7f0b015f

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 436
    check-cast v1, Landroid/widget/ImageView;

    .line 438
    .local v1, "loadingImg":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    .line 439
    const v6, 0x7f0b0160

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 438
    check-cast v2, Landroid/widget/TextView;

    .line 440
    .local v2, "manual":Landroid/widget/TextView;
    const v5, 0x7f070070

    invoke-virtual {p0, v5}, Lcom/touchus/benchilauncher/MainService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 441
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 453
    :goto_1
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 456
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v6, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    invoke-interface {v5, v6, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 457
    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    new-instance v6, Lcom/touchus/benchilauncher/MainService$20;

    invoke-direct {v6, p0, v1}, Lcom/touchus/benchilauncher/MainService$20;-><init>(Lcom/touchus/benchilauncher/MainService;Landroid/widget/ImageView;)V

    .line 466
    const-wide/16 v8, 0x64

    .line 457
    invoke-virtual {v5, v6, v8, v9}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 443
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 444
    new-instance v5, Lcom/touchus/benchilauncher/MainService$19;

    invoke-direct {v5, p0}, Lcom/touchus/benchilauncher/MainService$19;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public createNoTouchScreens()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 470
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    if-eqz v4, :cond_0

    .line 542
    :goto_0
    return-void

    .line 476
    :cond_0
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    if-nez v4, :cond_1

    .line 477
    const-string v4, "window"

    invoke-virtual {p0, v4}, Lcom/touchus/benchilauncher/MainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    iput-object v4, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    .line 479
    :cond_1
    new-instance v3, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v3}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 481
    .local v3, "wmParams":Landroid/view/WindowManager$LayoutParams;
    const/16 v4, 0x7d2

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 483
    iput v10, v3, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 484
    const/16 v4, 0x28

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 487
    const/16 v4, 0x33

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 489
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 490
    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 492
    const/16 v4, 0x500

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 493
    const/16 v4, 0x1e0

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 495
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 498
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03003c

    const/4 v5, 0x0

    .line 497
    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/touchus/benchilauncher/views/MyLinearlayout;

    iput-object v4, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    .line 500
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    new-instance v5, Lcom/touchus/benchilauncher/MainService$21;

    invoke-direct {v5, p0}, Lcom/touchus/benchilauncher/MainService$21;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-virtual {v4, v5}, Lcom/touchus/benchilauncher/views/MyLinearlayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 530
    const-string v4, ""

    .line 531
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "listenlog creat noTouchLayout  = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 532
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/touchus/benchilauncher/MainService;->startTime:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 531
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 530
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :try_start_0
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    invoke-interface {v4, v5, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 539
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 540
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "isRed"

    invoke-virtual {v0, v4, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 541
    iget-object v4, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/16 v5, 0x414

    invoke-virtual {v4, v5, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 535
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 536
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "launcherlog"

    .line 537
    const-string v5, "canbox:error:mWindowManager.addView(onNoTouchLayout)"

    .line 536
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public enterIntoUpgradeCanboxCheck()V
    .locals 2

    .prologue
    .line 283
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/MainService$17;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$17;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 324
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 325
    return-void
.end method

.method public enterIntoUpgradeMcuCheck()V
    .locals 2

    .prologue
    .line 345
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/touchus/benchilauncher/MainService$18;

    invoke-direct {v1, p0}, Lcom/touchus/benchilauncher/MainService$18;-><init>(Lcom/touchus/benchilauncher/MainService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 391
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 392
    return-void
.end method

.method public forceStopApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v0, p1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    .line 1547
    return-void
.end method

.method public getWeather()V
    .locals 3

    .prologue
    .line 975
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 976
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.unibroad.getweather"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 977
    const-string v1, "cityName"

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v2, v2, Lcom/touchus/benchilauncher/LauncherApplication;->gpsCityName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 978
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 979
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2615
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mBinder:Lcom/touchus/benchilauncher/MainService$MainBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 122
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 123
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/touchus/benchilauncher/LauncherApplication;

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    .line 124
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iput-object v1, v0, Lcom/touchus/benchilauncher/LauncherApplication;->serviceHandler:Landroid/os/Handler;

    .line 125
    new-instance v0, Lcom/touchus/benchilauncher/utils/SpUtilK;

    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/touchus/benchilauncher/utils/SpUtilK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mSpUtilK:Lcom/touchus/benchilauncher/utils/SpUtilK;

    .line 126
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->audioManager:Landroid/media/AudioManager;

    .line 127
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mActivityManager:Landroid/app/ActivityManager;

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/touchus/benchilauncher/MainService;->startTime:J

    .line 129
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "listenlog kill KernelMCU  = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/touchus/benchilauncher/MainService;->startTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-static {}, Lcom/touchus/benchilauncher/utils/Utiltools;->stopKernelMCU()V

    .line 133
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->addWeatherReceiver()V

    .line 134
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->addNetworkReceiver()V

    .line 135
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->addEasyConnectReceiver()V

    .line 136
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->addUnibroadAudioFocusReceiver()V

    .line 137
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->addRecoveryReceiver()V

    .line 138
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->addMusicReceiver()V

    .line 139
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->registerGPSListener()V

    .line 141
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "listenlog initMCU  = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 142
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/touchus/benchilauncher/MainService;->startTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/touchus/benchilauncher/MainService;->startTime:J

    .line 145
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->broadcastWakeUpEvent()V

    .line 146
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->initMcuAndCanbox()V

    .line 148
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->requestMainAudioFocus()V

    .line 150
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->startSettingRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 151
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 155
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/touchus/benchilauncher/LauncherApplication;->setTypeMute(IZ)V

    .line 160
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->getWifiApState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-static {p0}, Lcom/touchus/benchilauncher/utils/WifiTool;->getCurrentWifiApConfig(Landroid/content/Context;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/touchus/benchilauncher/utils/WifiTool;->openWifiAp(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;)V

    .line 163
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    iget v1, v1, Lcom/touchus/benchilauncher/LauncherApplication;->breakpos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/touchus/publicutils/sysconst/PubSysConst;->GT9XX_INT_TRIGGER:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/touchus/publicutils/utils/UtilTools;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 165
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->testBTname:Ljava/util/List;

    const-string v1, "A2"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->testBTname:Ljava/util/List;

    const-string v1, "icar_500724"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 172
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->networkReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 173
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->unibroadAudioFocusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 174
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->recoveryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 175
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->easyConnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 176
    invoke-static {}, Lcom/touchus/benchilauncher/SysConst;->isBT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->bluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->unregisterMusicReceiver()V

    .line 180
    invoke-direct {p0}, Lcom/touchus/benchilauncher/MainService;->unregisterGPSListener()V

    .line 181
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    invoke-virtual {v0}, Lcom/touchus/benchilauncher/LauncherApplication;->abandonMainAudioFocus()V

    .line 182
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 183
    return-void
.end method

.method public openNaviThread()V
    .locals 4

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->checkNavigationRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1130
    return-void
.end method

.method public regainWeather()V
    .locals 4

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/touchus/benchilauncher/MainService$Mhandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1024
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mHandler:Lcom/touchus/benchilauncher/MainService$Mhandler;

    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->getWeatherRunnabale:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/touchus/benchilauncher/MainService$Mhandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1025
    return-void
.end method

.method public removeDesk()V
    .locals 3

    .prologue
    .line 655
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    if-nez v1, :cond_0

    .line 665
    :goto_0
    return-void

    .line 659
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 660
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mDesktopLayout:Lcom/touchus/benchilauncher/views/DesktopLayout;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 661
    :catch_0
    move-exception v0

    .line 662
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "launcherlog"

    .line 663
    const-string v2, "canbox:error:mWindowManager.removeView(mDesktopLayout)"

    .line 662
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeLoadingFloatView()V
    .locals 3

    .prologue
    .line 561
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    if-nez v1, :cond_0

    .line 577
    :goto_0
    return-void

    .line 565
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 566
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/touchus/benchilauncher/MainService;->animationDrawable:Landroid/graphics/drawable/AnimationDrawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 571
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 572
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/touchus/benchilauncher/MainService;->loadingFloatLayout:Landroid/widget/FrameLayout;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 573
    :catch_0
    move-exception v0

    .line 574
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "launcherlog"

    .line 575
    const-string v2, "canbox:error:mWindowManager.removeView(bootFloatLayout)"

    .line 574
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 567
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 568
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "launcherlog"

    const-string v2, "canbox:error:animationDrawable.stop()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeNoTouchScreens()V
    .locals 4

    .prologue
    .line 545
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    if-nez v2, :cond_0

    .line 558
    :goto_0
    return-void

    .line 549
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 555
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "isRed"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 556
    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->app:Lcom/touchus/benchilauncher/LauncherApplication;

    const/16 v3, 0x414

    invoke-virtual {v2, v3, v0}, Lcom/touchus/benchilauncher/LauncherApplication;->sendMessage(ILandroid/os/Bundle;)V

    .line 557
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/touchus/benchilauncher/MainService;->noTouchLayout:Lcom/touchus/benchilauncher/views/MyLinearlayout;

    goto :goto_0

    .line 550
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 551
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "launcherlog"

    .line 552
    const-string v3, "canbox:error:mWindowManager.removeView(onNoTouchLayout)"

    .line 551
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public sendEasyConnectBroadcast(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 1115
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1116
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1117
    const-string v1, "net.easyconn.bt.opened"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1118
    const-string v1, "name"

    sget-object v2, Lcom/touchus/benchilauncher/service/BluetoothService;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1120
    :cond_0
    invoke-virtual {p0, v0}, Lcom/touchus/benchilauncher/MainService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1121
    return-void
.end method

.method public stopOtherAPP()V
    .locals 6

    .prologue
    .line 821
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mActivityManager:Landroid/app/ActivityManager;

    .line 822
    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 823
    .local v2, "runningTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v2, :cond_1

    .line 837
    :cond_0
    return-void

    .line 826
    :cond_1
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "enteracc start stop app = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 828
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 829
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 830
    .local v0, "cmpNameTemp":Ljava/lang/String;
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "enteracc start stop cmpNameTemp = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 832
    invoke-virtual {p0}, Lcom/touchus/benchilauncher/MainService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 833
    iget-object v3, p0, Lcom/touchus/benchilauncher/MainService;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v3, v0}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    .line 827
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public unregisterMusicReceiver()V
    .locals 3

    .prologue
    .line 889
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    .line 896
    :goto_0
    return-void

    .line 892
    :cond_0
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "musicControl unregisterMusicReceiver "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    iget-object v0, p0, Lcom/touchus/benchilauncher/MainService;->audioManager:Landroid/media/AudioManager;

    .line 894
    iget-object v1, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 895
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/touchus/benchilauncher/MainService;->mRemoteControlClientReceiverComponent:Landroid/content/ComponentName;

    goto :goto_0
.end method
