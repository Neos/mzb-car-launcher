.class public final enum Lcom/backaudio/android/driver/Mainboard$EControlSource;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EControlSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EControlSource;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUX:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum BT:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum CAMERA:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum DISC:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum DVR:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum GPS:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum MENU:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum MOBLIE_WEB:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum RADIO:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum SD:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum TPMS:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum TV:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum USB:Lcom/backaudio/android/driver/Mainboard$EControlSource;

.field public static final enum iPod:Lcom/backaudio/android/driver/Mainboard$EControlSource;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1456
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "POWER_OFF"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "RADIO"

    invoke-direct {v0, v1, v5, v5}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->RADIO:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "DISC"

    invoke-direct {v0, v1, v6, v6}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->DISC:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "TV"

    invoke-direct {v0, v1, v7, v7}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->TV:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "GPS"

    invoke-direct {v0, v1, v8, v8}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->GPS:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "BT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->BT:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "iPod"

    const/4 v2, 0x6

    .line 1457
    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->iPod:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "AUX"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->AUX:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "USB"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->USB:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "SD"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->SD:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "DVR"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->DVR:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "MENU"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->MENU:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "CAMERA"

    const/16 v2, 0xc

    .line 1458
    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->CAMERA:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "TPMS"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->TPMS:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    const-string v1, "MOBLIE_WEB"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->MOBLIE_WEB:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    .line 1455
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EControlSource;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EControlSource;->POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EControlSource;->RADIO:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EControlSource;->DISC:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EControlSource;->TV:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v1, v0, v7

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EControlSource;->GPS:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->BT:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->iPod:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->AUX:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->USB:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->SD:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->DVR:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->MENU:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->CAMERA:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->TPMS:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->MOBLIE_WEB:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EControlSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 1462
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1463
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->code:I

    .line 1464
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EControlSource;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EControlSource;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EControlSource;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EControlSource;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1467
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EControlSource;->code:I

    int-to-byte v0, v0

    return v0
.end method
