.class public final enum Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECanboxUpgrade"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

.field public static final enum ERROR:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

.field public static final enum FINISH:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

.field public static final enum READY_UPGRADING:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1555
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v2, v2}, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    const-string v1, "READY_UPGRADING"

    invoke-direct {v0, v1, v3, v3}, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->READY_UPGRADING:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    const-string v1, "FINISH"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->FINISH:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    .line 1554
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->READY_UPGRADING:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->FINISH:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    aput-object v1, v0, v4

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 1559
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1560
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->code:I

    .line 1561
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1564
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->code:I

    int-to-byte v0, v0

    return v0
.end method
