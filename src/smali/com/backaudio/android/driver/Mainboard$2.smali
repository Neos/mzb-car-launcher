.class Lcom/backaudio/android/driver/Mainboard$2;
.super Ljava/lang/Object;
.source "Mainboard.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/backaudio/android/driver/Mainboard;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/backaudio/android/driver/Mainboard;


# direct methods
.method constructor <init>(Lcom/backaudio/android/driver/Mainboard;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/backaudio/android/driver/Mainboard$2;->this$0:Lcom/backaudio/android/driver/Mainboard;

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 116
    const/16 v3, -0x13

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    .line 117
    const/16 v3, 0xc8

    new-array v0, v3, [B

    .line 118
    .local v0, "buffer":[B
    :goto_0
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard$2;->this$0:Lcom/backaudio/android/driver/Mainboard;

    invoke-static {v3}, Lcom/backaudio/android/driver/Mainboard;->access$0(Lcom/backaudio/android/driver/Mainboard;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 133
    return-void

    .line 121
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard$2;->this$0:Lcom/backaudio/android/driver/Mainboard;

    invoke-static {v3}, Lcom/backaudio/android/driver/Mainboard;->access$2(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/SerialPortMainboardIO;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/backaudio/android/driver/SerialPortMainboardIO;->read([B)I

    move-result v2

    .line 123
    .local v2, "size":I
    if-lez v2, :cond_1

    .line 125
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard$2;->this$0:Lcom/backaudio/android/driver/Mainboard;

    invoke-static {v3}, Lcom/backaudio/android/driver/Mainboard;->access$3(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, v2}, Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;->push([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 129
    .end local v2    # "size":I
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 127
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "size":I
    :cond_1
    const-wide/16 v4, 0x2

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
