.class public final enum Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EBtnStateEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

.field public static final enum BTN_LONG_PRESS:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

.field public static final enum BTN_UP:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1527
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    const-string v1, "BTN_UP"

    invoke-direct {v0, v1, v2, v2}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_UP:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    const-string v1, "BTN_DOWN"

    invoke-direct {v0, v1, v3, v3}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    const-string v1, "BTN_LONG_PRESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_LONG_PRESS:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 1526
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_UP:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_LONG_PRESS:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    aput-object v1, v0, v4

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 1531
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1532
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->code:I

    .line 1533
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1536
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->code:I

    int-to-byte v0, v0

    return v0
.end method
