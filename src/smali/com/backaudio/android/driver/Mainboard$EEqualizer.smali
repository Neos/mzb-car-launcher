.class public final enum Lcom/backaudio/android/driver/Mainboard$EEqualizer;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EEqualizer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EEqualizer;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BASS:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EEqualizer;

.field public static final enum MIDTONES:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

.field public static final enum TREBLE:Lcom/backaudio/android/driver/Mainboard$EEqualizer;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1513
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    const-string v1, "TREBLE"

    invoke-direct {v0, v1, v2, v4}, Lcom/backaudio/android/driver/Mainboard$EEqualizer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->TREBLE:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    const-string v1, "MIDTONES"

    invoke-direct {v0, v1, v3, v3}, Lcom/backaudio/android/driver/Mainboard$EEqualizer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->MIDTONES:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    const-string v1, "BASS"

    invoke-direct {v0, v1, v4, v2}, Lcom/backaudio/android/driver/Mainboard$EEqualizer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->BASS:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    .line 1512
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->TREBLE:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->MIDTONES:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->BASS:Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    aput-object v1, v0, v4

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 1517
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1518
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->code:I

    .line 1519
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EEqualizer;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EEqualizer;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EEqualizer;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1522
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EEqualizer;->code:I

    int-to-byte v0, v0

    return v0
.end method
