.class public final enum Lcom/backaudio/android/driver/Mainboard$EAUXStutas;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EAUXStutas"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EAUXStutas;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTIVATING:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

.field public static final enum FAILED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

.field public static final enum SUCCEED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1612
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    const-string v1, "ACTIVATING"

    invoke-direct {v0, v1, v2, v2}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ACTIVATING:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    const-string v1, "SUCCEED"

    invoke-direct {v0, v1, v3, v3}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->SUCCEED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->FAILED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    .line 1611
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ACTIVATING:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->SUCCEED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->FAILED:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    aput-object v1, v0, v4

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 1616
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1617
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->code:I

    .line 1618
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EAUXStutas;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EAUXStutas;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1621
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->code:I

    int-to-byte v0, v0

    return v0
.end method
