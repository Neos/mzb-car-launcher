.class public interface abstract Lcom/backaudio/android/driver/IMainboardEventLisenner;
.super Ljava/lang/Object;
.source "IMainboardEventLisenner.java"


# virtual methods
.method public abstract logcatCanbox(Ljava/lang/String;)V
.end method

.method public abstract obtainBenzSize(I)V
.end method

.method public abstract obtainBenzType(Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;)V
.end method

.method public abstract obtainBrightness(I)V
.end method

.method public abstract obtainLanguageMediaSet(Lcom/backaudio/android/driver/Mainboard$ELanguage;)V
.end method

.method public abstract obtainReverseMediaSet(Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;)V
.end method

.method public abstract obtainReverseType(Lcom/backaudio/android/driver/Mainboard$EReverseTpye;)V
.end method

.method public abstract obtainStoreData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract obtainVersionDate(Ljava/lang/String;)V
.end method

.method public abstract obtainVersionNumber(Ljava/lang/String;)V
.end method

.method public abstract onAUXActivateStutas(Lcom/backaudio/android/driver/Mainboard$EAUXStutas;)V
.end method

.method public abstract onAirInfo(Lcom/backaudio/android/driver/beans/AirInfo;)V
.end method

.method public abstract onCanboxInfo(Ljava/lang/String;)V
.end method

.method public abstract onCanboxUpgradeForGetDataByIndex(I)V
.end method

.method public abstract onCanboxUpgradeState(Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;)V
.end method

.method public abstract onCarBaseInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V
.end method

.method public abstract onCarRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V
.end method

.method public abstract onEnterStandbyMode()V
.end method

.method public abstract onHandleIdriver(Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;)V
.end method

.method public abstract onHornSoundValue(IIII)V
.end method

.method public abstract onMcuUpgradeForGetDataByIndex(I)V
.end method

.method public abstract onMcuUpgradeState(Lcom/backaudio/android/driver/Mainboard$EUpgrade;)V
.end method

.method public abstract onOriginalCarView(Lcom/backaudio/android/driver/Mainboard$EControlSource;Z)V
.end method

.method public abstract onShowOrHideCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V
.end method

.method public abstract onTime(IIIIIII)V
.end method

.method public abstract onWakeUp(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V
.end method
