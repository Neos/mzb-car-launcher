.class public Lcom/backaudio/android/driver/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static AUDIO_SOURCE:Ljava/lang/String; = null

.field public static PATH:Ljava/lang/String; = null

.field public static final TIME_OUT:I = 0x3e8

.field public static VIDEO_SOURCE:Ljava/lang/String; = null

.field public static final aux_connect_flag:Ljava/lang/String; = "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

.field public static final collison_happen:Ljava/lang/String; = "/sys/bus/i2c/drivers/DA380/da380_flag"

.field public static final collison_level:Ljava/lang/String; = "/sys/bus/i2c/drivers/DA380/da380_sensitivity"

.field private static final droid_up:Ljava/lang/String; = "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_droid_up"

.field private static flag:Z = false

.field public static final gpio_radar:Ljava/lang/String; = "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_rada"

.field private static path:Ljava/lang/String; = null

.field public static final radio_frequency:Ljava/lang/String; = "/sys/bus/i2c/drivers/qn8027/qn8027_mode"

.field public static final usb_protocol:Ljava/lang/String; = "/sys/bus/platform/drivers/mt_usb/mt_usb/usb_protocol"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/unibroad/driver"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/backaudio/android/driver/Utils;->PATH:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/backaudio/android/driver/Utils;->PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/audio_source.prop"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/backaudio/android/driver/Utils;->AUDIO_SOURCE:Ljava/lang/String;

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/backaudio/android/driver/Utils;->PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/video_source.prop"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/backaudio/android/driver/Utils;->VIDEO_SOURCE:Ljava/lang/String;

    .line 368
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/backaudio/android/driver/Utils;->path:Ljava/lang/String;

    .line 389
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bash(Ljava/lang/String;)V
    .locals 10
    .param p0, "cmdLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 278
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    const/4 v7, 0x0

    .line 279
    new-instance v8, Ljava/io/File;

    const-string v9, "/system/bin"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 278
    invoke-virtual {v6, p0, v7, v8}, Ljava/lang/Runtime;->exec(Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;

    move-result-object v3

    .line 280
    .local v3, "process":Ljava/lang/Process;
    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 281
    .local v2, "input":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 283
    .local v4, "reader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 284
    .local v0, "buffer":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    .line 285
    invoke-virtual {v3}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 284
    invoke-direct {v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v5, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 289
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 290
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 293
    invoke-virtual {v3}, Ljava/lang/Process;->waitFor()I

    .line 294
    invoke-virtual {v3}, Ljava/lang/Process;->exitValue()I

    move-result v1

    .line 295
    .local v1, "exitValue":I
    if-eqz v1, :cond_6

    .line 296
    new-instance v6, Ljava/lang/Exception;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cmd ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]exit:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 298
    .end local v1    # "exitValue":I
    :catch_0
    move-exception v6

    .line 301
    :goto_2
    if-eqz v2, :cond_0

    .line 302
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 304
    :cond_0
    if-eqz v4, :cond_1

    .line 305
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 308
    :cond_1
    :goto_3
    return-void

    .line 287
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 298
    :catch_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 291
    :cond_3
    :try_start_4
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 300
    :catchall_0
    move-exception v6

    .line 301
    :goto_4
    if-eqz v2, :cond_4

    .line 302
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 304
    :cond_4
    if-eqz v4, :cond_5

    .line 305
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 307
    :cond_5
    throw v6

    .line 301
    .restart local v1    # "exitValue":I
    :cond_6
    if-eqz v2, :cond_7

    .line 302
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 304
    :cond_7
    if-eqz v4, :cond_1

    .line 305
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    goto :goto_3

    .line 300
    .end local v1    # "exitValue":I
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    goto :goto_4
.end method

.method public static byteArrayToHexString([B)Ljava/lang/String;
    .locals 6
    .param p0, "byteArray"    # [B

    .prologue
    .line 172
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 173
    .local v2, "stringBuilder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_0

    array-length v4, p0

    if-gtz v4, :cond_1

    .line 174
    :cond_0
    const/4 v4, 0x0

    .line 184
    :goto_0
    return-object v4

    .line 176
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, p0

    if-lt v1, v4, :cond_2

    .line 184
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 177
    :cond_2
    aget-byte v4, p0, v1

    and-int/lit16 v3, v4, 0xff

    .line 178
    .local v3, "v":I
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "hv":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_3

    .line 180
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 182
    :cond_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static byteArrayToHexString([BII)Ljava/lang/String;
    .locals 7
    .param p0, "byteArray"    # [B
    .param p1, "index"    # I
    .param p2, "len"    # I

    .prologue
    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, ""

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 157
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_0

    array-length v5, p0

    if-gtz v5, :cond_1

    .line 158
    :cond_0
    const/4 v5, 0x0

    .line 168
    :goto_0
    return-object v5

    .line 160
    :cond_1
    move v1, p1

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v5, p0

    if-ge v1, v5, :cond_2

    if-lt v2, p2, :cond_3

    .line 168
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 161
    :cond_3
    aget-byte v5, p0, v1

    and-int/lit16 v4, v5, 0xff

    .line 162
    .local v4, "v":I
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "hv":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_4

    .line 164
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 166
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static calcCheckSum([B)[B
    .locals 5
    .param p0, "buffer"    # [B

    .prologue
    .line 195
    array-length v2, p0

    const/4 v3, 0x5

    if-ge v2, v3, :cond_0

    .line 196
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid buffer length:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 197
    invoke-static {p0}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 196
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 199
    :cond_0
    const/4 v2, 0x2

    aget-byte v0, p0, v2

    .line 200
    .local v0, "check":B
    const/4 v1, 0x3

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_1

    .line 203
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    aput-byte v0, p0, v2

    .line 204
    return-object p0

    .line 201
    :cond_1
    aget-byte v2, p0, v1

    add-int/2addr v2, v0

    int-to-byte v0, v2

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static catFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    .line 255
    const/4 v2, 0x0

    .line 257
    .local v2, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 258
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 259
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    .end local v2    # "in":Ljava/io/FileInputStream;
    .local v3, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->available()I

    move-result v6

    new-array v0, v6, [B

    .line 261
    .local v0, "buf":[B
    const/4 v6, 0x0

    array-length v7, v0

    invoke-virtual {v3, v0, v6, v7}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    .line 262
    .local v4, "r":I
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6, v4}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 267
    .local v5, "str":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 269
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_0
    move-object v2, v3

    .line 274
    .end local v0    # "buf":[B
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "in":Ljava/io/FileInputStream;
    .end local v4    # "r":I
    .end local v5    # "str":Ljava/lang/String;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :goto_1
    return-object v5

    .line 265
    :catch_0
    move-exception v6

    .line 267
    :goto_2
    if-eqz v2, :cond_1

    .line 269
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 274
    :cond_1
    :goto_3
    const/4 v5, 0x0

    goto :goto_1

    .line 266
    :catchall_0
    move-exception v6

    .line 267
    :goto_4
    if-eqz v2, :cond_2

    .line 269
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 273
    :cond_2
    :goto_5
    throw v6

    .line 267
    .restart local v1    # "file":Ljava/io/File;
    :cond_3
    if-eqz v2, :cond_1

    .line 269
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    .line 270
    :catch_1
    move-exception v6

    goto :goto_3

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v0    # "buf":[B
    .restart local v3    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "r":I
    .restart local v5    # "str":Ljava/lang/String;
    :catch_2
    move-exception v6

    goto :goto_0

    .end local v0    # "buf":[B
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "in":Ljava/io/FileInputStream;
    .end local v4    # "r":I
    .end local v5    # "str":Ljava/lang/String;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_3
    move-exception v6

    goto :goto_3

    :catch_4
    move-exception v7

    goto :goto_5

    .line 266
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .line 265
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :catch_5
    move-exception v6

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public static final closeEdog()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "0"

    const-string v1, "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_rada"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 40
    return-void
.end method

.method public static final closeRadioFrequency()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "0"

    const-string v1, "/sys/bus/i2c/drivers/qn8027/qn8027_mode"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 48
    return-void
.end method

.method public static deleteDirectory(Ljava/lang/String;)Z
    .locals 7
    .param p0, "sPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 392
    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 393
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 395
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 397
    .local v0, "dirFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_2

    .line 419
    :cond_1
    :goto_0
    return v3

    .line 400
    :cond_2
    sput-boolean v4, Lcom/backaudio/android/driver/Utils;->flag:Z

    .line 402
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 403
    .local v1, "files":[Ljava/io/File;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v1

    if-lt v2, v5, :cond_4

    .line 414
    :cond_3
    :goto_2
    sget-boolean v5, Lcom/backaudio/android/driver/Utils;->flag:Z

    if-eqz v5, :cond_1

    .line 416
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v4

    .line 417
    goto :goto_0

    .line 405
    :cond_4
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 406
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/backaudio/android/driver/Utils;->deleteFile(Ljava/lang/String;)Z

    move-result v5

    sput-boolean v5, Lcom/backaudio/android/driver/Utils;->flag:Z

    .line 407
    sget-boolean v5, Lcom/backaudio/android/driver/Utils;->flag:Z

    if-eqz v5, :cond_3

    .line 403
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 410
    :cond_6
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/backaudio/android/driver/Utils;->deleteDirectory(Ljava/lang/String;)Z

    move-result v5

    sput-boolean v5, Lcom/backaudio/android/driver/Utils;->flag:Z

    .line 411
    sget-boolean v5, Lcom/backaudio/android/driver/Utils;->flag:Z

    if-nez v5, :cond_5

    goto :goto_2
.end method

.method public static deleteFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "sPath"    # Ljava/lang/String;

    .prologue
    .line 424
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 426
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 428
    const/4 v1, 0x1

    sput-boolean v1, Lcom/backaudio/android/driver/Utils;->flag:Z

    .line 430
    :cond_0
    sget-boolean v1, Lcom/backaudio/android/driver/Utils;->flag:Z

    return v1
.end method

.method public static echoFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 231
    const-string v4, "driverlog"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "echoFile: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v2, 0x0

    .line 234
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->canWrite()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 236
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 238
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 244
    if-eqz v3, :cond_0

    .line 246
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 239
    :cond_0
    :goto_0
    const/4 v4, 0x1

    move-object v2, v3

    .line 251
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :goto_1
    return v4

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v4, "driverlog"

    const-string v5, "echofile err----------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 244
    if-eqz v2, :cond_1

    .line 246
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 251
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_3
    const/4 v4, 0x0

    goto :goto_1

    .line 243
    :catchall_0
    move-exception v4

    .line 244
    :goto_4
    if-eqz v2, :cond_2

    .line 246
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 250
    :cond_2
    :goto_5
    throw v4

    .line 244
    .restart local v1    # "file":Ljava/io/File;
    :cond_3
    if-eqz v2, :cond_1

    .line 246
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 247
    :catch_1
    move-exception v4

    goto :goto_3

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v4

    goto :goto_0

    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v4

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    goto :goto_5

    .line 243
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 241
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static final getAuxConnect()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0}, Lcom/backaudio/android/driver/Utils;->catFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getCollisonHappenState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "/sys/bus/i2c/drivers/DA380/da380_flag"

    invoke-static {v0}, Lcom/backaudio/android/driver/Utils;->catFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSource(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 129
    const/4 v3, 0x0

    .line 131
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 132
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_1

    .line 145
    if-eqz v3, :cond_0

    .line 147
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 133
    :cond_0
    :goto_0
    const-string v6, ""

    .line 143
    .end local v2    # "file":Ljava/io/File;
    :goto_1
    return-object v6

    .line 135
    .restart local v2    # "file":Ljava/io/File;
    :cond_1
    :try_start_2
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->available()I

    move-result v6

    new-array v0, v6, [B

    .line 137
    .local v0, "buf":[B
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v5

    .line 138
    .local v5, "size":I
    if-nez v5, :cond_3

    .line 145
    if-eqz v4, :cond_2

    .line 147
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 139
    :cond_2
    :goto_2
    const-string v6, ""

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 141
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :cond_3
    :try_start_5
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 145
    if-eqz v4, :cond_4

    .line 147
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :cond_4
    :goto_3
    move-object v3, v4

    .line 141
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 142
    .end local v0    # "buf":[B
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "size":I
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/Exception;
    :goto_4
    if-eqz v3, :cond_5

    .line 147
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 143
    :cond_5
    :goto_5
    const-string v6, ""

    goto :goto_1

    .line 144
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 145
    :goto_6
    if-eqz v3, :cond_6

    .line 147
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 151
    :cond_6
    :goto_7
    throw v6

    .line 148
    .restart local v2    # "file":Ljava/io/File;
    :catch_1
    move-exception v6

    goto :goto_0

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "buf":[B
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "size":I
    :catch_2
    move-exception v6

    goto :goto_2

    :catch_3
    move-exception v7

    goto :goto_3

    .end local v0    # "buf":[B
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "size":I
    .restart local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v6

    goto :goto_5

    .end local v1    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v7

    goto :goto_7

    .line 144
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_6

    .line 142
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4
.end method

.method public static hex2int(C)I
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 227
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    return v0
.end method

.method public static logFileSize(Ljava/io/File;)D
    .locals 8
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    .line 372
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 374
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 375
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 376
    .local v0, "children":[Ljava/io/File;
    const-wide/16 v2, 0x0

    .line 377
    .local v2, "size":D
    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 385
    .end local v0    # "children":[Ljava/io/File;
    .end local v2    # "size":D
    :goto_1
    return-wide v2

    .line 377
    .restart local v0    # "children":[Ljava/io/File;
    .restart local v2    # "size":D
    :cond_0
    aget-object v1, v0, v4

    .line 378
    .local v1, "f":Ljava/io/File;
    invoke-static {v1}, Lcom/backaudio/android/driver/Utils;->logFileSize(Ljava/io/File;)D

    move-result-wide v6

    add-double/2addr v2, v6

    .line 377
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 381
    .end local v0    # "children":[Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "size":D
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v4, v6

    div-double v2, v4, v6

    .line 382
    .restart local v2    # "size":D
    goto :goto_1

    .line 385
    .end local v2    # "size":D
    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method public static final openEdog()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "1"

    const-string v1, "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_rada"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 44
    return-void
.end method

.method public static final openRadioFrequency()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "1"

    const-string v1, "/sys/bus/i2c/drivers/qn8027/qn8027_mode"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 52
    return-void
.end method

.method public static final readToWork()V
    .locals 2

    .prologue
    .line 93
    const-string v0, "1"

    const-string v1, "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_droid_up"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 94
    return-void
.end method

.method public static saveLogLine(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 14
    .param p0, "line"    # Ljava/lang/String;
    .param p1, "isbyte"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x3c

    .line 342
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v8, "yyyy-MM-dd"

    invoke-direct {v2, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 343
    .local v2, "formatter":Ljava/text/DateFormat;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    div-long/2addr v8, v12

    div-long v6, v8, v12

    .line 344
    .local v6, "timestamp":J
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0xa

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 345
    .local v5, "time":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "log-"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".log"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 347
    .local v1, "fileName":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 348
    new-instance v8, Ljava/lang/StringBuilder;

    sget-object v9, Lcom/backaudio/android/driver/Utils;->path:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/unibroad/benzbluetoothlog_byte/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 352
    .local v4, "logPath":Ljava/lang/String;
    :goto_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v8

    const-string v9, "mounted"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 353
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 354
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0}, Lcom/backaudio/android/driver/Utils;->logFileSize(Ljava/io/File;)D

    move-result-wide v8

    const-wide/high16 v10, 0x4014000000000000L    # 5.0

    cmpl-double v8, v8, v10

    if-lez v8, :cond_0

    .line 355
    invoke-static {v4}, Lcom/backaudio/android/driver/Utils;->deleteDirectory(Ljava/lang/String;)Z

    .line 357
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    .line 358
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 360
    :cond_1
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct {v3, v8, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    .line 361
    .local v3, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 362
    const/4 v8, 0x2

    new-array v8, v8, [B

    fill-array-data v8, :array_0

    invoke-virtual {v3, v8}, Ljava/io/FileOutputStream;->write([B)V

    .line 363
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 365
    .end local v0    # "dir":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    return-object v1

    .line 350
    .end local v4    # "logPath":Ljava/lang/String;
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    sget-object v9, Lcom/backaudio/android/driver/Utils;->path:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/unibroad/benzbluetoothlog_line/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "logPath":Ljava/lang/String;
    goto :goto_0

    .line 362
    nop

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method public static saveSource(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 105
    const/4 v2, 0x0

    .line 107
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 108
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 109
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 110
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 112
    :cond_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 114
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 118
    if-eqz v3, :cond_1

    .line 120
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 125
    :cond_1
    :goto_0
    const/4 v4, 0x1

    move-object v2, v3

    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :goto_1
    return v4

    .line 115
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    if-eqz v2, :cond_2

    .line 120
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 116
    :cond_2
    :goto_3
    const/4 v4, 0x0

    goto :goto_1

    .line 117
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 118
    :goto_4
    if-eqz v2, :cond_3

    .line 120
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 124
    :cond_3
    :goto_5
    throw v4

    .line 121
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    goto :goto_5

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v4

    goto :goto_0

    .line 117
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 115
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static final setCollisonCheckLevel(Ljava/lang/String;)V
    .locals 1
    .param p0, "level"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v0, "/sys/bus/i2c/drivers/DA380/da380_sensitivity"

    invoke-static {p0, v0}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 56
    return-void
.end method

.method public static final setCollisonHappenState(Ljava/lang/String;)V
    .locals 1
    .param p0, "state"    # Ljava/lang/String;

    .prologue
    .line 59
    const-string v0, "/sys/bus/i2c/drivers/DA380/da380_flag"

    invoke-static {p0, v0}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 60
    return-void
.end method

.method public static settingReveringLight(Ljava/lang/String;)V
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 85
    const-string v0, "ff 00"

    const-string v1, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 86
    const-string v0, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {p0, v0}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 87
    return-void
.end method

.method public static stringToByte(Ljava/lang/String;[B)I
    .locals 9
    .param p0, "in"    # Ljava/lang/String;
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 209
    array-length v4, p1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    if-ge v4, v5, :cond_0

    .line 210
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "byte array too small"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 212
    :cond_0
    const/4 v2, 0x0

    .line 213
    .local v2, "j":I
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 214
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 223
    return v2

    .line 215
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v7, v4}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 216
    const/4 v4, 0x1

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 217
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    .line 218
    .local v3, "t":I
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "byte hex value:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 219
    int-to-byte v4, v3

    aput-byte v4, p1, v2

    .line 220
    add-int/lit8 v1, v1, 0x1

    .line 221
    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 214
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static switch2AUXSource()V
    .locals 2

    .prologue
    .line 71
    const-string v0, "ff 01"

    const-string v1, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 72
    const-string v0, "02 4c"

    const-string v1, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 73
    return-void
.end method

.method public static switch2NoSource()V
    .locals 2

    .prologue
    .line 81
    const-string v0, "ff 01"

    const-string v1, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 82
    return-void
.end method

.method public static switch2ReversingSource()V
    .locals 2

    .prologue
    .line 76
    const-string v0, "ff 01"

    const-string v1, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 77
    const-string v0, "02 44"

    const-string v1, "/sys/bus/platform/drivers/image_sensor/tw8836_reg"

    invoke-static {v0, v1}, Lcom/backaudio/android/driver/Utils;->echoFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 78
    return-void
.end method

.method public static system(Ljava/lang/String;)V
    .locals 9
    .param p0, "cmdLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 311
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 312
    .local v3, "process":Ljava/lang/Process;
    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 313
    .local v2, "input":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 314
    .local v4, "reader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 316
    .local v0, "buffer":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    .line 317
    invoke-virtual {v3}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 316
    invoke-direct {v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v5, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 321
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 322
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 325
    invoke-virtual {v3}, Ljava/lang/Process;->waitFor()I

    .line 326
    invoke-virtual {v3}, Ljava/lang/Process;->exitValue()I

    move-result v1

    .line 327
    .local v1, "exitValue":I
    if-eqz v1, :cond_6

    .line 328
    new-instance v6, Ljava/lang/Exception;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cmd ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]exit:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 330
    .end local v1    # "exitValue":I
    :catch_0
    move-exception v6

    .line 333
    :goto_2
    if-eqz v2, :cond_0

    .line 334
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 336
    :cond_0
    if-eqz v4, :cond_1

    .line 337
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 340
    :cond_1
    :goto_3
    return-void

    .line 319
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 330
    :catch_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 323
    :cond_3
    :try_start_4
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 332
    :catchall_0
    move-exception v6

    .line 333
    :goto_4
    if-eqz v2, :cond_4

    .line 334
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 336
    :cond_4
    if-eqz v4, :cond_5

    .line 337
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 339
    :cond_5
    throw v6

    .line 333
    .restart local v1    # "exitValue":I
    :cond_6
    if-eqz v2, :cond_7

    .line 334
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 336
    :cond_7
    if-eqz v4, :cond_1

    .line 337
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    goto :goto_3

    .line 332
    .end local v1    # "exitValue":I
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    goto :goto_4
.end method
