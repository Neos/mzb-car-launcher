.class public final enum Lcom/backaudio/android/driver/Mainboard$EModeInfo;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EModeInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EModeInfo;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AIR_CONDITION:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum BASE_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum CANBOX_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum CAR_DOOR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum DASHBOARD_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum FRONT_RADAR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum ORIGINAL_SOURCE:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum ORIGINAL_TIME:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum REAR_RADAR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

.field public static final enum WHEEL_ANGLE:Lcom/backaudio/android/driver/Mainboard$EModeInfo;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 1477
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "BASE_INFO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->BASE_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "AIR_CONDITION"

    invoke-direct {v0, v1, v4, v5}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->AIR_CONDITION:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "REAR_RADAR"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->REAR_RADAR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "FRONT_RADAR"

    .line 1478
    invoke-direct {v0, v1, v5, v7}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->FRONT_RADAR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "CAR_DOOR"

    invoke-direct {v0, v1, v6, v8}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->CAR_DOOR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "ORIGINAL_TIME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v7, v2}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->ORIGINAL_TIME:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "WHEEL_ANGLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v8, v2}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->WHEEL_ANGLE:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "ORIGINAL_SOURCE"

    const/4 v2, 0x7

    .line 1479
    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->ORIGINAL_SOURCE:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "DASHBOARD_INFO"

    const/16 v2, 0x8

    const/16 v3, 0x35

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->DASHBOARD_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const-string v1, "CANBOX_INFO"

    const/16 v2, 0x9

    const/16 v3, 0x7f

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->CANBOX_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    .line 1476
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    const/4 v1, 0x0

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->BASE_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v2, v0, v1

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->AIR_CONDITION:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->REAR_RADAR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v2, v0, v1

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->FRONT_RADAR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->CAR_DOOR:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->ORIGINAL_TIME:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v1, v0, v7

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->WHEEL_ANGLE:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->ORIGINAL_SOURCE:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->DASHBOARD_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->CANBOX_INFO:Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 1483
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1484
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->code:I

    .line 1485
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EModeInfo;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EModeInfo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1488
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->code:I

    int-to-byte v0, v0

    return v0
.end method
