.class public Lcom/backaudio/android/driver/Mainboard;
.super Ljava/lang/Object;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/backaudio/android/driver/Mainboard$EAUXOperate;,
        Lcom/backaudio/android/driver/Mainboard$EAUXStutas;,
        Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;,
        Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;,
        Lcom/backaudio/android/driver/Mainboard$ECarLayer;,
        Lcom/backaudio/android/driver/Mainboard$EControlSource;,
        Lcom/backaudio/android/driver/Mainboard$EEqualizer;,
        Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;,
        Lcom/backaudio/android/driver/Mainboard$ELanguage;,
        Lcom/backaudio/android/driver/Mainboard$EModeInfo;,
        Lcom/backaudio/android/driver/Mainboard$EReverseTpye;,
        Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;,
        Lcom/backaudio/android/driver/Mainboard$EUpgrade;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye:[I

.field private static instance:Lcom/backaudio/android/driver/Mainboard;


# instance fields
.field CANBOX:Ljava/lang/String;

.field MCU:Ljava/lang/String;

.field private baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

.field private bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

.field private iAirMenu:Z

.field private isRecvThreadStarted:Z

.field private mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

.field private mainboardIO:Lcom/backaudio/android/driver/SerialPortMainboardIO;

.field private mcuIO:Lcom/backaudio/android/driver/SerialPortMcuIO;

.field private mcuProtocolAnalyzer:Lcom/backaudio/android/driver/ProtocolAnalyzer;

.field private recvThread:Ljava/lang/Thread;

.field private runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;


# direct methods
.method static synthetic $SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/backaudio/android/driver/Mainboard;->$SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->A:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_1
    :try_start_1
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->B:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_2
    :try_start_2
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_3
    :try_start_3
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_10:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_4
    :try_start_4
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_11:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_5
    :try_start_5
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_13:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_6
    :try_start_6
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->E_15:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_7
    :try_start_7
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLA:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_8
    :try_start_8
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLC:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_9
    :try_start_9
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLE:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_a
    :try_start_a
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->GLK:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_b
    :try_start_b
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ML:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    invoke-virtual {v1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_c
    sput-object v0, Lcom/backaudio/android/driver/Mainboard;->$SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_c

    :catch_1
    move-exception v1

    goto :goto_b

    :catch_2
    move-exception v1

    goto :goto_a

    :catch_3
    move-exception v1

    goto :goto_9

    :catch_4
    move-exception v1

    goto :goto_8

    :catch_5
    move-exception v1

    goto :goto_7

    :catch_6
    move-exception v1

    goto :goto_6

    :catch_7
    move-exception v1

    goto :goto_5

    :catch_8
    move-exception v1

    goto :goto_4

    :catch_9
    move-exception v1

    goto :goto_3

    :catch_a
    move-exception v1

    goto :goto_2

    :catch_b
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/backaudio/android/driver/Mainboard;->instance:Lcom/backaudio/android/driver/Mainboard;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardIO:Lcom/backaudio/android/driver/SerialPortMainboardIO;

    .line 44
    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuIO:Lcom/backaudio/android/driver/SerialPortMcuIO;

    .line 46
    iput-boolean v1, p0, Lcom/backaudio/android/driver/Mainboard;->isRecvThreadStarted:Z

    .line 47
    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->recvThread:Ljava/lang/Thread;

    .line 49
    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuProtocolAnalyzer:Lcom/backaudio/android/driver/ProtocolAnalyzer;

    .line 51
    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    .line 53
    new-instance v0, Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-direct {v0}, Lcom/backaudio/android/driver/beans/CarRunInfo;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    .line 54
    new-instance v0, Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-direct {v0}, Lcom/backaudio/android/driver/beans/CarBaseInfo;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    .line 56
    iput-boolean v1, p0, Lcom/backaudio/android/driver/Mainboard;->iAirMenu:Z

    .line 241
    const-string v0, "listenlogwrite"

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    .line 242
    const-string v0, "listenlogwrite"

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/backaudio/android/driver/SerialPortMainboardIO;->getInstance()Lcom/backaudio/android/driver/SerialPortMainboardIO;

    move-result-object v0

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardIO:Lcom/backaudio/android/driver/SerialPortMainboardIO;

    .line 64
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;

    invoke-direct {v0}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    .line 66
    iput-boolean v3, p0, Lcom/backaudio/android/driver/Mainboard;->isRecvThreadStarted:Z

    .line 67
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/backaudio/android/driver/Mainboard$1;

    invoke-direct {v1, p0}, Lcom/backaudio/android/driver/Mainboard$1;-><init>(Lcom/backaudio/android/driver/Mainboard;)V

    .line 112
    const-string v2, "event2Thread"

    .line 67
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 113
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/backaudio/android/driver/Mainboard$2;

    invoke-direct {v1, p0}, Lcom/backaudio/android/driver/Mainboard$2;-><init>(Lcom/backaudio/android/driver/Mainboard;)V

    .line 134
    const-string v2, "btThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 113
    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->recvThread:Ljava/lang/Thread;

    .line 135
    iput-boolean v3, p0, Lcom/backaudio/android/driver/Mainboard;->isRecvThreadStarted:Z

    .line 136
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->recvThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 138
    invoke-static {}, Lcom/backaudio/android/driver/SerialPortMcuIO;->getInstance()Lcom/backaudio/android/driver/SerialPortMcuIO;

    move-result-object v0

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuIO:Lcom/backaudio/android/driver/SerialPortMcuIO;

    .line 139
    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer;

    invoke-direct {v0}, Lcom/backaudio/android/driver/ProtocolAnalyzer;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuProtocolAnalyzer:Lcom/backaudio/android/driver/ProtocolAnalyzer;

    .line 140
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/backaudio/android/driver/Mainboard$3;

    invoke-direct {v1, p0}, Lcom/backaudio/android/driver/Mainboard$3;-><init>(Lcom/backaudio/android/driver/Mainboard;)V

    .line 160
    const-string v2, "mcuThread"

    .line 140
    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 162
    return-void
.end method

.method static synthetic access$0(Lcom/backaudio/android/driver/Mainboard;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/backaudio/android/driver/Mainboard;->isRecvThreadStarted:Z

    return v0
.end method

.method static synthetic access$1(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/ProtocolAnalyzer;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuProtocolAnalyzer:Lcom/backaudio/android/driver/ProtocolAnalyzer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/SerialPortMainboardIO;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardIO:Lcom/backaudio/android/driver/SerialPortMainboardIO;

    return-object v0
.end method

.method static synthetic access$3(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    return-object v0
.end method

.method static synthetic access$4(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/SerialPortMcuIO;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuIO:Lcom/backaudio/android/driver/SerialPortMcuIO;

    return-object v0
.end method

.method private getCheckCanboxByte([B)B
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    .line 1007
    array-length v2, p1

    .line 1008
    .local v2, "lenght":I
    const/4 v3, 0x1

    aget-byte v0, p1, v3

    .line 1009
    .local v0, "check":B
    const/4 v1, 0x2

    .local v1, "i":I
    :goto_0
    add-int/lit8 v3, v2, -0x1

    if-lt v1, v3, :cond_0

    .line 1012
    xor-int/lit16 v3, v0, 0xff

    int-to-byte v3, v3

    return v3

    .line 1010
    :cond_0
    aget-byte v3, p1, v1

    add-int/2addr v3, v0

    int-to-byte v0, v3

    .line 1009
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getCheckMcuByte([B)B
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 1016
    const/4 v2, 0x2

    aget-byte v0, p1, v2

    .line 1017
    .local v0, "check":B
    const/4 v1, 0x3

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    .line 1020
    return v0

    .line 1018
    :cond_0
    aget-byte v2, p1, v1

    add-int/2addr v2, v0

    int-to-byte v0, v2

    .line 1017
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getInstance()Lcom/backaudio/android/driver/Mainboard;
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/backaudio/android/driver/Mainboard;->instance:Lcom/backaudio/android/driver/Mainboard;

    if-nez v0, :cond_1

    .line 166
    const-class v1, Lcom/backaudio/android/driver/Mainboard;

    monitor-enter v1

    .line 167
    :try_start_0
    sget-object v0, Lcom/backaudio/android/driver/Mainboard;->instance:Lcom/backaudio/android/driver/Mainboard;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Lcom/backaudio/android/driver/Mainboard;

    invoke-direct {v0}, Lcom/backaudio/android/driver/Mainboard;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard;->instance:Lcom/backaudio/android/driver/Mainboard;

    .line 166
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    :cond_1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard;->instance:Lcom/backaudio/android/driver/Mainboard;

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public addEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V
    .locals 1
    .param p1, "handler"    # Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 180
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    invoke-interface {v0, p1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;->setEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V

    goto :goto_0
.end method

.method public analyzeAUXStatus([B)V
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    .line 1291
    const/4 v3, 0x3

    aget-byte v2, p1, v3

    .line 1292
    .local v2, "type":B
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->ACTIVATING:Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    .line 1293
    .local v0, "source":Lcom/backaudio/android/driver/Mainboard$EAUXStutas;
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->values()[Lcom/backaudio/android/driver/Mainboard$EAUXStutas;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 1299
    :goto_1
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onAUXActivateStutas(Lcom/backaudio/android/driver/Mainboard$EAUXStutas;)V

    .line 1300
    return-void

    .line 1293
    :cond_0
    aget-object v1, v4, v3

    .line 1294
    .local v1, "temp":Lcom/backaudio/android/driver/Mainboard$EAUXStutas;
    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EAUXStutas;->getCode()B

    move-result v6

    if-ne v6, v2, :cond_1

    .line 1295
    move-object v0, v1

    .line 1296
    goto :goto_1

    .line 1293
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public analyzeAirInfo([B)V
    .locals 32
    .param p1, "buffer"    # [B

    .prologue
    .line 1049
    const/16 v25, 0x3

    aget-byte v4, p1, v25

    .line 1050
    .local v4, "b1":B
    new-instance v18, Lcom/backaudio/android/driver/beans/AirInfo;

    invoke-direct/range {v18 .. v18}, Lcom/backaudio/android/driver/beans/AirInfo;-><init>()V

    .line 1051
    .local v18, "info":Lcom/backaudio/android/driver/beans/AirInfo;
    shr-int/lit8 v25, v4, 0x7

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1

    const/4 v8, 0x1

    .line 1052
    .local v8, "iAirOpen":Z
    :goto_0
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/backaudio/android/driver/beans/AirInfo;->setiAirOpen(Z)V

    .line 1053
    shr-int/lit8 v25, v4, 0x6

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2

    const/4 v6, 0x1

    .line 1054
    .local v6, "iACOpen":Z
    :goto_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Lcom/backaudio/android/driver/beans/AirInfo;->setiACOpen(Z)V

    .line 1055
    shr-int/lit8 v25, v4, 0x5

    and-int/lit8 v25, v25, 0x1

    if-nez v25, :cond_3

    const/4 v15, 0x1

    .line 1056
    .local v15, "iInnerLoop":Z
    :goto_2
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/backaudio/android/driver/beans/AirInfo;->setiInnerLoop(Z)V

    .line 1059
    shr-int/lit8 v25, v4, 0x4

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_4

    const/4 v10, 0x1

    .line 1060
    .local v10, "iAuto2":Z
    :goto_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/backaudio/android/driver/beans/AirInfo;->setiAuto2(Z)V

    .line 1061
    shr-int/lit8 v25, v4, 0x3

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_5

    const/4 v9, 0x1

    .line 1062
    .local v9, "iAuto1":Z
    :goto_4
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lcom/backaudio/android/driver/beans/AirInfo;->setiAuto1(Z)V

    .line 1069
    shr-int/lit8 v25, v4, 0x2

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    const/4 v12, 0x1

    .line 1070
    .local v12, "iDual":Z
    :goto_5
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/backaudio/android/driver/beans/AirInfo;->setiDual(Z)V

    .line 1071
    shr-int/lit8 v25, v4, 0x1

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_7

    const/16 v16, 0x1

    .line 1072
    .local v16, "iMaxFrontWind":Z
    :goto_6
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/beans/AirInfo;->setiMaxFrontWind(Z)V

    .line 1073
    and-int/lit8 v25, v4, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_8

    const/16 v17, 0x1

    .line 1074
    .local v17, "iRear":Z
    :goto_7
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/beans/AirInfo;->setiRear(Z)V

    .line 1076
    const/16 v25, 0x4

    aget-byte v5, p1, v25

    .line 1077
    .local v5, "b2":B
    shr-int/lit8 v25, v5, 0x7

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_9

    const/4 v14, 0x1

    .line 1078
    .local v14, "iFrontWind":Z
    :goto_8
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/backaudio/android/driver/beans/AirInfo;->setiFrontWind(Z)V

    .line 1081
    shr-int/lit8 v25, v5, 0x6

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_a

    const/4 v13, 0x1

    .line 1082
    .local v13, "iFlatWind":Z
    :goto_9
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/backaudio/android/driver/beans/AirInfo;->setiFlatWind(Z)V

    .line 1083
    shr-int/lit8 v25, v5, 0x5

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    const/4 v11, 0x1

    .line 1084
    .local v11, "iDownWind":Z
    :goto_a
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/backaudio/android/driver/beans/AirInfo;->setiDownWind(Z)V

    .line 1086
    and-int/lit8 v25, v5, 0x8

    const/16 v26, 0x8

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_c

    const-wide/high16 v22, 0x3fe0000000000000L    # 0.5

    .line 1087
    .local v22, "leveltmp":D
    :goto_b
    and-int/lit8 v25, v5, 0x7

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v26, v0

    add-double v20, v26, v22

    .line 1088
    .local v20, "level":D
    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/backaudio/android/driver/beans/AirInfo;->setLevel(D)V

    .line 1090
    const/16 v25, 0x5

    aget-byte v19, p1, v25

    .line 1091
    .local v19, "leftTemp":I
    const-wide/high16 v26, 0x402e000000000000L    # 15.0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x3fe0000000000000L    # 0.5

    mul-double v28, v28, v30

    add-double v26, v26, v28

    move-object/from16 v0, v18

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/backaudio/android/driver/beans/AirInfo;->setLeftTemp(D)V

    .line 1093
    const/16 v25, 0x5

    aget-byte v25, p1, v25

    const/16 v26, 0xff

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_d

    const/16 v24, -0x1

    .line 1094
    .local v24, "rightTemp":I
    :goto_c
    const-wide/high16 v26, 0x402e000000000000L    # 15.0

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x3fe0000000000000L    # 0.5

    mul-double v28, v28, v30

    add-double v26, v26, v28

    move-object/from16 v0, v18

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/backaudio/android/driver/beans/AirInfo;->setRightTemp(D)V

    .line 1097
    const/16 v25, 0x7

    aget-byte v25, p1, v25

    shr-int/lit8 v25, v25, 0x1

    and-int/lit8 v25, v25, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_e

    const/4 v7, 0x1

    .line 1098
    .local v7, "iAirMenu":Z
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/backaudio/android/driver/Mainboard;->iAirMenu:Z

    move/from16 v25, v0

    move/from16 v0, v25

    if-eq v0, v7, :cond_0

    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    move-object/from16 v25, v0

    sget-object v26, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 1100
    sget-object v27, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 1099
    invoke-interface/range {v25 .. v27}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onHandleIdriver(Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;)V

    .line 1103
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onAirInfo(Lcom/backaudio/android/driver/beans/AirInfo;)V

    .line 1104
    return-void

    .line 1051
    .end local v5    # "b2":B
    .end local v6    # "iACOpen":Z
    .end local v7    # "iAirMenu":Z
    .end local v8    # "iAirOpen":Z
    .end local v9    # "iAuto1":Z
    .end local v10    # "iAuto2":Z
    .end local v11    # "iDownWind":Z
    .end local v12    # "iDual":Z
    .end local v13    # "iFlatWind":Z
    .end local v14    # "iFrontWind":Z
    .end local v15    # "iInnerLoop":Z
    .end local v16    # "iMaxFrontWind":Z
    .end local v17    # "iRear":Z
    .end local v19    # "leftTemp":I
    .end local v20    # "level":D
    .end local v22    # "leveltmp":D
    .end local v24    # "rightTemp":I
    :cond_1
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1053
    .restart local v8    # "iAirOpen":Z
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 1055
    .restart local v6    # "iACOpen":Z
    :cond_3
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 1059
    .restart local v15    # "iInnerLoop":Z
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 1061
    .restart local v10    # "iAuto2":Z
    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 1069
    .restart local v9    # "iAuto1":Z
    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_5

    .line 1071
    .restart local v12    # "iDual":Z
    :cond_7
    const/16 v16, 0x0

    goto/16 :goto_6

    .line 1073
    .restart local v16    # "iMaxFrontWind":Z
    :cond_8
    const/16 v17, 0x0

    goto/16 :goto_7

    .line 1077
    .restart local v5    # "b2":B
    .restart local v17    # "iRear":Z
    :cond_9
    const/4 v14, 0x0

    goto/16 :goto_8

    .line 1081
    .restart local v14    # "iFrontWind":Z
    :cond_a
    const/4 v13, 0x0

    goto/16 :goto_9

    .line 1083
    .restart local v13    # "iFlatWind":Z
    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_a

    .line 1086
    .restart local v11    # "iDownWind":Z
    :cond_c
    const-wide/16 v22, 0x0

    goto/16 :goto_b

    .line 1093
    .restart local v19    # "leftTemp":I
    .restart local v20    # "level":D
    .restart local v22    # "leveltmp":D
    :cond_d
    const/16 v25, 0x6

    aget-byte v24, p1, v25

    goto :goto_c

    .line 1097
    .restart local v24    # "rightTemp":I
    :cond_e
    const/4 v7, 0x0

    goto :goto_d
.end method

.method public analyzeBenzSize([B)V
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    const/4 v1, 0x5

    aget-byte v1, p1, v1

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainBenzSize(I)V

    .line 1404
    return-void
.end method

.method public analyzeBenzTpye([B)V
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    .line 1391
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->C:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .line 1392
    .local v1, "eBenzTpye":Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    const/4 v3, 0x5

    aget-byte v0, p1, v3

    .line 1393
    .local v0, "b":B
    invoke-static {}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->values()[Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 1399
    :goto_1
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainBenzType(Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;)V

    .line 1400
    return-void

    .line 1393
    :cond_0
    aget-object v2, v4, v3

    .line 1394
    .local v2, "value":Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    invoke-virtual {v2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v6

    if-ne v0, v6, :cond_1

    .line 1395
    move-object v1, v2

    .line 1396
    goto :goto_1

    .line 1393
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public analyzeBrightnessValue([B)V
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 1407
    const/4 v1, 0x5

    aget-byte v0, p1, v1

    .line 1408
    .local v0, "index":I
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v1, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainBrightness(I)V

    .line 1409
    return-void
.end method

.method public analyzeCanboxPro([B)V
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    .line 1179
    const/4 v7, 0x2

    aget-byte v6, p1, v7

    .line 1180
    .local v6, "length":I
    const/4 v7, 0x3

    add-int/lit8 v8, v6, 0x3

    invoke-static {p1, v7, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    .line 1181
    .local v5, "infoByte":[B
    const-string v7, "US-ASCII"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    .line 1182
    .local v3, "cs":Ljava/nio/charset/Charset;
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1183
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1184
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1185
    invoke-virtual {v3, v0}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v2

    .line 1186
    .local v2, "cb":Ljava/nio/CharBuffer;
    invoke-virtual {v2}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v1

    .line 1187
    .local v1, "cChar":[C
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([C)V

    .line 1188
    .local v4, "info":Ljava/lang/String;
    iget-object v7, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v7, v4}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCanboxInfo(Ljava/lang/String;)V

    .line 1189
    return-void
.end method

.method public analyzeCarBasePro([B)V
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 1108
    const/4 v8, 0x3

    aget-byte v0, p1, v8

    .line 1109
    .local v0, "b1":B
    shr-int/lit8 v8, v0, 0x4

    and-int/lit8 v8, v8, 0x1

    if-ne v8, v2, :cond_0

    move v6, v2

    .line 1110
    .local v6, "iPowerOn":Z
    :goto_0
    shr-int/lit8 v8, v0, 0x3

    and-int/lit8 v8, v8, 0x1

    if-ne v8, v2, :cond_1

    move v3, v2

    .line 1111
    .local v3, "iInP":Z
    :goto_1
    shr-int/lit8 v8, v0, 0x2

    and-int/lit8 v8, v8, 0x1

    if-ne v8, v2, :cond_2

    move v4, v2

    .line 1112
    .local v4, "iInRevering":Z
    :goto_2
    shr-int/lit8 v8, v0, 0x1

    and-int/lit8 v8, v8, 0x1

    if-ne v8, v2, :cond_3

    move v5, v2

    .line 1113
    .local v5, "iNearLightOpen":Z
    :goto_3
    and-int/lit8 v8, v0, 0x1

    if-ne v8, v2, :cond_4

    .line 1115
    .local v2, "iAccOpen":Z
    :goto_4
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v8, v6}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiPowerOn(Z)V

    .line 1116
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v8, v3}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiInP(Z)V

    .line 1117
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v8, v4}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiInRevering(Z)V

    .line 1118
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v8, v5}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiNearLightOpen(Z)V

    .line 1119
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v8, v2}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiAccOpen(Z)V

    .line 1121
    const/4 v8, 0x7

    aget-byte v8, p1, v8

    and-int/lit16 v1, v8, 0xff

    .line 1122
    .local v1, "curSpeed":I
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-virtual {v8, v1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setCurSpeed(I)V

    .line 1124
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v8, v7}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiFlash(Z)V

    .line 1125
    iget-object v7, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-interface {v7, v8}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarBaseInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    .line 1126
    iget-object v7, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-interface {v7, v8}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    .line 1127
    return-void

    .end local v1    # "curSpeed":I
    .end local v2    # "iAccOpen":Z
    .end local v3    # "iInP":Z
    .end local v4    # "iInRevering":Z
    .end local v5    # "iNearLightOpen":Z
    .end local v6    # "iPowerOn":Z
    :cond_0
    move v6, v7

    .line 1109
    goto :goto_0

    .restart local v6    # "iPowerOn":Z
    :cond_1
    move v3, v7

    .line 1110
    goto :goto_1

    .restart local v3    # "iInP":Z
    :cond_2
    move v4, v7

    .line 1111
    goto :goto_2

    .restart local v4    # "iInRevering":Z
    :cond_3
    move v5, v7

    .line 1112
    goto :goto_3

    .restart local v5    # "iNearLightOpen":Z
    :cond_4
    move v2, v7

    .line 1113
    goto :goto_4
.end method

.method public analyzeCarBasePro1([B)V
    .locals 10
    .param p1, "buffer"    # [B

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 1131
    const/4 v9, 0x3

    aget-byte v0, p1, v9

    .line 1133
    .local v0, "b1":B
    shr-int/lit8 v9, v0, 0x7

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_1

    move v6, v8

    .line 1134
    .local v6, "iRightFrontOpen":Z
    :goto_0
    shr-int/lit8 v9, v0, 0x6

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_2

    move v4, v8

    .line 1135
    .local v4, "iLeftFrontOpen":Z
    :goto_1
    shr-int/lit8 v9, v0, 0x5

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_3

    move v5, v8

    .line 1136
    .local v5, "iRightBackOpen":Z
    :goto_2
    shr-int/lit8 v9, v0, 0x4

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_4

    move v3, v8

    .line 1137
    .local v3, "iLeftBackOpen":Z
    :goto_3
    shr-int/lit8 v9, v0, 0x3

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_5

    move v1, v8

    .line 1138
    .local v1, "iBack":Z
    :goto_4
    shr-int/lit8 v9, v0, 0x2

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_6

    move v2, v8

    .line 1139
    .local v2, "iFront":Z
    :goto_5
    shr-int/lit8 v9, v0, 0x1

    and-int/lit8 v9, v9, 0x1

    if-ne v9, v8, :cond_0

    move v7, v8

    .line 1140
    .local v7, "iSafetyBelt":Z
    :cond_0
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v6}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiRightFrontOpen(Z)V

    .line 1141
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v4}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiLeftFrontOpen(Z)V

    .line 1142
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v5}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiRightBackOpen(Z)V

    .line 1143
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v3}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiLeftBackOpen(Z)V

    .line 1144
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v1}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiBack(Z)V

    .line 1145
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v2}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiFront(Z)V

    .line 1146
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v7}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiSafetyBelt(Z)V

    .line 1147
    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-virtual {v9, v8}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiFlash(Z)V

    .line 1148
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    iget-object v9, p0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    invoke-interface {v8, v9}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarBaseInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    .line 1149
    return-void

    .end local v1    # "iBack":Z
    .end local v2    # "iFront":Z
    .end local v3    # "iLeftBackOpen":Z
    .end local v4    # "iLeftFrontOpen":Z
    .end local v5    # "iRightBackOpen":Z
    .end local v6    # "iRightFrontOpen":Z
    .end local v7    # "iSafetyBelt":Z
    :cond_1
    move v6, v7

    .line 1133
    goto :goto_0

    .restart local v6    # "iRightFrontOpen":Z
    :cond_2
    move v4, v7

    .line 1134
    goto :goto_1

    .restart local v4    # "iLeftFrontOpen":Z
    :cond_3
    move v5, v7

    .line 1135
    goto :goto_2

    .restart local v5    # "iRightBackOpen":Z
    :cond_4
    move v3, v7

    .line 1136
    goto :goto_3

    .restart local v3    # "iLeftBackOpen":Z
    :cond_5
    move v1, v7

    .line 1137
    goto :goto_4

    .restart local v1    # "iBack":Z
    :cond_6
    move v2, v7

    .line 1138
    goto :goto_5
.end method

.method public analyzeCarRunningInfoPro([B)V
    .locals 26
    .param p1, "buffer"    # [B

    .prologue
    .line 1249
    const/16 v20, 0x3

    aget-byte v4, p1, v20

    .line 1250
    .local v4, "b":B
    and-int/lit8 v20, v4, 0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    const/4 v9, 0x1

    .line 1251
    .local v9, "foglight":Z
    :goto_0
    shr-int/lit8 v20, v4, 0x1

    and-int/lit8 v20, v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    const/4 v13, 0x1

    .line 1252
    .local v13, "iNearLightOpen":Z
    :goto_1
    shr-int/lit8 v20, v4, 0x2

    and-int/lit8 v20, v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    const/4 v11, 0x1

    .line 1253
    .local v11, "iDistantLightOpen":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput-boolean v0, v1, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 1254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setFoglight(Z)V

    .line 1255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiNearLightOpen(Z)V

    .line 1256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiDistantLightOpen(Z)V

    .line 1258
    const/16 v20, 0x4

    aget-byte v20, p1, v20

    and-int/lit8 v20, v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    const/4 v10, 0x1

    .line 1259
    .local v10, "iBrake":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiBrake(Z)V

    .line 1261
    const/16 v20, 0x4

    aget-byte v20, p1, v20

    shr-int/lit8 v20, v20, 0x1

    and-int/lit8 v20, v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    const/4 v12, 0x1

    .line 1262
    .local v12, "iFootBrake":Z
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiFootBrake(Z)V

    .line 1264
    const/16 v20, 0x5

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    .line 1265
    .local v17, "value":I
    shl-int/lit8 v20, v17, 0x8

    const/16 v21, 0x6

    aget-byte v21, p1, v21

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    add-int v14, v20, v21

    .line 1266
    .local v14, "mileage":I
    const/16 v20, 0x7

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    .line 1267
    shl-int/lit8 v20, v17, 0x8

    const/16 v21, 0x8

    aget-byte v21, p1, v21

    move/from16 v0, v21

    and-int/lit16 v0, v0, 0xff

    move/from16 v21, v0

    add-int v15, v20, v21

    .line 1268
    .local v15, "revolutions":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setMileage(I)V

    .line 1269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setRevolutions(I)V

    .line 1271
    const/16 v20, 0x9

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 1272
    .local v16, "temp":I
    const/16 v20, 0xa

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v5, v0, 0xff

    .line 1273
    .local v5, "data1":I
    const/16 v20, 0xb

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v6, v0, 0xff

    .line 1274
    .local v6, "data2":I
    const/16 v20, 0xc

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v7, v0, 0xff

    .line 1275
    .local v7, "data3":I
    const/16 v20, 0xd

    aget-byte v20, p1, v20

    move/from16 v0, v20

    and-int/lit16 v8, v0, 0xff

    .line 1277
    .local v8, "data4":I
    shl-int/lit8 v20, v5, 0x18

    shl-int/lit8 v21, v6, 0x10

    add-int v20, v20, v21

    shl-int/lit8 v21, v7, 0x8

    add-int v20, v20, v21

    add-int v20, v20, v8

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 1280
    .local v18, "totalMileage":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    move-object/from16 v20, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4044000000000000L    # 40.0

    sub-double v22, v22, v24

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setOutsideTemp(D)V

    .line 1281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setTotalMileage(J)V

    .line 1283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/backaudio/android/driver/beans/CarBaseInfo;->setiFlash(Z)V

    .line 1284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    move/from16 v20, v0

    if-eqz v20, :cond_0

    .line 1285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->baseInfo:Lcom/backaudio/android/driver/beans/CarBaseInfo;

    move-object/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarBaseInfo(Lcom/backaudio/android/driver/beans/CarBaseInfo;)V

    .line 1287
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    move-object/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    .line 1288
    return-void

    .line 1250
    .end local v5    # "data1":I
    .end local v6    # "data2":I
    .end local v7    # "data3":I
    .end local v8    # "data4":I
    .end local v9    # "foglight":Z
    .end local v10    # "iBrake":Z
    .end local v11    # "iDistantLightOpen":Z
    .end local v12    # "iFootBrake":Z
    .end local v13    # "iNearLightOpen":Z
    .end local v14    # "mileage":I
    .end local v15    # "revolutions":I
    .end local v16    # "temp":I
    .end local v17    # "value":I
    .end local v18    # "totalMileage":J
    :cond_1
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 1251
    .restart local v9    # "foglight":Z
    :cond_2
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 1252
    .restart local v13    # "iNearLightOpen":Z
    :cond_3
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 1258
    .restart local v11    # "iDistantLightOpen":Z
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 1261
    .restart local v10    # "iBrake":Z
    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_4
.end method

.method public analyzeCarRunningInfoPro1([B)V
    .locals 5
    .param p1, "buffer"    # [B

    .prologue
    .line 1157
    const/4 v3, 0x3

    aget-byte v3, p1, v3

    and-int/lit16 v0, v3, 0xff

    .line 1158
    .local v0, "curSpeed":I
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-virtual {v3, v0}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setCurSpeed(I)V

    .line 1159
    const/4 v3, 0x4

    aget-byte v3, p1, v3

    and-int/lit16 v2, v3, 0xff

    .line 1160
    .local v2, "value":I
    shl-int/lit8 v3, v2, 0x8

    const/4 v4, 0x5

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    add-int v1, v3, v4

    .line 1161
    .local v1, "revolutions":I
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-virtual {v3, v1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setRevolutions(I)V

    .line 1162
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    iget-object v4, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-interface {v3, v4}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    .line 1163
    return-void
.end method

.method public analyzeCarRunningInfoPro2([B)V
    .locals 5
    .param p1, "buffer"    # [B

    .prologue
    .line 1170
    const/4 v3, 0x5

    aget-byte v3, p1, v3

    and-int/lit16 v0, v3, 0xff

    .line 1171
    .local v0, "curSpeed":I
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-virtual {v3, v0}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setCurSpeed(I)V

    .line 1172
    const/4 v3, 0x3

    aget-byte v3, p1, v3

    and-int/lit16 v2, v3, 0xff

    .line 1173
    .local v2, "value":I
    shl-int/lit8 v3, v2, 0x8

    const/4 v4, 0x4

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    add-int v1, v3, v4

    .line 1174
    .local v1, "revolutions":I
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-virtual {v3, v1}, Lcom/backaudio/android/driver/beans/CarRunInfo;->setRevolutions(I)V

    .line 1175
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    iget-object v4, p0, Lcom/backaudio/android/driver/Mainboard;->runInfo:Lcom/backaudio/android/driver/beans/CarRunInfo;

    invoke-interface {v3, v4}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCarRunningInfo(Lcom/backaudio/android/driver/beans/CarRunInfo;)V

    .line 1176
    return-void
.end method

.method public analyzeHornSoundValue([B)V
    .locals 6
    .param p1, "buffer"    # [B

    .prologue
    .line 1314
    const/4 v5, 0x4

    aget-byte v0, p1, v5

    .line 1315
    .local v0, "b":B
    const/4 v5, 0x5

    aget-byte v1, p1, v5

    .line 1316
    .local v1, "fLeft":I
    const/4 v5, 0x6

    aget-byte v2, p1, v5

    .line 1317
    .local v2, "fRight":I
    const/4 v5, 0x7

    aget-byte v3, p1, v5

    .line 1318
    .local v3, "rLeft":I
    const/16 v5, 0x8

    aget-byte v4, p1, v5

    .line 1319
    .local v4, "rRight":I
    iget-object v5, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v5, v1, v2, v3, v4}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onHornSoundValue(IIII)V

    .line 1320
    return-void
.end method

.method public analyzeIdriverPro([B)V
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    const/4 v8, 0x4

    .line 1192
    array-length v4, p1

    const/4 v5, 0x6

    if-ge v4, v5, :cond_0

    .line 1210
    :goto_0
    return-void

    .line 1195
    :cond_0
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NONE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 1196
    .local v2, "idriverValue":Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
    const/4 v4, 0x3

    aget-byte v0, p1, v4

    .line 1197
    .local v0, "b":B
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->values()[Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_1
    if-lt v4, v6, :cond_2

    .line 1203
    :goto_2
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_DOWN:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 1204
    .local v1, "btnValue":Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;
    aget-byte v4, p1, v8

    if-nez v4, :cond_4

    .line 1205
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_UP:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    .line 1209
    :cond_1
    :goto_3
    iget-object v4, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v4, v2, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onHandleIdriver(Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;)V

    goto :goto_0

    .line 1197
    .end local v1    # "btnValue":Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;
    :cond_2
    aget-object v3, v5, v4

    .line 1198
    .local v3, "value":Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->getCode()B

    move-result v7

    if-ne v0, v7, :cond_3

    .line 1199
    move-object v2, v3

    .line 1200
    goto :goto_2

    .line 1197
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1206
    .end local v3    # "value":Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
    .restart local v1    # "btnValue":Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;
    :cond_4
    aget-byte v4, p1, v8

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1207
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;->BTN_LONG_PRESS:Lcom/backaudio/android/driver/Mainboard$EBtnStateEnum;

    goto :goto_3
.end method

.method public analyzeLanguageMediaSet([B)V
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 1375
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$ELanguage;->SIMPLIFIED_CHINESE:Lcom/backaudio/android/driver/Mainboard$ELanguage;

    .line 1376
    .local v0, "eLanguage":Lcom/backaudio/android/driver/Mainboard$ELanguage;
    const/4 v1, 0x5

    aget-byte v1, p1, v1

    packed-switch v1, :pswitch_data_0

    .line 1387
    :goto_0
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v1, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainLanguageMediaSet(Lcom/backaudio/android/driver/Mainboard$ELanguage;)V

    .line 1388
    return-void

    .line 1378
    :pswitch_0
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$ELanguage;->SIMPLIFIED_CHINESE:Lcom/backaudio/android/driver/Mainboard$ELanguage;

    .line 1379
    goto :goto_0

    .line 1381
    :pswitch_1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$ELanguage;->TRADITIONAL_CHINESE:Lcom/backaudio/android/driver/Mainboard$ELanguage;

    .line 1382
    goto :goto_0

    .line 1384
    :pswitch_2
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$ELanguage;->US:Lcom/backaudio/android/driver/Mainboard$ELanguage;

    goto :goto_0

    .line 1376
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public analyzeMcuEnterAccOrWakeup([B)V
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    const/4 v5, 0x5

    .line 1026
    aget-byte v3, p1, v5

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 1027
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onEnterStandbyMode()V

    .line 1039
    :cond_0
    :goto_0
    return-void

    .line 1028
    :cond_1
    aget-byte v3, p1, v5

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1029
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    .line 1030
    .local v1, "eCarLayer":Lcom/backaudio/android/driver/Mainboard$ECarLayer;
    const/4 v3, 0x6

    aget-byte v0, p1, v3

    .line 1031
    .local v0, "b":B
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->values()[Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_1
    if-lt v3, v5, :cond_2

    .line 1037
    :goto_2
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onWakeUp(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    goto :goto_0

    .line 1031
    :cond_2
    aget-object v2, v4, v3

    .line 1032
    .local v2, "value":Lcom/backaudio/android/driver/Mainboard$ECarLayer;
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->getCode()B

    move-result v6

    if-ne v0, v6, :cond_3

    .line 1033
    move-object v1, v2

    .line 1034
    goto :goto_2

    .line 1031
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public analyzeMcuInfoPro([B)V
    .locals 5
    .param p1, "buffer"    # [B

    .prologue
    .line 1042
    const/4 v3, 0x2

    aget-byte v3, p1, v3

    add-int/lit8 v1, v3, -0x2

    .line 1043
    .local v1, "infoLenght":I
    const/4 v3, 0x5

    add-int/lit8 v4, v1, 0x5

    invoke-static {p1, v3, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 1044
    .local v0, "infoByte":[B
    new-instance v2, Ljava/lang/String;

    const-string v3, "utf-8"

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 1045
    .local v2, "versionNumber":Ljava/lang/String;
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v2}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainVersionNumber(Ljava/lang/String;)V

    .line 1046
    return-void
.end method

.method public analyzeMcuUpgradeDateByindexPro([B)V
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    .line 1323
    const/4 v3, 0x5

    aget-byte v3, p1, v3

    and-int/lit16 v2, v3, 0xff

    .line 1324
    .local v2, "l":I
    const/4 v3, 0x4

    aget-byte v3, p1, v3

    and-int/lit16 v0, v3, 0xff

    .line 1326
    .local v0, "h":I
    shl-int/lit8 v3, v0, 0x8

    add-int v1, v3, v2

    .line 1327
    .local v1, "index":I
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onMcuUpgradeForGetDataByIndex(I)V

    .line 1328
    return-void
.end method

.method public analyzeMcuUpgradeStatePro([B)V
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    .line 1302
    const/4 v3, 0x4

    aget-byte v0, p1, v3

    .line 1303
    .local v0, "b":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    .line 1304
    .local v2, "value":Lcom/backaudio/android/driver/Mainboard$EUpgrade;
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->values()[Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 1310
    :goto_1
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v2}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onMcuUpgradeState(Lcom/backaudio/android/driver/Mainboard$EUpgrade;)V

    .line 1311
    return-void

    .line 1304
    :cond_0
    aget-object v1, v4, v3

    .line 1305
    .local v1, "e":Lcom/backaudio/android/driver/Mainboard$EUpgrade;
    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->getCode()B

    move-result v6

    if-ne v6, v0, :cond_1

    .line 1306
    move-object v2, v1

    .line 1307
    goto :goto_1

    .line 1304
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public analyzeOriginalCarPro([B)V
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    .line 1225
    const/4 v0, 0x1

    .line 1226
    .local v0, "iOriginal":Z
    const/4 v5, 0x4

    aget-byte v1, p1, v5

    .line 1227
    .local v1, "isAux":B
    const/4 v5, 0x3

    aget-byte v4, p1, v5

    .line 1228
    .local v4, "type":B
    packed-switch v4, :pswitch_data_0

    .line 1238
    :goto_0
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->RADIO:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    .line 1239
    .local v2, "source":Lcom/backaudio/android/driver/Mainboard$EControlSource;
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EControlSource;->values()[Lcom/backaudio/android/driver/Mainboard$EControlSource;

    move-result-object v6

    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v7, :cond_0

    .line 1245
    :goto_2
    iget-object v5, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v5, v2, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onOriginalCarView(Lcom/backaudio/android/driver/Mainboard$EControlSource;Z)V

    .line 1246
    return-void

    .line 1230
    .end local v2    # "source":Lcom/backaudio/android/driver/Mainboard$EControlSource;
    :pswitch_0
    const/4 v0, 0x0

    .line 1231
    goto :goto_0

    .line 1233
    :pswitch_1
    const/4 v0, 0x1

    .line 1234
    goto :goto_0

    .line 1239
    .restart local v2    # "source":Lcom/backaudio/android/driver/Mainboard$EControlSource;
    :cond_0
    aget-object v3, v6, v5

    .line 1240
    .local v3, "temp":Lcom/backaudio/android/driver/Mainboard$EControlSource;
    invoke-virtual {v3}, Lcom/backaudio/android/driver/Mainboard$EControlSource;->getCode()B

    move-result v8

    if-ne v8, v1, :cond_1

    .line 1241
    move-object v2, v3

    .line 1242
    goto :goto_2

    .line 1239
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public analyzeReverseMediaSet([B)V
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 1359
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 1360
    .local v0, "eMediaSet":Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;
    const/4 v1, 0x5

    aget-byte v1, p1, v1

    packed-switch v1, :pswitch_data_0

    .line 1371
    :goto_0
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v1, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainReverseMediaSet(Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;)V

    .line 1372
    return-void

    .line 1362
    :pswitch_0
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 1363
    goto :goto_0

    .line 1365
    :pswitch_1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->FLAT:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 1366
    goto :goto_0

    .line 1368
    :pswitch_2
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    goto :goto_0

    .line 1360
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public analyzeReverseTpye([B)V
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 1343
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    .line 1344
    .local v0, "eReverseTpye":Lcom/backaudio/android/driver/Mainboard$EReverseTpye;
    const/4 v1, 0x5

    aget-byte v1, p1, v1

    packed-switch v1, :pswitch_data_0

    .line 1355
    :goto_0
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v1, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainReverseType(Lcom/backaudio/android/driver/Mainboard$EReverseTpye;)V

    .line 1356
    return-void

    .line 1346
    :pswitch_0
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ADD_REVERSE:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    .line 1347
    goto :goto_0

    .line 1349
    :pswitch_1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    .line 1350
    goto :goto_0

    .line 1352
    :pswitch_2
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverseTpye;->REVERSE_360:Lcom/backaudio/android/driver/Mainboard$EReverseTpye;

    goto :goto_0

    .line 1344
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public analyzeShowOrHideCarLayer([B)V
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    .line 1331
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    .line 1332
    .local v1, "eCarLayer":Lcom/backaudio/android/driver/Mainboard$ECarLayer;
    const/4 v3, 0x5

    aget-byte v0, p1, v3

    .line 1333
    .local v0, "b":B
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->values()[Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 1339
    :goto_1
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onShowOrHideCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V

    .line 1340
    return-void

    .line 1333
    :cond_0
    aget-object v2, v4, v3

    .line 1334
    .local v2, "value":Lcom/backaudio/android/driver/Mainboard$ECarLayer;
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->getCode()B

    move-result v6

    if-ne v0, v6, :cond_1

    .line 1335
    move-object v1, v2

    .line 1336
    goto :goto_1

    .line 1333
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public analyzeStoreDateFromMCU([B)V
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 1445
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1446
    .local v0, "datalist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Byte;>;"
    const/4 v1, 0x5

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    .line 1449
    iget-object v2, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v2, v0}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->obtainStoreData(Ljava/util/List;)V

    .line 1450
    return-void

    .line 1447
    :cond_0
    aget-byte v2, p1, v1

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public analyzeTimePro([B)V
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    const/4 v8, 0x6

    .line 1213
    const/4 v0, 0x3

    aget-byte v0, p1, v0

    add-int/lit16 v1, v0, 0x7d0

    .line 1214
    .local v1, "year":I
    const/4 v0, 0x4

    aget-byte v2, p1, v0

    .line 1215
    .local v2, "month":I
    const/4 v0, 0x5

    aget-byte v3, p1, v0

    .line 1216
    .local v3, "day":I
    aget-byte v0, p1, v8

    shr-int/lit8 v0, v0, 0x7

    and-int/lit8 v0, v0, 0x1

    const/4 v7, 0x1

    if-ne v0, v7, :cond_0

    const/16 v4, 0xc

    .line 1217
    .local v4, "timeFormat":I
    :goto_0
    aget-byte v0, p1, v8

    and-int/lit8 v5, v0, 0x7f

    .line 1218
    .local v5, "time":I
    const/4 v0, 0x7

    aget-byte v6, p1, v0

    .line 1220
    .local v6, "min":I
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    .line 1221
    const/4 v7, 0x0

    .line 1220
    invoke-interface/range {v0 .. v7}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onTime(IIIIIII)V

    .line 1222
    return-void

    .line 1216
    .end local v4    # "timeFormat":I
    .end local v5    # "time":I
    .end local v6    # "min":I
    :cond_0
    const/16 v4, 0x18

    goto :goto_0
.end method

.method public analyzeUpgradeDateByindexPro([B)V
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    .line 1424
    const/4 v3, 0x4

    aget-byte v3, p1, v3

    and-int/lit16 v2, v3, 0xff

    .line 1425
    .local v2, "l":I
    const/4 v3, 0x3

    aget-byte v3, p1, v3

    and-int/lit16 v0, v3, 0xff

    .line 1426
    .local v0, "h":I
    shl-int/lit8 v3, v0, 0x8

    add-int v1, v3, v2

    .line 1427
    .local v1, "index":I
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCanboxUpgradeForGetDataByIndex(I)V

    .line 1428
    return-void
.end method

.method public analyzeUpgradeStatePro([B)V
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    .line 1412
    const/4 v3, 0x3

    aget-byte v0, p1, v3

    .line 1413
    .local v0, "b":B
    sget-object v2, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    .line 1414
    .local v2, "value":Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->values()[Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 1420
    :goto_1
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v3, v2}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->onCanboxUpgradeState(Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;)V

    .line 1421
    return-void

    .line 1414
    :cond_0
    aget-object v1, v4, v3

    .line 1415
    .local v1, "e":Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;
    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ECanboxUpgrade;->getCode()B

    move-result v6

    if-ne v6, v0, :cond_1

    .line 1416
    move-object v2, v1

    .line 1417
    goto :goto_1

    .line 1414
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public closeOrOpenScreen(Z)V
    .locals 3
    .param p1, "iClose"    # Z

    .prologue
    const/4 v0, 0x7

    .line 404
    if-eqz p1, :cond_0

    .line 405
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 411
    :goto_0
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>>\tcloseOrOpenScreen:\t"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    return-void

    .line 408
    :cond_0
    new-array v0, v0, [B

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    goto :goto_0

    .line 405
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x12t
        0x5t
        0x1bt
    .end array-data

    .line 408
    :array_1
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x12t
        0x4t
        0x1at
    .end array-data
.end method

.method public connectOrDisConnectCanbox(Z)V
    .locals 5
    .param p1, "iConnect"    # Z

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 715
    const/4 v1, 0x5

    new-array v0, v1, [B

    const/16 v1, 0x2e

    aput-byte v1, v0, v3

    const/16 v1, -0x7f

    aput-byte v1, v0, v2

    const/4 v1, 0x2

    aput-byte v2, v0, v1

    .line 716
    .local v0, "buffer":[B
    if-eqz p1, :cond_0

    .line 717
    aput-byte v2, v0, v4

    .line 721
    :goto_0
    const/4 v1, 0x4

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 722
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 723
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tconnectOrDisConnectCanbox:\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    return-void

    .line 719
    :cond_0
    aput-byte v3, v0, v4

    goto :goto_0
.end method

.method public enterIntoAcc()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 213
    const/4 v1, 0x6

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    aput-byte v4, v0, v4

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    const/4 v1, 0x4

    .line 214
    aput-byte v3, v0, v1

    .line 215
    .local v0, "buffer":[B
    const/4 v1, 0x5

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 216
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 217
    return-void
.end method

.method public enterOrOutMute(Z)V
    .locals 6
    .param p1, "iEnter"    # Z

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 275
    const/4 v3, 0x7

    new-array v0, v3, [B

    const/16 v3, -0x56

    aput-byte v3, v0, v2

    const/16 v3, 0x55

    aput-byte v3, v0, v1

    const/4 v3, 0x2

    aput-byte v4, v0, v3

    aput-byte v1, v0, v4

    const/4 v3, 0x4

    .line 276
    const/16 v4, 0x41

    aput-byte v4, v0, v3

    aput-byte v1, v0, v5

    .line 277
    .local v0, "buffer":[B
    if-eqz p1, :cond_0

    :goto_0
    aput-byte v1, v0, v5

    .line 278
    const/4 v1, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 279
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 280
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tenterOrOutMute:\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    return-void

    :cond_0
    move v1, v2

    .line 277
    goto :goto_0
.end method

.method public forcePress()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 854
    const/4 v1, 0x6

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, 0x2e

    aput-byte v2, v0, v1

    const/16 v1, -0x5a

    aput-byte v1, v0, v3

    aput-byte v4, v0, v4

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    .line 856
    .local v0, "buffer":[B
    const/4 v1, 0x5

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 857
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 858
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    const-string v2, ">>>\tforcePress-- "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    return-void
.end method

.method public forceRequestBTAudio(ZLcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;ZZZI)V
    .locals 7
    .param p1, "isForce"    # Z
    .param p2, "eBenzTpye"    # Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;
    .param p3, "isSquare"    # Z
    .param p4, "hasNavi"    # Z
    .param p5, "hasAUX"    # Z
    .param p6, "usbNum"    # I

    .prologue
    const/16 v4, -0x5b

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x4

    .line 782
    if-eqz p1, :cond_2

    .line 783
    const/4 v3, 0x6

    new-array v0, v3, [B

    const/16 v3, 0x2e

    aput-byte v3, v0, v1

    aput-byte v4, v0, v2

    aput-byte v6, v0, v6

    const/4 v3, 0x3

    aput-byte v6, v0, v3

    .line 790
    .local v0, "buffer":[B
    :goto_0
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->$SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye()[I

    move-result-object v3

    invoke-virtual {p2}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    .line 810
    :cond_0
    :goto_1
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v4, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v3, v4, :cond_1

    .line 811
    if-eqz p3, :cond_4

    .line 812
    const/16 v3, 0x40

    aput-byte v3, v0, v5

    .line 813
    if-ne p6, v2, :cond_3

    .line 814
    const/16 v3, 0x41

    aput-byte v3, v0, v5

    .line 831
    :cond_1
    :goto_2
    const/4 v3, 0x5

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v4

    aput-byte v4, v0, v3

    .line 832
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 833
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>\tforceRequestBTAudio-- "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",eBenzTpye = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 834
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",RadioType isSquare = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 835
    const-string v5, ",hasNavi = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",isBT = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p5, :cond_8

    :goto_3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",isAUX = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 836
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",usbNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 833
    invoke-static {v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    return-void

    .line 786
    .end local v0    # "buffer":[B
    :cond_2
    const/4 v3, 0x6

    new-array v0, v3, [B

    const/16 v3, 0x2e

    aput-byte v3, v0, v1

    aput-byte v4, v0, v2

    aput-byte v6, v0, v6

    const/4 v3, 0x3

    aput-byte v2, v0, v3

    .restart local v0    # "buffer":[B
    goto/16 :goto_0

    .line 792
    :sswitch_0
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v4, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v3, v4, :cond_0

    .line 793
    aput-byte v1, v0, v5

    goto/16 :goto_1

    .line 798
    :sswitch_1
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v4, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v3, v4, :cond_0

    .line 799
    aput-byte v2, v0, v5

    goto/16 :goto_1

    .line 803
    :sswitch_2
    sget-object v3, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v4, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZHTD:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v3, v4, :cond_0

    .line 804
    aput-byte v6, v0, v5

    goto/16 :goto_1

    .line 815
    :cond_3
    if-ne p6, v6, :cond_1

    .line 816
    const/16 v3, 0x42

    aput-byte v3, v0, v5

    goto/16 :goto_2

    .line 819
    :cond_4
    const/16 v3, -0x80

    aput-byte v3, v0, v5

    .line 820
    if-nez p4, :cond_5

    if-eqz p5, :cond_5

    .line 821
    const/16 v3, -0x7f

    aput-byte v3, v0, v5

    goto/16 :goto_2

    .line 822
    :cond_5
    if-eqz p4, :cond_6

    if-eqz p5, :cond_6

    .line 823
    const/16 v3, -0x7e

    aput-byte v3, v0, v5

    goto/16 :goto_2

    .line 824
    :cond_6
    if-nez p4, :cond_7

    if-nez p5, :cond_7

    .line 825
    const/16 v3, -0x7d

    aput-byte v3, v0, v5

    goto/16 :goto_2

    .line 826
    :cond_7
    if-eqz p4, :cond_1

    if-nez p5, :cond_1

    .line 827
    const/16 v3, -0x7c

    aput-byte v3, v0, v5

    goto/16 :goto_2

    :cond_8
    move v1, v2

    .line 835
    goto/16 :goto_3

    .line 790
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_0
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method public getAllHornSoundValue()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 394
    const/4 v1, 0x7

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x4

    .line 395
    const/16 v2, 0x3d

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    .line 396
    .local v0, "buffer":[B
    const/4 v1, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 397
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 398
    return-void
.end method

.method public getBenzSize()V
    .locals 2

    .prologue
    .line 674
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 676
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetBenzSize "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    return-void

    .line 674
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x69t
        -0x1t
        0x6ct
    .end array-data
.end method

.method public getBenzType()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 665
    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0x64

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    .line 666
    const/16 v2, 0x68

    aput-byte v2, v0, v1

    .line 665
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 667
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetBenzType "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    return-void
.end method

.method public getBrightnessValue()V
    .locals 2

    .prologue
    .line 515
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 517
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetBrightnessValue"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    return-void

    .line 515
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x2t
        -0x1t
        0x5t
    .end array-data
.end method

.method public getControlInfo(ZLcom/backaudio/android/driver/Mainboard$EControlSource;)V
    .locals 6
    .param p1, "isOriginal"    # Z
    .param p2, "soure"    # Lcom/backaudio/android/driver/Mainboard$EControlSource;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 732
    const/16 v1, 0xd

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, 0x2e

    aput-byte v2, v0, v1

    const/16 v1, -0x7e

    aput-byte v1, v0, v3

    const/16 v1, 0x9

    aput-byte v1, v0, v4

    .line 734
    .local v0, "buffer":[B
    if-eqz p1, :cond_0

    .line 735
    aput-byte v4, v0, v5

    .line 739
    :goto_0
    const/4 v1, 0x4

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EControlSource;->RADIO:Lcom/backaudio/android/driver/Mainboard$EControlSource;

    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EControlSource;->getCode()B

    move-result v2

    aput-byte v2, v0, v1

    .line 740
    const/16 v1, 0xc

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 742
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tgetControlInfo:isOriginal = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 743
    const-string v3, "\tEControlSource--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/backaudio/android/driver/Mainboard$EControlSource;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 742
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    return-void

    .line 737
    :cond_0
    aput-byte v3, v0, v5

    goto :goto_0
.end method

.method public getLanguageSetFromMcu()V
    .locals 2

    .prologue
    .line 605
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 607
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetLanguageSetFromMcu "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    return-void

    .line 605
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x6et
        -0x1t
        0x71t
    .end array-data
.end method

.method public getMCUVersionDate()V
    .locals 2

    .prologue
    .line 437
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 439
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetMCUVersionDate"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    return-void

    .line 437
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x10t
        0x1t
        0x15t
    .end array-data
.end method

.method public getMCUVersionNumber()V
    .locals 2

    .prologue
    .line 446
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 448
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetMCUVersionNumber"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    return-void

    .line 446
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x11t
        0x1t
        0x16t
    .end array-data
.end method

.method public getModeInfo(Lcom/backaudio/android/driver/Mainboard$EModeInfo;)V
    .locals 4
    .param p1, "mode"    # Lcom/backaudio/android/driver/Mainboard$EModeInfo;

    .prologue
    const/4 v3, 0x1

    .line 752
    const/4 v1, 0x5

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, 0x2e

    aput-byte v2, v0, v1

    const/4 v1, -0x1

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v3, v0, v1

    .line 753
    .local v0, "buffer":[B
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->getCode()B

    move-result v2

    aput-byte v2, v0, v1

    .line 754
    const/4 v1, 0x4

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 755
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 756
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tgetModeInfo:EModeInfo--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EModeInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    return-void
.end method

.method public getReverseMediaSetFromMcu()V
    .locals 2

    .prologue
    .line 628
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 630
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetReverseMediaSetFromMcu "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    return-void

    .line 628
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x67t
        -0x1t
        0x6at
    .end array-data
.end method

.method public getReverseSetFromMcu()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 617
    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0x5b

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    .line 618
    const/16 v2, 0x5f

    aput-byte v2, v0, v1

    .line 617
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 619
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetReverseSetFromMcu "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    return-void
.end method

.method public getStoreDataFromMcu()V
    .locals 2

    .prologue
    .line 683
    const/4 v0, 0x7

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 685
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\tgetStoreDataFromMcu  "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    return-void

    .line 683
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0x60t
        0x1t
        0x65t
    .end array-data
.end method

.method public logcatCanbox(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    invoke-interface {v0, p1}, Lcom/backaudio/android/driver/IMainboardEventLisenner;->logcatCanbox(Ljava/lang/String;)V

    .line 1437
    return-void
.end method

.method public openOrCloseRelay(Z)V
    .locals 7
    .param p1, "iOpen"    # Z

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 233
    const/4 v3, 0x7

    new-array v0, v3, [B

    const/4 v3, 0x0

    const/16 v4, -0x56

    aput-byte v4, v0, v3

    const/16 v3, 0x55

    aput-byte v3, v0, v1

    aput-byte v5, v0, v2

    aput-byte v1, v0, v5

    const/4 v3, 0x4

    .line 234
    const/16 v4, 0x40

    aput-byte v4, v0, v3

    aput-byte v1, v0, v6

    .line 235
    .local v0, "buffer":[B
    if-eqz p1, :cond_0

    :goto_0
    aput-byte v1, v0, v6

    .line 236
    const/4 v1, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 237
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 238
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\topenOrCloseRelay:\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    return-void

    :cond_0
    move v1, v2

    .line 235
    goto :goto_0
.end method

.method public powerOffAudio()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 262
    const/4 v1, 0x7

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 264
    .local v0, "protocol":[B
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 265
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v2, ">>>\tpowerOffAudio"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    return-void

    .line 262
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0xet
        0x7t
        0x19t
    .end array-data
.end method

.method public powerOnAudio()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    const/4 v1, 0x7

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    .line 252
    .local v0, "protocol":[B
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 253
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v2, ">>>\tpowerOnAudio"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    return-void

    .line 250
    nop

    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x3t
        0x1t
        0xet
        0x8t
        0x1at
    .end array-data
.end method

.method public readyToStandby()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x1

    const/4 v3, 0x3

    .line 419
    new-array v0, v5, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v4

    const/4 v1, 0x2

    aput-byte v3, v0, v1

    aput-byte v4, v0, v3

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x6

    .line 420
    aput-byte v5, v0, v1

    .line 419
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 421
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\treadyToStandby"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    return-void
.end method

.method public readyToWork()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 428
    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x5

    const/4 v2, 0x4

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    .line 429
    const/16 v2, 0x8

    aput-byte v2, v0, v1

    .line 428
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 430
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    const-string v1, ">>>\treadyToWork"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    return-void
.end method

.method public requestAUXOperate(Lcom/backaudio/android/driver/Mainboard$EAUXOperate;)V
    .locals 4
    .param p1, "eOperate"    # Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    .prologue
    const/4 v3, 0x4

    .line 765
    const/16 v1, 0x8

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, 0x2e

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, -0x59

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte v3, v0, v1

    .line 766
    .local v0, "buffer":[B
    const/4 v1, 0x3

    const/16 v2, 0x59

    aput-byte v2, v0, v1

    .line 767
    const/16 v1, 0x54

    aput-byte v1, v0, v3

    .line 768
    const/4 v1, 0x5

    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->getCode()B

    move-result v2

    aput-byte v2, v0, v1

    .line 769
    const/4 v1, 0x7

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 770
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 771
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\trequestAUXOperate --"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    return-void
.end method

.method public requestUpgradeCanbox(I)V
    .locals 9
    .param p1, "total_packet"    # I

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v4, 0x0

    .line 863
    const/4 v5, 0x6

    new-array v0, v5, [B

    const/16 v5, 0x2e

    aput-byte v5, v0, v4

    const/4 v5, 0x1

    const/16 v6, -0x1f

    aput-byte v6, v0, v5

    aput-byte v7, v0, v7

    .line 865
    .local v0, "buffer":[B
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 866
    .local v1, "fmString":Ljava/lang/String;
    const-string v2, ""

    .line 867
    .local v2, "h":Ljava/lang/String;
    move-object v3, v1

    .line 868
    .local v3, "l":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_0

    .line 869
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 870
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 872
    :cond_0
    const/4 v5, 0x3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_0
    aput-byte v4, v0, v5

    .line 874
    const/4 v4, 0x4

    invoke-static {v3, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 875
    const/4 v4, 0x5

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v5

    aput-byte v5, v0, v4

    .line 876
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 877
    iget-object v4, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    const-string v5, ">>>\trequestUpgradeCanbox"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    return-void

    .line 873
    :cond_1
    invoke-static {v2, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    goto :goto_0
.end method

.method public requestUpgradeMcu([B)V
    .locals 7
    .param p1, "updateInfo"    # [B

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 920
    const/16 v1, 0x9

    new-array v0, v1, [B

    const/16 v1, -0x56

    aput-byte v1, v0, v2

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    aput-byte v6, v0, v4

    .line 921
    const/16 v1, -0x1f

    aput-byte v1, v0, v5

    .line 922
    .local v0, "buffer":[B
    const/4 v1, 0x4

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    .line 923
    aget-byte v1, p1, v3

    aput-byte v1, v0, v6

    .line 924
    const/4 v1, 0x6

    aget-byte v2, p1, v4

    aput-byte v2, v0, v1

    .line 925
    const/4 v1, 0x7

    aget-byte v2, p1, v5

    aput-byte v2, v0, v1

    .line 926
    const/16 v1, 0x8

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 927
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 928
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    const-string v2, ">>>\trequestUpgradeMCU"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    return-void
.end method

.method public sendCoordinateToMcu(III)V
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "status"    # I

    .prologue
    .line 473
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 474
    .local v6, "tmp":Ljava/lang/String;
    const-string v4, ""

    .line 475
    .local v4, "h":Ljava/lang/String;
    move-object v5, v6

    .line 476
    .local v5, "l":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x2

    if-le v7, v8, :cond_0

    .line 477
    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 478
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 480
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    .line 482
    .local v0, "data0":B
    :goto_0
    const/16 v7, 0x10

    invoke-static {v5, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    int-to-byte v1, v7

    .line 483
    .local v1, "data1":B
    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 484
    const-string v4, ""

    .line 485
    move-object v5, v6

    .line 486
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x2

    if-le v7, v8, :cond_1

    .line 487
    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 488
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x2

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 490
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v2, 0x0

    .line 492
    .local v2, "data2":B
    :goto_1
    const/16 v7, 0x10

    invoke-static {v5, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    int-to-byte v3, v7

    .line 493
    .local v3, "data3":B
    const/16 v7, 0xb

    new-array v7, v7, [B

    const/4 v8, 0x0

    .line 494
    const/16 v9, -0x56

    aput-byte v9, v7, v8

    const/4 v8, 0x1

    .line 495
    const/16 v9, 0x55

    aput-byte v9, v7, v8

    const/4 v8, 0x2

    .line 496
    const/4 v9, 0x7

    aput-byte v9, v7, v8

    const/4 v8, 0x3

    .line 497
    const/4 v9, 0x1

    aput-byte v9, v7, v8

    const/4 v8, 0x4

    .line 498
    const/16 v9, 0x49

    aput-byte v9, v7, v8

    const/4 v8, 0x5

    .line 499
    aput-byte v0, v7, v8

    const/4 v8, 0x6

    .line 500
    aput-byte v1, v7, v8

    const/4 v8, 0x7

    .line 501
    aput-byte v2, v7, v8

    const/16 v8, 0x8

    .line 502
    aput-byte v3, v7, v8

    const/16 v8, 0x9

    .line 503
    int-to-byte v9, p3

    aput-byte v9, v7, v8

    const/16 v8, 0xa

    .line 504
    add-int/lit8 v9, v0, 0x51

    add-int/2addr v9, v1

    add-int/2addr v9, v2

    add-int/2addr v9, v3

    add-int/2addr v9, p3

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    .line 493
    invoke-virtual {p0, v7}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 506
    iget-object v7, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, ">>>\tsendCoordinateToMcu x = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",y = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 507
    const-string v9, ",data0 = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",data1 = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",data2 = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 508
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",data3 = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",data4 = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 506
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    return-void

    .line 481
    .end local v0    # "data0":B
    .end local v1    # "data1":B
    .end local v2    # "data2":B
    .end local v3    # "data3":B
    :cond_2
    const/16 v7, 0x10

    .line 480
    invoke-static {v4, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    int-to-byte v0, v7

    goto/16 :goto_0

    .line 491
    .restart local v0    # "data0":B
    .restart local v1    # "data1":B
    :cond_3
    const/16 v7, 0x10

    .line 490
    invoke-static {v4, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    int-to-byte v2, v7

    goto/16 :goto_1
.end method

.method public sendLanguageSetToMcu(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 585
    const/4 v4, 0x7

    new-array v0, v4, [B

    const/16 v4, -0x56

    aput-byte v4, v0, v3

    const/16 v4, 0x55

    aput-byte v4, v0, v5

    const/4 v4, 0x2

    aput-byte v6, v0, v4

    aput-byte v5, v0, v6

    const/4 v4, 0x4

    const/16 v5, 0x6e

    aput-byte v5, v0, v4

    .line 587
    .local v0, "buffer":[B
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ELanguage;->US:Lcom/backaudio/android/driver/Mainboard$ELanguage;

    .line 588
    .local v1, "source":Lcom/backaudio/android/driver/Mainboard$ELanguage;
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$ELanguage;->values()[Lcom/backaudio/android/driver/Mainboard$ELanguage;

    move-result-object v4

    array-length v5, v4

    :goto_0
    if-lt v3, v5, :cond_0

    .line 594
    :goto_1
    const/4 v3, 0x5

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$ELanguage;->getCode()B

    move-result v4

    aput-byte v4, v0, v3

    .line 595
    const/4 v3, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v4

    aput-byte v4, v0, v3

    .line 596
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 597
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>\tsendLanguageSetToMcu type = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    return-void

    .line 588
    :cond_0
    aget-object v2, v4, v3

    .line 589
    .local v2, "temp":Lcom/backaudio/android/driver/Mainboard$ELanguage;
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$ELanguage;->getCode()B

    move-result v6

    if-ne v6, p1, :cond_1

    .line 590
    move-object v1, v2

    .line 591
    goto :goto_1

    .line 588
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public sendRecordToMcu()V
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 467
    return-void

    .line 465
    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x2t
        0x1t
        0x54t
        0x57t
    .end array-data
.end method

.method public sendReverseMediaSetToMcu(I)V
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 564
    const/4 v4, 0x7

    new-array v0, v4, [B

    const/16 v4, -0x56

    aput-byte v4, v0, v3

    const/16 v4, 0x55

    aput-byte v4, v0, v5

    const/4 v4, 0x2

    aput-byte v6, v0, v4

    aput-byte v5, v0, v6

    const/4 v4, 0x4

    const/16 v5, 0x67

    aput-byte v5, v0, v4

    .line 566
    .local v0, "buffer":[B
    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 567
    .local v1, "source":Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->values()[Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    move-result-object v4

    array-length v5, v4

    :goto_0
    if-lt v3, v5, :cond_0

    .line 573
    :goto_1
    const/4 v3, 0x5

    invoke-virtual {v1}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->getCode()B

    move-result v4

    aput-byte v4, v0, v3

    .line 574
    const/4 v3, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v4

    aput-byte v4, v0, v3

    .line 575
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 576
    iget-object v3, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>\tsendReverseMediaSetToMcu type = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    return-void

    .line 567
    :cond_0
    aget-object v2, v4, v3

    .line 568
    .local v2, "temp":Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;
    invoke-virtual {v2}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->getCode()B

    move-result v6

    if-ne v6, p1, :cond_1

    .line 569
    move-object v1, v2

    .line 570
    goto :goto_1

    .line 567
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public sendReverseSetToMcu(I)V
    .locals 5
    .param p1, "parkingType"    # I

    .prologue
    const/4 v4, 0x4

    const/16 v0, 0x8

    const/4 v3, 0x1

    .line 538
    packed-switch p1, :pswitch_data_0

    .line 555
    :goto_0
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>>\tsendReverseSetToMcu type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    return-void

    .line 540
    :pswitch_0
    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    goto :goto_0

    .line 544
    :pswitch_1
    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    const/16 v1, 0x5b

    aput-byte v1, v0, v4

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x7

    .line 545
    const/16 v2, 0x61

    aput-byte v2, v0, v1

    .line 544
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    goto :goto_0

    .line 548
    :pswitch_2
    new-array v0, v0, [B

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    goto :goto_0

    .line 538
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 540
    :array_0
    .array-data 1
        -0x56t
        0x55t
        0x4t
        0x1t
        0x5bt
        0x1t
        0x1t
        0x62t
    .end array-data

    .line 548
    :array_1
    .array-data 1
        -0x56t
        0x55t
        0x4t
        0x1t
        0x5bt
        0x1t
        0x2t
        0x63t
    .end array-data
.end method

.method public sendStoreDataToMcu([B)V
    .locals 9
    .param p1, "date"    # [B

    .prologue
    const/16 v8, 0x80

    const/4 v5, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 692
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 693
    .local v2, "tempBuffer":Ljava/io/ByteArrayOutputStream;
    new-array v0, v5, [B

    fill-array-data v0, :array_0

    .line 695
    .local v0, "buffer":[B
    invoke-virtual {v2, v0, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 696
    array-length v5, p1

    if-ge v5, v8, :cond_0

    .line 697
    array-length v5, p1

    invoke-virtual {v2, p1, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 698
    array-length v5, p1

    rsub-int v5, v5, 0x80

    new-array v4, v5, [B

    .line 699
    .local v4, "tmp":[B
    array-length v5, v4

    invoke-virtual {v2, v4, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 703
    .end local v4    # "tmp":[B
    :goto_0
    new-array v5, v7, [B

    invoke-virtual {v2, v5, v6, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 704
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v1

    .line 705
    .local v1, "temp":B
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 706
    .local v3, "tempByte":[B
    array-length v5, v3

    add-int/lit8 v5, v5, -0x1

    aput-byte v1, v3, v5

    .line 707
    invoke-virtual {p0, v3}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 708
    iget-object v5, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    .line 709
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, ">>>\tsendStoreDataToMcu data = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 710
    invoke-static {v3}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 709
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 708
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    return-void

    .line 701
    .end local v1    # "temp":B
    .end local v3    # "tempByte":[B
    :cond_0
    invoke-virtual {v2, p1, v6, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 693
    :array_0
    .array-data 1
        -0x56t
        0x55t
        -0x7et
        0x1t
        0x61t
    .end array-data
.end method

.method public sendUpdateCanboxInfoByIndex(I[B)V
    .locals 11
    .param p1, "index"    # I
    .param p2, "date"    # [B

    .prologue
    .line 883
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 884
    .local v5, "tempBuffer":Ljava/io/ByteArrayOutputStream;
    const/4 v8, 0x5

    new-array v0, v8, [B

    const/4 v8, 0x0

    const/16 v9, 0x2e

    aput-byte v9, v0, v8

    const/4 v8, 0x1

    const/16 v9, -0x1d

    aput-byte v9, v0, v8

    const/4 v8, 0x2

    const/16 v9, -0x7e

    aput-byte v9, v0, v8

    .line 886
    .local v0, "buffer":[B
    if-ltz p1, :cond_2

    .line 887
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 888
    .local v1, "fmString":Ljava/lang/String;
    const-string v2, ""

    .line 889
    .local v2, "h":Ljava/lang/String;
    move-object v3, v1

    .line 890
    .local v3, "l":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-le v8, v9, :cond_0

    .line 891
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 892
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 894
    :cond_0
    const/4 v9, 0x3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x0

    :goto_0
    aput-byte v8, v0, v9

    .line 896
    const/4 v8, 0x4

    const/16 v9, 0x10

    invoke-static {v3, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    int-to-byte v9, v9

    aput-byte v9, v0, v8

    .line 901
    .end local v1    # "fmString":Ljava/lang/String;
    .end local v2    # "h":Ljava/lang/String;
    .end local v3    # "l":Ljava/lang/String;
    :goto_1
    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {v5, v0, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 902
    array-length v8, p2

    const/16 v9, 0x80

    if-ge v8, v9, :cond_3

    .line 903
    const/4 v8, 0x0

    array-length v9, p2

    invoke-virtual {v5, p2, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 904
    array-length v8, p2

    rsub-int v8, v8, 0x80

    new-array v7, v8, [B

    .line 905
    .local v7, "tmp":[B
    const/4 v8, 0x0

    array-length v9, v7

    invoke-virtual {v5, v7, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 909
    .end local v7    # "tmp":[B
    :goto_2
    const/4 v8, 0x1

    new-array v8, v8, [B

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v5, v8, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 910
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v4

    .line 911
    .local v4, "temp":B
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 912
    .local v6, "tempByte":[B
    array-length v8, v6

    add-int/lit8 v8, v8, -0x1

    aput-byte v4, v6, v8

    .line 914
    invoke-virtual {p0, v6}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 915
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ">>>\tsendUpdateCanboxInfoByIndex;\tindex--"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    return-void

    .line 895
    .end local v4    # "temp":B
    .end local v6    # "tempByte":[B
    .restart local v1    # "fmString":Ljava/lang/String;
    .restart local v2    # "h":Ljava/lang/String;
    .restart local v3    # "l":Ljava/lang/String;
    :cond_1
    const/16 v8, 0x10

    .line 894
    invoke-static {v2, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v8

    int-to-byte v8, v8

    goto :goto_0

    .line 898
    .end local v1    # "fmString":Ljava/lang/String;
    .end local v2    # "h":Ljava/lang/String;
    .end local v3    # "l":Ljava/lang/String;
    :cond_2
    const/4 v8, 0x3

    const/16 v9, 0x1f

    aput-byte v9, v0, v8

    .line 899
    const/4 v8, 0x4

    const/16 v9, 0x1f

    aput-byte v9, v0, v8

    goto :goto_1

    .line 907
    :cond_3
    const/4 v8, 0x0

    const/16 v9, 0x80

    invoke-virtual {v5, p2, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2
.end method

.method public sendUpdateMcuInfoByIndex(I[B)V
    .locals 11
    .param p1, "index"    # I
    .param p2, "date"    # [B

    .prologue
    .line 953
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 954
    .local v5, "tempBuffer":Ljava/io/ByteArrayOutputStream;
    const/4 v8, 0x6

    new-array v0, v8, [B

    const/4 v8, 0x0

    const/16 v9, -0x56

    aput-byte v9, v0, v8

    const/4 v8, 0x1

    const/16 v9, 0x55

    aput-byte v9, v0, v8

    const/4 v8, 0x2

    const/16 v9, -0x7d

    aput-byte v9, v0, v8

    const/4 v8, 0x3

    .line 955
    const/16 v9, -0x1d

    aput-byte v9, v0, v8

    .line 956
    .local v0, "buffer":[B
    if-ltz p1, :cond_2

    .line 957
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 958
    .local v1, "fmString":Ljava/lang/String;
    const-string v2, ""

    .line 959
    .local v2, "h":Ljava/lang/String;
    move-object v3, v1

    .line 960
    .local v3, "l":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-le v8, v9, :cond_0

    .line 961
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 962
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 964
    :cond_0
    const/4 v9, 0x4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x0

    :goto_0
    aput-byte v8, v0, v9

    .line 966
    const/4 v8, 0x5

    const/16 v9, 0x10

    invoke-static {v3, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v9

    int-to-byte v9, v9

    aput-byte v9, v0, v8

    .line 971
    .end local v1    # "fmString":Ljava/lang/String;
    .end local v2    # "h":Ljava/lang/String;
    .end local v3    # "l":Ljava/lang/String;
    :goto_1
    const/4 v8, 0x0

    const/4 v9, 0x6

    invoke-virtual {v5, v0, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 972
    array-length v8, p2

    const/16 v9, 0x80

    if-ge v8, v9, :cond_3

    .line 973
    const/4 v8, 0x0

    array-length v9, p2

    invoke-virtual {v5, p2, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 974
    array-length v8, p2

    rsub-int v8, v8, 0x80

    new-array v7, v8, [B

    .line 975
    .local v7, "tmp":[B
    const/4 v8, 0x0

    array-length v9, v7

    invoke-virtual {v5, v7, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 979
    .end local v7    # "tmp":[B
    :goto_2
    const/4 v8, 0x1

    new-array v8, v8, [B

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v5, v8, v9, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 980
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v4

    .line 981
    .local v4, "temp":B
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 982
    .local v6, "tempByte":[B
    array-length v8, v6

    add-int/lit8 v8, v8, -0x1

    aput-byte v4, v6, v8

    .line 983
    invoke-virtual {p0, v6}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 984
    iget-object v8, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ">>>\tsendUpdateMcuInfoByIndex;\tindex-- "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    return-void

    .line 965
    .end local v4    # "temp":B
    .end local v6    # "tempByte":[B
    .restart local v1    # "fmString":Ljava/lang/String;
    .restart local v2    # "h":Ljava/lang/String;
    .restart local v3    # "l":Ljava/lang/String;
    :cond_1
    const/16 v8, 0x10

    .line 964
    invoke-static {v2, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v8

    int-to-byte v8, v8

    goto :goto_0

    .line 968
    .end local v1    # "fmString":Ljava/lang/String;
    .end local v2    # "h":Ljava/lang/String;
    .end local v3    # "l":Ljava/lang/String;
    :cond_2
    const/4 v8, 0x4

    const/16 v9, 0x1f

    aput-byte v9, v0, v8

    .line 969
    const/4 v8, 0x5

    const/16 v9, 0x1f

    aput-byte v9, v0, v8

    goto :goto_1

    .line 977
    :cond_3
    const/4 v8, 0x0

    const/16 v9, 0x80

    invoke-virtual {v5, p2, v8, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2
.end method

.method public sendUpgradeMcuHeaderInfo(I)V
    .locals 10
    .param p1, "length"    # I

    .prologue
    const/16 v9, 0x10

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x0

    .line 933
    const/4 v5, 0x7

    new-array v0, v5, [B

    const/16 v5, -0x56

    aput-byte v5, v0, v4

    const/4 v5, 0x1

    const/16 v6, 0x55

    aput-byte v6, v0, v5

    aput-byte v8, v0, v7

    .line 934
    const/16 v5, -0x1b

    aput-byte v5, v0, v8

    .line 935
    .local v0, "buffer":[B
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    .line 936
    .local v1, "fmString":Ljava/lang/String;
    const-string v2, ""

    .line 937
    .local v2, "h":Ljava/lang/String;
    move-object v3, v1

    .line 938
    .local v3, "l":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_0

    .line 939
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 940
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 942
    :cond_0
    const/4 v5, 0x4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_0
    aput-byte v4, v0, v5

    .line 944
    const/4 v4, 0x5

    invoke-static {v3, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 945
    const/4 v4, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v5

    aput-byte v5, v0, v4

    .line 946
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 947
    iget-object v4, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    const-string v5, ">>>\tsendUpgradeMcuHeaderInfo"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    return-void

    .line 943
    :cond_1
    invoke-static {v2, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    goto :goto_0
.end method

.method public setAllHornSoundValue(IIIII)V
    .locals 5
    .param p1, "main"    # I
    .param p2, "fLeft"    # I
    .param p3, "fRight"    # I
    .param p4, "rLeft"    # I
    .param p5, "rRight"    # I

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x1

    .line 323
    const/16 v1, 0xb

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    const/4 v1, 0x4

    .line 324
    const/16 v2, 0x3c

    aput-byte v2, v0, v1

    .line 325
    .local v0, "buffer":[B
    const/4 v1, 0x5

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 326
    const/4 v1, 0x6

    int-to-byte v2, p2

    aput-byte v2, v0, v1

    .line 327
    int-to-byte v1, p3

    aput-byte v1, v0, v4

    .line 328
    const/16 v1, 0x8

    int-to-byte v2, p4

    aput-byte v2, v0, v1

    .line 329
    const/16 v1, 0x9

    int-to-byte v2, p5

    aput-byte v2, v0, v1

    .line 330
    const/16 v1, 0xa

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 331
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 332
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tsetAllHornSoundValue:\tmain = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",fLeft = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 333
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",fRight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",rLeft = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 334
    const-string v3, ",rRight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 332
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void
.end method

.method public setBenzSize(I)V
    .locals 5
    .param p1, "benzSize"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 653
    const/4 v1, 0x7

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0x69

    aput-byte v2, v0, v1

    .line 655
    .local v0, "buffer":[B
    const/4 v1, 0x5

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 656
    const/4 v1, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 657
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 658
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tsetBenzSize type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    return-void
.end method

.method public setBenzType(Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;)V
    .locals 5
    .param p1, "benzType"    # Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 639
    const/4 v1, 0x7

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0x65

    aput-byte v2, v0, v1

    .line 641
    .local v0, "buffer":[B
    const/4 v1, 0x5

    invoke-virtual {p1}, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzTpye;->getCode()B

    move-result v2

    aput-byte v2, v0, v1

    .line 642
    const/4 v1, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 643
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 644
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tsetBenzType type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    return-void
.end method

.method public setBrightnessToMcu(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 525
    const/4 v1, 0x7

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    aput-byte v5, v0, v4

    aput-byte v3, v0, v5

    const/4 v1, 0x4

    aput-byte v4, v0, v1

    .line 526
    .local v0, "buffer":[B
    const/4 v1, 0x5

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 527
    const/4 v1, 0x6

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 528
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 529
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tsetBrightnessToMcu value = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    return-void
.end method

.method public setDVSoundValue(IIIII)V
    .locals 5
    .param p1, "main"    # I
    .param p2, "fLeft"    # I
    .param p3, "fRight"    # I
    .param p4, "rLeft"    # I
    .param p5, "rRight"    # I

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x1

    .line 352
    const/16 v1, 0xb

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    const/4 v1, 0x4

    .line 353
    const/16 v2, 0x63

    aput-byte v2, v0, v1

    .line 354
    .local v0, "buffer":[B
    const/4 v1, 0x5

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 355
    const/4 v1, 0x6

    int-to-byte v2, p2

    aput-byte v2, v0, v1

    .line 356
    int-to-byte v1, p3

    aput-byte v1, v0, v4

    .line 357
    const/16 v1, 0x8

    int-to-byte v2, p4

    aput-byte v2, v0, v1

    .line 358
    const/16 v1, 0x9

    int-to-byte v2, p5

    aput-byte v2, v0, v1

    .line 359
    const/16 v1, 0xa

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 360
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 361
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tsetDVSoundValue:\tDV main = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",fLeft = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 362
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",fRight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",rLeft = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 363
    const-string v3, ",rRight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 361
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    return-void
.end method

.method public setMainboardEventLisenner(Lcom/backaudio/android/driver/IMainboardEventLisenner;)V
    .locals 0
    .param p1, "mainboardEventLisenner"    # Lcom/backaudio/android/driver/IMainboardEventLisenner;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardEventLisenner:Lcom/backaudio/android/driver/IMainboardEventLisenner;

    .line 210
    return-void
.end method

.method public setOldCBTAudio()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 843
    sget-object v1, Lcom/touchus/publicutils/sysconst/BenzModel;->benzCan:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    sget-object v2, Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;->ZMYT:Lcom/touchus/publicutils/sysconst/BenzModel$EBenzCAN;

    if-ne v1, v2, :cond_0

    .line 844
    const/4 v1, 0x6

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, 0x2e

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, -0x5b

    aput-byte v2, v0, v1

    aput-byte v3, v0, v3

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    const/4 v1, 0x4

    .line 845
    const/16 v2, -0x80

    aput-byte v2, v0, v1

    .line 847
    .local v0, "buffer":[B
    const/4 v1, 0x5

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckCanboxByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 848
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeCanbox([B)V

    .line 849
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->CANBOX:Ljava/lang/String;

    const-string v2, ">>>\tsetOldCBTAudio-- "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    .end local v0    # "buffer":[B
    :cond_0
    return-void
.end method

.method public setSoundTypeValue(III)V
    .locals 6
    .param p1, "trebleValue"    # I
    .param p2, "midtonesValue"    # I
    .param p3, "bassValue"    # I

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 376
    const/16 v1, 0x9

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    const/4 v1, 0x3

    aput-byte v3, v0, v1

    .line 377
    const/16 v1, 0x5c

    aput-byte v1, v0, v4

    aput-byte v3, v0, v5

    .line 378
    .local v0, "buffer":[B
    int-to-byte v1, p1

    aput-byte v1, v0, v5

    .line 379
    const/4 v1, 0x6

    int-to-byte v2, p2

    aput-byte v2, v0, v1

    .line 380
    const/4 v1, 0x7

    int-to-byte v2, p3

    aput-byte v2, v0, v1

    .line 382
    const/16 v1, 0x8

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 383
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 384
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>\tsetSoundTypeValue:\ttrebleValue = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 385
    const-string v3, ",midtonesValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",bassValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 386
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 384
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    return-void
.end method

.method public showCarLayer(Lcom/backaudio/android/driver/Mainboard$ECarLayer;)V
    .locals 5
    .param p1, "eCarLayer"    # Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 455
    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/4 v1, 0x2

    aput-byte v4, v0, v1

    aput-byte v3, v0, v4

    const/4 v1, 0x4

    const/16 v2, 0x4a

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    .line 456
    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->getCode()B

    move-result v2

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    .line 457
    invoke-virtual {p1}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->getCode()B

    move-result v2

    add-int/lit8 v2, v2, 0x4e

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 455
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 458
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->MCU:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>>\tshowCarLayer == "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    return-void
.end method

.method public wakeupMcu()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 220
    const/4 v1, 0x6

    new-array v0, v1, [B

    const/4 v1, 0x0

    const/16 v2, -0x56

    aput-byte v2, v0, v1

    const/16 v1, 0x55

    aput-byte v1, v0, v4

    aput-byte v3, v0, v3

    const/4 v1, 0x3

    aput-byte v4, v0, v1

    const/4 v1, 0x4

    .line 221
    aput-byte v3, v0, v1

    .line 222
    .local v0, "buffer":[B
    const/4 v1, 0x5

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->getCheckMcuByte([B)B

    move-result v2

    aput-byte v2, v0, v1

    .line 223
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/Mainboard;->writeToMcu([B)V

    .line 224
    return-void
.end method

.method protected wrapCanbox([B)[B
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 988
    if-nez p1, :cond_0

    .line 989
    const/4 v2, 0x0

    .line 1003
    :goto_0
    return-object v2

    .line 991
    :cond_0
    array-length v3, p1

    add-int/lit8 v3, v3, 0x6

    new-array v2, v3, [B

    .line 992
    .local v2, "mcuProtocol":[B
    const/4 v3, 0x0

    const/16 v4, -0x56

    aput-byte v4, v2, v3

    .line 993
    const/16 v3, 0x55

    aput-byte v3, v2, v5

    .line 994
    array-length v3, p1

    add-int/lit8 v3, v3, 0x2

    int-to-byte v3, v3

    aput-byte v3, v2, v6

    .line 995
    aput-byte v5, v2, v7

    .line 996
    const/16 v3, 0x50

    aput-byte v3, v2, v8

    .line 997
    aget-byte v3, v2, v6

    aget-byte v4, v2, v7

    add-int/2addr v3, v4

    aget-byte v4, v2, v8

    add-int/2addr v3, v4

    int-to-byte v0, v3

    .line 998
    .local v0, "check":B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p1

    if-lt v1, v3, :cond_1

    .line 1002
    array-length v3, p1

    add-int/lit8 v3, v3, 0x5

    aput-byte v0, v2, v3

    goto :goto_0

    .line 999
    :cond_1
    add-int/lit8 v3, v1, 0x5

    aget-byte v4, p1, v1

    aput-byte v4, v2, v3

    .line 1000
    aget-byte v3, p1, v1

    add-int/2addr v3, v0

    int-to-byte v0, v3

    .line 998
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public declared-synchronized writeBlueTooth([B)V
    .locals 1
    .param p1, "btProtocol"    # [B

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardIO:Lcom/backaudio/android/driver/SerialPortMainboardIO;

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/SerialPortMainboardIO;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    monitor-exit p0

    return-void

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized writeCanbox([B)V
    .locals 4
    .param p1, "buff"    # [B

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/backaudio/android/driver/Mainboard;->wrapCanbox([B)[B

    move-result-object v0

    .line 191
    .local v0, "canboxProtocol":[B
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->mainboardIO:Lcom/backaudio/android/driver/SerialPortMainboardIO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 201
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 194
    :cond_1
    if-eqz v0, :cond_0

    .line 195
    :try_start_1
    const-string v1, "driverlog"

    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "canboxProtocol write = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 197
    invoke-static {v0}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 196
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v1, p0, Lcom/backaudio/android/driver/Mainboard;->mcuIO:Lcom/backaudio/android/driver/SerialPortMcuIO;

    invoke-virtual {v1, v0}, Lcom/backaudio/android/driver/SerialPortMcuIO;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 190
    .end local v0    # "canboxProtocol":[B
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public writeToMcu([B)V
    .locals 1
    .param p1, "mcuProtocol"    # [B

    .prologue
    .line 186
    iget-object v0, p0, Lcom/backaudio/android/driver/Mainboard;->mcuIO:Lcom/backaudio/android/driver/SerialPortMcuIO;

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/SerialPortMcuIO;->write([B)V

    .line 187
    return-void
.end method
