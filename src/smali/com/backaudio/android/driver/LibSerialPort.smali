.class public Lcom/backaudio/android/driver/LibSerialPort;
.super Ljava/lang/Object;
.source "LibSerialPort.java"


# static fields
.field private static openedSerialPort:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mFd:Ljava/io/FileDescriptor;

.field private mFileInputStream:Ljava/io/FileInputStream;

.field private mFileOutputStream:Ljava/io/FileOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "SerialPort"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/LibSerialPort;->openedSerialPort:Ljava/util/List;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/io/File;II)V
    .locals 5
    .param p1, "device"    # Ljava/io/File;
    .param p2, "baudrate"    # I
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    .line 33
    .local v0, "alreadyOpened":Z
    sget-object v1, Lcom/backaudio/android/driver/LibSerialPort;->openedSerialPort:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 34
    const-class v2, Lcom/backaudio/android/driver/LibSerialPort;

    monitor-enter v2

    .line 35
    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/LibSerialPort;->openedSerialPort:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/backaudio/android/driver/LibSerialPort;->open(Ljava/lang/String;II)Ljava/io/FileDescriptor;

    move-result-object v1

    iput-object v1, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFd:Ljava/io/FileDescriptor;

    .line 38
    iget-object v1, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFd:Ljava/io/FileDescriptor;

    if-nez v1, :cond_0

    .line 39
    new-instance v1, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot open serialport:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 34
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 41
    :cond_0
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFd:Ljava/io/FileDescriptor;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    iput-object v1, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFileInputStream:Ljava/io/FileInputStream;

    .line 42
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFd:Ljava/io/FileDescriptor;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    iput-object v1, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 43
    sget-object v1, Lcom/backaudio/android/driver/LibSerialPort;->openedSerialPort:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :goto_1
    if-eqz v0, :cond_3

    .line 53
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "serialport is already opened! cannot open now"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 55
    :cond_3
    return-void
.end method

.method private native open(Ljava/lang/String;II)Ljava/io/FileDescriptor;
.end method


# virtual methods
.method public native close(Ljava/io/FileDescriptor;)V
.end method

.method public getmFileInputStream()Ljava/io/FileInputStream;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFileInputStream:Ljava/io/FileInputStream;

    return-object v0
.end method

.method public getmFileOutputStream()Ljava/io/FileOutputStream;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/backaudio/android/driver/LibSerialPort;->mFileOutputStream:Ljava/io/FileOutputStream;

    return-object v0
.end method
