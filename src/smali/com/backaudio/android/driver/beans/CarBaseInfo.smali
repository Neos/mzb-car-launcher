.class public Lcom/backaudio/android/driver/beans/CarBaseInfo;
.super Ljava/lang/Object;
.source "CarBaseInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/backaudio/android/driver/beans/CarBaseInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private foglight:Z

.field private iAccOpen:Z

.field private iBack:Z

.field private iBrake:Z

.field private iDistantLightOpen:Z

.field private iFlash:Z

.field private iFootBrake:Z

.field private iFront:Z

.field private iInP:Z

.field private iInRevering:Z

.field private iLeftBackOpen:Z

.field private iLeftFrontOpen:Z

.field private iLightValue:I

.field private iNearLightOpen:Z

.field private iPowerOn:Z

.field private iRightBackOpen:Z

.field private iRightFrontOpen:Z

.field private iSafetyBelt:Z

.field public ichange:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/backaudio/android/driver/beans/CarBaseInfo$1;

    invoke-direct {v0}, Lcom/backaudio/android/driver/beans/CarBaseInfo$1;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 77
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    .line 10
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    .line 12
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    .line 14
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    .line 16
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    .line 18
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    .line 20
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    .line 22
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    .line 24
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    .line 26
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    .line 28
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    .line 30
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    .line 32
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    .line 34
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    .line 36
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    .line 38
    iput v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLightValue:I

    .line 40
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->foglight:Z

    .line 42
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    .line 44
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 46
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    .line 10
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    .line 12
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    .line 14
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    .line 16
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    .line 18
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    .line 20
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    .line 22
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    .line 24
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    .line 26
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    .line 28
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    .line 30
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    .line 32
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    .line 34
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    .line 36
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    .line 38
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLightValue:I

    .line 40
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->foglight:Z

    .line 42
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    .line 44
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_f

    :goto_f
    iput-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    .line 65
    return-void

    :cond_0
    move v0, v2

    .line 49
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 50
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 51
    goto :goto_2

    :cond_3
    move v0, v2

    .line 52
    goto :goto_3

    :cond_4
    move v0, v2

    .line 53
    goto :goto_4

    :cond_5
    move v0, v2

    .line 54
    goto :goto_5

    :cond_6
    move v0, v2

    .line 55
    goto :goto_6

    :cond_7
    move v0, v2

    .line 56
    goto :goto_7

    :cond_8
    move v0, v2

    .line 57
    goto :goto_8

    :cond_9
    move v0, v2

    .line 58
    goto :goto_9

    :cond_a
    move v0, v2

    .line 59
    goto :goto_a

    :cond_b
    move v0, v2

    .line 60
    goto :goto_b

    :cond_c
    move v0, v2

    .line 61
    goto :goto_c

    :cond_d
    move v0, v2

    .line 62
    goto :goto_d

    :cond_e
    move v0, v2

    .line 63
    goto :goto_e

    :cond_f
    move v1, v2

    .line 64
    goto :goto_f
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public getiLightValue()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLightValue:I

    return v0
.end method

.method public isFoglight()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->foglight:Z

    return v0
.end method

.method public isiAccOpen()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    return v0
.end method

.method public isiBack()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    return v0
.end method

.method public isiBrake()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    return v0
.end method

.method public isiDistantLightOpen()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    return v0
.end method

.method public isiFlash()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    return v0
.end method

.method public isiFootBrake()Z
    .locals 1

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    return v0
.end method

.method public isiFront()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    return v0
.end method

.method public isiInP()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    return v0
.end method

.method public isiInRevering()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    return v0
.end method

.method public isiLeftBackOpen()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    return v0
.end method

.method public isiLeftFrontOpen()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    return v0
.end method

.method public isiNearLightOpen()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    return v0
.end method

.method public isiPowerOn()Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    return v0
.end method

.method public isiRightBackOpen()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    return v0
.end method

.method public isiRightFrontOpen()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    return v0
.end method

.method public isiSafetyBelt()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    return v0
.end method

.method public setFoglight(Z)V
    .locals 2
    .param p1, "foglight"    # Z

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->foglight:Z

    xor-int/2addr v1, p1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 264
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->foglight:Z

    .line 265
    return-void
.end method

.method public setiAccOpen(Z)V
    .locals 0
    .param p1, "iAccOpen"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    .line 94
    return-void
.end method

.method public setiBack(Z)V
    .locals 0
    .param p1, "iBack"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    .line 167
    return-void
.end method

.method public setiBrake(Z)V
    .locals 2
    .param p1, "iBrake"    # Z

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    xor-int/2addr v1, p1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 239
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    .line 240
    return-void
.end method

.method public setiDistantLightOpen(Z)V
    .locals 2
    .param p1, "iDistantLightOpen"    # Z

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    xor-int/2addr v1, p1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 85
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    .line 86
    return-void
.end method

.method public setiFlash(Z)V
    .locals 0
    .param p1, "iFlash"    # Z

    .prologue
    .line 272
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    .line 273
    return-void
.end method

.method public setiFootBrake(Z)V
    .locals 2
    .param p1, "iFootBrake"    # Z

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    xor-int/2addr v1, p1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 289
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    .line 290
    return-void
.end method

.method public setiFront(Z)V
    .locals 0
    .param p1, "iFront"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    .line 159
    return-void
.end method

.method public setiInP(Z)V
    .locals 0
    .param p1, "iInP"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    .line 111
    return-void
.end method

.method public setiInRevering(Z)V
    .locals 0
    .param p1, "iInRevering"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    .line 119
    return-void
.end method

.method public setiLeftBackOpen(Z)V
    .locals 0
    .param p1, "iLeftBackOpen"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    .line 135
    return-void
.end method

.method public setiLeftFrontOpen(Z)V
    .locals 0
    .param p1, "iLeftFrontOpen"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    .line 151
    return-void
.end method

.method public setiLightValue(I)V
    .locals 0
    .param p1, "iLightValue"    # I

    .prologue
    .line 247
    iput p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLightValue:I

    .line 248
    return-void
.end method

.method public setiNearLightOpen(Z)V
    .locals 1
    .param p1, "iNearLightOpen"    # Z

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    xor-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->ichange:Z

    .line 102
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    .line 103
    return-void
.end method

.method public setiPowerOn(Z)V
    .locals 0
    .param p1, "iPowerOn"    # Z

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    .line 256
    return-void
.end method

.method public setiRightBackOpen(Z)V
    .locals 0
    .param p1, "iRightBackOpen"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    .line 127
    return-void
.end method

.method public setiRightFrontOpen(Z)V
    .locals 0
    .param p1, "iRightFrontOpen"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    .line 143
    return-void
.end method

.method public setiSafetyBelt(Z)V
    .locals 0
    .param p1, "iSafetyBelt"    # Z

    .prologue
    .line 280
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    .line 281
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "CarBaseInfo [iDistantLightOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 199
    const-string v1, ", iPowerOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 201
    const-string v1, ", iAccOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 203
    const-string v1, ", iBrake="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 205
    const-string v1, ", iNearLightOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 207
    const-string v1, ", iInP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 209
    const-string v1, ", iInRevering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 211
    const-string v1, ", iRightBackOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213
    const-string v1, ", iLeftBackOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 215
    const-string v1, ", iRightFrontOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 217
    const-string v1, ", iLeftFrontOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 219
    const-string v1, ", iFront="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 221
    const-string v1, ", iBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 223
    const-string v1, ", iFlash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 225
    const-string v1, ", iSafetyBelt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 227
    const-string v1, ", iFootBrake="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 229
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iDistantLightOpen:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 177
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iPowerOn:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 178
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iAccOpen:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 179
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBrake:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 180
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iNearLightOpen:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 181
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInP:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 182
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iInRevering:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 183
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightBackOpen:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 184
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftBackOpen:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 185
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iRightFrontOpen:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 186
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iLeftFrontOpen:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 187
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFront:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 188
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iBack:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 189
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFlash:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 190
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iSafetyBelt:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 191
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/CarBaseInfo;->iFootBrake:Z

    if-eqz v0, :cond_f

    :goto_f
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 192
    return-void

    :cond_0
    move v0, v2

    .line 176
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 177
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 178
    goto :goto_2

    :cond_3
    move v0, v2

    .line 179
    goto :goto_3

    :cond_4
    move v0, v2

    .line 180
    goto :goto_4

    :cond_5
    move v0, v2

    .line 181
    goto :goto_5

    :cond_6
    move v0, v2

    .line 182
    goto :goto_6

    :cond_7
    move v0, v2

    .line 183
    goto :goto_7

    :cond_8
    move v0, v2

    .line 184
    goto :goto_8

    :cond_9
    move v0, v2

    .line 185
    goto :goto_9

    :cond_a
    move v0, v2

    .line 186
    goto :goto_a

    :cond_b
    move v0, v2

    .line 187
    goto :goto_b

    :cond_c
    move v0, v2

    .line 188
    goto :goto_c

    :cond_d
    move v0, v2

    .line 189
    goto :goto_d

    :cond_e
    move v0, v2

    .line 190
    goto :goto_e

    :cond_f
    move v1, v2

    .line 191
    goto :goto_f
.end method
