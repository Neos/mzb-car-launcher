.class public Lcom/backaudio/android/driver/beans/CarRunInfo;
.super Ljava/lang/Object;
.source "CarRunInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/backaudio/android/driver/beans/CarRunInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private averSpeed:I

.field private curSpeed:I

.field private mileage:I

.field private outsideTemp:D

.field private revolutions:I

.field private totalMileage:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/backaudio/android/driver/beans/CarRunInfo$1;

    invoke-direct {v0}, Lcom/backaudio/android/driver/beans/CarRunInfo$1;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/beans/CarRunInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    .line 18
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    .line 33
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    .line 38
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    .line 18
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    .line 33
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    .line 38
    iput v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    .line 49
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public getAverSpeed()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    return v0
.end method

.method public getCurSpeed()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    return v0
.end method

.method public getMileage()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    return v0
.end method

.method public getOutsideTemp()D
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    return-wide v0
.end method

.method public getRevolutions()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    return v0
.end method

.method public getTotalMileage()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    return-wide v0
.end method

.method public setAverSpeed(I)V
    .locals 0
    .param p1, "averSpeed"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    .line 69
    return-void
.end method

.method public setCurSpeed(I)V
    .locals 0
    .param p1, "curSpeed"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    .line 109
    return-void
.end method

.method public setMileage(I)V
    .locals 0
    .param p1, "mileage"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    .line 77
    return-void
.end method

.method public setOutsideTemp(D)V
    .locals 1
    .param p1, "outsideTemp"    # D

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    .line 93
    return-void
.end method

.method public setRevolutions(I)V
    .locals 0
    .param p1, "revolutions"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    .line 101
    return-void
.end method

.method public setTotalMileage(J)V
    .locals 1
    .param p1, "totalMileage"    # J

    .prologue
    .line 84
    iput-wide p1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    .line 85
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "CarRunInfo [averSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    iget v1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 132
    const-string v1, ", mileage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget v1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 134
    const-string v1, ", totalMileage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    iget-wide v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 136
    const-string v1, ", outsideTemp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    iget-wide v2, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, ", revolutions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    iget v1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, ", curSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    iget v1, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 142
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 119
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->averSpeed:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->mileage:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->totalMileage:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 122
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->outsideTemp:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 123
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->revolutions:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    iget v0, p0, Lcom/backaudio/android/driver/beans/CarRunInfo;->curSpeed:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    return-void
.end method
