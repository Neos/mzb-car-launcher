.class public Lcom/backaudio/android/driver/beans/AirInfo;
.super Ljava/lang/Object;
.source "AirInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/backaudio/android/driver/beans/AirInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private iACOpen:Z

.field private iAirOpen:Z

.field private iAuto1:Z

.field private iAuto2:Z

.field private iBackWind:Z

.field private iDownWind:Z

.field private iDual:Z

.field private iFlatWind:Z

.field private iFront:Z

.field private iFrontWind:Z

.field private iHadChange:Z

.field private iInnerLoop:Z

.field private iMaxFrontWind:Z

.field private iRear:Z

.field private iSync:Z

.field private iWindDirAuto:Z

.field private leftTemp:D

.field private level:D

.field private rightTemp:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/backaudio/android/driver/beans/AirInfo$1;

    invoke-direct {v0}, Lcom/backaudio/android/driver/beans/AirInfo$1;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/beans/AirInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 111
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    .line 10
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    .line 12
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    .line 14
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    .line 16
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    .line 18
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    .line 20
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    .line 22
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    .line 24
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    .line 26
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    .line 28
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    .line 30
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    .line 32
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    .line 34
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    .line 36
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    .line 38
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    .line 43
    iput-wide v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    .line 45
    iput-wide v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    .line 49
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide/high16 v6, 0x402e000000000000L    # 15.0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    .line 10
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    .line 12
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    .line 14
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    .line 16
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    .line 18
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    .line 20
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    .line 22
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    .line 24
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    .line 26
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    .line 28
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    .line 30
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    .line 32
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    .line 34
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    .line 36
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    .line 38
    iput-boolean v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    .line 41
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    .line 43
    iput-wide v6, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    .line 45
    iput-wide v6, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_f

    :goto_f
    iput-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    .line 71
    return-void

    :cond_0
    move v0, v2

    .line 52
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 53
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 54
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 55
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 56
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 57
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 58
    goto :goto_6

    :cond_7
    move v0, v2

    .line 59
    goto :goto_7

    :cond_8
    move v0, v2

    .line 60
    goto :goto_8

    :cond_9
    move v0, v2

    .line 61
    goto :goto_9

    :cond_a
    move v0, v2

    .line 62
    goto :goto_a

    :cond_b
    move v0, v2

    .line 63
    goto :goto_b

    :cond_c
    move v0, v2

    .line 64
    goto :goto_c

    :cond_d
    move v0, v2

    .line 65
    goto :goto_d

    :cond_e
    move v0, v2

    .line 66
    goto :goto_e

    :cond_f
    move v1, v2

    .line 67
    goto :goto_f
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public getLeftTemp()D
    .locals 2

    .prologue
    .line 210
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    return-wide v0
.end method

.method public getLevel()D
    .locals 2

    .prologue
    .line 202
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    return-wide v0
.end method

.method public getRightTemp()D
    .locals 2

    .prologue
    .line 218
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    return-wide v0
.end method

.method public isiACOpen()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    return v0
.end method

.method public isiAirOpen()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    return v0
.end method

.method public isiAuto1()Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    return v0
.end method

.method public isiAuto2()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    return v0
.end method

.method public isiBackWind()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    return v0
.end method

.method public isiDownWind()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    return v0
.end method

.method public isiDual()Z
    .locals 1

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    return v0
.end method

.method public isiFlatWind()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    return v0
.end method

.method public isiFront()Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    return v0
.end method

.method public isiFrontWind()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    return v0
.end method

.method public isiHadChange()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    return v0
.end method

.method public isiInnerLoop()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    return v0
.end method

.method public isiMaxFrontWind()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    return v0
.end method

.method public isiRear()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    return v0
.end method

.method public isiSync()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    return v0
.end method

.method public isiWindDirAuto()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    return v0
.end method

.method public setLeftTemp(D)V
    .locals 1
    .param p1, "leftTemp"    # D

    .prologue
    .line 214
    iput-wide p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    .line 215
    return-void
.end method

.method public setLevel(D)V
    .locals 1
    .param p1, "level"    # D

    .prologue
    .line 206
    iput-wide p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    .line 207
    return-void
.end method

.method public setRightTemp(D)V
    .locals 1
    .param p1, "rightTemp"    # D

    .prologue
    .line 222
    iput-wide p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    .line 223
    return-void
.end method

.method public setiACOpen(Z)V
    .locals 0
    .param p1, "iACOpen"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    .line 127
    return-void
.end method

.method public setiAirOpen(Z)V
    .locals 0
    .param p1, "iAirOpen"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    .line 119
    return-void
.end method

.method public setiAuto1(Z)V
    .locals 0
    .param p1, "iAuto1"    # Z

    .prologue
    .line 230
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    .line 231
    return-void
.end method

.method public setiAuto2(Z)V
    .locals 0
    .param p1, "iAuto2"    # Z

    .prologue
    .line 238
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    .line 239
    return-void
.end method

.method public setiBackWind(Z)V
    .locals 0
    .param p1, "iBackWind"    # Z

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    .line 175
    return-void
.end method

.method public setiDownWind(Z)V
    .locals 0
    .param p1, "iDownWind"    # Z

    .prologue
    .line 190
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    .line 191
    return-void
.end method

.method public setiDual(Z)V
    .locals 0
    .param p1, "iDual"    # Z

    .prologue
    .line 246
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    .line 247
    return-void
.end method

.method public setiFlatWind(Z)V
    .locals 0
    .param p1, "iFlatWind"    # Z

    .prologue
    .line 182
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    .line 183
    return-void
.end method

.method public setiFront(Z)V
    .locals 0
    .param p1, "iFront"    # Z

    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    .line 255
    return-void
.end method

.method public setiFrontWind(Z)V
    .locals 0
    .param p1, "iFrontWind"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    .line 151
    return-void
.end method

.method public setiHadChange(Z)V
    .locals 0
    .param p1, "iHadChange"    # Z

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    .line 199
    return-void
.end method

.method public setiInnerLoop(Z)V
    .locals 0
    .param p1, "iInnerLoop"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    .line 135
    return-void
.end method

.method public setiMaxFrontWind(Z)V
    .locals 0
    .param p1, "iMaxFrontWind"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    .line 167
    return-void
.end method

.method public setiRear(Z)V
    .locals 0
    .param p1, "iRear"    # Z

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    .line 263
    return-void
.end method

.method public setiSync(Z)V
    .locals 0
    .param p1, "iSync"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    .line 139
    return-void
.end method

.method public setiWindDirAuto(Z)V
    .locals 0
    .param p1, "iWindDirAuto"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    .line 159
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "AirInfo [iAirOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 270
    const-string v1, ", iACOpen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 272
    const-string v1, ", iInnerLoop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 274
    const-string v1, ", iSync="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 276
    const-string v1, ", iAuto1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 278
    const-string v1, ", iAuto2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 280
    const-string v1, ", iDual="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 282
    const-string v1, ", iFront="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 284
    const-string v1, ", iRear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 286
    const-string v1, ", iWindDirAuto="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 288
    const-string v1, ", iFrontWind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 290
    const-string v1, ", iMaxFrontWind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 292
    const-string v1, ", iBackWind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 294
    const-string v1, ", iFlatWind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 296
    const-string v1, ", iDownWind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 298
    const-string v1, ", iHadChange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    iget-boolean v1, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 300
    const-string v1, ", level="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    iget-wide v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 302
    const-string v1, ", leftTemp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    iget-wide v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 304
    const-string v1, ", rightTemp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    iget-wide v2, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 306
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAirOpen:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 76
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iACOpen:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 77
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iInnerLoop:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 78
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iSync:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 79
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto1:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 80
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iAuto2:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 81
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDual:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 82
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFront:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 83
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iRear:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 84
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iWindDirAuto:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 85
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFrontWind:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 86
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iMaxFrontWind:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 87
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iBackWind:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 88
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iFlatWind:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 89
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iDownWind:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 90
    iget-boolean v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->iHadChange:Z

    if-eqz v0, :cond_f

    :goto_f
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 91
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->level:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 92
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->leftTemp:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 93
    iget-wide v0, p0, Lcom/backaudio/android/driver/beans/AirInfo;->rightTemp:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 94
    return-void

    :cond_0
    move v0, v2

    .line 75
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 76
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 77
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 78
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 79
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 80
    goto :goto_5

    :cond_6
    move v0, v2

    .line 81
    goto :goto_6

    :cond_7
    move v0, v2

    .line 82
    goto :goto_7

    :cond_8
    move v0, v2

    .line 83
    goto :goto_8

    :cond_9
    move v0, v2

    .line 84
    goto :goto_9

    :cond_a
    move v0, v2

    .line 85
    goto :goto_a

    :cond_b
    move v0, v2

    .line 86
    goto :goto_b

    :cond_c
    move v0, v2

    .line 87
    goto :goto_c

    :cond_d
    move v0, v2

    .line 88
    goto :goto_d

    :cond_e
    move v0, v2

    .line 89
    goto :goto_e

    :cond_f
    move v1, v2

    .line 90
    goto :goto_f
.end method
