.class public final enum Lcom/backaudio/android/driver/Mainboard$EUpgrade;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EUpgrade"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EUpgrade;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EUpgrade;

.field public static final enum ERROR:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

.field public static final enum FINISH:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

.field public static final enum GET_HERDER:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

.field public static final enum UPGRADING:Lcom/backaudio/android/driver/Mainboard$EUpgrade;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1541
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v2, v2}, Lcom/backaudio/android/driver/Mainboard$EUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    const-string v1, "GET_HERDER"

    invoke-direct {v0, v1, v3, v3}, Lcom/backaudio/android/driver/Mainboard$EUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->GET_HERDER:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    const-string v1, "UPGRADING"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$EUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->UPGRADING:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    const-string v1, "FINISH"

    invoke-direct {v0, v1, v5, v5}, Lcom/backaudio/android/driver/Mainboard$EUpgrade;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->FINISH:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    .line 1540
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->ERROR:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->GET_HERDER:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->UPGRADING:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->FINISH:Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    aput-object v1, v0, v5

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 1545
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1546
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->code:I

    .line 1547
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EUpgrade;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EUpgrade;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EUpgrade;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EUpgrade;->code:I

    int-to-byte v0, v0

    return v0
.end method
