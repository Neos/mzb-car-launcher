.class public final enum Lcom/backaudio/android/driver/Mainboard$ECarLayer;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ECarLayer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$ECarLayer;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum BT_CONNECT:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum DV:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum QUERY:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum RECORDER:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum REVERSE_360:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

.field public static final enum SCREEN_OFF:Lcom/backaudio/android/driver/Mainboard$ECarLayer;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1583
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "ANDROID"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v4, v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "RECORDER"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v5, v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->RECORDER:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "DV"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v6, v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->DV:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "REVERSE_360"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v7, v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->REVERSE_360:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "ORIGINAL"

    .line 1584
    const/16 v2, 0x40

    invoke-direct {v0, v1, v8, v2}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "BT_CONNECT"

    const/4 v2, 0x5

    const/16 v3, 0x41

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->BT_CONNECT:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "ORIGINAL_REVERSE"

    const/4 v2, 0x6

    const/16 v3, 0x42

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "SCREEN_OFF"

    const/4 v2, 0x7

    .line 1585
    const/16 v3, 0x60

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->SCREEN_OFF:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "REVERSE"

    const/16 v2, 0x8

    const/16 v3, 0x50

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    const-string v1, "QUERY"

    const/16 v2, 0x9

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$ECarLayer;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->QUERY:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    .line 1582
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ANDROID:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->RECORDER:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->DV:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->REVERSE_360:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v1, v0, v7

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->BT_CONNECT:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ORIGINAL_REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->SCREEN_OFF:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->REVERSE:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->QUERY:Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 1589
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1590
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->code:I

    .line 1591
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$ECarLayer;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$ECarLayer;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$ECarLayer;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1594
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$ECarLayer;->code:I

    int-to-byte v0, v0

    return v0
.end method
