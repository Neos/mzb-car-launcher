.class public Lcom/backaudio/android/driver/bluetooth/Bluetooth;
.super Ljava/lang/Object;
.source "Bluetooth.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton:[I = null

.field private static final GPIO_BT:Ljava/lang/String; = "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_bt"

.field private static instance:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

.field private static isRenGao:Z

.field private static logger:Lorg/slf4j/Logger;

.field private static sdf:Ljava/text/SimpleDateFormat;


# instance fields
.field private bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

.field private eventHandlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton()[I
    .locals 3

    .prologue
    .line 20
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->values()[Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ASTERISK:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->EIGHT:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FIVE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_3
    :try_start_3
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FOUR:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_4
    :try_start_4
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->NINE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_5
    :try_start_5
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ONE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_6
    :try_start_6
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SEVEN:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_7
    :try_start_7
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SIX:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_8
    :try_start_8
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->THREE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_9
    :try_start_9
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->TWO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_a
    :try_start_a
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->WELL:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_b
    :try_start_b
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ZERO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_c
    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_c

    :catch_1
    move-exception v1

    goto :goto_b

    :catch_2
    move-exception v1

    goto :goto_a

    :catch_3
    move-exception v1

    goto :goto_9

    :catch_4
    move-exception v1

    goto :goto_8

    :catch_5
    move-exception v1

    goto :goto_7

    :catch_6
    move-exception v1

    goto :goto_6

    :catch_7
    move-exception v1

    goto :goto_5

    :catch_8
    move-exception v1

    goto :goto_4

    :catch_9
    move-exception v1

    goto :goto_3

    :catch_a
    move-exception v1

    goto :goto_2

    :catch_b
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    const-class v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->logger:Lorg/slf4j/Logger;

    .line 25
    const/4 v0, 0x0

    sput-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->instance:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    .line 34
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd hh:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->sdf:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->eventHandlers:Ljava/util/List;

    .line 29
    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->eventHandlers:Ljava/util/List;

    .line 39
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;

    invoke-direct {v0}, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;

    invoke-direct {v0}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    goto :goto_0
.end method

.method public static getInstance()Lcom/backaudio/android/driver/bluetooth/Bluetooth;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    const-class v1, Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    monitor-enter v1

    .line 48
    :try_start_0
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->instance:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    invoke-direct {v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->instance:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->instance:Lcom/backaudio/android/driver/bluetooth/Bluetooth;

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private ioWrite(Ljava/lang/String;)V
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 83
    .local v0, "buffer":[B
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 84
    .local v1, "str":Ljava/lang/String;
    sget-object v2, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "send cmd:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->writeBluetooth([B)V

    .line 88
    return-void
.end method

.method private sendVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "button"    # Lcom/backaudio/android/driver/bluetooth/EVirtualButton;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "suffix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 651
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 689
    :goto_0
    return-void

    .line 653
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 656
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 659
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 662
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 665
    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 668
    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "5"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 671
    :pswitch_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "6"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 674
    :pswitch_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "7"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 677
    :pswitch_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "8"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 680
    :pswitch_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "9"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 683
    :pswitch_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 686
    :pswitch_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 651
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public SetBoot()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 591
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 592
    const-string v0, "AT+BOOT\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 597
    :goto_0
    return-void

    .line 594
    :cond_0
    const-string v0, "AT#CC\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V
    .locals 1
    .param p1, "handler"    # Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .prologue
    .line 71
    if-nez p1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->eventHandlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->eventHandlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->bluetoothProtocolAnalyzer:Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;

    invoke-interface {v0, p1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;->setEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V

    goto :goto_0
.end method

.method public answerThePhone()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 274
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "AT+HFANSW\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 280
    :goto_0
    return-void

    .line 277
    :cond_0
    const-string v0, "AT#CE\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public call(Ljava/lang/String;)V
    .locals 2
    .param p1, "phone"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 289
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT+HFDIAL="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 295
    :goto_0
    return-void

    .line 292
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT#CW"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cancelDownloadPhoneBook()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 444
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 445
    const-string v0, "AT+PBABORT\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 450
    :goto_0
    return-void

    .line 447
    :cond_0
    const-string v0, "AT#PS\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public clearPairingList()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 137
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 138
    const-string v0, "AT+DLPD=000000000000"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    const-string v0, "AT#CV\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public connectDeviceSync(Ljava/lang/String;)V
    .locals 2
    .param p1, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 459
    const-string v0, "\r\n"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 460
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 461
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT+CPDL="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 466
    :goto_0
    return-void

    .line 463
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT#CC"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public decVolume()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 488
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 489
    const-string v0, "AT+VDN\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 494
    :goto_0
    return-void

    .line 491
    :cond_0
    const-string v0, "AT#CL\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disconnectCurrentDevice()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 621
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 622
    const-string v0, "AT+DSCA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 626
    :goto_0
    return-void

    .line 624
    :cond_0
    const-string v0, "AT#CD\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public downloadPhoneBookSync(I)V
    .locals 2
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 397
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 398
    packed-switch p1, :pswitch_data_0

    .line 408
    const-string v0, "AT+PBDOWN=1\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 436
    :goto_0
    return-void

    .line 404
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT+PBDOWN="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 412
    :cond_0
    packed-switch p1, :pswitch_data_1

    .line 430
    const-string v0, "AT#PA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 414
    :pswitch_1
    const-string v0, "AT#PA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 417
    :pswitch_2
    const-string v0, "AT#PH\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 420
    :pswitch_3
    const-string v0, "AT#PI\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 423
    :pswitch_4
    const-string v0, "AT#PJ\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 426
    :pswitch_5
    const-string v0, "AT#PX\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 412
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public enterPairingModeSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 232
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "AT+EPRM=1\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_0
    const-string v0, "AT#CA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hangUpThePhone()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 260
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 261
    const-string v0, "AT+HFCHUP\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 266
    :goto_0
    return-void

    .line 263
    :cond_0
    const-string v0, "AT#CF\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public incVolume()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 474
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 475
    const-string v0, "AT+VUP\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 480
    :goto_0
    return-void

    .line 477
    :cond_0
    const-string v0, "AT#CK\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public leavePairingModeSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 245
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 246
    const-string v0, "AT+EPRM=0\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    const-string v0, "AT#CB\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pausePlaySync(Z)V
    .locals 1
    .param p1, "iPlay"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 324
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 325
    const-string v0, "AT+PP\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 334
    :goto_0
    return-void

    .line 327
    :cond_0
    if-eqz p1, :cond_1

    .line 328
    const-string v0, "AT#MS\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_1
    const-string v0, "AT#MB\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pauseSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 341
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-nez v0, :cond_0

    .line 343
    const-string v0, "AT#MB\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 346
    :cond_0
    return-void
.end method

.method public playNext()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 369
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 370
    const-string v0, "AT+FWD\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 375
    :goto_0
    return-void

    .line 372
    :cond_0
    const-string v0, "AT#MD\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public playPrev()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 383
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 384
    const-string v0, "AT+BACK\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 389
    :goto_0
    return-void

    .line 386
    :cond_0
    const-string v0, "AT#ME\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pressVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)V
    .locals 2
    .param p1, "button"    # Lcom/backaudio/android/driver/bluetooth/EVirtualButton;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 698
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 699
    const-string v0, "AT+HFDTMF="

    const-string v1, "\r\n"

    invoke-direct {p0, p1, v0, v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->sendVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    :goto_0
    return-void

    .line 701
    :cond_0
    const-string v0, "AT#CX"

    const-string v1, "\r\n"

    invoke-direct {p0, p1, v0, v1}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->sendVirutalButton(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public push([B)V
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    .line 56
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->logger:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rec cmd:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    array-length v3, p1

    invoke-static {p1, v2, v3}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->logger:Lorg/slf4j/Logger;

    const-string v1, "push--------------"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public queryPhoneStatus()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 634
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 635
    const-string v0, "AT+HFPSTAT\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 639
    :goto_0
    return-void

    .line 637
    :cond_0
    const-string v0, "AT#CY\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readDeviceAddr()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 166
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 167
    const-string v0, "AT+GLBA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 172
    :goto_0
    return-void

    .line 169
    :cond_0
    const-string v0, "AT#DF\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readDeviceNameSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 188
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 189
    const-string v0, "AT+GLDN\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 194
    :goto_0
    return-void

    .line 191
    :cond_0
    const-string v0, "AT#MM\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readDevicePIN()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 175
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-nez v0, :cond_0

    .line 177
    const-string v0, "AT#MN\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 180
    :cond_0
    return-void
.end method

.method public readMediaInfo()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 308
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-nez v0, :cond_0

    .line 311
    const-string v0, "AT#MK\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 314
    :cond_0
    return-void
.end method

.method public readMediaStatus()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 299
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "AT+A2DPSTAT\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 305
    :goto_0
    return-void

    .line 302
    :cond_0
    const-string v0, "AT#MV\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readPairingListSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 202
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 203
    const-string v0, "AT+LSPD\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 208
    :goto_0
    return-void

    .line 205
    :cond_0
    const-string v0, "AT#MX\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readPhoneStatusSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 515
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 516
    const-string v0, "AT+HFPSTAT\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 521
    :goto_0
    return-void

    .line 518
    :cond_0
    const-string v0, "AT#CY\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public readVersionSync()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 150
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 151
    const-string v0, "AT+GVER\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    const-string v0, "AT#MY\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeDevice(Ljava/lang/String;)V
    .locals 2
    .param p1, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 530
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "\r\n"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT+DLPD="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 537
    :goto_0
    return-void

    .line 534
    :cond_0
    const-string v0, "AT#CV\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBTEnterACC(Z)V
    .locals 1
    .param p1, "isAccOff"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 604
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-nez v0, :cond_0

    .line 606
    if-eqz p1, :cond_1

    .line 607
    const-string v0, "AT#CZ0\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 609
    :cond_1
    const-string v0, "AT#CZ1\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBTVolume(I)V
    .locals 2
    .param p1, "volume"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 158
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT#VF"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 163
    :cond_0
    return-void
.end method

.method public setBluetoothMusicMute(Z)V
    .locals 1
    .param p1, "isMute"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 216
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-nez v0, :cond_0

    .line 218
    if-eqz p1, :cond_1

    .line 219
    const-string v0, "AT#VA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    const-string v0, "AT#VB\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 642
    const-string v0, "\r\n"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 643
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT+SLDN="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 648
    :goto_0
    return-void

    .line 646
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AT#MM"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopPlay()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 355
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 356
    const-string v0, "AT+STOP\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    const-string v0, "AT#MC\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public switchBtDevice()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 559
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 560
    const-string v0, "AT+HFADTS\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 565
    :goto_0
    return-void

    .line 562
    :cond_0
    const-string v0, "AT#CP\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public switchDevice(Z)V
    .locals 1
    .param p1, "iPhone"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 573
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 574
    const-string v0, "AT+HFADTS\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 583
    :goto_0
    return-void

    .line 576
    :cond_0
    if-eqz p1, :cond_1

    .line 577
    const-string v0, "AT#CO\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0

    .line 579
    :cond_1
    const-string v0, "AT#CP\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public switchPhoneDevice()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 545
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 546
    const-string v0, "AT+HFADTS\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 551
    :goto_0
    return-void

    .line 548
    :cond_0
    const-string v0, "AT#CO\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public tryToDownloadPhoneBook()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 502
    sget-boolean v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->isRenGao:Z

    if-eqz v0, :cond_0

    .line 503
    const-string v0, "AT+PBCONN\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    .line 507
    :goto_0
    return-void

    .line 505
    :cond_0
    const-string v0, "AT#PA\r\n"

    invoke-direct {p0, v0}, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->ioWrite(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected wrap([B)[B
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 106
    array-length v3, p1

    add-int/lit8 v3, v3, 0x4

    new-array v2, v3, [B

    .line 107
    .local v2, "mcuProtocol":[B
    array-length v3, p1

    add-int/lit8 v3, v3, 0x2

    int-to-byte v3, v3

    aput-byte v3, v2, v5

    .line 108
    aput-byte v4, v2, v4

    .line 109
    const/16 v3, 0xf

    aput-byte v3, v2, v6

    .line 110
    aget-byte v0, v2, v5

    .line 111
    .local v0, "check":B
    aget-byte v3, v2, v4

    aget-byte v4, v2, v6

    add-int/2addr v3, v4

    int-to-byte v3, v3

    add-int/2addr v3, v0

    int-to-byte v0, v3

    .line 112
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-lt v1, v3, :cond_0

    .line 116
    array-length v3, p1

    add-int/lit8 v3, v3, 0x3

    aput-byte v0, v2, v3

    .line 117
    return-object v2

    .line 113
    :cond_0
    add-int/lit8 v3, v1, 0x3

    aget-byte v4, p1, v1

    aput-byte v4, v2, v3

    .line 114
    aget-byte v3, p1, v1

    add-int/2addr v3, v0

    int-to-byte v0, v3

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public writeBluetooth([B)V
    .locals 3
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->writeBlueTooth([B)V

    .line 130
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bluetooth;->logger:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bluetoothprotocal write::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 131
    return-void
.end method
