.class public Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;
.super Ljava/lang/Object;
.source "Bc8mpBluetoothProtocolAnalyzer.java"

# interfaces
.implements Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;


# static fields
.field private static logger:Lorg/slf4j/Logger;

.field private static playStatusTag:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

.field private isLineEndDetected:Z

.field private isLineStartDeteceted:Z

.field private mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

.field private mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

.field private mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

.field private nextExceptedValue:B

.field private phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

.field private phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

.field private protocolBuffer:Ljava/io/ByteArrayOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 45
    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    .line 65
    const/4 v0, 0x0

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .line 54
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    .line 55
    iput-boolean v2, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 56
    iput-boolean v2, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    .line 57
    iput-byte v2, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    .line 59
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    .line 61
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    .line 62
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    .line 63
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 64
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    .line 70
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    const-string v1, "PP"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    const-string v1, "STOP"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    const-string v1, "FWD"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    const-string v1, "BACK"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method private newLineProtocol([B)Z
    .locals 28
    .param p1, "byteArray"    # [B

    .prologue
    .line 218
    new-instance v15, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/lang/String;-><init>([B)V

    .line 219
    .local v15, "line":Ljava/lang/String;
    if-eqz v15, :cond_0

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v25

    if-nez v25, :cond_1

    .line 220
    :cond_0
    const/16 v25, 0x0

    .line 449
    :goto_0
    return v25

    .line 222
    :cond_1
    const-string v25, "HFPSTAT"

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_2

    .line 224
    const/16 v25, 0x0

    :try_start_0
    move/from16 v0, v25

    invoke-static {v15, v0}, Lcom/backaudio/android/driver/Utils;->saveLogLine(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :cond_2
    :goto_1
    const/4 v14, 0x0

    .line 232
    .local v14, "key":Ljava/lang/String;
    const/16 v23, 0x0

    .line 233
    .local v23, "value":Ljava/lang/String;
    const/16 v25, 0x3d

    :try_start_1
    move/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_4

    .line 234
    const-string v25, "+"

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    const-string v26, "="

    move-object/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 235
    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    .line 236
    const/16 v26, 0xd

    move/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v26

    .line 235
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 237
    sget-object v25, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "bluetoothprotocal line recv::"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    .line 238
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 237
    invoke-interface/range {v25 .. v26}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 244
    :goto_2
    const-string v25, "PBDN"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 245
    const-string v25, "PBDNDATA"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    if-nez v25, :cond_3

    .line 247
    new-instance v25, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 248
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 250
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    if-nez v25, :cond_5

    .line 251
    new-instance v25, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 262
    :goto_3
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 225
    .end local v14    # "key":Ljava/lang/String;
    .end local v23    # "value":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 226
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 240
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v14    # "key":Ljava/lang/String;
    .restart local v23    # "value":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v25, "+"

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    add-int/lit8 v25, v25, 0x1

    .line 241
    const/16 v26, 0xd

    move/from16 v0, v26

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v26

    .line 240
    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    goto :goto_2

    .line 253
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v25

    if-nez v25, :cond_7

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->addUnit(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;)V

    .line 255
    new-instance v25, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 256
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 444
    :catch_1
    move-exception v11

    .line 445
    .local v11, "ex":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 449
    .end local v11    # "ex":Ljava/lang/Exception;
    :cond_6
    const/16 v25, 0x0

    goto/16 :goto_0

    .line 258
    :cond_7
    :try_start_3
    sget-object v25, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "phonebook error,needData:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 258
    invoke-interface/range {v25 .. v26}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_3

    .line 264
    :cond_8
    const-string v25, "PBDNEND"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v25

    if-eqz v25, :cond_a

    .line 267
    sget-object v25, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "phonebook error,needData:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 267
    invoke-interface/range {v25 .. v26}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 273
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneBookList(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;)V

    .line 275
    :cond_9
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 276
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 277
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 270
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->addUnit(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->reset()V

    goto :goto_4

    .line 282
    :cond_b
    const-string v25, "PREND"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingModeEnd()V

    .line 284
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 286
    :cond_c
    const-string v25, "HFPOCID"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 287
    new-instance v4, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;

    move-object/from16 v0, v23

    invoke-direct {v4, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    .local v4, "callingOutProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneCallingOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;)V

    .line 290
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 292
    .end local v4    # "callingOutProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;
    :cond_d
    const-string v25, "HFPAUDO"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 293
    new-instance v8, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;

    move-object/from16 v0, v23

    invoke-direct {v8, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    .local v8, "deviceSwitchedProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v8}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->ondeviceSwitchedProtocol(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V

    .line 296
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 298
    .end local v8    # "deviceSwitchedProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    :cond_e
    const-string v25, "GVER"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 299
    new-instance v24, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    .local v24, "versionProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onVersion(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;)V

    .line 302
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 305
    .end local v24    # "versionProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;
    :cond_f
    const-string v25, "GLDN"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 306
    new-instance v6, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    move-object/from16 v0, v23

    invoke-direct {v6, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    .local v6, "deviceNameProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onDeviceName(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V

    .line 309
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 312
    .end local v6    # "deviceNameProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
    :cond_10
    const-string v25, "EPRM"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 313
    new-instance v10, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;

    move-object/from16 v0, v23

    invoke-direct {v10, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    .local v10, "enterPairingModeResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v10}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V

    .line 316
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 319
    .end local v10    # "enterPairingModeResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    :cond_11
    sget-object v25, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->playStatusTag:Ljava/util/List;

    move-object/from16 v0, v25

    invoke-interface {v0, v14}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 320
    new-instance v21, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    .local v21, "playStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onSetPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;)V

    .line 323
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 326
    .end local v21    # "playStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;
    :cond_12
    const-string v25, "HFPCLID"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 327
    new-instance v13, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;

    move-object/from16 v0, v23

    invoke-direct {v13, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    .local v13, "incomingCallProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v13}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onIncomingCall(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;)V

    .line 330
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 333
    .end local v13    # "incomingCallProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;
    :cond_13
    const-string v25, "HFPSTAT"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 334
    new-instance v20, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    .local v20, "phoneStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V

    .line 337
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 340
    .end local v20    # "phoneStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    :cond_14
    const-string v25, "HFANSW"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_15

    .line 341
    new-instance v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;

    move-object/from16 v0, v23

    invoke-direct {v2, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    .local v2, "answerPhoneResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onAnswerPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;)V

    .line 344
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 347
    .end local v2    # "answerPhoneResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;
    :cond_15
    const-string v25, "HFCHUP"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_16

    .line 348
    new-instance v12, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;

    move-object/from16 v0, v23

    invoke-direct {v12, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    .local v12, "hangUpPhoneResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v12}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onHangUpPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;)V

    .line 351
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 354
    .end local v12    # "hangUpPhoneResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;
    :cond_16
    const-string v25, "HFDIAL"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_17

    .line 355
    new-instance v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;

    move-object/from16 v0, v23

    invoke-direct {v3, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    .local v3, "callOutResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onCallOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;)V

    .line 357
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 360
    .end local v3    # "callOutResult":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;
    :cond_17
    const-string v25, "AVRCPSTAT"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_18

    .line 361
    new-instance v17, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    .local v17, "mediaStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;)V

    .line 364
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 367
    .end local v17    # "mediaStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;
    :cond_18
    const-string v25, "A2DPSTAT"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_19

    const-string v25, "PLAYSTAT"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1a

    .line 368
    :cond_19
    new-instance v16, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    .local v16, "mediaPlayStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V

    .line 371
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 374
    .end local v16    # "mediaPlayStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    :cond_1a
    const-string v25, "PBSTAT"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1b

    .line 375
    new-instance v19, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    .local v19, "phoneBookCtrlStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneBookCtrlStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;)V

    .line 378
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 381
    .end local v19    # "phoneBookCtrlStatusProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;
    :cond_1b
    const-string v25, "CCDA"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1c

    .line 382
    new-instance v5, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;

    move-object/from16 v0, v23

    invoke-direct {v5, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    .local v5, "connectedDeviceProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onConnectedDevice(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;)V

    .line 385
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 388
    .end local v5    # "connectedDeviceProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
    :cond_1c
    const-string v25, "DLPD"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1d

    .line 389
    new-instance v7, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;

    move-object/from16 v0, v23

    invoke-direct {v7, v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    .local v7, "deviceRemovedProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-interface {v0, v7}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onDeviceRemoved(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;)V

    .line 392
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 397
    .end local v7    # "deviceRemovedProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;
    :cond_1d
    const-string v25, "LSPD"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_22

    .line 399
    const-string v25, "LSPDSTART"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1e

    .line 400
    new-instance v25, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    invoke-direct/range {v25 .. v25}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    .line 401
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 403
    :cond_1e
    const-string v25, "LSPD"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_20

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1f

    .line 405
    new-instance v22, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    .local v22, "unit":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->addUnit(Ljava/lang/Object;)V

    .line 409
    .end local v22    # "unit":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;
    :cond_1f
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 411
    :cond_20
    const-string v25, "LSPDEND"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_21

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    move-object/from16 v25, v0

    if-eqz v25, :cond_21

    .line 414
    new-instance v18, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    move-object/from16 v25, v0

    .line 414
    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;-><init>(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;)V

    .line 416
    .local v18, "pairingListProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingList(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;)V

    .line 417
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    .line 420
    .end local v18    # "pairingListProtocol":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;
    :cond_21
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 423
    :cond_22
    const-string v25, "MEDIAINFO"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 424
    const-string v25, "MEDIAINFOSTART"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_23

    .line 425
    new-instance v25, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    invoke-direct/range {v25 .. v25}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    .line 426
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 428
    :cond_23
    const-string v25, "MEDIAINFOEND"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_25

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    move-object/from16 v25, v0

    if-eqz v25, :cond_24

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaInfo(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;)V

    .line 431
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    .line 432
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    .line 434
    :cond_24
    const/16 v25, 0x1

    goto/16 :goto_0

    .line 436
    :cond_25
    const-string v25, "MEDIAINFO"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 437
    new-instance v25, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    .line 438
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-direct {v0, v14, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    .line 439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->addUnit(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 440
    const/16 v25, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public push([B)V
    .locals 14
    .param p1, "buffer"    # [B

    .prologue
    const/4 v13, 0x2

    const/16 v12, 0xd

    const/16 v11, 0xa

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 96
    const/4 v1, 0x0

    .line 98
    .local v1, "i":I
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    invoke-virtual {v6}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v6

    if-lez v6, :cond_3

    .line 99
    array-length v6, p1

    iget-object v7, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    invoke-virtual {v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v7

    if-gt v6, v7, :cond_1

    .line 100
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    array-length v7, p1

    invoke-virtual {v6, p1, v9, v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->push([BII)V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    invoke-virtual {v6}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v0

    .line 103
    .local v0, "endIndex":I
    array-length v6, p1

    iget-object v7, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    invoke-virtual {v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v7

    sub-int v2, v6, v7

    .line 104
    .local v2, "remain":I
    new-array v5, v2, [B

    .line 105
    .local v5, "tmp":[B
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    .line 106
    iget-object v7, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;

    invoke-virtual {v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->getNeedDataLength()I

    move-result v7

    invoke-virtual {v6, p1, v9, v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;->push([BII)V

    .line 107
    const/4 v4, 0x0

    .local v4, "t":I
    :goto_1
    if-lt v4, v2, :cond_2

    .line 110
    invoke-virtual {p0, v5}, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->push([B)V

    goto :goto_0

    .line 108
    :cond_2
    add-int v6, v4, v0

    aget-byte v6, p1, v6

    aput-byte v6, v5, v4

    .line 107
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 116
    .end local v0    # "endIndex":I
    .end local v2    # "remain":I
    .end local v4    # "t":I
    .end local v5    # "tmp":[B
    :cond_3
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    if-eqz v6, :cond_4

    .line 117
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    invoke-virtual {v6}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;->getNeedDataLength()I

    move-result v6

    if-lez v6, :cond_4

    .line 118
    :goto_2
    array-length v6, p1

    if-ge v1, v6, :cond_4

    .line 119
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    invoke-virtual {v6}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;->getNeedDataLength()I

    move-result v6

    .line 118
    if-gtz v6, :cond_6

    .line 125
    :cond_4
    :goto_3
    array-length v6, p1

    if-ge v1, v6, :cond_0

    .line 126
    iget-boolean v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    if-nez v6, :cond_9

    .line 127
    aget-byte v6, p1, v1

    if-ne v6, v12, :cond_7

    .line 128
    iput-boolean v10, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 129
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, p1, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 130
    iput-byte v11, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    .line 125
    :cond_5
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 120
    :cond_6
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    invoke-virtual {v6, p1, v1, v10}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;->push([BII)V

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 132
    :cond_7
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 133
    sget-object v7, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v6, "skip:"

    invoke-direct {v8, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 134
    aget-byte v6, p1, v1

    if-lez v6, :cond_8

    aget-byte v6, p1, v1

    :goto_5
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 133
    invoke-interface {v7, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 136
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto :goto_4

    .line 135
    :cond_8
    aget-byte v6, p1, v1

    add-int/lit16 v6, v6, 0x100

    goto :goto_5

    .line 140
    :cond_9
    iget-byte v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    if-eqz v6, :cond_c

    iget-boolean v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    if-nez v6, :cond_c

    .line 141
    iget-byte v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    aget-byte v7, p1, v1

    if-ne v6, v7, :cond_a

    .line 143
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, p1, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 144
    iput-byte v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    goto :goto_4

    .line 147
    :cond_a
    aget-byte v6, p1, v1

    if-eq v6, v12, :cond_5

    .line 148
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 149
    sget-object v7, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v6, "skip:"

    invoke-direct {v8, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 151
    aget-byte v6, p1, v1

    if-lez v6, :cond_b

    aget-byte v6, p1, v1

    :goto_6
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 150
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 149
    invoke-interface {v7, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 153
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto :goto_4

    .line 152
    :cond_b
    aget-byte v6, p1, v1

    add-int/lit16 v6, v6, 0x100

    goto :goto_6

    .line 158
    :cond_c
    iget-boolean v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    if-nez v6, :cond_e

    .line 159
    aget-byte v6, p1, v1

    if-eq v6, v12, :cond_d

    .line 160
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, p1, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_4

    .line 162
    :cond_d
    iput-boolean v10, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    .line 163
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, p1, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 164
    iput-byte v11, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    goto/16 :goto_4

    .line 167
    :cond_e
    iget-byte v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    if-ne v6, v11, :cond_11

    .line 168
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, p1, v1, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 169
    new-instance v6, Ljava/lang/String;

    iget-object v7, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    .line 170
    const-string v7, "\r\n\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 172
    sget-object v6, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    const-string v7, "0d0a0d0a error protocol,try to repare"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 173
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 174
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    new-array v7, v13, [B

    fill-array-data v7, :array_0

    invoke-virtual {v6, v7, v9, v13}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 176
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    .line 177
    iput-boolean v10, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 178
    iput-byte v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    goto/16 :goto_4

    .line 180
    :cond_f
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->newLineProtocol([B)Z

    .line 181
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 182
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    .line 183
    iput-byte v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    .line 184
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 187
    array-length v6, p1

    sub-int/2addr v6, v1

    add-int/lit8 v2, v6, -0x1

    .line 188
    .restart local v2    # "remain":I
    if-lez v2, :cond_5

    .line 189
    new-array v5, v2, [B

    .line 190
    .restart local v5    # "tmp":[B
    const/4 v3, 0x0

    .local v3, "rr":I
    :goto_7
    if-lt v3, v2, :cond_10

    .line 193
    invoke-virtual {p0, v5}, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->push([B)V

    goto/16 :goto_0

    .line 191
    :cond_10
    add-int v6, v3, v1

    add-int/lit8 v6, v6, 0x1

    aget-byte v6, p1, v6

    aput-byte v6, v5, v3

    .line 190
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 199
    .end local v2    # "remain":I
    .end local v3    # "rr":I
    .end local v5    # "tmp":[B
    :cond_11
    sget-object v6, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "protocol error, drop:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 200
    aget-byte v8, p1, v1

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 199
    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 201
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineStartDeteceted:Z

    .line 202
    iput-boolean v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->isLineEndDetected:Z

    .line 203
    iput-byte v9, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->nextExceptedValue:B

    .line 204
    iget-object v6, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_4

    .line 174
    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method public push([BII)V
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v5, 0x0

    .line 77
    invoke-static {p1, p3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    .line 78
    .local v1, "newbuffer":[B
    sget-object v2, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bluetoothprot recv::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 79
    array-length v4, v1

    invoke-static {v1, v5, v4}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 78
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 82
    const/4 v2, 0x0

    :try_start_0
    array-length v3, v1

    invoke-static {v1, v2, v3}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v2

    .line 83
    const/4 v3, 0x1

    .line 81
    invoke-static {v2, v3}, Lcom/backaudio/android/driver/Utils;->saveLogLine(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    invoke-virtual {p0, v1}, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->push([B)V

    .line 88
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 453
    return-void
.end method

.method public setEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer;->handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .line 52
    return-void
.end method
