.class public final enum Lcom/backaudio/android/driver/bluetooth/EVirtualButton;
.super Ljava/lang/Enum;
.source "EVirtualButton.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/bluetooth/EVirtualButton;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton:[I

.field public static final enum ASTERISK:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum EIGHT:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum FIVE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum FOUR:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum NINE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum ONE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum SEVEN:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum SIX:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum THREE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum TWO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum WELL:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

.field public static final enum ZERO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;


# direct methods
.method static synthetic $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton()[I
    .locals 3

    .prologue
    .line 3
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->values()[Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ASTERISK:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_1
    :try_start_1
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->EIGHT:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_2
    :try_start_2
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FIVE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_3
    :try_start_3
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FOUR:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_4
    :try_start_4
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->NINE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_5
    :try_start_5
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ONE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_6
    :try_start_6
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SEVEN:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_7
    :try_start_7
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SIX:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_8
    :try_start_8
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->THREE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_9
    :try_start_9
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->TWO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_a
    :try_start_a
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->WELL:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_b
    :try_start_b
    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ZERO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-virtual {v1}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_c
    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_c

    :catch_1
    move-exception v1

    goto :goto_b

    :catch_2
    move-exception v1

    goto :goto_a

    :catch_3
    move-exception v1

    goto :goto_9

    :catch_4
    move-exception v1

    goto :goto_8

    :catch_5
    move-exception v1

    goto :goto_7

    :catch_6
    move-exception v1

    goto :goto_6

    :catch_7
    move-exception v1

    goto :goto_5

    :catch_8
    move-exception v1

    goto :goto_4

    :catch_9
    move-exception v1

    goto :goto_3

    :catch_a
    move-exception v1

    goto :goto_2

    :catch_b
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "ONE"

    invoke-direct {v0, v1, v3}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ONE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "TWO"

    invoke-direct {v0, v1, v4}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->TWO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "THREE"

    invoke-direct {v0, v1, v5}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->THREE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "FOUR"

    invoke-direct {v0, v1, v6}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FOUR:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "FIVE"

    invoke-direct {v0, v1, v7}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FIVE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "SIX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SIX:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "SEVEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SEVEN:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "EIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->EIGHT:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "NINE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->NINE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "ZERO"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ZERO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "ASTERISK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ASTERISK:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    const-string v1, "WELL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->WELL:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    .line 3
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ONE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->TWO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->THREE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FOUR:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->FIVE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SIX:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->SEVEN:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->EIGHT:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->NINE:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ZERO:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ASTERISK:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->WELL:Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static final parse(C)Ljava/lang/String;
    .locals 1
    .param p0, "button"    # C

    .prologue
    .line 38
    const/16 v0, 0x2a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x23

    if-eq p0, v0, :cond_0

    const/16 v0, 0x30

    if-lt p0, v0, :cond_1

    const/16 v0, 0x39

    if-gt p0, v0, :cond_1

    .line 39
    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final parse(Lcom/backaudio/android/driver/bluetooth/EVirtualButton;)Ljava/lang/String;
    .locals 2
    .param p0, "button"    # Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    .prologue
    .line 7
    invoke-static {}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 33
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 9
    :pswitch_0
    const-string v0, "1"

    goto :goto_0

    .line 11
    :pswitch_1
    const-string v0, "2"

    goto :goto_0

    .line 13
    :pswitch_2
    const-string v0, "3"

    goto :goto_0

    .line 15
    :pswitch_3
    const-string v0, "4"

    goto :goto_0

    .line 17
    :pswitch_4
    const-string v0, "5"

    goto :goto_0

    .line 19
    :pswitch_5
    const-string v0, "6"

    goto :goto_0

    .line 21
    :pswitch_6
    const-string v0, "7"

    goto :goto_0

    .line 23
    :pswitch_7
    const-string v0, "8"

    goto :goto_0

    .line 25
    :pswitch_8
    const-string v0, "9"

    goto :goto_0

    .line 27
    :pswitch_9
    const-string v0, "0"

    goto :goto_0

    .line 29
    :pswitch_a
    const-string v0, "*"

    goto :goto_0

    .line 31
    :pswitch_b
    const-string v0, "#"

    goto :goto_0

    .line 7
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/bluetooth/EVirtualButton;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/bluetooth/EVirtualButton;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/EVirtualButton;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/bluetooth/EVirtualButton;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
