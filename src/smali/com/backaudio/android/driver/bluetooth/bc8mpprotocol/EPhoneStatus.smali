.class public final enum Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;
.super Ljava/lang/Enum;
.source "EPhoneStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum INITIALIZING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum MULTI_TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

.field public static final enum UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "UNCONNECT"

    invoke-direct {v0, v1, v3}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v5}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "INCOMING_CALL"

    invoke-direct {v0, v1, v6}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "CALLING_OUT"

    invoke-direct {v0, v1, v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "TALKING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "MULTI_TALKING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->MULTI_TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    const-string v1, "INITIALIZING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INITIALIZING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 9
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->MULTI_TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INITIALIZING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
