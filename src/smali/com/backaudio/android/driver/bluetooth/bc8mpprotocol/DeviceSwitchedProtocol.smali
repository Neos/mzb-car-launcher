.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "DeviceSwitchedProtocol.java"


# instance fields
.field private connectedDevice:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;->LOCAL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;->connectedDevice:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    .line 20
    :goto_0
    return-void

    .line 18
    :cond_0
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;->REMOTE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;->connectedDevice:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    goto :goto_0
.end method


# virtual methods
.method public getConnectedDevice()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;->connectedDevice:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EConnectedDevice;

    return-object v0
.end method
