.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "DeviceRemovedProtocol.java"


# instance fields
.field private address:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const/16 v1, 0x2c

    :try_start_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;->address:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    :goto_0
    return-void

    .line 17
    :catch_0
    move-exception v0

    .line 18
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;->address:Ljava/lang/String;

    return-object v0
.end method
