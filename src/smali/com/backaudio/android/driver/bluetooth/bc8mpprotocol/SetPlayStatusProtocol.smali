.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "SetPlayStatusProtocol.java"


# static fields
.field private static final PAUSE:Ljava/lang/String; = "PP"

.field private static final PLAY_NEXT:Ljava/lang/String; = "FWD"

.field private static final PLAY_PREV:Ljava/lang/String; = "BACK"

.field private static final STOP:Ljava/lang/String; = "STOP"


# instance fields
.field private isSuccess:Z

.field private playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->isSuccess:Z

    .line 16
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->isSuccess:Z

    .line 18
    const-string v0, "PP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;->PALY_OR_PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const-string v0, "STOP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 23
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;->STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    goto :goto_0

    .line 26
    :cond_2
    const-string v0, "FWD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 27
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;->PLAY_NEXT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    goto :goto_0

    .line 30
    :cond_3
    const-string v0, "BACK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;->PLAY_PREV:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    goto :goto_0
.end method


# virtual methods
.method public getPlayStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->playStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ESetPlayStatus;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;->isSuccess:Z

    return v0
.end method
