.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "AnswerPhoneResult.java"


# instance fields
.field private isSuccess:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;->isSuccess:Z

    .line 15
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;->isSuccess:Z

    .line 18
    :cond_0
    return-void
.end method


# virtual methods
.method public isSuccess()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;->isSuccess:Z

    return v0
.end method
