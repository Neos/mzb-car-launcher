.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "EnterPairingModeResult.java"


# instance fields
.field private isSuccess:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;->isSuccess:Z

    .line 10
    :try_start_0
    const-string v1, "0"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;->isSuccess:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    :cond_0
    :goto_0
    return-void

    .line 13
    :catch_0
    move-exception v0

    .line 14
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public isSuccess()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;->isSuccess:Z

    return v0
.end method
