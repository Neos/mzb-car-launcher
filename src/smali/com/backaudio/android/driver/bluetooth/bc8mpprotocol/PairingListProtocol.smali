.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;
.super Ljava/lang/Object;
.source "PairingListProtocol.java"


# instance fields
.field private units:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;)V
    .locals 2
    .param p1, "mutilineProtocol"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    :try_start_0
    invoke-virtual {p1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->getUnits()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;->units:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    :goto_0
    return-void

    .line 18
    :catch_0
    move-exception v0

    .line 19
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getUnits()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;->units:Ljava/util/List;

    return-object v0
.end method
