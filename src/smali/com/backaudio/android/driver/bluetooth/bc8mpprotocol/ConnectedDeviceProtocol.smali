.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "ConnectedDeviceProtocol.java"


# instance fields
.field private deviceAddress:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    iput-object p2, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->deviceAddress:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public getDeviceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->deviceAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->deviceName:Ljava/lang/String;

    .line 29
    return-void
.end method
