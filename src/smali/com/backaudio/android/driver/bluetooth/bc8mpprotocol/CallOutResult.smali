.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "CallOutResult.java"


# instance fields
.field private isSuccess:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;->isSuccess:Z

    .line 9
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;->isSuccess:Z

    .line 12
    :cond_0
    return-void
.end method


# virtual methods
.method public isSuccess()Z
    .locals 1

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;->isSuccess:Z

    return v0
.end method
