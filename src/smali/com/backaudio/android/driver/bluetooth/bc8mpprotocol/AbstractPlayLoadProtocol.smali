.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "AbstractPlayLoadProtocol.java"


# instance fields
.field private needDataLength:I

.field private playload:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    const/4 v1, 0x0

    iput v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->needDataLength:I

    .line 13
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->needDataLength:I

    .line 14
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    iget v2, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->needDataLength:I

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->playload:Ljava/io/ByteArrayOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    :goto_0
    return-void

    .line 15
    :catch_0
    move-exception v0

    .line 16
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getNeedDataLength()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->needDataLength:I

    return v0
.end method

.method public getPlayload()[B
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->playload:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public push([BII)V
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 27
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->playload:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 28
    iget v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->needDataLength:I

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->needDataLength:I

    .line 34
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;->playload:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 42
    return-void
.end method
