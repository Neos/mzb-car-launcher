.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "MediaPlayStatusProtocol.java"


# instance fields
.field private mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v0, "A2DPSTAT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 20
    :cond_1
    const-string v0, "3"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 21
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PLAYING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    goto :goto_0

    .line 24
    :cond_2
    const-string v0, "2"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    goto :goto_0

    .line 28
    :cond_3
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    if-nez v0, :cond_0

    .line 29
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    goto :goto_0

    .line 33
    :cond_4
    const-string v0, "PLAYSTAT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 35
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    goto :goto_0

    .line 38
    :cond_5
    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 39
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PLAYING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    goto :goto_0

    .line 42
    :cond_6
    const-string v0, "2"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    goto :goto_0
.end method


# virtual methods
.method public getMediaPlayStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    return-object v0
.end method

.method public setMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;)V
    .locals 0
    .param p1, "mediaPlayStatus"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->mediaPlayStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    .line 53
    return-void
.end method
