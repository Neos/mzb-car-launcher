.class public final enum Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;
.super Ljava/lang/Enum;
.source "EPlayStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

.field public static final enum PALY_OR_PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

.field public static final enum PLAY_NEXT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

.field public static final enum PLAY_PREV:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

.field public static final enum STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    const-string v1, "PALY_OR_PAUSE"

    invoke-direct {v0, v1, v3}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->PALY_OR_PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    const-string v1, "PLAY_NEXT"

    invoke-direct {v0, v1, v4}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->PLAY_NEXT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    const-string v1, "PLAY_PREV"

    invoke-direct {v0, v1, v5}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->PLAY_PREV:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    .line 3
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->STOP:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->PALY_OR_PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->PLAY_NEXT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->PLAY_PREV:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPlayStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
