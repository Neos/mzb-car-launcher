.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "MediaStatusProtocol.java"


# instance fields
.field private mediaStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const-string v0, "0"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;->mediaStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const-string v0, "1"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;->CONNECTIING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;->mediaStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    goto :goto_0

    .line 27
    :cond_2
    const-string v0, "2"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;->mediaStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    goto :goto_0
.end method


# virtual methods
.method public getMediaStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;->mediaStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    return-object v0
.end method

.method public setMediaStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;)V
    .locals 0
    .param p1, "mediaStatus"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;->mediaStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaStatus;

    .line 34
    return-void
.end method
