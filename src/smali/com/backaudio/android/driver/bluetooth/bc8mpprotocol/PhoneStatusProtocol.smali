.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "PhoneStatusProtocol.java"


# instance fields
.field private phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 17
    :cond_1
    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 18
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 19
    :cond_2
    const-string v0, "2"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 20
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 21
    :cond_3
    const-string v0, "3"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 22
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 23
    :cond_4
    const-string v0, "4"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 24
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 25
    :cond_5
    const-string v0, "5"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 26
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 27
    :cond_6
    const-string v0, "6"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->MULTI_TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0
.end method


# virtual methods
.method public getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    return-object v0
.end method

.method public setPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;)V
    .locals 0
    .param p1, "phoneStatus"    # Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 58
    return-void
.end method

.method public setPhoneStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    .line 54
    :goto_0
    return-void

    .line 37
    :cond_0
    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INITIALIZING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 39
    :cond_1
    const-string v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 43
    :cond_3
    const-string v0, "3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 45
    :cond_4
    const-string v0, "4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 46
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CALLING_OUT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 47
    :cond_5
    const-string v0, "5"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 48
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->INCOMING_CALL:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 49
    :cond_6
    const-string v0, "6"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 50
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0

    .line 52
    :cond_7
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->phoneStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    goto :goto_0
.end method
