.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "PhoneBookCtrlStatusProtocol.java"


# instance fields
.field private phoneBookCtrlStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->phoneBookCtrlStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 19
    :cond_1
    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->phoneBookCtrlStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    goto :goto_0

    .line 23
    :cond_2
    const-string v0, "2"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 24
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->phoneBookCtrlStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    goto :goto_0

    .line 27
    :cond_3
    const-string v0, "3"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->DOWNLOADING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->phoneBookCtrlStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    goto :goto_0
.end method


# virtual methods
.method public getPhoneBookCtrlStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;->phoneBookCtrlStatus:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    return-object v0
.end method
