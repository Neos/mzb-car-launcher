.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;
.source "DeviceNameProtocol.java"


# instance fields
.field private MACAddr:Ljava/lang/String;

.field private PIN:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private success:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractLineProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    iput-boolean v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->success:Z

    .line 15
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_0

    .line 16
    const/16 v1, 0x2c

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->deviceName:Ljava/lang/String;

    .line 17
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->success:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    :cond_0
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getMACAdress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->MACAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getPIN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->PIN:Ljava/lang/String;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->success:Z

    return v0
.end method

.method public setDeviceName(Ljava/lang/String;)V
    .locals 1
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->success:Z

    .line 40
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->deviceName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setMACAddr(Ljava/lang/String;)V
    .locals 0
    .param p1, "MACAddr"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->MACAddr:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setPIN(Ljava/lang/String;)V
    .locals 0
    .param p1, "pIN"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->PIN:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "DeviceNameProtocol [deviceName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    const-string v1, ", PIN="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    iget-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->PIN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    const-string v1, ", MACAddr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    iget-object v1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->MACAddr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
