.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
.super Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
.source "MediaInfoProtocol.java"


# instance fields
.field private album:Ljava/lang/String;

.field private artist:Ljava/lang/String;

.field private genre:Ljava/lang/String;

.field private isAnalyzed:Z

.field private number:I

.field private playTime:I

.field private title:Ljava/lang/String;

.field private totalNumber:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    .line 34
    return-void
.end method

.method private analyze()V
    .locals 13

    .prologue
    .line 37
    invoke-super {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->getUnits()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_0

    .line 79
    return-void

    .line 37
    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 39
    .local v4, "o":Ljava/lang/Object;
    :try_start_0
    move-object v0, v4

    check-cast v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;

    move-object v8, v0

    .line 40
    .local v8, "unit":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
    invoke-virtual {v8}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;->getPlayload()[B

    move-result-object v6

    .line 41
    .local v6, "playload":[B
    invoke-direct {p0, v6}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->getCharSet([B)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "charset":Ljava/lang/String;
    const/4 v10, 0x0

    aget-byte v10, v6, v10

    packed-switch v10, :pswitch_data_0

    goto :goto_0

    .line 44
    :pswitch_0
    new-instance v10, Ljava/lang/String;

    const/4 v11, 0x3

    array-length v12, v6

    add-int/lit8 v12, v12, -0x3

    .line 45
    invoke-direct {v10, v6, v11, v12, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 44
    iput-object v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->title:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    .end local v1    # "charset":Ljava/lang/String;
    .end local v6    # "playload":[B
    .end local v8    # "unit":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
    :catch_0
    move-exception v2

    .line 76
    .local v2, "ex":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 48
    .end local v2    # "ex":Ljava/lang/Exception;
    .restart local v1    # "charset":Ljava/lang/String;
    .restart local v6    # "playload":[B
    .restart local v8    # "unit":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
    :pswitch_1
    :try_start_1
    new-instance v10, Ljava/lang/String;

    const/4 v11, 0x3

    array-length v12, v6

    add-int/lit8 v12, v12, -0x3

    .line 49
    invoke-direct {v10, v6, v11, v12, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 48
    iput-object v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->artist:Ljava/lang/String;

    goto :goto_0

    .line 52
    :pswitch_2
    new-instance v10, Ljava/lang/String;

    const/4 v11, 0x3

    array-length v12, v6

    add-int/lit8 v12, v12, -0x3

    .line 53
    invoke-direct {v10, v6, v11, v12, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 52
    iput-object v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->album:Ljava/lang/String;

    goto :goto_0

    .line 56
    :pswitch_3
    new-instance v3, Ljava/lang/String;

    const/4 v10, 0x3

    .line 57
    array-length v11, v6

    add-int/lit8 v11, v11, -0x3

    .line 56
    invoke-direct {v3, v6, v10, v11, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 58
    .local v3, "numberStr":Ljava/lang/String;
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->number:I

    goto :goto_0

    .line 61
    .end local v3    # "numberStr":Ljava/lang/String;
    :pswitch_4
    new-instance v7, Ljava/lang/String;

    const/4 v10, 0x3

    .line 62
    array-length v11, v6

    add-int/lit8 v11, v11, -0x3

    .line 61
    invoke-direct {v7, v6, v10, v11, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 63
    .local v7, "totalNumberStr":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->totalNumber:I

    goto :goto_0

    .line 66
    .end local v7    # "totalNumberStr":Ljava/lang/String;
    :pswitch_5
    new-instance v10, Ljava/lang/String;

    const/4 v11, 0x3

    array-length v12, v6

    add-int/lit8 v12, v12, -0x3

    .line 67
    invoke-direct {v10, v6, v11, v12, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 66
    iput-object v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->genre:Ljava/lang/String;

    goto :goto_0

    .line 70
    :pswitch_6
    new-instance v5, Ljava/lang/String;

    const/4 v10, 0x3

    .line 71
    array-length v11, v6

    add-int/lit8 v11, v11, -0x3

    .line 70
    invoke-direct {v5, v6, v10, v11, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 72
    .local v5, "playTimeStr":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->playTime:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getCharSet([B)Ljava/lang/String;
    .locals 3
    .param p1, "playload"    # [B

    .prologue
    .line 82
    const/4 v1, 0x1

    aget-byte v1, p1, v1

    const/4 v2, 0x2

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x8

    add-int v0, v1, v2

    .line 83
    .local v0, "code":I
    sparse-switch v0, :sswitch_data_0

    .line 105
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 85
    :sswitch_0
    const-string v1, "ASCII"

    goto :goto_0

    .line 87
    :sswitch_1
    const-string v1, "ISO-8859-1"

    goto :goto_0

    .line 89
    :sswitch_2
    const-string v1, "JIS-X0201"

    goto :goto_0

    .line 91
    :sswitch_3
    const-string v1, "SHIFT-JIS"

    goto :goto_0

    .line 93
    :sswitch_4
    const-string v1, "KS-C-5601-1987"

    goto :goto_0

    .line 95
    :sswitch_5
    const-string v1, "UTF-8"

    goto :goto_0

    .line 97
    :sswitch_6
    const-string v1, "UCS2"

    goto :goto_0

    .line 99
    :sswitch_7
    const-string v1, "UTF-16BE"

    goto :goto_0

    .line 101
    :sswitch_8
    const-string v1, "GB2312"

    goto :goto_0

    .line 103
    :sswitch_9
    const-string v1, "BIG5"

    goto :goto_0

    .line 83
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0xf -> :sswitch_2
        0x11 -> :sswitch_3
        0x24 -> :sswitch_4
        0x6a -> :sswitch_5
        0x3e8 -> :sswitch_6
        0x3f5 -> :sswitch_7
        0x7e9 -> :sswitch_8
        0x7ea -> :sswitch_9
    .end sparse-switch
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->album:Ljava/lang/String;

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 145
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->genre:Ljava/lang/String;

    return-object v0
.end method

.method public getNumber()I
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 133
    :cond_0
    iget v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->number:I

    return v0
.end method

.method public getPlayTime()I
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 152
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 154
    :cond_0
    iget v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->playTime:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 110
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalNumber()I
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    if-nez v0, :cond_0

    .line 138
    invoke-direct {p0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->analyze()V

    .line 140
    :cond_0
    iget v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->totalNumber:I

    return v0
.end method

.method public setAnalyzed(Z)V
    .locals 0
    .param p1, "isAnalyzed"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->isAnalyzed:Z

    .line 159
    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 0
    .param p1, "singer"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->artist:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public setNumber(I)V
    .locals 0
    .param p1, "number"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->number:I

    .line 20
    return-void
.end method

.method public setPlayTime(I)V
    .locals 0
    .param p1, "playTime"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->playTime:I

    .line 28
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->title:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public setTotalNumber(I)V
    .locals 0
    .param p1, "totalNumber"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->totalNumber:I

    .line 24
    return-void
.end method
