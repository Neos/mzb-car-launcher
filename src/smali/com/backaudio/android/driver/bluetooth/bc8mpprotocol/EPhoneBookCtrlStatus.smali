.class public final enum Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;
.super Ljava/lang/Enum;
.source "EPhoneBookCtrlStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CONECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

.field public static final enum CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

.field public static final enum DOWNLOADING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

.field public static final enum UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    const-string v1, "UNCONNECT"

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    const-string v1, "CONECTING"

    invoke-direct {v0, v1, v3}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v5}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->DOWNLOADING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->DOWNLOADING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;->ENUM$VALUES:[Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneBookCtrlStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
