.class public Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
.super Ljava/lang/Object;
.source "BaseMultilineProtocol.java"


# instance fields
.field private units:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->units:Ljava/util/List;

    .line 11
    return-void
.end method


# virtual methods
.method public addUnit(Ljava/lang/Object;)V
    .locals 1
    .param p1, "unit"    # Ljava/lang/Object;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->units:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->units:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    :cond_0
    return-void
.end method

.method public getUnits()Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;->units:Ljava/util/List;

    return-object v0
.end method
