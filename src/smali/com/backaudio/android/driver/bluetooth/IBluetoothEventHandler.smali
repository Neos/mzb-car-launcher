.class public interface abstract Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
.super Ljava/lang/Object;
.source "IBluetoothEventHandler.java"


# virtual methods
.method public abstract onAnswerPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;)V
.end method

.method public abstract onCallOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;)V
.end method

.method public abstract onConnectedDevice(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;)V
.end method

.method public abstract onDeviceName(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V
.end method

.method public abstract onDeviceRemoved(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;)V
.end method

.method public abstract onFinishDownloadPhoneBook()V
.end method

.method public abstract onHangUpPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;)V
.end method

.method public abstract onIncomingCall(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;)V
.end method

.method public abstract onMediaInfo(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;)V
.end method

.method public abstract onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
.end method

.method public abstract onMediaStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;)V
.end method

.method public abstract onPairedDevice(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPairingList(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;)V
.end method

.method public abstract onPairingModeEnd()V
.end method

.method public abstract onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V
.end method

.method public abstract onPhoneBook(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPhoneBookCtrlStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;)V
.end method

.method public abstract onPhoneBookList(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;)V
.end method

.method public abstract onPhoneCallingOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;)V
.end method

.method public abstract onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V
.end method

.method public abstract onSetPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;)V
.end method

.method public abstract onVersion(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;)V
.end method

.method public abstract ondeviceSwitchedProtocol(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V
.end method
