.class public Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;
.super Ljava/lang/Object;
.source "BluetoothProtocolAnalyzer2.java"

# interfaces
.implements Lcom/backaudio/android/driver/bluetooth/IBluetoothProtocolAnalyzer;


# static fields
.field private static final TAG:Ljava/lang/String; = "tag-bt2"

.field private static logger:Lorg/slf4j/Logger;

.field public static sdf:Ljava/text/SimpleDateFormat;


# instance fields
.field private baos:Ljava/io/ByteArrayOutputStream;

.field dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

.field private eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

.field private mNextExpectedValue:B

.field private mValidProtocol:Z

.field public out:Ljava/io/FileOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-class v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    .line 35
    sput-object v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 44
    const-string v1, "yyyy-MM-dd hh:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 43
    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->sdf:Ljava/text/SimpleDateFormat;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .line 39
    const/4 v1, -0x1

    iput-byte v1, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mNextExpectedValue:B

    .line 40
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mValidProtocol:Z

    .line 41
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->baos:Ljava/io/ByteArrayOutputStream;

    .line 42
    iput-object v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;

    .line 242
    new-instance v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    const-string v2, ""

    invoke-direct {v1, v3, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    .line 53
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    .line 54
    const-string v2, "/data/data/com.touchus.benchilauncher/bluetooth2.txt"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    .line 53
    iput-object v1, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private analyze([B)V
    .locals 10
    .param p1, "buffer"    # [B

    .prologue
    const/4 v9, 0x0

    .line 89
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "tag-bt2 hexstring: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, p1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 90
    iget-object v4, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;

    if-eqz v4, :cond_0

    .line 91
    iget-object v5, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;

    monitor-enter v5

    .line 93
    :try_start_0
    iget-object v4, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v4, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 94
    iget-object v4, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\t"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->sdf:Ljava/text/SimpleDateFormat;

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 95
    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 94
    invoke-virtual {v4, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 96
    iget-object v4, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :goto_0
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :cond_0
    iget-object v4, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    if-eqz v4, :cond_1

    if-eqz p1, :cond_1

    array-length v4, p1

    const/4 v5, 0x4

    if-ge v4, v5, :cond_3

    .line 103
    :cond_1
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v5, "tag-bt2--------invalid--------"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 153
    :cond_2
    :goto_1
    return-void

    .line 97
    :catch_0
    move-exception v3

    .line 98
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 91
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 106
    :cond_3
    aget-byte v4, p1, v9

    int-to-char v0, v4

    .line 107
    .local v0, "ch1":C
    const/4 v4, 0x1

    aget-byte v4, p1, v4

    int-to-char v1, v4

    .line 109
    .local v1, "ch2":C
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 111
    :pswitch_1
    invoke-direct {p0, v1, p1}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->handleStartD(C[B)V

    goto :goto_1

    .line 114
    :pswitch_2
    invoke-direct {p0, v1, p1}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->handleStartI(C[B)V

    goto :goto_1

    .line 117
    :pswitch_3
    invoke-direct {p0, v1, p1}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->handleStartM(C[B)V

    goto :goto_1

    .line 120
    :pswitch_4
    invoke-direct {p0, v1, p1}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->handleStartP(C[B)V

    goto :goto_1

    .line 123
    :pswitch_5
    const/16 v4, 0x31

    if-eq v1, v4, :cond_4

    const/16 v4, 0x30

    if-ne v1, v4, :cond_2

    .line 124
    :cond_4
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v5, "tag-bt2T: \u8f66\u673a\uff08\u84dd\u7259/LOCAL\uff09\u51fa\u58f0\u97f3"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 125
    new-instance v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;

    const/4 v4, 0x0

    .line 126
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    .line 125
    invoke-direct {v2, v4, v5}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .local v2, "dsp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    iget-object v4, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v4, v2}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->ondeviceSwitchedProtocol(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V

    goto :goto_1

    .line 131
    .end local v2    # "dsp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    :pswitch_6
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "tag-bt2---start S----"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 132
    new-instance v6, Ljava/lang/String;

    array-length v7, p1

    add-int/lit8 v7, v7, -0x2

    invoke-direct {v6, p1, v9, v7}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 136
    :pswitch_7
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "tag-bt2---start H----"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 137
    new-instance v6, Ljava/lang/String;

    array-length v7, p1

    add-int/lit8 v7, v7, -0x2

    invoke-direct {v6, p1, v9, v7}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 136
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 141
    :pswitch_8
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "tag-bt2---start O----"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 142
    new-instance v6, Ljava/lang/String;

    array-length v7, p1

    add-int/lit8 v7, v7, -0x2

    invoke-direct {v6, p1, v9, v7}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 141
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 146
    :pswitch_9
    sget-object v4, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "tag-bt2---start N----"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 147
    new-instance v6, Ljava/lang/String;

    array-length v7, p1

    add-int/lit8 v7, v7, -0x2

    invoke-direct {v6, p1, v9, v7}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 146
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_9
        :pswitch_8
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method private handleStartD(C[B)V
    .locals 5
    .param p1, "ch"    # C
    .param p2, "buffer"    # [B

    .prologue
    .line 156
    array-length v0, p2

    .line 158
    .local v0, "len":I
    packed-switch p1, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 160
    :pswitch_0
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x2

    add-int/lit8 v3, v0, -0x4

    invoke-direct {v1, p2, v2, v3}, Ljava/lang/String;-><init>([BII)V

    .line 161
    .local v1, "value":Ljava/lang/String;
    iget-object v2, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    invoke-virtual {v2, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->setMACAddr(Ljava/lang/String;)V

    .line 162
    iget-object v2, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    iget-object v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    invoke-interface {v2, v3}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onDeviceName(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V

    .line 163
    sget-object v2, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tag-bt2MN: MACAddr() PIN = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_0
    .end packed-switch
.end method

.method private handleStartI(C[B)V
    .locals 12
    .param p1, "ch"    # C
    .param p2, "buffer"    # [B

    .prologue
    const/4 v10, 0x2

    const/4 v11, 0x0

    .line 171
    array-length v6, p2

    .line 172
    .local v6, "len":I
    packed-switch p1, :pswitch_data_0

    .line 241
    :goto_0
    :pswitch_0
    return-void

    .line 174
    :pswitch_1
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IA: HFP\u65ad\u5f00"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 175
    new-instance v8, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    const-string v9, ""

    invoke-direct {v8, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .local v8, "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->UNCONNECT:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v8, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->setPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;)V

    .line 177
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v8}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V

    goto :goto_0

    .line 180
    .end local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    :pswitch_2
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IB: HFP\u8fde\u63a5\u6210\u529f"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 181
    new-instance v8, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    const-string v9, ""

    invoke-direct {v8, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .restart local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTED:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v8, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->setPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;)V

    .line 183
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v8}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V

    goto :goto_0

    .line 186
    .end local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    :pswitch_3
    new-instance v1, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;

    const-string v9, "0"

    invoke-direct {v1, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    .local v1, "cor":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onCallOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;)V

    .line 188
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IC: onCallOut() "

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    .end local v1    # "cor":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;
    :pswitch_4
    new-instance v7, Ljava/lang/String;

    add-int/lit8 v9, v6, -0x4

    invoke-direct {v7, p2, v10, v9}, Ljava/lang/String;-><init>([BII)V

    .line 192
    .local v7, "number":Ljava/lang/String;
    new-instance v5, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;

    invoke-direct {v5, v11, v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .local v5, "icp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v5}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onIncomingCall(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;)V

    .line 194
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2ID: onIncomingCall() "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 197
    .end local v5    # "icp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;
    .end local v7    # "number":Ljava/lang/String;
    :pswitch_5
    new-instance v7, Ljava/lang/String;

    add-int/lit8 v9, v6, -0x4

    invoke-direct {v7, p2, v10, v9}, Ljava/lang/String;-><init>([BII)V

    .line 198
    .restart local v7    # "number":Ljava/lang/String;
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2\u4e09\u65b9\u6765\u7535\uff1a"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 201
    .end local v7    # "number":Ljava/lang/String;
    :pswitch_6
    new-instance v4, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;

    const-string v9, "0"

    invoke-direct {v4, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    .local v4, "hupr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v4}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onHangUpPhone(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;)V

    .line 203
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IF: \u6302\u673a------------"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    .end local v4    # "hupr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;
    :pswitch_7
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IG: \u5df2\u63a5\u901a-----------"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 207
    new-instance v8, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    const-string v9, ""

    invoke-direct {v8, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    .restart local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->TALKING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v8, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->setPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;)V

    .line 209
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v8}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V

    goto/16 :goto_0

    .line 212
    .end local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    :pswitch_8
    new-instance v7, Ljava/lang/String;

    add-int/lit8 v9, v6, -0x4

    invoke-direct {v7, p2, v10, v9}, Ljava/lang/String;-><init>([BII)V

    .line 213
    .restart local v7    # "number":Ljava/lang/String;
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;

    invoke-direct {v0, v11, v7}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .local v0, "cop":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v0}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneCallingOut(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;)V

    .line 215
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2IR: onPhoneCallingOut() "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    .end local v0    # "cop":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;
    .end local v7    # "number":Ljava/lang/String;
    :pswitch_9
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IV: HFP\u8fde\u63a5\u4e2d"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 219
    new-instance v8, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    const-string v9, ""

    invoke-direct {v8, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    .restart local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;->CONNECTING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    invoke-virtual {v8, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->setPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;)V

    .line 221
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v8}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V

    goto/16 :goto_0

    .line 224
    .end local v8    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    :pswitch_a
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2II: \u8fdb\u5165\u914d\u5bf9"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 225
    new-instance v2, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;

    const-string v9, "0"

    invoke-direct {v2, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .local v2, "epmr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v2}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V

    goto/16 :goto_0

    .line 229
    .end local v2    # "epmr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    :pswitch_b
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IJ: \u9000\u51fa\u914d\u5bf9"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 230
    new-instance v3, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;

    const-string v9, "1"

    invoke-direct {v3, v11, v9}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    .local v3, "epmr1":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v3}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V

    goto/16 :goto_0

    .line 234
    .end local v3    # "epmr1":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    :pswitch_c
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2IS: \u84dd\u7259\u521d\u59cb\u5316\u5b8c\u6210"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 237
    :pswitch_d
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2\u6253\u5f00/\u5173\u95ed\u54aa\u5934"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method private handleStartM(C[B)V
    .locals 33
    .param p1, "ch"    # C
    .param p2, "buffer"    # [B

    .prologue
    .line 244
    move-object/from16 v0, p2

    array-length v12, v0

    .line 246
    .local v12, "len":I
    packed-switch p1, :pswitch_data_0

    .line 360
    :goto_0
    :pswitch_0
    return-void

    .line 248
    :pswitch_1
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v30, "tag-bt2MA: \u97f3\u4e50\u6682\u505c\u4e2d/\u8fde\u63a5\u6210\u529f"

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 249
    new-instance v15, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;

    const-string v29, ""

    const-string v30, ""

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-direct {v15, v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    .local v15, "mpsp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->setMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;)V

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v15}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V

    goto :goto_0

    .line 254
    .end local v15    # "mpsp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    :pswitch_2
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v30, "tag-bt2MB: \u97f3\u4e50\u64ad\u653e\u4e2d"

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 255
    new-instance v16, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;

    const-string v29, ""

    const-string v30, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    .local v16, "mpsp1":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;->PLAYING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;->setMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V

    goto :goto_0

    .line 260
    .end local v16    # "mpsp1":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    :pswitch_3
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v30, "tag-bt2MC: \u8f66\u673a\uff08\u84dd\u7259/LOCAL\uff09\u51fa\u58f0\u97f3"

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 261
    new-instance v6, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;

    const/16 v29, 0x0

    const-string v30, "0"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-direct {v6, v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    .local v6, "dsp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v6}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->ondeviceSwitchedProtocol(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V

    goto :goto_0

    .line 265
    .end local v6    # "dsp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    :pswitch_4
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v30, "tag-bt2MD: \u624b\u673a\uff08REMOTE\uff09\u51fa\u58f0\u97f3"

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 266
    new-instance v7, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;

    const/16 v29, 0x0

    const-string v30, "1"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-direct {v7, v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    .local v7, "dsp1":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v7}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->ondeviceSwitchedProtocol(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V

    goto/16 :goto_0

    .line 270
    .end local v7    # "dsp1":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
    :pswitch_5
    const/16 v29, 0x2

    aget-byte v29, p2, v29

    add-int/lit8 v26, v29, -0x30

    .line 271
    .local v26, "type":I
    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v27

    .line 272
    .local v27, "value":Ljava/lang/String;
    new-instance v21, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;

    const/16 v29, 0x0

    const-string v30, ""

    move-object/from16 v0, v21

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    .local v21, "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->setPhoneStatus(Ljava/lang/String;)V

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V

    .line 275
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MG: onPhoneStatus() "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;->getPhoneStatus()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 278
    .end local v21    # "psp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
    .end local v26    # "type":I
    .end local v27    # "value":Ljava/lang/String;
    :pswitch_6
    const/16 v29, 0x2

    aget-byte v29, p2, v29

    add-int/lit8 v23, v29, -0x30

    .line 279
    .local v23, "st":I
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MU: "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 280
    new-instance v18, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;

    const-string v29, "A2DPSTAT"

    .line 281
    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    .line 280
    move-object/from16 v0, v18

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    .local v18, "msp3":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V

    goto/16 :goto_0

    .line 285
    .end local v18    # "msp3":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    .end local v23    # "st":I
    :pswitch_7
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v30, "tag-bt2MY: A2DP\u65ad\u5f00"

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 286
    new-instance v17, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;

    const-string v29, "A2DPSTAT"

    const-string v30, "0"

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .local v17, "msp2":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaPlayStatus(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V

    goto/16 :goto_0

    .line 290
    .end local v17    # "msp2":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
    :pswitch_8
    new-instance v27, Ljava/lang/String;

    const/16 v29, 0x2

    add-int/lit8 v30, v12, -0x4

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    .line 291
    .restart local v27    # "value":Ljava/lang/String;
    new-instance v28, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;

    const/16 v29, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    .local v28, "vp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onVersion(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;)V

    .line 293
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MW: onVersion() "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 296
    .end local v27    # "value":Ljava/lang/String;
    .end local v28    # "vp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;
    :pswitch_9
    new-instance v27, Ljava/lang/String;

    const/16 v29, 0x2

    add-int/lit8 v30, v12, -0x4

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    .line 297
    .restart local v27    # "value":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->setDeviceName(Ljava/lang/String;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    move-object/from16 v30, v0

    invoke-interface/range {v29 .. v30}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onDeviceName(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V

    .line 299
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MM: onDeviceName() name = "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    .end local v27    # "value":Ljava/lang/String;
    :pswitch_a
    new-instance v27, Ljava/lang/String;

    const/16 v29, 0x2

    add-int/lit8 v30, v12, -0x4

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    .line 303
    .restart local v27    # "value":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;->setPIN(Ljava/lang/String;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;

    move-object/from16 v30, v0

    invoke-interface/range {v29 .. v30}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onDeviceName(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V

    .line 305
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MN: onDeviceName() PIN = "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 308
    .end local v27    # "value":Ljava/lang/String;
    :pswitch_b
    const/4 v4, 0x0

    .line 309
    .local v4, "address":Ljava/lang/String;
    const/16 v19, 0x0

    .line 310
    .local v19, "name":Ljava/lang/String;
    const/16 v29, 0x2

    aget-byte v29, p2, v29

    if-gez v29, :cond_0

    const/16 v29, 0x2

    aget-byte v29, p2, v29

    move/from16 v0, v29

    add-int/lit16 v9, v0, 0x100

    .line 311
    .local v9, "index":I
    :goto_1
    new-instance v4, Ljava/lang/String;

    .end local v4    # "address":Ljava/lang/String;
    const/16 v29, 0x3

    const/16 v30, 0xc

    move-object/from16 v0, p2

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v4, v0, v1, v2}, Ljava/lang/String;-><init>([BII)V

    .line 312
    .restart local v4    # "address":Ljava/lang/String;
    const/16 v29, 0x30

    move/from16 v0, v29

    if-ne v9, v0, :cond_1

    .line 313
    new-instance v5, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;

    const/16 v29, 0x0

    move-object/from16 v0, v29

    invoke-direct {v5, v0, v4}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    .local v5, "cdp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
    new-instance v29, Ljava/lang/String;

    const/16 v30, 0xf

    add-int/lit8 v31, v12, -0x11

    move-object/from16 v0, v29

    move-object/from16 v1, p2

    move/from16 v2, v30

    move/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;->setDeviceName(Ljava/lang/String;)V

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onConnectedDevice(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;)V

    .line 317
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MX: address="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 310
    .end local v5    # "cdp":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
    .end local v9    # "index":I
    :cond_0
    const/16 v29, 0x2

    aget-byte v9, p2, v29

    goto :goto_1

    .line 319
    .restart local v9    # "index":I
    :cond_1
    new-instance v19, Ljava/lang/String;

    .end local v19    # "name":Ljava/lang/String;
    const/16 v29, 0xf

    add-int/lit8 v30, v12, -0x11

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move/from16 v2, v29

    move/from16 v3, v30

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    .line 320
    .restart local v19    # "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-interface {v0, v4, v1}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairedDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MX: address="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " name="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 325
    .end local v4    # "address":Ljava/lang/String;
    .end local v9    # "index":I
    .end local v19    # "name":Ljava/lang/String;
    :pswitch_c
    new-instance v29, Ljava/lang/String;

    const/16 v30, 0x2

    add-int/lit8 v31, v12, -0x4

    move-object/from16 v0, v29

    move-object/from16 v1, p2

    move/from16 v2, v30

    move/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    const-string v30, "\n"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 326
    .local v10, "info":[Ljava/lang/String;
    array-length v11, v10

    .line 327
    .local v11, "l":I
    new-instance v14, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;

    invoke-direct {v14}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;-><init>()V

    .line 328
    .local v14, "mip":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
    const/16 v29, 0x1

    move/from16 v0, v29

    invoke-virtual {v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->setAnalyzed(Z)V

    .line 329
    if-lez v11, :cond_2

    const/16 v29, 0x0

    aget-object v29, v10, v29

    :goto_2
    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->setTitle(Ljava/lang/String;)V

    .line 330
    const/16 v29, 0x1

    move/from16 v0, v29

    if-le v11, v0, :cond_3

    const/16 v29, 0x1

    aget-object v29, v10, v29

    :goto_3
    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->setArtist(Ljava/lang/String;)V

    .line 331
    const/16 v24, 0x0

    .line 332
    .local v24, "time":I
    const/16 v20, 0x0

    .line 333
    .local v20, "number":I
    const/16 v25, 0x0

    .line 335
    .local v25, "total":I
    const/16 v29, 0x2

    move/from16 v0, v29

    if-le v11, v0, :cond_4

    const/16 v29, 0x2

    :try_start_0
    aget-object v29, v10, v29

    :goto_4
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v24

    .line 340
    :goto_5
    const/16 v29, 0x3

    move/from16 v0, v29

    if-le v11, v0, :cond_5

    const/16 v29, 0x3

    :try_start_1
    aget-object v29, v10, v29

    :goto_6
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v20

    .line 345
    :goto_7
    const/16 v29, 0x4

    move/from16 v0, v29

    if-le v11, v0, :cond_6

    const/16 v29, 0x4

    :try_start_2
    aget-object v29, v10, v29

    :goto_8
    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v25

    .line 349
    :goto_9
    move/from16 v0, v24

    invoke-virtual {v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->setPlayTime(I)V

    .line 350
    move/from16 v0, v20

    invoke-virtual {v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->setNumber(I)V

    .line 351
    move/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;->setTotalNumber(I)V

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v14}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onMediaInfo(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;)V

    .line 353
    const-string v13, ""

    .line 354
    .local v13, "mi":Ljava/lang/String;
    array-length v0, v10

    move/from16 v30, v0

    const/16 v29, 0x0

    :goto_a
    move/from16 v0, v29

    move/from16 v1, v30

    if-lt v0, v1, :cond_7

    .line 357
    sget-object v29, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "tag-bt2MI: -+- "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    .end local v13    # "mi":Ljava/lang/String;
    .end local v20    # "number":I
    .end local v24    # "time":I
    .end local v25    # "total":I
    :cond_2
    const-string v29, "UNKNOWN"

    goto/16 :goto_2

    .line 330
    :cond_3
    const-string v29, "UNKNOWN"

    goto :goto_3

    .line 335
    .restart local v20    # "number":I
    .restart local v24    # "time":I
    .restart local v25    # "total":I
    :cond_4
    :try_start_3
    const-string v29, "-1"
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    .line 336
    :catch_0
    move-exception v8

    .line 337
    .local v8, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v8}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_5

    .line 340
    .end local v8    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    :try_start_4
    const-string v29, "-1"
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    .line 341
    :catch_1
    move-exception v8

    .line 342
    .restart local v8    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v8}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_7

    .line 345
    .end local v8    # "e":Ljava/lang/NumberFormatException;
    :cond_6
    :try_start_5
    const-string v29, "-1"
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_8

    .line 346
    :catch_2
    move-exception v8

    .line 347
    .restart local v8    # "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v8}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_9

    .line 354
    .end local v8    # "e":Ljava/lang/NumberFormatException;
    .restart local v13    # "mi":Ljava/lang/String;
    :cond_7
    aget-object v22, v10, v29

    .line 355
    .local v22, "s":Ljava/lang/String;
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 354
    add-int/lit8 v29, v29, 0x1

    goto :goto_a

    .line 246
    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_7
    .end packed-switch
.end method

.method private handleStartP(C[B)V
    .locals 12
    .param p1, "ch"    # C
    .param p2, "buffer"    # [B

    .prologue
    .line 363
    array-length v3, p2

    .line 364
    .local v3, "len":I
    sparse-switch p1, :sswitch_data_0

    .line 410
    :goto_0
    return-void

    .line 366
    :sswitch_0
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2P1: onPairingModeResult() success"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 367
    new-instance v0, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;

    const/4 v9, 0x0

    const-string v10, "0"

    invoke-direct {v0, v9, v10}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    .local v0, "epmr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v0}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V

    goto :goto_0

    .line 371
    .end local v0    # "epmr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    :sswitch_1
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2P0: onPairingModeResult() fail"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 372
    new-instance v4, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;

    const/4 v9, 0x0

    .line 373
    const/4 v10, 0x0

    .line 372
    invoke-direct {v4, v9, v10}, Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    .local v4, "lepmr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v4}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPairingModeResult(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V

    goto :goto_0

    .line 377
    .end local v4    # "lepmr":Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
    :sswitch_2
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2PA: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :sswitch_3
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x2

    add-int/lit8 v11, v3, -0x4

    invoke-direct {v9, p2, v10, v11}, Ljava/lang/String;-><init>([BII)V

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "kv":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v9, v1

    const/4 v10, 0x2

    if-ge v9, v10, :cond_1

    .line 382
    :cond_0
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2PB: invalid protocol name="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    aget-object v11, v1, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :cond_1
    const/4 v5, 0x0

    .line 386
    .local v5, "name":Ljava/lang/String;
    const/4 v7, 0x0

    .line 387
    .local v7, "number":Ljava/lang/String;
    const/4 v9, 0x0

    aget-object v5, v1, v9

    .line 388
    const/4 v9, 0x1

    aget-object v7, v1, v9

    .line 389
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v5, v7}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneBook(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2PB: name="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " number="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 393
    .end local v1    # "kv":[Ljava/lang/String;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "number":Ljava/lang/String;
    :sswitch_4
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v10, "tag-bt2PC: onFinishDownloadPhoneBook()"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 394
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onFinishDownloadPhoneBook()V

    goto/16 :goto_0

    .line 397
    :sswitch_5
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x2

    add-int/lit8 v11, v3, -0x4

    invoke-direct {v9, p2, v10, v11}, Ljava/lang/String;-><init>([BII)V

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 398
    .local v2, "kv1":[Ljava/lang/String;
    if-eqz v2, :cond_2

    array-length v9, v2

    const/4 v10, 0x2

    if-ge v9, v10, :cond_3

    .line 399
    :cond_2
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2PD: invalid protocol name="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    aget-object v11, v2, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 402
    :cond_3
    const/4 v6, 0x0

    .line 403
    .local v6, "name1":Ljava/lang/String;
    const/4 v8, 0x0

    .line 404
    .local v8, "number1":Ljava/lang/String;
    const/4 v9, 0x0

    aget-object v5, v2, v9

    .line 405
    .restart local v5    # "name":Ljava/lang/String;
    const/4 v9, 0x1

    aget-object v7, v2, v9

    .line 406
    .restart local v7    # "number":Ljava/lang/String;
    iget-object v9, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    invoke-interface {v9, v6, v8}, Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;->onPhoneBook(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    sget-object v9, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "tag-bt2PD: name="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " number="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 364
    nop

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_1
        0x31 -> :sswitch_0
        0x41 -> :sswitch_2
        0x42 -> :sswitch_3
        0x43 -> :sswitch_4
        0x44 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public push([B)V
    .locals 9
    .param p1, "buffer"    # [B

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/16 v6, 0xa

    .line 61
    array-length v2, p1

    .line 62
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 86
    return-void

    .line 63
    :cond_0
    aget-byte v3, p1, v1

    const/16 v4, 0xd

    if-ne v3, v4, :cond_4

    .line 64
    iput-byte v6, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mNextExpectedValue:B

    .line 72
    :cond_1
    :goto_1
    aget-byte v3, p1, v1

    if-ne v3, v7, :cond_2

    .line 73
    aput-byte v6, p1, v1

    .line 75
    :cond_2
    iget-object v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->baos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3, p1, v1, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 76
    iget-boolean v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mValidProtocol:Z

    if-eqz v3, :cond_3

    .line 78
    :try_start_0
    iget-object v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->baos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->analyze([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-virtual {p0}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->reset()V

    .line 62
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :cond_4
    aget-byte v3, p1, v1

    if-ne v3, v6, :cond_5

    .line 66
    iget-byte v3, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mNextExpectedValue:B

    if-ne v3, v6, :cond_1

    .line 67
    iput-boolean v8, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mValidProtocol:Z

    goto :goto_1

    .line 70
    :cond_5
    iput-byte v7, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mNextExpectedValue:B

    goto :goto_1

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "tag-bt2analyze error"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    invoke-virtual {p0}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->reset()V

    goto :goto_2

    .line 81
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 82
    invoke-virtual {p0}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->reset()V

    .line 83
    throw v3
.end method

.method public push([BII)V
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v5, 0x0

    .line 421
    invoke-static {p1, p3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    .line 422
    .local v1, "newbuffer":[B
    sget-object v2, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tag-bt2 bluetoothprot recv::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, v1

    invoke-static {v1, v5, v4}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 424
    const/4 v2, 0x0

    :try_start_0
    array-length v3, v1

    invoke-static {v1, v2, v3}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/backaudio/android/driver/Utils;->saveLogLine(Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 428
    :goto_0
    invoke-virtual {p0, v1}, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->push([B)V

    .line 429
    return-void

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 414
    const/4 v0, -0x1

    iput-byte v0, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mNextExpectedValue:B

    .line 415
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->mValidProtocol:Z

    .line 416
    iget-object v0, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->baos:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 417
    return-void
.end method

.method public setEventHandler(Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;)V
    .locals 2
    .param p1, "eventHandler"    # Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .prologue
    .line 47
    sget-object v0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->logger:Lorg/slf4j/Logger;

    const-string v1, "tag-bt2            handler"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2;->eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;

    .line 49
    return-void
.end method
