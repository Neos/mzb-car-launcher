.class public Lcom/backaudio/android/driver/SerialPortMcuIO;
.super Ljava/lang/Object;
.source "SerialPortMcuIO.java"


# static fields
.field private static instance:Lcom/backaudio/android/driver/SerialPortMcuIO;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private logger:Lorg/slf4j/Logger;

.field private mFileInputStream:Ljava/io/FileInputStream;

.field private mFileOutputStream:Ljava/io/FileOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/backaudio/android/driver/SerialPortMcuIO;->instance:Lcom/backaudio/android/driver/SerialPortMcuIO;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "driverlog"

    iput-object v0, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->TAG:Ljava/lang/String;

    .line 17
    const-class v0, Lcom/backaudio/android/driver/SerialPortMcuIO;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->logger:Lorg/slf4j/Logger;

    .line 30
    iput-object v1, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileInputStream:Ljava/io/FileInputStream;

    .line 31
    iput-object v1, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 34
    invoke-direct {p0}, Lcom/backaudio/android/driver/SerialPortMcuIO;->init()V

    .line 35
    return-void
.end method

.method public static getInstance()Lcom/backaudio/android/driver/SerialPortMcuIO;
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/backaudio/android/driver/SerialPortMcuIO;->instance:Lcom/backaudio/android/driver/SerialPortMcuIO;

    if-nez v0, :cond_1

    .line 21
    const-class v1, Lcom/backaudio/android/driver/SerialPortMcuIO;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Lcom/backaudio/android/driver/SerialPortMcuIO;->instance:Lcom/backaudio/android/driver/SerialPortMcuIO;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/backaudio/android/driver/SerialPortMcuIO;

    invoke-direct {v0}, Lcom/backaudio/android/driver/SerialPortMcuIO;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/SerialPortMcuIO;->instance:Lcom/backaudio/android/driver/SerialPortMcuIO;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lcom/backaudio/android/driver/SerialPortMcuIO;->instance:Lcom/backaudio/android/driver/SerialPortMcuIO;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private init()V
    .locals 7

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 41
    .local v0, "count":I
    :goto_0
    :try_start_0
    new-instance v3, Lcom/backaudio/android/driver/LibSerialPort;

    new-instance v4, Ljava/io/File;

    const-string v5, "/dev/ttyMT3"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const v5, 0x1c200

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/backaudio/android/driver/LibSerialPort;-><init>(Ljava/io/File;II)V

    .line 42
    .local v3, "serialPort":Lcom/backaudio/android/driver/LibSerialPort;
    invoke-virtual {v3}, Lcom/backaudio/android/driver/LibSerialPort;->getmFileInputStream()Ljava/io/FileInputStream;

    move-result-object v4

    iput-object v4, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileInputStream:Ljava/io/FileInputStream;

    .line 43
    invoke-virtual {v3}, Lcom/backaudio/android/driver/LibSerialPort;->getmFileOutputStream()Ljava/io/FileOutputStream;

    move-result-object v4

    iput-object v4, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileOutputStream:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 44
    .end local v3    # "serialPort":Lcom/backaudio/android/driver/LibSerialPort;
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Ljava/lang/Exception;
    add-int/lit8 v0, v0, 0x1

    .line 46
    const/4 v4, 0x2

    if-le v0, v4, :cond_0

    .line 56
    return-void

    .line 50
    :cond_0
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 51
    :catch_1
    move-exception v2

    .line 52
    .local v2, "e1":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public read([B)I
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileInputStream:Ljava/io/FileInputStream;

    if-nez v0, :cond_1

    .line 60
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileInputStream:Ljava/io/FileInputStream;

    if-eqz v0, :cond_2

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v0, p1}, Ljava/io/FileInputStream;->read([B)I

    move-result v0

    return v0

    .line 61
    :cond_2
    invoke-direct {p0}, Lcom/backaudio/android/driver/SerialPortMcuIO;->init()V

    .line 62
    iget-object v0, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileInputStream:Ljava/io/FileInputStream;

    if-nez v0, :cond_0

    .line 63
    const-string v0, "driverlog"

    const-string v1, "/dev/ttyMT3 cannot open serial port"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public write([B)V
    .locals 6
    .param p1, "responseBuffer"    # [B

    .prologue
    .line 71
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mcu: write->["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    array-length v4, p1

    invoke-static {p1, v3, v4}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "temp":Ljava/lang/String;
    iget-object v2, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->logger:Lorg/slf4j/Logger;

    invoke-interface {v2, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 74
    const-class v3, Lcom/backaudio/android/driver/SerialPortMcuIO;

    monitor-enter v3

    .line 75
    :try_start_0
    iget-object v2, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileOutputStream:Ljava/io/FileOutputStream;

    if-nez v2, :cond_0

    .line 76
    invoke-direct {p0}, Lcom/backaudio/android/driver/SerialPortMcuIO;->init()V

    .line 77
    iget-object v2, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileOutputStream:Ljava/io/FileOutputStream;

    if-nez v2, :cond_0

    .line 78
    const-string v2, "driverlog"

    const-string v4, "/dev/ttyMT3 cannot open serial port"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :goto_0
    return-void

    .line 83
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 84
    iget-object v2, p0, Lcom/backaudio/android/driver/SerialPortMcuIO;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :goto_1
    const-wide/16 v4, 0x32

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    :goto_2
    :try_start_3
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v2, "driverlog"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/dev/ttyMT3 cannot write error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 90
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    goto :goto_2
.end method
