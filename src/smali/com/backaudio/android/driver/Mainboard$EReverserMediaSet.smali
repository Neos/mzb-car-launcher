.class public final enum Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EReverserMediaSet"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

.field public static final enum FLAT:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

.field public static final enum MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

.field public static final enum NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

.field public static final enum QUERY:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1599
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    const-string v1, "MUTE"

    invoke-direct {v0, v1, v3, v3}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    const-string v1, "FLAT"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->FLAT:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    const-string v1, "NOMAL"

    invoke-direct {v0, v1, v5, v5}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    const-string v1, "QUERY"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v6, v2}, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->QUERY:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    .line 1598
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->MUTE:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->FLAT:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->NOMAL:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->QUERY:Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    aput-object v1, v0, v6

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 1603
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1604
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->code:I

    .line 1605
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1608
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EReverserMediaSet;->code:I

    int-to-byte v0, v0

    return v0
.end method
