.class public final enum Lcom/backaudio/android/driver/Mainboard$EAUXOperate;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EAUXOperate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EAUXOperate;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTIVATE:Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

.field public static final enum DEACTIVATE:Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EAUXOperate;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1625
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    const-string v1, "ACTIVATE"

    invoke-direct {v0, v1, v3, v2}, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->ACTIVATE:Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    const-string v1, "DEACTIVATE"

    invoke-direct {v0, v1, v2, v4}, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->DEACTIVATE:Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    .line 1624
    new-array v0, v4, [Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->ACTIVATE:Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    aput-object v1, v0, v3

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->DEACTIVATE:Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    aput-object v1, v0, v2

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 1629
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1630
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->code:I

    .line 1631
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EAUXOperate;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EAUXOperate;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EAUXOperate;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1634
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EAUXOperate;->code:I

    int-to-byte v0, v0

    return v0
.end method
