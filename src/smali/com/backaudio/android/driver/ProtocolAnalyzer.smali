.class public Lcom/backaudio/android/driver/ProtocolAnalyzer;
.super Ljava/lang/Object;
.source "ProtocolAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;
    }
.end annotation


# static fields
.field static final eventLock:Ljava/lang/Object;


# instance fields
.field private final TAG:Ljava/lang/String;

.field canCurrentProtocolLength:I

.field canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

.field canProtocolLength:I

.field currentProtocolLength:I

.field iDown:Z

.field private logger:Lorg/slf4j/Logger;

.field protocolBuffer:Ljava/io/ByteArrayOutputStream;

.field protocolLength:I

.field private final tmp:[B

.field touch_x:I

.field touch_y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 351
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->eventLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, "driverlog"

    iput-object v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->TAG:Ljava/lang/String;

    .line 14
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    .line 15
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    .line 16
    iput v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    .line 17
    iput v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    .line 18
    iput v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    .line 19
    iput v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    .line 21
    const-class v0, Lcom/backaudio/android/driver/ProtocolAnalyzer;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    .line 352
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->tmp:[B

    .line 10
    return-void
.end method

.method private analyzeCanbox([B)V
    .locals 9
    .param p1, "buf"    # [B

    .prologue
    .line 24
    const/4 v4, 0x5

    .local v4, "i":I
    :goto_0
    array-length v5, p1

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    .line 82
    return-void

    .line 26
    :cond_0
    :try_start_0
    aget-byte v0, p1, v4

    .line 27
    .local v0, "b":B
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    packed-switch v5, :pswitch_data_0

    .line 55
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    iget v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    if-ge v5, v6, :cond_4

    .line 56
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 57
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    .line 58
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    iget v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v6, :cond_1

    .line 60
    :try_start_1
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->newProtocol([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    :try_start_2
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 65
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    .line 66
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    .line 24
    .end local v0    # "b":B
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 30
    .restart local v0    # "b":B
    :pswitch_0
    const/16 v5, 0x2e

    if-ne v0, v5, :cond_2

    .line 31
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 32
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 78
    .end local v0    # "b":B
    :catch_0
    move-exception v2

    .line 79
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 34
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "b":B
    :cond_2
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "canbox: require 0x2E or 0xFF but "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 36
    .local v1, "canboxErrString":Ljava/lang/String;
    const-string v5, "driverlog"

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 40
    .end local v1    # "canboxErrString":Ljava/lang/String;
    :pswitch_1
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 41
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    goto :goto_1

    .line 45
    :pswitch_2
    iput v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    .line 46
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    if-gez v5, :cond_3

    .line 47
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    add-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    .line 49
    :cond_3
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    .line 50
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 51
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 61
    :catch_1
    move-exception v2

    .line 62
    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 64
    :try_start_5
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 65
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    .line 66
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    goto/16 :goto_1

    .line 63
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 64
    iget-object v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 65
    const/4 v6, 0x0

    iput v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    .line 66
    const/4 v6, 0x0

    iput v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    .line 67
    throw v5

    .line 70
    :cond_4
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canCurrentProtocolLength:I

    iget v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->canProtocolLength:I

    if-le v5, v6, :cond_1

    .line 71
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "canbox: shit:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 72
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 73
    .local v3, "err":Ljava/lang/String;
    const-string v5, "driverlog"

    invoke-static {v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 27
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private analyzeMcu([BII)V
    .locals 9
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 85
    move v3, p2

    .local v3, "i":I
    const/4 v4, 0x0

    .local v4, "l":I
    :goto_0
    array-length v5, p1

    if-ge v3, v5, :cond_0

    if-lt v4, p3, :cond_1

    .line 152
    :cond_0
    return-void

    .line 87
    :cond_1
    :try_start_0
    aget-byte v0, p1, v3

    .line 88
    .local v0, "b":B
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    packed-switch v5, :pswitch_data_0

    .line 125
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    iget v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    if-ge v5, v6, :cond_6

    .line 126
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 127
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    .line 128
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    iget v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v6, :cond_2

    .line 130
    :try_start_1
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->newMcuProtocol([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :try_start_2
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 135
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    .line 136
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    .line 85
    .end local v0    # "b":B
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 91
    .restart local v0    # "b":B
    :pswitch_0
    const/16 v5, -0x56

    if-ne v0, v5, :cond_3

    .line 92
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 93
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 148
    .end local v0    # "b":B
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 95
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "b":B
    :cond_3
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mcu: require 0xAA but "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "errString":Ljava/lang/String;
    const-string v5, "driverlog"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 101
    .end local v2    # "errString":Ljava/lang/String;
    :pswitch_1
    const/16 v5, 0x55

    if-ne v0, v5, :cond_4

    .line 103
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 104
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    goto :goto_1

    .line 107
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "require 0x55 but "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 107
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 109
    .restart local v2    # "errString":Ljava/lang/String;
    const-string v5, "driverlog"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto :goto_1

    .line 115
    .end local v2    # "errString":Ljava/lang/String;
    :pswitch_2
    iput v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    .line 116
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    if-gez v5, :cond_5

    .line 117
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    add-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    .line 119
    :cond_5
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    .line 120
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    const/4 v6, 0x1

    new-array v6, v6, [B

    const/4 v7, 0x0

    aput-byte v0, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 121
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 131
    :catch_1
    move-exception v1

    .line 132
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 134
    :try_start_5
    iget-object v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 135
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    .line 136
    const/4 v5, 0x0

    iput v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    goto/16 :goto_1

    .line 133
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 134
    iget-object v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolBuffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 135
    const/4 v6, 0x0

    iput v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    .line 136
    const/4 v6, 0x0

    iput v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    .line 137
    throw v5

    .line 140
    :cond_6
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->currentProtocolLength:I

    iget v6, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->protocolLength:I

    if-le v5, v6, :cond_2

    .line 141
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mcu: shit:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 142
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 141
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 143
    .restart local v2    # "errString":Ljava/lang/String;
    const-string v5, "driverlog"

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private checkMcu([B)Z
    .locals 6
    .param p1, "buffer"    # [B

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 335
    array-length v3, p1

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    .line 336
    const-string v3, "driverlog"

    const-string v4, "mcuParse_checkMcu buffer too short"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    :goto_0
    return v2

    .line 339
    :cond_1
    aget-byte v3, p1, v5

    array-length v4, p1

    add-int/lit8 v4, v4, -0x4

    int-to-byte v4, v4

    if-ne v3, v4, :cond_0

    .line 342
    aget-byte v0, p1, v5

    .line 343
    .local v0, "checksum":B
    const/4 v1, 0x3

    .local v1, "i":I
    :goto_1
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_2

    .line 346
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    aget-byte v3, p1, v3

    if-ne v0, v3, :cond_0

    .line 347
    const/4 v2, 0x1

    goto :goto_0

    .line 344
    :cond_2
    aget-byte v3, p1, v1

    add-int/2addr v3, v0

    int-to-byte v0, v3

    .line 343
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private handleEvent([BII)V
    .locals 7
    .param p1, "buffer"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v6, 0x1

    .line 374
    aget-byte v3, p1, v6

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x0

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    add-int v1, v3, v4

    .line 375
    .local v1, "type":I
    const/4 v3, 0x3

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x2

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    add-int v0, v3, v4

    .line 376
    .local v0, "code":I
    const/4 v3, 0x5

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x4

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    add-int v2, v3, v4

    .line 377
    .local v2, "value":I
    sget-object v3, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_ABS:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->getCode()I

    move-result v3

    if-ne v1, v3, :cond_2

    .line 378
    sget-object v3, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ABS_X:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->getCode()I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 379
    iput v2, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->touch_x:I

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    sget-object v3, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ABS_Y:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->getCode()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 381
    iput v2, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->touch_y:I

    goto :goto_0

    .line 383
    :cond_2
    sget-object v3, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_KEY:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->getCode()I

    move-result v3

    if-ne v1, v3, :cond_3

    .line 384
    sget-object v3, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->BTN_TOUCH:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->getCode()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 385
    if-nez v2, :cond_0

    .line 386
    iget-object v3, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "button handleEvent touch_x = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 387
    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->touch_x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",touch_y = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->touch_y:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 386
    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 388
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v3

    iget v4, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->touch_x:I

    iget v5, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->touch_y:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/backaudio/android/driver/Mainboard;->sendCoordinateToMcu(III)V

    goto :goto_0

    .line 391
    :cond_3
    sget-object v3, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_SYN:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-virtual {v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->getCode()I

    goto :goto_0
.end method

.method private newMcuProtocol([B)V
    .locals 6
    .param p1, "buffer"    # [B

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 178
    const-string v0, "driverlog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mcuParse:["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 179
    invoke-static {p1}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    .line 181
    :cond_0
    const-string v0, "driverlog"

    const-string v1, "mcu: ProtocolAnalyzer invalid mcu buffer, drop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_1
    :goto_0
    return-void

    .line 184
    :cond_2
    invoke-direct {p0, p1}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->checkMcu([B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 185
    const-string v0, "driverlog"

    const-string v1, "checkMcu failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 188
    :cond_3
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_4

    aget-byte v0, p1, v4

    if-nez v0, :cond_4

    .line 189
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeMcuEnterAccOrWakeup([B)V

    goto :goto_0

    .line 193
    :cond_4
    aget-byte v0, p1, v4

    const/16 v1, 0x11

    if-ne v0, v1, :cond_5

    .line 194
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeMcuInfoPro([B)V

    goto :goto_0

    .line 197
    :cond_5
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_6

    aget-byte v0, p1, v4

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_1

    .line 200
    :cond_6
    aget-byte v0, p1, v3

    const/16 v1, -0x1e

    if-ne v0, v1, :cond_7

    .line 201
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeMcuUpgradeStatePro([B)V

    goto :goto_0

    .line 204
    :cond_7
    aget-byte v0, p1, v3

    const/16 v1, -0x1c

    if-ne v0, v1, :cond_8

    .line 205
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeMcuUpgradeDateByindexPro([B)V

    goto :goto_0

    .line 208
    :cond_8
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_9

    aget-byte v0, p1, v4

    const/16 v1, 0x50

    if-ne v0, v1, :cond_9

    .line 209
    invoke-direct {p0, p1}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->pushCanbox([B)V

    goto :goto_0

    .line 212
    :cond_9
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_a

    aget-byte v0, p1, v4

    const/16 v1, 0x4b

    if-ne v0, v1, :cond_a

    .line 213
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeShowOrHideCarLayer([B)V

    goto :goto_0

    .line 216
    :cond_a
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_b

    aget-byte v0, p1, v4

    const/16 v1, 0x5b

    if-ne v0, v1, :cond_b

    .line 217
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeReverseTpye([B)V

    goto/16 :goto_0

    .line 220
    :cond_b
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_c

    aget-byte v0, p1, v4

    const/16 v1, 0x62

    if-ne v0, v1, :cond_c

    .line 221
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeStoreDateFromMCU([B)V

    goto/16 :goto_0

    .line 224
    :cond_c
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_d

    aget-byte v0, p1, v4

    const/16 v1, 0x66

    if-ne v0, v1, :cond_d

    .line 225
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeBenzTpye([B)V

    goto/16 :goto_0

    .line 228
    :cond_d
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_e

    aget-byte v0, p1, v4

    const/16 v1, 0x6a

    if-ne v0, v1, :cond_e

    .line 229
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeBenzSize([B)V

    goto/16 :goto_0

    .line 232
    :cond_e
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_f

    aget-byte v0, p1, v4

    const/16 v1, 0x68

    if-ne v0, v1, :cond_f

    .line 233
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeReverseMediaSet([B)V

    goto/16 :goto_0

    .line 236
    :cond_f
    aget-byte v0, p1, v3

    if-ne v0, v5, :cond_1

    aget-byte v0, p1, v4

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_1

    .line 237
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeLanguageMediaSet([B)V

    goto/16 :goto_0
.end method

.method private newProtocol([B)V
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 248
    const-string v0, "driverlog"

    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "canboxParse:["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 250
    invoke-static {p1}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 249
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 252
    :cond_0
    const-string v0, "driverlog"

    const-string v1, "canboxParse invalid canbox buffer, drop"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :goto_0
    :sswitch_0
    return-void

    .line 255
    :cond_1
    invoke-virtual {p0, p1}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->checkCanbox([B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 256
    const-string v0, "driverlog"

    const-string v1, "error check"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 259
    :cond_2
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-static {p1}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/backaudio/android/driver/Mainboard;->logcatCanbox(Ljava/lang/String;)V

    .line 260
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 297
    :sswitch_1
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeUpgradeStatePro([B)V

    goto :goto_0

    .line 262
    :sswitch_2
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeCarBasePro([B)V

    goto :goto_0

    .line 265
    :sswitch_3
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeIdriverPro([B)V

    goto :goto_0

    .line 268
    :sswitch_4
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeAirInfo([B)V

    goto :goto_0

    .line 271
    :sswitch_5
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeCarBasePro1([B)V

    goto :goto_0

    .line 274
    :sswitch_6
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeCarRunningInfoPro1([B)V

    goto :goto_0

    .line 277
    :sswitch_7
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeTimePro([B)V

    goto :goto_0

    .line 282
    :sswitch_8
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeOriginalCarPro([B)V

    goto :goto_0

    .line 285
    :sswitch_9
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeAUXStatus([B)V

    goto :goto_0

    .line 288
    :sswitch_a
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeCarRunningInfoPro([B)V

    goto :goto_0

    .line 291
    :sswitch_b
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeCarRunningInfoPro2([B)V

    goto :goto_0

    .line 294
    :sswitch_c
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeCanboxPro([B)V

    goto :goto_0

    .line 300
    :sswitch_d
    invoke-static {}, Lcom/backaudio/android/driver/Mainboard;->getInstance()Lcom/backaudio/android/driver/Mainboard;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/backaudio/android/driver/Mainboard;->analyzeUpgradeDateByindexPro([B)V

    goto/16 :goto_0

    .line 260
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1e -> :sswitch_1
        -0x1c -> :sswitch_d
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0xa -> :sswitch_0
        0x12 -> :sswitch_8
        0x2c -> :sswitch_9
        0x35 -> :sswitch_a
        0x36 -> :sswitch_b
        0x7f -> :sswitch_c
    .end sparse-switch
.end method

.method private pushCanbox([B)V
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    .line 168
    const-class v2, Lcom/backaudio/android/driver/ProtocolAnalyzer;

    monitor-enter v2

    .line 169
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "canbox:recv:<["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 170
    const-string v3, "]>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "temp":Ljava/lang/String;
    const-string v1, "driverlog"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 173
    invoke-direct {p0, p1}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->analyzeCanbox([B)V

    .line 168
    monitor-exit v2

    .line 175
    return-void

    .line 168
    .end local v0    # "temp":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public checkCanbox([B)Z
    .locals 7
    .param p1, "buffer"    # [B

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 314
    array-length v5, p1

    const/4 v6, 0x5

    if-ge v5, v6, :cond_1

    .line 315
    const-string v4, "driverlog"

    const-string v5, "canboxParse_checkMcu buffer too short"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_0
    :goto_0
    return v3

    .line 318
    :cond_1
    const/4 v5, 0x2

    aget-byte v5, p1, v5

    array-length v6, p1

    add-int/lit8 v6, v6, -0x4

    int-to-byte v6, v6

    if-ne v5, v6, :cond_0

    .line 321
    aget-byte v0, p1, v4

    .line 322
    .local v0, "checksum":B
    const/4 v2, 0x2

    .local v2, "i":I
    :goto_1
    array-length v5, p1

    add-int/lit8 v5, v5, -0x1

    if-lt v2, v5, :cond_2

    .line 325
    xor-int/lit16 v5, v0, 0xff

    int-to-byte v1, v5

    .line 326
    .local v1, "compCheck":B
    array-length v5, p1

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, p1, v5

    if-ne v1, v5, :cond_0

    move v3, v4

    .line 327
    goto :goto_0

    .line 323
    .end local v1    # "compCheck":B
    :cond_2
    aget-byte v5, p1, v2

    add-int/2addr v5, v0

    int-to-byte v0, v5

    .line 322
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public pushEvent([BII)V
    .locals 5
    .param p1, "buffer"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 362
    array-length v1, p1

    div-int/lit8 v0, v1, 0x2

    .line 364
    .local v0, "size":I
    sget-object v2, Lcom/backaudio/android/driver/ProtocolAnalyzer;->eventLock:Ljava/lang/Object;

    monitor-enter v2

    .line 365
    :try_start_0
    iget-object v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->tmp:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->tmp:[B

    array-length v4, v4

    invoke-static {p1, v0, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 366
    iget-object v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->tmp:[B

    invoke-direct {p0, v1, p2, p3}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->handleEvent([BII)V

    .line 364
    monitor-exit v2

    .line 368
    return-void

    .line 364
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public pushMcu([BII)V
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "index"    # I
    .param p3, "len"    # I

    .prologue
    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mcu:recv:<["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-static {p1, p2, p3}, Lcom/backaudio/android/driver/Utils;->byteArrayToHexString([BII)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 155
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "temp":Ljava/lang/String;
    const-string v1, "driverlog"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v1, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer;->logger:Lorg/slf4j/Logger;

    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 159
    invoke-direct {p0, p1, p2, p3}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->analyzeMcu([BII)V

    .line 160
    return-void
.end method
