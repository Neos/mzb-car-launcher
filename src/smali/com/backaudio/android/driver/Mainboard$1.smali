.class Lcom/backaudio/android/driver/Mainboard$1;
.super Ljava/lang/Object;
.source "Mainboard.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/backaudio/android/driver/Mainboard;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/backaudio/android/driver/Mainboard;


# direct methods
.method constructor <init>(Lcom/backaudio/android/driver/Mainboard;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/backaudio/android/driver/Mainboard$1;->this$0:Lcom/backaudio/android/driver/Mainboard;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 71
    const/16 v6, -0x13

    invoke-static {v6}, Landroid/os/Process;->setThreadPriority(I)V

    .line 72
    const/16 v6, 0x10

    new-array v0, v6, [B

    .line 74
    .local v0, "buffer":[B
    :goto_0
    iget-object v6, p0, Lcom/backaudio/android/driver/Mainboard$1;->this$0:Lcom/backaudio/android/driver/Mainboard;

    invoke-static {v6}, Lcom/backaudio/android/driver/Mainboard;->access$0(Lcom/backaudio/android/driver/Mainboard;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 111
    return-void

    .line 76
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v6, "/dev/input/event2"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 77
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 78
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 79
    .local v4, "in":Ljava/io/FileInputStream;
    :goto_1
    iget-object v6, p0, Lcom/backaudio/android/driver/Mainboard$1;->this$0:Lcom/backaudio/android/driver/Mainboard;

    invoke-static {v6}, Lcom/backaudio/android/driver/Mainboard;->access$0(Lcom/backaudio/android/driver/Mainboard;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 94
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "in":Ljava/io/FileInputStream;
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 105
    const-wide/16 v6, 0x7d0

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 106
    :catch_1
    move-exception v2

    .line 107
    .local v2, "e1":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 82
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    :cond_1
    const/4 v6, 0x0

    :try_start_2
    array-length v7, v0

    invoke-virtual {v4, v0, v6, v7}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    .line 83
    .local v5, "size":I
    iget-object v6, p0, Lcom/backaudio/android/driver/Mainboard$1;->this$0:Lcom/backaudio/android/driver/Mainboard;

    invoke-static {v6}, Lcom/backaudio/android/driver/Mainboard;->access$1(Lcom/backaudio/android/driver/Mainboard;)Lcom/backaudio/android/driver/ProtocolAnalyzer;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7, v5}, Lcom/backaudio/android/driver/ProtocolAnalyzer;->pushEvent([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 85
    .end local v5    # "size":I
    :catch_2
    move-exception v1

    .line 86
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 88
    const-wide/16 v6, 0x7d0

    :try_start_4
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 89
    :catch_3
    move-exception v2

    .line 90
    .restart local v2    # "e1":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1

    .line 97
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    .end local v4    # "in":Ljava/io/FileInputStream;
    :cond_2
    const-wide/16 v6, 0xbb8

    :try_start_6
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    .line 98
    :catch_4
    move-exception v1

    .line 99
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_0
.end method
