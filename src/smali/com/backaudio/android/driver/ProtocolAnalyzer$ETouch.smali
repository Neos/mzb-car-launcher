.class public final enum Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;
.super Ljava/lang/Enum;
.source "ProtocolAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/ProtocolAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ETouch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ABS_X:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field public static final enum ABS_Y:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field public static final enum BTN_TOUCH:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field public static final enum EV_ABS:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field public static final enum EV_KEY:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field public static final enum EV_SYN:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

.field public static final enum SYN_REPORT:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 397
    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "EV_SYN"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_SYN:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "EV_KEY"

    invoke-direct {v0, v1, v5, v5}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_KEY:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "EV_ABS"

    invoke-direct {v0, v1, v6, v7}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_ABS:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    .line 398
    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "BTN_TOUCH"

    const/16 v2, 0x14a

    invoke-direct {v0, v1, v7, v2}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->BTN_TOUCH:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "ABS_X"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v8, v2}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ABS_X:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "ABS_Y"

    const/4 v2, 0x5

    const/16 v3, 0x36

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ABS_Y:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    .line 399
    new-instance v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    const-string v1, "SYN_REPORT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v6}, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->SYN_REPORT:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    .line 396
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    sget-object v1, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_SYN:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_KEY:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->EV_ABS:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->BTN_TOUCH:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v1, v0, v7

    sget-object v1, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ABS_X:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ABS_Y:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->SYN_REPORT:Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ENUM$VALUES:[Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "temp"    # I

    .prologue
    .line 402
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 403
    iput p3, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->code:I

    .line 404
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->ENUM$VALUES:[Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/backaudio/android/driver/ProtocolAnalyzer$ETouch;->code:I

    return v0
.end method
