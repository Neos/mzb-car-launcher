.class public final enum Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
.super Ljava/lang/Enum;
.source "Mainboard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/backaudio/android/driver/Mainboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EIdriverEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum BT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum CALL_FIX:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum CALL_HELP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum CALL_SOS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum CARSET:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field private static final synthetic ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum ESC:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum HANG_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum K_HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum MEDIA:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum MODE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum MUTE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum NAVI:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum NEXT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum NONE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum PREV:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum RADIO:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum SPEAKOFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum SPEECH:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum VOL_ADD:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

.field public static final enum VOL_DES:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1493
    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NONE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "VOL_ADD"

    invoke-direct {v0, v1, v5, v5}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->VOL_ADD:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "VOL_DES"

    invoke-direct {v0, v1, v6, v6}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->VOL_DES:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v7, v7}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NEXT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "PREV"

    invoke-direct {v0, v1, v8, v8}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PREV:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "MODE"

    const/4 v2, 0x5

    .line 1494
    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MODE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "MUTE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MUTE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "BT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "PICK_UP"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "HANG_UP"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HANG_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "UP"

    const/16 v2, 0xa

    .line 1495
    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "DOWN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "HOME"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "PRESS"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "RIGHT"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "LEFT"

    const/16 v2, 0xf

    .line 1496
    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "BACK"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "RADIO"

    const/16 v2, 0x11

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RADIO:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "NAVI"

    const/16 v2, 0x12

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NAVI:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "MEDIA"

    const/16 v2, 0x13

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MEDIA:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "POWER_OFF"

    const/16 v2, 0x14

    .line 1497
    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "SPEECH"

    const/16 v2, 0x15

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->SPEECH:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "ESC"

    const/16 v2, 0x16

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->ESC:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "K_HOME"

    const/16 v2, 0x17

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->K_HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "SPEAKOFF"

    const/16 v2, 0x18

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->SPEAKOFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "STAR_BTN"

    const/16 v2, 0x19

    .line 1498
    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "CARSET"

    const/16 v2, 0x1a

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CARSET:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "CALL_HELP"

    const/16 v2, 0x1b

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CALL_HELP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "CALL_SOS"

    const/16 v2, 0x1c

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CALL_SOS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "CALL_FIX"

    const/16 v2, 0x1d

    .line 1499
    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CALL_FIX:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "TURN_RIGHT"

    const/16 v2, 0x1e

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    new-instance v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    const-string v1, "TURN_LEFT"

    const/16 v2, 0x1f

    const/16 v3, 0x41

    invoke-direct {v0, v1, v2, v3}, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    .line 1492
    const/16 v0, 0x20

    new-array v0, v0, [Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NONE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->VOL_ADD:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->VOL_DES:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NEXT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v1, v0, v7

    sget-object v1, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PREV:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MODE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MUTE:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PICK_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HANG_UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->UP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->DOWN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->PRESS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->BACK:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->RADIO:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->NAVI:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->MEDIA:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->POWER_OFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->SPEECH:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->ESC:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->K_HOME:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->SPEAKOFF:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->STAR_BTN:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CARSET:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CALL_HELP:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CALL_SOS:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->CALL_FIX:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_RIGHT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->TURN_LEFT:Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 1503
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1504
    iput p3, p0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->code:I

    .line 1505
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    return-object v0
.end method

.method public static values()[Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->ENUM$VALUES:[Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    array-length v1, v0

    new-array v2, v1, [Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getCode()B
    .locals 1

    .prologue
    .line 1508
    iget v0, p0, Lcom/backaudio/android/driver/Mainboard$EIdriverEnum;->code:I

    int-to-byte v0, v0

    return v0
.end method
