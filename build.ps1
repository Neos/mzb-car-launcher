Add-Type -A System.IO.Compression.FileSystem

# set dirs
$projectName  = "bcLaucnher2"
$srcDir       = "$PWD\src"
$studioResDir = "$PWD\studio_project\app\src\main\res"

$javaHome    = "C:\Program Files\Java\jdk1.8.0_152\bin"
$signapk     = "$PWD\tools\signapk.jar"
$apktool     = "$PWD\tools\apktool.jar"
$key_pk8     = "$PWD\keys\key.pk8"
$key_x509    = "$PWD\keys\key.x509.pem"

$apkName       = "$PWD\dist\$projectName.apk"
$signedApkName = "$PWD\dist\$projectName.signed.apk"

# set Java path
$env:Path = $env:Path + ";" + $javaHome

#clean
if( !(Test-Path $signedApkName ) ) {
    Remove-Item -Force -Recurse -Confirm:$false -Path:"$signedApkName"
}
if( !(Test-Path $apkName ) ) {
    Remove-Item -Force -Recurse -Confirm:$false -Path:"$apkName"
}
Remove-Item -Force -Recurse -Confirm:$false -Path:"$srcDir\res\"

# Start! :)
Write-Host "** Copy studio project designs ($studioResDir)"
Copy-Item $studioResDir -Destination "$srcDir\res\" -Recurse

Write-Host "** Building project $projectName"
Invoke-Command -scriptblock {java -jar "$apktool" b -o "$apkName" "$srcDir"}

if( !(Test-Path $apkName ) ) {
    Write-Error "Build output failed APK file not found"
    return
}

Write-Host "** Signing APK $apkName"
Invoke-Command -scriptblock {java -jar "$signapk" "$key_x509" "$key_pk8" "$apkName" "$signedApkName"}

Write-Host "** Build for '$projectName' completed"