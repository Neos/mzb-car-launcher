package cn.kuwo.autosdk;

import android.os.*;
import cn.kuwo.autosdk.api.*;

public class h implements g
{
    private volatile k a;
    private String b;
    private int c;
    private Handler d;
    
    public h() {
        this.a = null;
        this.b = "";
        this.c = 0;
        this.d = new i(Looper.getMainLooper());
    }
    
    @Override
    public void a(final String b, final SearchMode searchMode, final OnSearchListener onSearchListener) {
        this.b = b;
        this.c = 0;
        if (this.a != null) {
            this.a.a = true;
            this.a = null;
        }
        (this.a = new k(this.b, this.c, searchMode)).a(this.d, onSearchListener);
        r.a(s.b, this.a);
    }
}
