package cn.kuwo.autosdk;

public class o
{
    public static int a(final byte[] array, final boolean b) {
        return (int)a(array, 4, b);
    }
    
    public static long a(final byte[] array, int i, final boolean b) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("byte array is null or empty!");
        }
        final int min = Math.min(i, array.length);
        long n3;
        if (b) {
            long n;
            long n2;
            for (n = 0L, i = 0; i < min; ++i, n = (n2 | n << 8)) {
                n2 = (array[i] & 0xFF);
            }
            n3 = n;
        }
        else {
            long n4 = 0L;
            i = min - 1;
            while (true) {
                n3 = n4;
                if (i < 0) {
                    break;
                }
                final long n5 = array[i] & 0xFF;
                --i;
                n4 = (n5 | n4 << 8);
            }
        }
        return n3;
    }
}
