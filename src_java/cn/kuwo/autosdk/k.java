package cn.kuwo.autosdk;

import android.os.*;
import cn.kuwo.autosdk.api.*;
import java.util.*;
import android.text.*;

public final class k implements Runnable
{
    public volatile boolean a;
    private String b;
    private int c;
    private OnSearchListener d;
    private SearchMode e;
    private Handler f;
    
    public k(final String b, final int c, final SearchMode e) {
        this.a = false;
        this.b = null;
        this.c = 0;
        this.d = null;
        this.e = SearchMode.ALL;
        this.e = e;
        this.b = b;
        this.c = c;
    }
    
    private void a() {
        final boolean b = true;
        boolean b2 = true;
        final boolean b3 = false;
        final j j = new j(this.e);
        if (!this.a) {
            int n = 3;
            final int[] array = { 0 };
            boolean b4 = false;
            List a;
            for (a = null; a == null && n != 0; --n) {
                final long currentTimeMillis = System.currentTimeMillis();
                a = j.a(this.b, this.c, array);
                b4 = (System.currentTimeMillis() - currentTimeMillis > 8000L);
                if (this.a) {
                    return;
                }
            }
            if (!this.a) {
                if (a == null) {
                    if (array[0] == 1) {
                        if (this.d != null) {
                            final SearchStatus failed = SearchStatus.FAILED;
                            boolean b5 = b3;
                            if (this.c == 0) {
                                b5 = true;
                            }
                            this.a(failed, b5, null, true);
                        }
                    }
                    else if (this.d != null) {
                        final SearchStatus failed2 = SearchStatus.FAILED;
                        if (this.c != 0) {
                            b2 = false;
                        }
                        this.a(failed2, b2, null, b4);
                    }
                }
                else if (this.d != null) {
                    this.a(SearchStatus.SUCCESS, this.c == 0 && b, a, false);
                }
            }
        }
    }
    
    private void a(final SearchStatus searchStatus, final boolean b, final List list, final boolean b2) {
        if (this.f != null) {
            this.f.post((Runnable)new l(this, searchStatus, b, list, b2));
        }
    }
    
    public void a(final Handler f, final OnSearchListener d) {
        this.f = f;
        this.d = d;
    }
    
    @Override
    public void run() {
        if (!this.a && TextUtils.isEmpty((CharSequence)this.b)) {
            if (this.d != null) {
                this.a(SearchStatus.FAILED, this.c == 0, null, false);
            }
        }
        else if (!this.a) {
            this.a();
        }
    }
}
