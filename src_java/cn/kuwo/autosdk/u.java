package cn.kuwo.autosdk;

import android.os.*;
import android.util.*;
import java.util.*;

public class u
{
    private static final String a;
    
    static {
        if (Build$VERSION.SDK_INT >= 17) {
            a = "u\\d+_a\\d+";
            return;
        }
        a = "app_\\d+";
    }
    
    public static List a() {
        final ArrayList<v> list = new ArrayList<v>();
        for (final String s : a("toolbox ps -p -P -x -c")) {
            try {
                list.add(new v(s, null));
            }
            catch (Exception ex) {
                Log.d("ProcessManager", "Failed parsing line " + s);
            }
        }
        return list;
    }
    
    public static List a(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aconst_null    
        //     3: astore_1       
        //     4: new             Ljava/util/ArrayList;
        //     7: dup            
        //     8: invokespecial   java/util/ArrayList.<init>:()V
        //    11: astore_3       
        //    12: invokestatic    java/lang/Runtime.getRuntime:()Ljava/lang/Runtime;
        //    15: aload_0        
        //    16: invokevirtual   java/lang/Runtime.exec:(Ljava/lang/String;)Ljava/lang/Process;
        //    19: astore_0       
        //    20: aload_0        
        //    21: astore_1       
        //    22: aload_0        
        //    23: astore_2       
        //    24: aload_0        
        //    25: invokevirtual   java/lang/Process.getInputStream:()Ljava/io/InputStream;
        //    28: astore          4
        //    30: aload_0        
        //    31: astore_1       
        //    32: aload_0        
        //    33: astore_2       
        //    34: new             Ljava/io/BufferedReader;
        //    37: dup            
        //    38: new             Ljava/io/InputStreamReader;
        //    41: dup            
        //    42: aload           4
        //    44: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    47: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    50: astore          5
        //    52: aload           5
        //    54: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    57: astore_1       
        //    58: aload_1        
        //    59: ifnonnull       95
        //    62: aload           5
        //    64: invokevirtual   java/io/BufferedReader.close:()V
        //    67: aload_0        
        //    68: astore_1       
        //    69: aload_0        
        //    70: astore_2       
        //    71: aload           4
        //    73: invokevirtual   java/io/InputStream.close:()V
        //    76: aload_0        
        //    77: astore_1       
        //    78: aload_0        
        //    79: astore_2       
        //    80: aload_0        
        //    81: invokevirtual   java/lang/Process.waitFor:()I
        //    84: pop            
        //    85: aload_0        
        //    86: ifnull          93
        //    89: aload_0        
        //    90: invokevirtual   java/lang/Process.destroy:()V
        //    93: aload_3        
        //    94: areturn        
        //    95: aload_3        
        //    96: aload_1        
        //    97: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   100: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   105: pop            
        //   106: goto            52
        //   109: astore_1       
        //   110: aload_1        
        //   111: invokevirtual   java/io/IOException.printStackTrace:()V
        //   114: aload_0        
        //   115: astore_1       
        //   116: aload_0        
        //   117: astore_2       
        //   118: aload           4
        //   120: invokevirtual   java/io/InputStream.close:()V
        //   123: goto            76
        //   126: astore          4
        //   128: aload_0        
        //   129: astore_1       
        //   130: aload_0        
        //   131: astore_2       
        //   132: aload           4
        //   134: invokevirtual   java/io/IOException.printStackTrace:()V
        //   137: goto            76
        //   140: astore_0       
        //   141: aload_1        
        //   142: astore_2       
        //   143: aload_0        
        //   144: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   147: aload_1        
        //   148: astore_2       
        //   149: aload_1        
        //   150: invokevirtual   java/lang/Process.getErrorStream:()Ljava/io/InputStream;
        //   153: invokevirtual   java/io/InputStream.close:()V
        //   156: aload_1        
        //   157: astore_2       
        //   158: aload_1        
        //   159: invokevirtual   java/lang/Process.getInputStream:()Ljava/io/InputStream;
        //   162: invokevirtual   java/io/InputStream.close:()V
        //   165: aload_1        
        //   166: astore_2       
        //   167: aload_1        
        //   168: invokevirtual   java/lang/Process.getOutputStream:()Ljava/io/OutputStream;
        //   171: invokevirtual   java/io/OutputStream.close:()V
        //   174: aload_1        
        //   175: ifnull          93
        //   178: aload_1        
        //   179: invokevirtual   java/lang/Process.destroy:()V
        //   182: aload_3        
        //   183: areturn        
        //   184: astore          5
        //   186: aload_0        
        //   187: astore_1       
        //   188: aload_0        
        //   189: astore_2       
        //   190: aload           4
        //   192: invokevirtual   java/io/InputStream.close:()V
        //   195: aload_0        
        //   196: astore_1       
        //   197: aload_0        
        //   198: astore_2       
        //   199: aload           5
        //   201: athrow         
        //   202: astore_0       
        //   203: aload_2        
        //   204: ifnull          211
        //   207: aload_2        
        //   208: invokevirtual   java/lang/Process.destroy:()V
        //   211: aload_0        
        //   212: athrow         
        //   213: astore          4
        //   215: aload_0        
        //   216: astore_1       
        //   217: aload_0        
        //   218: astore_2       
        //   219: aload           4
        //   221: invokevirtual   java/io/IOException.printStackTrace:()V
        //   224: goto            195
        //   227: astore          4
        //   229: aload_0        
        //   230: astore_1       
        //   231: aload_0        
        //   232: astore_2       
        //   233: aload           4
        //   235: invokevirtual   java/io/IOException.printStackTrace:()V
        //   238: goto            76
        //   241: astore_2       
        //   242: aload_1        
        //   243: astore_2       
        //   244: aload_0        
        //   245: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   248: goto            174
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  12     20     140    251    Ljava/lang/Exception;
        //  12     20     202    213    Any
        //  24     30     140    251    Ljava/lang/Exception;
        //  24     30     202    213    Any
        //  34     52     140    251    Ljava/lang/Exception;
        //  34     52     202    213    Any
        //  52     58     109    140    Ljava/io/IOException;
        //  52     58     184    227    Any
        //  62     67     109    140    Ljava/io/IOException;
        //  62     67     184    227    Any
        //  71     76     227    241    Ljava/io/IOException;
        //  71     76     140    251    Ljava/lang/Exception;
        //  71     76     202    213    Any
        //  80     85     140    251    Ljava/lang/Exception;
        //  80     85     202    213    Any
        //  95     106    109    140    Ljava/io/IOException;
        //  95     106    184    227    Any
        //  110    114    184    227    Any
        //  118    123    126    140    Ljava/io/IOException;
        //  118    123    140    251    Ljava/lang/Exception;
        //  118    123    202    213    Any
        //  132    137    140    251    Ljava/lang/Exception;
        //  132    137    202    213    Any
        //  143    147    202    213    Any
        //  149    156    241    251    Ljava/io/IOException;
        //  149    156    202    213    Any
        //  158    165    241    251    Ljava/io/IOException;
        //  158    165    202    213    Any
        //  167    174    241    251    Ljava/io/IOException;
        //  167    174    202    213    Any
        //  190    195    213    227    Ljava/io/IOException;
        //  190    195    140    251    Ljava/lang/Exception;
        //  190    195    202    213    Any
        //  199    202    140    251    Ljava/lang/Exception;
        //  199    202    202    213    Any
        //  219    224    140    251    Ljava/lang/Exception;
        //  219    224    202    213    Any
        //  233    238    140    251    Ljava/lang/Exception;
        //  233    238    202    213    Any
        //  244    248    202    213    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 149, Size: 149
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
