package cn.kuwo.autosdk;

enum e
{
    a("NOTIFY_START", 0), 
    b("NOTIFY_FAILED", 1), 
    c("NOTIFY_FINISH", 2);
    
    private static final /* synthetic */ e[] d;
    
    static {
        d = new e[] { e.a, e.b, e.c };
    }
    
    private e(final String s, final int n) {
    }
    
    public static e[] a() {
        final e[] d = e.d;
        final int length = d.length;
        final e[] array = new e[length];
        System.arraycopy(d, 0, array, 0, length);
        return array;
    }
}
