package cn.kuwo.autosdk;

public final class m
{
    private static char[] a;
    private static byte[] b;
    
    static {
        final int n = 0;
        m.a = new char[64];
        char c;
        int n2;
        for (c = 'A', n2 = 0; c <= 'Z'; ++c, ++n2) {
            m.a[n2] = c;
        }
        for (char c2 = 'a'; c2 <= 'z'; ++c2, ++n2) {
            m.a[n2] = c2;
        }
        for (char c3 = '0'; c3 <= '9'; ++c3, ++n2) {
            m.a[n2] = c3;
        }
        final char[] a = m.a;
        final int n3 = n2 + 1;
        a[n2] = '+';
        m.a[n3] = '/';
        m.b = new byte[128];
        for (int i = 0; i < m.b.length; ++i) {
            m.b[i] = -1;
        }
        for (int j = n; j < 64; ++j) {
            m.b[m.a[j]] = (byte)j;
        }
    }
    
    public static char[] a(final byte[] array, final int n) {
        return a(array, n, null);
    }
    
    public static char[] a(final byte[] array, final int n, final String s) {
        if (s != null && !s.equals("")) {
            final byte[] bytes = s.getBytes();
            int i = 0;
            while (i < array.length) {
                int n2 = 0;
                int n3 = i;
                while (true) {
                    i = n3;
                    if (n2 >= bytes.length || (i = n3) >= array.length) {
                        break;
                    }
                    array[n3] ^= bytes[n2];
                    ++n2;
                    ++n3;
                }
            }
        }
        final int n4 = (n * 4 + 2) / 3;
        final char[] array2 = new char[(n + 2) / 3 * 4];
        int n5 = 0;
        int j = 0;
        while (j < n) {
            final int n6 = j + 1;
            final int n7 = array[j] & 0xFF;
            int n10;
            if (n6 < n) {
                final int n8 = array[n6] & 0xFF;
                final int n9 = n6 + 1;
                n10 = n8;
                j = n9;
            }
            else {
                final int n11 = 0;
                j = n6;
                n10 = n11;
            }
            int n14;
            if (j < n) {
                final int n12 = j + 1;
                final int n13 = array[j] & 0xFF;
                j = n12;
                n14 = n13;
            }
            else {
                n14 = 0;
            }
            final int n15 = n5 + 1;
            array2[n5] = m.a[n7 >>> 2];
            final int n16 = n15 + 1;
            array2[n15] = m.a[(n7 & 0x3) << 4 | n10 >>> 4];
            char c;
            if (n16 < n4) {
                c = m.a[(n10 & 0xF) << 2 | n14 >>> 6];
            }
            else {
                c = '=';
            }
            array2[n16] = c;
            final int n17 = n16 + 1;
            char c2;
            if (n17 < n4) {
                c2 = m.a[n14 & 0x3F];
            }
            else {
                c2 = '=';
            }
            array2[n17] = c2;
            n5 = n17 + 1;
        }
        return array2;
    }
}
