package cn.kuwo.autosdk;

import android.os.*;

public class v implements Parcelable
{
    public static final Parcelable$Creator f;
    public final String a;
    public final int b;
    public final int c;
    public final int d;
    public final String e;
    
    static {
        f = (Parcelable$Creator)new w();
    }
    
    private v(final Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readString();
    }
    
    private v(final String s) {
        final String[] split = s.split("\\s+");
        this.a = split[0];
        this.b = Process.getUidForName(this.a);
        this.c = Integer.parseInt(split[1]);
        this.d = Integer.parseInt(split[2]);
        if (split.length == 16) {
            this.e = split[13];
            return;
        }
        this.e = split[14];
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
    }
}
