package cn.kuwo.autosdk;

public final class p
{
    public static final byte[] a;
    public static final int b;
    private static final long[] c;
    private static final int[] d;
    private static final int[] e;
    private static final char[][] f;
    private static final int[] g;
    private static final int[] h;
    private static final int[] i;
    private static final int[] j;
    private static final int[] k;
    private static final long[] l;
    private static long m;
    private static long n;
    private static long o;
    private static int[] p;
    private static byte[] q;
    private static int r;
    private static int s;
    private static int t;
    
    static {
        a = "ylzsxkwm".getBytes();
        b = cn.kuwo.autosdk.p.a.length;
        c = new long[] { 1L, 2L, 4L, 8L, 16L, 32L, 64L, 128L, 256L, 512L, 1024L, 2048L, 4096L, 8192L, 16384L, 32768L, 65536L, 131072L, 262144L, 524288L, 1048576L, 2097152L, 4194304L, 8388608L, 16777216L, 33554432L, 67108864L, 134217728L, 268435456L, 536870912L, 1073741824L, 2147483648L, 4294967296L, 8589934592L, 17179869184L, 34359738368L, 68719476736L, 137438953472L, 274877906944L, 549755813888L, 1099511627776L, 2199023255552L, 4398046511104L, 8796093022208L, 17592186044416L, 35184372088832L, 70368744177664L, 140737488355328L, 281474976710656L, 562949953421312L, 1125899906842624L, 2251799813685248L, 4503599627370496L, 9007199254740992L, 18014398509481984L, 36028797018963968L, 72057594037927936L, 144115188075855872L, 288230376151711744L, 576460752303423488L, 1152921504606846976L, 2305843009213693952L, 4611686018427387904L, Long.MIN_VALUE };
        d = new int[] { 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7, 56, 48, 40, 32, 24, 16, 8, 0, 58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6 };
        e = new int[] { 31, 0, 1, 2, 3, 4, -1, -1, 3, 4, 5, 6, 7, 8, -1, -1, 7, 8, 9, 10, 11, 12, -1, -1, 11, 12, 13, 14, 15, 16, -1, -1, 15, 16, 17, 18, 19, 20, -1, -1, 19, 20, 21, 22, 23, 24, -1, -1, 23, 24, 25, 26, 27, 28, -1, -1, 27, 28, 29, 30, 31, 30, -1, -1 };
        f = new char[][] { { '\u000e', '\u0004', '\u0003', '\u000f', '\u0002', '\r', '\u0005', '\u0003', '\r', '\u000e', '\u0006', '\t', '\u000b', '\u0002', '\0', '\u0005', '\u0004', '\u0001', '\n', '\f', '\u000f', '\u0006', '\t', '\n', '\u0001', '\b', '\f', '\u0007', '\b', '\u000b', '\u0007', '\0', '\0', '\u000f', '\n', '\u0005', '\u000e', '\u0004', '\t', '\n', '\u0007', '\b', '\f', '\u0003', '\r', '\u0001', '\u0003', '\u0006', '\u000f', '\f', '\u0006', '\u000b', '\u0002', '\t', '\u0005', '\0', '\u0004', '\u0002', '\u000b', '\u000e', '\u0001', '\u0007', '\b', '\r' }, { '\u000f', '\0', '\t', '\u0005', '\u0006', '\n', '\f', '\t', '\b', '\u0007', '\u0002', '\f', '\u0003', '\r', '\u0005', '\u0002', '\u0001', '\u000e', '\u0007', '\b', '\u000b', '\u0004', '\0', '\u0003', '\u000e', '\u000b', '\r', '\u0006', '\u0004', '\u0001', '\n', '\u000f', '\u0003', '\r', '\f', '\u000b', '\u000f', '\u0003', '\u0006', '\0', '\u0004', '\n', '\u0001', '\u0007', '\b', '\u0004', '\u000b', '\u000e', '\r', '\b', '\0', '\u0006', '\u0002', '\u000f', '\t', '\u0005', '\u0007', '\u0001', '\n', '\f', '\u000e', '\u0002', '\u0005', '\t' }, { '\n', '\r', '\u0001', '\u000b', '\u0006', '\b', '\u000b', '\u0005', '\t', '\u0004', '\f', '\u0002', '\u000f', '\u0003', '\u0002', '\u000e', '\0', '\u0006', '\r', '\u0001', '\u0003', '\u000f', '\u0004', '\n', '\u000e', '\t', '\u0007', '\f', '\u0005', '\0', '\b', '\u0007', '\r', '\u0001', '\u0002', '\u0004', '\u0003', '\u0006', '\f', '\u000b', '\0', '\r', '\u0005', '\u000e', '\u0006', '\b', '\u000f', '\u0002', '\u0007', '\n', '\b', '\u000f', '\u0004', '\t', '\u000b', '\u0005', '\t', '\0', '\u000e', '\u0003', '\n', '\u0007', '\u0001', '\f' }, { '\u0007', '\n', '\u0001', '\u000f', '\0', '\f', '\u000b', '\u0005', '\u000e', '\t', '\b', '\u0003', '\t', '\u0007', '\u0004', '\b', '\r', '\u0006', '\u0002', '\u0001', '\u0006', '\u000b', '\f', '\u0002', '\u0003', '\0', '\u0005', '\u000e', '\n', '\r', '\u000f', '\u0004', '\r', '\u0003', '\u0004', '\t', '\u0006', '\n', '\u0001', '\f', '\u000b', '\0', '\u0002', '\u0005', '\0', '\r', '\u000e', '\u0002', '\b', '\u000f', '\u0007', '\u0004', '\u000f', '\u0001', '\n', '\u0007', '\u0005', '\u0006', '\f', '\u000b', '\u0003', '\b', '\t', '\u000e' }, { '\u0002', '\u0004', '\b', '\u000f', '\u0007', '\n', '\r', '\u0006', '\u0004', '\u0001', '\u0003', '\f', '\u000b', '\u0007', '\u000e', '\0', '\f', '\u0002', '\u0005', '\t', '\n', '\r', '\0', '\u0003', '\u0001', '\u000b', '\u000f', '\u0005', '\u0006', '\b', '\t', '\u000e', '\u000e', '\u000b', '\u0005', '\u0006', '\u0004', '\u0001', '\u0003', '\n', '\u0002', '\f', '\u000f', '\0', '\r', '\u0002', '\b', '\u0005', '\u000b', '\b', '\0', '\u000f', '\u0007', '\u000e', '\t', '\u0004', '\f', '\u0007', '\n', '\t', '\u0001', '\r', '\u0006', '\u0003' }, { '\f', '\t', '\0', '\u0007', '\t', '\u0002', '\u000e', '\u0001', '\n', '\u000f', '\u0003', '\u0004', '\u0006', '\f', '\u0005', '\u000b', '\u0001', '\u000e', '\r', '\0', '\u0002', '\b', '\u0007', '\r', '\u000f', '\u0005', '\u0004', '\n', '\b', '\u0003', '\u000b', '\u0006', '\n', '\u0004', '\u0006', '\u000b', '\u0007', '\t', '\0', '\u0006', '\u0004', '\u0002', '\r', '\u0001', '\t', '\u000f', '\u0003', '\b', '\u000f', '\u0003', '\u0001', '\u000e', '\f', '\u0005', '\u000b', '\0', '\u0002', '\f', '\u000e', '\u0007', '\u0005', '\n', '\b', '\r' }, { '\u0004', '\u0001', '\u0003', '\n', '\u000f', '\f', '\u0005', '\0', '\u0002', '\u000b', '\t', '\u0006', '\b', '\u0007', '\u0006', '\t', '\u000b', '\u0004', '\f', '\u000f', '\0', '\u0003', '\n', '\u0005', '\u000e', '\r', '\u0007', '\b', '\r', '\u000e', '\u0001', '\u0002', '\r', '\u0006', '\u000e', '\t', '\u0004', '\u0001', '\u0002', '\u000e', '\u000b', '\r', '\u0005', '\0', '\u0001', '\n', '\b', '\u0003', '\0', '\u000b', '\u0003', '\u0005', '\t', '\u0004', '\u000f', '\u0002', '\u0007', '\b', '\f', '\u000f', '\n', '\u0007', '\u0006', '\f' }, { '\r', '\u0007', '\n', '\0', '\u0006', '\t', '\u0005', '\u000f', '\b', '\u0004', '\u0003', '\n', '\u000b', '\u000e', '\f', '\u0005', '\u0002', '\u000b', '\t', '\u0006', '\u000f', '\f', '\0', '\u0003', '\u0004', '\u0001', '\u000e', '\r', '\u0001', '\u0002', '\u0007', '\b', '\u0001', '\u0002', '\f', '\u000f', '\n', '\u0004', '\0', '\u0003', '\r', '\u000e', '\u0006', '\t', '\u0007', '\b', '\t', '\u0006', '\u000f', '\u0001', '\u0005', '\f', '\u0003', '\n', '\u000e', '\u0005', '\b', '\u0007', '\u000b', '\0', '\u0004', '\r', '\u0002', '\u000b' } };
        g = new int[] { 15, 6, 19, 20, 28, 11, 27, 16, 0, 14, 22, 25, 4, 17, 30, 9, 1, 7, 23, 13, 31, 26, 2, 8, 18, 12, 29, 5, 21, 10, 3, 24 };
        h = new int[] { 39, 7, 47, 15, 55, 23, 63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25, 32, 0, 40, 8, 48, 16, 56, 24 };
        i = new int[] { 56, 48, 40, 32, 24, 16, 8, 0, 57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 60, 52, 44, 36, 28, 20, 12, 4, 27, 19, 11, 3 };
        j = new int[] { 13, 16, 10, 23, 0, 4, -1, -1, 2, 27, 14, 5, 20, 9, -1, -1, 22, 18, 11, 3, 25, 7, -1, -1, 15, 6, 26, 19, 12, 1, -1, -1, 40, 51, 30, 36, 46, 54, -1, -1, 29, 39, 50, 44, 32, 47, -1, -1, 43, 48, 38, 55, 33, 52, -1, -1, 45, 41, 49, 35, 28, 31, -1, -1 };
        k = new int[] { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };
        l = new long[] { 0L, 1048577L, 3145731L };
        cn.kuwo.autosdk.p.m = 0L;
        cn.kuwo.autosdk.p.p = new int[2];
        cn.kuwo.autosdk.p.q = new byte[8];
    }
    
    private static long a(final int[] array, final int n, final long n2) {
        int i = 0;
        long n3 = 0L;
        while (i < n) {
            long n4 = n3;
            if (array[i] >= 0) {
                n4 = n3;
                if ((cn.kuwo.autosdk.p.c[array[i]] & n2) != 0x0L) {
                    n4 = (n3 | cn.kuwo.autosdk.p.c[i]);
                }
            }
            ++i;
            n3 = n4;
        }
        return n3;
    }
    
    private static long a(final long[] array, final long n) {
        cn.kuwo.autosdk.p.m = a(cn.kuwo.autosdk.p.d, 64, n);
        cn.kuwo.autosdk.p.p[0] = (int)(cn.kuwo.autosdk.p.m & 0xFFFFFFFFL);
        cn.kuwo.autosdk.p.p[1] = (int)((cn.kuwo.autosdk.p.m & 0xFFFFFFFF00000000L) >> 32);
        for (int i = 0; i < 16; ++i) {
            cn.kuwo.autosdk.p.o = cn.kuwo.autosdk.p.p[1];
            cn.kuwo.autosdk.p.o = a(cn.kuwo.autosdk.p.e, 64, cn.kuwo.autosdk.p.o);
            cn.kuwo.autosdk.p.o ^= array[i];
            for (int j = 0; j < 8; ++j) {
                cn.kuwo.autosdk.p.q[j] = (byte)(0xFFL & cn.kuwo.autosdk.p.o >> j * 8);
            }
            cn.kuwo.autosdk.p.r = 0;
            cn.kuwo.autosdk.p.t = 7;
            while (cn.kuwo.autosdk.p.t >= 0) {
                cn.kuwo.autosdk.p.r <<= 4;
                cn.kuwo.autosdk.p.r |= cn.kuwo.autosdk.p.f[cn.kuwo.autosdk.p.t][cn.kuwo.autosdk.p.q[cn.kuwo.autosdk.p.t]];
                --cn.kuwo.autosdk.p.t;
            }
            cn.kuwo.autosdk.p.o = cn.kuwo.autosdk.p.r;
            cn.kuwo.autosdk.p.o = a(cn.kuwo.autosdk.p.g, 32, cn.kuwo.autosdk.p.o);
            cn.kuwo.autosdk.p.n = cn.kuwo.autosdk.p.p[0];
            cn.kuwo.autosdk.p.p[0] = cn.kuwo.autosdk.p.p[1];
            cn.kuwo.autosdk.p.p[1] = (int)(cn.kuwo.autosdk.p.n ^ cn.kuwo.autosdk.p.o);
        }
        cn.kuwo.autosdk.p.s = cn.kuwo.autosdk.p.p[0];
        cn.kuwo.autosdk.p.p[0] = cn.kuwo.autosdk.p.p[1];
        cn.kuwo.autosdk.p.p[1] = cn.kuwo.autosdk.p.s;
        cn.kuwo.autosdk.p.m = ((cn.kuwo.autosdk.p.p[1] << 32 & 0xFFFFFFFF00000000L) | (0xFFFFFFFFL & cn.kuwo.autosdk.p.p[0]));
        return cn.kuwo.autosdk.p.m = a(cn.kuwo.autosdk.p.h, 64, cn.kuwo.autosdk.p.m);
    }
    
    private static void a(long a, final long[] array, int i) {
        final int n = 0;
        a = a(cn.kuwo.autosdk.p.i, 56, a);
        for (int j = 0; j < 16; ++j) {
            a = ((a & ~cn.kuwo.autosdk.p.l[cn.kuwo.autosdk.p.k[j]]) >> cn.kuwo.autosdk.p.k[j] | (cn.kuwo.autosdk.p.l[cn.kuwo.autosdk.p.k[j]] & a) << 28 - cn.kuwo.autosdk.p.k[j]);
            array[j] = a(cn.kuwo.autosdk.p.j, 64, a);
        }
        if (i == 1) {
            for (i = n; i < 8; ++i) {
                a = array[i];
                array[i] = array[15 - i];
                array[15 - i] = a;
            }
        }
    }
    
    public static byte[] a(byte[] o, int n, byte[] array, int i) {
        // monitorenter(p.class)
        long n2 = 0L;
        i = 0;
        while (true) {
            Label_0160: {
                if (i < 8) {
                    break Label_0160;
                }
                int n3;
                long[] array2 = null;
                long[] array3 = null;
                int j = 0;
                Label_0042_Outer:Label_0114_Outer:Label_0145_Outer:Label_0200_Outer:
                while (true) {
                    while (true) {
                        while (true) {
                            Label_0325: {
                            Label_0322:
                                while (true) {
                                Label_0300:
                                    while (true) {
                                    Label_0249:
                                        while (true) {
                                            Label_0237: {
                                                try {
                                                    n3 = n / 8;
                                                    array2 = new long[16];
                                                    i = 0;
                                                    if (i < 16) {
                                                        break Label_0237;
                                                    }
                                                    array3 = new long[n3];
                                                    i = 0;
                                                    if (i < n3) {
                                                        break Label_0249;
                                                    }
                                                    array = (byte[])new long[((n3 + 1) * 8 + 1) / 8];
                                                    a(n2, array2, 0);
                                                    for (i = 0; i < n3; ++i) {
                                                        array[i] = (byte)a(array2, array3[i]);
                                                    }
                                                    array3 = (long[])new byte[n - n3 * 8];
                                                    System.arraycopy(o, n3 * 8, array3, 0, n - n3 * 8);
                                                    n2 = 0L;
                                                    i = 0;
                                                    if (i < n % 8) {
                                                        break Label_0300;
                                                    }
                                                    array[n3] = (byte)a(array2, n2);
                                                    o = new byte[array.length * 8];
                                                    i = 0;
                                                    n = 0;
                                                    j = array.length;
                                                    if (n >= j) {
                                                        return (byte[])o;
                                                    }
                                                    break Label_0322;
                                                    n2 |= array[i] << i * 8;
                                                    ++i;
                                                    break;
                                                    o[i] = (byte)(0xFFL & array[n] >> j * 8);
                                                    ++i;
                                                    ++j;
                                                    break Label_0325;
                                                }
                                                finally {
                                                }
                                                // monitorexit(p.class)
                                            }
                                            array2[i] = 0L;
                                            ++i;
                                            continue Label_0042_Outer;
                                        }
                                        for (j = 0; j < 8; ++j) {
                                            array3[i] |= (long)o[i * 8 + j] << j * 8;
                                        }
                                        ++i;
                                        continue Label_0114_Outer;
                                    }
                                    n2 |= array3[i] << i * 8;
                                    ++i;
                                    continue Label_0145_Outer;
                                }
                                j = 0;
                            }
                            if (j >= 8) {
                                ++n;
                                continue Label_0200_Outer;
                            }
                            break;
                        }
                        continue;
                    }
                }
            }
        }
    }
}
