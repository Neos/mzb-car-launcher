package cn.kuwo.autosdk;

public final class r
{
    private static volatile int a;
    private static t[] b;
    
    static {
        r.a = 0;
        r.b = new t[5];
    }
    
    public static void a(final s s, final Runnable runnable) {
        if (s == s.b) {}
        c().a(runnable, 0);
    }
    
    private static t c() {
        if (r.a == 0) {
            return new t((t)null);
        }
        synchronized (r.b) {
            if (r.a == 0) {
                return new t((t)null);
            }
        }
        --r.a;
        final t t = r.b[r.a];
        r.b[r.a] = null;
        // monitorexit(array)
        return t;
    }
}
