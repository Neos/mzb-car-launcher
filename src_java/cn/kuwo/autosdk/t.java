package cn.kuwo.autosdk;

import android.os.*;

final class t extends Thread
{
    private volatile Runnable a;
    private volatile int b;
    private volatile boolean c;
    
    public void a(final Runnable a, final int b) {
        this.a = a;
        this.b = b;
        if (!this.c) {
            this.start();
            this.c = true;
            return;
        }
        synchronized (this) {
            this.notify();
        }
    }
    
    @Override
    public void run() {
        while (true) {
            Process.setThreadPriority(this.b);
            this.a.run();
            if (r.a >= 5) {
                break;
            }
            // monitorenter(this)
            Label_0056: {
                t[] b;
                try {
                    b = r.b;
                    // monitorenter(b)
                    final int n = r.a;
                    final int n2 = 5;
                    if (n >= n2) {
                        final t[] array = b;
                        // monitorexit(array)
                        break;
                    }
                    break Label_0056;
                }
                finally {
                    final t[] array2;
                    b = array2;
                }
                // monitorexit(this)
                try {
                    final int n = r.a;
                    final int n2 = 5;
                    if (n >= n2) {
                        final t[] array = b;
                        // monitorexit(array)
                        break;
                    }
                    r.b[r.a] = this;
                    r.a(r.a + 1);
                    // monitorexit(b)
                    final t t = this;
                    t.wait();
                    continue;
                }
                finally {
                }
                // monitorexit(b)
            }
            try {
                final t t = this;
                t.wait();
            }
            catch (InterruptedException ex) {
                // monitorexit(this)
                break;
            }
        }
        this.c = false;
    }
}
