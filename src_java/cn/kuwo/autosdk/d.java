package cn.kuwo.autosdk;

class d implements Runnable
{
    int a;
    int b;
    byte[] c;
    int d;
    final /* synthetic */ b e;
    
    d(final b e) {
        this.e = e;
    }
    
    public d a(final int a, final int b, final byte[] c, final int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        return this;
    }
    
    @Override
    public void run() {
        if (this.e.p) {
            return;
        }
        synchronized (this.e) {
            this.e.x.a(this.e, this.a, this.b, this.c, this.d);
            // monitorexit(this.e)
            this.c = null;
        }
    }
}
