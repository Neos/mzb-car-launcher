package cn.kuwo.autosdk;

import java.util.*;
import java.text.*;

public class q extends Date
{
    public q() {
    }
    
    public q(final String s) {
        this.b(s);
    }
    
    public final String a() {
        return this.a("yyyy-MM-dd HH:mm:ss");
    }
    
    public final String a(final String s) {
        return new SimpleDateFormat(s, Locale.CHINA).format(this);
    }
    
    public final boolean a(final String s, final String s2) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(s2, Locale.CHINA);
        try {
            this.setTime(simpleDateFormat.parse(s).getTime());
            return true;
        }
        catch (ParseException ex) {
            ex.printStackTrace();
            System.out.println(s);
            return false;
        }
    }
    
    public final boolean b(final String s) {
        String s2 = "yyyy-MM-dd";
        if (s.length() > 10) {
            s2 = "yyyy-MM-dd HH:mm:ss";
        }
        return this.a(s, s2);
    }
}
