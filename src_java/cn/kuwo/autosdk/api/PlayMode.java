package cn.kuwo.autosdk.api;

public enum PlayMode
{
    private static final String MEDIA_CIRCLE = "MEDIA_CIRCLE";
    private static final String MEDIA_ONE = "MEDIA_ONE";
    private static final String MEDIA_ORDER = "MEDIA_ORDER";
    private static final String MEDIA_RANDOM = "MEDIA_RANDOM";
    private static final String MEDIA_SINGLE = "MEDIA_SINGLE";
    
    MODE_ALL_CIRCLE("MODE_ALL_CIRCLE", 3), 
    MODE_ALL_ORDER("MODE_ALL_ORDER", 2), 
    MODE_ALL_RANDOM("MODE_ALL_RANDOM", 4), 
    MODE_SINGLE_CIRCLE("MODE_SINGLE_CIRCLE", 0), 
    MODE_SINGLE_PLAY("MODE_SINGLE_PLAY", 1);
    
    private PlayMode(final String s, final int n) {
    }
    
    public abstract String getPlayMode();
}
