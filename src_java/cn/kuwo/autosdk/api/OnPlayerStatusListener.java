package cn.kuwo.autosdk.api;

import cn.kuwo.autosdk.bean.*;

public interface OnPlayerStatusListener
{
    void onPlayerStatus(final PlayerStatus p0, final Music p1);
}
