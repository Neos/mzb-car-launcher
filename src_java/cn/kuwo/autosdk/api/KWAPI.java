package cn.kuwo.autosdk.api;

import android.text.*;
import android.content.pm.*;
import cn.kuwo.autosdk.bean.*;
import org.json.*;
import android.app.*;
import cn.kuwo.autosdk.*;
import java.util.*;
import android.os.*;
import android.widget.*;
import android.content.*;

public class KWAPI
{
    private static final String AUTO_PLAY = "auto_play";
    private static final String CLOSE_DESKLYRIC = "cn.kuwo.kwmusicauto.action.CLOSE_DESKLYRIC";
    private static final String CLOSE_TOAST = "cn.kuwo.kwmusicauto.action.CLOSE_TOAST";
    private static final String ENDTYPE = "ENDTYPE";
    private static final String EXIT = "cn.kuwo.kwmusicauto.action.EXIT";
    private static final String EXIT_KWMUSICAPP = "cn.kuwo.kwmusicauto.action.EXITAPP";
    private static final String EXIT_PLAY_MUSIC = "exit_play_music";
    private static final String EXTRA = "EXTRA";
    private static final String HAS_MV = "hasMv";
    private static final String KEY_FULL_SCREEN = "key_full_screen";
    private static final String KUWO_CARMUSIC_MEDIABUTTON_ACTION = "cn.kuwo.kwmusicauto.action.MEDIA_BUTTON";
    private static final String KUWO_KEY = "kuwo_key";
    private static final String KW_CAR_PROCESS_NAME = "cn.kuwo.kwmusiccar";
    private static final String MUSIC = "music";
    private static final String MUSIC_ALBUM = "album";
    private static final String MUSIC_ALL_LIST_KEY = "music_all_list_key";
    private static final String MUSIC_ARTISTID = "artistid";
    private static final String MUSIC_INDEX_KEY = "music_index_key";
    private static final String MUSIC_NAME = "name";
    private static final String MUSIC_RID = "rid";
    private static final String MUSIC_SINGER = "singer";
    private static final String MUSIC_SOURCE = "source";
    private static final String MV_QUALITY = "mvQuality";
    private static final String NoFoundAPP = "\u672a\u627e\u5230\u53ef\u7528\u7684\u5b89\u88c5\u7a0b\u5e8f\uff01";
    private static final String OPEN_DESKLYRIC = "cn.kuwo.kwmusicauto.action.OPEN_DESKLYRIC";
    private static final String OPEN_TOAST = "cn.kuwo.kwmusicauto.action.OPEN_TOAST";
    private static final String PALY_MUSIC = "cn.kuwo.kwmusicauto.action.PLAY_MUSIC";
    private static final String PLAYERMODE = "PLAYERMODE";
    private static final String PLAYERSTATUS = "PLAYERSTATUS";
    private static final String PLAYER_MODE = "cn.kuwo.kwmusicauto.action.PLAYER_MODE";
    private static final String PLAYER_STATUS = "cn.kuwo.kwmusicauto.action.PLAYER_STATUS";
    private static final String PLAYMUSIC_ALBUM = "play_music_album";
    private static final String PLAYMUSIC_ARTIST = "play_music_artist";
    private static final String PLAYMUSIC_NAME = "play_music_name";
    private static final String PLAY_ALL_MUSIC = "cn.kuwo.kwmusicauto.action.PLAY_ALL_MUSIC";
    private static final String PLAY_END = "cn.kuwo.kwmusicauto.action.PLAY_END";
    private static final String SEARCH_MUSIC = "cn.kuwo.kwmusicauto.action.SEARCH_MUSIC";
    private static final String SET_FULL_SCREEN = "cn.kuwo.kwmusicauto.action.SET_FULL_SCREEN";
    private static final String START_KWMUSICAPP = "cn.kuwo.kwmusicauto.action.STARTAPP";
    private static final KWAPI$CarExitReceiver mCarExitReceiver;
    private static final KWAPI$CarPlayerModeReceiver mCarPlayerModeReceiver;
    private static String mKey;
    private static KWAPI mKwapi;
    private static final KWAPI$CarPlayEndReceiver mPlayEndReceiver;
    private static final KWAPI$CarPlayerStatusReceiver mPlayerStatusReceiver;
    private static g mSearchMgr;
    private OnExitListener mExitListener;
    private boolean mExitRegistered;
    private OnPlayEndListener mPlayEndListener;
    private boolean mPlayEndRegistered;
    private OnPlayerModeListener mPlayerModeListener;
    private boolean mPlayerModeRegistered;
    private OnPlayerStatusListener mPlayerStatusListener;
    private boolean mPlayerStatusRegistered;
    
    static {
        KWAPI.mKwapi = null;
        KWAPI.mKey = "";
        KWAPI.mSearchMgr = null;
        mPlayEndReceiver = new KWAPI$CarPlayEndReceiver(null);
        mCarPlayerModeReceiver = new KWAPI$CarPlayerModeReceiver(null);
        mCarExitReceiver = new KWAPI$CarExitReceiver(null);
        mPlayerStatusReceiver = new KWAPI$CarPlayerStatusReceiver(null);
    }
    
    private KWAPI() {
        this.mPlayEndListener = null;
        this.mPlayEndRegistered = false;
        this.mPlayerModeListener = null;
        this.mExitListener = null;
        this.mPlayerStatusListener = null;
        this.mPlayerStatusRegistered = false;
        this.mPlayerModeRegistered = false;
        this.mExitRegistered = false;
    }
    
    private boolean checkInstalledPackage(final Context context, final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            while (true) {
                try {
                    final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(s, 0);
                    if (packageInfo != null) {
                        return true;
                    }
                }
                catch (PackageManager$NameNotFoundException ex) {
                    final PackageInfo packageInfo = null;
                    continue;
                }
                break;
            }
        }
        return false;
    }
    
    public static KWAPI createKWAPI(final Context context, final String mKey) {
        if (KWAPI.mKwapi == null || TextUtils.isEmpty((CharSequence)KWAPI.mKey)) {
            n.a(context, mKey);
            KWAPI.mKwapi = new KWAPI();
            KWAPI.mKey = mKey;
        }
        return KWAPI.mKwapi;
    }
    
    private OnPlayEndListener getOnPlayEndListener() {
        return this.mPlayEndListener;
    }
    
    private OnPlayerStatusListener getOnPlayerStatusListener() {
        return this.mPlayerStatusListener;
    }
    
    private String packJson(final List list) {
        final JSONArray jsonArray = new JSONArray();
        int i = 0;
        while (i < list.size()) {
            final JSONObject jsonObject = new JSONObject();
            while (true) {
                try {
                    jsonObject.put("rid", list.get(i).rid);
                    jsonObject.put("name", (Object)list.get(i).name);
                    jsonObject.put("singer", (Object)list.get(i).artist);
                    jsonObject.put("album", (Object)list.get(i).album);
                    jsonObject.put("artistid", list.get(i).artistId);
                    jsonObject.put("mvQuality", (Object)list.get(i).mvQuality);
                    jsonObject.put("hasMv", list.get(i).hasMv);
                    jsonArray.put((Object)jsonObject);
                    ++i;
                }
                catch (JSONException ex) {
                    ex.printStackTrace();
                    continue;
                }
                break;
            }
        }
        return jsonArray.toString();
    }
    
    private void registerExitReceiver(final Context context) {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.setPriority(Integer.MAX_VALUE);
        intentFilter.addAction("cn.kuwo.kwmusicauto.action.EXIT");
        context.registerReceiver((BroadcastReceiver)KWAPI.mCarExitReceiver, intentFilter);
    }
    
    private void registerPlayEndReceiver(final Context context) {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("cn.kuwo.kwmusicauto.action.PLAY_END");
        context.registerReceiver((BroadcastReceiver)KWAPI.mPlayEndReceiver, intentFilter);
    }
    
    private void registerPlayerModeReceiver(final Context context) {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.setPriority(Integer.MAX_VALUE);
        intentFilter.addAction("cn.kuwo.kwmusicauto.action.PLAYER_MODE");
        context.registerReceiver((BroadcastReceiver)KWAPI.mCarPlayerModeReceiver, intentFilter);
    }
    
    private void registerPlayerStatusReceiver(final Context context) {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.setPriority(Integer.MAX_VALUE);
        intentFilter.addAction("cn.kuwo.kwmusicauto.action.PLAYER_STATUS");
        context.registerReceiver((BroadcastReceiver)KWAPI.mPlayerStatusReceiver, intentFilter);
    }
    
    private void searchOnlineMusic(final String s, final SearchMode searchMode, final OnSearchListener onSearchListener) {
        if (KWAPI.mSearchMgr == null) {
            KWAPI.mSearchMgr = new h();
        }
        KWAPI.mSearchMgr.a(s, searchMode, onSearchListener);
    }
    
    private void unRegisterExitReceiver(final Context context) {
        context.unregisterReceiver((BroadcastReceiver)KWAPI.mCarExitReceiver);
    }
    
    private void unRegisterPlayEndReceiver(final Context context) {
        context.unregisterReceiver((BroadcastReceiver)KWAPI.mPlayEndReceiver);
    }
    
    private void unRegisterPlayerModeReceiver(final Context context) {
        context.unregisterReceiver((BroadcastReceiver)KWAPI.mCarPlayerModeReceiver);
    }
    
    private void unRegisterPlayerStatusReceiver(final Context context) {
        context.unregisterReceiver((BroadcastReceiver)KWAPI.mPlayerStatusReceiver);
    }
    
    public void closeToast(final Context context) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.CLOSE_TOAST");
        intent.putExtra("kuwo_key", KWAPI.mKey);
        context.sendBroadcast(intent);
    }
    
    public void exitAPP(final Context context) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.EXITAPP");
        intent.putExtra("kuwo_key", KWAPI.mKey);
        context.sendBroadcast(intent);
    }
    
    public boolean isKuwoRunning(final Context context) {
        if (Build$VERSION.SDK_INT < 22) {
            final List runningAppProcesses = ((ActivityManager)context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses != null && runningAppProcesses.size() != 0) {
                for (int i = 0; i < runningAppProcesses.size(); ++i) {
                    if ("cn.kuwo.kwmusiccar".equals(runningAppProcesses.get(i).processName)) {
                        return true;
                    }
                }
            }
        }
        else {
            final List a = u.a();
            if (a != null) {
                final Iterator<v> iterator = a.iterator();
                while (iterator.hasNext()) {
                    if ("cn.kuwo.kwmusiccar".equals(iterator.next().e)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public void openToast(final Context context) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.OPEN_TOAST");
        intent.putExtra("kuwo_key", KWAPI.mKey);
        context.sendBroadcast(intent);
    }
    
    public void playClientMusics(final Context context, final String s, final String s2, final String s3) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.SEARCH_MUSIC");
        intent.setFlags(268435456);
        final Bundle bundle = new Bundle();
        bundle.putString("name", s);
        bundle.putString("singer", s2);
        bundle.putString("album", s3);
        bundle.putString("kuwo_key", KWAPI.mKey);
        intent.putExtras(bundle);
        try {
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException ex) {
            Toast.makeText(context, (CharSequence)"\u672a\u627e\u5230\u53ef\u7528\u7684\u5b89\u88c5\u7a0b\u5e8f\uff01", 1).show();
        }
    }
    
    public void playLocalMusic(final Context context, final String s) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.SEARCH_MUSIC");
        intent.setFlags(268435456);
        final Bundle bundle = new Bundle();
        bundle.putString("source", s);
        bundle.putString("kuwo_key", KWAPI.mKey);
        intent.putExtras(bundle);
        try {
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException ex) {
            Toast.makeText(context, (CharSequence)"\u672a\u627e\u5230\u53ef\u7528\u7684\u5b89\u88c5\u7a0b\u5e8f\uff01", 1).show();
        }
    }
    
    public void playMusic(final Context context, final List list, final int n, final boolean b, final boolean b2) {
        if (list == null) {
            Toast.makeText(context, (CharSequence)"\u64ad\u653e\u7684\u6b4c\u66f2\u4fe1\u606f\u6709\u8bef\uff01", 1).show();
            return;
        }
        if (n >= list.size()) {
            Toast.makeText(context, (CharSequence)"\u64ad\u653e\u7684\u6b4c\u66f2\u4f4d\u7f6e\u6709\u8bef\uff01", 1).show();
            return;
        }
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.PLAY_ALL_MUSIC");
        intent.setFlags(268435456);
        final Bundle bundle = new Bundle();
        bundle.putString("music_all_list_key", this.packJson(list));
        bundle.putString("kuwo_key", KWAPI.mKey);
        bundle.putInt("music_index_key", n);
        while (true) {
            if (b) {
                try {
                    bundle.putBoolean("exit_play_music", b2);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    return;
                    while (true) {
                        intent.putExtras(bundle);
                        context.sendBroadcast(intent);
                        return;
                        continue;
                    }
                }
                // iftrue(Label_0137:, !this.isKuwoRunning(context))
                catch (ActivityNotFoundException ex) {
                    Toast.makeText(context, (CharSequence)"\u672a\u627e\u5230\u53ef\u7528\u7684\u5b89\u88c5\u7a0b\u5e8f\uff01", 1).show();
                    return;
                }
                Label_0137: {
                    bundle.putBoolean("exit_play_music", true);
                }
                intent.putExtras(bundle);
                context.startActivity(intent);
                return;
            }
            continue;
        }
    }
    
    public void registerExitListener(final Context context, final OnExitListener mExitListener) {
        if (!this.mExitRegistered) {
            this.registerExitReceiver(context);
            this.mExitRegistered = true;
        }
        this.mExitListener = mExitListener;
    }
    
    public void registerPlayEndListener(final Context context, final OnPlayEndListener mPlayEndListener) {
        if (!this.mPlayEndRegistered) {
            this.registerPlayEndReceiver(context);
            this.mPlayEndRegistered = true;
        }
        this.mPlayEndListener = mPlayEndListener;
    }
    
    public void registerPlayerModeListener(final Context context, final OnPlayerModeListener mPlayerModeListener) {
        if (!this.mPlayerModeRegistered) {
            this.registerPlayerModeReceiver(context);
            this.mPlayerModeRegistered = true;
        }
        this.mPlayerModeListener = mPlayerModeListener;
    }
    
    public void registerPlayerStatusListener(final Context context, final OnPlayerStatusListener mPlayerStatusListener) {
        if (!this.mPlayerStatusRegistered) {
            this.registerPlayerStatusReceiver(context);
            this.mPlayerStatusRegistered = true;
        }
        this.mPlayerStatusListener = mPlayerStatusListener;
    }
    
    public void searchOnlineMusic(final String s, final OnSearchListener onSearchListener) {
        this.searchOnlineMusic(s, SearchMode.ALL, onSearchListener);
    }
    
    public void setFullScreen(final Context context, final boolean b) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.SET_FULL_SCREEN");
        intent.putExtra("key_full_screen", b);
        context.sendBroadcast(intent);
    }
    
    public void setPlayMode(final Context context, final PlayMode playMode) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.MEDIA_BUTTON");
        intent.putExtra("EXTRA", playMode.getPlayMode());
        intent.putExtra("kuwo_key", KWAPI.mKey);
        context.sendBroadcast(intent);
    }
    
    public void setPlayState(final Context context, final PlayState playState) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.MEDIA_BUTTON");
        intent.putExtra("EXTRA", playState.getPlayState());
        intent.putExtra("kuwo_key", KWAPI.mKey);
        context.sendBroadcast(intent);
    }
    
    public void startAPP(final Context context, final boolean b) {
        final Intent intent = new Intent("cn.kuwo.kwmusicauto.action.STARTAPP");
        intent.setFlags(268435456);
        final Bundle bundle = new Bundle();
        bundle.putString("kuwo_key", KWAPI.mKey);
        bundle.putBoolean("auto_play", b);
        intent.putExtras(bundle);
        try {
            context.startActivity(intent);
        }
        catch (ActivityNotFoundException ex) {
            Toast.makeText(context, (CharSequence)"\u672a\u627e\u5230\u53ef\u7528\u7684\u5b89\u88c5\u7a0b\u5e8f\uff01", 1).show();
        }
    }
    
    public void unRegisterExitListener(final Context context) {
        if (this.mExitRegistered) {
            this.unRegisterExitReceiver(context);
            this.mExitRegistered = false;
        }
    }
    
    public void unRegisterPlayEndListener(final Context context) {
        if (this.mPlayEndRegistered) {
            this.unRegisterPlayEndReceiver(context);
            this.mPlayEndRegistered = false;
        }
    }
    
    public void unRegisterPlayerModeListener(final Context context) {
        if (this.mPlayerModeRegistered) {
            this.unRegisterPlayerModeReceiver(context);
            this.mPlayerModeRegistered = false;
        }
    }
    
    public void unRegisterPlayerStatusListener(final Context context) {
        if (this.mPlayerStatusRegistered) {
            this.unRegisterPlayerStatusReceiver(context);
            this.mPlayerStatusRegistered = false;
        }
    }
}
