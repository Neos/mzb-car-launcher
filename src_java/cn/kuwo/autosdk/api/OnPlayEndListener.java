package cn.kuwo.autosdk.api;

public interface OnPlayEndListener
{
    void onPlayEnd(final PlayEndType p0);
}
