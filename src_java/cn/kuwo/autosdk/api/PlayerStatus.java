package cn.kuwo.autosdk.api;

public enum PlayerStatus
{
    BUFFERING("BUFFERING", 2), 
    INIT("INIT", 0), 
    PAUSE("PAUSE", 3), 
    PLAYING("PLAYING", 1), 
    STOP("STOP", 4);
    
    private PlayerStatus(final String s, final int n) {
    }
}
