package cn.kuwo.autosdk.api;

public enum PlayState
{
    public static final String CHANGE_SONGLIST = "CHANGE_SONGLIST";
    private static final String MEDIA_NEXT = "MEDIA_NEXT";
    private static final String MEDIA_PAUSE = "MEDIA_PAUSE";
    private static final String MEDIA_PLAY = "MEDIA_PLAY";
    private static final String MEDIA_PRE = "MEDIA_PRE";
    
    STATE_NEXT("STATE_NEXT", 1), 
    STATE_PAUSE("STATE_PAUSE", 2), 
    STATE_PLAY("STATE_PLAY", 3), 
    STATE_PRE("STATE_PRE", 0);
    
    private PlayState(final String s, final int n) {
    }
    
    public abstract String getPlayState();
}
