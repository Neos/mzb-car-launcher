package cn.kuwo.autosdk;

import cn.kuwo.autosdk.api.*;
import android.text.*;
import java.net.*;
import java.io.*;

public class z
{
    protected static String a;
    public static String b;
    
    static {
        z.a = null;
        z.b = "http://mobi.kuwo.cn/mobi.s?f=kuwo&q=";
    }
    
    private static String a(String s) {
        final StringBuilder a = a();
        a.append(s);
        final byte[] bytes = a.toString().getBytes();
        final byte[] a2 = p.a(bytes, bytes.length, p.a, p.b);
        s = new String(m.a(a2, a2.length));
        return String.valueOf(z.b) + s;
    }
    
    public static String a(final String s, final SearchMode searchMode, final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("type=new_search");
        if (searchMode != null) {
            sb.append("&mode=all");
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            sb.append("&word=");
        }
        else {
            try {
                sb.append("&word=" + URLEncoder.encode(s, "utf-8"));
            }
            catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
        }
        if (n >= 0) {
            sb.append("&pn=" + n);
        }
        if (n2 >= 0) {
            sb.append("&rn=" + n2);
        }
        return a(sb.toString());
    }
    
    public static StringBuilder a() {
        synchronized (z.class) {
            final StringBuilder sb = new StringBuilder();
            if (z.a == null) {
                sb.append("user=").append(n.b);
                sb.append("&prod=").append(n.f);
                sb.append("&corp=kuwo");
                sb.append("&source=").append(n.g);
                sb.append("&");
                z.a = sb.toString();
            }
            else {
                sb.append(z.a);
            }
            return sb;
        }
    }
}
