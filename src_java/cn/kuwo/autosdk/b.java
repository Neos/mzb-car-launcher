package cn.kuwo.autosdk;

import java.util.concurrent.atomic.*;
import java.net.*;
import android.os.*;
import java.util.*;
import android.text.*;
import java.io.*;

public class b implements Runnable
{
    static String a;
    static long b;
    static final AtomicLong c;
    static Proxy d;
    public static int e;
    d f;
    String g;
    Map h;
    boolean i;
    byte[] j;
    String k;
    int l;
    long m;
    boolean n;
    Proxy o;
    volatile boolean p;
    volatile boolean q;
    int r;
    long s;
    HttpURLConnection t;
    InputStream u;
    OutputStream v;
    ByteArrayOutputStream w;
    f x;
    Handler y;
    a z;
    
    static {
        cn.kuwo.autosdk.b.a = "HttpSession";
        cn.kuwo.autosdk.b.b = Long.MAX_VALUE;
        c = new AtomicLong();
        cn.kuwo.autosdk.b.d = Proxy.NO_PROXY;
        cn.kuwo.autosdk.b.e = 8192;
    }
    
    public b() {
        this.f = new d(this);
        this.g = "";
        this.h = new HashMap();
        this.n = true;
        this.y = null;
        this.z = new a();
        this.s = Thread.currentThread().getId();
        this.a("Accept", "*/*");
        this.a("Connection", "Close");
    }
    
    private void a(final Handler handler, final Runnable runnable) {
        if (handler == null) {
            return;
        }
        if (handler.getLooper().getThread().getId() == Thread.currentThread().getId()) {
            runnable.run();
            return;
        }
        handler.post(runnable);
    }
    
    public a a(final String g) {
        this.g = g;
        return this.d();
    }
    
    void a(final int n, final int n2, final byte[] array, final int n3) {
        if (this.x != null && this.y != null) {
            this.a(this.y, this.f.a(n, n2, array, n3));
        }
    }
    
    public void a(final long m) {
        this.m = m;
    }
    
    void a(final e e, final int n) {
        if (this.x != null && this.y != null) {
            this.a(this.y, new c(this, e, n));
        }
    }
    
    public void a(final String s, final String s2) {
        this.h.put(s, s2);
    }
    
    boolean a() {
        if (!TextUtils.isEmpty((CharSequence)this.g) && this.g.length() <= cn.kuwo.autosdk.b.e && (!this.i || this.j != null) && Thread.currentThread().getId() == this.s) {
            ++this.r;
            if (1 == this.r) {
                return true;
            }
        }
        return false;
    }
    
    boolean a(final byte[] p0, final int p1, final int p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aload_0        
        //     4: getfield        cn/kuwo/autosdk/b.k:Ljava/lang/String;
        //     7: ifnull          57
        //    10: aconst_null    
        //    11: astore          8
        //    13: new             Ljava/io/RandomAccessFile;
        //    16: dup            
        //    17: aload_0        
        //    18: getfield        cn/kuwo/autosdk/b.k:Ljava/lang/String;
        //    21: ldc             "rw"
        //    23: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    26: astore          7
        //    28: iload_3        
        //    29: i2l            
        //    30: lstore          5
        //    32: aload           7
        //    34: lload           5
        //    36: invokevirtual   java/io/RandomAccessFile.seek:(J)V
        //    39: aload           7
        //    41: aload_1        
        //    42: iconst_0       
        //    43: iload_2        
        //    44: invokevirtual   java/io/RandomAccessFile.write:([BII)V
        //    47: aload           7
        //    49: ifnull          57
        //    52: aload           7
        //    54: invokevirtual   java/io/RandomAccessFile.close:()V
        //    57: iconst_1       
        //    58: istore          4
        //    60: iload           4
        //    62: ireturn        
        //    63: astore_1       
        //    64: aconst_null    
        //    65: astore          7
        //    67: aload           7
        //    69: ifnull          60
        //    72: aload           7
        //    74: invokevirtual   java/io/RandomAccessFile.close:()V
        //    77: iconst_0       
        //    78: ireturn        
        //    79: astore_1       
        //    80: iconst_0       
        //    81: ireturn        
        //    82: astore_1       
        //    83: aload           8
        //    85: astore          7
        //    87: aload           7
        //    89: ifnull          97
        //    92: aload           7
        //    94: invokevirtual   java/io/RandomAccessFile.close:()V
        //    97: aload_1        
        //    98: athrow         
        //    99: astore          7
        //   101: goto            97
        //   104: astore_1       
        //   105: goto            57
        //   108: astore_1       
        //   109: goto            87
        //   112: astore_1       
        //   113: goto            67
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  13     28     63     67     Ljava/io/IOException;
        //  13     28     82     87     Any
        //  32     47     112    116    Ljava/io/IOException;
        //  32     47     108    112    Any
        //  52     57     104    108    Ljava/io/IOException;
        //  72     77     79     82     Ljava/io/IOException;
        //  92     97     99     104    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0057:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    boolean b() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: astore_1       
        //     5: aload_0        
        //     6: getfield        cn/kuwo/autosdk/b.g:Ljava/lang/String;
        //     9: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //    12: ldc             "HTTP"
        //    14: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    17: ifne            40
        //    20: new             Ljava/lang/StringBuilder;
        //    23: dup            
        //    24: ldc             "http://"
        //    26: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    29: aload_0        
        //    30: getfield        cn/kuwo/autosdk/b.g:Ljava/lang/String;
        //    33: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    36: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    39: astore_1       
        //    40: new             Ljava/net/URL;
        //    43: dup            
        //    44: aload_1        
        //    45: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
        //    48: astore_2       
        //    49: aload_0        
        //    50: getfield        cn/kuwo/autosdk/b.o:Ljava/net/Proxy;
        //    53: ifnonnull       224
        //    56: getstatic       cn/kuwo/autosdk/b.d:Ljava/net/Proxy;
        //    59: astore_1       
        //    60: aload_0        
        //    61: aload_2        
        //    62: aload_1        
        //    63: invokevirtual   java/net/URL.openConnection:(Ljava/net/Proxy;)Ljava/net/URLConnection;
        //    66: checkcast       Ljava/net/HttpURLConnection;
        //    69: putfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //    72: aload_0        
        //    73: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //    76: iconst_1       
        //    77: invokevirtual   java/net/HttpURLConnection.setInstanceFollowRedirects:(Z)V
        //    80: aload_0        
        //    81: getfield        cn/kuwo/autosdk/b.h:Ljava/util/Map;
        //    84: ifnull          111
        //    87: aload_0        
        //    88: getfield        cn/kuwo/autosdk/b.h:Ljava/util/Map;
        //    91: invokeinterface java/util/Map.keySet:()Ljava/util/Set;
        //    96: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //   101: astore_1       
        //   102: aload_1        
        //   103: invokeinterface java/util/Iterator.hasNext:()Z
        //   108: ifne            249
        //   111: aload_0        
        //   112: getfield        cn/kuwo/autosdk/b.m:J
        //   115: l2i            
        //   116: ifeq            139
        //   119: aload_0        
        //   120: getfield        cn/kuwo/autosdk/b.m:J
        //   123: l2i            
        //   124: ifle            303
        //   127: aload_0        
        //   128: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //   131: aload_0        
        //   132: getfield        cn/kuwo/autosdk/b.m:J
        //   135: l2i            
        //   136: invokevirtual   java/net/HttpURLConnection.setConnectTimeout:(I)V
        //   139: aload_0        
        //   140: getfield        cn/kuwo/autosdk/b.i:Z
        //   143: ifeq            328
        //   146: aload_0        
        //   147: getfield        cn/kuwo/autosdk/b.j:[B
        //   150: ifnull          205
        //   153: aload_0        
        //   154: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //   157: iconst_1       
        //   158: invokevirtual   java/net/HttpURLConnection.setDoOutput:(Z)V
        //   161: aload_0        
        //   162: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //   165: iconst_1       
        //   166: invokevirtual   java/net/HttpURLConnection.setDoInput:(Z)V
        //   169: aload_0        
        //   170: new             Ljava/io/BufferedOutputStream;
        //   173: dup            
        //   174: aload_0        
        //   175: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //   178: invokevirtual   java/net/HttpURLConnection.getOutputStream:()Ljava/io/OutputStream;
        //   181: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   184: putfield        cn/kuwo/autosdk/b.v:Ljava/io/OutputStream;
        //   187: aload_0        
        //   188: getfield        cn/kuwo/autosdk/b.v:Ljava/io/OutputStream;
        //   191: aload_0        
        //   192: getfield        cn/kuwo/autosdk/b.j:[B
        //   195: invokevirtual   java/io/OutputStream.write:([B)V
        //   198: aload_0        
        //   199: getfield        cn/kuwo/autosdk/b.v:Ljava/io/OutputStream;
        //   202: invokevirtual   java/io/OutputStream.flush:()V
        //   205: iconst_1       
        //   206: ireturn        
        //   207: astore_1       
        //   208: aload_1        
        //   209: invokevirtual   java/net/MalformedURLException.printStackTrace:()V
        //   212: aload_0        
        //   213: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   216: ldc_w           "url error"
        //   219: putfield        cn/kuwo/autosdk/a.g:Ljava/lang/String;
        //   222: iconst_0       
        //   223: ireturn        
        //   224: aload_0        
        //   225: getfield        cn/kuwo/autosdk/b.o:Ljava/net/Proxy;
        //   228: astore_1       
        //   229: goto            60
        //   232: astore_1       
        //   233: aload_1        
        //   234: invokevirtual   java/io/IOException.printStackTrace:()V
        //   237: aload_0        
        //   238: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   241: ldc_w           "connect error"
        //   244: putfield        cn/kuwo/autosdk/a.g:Ljava/lang/String;
        //   247: iconst_0       
        //   248: ireturn        
        //   249: aload_1        
        //   250: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   255: checkcast       Ljava/lang/String;
        //   258: astore_2       
        //   259: aload_0        
        //   260: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //   263: aload_2        
        //   264: aload_0        
        //   265: getfield        cn/kuwo/autosdk/b.h:Ljava/util/Map;
        //   268: aload_2        
        //   269: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   274: checkcast       Ljava/lang/String;
        //   277: invokevirtual   java/net/HttpURLConnection.setRequestProperty:(Ljava/lang/String;Ljava/lang/String;)V
        //   280: goto            102
        //   283: astore_1       
        //   284: aload_0        
        //   285: getfield        cn/kuwo/autosdk/b.p:Z
        //   288: ifne            301
        //   291: aload_0        
        //   292: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   295: ldc_w           "unknown"
        //   298: putfield        cn/kuwo/autosdk/a.g:Ljava/lang/String;
        //   301: iconst_0       
        //   302: ireturn        
        //   303: aload_0        
        //   304: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   307: ldc_w           "connect timeout"
        //   310: putfield        cn/kuwo/autosdk/a.g:Ljava/lang/String;
        //   313: iconst_0       
        //   314: ireturn        
        //   315: astore_1       
        //   316: aload_0        
        //   317: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   320: ldc_w           "post write failed"
        //   323: putfield        cn/kuwo/autosdk/a.g:Ljava/lang/String;
        //   326: iconst_0       
        //   327: ireturn        
        //   328: aload_0        
        //   329: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //   332: invokevirtual   java/net/HttpURLConnection.connect:()V
        //   335: aload_0        
        //   336: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   339: invokestatic    java/lang/System.currentTimeMillis:()J
        //   342: aload_0        
        //   343: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   346: getfield        cn/kuwo/autosdk/a.e:J
        //   349: lsub           
        //   350: putfield        cn/kuwo/autosdk/a.m:J
        //   353: goto            205
        //   356: astore_1       
        //   357: aload_0        
        //   358: getfield        cn/kuwo/autosdk/b.z:Lcn/kuwo/autosdk/a;
        //   361: ldc_w           "connect failed"
        //   364: putfield        cn/kuwo/autosdk/a.g:Ljava/lang/String;
        //   367: iconst_0       
        //   368: ireturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  40     49     207    224    Ljava/net/MalformedURLException;
        //  49     60     232    249    Ljava/io/IOException;
        //  49     60     283    303    Ljava/lang/Exception;
        //  60     80     232    249    Ljava/io/IOException;
        //  60     80     283    303    Ljava/lang/Exception;
        //  80     102    283    303    Ljava/lang/Exception;
        //  102    111    283    303    Ljava/lang/Exception;
        //  111    139    283    303    Ljava/lang/Exception;
        //  139    169    283    303    Ljava/lang/Exception;
        //  169    205    315    328    Ljava/io/IOException;
        //  169    205    283    303    Ljava/lang/Exception;
        //  224    229    232    249    Ljava/io/IOException;
        //  224    229    283    303    Ljava/lang/Exception;
        //  233    247    283    303    Ljava/lang/Exception;
        //  249    280    283    303    Ljava/lang/Exception;
        //  303    313    283    303    Ljava/lang/Exception;
        //  316    326    283    303    Ljava/lang/Exception;
        //  328    353    356    369    Ljava/io/IOException;
        //  328    353    283    303    Ljava/lang/Exception;
        //  357    367    283    303    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 173, Size: 173
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    void c() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: aload_0        
        //     3: getfield        cn/kuwo/autosdk/b.w:Ljava/io/ByteArrayOutputStream;
        //     6: ifnull          16
        //     9: aload_0        
        //    10: getfield        cn/kuwo/autosdk/b.w:Ljava/io/ByteArrayOutputStream;
        //    13: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //    16: aload_0        
        //    17: getfield        cn/kuwo/autosdk/b.v:Ljava/io/OutputStream;
        //    20: ifnull          30
        //    23: aload_0        
        //    24: getfield        cn/kuwo/autosdk/b.v:Ljava/io/OutputStream;
        //    27: invokevirtual   java/io/OutputStream.close:()V
        //    30: aload_0        
        //    31: getfield        cn/kuwo/autosdk/b.u:Ljava/io/InputStream;
        //    34: ifnull          44
        //    37: aload_0        
        //    38: getfield        cn/kuwo/autosdk/b.u:Ljava/io/InputStream;
        //    41: invokevirtual   java/io/InputStream.close:()V
        //    44: aload_0        
        //    45: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //    48: ifnull          58
        //    51: aload_0        
        //    52: getfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //    55: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //    58: aload_0        
        //    59: monitorexit    
        //    60: return         
        //    61: astore_1       
        //    62: aload_0        
        //    63: aconst_null    
        //    64: putfield        cn/kuwo/autosdk/b.t:Ljava/net/HttpURLConnection;
        //    67: goto            58
        //    70: astore_1       
        //    71: aload_0        
        //    72: monitorexit    
        //    73: aload_1        
        //    74: athrow         
        //    75: astore_1       
        //    76: goto            44
        //    79: astore_1       
        //    80: goto            30
        //    83: astore_1       
        //    84: goto            16
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      16     83     87     Ljava/io/IOException;
        //  2      16     70     75     Any
        //  16     30     79     83     Ljava/io/IOException;
        //  16     30     70     75     Any
        //  30     44     75     79     Ljava/io/IOException;
        //  30     44     70     75     Any
        //  44     58     61     70     Ljava/lang/Throwable;
        //  44     58     70     75     Any
        //  62     67     70     75     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0016:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    a d() {
        if (!this.a()) {
            this.z.b = -1;
            return this.z;
        }
        this.z.k = this.g;
        if (this.p) {
            this.z.g = "user cancel";
            this.z.b = -3;
            return this.z;
        }
        if (this.n && cn.kuwo.autosdk.b.c.get() > cn.kuwo.autosdk.b.b) {
            this.z.g = "flow limit";
            this.z.b = -4;
            return this.z;
        }
        if (!this.b()) {
            return this.z;
        }
        if (this.p) {
            this.z.g = "user cancel";
            this.z.b = -3;
            return this.z;
        }
        try {
            this.z.b = this.t.getResponseCode();
            if (this.z.b == 200 || this.z.b == 201 || this.z.b == 206) {
                while (true) {
                    this.u = new BufferedInputStream(this.t.getInputStream());
                    this.w = new ByteArrayOutputStream();
                    while (true) {
                        byte[] array;
                        int read;
                        try {
                            array = new byte[4096];
                            if ((int)this.m > 0) {
                                this.t.setReadTimeout((int)this.m);
                            }
                            read = this.u.read(array, 0, 4096);
                            if (read <= 0 || this.p) {
                                if (this.p) {
                                    this.z.g = "user cancel";
                                    this.z.b = -3;
                                    return this.z;
                                }
                                break;
                            }
                        }
                        catch (OutOfMemoryError outOfMemoryError) {
                            this.z.b = -5;
                            this.z.g = "OutOfMemoryError";
                            return this.z;
                        }
                        if (this.n) {
                            cn.kuwo.autosdk.b.c.set(cn.kuwo.autosdk.b.c.get() + read);
                        }
                        try {
                            this.w.write(array, 0, read);
                            continue;
                        }
                        catch (OutOfMemoryError outOfMemoryError2) {
                            this.z.b = -5;
                            this.z.g = "write data failed";
                            return this.z;
                        }
                        break;
                    }
                    break;
                }
                this.z.n = System.currentTimeMillis() - this.z.e - this.z.m;
                try {
                    this.z.c = this.w.toByteArray();
                    this.z.a = true;
                    this.z.f = System.currentTimeMillis() - this.z.d;
                    this.q = true;
                    this.c();
                    return this.z;
                }
                catch (OutOfMemoryError outOfMemoryError3) {
                    this.z.b = -5;
                    this.z.g = "OutOfMemoryError";
                    return this.z;
                }
            }
            this.z.g = "resqonse code error ";
            return this.z;
        }
        catch (IOException ex) {
            this.z.b = -1;
            this.z.g = "read data failed";
        }
        catch (Exception ex2) {
            this.z.b = -1;
            this.z.g = "unknown";
        }
        finally {
            this.z.f = System.currentTimeMillis() - this.z.d;
            this.q = true;
            this.c();
        }
    }
    
    boolean e() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: aconst_null    
        //     3: astore_2       
        //     4: aconst_null    
        //     5: astore_1       
        //     6: aload_0        
        //     7: getfield        cn/kuwo/autosdk/b.k:Ljava/lang/String;
        //    10: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    13: ifeq            18
        //    16: iconst_1       
        //    17: ireturn        
        //    18: new             Ljava/io/File;
        //    21: dup            
        //    22: aload_0        
        //    23: getfield        cn/kuwo/autosdk/b.k:Ljava/lang/String;
        //    26: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    29: astore          4
        //    31: aload           4
        //    33: invokevirtual   java/io/File.exists:()Z
        //    36: ifne            45
        //    39: aload           4
        //    41: invokevirtual   java/io/File.createNewFile:()Z
        //    44: pop            
        //    45: aload           4
        //    47: invokevirtual   java/io/File.length:()J
        //    50: aload_0        
        //    51: getfield        cn/kuwo/autosdk/b.l:I
        //    54: i2l            
        //    55: lcmp           
        //    56: ifle            181
        //    59: new             Ljava/io/RandomAccessFile;
        //    62: dup            
        //    63: aload_0        
        //    64: getfield        cn/kuwo/autosdk/b.k:Ljava/lang/String;
        //    67: ldc             "rw"
        //    69: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    72: astore_2       
        //    73: aload_2        
        //    74: astore_1       
        //    75: aload_2        
        //    76: aload_0        
        //    77: getfield        cn/kuwo/autosdk/b.l:I
        //    80: i2l            
        //    81: invokevirtual   java/io/RandomAccessFile.setLength:(J)V
        //    84: aload_2        
        //    85: ifnull          92
        //    88: aload_2        
        //    89: invokevirtual   java/io/RandomAccessFile.close:()V
        //    92: aload_0        
        //    93: getfield        cn/kuwo/autosdk/b.l:I
        //    96: ifle            132
        //    99: aload_0        
        //   100: ldc_w           "Range"
        //   103: new             Ljava/lang/StringBuilder;
        //   106: dup            
        //   107: ldc_w           "bytes="
        //   110: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   113: aload_0        
        //   114: getfield        cn/kuwo/autosdk/b.l:I
        //   117: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   120: ldc_w           "-"
        //   123: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   126: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   129: invokevirtual   cn/kuwo/autosdk/b.a:(Ljava/lang/String;Ljava/lang/String;)V
        //   132: aload_0        
        //   133: ldc_w           "Accept-Encoding"
        //   136: ldc_w           "identity"
        //   139: invokevirtual   cn/kuwo/autosdk/b.a:(Ljava/lang/String;Ljava/lang/String;)V
        //   142: iconst_1       
        //   143: ireturn        
        //   144: astore_1       
        //   145: iconst_0       
        //   146: ireturn        
        //   147: astore_3       
        //   148: aconst_null    
        //   149: astore_2       
        //   150: aload_2        
        //   151: astore_1       
        //   152: aload_3        
        //   153: invokevirtual   java/io/IOException.printStackTrace:()V
        //   156: aload_2        
        //   157: ifnull          164
        //   160: aload_2        
        //   161: invokevirtual   java/io/RandomAccessFile.close:()V
        //   164: iconst_0       
        //   165: ireturn        
        //   166: astore_3       
        //   167: aload_1        
        //   168: astore_2       
        //   169: aload_3        
        //   170: astore_1       
        //   171: aload_2        
        //   172: ifnull          179
        //   175: aload_2        
        //   176: invokevirtual   java/io/RandomAccessFile.close:()V
        //   179: aload_1        
        //   180: athrow         
        //   181: aload           4
        //   183: invokevirtual   java/io/File.length:()J
        //   186: aload_0        
        //   187: getfield        cn/kuwo/autosdk/b.l:I
        //   190: i2l            
        //   191: lcmp           
        //   192: ifge            268
        //   195: aload_2        
        //   196: astore_1       
        //   197: new             Ljava/io/RandomAccessFile;
        //   200: dup            
        //   201: aload_0        
        //   202: getfield        cn/kuwo/autosdk/b.k:Ljava/lang/String;
        //   205: ldc             "rw"
        //   207: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   210: astore_2       
        //   211: aload_2        
        //   212: lconst_0       
        //   213: invokevirtual   java/io/RandomAccessFile.setLength:(J)V
        //   216: aload_2        
        //   217: ifnull          224
        //   220: aload_2        
        //   221: invokevirtual   java/io/RandomAccessFile.close:()V
        //   224: aload_0        
        //   225: iconst_0       
        //   226: putfield        cn/kuwo/autosdk/b.l:I
        //   229: goto            132
        //   232: astore_1       
        //   233: aload_3        
        //   234: astore_2       
        //   235: aload_1        
        //   236: astore_3       
        //   237: aload_2        
        //   238: astore_1       
        //   239: aload_3        
        //   240: invokevirtual   java/io/IOException.printStackTrace:()V
        //   243: aload_2        
        //   244: ifnull          251
        //   247: aload_2        
        //   248: invokevirtual   java/io/RandomAccessFile.close:()V
        //   251: iconst_0       
        //   252: ireturn        
        //   253: astore_3       
        //   254: aload_1        
        //   255: astore_2       
        //   256: aload_3        
        //   257: astore_1       
        //   258: aload_2        
        //   259: ifnull          266
        //   262: aload_2        
        //   263: invokevirtual   java/io/RandomAccessFile.close:()V
        //   266: aload_1        
        //   267: athrow         
        //   268: aload_0        
        //   269: getfield        cn/kuwo/autosdk/b.l:I
        //   272: ifle            132
        //   275: aload_0        
        //   276: ldc_w           "Range"
        //   279: new             Ljava/lang/StringBuilder;
        //   282: dup            
        //   283: ldc_w           "bytes="
        //   286: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   289: aload_0        
        //   290: getfield        cn/kuwo/autosdk/b.l:I
        //   293: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   296: ldc_w           "-"
        //   299: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   302: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   305: invokevirtual   cn/kuwo/autosdk/b.a:(Ljava/lang/String;Ljava/lang/String;)V
        //   308: goto            132
        //   311: astore_1       
        //   312: goto            164
        //   315: astore_2       
        //   316: goto            179
        //   319: astore_1       
        //   320: goto            92
        //   323: astore_1       
        //   324: goto            251
        //   327: astore_2       
        //   328: goto            266
        //   331: astore_1       
        //   332: goto            224
        //   335: astore_1       
        //   336: goto            258
        //   339: astore_3       
        //   340: goto            237
        //   343: astore_3       
        //   344: aload_1        
        //   345: astore_2       
        //   346: aload_3        
        //   347: astore_1       
        //   348: goto            171
        //   351: astore_3       
        //   352: goto            150
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  18     45     144    147    Ljava/io/IOException;
        //  59     73     147    150    Ljava/io/IOException;
        //  59     73     166    171    Any
        //  75     84     351    355    Ljava/io/IOException;
        //  75     84     343    351    Any
        //  88     92     319    323    Ljava/io/IOException;
        //  152    156    343    351    Any
        //  160    164    311    315    Ljava/io/IOException;
        //  175    179    315    319    Ljava/io/IOException;
        //  197    211    232    237    Ljava/io/IOException;
        //  197    211    253    258    Any
        //  211    216    339    343    Ljava/io/IOException;
        //  211    216    335    339    Any
        //  220    224    331    335    Ljava/io/IOException;
        //  239    243    253    258    Any
        //  247    251    323    327    Ljava/io/IOException;
        //  262    266    327    331    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.util.Collections$1.remove(Unknown Source)
        //     at java.util.AbstractCollection.removeAll(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2968)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    int f() {
        int contentLength = 0;
        boolean b = false;
        Label_0210: {
            try {
                final String host = this.t.getURL().getHost();
                this.z.b = this.t.getResponseCode();
                if (!this.t.getURL().getHost().equalsIgnoreCase(host)) {
                    this.z.l = this.t.getURL().toString();
                }
                if (this.z.b != 200 && this.z.b != 201 && this.z.b != 206) {
                    this.z.g = "response code error" + this.z.b;
                    return -2;
                }
                contentLength = this.t.getContentLength();
                b = (this.h != null && "identity".equals(this.h.get("Accept-Encoding")));
                break Label_0210;
            }
            catch (IOException ex) {
                this.z.g = "get response code exception";
                return -2;
            }
            catch (Exception ex2) {
                if (!this.p) {
                    this.z.g = "unknown";
                }
                contentLength = -2;
            }
            return contentLength;
        }
        if (!b) {
            return -1;
        }
        return contentLength;
    }
    
    boolean g() {
        try {
            if ((int)this.m > 0) {
                this.t.setReadTimeout((int)this.m);
            }
            return true;
        }
        catch (Exception ex) {
            if (!this.p) {
                this.z.g = "unknown";
            }
            return false;
        }
    }
    
    void h() {
        this.c();
        this.q = true;
        this.z.f = System.currentTimeMillis() - this.z.d;
        if (this.z.a()) {
            this.a(cn.kuwo.autosdk.e.c, 0);
        }
        else if (!this.p) {
            this.a(cn.kuwo.autosdk.e.b, 0);
        }
    }
    
    @Override
    public void run() {
        int n = 0;
        this.z.e = System.currentTimeMillis();
        this.z.k = this.g;
        if (this.n && cn.kuwo.autosdk.b.c.get() > cn.kuwo.autosdk.b.b) {
            this.z.g = "flow limit";
            this.h();
            return;
        }
        if (!this.e()) {
            this.z.g = "file error";
            this.h();
            return;
        }
        if (this.p) {
            this.z.g = "user cancel";
            this.h();
            return;
        }
        if (!this.b()) {
            this.h();
            return;
        }
        final int f = this.f();
        if (-2 == f) {
            this.h();
            return;
        }
        if (f != -1 || this.k == null) {
            this.a(cn.kuwo.autosdk.e.a, this.l + f);
        }
        byte[] array = null;
        Label_0238: {
            try {
                this.u = new BufferedInputStream(this.t.getInputStream());
                this.w = new ByteArrayOutputStream();
                final int n2 = 16384;
                array = new byte[n2];
                final b b = this;
                final ByteArrayOutputStream byteArrayOutputStream = b.w;
                if (byteArrayOutputStream == null) {
                    final b b2 = this;
                    b2.h();
                    return;
                }
                break Label_0238;
            }
            catch (IOException ex) {
                ex.printStackTrace();
                this.z.g = "read data failed";
                this.h();
                return;
            }
            try {
                final int n2 = 16384;
                array = new byte[n2];
                final b b = this;
                final ByteArrayOutputStream byteArrayOutputStream = b.w;
                if (byteArrayOutputStream == null) {
                    final b b2 = this;
                    b2.h();
                    return;
                }
            }
            catch (OutOfMemoryError outOfMemoryError) {
                this.z.g = "OutOfMemoryError";
                return;
            }
        }
        if (!this.g()) {
            this.h();
            return;
        }
        while (!this.p) {
            Label_0343: {
                int read = 0;
                Label_0387: {
                    try {
                        read = this.u.read(array, 0, array.length);
                        if (read >= 0) {
                            break Label_0387;
                        }
                        if (this.k == null) {
                            break;
                        }
                        if (n != f && f != -1) {
                            this.z.g = "ContentLength error";
                            this.h();
                            return;
                        }
                    }
                    catch (IOException ex2) {
                        this.z.g = "read error";
                        this.h();
                        return;
                    }
                    catch (Exception ex3) {
                        if (!this.p) {
                            this.z.g = "unknown";
                        }
                        this.h();
                        return;
                    }
                    break Label_0343;
                }
                if (!this.g()) {
                    this.h();
                    return;
                }
                if (read == 0) {
                    continue;
                }
                if (this.n) {
                    cn.kuwo.autosdk.b.c.set(cn.kuwo.autosdk.b.c.get() + read);
                }
                Label_0475: {
                    if (this.k != null) {
                        break Label_0475;
                    }
                    try {
                        this.w.write(array, 0, read);
                        if (this.k != null && this.l + n != new File(this.k).length()) {
                            this.z.g = "io error (check file length error)";
                            this.h();
                            return;
                        }
                    }
                    catch (OutOfMemoryError outOfMemoryError2) {
                        this.z.g = "out of memory error";
                        this.h();
                        return;
                    }
                }
                if (!this.a(array, read, this.l + n)) {
                    this.z.g = "write file error";
                    this.h();
                    return;
                }
                final int n3 = n + read;
                if (f == -1) {
                    n = n3;
                    if (this.k != null) {
                        continue;
                    }
                }
                this.a(this.l + f, this.l + n3, array, read);
                n = n3;
                continue;
            }
            if (n + this.l != new File(this.k).length()) {
                this.z.g = "io error (file lenght error) ";
                this.h();
                return;
            }
            break;
        }
        if (this.p) {
            this.z.g = "user cancel";
            this.h();
            return;
        }
        this.z.n = System.currentTimeMillis() - this.z.e - this.z.m;
        try {
            this.z.c = this.w.toByteArray();
            this.z.a = true;
            if (f == -1 && this.k != null) {
                this.a(cn.kuwo.autosdk.e.a, f + this.l);
            }
            this.h();
        }
        catch (OutOfMemoryError outOfMemoryError3) {
            this.z.g = "OutOfMemoryError";
            this.h();
        }
    }
}
