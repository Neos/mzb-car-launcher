package cn.kuwo.autosdk;

import android.text.*;
import java.util.*;

public class x
{
    private static y a;
    private static final String[] b;
    
    static {
        x.a = null;
        b = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
    }
    
    public static boolean a(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            int length = s.length();
            while (true) {
                final int n = length - 1;
                if (n < 0) {
                    break;
                }
                length = n;
                if (Character.isDigit(s.charAt(n))) {
                    continue;
                }
                if (n != 0) {
                    return false;
                }
                length = n;
                if (s.charAt(n) != '-') {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public static boolean a(final String s, final String s2) {
        return s != null && s2 != null && s.equals(s2);
    }
    
    public static String[] a(final String s, final char c) {
        return a(s, c, false);
    }
    
    private static String[] a(final String s, final char c, final boolean b) {
        if (s == null) {
            return null;
        }
        final int length = s.length();
        if (length == 0) {
            return new String[0];
        }
        final ArrayList<String> list = new ArrayList<String>();
        boolean b2 = false;
        int n = 0;
        int n2 = 0;
        int i = 0;
        while (i < length) {
            if (s.charAt(i) == c) {
                if (n != 0 || b) {
                    list.add(s.substring(n2, i));
                    b2 = true;
                }
                n2 = ++i;
            }
            else {
                n = 1;
                ++i;
                b2 = false;
            }
        }
        if (n != 0 || (b && b2)) {
            list.add(s.substring(n2, i));
        }
        return list.toArray(new String[list.size()]);
    }
    
    public static String b(final String s) {
        final String[] split = s.split(":");
        if (split.length == 2) {
            return split[1];
        }
        return "";
    }
    
    public static String c(final String s) {
        String s2 = s;
        if (s == null) {
            s2 = "";
        }
        return s2;
    }
}
