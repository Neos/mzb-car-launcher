package cn.kuwo.autosdk;

import cn.kuwo.autosdk.api.*;
import android.text.*;
import cn.kuwo.autosdk.bean.*;
import org.json.*;
import java.util.*;

public class j
{
    private SearchMode a;
    
    public j(final SearchMode a) {
        this.a = SearchMode.ALL;
        this.a = a;
    }
    
    public int a(final String s, final int n) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return n;
        }
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            return n;
        }
    }
    
    public Music a(final JSONObject jsonObject) {
        if (jsonObject != null) {
            final Music music = new Music();
            try {
                music.rid = this.b(jsonObject.getString("MUSICID"));
                if (music.rid != -1L) {
                    music.name = jsonObject.getString("SONGNAME").replaceAll("&quot;", "\"");
                    music.artist = jsonObject.getString("ARTIST").replaceAll("&quot;", "\"");
                    music.album = jsonObject.getString("ALBUM").replaceAll("&quot;", "\"");
                    music.fileFormat = jsonObject.getString("FORMAT").replaceAll("&quot;", "\"");
                    music.duration = this.c(jsonObject.getString("DURATION"));
                    music.tag = jsonObject.getString("TAG").replaceAll("&quot;", "\"");
                    music.hasMv = this.a(jsonObject.getString("MVFLAG"));
                    music.mvQuality = jsonObject.getString("MVQUALITY").replaceAll("&quot;", "\"");
                    music.parseResourceStringFromQuku(jsonObject.getString("MINFO"));
                    music.hasKalaok = this.a(jsonObject.getString("KMARK"), -1);
                    return music;
                }
            }
            catch (JSONException ex) {
                ex.printStackTrace();
                return music;
            }
        }
        return null;
    }
    
    public String a(final byte[] p0, final int p1, final int p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          4
        //     3: new             Ljava/io/ByteArrayInputStream;
        //     6: dup            
        //     7: aload_1        
        //     8: iload_2        
        //     9: iload_3        
        //    10: invokespecial   java/io/ByteArrayInputStream.<init>:([BII)V
        //    13: astore          5
        //    15: new             Ljava/util/zip/Inflater;
        //    18: dup            
        //    19: invokespecial   java/util/zip/Inflater.<init>:()V
        //    22: astore_1       
        //    23: new             Ljava/util/zip/InflaterInputStream;
        //    26: dup            
        //    27: aload           5
        //    29: aload_1        
        //    30: invokespecial   java/util/zip/InflaterInputStream.<init>:(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V
        //    33: astore          6
        //    35: new             Ljava/io/ByteArrayOutputStream;
        //    38: dup            
        //    39: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //    42: astore          5
        //    44: sipush          512
        //    47: newarray        B
        //    49: astore          7
        //    51: aload           6
        //    53: aload           7
        //    55: invokevirtual   java/util/zip/InflaterInputStream.read:([B)I
        //    58: istore_2       
        //    59: iload_2        
        //    60: iconst_m1      
        //    61: if_icmpne       109
        //    64: aload           6
        //    66: invokevirtual   java/util/zip/InflaterInputStream.close:()V
        //    69: aload_1        
        //    70: invokevirtual   java/util/zip/Inflater.end:()V
        //    73: aload           5
        //    75: ldc             "utf-8"
        //    77: invokevirtual   java/io/ByteArrayOutputStream.toString:(Ljava/lang/String;)Ljava/lang/String;
        //    80: astore_1       
        //    81: aload           5
        //    83: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //    86: aload_1        
        //    87: areturn        
        //    88: astore_1       
        //    89: aload_1        
        //    90: invokevirtual   java/lang/OutOfMemoryError.printStackTrace:()V
        //    93: aconst_null    
        //    94: areturn        
        //    95: astore          8
        //    97: iconst_m1      
        //    98: istore_2       
        //    99: goto            59
        //   102: astore_1       
        //   103: aload_1        
        //   104: invokevirtual   java/io/IOException.printStackTrace:()V
        //   107: aconst_null    
        //   108: areturn        
        //   109: aload           5
        //   111: aload           7
        //   113: iconst_0       
        //   114: iload_2        
        //   115: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   118: goto            51
        //   121: astore          6
        //   123: aload           6
        //   125: invokevirtual   java/io/IOException.printStackTrace:()V
        //   128: goto            69
        //   131: astore_1       
        //   132: aload_1        
        //   133: invokevirtual   java/io/UnsupportedEncodingException.printStackTrace:()V
        //   136: aload           4
        //   138: astore_1       
        //   139: goto            81
        //   142: astore_1       
        //   143: aload_1        
        //   144: invokevirtual   java/lang/Error.printStackTrace:()V
        //   147: aload           4
        //   149: astore_1       
        //   150: goto            81
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  44     51     88     95     Ljava/lang/OutOfMemoryError;
        //  51     59     95     102    Ljava/io/EOFException;
        //  51     59     102    109    Ljava/io/IOException;
        //  64     69     121    131    Ljava/io/IOException;
        //  73     81     131    142    Ljava/io/UnsupportedEncodingException;
        //  73     81     142    153    Ljava/lang/Error;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 81, Size: 81
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public List a(String a, final int n, final int[] array) {
        if (array != null && array.length > 0) {
            array[0] = 0;
        }
        a = z.a(a, this.a, n, 30);
        if (!TextUtils.isEmpty((CharSequence)a)) {
            final b b = new b();
            b.a(8000L);
            final a a2 = b.a(a);
            if (a2 != null && a2.a() && a2.c != null) {
                return this.a(a2.c);
            }
            if (array != null && array.length > 0) {
                array[0] = 1;
                return null;
            }
        }
        return null;
    }
    
    public List a(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_2       
        //     2: aload_1        
        //     3: ifnonnull       8
        //     6: aconst_null    
        //     7: areturn        
        //     8: iconst_4       
        //     9: newarray        B
        //    11: astore          4
        //    13: aload_1        
        //    14: iconst_0       
        //    15: aload           4
        //    17: iconst_0       
        //    18: iconst_4       
        //    19: invokestatic    java/lang/System.arraycopy:(Ljava/lang/Object;ILjava/lang/Object;II)V
        //    22: aload_1        
        //    23: iconst_4       
        //    24: aload           4
        //    26: iconst_0       
        //    27: iconst_4       
        //    28: invokestatic    java/lang/System.arraycopy:(Ljava/lang/Object;ILjava/lang/Object;II)V
        //    31: aload           4
        //    33: iconst_0       
        //    34: invokestatic    cn/kuwo/autosdk/o.a:([BZ)I
        //    37: istore_3       
        //    38: aload_0        
        //    39: aload_1        
        //    40: bipush          8
        //    42: iload_3        
        //    43: invokevirtual   cn/kuwo/autosdk/j.a:([BII)Ljava/lang/String;
        //    46: astore          4
        //    48: aload           4
        //    50: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    53: ifne            6
        //    56: new             Ljava/util/ArrayList;
        //    59: dup            
        //    60: invokespecial   java/util/ArrayList.<init>:()V
        //    63: astore_1       
        //    64: new             Lorg/json/JSONObject;
        //    67: dup            
        //    68: aload           4
        //    70: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //    73: astore          4
        //    75: aload           4
        //    77: ldc             "hitmode"
        //    79: invokevirtual   org/json/JSONObject.getString:(Ljava/lang/String;)Ljava/lang/String;
        //    82: astore          5
        //    84: aload           4
        //    86: ldc             "Hit"
        //    88: invokevirtual   org/json/JSONObject.getInt:(Ljava/lang/String;)I
        //    91: istore_3       
        //    92: ldc             "song"
        //    94: aload           5
        //    96: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    99: ifeq            126
        //   102: iload_3        
        //   103: ifle            126
        //   106: aload           4
        //   108: ldc             "musiclist"
        //   110: invokevirtual   org/json/JSONObject.getJSONArray:(Ljava/lang/String;)Lorg/json/JSONArray;
        //   113: astore          4
        //   115: aload           4
        //   117: invokevirtual   org/json/JSONArray.length:()I
        //   120: istore_3       
        //   121: iload_2        
        //   122: iload_3        
        //   123: if_icmplt       155
        //   126: aload_1        
        //   127: ifnull          6
        //   130: aload_1        
        //   131: invokeinterface java/util/List.isEmpty:()Z
        //   136: ifne            6
        //   139: aload_1        
        //   140: areturn        
        //   141: astore_1       
        //   142: aload_1        
        //   143: invokevirtual   java/lang/OutOfMemoryError.printStackTrace:()V
        //   146: aconst_null    
        //   147: areturn        
        //   148: astore_1       
        //   149: aload_1        
        //   150: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   153: aconst_null    
        //   154: areturn        
        //   155: aload_0        
        //   156: aload           4
        //   158: iload_2        
        //   159: invokevirtual   org/json/JSONArray.getJSONObject:(I)Lorg/json/JSONObject;
        //   162: invokevirtual   cn/kuwo/autosdk/j.a:(Lorg/json/JSONObject;)Lcn/kuwo/autosdk/bean/Music;
        //   165: astore          5
        //   167: aload           5
        //   169: ifnull          181
        //   172: aload_1        
        //   173: aload           5
        //   175: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   180: pop            
        //   181: iload_2        
        //   182: iconst_1       
        //   183: iadd           
        //   184: istore_2       
        //   185: goto            115
        //   188: astore          4
        //   190: goto            126
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  8      13     141    148    Ljava/lang/OutOfMemoryError;
        //  13     38     148    155    Ljava/lang/Exception;
        //  64     102    188    193    Lorg/json/JSONException;
        //  106    115    188    193    Lorg/json/JSONException;
        //  115    121    188    193    Lorg/json/JSONException;
        //  155    167    188    193    Lorg/json/JSONException;
        //  172    181    188    193    Lorg/json/JSONException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 103, Size: 103
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean a(final String s) {
        return !TextUtils.isEmpty((CharSequence)s) && "1".equalsIgnoreCase(s);
    }
    
    public long b(final String s) {
        if (TextUtils.isEmpty((CharSequence)s) || s.indexOf("MUSIC_") < 0) {
            return -1L;
        }
        try {
            return Long.parseLong(s.substring("MUSIC_".length(), s.length()));
        }
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            return -1L;
        }
    }
    
    public int c(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return 0;
        }
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException ex) {
            ex.printStackTrace();
            return 0;
        }
    }
}
