package cn.kuwo.autosdk;

import java.util.*;
import android.content.*;

public final class n
{
    public static String a;
    public static String b;
    public static String c;
    public static String d;
    public static String e;
    public static String f;
    public static String g;
    
    static {
        n.c = "kwplayerhd";
        n.d = "ar";
        n.e = "1.2.0.0";
        n.f = String.valueOf(n.c) + "_" + n.d + "_" + n.e;
        n.g = "";
    }
    
    private static String a() {
        final Random random = new Random(System.currentTimeMillis());
        final StringBuilder sb = new StringBuilder();
        int nextInt;
        if ((nextInt = random.nextInt(5)) == 0) {
            nextInt = 1;
        }
        final int n = nextInt * 10000;
        sb.append(n + random.nextInt(n));
        final int n2 = (random.nextInt(5) + 5) * 100000;
        sb.append(n2 + random.nextInt(n2));
        return sb.toString();
    }
    
    public static void a(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: getstatic       cn/kuwo/autosdk/n.f:Ljava/lang/String;
        //     7: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    10: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    13: ldc             "_"
        //    15: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    18: aload_1        
        //    19: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    22: ldc             ".apk"
        //    24: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    27: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    30: putstatic       cn/kuwo/autosdk/n.g:Ljava/lang/String;
        //    33: aload_0        
        //    34: ldc             "wifi"
        //    36: invokevirtual   android/content/Context.getSystemService:(Ljava/lang/String;)Ljava/lang/Object;
        //    39: checkcast       Landroid/net/wifi/WifiManager;
        //    42: invokevirtual   android/net/wifi/WifiManager.getConnectionInfo:()Landroid/net/wifi/WifiInfo;
        //    45: invokevirtual   android/net/wifi/WifiInfo.getMacAddress:()Ljava/lang/String;
        //    48: putstatic       cn/kuwo/autosdk/n.a:Ljava/lang/String;
        //    51: getstatic       cn/kuwo/autosdk/n.a:Ljava/lang/String;
        //    54: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    57: ifeq            65
        //    60: ldc             ""
        //    62: putstatic       cn/kuwo/autosdk/n.a:Ljava/lang/String;
        //    65: aload_0        
        //    66: ldc             "phone"
        //    68: invokevirtual   android/content/Context.getSystemService:(Ljava/lang/String;)Ljava/lang/Object;
        //    71: checkcast       Landroid/telephony/TelephonyManager;
        //    74: invokevirtual   android/telephony/TelephonyManager.getDeviceId:()Ljava/lang/String;
        //    77: astore_0       
        //    78: aload_0        
        //    79: astore_1       
        //    80: aload_0        
        //    81: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    84: ifeq            125
        //    87: getstatic       cn/kuwo/autosdk/n.a:Ljava/lang/String;
        //    90: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    93: ifne            154
        //    96: new             Ljava/lang/StringBuilder;
        //    99: dup            
        //   100: getstatic       cn/kuwo/autosdk/n.a:Ljava/lang/String;
        //   103: ldc             ":"
        //   105: ldc             ""
        //   107: invokevirtual   java/lang/String.replace:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
        //   110: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   113: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   116: ldc             "000"
        //   118: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   121: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   124: astore_1       
        //   125: aload_1        
        //   126: putstatic       cn/kuwo/autosdk/n.b:Ljava/lang/String;
        //   129: return         
        //   130: astore_1       
        //   131: aload_1        
        //   132: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //   135: ldc             ""
        //   137: putstatic       cn/kuwo/autosdk/n.a:Ljava/lang/String;
        //   140: goto            65
        //   143: astore_0       
        //   144: aload_0        
        //   145: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //   148: ldc             ""
        //   150: astore_0       
        //   151: goto            78
        //   154: invokestatic    cn/kuwo/autosdk/n.a:()Ljava/lang/String;
        //   157: astore_1       
        //   158: goto            125
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  33     65     130    143    Ljava/lang/Throwable;
        //  65     78     143    154    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0065:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
