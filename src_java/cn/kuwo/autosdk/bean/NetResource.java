package cn.kuwo.autosdk.bean;

public class NetResource implements Cloneable
{
    public int bitrate;
    public MusicFormat format;
    public MusicQuality quality;
    public int size;
    
    public NetResource(final MusicQuality quality, final int bitrate, final MusicFormat format, final int size) {
        this.quality = quality;
        this.bitrate = bitrate;
        this.format = format;
        this.size = size;
    }
    
    public NetResource clone() {
        try {
            return (NetResource)super.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o != null && o instanceof NetResource) {
            final NetResource netResource = (NetResource)o;
            if (netResource.quality == this.quality && netResource.bitrate == this.bitrate && netResource.format == this.format && netResource.size == this.size) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.quality.ordinal() + this.bitrate + this.format.ordinal() + this.size;
    }
    
    public boolean isEQ() {
        return this.quality.isEQ();
    }
    
    public boolean isFLAC() {
        return this.quality.isFLAC();
    }
}
