package cn.kuwo.autosdk.bean;

import android.text.*;

public enum MusicFormat
{
    AAC("AAC", 1), 
    APE("APE", 5), 
    FLAC("FLAC", 6), 
    MP3("MP3", 2), 
    MP4("MP4", 3), 
    NONE("NONE", 0), 
    WMA("WMA", 4);
    
    private MusicFormat(final String s, final int n) {
    }
    
    public static MusicFormat getFormatFromDiscribe(final String s) {
        if (s != null) {
            final MusicFormat[] values = values();
            for (int length = values.length, i = 0; i < length; ++i) {
                final MusicFormat none;
                if ((none = values[i]).getDiscribe().equals(s)) {
                    return none;
                }
            }
            return MusicFormat.NONE;
        }
        return MusicFormat.NONE;
    }
    
    public static MusicFormat getFormatFromDiscribe4Quku(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final MusicFormat[] values = values();
            for (int length = values.length, i = 0; i < length; ++i) {
                final MusicFormat none;
                if ((none = values[i]).getDiscribe().equals(s)) {
                    return none;
                }
            }
            return MusicFormat.NONE;
        }
        return MusicFormat.NONE;
    }
    
    public abstract String getDiscribe();
}
