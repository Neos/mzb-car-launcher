package cn.kuwo.autosdk.bean;

import android.text.*;

public enum MusicQuality
{
    FLUENT("FLUENT", 0), 
    HIGHQUALITY("HIGHQUALITY", 1), 
    LOSSLESS("LOSSLESS", 3), 
    PERFECT("PERFECT", 2);
    
    private MusicQuality(final String s, final int n) {
    }
    
    public static MusicQuality getQualityFromBitrate(final int n) {
        if (n <= 48) {
            return MusicQuality.FLUENT;
        }
        if (n <= 128) {
            return MusicQuality.HIGHQUALITY;
        }
        if (n <= 320) {
            return MusicQuality.PERFECT;
        }
        return MusicQuality.LOSSLESS;
    }
    
    public static MusicQuality getQualityFromDiscribe(final String s) {
        if (s != null) {
            final MusicQuality[] values = values();
            for (int length = values.length, i = 0; i < length; ++i) {
                final MusicQuality fluent;
                if ((fluent = values[i]).getDiscribe().equals(s)) {
                    return fluent;
                }
            }
            return MusicQuality.FLUENT;
        }
        return MusicQuality.FLUENT;
    }
    
    public static MusicQuality getQualityFromDiscribe4Quku(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return MusicQuality.FLUENT;
        }
        if ("s".equals(s)) {
            return MusicQuality.FLUENT;
        }
        if ("h".equals(s)) {
            return MusicQuality.HIGHQUALITY;
        }
        if ("p".equals(s)) {
            return MusicQuality.PERFECT;
        }
        if ("pp".equals(s)) {
            return MusicQuality.LOSSLESS;
        }
        if ("ff".equals(s)) {
            return MusicQuality.LOSSLESS;
        }
        return MusicQuality.FLUENT;
    }
    
    abstract String getDiscribe();
    
    public boolean isEQ() {
        return this == MusicQuality.LOSSLESS || this == MusicQuality.PERFECT;
    }
    
    public boolean isFLAC() {
        return this == MusicQuality.LOSSLESS;
    }
}
