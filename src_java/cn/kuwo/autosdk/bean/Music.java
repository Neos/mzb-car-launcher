package cn.kuwo.autosdk.bean;

import java.io.*;
import cn.kuwo.autosdk.*;
import java.util.*;
import android.text.*;
import android.database.*;
import android.content.*;

public class Music implements Serializable, Cloneable
{
    private static final long serialVersionUID = 3985672550071260552L;
    public String album;
    public String artist;
    public long artistId;
    public boolean checked;
    public q createDate;
    public DownloadQuality$Quality downQuality;
    public long downSize;
    public int duration;
    private boolean eq;
    public String fileFormat;
    public String filePath;
    public long fileSize;
    private boolean flac;
    public int hasKalaok;
    public boolean hasMv;
    public int hot;
    public Music$LocalFileState localFileState;
    public String mvIconUrl;
    public String mvQuality;
    public String name;
    public boolean playFail;
    public String psrc;
    private Collection resourceCollection;
    public long rid;
    public String source;
    private long storageId;
    public String tag;
    public int trend;
    
    public Music() {
        this.name = "";
        this.artist = "";
        this.album = "";
        this.tag = "";
        this.mvQuality = "";
        this.mvIconUrl = "";
        this.source = "";
        this.createDate = new q();
        this.localFileState = Music$LocalFileState.NOT_CHECK;
        this.filePath = "";
        this.fileFormat = "";
        this.downQuality = DownloadQuality$Quality.Q_AUTO;
    }
    
    public boolean Contain(final Music music) {
        boolean b = false;
        if (music.rid > 0L && this.rid > 0L) {
            if (music.rid == this.rid) {
                b = true;
            }
        }
        else {
            if (this.rid > 0L) {
                return x.a(music.filePath, this.filePath);
            }
            if (music.rid == 0L) {
                return x.a(music.filePath, this.filePath);
            }
        }
        return b;
    }
    
    public boolean addResource(final MusicQuality musicQuality, final int n, final MusicFormat musicFormat, final int n2) {
        return this.addResource(new NetResource(musicQuality, n, musicFormat, n2));
    }
    
    public boolean addResource(final NetResource netResource) {
        if (netResource == null) {
            return false;
        }
        if (this.resourceCollection == null) {
            this.resourceCollection = new ArrayList();
        }
        final Iterator<NetResource> iterator = this.resourceCollection.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().equals(netResource)) {
                return false;
            }
        }
        if (netResource.isEQ()) {
            this.eq = true;
        }
        if (netResource.isFLAC()) {
            this.flac = true;
        }
        return this.resourceCollection.add(netResource);
    }
    
    public Music clone() {
        Music music2;
        try {
            final Music music = music2 = (Music)super.clone();
            if (this.resourceCollection != null) {
                music.resourceCollection = new ArrayList();
                final Iterator<NetResource> iterator = this.resourceCollection.iterator();
                while (iterator.hasNext()) {
                    music.resourceCollection.add(iterator.next().clone());
                }
                return music;
            }
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            music2 = null;
        }
        return music2;
    }
    
    public boolean equalsEx(final Music music) {
        if (music.rid > 0L) {
            if (music.rid != this.rid) {
                return false;
            }
        }
        else {
            if (this.filePath != null && music.filePath != null) {
                return this.filePath.equals(music.filePath);
            }
            if (this.filePath != null || music.filePath != null) {
                return false;
            }
        }
        return true;
    }
    
    public NetResource getBestResource() {
        if (this.resourceCollection == null) {
            return null;
        }
        final Iterator<NetResource> iterator = this.resourceCollection.iterator();
        NetResource netResource = null;
        while (iterator.hasNext()) {
            final NetResource netResource2 = iterator.next();
            if (netResource == null) {
                netResource = netResource2;
            }
            else {
                if (netResource.bitrate >= netResource2.bitrate) {
                    continue;
                }
                netResource = netResource2;
            }
        }
        return netResource;
    }
    
    public NetResource getBestResource(final MusicQuality musicQuality) {
        if (this.resourceCollection == null) {
            return null;
        }
        final Iterator<NetResource> iterator = this.resourceCollection.iterator();
        NetResource netResource = null;
        while (iterator.hasNext()) {
            final NetResource netResource2 = iterator.next();
            if (netResource2.quality.ordinal() <= musicQuality.ordinal() && (netResource == null || netResource.bitrate < netResource2.bitrate)) {
                netResource = netResource2;
            }
        }
        return netResource;
    }
    
    public boolean getInfoFromDatabase(final Cursor cursor) {
        while (true) {
            while (true) {
                Label_0419: {
                    try {
                        this.setStorageId(cursor.getLong(cursor.getColumnIndex("id")));
                        this.rid = cursor.getLong(cursor.getColumnIndex("rid"));
                        this.name = x.c(cursor.getString(cursor.getColumnIndex("name")));
                        this.artist = x.c(cursor.getString(cursor.getColumnIndex("artist")));
                        this.artistId = cursor.getLong(cursor.getColumnIndex("artistid"));
                        this.album = x.c(cursor.getString(cursor.getColumnIndex("album")));
                        this.duration = cursor.getInt(cursor.getColumnIndex("duration"));
                        if (cursor.getInt(cursor.getColumnIndex("hasmv")) <= 0) {
                            break Label_0419;
                        }
                        final boolean hasMv = true;
                        this.hasMv = hasMv;
                        this.mvQuality = x.c(cursor.getString(cursor.getColumnIndex("mvquality")));
                        this.hasKalaok = cursor.getInt(cursor.getColumnIndex("haskalaok"));
                        this.downSize = cursor.getInt(cursor.getColumnIndex("downsize"));
                        this.downQuality = DownloadQuality$Quality.valueOf(x.c(cursor.getString(cursor.getColumnIndex("downquality"))));
                        this.filePath = x.c(cursor.getString(cursor.getColumnIndex("filepath")));
                        this.fileSize = cursor.getLong(cursor.getColumnIndex("filesize"));
                        this.fileFormat = x.c(cursor.getString(cursor.getColumnIndex("fileformat")));
                        if (cursor.getColumnIndex("resource") >= 0) {
                            this.parseResourceStringFromDatabase(x.c(cursor.getString(cursor.getColumnIndex("resource"))));
                        }
                        if (cursor.getColumnIndex("createtime") >= 0) {
                            final String c = x.c(cursor.getString(cursor.getColumnIndex("createtime")));
                            if (TextUtils.isEmpty((CharSequence)c)) {
                                this.createDate = new q();
                                return true;
                            }
                            this.createDate = new q(c);
                            return true;
                        }
                    }
                    catch (SQLException ex) {
                        ex.printStackTrace();
                        return false;
                    }
                    break;
                }
                final boolean hasMv = false;
                continue;
            }
        }
        this.createDate = new q();
        return true;
    }
    
    public ContentValues getMusicContentValues(final long n) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("rid", this.rid);
        contentValues.put("listid", n);
        contentValues.put("name", x.c(this.name));
        contentValues.put("artist", x.c(this.artist));
        contentValues.put("artistid", this.artistId);
        contentValues.put("album", x.c(this.album));
        contentValues.put("duration", this.duration);
        contentValues.put("hot", this.hot);
        contentValues.put("source", x.c(this.source));
        contentValues.put("resource", x.c(this.getResourceStringForDatabase()));
        int n2;
        if (this.hasMv) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        contentValues.put("hasmv", n2);
        contentValues.put("mvquality", x.c(this.mvQuality));
        contentValues.put("haskalaok", this.hasKalaok);
        contentValues.put("downsize", this.downSize);
        String string;
        if (this.downQuality == null) {
            string = "";
        }
        else {
            string = this.downQuality.toString();
        }
        contentValues.put("downquality", string);
        contentValues.put("filepath", x.c(this.filePath));
        contentValues.put("fileformat", x.c(this.fileFormat));
        contentValues.put("filesize", this.fileSize);
        contentValues.put("createtime", x.c(this.createDate.a()));
        return contentValues;
    }
    
    public NetResource getResource(final MusicFormat musicFormat) {
        if (this.resourceCollection == null) {
            return null;
        }
        final Iterator<NetResource> iterator = this.resourceCollection.iterator();
        NetResource netResource = null;
        while (iterator.hasNext()) {
            final NetResource netResource2 = iterator.next();
            if (netResource2.format == musicFormat && (netResource == null || netResource.bitrate < netResource2.bitrate)) {
                netResource = netResource2;
            }
        }
        return netResource;
    }
    
    public NetResource getResource(final MusicQuality musicQuality) {
        if (this.resourceCollection == null) {
            return null;
        }
        final Iterator<NetResource> iterator = this.resourceCollection.iterator();
        NetResource netResource = null;
        while (iterator.hasNext()) {
            final NetResource netResource2 = iterator.next();
            if (netResource2.quality == musicQuality && (netResource == null || netResource.bitrate < netResource2.bitrate)) {
                netResource = netResource2;
            }
        }
        return netResource;
    }
    
    public Collection getResourceCollection() {
        return this.resourceCollection;
    }
    
    public String getResourceStringForDatabase() {
        if (this.resourceCollection == null) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        for (final NetResource netResource : this.resourceCollection) {
            sb.append(netResource.quality.getDiscribe()).append(".").append(netResource.bitrate).append(".");
            sb.append(netResource.format.getDiscribe()).append(".").append(netResource.size).append(";");
        }
        return sb.toString();
    }
    
    public long getStorageId() {
        return this.storageId;
    }
    
    public boolean hasHighMv() {
        if (this.hasMv && this.mvQuality != null) {
            final String[] a = x.a(this.mvQuality, ';');
            for (int length = a.length, i = 0; i < length; ++i) {
                if (a[i].equalsIgnoreCase("MP4")) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean hasLowMv() {
        if (this.hasMv && this.mvQuality != null) {
            final String[] a = x.a(this.mvQuality, ';');
            for (int length = a.length, i = 0; i < length; ++i) {
                if (a[i].equalsIgnoreCase("MP4L")) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public int hashCodeEx() {
        if (this.rid > 0L) {
            return (int)this.rid;
        }
        if (this.filePath == null) {
            return 0;
        }
        return this.filePath.hashCode();
    }
    
    public boolean isEQ() {
        return this.eq;
    }
    
    public boolean isFLAC() {
        return this.flac;
    }
    
    public boolean isLocalFile() {
        return this.rid <= 0L;
    }
    
    public int parseResourceStringFromDatabase(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //     4: ifeq            13
        //     7: iconst_0       
        //     8: istore          4
        //    10: iload           4
        //    12: ireturn        
        //    13: aload_1        
        //    14: bipush          59
        //    16: invokestatic    cn/kuwo/autosdk/x.a:(Ljava/lang/String;C)[Ljava/lang/String;
        //    19: astore_1       
        //    20: aload_1        
        //    21: arraylength    
        //    22: istore          5
        //    24: iconst_0       
        //    25: istore_3       
        //    26: iconst_0       
        //    27: istore_2       
        //    28: iload_2        
        //    29: istore          4
        //    31: iload_3        
        //    32: iload           5
        //    34: if_icmpge       10
        //    37: aload_1        
        //    38: iload_3        
        //    39: aaload         
        //    40: astore          7
        //    42: aload           7
        //    44: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    47: ifeq            63
        //    50: iload_2        
        //    51: istore          4
        //    53: iload_3        
        //    54: iconst_1       
        //    55: iadd           
        //    56: istore_3       
        //    57: iload           4
        //    59: istore_2       
        //    60: goto            28
        //    63: aload           7
        //    65: bipush          46
        //    67: invokestatic    cn/kuwo/autosdk/x.a:(Ljava/lang/String;C)[Ljava/lang/String;
        //    70: astore          7
        //    72: iload_2        
        //    73: istore          4
        //    75: aload           7
        //    77: arraylength    
        //    78: iconst_4       
        //    79: if_icmpne       53
        //    82: aload           7
        //    84: iconst_0       
        //    85: aaload         
        //    86: invokestatic    cn/kuwo/autosdk/bean/MusicQuality.getQualityFromDiscribe:(Ljava/lang/String;)Lcn/kuwo/autosdk/bean/MusicQuality;
        //    89: astore          8
        //    91: aload           7
        //    93: iconst_2       
        //    94: aaload         
        //    95: invokestatic    cn/kuwo/autosdk/bean/MusicFormat.getFormatFromDiscribe:(Ljava/lang/String;)Lcn/kuwo/autosdk/bean/MusicFormat;
        //    98: astore          9
        //   100: aload_0        
        //   101: new             Lcn/kuwo/autosdk/bean/NetResource;
        //   104: dup            
        //   105: aload           8
        //   107: aload           7
        //   109: iconst_1       
        //   110: aaload         
        //   111: invokestatic    java/lang/Integer.valueOf:(Ljava/lang/String;)Ljava/lang/Integer;
        //   114: invokevirtual   java/lang/Integer.intValue:()I
        //   117: aload           9
        //   119: aload           7
        //   121: iconst_3       
        //   122: aaload         
        //   123: invokestatic    java/lang/Integer.valueOf:(Ljava/lang/String;)Ljava/lang/Integer;
        //   126: invokevirtual   java/lang/Integer.intValue:()I
        //   129: invokespecial   cn/kuwo/autosdk/bean/NetResource.<init>:(Lcn/kuwo/autosdk/bean/MusicQuality;ILcn/kuwo/autosdk/bean/MusicFormat;I)V
        //   132: invokevirtual   cn/kuwo/autosdk/bean/Music.addResource:(Lcn/kuwo/autosdk/bean/NetResource;)Z
        //   135: istore          6
        //   137: iload_2        
        //   138: istore          4
        //   140: iload           6
        //   142: ifeq            53
        //   145: iload_2        
        //   146: iconst_1       
        //   147: iadd           
        //   148: istore          4
        //   150: goto            53
        //   153: astore          7
        //   155: aload           7
        //   157: invokevirtual   java/lang/NumberFormatException.printStackTrace:()V
        //   160: iload_2        
        //   161: istore          4
        //   163: goto            53
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  100    137    153    166    Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public int parseResourceStringFromQuku(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //     4: ifeq            9
        //     7: iconst_0       
        //     8: ireturn        
        //     9: aload_1        
        //    10: bipush          59
        //    12: invokestatic    cn/kuwo/autosdk/x.a:(Ljava/lang/String;C)[Ljava/lang/String;
        //    15: astore_1       
        //    16: aload_1        
        //    17: arraylength    
        //    18: istore          7
        //    20: iconst_0       
        //    21: istore          5
        //    23: iconst_0       
        //    24: istore          4
        //    26: iload           5
        //    28: iload           7
        //    30: if_icmplt       36
        //    33: iload           4
        //    35: ireturn        
        //    36: aload_1        
        //    37: iload           5
        //    39: aaload         
        //    40: bipush          44
        //    42: invokestatic    cn/kuwo/autosdk/x.a:(Ljava/lang/String;C)[Ljava/lang/String;
        //    45: astore          11
        //    47: aload           11
        //    49: arraylength    
        //    50: iconst_4       
        //    51: if_icmpne       316
        //    54: aload           11
        //    56: iconst_0       
        //    57: aaload         
        //    58: invokestatic    cn/kuwo/autosdk/x.b:(Ljava/lang/String;)Ljava/lang/String;
        //    61: astore          8
        //    63: aload           11
        //    65: iconst_1       
        //    66: aaload         
        //    67: invokestatic    cn/kuwo/autosdk/x.b:(Ljava/lang/String;)Ljava/lang/String;
        //    70: astore          9
        //    72: aload           11
        //    74: iconst_2       
        //    75: aaload         
        //    76: invokestatic    cn/kuwo/autosdk/x.b:(Ljava/lang/String;)Ljava/lang/String;
        //    79: astore          10
        //    81: aload           11
        //    83: iconst_3       
        //    84: aaload         
        //    85: invokestatic    cn/kuwo/autosdk/x.b:(Ljava/lang/String;)Ljava/lang/String;
        //    88: astore          11
        //    90: aload           8
        //    92: invokestatic    cn/kuwo/autosdk/bean/MusicQuality.getQualityFromDiscribe4Quku:(Ljava/lang/String;)Lcn/kuwo/autosdk/bean/MusicQuality;
        //    95: astore          8
        //    97: aload           9
        //    99: invokestatic    cn/kuwo/autosdk/x.a:(Ljava/lang/String;)Z
        //   102: ifeq            322
        //   105: aload           9
        //   107: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   110: istore          6
        //   112: aload           10
        //   114: invokestatic    cn/kuwo/autosdk/bean/MusicFormat.getFormatFromDiscribe4Quku:(Ljava/lang/String;)Lcn/kuwo/autosdk/bean/MusicFormat;
        //   117: astore          9
        //   119: aload           11
        //   121: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //   124: ldc_w           "KB"
        //   127: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //   130: ifle            208
        //   133: aload           11
        //   135: ldc_w           "(?i)kb"
        //   138: ldc             ""
        //   140: invokevirtual   java/lang/String.replaceAll:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   143: astore          10
        //   145: aload           10
        //   147: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   150: fstore_2       
        //   151: fload_2        
        //   152: ldc_w           1024.0
        //   155: fmul           
        //   156: f2i            
        //   157: istore_3       
        //   158: aload_0        
        //   159: new             Lcn/kuwo/autosdk/bean/NetResource;
        //   162: dup            
        //   163: aload           8
        //   165: iload           6
        //   167: aload           9
        //   169: iload_3        
        //   170: invokespecial   cn/kuwo/autosdk/bean/NetResource.<init>:(Lcn/kuwo/autosdk/bean/MusicQuality;ILcn/kuwo/autosdk/bean/MusicFormat;I)V
        //   173: invokevirtual   cn/kuwo/autosdk/bean/Music.addResource:(Lcn/kuwo/autosdk/bean/NetResource;)Z
        //   176: ifeq            316
        //   179: iload           4
        //   181: iconst_1       
        //   182: iadd           
        //   183: istore_3       
        //   184: iload           5
        //   186: iconst_1       
        //   187: iadd           
        //   188: istore          5
        //   190: iload_3        
        //   191: istore          4
        //   193: goto            26
        //   196: astore          10
        //   198: aload           10
        //   200: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   203: iconst_0       
        //   204: istore_3       
        //   205: goto            158
        //   208: aload           11
        //   210: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //   213: ldc_w           "MB"
        //   216: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //   219: ifle            266
        //   222: aload           11
        //   224: ldc_w           "(?i)mb"
        //   227: ldc             ""
        //   229: invokevirtual   java/lang/String.replaceAll:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   232: astore          10
        //   234: aload           10
        //   236: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   239: fstore_2       
        //   240: fload_2        
        //   241: ldc_w           1024.0
        //   244: fmul           
        //   245: ldc_w           1024.0
        //   248: fmul           
        //   249: f2i            
        //   250: istore_3       
        //   251: goto            158
        //   254: astore          10
        //   256: aload           10
        //   258: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   261: iconst_0       
        //   262: istore_3       
        //   263: goto            158
        //   266: aload           11
        //   268: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //   271: ldc_w           "B"
        //   274: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //   277: ifle            311
        //   280: aload           11
        //   282: ldc_w           "(?i)b"
        //   285: ldc             ""
        //   287: invokevirtual   java/lang/String.replaceAll:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   290: astore          10
        //   292: aload           10
        //   294: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   297: fstore_2       
        //   298: fload_2        
        //   299: f2i            
        //   300: istore_3       
        //   301: goto            158
        //   304: astore          10
        //   306: aload           10
        //   308: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   311: iconst_0       
        //   312: istore_3       
        //   313: goto            158
        //   316: iload           4
        //   318: istore_3       
        //   319: goto            184
        //   322: iconst_0       
        //   323: istore          6
        //   325: goto            112
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  145    151    196    208    Ljava/lang/Exception;
        //  234    240    254    266    Ljava/lang/Exception;
        //  292    298    304    311    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setLocalFileExist(final boolean b) {
        Music$LocalFileState localFileState;
        if (b) {
            localFileState = Music$LocalFileState.EXIST;
        }
        else {
            localFileState = Music$LocalFileState.NOT_EXIST;
        }
        this.localFileState = localFileState;
    }
    
    public void setResourceCollection(final Collection resourceCollection) {
        this.resourceCollection = resourceCollection;
    }
    
    public void setStorageId(final long storageId) {
        if (0L > storageId) {
            return;
        }
        this.storageId = storageId;
    }
    
    public String toDebugString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Name:").append(this.name);
        sb.append(", Artist:").append(this.artist);
        sb.append(", Album:").append(this.album);
        sb.append(", Rid:").append(this.rid);
        sb.append(", Path:").append(this.filePath);
        return sb.toString();
    }
    
    public boolean vaild() {
        return this.rid > 0L || !TextUtils.isEmpty((CharSequence)this.filePath);
    }
}
