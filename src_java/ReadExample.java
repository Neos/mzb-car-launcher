import java.io.*;
import a_vcard.android.syncml.pim.vcard.*;
import a_vcard.android.syncml.pim.*;
import java.util.*;

public class ReadExample
{
    public static void main(final String[] array) throws Exception {
        final VCardParser vCardParser = new VCardParser();
        final VDataBuilder vDataBuilder = new VDataBuilder();
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("example.vcard"), "UTF-8"));
        String string = "";
        while (true) {
            final String line = bufferedReader.readLine();
            if (line == null) {
                break;
            }
            string = string + line + "\n";
        }
        bufferedReader.close();
        if (!vCardParser.parse(string, "UTF-8", vDataBuilder)) {
            throw new VCardException("Could not parse vCard file: " + "example.vcard");
        }
        final Iterator<VNode> iterator = vDataBuilder.vNodeList.iterator();
    Label_0137:
        while (iterator.hasNext()) {
            final ArrayList<PropertyNode> propList = iterator.next().propList;
            final String s = null;
            final Iterator<PropertyNode> iterator2 = propList.iterator();
            while (true) {
                PropertyNode propertyNode;
                do {
                    final String propValue = s;
                    if (!iterator2.hasNext()) {
                        System.out.println("Found contact: " + propValue);
                        continue Label_0137;
                    }
                    propertyNode = iterator2.next();
                } while (!"FN".equals(propertyNode.propName));
                final String propValue = propertyNode.propValue;
                continue;
            }
        }
    }
}
