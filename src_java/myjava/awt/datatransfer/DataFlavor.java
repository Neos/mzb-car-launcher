package myjava.awt.datatransfer;

import org.apache.harmony.awt.internal.nls.*;
import org.apache.harmony.awt.datatransfer.*;
import java.nio.charset.*;
import java.nio.*;
import java.util.*;
import java.io.*;

public class DataFlavor implements Externalizable, Cloneable
{
    public static final DataFlavor javaFileListFlavor;
    public static final String javaJVMLocalObjectMimeType = "application/x-java-jvm-local-objectref";
    public static final String javaRemoteObjectMimeType = "application/x-java-remote-object";
    public static final String javaSerializedObjectMimeType = "application/x-java-serialized-object";
    @Deprecated
    public static final DataFlavor plainTextFlavor;
    private static DataFlavor plainUnicodeFlavor;
    private static final long serialVersionUID = 8367026044764648243L;
    private static final String[] sortedTextFlavors;
    public static final DataFlavor stringFlavor;
    private String humanPresentableName;
    private MimeTypeProcessor.MimeType mimeInfo;
    private Class<?> representationClass;
    
    static {
        plainTextFlavor = new DataFlavor("text/plain; charset=unicode; class=java.io.InputStream", "Plain Text");
        stringFlavor = new DataFlavor("application/x-java-serialized-object; class=java.lang.String", "Unicode String");
        javaFileListFlavor = new DataFlavor("application/x-java-file-list; class=java.util.List", "application/x-java-file-list");
        sortedTextFlavors = new String[] { "text/sgml", "text/xml", "text/html", "text/rtf", "text/enriched", "text/richtext", "text/uri-list", "text/tab-separated-values", "text/t140", "text/rfc822-headers", "text/parityfec", "text/directory", "text/css", "text/calendar", "application/x-java-serialized-object", "text/plain" };
        DataFlavor.plainUnicodeFlavor = null;
    }
    
    public DataFlavor() {
        this.mimeInfo = null;
        this.humanPresentableName = null;
        this.representationClass = null;
    }
    
    public DataFlavor(final Class<?> representationClass, final String humanPresentableName) {
        this.mimeInfo = new MimeTypeProcessor.MimeType("application", "x-java-serialized-object");
        if (humanPresentableName != null) {
            this.humanPresentableName = humanPresentableName;
        }
        else {
            this.humanPresentableName = "application/x-java-serialized-object";
        }
        this.mimeInfo.addParameter("class", representationClass.getName());
        this.representationClass = representationClass;
    }
    
    public DataFlavor(final String s) throws ClassNotFoundException {
        this.init(s, null, null);
    }
    
    public DataFlavor(final String s, final String s2) {
        try {
            this.init(s, s2, null);
        }
        catch (ClassNotFoundException ex) {
            throw new IllegalArgumentException(Messages.getString("awt.16C", this.mimeInfo.getParameter("class")), ex);
        }
    }
    
    public DataFlavor(final String s, final String s2, final ClassLoader classLoader) throws ClassNotFoundException {
        this.init(s, s2, classLoader);
    }
    
    private static List<DataFlavor> fetchTextFlavors(final List<DataFlavor> list, final String s) {
        final LinkedList<DataFlavor> list2 = new LinkedList<DataFlavor>();
        final Iterator<DataFlavor> iterator = list.iterator();
        while (iterator.hasNext()) {
            final DataFlavor dataFlavor = iterator.next();
            if (dataFlavor.isFlavorTextType()) {
                if (!dataFlavor.mimeInfo.getFullType().equals(s)) {
                    continue;
                }
                if (!list2.contains(dataFlavor)) {
                    list2.add(dataFlavor);
                }
                iterator.remove();
            }
            else {
                iterator.remove();
            }
        }
        LinkedList<DataFlavor> list3 = list2;
        if (list2.isEmpty()) {
            list3 = null;
        }
        return list3;
    }
    
    private String getCharset() {
        String s;
        if (this.mimeInfo == null || this.isCharsetRedundant()) {
            s = "";
        }
        else {
            final String parameter = this.mimeInfo.getParameter("charset");
            if (this.isCharsetRequired() && (parameter == null || parameter.length() == 0)) {
                return DTK.getDTK().getDefaultCharset();
            }
            if ((s = parameter) == null) {
                return "";
            }
        }
        return s;
    }
    
    private static List<DataFlavor> getFlavors(List<DataFlavor> list, final Class<?> clazz) {
        final LinkedList<DataFlavor> list2 = new LinkedList<DataFlavor>();
        for (final DataFlavor dataFlavor : list) {
            if (dataFlavor.representationClass.equals(clazz)) {
                list2.add(dataFlavor);
            }
        }
        if (list2.isEmpty()) {
            list = null;
        }
        return list;
    }
    
    private static List<DataFlavor> getFlavors(List<DataFlavor> list, final String[] array) {
        final LinkedList<DataFlavor> list2 = new LinkedList<DataFlavor>();
        final Iterator<DataFlavor> iterator = list.iterator();
        while (iterator.hasNext()) {
            final DataFlavor dataFlavor = iterator.next();
            if (isCharsetSupported(dataFlavor.getCharset())) {
                for (int length = array.length, i = 0; i < length; ++i) {
                    if (Charset.forName(array[i]).equals(Charset.forName(dataFlavor.getCharset()))) {
                        list2.add(dataFlavor);
                    }
                }
            }
            else {
                iterator.remove();
            }
        }
        if (list2.isEmpty()) {
            list = null;
        }
        return list;
    }
    
    private String getKeyInfo() {
        final String string = String.valueOf(this.mimeInfo.getFullType()) + ";class=" + this.representationClass.getName();
        if (!this.mimeInfo.getPrimaryType().equals("text") || this.isUnicodeFlavor()) {
            return string;
        }
        return String.valueOf(string) + ";charset=" + this.getCharset().toLowerCase();
    }
    
    public static final DataFlavor getTextPlainUnicodeFlavor() {
        if (DataFlavor.plainUnicodeFlavor == null) {
            DataFlavor.plainUnicodeFlavor = new DataFlavor("text/plain; charset=" + DTK.getDTK().getDefaultCharset() + "; class=java.io.InputStream", "Plain Text");
        }
        return DataFlavor.plainUnicodeFlavor;
    }
    
    private void init(String representationClass, String humanPresentableName, final ClassLoader classLoader) throws ClassNotFoundException {
    Label_0058_Outer:
        while (true) {
            while (true) {
            Label_0122:
                while (true) {
                    try {
                        this.mimeInfo = MimeTypeProcessor.parse((String)representationClass);
                        if (humanPresentableName != null) {
                            this.humanPresentableName = humanPresentableName;
                            humanPresentableName = (String)(representationClass = this.mimeInfo.getParameter("class"));
                            if (humanPresentableName == null) {
                                representationClass = "java.io.InputStream";
                                this.mimeInfo.addParameter("class", "java.io.InputStream");
                            }
                            if (classLoader == null) {
                                representationClass = Class.forName((String)representationClass);
                                this.representationClass = (Class<?>)representationClass;
                                return;
                            }
                            break Label_0122;
                        }
                    }
                    catch (IllegalArgumentException ex) {
                        throw new IllegalArgumentException(Messages.getString("awt.16D", representationClass));
                    }
                    this.humanPresentableName = String.valueOf(this.mimeInfo.getPrimaryType()) + '/' + this.mimeInfo.getSubType();
                    continue Label_0058_Outer;
                }
                representationClass = classLoader.loadClass((String)representationClass);
                continue;
            }
        }
    }
    
    private boolean isByteCodeFlavor() {
        return this.representationClass != null && (this.representationClass.equals(InputStream.class) || this.representationClass.equals(ByteBuffer.class) || this.representationClass.equals(byte[].class));
    }
    
    private boolean isCharsetRedundant() {
        final String fullType = this.mimeInfo.getFullType();
        return fullType.equals("text/rtf") || fullType.equals("text/tab-separated-values") || fullType.equals("text/t140") || fullType.equals("text/rfc822-headers") || fullType.equals("text/parityfec");
    }
    
    private boolean isCharsetRequired() {
        final String fullType = this.mimeInfo.getFullType();
        return fullType.equals("text/sgml") || fullType.equals("text/xml") || fullType.equals("text/html") || fullType.equals("text/enriched") || fullType.equals("text/richtext") || fullType.equals("text/uri-list") || fullType.equals("text/directory") || fullType.equals("text/css") || fullType.equals("text/calendar") || fullType.equals("application/x-java-serialized-object") || fullType.equals("text/plain");
    }
    
    private static boolean isCharsetSupported(final String s) {
        try {
            return Charset.isSupported(s);
        }
        catch (IllegalCharsetNameException ex) {
            return false;
        }
    }
    
    private boolean isUnicodeFlavor() {
        return this.representationClass != null && (this.representationClass.equals(Reader.class) || this.representationClass.equals(String.class) || this.representationClass.equals(CharBuffer.class) || this.representationClass.equals(char[].class));
    }
    
    private static List<DataFlavor> selectBestByAlphabet(final List<DataFlavor> list) {
        final String[] array = new String[list.size()];
        final LinkedList<DataFlavor> list2 = new LinkedList<DataFlavor>();
        for (int i = 0; i < array.length; ++i) {
            array[i] = list.get(i).getCharset();
        }
        Arrays.sort(array, String.CASE_INSENSITIVE_ORDER);
        for (final DataFlavor dataFlavor : list) {
            if (array[0].equalsIgnoreCase(dataFlavor.getCharset())) {
                list2.add(dataFlavor);
            }
        }
        LinkedList<DataFlavor> list3 = list2;
        if (list2.isEmpty()) {
            list3 = null;
        }
        return list3;
    }
    
    private static DataFlavor selectBestByCharset(final List<DataFlavor> list) {
        List<DataFlavor> list2;
        if ((list2 = getFlavors(list, new String[] { "UTF-16", "UTF-8", "UTF-16BE", "UTF-16LE" })) == null && (list2 = getFlavors(list, new String[] { DTK.getDTK().getDefaultCharset() })) == null && (list2 = getFlavors(list, new String[] { "US-ASCII" })) == null) {
            list2 = selectBestByAlphabet(list);
        }
        if (list2 == null) {
            return null;
        }
        if (list2.size() == 1) {
            return list2.get(0);
        }
        return selectBestFlavorWOCharset(list2);
    }
    
    private static DataFlavor selectBestFlavorWCharset(final List<DataFlavor> list) {
        final List<DataFlavor> flavors = getFlavors(list, Reader.class);
        if (flavors != null) {
            return flavors.get(0);
        }
        final List<DataFlavor> flavors2 = getFlavors(list, String.class);
        if (flavors2 != null) {
            return flavors2.get(0);
        }
        final List<DataFlavor> flavors3 = getFlavors(list, CharBuffer.class);
        if (flavors3 != null) {
            return flavors3.get(0);
        }
        final List<DataFlavor> flavors4 = getFlavors(list, char[].class);
        if (flavors4 != null) {
            return flavors4.get(0);
        }
        return selectBestByCharset(list);
    }
    
    private static DataFlavor selectBestFlavorWOCharset(final List<DataFlavor> list) {
        final List<DataFlavor> flavors = getFlavors(list, InputStream.class);
        if (flavors != null) {
            return flavors.get(0);
        }
        final List<DataFlavor> flavors2 = getFlavors(list, ByteBuffer.class);
        if (flavors2 != null) {
            return flavors2.get(0);
        }
        final List<DataFlavor> flavors3 = getFlavors(list, byte[].class);
        if (flavors3 != null) {
            return flavors3.get(0);
        }
        return list.get(0);
    }
    
    public static final DataFlavor selectBestTextFlavor(final DataFlavor[] array) {
        if (array != null) {
            final List<List<DataFlavor>> sortTextFlavorsByType = sortTextFlavorsByType(new LinkedList<DataFlavor>(Arrays.asList(array)));
            if (!sortTextFlavorsByType.isEmpty()) {
                final List<DataFlavor> list = sortTextFlavorsByType.get(0);
                if (list.size() == 1) {
                    return list.get(0);
                }
                if (list.get(0).getCharset().length() == 0) {
                    return selectBestFlavorWOCharset(list);
                }
                return selectBestFlavorWCharset(list);
            }
        }
        return null;
    }
    
    private static List<List<DataFlavor>> sortTextFlavorsByType(final List<DataFlavor> list) {
        final LinkedList<List<DataFlavor>> list2 = new LinkedList<List<DataFlavor>>();
        final String[] sortedTextFlavors = DataFlavor.sortedTextFlavors;
        for (int length = sortedTextFlavors.length, i = 0; i < length; ++i) {
            final List<DataFlavor> fetchTextFlavors = fetchTextFlavors(list, sortedTextFlavors[i]);
            if (fetchTextFlavors != null) {
                list2.addLast(fetchTextFlavors);
            }
        }
        if (!list.isEmpty()) {
            list2.addLast(list);
        }
        return list2;
    }
    
    protected static final Class<?> tryToLoadClass(final String s, final ClassLoader classLoader) throws ClassNotFoundException {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            try {
                return ClassLoader.getSystemClassLoader().loadClass(s);
            }
            catch (ClassNotFoundException ex2) {
                final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                if (contextClassLoader != null) {
                    try {
                        return contextClassLoader.loadClass(s);
                    }
                    catch (ClassNotFoundException ex3) {}
                }
                return classLoader.loadClass(s);
            }
        }
    }
    
    public Object clone() throws CloneNotSupportedException {
        final DataFlavor dataFlavor = new DataFlavor();
        dataFlavor.humanPresentableName = this.humanPresentableName;
        dataFlavor.representationClass = this.representationClass;
        MimeTypeProcessor.MimeType mimeInfo;
        if (this.mimeInfo != null) {
            mimeInfo = (MimeTypeProcessor.MimeType)this.mimeInfo.clone();
        }
        else {
            mimeInfo = null;
        }
        dataFlavor.mimeInfo = mimeInfo;
        return dataFlavor;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o != null && o instanceof DataFlavor && this.equals((DataFlavor)o);
    }
    
    @Deprecated
    public boolean equals(final String s) {
        return s != null && this.isMimeTypeEqual(s);
    }
    
    public boolean equals(final DataFlavor dataFlavor) {
        if (dataFlavor != this) {
            if (dataFlavor == null) {
                return false;
            }
            if (this.mimeInfo == null) {
                if (dataFlavor.mimeInfo != null) {
                    return false;
                }
            }
            else {
                if (!this.mimeInfo.equals(dataFlavor.mimeInfo) || !this.representationClass.equals(dataFlavor.representationClass)) {
                    return false;
                }
                if (this.mimeInfo.getPrimaryType().equals("text") && !this.isUnicodeFlavor()) {
                    final String charset = this.getCharset();
                    final String charset2 = dataFlavor.getCharset();
                    if (!isCharsetSupported(charset) || !isCharsetSupported(charset2)) {
                        return charset.equalsIgnoreCase(charset2);
                    }
                    return Charset.forName(charset).equals(Charset.forName(charset2));
                }
            }
        }
        return true;
    }
    
    public final Class<?> getDefaultRepresentationClass() {
        return InputStream.class;
    }
    
    public final String getDefaultRepresentationClassAsString() {
        return this.getDefaultRepresentationClass().getName();
    }
    
    public String getHumanPresentableName() {
        return this.humanPresentableName;
    }
    
    MimeTypeProcessor.MimeType getMimeInfo() {
        return this.mimeInfo;
    }
    
    public String getMimeType() {
        if (this.mimeInfo != null) {
            return MimeTypeProcessor.assemble(this.mimeInfo);
        }
        return null;
    }
    
    public String getParameter(String lowerCase) {
        lowerCase = lowerCase.toLowerCase();
        if (lowerCase.equals("humanpresentablename")) {
            return this.humanPresentableName;
        }
        if (this.mimeInfo != null) {
            return this.mimeInfo.getParameter(lowerCase);
        }
        return null;
    }
    
    public String getPrimaryType() {
        if (this.mimeInfo != null) {
            return this.mimeInfo.getPrimaryType();
        }
        return null;
    }
    
    public Reader getReaderForText(final Transferable transferable) throws UnsupportedFlavorException, IOException {
        final Object transferData = transferable.getTransferData(this);
        if (transferData == null) {
            throw new IllegalArgumentException(Messages.getString("awt.16E"));
        }
        if (transferData instanceof Reader) {
            final Reader reader = (Reader)transferData;
            reader.reset();
            return reader;
        }
        if (transferData instanceof String) {
            return new StringReader((String)transferData);
        }
        if (transferData instanceof CharBuffer) {
            return new CharArrayReader(((CharBuffer)transferData).array());
        }
        if (transferData instanceof char[]) {
            return new CharArrayReader((char[])transferData);
        }
        final String charset = this.getCharset();
        InputStream inputStream;
        if (transferData instanceof InputStream) {
            inputStream = (InputStream)transferData;
            inputStream.reset();
        }
        else if (transferData instanceof ByteBuffer) {
            inputStream = new ByteArrayInputStream(((ByteBuffer)transferData).array());
        }
        else {
            if (!(transferData instanceof byte[])) {
                throw new IllegalArgumentException(Messages.getString("awt.16F"));
            }
            inputStream = new ByteArrayInputStream((byte[])transferData);
        }
        if (charset.length() == 0) {
            return new InputStreamReader(inputStream);
        }
        return new InputStreamReader(inputStream, charset);
    }
    
    public Class<?> getRepresentationClass() {
        return this.representationClass;
    }
    
    public String getSubType() {
        if (this.mimeInfo != null) {
            return this.mimeInfo.getSubType();
        }
        return null;
    }
    
    @Override
    public int hashCode() {
        return this.getKeyInfo().hashCode();
    }
    
    public boolean isFlavorJavaFileListType() {
        return List.class.isAssignableFrom(this.representationClass) && this.isMimeTypeEqual(DataFlavor.javaFileListFlavor);
    }
    
    public boolean isFlavorRemoteObjectType() {
        return this.isMimeTypeEqual("application/x-java-remote-object") && this.isRepresentationClassRemote();
    }
    
    public boolean isFlavorSerializedObjectType() {
        return this.isMimeTypeSerializedObject() && this.isRepresentationClassSerializable();
    }
    
    public boolean isFlavorTextType() {
        if (!this.equals(DataFlavor.stringFlavor) && !this.equals(DataFlavor.plainTextFlavor)) {
            if (this.mimeInfo != null && !this.mimeInfo.getPrimaryType().equals("text")) {
                return false;
            }
            final String charset = this.getCharset();
            if (!this.isByteCodeFlavor()) {
                return this.isUnicodeFlavor();
            }
            if (charset.length() != 0) {
                return isCharsetSupported(charset);
            }
        }
        return true;
    }
    
    public boolean isMimeTypeEqual(final String s) {
        try {
            return this.mimeInfo.equals(MimeTypeProcessor.parse(s));
        }
        catch (IllegalArgumentException ex) {
            return false;
        }
    }
    
    public final boolean isMimeTypeEqual(final DataFlavor dataFlavor) {
        if (this.mimeInfo != null) {
            return this.mimeInfo.equals(dataFlavor.mimeInfo);
        }
        return dataFlavor.mimeInfo == null;
    }
    
    public boolean isMimeTypeSerializedObject() {
        return this.isMimeTypeEqual("application/x-java-serialized-object");
    }
    
    public boolean isRepresentationClassByteBuffer() {
        return ByteBuffer.class.isAssignableFrom(this.representationClass);
    }
    
    public boolean isRepresentationClassCharBuffer() {
        return CharBuffer.class.isAssignableFrom(this.representationClass);
    }
    
    public boolean isRepresentationClassInputStream() {
        return InputStream.class.isAssignableFrom(this.representationClass);
    }
    
    public boolean isRepresentationClassReader() {
        return Reader.class.isAssignableFrom(this.representationClass);
    }
    
    public boolean isRepresentationClassRemote() {
        return false;
    }
    
    public boolean isRepresentationClassSerializable() {
        return Serializable.class.isAssignableFrom(this.representationClass);
    }
    
    public boolean match(final DataFlavor dataFlavor) {
        return this.equals(dataFlavor);
    }
    
    @Deprecated
    protected String normalizeMimeType(final String s) {
        return s;
    }
    
    @Deprecated
    protected String normalizeMimeTypeParameter(final String s, final String s2) {
        return s2;
    }
    
    @Override
    public void readExternal(final ObjectInput objectInput) throws IOException, ClassNotFoundException {
        synchronized (this) {
            this.humanPresentableName = (String)objectInput.readObject();
            this.mimeInfo = (MimeTypeProcessor.MimeType)objectInput.readObject();
            Class<?> forName;
            if (this.mimeInfo != null) {
                forName = Class.forName(this.mimeInfo.getParameter("class"));
            }
            else {
                forName = null;
            }
            this.representationClass = forName;
        }
    }
    
    public void setHumanPresentableName(final String humanPresentableName) {
        this.humanPresentableName = humanPresentableName;
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.getClass().getName()) + "[MimeType=(" + this.getMimeType() + ");humanPresentableName=" + this.humanPresentableName + "]";
    }
    
    @Override
    public void writeExternal(final ObjectOutput objectOutput) throws IOException {
        synchronized (this) {
            objectOutput.writeObject(this.humanPresentableName);
            objectOutput.writeObject(this.mimeInfo);
        }
    }
}
