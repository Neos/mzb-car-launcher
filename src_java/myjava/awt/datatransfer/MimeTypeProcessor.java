package myjava.awt.datatransfer;

import java.util.*;
import java.io.*;

final class MimeTypeProcessor
{
    private static MimeTypeProcessor instance;
    
    static String assemble(final MimeType mimeType) {
        final StringBuilder sb = new StringBuilder();
        sb.append(mimeType.getFullType());
        final Enumeration<String> keys = (Enumeration<String>)mimeType.parameters.keys();
        while (keys.hasMoreElements()) {
            final String s = keys.nextElement();
            final String s2 = mimeType.parameters.get(s);
            sb.append("; ");
            sb.append(s);
            sb.append("=\"");
            sb.append(s2);
            sb.append('\"');
        }
        return sb.toString();
    }
    
    private static int getNextMeaningfulIndex(final String s, int n) {
        while (n < s.length() && !isMeaningfulChar(s.charAt(n))) {
            ++n;
        }
        return n;
    }
    
    private static boolean isMeaningfulChar(final char c) {
        return c >= '!' && c <= '~';
    }
    
    private static boolean isTSpecialChar(final char c) {
        return c == '(' || c == ')' || c == '[' || c == ']' || c == '<' || c == '>' || c == '@' || c == ',' || c == ';' || c == ':' || c == '\\' || c == '\"' || c == '/' || c == '?' || c == '=';
    }
    
    static MimeType parse(final String s) {
        if (MimeTypeProcessor.instance == null) {
            MimeTypeProcessor.instance = new MimeTypeProcessor();
        }
        final MimeType mimeType = new MimeType();
        if (s != null) {
            final StringPosition stringPosition = new StringPosition(null);
            retrieveType(s, mimeType, stringPosition);
            retrieveParams(s, mimeType, stringPosition);
        }
        return mimeType;
    }
    
    private static void retrieveParam(String s, final MimeType mimeType, final StringPosition stringPosition) {
        final String lowerCase = retrieveToken(s, stringPosition).toLowerCase();
        stringPosition.i = getNextMeaningfulIndex(s, stringPosition.i);
        if (stringPosition.i >= s.length() || s.charAt(stringPosition.i) != '=') {
            throw new IllegalArgumentException();
        }
        ++stringPosition.i;
        stringPosition.i = getNextMeaningfulIndex(s, stringPosition.i);
        if (stringPosition.i >= s.length()) {
            throw new IllegalArgumentException();
        }
        if (s.charAt(stringPosition.i) == '\"') {
            s = retrieveQuoted(s, stringPosition);
        }
        else {
            s = retrieveToken(s, stringPosition);
        }
        mimeType.parameters.put(lowerCase, s);
    }
    
    private static void retrieveParams(final String s, final MimeType mimeType, final StringPosition stringPosition) {
        MimeType.access$3(mimeType, new Hashtable());
        MimeType.access$4(mimeType, new Hashtable());
        while (true) {
            stringPosition.i = getNextMeaningfulIndex(s, stringPosition.i);
            if (stringPosition.i >= s.length()) {
                return;
            }
            if (s.charAt(stringPosition.i) != ';') {
                throw new IllegalArgumentException();
            }
            ++stringPosition.i;
            retrieveParam(s, mimeType, stringPosition);
        }
    }
    
    private static String retrieveQuoted(final String s, final StringPosition stringPosition) {
        final StringBuilder sb = new StringBuilder();
        int n = 1;
        ++stringPosition.i;
        while (s.charAt(stringPosition.i) != '\"' || n == 0) {
            final char char1 = s.charAt(stringPosition.i++);
            if (n == 0) {
                n = 1;
            }
            else if (char1 == '\\') {
                n = 0;
            }
            if (n != 0) {
                sb.append(char1);
            }
            if (stringPosition.i == s.length()) {
                throw new IllegalArgumentException();
            }
        }
        ++stringPosition.i;
        return sb.toString();
    }
    
    private static String retrieveToken(final String s, final StringPosition stringPosition) {
        final StringBuilder sb = new StringBuilder();
        stringPosition.i = getNextMeaningfulIndex(s, stringPosition.i);
        if (stringPosition.i >= s.length() || isTSpecialChar(s.charAt(stringPosition.i))) {
            throw new IllegalArgumentException();
        }
        do {
            sb.append(s.charAt(stringPosition.i++));
        } while (stringPosition.i < s.length() && isMeaningfulChar(s.charAt(stringPosition.i)) && !isTSpecialChar(s.charAt(stringPosition.i)));
        return sb.toString();
    }
    
    private static void retrieveType(final String s, final MimeType mimeType, final StringPosition stringPosition) {
        MimeType.access$1(mimeType, retrieveToken(s, stringPosition).toLowerCase());
        stringPosition.i = getNextMeaningfulIndex(s, stringPosition.i);
        if (stringPosition.i >= s.length() || s.charAt(stringPosition.i) != '/') {
            throw new IllegalArgumentException();
        }
        ++stringPosition.i;
        MimeType.access$2(mimeType, retrieveToken(s, stringPosition).toLowerCase());
    }
    
    static final class MimeType implements Cloneable, Serializable
    {
        private static final long serialVersionUID = -6693571907475992044L;
        private Hashtable<String, String> parameters;
        private String primaryType;
        private String subType;
        private Hashtable<String, Object> systemParameters;
        
        MimeType() {
            this.primaryType = null;
            this.subType = null;
            this.parameters = null;
            this.systemParameters = null;
        }
        
        MimeType(final String primaryType, final String subType) {
            this.primaryType = primaryType;
            this.subType = subType;
            this.parameters = new Hashtable<String, String>();
            this.systemParameters = new Hashtable<String, Object>();
        }
        
        static /* synthetic */ void access$1(final MimeType mimeType, final String primaryType) {
            mimeType.primaryType = primaryType;
        }
        
        static /* synthetic */ void access$2(final MimeType mimeType, final String subType) {
            mimeType.subType = subType;
        }
        
        static /* synthetic */ void access$3(final MimeType mimeType, final Hashtable parameters) {
            mimeType.parameters = (Hashtable<String, String>)parameters;
        }
        
        static /* synthetic */ void access$4(final MimeType mimeType, final Hashtable systemParameters) {
            mimeType.systemParameters = (Hashtable<String, Object>)systemParameters;
        }
        
        void addParameter(final String s, final String s2) {
            if (s2 != null) {
                String substring = s2;
                if (s2.charAt(0) == '\"') {
                    substring = s2;
                    if (s2.charAt(s2.length() - 1) == '\"') {
                        substring = s2.substring(1, s2.length() - 2);
                    }
                }
                if (substring.length() != 0) {
                    this.parameters.put(s, substring);
                }
            }
        }
        
        void addSystemParameter(final String s, final Object o) {
            this.systemParameters.put(s, o);
        }
        
        public Object clone() {
            final MimeType mimeType = new MimeType(this.primaryType, this.subType);
            mimeType.parameters = (Hashtable<String, String>)this.parameters.clone();
            mimeType.systemParameters = (Hashtable<String, Object>)this.systemParameters.clone();
            return mimeType;
        }
        
        boolean equals(final MimeType mimeType) {
            return mimeType != null && this.getFullType().equals(mimeType.getFullType());
        }
        
        String getFullType() {
            return String.valueOf(this.primaryType) + "/" + this.subType;
        }
        
        String getParameter(final String s) {
            return this.parameters.get(s);
        }
        
        String getPrimaryType() {
            return this.primaryType;
        }
        
        String getSubType() {
            return this.subType;
        }
        
        Object getSystemParameter(final String s) {
            return this.systemParameters.get(s);
        }
        
        void removeParameter(final String s) {
            this.parameters.remove(s);
        }
    }
    
    private static final class StringPosition
    {
        int i;
        
        private StringPosition() {
            this.i = 0;
        }
    }
}
