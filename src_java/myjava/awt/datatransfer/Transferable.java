package myjava.awt.datatransfer;

import java.io.*;

public interface Transferable
{
    Object getTransferData(final DataFlavor p0) throws UnsupportedFlavorException, IOException;
    
    DataFlavor[] getTransferDataFlavors();
    
    boolean isDataFlavorSupported(final DataFlavor p0);
}
