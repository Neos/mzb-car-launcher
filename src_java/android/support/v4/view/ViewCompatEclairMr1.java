package android.support.v4.view;

import android.view.*;

class ViewCompatEclairMr1
{
    public static boolean isOpaque(final View view) {
        return view.isOpaque();
    }
}
