package android.support.v4.app;

import android.view.*;

interface FragmentContainer
{
    View findViewById(final int p0);
}
