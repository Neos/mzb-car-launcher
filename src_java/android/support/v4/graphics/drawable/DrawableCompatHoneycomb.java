package android.support.v4.graphics.drawable;

import android.graphics.drawable.*;

class DrawableCompatHoneycomb
{
    public static void jumpToCurrentState(final Drawable drawable) {
        drawable.jumpToCurrentState();
    }
}
