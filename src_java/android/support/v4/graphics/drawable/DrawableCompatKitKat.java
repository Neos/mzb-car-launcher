package android.support.v4.graphics.drawable;

import android.graphics.drawable.*;

class DrawableCompatKitKat
{
    public static boolean isAutoMirrored(final Drawable drawable) {
        return drawable.isAutoMirrored();
    }
    
    public static void setAutoMirrored(final Drawable drawable, final boolean autoMirrored) {
        drawable.setAutoMirrored(autoMirrored);
    }
}
