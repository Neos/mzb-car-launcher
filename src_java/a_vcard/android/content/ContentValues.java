package a_vcard.android.content;

import a_vcard.android.util.*;
import java.util.*;
import java.io.*;

public final class ContentValues
{
    public static final String TAG = "ContentValues";
    private HashMap<String, Object> mValues;
    
    public ContentValues() {
        this.mValues = new HashMap<String, Object>(8);
    }
    
    public ContentValues(final int n) {
        this.mValues = new HashMap<String, Object>(n, 1.0f);
    }
    
    public ContentValues(final ContentValues contentValues) {
        this.mValues = new HashMap<String, Object>(contentValues.mValues);
    }
    
    private ContentValues(final HashMap<String, Object> mValues) {
        this.mValues = mValues;
    }
    
    public void clear() {
        this.mValues.clear();
    }
    
    public boolean containsKey(final String s) {
        return this.mValues.containsKey(s);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ContentValues && this.mValues.equals(((ContentValues)o).mValues);
    }
    
    public Object get(final String s) {
        return this.mValues.get(s);
    }
    
    public Boolean getAsBoolean(final String s) {
        final Boolean value = this.mValues.get(s);
        try {
            return value;
        }
        catch (ClassCastException ex) {
            if (value instanceof CharSequence) {
                return Boolean.valueOf(value.toString());
            }
            Log.e("ContentValues", "Cannot cast value for " + s + " to a Boolean");
            return null;
        }
    }
    
    public Byte getAsByte(String value) {
        final Number value2 = this.mValues.get(value);
        Label_0028: {
            if (value2 == null) {
                break Label_0028;
            }
            try {
                value = value2.byteValue();
                return (Byte)value;
                value = null;
                return (Byte)value;
            }
            catch (ClassCastException ex) {
                if (value2 instanceof CharSequence) {
                    try {
                        return Byte.valueOf(value2.toString());
                    }
                    catch (NumberFormatException ex2) {
                        Log.e("ContentValues", "Cannot parse Byte value for " + value2 + " at key " + (String)value);
                        return null;
                    }
                }
                Log.e("ContentValues", "Cannot cast value for " + (String)value + " to a Byte");
                return null;
            }
        }
    }
    
    public byte[] getAsByteArray(final String s) {
        final byte[] value = this.mValues.get(s);
        if (value instanceof byte[]) {
            return value;
        }
        return null;
    }
    
    public Double getAsDouble(String value) {
        final Number value2 = this.mValues.get(value);
        Label_0031: {
            if (value2 == null) {
                break Label_0031;
            }
            try {
                value = value2.doubleValue();
                return (Double)value;
                value = null;
                return (Double)value;
            }
            catch (ClassCastException ex) {
                if (value2 instanceof CharSequence) {
                    try {
                        return Double.valueOf(value2.toString());
                    }
                    catch (NumberFormatException ex2) {
                        Log.e("ContentValues", "Cannot parse Double value for " + value2 + " at key " + (String)value);
                        return null;
                    }
                }
                Log.e("ContentValues", "Cannot cast value for " + (String)value + " to a Double");
                return null;
            }
        }
    }
    
    public Float getAsFloat(String value) {
        final Number value2 = this.mValues.get(value);
        Label_0028: {
            if (value2 == null) {
                break Label_0028;
            }
            try {
                value = value2.floatValue();
                return (Float)value;
                value = null;
                return (Float)value;
            }
            catch (ClassCastException ex) {
                if (value2 instanceof CharSequence) {
                    try {
                        return Float.valueOf(value2.toString());
                    }
                    catch (NumberFormatException ex2) {
                        Log.e("ContentValues", "Cannot parse Float value for " + value2 + " at key " + (String)value);
                        return null;
                    }
                }
                Log.e("ContentValues", "Cannot cast value for " + (String)value + " to a Float");
                return null;
            }
        }
    }
    
    public Integer getAsInteger(String value) {
        final Number value2 = this.mValues.get(value);
        Label_0028: {
            if (value2 == null) {
                break Label_0028;
            }
            try {
                value = value2.intValue();
                return (Integer)value;
                value = null;
                return (Integer)value;
            }
            catch (ClassCastException ex) {
                if (value2 instanceof CharSequence) {
                    try {
                        return Integer.valueOf(value2.toString());
                    }
                    catch (NumberFormatException ex2) {
                        Log.e("ContentValues", "Cannot parse Integer value for " + value2 + " at key " + (String)value);
                        return null;
                    }
                }
                Log.e("ContentValues", "Cannot cast value for " + (String)value + " to a Integer");
                return null;
            }
        }
    }
    
    public Long getAsLong(String value) {
        final Number value2 = this.mValues.get(value);
        Label_0031: {
            if (value2 == null) {
                break Label_0031;
            }
            try {
                value = value2.longValue();
                return (Long)value;
                value = null;
                return (Long)value;
            }
            catch (ClassCastException ex) {
                if (value2 instanceof CharSequence) {
                    try {
                        return Long.valueOf(value2.toString());
                    }
                    catch (NumberFormatException ex2) {
                        Log.e("ContentValues", "Cannot parse Long value for " + value2 + " at key " + (String)value);
                        return null;
                    }
                }
                Log.e("ContentValues", "Cannot cast value for " + (String)value + " to a Long");
                return null;
            }
        }
    }
    
    public Short getAsShort(String value) {
        final Number value2 = this.mValues.get(value);
        Label_0028: {
            if (value2 == null) {
                break Label_0028;
            }
            try {
                value = value2.shortValue();
                return (Short)value;
                value = null;
                return (Short)value;
            }
            catch (ClassCastException ex) {
                if (value2 instanceof CharSequence) {
                    try {
                        return Short.valueOf(value2.toString());
                    }
                    catch (NumberFormatException ex2) {
                        Log.e("ContentValues", "Cannot parse Short value for " + value2 + " at key " + (String)value);
                        return null;
                    }
                }
                Log.e("ContentValues", "Cannot cast value for " + (String)value + " to a Short");
                return null;
            }
        }
    }
    
    public String getAsString(final String s) {
        if (this.mValues.get(s) != null) {
            return this.mValues.get(s).toString();
        }
        return null;
    }
    
    @Deprecated
    public ArrayList<String> getStringArrayList(final String s) {
        return this.mValues.get(s);
    }
    
    @Override
    public int hashCode() {
        return this.mValues.hashCode();
    }
    
    public void put(final String s, final Boolean b) {
        this.mValues.put(s, b);
    }
    
    public void put(final String s, final Byte b) {
        this.mValues.put(s, b);
    }
    
    public void put(final String s, final Double n) {
        this.mValues.put(s, n);
    }
    
    public void put(final String s, final Float n) {
        this.mValues.put(s, n);
    }
    
    public void put(final String s, final Integer n) {
        this.mValues.put(s, n);
    }
    
    public void put(final String s, final Long n) {
        this.mValues.put(s, n);
    }
    
    public void put(final String s, final Short n) {
        this.mValues.put(s, n);
    }
    
    public void put(final String s, final String s2) {
        this.mValues.put(s, s2);
    }
    
    public void put(final String s, final byte[] array) {
        this.mValues.put(s, array);
    }
    
    public void putAll(final ContentValues contentValues) {
        this.mValues.putAll(contentValues.mValues);
    }
    
    public void putNull(final String s) {
        this.mValues.put(s, null);
    }
    
    @Deprecated
    public void putStringArrayList(final String s, final ArrayList<String> list) {
        this.mValues.put(s, list);
    }
    
    public void remove(final String s) {
        this.mValues.remove(s);
    }
    
    public int size() {
        return this.mValues.size();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (final String s : this.mValues.keySet()) {
            final String asString = this.getAsString(s);
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(s + "=" + asString);
        }
        return sb.toString();
    }
    
    public Set<Map.Entry<String, Object>> valueSet() {
        return this.mValues.entrySet();
    }
}
