package a_vcard.android.text;

import java.io.*;

public class TextUtils
{
    public static void getChars(final CharSequence charSequence, int i, final int n, final char[] array, int n2) {
        final Class<? extends CharSequence> class1 = charSequence.getClass();
        if (class1 == String.class) {
            ((String)charSequence).getChars(i, n, array, n2);
            return;
        }
        if (class1 == StringBuffer.class) {
            ((StringBuffer)charSequence).getChars(i, n, array, n2);
            return;
        }
        if (class1 == StringBuilder.class) {
            ((StringBuilder)charSequence).getChars(i, n, array, n2);
            return;
        }
        if (charSequence instanceof GetChars) {
            ((GetChars)charSequence).getChars(i, n, array, n2);
            return;
        }
        while (i < n) {
            array[n2] = charSequence.charAt(i);
            ++i;
            ++n2;
        }
    }
    
    public static boolean isEmpty(final CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }
}
