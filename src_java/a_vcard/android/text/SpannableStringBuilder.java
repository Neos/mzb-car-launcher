package a_vcard.android.text;

import a_vcard.com.android.internal.util.*;
import java.io.*;
import java.lang.reflect.*;

public class SpannableStringBuilder implements Spannable, Editable
{
    private static final int END_MASK = 15;
    private static final int MARK = 1;
    private static final InputFilter[] NO_FILTERS;
    private static final int PARAGRAPH = 3;
    private static final int POINT = 2;
    private static final int START_MASK = 240;
    private static final int START_SHIFT = 4;
    private InputFilter[] mFilters;
    private int mGapLength;
    private int mGapStart;
    private int mSpanCount;
    private int[] mSpanEnds;
    private int[] mSpanFlags;
    private int[] mSpanStarts;
    private Object[] mSpans;
    private char[] mText;
    
    static {
        NO_FILTERS = new InputFilter[0];
    }
    
    public SpannableStringBuilder() {
        this("");
    }
    
    public SpannableStringBuilder(final CharSequence charSequence) {
        this(charSequence, 0, charSequence.length());
    }
    
    public SpannableStringBuilder(final CharSequence charSequence, final int n, final int n2) {
        this.mFilters = SpannableStringBuilder.NO_FILTERS;
        final int mGapStart = n2 - n;
        final int idealCharArraySize = ArrayUtils.idealCharArraySize(mGapStart + 1);
        this.mText = new char[idealCharArraySize];
        this.mGapStart = mGapStart;
        this.mGapLength = idealCharArraySize - mGapStart;
        TextUtils.getChars(charSequence, n, n2, this.mText, 0);
        this.mSpanCount = 0;
        final int idealIntArraySize = ArrayUtils.idealIntArraySize(0);
        this.mSpans = new Object[idealIntArraySize];
        this.mSpanStarts = new int[idealIntArraySize];
        this.mSpanEnds = new int[idealIntArraySize];
        this.mSpanFlags = new int[idealIntArraySize];
        if (charSequence instanceof Spanned) {
            final Spanned spanned = (Spanned)charSequence;
            final Object[] spans = spanned.getSpans(n, n2, Object.class);
            for (int i = 0; i < spans.length; ++i) {
                if (!(spans[i] instanceof NoCopySpan)) {
                    final int n3 = spanned.getSpanStart(spans[i]) - n;
                    final int n4 = spanned.getSpanEnd(spans[i]) - n;
                    final int spanFlags = spanned.getSpanFlags(spans[i]);
                    int n5;
                    if ((n5 = n3) < 0) {
                        n5 = 0;
                    }
                    int n6;
                    if ((n6 = n5) > n2 - n) {
                        n6 = n2 - n;
                    }
                    int n7;
                    if ((n7 = n4) < 0) {
                        n7 = 0;
                    }
                    int n8;
                    if ((n8 = n7) > n2 - n) {
                        n8 = n2 - n;
                    }
                    this.setSpan(spans[i], n6, n8, spanFlags);
                }
            }
        }
    }
    
    private int change(final int n, final int n2, final CharSequence charSequence, final int n3, final int n4) {
        return this.change(true, n, n2, charSequence, n3, n4);
    }
    
    private int change(final boolean b, final int n, final int n2, final CharSequence charSequence, final int n3, final int n4) {
        this.checkRange("replace", n, n2);
        final int n5 = n4 - n3;
        TextWatcher[] sendTextWillChange = null;
        if (b) {
            sendTextWillChange = this.sendTextWillChange(n, n2 - n, n4 - n3);
        }
        for (int i = this.mSpanCount - 1; i >= 0; --i) {
            if ((this.mSpanFlags[i] & 0x33) == 0x33) {
                final int n6 = this.mSpanStarts[i];
                int n7;
                if ((n7 = n6) > this.mGapStart) {
                    n7 = n6 - this.mGapLength;
                }
                final int n8 = this.mSpanEnds[i];
                int n9;
                if ((n9 = n8) > this.mGapStart) {
                    n9 = n8 - this.mGapLength;
                }
                final int length = this.length();
                int n10 = n7;
                if (n7 > n && (n10 = n7) <= n2) {
                    int n11 = n2;
                    while (true) {
                        n10 = n11;
                        if (n11 >= length) {
                            break;
                        }
                        if (n11 > n2 && this.charAt(n11 - 1) == '\n') {
                            n10 = n11;
                            break;
                        }
                        ++n11;
                    }
                }
                final int n12 = n10;
                int n13;
                if ((n13 = n9) > n && (n13 = n9) <= n2) {
                    int n14 = n2;
                    while (true) {
                        n13 = n14;
                        if (n14 >= length) {
                            break;
                        }
                        if (n14 > n2 && this.charAt(n14 - 1) == '\n') {
                            n13 = n14;
                            break;
                        }
                        ++n14;
                    }
                }
                if (n12 != n7 || n13 != n9) {
                    this.setSpan(this.mSpans[i], n12, n13, this.mSpanFlags[i]);
                }
            }
        }
        this.moveGapTo(n2);
        if (n4 - n3 >= this.mGapLength + (n2 - n)) {
            this.resizeFor(this.mText.length - this.mGapLength + n4 - n3 - (n2 - n));
        }
        this.mGapStart += n4 - n3 - (n2 - n);
        this.mGapLength -= n4 - n3 - (n2 - n);
        if (this.mGapLength < 1) {
            new Exception("mGapLength < 1").printStackTrace();
        }
        TextUtils.getChars(charSequence, n3, n4, this.mText, n);
        if (charSequence instanceof Spanned) {
            final Spanned spanned = (Spanned)charSequence;
            final Object[] spans = spanned.getSpans(n3, n4, Object.class);
            for (int j = 0; j < spans.length; ++j) {
                final int spanStart = spanned.getSpanStart(spans[j]);
                final int spanEnd = spanned.getSpanEnd(spans[j]);
                int n15;
                if ((n15 = spanStart) < n3) {
                    n15 = n3;
                }
                int n16;
                if ((n16 = spanEnd) > n4) {
                    n16 = n4;
                }
                if (this.getSpanStart(spans[j]) < 0) {
                    this.setSpan(false, spans[j], n15 - n3 + n, n16 - n3 + n, spanned.getSpanFlags(spans[j]));
                }
            }
        }
        if (n4 > n3 && n2 - n == 0) {
            if (b) {
                this.sendTextChange(sendTextWillChange, n, n2 - n, n4 - n3);
                this.sendTextHasChanged(sendTextWillChange);
            }
        }
        else {
            boolean b2;
            if (this.mGapStart + this.mGapLength == this.mText.length) {
                b2 = true;
            }
            else {
                b2 = false;
            }
            for (int k = this.mSpanCount - 1; k >= 0; --k) {
                if (this.mSpanStarts[k] >= n && this.mSpanStarts[k] < this.mGapStart + this.mGapLength) {
                    final int n17 = (this.mSpanFlags[k] & 0xF0) >> 4;
                    if (n17 == 2 || (n17 == 3 && b2)) {
                        this.mSpanStarts[k] = this.mGapStart + this.mGapLength;
                    }
                    else {
                        this.mSpanStarts[k] = n;
                    }
                }
                if (this.mSpanEnds[k] >= n && this.mSpanEnds[k] < this.mGapStart + this.mGapLength) {
                    final int n18 = this.mSpanFlags[k] & 0xF;
                    if (n18 == 2 || (n18 == 3 && b2)) {
                        this.mSpanEnds[k] = this.mGapStart + this.mGapLength;
                    }
                    else {
                        this.mSpanEnds[k] = n;
                    }
                }
                if (this.mSpanEnds[k] < this.mSpanStarts[k]) {
                    System.arraycopy(this.mSpans, k + 1, this.mSpans, k, this.mSpanCount - (k + 1));
                    System.arraycopy(this.mSpanStarts, k + 1, this.mSpanStarts, k, this.mSpanCount - (k + 1));
                    System.arraycopy(this.mSpanEnds, k + 1, this.mSpanEnds, k, this.mSpanCount - (k + 1));
                    System.arraycopy(this.mSpanFlags, k + 1, this.mSpanFlags, k, this.mSpanCount - (k + 1));
                    --this.mSpanCount;
                }
            }
            if (b) {
                this.sendTextChange(sendTextWillChange, n, n2 - n, n4 - n3);
                this.sendTextHasChanged(sendTextWillChange);
                return n5;
            }
        }
        return n5;
    }
    
    private void checkRange(final String s, final int n, final int n2) {
        if (n2 < n) {
            throw new IndexOutOfBoundsException(s + " " + region(n, n2) + " has end before start");
        }
        final int length = this.length();
        if (n > length || n2 > length) {
            throw new IndexOutOfBoundsException(s + " " + region(n, n2) + " ends beyond length " + length);
        }
        if (n < 0 || n2 < 0) {
            throw new IndexOutOfBoundsException(s + " " + region(n, n2) + " starts before 0");
        }
    }
    
    private boolean isprint(final char c) {
        return c >= ' ' && c <= '~';
    }
    
    private void moveGapTo(final int mGapStart) {
        if (mGapStart == this.mGapStart) {
            return;
        }
        final boolean b = mGapStart == this.length();
        if (mGapStart < this.mGapStart) {
            final int n = this.mGapStart - mGapStart;
            System.arraycopy(this.mText, mGapStart, this.mText, this.mGapStart + this.mGapLength - n, n);
        }
        else {
            final int n2 = mGapStart - this.mGapStart;
            System.arraycopy(this.mText, this.mGapLength + mGapStart - n2, this.mText, this.mGapStart, n2);
        }
        for (int i = 0; i < this.mSpanCount; ++i) {
            final int n3 = this.mSpanStarts[i];
            final int n4 = this.mSpanEnds[i];
            int n5;
            if ((n5 = n3) > this.mGapStart) {
                n5 = n3 - this.mGapLength;
            }
            int n6 = 0;
            Label_0117: {
                if (n5 > mGapStart) {
                    n6 = n5 + this.mGapLength;
                }
                else if ((n6 = n5) == mGapStart) {
                    final int n7 = (this.mSpanFlags[i] & 0xF0) >> 4;
                    if (n7 != 2) {
                        n6 = n5;
                        if (!b) {
                            break Label_0117;
                        }
                        n6 = n5;
                        if (n7 != 3) {
                            break Label_0117;
                        }
                    }
                    n6 = n5 + this.mGapLength;
                }
            }
            int n8;
            if ((n8 = n4) > this.mGapStart) {
                n8 = n4 - this.mGapLength;
            }
            int n9 = 0;
            Label_0150: {
                if (n8 > mGapStart) {
                    n9 = n8 + this.mGapLength;
                }
                else if ((n9 = n8) == mGapStart) {
                    final int n10 = this.mSpanFlags[i] & 0xF;
                    if (n10 != 2) {
                        n9 = n8;
                        if (!b) {
                            break Label_0150;
                        }
                        n9 = n8;
                        if (n10 != 3) {
                            break Label_0150;
                        }
                    }
                    n9 = n8 + this.mGapLength;
                }
            }
            this.mSpanStarts[i] = n6;
            this.mSpanEnds[i] = n9;
        }
        this.mGapStart = mGapStart;
    }
    
    private static String region(final int n, final int n2) {
        return "(" + n + " ... " + n2 + ")";
    }
    
    private void resizeFor(int i) {
        final int idealCharArraySize = ArrayUtils.idealCharArraySize(i + 1);
        final char[] mText = new char[idealCharArraySize];
        i = this.mText.length - (this.mGapStart + this.mGapLength);
        System.arraycopy(this.mText, 0, mText, 0, this.mGapStart);
        System.arraycopy(this.mText, this.mText.length - i, mText, idealCharArraySize - i, i);
        int[] mSpanStarts;
        int[] mSpanEnds;
        for (i = 0; i < this.mSpanCount; ++i) {
            if (this.mSpanStarts[i] > this.mGapStart) {
                mSpanStarts = this.mSpanStarts;
                mSpanStarts[i] += idealCharArraySize - this.mText.length;
            }
            if (this.mSpanEnds[i] > this.mGapStart) {
                mSpanEnds = this.mSpanEnds;
                mSpanEnds[i] += idealCharArraySize - this.mText.length;
            }
        }
        i = this.mText.length;
        this.mText = mText;
        this.mGapLength += this.mText.length - i;
        if (this.mGapLength < 1) {
            new Exception("mGapLength < 1").printStackTrace();
        }
    }
    
    private void sendSpanAdded(final Object o, final int n, final int n2) {
        final SpanWatcher[] array = this.getSpans(n, n2, SpanWatcher.class);
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].onSpanAdded(this, o, n, n2);
        }
    }
    
    private void sendSpanChanged(final Object o, final int n, final int n2, final int n3, final int n4) {
        final SpanWatcher[] array = this.getSpans(Math.min(n, n3), Math.max(n2, n4), SpanWatcher.class);
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].onSpanChanged(this, o, n, n2, n3, n4);
        }
    }
    
    private void sendSpanRemoved(final Object o, final int n, final int n2) {
        final SpanWatcher[] array = this.getSpans(n, n2, SpanWatcher.class);
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].onSpanRemoved(this, o, n, n2);
        }
    }
    
    private void sendTextChange(final TextWatcher[] array, final int n, final int n2, final int n3) {
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].onTextChanged(this, n, n2, n3);
        }
    }
    
    private void sendTextHasChanged(final TextWatcher[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].afterTextChanged(this);
        }
    }
    
    private TextWatcher[] sendTextWillChange(final int n, final int n2, final int n3) {
        final TextWatcher[] array = this.getSpans(n, n + n2, TextWatcher.class);
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].beforeTextChanged(this, n, n2, n3);
        }
        return array;
    }
    
    private void setSpan(final boolean b, final Object o, final int n, final int n2, final int n3) {
        this.checkRange("setSpan", n, n2);
        if ((n3 & 0xF0) == 0x30 && n != 0 && n != this.length() && this.charAt(n - 1) != '\n') {
            throw new RuntimeException("PARAGRAPH span must start at paragraph boundary");
        }
        if ((n3 & 0xF) == 0x3 && n2 != 0 && n2 != this.length() && this.charAt(n2 - 1) != '\n') {
            throw new RuntimeException("PARAGRAPH span must end at paragraph boundary");
        }
        int n4 = 0;
        Label_0118: {
            if (n > this.mGapStart) {
                n4 = n + this.mGapLength;
            }
            else if ((n4 = n) == this.mGapStart) {
                final int n5 = (n3 & 0xF0) >> 4;
                if (n5 != 2) {
                    n4 = n;
                    if (n5 != 3 || (n4 = n) != this.length()) {
                        break Label_0118;
                    }
                }
                n4 = n + this.mGapLength;
            }
        }
        final int n6 = n4;
        int n7 = 0;
        Label_0140: {
            if (n2 > this.mGapStart) {
                n7 = n2 + this.mGapLength;
            }
            else if ((n7 = n2) == this.mGapStart) {
                final int n8 = n3 & 0xF;
                if (n8 != 2) {
                    n7 = n2;
                    if (n8 != 3 || (n7 = n2) != this.length()) {
                        break Label_0140;
                    }
                }
                n7 = n2 + this.mGapLength;
            }
        }
        final int n9 = n7;
        final int mSpanCount = this.mSpanCount;
        final Object[] mSpans = this.mSpans;
        for (int i = 0; i < mSpanCount; ++i) {
            if (mSpans[i] == o) {
                final int n10 = this.mSpanStarts[i];
                final int n11 = this.mSpanEnds[i];
                int n12;
                if ((n12 = n10) > this.mGapStart) {
                    n12 = n10 - this.mGapLength;
                }
                int n13;
                if ((n13 = n11) > this.mGapStart) {
                    n13 = n11 - this.mGapLength;
                }
                this.mSpanStarts[i] = n6;
                this.mSpanEnds[i] = n9;
                this.mSpanFlags[i] = n3;
                if (b) {
                    this.sendSpanChanged(o, n12, n13, n, n2);
                }
                return;
            }
        }
        if (this.mSpanCount + 1 >= this.mSpans.length) {
            final int idealIntArraySize = ArrayUtils.idealIntArraySize(this.mSpanCount + 1);
            final Object[] mSpans2 = new Object[idealIntArraySize];
            final int[] mSpanStarts = new int[idealIntArraySize];
            final int[] mSpanEnds = new int[idealIntArraySize];
            final int[] mSpanFlags = new int[idealIntArraySize];
            System.arraycopy(this.mSpans, 0, mSpans2, 0, this.mSpanCount);
            System.arraycopy(this.mSpanStarts, 0, mSpanStarts, 0, this.mSpanCount);
            System.arraycopy(this.mSpanEnds, 0, mSpanEnds, 0, this.mSpanCount);
            System.arraycopy(this.mSpanFlags, 0, mSpanFlags, 0, this.mSpanCount);
            this.mSpans = mSpans2;
            this.mSpanStarts = mSpanStarts;
            this.mSpanEnds = mSpanEnds;
            this.mSpanFlags = mSpanFlags;
        }
        this.mSpans[this.mSpanCount] = o;
        this.mSpanStarts[this.mSpanCount] = n6;
        this.mSpanEnds[this.mSpanCount] = n9;
        this.mSpanFlags[this.mSpanCount] = n3;
        ++this.mSpanCount;
        if (b) {
            this.sendSpanAdded(o, n, n2);
        }
    }
    
    public static SpannableStringBuilder valueOf(final CharSequence charSequence) {
        if (charSequence instanceof SpannableStringBuilder) {
            return (SpannableStringBuilder)charSequence;
        }
        return new SpannableStringBuilder(charSequence);
    }
    
    @Override
    public SpannableStringBuilder append(final char c) {
        return this.append((CharSequence)String.valueOf(c));
    }
    
    @Override
    public SpannableStringBuilder append(final CharSequence charSequence) {
        final int length = this.length();
        return this.replace(length, length, charSequence, 0, charSequence.length());
    }
    
    @Override
    public SpannableStringBuilder append(final CharSequence charSequence, final int n, final int n2) {
        final int length = this.length();
        return this.replace(length, length, charSequence, n, n2);
    }
    
    @Override
    public char charAt(final int n) {
        final int length = this.length();
        if (n < 0) {
            throw new IndexOutOfBoundsException("charAt: " + n + " < 0");
        }
        if (n >= length) {
            throw new IndexOutOfBoundsException("charAt: " + n + " >= length " + length);
        }
        if (n >= this.mGapStart) {
            return this.mText[this.mGapLength + n];
        }
        return this.mText[n];
    }
    
    @Override
    public void clear() {
        this.replace(0, this.length(), (CharSequence)"", 0, 0);
    }
    
    @Override
    public void clearSpans() {
        for (int i = this.mSpanCount - 1; i >= 0; --i) {
            final Object o = this.mSpans[i];
            final int n = this.mSpanStarts[i];
            final int n2 = this.mSpanEnds[i];
            int n3;
            if ((n3 = n) > this.mGapStart) {
                n3 = n - this.mGapLength;
            }
            int n4;
            if ((n4 = n2) > this.mGapStart) {
                n4 = n2 - this.mGapLength;
            }
            this.mSpanCount = i;
            this.mSpans[i] = null;
            this.sendSpanRemoved(o, n3, n4);
        }
    }
    
    @Override
    public SpannableStringBuilder delete(final int n, final int n2) {
        final SpannableStringBuilder replace = this.replace(n, n2, (CharSequence)"", 0, 0);
        if (this.mGapLength > this.length() * 2) {
            this.resizeFor(this.length());
        }
        return replace;
    }
    
    @Override
    public void getChars(final int n, final int n2, final char[] array, final int n3) {
        this.checkRange("getChars", n, n2);
        if (n2 <= this.mGapStart) {
            System.arraycopy(this.mText, n, array, n3, n2 - n);
            return;
        }
        if (n >= this.mGapStart) {
            System.arraycopy(this.mText, this.mGapLength + n, array, n3, n2 - n);
            return;
        }
        System.arraycopy(this.mText, n, array, n3, this.mGapStart - n);
        System.arraycopy(this.mText, this.mGapStart + this.mGapLength, array, this.mGapStart - n + n3, n2 - this.mGapStart);
    }
    
    @Override
    public InputFilter[] getFilters() {
        return this.mFilters;
    }
    
    @Override
    public int getSpanEnd(final Object o) {
        final int mSpanCount = this.mSpanCount;
        final Object[] mSpans = this.mSpans;
        for (int i = mSpanCount - 1; i >= 0; --i) {
            if (mSpans[i] == o) {
                final int n = this.mSpanEnds[i];
                int n2;
                if ((n2 = n) > this.mGapStart) {
                    n2 = n - this.mGapLength;
                }
                return n2;
            }
        }
        return -1;
    }
    
    @Override
    public int getSpanFlags(final Object o) {
        final int mSpanCount = this.mSpanCount;
        final Object[] mSpans = this.mSpans;
        for (int i = mSpanCount - 1; i >= 0; --i) {
            if (mSpans[i] == o) {
                return this.mSpanFlags[i];
            }
        }
        return 0;
    }
    
    @Override
    public int getSpanStart(final Object o) {
        final int mSpanCount = this.mSpanCount;
        final Object[] mSpans = this.mSpans;
        for (int i = mSpanCount - 1; i >= 0; --i) {
            if (mSpans[i] == o) {
                final int n = this.mSpanStarts[i];
                int n2;
                if ((n2 = n) > this.mGapStart) {
                    n2 = n - this.mGapLength;
                }
                return n2;
            }
        }
        return -1;
    }
    
    @Override
    public <T> T[] getSpans(final int n, final int n2, final Class<T> clazz) {
        final int mSpanCount = this.mSpanCount;
        final Object[] mSpans = this.mSpans;
        final int[] mSpanStarts = this.mSpanStarts;
        final int[] mSpanEnds = this.mSpanEnds;
        final int[] mSpanFlags = this.mSpanFlags;
        final int mGapStart = this.mGapStart;
        final int mGapLength = this.mGapLength;
        Object[] array = null;
        Object o = null;
        int i = 0;
        int n3 = 0;
        while (i < mSpanCount) {
            final int n4 = mSpanStarts[i];
            final int n5 = mSpanEnds[i];
            int n6;
            if ((n6 = n4) > mGapStart) {
                n6 = n4 - mGapLength;
            }
            int n7;
            if ((n7 = n5) > mGapStart) {
                n7 = n5 - mGapLength;
            }
            Label_0117: {
                if (n6 <= n2) {
                    if (n7 >= n) {
                        if (n6 != n7 && n != n2) {
                            if (n6 == n2) {
                                break Label_0117;
                            }
                            if (n7 == n) {
                                break Label_0117;
                            }
                        }
                        if (clazz == null || clazz.isInstance(mSpans[i])) {
                            if (n3 == 0) {
                                o = mSpans[i];
                                ++n3;
                            }
                            else {
                                if (n3 == 1) {
                                    array = (Object[])Array.newInstance(clazz, mSpanCount - i + 1);
                                    array[0] = o;
                                }
                                final int n8 = mSpanFlags[i] & 0xFF0000;
                                if (n8 != 0) {
                                    int n9;
                                    for (n9 = 0; n9 < n3 && n8 <= (this.getSpanFlags(array[n9]) & 0xFF0000); ++n9) {}
                                    System.arraycopy(array, n9, array, n9 + 1, n3 - n9);
                                    array[n9] = mSpans[i];
                                    ++n3;
                                }
                                else {
                                    final int n10 = n3 + 1;
                                    array[n3] = mSpans[i];
                                    n3 = n10;
                                }
                            }
                        }
                    }
                }
            }
            ++i;
        }
        if (n3 == 0) {
            return ArrayUtils.emptyArray(clazz);
        }
        if (n3 == 1) {
            final Object[] array2 = (Object[])Array.newInstance(clazz, 1);
            array2[0] = o;
            return (T[])array2;
        }
        if (n3 == ((T[])array).length) {
            return (T[])array;
        }
        final Object[] array3 = (Object[])Array.newInstance(clazz, n3);
        System.arraycopy(array, 0, array3, 0, n3);
        return (T[])array3;
    }
    
    @Override
    public SpannableStringBuilder insert(final int n, final CharSequence charSequence) {
        return this.replace(n, n, charSequence, 0, charSequence.length());
    }
    
    @Override
    public SpannableStringBuilder insert(final int n, final CharSequence charSequence, final int n2, final int n3) {
        return this.replace(n, n, charSequence, n2, n3);
    }
    
    @Override
    public int length() {
        return this.mText.length - this.mGapLength;
    }
    
    @Override
    public int nextSpanTransition(final int n, int n2, final Class clazz) {
        final int mSpanCount = this.mSpanCount;
        final Object[] mSpans = this.mSpans;
        final int[] mSpanStarts = this.mSpanStarts;
        final int[] mSpanEnds = this.mSpanEnds;
        final int mGapStart = this.mGapStart;
        final int mGapLength = this.mGapLength;
        Class<Object> clazz2 = (Class<Object>)clazz;
        if (clazz == null) {
            clazz2 = Object.class;
        }
        for (int i = 0; i < mSpanCount; ++i) {
            final int n3 = mSpanStarts[i];
            final int n4 = mSpanEnds[i];
            int n5;
            if ((n5 = n3) > mGapStart) {
                n5 = n3 - mGapLength;
            }
            int n6;
            if ((n6 = n4) > mGapStart) {
                n6 = n4 - mGapLength;
            }
            int n7 = n2;
            if (n5 > n && n5 < (n7 = n2)) {
                n7 = n2;
                if (clazz2.isInstance(mSpans[i])) {
                    n7 = n5;
                }
            }
            n2 = n7;
            if (n6 > n && n6 < (n2 = n7)) {
                n2 = n7;
                if (clazz2.isInstance(mSpans[i])) {
                    n2 = n6;
                }
            }
        }
        return n2;
    }
    
    @Override
    public void removeSpan(final Object o) {
        for (int i = this.mSpanCount - 1; i >= 0; --i) {
            if (this.mSpans[i] == o) {
                final int n = this.mSpanStarts[i];
                final int n2 = this.mSpanEnds[i];
                int n3;
                if ((n3 = n) > this.mGapStart) {
                    n3 = n - this.mGapLength;
                }
                int n4;
                if ((n4 = n2) > this.mGapStart) {
                    n4 = n2 - this.mGapLength;
                }
                final int n5 = this.mSpanCount - (i + 1);
                System.arraycopy(this.mSpans, i + 1, this.mSpans, i, n5);
                System.arraycopy(this.mSpanStarts, i + 1, this.mSpanStarts, i, n5);
                System.arraycopy(this.mSpanEnds, i + 1, this.mSpanEnds, i, n5);
                System.arraycopy(this.mSpanFlags, i + 1, this.mSpanFlags, i, n5);
                --this.mSpanCount;
                this.mSpans[this.mSpanCount] = null;
                this.sendSpanRemoved(o, n3, n4);
                break;
            }
        }
    }
    
    @Override
    public SpannableStringBuilder replace(final int n, final int n2, final CharSequence charSequence) {
        return this.replace(n, n2, charSequence, 0, charSequence.length());
    }
    
    @Override
    public SpannableStringBuilder replace(final int n, final int n2, CharSequence charSequence, int change, int length) {
        for (int length2 = this.mFilters.length, i = 0; i < length2; ++i) {
            final CharSequence filter = this.mFilters[i].filter(charSequence, change, length, this, n, n2);
            if (filter != null) {
                charSequence = filter;
                change = 0;
                length = filter.length();
            }
        }
        if (n2 == n && change == length) {
            return this;
        }
        if (n2 == n || change == length) {
            this.change(n, n2, charSequence, change, length);
            return this;
        }
        final int selectionStart = Selection.getSelectionStart(this);
        final int selectionEnd = Selection.getSelectionEnd(this);
        this.checkRange("replace", n, n2);
        this.moveGapTo(n2);
        final TextWatcher[] sendTextWillChange = this.sendTextWillChange(n, n2 - n, length - change);
        if (this.mGapLength < 2) {
            this.resizeFor(this.length() + 1);
        }
        for (int j = this.mSpanCount - 1; j >= 0; --j) {
            if (this.mSpanStarts[j] == this.mGapStart) {
                final int[] mSpanStarts = this.mSpanStarts;
                ++mSpanStarts[j];
            }
            if (this.mSpanEnds[j] == this.mGapStart) {
                final int[] mSpanEnds = this.mSpanEnds;
                ++mSpanEnds[j];
            }
        }
        this.mText[this.mGapStart] = ' ';
        ++this.mGapStart;
        --this.mGapLength;
        if (this.mGapLength < 1) {
            new Exception("mGapLength < 1").printStackTrace();
        }
        change = this.change(false, n + 1, n + 1, charSequence, change, length);
        this.change(false, n, n + 1, "", 0, 0);
        this.change(false, n + change, n + change + (n2 + 1 - n) - 1, "", 0, 0);
        if (selectionStart > n && selectionStart < n2) {
            length = change * (selectionStart - n) / (n2 - n) + n;
            this.setSpan(false, Selection.SELECTION_START, length, length, 34);
        }
        if (selectionEnd > n && selectionEnd < n2) {
            length = change * (selectionEnd - n) / (n2 - n) + n;
            this.setSpan(false, Selection.SELECTION_END, length, length, 34);
        }
        this.sendTextChange(sendTextWillChange, n, n2 - n, change);
        this.sendTextHasChanged(sendTextWillChange);
        return this;
    }
    
    @Override
    public void setFilters(final InputFilter[] mFilters) {
        if (mFilters == null) {
            throw new IllegalArgumentException();
        }
        this.mFilters = mFilters;
    }
    
    @Override
    public void setSpan(final Object o, final int n, final int n2, final int n3) {
        this.setSpan(true, o, n, n2, n3);
    }
    
    @Override
    public CharSequence subSequence(final int n, final int n2) {
        return new SpannableStringBuilder(this, n, n2);
    }
    
    @Override
    public String toString() {
        final int length = this.length();
        final char[] array = new char[length];
        this.getChars(0, length, array, 0);
        return new String(array);
    }
}
