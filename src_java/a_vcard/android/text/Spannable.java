package a_vcard.android.text;

public interface Spannable extends Spanned
{
    void removeSpan(final Object p0);
    
    void setSpan(final Object p0, final int p1, final int p2, final int p3);
}
