package a_vcard.android.text;

public interface Editable extends CharSequence, GetChars, Appendable
{
    Editable append(final char p0);
    
    Editable append(final CharSequence p0);
    
    Editable append(final CharSequence p0, final int p1, final int p2);
    
    void clear();
    
    void clearSpans();
    
    Editable delete(final int p0, final int p1);
    
    InputFilter[] getFilters();
    
    Editable insert(final int p0, final CharSequence p1);
    
    Editable insert(final int p0, final CharSequence p1, final int p2, final int p3);
    
    Editable replace(final int p0, final int p1, final CharSequence p2);
    
    Editable replace(final int p0, final int p1, final CharSequence p2, final int p3, final int p4);
    
    void setFilters(final InputFilter[] p0);
    
    public static class Factory
    {
        private static Factory sInstance;
        
        static {
            Factory.sInstance = new Factory();
        }
        
        public static Factory getInstance() {
            return Factory.sInstance;
        }
        
        public Editable newEditable(final CharSequence charSequence) {
            return new SpannableStringBuilder(charSequence);
        }
    }
}
