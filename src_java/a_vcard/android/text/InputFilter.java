package a_vcard.android.text;

public interface InputFilter
{
    CharSequence filter(final CharSequence p0, final int p1, final int p2, final Spanned p3, final int p4, final int p5);
    
    public static class LengthFilter implements InputFilter
    {
        private int mMax;
        
        public LengthFilter(final int mMax) {
            this.mMax = mMax;
        }
        
        @Override
        public CharSequence filter(final CharSequence charSequence, final int n, final int n2, final Spanned spanned, int n3, final int n4) {
            n3 = this.mMax - (spanned.length() - (n4 - n3));
            if (n3 <= 0) {
                return "";
            }
            if (n3 >= n2 - n) {
                return null;
            }
            return charSequence.subSequence(n, n + n3);
        }
    }
}
