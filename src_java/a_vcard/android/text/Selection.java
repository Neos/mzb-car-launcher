package a_vcard.android.text;

public class Selection
{
    public static final Object SELECTION_END;
    public static final Object SELECTION_START;
    
    static {
        SELECTION_START = new START();
        SELECTION_END = new END();
    }
    
    public static final void extendSelection(final Spannable spannable, final int n) {
        if (spannable.getSpanStart(Selection.SELECTION_END) != n) {
            spannable.setSpan(Selection.SELECTION_END, n, n, 34);
        }
    }
    
    public static final int getSelectionEnd(final CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return ((Spanned)charSequence).getSpanStart(Selection.SELECTION_END);
        }
        return -1;
    }
    
    public static final int getSelectionStart(final CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return ((Spanned)charSequence).getSpanStart(Selection.SELECTION_START);
        }
        return -1;
    }
    
    public static final void removeSelection(final Spannable spannable) {
        spannable.removeSpan(Selection.SELECTION_START);
        spannable.removeSpan(Selection.SELECTION_END);
    }
    
    public static final void selectAll(final Spannable spannable) {
        setSelection(spannable, 0, spannable.length());
    }
    
    public static final void setSelection(final Spannable spannable, final int n) {
        setSelection(spannable, n, n);
    }
    
    public static void setSelection(final Spannable spannable, final int n, final int n2) {
        final int selectionStart = getSelectionStart(spannable);
        final int selectionEnd = getSelectionEnd(spannable);
        if (selectionStart != n || selectionEnd != n2) {
            spannable.setSpan(Selection.SELECTION_START, n, n, 546);
            spannable.setSpan(Selection.SELECTION_END, n2, n2, 34);
        }
    }
    
    private static final class END implements NoCopySpan
    {
    }
    
    private static final class START implements NoCopySpan
    {
    }
}
