package a_vcard.android.syncml.pim;

import java.util.*;

public interface VBuilder
{
    void end();
    
    void endProperty();
    
    void endRecord();
    
    void propertyGroup(final String p0);
    
    void propertyName(final String p0);
    
    void propertyParamType(final String p0);
    
    void propertyParamValue(final String p0);
    
    void propertyValues(final List<String> p0);
    
    void start();
    
    void startProperty();
    
    void startRecord(final String p0);
}
