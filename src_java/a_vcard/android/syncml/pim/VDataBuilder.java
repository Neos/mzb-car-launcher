package a_vcard.android.syncml.pim;

import java.nio.charset.*;
import a_vcard.android.util.*;
import java.nio.*;
import java.util.*;
import a_vcard.android.content.*;
import java.io.*;

public class VDataBuilder implements VBuilder
{
    public static String DEFAULT_CHARSET;
    private static String LOG_TAG;
    private String mCurrentParamType;
    private PropertyNode mCurrentPropNode;
    private VNode mCurrentVNode;
    private int mNodeListPos;
    private String mSourceCharset;
    private boolean mStrictLineBreakParsing;
    private String mTargetCharset;
    public List<VNode> vNodeList;
    
    static {
        VDataBuilder.LOG_TAG = "VDATABuilder";
        VDataBuilder.DEFAULT_CHARSET = "UTF-8";
    }
    
    public VDataBuilder() {
        this(VDataBuilder.DEFAULT_CHARSET, VDataBuilder.DEFAULT_CHARSET, false);
    }
    
    public VDataBuilder(final String mSourceCharset, final String mTargetCharset, final boolean mStrictLineBreakParsing) {
        this.vNodeList = new ArrayList<VNode>();
        this.mNodeListPos = 0;
        if (mSourceCharset != null) {
            this.mSourceCharset = mSourceCharset;
        }
        else {
            this.mSourceCharset = VDataBuilder.DEFAULT_CHARSET;
        }
        if (mTargetCharset != null) {
            this.mTargetCharset = mTargetCharset;
        }
        else {
            this.mTargetCharset = VDataBuilder.DEFAULT_CHARSET;
        }
        this.mStrictLineBreakParsing = mStrictLineBreakParsing;
    }
    
    public VDataBuilder(final String s, final boolean b) {
        this(null, s, b);
    }
    
    private String encodeString(String s, final String s2) {
        if (this.mSourceCharset.equalsIgnoreCase(s2)) {
            return s;
        }
        final ByteBuffer encode = Charset.forName(this.mSourceCharset).encode(s);
        s = (String)(Object)new byte[encode.remaining()];
        encode.get((byte[])(Object)s);
        try {
            return new String((byte[])(Object)s, s2);
        }
        catch (UnsupportedEncodingException ex) {
            Log.e(VDataBuilder.LOG_TAG, "Failed to encode: charset=" + s2);
            return new String((byte[])(Object)s);
        }
    }
    
    private String handleOneValue(final String p0, final String p1, final String p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnull          490
        //     4: aload_3        
        //     5: ldc             "BASE64"
        //     7: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    10: ifne            22
        //    13: aload_3        
        //    14: ldc             "B"
        //    16: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    19: ifeq            38
        //    22: aload_0        
        //    23: getfield        a_vcard/android/syncml/pim/VDataBuilder.mCurrentPropNode:La_vcard/android/syncml/pim/PropertyNode;
        //    26: aload_1        
        //    27: invokevirtual   java/lang/String.getBytes:()[B
        //    30: invokestatic    org/apache/commons/codec/binary/Base64.decodeBase64:([B)[B
        //    33: putfield        a_vcard/android/syncml/pim/PropertyNode.propValue_bytes:[B
        //    36: aload_1        
        //    37: areturn        
        //    38: aload_3        
        //    39: ldc             "QUOTED-PRINTABLE"
        //    41: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    44: ifeq            490
        //    47: aload_1        
        //    48: ldc             "= "
        //    50: ldc             " "
        //    52: invokevirtual   java/lang/String.replaceAll:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    55: ldc             "=\t"
        //    57: ldc             "\t"
        //    59: invokevirtual   java/lang/String.replaceAll:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    62: astore          9
        //    64: aload_0        
        //    65: getfield        a_vcard/android/syncml/pim/VDataBuilder.mStrictLineBreakParsing:Z
        //    68: ifeq            151
        //    71: aload           9
        //    73: ldc             "\r\n"
        //    75: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //    78: astore_1       
        //    79: new             Ljava/lang/StringBuilder;
        //    82: dup            
        //    83: invokespecial   java/lang/StringBuilder.<init>:()V
        //    86: astore          9
        //    88: aload_1        
        //    89: arraylength    
        //    90: istore          6
        //    92: iconst_0       
        //    93: istore          5
        //    95: iload           5
        //    97: iload           6
        //    99: if_icmpge       352
        //   102: aload_1        
        //   103: iload           5
        //   105: aaload         
        //   106: astore          8
        //   108: aload           8
        //   110: astore_3       
        //   111: aload           8
        //   113: ldc             "="
        //   115: invokevirtual   java/lang/String.endsWith:(Ljava/lang/String;)Z
        //   118: ifeq            135
        //   121: aload           8
        //   123: iconst_0       
        //   124: aload           8
        //   126: invokevirtual   java/lang/String.length:()I
        //   129: iconst_1       
        //   130: isub           
        //   131: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   134: astore_3       
        //   135: aload           9
        //   137: aload_3        
        //   138: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   141: pop            
        //   142: iload           5
        //   144: iconst_1       
        //   145: iadd           
        //   146: istore          5
        //   148: goto            95
        //   151: new             Ljava/lang/StringBuilder;
        //   154: dup            
        //   155: invokespecial   java/lang/StringBuilder.<init>:()V
        //   158: astore_1       
        //   159: aload           9
        //   161: invokevirtual   java/lang/String.length:()I
        //   164: istore          7
        //   166: new             Ljava/util/ArrayList;
        //   169: dup            
        //   170: invokespecial   java/util/ArrayList.<init>:()V
        //   173: astore          8
        //   175: iconst_0       
        //   176: istore          5
        //   178: iload           5
        //   180: iload           7
        //   182: if_icmpge       317
        //   185: aload           9
        //   187: iload           5
        //   189: invokevirtual   java/lang/String.charAt:(I)C
        //   192: istore          4
        //   194: iload           4
        //   196: bipush          10
        //   198: if_icmpne       232
        //   201: aload           8
        //   203: aload_1        
        //   204: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   207: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   210: pop            
        //   211: new             Ljava/lang/StringBuilder;
        //   214: dup            
        //   215: invokespecial   java/lang/StringBuilder.<init>:()V
        //   218: astore_1       
        //   219: iload           5
        //   221: istore          6
        //   223: iload           6
        //   225: iconst_1       
        //   226: iadd           
        //   227: istore          5
        //   229: goto            178
        //   232: iload           4
        //   234: bipush          13
        //   236: if_icmpne       303
        //   239: aload           8
        //   241: aload_1        
        //   242: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   245: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   248: pop            
        //   249: new             Ljava/lang/StringBuilder;
        //   252: dup            
        //   253: invokespecial   java/lang/StringBuilder.<init>:()V
        //   256: astore_3       
        //   257: aload_3        
        //   258: astore_1       
        //   259: iload           5
        //   261: istore          6
        //   263: iload           5
        //   265: iload           7
        //   267: iconst_1       
        //   268: isub           
        //   269: if_icmpge       223
        //   272: aload_3        
        //   273: astore_1       
        //   274: iload           5
        //   276: istore          6
        //   278: aload           9
        //   280: iload           5
        //   282: iconst_1       
        //   283: iadd           
        //   284: invokevirtual   java/lang/String.charAt:(I)C
        //   287: bipush          10
        //   289: if_icmpne       223
        //   292: iload           5
        //   294: iconst_1       
        //   295: iadd           
        //   296: istore          6
        //   298: aload_3        
        //   299: astore_1       
        //   300: goto            223
        //   303: aload_1        
        //   304: iload           4
        //   306: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   309: pop            
        //   310: iload           5
        //   312: istore          6
        //   314: goto            223
        //   317: aload_1        
        //   318: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   321: astore_1       
        //   322: aload_1        
        //   323: invokevirtual   java/lang/String.length:()I
        //   326: ifle            336
        //   329: aload           8
        //   331: aload_1        
        //   332: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   335: pop            
        //   336: aload           8
        //   338: iconst_0       
        //   339: anewarray       Ljava/lang/String;
        //   342: invokevirtual   java/util/ArrayList.toArray:([Ljava/lang/Object;)[Ljava/lang/Object;
        //   345: checkcast       [Ljava/lang/String;
        //   348: astore_1       
        //   349: goto            79
        //   352: aload           9
        //   354: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   357: aload_0        
        //   358: getfield        a_vcard/android/syncml/pim/VDataBuilder.mSourceCharset:Ljava/lang/String;
        //   361: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //   364: astore_1       
        //   365: aload_1        
        //   366: invokestatic    org/apache/commons/codec/net/QuotedPrintableCodec.decodeQuotedPrintable:([B)[B
        //   369: astore_1       
        //   370: new             Ljava/lang/String;
        //   373: dup            
        //   374: aload_1        
        //   375: aload_2        
        //   376: invokespecial   java/lang/String.<init>:([BLjava/lang/String;)V
        //   379: astore_3       
        //   380: aload_3        
        //   381: areturn        
        //   382: astore_3       
        //   383: getstatic       a_vcard/android/syncml/pim/VDataBuilder.LOG_TAG:Ljava/lang/String;
        //   386: new             Ljava/lang/StringBuilder;
        //   389: dup            
        //   390: invokespecial   java/lang/StringBuilder.<init>:()V
        //   393: ldc             "Failed to encode: charset="
        //   395: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   398: aload_2        
        //   399: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   402: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   405: invokestatic    a_vcard/android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   408: pop            
        //   409: new             Ljava/lang/String;
        //   412: dup            
        //   413: aload_1        
        //   414: invokespecial   java/lang/String.<init>:([B)V
        //   417: areturn        
        //   418: astore_1       
        //   419: getstatic       a_vcard/android/syncml/pim/VDataBuilder.LOG_TAG:Ljava/lang/String;
        //   422: new             Ljava/lang/StringBuilder;
        //   425: dup            
        //   426: invokespecial   java/lang/StringBuilder.<init>:()V
        //   429: ldc             "Failed to encode: charset="
        //   431: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   434: aload_0        
        //   435: getfield        a_vcard/android/syncml/pim/VDataBuilder.mSourceCharset:Ljava/lang/String;
        //   438: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   441: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   444: invokestatic    a_vcard/android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   447: pop            
        //   448: aload           9
        //   450: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   453: invokevirtual   java/lang/String.getBytes:()[B
        //   456: astore_1       
        //   457: goto            365
        //   460: astore_1       
        //   461: getstatic       a_vcard/android/syncml/pim/VDataBuilder.LOG_TAG:Ljava/lang/String;
        //   464: new             Ljava/lang/StringBuilder;
        //   467: dup            
        //   468: invokespecial   java/lang/StringBuilder.<init>:()V
        //   471: ldc             "Failed to decode quoted-printable: "
        //   473: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   476: aload_1        
        //   477: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   480: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   483: invokestatic    a_vcard/android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   486: pop            
        //   487: ldc             ""
        //   489: areturn        
        //   490: aload_0        
        //   491: aload_1        
        //   492: aload_2        
        //   493: invokespecial   a_vcard/android/syncml/pim/VDataBuilder.encodeString:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   496: areturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                       
        //  -----  -----  -----  -----  -------------------------------------------
        //  352    365    418    460    Ljava/io/UnsupportedEncodingException;
        //  365    370    460    490    Lorg/apache/commons/codec/DecoderException;
        //  370    380    382    418    Ljava/io/UnsupportedEncodingException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0365:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private String listToString(final List<String> list) {
        final int size = list.size();
        if (size > 1) {
            final StringBuilder sb = new StringBuilder();
            final Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next()).append(";");
            }
            final int length = sb.length();
            if (length > 0 && sb.charAt(length - 1) == ';') {
                return sb.substring(0, length - 1);
            }
            return sb.toString();
        }
        else {
            if (size == 1) {
                return list.get(0);
            }
            return "";
        }
    }
    
    @Override
    public void end() {
    }
    
    @Override
    public void endProperty() {
        this.mCurrentVNode.propList.add(this.mCurrentPropNode);
    }
    
    @Override
    public void endRecord() {
        this.vNodeList.get(this.mNodeListPos).parseStatus = 0;
        while (this.mNodeListPos > 0) {
            --this.mNodeListPos;
            if (this.vNodeList.get(this.mNodeListPos).parseStatus == 1) {
                break;
            }
        }
        this.mCurrentVNode = this.vNodeList.get(this.mNodeListPos);
    }
    
    public String getResult() {
        return null;
    }
    
    @Override
    public void propertyGroup(final String s) {
        this.mCurrentPropNode.propGroupSet.add(s);
    }
    
    @Override
    public void propertyName(final String propName) {
        this.mCurrentPropNode.propName = propName;
    }
    
    @Override
    public void propertyParamType(final String mCurrentParamType) {
        this.mCurrentParamType = mCurrentParamType;
    }
    
    @Override
    public void propertyParamValue(final String s) {
        if (this.mCurrentParamType == null || this.mCurrentParamType.equalsIgnoreCase("TYPE")) {
            this.mCurrentPropNode.paramMap_TYPE.add(s);
        }
        else {
            this.mCurrentPropNode.paramMap.put(this.mCurrentParamType, s);
        }
        this.mCurrentParamType = null;
    }
    
    @Override
    public void propertyValues(final List<String> list) {
        if (list == null || list.size() == 0) {
            this.mCurrentPropNode.propValue_bytes = null;
            this.mCurrentPropNode.propValue_vector.clear();
            this.mCurrentPropNode.propValue_vector.add("");
            this.mCurrentPropNode.propValue = "";
            return;
        }
        final ContentValues paramMap = this.mCurrentPropNode.paramMap;
        final String default_CHARSET = VDataBuilder.DEFAULT_CHARSET;
        final String asString = paramMap.getAsString("ENCODING");
        String mTargetCharset = null;
        Label_0097: {
            if (default_CHARSET != null) {
                mTargetCharset = default_CHARSET;
                if (default_CHARSET.length() != 0) {
                    break Label_0097;
                }
            }
            mTargetCharset = this.mTargetCharset;
        }
        final Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.mCurrentPropNode.propValue_vector.add(this.handleOneValue(iterator.next(), mTargetCharset, asString));
        }
        this.mCurrentPropNode.propValue = this.listToString(this.mCurrentPropNode.propValue_vector);
    }
    
    @Override
    public void start() {
    }
    
    @Override
    public void startProperty() {
        this.mCurrentPropNode = new PropertyNode();
    }
    
    @Override
    public void startRecord(final String vName) {
        final VNode vNode = new VNode();
        vNode.parseStatus = 1;
        vNode.VName = vName;
        this.vNodeList.add(vNode);
        this.mNodeListPos = this.vNodeList.size() - 1;
        this.mCurrentVNode = this.vNodeList.get(this.mNodeListPos);
    }
}
