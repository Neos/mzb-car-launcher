package a_vcard.android.syncml.pim.vcard;

import java.util.*;
import org.apache.commons.codec.binary.*;

public class VCardComposer
{
    private static final String TAG = "VCardComposer";
    public static final int VERSION_VCARD21_INT = 1;
    public static final int VERSION_VCARD30_INT = 2;
    private static final HashMap<Integer, String> emailTypeMap;
    private static final HashSet<String> emailTypes;
    private static final HashMap<Integer, String> phoneTypeMap;
    private static final HashSet<String> phoneTypes;
    private String mNewline;
    private StringBuilder mResult;
    
    static {
        emailTypes = new HashSet<String>(Arrays.asList("CELL", "AOL", "APPLELINK", "ATTMAIL", "CIS", "EWORLD", "INTERNET", "IBMMAIL", "MCIMAIL", "POWERSHARE", "PRODIGY", "TLX", "X400"));
        phoneTypes = new HashSet<String>(Arrays.asList("PREF", "WORK", "HOME", "VOICE", "FAX", "MSG", "CELL", "PAGER", "BBS", "MODEM", "CAR", "ISDN", "VIDEO"));
        phoneTypeMap = new HashMap<Integer, String>();
        emailTypeMap = new HashMap<Integer, String>();
        VCardComposer.phoneTypeMap.put(1, "HOME");
        VCardComposer.phoneTypeMap.put(2, "CELL");
        VCardComposer.phoneTypeMap.put(3, "WORK");
        VCardComposer.phoneTypeMap.put(4, "WORK;FAX");
        VCardComposer.phoneTypeMap.put(5, "HOME;FAX");
        VCardComposer.phoneTypeMap.put(6, "PAGER");
        VCardComposer.phoneTypeMap.put(7, "X-OTHER");
        VCardComposer.emailTypeMap.put(1, "HOME");
        VCardComposer.emailTypeMap.put(2, "WORK");
    }
    
    private void appendContactMethodStr(final List<ContactStruct.ContactMethod> list, final int n) {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        String s;
        if (n == 1) {
            s = ";";
        }
        else {
            s = ",";
        }
        for (final ContactStruct.ContactMethod contactMethod : list) {
            switch (contactMethod.kind) {
                default: {
                    continue;
                }
                case 1: {
                    final String s2 = "INTERNET";
                    if (!this.isNull(contactMethod.data)) {
                        final int intValue = new Integer(contactMethod.type);
                        String upperCase;
                        if (VCardComposer.emailTypeMap.containsKey(intValue)) {
                            upperCase = VCardComposer.emailTypeMap.get(intValue);
                        }
                        else {
                            upperCase = s2;
                            if (!this.isNull(contactMethod.label)) {
                                upperCase = s2;
                                if (VCardComposer.emailTypes.contains(contactMethod.label.toUpperCase())) {
                                    upperCase = contactMethod.label.toUpperCase();
                                }
                            }
                        }
                        String string = upperCase;
                        if (hashMap.containsKey(contactMethod.data)) {
                            string = hashMap.get(contactMethod.data) + s + upperCase;
                        }
                        hashMap.put(contactMethod.data, string);
                        continue;
                    }
                    continue;
                }
                case 2: {
                    if (!this.isNull(contactMethod.data)) {
                        this.mResult.append("ADR;TYPE=POSTAL:").append(this.foldingString(contactMethod.data, n)).append(this.mNewline);
                        continue;
                    }
                    continue;
                }
            }
        }
        for (final Map.Entry<String, String> entry : hashMap.entrySet()) {
            if (n == 1) {
                this.mResult.append("EMAIL;");
            }
            else {
                this.mResult.append("EMAIL;TYPE=");
            }
            this.mResult.append(entry.getValue()).append(":").append(entry.getKey()).append(this.mNewline);
        }
    }
    
    private void appendNameStr(final String s) {
        this.mResult.append("FN:").append(s).append(this.mNewline);
        this.mResult.append("N:").append(s).append(this.mNewline);
    }
    
    private void appendPhoneStr(final List<ContactStruct.PhoneData> list, final int n) {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        String s;
        if (n == 1) {
            s = ";";
        }
        else {
            s = ",";
        }
        for (final ContactStruct.PhoneData phoneData : list) {
            if (!this.isNull(phoneData.data)) {
                String s3;
                final String s2 = s3 = this.getPhoneTypeStr(phoneData);
                if (n == 2) {
                    s3 = s2;
                    if (s2.indexOf(";") != -1) {
                        s3 = s2.replace(";", ",");
                    }
                }
                String string = s3;
                if (hashMap.containsKey(phoneData.data)) {
                    string = hashMap.get(phoneData.data) + s + s3;
                }
                hashMap.put(phoneData.data, string);
            }
        }
        for (final Map.Entry<String, String> entry : hashMap.entrySet()) {
            if (n == 1) {
                this.mResult.append("TEL;");
            }
            else {
                this.mResult.append("TEL;TYPE=");
            }
            this.mResult.append(entry.getValue()).append(":").append(entry.getKey()).append(this.mNewline);
        }
    }
    
    private void appendPhotoStr(final byte[] array, final String s, final int n) throws VCardException {
    Label_0092_Outer:
        while (true) {
            while (true) {
            Label_0204:
                while (true) {
                    try {
                        String s2 = this.foldingString(new String(Base64.encodeBase64(array, true)), n);
                        if (this.isNull(s) || s.toUpperCase().indexOf("JPEG") >= 0) {
                            final String s3 = "JPEG";
                            this.mResult.append("LOGO;TYPE=").append(s3);
                            if (n == 1) {
                                final String s4 = ";ENCODING=BASE64:";
                                s2 += this.mNewline;
                                this.mResult.append(s4).append(s2).append(this.mNewline);
                                return;
                            }
                            break Label_0204;
                        }
                    }
                    catch (Exception ex) {
                        throw new VCardException(ex.getMessage());
                    }
                    if (s.toUpperCase().indexOf("GIF") >= 0) {
                        final String s3 = "GIF";
                        continue Label_0092_Outer;
                    }
                    if (s.toUpperCase().indexOf("BMP") >= 0) {
                        final String s3 = "BMP";
                        continue Label_0092_Outer;
                    }
                    final int index = s.indexOf("/");
                    if (index >= 0) {
                        final String s3 = s.substring(index + 1).toUpperCase();
                        continue Label_0092_Outer;
                    }
                    final String s3 = s.toUpperCase();
                    continue Label_0092_Outer;
                }
                if (n == 2) {
                    final String s4 = ";ENCODING=b:";
                    continue;
                }
                break;
            }
        }
    }
    
    private String foldingString(String replaceAll, final int n) {
        String s;
        if (replaceAll.endsWith("\r\n")) {
            s = replaceAll.substring(0, replaceAll.length() - 2);
        }
        else {
            s = replaceAll;
            if (replaceAll.endsWith("\n")) {
                s = replaceAll.substring(0, replaceAll.length() - 1);
            }
        }
        replaceAll = s.replaceAll("\r\n", "\n");
        if (n == 1) {
            return replaceAll.replaceAll("\n", "\r\n ");
        }
        if (n == 2) {
            return replaceAll.replaceAll("\n", "\n ");
        }
        return null;
    }
    
    private String getPhoneTypeStr(final ContactStruct.PhoneData phoneData) {
        final int type = phoneData.type;
        if (VCardComposer.phoneTypeMap.containsKey(type)) {
            return VCardComposer.phoneTypeMap.get(type);
        }
        if (type != 0) {
            return "VOICE";
        }
        final String upperCase = phoneData.label.toUpperCase();
        if (VCardComposer.phoneTypes.contains(upperCase) || upperCase.startsWith("X-")) {
            return upperCase;
        }
        return "X-CUSTOM-" + upperCase;
    }
    
    private boolean isNull(final String s) {
        return s == null || s.trim().equals("");
    }
    
    public String createVCard(final ContactStruct contactStruct, final int n) throws VCardException {
        this.mResult = new StringBuilder();
        if (contactStruct.name == null || contactStruct.name.trim().equals("")) {
            throw new VCardException(" struct.name MUST have value.");
        }
        if (n == 1) {
            this.mNewline = "\r\n";
        }
        else {
            if (n != 2) {
                throw new VCardException(" version not match VERSION_VCARD21 or VERSION_VCARD30.");
            }
            this.mNewline = "\n";
        }
        this.mResult.append("BEGIN:VCARD").append(this.mNewline);
        if (n == 1) {
            this.mResult.append("VERSION:2.1").append(this.mNewline);
        }
        else {
            this.mResult.append("VERSION:3.0").append(this.mNewline);
        }
        if (!this.isNull(contactStruct.name)) {
            this.appendNameStr(contactStruct.name);
        }
        if (!this.isNull(contactStruct.company)) {
            this.mResult.append("ORG:").append(contactStruct.company).append(this.mNewline);
        }
        if (contactStruct.notes.size() > 0 && !this.isNull(contactStruct.notes.get(0))) {
            this.mResult.append("NOTE:").append(this.foldingString(contactStruct.notes.get(0), n)).append(this.mNewline);
        }
        if (!this.isNull(contactStruct.title)) {
            this.mResult.append("TITLE:").append(this.foldingString(contactStruct.title, n)).append(this.mNewline);
        }
        if (contactStruct.photoBytes != null) {
            this.appendPhotoStr(contactStruct.photoBytes, contactStruct.photoType, n);
        }
        if (contactStruct.phoneList != null) {
            this.appendPhoneStr(contactStruct.phoneList, n);
        }
        if (contactStruct.contactmethodList != null) {
            this.appendContactMethodStr(contactStruct.contactmethodList, n);
        }
        this.mResult.append("END:VCARD").append(this.mNewline);
        return this.mResult.toString();
    }
}
