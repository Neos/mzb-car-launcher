package a_vcard.android.syncml.pim.vcard;

public class VCardNestedException extends VCardException
{
    public VCardNestedException() {
    }
    
    public VCardNestedException(final String s) {
        super(s);
    }
}
