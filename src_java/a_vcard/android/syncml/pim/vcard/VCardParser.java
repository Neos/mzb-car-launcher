package a_vcard.android.syncml.pim.vcard;

import java.io.*;
import a_vcard.android.syncml.pim.*;

public class VCardParser
{
    private static final String TAG = "VCardParser";
    public static final String VERSION_VCARD21 = "vcard2.1";
    public static final int VERSION_VCARD21_INT = 1;
    public static final String VERSION_VCARD30 = "vcard3.0";
    public static final int VERSION_VCARD30_INT = 2;
    VCardParser_V21 mParser;
    String mVersion;
    
    public VCardParser() {
        this.mParser = null;
        this.mVersion = null;
    }
    
    private void judgeVersion(String substring) {
        if (this.mVersion == null) {
            final int index = substring.indexOf("\nVERSION:");
            if (index == -1) {
                this.mVersion = "vcard2.1";
            }
            else {
                substring = substring.substring(index, substring.indexOf("\n", index + 1));
                if (substring.indexOf("2.1") > 0) {
                    this.mVersion = "vcard2.1";
                }
                else if (substring.indexOf("3.0") > 0) {
                    this.mVersion = "vcard3.0";
                }
                else {
                    this.mVersion = "vcard2.1";
                }
            }
        }
        if (this.mVersion.equals("vcard2.1")) {
            this.mParser = new VCardParser_V21();
        }
        if (this.mVersion.equals("vcard3.0")) {
            this.mParser = new VCardParser_V30();
        }
    }
    
    private void setVersion(final String mVersion) {
        this.mVersion = mVersion;
    }
    
    private String verifyVCard(final String s) {
        this.judgeVersion(s);
        final String[] split = s.replaceAll("\r\n", "\n").split("\n");
        final StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < split.length; ++i) {
            if (split[i].indexOf(":") < 0) {
                if (split[i].length() == 0 && split[i + 1].indexOf(":") > 0) {
                    sb.append(split[i]).append("\r\n");
                }
                else {
                    sb.append(" ").append(split[i]).append("\r\n");
                }
            }
            else {
                sb.append(split[i]).append("\r\n");
            }
        }
        return sb.toString();
    }
    
    public boolean parse(final String s, final VDataBuilder vDataBuilder) throws VCardException, IOException {
        return this.parse(s, "US-ASCII", vDataBuilder);
    }
    
    public boolean parse(String verifyVCard, final String s, final VDataBuilder vDataBuilder) throws VCardException, IOException {
        verifyVCard = this.verifyVCard(verifyVCard);
        if (this.mParser.parse(new ByteArrayInputStream(verifyVCard.getBytes(s)), s, vDataBuilder)) {
            return true;
        }
        if (this.mVersion.equals("vcard2.1")) {
            this.setVersion("vcard3.0");
            return this.parse(verifyVCard, vDataBuilder);
        }
        throw new VCardException("parse failed.(even use 3.0 parser)");
    }
}
