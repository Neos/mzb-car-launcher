package a_vcard.android.syncml.pim.vcard;

import a_vcard.android.syncml.pim.*;
import java.util.*;
import a_vcard.android.util.*;
import java.io.*;

public class VCardParser_V21
{
    public static final String DEFAULT_CHARSET = "UTF-8";
    private static final String LOG_TAG = "VCardParser_V21";
    private static final int STATE_GROUP_OR_PROPNAME = 0;
    private static final int STATE_PARAMS = 1;
    private static final int STATE_PARAMS_IN_DQUOTE = 2;
    private static final HashSet<String> sAvailableEncodingV21;
    private static final HashSet<String> sAvailablePropertyNameV21;
    private static final HashSet<String> sKnownTypeSet;
    private static final HashSet<String> sKnownValueSet;
    protected VBuilder mBuilder;
    private boolean mCanceled;
    protected String mEncoding;
    private int mNestCount;
    private String mPreviousLine;
    protected BufferedReader mReader;
    private long mTimeEndProperty;
    private long mTimeEndRecord;
    private long mTimeHandlePropertyValue1;
    private long mTimeHandlePropertyValue2;
    private long mTimeHandlePropertyValue3;
    private long mTimeParseItem1;
    private long mTimeParseItem2;
    private long mTimeParseItem3;
    private long mTimeParseItems;
    private long mTimeStartProperty;
    private long mTimeStartRecord;
    private long mTimeTotal;
    protected HashSet<String> mWarningValueMap;
    protected final String sDefaultEncoding;
    
    static {
        sKnownTypeSet = new HashSet<String>(Arrays.asList("DOM", "INTL", "POSTAL", "PARCEL", "HOME", "WORK", "PREF", "VOICE", "FAX", "MSG", "CELL", "PAGER", "BBS", "MODEM", "CAR", "ISDN", "VIDEO", "AOL", "APPLELINK", "ATTMAIL", "CIS", "EWORLD", "INTERNET", "IBMMAIL", "MCIMAIL", "POWERSHARE", "PRODIGY", "TLX", "X400", "GIF", "CGM", "WMF", "BMP", "MET", "PMB", "DIB", "PICT", "TIFF", "PDF", "PS", "JPEG", "QTIME", "MPEG", "MPEG2", "AVI", "WAVE", "AIFF", "PCM", "X509", "PGP"));
        sKnownValueSet = new HashSet<String>(Arrays.asList("INLINE", "URL", "CONTENT-ID", "CID"));
        sAvailablePropertyNameV21 = new HashSet<String>(Arrays.asList("BEGIN", "LOGO", "PHOTO", "LABEL", "FN", "TITLE", "SOUND", "VERSION", "TEL", "EMAIL", "TZ", "GEO", "NOTE", "URL", "BDAY", "ROLE", "REV", "UID", "KEY", "MAILER"));
        sAvailableEncodingV21 = new HashSet<String>(Arrays.asList("7BIT", "8BIT", "QUOTED-PRINTABLE", "BASE64", "B"));
    }
    
    public VCardParser_V21() {
        this.mBuilder = null;
        this.mEncoding = null;
        this.sDefaultEncoding = "8BIT";
        this.mWarningValueMap = new HashSet<String>();
    }
    
    public VCardParser_V21(final VCardSourceDetector vCardSourceDetector) {
        this.mBuilder = null;
        this.mEncoding = null;
        this.sDefaultEncoding = "8BIT";
        this.mWarningValueMap = new HashSet<String>();
        if (vCardSourceDetector != null && vCardSourceDetector.getType() == 3) {
            this.mNestCount = 1;
        }
    }
    
    private boolean isLetter(final char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
    
    private boolean parseOneVCard(final boolean b) throws IOException, VCardException {
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = b2;
        Label_0057: {
            if (!b) {
                break Label_0057;
            }
            b4 = b2;
            if (this.mNestCount <= 0) {
                break Label_0057;
            }
            int n = 0;
            boolean b5 = b3;
            while (true) {
                b4 = b5;
                if (n >= this.mNestCount) {
                    break Label_0057;
                }
                if (!this.readBeginVCard(b5)) {
                    break;
                }
                b5 = true;
                ++n;
            }
            return false;
        }
        if (this.readBeginVCard(b4)) {
            if (this.mBuilder != null) {
                final long currentTimeMillis = System.currentTimeMillis();
                this.mBuilder.startRecord("VCARD");
                this.mTimeStartRecord += System.currentTimeMillis() - currentTimeMillis;
            }
            final long currentTimeMillis2 = System.currentTimeMillis();
            this.parseItems();
            this.mTimeParseItems += System.currentTimeMillis() - currentTimeMillis2;
            this.readEndVCard(true, false);
            if (this.mBuilder != null) {
                final long currentTimeMillis3 = System.currentTimeMillis();
                this.mBuilder.endRecord();
                this.mTimeEndRecord += System.currentTimeMillis() - currentTimeMillis3;
            }
            return true;
        }
        return false;
    }
    
    public void cancel() {
        this.mCanceled = true;
    }
    
    protected String getBase64(String line) throws IOException, VCardException {
        final StringBuilder sb = new StringBuilder();
        sb.append(line);
        while (true) {
            line = this.getLine();
            if (line == null) {
                throw new VCardException("File ended during parsing BASE64 binary");
            }
            if (line.length() == 0) {
                return sb.toString();
            }
            sb.append(line);
        }
    }
    
    protected String getLine() throws IOException {
        return this.mReader.readLine();
    }
    
    protected String getNonEmptyLine() throws IOException, VCardException {
        String line;
        do {
            line = this.getLine();
            if (line == null) {
                throw new VCardException("Reached end of buffer.");
            }
        } while (line.trim().length() <= 0);
        return line;
    }
    
    protected String getQuotedPrintable(String line) throws IOException, VCardException {
        String string = line;
        if (line.trim().endsWith("=")) {
            final int n = line.length() - 1;
            while (line.charAt(n) != '=') {}
            final StringBuilder sb = new StringBuilder();
            sb.append(line.substring(0, n + 1));
            sb.append("\r\n");
            while (true) {
                line = this.getLine();
                if (line == null) {
                    throw new VCardException("File ended during parsing quoted-printable String");
                }
                if (!line.trim().endsWith("=")) {
                    sb.append(line);
                    string = sb.toString();
                    break;
                }
                final int n2 = line.length() - 1;
                while (line.charAt(n2) != '=') {}
                sb.append(line.substring(0, n2 + 1));
                sb.append("\r\n");
            }
        }
        return string;
    }
    
    protected String getVersion() {
        return "2.1";
    }
    
    protected void handleAgent(final String s) throws VCardException {
        throw new VCardException("AGENT Property is not supported.");
    }
    
    protected void handleAnyParam(final String s, final String s2) {
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType(s);
            this.mBuilder.propertyParamValue(s2);
        }
    }
    
    protected void handleCharset(final String s) {
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType("CHARSET");
            this.mBuilder.propertyParamValue(s);
        }
    }
    
    protected void handleEncoding(final String mEncoding) throws VCardException {
        if (this.isValidEncoding(mEncoding) || mEncoding.startsWith("X-")) {
            if (this.mBuilder != null) {
                this.mBuilder.propertyParamType("ENCODING");
                this.mBuilder.propertyParamValue(mEncoding);
            }
            this.mEncoding = mEncoding;
            return;
        }
        throw new VCardException("Unknown encoding \"" + mEncoding + "\"");
    }
    
    protected void handleLanguage(final String s) throws VCardException {
        final String[] split = s.split("-");
        if (split.length > 2) {
            throw new VCardException("Invalid Language: \"" + s + "\"");
        }
        final String s2 = split[0];
        for (int length = s2.length(), i = 0; i < length; ++i) {
            if (!this.isLetter(s2.charAt(i))) {
                throw new VCardException("Invalid Language: \"" + s + "\"");
            }
        }
        if (split.length > 1) {
            final String s3 = split[1];
            for (int length2 = s3.length(), j = 0; j < length2; ++j) {
                if (!this.isLetter(s3.charAt(j))) {
                    throw new VCardException("Invalid Language: \"" + s + "\"");
                }
            }
        }
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType("LANGUAGE");
            this.mBuilder.propertyParamValue(s);
        }
    }
    
    protected void handleMultiplePropertyValue(String quotedPrintable, final String s) throws IOException, VCardException {
        quotedPrintable = s;
        if (this.mEncoding.equalsIgnoreCase("QUOTED-PRINTABLE")) {
            quotedPrintable = this.getQuotedPrintable(s);
        }
        if (this.mBuilder != null) {
            StringBuilder sb = new StringBuilder();
            final ArrayList<String> list = new ArrayList<String>();
            for (int length = quotedPrintable.length(), i = 0; i < length; ++i) {
                final char char1 = quotedPrintable.charAt(i);
                if (char1 == '\\' && i < length - 1) {
                    final String maybeUnescape = this.maybeUnescape(quotedPrintable.charAt(i + 1));
                    if (maybeUnescape != null) {
                        sb.append(maybeUnescape);
                        ++i;
                    }
                    else {
                        sb.append(char1);
                    }
                }
                else if (char1 == ';') {
                    list.add(sb.toString());
                    sb = new StringBuilder();
                }
                else {
                    sb.append(char1);
                }
            }
            list.add(sb.toString());
            this.mBuilder.propertyValues(list);
        }
    }
    
    protected void handleParams(String trim) throws VCardException {
        final String[] split = trim.split("=", 2);
        if (split.length != 2) {
            this.handleType(split[0]);
            return;
        }
        trim = split[0].trim();
        final String trim2 = split[1].trim();
        if (trim.equals("TYPE")) {
            this.handleType(trim2);
            return;
        }
        if (trim.equals("VALUE")) {
            this.handleValue(trim2);
            return;
        }
        if (trim.equals("ENCODING")) {
            this.handleEncoding(trim2);
            return;
        }
        if (trim.equals("CHARSET")) {
            this.handleCharset(trim2);
            return;
        }
        if (trim.equals("LANGUAGE")) {
            this.handleLanguage(trim2);
            return;
        }
        if (trim.startsWith("X-")) {
            this.handleAnyParam(trim, trim2);
            return;
        }
        throw new VCardException("Unknown type \"" + trim + "\"");
    }
    
    protected void handlePropertyValue(String s, final String s2) throws IOException, VCardException {
        if (this.mEncoding.equalsIgnoreCase("QUOTED-PRINTABLE")) {
            final long currentTimeMillis = System.currentTimeMillis();
            s = this.getQuotedPrintable(s2);
            if (this.mBuilder != null) {
                final ArrayList<String> list = new ArrayList<String>();
                list.add(s);
                this.mBuilder.propertyValues(list);
            }
            this.mTimeHandlePropertyValue2 += System.currentTimeMillis() - currentTimeMillis;
            return;
        }
        if (this.mEncoding.equalsIgnoreCase("BASE64") || this.mEncoding.equalsIgnoreCase("B")) {
            final long currentTimeMillis2 = System.currentTimeMillis();
            while (true) {
                try {
                    s = this.getBase64(s2);
                    if (this.mBuilder != null) {
                        final ArrayList<String> list2 = new ArrayList<String>();
                        list2.add(s);
                        this.mBuilder.propertyValues(list2);
                    }
                    this.mTimeHandlePropertyValue3 += System.currentTimeMillis() - currentTimeMillis2;
                    return;
                }
                catch (OutOfMemoryError outOfMemoryError) {
                    Log.e("VCardParser_V21", "OutOfMemoryError happened during parsing BASE64 data!");
                    if (this.mBuilder != null) {
                        this.mBuilder.propertyValues(null);
                    }
                    continue;
                }
                break;
            }
        }
        if (this.mEncoding != null && !this.mEncoding.equalsIgnoreCase("7BIT") && !this.mEncoding.equalsIgnoreCase("8BIT") && !this.mEncoding.toUpperCase().startsWith("X-")) {
            Log.w("VCardParser_V21", "The encoding unsupported by vCard spec: \"" + this.mEncoding + "\".");
        }
        final long currentTimeMillis3 = System.currentTimeMillis();
        if (this.mBuilder != null) {
            final ArrayList<String> list3 = new ArrayList<String>();
            list3.add(this.maybeUnescapeText(s2));
            this.mBuilder.propertyValues(list3);
        }
        this.mTimeHandlePropertyValue1 += System.currentTimeMillis() - currentTimeMillis3;
    }
    
    protected void handleType(final String s) {
        if (!VCardParser_V21.sKnownTypeSet.contains(s) && !s.startsWith("X-") && !this.mWarningValueMap.contains(s)) {
            this.mWarningValueMap.add(s);
            Log.w("VCardParser_V21", "Type unsupported by vCard 2.1: " + s);
        }
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType("TYPE");
            this.mBuilder.propertyParamValue(s);
        }
    }
    
    protected void handleValue(final String s) throws VCardException {
        if (VCardParser_V21.sKnownValueSet.contains(s.toUpperCase()) || s.startsWith("X-")) {
            if (this.mBuilder != null) {
                this.mBuilder.propertyParamType("VALUE");
                this.mBuilder.propertyParamValue(s);
            }
            return;
        }
        throw new VCardException("Unknown value \"" + s + "\"");
    }
    
    protected boolean isValidEncoding(final String s) {
        return VCardParser_V21.sAvailableEncodingV21.contains(s.toUpperCase());
    }
    
    protected boolean isValidPropertyName(final String s) {
        if (!VCardParser_V21.sAvailablePropertyNameV21.contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mWarningValueMap.contains(s)) {
            this.mWarningValueMap.add(s);
            Log.w("VCardParser_V21", "Property name unsupported by vCard 2.1: " + s);
        }
        return true;
    }
    
    protected String maybeUnescape(final char c) {
        if (c == '\\' || c == ';' || c == ':' || c == ',') {
            return String.valueOf(c);
        }
        return null;
    }
    
    protected String maybeUnescapeText(final String s) {
        return s;
    }
    
    public void parse(final InputStream inputStream, final String s, final VBuilder vBuilder, final boolean mCanceled) throws IOException, VCardException {
        this.mCanceled = mCanceled;
        this.parse(inputStream, s, vBuilder);
    }
    
    public boolean parse(final InputStream inputStream, final VBuilder vBuilder) throws IOException, VCardException {
        return this.parse(inputStream, "UTF-8", vBuilder);
    }
    
    public boolean parse(final InputStream inputStream, final String s, final VBuilder mBuilder) throws IOException, VCardException {
        this.mReader = new CustomBufferedReader(new InputStreamReader(inputStream, s));
        this.mBuilder = mBuilder;
        final long currentTimeMillis = System.currentTimeMillis();
        if (this.mBuilder != null) {
            this.mBuilder.start();
        }
        this.parseVCardFile();
        if (this.mBuilder != null) {
            this.mBuilder.end();
        }
        this.mTimeTotal += System.currentTimeMillis() - currentTimeMillis;
        return true;
    }
    
    protected boolean parseItem() throws IOException, VCardException {
        this.mEncoding = "8BIT";
        final String nonEmptyLine = this.getNonEmptyLine();
        final long currentTimeMillis = System.currentTimeMillis();
        final String[] separateLineAndHandleGroup = this.separateLineAndHandleGroup(nonEmptyLine);
        if (separateLineAndHandleGroup == null) {
            return true;
        }
        if (separateLineAndHandleGroup.length != 2) {
            throw new VCardException("Invalid line \"" + nonEmptyLine + "\"");
        }
        final String upperCase = separateLineAndHandleGroup[0].toUpperCase();
        final String s = separateLineAndHandleGroup[1];
        this.mTimeParseItem1 += System.currentTimeMillis() - currentTimeMillis;
        if (upperCase.equals("ADR") || upperCase.equals("ORG") || upperCase.equals("N")) {
            final long currentTimeMillis2 = System.currentTimeMillis();
            this.handleMultiplePropertyValue(upperCase, s);
            this.mTimeParseItem3 += System.currentTimeMillis() - currentTimeMillis2;
            return false;
        }
        if (upperCase.equals("AGENT")) {
            this.handleAgent(s);
            return false;
        }
        if (!this.isValidPropertyName(upperCase)) {
            throw new VCardException("Unknown property name: \"" + upperCase + "\"");
        }
        if (upperCase.equals("BEGIN")) {
            if (s.equals("VCARD")) {
                throw new VCardNestedException("This vCard has nested vCard data in it.");
            }
            throw new VCardException("Unknown BEGIN type: " + s);
        }
        else {
            if (upperCase.equals("VERSION") && !s.equals(this.getVersion())) {
                throw new VCardVersionException("Incompatible version: " + s + " != " + this.getVersion());
            }
            final long currentTimeMillis3 = System.currentTimeMillis();
            this.handlePropertyValue(upperCase, s);
            this.mTimeParseItem2 += System.currentTimeMillis() - currentTimeMillis3;
            return false;
        }
    }
    
    protected void parseItems() throws IOException, VCardException {
        if (this.mBuilder != null) {
            final long currentTimeMillis = System.currentTimeMillis();
            this.mBuilder.startProperty();
            this.mTimeStartProperty += System.currentTimeMillis() - currentTimeMillis;
        }
        int i;
        final boolean b = (i = (this.parseItem() ? 1 : 0)) != 0;
        if (this.mBuilder != null && (i = (b ? 1 : 0)) == 0) {
            final long currentTimeMillis2 = System.currentTimeMillis();
            this.mBuilder.endProperty();
            this.mTimeEndProperty += System.currentTimeMillis() - currentTimeMillis2;
            i = (b ? 1 : 0);
        }
        while (i == 0) {
            if (this.mBuilder != null) {
                final long currentTimeMillis3 = System.currentTimeMillis();
                this.mBuilder.startProperty();
                this.mTimeStartProperty += System.currentTimeMillis() - currentTimeMillis3;
            }
            final boolean b2 = (i = (this.parseItem() ? 1 : 0)) != 0;
            if (this.mBuilder != null && (i = (b2 ? 1 : 0)) == 0) {
                final long currentTimeMillis4 = System.currentTimeMillis();
                this.mBuilder.endProperty();
                this.mTimeEndProperty += System.currentTimeMillis() - currentTimeMillis4;
                i = (b2 ? 1 : 0);
            }
        }
    }
    
    protected void parseVCardFile() throws IOException, VCardException {
        for (boolean b = true; !this.mCanceled && this.parseOneVCard(b); b = false) {}
        if (this.mNestCount > 0) {
            boolean b2 = true;
            for (int i = 0; i < this.mNestCount; ++i) {
                this.readEndVCard(b2, true);
                b2 = false;
            }
        }
    }
    
    protected boolean readBeginVCard(final boolean b) throws IOException, VCardException {
        while (true) {
            final String line = this.getLine();
            if (line == null) {
                return false;
            }
            if (line.trim().length() <= 0) {
                continue;
            }
            final String[] split = line.split(":", 2);
            if (split.length == 2 && split[0].trim().equalsIgnoreCase("BEGIN") && split[1].trim().equalsIgnoreCase("VCARD")) {
                return true;
            }
            if (!b) {
                if (this.mNestCount > 0) {
                    this.mPreviousLine = line;
                    return false;
                }
                throw new VCardException("Expected String \"BEGIN:VCARD\" did not come (Instead, \"" + line + "\" came)");
            }
            else {
                if (!b) {
                    throw new VCardException("Reached where must not be reached.");
                }
                continue;
            }
        }
    }
    
    protected void readEndVCard(boolean b, final boolean b2) throws IOException, VCardException {
        do {
            String s;
            if (b) {
                s = this.mPreviousLine;
            }
            else {
                do {
                    s = this.getLine();
                    if (s == null) {
                        throw new VCardException("Expected END:VCARD was not found.");
                    }
                } while (s.trim().length() <= 0);
            }
            final String[] split = s.split(":", 2);
            if (split.length == 2 && split[0].trim().equalsIgnoreCase("END") && split[1].trim().equalsIgnoreCase("VCARD")) {
                return;
            }
            if (!b2) {
                throw new VCardException("END:VCARD != \"" + this.mPreviousLine + "\"");
            }
            b = false;
        } while (b2);
    }
    
    protected String[] separateLineAndHandleGroup(final String s) throws VCardException {
        final int length = s.length();
        int n = 0;
        int n2 = 0;
        final String[] array = new String[2];
        int n3 = 0;
        for (int i = 0; i < length; ++i, n2 = n3) {
            final char char1 = s.charAt(i);
            switch (n) {
                default: {
                    n3 = n2;
                    break;
                }
                case 0: {
                    if (char1 == ':') {
                        final String substring = s.substring(n2, i);
                        if (substring.equalsIgnoreCase("END")) {
                            this.mPreviousLine = s;
                            return null;
                        }
                        if (this.mBuilder != null) {
                            this.mBuilder.propertyName(substring);
                        }
                        array[0] = substring;
                        if (i < length - 1) {
                            array[1] = s.substring(i + 1);
                            return array;
                        }
                        array[1] = "";
                        return array;
                    }
                    else {
                        if (char1 == '.') {
                            final String substring2 = s.substring(n2, i);
                            if (this.mBuilder != null) {
                                this.mBuilder.propertyGroup(substring2);
                            }
                            n3 = i + 1;
                            break;
                        }
                        n3 = n2;
                        if (char1 != ';') {
                            break;
                        }
                        final String substring3 = s.substring(n2, i);
                        if (substring3.equalsIgnoreCase("END")) {
                            this.mPreviousLine = s;
                            return null;
                        }
                        if (this.mBuilder != null) {
                            this.mBuilder.propertyName(substring3);
                        }
                        array[0] = substring3;
                        n3 = i + 1;
                        n = 1;
                        break;
                    }
                    break;
                }
                case 1: {
                    if (char1 == '\"') {
                        n = 2;
                        n3 = n2;
                        break;
                    }
                    if (char1 == ';') {
                        this.handleParams(s.substring(n2, i));
                        n3 = i + 1;
                        break;
                    }
                    n3 = n2;
                    if (char1 != ':') {
                        break;
                    }
                    this.handleParams(s.substring(n2, i));
                    if (i < length - 1) {
                        array[1] = s.substring(i + 1);
                        return array;
                    }
                    array[1] = "";
                    return array;
                }
                case 2: {
                    n3 = n2;
                    if (char1 == '\"') {
                        n = 1;
                        n3 = n2;
                        break;
                    }
                    break;
                }
            }
        }
        throw new VCardException("Invalid line: \"" + s + "\"");
    }
    
    public void showDebugInfo() {
        Log.d("VCardParser_V21", "total parsing time:  " + this.mTimeTotal + " ms");
        if (this.mReader instanceof CustomBufferedReader) {
            Log.d("VCardParser_V21", "total readLine time: " + ((CustomBufferedReader)this.mReader).getTotalmillisecond() + " ms");
        }
        Log.d("VCardParser_V21", "mTimeStartRecord: " + this.mTimeStartRecord + " ms");
        Log.d("VCardParser_V21", "mTimeEndRecord: " + this.mTimeEndRecord + " ms");
        Log.d("VCardParser_V21", "mTimeParseItem1: " + this.mTimeParseItem1 + " ms");
        Log.d("VCardParser_V21", "mTimeParseItem2: " + this.mTimeParseItem2 + " ms");
        Log.d("VCardParser_V21", "mTimeParseItem3: " + this.mTimeParseItem3 + " ms");
        Log.d("VCardParser_V21", "mTimeHandlePropertyValue1: " + this.mTimeHandlePropertyValue1 + " ms");
        Log.d("VCardParser_V21", "mTimeHandlePropertyValue2: " + this.mTimeHandlePropertyValue2 + " ms");
        Log.d("VCardParser_V21", "mTimeHandlePropertyValue3: " + this.mTimeHandlePropertyValue3 + " ms");
    }
}
