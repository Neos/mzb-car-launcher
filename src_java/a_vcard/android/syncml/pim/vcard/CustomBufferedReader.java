package a_vcard.android.syncml.pim.vcard;

import java.io.*;

class CustomBufferedReader extends BufferedReader
{
    private long mTime;
    
    public CustomBufferedReader(final Reader reader) {
        super(reader);
    }
    
    public long getTotalmillisecond() {
        return this.mTime;
    }
    
    @Override
    public String readLine() throws IOException {
        final long currentTimeMillis = System.currentTimeMillis();
        final String line = super.readLine();
        this.mTime += System.currentTimeMillis() - currentTimeMillis;
        return line;
    }
}
