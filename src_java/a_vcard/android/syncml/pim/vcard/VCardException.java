package a_vcard.android.syncml.pim.vcard;

public class VCardException extends Exception
{
    public VCardException() {
    }
    
    public VCardException(final String s) {
        super(s);
    }
}
