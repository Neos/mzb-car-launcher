package a_vcard.android.syncml.pim.vcard;

import a_vcard.android.syncml.pim.*;
import java.util.*;

public class VCardSourceDetector implements VBuilder
{
    private static Set<String> APPLE_SIGNS;
    private static Set<String> FOMA_SIGNS;
    private static Set<String> JAPANESE_MOBILE_PHONE_SIGNS;
    static final int TYPE_APPLE = 1;
    static final int TYPE_FOMA = 3;
    private static String TYPE_FOMA_CHARSET_SIGN;
    static final int TYPE_JAPANESE_MOBILE_PHONE = 2;
    static final int TYPE_UNKNOWN = 0;
    static final int TYPE_WINDOWS_MOBILE_JP = 4;
    private static Set<String> WINDOWS_MOBILE_PHONE_SIGNS;
    private boolean mNeedParseSpecifiedCharset;
    private String mSpecifiedCharset;
    private int mType;
    
    static {
        VCardSourceDetector.APPLE_SIGNS = new HashSet<String>(Arrays.asList("X-PHONETIC-FIRST-NAME", "X-PHONETIC-MIDDLE-NAME", "X-PHONETIC-LAST-NAME", "X-ABADR", "X-ABUID"));
        VCardSourceDetector.JAPANESE_MOBILE_PHONE_SIGNS = new HashSet<String>(Arrays.asList("X-GNO", "X-GN", "X-REDUCTION"));
        VCardSourceDetector.WINDOWS_MOBILE_PHONE_SIGNS = new HashSet<String>(Arrays.asList("X-MICROSOFT-ASST_TEL", "X-MICROSOFT-ASSISTANT", "X-MICROSOFT-OFFICELOC"));
        VCardSourceDetector.FOMA_SIGNS = new HashSet<String>(Arrays.asList("X-SD-VERN", "X-SD-FORMAT_VER", "X-SD-CATEGORIES", "X-SD-CLASS", "X-SD-DCREATED", "X-SD-DESCRIPTION"));
        VCardSourceDetector.TYPE_FOMA_CHARSET_SIGN = "X-SD-CHAR_CODE";
    }
    
    public VCardSourceDetector() {
        this.mType = 0;
    }
    
    @Override
    public void end() {
    }
    
    @Override
    public void endProperty() {
    }
    
    @Override
    public void endRecord() {
    }
    
    public String getEstimatedCharset() {
        if (this.mSpecifiedCharset != null) {
            return this.mSpecifiedCharset;
        }
        switch (this.mType) {
            default: {
                return null;
            }
            case 2:
            case 3:
            case 4: {
                return "SHIFT_JIS";
            }
            case 1: {
                return "UTF-8";
            }
        }
    }
    
    int getType() {
        return this.mType;
    }
    
    @Override
    public void propertyGroup(final String s) {
    }
    
    @Override
    public void propertyName(final String s) {
        if (s.equalsIgnoreCase(VCardSourceDetector.TYPE_FOMA_CHARSET_SIGN)) {
            this.mType = 3;
            this.mNeedParseSpecifiedCharset = true;
        }
        else if (this.mType == 0) {
            if (VCardSourceDetector.WINDOWS_MOBILE_PHONE_SIGNS.contains(s)) {
                this.mType = 4;
                return;
            }
            if (VCardSourceDetector.FOMA_SIGNS.contains(s)) {
                this.mType = 3;
                return;
            }
            if (VCardSourceDetector.JAPANESE_MOBILE_PHONE_SIGNS.contains(s)) {
                this.mType = 2;
                return;
            }
            if (VCardSourceDetector.APPLE_SIGNS.contains(s)) {
                this.mType = 1;
            }
        }
    }
    
    @Override
    public void propertyParamType(final String s) {
    }
    
    @Override
    public void propertyParamValue(final String s) {
    }
    
    @Override
    public void propertyValues(final List<String> list) {
        if (this.mNeedParseSpecifiedCharset && list.size() > 0) {
            this.mSpecifiedCharset = list.get(0);
        }
    }
    
    @Override
    public void start() {
    }
    
    @Override
    public void startProperty() {
        this.mNeedParseSpecifiedCharset = false;
    }
    
    @Override
    public void startRecord(final String s) {
    }
}
