package a_vcard.android.syncml.pim.vcard;

public class VCardVersionException extends VCardException
{
    public VCardVersionException() {
    }
    
    public VCardVersionException(final String s) {
        super(s);
    }
}
