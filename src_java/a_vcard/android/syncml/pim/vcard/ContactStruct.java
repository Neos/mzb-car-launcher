package a_vcard.android.syncml.pim.vcard;

import a_vcard.android.util.*;
import a_vcard.android.syncml.pim.*;
import a_vcard.android.text.*;
import java.util.*;
import a_vcard.android.telephony.*;

public class ContactStruct
{
    private static final String LOG_TAG = "ContactStruct";
    public static final int NAME_ORDER_TYPE_ENGLISH = 0;
    public static final int NAME_ORDER_TYPE_JAPANESE = 1;
    @Deprecated
    public String company;
    public List<ContactMethod> contactmethodList;
    public Map<String, List<String>> extensionMap;
    public String name;
    public List<String> notes;
    public List<OrganizationData> organizationList;
    public List<PhoneData> phoneList;
    public String phoneticName;
    public byte[] photoBytes;
    public String photoType;
    public String title;
    
    public ContactStruct() {
        this.notes = new ArrayList<String>();
    }
    
    public static ContactStruct constructContactFromVNode(final VNode vNode, final int n) {
        ContactStruct contactStruct;
        if (!vNode.VName.equals("VCARD")) {
            Log.e("ContactStruct", "Non VCARD data is inserted.");
            contactStruct = null;
        }
        else {
            String name = null;
            String nameFromNProperty = null;
            String propValue = null;
            String propValue2 = null;
            String propValue3 = null;
            final ContactStruct contactStruct2 = new ContactStruct();
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
        Label_0369_Outer:
            for (final PropertyNode propertyNode : vNode.propList) {
                final String propName = propertyNode.propName;
                if (!TextUtils.isEmpty(propertyNode.propValue) && !propName.equals("VERSION")) {
                    if (propName.equals("FN")) {
                        name = propertyNode.propValue;
                    }
                    else if (propName.equals("NAME") && name == null) {
                        name = propertyNode.propValue;
                    }
                    else if (propName.equals("N")) {
                        nameFromNProperty = getNameFromNProperty(propertyNode.propValue_vector, n);
                    }
                    else if (propName.equals("SORT-STRING")) {
                        contactStruct2.phoneticName = propertyNode.propValue;
                    }
                    else if (propName.equals("SOUND")) {
                        if (propertyNode.paramMap_TYPE.contains("X-IRMC-N") && contactStruct2.phoneticName == null) {
                            final StringBuilder sb = new StringBuilder();
                            final String propValue4 = propertyNode.propValue;
                            for (int length = propValue4.length(), i = 0; i < length; ++i) {
                                final char char1 = propValue4.charAt(i);
                                if (char1 != ';') {
                                    sb.append(char1);
                                }
                            }
                            contactStruct2.phoneticName = sb.toString();
                        }
                        else {
                            contactStruct2.addExtension(propertyNode);
                        }
                    }
                    else {
                        if (propName.equals("ADR")) {
                            final List<String> propValue_vector = propertyNode.propValue_vector;
                            final boolean b = true;
                            final Iterator<String> iterator2 = propValue_vector.iterator();
                            while (true) {
                                do {
                                    final boolean b2 = b;
                                    if (iterator2.hasNext()) {
                                        continue Label_0369_Outer;
                                    }
                                    if (!b2) {
                                        int n6 = -1;
                                        String substring = "";
                                        boolean b3 = false;
                                        for (final String s : propertyNode.paramMap_TYPE) {
                                            if (s.equals("PREF") && n2 == 0) {
                                                n2 = 1;
                                                b3 = true;
                                            }
                                            else if (s.equalsIgnoreCase("HOME")) {
                                                n6 = 1;
                                                substring = "";
                                            }
                                            else if (s.equalsIgnoreCase("WORK") || s.equalsIgnoreCase("COMPANY")) {
                                                n6 = 2;
                                                substring = "";
                                            }
                                            else {
                                                if (s.equalsIgnoreCase("POSTAL")) {
                                                    continue Label_0369_Outer;
                                                }
                                                if (s.equalsIgnoreCase("PARCEL") || s.equalsIgnoreCase("DOM") || s.equalsIgnoreCase("INTL")) {
                                                    continue Label_0369_Outer;
                                                }
                                                if (s.toUpperCase().startsWith("X-") && n6 < 0) {
                                                    n6 = 0;
                                                    substring = s.substring(2);
                                                }
                                                else {
                                                    if (n6 >= 0) {
                                                        continue Label_0369_Outer;
                                                    }
                                                    n6 = 0;
                                                    substring = s;
                                                }
                                            }
                                        }
                                        int n7;
                                        if ((n7 = n6) < 0) {
                                            n7 = 1;
                                        }
                                        final List<String> propValue_vector2 = propertyNode.propValue_vector;
                                        final int size = propValue_vector2.size();
                                        String s4;
                                        if (size > 1) {
                                            final StringBuilder sb2 = new StringBuilder();
                                            final boolean b4 = true;
                                            int n8 = 1;
                                            if (Locale.getDefault().getCountry().equals(Locale.JAPAN.getCountry())) {
                                                int n9;
                                                for (int j = size - 1; j >= 0; --j, n8 = n9) {
                                                    final String s2 = propValue_vector2.get(j);
                                                    n9 = n8;
                                                    if (s2.length() > 0) {
                                                        if (n8 == 0) {
                                                            sb2.append(' ');
                                                        }
                                                        sb2.append(s2);
                                                        n9 = 0;
                                                    }
                                                }
                                            }
                                            else {
                                                int k = 0;
                                                int n10 = b4 ? 1 : 0;
                                                while (k < size) {
                                                    final String s3 = propValue_vector2.get(k);
                                                    int n11 = n10;
                                                    if (s3.length() > 0) {
                                                        if (n10 == 0) {
                                                            sb2.append(' ');
                                                        }
                                                        sb2.append(s3);
                                                        n11 = 0;
                                                    }
                                                    ++k;
                                                    n10 = n11;
                                                }
                                            }
                                            s4 = sb2.toString().trim();
                                        }
                                        else {
                                            s4 = propertyNode.propValue;
                                        }
                                        contactStruct2.addContactmethod(2, n7, s4, substring, b3);
                                        continue Label_0369_Outer;
                                    }
                                    continue Label_0369_Outer;
                                } while (iterator2.next().length() <= 0);
                                final boolean b2 = false;
                                continue;
                            }
                        }
                        if (propName.equals("ORG")) {
                            boolean b5 = false;
                            final Iterator<String> iterator4 = propertyNode.paramMap_TYPE.iterator();
                            while (iterator4.hasNext()) {
                                if (iterator4.next().equals("PREF") && n5 == 0) {
                                    n5 = 1;
                                    b5 = true;
                                }
                            }
                            final List<String> propValue_vector3 = propertyNode.propValue_vector;
                            propValue_vector3.size();
                            final StringBuilder sb3 = new StringBuilder();
                            final Iterator<String> iterator5 = propValue_vector3.iterator();
                            while (iterator5.hasNext()) {
                                sb3.append(iterator5.next());
                                if (iterator5.hasNext()) {
                                    sb3.append(' ');
                                }
                            }
                            contactStruct2.addOrganization(1, sb3.toString(), "", b5);
                        }
                        else if (propName.equals("TITLE")) {
                            contactStruct2.setPosition(propertyNode.propValue);
                        }
                        else if (propName.equals("ROLE")) {
                            contactStruct2.setPosition(propertyNode.propValue);
                        }
                        else if (propName.equals("PHOTO")) {
                            final String asString = propertyNode.paramMap.getAsString("VALUE");
                            if (asString != null && asString.equals("URL")) {
                                continue;
                            }
                            contactStruct2.photoBytes = propertyNode.propValue_bytes;
                            final String asString2 = propertyNode.paramMap.getAsString("TYPE");
                            if (asString2 == null) {
                                continue;
                            }
                            contactStruct2.photoType = asString2;
                        }
                        else if (propName.equals("LOGO")) {
                            final String asString3 = propertyNode.paramMap.getAsString("VALUE");
                            if ((asString3 != null && asString3.equals("URL")) || contactStruct2.photoBytes != null) {
                                continue;
                            }
                            contactStruct2.photoBytes = propertyNode.propValue_bytes;
                            final String asString4 = propertyNode.paramMap.getAsString("TYPE");
                            if (asString4 == null) {
                                continue;
                            }
                            contactStruct2.photoType = asString4;
                        }
                        else if (propName.equals("EMAIL")) {
                            int n12 = -1;
                            String substring2 = null;
                            boolean b6 = false;
                            for (final String s5 : propertyNode.paramMap_TYPE) {
                                if (s5.equals("PREF") && n4 == 0) {
                                    n4 = 1;
                                    b6 = true;
                                }
                                else if (s5.equalsIgnoreCase("HOME")) {
                                    n12 = 1;
                                }
                                else if (s5.equalsIgnoreCase("WORK")) {
                                    n12 = 2;
                                }
                                else if (s5.equalsIgnoreCase("CELL")) {
                                    n12 = 0;
                                    substring2 = "_AUTO_CELL";
                                }
                                else if (s5.toUpperCase().startsWith("X-") && n12 < 0) {
                                    n12 = 0;
                                    substring2 = s5.substring(2);
                                }
                                else {
                                    if (n12 >= 0) {
                                        continue;
                                    }
                                    n12 = 0;
                                    substring2 = s5;
                                }
                            }
                            int n13;
                            if ((n13 = n12) < 0) {
                                n13 = 3;
                            }
                            contactStruct2.addContactmethod(1, n13, propertyNode.propValue, substring2, b6);
                        }
                        else if (propName.equals("TEL")) {
                            int n14 = -1;
                            String substring3 = null;
                            boolean b7 = false;
                            boolean b8 = false;
                            final Iterator<String> iterator7 = propertyNode.paramMap_TYPE.iterator();
                            int n15 = n3;
                            while (iterator7.hasNext()) {
                                final String s6 = iterator7.next();
                                if (s6.equals("PREF") && n15 == 0) {
                                    n15 = 1;
                                    b7 = true;
                                }
                                else if (s6.equalsIgnoreCase("HOME")) {
                                    n14 = 1;
                                }
                                else if (s6.equalsIgnoreCase("WORK")) {
                                    n14 = 3;
                                }
                                else if (s6.equalsIgnoreCase("CELL")) {
                                    n14 = 2;
                                }
                                else if (s6.equalsIgnoreCase("PAGER")) {
                                    n14 = 6;
                                }
                                else if (s6.equalsIgnoreCase("FAX")) {
                                    b8 = true;
                                }
                                else {
                                    if (s6.equalsIgnoreCase("VOICE") || s6.equalsIgnoreCase("MSG")) {
                                        continue;
                                    }
                                    if (s6.toUpperCase().startsWith("X-") && n14 < 0) {
                                        n14 = 0;
                                        substring3 = s6.substring(2);
                                    }
                                    else {
                                        if (n14 >= 0) {
                                            continue;
                                        }
                                        n14 = 0;
                                        substring3 = s6;
                                    }
                                }
                            }
                            int n16;
                            if ((n16 = n14) < 0) {
                                n16 = 1;
                            }
                            int n17 = n16;
                            if (b8) {
                                if (n16 == 1) {
                                    n17 = 5;
                                }
                                else if ((n17 = n16) == 3) {
                                    n17 = 4;
                                }
                            }
                            contactStruct2.addPhone(n17, propertyNode.propValue, substring3, b7);
                            n3 = n15;
                        }
                        else if (propName.equals("NOTE")) {
                            contactStruct2.notes.add(propertyNode.propValue);
                        }
                        else if (propName.equals("BDAY")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("URL")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("REV")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("UID")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("KEY")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("MAILER")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("TZ")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("GEO")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("NICKNAME")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("CLASS")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("PROFILE")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("CATEGORIES")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("SOURCE")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("PRODID")) {
                            contactStruct2.addExtension(propertyNode);
                        }
                        else if (propName.equals("X-PHONETIC-FIRST-NAME")) {
                            propValue = propertyNode.propValue;
                        }
                        else if (propName.equals("X-PHONETIC-MIDDLE-NAME")) {
                            propValue2 = propertyNode.propValue;
                        }
                        else if (propName.equals("X-PHONETIC-LAST-NAME")) {
                            propValue3 = propertyNode.propValue;
                        }
                        else {
                            contactStruct2.addExtension(propertyNode);
                        }
                    }
                }
            }
            if (name != null) {
                contactStruct2.name = name;
            }
            else if (nameFromNProperty != null) {
                contactStruct2.name = nameFromNProperty;
            }
            else {
                contactStruct2.name = "";
            }
            if (contactStruct2.phoneticName == null && (propValue != null || propValue2 != null || propValue3 != null)) {
                String s7;
                if (n == 1) {
                    s7 = propValue;
                }
                else {
                    s7 = propValue3;
                    propValue3 = propValue;
                }
                final StringBuilder sb4 = new StringBuilder();
                if (propValue3 != null) {
                    sb4.append(propValue3);
                }
                if (propValue2 != null) {
                    sb4.append(propValue2);
                }
                if (s7 != null) {
                    sb4.append(s7);
                }
                contactStruct2.phoneticName = sb4.toString();
            }
            if (contactStruct2.phoneticName != null) {
                contactStruct2.phoneticName = contactStruct2.phoneticName.trim();
            }
            if (n3 == 0 && contactStruct2.phoneList != null && contactStruct2.phoneList.size() > 0) {
                contactStruct2.phoneList.get(0).isPrimary = true;
            }
            if (n2 == 0 && contactStruct2.contactmethodList != null) {
                for (final ContactMethod contactMethod : contactStruct2.contactmethodList) {
                    if (contactMethod.kind == 2) {
                        contactMethod.isPrimary = true;
                        break;
                    }
                }
            }
            if (n4 == 0 && contactStruct2.contactmethodList != null) {
                for (final ContactMethod contactMethod2 : contactStruct2.contactmethodList) {
                    if (contactMethod2.kind == 1) {
                        contactMethod2.isPrimary = true;
                        break;
                    }
                }
            }
            contactStruct = contactStruct2;
            if (n5 == 0) {
                contactStruct = contactStruct2;
                if (contactStruct2.organizationList != null) {
                    contactStruct = contactStruct2;
                    if (contactStruct2.organizationList.size() > 0) {
                        contactStruct2.organizationList.get(0).isPrimary = true;
                        return contactStruct2;
                    }
                }
            }
        }
        return contactStruct;
    }
    
    private static String getNameFromNProperty(final List<String> list, int n) {
        final int size = list.size();
        if (size > 1) {
            final StringBuilder sb = new StringBuilder();
            int n3;
            final int n2 = n3 = 1;
            if (size > 3) {
                n3 = n2;
                if (list.get(3).length() > 0) {
                    sb.append(list.get(3));
                    n3 = 0;
                }
            }
            String s;
            String s2;
            if (n == 1) {
                s = list.get(0);
                s2 = list.get(1);
            }
            else {
                s = list.get(1);
                s2 = list.get(0);
            }
            n = n3;
            if (s.length() > 0) {
                if (n3 == 0) {
                    sb.append(' ');
                }
                sb.append(s);
                n = 0;
            }
            int n4 = n;
            if (size > 2) {
                n4 = n;
                if (list.get(2).length() > 0) {
                    if (n == 0) {
                        sb.append(' ');
                    }
                    sb.append(list.get(2));
                    n4 = 0;
                }
            }
            n = n4;
            if (s2.length() > 0) {
                if (n4 == 0) {
                    sb.append(' ');
                }
                sb.append(s2);
                n = 0;
            }
            if (size > 4 && list.get(4).length() > 0) {
                if (n == 0) {
                    sb.append(' ');
                }
                sb.append(list.get(4));
            }
            return sb.toString();
        }
        if (size == 1) {
            return list.get(0);
        }
        return "";
    }
    
    public void addContactmethod(final int kind, final int type, final String data, final String label, final boolean isPrimary) {
        if (this.contactmethodList == null) {
            this.contactmethodList = new ArrayList<ContactMethod>();
        }
        final ContactMethod contactMethod = new ContactMethod();
        contactMethod.kind = kind;
        contactMethod.type = type;
        contactMethod.data = data;
        contactMethod.label = label;
        contactMethod.isPrimary = isPrimary;
        this.contactmethodList.add(contactMethod);
    }
    
    public void addExtension(final PropertyNode propertyNode) {
        if (propertyNode.propValue.length() == 0) {
            return;
        }
        final String propName = propertyNode.propName;
        if (this.extensionMap == null) {
            this.extensionMap = new HashMap<String, List<String>>();
        }
        List<String> list;
        if (!this.extensionMap.containsKey(propName)) {
            list = new ArrayList<String>();
            this.extensionMap.put(propName, list);
        }
        else {
            list = this.extensionMap.get(propName);
        }
        list.add(propertyNode.encode());
    }
    
    public void addOrganization(final int type, final String companyName, final String positionName, final boolean isPrimary) {
        if (this.organizationList == null) {
            this.organizationList = new ArrayList<OrganizationData>();
        }
        final OrganizationData organizationData = new OrganizationData();
        organizationData.type = type;
        organizationData.companyName = companyName;
        organizationData.positionName = positionName;
        organizationData.isPrimary = isPrimary;
        this.organizationList.add(organizationData);
    }
    
    public void addPhone(int i, String trim, final String label, final boolean isPrimary) {
        if (this.phoneList == null) {
            this.phoneList = new ArrayList<PhoneData>();
        }
        final PhoneData phoneData = new PhoneData();
        phoneData.type = i;
        final StringBuilder sb = new StringBuilder();
        trim = trim.trim();
        int length;
        char char1;
        for (length = trim.length(), i = 0; i < length; ++i) {
            char1 = trim.charAt(i);
            if (('0' <= char1 && char1 <= '9') || (i == 0 && char1 == '+')) {
                sb.append(char1);
            }
        }
        phoneData.data = PhoneNumberUtils.formatNumber(sb.toString());
        phoneData.label = label;
        phoneData.isPrimary = isPrimary;
        this.phoneList.add(phoneData);
    }
    
    public String displayString() {
        if (this.name.length() > 0) {
            return this.name;
        }
        if (this.contactmethodList != null && this.contactmethodList.size() > 0) {
            for (final ContactMethod contactMethod : this.contactmethodList) {
                if (contactMethod.kind == 1 && contactMethod.isPrimary) {
                    return contactMethod.data;
                }
            }
        }
        if (this.phoneList != null && this.phoneList.size() > 0) {
            for (final PhoneData phoneData : this.phoneList) {
                if (phoneData.isPrimary) {
                    return phoneData.data;
                }
            }
        }
        return "";
    }
    
    public boolean isIgnorable() {
        return TextUtils.isEmpty(this.name) && TextUtils.isEmpty(this.phoneticName) && (this.phoneList == null || this.phoneList.size() == 0) && (this.contactmethodList == null || this.contactmethodList.size() == 0);
    }
    
    public void setPosition(final String positionName) {
        if (this.organizationList == null) {
            this.organizationList = new ArrayList<OrganizationData>();
        }
        int size;
        if ((size = this.organizationList.size()) == 0) {
            this.addOrganization(2, "", null, false);
            size = 1;
        }
        this.organizationList.get(size - 1).positionName = positionName;
    }
    
    public static class ContactMethod
    {
        public String data;
        public boolean isPrimary;
        public int kind;
        public String label;
        public int type;
    }
    
    public static class OrganizationData
    {
        public String companyName;
        public boolean isPrimary;
        public String positionName;
        public int type;
    }
    
    public static class PhoneData
    {
        public String data;
        public boolean isPrimary;
        public String label;
        public int type;
    }
}
