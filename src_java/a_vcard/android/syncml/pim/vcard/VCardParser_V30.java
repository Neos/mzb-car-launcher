package a_vcard.android.syncml.pim.vcard;

import java.util.*;
import java.io.*;
import a_vcard.android.util.*;

public class VCardParser_V30 extends VCardParser_V21
{
    private static final String LOG_TAG = "VCardParser_V30";
    private static final HashSet<String> acceptablePropsWithParam;
    private static final HashSet<String> acceptablePropsWithoutParam;
    private static final HashSet<String> sAcceptableEncodingV30;
    private String mPreviousLine;
    
    static {
        acceptablePropsWithParam = new HashSet<String>(Arrays.asList("BEGIN", "LOGO", "PHOTO", "LABEL", "FN", "TITLE", "SOUND", "VERSION", "TEL", "EMAIL", "TZ", "GEO", "NOTE", "URL", "BDAY", "ROLE", "REV", "UID", "KEY", "MAILER", "NAME", "PROFILE", "SOURCE", "NICKNAME", "CLASS", "SORT-STRING", "CATEGORIES", "PRODID"));
        sAcceptableEncodingV30 = new HashSet<String>(Arrays.asList("7BIT", "8BIT", "BASE64", "B"));
        acceptablePropsWithoutParam = new HashSet<String>();
    }
    
    @Override
    protected String getBase64(String line) throws IOException, VCardException {
        final StringBuilder sb = new StringBuilder();
        sb.append(line);
        Block_3: {
            while (true) {
                line = this.getLine();
                if (line == null) {
                    throw new VCardException("File ended during parsing BASE64 binary");
                }
                if (line.length() == 0) {
                    break;
                }
                if (!line.startsWith(" ") && !line.startsWith("\t")) {
                    break Block_3;
                }
                sb.append(line);
            }
            return sb.toString();
        }
        this.mPreviousLine = line;
        return sb.toString();
    }
    
    @Override
    protected String getLine() throws IOException {
        if (this.mPreviousLine != null) {
            final String mPreviousLine = this.mPreviousLine;
            this.mPreviousLine = null;
            return mPreviousLine;
        }
        return this.mReader.readLine();
    }
    
    @Override
    protected String getNonEmptyLine() throws IOException, VCardException {
        StringBuilder sb = null;
        while (true) {
            final String line = this.mReader.readLine();
            if (line == null) {
                if (sb != null) {
                    return sb.toString();
                }
                if (this.mPreviousLine != null) {
                    final String mPreviousLine = this.mPreviousLine;
                    this.mPreviousLine = null;
                    return mPreviousLine;
                }
                throw new VCardException("Reached end of buffer.");
            }
            else if (line.length() == 0) {
                if (sb != null) {
                    return sb.toString();
                }
                if (this.mPreviousLine != null) {
                    final String mPreviousLine2 = this.mPreviousLine;
                    this.mPreviousLine = null;
                    return mPreviousLine2;
                }
                continue;
            }
            else if (line.charAt(0) == ' ' || line.charAt(0) == '\t') {
                if (sb != null) {
                    sb.append(line.substring(1));
                }
                else {
                    if (this.mPreviousLine == null) {
                        throw new VCardException("Space exists at the beginning of the line");
                    }
                    sb = new StringBuilder();
                    sb.append(this.mPreviousLine);
                    this.mPreviousLine = null;
                    sb.append(line.substring(1));
                }
            }
            else {
                if (this.mPreviousLine != null) {
                    final String mPreviousLine3 = this.mPreviousLine;
                    this.mPreviousLine = line;
                    return mPreviousLine3;
                }
                this.mPreviousLine = line;
                if (sb != null) {
                    return sb.toString();
                }
                continue;
            }
        }
    }
    
    @Override
    protected String getVersion() {
        return "3.0";
    }
    
    @Override
    protected void handleAgent(final String s) throws VCardException {
        throw new VCardException("AGENT in vCard 3.0 is not supported yet.");
    }
    
    @Override
    protected void handleAnyParam(final String s, final String s2) {
        super.handleAnyParam(s, s2);
    }
    
    @Override
    protected void handleParams(final String s) throws VCardException {
        try {
            super.handleParams(s);
        }
        catch (VCardException ex) {
            final String[] split = s.split("=", 2);
            if (split.length == 2) {
                this.handleAnyParam(split[0], split[1]);
                return;
            }
            throw new VCardException("Unknown params value: " + s);
        }
    }
    
    @Override
    protected void handleType(final String s) {
        final String[] split = s.split(",");
        this.mBuilder.propertyParamType("TYPE");
        for (int length = split.length, i = 0; i < length; ++i) {
            final String s2 = split[i];
            if (s2.length() >= 2 && s2.startsWith("\"") && s2.endsWith("\"")) {
                this.mBuilder.propertyParamValue(s2.substring(1, s2.length() - 1));
            }
            else {
                this.mBuilder.propertyParamValue(s2);
            }
        }
    }
    
    @Override
    protected boolean isValidEncoding(final String s) {
        return VCardParser_V30.sAcceptableEncodingV30.contains(s.toUpperCase());
    }
    
    @Override
    protected boolean isValidPropertyName(final String s) {
        if (!VCardParser_V30.acceptablePropsWithParam.contains(s) && !VCardParser_V30.acceptablePropsWithoutParam.contains(s) && !s.startsWith("X-") && !this.mWarningValueMap.contains(s)) {
            this.mWarningValueMap.add(s);
            Log.w("VCardParser_V30", "Property name unsupported by vCard 3.0: " + s);
        }
        return true;
    }
    
    @Override
    protected String maybeUnescape(final char c) {
        if (c == 'n' || c == 'N') {
            return "\r\n";
        }
        return String.valueOf(c);
    }
    
    @Override
    protected String maybeUnescapeText(final String s) {
        final StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\\' && i < length - 1) {
                ++i;
                final char char2 = s.charAt(i);
                if (char2 == 'n' || char2 == 'N') {
                    sb.append("\r\n");
                }
                else {
                    sb.append(char2);
                }
            }
            else {
                sb.append(char1);
            }
        }
        return sb.toString();
    }
    
    @Override
    protected boolean readBeginVCard(final boolean b) throws IOException, VCardException {
        return super.readBeginVCard(b);
    }
    
    @Override
    protected void readEndVCard(final boolean b, final boolean b2) throws IOException, VCardException {
        super.readEndVCard(b, b2);
    }
}
