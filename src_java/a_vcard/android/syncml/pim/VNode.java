package a_vcard.android.syncml.pim;

import java.util.*;

public class VNode
{
    public String VName;
    public int parseStatus;
    public ArrayList<PropertyNode> propList;
    
    public VNode() {
        this.propList = new ArrayList<PropertyNode>();
        this.parseStatus = 1;
    }
}
