package a_vcard.android.syncml.pim;

import a_vcard.android.content.*;
import java.util.regex.*;
import org.apache.commons.codec.binary.*;
import java.util.*;

public class PropertyNode
{
    public ContentValues paramMap;
    public Set<String> paramMap_TYPE;
    public Set<String> propGroupSet;
    public String propName;
    public String propValue;
    public byte[] propValue_bytes;
    public List<String> propValue_vector;
    
    public PropertyNode() {
        this.propName = "";
        this.propValue = "";
        this.propValue_vector = new ArrayList<String>();
        this.paramMap = new ContentValues();
        this.paramMap_TYPE = new HashSet<String>();
        this.propGroupSet = new HashSet<String>();
    }
    
    public PropertyNode(final String propName, final String propValue, final List<String> propValue_vector, final byte[] propValue_bytes, final ContentValues paramMap, final Set<String> paramMap_TYPE, final Set<String> propGroupSet) {
        if (propName != null) {
            this.propName = propName;
        }
        else {
            this.propName = "";
        }
        if (propValue != null) {
            this.propValue = propValue;
        }
        else {
            this.propValue = "";
        }
        if (propValue_vector != null) {
            this.propValue_vector = propValue_vector;
        }
        else {
            this.propValue_vector = new ArrayList<String>();
        }
        this.propValue_bytes = propValue_bytes;
        if (paramMap != null) {
            this.paramMap = paramMap;
        }
        else {
            this.paramMap = new ContentValues();
        }
        if (paramMap_TYPE != null) {
            this.paramMap_TYPE = paramMap_TYPE;
        }
        else {
            this.paramMap_TYPE = new HashSet<String>();
        }
        if (propGroupSet != null) {
            this.propGroupSet = propGroupSet;
            return;
        }
        this.propGroupSet = new HashSet<String>();
    }
    
    public static PropertyNode decode(String s) {
        final PropertyNode propertyNode = new PropertyNode();
        s = s.trim();
        if (s.length() != 0) {
            final String[] split = s.split("],");
            for (int length = split.length, i = 0; i < length; ++i) {
                final String s2 = split[i];
                final int index = s2.indexOf(91);
                final String substring = s2.substring(0, index - 1);
                final String[] split2 = Pattern.compile("(?<!\\\\),").split(s2.substring(index + 1), -1);
                if (substring.equals("propName")) {
                    propertyNode.propName = split2[0];
                }
                else if (substring.equals("propGroupSet")) {
                    for (int length2 = split2.length, j = 0; j < length2; ++j) {
                        propertyNode.propGroupSet.add(split2[j]);
                    }
                }
                else if (substring.equals("paramMap")) {
                    final ContentValues paramMap = propertyNode.paramMap;
                    final Set<String> paramMap_TYPE = propertyNode.paramMap_TYPE;
                    for (int length3 = split2.length, k = 0; k < length3; ++k) {
                        final String[] split3 = split2[k].split("=", 2);
                        final String s3 = split3[0];
                        final String replaceAll = split3[1].replaceAll("\\\\,", ",").replaceAll("\\\\\\\\", "\\\\");
                        if (s3.equalsIgnoreCase("TYPE")) {
                            paramMap_TYPE.add(replaceAll);
                        }
                        else {
                            paramMap.put(s3, replaceAll);
                        }
                    }
                }
                else if (substring.equals("propValue")) {
                    final StringBuilder sb = new StringBuilder();
                    final List<String> propValue_vector = propertyNode.propValue_vector;
                    for (int length4 = split2.length, l = 0; l < length4; ++l) {
                        final String replaceAll2 = split2[l].replaceAll("\\\\,", ",").replaceAll("\\\\\\\\", "\\\\");
                        propValue_vector.add(replaceAll2);
                        sb.append(replaceAll2);
                        if (l < length4 - 1) {
                            sb.append(";");
                        }
                    }
                    propertyNode.propValue = sb.toString();
                }
            }
            s = propertyNode.paramMap.getAsString("ENCODING");
            if (s != null && (s.equalsIgnoreCase("BASE64") || s.equalsIgnoreCase("B"))) {
                propertyNode.propValue_bytes = Base64.decodeBase64(propertyNode.propValue_vector.get(0).getBytes());
                return propertyNode;
            }
        }
        return propertyNode;
    }
    
    public String encode() {
        final StringBuilder sb = new StringBuilder();
        if (this.propName.length() > 0) {
            sb.append("propName:[");
            sb.append(this.propName);
            sb.append("],");
        }
        final int size = this.propGroupSet.size();
        if (size > 0) {
            final Set<String> propGroupSet = this.propGroupSet;
            sb.append("propGroup:[");
            int n = 0;
            final Iterator<String> iterator = propGroupSet.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
                if (n < size - 1) {
                    sb.append(",");
                }
                ++n;
            }
            sb.append("],");
        }
        if (this.paramMap.size() > 0 || this.paramMap_TYPE.size() > 0) {
            final ContentValues paramMap = this.paramMap;
            sb.append("paramMap:[");
            final int size2 = this.paramMap.size();
            int n2 = 0;
            for (final Map.Entry<String, Object> entry : paramMap.valueSet()) {
                sb.append(entry.getKey());
                sb.append("=");
                sb.append(entry.getValue().toString().replaceAll("\\\\", "\\\\\\\\").replaceAll(",", "\\\\,"));
                if (n2 < size2 - 1) {
                    sb.append(",");
                }
                ++n2;
            }
            final Set<String> paramMap_TYPE = this.paramMap_TYPE;
            final int size3 = this.paramMap_TYPE.size();
            if (n2 > 0 && size3 > 0) {
                sb.append(",");
            }
            int n3 = 0;
            for (final String s : paramMap_TYPE) {
                sb.append("TYPE=");
                sb.append(s.replaceAll("\\\\", "\\\\\\\\").replaceAll(",", "\\\\,"));
                if (n3 < size3 - 1) {
                    sb.append(",");
                }
                ++n3;
            }
            sb.append("],");
        }
        final int size4 = this.propValue_vector.size();
        if (size4 > 0) {
            sb.append("propValue:[");
            final List<String> propValue_vector = this.propValue_vector;
            for (int i = 0; i < size4; ++i) {
                sb.append(propValue_vector.get(i).replaceAll("\\\\", "\\\\\\\\").replaceAll(",", "\\\\,"));
                if (i < size4 - 1) {
                    sb.append(",");
                }
            }
            sb.append("],");
        }
        return sb.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof PropertyNode) {
            final PropertyNode propertyNode = (PropertyNode)o;
            if (this.propName != null && this.propName.equals(propertyNode.propName) && this.paramMap.equals(propertyNode.paramMap) && this.paramMap_TYPE.equals(propertyNode.paramMap_TYPE) && this.propGroupSet.equals(propertyNode.propGroupSet)) {
                if (this.propValue_bytes != null && Arrays.equals(this.propValue_bytes, propertyNode.propValue_bytes)) {
                    return true;
                }
                if (this.propValue.equals(propertyNode.propValue) && (this.propValue_vector.equals(propertyNode.propValue_vector) || this.propValue_vector.size() == 1 || propertyNode.propValue_vector.size() == 1)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("propName: ");
        sb.append(this.propName);
        sb.append(", paramMap: ");
        sb.append(this.paramMap.toString());
        sb.append(", propmMap_TYPE: ");
        sb.append(this.paramMap_TYPE.toString());
        sb.append(", propGroupSet: ");
        sb.append(this.propGroupSet.toString());
        if (this.propValue_vector != null && this.propValue_vector.size() > 1) {
            sb.append(", propValue_vector size: ");
            sb.append(this.propValue_vector.size());
        }
        if (this.propValue_bytes != null) {
            sb.append(", propValue_bytes size: ");
            sb.append(this.propValue_bytes.length);
        }
        sb.append(", propValue: ");
        sb.append(this.propValue);
        return sb.toString();
    }
}
