package a_vcard.android.util;

import java.io.*;
import java.util.logging.*;

public final class Log
{
    public static final int ASSERT = 7;
    public static final int DEBUG = 3;
    public static final int ERROR = 6;
    public static final int INFO = 4;
    public static final int VERBOSE = 2;
    public static final int WARN = 5;
    private static final Logger logger;
    
    static {
        logger = Logger.getLogger(Log.class.getName());
    }
    
    public static int d(final String s, final String s2) {
        return println(3, s, s2);
    }
    
    public static int d(final String s, final String s2, final Throwable t) {
        return println(3, s, s2 + '\n' + getStackTraceString(t));
    }
    
    public static int e(final String s, final String s2) {
        return println(6, s, s2);
    }
    
    public static int e(final String s, final String s2, final Throwable t) {
        return println(6, s, s2 + '\n' + getStackTraceString(t));
    }
    
    public static String getStackTraceString(final Throwable t) {
        if (t == null) {
            return "";
        }
        final StringWriter stringWriter = new StringWriter();
        t.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }
    
    public static int i(final String s, final String s2) {
        return println(4, s, s2);
    }
    
    public static int i(final String s, final String s2, final Throwable t) {
        return println(4, s, s2 + '\n' + getStackTraceString(t));
    }
    
    public static boolean isLoggable(final String s, final int n) {
        return true;
    }
    
    public static int println(final int n, final String s, final String s2) {
        Log.logger.logp(prioToLevel(n), s, null, s2);
        return 1;
    }
    
    private static Level prioToLevel(final int n) {
        switch (n) {
            default: {
                return Level.WARNING;
            }
            case 7: {
                return Level.ALL;
            }
            case 3: {
                return Level.FINEST;
            }
            case 6: {
                return Level.SEVERE;
            }
            case 4: {
                return Level.INFO;
            }
            case 2: {
                return Level.ALL;
            }
            case 5: {
                return Level.WARNING;
            }
        }
    }
    
    public static int v(final String s, final String s2) {
        return println(2, s, s2);
    }
    
    public static int v(final String s, final String s2, final Throwable t) {
        return println(2, s, s2 + '\n' + getStackTraceString(t));
    }
    
    public static int w(final String s, final String s2) {
        return println(5, s, s2);
    }
    
    public static int w(final String s, final String s2, final Throwable t) {
        return println(5, s, s2 + '\n' + getStackTraceString(t));
    }
    
    public static int w(final String s, final Throwable t) {
        return println(5, s, getStackTraceString(t));
    }
}
