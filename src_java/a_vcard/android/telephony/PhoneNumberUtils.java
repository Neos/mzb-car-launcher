package a_vcard.android.telephony;

import a_vcard.android.text.*;
import java.util.*;

public class PhoneNumberUtils
{
    public static final int FORMAT_JAPAN = 2;
    public static final int FORMAT_NANP = 1;
    public static final int FORMAT_UNKNOWN = 0;
    private static final String[] NANP_COUNTRIES;
    private static final int NANP_STATE_DASH = 4;
    private static final int NANP_STATE_DIGIT = 1;
    private static final int NANP_STATE_ONE = 3;
    private static final int NANP_STATE_PLUS = 2;
    
    static {
        NANP_COUNTRIES = new String[] { "US", "CA", "AS", "AI", "AG", "BS", "BB", "BM", "VG", "KY", "DM", "DO", "GD", "GU", "JM", "PR", "MS", "NP", "KN", "LC", "VC", "TT", "TC", "VI" };
    }
    
    public static void formatJapaneseNumber(final Editable editable) {
        JapanesePhoneNumberFormatter.format(editable);
    }
    
    public static void formatNanpNumber(final Editable editable) {
        final int length = editable.length();
        if (length <= "+1-nnn-nnn-nnnn".length()) {
            final CharSequence subSequence = editable.subSequence(0, length);
            int i = 0;
            while (i < editable.length()) {
                if (editable.charAt(i) == '-') {
                    editable.delete(i, i + 1);
                }
                else {
                    ++i;
                }
            }
            final int length2 = editable.length();
            final int[] array = new int[3];
            int n = 1;
            int n2 = 0;
            int j = 0;
            int n3 = 0;
            while (j < length2) {
                Label_0180: {
                    switch (editable.charAt(j)) {
                        case '1': {
                            if (n2 == 0 || n == 2) {
                                n = 3;
                                break;
                            }
                        }
                        case '0':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9': {
                            if (n == 2) {
                                editable.replace(0, length2, subSequence);
                                return;
                            }
                            if (n == 3) {
                                final int n4 = n3 + 1;
                                array[n3] = j;
                                n3 = n4;
                            }
                            else if (n != 4 && (n2 == 3 || n2 == 6)) {
                                final int n5 = n3 + 1;
                                array[n3] = j;
                                n3 = n5;
                            }
                            n = 1;
                            ++n2;
                            break;
                        }
                        case '-': {
                            n = 4;
                            break;
                        }
                        case '+': {
                            if (j == 0) {
                                n = 2;
                                break;
                            }
                            break Label_0180;
                        }
                    }
                    ++j;
                    continue;
                }
                editable.replace(0, length2, subSequence);
                return;
            }
            if (n2 == 7) {
                --n3;
            }
            for (int k = 0; k < n3; ++k) {
                final int n6 = array[k];
                editable.replace(n6 + k, n6 + k, "-");
            }
            for (int length3 = editable.length(); length3 > 0 && editable.charAt(length3 - 1) == '-'; --length3) {
                editable.delete(length3 - 1, length3);
            }
        }
    }
    
    public static String formatNumber(final String s) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(s);
        formatNumber(spannableStringBuilder, getFormatTypeForLocale(Locale.getDefault()));
        return spannableStringBuilder.toString();
    }
    
    public static void formatNumber(final Editable editable, int n) {
        final int n2 = n = n;
        if (editable.length() > 2) {
            n = n2;
            if (editable.charAt(0) == '+') {
                if (editable.charAt(1) == '1') {
                    n = 1;
                }
                else {
                    if (editable.length() < 3 || editable.charAt(1) != '8' || editable.charAt(2) != '1') {
                        return;
                    }
                    n = 2;
                }
            }
        }
        switch (n) {
            default: {}
            case 1: {
                formatNanpNumber(editable);
            }
            case 2: {
                formatJapaneseNumber(editable);
            }
        }
    }
    
    public static int getFormatTypeForLocale(final Locale locale) {
        final String country = locale.getCountry();
        for (int length = PhoneNumberUtils.NANP_COUNTRIES.length, i = 0; i < length; ++i) {
            if (PhoneNumberUtils.NANP_COUNTRIES[i].equals(country)) {
                return 1;
            }
        }
        if (locale.equals(Locale.JAPAN)) {
            return 2;
        }
        return 0;
    }
}
