package a_vcard.android.telephony;

import a_vcard.android.text.*;

class JapanesePhoneNumberFormatter
{
    private static short[] FORMAT_MAP;
    
    static {
        JapanesePhoneNumberFormatter.FORMAT_MAP = new short[] { -100, 10, 220, -15, 410, 530, -15, 670, 780, 1060, -100, -25, 20, 40, 70, 100, 150, 190, 200, 210, -36, -100, -100, -35, -35, -35, 30, -100, -100, -100, -35, -35, -35, -35, -35, -35, -35, -45, -35, -35, -100, -100, -100, -35, -35, -35, -35, 50, -35, 60, -35, -35, -45, -35, -45, -35, -35, -45, -35, -35, -35, -35, -45, -35, -35, -35, -35, -45, -45, -35, -100, -100, -35, -35, -35, 80, 90, -100, -100, -100, -35, -35, -35, -35, -35, -35, -45, -45, -35, -35, -35, -35, -35, -35, -35, -35, -45, -35, -35, -35, -25, -25, -35, -35, 110, 120, 130, -35, 140, -25, -35, -25, -35, -35, -35, -35, -35, -45, -25, -35, -35, -25, -35, -35, -35, -35, -35, -25, -45, -35, -35, -35, -35, -35, -45, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -45, -45, -35, -35, -100, -100, -35, 160, 170, 180, -35, -35, -100, -100, -35, -35, -45, -35, -45, -45, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -45, -35, -35, -35, -35, -35, -45, -45, -45, -35, -45, -35, -25, -25, -35, -35, -35, -35, -35, -25, -35, -35, -25, -25, -35, -35, -35, -35, -35, -35, -25, -25, -25, -35, -35, -35, -35, -35, -25, -35, -35, -25, -100, -100, 230, 250, 260, 270, 320, 340, 360, 390, -35, -25, -25, 240, -35, -35, -35, -25, -35, -35, -25, -35, -35, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -35, -35, -35, -25, -35, -35, -25, -35, -35, -35, -35, -35, -25, -35, -35, -35, -25, -35, -25, -25, -25, -35, 280, 290, 300, 310, -35, -25, -25, -25, -25, -25, -25, -25, -35, -35, -25, -25, -35, -35, -35, -35, -35, -35, -35, -35, -35, -25, -25, -35, -35, -35, -25, -25, -25, -25, -25, -25, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -25, -35, 330, -35, -35, -35, -35, -35, -25, -35, -35, -35, -35, -35, -25, -25, -25, -25, -35, -25, -25, -25, -35, -25, -35, -35, 350, -35, -25, -35, -35, -35, -35, -35, -35, -35, -25, -25, -35, -25, -35, 370, -35, -35, -25, -35, -35, 380, -25, -35, -35, -25, -25, -35, -35, -35, -35, -35, -25, -35, -25, -25, -25, -25, -35, -35, -35, -35, -25, -35, -25, 400, -35, -35, -35, -35, -25, -35, -25, -35, -35, -35, -35, -25, -25, -25, -25, -25, -15, -15, 420, 460, -25, -25, 470, 480, 500, 510, -15, -25, 430, -25, -25, -25, -25, -25, 440, 450, -25, -35, -35, -35, -35, -35, -35, -35, -35, -35, -25, -25, -35, -35, -25, -25, -25, -35, -35, -35, -15, -25, -15, -15, -15, -15, -15, -25, -25, -15, -25, -25, -25, -25, -25, -25, -35, -25, -35, -35, -35, -25, -25, -35, -25, -35, -35, -35, -25, -25, 490, -15, -25, -25, -25, -35, -35, -25, -35, -35, -15, -35, -35, -35, -35, -35, -35, -35, -35, -15, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -35, -35, -35, -25, -25, -25, 520, -100, -100, -45, -100, -45, -100, -45, -100, -45, -100, -25, -100, -25, 540, 580, 590, 600, 610, 630, 640, -25, -35, -35, -35, -25, -25, -35, -35, -35, 550, -35, -35, -25, -25, -25, -25, 560, 570, -25, -35, -35, -35, -35, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -25, -35, -25, -25, -35, -25, -25, -25, -25, -25, -25, -35, -35, -25, -35, -35, -25, -35, -35, -25, -35, -35, -35, -35, -35, -35, -25, -100, -35, -35, -35, -35, -35, -35, -35, -35, -35, -36, -100, -35, -35, -35, -35, 620, -35, -35, -100, -35, -35, -35, -35, -35, -35, -35, -35, -35, -45, -25, -35, -25, -25, -35, -35, -35, -35, -25, -25, -25, -25, -25, -25, -35, -35, -35, 650, -35, 660, -35, -35, -35, -35, -45, -35, -35, -35, -35, -45, -35, -35, -35, -35, -35, -35, -35, -35, -35, -25, -26, -100, 680, 690, 700, -25, 720, 730, -25, 740, -25, -35, -25, -25, -25, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -35, -35, -35, -35, -35, -35, -100, -35, -35, -35, -35, 710, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -45, -35, -25, -35, -25, -35, -25, -35, -35, -35, -35, -25, -35, -35, -35, -35, -35, -25, -35, -25, -35, -35, -35, -35, -25, -25, 750, 760, 770, -35, -35, -35, -25, -35, -25, -25, -25, -25, -35, -35, -35, -25, -25, -35, -35, -35, -35, -25, -25, -35, -35, -25, -25, -35, -35, -35, -35, -35, -25, -25, -35, -35, 790, -100, 800, 850, 900, 920, 940, 1030, 1040, 1050, -36, -26, -26, -26, -26, -26, -26, -26, -26, -26, -35, -25, -25, -35, 810, -25, -35, -35, -25, 820, -25, -35, -25, -25, -35, -35, -35, -35, -35, -25, -25, -35, 830, -35, 840, -35, -25, -35, -35, -25, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -100, -25, -25, -25, -100, -100, -100, -100, -100, -100, -25, -25, -35, -35, -35, -35, 860, -35, 870, 880, -25, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -45, -45, -35, -100, -100, -100, -100, -100, -100, 890, -100, -100, -100, -25, -45, -45, -25, -45, -45, -25, -45, -45, -45, -25, -25, -25, -25, -25, -35, -35, 910, -35, -25, -35, -35, -35, -35, -35, -35, -35, -45, -35, -35, -100, 930, -35, -35, -35, -35, -35, -35, -35, -35, -100, -100, -45, -100, -45, -100, -100, -100, -100, -100, -25, -25, -25, 950, -25, 970, 990, -35, 1000, 1010, -35, -35, -35, -35, -35, -35, 960, -35, -35, -35, -45, -45, -45, -45, -45, -45, -35, -45, -45, -45, -35, -35, -25, -35, -35, 980, -35, -35, -35, -35, -100, -100, -25, -25, -100, -100, -100, -100, -100, -100, -25, -35, -35, -35, -35, -35, -35, -35, -35, -35, -25, -35, -35, -35, -35, -35, -35, -35, -35, -25, -25, -35, -35, -35, -25, -25, -35, -35, -35, 1020, -45, -45, -35, -35, -45, -45, -45, -45, -45, -45, -25, -25, -25, -25, -25, -35, -25, -35, -25, -35, -35, -25, -25, -35, -35, -35, -25, -35, -25, -35, -25, -25, -35, -35, -35, -35, -35, -35, -35, -25, -26, -100, 1070, 1080, 1090, 1110, 1120, 1130, 1140, 1160, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -35, -25, -25, -25, -25, -25, -25, -25, -25, -25, -35, -100, -35, -35, -35, -100, -35, -35, -35, 1100, -35, -35, -35, -35, -35, -35, -45, -35, -35, -35, -35, -25, -35, -25, -35, -35, -35, -35, -25, -35, -25, -25, -25, -25, -35, -35, -35, -35, -35, -35, -25, -25, -35, -35, -35, -25, -25, -35, -35, -35, 1150, -25, -35, -35, -35, -35, -35, -35, -25, -25, -35, -35, -45, -35, -35, -35, -35, -35, -35, -35, -35, 1170, -25, -35, 1180, -35, 1190, -35, -25, -25, -100, -100, -45, -45, -100, -100, -100, -100, -100, -100, -25, -35, -35, -35, -35, -35, -35, -25, -25, -35, -35, -35, -35, -35, -35, -35, -35, -35, -35, -45 };
    }
    
    public static void format(final Editable editable) {
        int n = 1;
        final int length = editable.length();
        if (length > 3 && editable.subSequence(0, 3).toString().equals("+81")) {
            n = 3;
        }
        else if (length < 1 || editable.charAt(0) != '0') {
            return;
        }
        final CharSequence subSequence = editable.subSequence(0, length);
        int i = 0;
        while (i < editable.length()) {
            if (editable.charAt(i) == '-') {
                editable.delete(i, i + 1);
            }
            else {
                ++i;
            }
        }
        final int length2 = editable.length();
        int j = n;
        int n2 = 0;
        while (j < length2) {
            final char char1 = editable.charAt(j);
            if (!Character.isDigit(char1)) {
                editable.replace(0, length2, subSequence);
                return;
            }
            n2 = JapanesePhoneNumberFormatter.FORMAT_MAP[n2 + char1 - '0'];
            if (n2 < '\0') {
                if (n2 <= -100) {
                    editable.replace(0, length2, subSequence);
                    return;
                }
                final int n3 = n + Math.abs(n2) % 10;
                if (length2 > n3) {
                    editable.insert(n3, "-");
                }
                final int n4 = n + Math.abs(n2) / 10;
                if (length2 > n4) {
                    editable.insert(n4, "-");
                    break;
                }
                break;
            }
            else {
                ++j;
            }
        }
        if (length2 > 3 && n == 3) {
            editable.insert(n, "-");
        }
    }
}
