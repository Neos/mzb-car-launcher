package a_vcard.com.android.internal.util;

import java.lang.reflect.*;

public class ArrayUtils
{
    private static final int CACHE_SIZE = 73;
    private static Object[] EMPTY;
    private static Object[] sCache;
    
    static {
        ArrayUtils.EMPTY = new Object[0];
        ArrayUtils.sCache = new Object[73];
    }
    
    public static <T> boolean contains(final T[] array, final T t) {
        for (int length = array.length, i = 0; i < length; ++i) {
            final T t2 = array[i];
            if (t2 == null) {
                if (t == null) {
                    return true;
                }
            }
            else if (t != null && t2.equals(t)) {
                return true;
            }
        }
        return false;
    }
    
    public static <T> T[] emptyArray(final Class<T> clazz) {
        if (clazz == Object.class) {
            return (T[])ArrayUtils.EMPTY;
        }
        final int n = (System.identityHashCode(clazz) / 8 & Integer.MAX_VALUE) % 73;
        final Object o = ArrayUtils.sCache[n];
        if (o != null) {
            final Object instance = o;
            if (((T[])o).getClass().getComponentType() == clazz) {
                return (T[])instance;
            }
        }
        final Object instance = Array.newInstance(clazz, 0);
        ArrayUtils.sCache[n] = instance;
        return (T[])instance;
    }
    
    public static boolean equals(final byte[] array, final byte[] array2, final int n) {
        if (array != array2) {
            if (array == null || array2 == null || array.length < n || array2.length < n) {
                return false;
            }
            for (int i = 0; i < n; ++i) {
                if (array[i] != array2[i]) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static int idealBooleanArraySize(final int n) {
        return idealByteArraySize(n);
    }
    
    public static int idealByteArraySize(final int n) {
        int n2 = 4;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= 32) {
                break;
            }
            if (n <= (1 << n2) - 12) {
                n3 = (1 << n2) - 12;
                break;
            }
            ++n2;
        }
        return n3;
    }
    
    public static int idealCharArraySize(final int n) {
        return idealByteArraySize(n * 2) / 2;
    }
    
    public static int idealFloatArraySize(final int n) {
        return idealByteArraySize(n * 4) / 4;
    }
    
    public static int idealIntArraySize(final int n) {
        return idealByteArraySize(n * 4) / 4;
    }
    
    public static int idealLongArraySize(final int n) {
        return idealByteArraySize(n * 8) / 8;
    }
    
    public static int idealObjectArraySize(final int n) {
        return idealByteArraySize(n * 4) / 4;
    }
    
    public static int idealShortArraySize(final int n) {
        return idealByteArraySize(n * 2) / 2;
    }
}
