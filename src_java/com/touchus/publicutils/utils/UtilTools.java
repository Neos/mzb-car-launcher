package com.touchus.publicutils.utils;

import android.os.*;
import android.content.*;
import java.net.*;
import java.io.*;
import android.telephony.*;
import android.text.*;
import android.provider.*;
import android.util.*;
import java.util.*;
import android.net.*;
import android.app.*;
import java.util.regex.*;

public class UtilTools
{
    private static long lastClickTime;
    
    private static long calculateTotalSizeInMB(final StatFs statFs) {
        if (statFs != null) {
            return statFs.getBlockSize() * statFs.getBlockCount();
        }
        return 0L;
    }
    
    public static boolean checkTFCarkExist() {
        return calculateTotalSizeInMB(getStatFs("storage/sdcard1")) > 0L;
    }
    
    public static boolean checkURL(final Context context, final String s) {
        boolean b = false;
        if (!isNetworkConnected(context)) {
            return false;
        }
        while (true) {
            try {
                final int responseCode = ((HttpURLConnection)new URL(s).openConnection()).getResponseCode();
                System.out.println(">>>>>>>>>>>>>>>> " + responseCode + " <<<<<<<<<<<<<<<<<<");
                b = (responseCode == 200);
                return b;
            }
            catch (MalformedURLException ex) {
                ex.printStackTrace();
                return b;
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
                return b;
            }
            return b;
        }
    }
    
    public static boolean checkUSBExist() {
        return calculateTotalSizeInMB(getStatFs("/mnt/usbotg")) > 0L;
    }
    
    public static boolean echoFile(final String p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: new             Ljava/lang/StringBuilder;
        //     5: dup            
        //     6: ldc             "echoFile: "
        //     8: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    11: aload_0        
        //    12: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    15: ldc             " path: "
        //    17: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    20: aload_1        
        //    21: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    24: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    27: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //    30: pop            
        //    31: aconst_null    
        //    32: astore          4
        //    34: aconst_null    
        //    35: astore_3       
        //    36: aload           4
        //    38: astore_2       
        //    39: new             Ljava/io/File;
        //    42: dup            
        //    43: aload_1        
        //    44: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    47: astore_1       
        //    48: aload           4
        //    50: astore_2       
        //    51: aload_1        
        //    52: invokevirtual   java/io/File.exists:()Z
        //    55: ifeq            178
        //    58: aload           4
        //    60: astore_2       
        //    61: aload_1        
        //    62: invokevirtual   java/io/File.canWrite:()Z
        //    65: ifeq            178
        //    68: aload           4
        //    70: astore_2       
        //    71: new             Ljava/io/FileOutputStream;
        //    74: dup            
        //    75: aload_1        
        //    76: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //    79: astore_1       
        //    80: aload_1        
        //    81: aload_0        
        //    82: invokevirtual   java/lang/String.getBytes:()[B
        //    85: invokevirtual   java/io/FileOutputStream.write:([B)V
        //    88: aload_1        
        //    89: invokevirtual   java/io/FileOutputStream.flush:()V
        //    92: aload_1        
        //    93: ifnull          100
        //    96: aload_1        
        //    97: invokevirtual   java/io/FileOutputStream.close:()V
        //   100: iconst_1       
        //   101: ireturn        
        //   102: astore_0       
        //   103: ldc             "driverlog"
        //   105: aload_0        
        //   106: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   109: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   112: pop            
        //   113: goto            100
        //   116: astore_0       
        //   117: aload_3        
        //   118: astore_0       
        //   119: aload_0        
        //   120: astore_2       
        //   121: ldc             "driverlog"
        //   123: ldc             "echofile err----------"
        //   125: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   128: pop            
        //   129: aload_0        
        //   130: ifnull          137
        //   133: aload_0        
        //   134: invokevirtual   java/io/FileOutputStream.close:()V
        //   137: iconst_0       
        //   138: ireturn        
        //   139: astore_0       
        //   140: ldc             "driverlog"
        //   142: aload_0        
        //   143: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   146: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   149: pop            
        //   150: goto            137
        //   153: astore_0       
        //   154: aload_2        
        //   155: ifnull          162
        //   158: aload_2        
        //   159: invokevirtual   java/io/FileOutputStream.close:()V
        //   162: aload_0        
        //   163: athrow         
        //   164: astore_1       
        //   165: ldc             "driverlog"
        //   167: aload_1        
        //   168: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   171: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   174: pop            
        //   175: goto            162
        //   178: iconst_0       
        //   179: ifeq            137
        //   182: new             Ljava/lang/NullPointerException;
        //   185: dup            
        //   186: invokespecial   java/lang/NullPointerException.<init>:()V
        //   189: athrow         
        //   190: astore_0       
        //   191: ldc             "driverlog"
        //   193: aload_0        
        //   194: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   197: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   200: pop            
        //   201: goto            137
        //   204: astore_0       
        //   205: aload_1        
        //   206: astore_2       
        //   207: goto            154
        //   210: astore_0       
        //   211: aload_1        
        //   212: astore_0       
        //   213: goto            119
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  39     48     116    119    Ljava/lang/Exception;
        //  39     48     153    154    Any
        //  51     58     116    119    Ljava/lang/Exception;
        //  51     58     153    154    Any
        //  61     68     116    119    Ljava/lang/Exception;
        //  61     68     153    154    Any
        //  71     80     116    119    Ljava/lang/Exception;
        //  71     80     153    154    Any
        //  80     92     210    216    Ljava/lang/Exception;
        //  80     92     204    210    Any
        //  96     100    102    116    Ljava/lang/Exception;
        //  121    129    153    154    Any
        //  133    137    139    153    Ljava/lang/Exception;
        //  158    162    164    178    Ljava/lang/Exception;
        //  182    190    190    204    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0100:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String getIMEI(final Context context) {
        String deviceId;
        if (TextUtils.isEmpty((CharSequence)(deviceId = ((TelephonyManager)context.getSystemService("phone")).getDeviceId()))) {
            deviceId = "";
        }
        return deviceId;
    }
    
    public static UUID getMyUUID(final Context context) {
        final TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService("phone");
        final UUID uuid = new UUID(new StringBuilder().append(Settings$Secure.getString(context.getContentResolver(), "android_id")).toString().hashCode(), new StringBuilder().append(telephonyManager.getDeviceId()).toString().hashCode() << 32 | new StringBuilder().append(telephonyManager.getSimSerialNumber()).toString().hashCode());
        Log.d("debug", "uuid=" + uuid.toString());
        return uuid;
    }
    
    public static String getPackage(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }
    
    private static StatFs getStatFs(final String s) {
        try {
            return new StatFs(s);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static String getVersion(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }
    
    public static boolean isFastDoubleClick() {
        final long currentTimeMillis = System.currentTimeMillis();
        final long n = currentTimeMillis - UtilTools.lastClickTime;
        if (0L < n && n < 500L) {
            UtilTools.lastClickTime = currentTimeMillis;
            return true;
        }
        UtilTools.lastClickTime = currentTimeMillis;
        return false;
    }
    
    public static boolean isForeground(final Context context, final String s) {
        if (context == null || TextUtils.isEmpty((CharSequence)s)) {
            return false;
        }
        final List runningTasks = ((ActivityManager)context.getSystemService("activity")).getRunningTasks(1);
        return runningTasks != null && runningTasks.size() > 0 && s.equals(runningTasks.get(0).topActivity.getClassName());
    }
    
    public static boolean isLetterDigitOrChinese(final String s) {
        return s.matches("^[a-z0-9A-Z.]+$");
    }
    
    public static boolean isNetworkConnected(final Context context) {
        if (context != null) {
            final NetworkInfo activeNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isAvailable();
            }
        }
        return false;
    }
    
    public static void sendKeyeventToSystem(final int n) {
        if (n < 0) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Instrumentation().sendKeyDownUpSync(n);
            }
        }).start();
    }
    
    public static boolean stringFilter(final String s) {
        Block_0: {
            break Block_0;
        Label_0049:
            while (true) {
                int i;
                do {
                    Label_0017: {
                        break Label_0017;
                        try {
                            final Pattern compile = Pattern.compile("^[\u4e00-\u9fa5A-Za-z0-9_]+$");
                            i = s.length() - 1;
                            continue Label_0049;
                            Label_0039: {
                                --i;
                            }
                            continue Label_0049;
                            // iftrue(Label_0039:, compile.matcher((CharSequence)String.valueOf(s.charAt(i))).matches())
                            return false;
                        }
                        catch (Exception ex) {
                            return false;
                        }
                    }
                    continue Label_0049;
                } while (i >= 0);
                break;
            }
        }
        return true;
    }
}
