package com.touchus.publicutils.utils;

import java.io.*;
import java.text.*;
import java.util.*;
import android.os.*;

public class TimeUtils
{
    static SimpleDateFormat simpleCallDateFormat;
    static SimpleDateFormat simpleDateFormat;
    
    static {
        TimeUtils.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeUtils.simpleCallDateFormat = new SimpleDateFormat("MM/dd HH:mm");
    }
    
    public static Process createSuProcess() throws IOException {
        final File file = new File("/system/xbin/ru");
        if (file.exists()) {
            return Runtime.getRuntime().exec(file.getAbsolutePath());
        }
        return Runtime.getRuntime().exec("su");
    }
    
    public static Process createSuProcess(final String p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: invokestatic    com/touchus/publicutils/utils/TimeUtils.createSuProcess:()Ljava/lang/Process;
        //     5: astore_3       
        //     6: new             Ljava/io/DataOutputStream;
        //     9: dup            
        //    10: aload_3        
        //    11: invokevirtual   java/lang/Process.getOutputStream:()Ljava/io/OutputStream;
        //    14: invokespecial   java/io/DataOutputStream.<init>:(Ljava/io/OutputStream;)V
        //    17: astore_2       
        //    18: aload_2        
        //    19: new             Ljava/lang/StringBuilder;
        //    22: dup            
        //    23: aload_0        
        //    24: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    27: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    30: ldc             "\n"
        //    32: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    35: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    38: invokevirtual   java/io/DataOutputStream.writeBytes:(Ljava/lang/String;)V
        //    41: aload_2        
        //    42: ldc             "exit $?\n"
        //    44: invokevirtual   java/io/DataOutputStream.writeBytes:(Ljava/lang/String;)V
        //    47: aload_2        
        //    48: ifnull          55
        //    51: aload_2        
        //    52: invokevirtual   java/io/DataOutputStream.close:()V
        //    55: aload_3        
        //    56: areturn        
        //    57: astore_0       
        //    58: aload_1        
        //    59: ifnull          66
        //    62: aload_1        
        //    63: invokevirtual   java/io/DataOutputStream.close:()V
        //    66: aload_0        
        //    67: athrow         
        //    68: astore_1       
        //    69: goto            66
        //    72: astore_0       
        //    73: aload_3        
        //    74: areturn        
        //    75: astore_0       
        //    76: aload_2        
        //    77: astore_1       
        //    78: goto            58
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      18     57     58     Any
        //  18     47     75     81     Any
        //  51     55     72     75     Ljava/io/IOException;
        //  62     66     68     72     Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0055:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int dateTimeCompara(final String s, final String s2) throws ParseException {
        final Date parse = TimeUtils.simpleDateFormat.parse(s);
        final Date parse2 = TimeUtils.simpleDateFormat.parse(s2);
        if (parse.getTime() > parse2.getTime()) {
            return 1;
        }
        if (parse.getTime() < parse2.getTime()) {
            return -1;
        }
        return 0;
    }
    
    public static long getIntervalTime(final Date date, final Date date2) {
        try {
            return date2.getTime() - date.getTime();
        }
        catch (Exception ex) {
            return 0L;
        }
    }
    
    public static String getSimpleCallDate() {
        return TimeUtils.simpleCallDateFormat.format(new Date());
    }
    
    public static String getSimpleDate() {
        return getSimpleDateFormat().format(new Date());
    }
    
    public static String getSimpleDate(final String s) {
        return new SimpleDateFormat(s).format(new Date());
    }
    
    public static String getSimpleDate(final Date date) {
        return getSimpleDateFormat().format(date);
    }
    
    public static SimpleDateFormat getSimpleDateFormat() {
        return TimeUtils.simpleDateFormat;
    }
    
    public static SimpleDateFormat getSimpleDateFormat(final String s) {
        return new SimpleDateFormat(s);
    }
    
    public static String intToString(int n) {
        String string2;
        String string = string2 = "";
        if (n > 0) {
            final int n2 = n / 3600;
            final int n3 = (n - n2 * 3600) / 60;
            n %= 60;
            if (n2 > 0) {
                string = String.valueOf(n2) + ":";
            }
            String s;
            if (n3 > 0 || n2 > 0) {
                if (n3 < 10) {
                    if (n3 == 0) {
                        s = String.valueOf(string) + "00:";
                    }
                    else {
                        s = String.valueOf(string) + "0" + n3 + ":";
                    }
                }
                else {
                    s = String.valueOf(string) + n2 + ":";
                }
            }
            else {
                s = "00:";
            }
            if (n <= 0) {
                string2 = s;
                if (n3 <= 0) {
                    return string2;
                }
            }
            if (n >= 10) {
                return String.valueOf(s) + n;
            }
            if (n != 0) {
                return String.valueOf(s) + "0" + n;
            }
            string2 = String.valueOf(s) + "00";
        }
        return string2;
    }
    
    public static void requestPermission() throws InterruptedException, IOException {
        createSuProcess("chmod 666 /dev/alarm").waitFor();
    }
    
    public static String secToTimeString(final long n) {
        final long n2 = n / 3600L;
        if (n2 != 0L) {
            String s;
            if (n2 < 10L) {
                s = "0" + n2;
            }
            else {
                s = new StringBuilder().append(n2).toString();
            }
            return String.valueOf(s) + " : " + secToTimeString(n % 3600L);
        }
        final long n3 = n / 60L;
        String s2;
        if (n3 < 10L) {
            s2 = "0" + n3;
        }
        else {
            s2 = new StringBuilder().append(n3).toString();
        }
        if (n % 60L < 10L) {
            return String.valueOf(s2) + " : 0" + n % 60L;
        }
        return String.valueOf(s2) + " : " + n % 60L;
    }
    
    public static void setTime(final int n, final int n2) throws IOException, InterruptedException {
        requestPermission();
        final Calendar instance = Calendar.getInstance();
        instance.set(11, n);
        instance.set(12, n2);
        final long timeInMillis = instance.getTimeInMillis();
        if (timeInMillis / 1000L < 2147483647L) {
            SystemClock.setCurrentTimeMillis(timeInMillis);
        }
        if (Calendar.getInstance().getTimeInMillis() - timeInMillis > 1000L) {
            throw new IOException("failed to set Time.");
        }
    }
}
