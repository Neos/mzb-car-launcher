package com.touchus.publicutils.utils;

import java.text.*;
import java.util.*;
import java.io.*;
import android.util.*;
import android.content.*;
import com.touchus.publicutils.sysconst.*;
import android.os.*;

public class CrashHandler implements UncaughtExceptionHandler
{
    private static CrashHandler INSTANCE;
    public static final String TAG = "CrashHandler";
    private DateFormat formatter;
    private Map<String, String> infos;
    private String mAddress;
    private Context mContext;
    private UncaughtExceptionHandler mDefaultHandler;
    
    public CrashHandler(final Context mContext) {
        this.infos = new HashMap<String, String>();
        this.formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        this.mContext = mContext;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)this);
    }
    
    private String getCrashInfo(Throwable t) {
        final StringBuffer sb = new StringBuffer();
        sb.append("DATA=" + this.formatter.format(new Date()) + "\n");
        for (final Map.Entry<String, String> entry : this.infos.entrySet()) {
            sb.append(String.valueOf(entry.getKey()) + "=" + entry.getValue() + "\n");
        }
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        t.printStackTrace(printWriter);
        for (t = t.getCause(); t != null; t = t.getCause()) {
            t.printStackTrace(printWriter);
        }
        printWriter.close();
        sb.append(stringWriter.toString());
        return sb.toString();
    }
    
    public static CrashHandler getInstance(final Context context) {
        if (CrashHandler.INSTANCE == null) {
            CrashHandler.INSTANCE = new CrashHandler(context);
        }
        return CrashHandler.INSTANCE;
    }
    
    private boolean handleException(final Throwable t) {
        if (t == null) {
            return false;
        }
        this.collectDeviceInfo(this.mContext);
        final String saveCrashInfo2File = this.saveCrashInfo2File(this.getCrashInfo(t));
        if ("c200".equalsIgnoreCase(Build.MODEL) || "e200_13".equalsIgnoreCase(Build.MODEL) || "gla".equalsIgnoreCase(Build.MODEL)) {
            MailManager.getInstance(this.mContext).sendMailWithFile(String.valueOf(BenzModel.benzName()) + "\u5954\u6e83\u65e5\u5fd7", new StringBuilder().append(TimeUtils.getSimpleDate()).toString(), saveCrashInfo2File);
        }
        else {
            MailManager.getInstance(this.mContext).sendMailWithFile(String.valueOf(Build.MODEL) + "\u5954\u6e83\u65e5\u5fd7", new StringBuilder().append(TimeUtils.getSimpleDate()).toString(), saveCrashInfo2File);
        }
        return true;
    }
    
    private String saveCrashInfo2File(String string) {
        try {
            final String string2 = "crash-" + this.formatter.format(new Date()) + "-" + System.currentTimeMillis() + ".log";
            final String string3 = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/unibroad/crash/";
            if (Environment.getExternalStorageState().equals("mounted")) {
                final File file = new File(string3);
                if (!file.exists()) {
                    file.mkdirs();
                }
                final FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(string3) + string2);
                fileOutputStream.write(string.toString().getBytes());
                fileOutputStream.close();
            }
            string = String.valueOf(string3) + string2;
            return string;
        }
        catch (Exception ex) {
            Log.e("CrashHandler", "an error occured while writing file...", (Throwable)ex);
            return null;
        }
    }
    
    public void collectDeviceInfo(final Context p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //     4: aload_1        
        //     5: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //     8: iconst_1       
        //     9: invokevirtual   android/content/pm/PackageManager.getPackageInfo:(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
        //    12: astore          4
        //    14: aload           4
        //    16: ifnull          117
        //    19: aload           4
        //    21: getfield        android/content/pm/PackageInfo.versionName:Ljava/lang/String;
        //    24: ifnonnull       134
        //    27: ldc_w           "null"
        //    30: astore_1       
        //    31: new             Ljava/lang/StringBuilder;
        //    34: dup            
        //    35: aload           4
        //    37: getfield        android/content/pm/PackageInfo.versionCode:I
        //    40: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //    43: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    46: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    49: astore          4
        //    51: aload_0        
        //    52: getfield        com/touchus/publicutils/utils/CrashHandler.infos:Ljava/util/Map;
        //    55: ldc_w           "versionName"
        //    58: aload_1        
        //    59: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    64: pop            
        //    65: aload_0        
        //    66: getfield        com/touchus/publicutils/utils/CrashHandler.infos:Ljava/util/Map;
        //    69: ldc_w           "versionCode"
        //    72: aload           4
        //    74: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    79: pop            
        //    80: aload_0        
        //    81: getfield        com/touchus/publicutils/utils/CrashHandler.infos:Ljava/util/Map;
        //    84: ldc_w           "IMEI"
        //    87: aload_0        
        //    88: getfield        com/touchus/publicutils/utils/CrashHandler.mContext:Landroid/content/Context;
        //    91: invokestatic    com/touchus/publicutils/utils/UtilTools.getIMEI:(Landroid/content/Context;)Ljava/lang/String;
        //    94: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    99: pop            
        //   100: aload_0        
        //   101: getfield        com/touchus/publicutils/utils/CrashHandler.infos:Ljava/util/Map;
        //   104: ldc_w           "address"
        //   107: aload_0        
        //   108: getfield        com/touchus/publicutils/utils/CrashHandler.mAddress:Ljava/lang/String;
        //   111: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   116: pop            
        //   117: ldc             Landroid/os/Build;.class
        //   119: invokevirtual   java/lang/Class.getDeclaredFields:()[Ljava/lang/reflect/Field;
        //   122: astore_1       
        //   123: aload_1        
        //   124: arraylength    
        //   125: istore_3       
        //   126: iconst_0       
        //   127: istore_2       
        //   128: iload_2        
        //   129: iload_3        
        //   130: if_icmplt       157
        //   133: return         
        //   134: aload           4
        //   136: getfield        android/content/pm/PackageInfo.versionName:Ljava/lang/String;
        //   139: astore_1       
        //   140: goto            31
        //   143: astore_1       
        //   144: ldc             "CrashHandler"
        //   146: ldc_w           "an error occured when collect package info"
        //   149: aload_1        
        //   150: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   153: pop            
        //   154: goto            117
        //   157: aload_1        
        //   158: iload_2        
        //   159: aaload         
        //   160: astore          4
        //   162: aload           4
        //   164: iconst_1       
        //   165: invokevirtual   java/lang/reflect/Field.setAccessible:(Z)V
        //   168: aload_0        
        //   169: getfield        com/touchus/publicutils/utils/CrashHandler.infos:Ljava/util/Map;
        //   172: aload           4
        //   174: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   177: aload           4
        //   179: aconst_null    
        //   180: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   183: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   186: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   191: pop            
        //   192: ldc             "CrashHandler"
        //   194: new             Ljava/lang/StringBuilder;
        //   197: dup            
        //   198: aload           4
        //   200: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   203: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   206: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   209: ldc_w           " : "
        //   212: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   215: aload           4
        //   217: aconst_null    
        //   218: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   221: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   224: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   227: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   230: pop            
        //   231: iload_2        
        //   232: iconst_1       
        //   233: iadd           
        //   234: istore_2       
        //   235: goto            128
        //   238: astore          4
        //   240: ldc             "CrashHandler"
        //   242: ldc_w           "an error occured when collect crash info"
        //   245: aload           4
        //   247: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   250: pop            
        //   251: goto            231
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                     
        //  -----  -----  -----  -----  ---------------------------------------------------------
        //  0      14     143    157    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  19     27     143    157    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  31     117    143    157    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  134    140    143    157    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  162    231    238    254    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setAddress(final String mAddress) {
        this.mAddress = mAddress;
    }
    
    @Override
    public void uncaughtException(final Thread thread, final Throwable t) {
        this.mContext.sendBroadcast(new Intent("SHOW_NAVIGATION_BAR"));
        final boolean handleException = this.handleException(t);
        if (PubSysConst.DEBUG) {
            this.mDefaultHandler.uncaughtException(thread, t);
            return;
        }
        if (!handleException && this.mDefaultHandler != null) {
            this.mDefaultHandler.uncaughtException(thread, t);
            return;
        }
        while (true) {
            try {
                Thread.sleep(2000L);
                Process.killProcess(Process.myPid());
                System.exit(1);
            }
            catch (InterruptedException ex) {
                Log.e("CrashHandler", "error : ", (Throwable)ex);
                continue;
            }
            break;
        }
    }
}
