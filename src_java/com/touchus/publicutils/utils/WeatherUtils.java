package com.touchus.publicutils.utils;

import android.content.*;
import android.text.*;
import java.util.*;
import com.touchus.publicutils.*;

public class WeatherUtils
{
    public static int getResId(final Context context, String s) {
        String s2 = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            s2 = "weather_yin";
        }
        final Calendar instance = Calendar.getInstance();
        Label_0073: {
            if (instance.get(11) <= 6 || instance.get(11) >= 18) {
                break Label_0073;
            }
            int n = 1;
            Block_15_Outer:Block_20_Outer:
            while (true) {
                Label_0084: {
                    if (!s2.equals("\u6674")) {
                        break Label_0084;
                    }
                    Label_0078: {
                        if (n == 0) {
                            break Label_0078;
                        }
                        s = "weather_qing_baitian";
                        try {
                            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                            Label_0333: {
                                s = "weather_zhenxue_yejian";
                            }
                            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                            Label_0202:
                            // iftrue(Label_0227:, !s2.contains((CharSequence)"\u9635\u96e8"))
                            // iftrue(Label_0124:, !s2.equals((Object)"\u9634"))
                            // iftrue(Label_0266:, !s2.contains((CharSequence)"\u96e8\u5939\u96ea"))
                            // iftrue(Label_0187:, !s2.contains((CharSequence)"\u5c0f\u96e8") && !s2.contains((CharSequence)"\u4e2d\u96e8"))
                            // iftrue(Label_0139:, !s2.equals((Object)"\u96fe"))
                            // iftrue(Label_0314:, !s2.contains((CharSequence)"\u5c0f\u96ea") && !s2.contains((CharSequence)"\u4e2d\u96ea"))
                            // iftrue(Label_0387:, !s2.contains((CharSequence)"\u6c99\u5c18\u66b4"))
                            // iftrue(Label_0251:, !s2.contains((CharSequence)"\u51bb\u96e8") && !s2.contains((CharSequence)"\u51b0\u96f9"))
                            // iftrue(Label_0290:, !s2.contains((CharSequence)"\u5927\u96ea") && !s2.contains((CharSequence)"\u66b4\u96ea"))
                            // iftrue(Label_0372:, !s2.contains((CharSequence)"\u6d6e\u5c18") && !s2.contains((CharSequence)"\u626c\u6c99") && !s2.contains((CharSequence)"\u973e"))
                            // iftrue(Label_0202:, !s2.contains((CharSequence)"\u96f7\u9635\u96e8"))
                            // iftrue(Label_0333:, n == 0)
                            // iftrue(Label_0163:, !s2.contains((CharSequence)"\u5927\u96e8") && !s2.contains((CharSequence)"\u66b4\u96e8"))
                            // iftrue(Label_0109:, !s2.contains((CharSequence)"\u591a\u4e91"))
                            // iftrue(Label_0103:, n == 0)
                            // iftrue(Label_0221:, n == 0)
                            while (true) {
                                while (true) {
                                    Block_14: {
                                        break Block_14;
                                        Label_0109:
                                        Block_13_Outer:Block_21_Outer:Label_0157_Outer:
                                        while (true) {
                                            Label_0308: {
                                                while (true) {
                                                    Label_0366: {
                                                        while (true) {
                                                            Block_10: {
                                                                while (true) {
                                                                    Block_9: {
                                                                        break Block_9;
                                                                        Label_0251:
                                                                        s = "weather_yujiaxue";
                                                                        return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                        Label_0163:
                                                                    Block_24:
                                                                        while (true) {
                                                                            while (true) {
                                                                                Label_0181: {
                                                                                    break Label_0181;
                                                                                    Label_0124:
                                                                                    break Block_10;
                                                                                    s = "weather_daxuebaoxue";
                                                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                                    s = "weather_zhenyu_baitian";
                                                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                                    s = "weather_duoyun_baitian";
                                                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                                    s = "weather_dongyu";
                                                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                                }
                                                                                s = "weather_xiaoyuzhongyu";
                                                                                return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                                s = "weather_leizhenyu";
                                                                                return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                                Label_0290:
                                                                                break Label_0308;
                                                                                Label_0372:
                                                                                break Block_24;
                                                                                Label_0227:
                                                                                continue Block_13_Outer;
                                                                            }
                                                                            Label_0266:
                                                                            continue Block_15_Outer;
                                                                        }
                                                                        s = "weather_shachenbao";
                                                                        return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                        Label_0339:
                                                                        break Label_0366;
                                                                        s = "weather_zhenxue_baitian";
                                                                        return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                    }
                                                                    s = "weather_yin";
                                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                    s = "weather_qing_yejian";
                                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                                    Label_0187:
                                                                    continue Block_21_Outer;
                                                                }
                                                            }
                                                            s = "weather_wu";
                                                            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                            s = "weather_dayubaoyu";
                                                            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                            Label_0221:
                                                            s = "weather_zhenyu_yejian";
                                                            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                            n = 0;
                                                            continue Block_15_Outer;
                                                            continue Label_0157_Outer;
                                                        }
                                                    }
                                                    s = "weather_mai";
                                                    return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                                    Label_0139:
                                                    continue Block_20_Outer;
                                                }
                                                Label_0387:
                                                s = "weather_yin";
                                                return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                            }
                                            s = "weather_xiaoxuezhongxue";
                                            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                            Block_7: {
                                                break Block_7;
                                                Label_0103:
                                                s = "weather_duoyun_yejian";
                                                return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
                                            }
                                            continue Block_20_Outer;
                                        }
                                    }
                                    continue Block_20_Outer;
                                }
                                Label_0314:
                                continue;
                            }
                        }
                        // iftrue(Label_0339:, !s2.contains((CharSequence)"\u9635\u96ea"))
                        catch (Exception ex) {
                            ex.printStackTrace();
                            return R.drawable.weather_yin;
                        }
                    }
                }
                break;
            }
        }
    }
}
