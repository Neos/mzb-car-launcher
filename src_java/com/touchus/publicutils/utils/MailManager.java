package com.touchus.publicutils.utils;

import java.io.*;
import javax.mail.internet.*;
import android.os.*;
import javax.mail.*;
import android.content.*;
import java.util.*;

public class MailManager
{
    private static final String KEY_MAIL_AUTH = "mail.smtp.auth";
    private static final String KEY_MAIL_HOST = "mail.smtp.host";
    private static final String SENDER_NAME = "bug@unibroad.com";
    private static final String SENDER_PASS = "BUGzero0";
    private static final String VALUE_MAIL_AUTH = "true";
    private static final String VALUE_MAIL_HOST = "smtp.exmail.qq.com";
    private static Context mContext;
    
    private void appendFile(final MimeMessage mimeMessage, final String s) {
        try {
            final Multipart multipart = (Multipart)mimeMessage.getContent();
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.attachFile(s);
            multipart.addBodyPart(mimeBodyPart);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (MessagingException ex2) {
            ex2.printStackTrace();
        }
    }
    
    private void appendMultiFile(final MimeMessage mimeMessage, final List<String> list) {
        try {
            final Multipart multipart = (Multipart)mimeMessage.getContent();
            for (final String s : list) {
                final MimeBodyPart mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.attachFile(s);
                multipart.addBodyPart(mimeBodyPart);
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (MessagingException ex2) {
            ex2.printStackTrace();
        }
    }
    
    private MimeMessage createMessage(final String subject, final String s) {
        final Properties properties = System.getProperties();
        ((Hashtable<String, String>)properties).put("mail.smtp.host", "smtp.exmail.qq.com");
        ((Hashtable<String, String>)properties).put("mail.smtp.auth", "true");
        final MimeMessage mimeMessage = new MimeMessage(Session.getInstance(properties, this.getAuthenticator()));
        try {
            mimeMessage.setFrom(new InternetAddress("bug@unibroad.com"));
            mimeMessage.setRecipients(Message.RecipientType.TO, new InternetAddress[] { new InternetAddress("bug@unibroad.com") });
            mimeMessage.setSubject(subject);
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(s, "text/html");
            mimeBodyPart.setText(s, "UTF-8");
            final MimeMultipart content = new MimeMultipart();
            content.addBodyPart(mimeBodyPart);
            mimeMessage.setContent(content);
            mimeMessage.setSentDate(new Date());
            return mimeMessage;
        }
        catch (MessagingException ex) {
            ex.printStackTrace();
            return mimeMessage;
        }
    }
    
    private Authenticator getAuthenticator() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("bug@unibroad.com", "BUGzero0");
            }
        };
    }
    
    public static MailManager getInstance(final Context mContext) {
        MailManager.mContext = mContext;
        return InstanceHolder.instance;
    }
    
    public void sendMail(final String s, final String s2) {
        new MailTask(this.createMessage(s, s2)).execute((Object[])new Void[0]);
    }
    
    public void sendMailWithFile(final String s, final String s2, final String s3) {
        final MimeMessage message = this.createMessage(s, s2);
        this.appendFile(message, s3);
        new MailTask(message).execute((Object[])new Void[0]);
    }
    
    public void sendMailWithMultiFile(final String s, final String s2, final List<String> list) {
        final MimeMessage message = this.createMessage(s, s2);
        this.appendMultiFile(message, list);
        new MailTask(message).execute((Object[])new Void[0]);
    }
    
    private static class InstanceHolder
    {
        private static MailManager instance;
        
        static {
            InstanceHolder.instance = new MailManager(null);
        }
    }
    
    class MailTask extends AsyncTask<Void, Void, Boolean>
    {
        private MimeMessage mimeMessage;
        
        public MailTask(final MimeMessage mimeMessage) {
            this.mimeMessage = mimeMessage;
        }
        
        protected Boolean doInBackground(Void... array) {
            array = (Void[])(Object)new Bundle();
            try {
                Transport.send(this.mimeMessage);
                ((Bundle)(Object)array).putBoolean("FEEDBACK_RESULT", true);
                final Intent intent = new Intent();
                intent.setAction("com.unibroad.mail");
                intent.putExtras((Bundle)(Object)array);
                MailManager.mContext.sendBroadcast(intent);
                return Boolean.TRUE;
            }
            catch (MessagingException ex) {
                ex.printStackTrace();
                ((Bundle)(Object)array).putBoolean("FEEDBACK_RESULT", false);
                final Intent intent2 = new Intent();
                intent2.setAction("com.unibroad.mail");
                intent2.putExtras((Bundle)(Object)array);
                MailManager.mContext.sendBroadcast(intent2);
                return Boolean.FALSE;
            }
        }
    }
}
