package com.touchus.publicutils.utils;

import com.touchus.publicutils.bean.*;
import java.util.*;
import java.lang.ref.*;
import android.content.*;
import android.provider.*;
import android.graphics.*;
import android.os.*;

public class LoadLocalImageUtil
{
    public static int MUSIC_TYPE;
    public static int VIDEO_TYPE;
    private static LoadLocalImageUtil instance;
    private static MediaBean mediaBean;
    private HashMap<String, SoftReference<Bitmap>> imageCache;
    
    static {
        LoadLocalImageUtil.MUSIC_TYPE = 1;
        LoadLocalImageUtil.VIDEO_TYPE = 2;
        LoadLocalImageUtil.instance = null;
    }
    
    public LoadLocalImageUtil() {
        this.imageCache = new HashMap<String, SoftReference<Bitmap>>();
    }
    
    private static Bitmap createAlbumArt(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   android/media/MediaMetadataRetriever.<init>:()V
        //     7: astore_1       
        //     8: aload_1        
        //     9: aload_0        
        //    10: invokevirtual   android/media/MediaMetadataRetriever.setDataSource:(Ljava/lang/String;)V
        //    13: aload_1        
        //    14: invokevirtual   android/media/MediaMetadataRetriever.getEmbeddedPicture:()[B
        //    17: astore_0       
        //    18: aload_0        
        //    19: iconst_0       
        //    20: aload_0        
        //    21: arraylength    
        //    22: invokestatic    android/graphics/BitmapFactory.decodeByteArray:([BII)Landroid/graphics/Bitmap;
        //    25: astore_0       
        //    26: aload_1        
        //    27: invokevirtual   android/media/MediaMetadataRetriever.release:()V
        //    30: aload_0        
        //    31: areturn        
        //    32: astore_0       
        //    33: aload_0        
        //    34: invokevirtual   java/lang/Exception.printStackTrace:()V
        //    37: aload_1        
        //    38: invokevirtual   android/media/MediaMetadataRetriever.release:()V
        //    41: aconst_null    
        //    42: areturn        
        //    43: astore_0       
        //    44: aload_0        
        //    45: invokevirtual   java/lang/Exception.printStackTrace:()V
        //    48: aconst_null    
        //    49: areturn        
        //    50: astore_0       
        //    51: aload_1        
        //    52: invokevirtual   android/media/MediaMetadataRetriever.release:()V
        //    55: aload_0        
        //    56: athrow         
        //    57: astore_1       
        //    58: aload_1        
        //    59: invokevirtual   java/lang/Exception.printStackTrace:()V
        //    62: goto            55
        //    65: astore_1       
        //    66: aload_1        
        //    67: invokevirtual   java/lang/Exception.printStackTrace:()V
        //    70: aload_0        
        //    71: areturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      26     32     50     Ljava/lang/Exception;
        //  8      26     50     65     Any
        //  26     30     65     72     Ljava/lang/Exception;
        //  33     37     50     65     Any
        //  37     41     43     50     Ljava/lang/Exception;
        //  51     55     57     65     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0055:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static LoadLocalImageUtil getInstance() {
        synchronized (LoadLocalImageUtil.class) {
            if (LoadLocalImageUtil.instance == null) {
                LoadLocalImageUtil.instance = new LoadLocalImageUtil();
            }
            return LoadLocalImageUtil.instance;
        }
    }
    
    private static Bitmap getMusicPic(final Context context) {
        final Bitmap albumArt = createAlbumArt(LoadLocalImageUtil.mediaBean.getData());
        LoadLocalImageUtil.mediaBean.setBitmap(albumArt);
        return albumArt;
    }
    
    private static Bitmap getVideoPic(final Context context) {
        final Bitmap thumbnail = MediaStore$Video$Thumbnails.getThumbnail(context.getContentResolver(), LoadLocalImageUtil.mediaBean.getId(), 1, (BitmapFactory$Options)null);
        LoadLocalImageUtil.mediaBean.setBitmap(thumbnail);
        return thumbnail;
    }
    
    public Bitmap loadDrawable(final MediaBean mediaBean, final int n, final Context context, final String s, final ImageCallback imageCallback) {
        LoadLocalImageUtil.mediaBean = mediaBean;
        if (this.imageCache.containsKey(s)) {
            final Bitmap bitmap = this.imageCache.get(s).get();
            if (bitmap != null) {
                if (n == LoadLocalImageUtil.MUSIC_TYPE) {
                    LoadLocalImageUtil.mediaBean.setBitmap(bitmap);
                    return bitmap;
                }
                LoadLocalImageUtil.mediaBean.setBitmap(bitmap);
                return bitmap;
            }
        }
        new Thread() {
            private final /* synthetic */ Handler val$handler = new Handler(this, imageCallback, s) {
                private final /* synthetic */ ImageCallback val$imageCallback;
                private final /* synthetic */ String val$tag;
                
                public void handleMessage(final Message message) {
                    this.val$imageCallback.imageLoaded((Bitmap)message.obj, this.val$tag);
                }
            };
            
            @Override
            public void run() {
                Bitmap bitmap;
                if (n == LoadLocalImageUtil.MUSIC_TYPE) {
                    bitmap = getMusicPic(context);
                }
                else {
                    bitmap = getVideoPic(context);
                }
                LoadLocalImageUtil.this.imageCache.put(s, new SoftReference<Bitmap>(bitmap));
                this.val$handler.sendMessage(this.val$handler.obtainMessage(0, (Object)bitmap));
            }
        }.start();
        return null;
    }
    
    public interface ImageCallback
    {
        void imageLoaded(final Bitmap p0, final String p1);
    }
}
