package com.touchus.publicutils.bean;

import android.graphics.*;

public class MediaBean
{
    private Bitmap bitmap;
    private String data;
    private String display_name;
    private long duration;
    private long id;
    private String mime_type;
    private long size;
    private String thumbnail_path;
    private String title;
    
    public Bitmap getBitmap() {
        return this.bitmap;
    }
    
    public String getData() {
        return this.data;
    }
    
    public String getDisplay_name() {
        return this.display_name;
    }
    
    public long getDuration() {
        return this.duration;
    }
    
    public long getId() {
        return this.id;
    }
    
    public String getMime_type() {
        return this.mime_type;
    }
    
    public long getSize() {
        return this.size;
    }
    
    public String getThumbnail_path() {
        return this.thumbnail_path;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setBitmap(final Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    
    public void setData(final String data) {
        this.data = data;
    }
    
    public void setDisplay_name(final String display_name) {
        this.display_name = display_name;
    }
    
    public void setDuration(final long duration) {
        this.duration = duration;
    }
    
    public void setId(final long id) {
        this.id = id;
    }
    
    public void setMime_type(final String mime_type) {
        this.mime_type = mime_type;
    }
    
    public void setSize(final long size) {
        this.size = size;
    }
    
    public void setThumbnail_path(final String thumbnail_path) {
        this.thumbnail_path = thumbnail_path;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MediaBean [id=");
        sb.append(this.id);
        sb.append(", display_name=");
        sb.append(this.display_name);
        sb.append(", title=");
        sb.append(this.title);
        sb.append(", size=");
        sb.append(this.size);
        sb.append(", duration=");
        sb.append(this.duration);
        sb.append(", data=");
        sb.append(this.data);
        sb.append(", mime_type=");
        sb.append(this.mime_type);
        sb.append(", bitmap=");
        sb.append(this.bitmap);
        sb.append(", thumbnail_path=");
        sb.append(this.thumbnail_path);
        sb.append("]");
        return sb.toString();
    }
}
