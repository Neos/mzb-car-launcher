package com.touchus.publicutils.sysconst;

public class PubSysConst
{
    public static String ACTION_FACTORY_BREAKSET;
    public static boolean DEBUG;
    public static String GT9XX_INT_TRIGGER;
    public static String KEY_BREAKPOS;
    
    static {
        PubSysConst.DEBUG = false;
        PubSysConst.ACTION_FACTORY_BREAKSET = "com.touchus.factorytest.breakpos";
        PubSysConst.KEY_BREAKPOS = "breakpos";
        PubSysConst.GT9XX_INT_TRIGGER = "/sys/bus/i2c/drivers/gt9xx/gt9xx_int_trigger";
    }
}
