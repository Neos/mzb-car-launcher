package com.touchus.publicutils.sysconst;

public class BenzModel
{
    public static String KEY;
    public static String SIZE_KEY;
    public static EBenzCAN benzCan;
    public static EBenzSize benzSize;
    public static EBenzTpye benzTpye;
    
    static {
        BenzModel.KEY = "benzType";
        BenzModel.SIZE_KEY = "benzSize";
        BenzModel.benzTpye = EBenzTpye.C;
        BenzModel.benzSize = EBenzSize.size480_800;
        BenzModel.benzCan = EBenzCAN.ZMYT;
    }
    
    public static String benzName() {
        if (isBenzA()) {
            return EBenzModel.BENZ_A.getName();
        }
        if (isBenzB()) {
            return EBenzModel.BENZ_B.getName();
        }
        if (isBenzC()) {
            return EBenzModel.BENZ_C.getName();
        }
        if (isBenzE()) {
            return EBenzModel.BENZ_E.getName();
        }
        if (isBenzGLA()) {
            return EBenzModel.BENZ_GLA.getName();
        }
        if (isBenzGLK()) {
            return EBenzModel.BENZ_GLK.getName();
        }
        if (isBenzGLC()) {
            return EBenzModel.BENZ_GLC.getName();
        }
        if (isBenzGLE()) {
            return EBenzModel.BENZ_GLE.getName();
        }
        if (isBenzML()) {
            return EBenzModel.BENZ_ML.getName();
        }
        return EBenzModel.BENZ_C.getName();
    }
    
    public static boolean isBenzA() {
        return BenzModel.benzTpye.equals(EBenzTpye.A);
    }
    
    public static boolean isBenzB() {
        return BenzModel.benzTpye.equals(EBenzTpye.B);
    }
    
    public static boolean isBenzC() {
        return BenzModel.benzTpye.equals(EBenzTpye.C);
    }
    
    public static boolean isBenzE() {
        return BenzModel.benzTpye.equals(EBenzTpye.E_10) || BenzModel.benzTpye.equals(EBenzTpye.E_11) || BenzModel.benzTpye.equals(EBenzTpye.E_13) || BenzModel.benzTpye.equals(EBenzTpye.E_15);
    }
    
    public static boolean isBenzGLA() {
        return BenzModel.benzTpye.equals(EBenzTpye.GLA);
    }
    
    public static boolean isBenzGLC() {
        return BenzModel.benzTpye.equals(EBenzTpye.GLC);
    }
    
    public static boolean isBenzGLE() {
        return BenzModel.benzTpye.equals(EBenzTpye.GLE);
    }
    
    public static boolean isBenzGLK() {
        return BenzModel.benzTpye.equals(EBenzTpye.GLK);
    }
    
    public static boolean isBenzML() {
        return BenzModel.benzTpye.equals(EBenzTpye.ML);
    }
    
    public static boolean isSUV() {
        return BenzModel.benzTpye == EBenzTpye.GLA || BenzModel.benzTpye == EBenzTpye.GLC || BenzModel.benzTpye == EBenzTpye.GLE || BenzModel.benzTpye == EBenzTpye.GLK;
    }
    
    public enum EBenzCAN
    {
        WCL("WCL", 3, "WCL"), 
        XBS("XBS", 2, "XBS"), 
        ZHTD("ZHTD", 1, "ZHTD"), 
        ZMYT("ZMYT", 0, "ZMYT");
        
        private String name;
        
        private EBenzCAN(final String s, final int n, final String name) {
            this.name = name;
        }
        
        public String getName() {
            return this.name;
        }
    }
    
    public enum EBenzFM
    {
        NAV_AUX("NAV_AUX", 1, 1), 
        NAV_BT("NAV_BT", 0, 0), 
        NONAV_AUX("NONAV_AUX", 3, 3), 
        NONAV_BT("NONAV_BT", 2, 2);
        
        private int name;
        
        private EBenzFM(final String s, final int n, final int name) {
            this.name = name;
        }
        
        public int getName() {
            return this.name;
        }
    }
    
    public enum EBenzModel
    {
        BENZ_A("BENZ_A", 0, "BENZ_A"), 
        BENZ_B("BENZ_B", 1, "BENZ_B"), 
        BENZ_C("BENZ_C", 2, "BENZ_C"), 
        BENZ_E("BENZ_E", 4, "BENZ_E"), 
        BENZ_GLA("BENZ_GLA", 6, "BENZ_GLA"), 
        BENZ_GLC("BENZ_GLC", 3, "BENZ_GLC"), 
        BENZ_GLE("BENZ_GLE", 5, "BENZ_GLE"), 
        BENZ_GLK("BENZ_GLK", 7, "BENZ_GLK"), 
        BENZ_ML("BENZ_ML", 8, "BENZ_ML");
        
        private String name;
        
        private EBenzModel(final String s, final int n, final String name) {
            this.name = name;
        }
        
        public String getName() {
            return this.name;
        }
    }
    
    public enum EBenzSize
    {
        size1280_480("size1280_480", 1, 1), 
        size480_800("size480_800", 0, 0);
        
        private int code;
        
        private EBenzSize(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EBenzTpye
    {
        A("A", 0, 0), 
        B("B", 1, 1), 
        C("C", 2, 2), 
        E_10("E_10", 3, 3), 
        E_11("E_11", 4, 4), 
        E_13("E_13", 5, 5), 
        E_15("E_15", 6, 6), 
        GLA("GLA", 7, 7), 
        GLC("GLC", 8, 8), 
        GLE("GLE", 9, 9), 
        GLK("GLK", 10, 10), 
        ML("ML", 11, 11);
        
        private int code;
        
        private EBenzTpye(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
}
