package com.touchus.publicutils.sysconst;

public class BMWModel
{
    public static String ACTION_BMW_ORIGINAL;
    public static String ACTION_BMW_SIZE;
    public static String ACTION_BMW_TYPE;
    public static String KEY;
    public static String ORIGINAL_KEY;
    public static String SIZE_KEY;
    
    static {
        BMWModel.ACTION_BMW_TYPE = "com.touchus.factorytest.bmwtype";
        BMWModel.ACTION_BMW_ORIGINAL = "com.touchus.factorytest.bmworiginal";
        BMWModel.ACTION_BMW_SIZE = "com.touchus.factorytest.bmwsize";
        BMWModel.KEY = "bmwType";
        BMWModel.ORIGINAL_KEY = "bmwOriginal";
        BMWModel.SIZE_KEY = "bmwSize";
    }
    
    public enum EBMWOriginal
    {
        EXIST("EXIST", 0, 0), 
        UNEXIST("UNEXIST", 1, 1);
        
        private int code;
        
        private EBMWOriginal(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EBMWTpye
    {
        Other("Other", 0, 0), 
        X1("X1", 1, 1);
        
        private int code;
        
        private EBMWTpye(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
}
