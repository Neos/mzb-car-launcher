package com.touchus.benchilauncher.adapter;

import com.touchus.benchilauncher.*;
import android.content.*;
import java.util.*;
import com.touchus.benchilauncher.bean.*;
import android.view.*;
import android.widget.*;

public class AppsetItemAdapter extends BaseAdapter
{
    private LauncherApplication app;
    private Context context;
    private List<AppInfo> listData;
    private int seclectIndex;
    
    public AppsetItemAdapter(final LauncherApplication app, final Context context, final List<AppInfo> listData) {
        this.app = app;
        this.context = context;
        this.listData = listData;
    }
    
    public int getCount() {
        return this.listData.size();
    }
    
    public Object getItem(final int n) {
        return this.listData.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        ViewHolder tag;
        if (inflate == null) {
            inflate = View.inflate(this.context, 2130903046, (ViewGroup)null);
            tag = new ViewHolder();
            tag.appIcon = (ImageView)inflate.findViewById(2131427353);
            tag.appName = (TextView)inflate.findViewById(2131427354);
            tag.appICheck = (CheckBox)inflate.findViewById(2131427355);
            inflate.setTag((Object)tag);
        }
        else {
            tag = (ViewHolder)inflate.getTag();
        }
        tag.appIcon.setImageDrawable(this.listData.get(n).getAppIcon());
        tag.appName.setText((CharSequence)this.listData.get(n).getAppName());
        tag.appICheck.setChecked(this.app.getNaviAPP().equals(this.listData.get(n).getPackageName()));
        final RelativeLayout relativeLayout = (RelativeLayout)inflate.findViewById(2131427352);
        if (this.seclectIndex == n) {
            relativeLayout.setBackgroundResource(2130837866);
            return inflate;
        }
        relativeLayout.setBackgroundColor(16777215);
        return inflate;
    }
    
    public void setSeclectIndex(final int seclectIndex) {
        this.seclectIndex = seclectIndex;
        this.notifyDataSetChanged();
    }
    
    static class ViewHolder
    {
        CheckBox appICheck;
        ImageView appIcon;
        TextView appName;
    }
}
