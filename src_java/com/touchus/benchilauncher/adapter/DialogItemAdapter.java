package com.touchus.benchilauncher.adapter;

import android.content.*;
import java.util.*;
import android.view.*;
import android.widget.*;
import android.util.*;

public class DialogItemAdapter extends BaseAdapter
{
    private Context context;
    private List<String> listData;
    private int seclectIndex;
    
    public DialogItemAdapter(final Context context, final List<String> listData) {
        this.context = context;
        this.listData = listData;
    }
    
    public int getCount() {
        return this.listData.size();
    }
    
    public Object getItem(final int n) {
        return this.listData.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        ViewHolder tag;
        if (inflate == null) {
            inflate = View.inflate(this.context, 2130903054, (ViewGroup)null);
            tag = new ViewHolder();
            tag.nameTview = (TextView)inflate.findViewById(2131427354);
            inflate.setTag((Object)tag);
        }
        else {
            tag = (ViewHolder)inflate.getTag();
        }
        tag.nameTview.setText((CharSequence)this.listData.get(n));
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131427352);
        Log.e("systemtime", " seclectIndex == position   " + (this.seclectIndex == n));
        if (this.seclectIndex == n) {
            linearLayout.setBackgroundResource(2130837866);
            Log.e("systemtime", " getView   " + System.currentTimeMillis());
            return inflate;
        }
        linearLayout.setBackgroundColor(16777215);
        return inflate;
    }
    
    public void setSeclectIndex(final int seclectIndex) {
        this.seclectIndex = seclectIndex;
    }
    
    static class ViewHolder
    {
        TextView nameTview;
    }
}
