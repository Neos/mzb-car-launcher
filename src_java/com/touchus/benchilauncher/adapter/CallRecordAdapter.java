package com.touchus.benchilauncher.adapter;

import com.touchus.benchilauncher.bean.*;
import android.content.*;
import android.view.*;
import android.widget.*;
import java.util.*;

public class CallRecordAdapter extends BaseAdapter
{
    private List<Person> callRecords;
    private Context context;
    private int selectPosition;
    private int type;
    
    public CallRecordAdapter(final Context context) {
        this(context, 0);
    }
    
    public CallRecordAdapter(final Context context, final int type) {
        this.selectPosition = 0;
        this.type = 0;
        this.callRecords = new ArrayList<Person>();
        this.context = context;
        this.type = type;
    }
    
    private void initData(final ViewHolder viewHolder, final Person person) {
        this.setCallState(viewHolder, person.getFlag());
        viewHolder.tv_yunyinshang.setText((CharSequence)person.getName());
        viewHolder.tv_phone_number.setText((CharSequence)person.getPhone());
        if (this.type == 0) {
            viewHolder.tv_call_time.setText((CharSequence)person.getRemark());
            return;
        }
        viewHolder.tv_call_time.setVisibility(8);
    }
    
    private void setCallState(final ViewHolder viewHolder, final int n) {
        switch (n) {
            default: {
                viewHolder.img_phone_state.setVisibility(4);
            }
            case 1414: {
                viewHolder.img_phone_state.setImageResource(2130838014);
            }
            case 1413: {
                viewHolder.img_phone_state.setImageResource(2130838015);
            }
            case 1412: {
                viewHolder.img_phone_state.setImageResource(2130838016);
            }
        }
    }
    
    public int getCount() {
        return this.callRecords.size();
    }
    
    public Object getItem(final int n) {
        return n;
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        ViewHolder tag;
        if (inflate == null) {
            inflate = View.inflate(this.context, 2130903083, (ViewGroup)null);
            tag = new ViewHolder((ViewHolder)null);
            tag.img_phone_state = (ImageView)inflate.findViewById(2131427575);
            tag.tv_yunyinshang = (TextView)inflate.findViewById(2131427576);
            tag.tv_phone_number = (TextView)inflate.findViewById(2131427577);
            tag.tv_call_time = (TextView)inflate.findViewById(2131427578);
            inflate.setTag((Object)tag);
        }
        else {
            tag = (ViewHolder)inflate.getTag();
        }
        if (n == this.selectPosition) {
            inflate.setBackgroundResource(2130837769);
        }
        else {
            inflate.setBackgroundColor(0);
        }
        this.initData(tag, this.callRecords.get(n));
        return inflate;
    }
    
    public void setData(final List<Person> list) {
        if (this.callRecords.size() > 0) {
            this.callRecords.clear();
        }
        this.callRecords.addAll(list);
    }
    
    public void setSelectItem(final int selectPosition) {
        this.selectPosition = selectPosition;
    }
    
    private class ViewHolder
    {
        public ImageView img_phone_state;
        public TextView tv_call_time;
        public TextView tv_phone_number;
        public TextView tv_yunyinshang;
    }
}
