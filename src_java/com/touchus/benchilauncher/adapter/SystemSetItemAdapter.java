package com.touchus.benchilauncher.adapter;

import android.content.*;
import com.touchus.benchilauncher.bean.*;
import com.touchus.benchilauncher.utils.*;
import android.view.*;
import java.util.*;
import android.widget.*;

public class SystemSetItemAdapter extends BaseAdapter
{
    private Context context;
    private List<SettingInfo> listData;
    private SpUtilK mSpUtilK;
    private int seclectIndex;
    
    public SystemSetItemAdapter(final Context context, final List<SettingInfo> listData) {
        this.context = context;
        this.listData = listData;
        this.mSpUtilK = new SpUtilK(context);
    }
    
    public int getCount() {
        return this.listData.size();
    }
    
    public Object getItem(final int n) {
        return this.listData.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        ViewHolder tag;
        if (inflate == null) {
            inflate = View.inflate(this.context, 2130903046, (ViewGroup)null);
            tag = new ViewHolder();
            tag.appIcon = (ImageView)inflate.findViewById(2131427353);
            tag.appName = (TextView)inflate.findViewById(2131427354);
            tag.appICheck = (CheckBox)inflate.findViewById(2131427355);
            inflate.setTag((Object)tag);
        }
        else {
            tag = (ViewHolder)inflate.getTag();
        }
        final SettingInfo settingInfo = this.listData.get(n);
        tag.appIcon.setVisibility(8);
        tag.appName.setText((CharSequence)settingInfo.getItem());
        if (Locale.getDefault().getLanguage().contains("en")) {
            tag.appName.setTextSize(22.0f);
        }
        else {
            tag.appName.setTextSize(28.0f);
        }
        if (settingInfo.isCheckItem()) {
            tag.appICheck.setVisibility(0);
            tag.appICheck.setChecked(settingInfo.isChecked());
        }
        else {
            tag.appICheck.setVisibility(8);
        }
        final RelativeLayout relativeLayout = (RelativeLayout)inflate.findViewById(2131427352);
        if (this.seclectIndex == n) {
            relativeLayout.setBackgroundResource(2130837866);
            return inflate;
        }
        relativeLayout.setBackgroundColor(16777215);
        return inflate;
    }
    
    public void setSeclectIndex(final int seclectIndex) {
        this.seclectIndex = seclectIndex;
        this.notifyDataSetChanged();
    }
    
    static class ViewHolder
    {
        CheckBox appICheck;
        ImageView appIcon;
        TextView appName;
    }
}
