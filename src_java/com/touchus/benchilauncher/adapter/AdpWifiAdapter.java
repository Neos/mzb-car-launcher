package com.touchus.benchilauncher.adapter;

import android.content.*;
import java.util.*;
import com.touchus.benchilauncher.bean.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import android.view.*;

public class AdpWifiAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<BeanWifi> listData;
    public Launcher mLauncher;
    public int rowCount;
    
    public AdpWifiAdapter(final Context context, final ArrayList<BeanWifi> listData) {
        this.rowCount = 5;
        this.context = context;
        this.listData = listData;
        this.rowCount = 5;
    }
    
    public int getCount() {
        return this.listData.size();
    }
    
    public Object getItem(final int n) {
        return this.listData.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public ArrayList<BeanWifi> getListData() {
        return this.listData;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        ViewHolder tag;
        if (inflate == null) {
            inflate = LayoutInflater.from(this.context).inflate(2130903085, (ViewGroup)null);
            tag = new ViewHolder();
            tag.nameTview = (TextView)inflate.findViewById(2131427583);
            tag.layoutSpace = (Space)inflate.findViewById(2131427580);
            tag.signImg = (ImageView)inflate.findViewById(2131427584);
            tag.selectImg = (ImageView)inflate.findViewById(2131427581);
            tag.loadingImg = (ImageView)inflate.findViewById(2131427582);
            inflate.setTag((Object)tag);
        }
        else {
            tag = (ViewHolder)inflate.getTag();
        }
        final BeanWifi beanWifi = this.listData.get(n);
        if (beanWifi.getLevel() > 0) {
            tag.signImg.setVisibility(0);
            tag.signImg.setImageLevel(beanWifi.getLevel());
        }
        else {
            tag.signImg.setVisibility(4);
        }
        tag.nameTview.setText((CharSequence)beanWifi.getName());
        tag.layoutSpace.setLayoutParams((ViewGroup$LayoutParams)new LinearLayout$LayoutParams(1, viewGroup.getHeight() / this.rowCount));
        if (beanWifi.getState() == 1) {
            tag.selectImg.setVisibility(0);
            tag.loadingImg.setVisibility(8);
            return inflate;
        }
        tag.selectImg.setVisibility(4);
        if (beanWifi.getState() == 2) {
            tag.loadingImg.setVisibility(0);
            return inflate;
        }
        tag.loadingImg.setVisibility(8);
        return inflate;
    }
    
    static class ViewHolder
    {
        Space layoutSpace;
        ImageView loadingImg;
        TextView nameTview;
        ImageView selectImg;
        ImageView signImg;
    }
}
