package com.touchus.benchilauncher.adapter;

import android.content.*;
import java.util.*;
import com.touchus.publicutils.bean.*;
import android.view.*;
import com.touchus.publicutils.utils.*;
import android.graphics.*;
import com.squareup.picasso.*;
import java.io.*;
import android.widget.*;

public class MediaAdapter extends BaseAdapter
{
    private Context context;
    private LayoutInflater layoutInflater;
    private List<MediaBean> mediaBeans;
    private int seclectIndex;
    private int type;
    
    public MediaAdapter(final Context context, final List<MediaBean> mediaBeans) {
        this.type = 0;
        this.mediaBeans = mediaBeans;
        this.context = context;
        this.layoutInflater = (LayoutInflater)context.getSystemService("layout_inflater");
    }
    
    public int getCount() {
        return this.mediaBeans.size();
    }
    
    public Object getItem(final int n) {
        return this.mediaBeans.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        View inflate = view;
        if (view == null) {
            inflate = this.layoutInflater.inflate(2130903098, (ViewGroup)null);
        }
        final MediaBean mediaBean = this.mediaBeans.get(n);
        final ImageView imageView = (ImageView)inflate.findViewById(2131427671);
        final TextView textView = (TextView)inflate.findViewById(2131427672);
        if (this.type == 0) {
            imageView.setVisibility(8);
        }
        else if (this.type == 1) {
            imageView.setVisibility(0);
            imageView.setTag((Object)mediaBean.getData());
            if (mediaBean.getBitmap() == null) {
                LoadLocalImageUtil.getInstance().loadDrawable(mediaBean, LoadLocalImageUtil.VIDEO_TYPE, this.context, mediaBean.getData(), (LoadLocalImageUtil.ImageCallback)new LoadLocalImageUtil.ImageCallback() {
                    @Override
                    public void imageLoaded(final Bitmap imageBitmap, final String s) {
                        if (imageView.getTag().equals(s)) {
                            imageView.setImageBitmap(imageBitmap);
                        }
                    }
                });
            }
            else {
                imageView.setImageBitmap(mediaBean.getBitmap());
            }
        }
        else {
            imageView.setVisibility(0);
            Picasso.with(this.context).load(new File(mediaBean.getData())).into(imageView);
        }
        textView.setText((CharSequence)mediaBean.getTitle());
        final RelativeLayout relativeLayout = (RelativeLayout)inflate.findViewById(2131427352);
        if (this.seclectIndex == n) {
            relativeLayout.setBackgroundResource(2130837769);
            return inflate;
        }
        relativeLayout.setBackgroundColor(16777215);
        return inflate;
    }
    
    public void notifyDataSetChanged(final int type) {
        this.type = type;
        this.notifyDataSetChanged();
    }
    
    public void setSeclectIndex(final int seclectIndex) {
        this.seclectIndex = seclectIndex;
    }
}
