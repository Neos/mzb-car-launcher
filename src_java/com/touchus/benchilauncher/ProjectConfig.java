package com.touchus.benchilauncher;

import java.util.*;

public class ProjectConfig
{
    public static String APPSoft;
    public static String CANSoft;
    public static final String CONFIG_URL = "http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh";
    public static List<String> FactoryPwd;
    public static String MCUSoft;
    public static String projectName;
    public static String systemSoft;
    
    static {
        ProjectConfig.projectName = "TYPE\uff1aXHD_";
        ProjectConfig.systemSoft = "c200_en-ota-user.smartpie.zip";
        ProjectConfig.MCUSoft = "BENZ_MCU.BIN";
        ProjectConfig.CANSoft = "can_app.bin";
        ProjectConfig.APPSoft = "C200_EN_BCLauncher.apk";
        (ProjectConfig.FactoryPwd = new ArrayList<String>()).add("111111");
        ProjectConfig.FactoryPwd.add("2109");
    }
}
