package com.touchus.benchilauncher.bean;

public class RadioWaveItem
{
    public boolean bk;
    public boolean point;
    public String txt;
    
    public RadioWaveItem(final boolean point, final boolean bk, final String txt) {
        this.point = point;
        this.bk = bk;
        this.txt = txt;
    }
}
