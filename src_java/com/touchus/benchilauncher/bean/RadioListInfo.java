package com.touchus.benchilauncher.bean;

import java.util.*;

public class RadioListInfo
{
    public int curIndexAm;
    public int curIndexFm;
    public ArrayList<String> listAm;
    public ArrayList<String> listFm;
    public int type;
}
