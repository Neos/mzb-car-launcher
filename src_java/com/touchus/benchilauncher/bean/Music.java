package com.touchus.benchilauncher.bean;

public class Music
{
    private String data;
    private String display_name;
    private int id;
    private String mime_type;
    private int size;
    private String title;
    
    public String getData() {
        return this.data;
    }
    
    public String getDisplay_name() {
        return this.display_name;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getMime_type() {
        return this.mime_type;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setData(final String data) {
        this.data = data;
    }
    
    public void setDisplay_name(final String display_name) {
        this.display_name = display_name;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public void setMime_type(final String mime_type) {
        this.mime_type = mime_type;
    }
    
    public void setSize(final int size) {
        this.size = size;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
}
