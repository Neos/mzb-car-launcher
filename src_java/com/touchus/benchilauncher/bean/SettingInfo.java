package com.touchus.benchilauncher.bean;

public class SettingInfo
{
    private boolean checked;
    private boolean isCheckItem;
    private String item;
    
    public SettingInfo(final String item, final boolean isCheckItem) {
        this.item = item;
        this.isCheckItem = isCheckItem;
    }
    
    public SettingInfo(final String item, final boolean isCheckItem, final boolean checked) {
        this.item = item;
        this.isCheckItem = isCheckItem;
        this.checked = checked;
    }
    
    public String getItem() {
        return this.item;
    }
    
    public boolean isCheckItem() {
        return this.isCheckItem;
    }
    
    public boolean isChecked() {
        return this.checked;
    }
    
    public void setCheckItem(final boolean isCheckItem) {
        this.isCheckItem = isCheckItem;
    }
    
    public void setChecked(final boolean checked) {
        this.checked = checked;
    }
    
    public void setItem(final String item) {
        this.item = item;
    }
}
