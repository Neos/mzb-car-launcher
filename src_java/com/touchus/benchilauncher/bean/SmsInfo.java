package com.touchus.benchilauncher.bean;

public class SmsInfo
{
    public static int TYPE_AD;
    public static int TYPE_ALL;
    public static int TYPE_LOCATE;
    public static int TYPE_SMS;
    private String data1;
    private String data2;
    private boolean hadRead;
    private String id;
    private boolean inEdit;
    private String name;
    private String sender;
    private long time;
    private int type;
    
    static {
        SmsInfo.TYPE_LOCATE = 1;
        SmsInfo.TYPE_AD = 2;
        SmsInfo.TYPE_SMS = 3;
        SmsInfo.TYPE_ALL = 4;
    }
    
    public SmsInfo() {
        this.hadRead = true;
        this.inEdit = false;
    }
    
    public String getData1() {
        return this.data1;
    }
    
    public String getData2() {
        return this.data2;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getSender() {
        return this.sender;
    }
    
    public long getTime() {
        return this.time;
    }
    
    public int getType() {
        return this.type;
    }
    
    public boolean isHadRead() {
        return this.hadRead;
    }
    
    public boolean isInEdit() {
        return this.inEdit;
    }
    
    public void setData1(final String data1) {
        this.data1 = data1;
    }
    
    public void setData2(final String data2) {
        this.data2 = data2;
    }
    
    public void setHadRead(final boolean hadRead) {
        this.hadRead = hadRead;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public void setInEdit(final boolean inEdit) {
        this.inEdit = inEdit;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setSender(final String sender) {
        this.sender = sender;
    }
    
    public void setTime(final long time) {
        this.time = time;
    }
    
    public void setType(final int type) {
        this.type = type;
    }
}
