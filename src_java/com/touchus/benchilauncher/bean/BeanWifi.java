package com.touchus.benchilauncher.bean;

public class BeanWifi
{
    public static final int CONNECT = 1;
    public static final int CONNECTING = 2;
    public static final int UNCONNECT = 3;
    private String capabilities;
    private int level;
    private String name;
    private boolean pwd;
    private boolean saveInlocal;
    private int state;
    
    public String getCapabilities() {
        return this.capabilities;
    }
    
    public int getLevel() {
        return this.level;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getState() {
        return this.state;
    }
    
    public boolean isPwd() {
        return this.pwd;
    }
    
    public boolean isSaveInlocal() {
        return this.saveInlocal;
    }
    
    public void setCapabilities(final String capabilities) {
        this.capabilities = capabilities;
    }
    
    public void setLevel(final int level) {
        this.level = level;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setPwd(final boolean pwd) {
        this.pwd = pwd;
    }
    
    public void setSaveInlocal(final boolean saveInlocal) {
        this.saveInlocal = saveInlocal;
    }
    
    public void setState(final int state) {
        this.state = state;
    }
}
