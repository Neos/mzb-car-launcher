package com.touchus.benchilauncher.bean;

import java.io.*;

public class CallRecord implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String callName;
    private int callState;
    private String callTime;
    private String callphoneNumber;
    
    public String getCallName() {
        return this.callName;
    }
    
    public int getCallState() {
        return this.callState;
    }
    
    public String getCallTime() {
        return this.callTime;
    }
    
    public String getCallphoneNumber() {
        return this.callphoneNumber;
    }
    
    public void setCallName(final String callName) {
        this.callName = callName;
    }
    
    public void setCallState(final int callState) {
        this.callState = callState;
    }
    
    public void setCallTime(final String callTime) {
        this.callTime = callTime;
    }
    
    public void setCallphoneNumber(final String callphoneNumber) {
        this.callphoneNumber = callphoneNumber;
    }
}
