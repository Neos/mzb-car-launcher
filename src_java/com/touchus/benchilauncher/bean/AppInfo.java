package com.touchus.benchilauncher.bean;

import android.graphics.drawable.*;

public class AppInfo
{
    private Drawable appIcon;
    private String appName;
    private boolean iCheck;
    private String packageName;
    private int versionCode;
    private String versionName;
    
    public AppInfo() {
        this.appName = "";
        this.packageName = "";
        this.versionName = "";
        this.versionCode = 0;
        this.iCheck = false;
        this.appIcon = null;
    }
    
    public Drawable getAppIcon() {
        return this.appIcon;
    }
    
    public String getAppName() {
        return this.appName;
    }
    
    public String getPackageName() {
        return this.packageName;
    }
    
    public int getVersionCode() {
        return this.versionCode;
    }
    
    public String getVersionName() {
        return this.versionName;
    }
    
    public boolean isiCheck() {
        return this.iCheck;
    }
    
    public void setAppIcon(final Drawable appIcon) {
        this.appIcon = appIcon;
    }
    
    public void setAppName(final String appName) {
        this.appName = appName;
    }
    
    public void setPackageName(final String packageName) {
        this.packageName = packageName;
    }
    
    public void setVersionCode(final int versionCode) {
        this.versionCode = versionCode;
    }
    
    public void setVersionName(final String versionName) {
        this.versionName = versionName;
    }
    
    public void setiCheck(final boolean iCheck) {
        this.iCheck = iCheck;
    }
}
