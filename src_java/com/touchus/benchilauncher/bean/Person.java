package com.touchus.benchilauncher.bean;

public class Person
{
    private int flag;
    private String id;
    private String name;
    private String phone;
    private String remark;
    
    public Person() {
    }
    
    public Person(final String id, final String name, final String phone, final int flag, final String remark) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.flag = flag;
        this.remark = remark;
    }
    
    public int getFlag() {
        return this.flag;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getPhone() {
        return this.phone;
    }
    
    public String getRemark() {
        return this.remark;
    }
    
    public void setFlag(final int flag) {
        this.flag = flag;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setPhone(final String phone) {
        this.phone = phone;
    }
    
    public void setRemark(final String remark) {
        this.remark = remark;
    }
}
