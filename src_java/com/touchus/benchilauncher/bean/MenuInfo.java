package com.touchus.benchilauncher.bean;

public class MenuInfo
{
    private int bigIconRes;
    private int nameRes;
    private int smallIconRes;
    
    public MenuInfo() {
    }
    
    public MenuInfo(final int nameRes, final int bigIconRes, final int smallIconRes) {
        this.nameRes = nameRes;
        this.bigIconRes = bigIconRes;
        this.smallIconRes = smallIconRes;
    }
    
    public int getBigIconRes() {
        return this.bigIconRes;
    }
    
    public int getNameRes() {
        return this.nameRes;
    }
    
    public int getSmallIconRes() {
        return this.smallIconRes;
    }
    
    public void setBigIconRes(final int bigIconRes) {
        this.bigIconRes = bigIconRes;
    }
    
    public void setNameRes(final int nameRes) {
        this.nameRes = nameRes;
    }
    
    public void setSmallIconRes(final int smallIconRes) {
        this.smallIconRes = smallIconRes;
    }
}
