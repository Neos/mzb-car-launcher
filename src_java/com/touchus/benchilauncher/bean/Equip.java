package com.touchus.benchilauncher.bean;

public class Equip
{
    private String address;
    private int index;
    private String name;
    
    public Equip() {
    }
    
    public Equip(final int index, final String name, final String address) {
        this.index = index;
        this.name = name;
        this.address = address;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setAddress(final String address) {
        this.address = address;
    }
    
    public void setIndex(final int index) {
        this.index = index;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
}
