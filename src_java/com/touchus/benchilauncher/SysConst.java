package com.touchus.benchilauncher;

public class SysConst
{
    public static final int AGILITY_SETTING = 6003;
    public static final int AIR_INFO = 1012;
    public static final int AUDIO_SOURCE = 1001;
    public static final int AUX_ACTIVATE_STUTAS = 1048;
    public static final int BACKLIGHT = 1019;
    public static final int BLUETOOTH_CONNECT = 1002;
    public static final int BLUETOOTH_PHONE_STATE = 6002;
    public static final int BLUE_TOOTH_ISOPNESTATE = 6005;
    public static final int BT_A2DP_STATUS = 1041;
    public static final int BT_HEADSET_STATUS = 1042;
    public static final int CALL_FLOAT = 1040;
    public static final int CANBOX_INFO_UPDATE = 1127;
    public static final int CAR_BASE_INFO = 1007;
    public static final int CD_MUSIC_PLAY = 1005;
    public static final int CHANGE_TO_HOME = 1024;
    public static final int CLOSE_VOICE_APP = 1034;
    public static final int COPY_ERROR = 1128;
    public static final int CURENTRADIO = 1011;
    public static boolean DEBUG = false;
    public static boolean DV_CUSTOM = false;
    public static final String EVENT_EASYCONN_A2DP_ACQUIRE = "net.easyconn.a2dp.acquire";
    public static final String EVENT_EASYCONN_A2DP_ACQUIRE_FAIL = "net.easyconn.a2dp.acquire.fail";
    public static final String EVENT_EASYCONN_A2DP_ACQUIRE_OK = "net.easyconn.a2dp.acquire.ok";
    public static final String EVENT_EASYCONN_A2DP_RELEASE = "net.easyconn.a2dp.release";
    public static final String EVENT_EASYCONN_A2DP_RELEASE_FAIL = "net.easyconn.a2dp.release.fail";
    public static final String EVENT_EASYCONN_A2DP_RELEASE_OK = "net.easyconn.a2dp.release.ok ";
    public static final String EVENT_EASYCONN_APP_QUIT = "net.easyconn.app.quit";
    public static final String EVENT_EASYCONN_BT_CHECKSTATUS = "net.easyconn.bt.checkstatus";
    public static final String EVENT_EASYCONN_BT_CONNECT = "net.easyconn.bt.connect";
    public static final String EVENT_EASYCONN_BT_CONNECTED = "net.easyconn.bt.connected";
    public static final String EVENT_EASYCONN_BT_OPENED = "net.easyconn.bt.opened";
    public static final String EVENT_EASYCONN_BT_UNCONNECTED = "net.easyconn.bt.unconnected";
    public static final String EVENT_MCU_ENTER_STANDBY = "com.unibroad.mcu.enterStandbyMode";
    public static final String EVENT_MCU_WAKE_UP = "com.unibroad.mcu.wakeUp";
    public static final String EVENT_UNIBROAD_AUDIO_FOCUS = "com.unibroad.AudioFocus";
    public static final String EVENT_UNIBROAD_AUDIO_REGAIN = "com.unibroad.AudioFocus.REGAIN";
    public static final String EVENT_UNIBROAD_OPEN_ORIGINALCAR = "com.unibroad.sys.originalcar";
    public static final String EVENT_UNIBROAD_PAPAGOGAIN = "com.unibroad.AudioFocus.PAPAGOGAIN";
    public static final String EVENT_UNIBROAD_PAPAGOLOSS = "com.unibroad.AudioFocus.PAPAGOLOSS";
    public static final String EVENT_UNIBROAD_WEATHERDATA = "com.unibroad.weatherdata";
    public static final int FEEDBACK_RESULT = 1039;
    public static final String FLAG_AIR_INFO = "airInfo";
    public static final String FLAG_AUDIO_SOURCE = "audioSource";
    public static final String FLAG_AUX_ACTIVATE_STUTAS = "aux_activate_stutas";
    public static final String FLAG_CAR_BASE_INFO = "carBaseInfo";
    public static final String FLAG_CONFIG_CONNECT = "connectType";
    public static final String FLAG_CONFIG_NAVI = "naviExist";
    public static final String FLAG_CONFIG_RADIO = "radioType";
    public static final String FLAG_CONFIG_SCREEN = "screenType";
    public static final String FLAG_CONFIG_USB_NUM = "usbNum";
    public static final String FLAG_LANGUAGE_TYPE = "languageType";
    public static final String FLAG_MUSIC_LOOP = "musicloop";
    public static final String FLAG_MUSIC_POS = "musicPos";
    public static final String FLAG_REVERSING_TYPE = "reversingType";
    public static final String FLAG_REVERSING_VOICE = "voiceType";
    public static final String FLAG_RUNNING_CURRSPEED = "flag_running_currspeed";
    public static final String FLAG_RUNNING_STATE = "runningState";
    public static final String FLAG_RUNNING_ZHUANSU = "flag_running_zhuansu";
    public static final String FLAG_UPGRADE_PER = "upgradePer";
    public static final String FLAG_USB_LISTENER = "usblistener";
    public static final String FLAG_VIDEO_POS = "videoPos";
    public static final int FORCE_MAIN_VIEW = 1030;
    public static final int GET_CURRENT_CHANNEL = 1037;
    public static final int GOTO_BT_VIEW = 1013;
    public static final int GPS_STATUS = 1038;
    public static final int HIDE_ORIGINAL_VIEW = 1035;
    public static final String IDRIVER_ENUM = "idriver_enum";
    public static final String IDRIVER_STATE_ENUM = "idriver_state_enum";
    public static final int IDRVIER_STATE = 6001;
    public static final String IS_CILK_SHIZI = "isCilkshizi";
    public static final int LISTITEM_UPDATE = 1031;
    public static final int MCU_SHOW_UPGRADE_DIALOG = 1132;
    public static final int MCU_UPGRADE_CUR_DATA = 1133;
    public static final int MCU_UPGRADE_FINISH = 1131;
    public static final int MCU_UPGRADE_HEADER = 1130;
    public static final int[] MENU_BIG_ICON;
    public static final int[] MENU_LIST;
    public static final int[] MENU_SMALL_ICON;
    public static final int MUSIC_PLAY = 1004;
    public static final int ONLINE_CONFIG_DATA = 1124;
    public static final int ONLINE_CONFIG_FINISH = 1126;
    public static final int ONLINE_CONFIG_VALID = 1125;
    public static final int ORIGINAL_FLAG = 1044;
    public static final int PRESS_NEXT = 1014;
    public static final int RADAR_BACK = 1016;
    public static final int RADAR_FRONT = 1015;
    public static final int RADIOLIST = 1025;
    public static final int REVERSING = 1018;
    public static final int REVERSING_FINISH = 1026;
    public static final int RUNNING_STATE = 1008;
    public static final int SHOW_LOADING_VIEW = 2001;
    public static final int SHOW_UPGRADE_DIALOG = 1122;
    public static final String SP_SETTING_TIME = "sp_setting_time";
    public static final String SP_SETTING_YEAR = "sp_setting_year";
    public static final int START_VOICE_APP = 1033;
    public static final int SYSTEM_SETTING = 1003;
    public static final int TIME_CANDLER = 1009;
    public static final int UPDATE_CUR_MUSIC_PLAY_INFO = 7001;
    public static final int UPDATE_MUSIC_PLAY_STATE = 7003;
    public static final int UPDATE_NAVI = 1045;
    public static final int UPDATE_NAVI_END = 1047;
    public static final int UPDATE_NAVI_START = 1046;
    public static final int UPDATE_TIME_AUTO = 2003;
    public static final int UPDATE_WEATHER = 1010;
    public static final int UPGRADE_CHECK_FINISH = 1111;
    public static final int UPGRADE_COPY_PER = 1112;
    public static final int UPGRADE_CUR_DATA = 1123;
    public static final int UPGRADE_ERROR = 1129;
    public static final int UPGRADE_FINISH = 1121;
    public static final int UPGRADE_HEADER = 1120;
    public static final int UPGRADE_UPGRADE_PER = 1113;
    public static final int USB_CD_MUSIC_PLAY = 1006;
    public static final int USB_LISTENER = 7002;
    public static final int VOICE_WAKEUP_MENU = 1036;
    public static final int WHEEL_TURN = 1017;
    public static final int YOUHAO_STATE = 6004;
    public static int basicNum;
    public static String callVoice;
    public static int[] callnum;
    public static String dvVoice;
    public static int mediaBasicNum;
    public static String mediaVoice;
    public static String mixPro;
    public static float musicDecVolume;
    public static float musicNorVolume;
    public static String naviVoice;
    public static int[] num;
    public static byte[] storeData;
    
    static {
        SysConst.DEBUG = false;
        SysConst.DV_CUSTOM = false;
        SysConst.musicDecVolume = 0.3f;
        SysConst.musicNorVolume = 1.0f;
        SysConst.basicNum = 23;
        SysConst.mediaBasicNum = 10;
        SysConst.num = new int[] { 0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 };
        SysConst.callnum = new int[] { 0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 };
        SysConst.mixPro = "mixpro";
        SysConst.naviVoice = "naviVoice";
        SysConst.mediaVoice = "mediaVoice";
        SysConst.dvVoice = "dvVoice";
        SysConst.callVoice = "callVoice";
        MENU_LIST = new int[] { 2131165262, 2131165210, 2131165264, 2131165263, 2131165270, 2131165266, 2131165267, 2131165268, 2131165269, 2131165276 };
        MENU_BIG_ICON = new int[] { 2130837829, 2130837823, 2130837819, 2130837821, 2130838012, 2130837895, 2130837770, 2130837809, 2130837825, 2130837713 };
        MENU_SMALL_ICON = new int[] { 2130837830, 2130837824, 2130837820, 2130837822, 2130838013, 2130837896, 2130837771, 2130837810, 2130837826, 2130837714 };
        SysConst.storeData = new byte[10];
    }
    
    public static boolean isBT() {
        return LauncherApplication.isBT;
    }
    
    public static class Cardoor
    {
        public static final int BEHIND = 1;
        public static final int FRONT = 0;
        public static final int LEFT_BEHIND = 2;
        public static final int LEFT_FRONT = 3;
        public static final int RIGHT_BEHIND = 4;
        public static final int RIGHT_FRONT = 5;
    }
    
    public enum DataTpye
    {
        BT_PHONE_VOLUME("BT_PHONE_VOLUME", 1, 2), 
        LANGUAGE("LANGUAGE", 2, 3), 
        NAVI_VOLUME("NAVI_VOLUME", 0, 1);
        
        private int code;
        
        private DataTpye(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
}
