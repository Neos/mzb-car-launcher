package com.touchus.benchilauncher.receiver;

import android.content.*;
import com.touchus.benchilauncher.*;

public class PublibReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final LauncherApplication launcherApplication = (LauncherApplication)context.getApplicationContext();
        if ("com.unibroad.mail".equals(intent.getAction())) {
            launcherApplication.sendMessage(1039, intent.getExtras());
        }
    }
}
