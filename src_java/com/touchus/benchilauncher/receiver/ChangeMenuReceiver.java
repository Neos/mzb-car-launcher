package com.touchus.benchilauncher.receiver;

import android.content.*;
import com.touchus.benchilauncher.*;
import android.util.*;

public class ChangeMenuReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final LauncherApplication launcherApplication = (LauncherApplication)context.getApplicationContext();
        if (launcherApplication.launcherHandler != null) {
            return;
        }
        if (intent.getAction().equals("com.unibroad.AudioFocus.PAPAGOGAIN") || intent.getAction().equals("com.unibroad.AudioFocus.PAPAGOLOSS")) {
            Log.d("unibroadAudioFocus", "action = " + intent.getAction());
            return;
        }
        final String stringExtra = intent.getStringExtra("pos");
        int n = 0;
        if ("main_navi".equals(stringExtra)) {
            n = 0;
        }
        else if ("main_radio".equals(stringExtra) || "main_original".equals(stringExtra)) {
            n = 1;
        }
        else if ("main_media".equals(stringExtra)) {
            n = 2;
        }
        else if ("main_phone".equals(stringExtra)) {
            n = 3;
        }
        else if ("main_voice".equals(stringExtra)) {
            n = 5;
        }
        else if ("main_instrument".equals(stringExtra)) {
            n = 4;
        }
        else if ("main_recorder".equals(stringExtra)) {
            n = 6;
        }
        else if ("main_app".equals(stringExtra)) {
            n = 7;
        }
        else if ("main_setting".equals(stringExtra)) {
            n = 8;
        }
        else if ("main_close".equals(stringExtra)) {
            n = 9;
        }
        launcherApplication.serviceHandler.obtainMessage(1036, (Object)n).sendToTarget();
    }
}
