package com.touchus.benchilauncher.receiver;

import android.content.*;
import com.touchus.benchilauncher.*;
import android.util.*;
import android.os.*;

public class GuideInfoReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final LauncherApplication launcherApplication = (LauncherApplication)context.getApplicationContext();
        if (launcherApplication == null) {
            Log.e("GuideInfoReceiver", "guideInfo coming " + intent.getIntExtra("KEY_TYPE", 0) + ",intent.getExtras = " + intent.getExtras());
            if (intent.getIntExtra("KEY_TYPE", 0) == 10001) {
                Log.e("GuideInfoReceiver", "guideInfo coming " + intent.getExtras());
                LauncherApplication.pageCount = 5;
                launcherApplication.sendMessage(1045, intent.getExtras());
                return;
            }
            if (intent.getIntExtra("KEY_TYPE", 0) == 10019) {
                switch (intent.getIntExtra("EXTRA_STATE", 0)) {
                    default: {}
                    case 8:
                    case 10: {
                        LauncherApplication.pageCount = 5;
                        launcherApplication.sendMessage(1046, null);
                    }
                    case 9:
                    case 12: {
                        LauncherApplication.pageCount = 4;
                        launcherApplication.sendMessage(1047, null);
                    }
                    case 39: {
                        launcherApplication.sendMessage(1047, null);
                    }
                }
            }
        }
    }
}
