package com.touchus.benchilauncher.receiver;

import android.content.*;
import com.touchus.benchilauncher.*;
import java.util.*;
import com.touchus.publicutils.bean.*;
import com.touchus.benchilauncher.service.*;
import android.os.*;

public class UsbBroadcastReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final String action = intent.getAction();
        final LauncherApplication launcherApplication = (LauncherApplication)context.getApplicationContext();
        if (!"android.intent.action.MEDIA_CHECKING".equals(action) && !"android.intent.action.MEDIA_MOUNTED".equals(action)) {
            if ("android.intent.action.MEDIA_EJECT".equals(action)) {
                LauncherApplication.musicList.clear();
                LauncherApplication.videoList.clear();
                LauncherApplication.imageList.clear();
            }
            else if ("android.intent.action.MEDIA_SCANNER_STARTED".equals(action)) {
                launcherApplication.isScanner = true;
            }
            else if ("android.intent.action.MEDIA_UNMOUNTED".equals(action)) {
                launcherApplication.isScanner = false;
            }
            else if ("android.intent.action.MEDIA_SCANNER_FINISHED".equals(action)) {
                LauncherApplication.musicList.clear();
                LauncherApplication.videoList.clear();
                LauncherApplication.imageList.clear();
                LauncherApplication.musicList.addAll(MusicLoader.instance(context.getContentResolver()).getMusicList());
                LauncherApplication.videoList.addAll(new VideoService(context).getVideoList());
                LauncherApplication.imageList.addAll(new PictrueUtil(context).getPicList());
                launcherApplication.isScanner = false;
            }
        }
        final Bundle bundle = new Bundle();
        bundle.putString("usblistener", action);
        launcherApplication.sendMessage(7002, bundle);
    }
}
