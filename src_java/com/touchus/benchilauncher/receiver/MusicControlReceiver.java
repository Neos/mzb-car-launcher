package com.touchus.benchilauncher.receiver;

import android.content.*;
import com.touchus.benchilauncher.*;
import android.view.*;
import android.util.*;
import cn.kuwo.autosdk.api.*;

public class MusicControlReceiver extends BroadcastReceiver
{
    int count;
    
    public MusicControlReceiver() {
        this.count = 0;
    }
    
    public void onReceive(final Context context, final Intent intent) {
        final LauncherApplication launcherApplication = (LauncherApplication)context.getApplicationContext();
        final String action = intent.getAction();
        final KeyEvent keyEvent = (KeyEvent)intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
        Log.i("musicControl", "Action ---->" + action + "  KeyEvent----->" + keyEvent.toString());
        Log.i("musicControl", "app.musicType ---->" + launcherApplication.musicType);
        if ("android.intent.action.MEDIA_BUTTON".equals(action)) {
            final int keyCode = keyEvent.getKeyCode();
            if (keyEvent.getAction() == 0 && launcherApplication.service != null) {
                if (87 == keyCode) {
                    if (launcherApplication.musicType == 1) {
                        if (launcherApplication.musicPlayControl != null) {
                            launcherApplication.musicPlayControl.playNextMusic();
                        }
                    }
                    else {
                        if (launcherApplication.musicType == 2) {
                            launcherApplication.btservice.playNext();
                            return;
                        }
                        if (launcherApplication.musicType == 5 && launcherApplication.kwapi.isKuwoRunning(context)) {
                            launcherApplication.kwapi.setPlayState(context, PlayState.STATE_NEXT);
                        }
                    }
                }
                else if (127 == keyCode) {
                    if (launcherApplication.musicType == 1) {
                        if (launcherApplication.musicPlayControl != null) {
                            launcherApplication.musicPlayControl.pauseMusic();
                        }
                    }
                    else {
                        if (launcherApplication.musicType == 2) {
                            launcherApplication.btservice.pausePlaySync(false);
                            return;
                        }
                        if (launcherApplication.musicType == 5 && launcherApplication.kwapi.isKuwoRunning(context)) {
                            launcherApplication.kwapi.setPlayState(context, PlayState.STATE_PAUSE);
                        }
                    }
                }
                else if (126 == keyCode) {
                    if (launcherApplication.musicType == 1) {
                        if (launcherApplication.musicPlayControl != null) {
                            launcherApplication.musicPlayControl.replayMusic();
                        }
                    }
                    else {
                        if (launcherApplication.musicType == 2) {
                            launcherApplication.btservice.pausePlaySync(true);
                            return;
                        }
                        if (launcherApplication.musicType == 5 && launcherApplication.kwapi.isKuwoRunning(context)) {
                            if (this.count % 2 == 1) {
                                launcherApplication.kwapi.setPlayState(context, PlayState.STATE_PAUSE);
                                return;
                            }
                            launcherApplication.kwapi.setPlayState(context, PlayState.STATE_PLAY);
                        }
                    }
                }
                else if (88 == keyCode) {
                    if (launcherApplication.musicType == 1) {
                        if (launcherApplication.musicPlayControl != null) {
                            launcherApplication.musicPlayControl.playPreviousMusic();
                        }
                    }
                    else {
                        if (launcherApplication.musicType == 2) {
                            launcherApplication.btservice.playPrev();
                            return;
                        }
                        if (launcherApplication.musicType == 5 && launcherApplication.kwapi.isKuwoRunning(context)) {
                            launcherApplication.kwapi.setPlayState(context, PlayState.STATE_PRE);
                        }
                    }
                }
                else if (86 == keyCode) {
                    if (launcherApplication.musicType == 1) {
                        if (launcherApplication.musicPlayControl != null) {
                            launcherApplication.musicPlayControl.stopMusic();
                        }
                    }
                    else {
                        if (launcherApplication.musicType == 2) {
                            launcherApplication.btservice.stopMusic();
                            return;
                        }
                        if (launcherApplication.musicType == 5 && launcherApplication.kwapi.isKuwoRunning(context)) {
                            launcherApplication.kwapi.setPlayState(context, PlayState.STATE_PAUSE);
                        }
                    }
                }
            }
        }
    }
}
