package com.touchus.benchilauncher.receiver;

import android.content.*;
import com.touchus.benchilauncher.*;
import com.touchus.publicutils.utils.*;
import android.util.*;

public class AdressInfoReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final LauncherApplication launcherApplication = (LauncherApplication)context.getApplicationContext();
        if (launcherApplication != null) {
            return;
        }
        launcherApplication.address = intent.getStringExtra("addr");
        launcherApplication.gpsCityName = intent.getStringExtra("cityName");
        CrashHandler.getInstance(context).setAddress(launcherApplication.address);
        Log.e("AdressInfoReceiver", "address  = " + launcherApplication.address + ",city = " + launcherApplication.gpsCityName);
    }
}
