package com.touchus.benchilauncher.service;

import android.content.*;
import com.touchus.publicutils.bean.*;
import android.os.*;
import com.touchus.benchilauncher.utils.*;
import android.util.*;
import com.touchus.benchilauncher.*;
import java.util.*;
import android.media.*;
import java.io.*;

public class MusicPlayControl
{
    public static final int PLAY_LOOP_LOOP = 1001;
    public static final int PLAY_LOOP_RANDOM = 1003;
    public static final int PLAY_SINGE_LOOP = 1002;
    private Context context;
    public MediaBean currentMusic;
    public String currentPlayPath;
    public boolean iHoldAudioFocus;
    public boolean iHoldLongAudioFocus;
    public boolean iIsManualPause;
    public boolean iNeedUpsetPlayOrder;
    public boolean ipause;
    private LauncherApplication mApp;
    private AudioManager$OnAudioFocusChangeListener mAudioFocusListener;
    public AudioManager mAudioManager;
    public Handler mHandler;
    private SpUtilK mSpUtilK;
    public int musicIndex;
    private List<MediaBean> musicList;
    private MediaPlayer musicPlayer;
    private int playLoopType;
    
    public MusicPlayControl(final Context context) {
        this.musicPlayer = null;
        this.mHandler = null;
        this.musicList = new ArrayList<MediaBean>();
        this.musicIndex = -1;
        this.currentMusic = null;
        this.playLoopType = 1001;
        this.iIsManualPause = false;
        this.iHoldAudioFocus = false;
        this.iHoldLongAudioFocus = false;
        this.ipause = false;
        this.iNeedUpsetPlayOrder = false;
        this.mAudioFocusListener = (AudioManager$OnAudioFocusChangeListener)new AudioManager$OnAudioFocusChangeListener() {
            public void onAudioFocusChange(final int n) {
                switch (n) {
                    case -1: {
                        Log.d("requestAudioFocus", "music AudioManager.AUDIOFOCUS_LOSS   iHoldAudioFocus:" + MusicPlayControl.this.iHoldAudioFocus);
                        MusicPlayControl.this.stopLocalMusic();
                    }
                    case -3: {
                        Log.d("requestAudioFocus", "music  AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                        MusicPlayControl.this.setHoldAudioFocus(false, false);
                        if (SysConst.isBT() || MusicPlayControl.this.mApp.ismix) {
                            MusicPlayControl.this.musicPlayer.setVolume(SysConst.musicDecVolume, SysConst.musicDecVolume);
                            return;
                        }
                        if (MusicPlayControl.this.isMusicPlaying()) {
                            MusicPlayControl.this.pauseMusic();
                            return;
                        }
                        break;
                    }
                    case -2: {
                        Log.d("requestAudioFocus", "music  AudioManager.AUDIOFOCUS_LOSS_TRANSIENT");
                        MusicPlayControl.this.setHoldAudioFocus(false, false);
                        if (MusicPlayControl.this.isMusicPlaying()) {
                            MusicPlayControl.this.pauseMusic();
                            return;
                        }
                        break;
                    }
                    case 1: {
                        Log.d("requestAudioFocus", "music  AudioManager.AUDIOFOCUS_GAIN");
                        MusicPlayControl.this.musicPlayer.setVolume(SysConst.musicNorVolume, SysConst.musicNorVolume);
                        MusicPlayControl.this.setHoldAudioFocus(true, true);
                        MusicPlayControl.this.mApp.musicType = 1;
                        if (!MusicPlayControl.this.ipause) {
                            MusicPlayControl.this.replayMusic();
                            return;
                        }
                        break;
                    }
                }
            }
        };
        this.context = context;
        this.mApp = (LauncherApplication)context.getApplicationContext();
        this.musicPlayer = new MediaPlayer();
        this.mAudioManager = (AudioManager)this.context.getSystemService("audio");
    }
    
    private int getNextIndex() {
        final int musicIndex = this.musicIndex;
        int n;
        if (this.playLoopType == 1001) {
            n = musicIndex + 1;
            if (n >= this.musicList.size()) {
                return 0;
            }
        }
        else {
            n = musicIndex;
            if (this.playLoopType == 1003) {
                final int size = this.musicList.size();
                if (size <= 1) {
                    return 0;
                }
                Random random;
                int i;
                for (random = new Random(), i = random.nextInt(size); i == musicIndex; i = random.nextInt(size)) {}
                return i;
            }
        }
        return n;
    }
    
    private void play(final int n) throws IOException {
        this.currentMusic = this.musicList.get(n);
        this.settingPlayState(false);
        if (this.musicPlayer != null) {
            this.musicPlayer.stop();
            this.musicPlayer.release();
            this.musicPlayer = null;
            this.musicPlayer = new MediaPlayer();
        }
        this.musicPlayer.setDataSource(this.currentMusic.getData());
        this.musicPlayer.setAudioStreamType(3);
        this.musicPlayer.setLooping(false);
        this.musicPlayer.prepareAsync();
        this.musicPlayer.setOnPreparedListener((MediaPlayer$OnPreparedListener)new MediaPlayer$OnPreparedListener() {
            public void onPrepared(final MediaPlayer mediaPlayer) {
                mediaPlayer.start();
                MusicPlayControl.this.settingPlayState(true);
            }
        });
        this.musicPlayer.setOnCompletionListener((MediaPlayer$OnCompletionListener)new MediaPlayer$OnCompletionListener() {
            public void onCompletion(final MediaPlayer mediaPlayer) {
                MusicPlayControl.this.musicIndex = MusicPlayControl.this.getNextIndex();
                MusicPlayControl.this.playMusic(MusicPlayControl.this.musicIndex);
            }
        });
        this.musicPlayer.setOnErrorListener((MediaPlayer$OnErrorListener)new MediaPlayer$OnErrorListener() {
            public boolean onError(final MediaPlayer mediaPlayer, final int n, final int n2) {
                mediaPlayer.reset();
                return false;
            }
        });
        if (this.mHandler != null) {
            this.mHandler.obtainMessage(7001).sendToTarget();
        }
    }
    
    private void randomMusic() {
        final int size = this.musicList.size();
        if (size <= 1) {
            this.musicIndex = 0;
            return;
        }
        Random random;
        int i;
        for (random = new Random(), i = random.nextInt(size); i == this.musicIndex; i = random.nextInt(size)) {}
        this.musicIndex = i;
    }
    
    private void saveMusicDate() {
    }
    
    private void setHoldAudioFocus(final boolean b, final boolean b2) {
        synchronized (this) {
            this.iHoldAudioFocus = b;
            if (b2) {
                this.iHoldLongAudioFocus = b;
            }
        }
    }
    
    public void changeCurrentPlayProgress(final int n) {
        this.musicPlayer.seekTo(n);
    }
    
    public void destroy() {
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages((Object)null);
        }
        this.unrequestMusicAudioFocus();
        this.setHoldAudioFocus(false, true);
        this.musicPlayer.release();
    }
    
    public List<MediaBean> getMusicList() {
        return this.musicList;
    }
    
    public int getPlayLoopType() {
        return this.playLoopType;
    }
    
    public int getPosition() {
        try {
            return this.musicPlayer.getCurrentPosition();
        }
        catch (Exception ex) {
            return 0;
        }
    }
    
    public boolean isMusicPlaying() {
        return LauncherApplication.iPlaying;
    }
    
    public void pauseMusic() {
        try {
            this.musicPlayer.pause();
            this.settingPlayState(false);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void playMusic(final int musicIndex) {
        synchronized (this) {
            try {
                if (this.musicList != null && this.musicList.size() > 0) {
                    if (musicIndex != -1 && this.musicIndex != musicIndex) {
                        this.musicIndex = musicIndex;
                    }
                    if (this.musicIndex >= this.musicList.size()) {
                        this.musicIndex = 0;
                    }
                    LauncherApplication.musicIndex = this.musicIndex;
                    LauncherApplication.mSpUtils.putInt("musicPos", this.musicIndex);
                    this.play(musicIndex);
                    this.requestAudioFocus();
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void playNextMusic() {
        if (this.musicList == null || this.musicList.size() <= 0) {
            return;
        }
        if (this.playLoopType == 1003) {
            this.randomMusic();
        }
        else {
            ++this.musicIndex;
            int musicIndex;
            if (this.musicIndex < this.musicList.size()) {
                musicIndex = this.musicIndex;
            }
            else {
                musicIndex = 0;
            }
            this.musicIndex = musicIndex;
        }
        this.playMusic(this.musicIndex);
    }
    
    public void playPreviousMusic() {
        if (this.musicList == null || this.musicList.size() <= 0) {
            return;
        }
        if (this.playLoopType == 1003) {
            this.randomMusic();
        }
        else {
            --this.musicIndex;
            int musicIndex;
            if (this.musicIndex >= 0) {
                musicIndex = this.musicIndex;
            }
            else {
                musicIndex = this.musicList.size() - 1;
            }
            this.musicIndex = musicIndex;
        }
        this.playMusic(this.musicIndex);
    }
    
    public void replayMusic() {
        if (!this.iHoldAudioFocus) {
            this.pauseMusic();
            return;
        }
        try {
            this.musicPlayer.start();
            this.requestAudioFocus();
            this.settingPlayState(true);
        }
        catch (Exception ex) {}
    }
    
    public void requestAudioFocus() {
        this.mApp.musicType = 1;
        final int requestAudioFocus = this.mAudioManager.requestAudioFocus(this.mAudioFocusListener, 3, 1);
        Log.e("Music requestAudioFocus", "flag" + requestAudioFocus);
        if (requestAudioFocus == 1) {
            this.setHoldAudioFocus(true, true);
        }
        else if (this.mAudioManager.requestAudioFocus(this.mAudioFocusListener, 3, 1) == 1) {
            this.setHoldAudioFocus(true, true);
        }
        else {
            this.setHoldAudioFocus(false, true);
        }
        LauncherApplication.iPlayingAuto = true;
    }
    
    public void setHoldAudioFocus(final boolean b) {
        synchronized (this) {
            this.iHoldAudioFocus = b;
            this.iHoldLongAudioFocus = b;
        }
    }
    
    public void setPlayList(final List<MediaBean> musicList) {
        this.musicList = musicList;
    }
    
    public void setPlayLoopType(final int playLoopType) {
        this.playLoopType = playLoopType;
    }
    
    public void settingPlayState(final boolean iPlaying) {
        if ((!iPlaying || !LauncherApplication.iPlaying) && (iPlaying || LauncherApplication.iPlaying)) {
            LauncherApplication.iPlaying = iPlaying;
            if (this.mHandler != null) {
                this.mHandler.obtainMessage(7003).sendToTarget();
            }
        }
    }
    
    public void stopLocalMusic() {
        this.setHoldAudioFocus(false, true);
        if (this.mApp.musicPlayControl != null && this.isMusicPlaying()) {
            this.pauseMusic();
            this.settingPlayState(false);
            this.destroy();
            this.mApp.musicPlayControl.ipause = false;
            this.mApp.musicPlayControl = null;
            this.saveMusicDate();
            LauncherApplication.iPlayingAuto = false;
        }
        this.unrequestMusicAudioFocus();
    }
    
    public void stopMusic() {
        try {
            this.musicPlayer.stop();
            this.settingPlayState(false);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void unrequestMusicAudioFocus() {
        this.mAudioManager.abandonAudioFocus(this.mAudioFocusListener);
    }
}
