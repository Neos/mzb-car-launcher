package com.touchus.benchilauncher.service;

import android.content.*;
import android.database.*;
import com.touchus.publicutils.bean.*;
import java.io.*;
import com.touchus.benchilauncher.utils.*;
import android.provider.*;
import java.util.*;

public class VideoService
{
    private static String TAG;
    private static Context context;
    private static Cursor cursor;
    private static List<MediaBean> mediaBeans;
    
    static {
        VideoService.TAG = "VideoService";
    }
    
    public VideoService() {
    }
    
    public VideoService(final Context context) {
        VideoService.context = context;
    }
    
    private void GetFiles(final String s, final boolean b) {
        final File[] listFiles = new File(s).listFiles();
        for (int i = 0; i < listFiles.length; ++i) {
            final File file = listFiles[i];
            if (file.isFile()) {
                if (VideoType.isVideo(file.getPath().substring(file.getPath().lastIndexOf(".")))) {
                    VideoService.mediaBeans.add(new MediaBean());
                }
                if (!b) {
                    break;
                }
            }
            else if (file.isDirectory() && file.getPath().indexOf("/.") == -1) {
                this.GetFiles(file.getPath(), b);
            }
        }
    }
    
    private void getFilesBySystem() {
        VideoService.cursor = VideoService.context.getContentResolver().query(MediaStore$Video$Media.EXTERNAL_CONTENT_URI, new String[] { "_id", "_display_name", "title", "_size", "_data", "duration", "mime_type" }, (String)null, (String[])null, (String)null);
        if (VideoService.cursor == null) {
            return;
        }
        while (VideoService.cursor.moveToNext()) {
            final MediaBean mediaBean = new MediaBean();
            mediaBean.setId(VideoService.cursor.getLong(VideoService.cursor.getColumnIndexOrThrow("_id")));
            mediaBean.setDisplay_name(VideoService.cursor.getString(VideoService.cursor.getColumnIndexOrThrow("_display_name")));
            mediaBean.setTitle(VideoService.cursor.getString(VideoService.cursor.getColumnIndexOrThrow("title")));
            mediaBean.setMime_type(VideoService.cursor.getString(VideoService.cursor.getColumnIndexOrThrow("mime_type")));
            mediaBean.setSize(VideoService.cursor.getLong(VideoService.cursor.getColumnIndexOrThrow("_size")));
            mediaBean.setData(VideoService.cursor.getString(VideoService.cursor.getColumnIndexOrThrow("_data")));
            VideoService.mediaBeans.add(mediaBean);
        }
        VideoService.cursor.close();
    }
    
    public static MediaBean getVideo(final int n) {
        return VideoService.mediaBeans.get(n);
    }
    
    public int getCount() {
        return VideoService.context.getContentResolver().query(MediaStore$Video$Media.EXTERNAL_CONTENT_URI, (String[])null, (String)null, (String[])null, (String)null).getCount();
    }
    
    public List<MediaBean> getVideoList() {
        VideoService.mediaBeans = new ArrayList<MediaBean>();
        this.getFilesBySystem();
        return VideoService.mediaBeans;
    }
    
    public List<MediaBean> getVideoList(final int n, final int n2) {
        VideoService.mediaBeans = new ArrayList<MediaBean>();
        this.getFilesBySystem();
        return VideoService.mediaBeans;
    }
}
