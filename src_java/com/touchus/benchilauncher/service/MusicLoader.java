package com.touchus.benchilauncher.service;

import java.util.*;
import com.touchus.publicutils.bean.*;
import android.net.*;
import android.provider.*;
import android.database.*;
import android.content.*;

public class MusicLoader
{
    private static final String TAG = "com.cwf.app.musicplay";
    private static ContentResolver contentResolver;
    private static ArrayList<MediaBean> musicList;
    private static MusicLoader musicLoader;
    private Uri contentUri;
    private String[] projection;
    private String sortOrder;
    
    private MusicLoader() {
        this.contentUri = MediaStore$Audio$Media.EXTERNAL_CONTENT_URI;
        this.projection = new String[] { "_id", "_display_name", "title", "_data", "album", "artist", "duration", "_size" };
        this.sortOrder = "_size";
    }
    
    public static MusicLoader instance(final ContentResolver contentResolver) {
        if (MusicLoader.musicLoader == null) {
            MusicLoader.contentResolver = contentResolver;
            MusicLoader.musicLoader = new MusicLoader();
        }
        return MusicLoader.musicLoader;
    }
    
    private void queryMusicList() {
        final Cursor query = MusicLoader.contentResolver.query(this.contentUri, this.projection, (String)null, (String[])null, (String)null);
        final ArrayList<String> list = new ArrayList<String>();
        if (query == null) {
            return;
        }
        if (query.moveToFirst()) {
            final int columnIndex = query.getColumnIndex("_display_name");
            final int columnIndex2 = query.getColumnIndex("title");
            final int columnIndex3 = query.getColumnIndex("_id");
            final int columnIndex4 = query.getColumnIndex("duration");
            final int columnIndex5 = query.getColumnIndex("_size");
            final int columnIndex6 = query.getColumnIndex("_data");
            do {
                final String string = query.getString(columnIndex2);
                final String string2 = query.getString(columnIndex);
                final long long1 = query.getLong(columnIndex3);
                final int int1 = query.getInt(columnIndex4);
                final long long2 = query.getLong(columnIndex5);
                final String string3 = query.getString(columnIndex6);
                final MediaBean mediaBean = new MediaBean();
                mediaBean.setId(long1);
                mediaBean.setTitle(string);
                mediaBean.setDisplay_name(string2);
                mediaBean.setDuration(int1);
                mediaBean.setSize(long2);
                mediaBean.setData(string3);
                if (!list.contains(string3) && int1 != 0) {
                    list.add(string3);
                    MusicLoader.musicList.add(mediaBean);
                }
            } while (query.moveToNext());
        }
        query.close();
    }
    
    public ArrayList<MediaBean> getMusicList() {
        MusicLoader.musicList = new ArrayList<MediaBean>();
        this.queryMusicList();
        return MusicLoader.musicList;
    }
    
    public Uri getMusicUriById(final long n) {
        return ContentUris.withAppendedId(this.contentUri, n);
    }
}
