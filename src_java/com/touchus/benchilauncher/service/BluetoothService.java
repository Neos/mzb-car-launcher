package com.touchus.benchilauncher.service;

import android.app.*;
import com.touchus.benchilauncher.bean.*;
import android.media.*;
import org.slf4j.*;
import com.touchus.benchilauncher.*;
import android.text.*;
import com.touchus.benchilauncher.views.*;
import android.util.*;
import com.backaudio.android.driver.*;
import com.unibroad.c2py.*;
import android.content.*;
import com.backaudio.android.driver.bluetooth.*;
import java.io.*;
import com.touchus.publicutils.utils.*;
import java.util.*;
import com.touchus.benchilauncher.utils.*;
import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import android.os.*;

public class BluetoothService extends Service
{
    public static final int BOOK_LIST_CANCEL_LOAD = 1408;
    public static final int BOOK_LIST_START_LOAD = 1407;
    public static final int CALL_TALKING = 1411;
    public static final int CALL_TYPE_IN = 1412;
    public static final int CALL_TYPE_MISS = 1414;
    public static final int CALL_TYPE_OUT = 1413;
    public static final int DEVICE_SWITCH = 1410;
    public static final int DRIVER_BOOK_LIST = 1409;
    public static final int END_PAIRMODE = 1402;
    public static final int HANGUP_PHONE = 1416;
    public static final int MEDIAINFO = 1421;
    public static final int ONGOING_NOTIFICATION = 1400;
    public static final int OUTCALL_FLOAT = 1422;
    public static final int PLAY_STATE_PAUSE = 1419;
    public static final int PLAY_STATE_PLAYING = 1418;
    public static final int PLAY_STATE_STOP = 1420;
    public static final int START_PAIRMODE = 1401;
    public static final int TIME_FLAG = 1417;
    public static final int UPDATE_NAME = 1406;
    public static final int UPDATE_PHONENUM = 1415;
    public static EPhoneStatus bluetoothStatus;
    public static int currentCallingType;
    public static String currentEquipAddress;
    public static String deviceName;
    public static ArrayList<Equip> equipList;
    public static boolean iIsCallingInButNotalkingState;
    private static Logger logger;
    public static int mediaPlayState;
    public static boolean talkingflag;
    public String Apppath;
    public boolean accFlag;
    private LauncherApplication app;
    private BroadcastReceiver assistantReceiver;
    public boolean blueMusicFocus;
    public Bluetooth bluetooth;
    ArrayList<Person> bookList;
    private int booklistIndex;
    public int calltime;
    public boolean changeAudioResult;
    public String currentEquipName;
    public int downloadType;
    public int downloadflag;
    public boolean histalkflag;
    private boolean iHoldFocus;
    public boolean iSoundInPhone;
    public boolean ipause;
    public boolean isAfterInOrOutCall;
    boolean isDownloading;
    private long lastCloseCallingViewTime;
    private long lastPhoneBookDownloadTime;
    public int lasthisindex;
    public int lastindex;
    private AudioManager$OnAudioFocusChangeListener mAudioFocusListener;
    public AudioManager mAudioManager;
    public PlayerBinder mBinder;
    private AudioManager$OnAudioFocusChangeListener mBlueMusicFocusListener;
    private Context mContext;
    private SpUtilK mSpUtilK;
    public String music;
    public boolean onPairingResult;
    private Handler serviceHandler;
    private SharedPreferences shared_device;
    public boolean showDialogflag;
    public String singer;
    private Thread timeThread;
    
    static {
        BluetoothService.logger = LoggerFactory.getLogger(BluetoothService.class);
        BluetoothService.mediaPlayState = 1;
        BluetoothService.iIsCallingInButNotalkingState = false;
        BluetoothService.equipList = new ArrayList<Equip>();
        BluetoothService.talkingflag = false;
        BluetoothService.bluetoothStatus = EPhoneStatus.UNCONNECT;
        BluetoothService.deviceName = "Benz";
        BluetoothService.currentEquipAddress = "";
    }
    
    public BluetoothService() {
        this.accFlag = false;
        this.histalkflag = false;
        this.iSoundInPhone = false;
        this.isAfterInOrOutCall = false;
        this.showDialogflag = false;
        this.calltime = 0;
        this.lastindex = 0;
        this.lasthisindex = 0;
        this.downloadflag = -1;
        this.downloadType = 0;
        this.lastPhoneBookDownloadTime = 0L;
        this.currentEquipName = "";
        this.music = "";
        this.singer = "";
        this.bookList = new ArrayList<Person>();
        this.booklistIndex = 0;
        this.mAudioManager = null;
        this.timeThread = null;
        this.Apppath = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/unibroad/";
        this.mBinder = new PlayerBinder();
        this.onPairingResult = false;
        this.serviceHandler = new Handler();
        this.changeAudioResult = false;
        this.ipause = false;
        this.iHoldFocus = false;
        this.mAudioFocusListener = (AudioManager$OnAudioFocusChangeListener)new AudioManager$OnAudioFocusChangeListener() {
            public void onAudioFocusChange(final int n) {
                switch (n) {
                    default: {}
                    case -3:
                    case -2:
                    case -1: {
                        BluetoothService.logger.debug("mAudioFocusListener bluetooth  AUDIOFOCUS_LOSS ,talkingflag:" + BluetoothService.talkingflag);
                        if (BluetoothService.talkingflag) {
                            BluetoothService.this.requestAudioFocus();
                        }
                        BluetoothService.access$1(BluetoothService.this, false);
                    }
                    case 1: {
                        BluetoothService.access$1(BluetoothService.this, true);
                    }
                }
            }
        };
        this.lastCloseCallingViewTime = -1L;
        this.isDownloading = false;
        this.blueMusicFocus = false;
        this.mBlueMusicFocusListener = (AudioManager$OnAudioFocusChangeListener)new AudioManager$OnAudioFocusChangeListener() {
            public void onAudioFocusChange(final int n) {
                switch (n) {
                    case -1: {
                        BluetoothService.logger.debug("btmusic AUDIOFOCUS_LOSS  " + BluetoothService.mediaPlayState);
                        BluetoothService.this.stopBTMusic();
                        break;
                    }
                    case -3: {
                        BluetoothService.logger.debug("requestAudioFocus\tbtmusic AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK" + BluetoothService.mediaPlayState);
                        if (SysConst.isBT() || BluetoothService.this.app.ismix) {
                            BluetoothService.this.setBTVolume((int)(SysConst.musicDecVolume * 100.0f));
                        }
                        else if (BluetoothService.mediaPlayState == 1418 && BluetoothService.this.blueMusicFocus) {
                            BluetoothService.this.pausePlaySync(false);
                        }
                        BluetoothService.this.blueMusicFocus = false;
                        break;
                    }
                    case -2: {
                        BluetoothService.logger.debug("requestAudioFocus\tbtmusic AUDIOFOCUS_LOSS_TRANSIENT" + BluetoothService.mediaPlayState);
                        if (BluetoothService.mediaPlayState == 1418 && BluetoothService.this.blueMusicFocus) {
                            BluetoothService.this.pausePlaySync(false);
                        }
                        BluetoothService.this.blueMusicFocus = false;
                        break;
                    }
                    case 1: {
                        BluetoothService.this.blueMusicFocus = true;
                        BluetoothService.logger.debug("requestAudioFocus\tbtmusic AUDIOFOCUS_GAIN" + BluetoothService.mediaPlayState);
                        BluetoothService.this.setBTVolume((int)(SysConst.musicNorVolume * 100.0f));
                        if (1419 == BluetoothService.mediaPlayState && !BluetoothService.this.ipause) {
                            BluetoothService.this.pausePlaySync(true);
                        }
                        BluetoothService.this.app.musicType = 2;
                        break;
                    }
                }
                BluetoothService.this.app.sendMessage(BluetoothService.mediaPlayState, null);
            }
        };
        this.assistantReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                try {
                    final int intExtra = intent.getIntExtra("analyticCmd", -1);
                    final String stringExtra = intent.getStringExtra("stringArg1");
                    final String stringExtra2 = intent.getStringExtra("stringArg2");
                    BluetoothService.logger.debug("assistantReceiver  cmdId: " + intExtra + " , strParam2: " + stringExtra2);
                    if (intExtra == -1) {
                        return;
                    }
                    switch (intExtra) {
                        case 1011: {
                            BluetoothService.currentCallingType = 1413;
                            if (EPhoneStatus.CONNECTING == BluetoothService.bluetoothStatus) {
                                break;
                            }
                            if (EPhoneStatus.CONNECTED != BluetoothService.bluetoothStatus) {
                                BluetoothService.this.app.launcherHandler.obtainMessage(1036, (Object)3).sendToTarget();
                                return;
                            }
                            if (TextUtils.isEmpty((CharSequence)stringExtra2) || stringExtra2.length() < 2 || stringExtra2.contains("*") || stringExtra2.contains("#")) {
                                ToastTool.showBigShortToast(BluetoothService.this.mContext, 2131165239);
                                return;
                            }
                            BluetoothService.this.app.phoneNumber = stringExtra2;
                            BluetoothService.this.bluetooth.call(BluetoothService.this.app.phoneNumber);
                        }
                        case 1012:
                        case 1013:
                        case 1014: {
                            BluetoothService.this.app.launcherHandler.obtainMessage(1036, (Object)3).sendToTarget();
                        }
                        case 1015: {
                            if ("1".equals(stringExtra)) {
                                BluetoothService.this.answerCalling();
                                return;
                            }
                            BluetoothService.this.cutdownCurrentCalling();
                            break;
                        }
                        default: {}
                    }
                }
                catch (Exception ex) {}
            }
        };
    }
    
    private void CreateFile(final String s) {
        try {
            final File file = new File(String.valueOf(this.Apppath) + "/" + s);
            if (!file.exists()) {
                file.createNewFile();
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
            BluetoothService.logger.debug("create file error : " + ex.getMessage());
        }
    }
    
    private void CreatePerFile(final String s) {
        try {
            final File file = new File(String.valueOf(this.Apppath) + "/_Per.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(String.valueOf(s) + "_Per.xml");
            bufferedWriter.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    static /* synthetic */ void access$1(final BluetoothService bluetoothService, final boolean iHoldFocus) {
        bluetoothService.iHoldFocus = iHoldFocus;
    }
    
    static /* synthetic */ void access$11(final BluetoothService bluetoothService, final Thread timeThread) {
        bluetoothService.timeThread = timeThread;
    }
    
    static /* synthetic */ void access$18(final BluetoothService bluetoothService, final int booklistIndex) {
        bluetoothService.booklistIndex = booklistIndex;
    }
    
    static /* synthetic */ void access$7(final BluetoothService bluetoothService, final long lastPhoneBookDownloadTime) {
        bluetoothService.lastPhoneBookDownloadTime = lastPhoneBookDownloadTime;
    }
    
    private void putDeviceInfo(final String s, final String s2) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        final SharedPreferences$Editor edit = this.shared_device.edit();
        edit.putString("name", s);
        edit.putString("addr", s2);
        edit.commit();
    }
    
    private void sendVoiceAssistantCallEndEvent() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.voiceassistant.start.other.app");
        intent.putExtra("iNeedEnterIncall", "end");
        intent.setFlags(32);
        this.sendBroadcast(intent);
    }
    
    private void sendVoiceAssistantInTalkingEvent() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.voiceassistant.start.other.app");
        intent.putExtra("iNeedEnterIncall", "talking");
        intent.setFlags(32);
        this.sendBroadcast(intent);
    }
    
    private void sendVoiceAssistantStartEvent() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.voiceassistant.start.other.app");
        intent.putExtra("iNeedEnterIncall", "incall");
        intent.putExtra("name", this.app.phoneName);
        intent.putExtra("number", this.app.phoneNumber);
        intent.setFlags(32);
        this.sendBroadcast(intent);
    }
    
    public void CallOut(final String phoneNumber) {
        try {
            if (phoneNumber.length() < 2 || phoneNumber.contains("*") || phoneNumber.contains("#")) {
                ToastTool.showBigShortToast(this.mContext, 2131165239);
                return;
            }
            BluetoothService.currentCallingType = 1413;
            BluetoothService.bluetoothStatus = EPhoneStatus.CALLING_OUT;
            this.app.phoneNumber = phoneNumber;
            FloatSystemCallDialog.getInstance().clearView();
            FloatSystemCallDialog.getInstance().setShowStatus(FloatSystemCallDialog.FloatShowST.OUTCALLING);
            this.bluetooth.call(phoneNumber);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void answerCalling() {
        try {
            this.requestAudioFocus();
            this.bluetooth.answerThePhone();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void cancelDownloadPhoneBook() {
        try {
            this.bluetooth.cancelDownloadPhoneBook();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void connectDevice(final String s) {
        try {
            this.bluetooth.connectDeviceSync(s);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void cutdownCurrentCalling() {
        this.lastCloseCallingViewTime = System.currentTimeMillis();
        if (BluetoothService.currentCallingType != 1413 && !this.histalkflag && !TextUtils.isEmpty((CharSequence)this.app.phoneNumber)) {
            this.setHistoryList(1414);
        }
        this.app.phoneNumber = "";
        this.app.phoneName = "";
        this.app.isCalling = false;
        this.unrequestAudioFocus();
        while (true) {
            try {
                this.bluetooth.hangUpThePhone();
                if (this.app.currentDialog4 != null) {
                    this.app.currentDialog4.dismiss();
                }
                this.sendVoiceAssistantCallEndEvent();
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void decVolume() {
        try {
            this.bluetooth.decVolume();
        }
        catch (Exception ex) {}
    }
    
    public void disconnectCurDevice() {
        try {
            this.bluetooth.disconnectCurrentDevice();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void downloadPhoneBook() {
        try {
            this.bluetooth.downloadPhoneBookSync(1);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void downloadRecordList() {
        try {
            this.bluetooth.downloadPhoneBookSync(5);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void enterPairingMode() {
        try {
            this.bluetooth.enterPairingModeSync();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void enterSystemFloatCallView() {
        BluetoothService.logger.debug("FloatSystemCallDialog >>>>>>enterSystemFloatCallView");
        if (this.app.topIsBlueMainFragment()) {
            return;
        }
        final FloatSystemCallDialog instance = FloatSystemCallDialog.getInstance();
        if (instance.isDestory()) {
            instance.showFloatCallView(this.getApplicationContext());
        }
        else {
            instance.show();
        }
        this.app.sendMessage(1040, null);
    }
    
    public void exitsource() {
        while (true) {
            try {
                BluetoothService.iIsCallingInButNotalkingState = false;
                this.unrequestAudioFocus();
                FloatSystemCallDialog.getInstance().setCallingPhonenumber("-1");
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public ArrayList<Person> getBookList() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aconst_null    
        //     3: astore_3       
        //     4: new             Ljava/util/ArrayList;
        //     7: dup            
        //     8: invokespecial   java/util/ArrayList.<init>:()V
        //    11: astore          4
        //    13: getstatic       com/touchus/benchilauncher/LauncherApplication.isBlueConnectState:Z
        //    16: ifne            22
        //    19: aload           4
        //    21: areturn        
        //    22: aload_1        
        //    23: astore_2       
        //    24: new             Ljava/io/File;
        //    27: dup            
        //    28: new             Ljava/lang/StringBuilder;
        //    31: dup            
        //    32: aload_0        
        //    33: getfield        com/touchus/benchilauncher/service/BluetoothService.Apppath:Ljava/lang/String;
        //    36: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    39: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    42: ldc_w           "/"
        //    45: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    48: getstatic       com/touchus/benchilauncher/service/BluetoothService.currentEquipAddress:Ljava/lang/String;
        //    51: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    54: ldc_w           "_Per.xml"
        //    57: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    60: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    63: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    66: astore          5
        //    68: aload_1        
        //    69: astore_2       
        //    70: aload           5
        //    72: invokevirtual   java/io/File.exists:()Z
        //    75: ifne            86
        //    78: aload_1        
        //    79: astore_2       
        //    80: aload           5
        //    82: invokevirtual   java/io/File.createNewFile:()Z
        //    85: pop            
        //    86: aload_1        
        //    87: astore_2       
        //    88: new             Ljava/io/FileInputStream;
        //    91: dup            
        //    92: aload           5
        //    94: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    97: astore_1       
        //    98: aload           4
        //   100: astore_2       
        //   101: aload_1        
        //   102: invokestatic    com/touchus/benchilauncher/utils/PersonParse.getPersons:(Ljava/io/InputStream;)Ljava/util/ArrayList;
        //   105: astore          4
        //   107: aload           4
        //   109: astore_3       
        //   110: aload           4
        //   112: ifnonnull       171
        //   115: aload           4
        //   117: astore_2       
        //   118: new             Ljava/util/ArrayList;
        //   121: dup            
        //   122: invokespecial   java/util/ArrayList.<init>:()V
        //   125: astore          4
        //   127: getstatic       com/touchus/benchilauncher/service/BluetoothService.logger:Lorg/slf4j/Logger;
        //   130: new             Ljava/lang/StringBuilder;
        //   133: dup            
        //   134: ldc_w           "onPhoneBookList::getBookList::"
        //   137: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   140: aload           4
        //   142: invokevirtual   java/util/ArrayList.size:()I
        //   145: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   148: ldc_w           "::"
        //   151: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   154: getstatic       com/touchus/benchilauncher/service/BluetoothService.currentEquipAddress:Ljava/lang/String;
        //   157: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   160: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   163: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   168: aload           4
        //   170: astore_3       
        //   171: aload_3        
        //   172: astore_2       
        //   173: getstatic       com/touchus/benchilauncher/service/BluetoothService.logger:Lorg/slf4j/Logger;
        //   176: new             Ljava/lang/StringBuilder;
        //   179: dup            
        //   180: ldc_w           "onPhoneBookList::getBookList::"
        //   183: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   186: aload_3        
        //   187: invokevirtual   java/util/ArrayList.size:()I
        //   190: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   193: ldc_w           "::"
        //   196: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   199: getstatic       com/touchus/benchilauncher/service/BluetoothService.currentEquipAddress:Ljava/lang/String;
        //   202: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   205: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   208: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   213: aload_1        
        //   214: ifnull          221
        //   217: aload_1        
        //   218: invokevirtual   java/io/FileInputStream.close:()V
        //   221: aload_3        
        //   222: areturn        
        //   223: astore_1       
        //   224: aload_1        
        //   225: invokevirtual   java/io/IOException.printStackTrace:()V
        //   228: goto            221
        //   231: astore_2       
        //   232: aload_3        
        //   233: astore_1       
        //   234: aload_2        
        //   235: astore_3       
        //   236: aload_1        
        //   237: astore_2       
        //   238: aload_3        
        //   239: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   242: aload_1        
        //   243: ifnull          250
        //   246: aload_1        
        //   247: invokevirtual   java/io/FileInputStream.close:()V
        //   250: aload           4
        //   252: areturn        
        //   253: astore_1       
        //   254: aload_1        
        //   255: invokevirtual   java/io/IOException.printStackTrace:()V
        //   258: goto            250
        //   261: astore_1       
        //   262: aload_2        
        //   263: ifnull          270
        //   266: aload_2        
        //   267: invokevirtual   java/io/FileInputStream.close:()V
        //   270: aload_1        
        //   271: athrow         
        //   272: astore_2       
        //   273: aload_2        
        //   274: invokevirtual   java/io/IOException.printStackTrace:()V
        //   277: goto            270
        //   280: astore_3       
        //   281: aload_1        
        //   282: astore_2       
        //   283: aload_3        
        //   284: astore_1       
        //   285: goto            262
        //   288: astore_3       
        //   289: aload_1        
        //   290: astore_2       
        //   291: aload_3        
        //   292: astore_1       
        //   293: goto            262
        //   296: astore_3       
        //   297: aload_2        
        //   298: astore          4
        //   300: goto            236
        //   303: astore_3       
        //   304: goto            236
        //    Signature:
        //  ()Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  24     68     231    236    Ljava/lang/Exception;
        //  24     68     261    262    Any
        //  70     78     231    236    Ljava/lang/Exception;
        //  70     78     261    262    Any
        //  80     86     231    236    Ljava/lang/Exception;
        //  80     86     261    262    Any
        //  88     98     231    236    Ljava/lang/Exception;
        //  88     98     261    262    Any
        //  101    107    296    303    Ljava/lang/Exception;
        //  101    107    280    288    Any
        //  118    127    296    303    Ljava/lang/Exception;
        //  118    127    280    288    Any
        //  127    168    303    307    Ljava/lang/Exception;
        //  127    168    288    296    Any
        //  173    213    296    303    Ljava/lang/Exception;
        //  173    213    280    288    Any
        //  217    221    223    231    Ljava/io/IOException;
        //  238    242    261    262    Any
        //  246    250    253    261    Ljava/io/IOException;
        //  266    270    272    280    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 153, Size: 153
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public String getDeviceName() {
        return BluetoothService.deviceName;
    }
    
    public ArrayList<Person> getHistoryList() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aconst_null    
        //     3: astore          4
        //     5: new             Ljava/util/ArrayList;
        //     8: dup            
        //     9: invokespecial   java/util/ArrayList.<init>:()V
        //    12: astore_3       
        //    13: getstatic       com/touchus/benchilauncher/LauncherApplication.isBlueConnectState:Z
        //    16: ifne            21
        //    19: aload_3        
        //    20: areturn        
        //    21: new             Ljava/io/FileInputStream;
        //    24: dup            
        //    25: new             Ljava/io/File;
        //    28: dup            
        //    29: new             Ljava/lang/StringBuilder;
        //    32: dup            
        //    33: aload_0        
        //    34: getfield        com/touchus/benchilauncher/service/BluetoothService.Apppath:Ljava/lang/String;
        //    37: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    40: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    43: ldc_w           "/"
        //    46: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    49: getstatic       com/touchus/benchilauncher/service/BluetoothService.currentEquipAddress:Ljava/lang/String;
        //    52: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    55: ldc_w           "_His.xml"
        //    58: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    61: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    64: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    67: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    70: astore_2       
        //    71: aload_3        
        //    72: astore_1       
        //    73: aload_2        
        //    74: invokestatic    com/touchus/benchilauncher/utils/PersonParse.getPersons:(Ljava/io/InputStream;)Ljava/util/ArrayList;
        //    77: astore_3       
        //    78: aload_3        
        //    79: astore_1       
        //    80: getstatic       com/touchus/benchilauncher/service/BluetoothService.logger:Lorg/slf4j/Logger;
        //    83: new             Ljava/lang/StringBuilder;
        //    86: dup            
        //    87: ldc_w           "onPhoneBookList::getHistoryList::"
        //    90: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    93: aload_3        
        //    94: invokevirtual   java/util/ArrayList.size:()I
        //    97: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   100: ldc_w           "::"
        //   103: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   106: getstatic       com/touchus/benchilauncher/service/BluetoothService.currentEquipAddress:Ljava/lang/String;
        //   109: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   112: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   115: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   120: aload_2        
        //   121: ifnull          128
        //   124: aload_2        
        //   125: invokevirtual   java/io/FileInputStream.close:()V
        //   128: aload_3        
        //   129: areturn        
        //   130: astore_1       
        //   131: aload_1        
        //   132: invokevirtual   java/io/IOException.printStackTrace:()V
        //   135: goto            128
        //   138: astore_1       
        //   139: aload           4
        //   141: astore_2       
        //   142: aload_1        
        //   143: astore          4
        //   145: aload_2        
        //   146: astore_1       
        //   147: getstatic       com/touchus/benchilauncher/service/BluetoothService.logger:Lorg/slf4j/Logger;
        //   150: ldc_w           "onPhoneBookList::getHistoryList::Exception"
        //   153: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   158: aload_2        
        //   159: astore_1       
        //   160: aload           4
        //   162: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   165: aload_2        
        //   166: ifnull          173
        //   169: aload_2        
        //   170: invokevirtual   java/io/FileInputStream.close:()V
        //   173: aload_3        
        //   174: areturn        
        //   175: astore_1       
        //   176: aload_1        
        //   177: invokevirtual   java/io/IOException.printStackTrace:()V
        //   180: goto            173
        //   183: astore_2       
        //   184: aload_1        
        //   185: ifnull          192
        //   188: aload_1        
        //   189: invokevirtual   java/io/FileInputStream.close:()V
        //   192: aload_2        
        //   193: athrow         
        //   194: astore_1       
        //   195: aload_1        
        //   196: invokevirtual   java/io/IOException.printStackTrace:()V
        //   199: goto            192
        //   202: astore_3       
        //   203: aload_2        
        //   204: astore_1       
        //   205: aload_3        
        //   206: astore_2       
        //   207: goto            184
        //   210: astore          4
        //   212: aload_1        
        //   213: astore_3       
        //   214: goto            145
        //    Signature:
        //  ()Ljava/util/ArrayList<Lcom/touchus/benchilauncher/bean/Person;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  21     71     138    145    Ljava/lang/Exception;
        //  21     71     183    184    Any
        //  73     78     210    217    Ljava/lang/Exception;
        //  73     78     202    210    Any
        //  80     120    210    217    Ljava/lang/Exception;
        //  80     120    202    210    Any
        //  124    128    130    138    Ljava/io/IOException;
        //  147    158    183    184    Any
        //  160    165    183    184    Any
        //  169    173    175    183    Ljava/io/IOException;
        //  188    192    194    202    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0128:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public String getbookName(final String s) {
        final ArrayList<Person> bookList = this.getBookList();
        String s2 = "";
        int n = 0;
        while (true) {
            String s3 = null;
            Label_0207: {
                while (true) {
                    try {
                        if (n >= bookList.size()) {
                            if (TextUtils.isEmpty((CharSequence)s2)) {
                                return s;
                            }
                        }
                        else {
                            if (s.equals(bookList.get(n).getPhone())) {
                                s3 = bookList.get(n).getName();
                                break Label_0207;
                            }
                            if (!bookList.get(n).getPhone().contains(s)) {
                                s3 = s2;
                                if (!s.contains(bookList.get(n).getPhone())) {
                                    break Label_0207;
                                }
                            }
                            s3 = s2;
                            if (s.length() <= 10) {
                                break Label_0207;
                            }
                            s3 = s2;
                            if (bookList.get(n).getPhone().length() > 10) {
                                s3 = bookList.get(n).getName();
                            }
                            break Label_0207;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                        BluetoothService.logger.debug("get name error e:" + ex.getMessage());
                        continue;
                    }
                    break;
                }
                break;
            }
            ++n;
            s2 = s3;
        }
        return s2;
    }
    
    public void incVolume() {
        try {
            this.bluetooth.incVolume();
        }
        catch (Exception ex) {}
    }
    
    public void leavePairingMode() {
        try {
            this.bluetooth.leavePairingModeSync();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public IBinder onBind(final Intent intent) {
        return (IBinder)this.mBinder;
    }
    
    public void onCreate() {
        super.onCreate();
        Log.e("", "BluetoothService ==== ");
        while (true) {
            try {
                this.mContext = (Context)this;
                this.bluetooth = Bluetooth.getInstance();
                this.shared_device = this.getSharedPreferences("device_name", 0);
                this.mSpUtilK = new SpUtilK(this.getApplicationContext());
                this.mAudioManager = (AudioManager)this.getSystemService("audio");
                this.bluetooth.addEventHandler(new BluetoothHandler());
                Mainboard.getInstance().addEventHandler(new BluetoothHandler());
                final File file = new File(this.Apppath);
                if (!file.exists()) {
                    file.mkdirs();
                }
                Parser.initialize();
                this.app = (LauncherApplication)this.getApplication();
                this.app.btservice = this;
                this.app.openOrCloseBluetooth(this.app.getNeedToEnterPairMode());
                this.serviceHandler.postDelayed((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        try {
                            BluetoothService.this.readPhoneStatus();
                            BluetoothService.this.readPairingList();
                            BluetoothService.this.setDeviceName(BluetoothService.this.mContext);
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, 1500L);
                final IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("com.unibroad.voiceassistant.analyticCmd");
                this.registerReceiver(this.assistantReceiver, intentFilter);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        BluetoothService.mediaPlayState = 1;
        this.unrequestAudioFocus();
        Parser.release();
        this.stopForeground(true);
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        return super.onStartCommand(intent, 1, n2);
    }
    
    public void pauseForce() {
        try {
            this.bluetooth.pauseSync();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void pausePlaySync(final boolean b) {
        try {
            this.bluetooth.pausePlaySync(b);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void playNext() {
        try {
            this.bluetooth.playNext();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void playPrev() {
        try {
            this.bluetooth.playPrev();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void pressVirutalButton(final EVirtualButton eVirtualButton) {
        try {
            this.bluetooth.pressVirutalButton(eVirtualButton);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void readDeviceAddr() {
        try {
            this.bluetooth.readDeviceAddr();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void readMediaInfo() {
        try {
            this.bluetooth.readMediaInfo();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void readMediaStatus() {
        try {
            this.bluetooth.readMediaStatus();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void readPairingList() {
        try {
            this.bluetooth.readPairingListSync();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void readPhoneStatus() {
        try {
            this.bluetooth.readPhoneStatusSync();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void removeDevice(final Context context, final String s) {
        try {
            this.bluetooth.removeDevice(s);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void requestAudioFocus() {
        boolean iHoldFocus = true;
        BluetoothService.logger.debug("bluetooth btservice requestAudioFocus");
        int n = this.mAudioManager.requestAudioFocus(this.mAudioFocusListener, 3, 2);
        for (int n2 = 0; n != 1 && n2 < 2; ++n2, n = this.mAudioManager.requestAudioFocus(this.mAudioFocusListener, 3, 2)) {}
        if (n != 1) {
            iHoldFocus = false;
        }
        this.iHoldFocus = iHoldFocus;
    }
    
    public void requestMusicAudioFocus() {
        this.app.musicType = 2;
        final int requestAudioFocus = this.mAudioManager.requestAudioFocus(this.mBlueMusicFocusListener, 3, 1);
        this.app.btservice.blueMusicFocus = (requestAudioFocus == 1);
        BluetoothService.logger.debug("bluetooth btservice requestMusicAudioFocus" + requestAudioFocus);
        this.setBTVolume((int)(SysConst.musicNorVolume * 100.0f));
        LauncherApplication.iPlayingAuto = true;
    }
    
    public void savePhoneBookList(final ArrayList<Person> list) {
        while (true) {
            try {
                final File file = new File(String.valueOf(this.Apppath) + "/" + BluetoothService.currentEquipAddress + "_Per.xml");
                if (!file.exists()) {
                    file.createNewFile();
                }
                PersonParse.save(list, new FileOutputStream(file));
                this.app.sendMessage(1409, null);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                BluetoothService.logger.debug("save error " + ex);
                continue;
            }
            break;
        }
    }
    
    public void sendBookchanged() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.voiceassistant.contactupdate");
        this.sendBroadcast(intent);
    }
    
    public void setBTEnterACC(final boolean btEnterACC) {
        try {
            this.bluetooth.setBTEnterACC(btEnterACC);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void setBTVolume(final int btVolume) {
        try {
            this.bluetooth.setBTVolume(btVolume);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void setDeviceName(final Context context) {
        Label_0059: {
            if (!TextUtils.isEmpty((CharSequence)UtilTools.getIMEI((Context)this))) {
                break Label_0059;
            }
            String imei = "000000000000000";
            try {
                while (true) {
                    BluetoothService.deviceName = String.valueOf(BluetoothService.deviceName) + "_" + imei.substring(11);
                    this.bluetooth.setDeviceName(BluetoothService.deviceName);
                    return;
                    imei = UtilTools.getIMEI((Context)this);
                    continue;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void setHistoryList(final int p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          5
        //     3: aconst_null    
        //     4: astore          6
        //     6: aload           5
        //     8: astore          4
        //    10: new             Ljava/io/File;
        //    13: dup            
        //    14: new             Ljava/lang/StringBuilder;
        //    17: dup            
        //    18: aload_0        
        //    19: getfield        com/touchus/benchilauncher/service/BluetoothService.Apppath:Ljava/lang/String;
        //    22: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    25: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    28: ldc_w           "/"
        //    31: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    34: getstatic       com/touchus/benchilauncher/service/BluetoothService.currentEquipAddress:Ljava/lang/String;
        //    37: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    40: ldc_w           "_His.xml"
        //    43: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    46: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    49: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    52: astore          7
        //    54: aload           5
        //    56: astore          4
        //    58: aload           7
        //    60: invokevirtual   java/io/File.exists:()Z
        //    63: ifne            76
        //    66: aload           5
        //    68: astore          4
        //    70: aload           7
        //    72: invokevirtual   java/io/File.createNewFile:()Z
        //    75: pop            
        //    76: aload           5
        //    78: astore          4
        //    80: new             Ljava/io/FileInputStream;
        //    83: dup            
        //    84: aload           7
        //    86: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    89: astore          5
        //    91: aload           5
        //    93: invokestatic    com/touchus/benchilauncher/utils/PersonParse.getPersons:(Ljava/io/InputStream;)Ljava/util/ArrayList;
        //    96: astore          6
        //    98: aload           6
        //   100: invokevirtual   java/util/ArrayList.size:()I
        //   103: bipush          30
        //   105: if_icmple       116
        //   108: aload           6
        //   110: bipush          29
        //   112: invokevirtual   java/util/ArrayList.remove:(I)Ljava/lang/Object;
        //   115: pop            
        //   116: getstatic       com/touchus/benchilauncher/service/BluetoothService.logger:Lorg/slf4j/Logger;
        //   119: new             Ljava/lang/StringBuilder;
        //   122: dup            
        //   123: ldc_w           "app.phoneNumber = "
        //   126: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   129: aload_0        
        //   130: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   133: getfield        com/touchus/benchilauncher/LauncherApplication.phoneNumber:Ljava/lang/String;
        //   136: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   139: ldc_w           ",app.phoneName = "
        //   142: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   145: aload_0        
        //   146: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   149: getfield        com/touchus/benchilauncher/LauncherApplication.phoneName:Ljava/lang/String;
        //   152: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   155: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   158: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   163: invokestatic    java/lang/System.currentTimeMillis:()J
        //   166: lstore_2       
        //   167: aload_0        
        //   168: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   171: getfield        com/touchus/benchilauncher/LauncherApplication.phoneName:Ljava/lang/String;
        //   174: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   177: ifeq            258
        //   180: aload_0        
        //   181: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   184: getfield        com/touchus/benchilauncher/LauncherApplication.phoneNumber:Ljava/lang/String;
        //   187: astore          4
        //   189: new             Lcom/touchus/benchilauncher/bean/Person;
        //   192: dup            
        //   193: lload_2        
        //   194: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
        //   197: aload           4
        //   199: aload_0        
        //   200: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   203: getfield        com/touchus/benchilauncher/LauncherApplication.phoneNumber:Ljava/lang/String;
        //   206: iload_1        
        //   207: invokestatic    com/touchus/publicutils/utils/TimeUtils.getSimpleCallDate:()Ljava/lang/String;
        //   210: invokespecial   com/touchus/benchilauncher/bean/Person.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
        //   213: astore          4
        //   215: aload_0        
        //   216: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   219: getfield        com/touchus/benchilauncher/LauncherApplication.phoneNumber:Ljava/lang/String;
        //   222: ifnull          233
        //   225: aload           6
        //   227: iconst_0       
        //   228: aload           4
        //   230: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   233: aload           6
        //   235: new             Ljava/io/FileOutputStream;
        //   238: dup            
        //   239: aload           7
        //   241: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   244: invokestatic    com/touchus/benchilauncher/utils/PersonParse.save:(Ljava/util/ArrayList;Ljava/io/OutputStream;)V
        //   247: aload           5
        //   249: ifnull          348
        //   252: aload           5
        //   254: invokevirtual   java/io/InputStream.close:()V
        //   257: return         
        //   258: aload_0        
        //   259: getfield        com/touchus/benchilauncher/service/BluetoothService.app:Lcom/touchus/benchilauncher/LauncherApplication;
        //   262: getfield        com/touchus/benchilauncher/LauncherApplication.phoneName:Ljava/lang/String;
        //   265: astore          4
        //   267: goto            189
        //   270: astore          4
        //   272: aload           6
        //   274: astore          5
        //   276: aload           4
        //   278: astore          6
        //   280: aload           5
        //   282: astore          4
        //   284: aload           6
        //   286: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   289: aload           5
        //   291: ifnull          257
        //   294: aload           5
        //   296: invokevirtual   java/io/InputStream.close:()V
        //   299: return         
        //   300: astore          4
        //   302: aload           4
        //   304: invokevirtual   java/io/IOException.printStackTrace:()V
        //   307: return         
        //   308: astore          6
        //   310: aload           4
        //   312: astore          5
        //   314: aload           6
        //   316: astore          4
        //   318: aload           5
        //   320: ifnull          328
        //   323: aload           5
        //   325: invokevirtual   java/io/InputStream.close:()V
        //   328: aload           4
        //   330: athrow         
        //   331: astore          5
        //   333: aload           5
        //   335: invokevirtual   java/io/IOException.printStackTrace:()V
        //   338: goto            328
        //   341: astore          4
        //   343: aload           4
        //   345: invokevirtual   java/io/IOException.printStackTrace:()V
        //   348: return         
        //   349: astore          4
        //   351: goto            318
        //   354: astore          6
        //   356: goto            280
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  10     54     270    280    Ljava/lang/Exception;
        //  10     54     308    318    Any
        //  58     66     270    280    Ljava/lang/Exception;
        //  58     66     308    318    Any
        //  70     76     270    280    Ljava/lang/Exception;
        //  70     76     308    318    Any
        //  80     91     270    280    Ljava/lang/Exception;
        //  80     91     308    318    Any
        //  91     116    354    359    Ljava/lang/Exception;
        //  91     116    349    354    Any
        //  116    189    354    359    Ljava/lang/Exception;
        //  116    189    349    354    Any
        //  189    233    354    359    Ljava/lang/Exception;
        //  189    233    349    354    Any
        //  233    247    354    359    Ljava/lang/Exception;
        //  233    247    349    354    Any
        //  252    257    341    348    Ljava/io/IOException;
        //  258    267    354    359    Ljava/lang/Exception;
        //  258    267    349    354    Any
        //  284    289    308    318    Any
        //  294    299    300    308    Ljava/io/IOException;
        //  323    328    331    341    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.util.Collections$1.remove(Unknown Source)
        //     at java.util.AbstractCollection.removeAll(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2968)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void stopBTMusic() {
        this.blueMusicFocus = false;
        this.stopMusic();
        this.ipause = false;
        LauncherApplication.iPlayingAuto = false;
        this.unrequestMusicAudioFocus();
    }
    
    public void stopMusic() {
        try {
            this.bluetooth.stopPlay();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void switchDevice() {
        Label_0054: {
            if (!this.iSoundInPhone) {
                break Label_0054;
            }
            boolean iSoundInPhone = false;
            while (true) {
                this.iSoundInPhone = iSoundInPhone;
                BluetoothService.logger.debug("FloatSystemCallDialog iSoundInPhone=" + this.iSoundInPhone);
                try {
                    this.bluetooth.switchDevice(this.iSoundInPhone);
                    return;
                    iSoundInPhone = true;
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    public void tryToDownloadPhoneBook() {
        try {
            this.bluetooth.tryToDownloadPhoneBook();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void unrequestAudioFocus() {
        BluetoothService.logger.debug("mAudioFocusListener btservice unrequestAudioFocus");
        this.mAudioManager.abandonAudioFocus(this.mAudioFocusListener);
    }
    
    public void unrequestMusicAudioFocus() {
        BluetoothService.logger.debug("mAudioFocusListener btservice unrequestMusicAudioFocus");
        this.mAudioManager.abandonAudioFocus(this.mBlueMusicFocusListener);
        final LauncherApplication app = this.app;
        this.app.getClass();
        app.setTypeMute(200, true);
    }
    
    class BluetoothHandler implements IBluetoothEventHandler
    {
        @Override
        public void onAnswerPhone(final AnswerPhoneResult answerPhoneResult) {
        }
        
        @Override
        public void onCallOut(final CallOutResult callOutResult) {
        }
        
        @Override
        public void onConnectedDevice(final ConnectedDeviceProtocol connectedDeviceProtocol) {
            if (!BluetoothService.currentEquipAddress.equals(connectedDeviceProtocol.getDeviceAddress())) {
                BluetoothService.this.currentEquipName = "";
            }
            BluetoothService.currentEquipAddress = connectedDeviceProtocol.getDeviceAddress();
            BluetoothService.this.currentEquipName = connectedDeviceProtocol.getDeviceName();
            BluetoothService.logger.debug("onConnectedDevice: " + BluetoothService.currentEquipAddress);
            if (!BluetoothService.equipList.isEmpty() && TextUtils.isEmpty((CharSequence)BluetoothService.this.currentEquipName)) {
                for (int i = 0; i < BluetoothService.equipList.size(); ++i) {
                    final Equip equip = BluetoothService.equipList.get(i);
                    if (BluetoothService.currentEquipAddress.equals(equip.getAddress())) {
                        BluetoothService.this.currentEquipName = equip.getName();
                        break;
                    }
                }
            }
            BluetoothService.this.putDeviceInfo(BluetoothService.this.currentEquipName, BluetoothService.currentEquipAddress);
            BluetoothService.this.app.sendMessage(1406, null);
            BluetoothService.this.CreateFile(String.valueOf(BluetoothService.currentEquipAddress) + "_Per.xml");
            BluetoothService.this.CreateFile(String.valueOf(BluetoothService.currentEquipAddress) + "_His.xml");
            BluetoothService.this.CreatePerFile(BluetoothService.currentEquipAddress);
            if (!BluetoothService.this.app.iNeedToChangeLocalContacts(BluetoothService.currentEquipAddress)) {
                BluetoothService.this.app.setLastDeviceName(BluetoothService.currentEquipAddress);
                BluetoothService.this.sendBookchanged();
            }
            BluetoothService.logger.debug("onConnectedDevice: " + BluetoothService.currentEquipAddress);
        }
        
        @Override
        public void onDeviceName(final DeviceNameProtocol deviceNameProtocol) {
            BluetoothService.logger.debug("device : " + deviceNameProtocol.getDeviceName());
        }
        
        @Override
        public void onDeviceRemoved(final DeviceRemovedProtocol deviceRemovedProtocol) {
        }
        
        @Override
        public void onFinishDownloadPhoneBook() {
            BluetoothService.logger.debug("onFinishDownloadPhoneBook " + BluetoothService.this.bookList.size());
            while (true) {
                try {
                    Collections.sort(BluetoothService.this.bookList, new PinyinComparator());
                    PersonParse.save(BluetoothService.this.bookList, new FileOutputStream(new File(String.valueOf(BluetoothService.this.Apppath) + "/" + BluetoothService.currentEquipAddress + "_Per.xml")));
                    BluetoothService.this.downloadflag = 2;
                    if (BluetoothService.this.downloadType == 0) {
                        BluetoothService.access$18(BluetoothService.this, 0);
                        BluetoothService.this.sendBookchanged();
                        BluetoothService.this.app.sendMessage(1409, null);
                        BluetoothService.access$7(BluetoothService.this, System.currentTimeMillis());
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    continue;
                }
                break;
            }
        }
        
        @Override
        public void onHangUpPhone(final HangUpPhoneResult hangUpPhoneResult) {
            BluetoothService.logger.debug("bluetoothlog onHangUpPhone----");
            BluetoothService.this.unrequestAudioFocus();
            BluetoothService.this.isAfterInOrOutCall = false;
            BluetoothService.this.app.sendMessage(1416, null);
        }
        
        @Override
        public void onIncomingCall(final IncomingCallProtocol incomingCallProtocol) {
            BluetoothService.logger.debug("FloatSystemCallDialog onIncomingCall arg0: " + incomingCallProtocol.getPhone() + " : ");
            if (BluetoothService.this.app.iIsScreenClose.get()) {
                BluetoothService.this.app.closeOrWakeupScreen(false);
            }
            if (System.currentTimeMillis() - BluetoothService.this.lastCloseCallingViewTime >= 5000L) {
                BluetoothService.bluetoothStatus = EPhoneStatus.INCOMING_CALL;
                FloatSystemCallDialog.getInstance().setShowStatus(FloatSystemCallDialog.FloatShowST.INCOMING);
                BluetoothService.this.app.phoneNumber = incomingCallProtocol.getPhone();
                if (TextUtils.isEmpty((CharSequence)BluetoothService.this.app.phoneNumber)) {
                    BluetoothService.this.app.phoneNumber = "";
                }
                BluetoothService.this.app.phoneName = BluetoothService.this.getbookName(BluetoothService.this.app.phoneNumber);
                BluetoothService.access$1(BluetoothService.this, false);
                BluetoothService.currentCallingType = 1412;
                BluetoothService.iIsCallingInButNotalkingState = true;
                BluetoothService.this.isAfterInOrOutCall = true;
                BluetoothService.this.app.sendMessage(1415, null);
                BluetoothService.this.sendVoiceAssistantStartEvent();
                if (BluetoothService.this.app.service.iIsInRecorder || BluetoothService.this.app.service.iIsInOriginal || BluetoothService.this.app.service.iIsBTConnect) {
                    BluetoothService.this.app.interAndroidView();
                    BluetoothService.this.app.service.iIsInRecorder = false;
                    BluetoothService.this.app.service.iIsInOriginal = false;
                    BluetoothService.this.app.service.iIsBTConnect = false;
                }
            }
        }
        
        @Override
        public void onMediaInfo(final MediaInfoProtocol mediaInfoProtocol) {
            LauncherApplication.isBlueConnectState = true;
            try {
                BluetoothService.this.music = mediaInfoProtocol.getTitle();
                BluetoothService.this.singer = mediaInfoProtocol.getArtist();
                if (!TextUtils.isEmpty((CharSequence)BluetoothService.this.singer)) {
                    BluetoothService.this.music = String.valueOf(BluetoothService.this.music) + " - " + BluetoothService.this.singer;
                }
                BluetoothService.this.app.sendMessage(1421, null);
                BluetoothService.logger.debug("bluetoothprotocal onMediaInfo" + BluetoothService.this.music + "singer::" + BluetoothService.this.singer);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        @Override
        public void onMediaPlayStatus(final MediaPlayStatusProtocol mediaPlayStatusProtocol) {
            BluetoothService.logger.debug("bluetoothprotocal MediaPlayStatus" + mediaPlayStatusProtocol.getMediaPlayStatus());
            if (mediaPlayStatusProtocol.getMediaPlayStatus() == EMediaPlayStatus.PLAYING) {
                LauncherApplication.isBlueConnectState = true;
                BluetoothService.mediaPlayState = 1418;
                BluetoothService.this.readMediaInfo();
            }
            else if (mediaPlayStatusProtocol.getMediaPlayStatus() == EMediaPlayStatus.PAUSE) {
                BluetoothService.mediaPlayState = 1419;
            }
            else {
                BluetoothService.this.music = " ";
                BluetoothService.this.singer = " ";
                BluetoothService.mediaPlayState = 1420;
            }
            BluetoothService.this.app.sendMessage(BluetoothService.mediaPlayState, null);
        }
        
        @Override
        public void onMediaStatus(final MediaStatusProtocol mediaStatusProtocol) {
        }
        
        @Override
        public void onPairedDevice(final String s, final String s2) {
        }
        
        @Override
        public void onPairingList(final PairingListProtocol pairingListProtocol) {
            BluetoothService.logger.debug("onPairingList " + pairingListProtocol.getUnits().size());
            BluetoothService.equipList.clear();
            final List<PairingListUnitProtocol> units = pairingListProtocol.getUnits();
            int index = 0;
            while (true) {
                Label_0090: {
                    if (index < units.size()) {
                        break Label_0090;
                    }
                    Label_0152: {
                        if (!BluetoothService.equipList.isEmpty()) {
                            break Label_0152;
                        }
                        BluetoothService.logger.debug("#############  equipList.isEmpty()");
                        try {
                            Thread.sleep(2000L);
                            BluetoothService.this.readPairingList();
                            return;
                            final Equip equip = new Equip();
                            equip.setIndex(index);
                            equip.setName(units.get(index).getDeviceName());
                            equip.setAddress(units.get(index).getDeviceAddress());
                            BluetoothService.equipList.add(equip);
                            ++index;
                            continue;
                            BluetoothService.logger.debug("addr : " + BluetoothService.equipList.get(0).getAddress());
                        }
                        catch (Exception ex) {}
                    }
                }
            }
        }
        
        @Override
        public void onPairingModeEnd() {
            BluetoothService.this.onPairingResult = false;
            BluetoothService.this.app.sendMessage(1402, null);
        }
        
        @Override
        public void onPairingModeResult(final EnterPairingModeResult enterPairingModeResult) {
            BluetoothService.this.onPairingResult = enterPairingModeResult.isSuccess();
            BluetoothService.logger.debug("onPairingModeResult " + BluetoothService.this.onPairingResult);
            BluetoothService.this.app.sendMessage(1401, null);
        }
        
        @Override
        public void onPhoneBook(final String name, String upperCase) {
            if (BluetoothService.this.booklistIndex < 1) {
                BluetoothService.this.bookList.clear();
                BluetoothService.this.app.sendMessage(1407, null);
            }
            BluetoothService.this.downloadflag = 1;
            final Person person = new Person();
            person.setId(String.valueOf(BluetoothService.this.booklistIndex));
            person.setName(name);
            person.setFlag(0);
            person.setPhone(upperCase.replace(" ", "").replace("-", ""));
            upperCase = Parser.getAllPinyin(name).toUpperCase();
            if ("\u6211\u7684\u7f16\u53f7".endsWith(name)) {
                new StringBuilder("0000").append(upperCase).toString();
                return;
            }
            person.setRemark(upperCase);
            final BluetoothService this$0 = BluetoothService.this;
            BluetoothService.access$18(this$0, this$0.booklistIndex + 1);
            BluetoothService.this.bookList.add(person);
        }
        
        @Override
        public void onPhoneBookCtrlStatus(final PhoneBookCtrlStatusProtocol phoneBookCtrlStatusProtocol) {
            if (phoneBookCtrlStatusProtocol.getPhoneBookCtrlStatus() == EPhoneBookCtrlStatus.CONNECTED && System.currentTimeMillis() - BluetoothService.this.lastPhoneBookDownloadTime > 10000L) {
                BluetoothService.this.downloadflag = 1;
                if (BluetoothService.this.downloadType == 0) {
                    BluetoothService.this.downloadPhoneBook();
                }
                else {
                    BluetoothService.this.downloadRecordList();
                }
                BluetoothService.this.app.sendMessage(1407, null);
            }
            else if (phoneBookCtrlStatusProtocol.getPhoneBookCtrlStatus() == EPhoneBookCtrlStatus.UNCONNECT) {
                BluetoothService.this.downloadflag = -1;
                BluetoothService.this.app.sendMessage(1408, null);
            }
        }
        
        @Override
        public void onPhoneBookList(final PhoneBookListProtocol phoneBookListProtocol) {
        Label_0416_Outer:
            while (true) {
                BluetoothService.access$7(BluetoothService.this, System.currentTimeMillis());
                List<VcardUnit> list;
                ArrayList<Object> list2;
                File file;
                int n;
                int size;
                int n2 = 0;
                Person person;
                String upperCase;
                Label_0416:Label_0211_Outer:
                while (true) {
                    Label_0429: {
                        while (true) {
                            while (true) {
                                while (true) {
                                    try {
                                        list = new LibVcard(phoneBookListProtocol.getPlayload()).getList();
                                        list2 = new ArrayList<Object>();
                                        if (BluetoothService.this.downloadType == 0) {
                                            file = new File(String.valueOf(BluetoothService.this.Apppath) + "/" + BluetoothService.currentEquipAddress + "_Per.xml");
                                            if (!file.exists()) {
                                                file.createNewFile();
                                            }
                                            list2.clear();
                                            n = 0;
                                            if (n < list.size()) {
                                                size = list.get(n).getNumbers().size();
                                                n2 = 0;
                                                break Label_0416;
                                            }
                                            Collections.sort(list2, (Comparator<? super Object>)new PinyinComparator());
                                            PersonParse.save((ArrayList<Person>)list2, new FileOutputStream(file));
                                        }
                                        BluetoothService.this.downloadflag = 2;
                                        BluetoothService.this.app.sendMessage(1409, null);
                                        BluetoothService.this.sendBookchanged();
                                        BluetoothService.access$7(BluetoothService.this, System.currentTimeMillis());
                                        return;
                                        Label_0365: {
                                            person.setRemark(upperCase);
                                        }
                                        list2.add(person);
                                        break Label_0429;
                                        person = new Person();
                                        person.setId(String.valueOf(n2) + "0000" + n);
                                        person.setName(list.get(n).getName());
                                        person.setFlag(0);
                                        person.setPhone(list.get(n).getNumbers().get(n2));
                                        upperCase = Parser.getAllPinyin(list.get(n).getName()).toUpperCase();
                                        // iftrue(Label_0365:, !"\u6211\u7684\u7f16\u53f7".endsWith((VcardUnit)list.get(n).getName()))
                                        new StringBuilder("0000").append(upperCase).toString();
                                        break Label_0429;
                                    }
                                    catch (Exception ex) {
                                        ex.printStackTrace();
                                        BluetoothService.logger.debug("parse error " + ex);
                                        continue Label_0211_Outer;
                                    }
                                    break;
                                }
                                if (n2 >= size) {
                                    ++n;
                                    continue Label_0416_Outer;
                                }
                                break;
                            }
                            continue;
                        }
                    }
                    ++n2;
                    continue Label_0416;
                }
            }
        }
        
        @Override
        public void onPhoneCallingOut(final CallingOutProtocol callingOutProtocol) {
            BluetoothService.bluetoothStatus = EPhoneStatus.CALLING_OUT;
            BluetoothService.logger.debug("FloatSystemCallDialog  onPhoneCallingOut...." + callingOutProtocol.getPhoneNumber());
            BluetoothService.this.app.phoneNumber = callingOutProtocol.getPhoneNumber();
            FloatSystemCallDialog.getInstance().setShowStatus(FloatSystemCallDialog.FloatShowST.OUTCALLING);
            BluetoothService.currentCallingType = 1413;
            BluetoothService.this.isAfterInOrOutCall = true;
            if (!TextUtils.isEmpty((CharSequence)BluetoothService.this.app.phoneNumber)) {
                BluetoothService.this.app.sendMessage(1415, null);
                BluetoothService.this.app.phoneName = BluetoothService.this.getbookName(BluetoothService.this.app.phoneNumber);
                BluetoothService.this.setHistoryList(1413);
            }
            BluetoothService.this.app.mHandler.obtainMessage(1422).sendToTarget();
            BluetoothService.this.requestAudioFocus();
            if (BluetoothService.this.app.service.iIsInRecorder || BluetoothService.this.app.service.iIsInOriginal || BluetoothService.this.app.service.iIsBTConnect) {
                BluetoothService.this.app.interAndroidView();
                BluetoothService.this.app.service.iIsInRecorder = false;
                BluetoothService.this.app.service.iIsInOriginal = false;
                BluetoothService.this.app.service.iIsBTConnect = false;
            }
        }
        
        @Override
        public void onPhoneStatus(final PhoneStatusProtocol phoneStatusProtocol) {
            if (phoneStatusProtocol != null && phoneStatusProtocol.getPhoneStatus() != null) {
                BluetoothService.bluetoothStatus = phoneStatusProtocol.getPhoneStatus();
                BluetoothService.logger.debug("onPhoneStatus: " + BluetoothService.bluetoothStatus);
                if (phoneStatusProtocol.getPhoneStatus() == EPhoneStatus.CALLING_OUT) {
                    LauncherApplication.isBlueConnectState = true;
                    BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: CALLING_OUT");
                    BluetoothService.this.accFlag = true;
                    BluetoothService.this.showDialogflag = true;
                    BluetoothService.currentCallingType = 1413;
                    BluetoothService.iIsCallingInButNotalkingState = true;
                    return;
                }
                if (phoneStatusProtocol.getPhoneStatus() == EPhoneStatus.CONNECTED) {
                    BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: CONNECTED");
                    LauncherApplication.isBlueConnectState = true;
                    BluetoothService.this.showDialogflag = false;
                    if (BluetoothService.talkingflag) {
                        BluetoothService.talkingflag = false;
                        BluetoothService.this.calltime = 0;
                    }
                    if (BluetoothService.iIsCallingInButNotalkingState) {
                        if (BluetoothService.currentCallingType != 1413 && !BluetoothService.this.histalkflag && !TextUtils.isEmpty((CharSequence)BluetoothService.this.app.phoneNumber)) {
                            BluetoothService.this.setHistoryList(1414);
                        }
                        BluetoothService.iIsCallingInButNotalkingState = false;
                        BluetoothService.this.histalkflag = false;
                        BluetoothService.currentCallingType = 0;
                        BluetoothService.logger.debug("----------------app.phoneNumber = " + BluetoothService.this.app.phoneNumber + ",app.phoneName =" + BluetoothService.this.app.phoneName);
                        BluetoothService.this.app.phoneNumber = "";
                        BluetoothService.this.app.phoneName = "";
                    }
                    BluetoothService.this.sendVoiceAssistantCallEndEvent();
                    BluetoothService.currentEquipAddress = BluetoothService.this.shared_device.getString("addr", "");
                    BluetoothService.this.currentEquipName = BluetoothService.this.shared_device.getString("name", "");
                    BluetoothService.this.app.sendMessage(6002, null);
                    return;
                }
                if (phoneStatusProtocol.getPhoneStatus() == EPhoneStatus.UNCONNECT) {
                    BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: UNCONNECT");
                    LauncherApplication.isBlueConnectState = false;
                    BluetoothService.this.app.isCalling = false;
                    BluetoothService.this.isAfterInOrOutCall = false;
                    BluetoothService.this.showDialogflag = false;
                    BluetoothService.this.lastindex = 0;
                    BluetoothService.this.lasthisindex = 0;
                    BluetoothService.this.downloadflag = -1;
                    BluetoothService.talkingflag = false;
                    BluetoothService.iIsCallingInButNotalkingState = false;
                    BluetoothService.this.music = " ";
                    BluetoothService.this.singer = " ";
                    BluetoothService.this.cutdownCurrentCalling();
                    BluetoothService.this.app.sendMessage(1421, null);
                    BluetoothService.this.app.sendMessage(6002, null);
                    return;
                }
                if (phoneStatusProtocol.getPhoneStatus() == EPhoneStatus.TALKING) {
                    LauncherApplication.isBlueConnectState = true;
                    BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: TALKING");
                    Mainboard.getInstance().setAllHornSoundValue(SysConst.basicNum, SysConst.callnum[BluetoothService.this.mSpUtilK.getInt(SysConst.callVoice, 5)], 0, 0, 0);
                    if (!BluetoothService.this.isAfterInOrOutCall) {
                        BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: NoCallTALKING");
                        return;
                    }
                    FloatSystemCallDialog.getInstance().setShowStatus(FloatSystemCallDialog.FloatShowST.TALKING);
                    BluetoothService.iIsCallingInButNotalkingState = false;
                    BluetoothService.this.showDialogflag = true;
                    BluetoothService.this.histalkflag = true;
                    if (!BluetoothService.talkingflag) {
                        BluetoothService.this.calltime = 0;
                        BluetoothService.talkingflag = true;
                        BluetoothService.this.app.sendMessage(1411, null);
                        BluetoothService.access$11(BluetoothService.this, new Thread(new TimeThread((TimeThread)null)));
                        BluetoothService.this.timeThread.start();
                    }
                    BluetoothService.this.requestAudioFocus();
                    BluetoothService.this.sendVoiceAssistantInTalkingEvent();
                    BluetoothService.logger.debug("bluetoothlog  TALKING...........");
                    if (BluetoothService.currentCallingType == 1412) {
                        BluetoothService.this.setHistoryList(1412);
                    }
                    BluetoothService.currentCallingType = 1411;
                    if (!BluetoothService.this.app.topIsBlueMainFragment() && FloatSystemCallDialog.getInstance().isDestory()) {
                        BluetoothService.this.app.mHandler.obtainMessage(1422).sendToTarget();
                    }
                }
                else {
                    if (phoneStatusProtocol.getPhoneStatus() == EPhoneStatus.CONNECTING) {
                        BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: CONNECTING");
                        BluetoothService.talkingflag = false;
                        BluetoothService.iIsCallingInButNotalkingState = false;
                        return;
                    }
                    if (phoneStatusProtocol.getPhoneStatus() == EPhoneStatus.INCOMING_CALL) {
                        LauncherApplication.isBlueConnectState = true;
                        BluetoothService.logger.debug("FloatSystemCallDialog onPhoneStatus: INCOMING_CALL");
                        BluetoothService.iIsCallingInButNotalkingState = true;
                        BluetoothService.currentCallingType = 1412;
                        BluetoothService.this.showDialogflag = true;
                        BluetoothService.this.accFlag = true;
                    }
                }
            }
        }
        
        @Override
        public void onSetPlayStatus(final SetPlayStatusProtocol setPlayStatusProtocol) {
        }
        
        @Override
        public void onVersion(final VersionProtocol versionProtocol) {
        }
        
        @Override
        public void ondeviceSwitchedProtocol(final DeviceSwitchedProtocol deviceSwitchedProtocol) {
            BluetoothService.logger.debug("FloatSystemCallDialog  [sound] ondeviceSwitchedProtocol: " + deviceSwitchedProtocol.getConnectedDevice());
            if (deviceSwitchedProtocol.getConnectedDevice() == EConnectedDevice.LOCAL) {
                BluetoothService.this.iSoundInPhone = false;
            }
            else {
                BluetoothService.this.iSoundInPhone = true;
            }
            BluetoothService.logger.debug("bluetooth ondeviceSwitchedProtocol requestAudioFocus isAfterInOrOutCall : " + BluetoothService.this.isAfterInOrOutCall);
        }
    }
    
    public class PlayerBinder extends Binder
    {
        public BluetoothService getService() {
            return BluetoothService.this;
        }
    }
    
    private class TimeThread implements Runnable
    {
        @Override
        public void run() {
            while (BluetoothService.talkingflag) {
                try {
                    final BluetoothService this$0 = BluetoothService.this;
                    ++this$0.calltime;
                    BluetoothService.this.app.sendMessage(1417, null);
                    Thread.sleep(1000L);
                }
                catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
