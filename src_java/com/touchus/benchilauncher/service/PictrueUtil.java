package com.touchus.benchilauncher.service;

import android.content.*;
import android.database.*;
import com.touchus.publicutils.bean.*;
import android.provider.*;
import android.os.*;
import java.util.*;

public class PictrueUtil
{
    private static Context context;
    private static Cursor cursor;
    private static List<MediaBean> picList;
    
    public PictrueUtil(final Context context) {
        PictrueUtil.context = context;
    }
    
    private void getFilesBySystem() {
        if (MediaStore$Images$Media.EXTERNAL_CONTENT_URI != null && Environment.getExternalStorageState().equals("mounted")) {
            PictrueUtil.cursor = PictrueUtil.context.getContentResolver().query(MediaStore$Images$Media.EXTERNAL_CONTENT_URI, (String[])null, (String)null, (String[])null, (String)null);
            while (PictrueUtil.cursor.moveToNext()) {
                final long long1 = PictrueUtil.cursor.getLong(PictrueUtil.cursor.getColumnIndex("_id"));
                final String string = PictrueUtil.cursor.getString(PictrueUtil.cursor.getColumnIndex("_display_name"));
                PictrueUtil.cursor.getString(PictrueUtil.cursor.getColumnIndex("description"));
                final String string2 = PictrueUtil.cursor.getString(PictrueUtil.cursor.getColumnIndex("_data"));
                final MediaBean mediaBean = new MediaBean();
                mediaBean.setId(long1);
                mediaBean.setTitle(string);
                mediaBean.setData(string2);
                PictrueUtil.picList.add(mediaBean);
            }
            PictrueUtil.cursor.close();
        }
    }
    
    public List<MediaBean> getPicList() {
        PictrueUtil.picList = new ArrayList<MediaBean>();
        this.getFilesBySystem();
        return PictrueUtil.picList;
    }
}
