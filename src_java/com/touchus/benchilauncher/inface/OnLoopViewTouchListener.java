package com.touchus.benchilauncher.inface;

import android.view.*;

public interface OnLoopViewTouchListener
{
    void onTouch(final MotionEvent p0);
}
