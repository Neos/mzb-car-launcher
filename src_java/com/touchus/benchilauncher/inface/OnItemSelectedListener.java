package com.touchus.benchilauncher.inface;

import android.view.*;

public interface OnItemSelectedListener
{
    void selected(final int p0, final View p1);
}
