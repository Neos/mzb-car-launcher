package com.touchus.benchilauncher.inface;

import android.view.*;

public interface OnItemClickListener
{
    void onItemClick(final int p0, final View p1);
}
