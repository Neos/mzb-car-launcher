package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.touchus.benchilauncher.views.*;
import com.touchus.benchilauncher.service.*;
import android.text.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class FragmentMusicLanYa extends BaseFragment
{
    private ImageView[] arr;
    private int count;
    private boolean isNoXuanZhuan;
    private boolean isPlaying;
    private LauncherApplication mApp;
    private ImageView mBofang;
    private Launcher mContext;
    private byte mIDRIVERENUM;
    private TextView mMTitle;
    public MusicLanyaHandler mMusicHandler;
    private View mRootView;
    private ImageView mShang;
    private ImageView mXiayiqu;
    
    public FragmentMusicLanYa() {
        this.isPlaying = true;
        this.arr = new ImageView[3];
        this.count = 0;
        this.mMusicHandler = new MusicLanyaHandler(this);
        this.isNoXuanZhuan = true;
    }
    
    static /* synthetic */ void access$0(final FragmentMusicLanYa fragmentMusicLanYa, final boolean isNoXuanZhuan) {
        fragmentMusicLanYa.isNoXuanZhuan = isNoXuanZhuan;
    }
    
    static /* synthetic */ void access$1(final FragmentMusicLanYa fragmentMusicLanYa, final int count) {
        fragmentMusicLanYa.count = count;
    }
    
    static /* synthetic */ void access$7(final FragmentMusicLanYa fragmentMusicLanYa, final boolean isPlaying) {
        fragmentMusicLanYa.isPlaying = isPlaying;
    }
    
    private void initListener() {
        this.mXiayiqu.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicLanYa.access$0(FragmentMusicLanYa.this, true);
                FragmentMusicLanYa.access$1(FragmentMusicLanYa.this, 2);
                FragmentMusicLanYa.this.seclectTrue(2);
                FragmentMusicLanYa.this.setImageSeclect(2);
                FragmentMusicLanYa.this.mApp.btservice.playNext();
            }
        });
        this.mShang.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicLanYa.access$0(FragmentMusicLanYa.this, true);
                FragmentMusicLanYa.access$1(FragmentMusicLanYa.this, 0);
                FragmentMusicLanYa.this.seclectTrue(0);
                FragmentMusicLanYa.this.setImageSeclect(0);
                FragmentMusicLanYa.this.mApp.btservice.playPrev();
            }
        });
        this.mBofang.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicLanYa.access$1(FragmentMusicLanYa.this, 1);
                FragmentMusicLanYa.this.seclectTrue(1);
                if (!FragmentMusicLanYa.this.isPlaying) {
                    FragmentMusicLanYa.this.mBofang.setImageResource(2130837526);
                    FragmentMusicLanYa.access$7(FragmentMusicLanYa.this, !FragmentMusicLanYa.this.isPlaying);
                    FragmentMusicLanYa.this.mApp.btservice.pausePlaySync(true);
                    FragmentMusicLanYa.this.mApp.btservice.ipause = false;
                    return;
                }
                FragmentMusicLanYa.this.mBofang.setImageResource(2130837506);
                FragmentMusicLanYa.access$7(FragmentMusicLanYa.this, !FragmentMusicLanYa.this.isPlaying);
                FragmentMusicLanYa.this.mApp.btservice.pausePlaySync(false);
                FragmentMusicLanYa.this.mApp.btservice.ipause = true;
            }
        });
    }
    
    private void initView() {
        this.mMTitle = (MyTextView)this.mRootView.findViewById(2131427520);
        this.mShang = (ImageView)this.mRootView.findViewById(2131427517);
        this.mBofang = (ImageView)this.mRootView.findViewById(2131427518);
        this.mXiayiqu = (ImageView)this.mRootView.findViewById(2131427519);
        this.arr[0] = this.mShang;
        this.arr[1] = this.mBofang;
        this.arr[2] = this.mXiayiqu;
    }
    
    private void seclectTrue(final int n) {
        for (int i = 0; i < this.arr.length; ++i) {
            if (this.isNoXuanZhuan) {
                if (n != 1) {
                    this.mBofang.setImageResource(2130837527);
                    this.isPlaying = true;
                }
                else if (this.isPlaying) {
                    this.mBofang.setImageResource(2130837526);
                }
                else {
                    this.mBofang.setImageResource(2130837506);
                }
            }
            else if (n != 1) {
                if (this.isPlaying) {
                    this.mBofang.setImageResource(2130837527);
                }
                else {
                    this.mBofang.setImageResource(2130837507);
                }
            }
            else if (this.isPlaying) {
                this.mBofang.setImageResource(2130837526);
            }
            else {
                this.mBofang.setImageResource(2130837506);
            }
            if (n == i) {
                this.arr[i].setSelected(true);
            }
            else {
                this.arr[i].setSelected(false);
            }
        }
    }
    
    private void setImageSeclect(final int n) {
        if (n == 0) {
            this.mShang.setImageResource(2130837518);
            this.mMusicHandler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    FragmentMusicLanYa.this.mShang.setImageResource(2130837519);
                }
            }, 500L);
        }
        else if (n == 2) {
            this.mXiayiqu.setImageResource(2130837522);
            this.mMusicHandler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    FragmentMusicLanYa.this.mXiayiqu.setImageResource(2130837523);
                }
            }, 500L);
        }
    }
    
    private void settingPlayName() {
        if (!LauncherApplication.isBlueConnectState) {
            this.mMTitle.setText((CharSequence)this.mContext.getString(2131165246));
            this.mShang.setClickable(false);
            this.mBofang.setClickable(false);
            this.mXiayiqu.setClickable(false);
        }
        else {
            this.mShang.setClickable(true);
            this.mBofang.setClickable(true);
            this.mXiayiqu.setClickable(true);
            if (BluetoothService.mediaPlayState == 1) {
                this.mMTitle.setText((CharSequence)this.mContext.getString(2131165234));
                return;
            }
            if (BluetoothService.mediaPlayState == 1418) {
                if (!TextUtils.isEmpty((CharSequence)this.mApp.btservice.music)) {
                    this.mMTitle.setText((CharSequence)this.mApp.btservice.music);
                }
                this.isPlaying = true;
                this.mApp.btservice.ipause = false;
                if (this.count == 1) {
                    this.mBofang.setImageResource(2130837526);
                    return;
                }
                this.mBofang.setImageResource(2130837527);
            }
            else if (BluetoothService.mediaPlayState == 1419) {
                this.mMTitle.setText((CharSequence)this.mContext.getString(2131165245));
                this.isPlaying = false;
                if (this.count == 1) {
                    this.mBofang.setImageResource(2130837506);
                    return;
                }
                this.mBofang.setImageResource(2130837507);
            }
            else if (BluetoothService.mediaPlayState == 1420) {
                this.mMTitle.setText((CharSequence)this.mContext.getString(2131165234));
                this.isPlaying = false;
                if (this.count == 1) {
                    this.mBofang.setImageResource(2130837506);
                    return;
                }
                this.mBofang.setImageResource(2130837507);
            }
        }
    }
    
    public void handlerMsg(final Message message) {
        if (LauncherApplication.isBlueConnectState) {
            switch (message.what) {
                case 1421: {
                    if (!TextUtils.isEmpty((CharSequence)this.mApp.btservice.music)) {
                        this.mMTitle.setText((CharSequence)this.mApp.btservice.music);
                    }
                }
                case 1418:
                case 1419:
                case 1420: {
                    this.settingPlayName();
                    break;
                }
            }
            if (message.what == 6001) {
                this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                    this.isNoXuanZhuan = false;
                    if (this.count < 2) {
                        ++this.count;
                    }
                    this.seclectTrue(this.count);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    this.isNoXuanZhuan = false;
                    if (this.count > 0) {
                        --this.count;
                    }
                    this.seclectTrue(this.count);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    if (this.count == 0) {
                        this.isPlaying = true;
                        this.setImageSeclect(0);
                        this.mApp.btservice.playPrev();
                    }
                    else if (this.count == 1) {
                        if (!this.isPlaying) {
                            this.mBofang.setImageResource(2130837526);
                            this.isPlaying = !this.isPlaying;
                            this.mApp.btservice.pausePlaySync(true);
                            this.mApp.btservice.ipause = false;
                        }
                        else {
                            this.mBofang.setImageResource(2130837506);
                            this.isPlaying = !this.isPlaying;
                            this.mApp.btservice.pausePlaySync(false);
                            this.mApp.btservice.ipause = true;
                        }
                    }
                    else if (this.count == 2) {
                        this.isPlaying = true;
                        this.setImageSeclect(2);
                        this.mApp.btservice.playNext();
                    }
                    this.seclectTrue(this.count);
                }
            }
        }
    }
    
    @Override
    public boolean onBack() {
        return LauncherApplication.iPlayingAuto = false;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mContext = (Launcher)this.getActivity();
        this.mRootView = layoutInflater.inflate(2130903073, (ViewGroup)null);
        this.mContext = (Launcher)this.getActivity();
        this.mApp = (LauncherApplication)this.mContext.getApplication();
        this.initView();
        this.initListener();
        return this.mRootView;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public void onStart() {
        this.mApp.btservice.requestMusicAudioFocus();
        this.mApp.btservice.readMediaStatus();
        this.mApp.registerHandler(this.mMusicHandler);
        this.settingPlayName();
        if (LauncherApplication.isBlueConnectState && BluetoothService.mediaPlayState == 1418) {
            this.mApp.service.btMusicConnect();
        }
        super.onStart();
    }
    
    @Override
    public void onStop() {
        this.mApp.unregisterHandler(this.mMusicHandler);
        super.onStop();
    }
    
    static class MusicLanyaHandler extends Handler
    {
        private WeakReference<FragmentMusicLanYa> target;
        
        public MusicLanyaHandler(final FragmentMusicLanYa fragmentMusicLanYa) {
            this.target = new WeakReference<FragmentMusicLanYa>(fragmentMusicLanYa);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
