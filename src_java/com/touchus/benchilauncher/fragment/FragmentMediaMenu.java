package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.touchus.benchilauncher.views.*;
import java.util.*;
import android.support.v4.app.*;
import android.content.*;
import com.touchus.benchilauncher.inface.*;
import android.util.*;
import android.view.*;
import com.backaudio.android.driver.*;
import android.os.*;
import java.lang.ref.*;

public class FragmentMediaMenu extends BaseFragment implements View$OnClickListener
{
    private TextView currentPlayInfoTview;
    String[] currentShowData;
    private LauncherApplication mApp;
    Launcher mContext;
    public USBMusicHandler mHandler;
    private ImageView mIvMenu;
    private String mKey;
    private LoopRotarySwitchView mLoopRotarySwitchView;
    private View mRootView;
    Map<String, String[]> rotateData;
    String[] texts1;
    String[] texts2;
    String[] texts3;
    String[] texts4;
    private int width;
    
    public FragmentMediaMenu() {
        this.mKey = "";
        this.currentShowData = null;
        this.rotateData = new HashMap<String, String[]>();
        this.mHandler = new USBMusicHandler(this);
    }
    
    private void handleClick() {
        if (this.getActivity() != null) {
            if (this.mKey.equals(this.getString(2131165249))) {
                this.mContext.changeRightTo(new FragmentMusicOutUSB());
                return;
            }
            if (this.mKey.equals(this.getString(2131165250))) {
                this.mContext.changeRightTo(new FragmentMusicOutUSB());
                return;
            }
            if (this.mKey.equals(this.getString(2131165251))) {
                this.mContext.changeRightTo(new FragmentMusicOutUSB());
                return;
            }
            if (this.mKey.equals(this.getString(2131165248))) {
                this.mContext.changeRightTo(new FragmentMusicLanYa());
            }
        }
    }
    
    private void initData() {
        for (int i = 0; i < 5; ++i) {
            this.mLoopRotarySwitchView.addView(LayoutInflater.from((Context)this.mContext).inflate(2130903087, (ViewGroup)null));
        }
    }
    
    private void initLinstener() {
        this.mIvMenu.setOnClickListener((View$OnClickListener)this);
        this.mLoopRotarySwitchView.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void selected(final int n, final View view) {
                FragmentMediaMenu.this.settingCurrentShowData(n);
            }
        });
        this.mLoopRotarySwitchView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final int n, final View view) {
                FragmentMediaMenu.this.handleClick();
            }
        });
    }
    
    private void initLoopRotarySwitchView() {
        this.mLoopRotarySwitchView.setR(this.width / 5);
        this.mLoopRotarySwitchView.setChildViewText(this.currentShowData);
    }
    
    private void initRotateData() {
        this.texts1 = new String[] { this.mContext.getString(2131165249), this.mContext.getString(2131165250), this.mContext.getString(2131165251), this.mContext.getString(2131165251), this.mContext.getString(2131165248) };
        this.texts2 = new String[] { this.mContext.getString(2131165250), this.mContext.getString(2131165251), this.mContext.getString(2131165248), this.mContext.getString(2131165248), this.mContext.getString(2131165249) };
        this.texts3 = new String[] { this.mContext.getString(2131165251), this.mContext.getString(2131165248), this.mContext.getString(2131165249), this.mContext.getString(2131165249), this.mContext.getString(2131165250) };
        this.texts4 = new String[] { this.mContext.getString(2131165248), this.mContext.getString(2131165249), this.mContext.getString(2131165250), this.mContext.getString(2131165250), this.mContext.getString(2131165251) };
        this.rotateData.put(this.texts1[0], this.texts1);
        this.rotateData.put(this.texts2[0], this.texts2);
        this.rotateData.put(this.texts3[0], this.texts3);
        this.rotateData.put(this.texts4[0], this.texts4);
        this.currentShowData = this.texts1;
    }
    
    private void initView() {
        this.mLoopRotarySwitchView = (LoopRotarySwitchView)this.mRootView.findViewById(2131427510);
        this.currentPlayInfoTview = (TextView)this.mRootView.findViewById(2131427512);
        this.mIvMenu = (ImageView)this.mRootView.findViewById(2131427511);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)this.getActivity().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.width = displayMetrics.widthPixels;
    }
    
    private void selectStateChange(final String s) {
        if (s.equals(this.getString(2131165249))) {
            this.mIvMenu.setImageResource(2130837976);
            LauncherApplication.menuSelectType = 0;
        }
        else {
            if (s.equals(this.getString(2131165250))) {
                this.mIvMenu.setImageResource(2130837696);
                LauncherApplication.menuSelectType = 1;
                return;
            }
            if (s.equals(this.getString(2131165251))) {
                this.mIvMenu.setImageResource(2130837850);
                LauncherApplication.menuSelectType = 2;
                return;
            }
            if (s.equals(this.getString(2131165248))) {
                this.mIvMenu.setImageResource(2130837652);
                LauncherApplication.menuSelectType = 3;
            }
        }
    }
    
    private void settingCurrentShowData(final int n) {
        if (this.getActivity() == null) {
            return;
        }
        this.mKey = this.currentShowData[n];
        final String[] array = this.rotateData.get(this.mKey);
        final String[] array2 = new String[5];
        for (int i = 0; i < array.length; ++i) {
            if (i < n) {
                array2[n - 1 - i] = array[array.length - 1 - i];
            }
            else {
                array2[i] = array[i - n];
            }
        }
        this.currentShowData = array2;
        this.mLoopRotarySwitchView.setChildViewText(array2);
        this.mLoopRotarySwitchView.setTextViewText();
        this.selectStateChange(this.mKey);
    }
    
    public void handlerMsgUSB(final Message message) {
        if (this.getActivity() != null && message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                this.handleClick();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.mLoopRotarySwitchView.setAutoScrollDirection(LoopRotarySwitchView.AutoScrollDirection.left);
                this.mLoopRotarySwitchView.doRotain();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                this.mLoopRotarySwitchView.setAutoScrollDirection(LoopRotarySwitchView.AutoScrollDirection.right);
                this.mLoopRotarySwitchView.doRotain();
            }
        }
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427511) {
            this.handleClick();
        }
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2130903072, (ViewGroup)null);
        this.mContext = (Launcher)this.getActivity();
        this.mApp = (LauncherApplication)this.mContext.getApplication();
        this.initView();
        this.initData();
        this.initRotateData();
        this.initLoopRotarySwitchView();
        this.initLinstener();
        if (this.mApp.musicPlayControl != null && LauncherApplication.iPlaying && LauncherApplication.iPlayingAuto) {
            this.mContext.changeRightTo(new FragmentMusicOutUSB());
        }
        if (this.mApp.musicType == 2 && LauncherApplication.iPlayingAuto) {
            this.mContext.changeRightTo(new FragmentMusicLanYa());
        }
        return this.mRootView;
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    public void onPause() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onPause();
    }
    
    public void onResume() {
        this.mApp.registerHandler(this.mHandler);
        super.onResume();
    }
    
    public void onStart() {
        if (LauncherApplication.menuSelectType == -1) {
            LauncherApplication.menuSelectType = 0;
        }
        if (LauncherApplication.menuSelectType == 0) {
            this.currentShowData = this.texts1;
        }
        else if (LauncherApplication.menuSelectType == 1) {
            this.currentShowData = this.texts2;
        }
        else if (LauncherApplication.menuSelectType == 2) {
            this.currentShowData = this.texts3;
        }
        else if (LauncherApplication.menuSelectType == 3) {
            this.currentShowData = this.texts4;
        }
        this.mLoopRotarySwitchView.setChildViewText(this.currentShowData);
        super.onStart();
    }
    
    static class USBMusicHandler extends Handler
    {
        private WeakReference<FragmentMediaMenu> target;
        
        public USBMusicHandler(final FragmentMediaMenu fragmentMediaMenu) {
            this.target = new WeakReference<FragmentMediaMenu>(fragmentMediaMenu);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
