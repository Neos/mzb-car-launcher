package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import com.backaudio.android.driver.beans.*;
import android.content.*;
import com.touchus.benchilauncher.views.*;
import java.util.*;
import com.touchus.publicutils.bean.*;
import com.touchus.benchilauncher.utils.*;
import android.media.*;
import android.util.*;
import com.touchus.benchilauncher.*;
import com.touchus.publicutils.utils.*;
import android.widget.*;
import com.backaudio.android.driver.*;
import android.os.*;
import java.lang.ref.*;
import android.view.*;
import java.io.*;

public class FragmentVideoPlay extends BaseFragment implements View$OnTouchListener
{
    private static String LAST_VIDEO_PLAY_NAME;
    private static String LAST_VIDEO_PLAY_POSITION;
    private boolean PasueFlag;
    private ImageButton[] arr;
    private Runnable autoSettingHideOrShowRunnable;
    int changeProgress;
    private int countSufaceVieew;
    int curProgress;
    private int currentPlayIndex;
    private int currentPosition;
    private boolean iIsPlaySeekbarShow;
    private boolean iNeedHeartThread;
    private int indexCount;
    private CarBaseInfo info;
    private boolean isBigView;
    private boolean isOpenSound;
    private boolean isPrepare;
    private ViewGroup$LayoutParams lp;
    private LauncherApplication mApp;
    private TextView mBoTime;
    Context mContext;
    private LinearLayout mDibuCaidan;
    private ViewGroup$LayoutParams mDibucandan;
    private ImageButton mFangsuo;
    private FrameLayout mFlsurfaceView;
    private byte mIDRIVERENUM;
    private Launcher mMMainActivity;
    private AudioManager$OnAudioFocusChangeListener mMainAudioFocusListener;
    public MusicLanyaHandler mMusicHandler;
    private LinearLayout mPaomadeng;
    private View mRootView;
    private SeekBar mSeekBar;
    private LinearLayout mSeekBeijing;
    private ImageButton mSound;
    private LinearLayout mToolBar;
    private ImageButton mVedioBofang;
    private ImageButton mVedioCaidan;
    private MyTextView mVedioName;
    private ImageButton mVedioShnag;
    private ImageButton mVedioXia;
    private ImageButton mYoushengdao;
    private TextView mZongTime;
    private ImageButton mZuoshengdao;
    public List<MediaBean> mediaBeans;
    private MediaPlayer mediaPlayer;
    int moveX;
    private SpUtilK share;
    int startX;
    private SurfaceView surfaceView;
    private SurfaceViewCallBack surfaceViewCallBack;
    private Runnable updateRunnable;
    private ImageView vedio_pause;
    
    static {
        FragmentVideoPlay.LAST_VIDEO_PLAY_NAME = "lastVideoPlayName";
        FragmentVideoPlay.LAST_VIDEO_PLAY_POSITION = "lastVideoPlayPosition";
    }
    
    public FragmentVideoPlay() {
        this.currentPosition = -1;
        this.PasueFlag = false;
        this.isOpenSound = false;
        this.isBigView = false;
        this.arr = new ImageButton[7];
        this.iIsPlaySeekbarShow = true;
        this.iNeedHeartThread = false;
        this.autoSettingHideOrShowRunnable = new Runnable() {
            @Override
            public void run() {
                FragmentVideoPlay.this.hidePlayControlBar();
            }
        };
        this.countSufaceVieew = 0;
        this.isPrepare = false;
        this.updateRunnable = new Runnable() {
            @Override
            public void run() {
                if (!FragmentVideoPlay.this.iNeedHeartThread) {
                    return;
                }
                FragmentVideoPlay.this.updateCurrentPlayInfo();
                FragmentVideoPlay.this.mMusicHandler.removeCallbacks(FragmentVideoPlay.this.updateRunnable);
                FragmentVideoPlay.this.mMusicHandler.postDelayed(FragmentVideoPlay.this.updateRunnable, 1000L);
            }
        };
        this.mMusicHandler = new MusicLanyaHandler(this);
        this.mMainAudioFocusListener = (AudioManager$OnAudioFocusChangeListener)new AudioManager$OnAudioFocusChangeListener() {
            public void onAudioFocusChange(final int n) {
                switch (n) {
                    case -2: {
                        if (FragmentVideoPlay.this.mediaPlayer != null) {
                            FragmentVideoPlay.this.mediaPlayer.pause();
                            FragmentVideoPlay.access$5(FragmentVideoPlay.this, true);
                            FragmentVideoPlay.this.vedio_pause.setVisibility(0);
                            return;
                        }
                        break;
                    }
                    case -3: {
                        if (SysConst.isBT() || FragmentVideoPlay.this.mApp.ismix) {
                            FragmentVideoPlay.this.mediaPlayer.setVolume(SysConst.musicDecVolume, SysConst.musicDecVolume);
                            return;
                        }
                        if (FragmentVideoPlay.this.mediaPlayer != null) {
                            FragmentVideoPlay.this.mediaPlayer.pause();
                            FragmentVideoPlay.access$5(FragmentVideoPlay.this, true);
                            FragmentVideoPlay.this.vedio_pause.setVisibility(0);
                            return;
                        }
                        break;
                    }
                    case 1: {
                        FragmentVideoPlay.this.mApp.musicType = 3;
                        FragmentVideoPlay.this.mediaPlayer.setVolume(SysConst.musicNorVolume, SysConst.musicNorVolume);
                        if (FragmentVideoPlay.this.mediaPlayer != null && FragmentVideoPlay.this.PasueFlag) {
                            FragmentVideoPlay.this.mediaPlayer.start();
                            FragmentVideoPlay.this.vedio_pause.setVisibility(8);
                            FragmentVideoPlay.access$5(FragmentVideoPlay.this, false);
                            return;
                        }
                        break;
                    }
                }
            }
        };
    }
    
    private void abandonAudioFocus() {
        if (this.getActivity() == null) {
            return;
        }
        ((AudioManager)this.getActivity().getSystemService("audio")).abandonAudioFocus(this.mMainAudioFocusListener);
    }
    
    static /* synthetic */ void access$13(final FragmentVideoPlay fragmentVideoPlay, final int currentPosition) {
        fragmentVideoPlay.currentPosition = currentPosition;
    }
    
    static /* synthetic */ void access$17(final FragmentVideoPlay fragmentVideoPlay, final boolean isPrepare) {
        fragmentVideoPlay.isPrepare = isPrepare;
    }
    
    static /* synthetic */ void access$19(final FragmentVideoPlay fragmentVideoPlay, final int indexCount) {
        fragmentVideoPlay.indexCount = indexCount;
    }
    
    static /* synthetic */ void access$23(final FragmentVideoPlay fragmentVideoPlay, final boolean isBigView) {
        fragmentVideoPlay.isBigView = isBigView;
    }
    
    static /* synthetic */ void access$5(final FragmentVideoPlay fragmentVideoPlay, final boolean pasueFlag) {
        fragmentVideoPlay.PasueFlag = pasueFlag;
    }
    
    private void hidePlayControlBar() {
        this.mFangsuo.setVisibility(8);
        this.mDibuCaidan.setVisibility(8);
        this.mToolBar.setVisibility(8);
        this.mSeekBeijing.setVisibility(8);
        this.mPaomadeng.setVisibility(8);
        if (this.isBigView) {
            this.mMMainActivity.hideButtom();
            this.mMMainActivity.hideTop();
        }
        else {
            this.mMMainActivity.showButtom();
            this.mMMainActivity.showTop();
        }
        this.iIsPlaySeekbarShow = false;
    }
    
    private void initListener() {
        this.vedio_pause.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.this.mediaPlayer.start();
                FragmentVideoPlay.this.vedio_pause.setVisibility(8);
                FragmentVideoPlay.this.mVedioBofang.setImageResource(2130837894);
                FragmentVideoPlay.access$5(FragmentVideoPlay.this, !FragmentVideoPlay.this.PasueFlag);
            }
        });
        this.mSound.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 0);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mZuoshengdao.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 1);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mYoushengdao.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 5);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mVedioCaidan.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 6);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mFlsurfaceView.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
            }
        });
        this.surfaceView.setOnTouchListener((View$OnTouchListener)this);
        this.mFangsuo.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                if (!FragmentVideoPlay.this.isBigView) {
                    FragmentVideoPlay.access$23(FragmentVideoPlay.this, true);
                    FragmentVideoPlay.this.fangdaVedio();
                    return;
                }
                FragmentVideoPlay.access$23(FragmentVideoPlay.this, false);
                FragmentVideoPlay.this.suoXiaoVedio();
            }
        });
        this.mVedioShnag.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 2);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mVedioXia.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 4);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mVedioBofang.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentVideoPlay.access$19(FragmentVideoPlay.this, 3);
                FragmentVideoPlay.this.press(FragmentVideoPlay.this.indexCount);
            }
        });
        this.mSeekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener() {
            public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
            }
            
            public void onStartTrackingTouch(final SeekBar seekBar) {
            }
            
            public void onStopTrackingTouch(final SeekBar seekBar) {
                if (FragmentVideoPlay.this.mediaPlayer != null) {
                    FragmentVideoPlay.access$13(FragmentVideoPlay.this, FragmentVideoPlay.this.mediaPlayer.getDuration() * seekBar.getProgress() / seekBar.getMax());
                    FragmentVideoPlay.this.mediaPlayer.seekTo(FragmentVideoPlay.this.currentPosition);
                }
            }
        });
    }
    
    private void initView() {
        this.mMMainActivity.showButtom();
        this.mSeekBar = (SeekBar)this.mRootView.findViewById(2131427550);
        this.mToolBar = (LinearLayout)this.mRootView.findViewById(2131427546);
        this.mZongTime = (TextView)this.mRootView.findViewById(2131427549);
        this.mBoTime = (TextView)this.mRootView.findViewById(2131427551);
        this.vedio_pause = (ImageView)this.mRootView.findViewById(2131427560);
        this.mVedioShnag = (ImageButton)this.mRootView.findViewById(2131427553);
        this.mVedioXia = (ImageButton)this.mRootView.findViewById(2131427555);
        this.mVedioBofang = (ImageButton)this.mRootView.findViewById(2131427554);
        this.mVedioCaidan = (ImageButton)this.mRootView.findViewById(2131427556);
        this.mFangsuo = (ImageButton)this.mRootView.findViewById(2131427561);
        this.mDibuCaidan = (LinearLayout)this.mRootView.findViewById(2131427562);
        this.mFlsurfaceView = (FrameLayout)this.mRootView.findViewById(2131427558);
        this.mSound = (ImageButton)this.mRootView.findViewById(2131427552);
        this.mZuoshengdao = (ImageButton)this.mRootView.findViewById(2131427563);
        this.mYoushengdao = (ImageButton)this.mRootView.findViewById(2131427564);
        this.mVedioName = (MyTextView)this.mRootView.findViewById(2131427567);
        this.mPaomadeng = (LinearLayout)this.mRootView.findViewById(2131427566);
        this.mSeekBeijing = (LinearLayout)this.mRootView.findViewById(2131427565);
        this.arr[0] = this.mSound;
        this.arr[1] = this.mZuoshengdao;
        this.arr[2] = this.mVedioShnag;
        this.arr[3] = this.mVedioBofang;
        this.arr[4] = this.mVedioXia;
        this.arr[5] = this.mYoushengdao;
        this.arr[6] = this.mVedioCaidan;
    }
    
    private void play(final int videoIndex) {
        this.mMusicHandler.removeCallbacks(this.updateRunnable);
        if (this.mediaBeans.size() == 0) {
            return;
        }
        while (true) {
            LauncherApplication.videoIndex = videoIndex;
            LauncherApplication.mSpUtils.putInt("videoPos", videoIndex);
            while (true) {
                Label_0249: {
                    try {
                        this.mediaPlayer.reset();
                        this.mediaPlayer.setDataSource(this.mediaBeans.get(videoIndex).getData());
                        this.mediaPlayer.setDisplay(this.surfaceView.getHolder());
                        this.isPrepare = true;
                        this.mediaPlayer.prepareAsync();
                        this.mediaPlayer.setOnErrorListener((MediaPlayer$OnErrorListener)new MediaPlayer$OnErrorListener() {
                            public boolean onError(final MediaPlayer mediaPlayer, final int n, final int n2) {
                                FragmentVideoPlay.this.resetPositon();
                                return false;
                            }
                        });
                        if (this.mediaBeans == null) {
                            break Label_0249;
                        }
                        final MediaBean mediaBean = this.mediaBeans.get(this.currentPlayIndex);
                        final String string = this.share.getString(FragmentVideoPlay.LAST_VIDEO_PLAY_NAME, "-");
                        if (mediaBean != null && string.equals(mediaBean.getDisplay_name())) {
                            this.currentPosition = this.share.getInt(FragmentVideoPlay.LAST_VIDEO_PLAY_POSITION, -1);
                            this.mediaPlayer.setOnPreparedListener((MediaPlayer$OnPreparedListener)new PrepareListener(this.currentPosition));
                            this.mVedioName.setText((CharSequence)this.mediaBeans.get(videoIndex).getTitle());
                            this.mediaPlayer.setOnCompletionListener((MediaPlayer$OnCompletionListener)new MediaPlayer$OnCompletionListener() {
                                public void onCompletion(final MediaPlayer mediaPlayer) {
                                    Log.e("videoshow", "onCompletion isPrepare = " + FragmentVideoPlay.this.isPrepare);
                                    FragmentVideoPlay.this.resetPositon();
                                    if (!FragmentVideoPlay.this.isPrepare) {
                                        FragmentVideoPlay.this.next();
                                    }
                                }
                            });
                            return;
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                        return;
                    }
                    this.currentPosition = -1;
                    continue;
                }
                this.currentPosition = -1;
                continue;
            }
        }
    }
    
    private void press(final int n) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean isOpenSound = false;
        switch (n) {
            case 0: {
                if (this.isOpenSound) {
                    this.mSound.setImageResource(2130837890);
                    if (!this.isOpenSound) {
                        isOpenSound = true;
                    }
                    this.isOpenSound = isOpenSound;
                    this.mediaPlayer.setVolume(1.0f, 1.0f);
                    break;
                }
                this.mSound.setImageResource(2130837888);
                this.isOpenSound = (!this.isOpenSound || b);
                this.mediaPlayer.setVolume(0.0f, 0.0f);
                break;
            }
            case 1: {
                this.mediaPlayer.seekTo(0);
                this.resetPositon();
                this.PasueFlag = true;
                this.mVedioBofang.setImageResource(2130837884);
                this.mediaPlayer.pause();
                break;
            }
            case 2: {
                this.resetPositon();
                if (this.currentPlayIndex > 0) {
                    this.play(--this.currentPlayIndex);
                    break;
                }
                this.play(this.currentPlayIndex);
                break;
            }
            case 3: {
                if (this.PasueFlag) {
                    this.mediaPlayer.start();
                    this.vedio_pause.setVisibility(8);
                    this.mVedioBofang.setImageResource(2130837894);
                    this.PasueFlag = (!this.PasueFlag || b2);
                    this.requestAudioFocus();
                    break;
                }
                this.mediaPlayer.pause();
                this.vedio_pause.setVisibility(0);
                this.mVedioBofang.setImageResource(2130837884);
                this.PasueFlag = (!this.PasueFlag || b3);
                break;
            }
            case 4: {
                this.resetPositon();
                if (this.currentPlayIndex < this.mediaBeans.size() - 1) {
                    this.play(++this.currentPlayIndex);
                    break;
                }
                this.play(this.currentPlayIndex);
                break;
            }
            case 5: {
                ++this.countSufaceVieew;
                this.showOrFangSuo();
                break;
            }
            case 6: {
                this.mMMainActivity.handleBackAction();
                break;
            }
        }
        this.seclectTrue(this.indexCount);
    }
    
    private void requestAudioFocus() {
        if (this.getActivity() == null) {
            return;
        }
        this.mApp.musicType = 3;
        Log.e("requestAudioFocus", "flag" + ((AudioManager)this.getActivity().getSystemService("audio")).requestAudioFocus(this.mMainAudioFocusListener, 3, 1));
        final MainService service = this.mApp.service;
    }
    
    private void seclectTrue(final int n) {
        for (int i = 0; i < this.arr.length; ++i) {
            if (n == i) {
                this.arr[i].setSelected(true);
            }
            else {
                this.arr[i].setSelected(false);
            }
        }
    }
    
    private void setAutoYincanMuen() {
        this.mMusicHandler.removeCallbacks(this.autoSettingHideOrShowRunnable);
        this.mMusicHandler.postDelayed(this.autoSettingHideOrShowRunnable, 5000L);
    }
    
    private void showOrFangSuo() {
        if (this.countSufaceVieew % 3 == 0) {
            if (!this.isBigView) {
                this.mYoushengdao.setImageResource(2130837882);
                final ViewGroup$LayoutParams layoutParams = this.surfaceView.getLayoutParams();
                layoutParams.width = 800;
                layoutParams.height = 480;
                this.surfaceView.setLayoutParams(layoutParams);
                return;
            }
            this.mYoushengdao.setImageResource(2130837882);
            final ViewGroup$LayoutParams layoutParams2 = this.surfaceView.getLayoutParams();
            layoutParams2.width = 1280;
            layoutParams2.height = 480;
            this.surfaceView.setLayoutParams(layoutParams2);
        }
        else {
            if (this.countSufaceVieew % 3 == 1) {
                this.mYoushengdao.setImageResource(2130837883);
                final ViewGroup$LayoutParams layoutParams3 = this.surfaceView.getLayoutParams();
                layoutParams3.width = 2000;
                layoutParams3.height = 1000;
                this.surfaceView.setLayoutParams(layoutParams3);
                return;
            }
            if (this.countSufaceVieew % 3 == 2) {
                if (this.isBigView) {
                    this.mYoushengdao.setImageResource(2130837881);
                    final ViewGroup$LayoutParams layoutParams4 = this.surfaceView.getLayoutParams();
                    layoutParams4.width = 1060;
                    layoutParams4.height = 300;
                    this.surfaceView.setLayoutParams(layoutParams4);
                    return;
                }
                this.mYoushengdao.setImageResource(2130837881);
                final ViewGroup$LayoutParams layoutParams5 = this.surfaceView.getLayoutParams();
                layoutParams5.width = 540;
                layoutParams5.height = 263;
                this.surfaceView.setLayoutParams(layoutParams5);
            }
        }
    }
    
    private void showOrHidePlaySeekbar() {
        if (this.iIsPlaySeekbarShow) {
            this.hidePlayControlBar();
            return;
        }
        this.showPlayControlBar();
    }
    
    private void showPlayControlBar() {
        this.mToolBar.setVisibility(0);
        this.mFangsuo.setVisibility(0);
        this.mDibuCaidan.setVisibility(0);
        this.mSeekBeijing.setVisibility(0);
        this.mPaomadeng.setVisibility(0);
        this.mMMainActivity.showButtom();
        this.mMMainActivity.showTop();
        this.setAutoYincanMuen();
        this.iIsPlaySeekbarShow = true;
    }
    
    private void updateCurrentPlayInfo() {
        try {
            this.currentPosition = this.mediaPlayer.getCurrentPosition();
            final int duration = this.mediaPlayer.getDuration();
            this.mSeekBar.setProgress(this.currentPosition * this.mSeekBar.getMax() / duration);
            this.mBoTime.setText((CharSequence)TimeUtils.secToTimeString(this.currentPosition / 1000));
            this.mZongTime.setText((CharSequence)TimeUtils.secToTimeString(duration / 1000));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void fangdaVedio() {
        this.lp = this.mFlsurfaceView.getLayoutParams();
        this.lp.width = 1280;
        this.lp.height = 480;
        this.mFlsurfaceView.setLayoutParams(this.lp);
        this.mDibucandan = this.mDibuCaidan.getLayoutParams();
        this.mDibucandan.width = 1280;
        this.mDibucandan.height = 480;
        this.mDibuCaidan.setLayoutParams(this.mDibucandan);
        this.mFangsuo.setImageResource(2130837623);
        this.mToolBar.setGravity(1);
        if (this.countSufaceVieew % 3 == 0) {
            this.mYoushengdao.setImageResource(2130837882);
            final ViewGroup$LayoutParams layoutParams = this.surfaceView.getLayoutParams();
            layoutParams.width = 1280;
            layoutParams.height = 480;
            this.surfaceView.setLayoutParams(layoutParams);
        }
        else if (this.countSufaceVieew % 3 == 2) {
            this.mYoushengdao.setImageResource(2130837881);
            final ViewGroup$LayoutParams layoutParams2 = this.surfaceView.getLayoutParams();
            layoutParams2.width = 1060;
            layoutParams2.height = 300;
            this.surfaceView.setLayoutParams(layoutParams2);
        }
        ((RelativeLayout$LayoutParams)this.mSeekBeijing.getLayoutParams()).setMargins(0, 358, 330, 0);
        final RelativeLayout$LayoutParams relativeLayout$LayoutParams = (RelativeLayout$LayoutParams)this.mPaomadeng.getLayoutParams();
        relativeLayout$LayoutParams.width = 480;
        this.mVedioName.setGravity(17);
        relativeLayout$LayoutParams.setMargins(400, 0, 0, 10);
    }
    
    public void handlerMsg(final Message message) {
        boolean pasueFlag = true;
        final boolean b = false;
        if (message.what == 6001) {
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode()) {
                this.showPlayControlBar();
                this.mToolBar.setVisibility(0);
                this.mFangsuo.setVisibility(0);
                this.mDibuCaidan.setVisibility(0);
                if (this.indexCount < this.arr.length - 1) {
                    ++this.indexCount;
                }
                this.seclectTrue(this.indexCount);
                this.setAutoYincanMuen();
            }
            else {
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.showPlayControlBar();
                    this.press(this.indexCount);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode()) {
                    this.showPlayControlBar();
                    this.mToolBar.setVisibility(0);
                    this.mFangsuo.setVisibility(0);
                    this.mDibuCaidan.setVisibility(0);
                    if (this.indexCount > 0) {
                        --this.indexCount;
                    }
                    this.seclectTrue(this.indexCount);
                    this.setAutoYincanMuen();
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    if (!this.isBigView) {
                        this.isBigView = true;
                        this.fangdaVedio();
                    }
                }
                else if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                    if (this.isBigView) {
                        this.isBigView = false;
                        this.suoXiaoVedio();
                    }
                }
                else if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.PasueFlag) {
                        this.mediaPlayer.start();
                        this.mVedioBofang.setImageResource(2130837894);
                        this.PasueFlag = (!this.PasueFlag || b);
                        this.vedio_pause.setVisibility(8);
                    }
                }
                else if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.DOWN.getCode() && !this.PasueFlag) {
                    this.mediaPlayer.pause();
                    this.mVedioBofang.setImageResource(2130837884);
                    if (this.PasueFlag) {
                        pasueFlag = false;
                    }
                    this.PasueFlag = pasueFlag;
                    this.vedio_pause.setVisibility(0);
                }
            }
        }
        else if (message.what == 1007) {
            this.info = this.mApp.carBaseInfo;
            if (this.info.isiFlash()) {
                if (this.info.isiLeftBackOpen() || this.info.isiLeftFrontOpen() || this.info.isiRightBackOpen() || this.info.isiRightFrontOpen()) {
                    this.suoXiaoVedio();
                    this.showOrFangSuo();
                    this.showPlayControlBar();
                    return;
                }
                this.setAutoYincanMuen();
            }
        }
    }
    
    public void next() {
        ++this.currentPlayIndex;
        if (this.mediaBeans.size() <= this.currentPlayIndex) {
            this.currentPlayIndex = 0;
        }
        this.play(this.currentPlayIndex);
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mContext = (Context)this.getActivity();
        this.mMMainActivity = (Launcher)this.getActivity();
        (this.mApp = (LauncherApplication)this.mMMainActivity.getApplication()).registerHandler(this.mMusicHandler);
        final long currentTimeMillis = System.currentTimeMillis();
        Log.e("", "videoshow = " + currentTimeMillis);
        this.share = new SpUtilK((Context)this.getActivity());
        this.mRootView = layoutInflater.inflate(2130903078, (ViewGroup)null);
        this.initView();
        this.mediaPlayer = new MediaPlayer();
        this.currentPlayIndex = this.getArguments().getInt("position");
        if (this.mediaBeans != null) {
            final MediaBean mediaBean = this.mediaBeans.get(this.currentPlayIndex);
            final String string = this.share.getString(FragmentVideoPlay.LAST_VIDEO_PLAY_NAME, "-");
            if (mediaBean != null && string.equals(mediaBean.getDisplay_name())) {
                this.currentPosition = this.share.getInt(FragmentVideoPlay.LAST_VIDEO_PLAY_POSITION, -1);
            }
            else {
                this.currentPosition = -1;
            }
        }
        else {
            this.currentPosition = -1;
        }
        this.surfaceView = (SurfaceView)this.mRootView.findViewById(2131427559);
        this.surfaceView.getHolder().setFixedSize(176, 144);
        this.surfaceView.getHolder().setKeepScreenOn(true);
        this.surfaceView.setClickable(true);
        this.surfaceViewCallBack = new SurfaceViewCallBack((SurfaceViewCallBack)null);
        this.surfaceView.getHolder().addCallback((SurfaceHolder$Callback)this.surfaceViewCallBack);
        this.initListener();
        Log.e("", "videoshow = " + (currentTimeMillis - System.currentTimeMillis()));
        this.setAutoYincanMuen();
        this.seclectTrue(0);
        this.requestAudioFocus();
        return this.mRootView;
    }
    
    public void onDestroy() {
        this.abandonAudioFocus();
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
        this.iNeedHeartThread = false;
        this.mMusicHandler.removeCallbacks(this.autoSettingHideOrShowRunnable);
        this.mApp.unregisterHandler(this.mMusicHandler);
        super.onDestroy();
    }
    
    public void onStart() {
        this.iNeedHeartThread = true;
        if (this.mediaPlayer == null) {
            this.mediaPlayer = new MediaPlayer();
        }
        super.onStart();
    }
    
    public void onStop() {
        super.onStop();
    }
    
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.startX = (int)motionEvent.getX();
            this.curProgress = this.mSeekBar.getProgress();
            this.moveX = 0;
        }
        else {
            if (motionEvent.getAction() == 2) {
                this.moveX = (int)(motionEvent.getX() - this.startX);
                if (this.moveX >= (100 - this.curProgress) * 10) {
                    this.changeProgress = 100;
                }
                else if (this.moveX <= -this.curProgress * 10) {
                    this.changeProgress = 0;
                }
                else {
                    this.changeProgress = this.curProgress + this.moveX / 10;
                }
                if ((this.moveX >= 5 || this.moveX <= -5) && this.iIsPlaySeekbarShow) {
                    this.currentPosition = this.mediaPlayer.getDuration() * this.changeProgress / 100;
                    this.mediaPlayer.seekTo(this.currentPosition);
                    this.mMusicHandler.post(this.updateRunnable);
                }
                Log.e("mVideo", "onTouch moveX = " + this.moveX);
                return true;
            }
            if (motionEvent.getAction() != 1) {
                return false;
            }
            if (this.moveX < 5 && this.moveX > -5) {
                this.showOrHidePlaySeekbar();
                return true;
            }
        }
        return true;
    }
    
    public void resetPositon() {
        this.share.putInt(FragmentVideoPlay.LAST_VIDEO_PLAY_POSITION, -1);
        this.share.putString(FragmentVideoPlay.LAST_VIDEO_PLAY_NAME, "");
    }
    
    public void suoXiaoVedio() {
        this.lp = this.mFlsurfaceView.getLayoutParams();
        this.lp.width = 800;
        this.lp.height = 480;
        this.mFlsurfaceView.setLayoutParams(this.lp);
        this.mDibucandan = this.mDibuCaidan.getLayoutParams();
        this.mDibucandan.width = 800;
        this.mDibucandan.height = 480;
        this.mDibuCaidan.setLayoutParams(this.mDibucandan);
        this.mFangsuo.setImageResource(2130837622);
        this.mToolBar.setGravity(5);
        ((RelativeLayout$LayoutParams)this.mSeekBeijing.getLayoutParams()).setMargins(54, 358, 74, 0);
        final RelativeLayout$LayoutParams relativeLayout$LayoutParams = (RelativeLayout$LayoutParams)this.mPaomadeng.getLayoutParams();
        this.mVedioName.setGravity(3);
        relativeLayout$LayoutParams.width = 480;
        relativeLayout$LayoutParams.setMargins(490, 0, 0, 10);
        if (this.countSufaceVieew % 3 == 0) {
            this.mYoushengdao.setImageResource(2130837882);
            final ViewGroup$LayoutParams layoutParams = this.surfaceView.getLayoutParams();
            layoutParams.width = 800;
            layoutParams.height = 480;
            this.surfaceView.setLayoutParams(layoutParams);
        }
        else if (this.countSufaceVieew % 3 == 2) {
            this.mYoushengdao.setImageResource(2130837881);
            final ViewGroup$LayoutParams layoutParams2 = this.surfaceView.getLayoutParams();
            layoutParams2.width = 540;
            layoutParams2.height = 263;
            this.surfaceView.setLayoutParams(layoutParams2);
        }
    }
    
    static class MusicLanyaHandler extends Handler
    {
        private WeakReference<FragmentVideoPlay> target;
        
        public MusicLanyaHandler(final FragmentVideoPlay fragmentVideoPlay) {
            this.target = new WeakReference<FragmentVideoPlay>(fragmentVideoPlay);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
    
    private final class PrepareListener implements MediaPlayer$OnPreparedListener
    {
        private int position;
        
        public PrepareListener(final int position) {
            this.position = position;
        }
        
        public void onPrepared(final MediaPlayer mediaPlayer) {
            FragmentVideoPlay.access$17(FragmentVideoPlay.this, false);
            FragmentVideoPlay.this.mediaPlayer.start();
            FragmentVideoPlay.this.vedio_pause.setVisibility(8);
            FragmentVideoPlay.this.mMusicHandler.post(FragmentVideoPlay.this.updateRunnable);
            if (this.position > 0) {
                FragmentVideoPlay.this.mediaPlayer.seekTo(this.position);
            }
        }
    }
    
    private final class SurfaceViewCallBack implements SurfaceHolder$Callback
    {
        public void surfaceChanged(final SurfaceHolder surfaceHolder, final int n, final int n2, final int n3) {
        }
        
        public void surfaceCreated(final SurfaceHolder surfaceHolder) {
            if (FragmentVideoPlay.this.mediaBeans != null && FragmentVideoPlay.this.currentPlayIndex >= 0 && FragmentVideoPlay.this.mediaBeans.size() > FragmentVideoPlay.this.currentPlayIndex && FragmentVideoPlay.this.mediaBeans.get(FragmentVideoPlay.this.currentPlayIndex).getData() != null && new File(FragmentVideoPlay.this.mediaBeans.get(FragmentVideoPlay.this.currentPlayIndex).getData()).exists()) {
                if (!FragmentVideoPlay.this.PasueFlag) {
                    FragmentVideoPlay.this.play(FragmentVideoPlay.this.currentPlayIndex);
                }
                else {
                    FragmentVideoPlay.this.mediaPlayer.setDisplay(FragmentVideoPlay.this.surfaceView.getHolder());
                }
                if (FragmentVideoPlay.this.currentPosition >= 0) {
                    FragmentVideoPlay.this.mediaPlayer.seekTo(FragmentVideoPlay.this.currentPosition);
                }
            }
        }
        
        public void surfaceDestroyed(final SurfaceHolder surfaceHolder) {
            if (FragmentVideoPlay.this.mediaPlayer != null) {
                Log.e("videoshow", "mediaPlayer isPlaying= " + FragmentVideoPlay.this.mediaPlayer.isPlaying());
                if (FragmentVideoPlay.this.mediaPlayer.isPlaying()) {
                    FragmentVideoPlay.access$13(FragmentVideoPlay.this, FragmentVideoPlay.this.mediaPlayer.getCurrentPosition());
                    FragmentVideoPlay.this.share.putInt(FragmentVideoPlay.LAST_VIDEO_PLAY_POSITION, FragmentVideoPlay.this.currentPosition);
                    FragmentVideoPlay.this.share.putString(FragmentVideoPlay.LAST_VIDEO_PLAY_NAME, FragmentVideoPlay.this.mediaBeans.get(FragmentVideoPlay.this.currentPlayIndex).getDisplay_name());
                    FragmentVideoPlay.this.mediaPlayer.stop();
                }
            }
        }
    }
}
