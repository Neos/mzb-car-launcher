package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import android.content.pm.*;
import java.util.*;
import android.graphics.drawable.*;
import android.net.*;
import com.touchus.benchilauncher.*;
import com.backaudio.android.driver.*;
import android.content.*;
import android.support.v4.view.*;
import com.touchus.benchilauncher.views.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;
import android.widget.*;

public class FragmentYingyong extends BaseFragment implements AdapterView$OnItemClickListener, AdapterView$OnItemLongClickListener
{
    BroadcastReceiver appBroadcastReceiver;
    private int currentPage;
    private LinearLayout dot_container;
    private int lastPage;
    private LauncherApplication mApp;
    public AppHandler mAppHandler;
    private Launcher mContext;
    private List<ResolveInfo> mNeedShowApps;
    private View mRootView;
    private int pageCount;
    private int selectIndex;
    private ArrayList<GridView> viewPageListData;
    private ViewPager viewPager;
    
    public FragmentYingyong() {
        this.mNeedShowApps = new ArrayList<ResolveInfo>();
        this.selectIndex = 0;
        this.currentPage = 0;
        this.lastPage = 0;
        this.mAppHandler = new AppHandler(this);
        this.appBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                FragmentYingyong.this.initData();
            }
        };
    }
    
    static /* synthetic */ void access$3(final FragmentYingyong fragmentYingyong, final int currentPage) {
        fragmentYingyong.currentPage = currentPage;
    }
    
    static /* synthetic */ void access$5(final FragmentYingyong fragmentYingyong, final int lastPage) {
        fragmentYingyong.lastPage = lastPage;
    }
    
    static /* synthetic */ void access$7(final FragmentYingyong fragmentYingyong, final int selectIndex) {
        fragmentYingyong.selectIndex = selectIndex;
    }
    
    private void addAppReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.mContext.registerReceiver(this.appBroadcastReceiver, intentFilter);
    }
    
    private void clearAllDotState() {
        for (int i = 0; i < this.dot_container.getChildCount(); ++i) {
            this.dot_container.getChildAt(i).setSelected(false);
        }
    }
    
    private void initDot() {
        this.pageCount = 1;
        if (this.mNeedShowApps != null) {
            this.pageCount = (this.mNeedShowApps.size() - 1) / 8 + 1;
        }
        if (this.dot_container.getChildCount() > 0) {
            this.dot_container.removeAllViews();
        }
        final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-2, -2);
        for (int i = 0; i < this.pageCount; ++i) {
            final ImageView imageView = new ImageView((Context)this.mContext);
            imageView.setLayoutParams(new ViewGroup$LayoutParams(-2, -2));
            imageView.setImageResource(2130837627);
            if (i != 0) {
                layoutParams.setMargins(20, 0, 0, 0);
                imageView.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
            }
            else {
                imageView.setSelected(true);
            }
            this.dot_container.addView((View)imageView);
        }
    }
    
    private void initViewPageData() {
        this.viewPageListData = new ArrayList<GridView>();
        for (int i = 0; i < this.pageCount; ++i) {
            final GridView gridView = new GridView((Context)this.mContext);
            gridView.setLayoutParams(new ViewGroup$LayoutParams(-1, -1));
            gridView.setNumColumns(4);
            final ArrayList<ResolveInfo> list = new ArrayList<ResolveInfo>();
            int size;
            if ((size = (i + 1) * 8) > this.mNeedShowApps.size()) {
                size = this.mNeedShowApps.size();
            }
            for (int j = i * 8; j < size; ++j) {
                list.add(this.mNeedShowApps.get(j));
            }
            gridView.setAdapter((ListAdapter)new AppsGridAdapter((Context)this.mContext, list, i));
            gridView.setSelector((Drawable)new ColorDrawable(0));
            gridView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
            gridView.setOnItemLongClickListener((AdapterView$OnItemLongClickListener)this);
            this.viewPageListData.add(gridView);
        }
    }
    
    private void loadApps() {
        final Intent intent = new Intent("android.intent.action.MAIN", (Uri)null);
        intent.addCategory("android.intent.category.LAUNCHER");
        final List queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(intent, 0);
        this.mNeedShowApps.clear();
        int i = 0;
    Label_0130_Outer:
        while (i < queryIntentActivities.size()) {
            final ResolveInfo resolveInfo = queryIntentActivities.get(i);
            boolean b = false;
            final String packageName = resolveInfo.activityInfo.packageName;
            while (true) {
                try {
                    if ((this.mContext.getPackageManager().getApplicationInfo(packageName, 0).flags & 0x1) > 0) {
                        b = true;
                    }
                    else {
                        b = false;
                    }
                    if ((!b && !packageName.equals("com.touchus.factorytest") && !packageName.equals("com.touchus.benchilauncher")) || packageName.equals("com.mediatek.filemanager") || packageName.equals("com.autonavi.amapauto") || packageName.equals("com.unibroad.voiceassistant") || packageName.equals("com.uc.browser.hd") || packageName.equals("com.android.vending") || packageName.equals("com.android.chrome") || packageName.equals("com.google.android.apps.maps") || packageName.equals("com.google.android.youtube") || packageName.equals("com.google.android.gms") || packageName.equals("com.papago.s1OBU") || packageName.equals("com.tima.carnet.vt")) {
                        this.mNeedShowApps.add(resolveInfo);
                    }
                    ++i;
                    continue Label_0130_Outer;
                }
                catch (Exception ex) {
                    continue;
                }
                continue;
            }
        }
        final boolean dv_CUSTOM = SysConst.DV_CUSTOM;
        this.mNeedShowApps.add(0, new ResolveInfo());
    }
    
    private void startApp() {
        final ResolveInfo resolveInfo = this.mNeedShowApps.get(this.selectIndex);
        if (resolveInfo.activityInfo == null && this.selectIndex == 0) {
            this.mApp.service.iIsInRecorder = true;
            this.mApp.service.createNoTouchScreens();
            Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.RECORDER);
            return;
        }
        final ComponentName component = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        final Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.addFlags(2097152);
        intent.setComponent(component);
        this.mContext.startActivity(intent);
    }
    
    private void unAppReceiver() {
        this.mContext.unregisterReceiver(this.appBroadcastReceiver);
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                ++this.selectIndex;
                if (this.selectIndex >= this.mNeedShowApps.size()) {
                    this.selectIndex = this.mNeedShowApps.size() - 1;
                }
                final int n = this.selectIndex / 8;
                if (n != this.currentPage) {
                    this.viewPager.setCurrentItem(n, true);
                }
                ((AppsGridAdapter)this.viewPageListData.get(this.currentPage).getAdapter()).notifyDataSetChanged();
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    --this.selectIndex;
                    if (this.selectIndex <= 0) {
                        this.selectIndex = 0;
                    }
                    final int n2 = this.selectIndex / 8;
                    if (n2 != this.currentPage) {
                        this.viewPager.setCurrentItem(n2, true);
                    }
                    ((AppsGridAdapter)this.viewPageListData.get(this.currentPage).getAdapter()).notifyDataSetChanged();
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.selectIndex != 0) {
                        this.selectIndex -= 4;
                        if (this.selectIndex <= 0) {
                            this.selectIndex = 0;
                        }
                        final int n3 = this.selectIndex / 8;
                        if (n3 != this.currentPage) {
                            this.viewPager.setCurrentItem(n3, true);
                        }
                        ((AppsGridAdapter)this.viewPageListData.get(this.currentPage).getAdapter()).notifyDataSetChanged();
                    }
                }
                else if (byte1 == Mainboard.EIdriverEnum.DOWN.getCode()) {
                    if (this.selectIndex != this.mNeedShowApps.size() - 1) {
                        this.selectIndex += 4;
                        if (this.selectIndex >= this.mNeedShowApps.size()) {
                            this.selectIndex = this.mNeedShowApps.size() - 1;
                        }
                        final int n4 = this.selectIndex / 8;
                        if (n4 != this.currentPage) {
                            this.viewPager.setCurrentItem(n4, true);
                        }
                        ((AppsGridAdapter)this.viewPageListData.get(this.currentPage).getAdapter()).notifyDataSetChanged();
                    }
                }
                else if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.startApp();
                }
            }
        }
    }
    
    public void initData() {
        this.currentPage = 0;
        this.loadApps();
        this.initDot();
        this.initViewPageData();
        this.viewPager.setAdapter(new AppPagerAdapter((Context)this.mContext, this.viewPageListData));
        this.viewPager.setOffscreenPageLimit(this.viewPageListData.size());
    }
    
    public void initEvent() {
        this.viewPager.setOnPageChangeListener((ViewPager.OnPageChangeListener)new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(final int n) {
                if (n == 1) {
                    FragmentYingyong.access$5(FragmentYingyong.this, FragmentYingyong.this.currentPage);
                }
                else if (n == 0) {
                    if (FragmentYingyong.this.lastPage > FragmentYingyong.this.currentPage) {
                        FragmentYingyong.access$7(FragmentYingyong.this, FragmentYingyong.this.lastPage * 8 - 1);
                    }
                    else if (FragmentYingyong.this.lastPage < FragmentYingyong.this.currentPage) {
                        FragmentYingyong.access$7(FragmentYingyong.this, FragmentYingyong.this.currentPage * 8);
                    }
                    ((AppsGridAdapter)FragmentYingyong.this.viewPageListData.get(FragmentYingyong.this.currentPage).getAdapter()).notifyDataSetChanged();
                }
            }
            
            @Override
            public void onPageScrolled(final int n, final float n2, final int n3) {
            }
            
            @Override
            public void onPageSelected(final int n) {
                FragmentYingyong.this.clearAllDotState();
                FragmentYingyong.this.dot_container.getChildAt(n).setSelected(true);
                FragmentYingyong.access$3(FragmentYingyong.this, n);
            }
        });
    }
    
    public void initView() {
        this.dot_container = (LinearLayout)this.mRootView.findViewById(2131427571);
        this.viewPager = (ViewPager)this.mRootView.findViewById(2131427570);
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2130903081, (ViewGroup)null);
        this.mContext = (Launcher)this.getActivity();
        this.initView();
        this.initData();
        this.initEvent();
        this.addAppReceiver();
        return this.mRootView;
    }
    
    public void onDestroy() {
        this.unAppReceiver();
        super.onDestroy();
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        this.selectIndex = this.currentPage * 8 + n;
        ((AppsGridAdapter)adapterView.getAdapter()).notifyDataSetChanged();
        this.startApp();
    }
    
    public boolean onItemLongClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        final UnInstallDialog unInstallDialog = new UnInstallDialog((Context)this.mContext, 2131230726);
        final Window window = unInstallDialog.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        attributes.x = 260;
        window.setAttributes(attributes);
        final ResolveInfo resolveInfo = this.mNeedShowApps.get(this.currentPage * 8 + n);
        if (resolveInfo.activityInfo == null) {
            return false;
        }
        unInstallDialog.setPkgName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.loadLabel(this.mContext.getPackageManager()).toString());
        unInstallDialog.show();
        return false;
    }
    
    public void onPause() {
        this.mApp.unregisterHandler(this.mAppHandler);
        super.onPause();
    }
    
    public void onResume() {
        (this.mApp = (LauncherApplication)this.mContext.getApplication()).registerHandler(this.mAppHandler);
        super.onResume();
    }
    
    class AppHandler extends Handler
    {
        private WeakReference<FragmentYingyong> target;
        
        public AppHandler(final FragmentYingyong fragmentYingyong) {
            this.target = new WeakReference<FragmentYingyong>(fragmentYingyong);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
    
    class AppPagerAdapter extends PagerAdapter
    {
        private ArrayList<GridView> listData;
        private Context mContext;
        
        public AppPagerAdapter(final Context mContext, final ArrayList<GridView> listData) {
            this.mContext = mContext;
            this.listData = listData;
        }
        
        @Override
        public void destroyItem(final ViewGroup viewGroup, final int n, final Object o) {
            viewGroup.removeView((View)this.listData.get(n));
        }
        
        @Override
        public int getCount() {
            return this.listData.size();
        }
        
        @Override
        public Object instantiateItem(final ViewGroup viewGroup, final int n) {
            viewGroup.addView((View)this.listData.get(n), n);
            return this.listData.get(n);
        }
        
        @Override
        public boolean isViewFromObject(final View view, final Object o) {
            return view == o;
        }
    }
    
    class AppsGridAdapter extends BaseAdapter
    {
        private int currentPage;
        private List<ResolveInfo> listData;
        private Context mContext;
        
        public AppsGridAdapter(final Context mContext, final ArrayList<ResolveInfo> listData, final int currentPage) {
            this.currentPage = 0;
            this.mContext = mContext;
            this.listData = listData;
            this.currentPage = currentPage;
        }
        
        public final int getCount() {
            return this.listData.size();
        }
        
        public final Object getItem(final int n) {
            return this.listData.get(n);
        }
        
        public final long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View inflate = view;
            if (view == null) {
                inflate = View.inflate(this.mContext, 2130903082, (ViewGroup)null);
            }
            final ImageView imageView = (ImageView)inflate.findViewById(2131427572);
            final TextView textView = (TextView)inflate.findViewById(2131427573);
            final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131427345);
            final ResolveInfo resolveInfo = this.listData.get(n);
            if (resolveInfo.activityInfo == null && this.currentPage == 0 && n == 0) {
                imageView.setImageResource(2130837721);
                textView.setText(2131165265);
            }
            else {
                imageView.setImageDrawable(resolveInfo.activityInfo.loadIcon(this.mContext.getPackageManager()));
                textView.setText((CharSequence)resolveInfo.activityInfo.loadLabel(this.mContext.getPackageManager()).toString());
            }
            if (FragmentYingyong.this.selectIndex == this.currentPage * 8 + n) {
                linearLayout.setBackgroundResource(2130837629);
                return inflate;
            }
            linearLayout.setBackgroundResource(2130837628);
            return inflate;
        }
        
        public void setData(final List<ResolveInfo> listData) {
            this.listData = listData;
        }
    }
}
