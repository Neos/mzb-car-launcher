package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.utils.*;
import android.text.*;
import android.widget.*;
import android.graphics.*;
import android.content.*;
import com.touchus.publicutils.bean.*;
import com.backaudio.android.driver.*;
import android.view.*;
import com.touchus.benchilauncher.views.*;
import com.touchus.publicutils.utils.*;
import com.touchus.benchilauncher.service.*;
import android.os.*;
import java.lang.ref.*;

public class FragmentMusicShow extends BaseFragment
{
    private static String POS;
    private static Launcher mContext;
    private ImageView[] arr;
    private int count;
    private TextView curIndexTview;
    private int loopType;
    private LauncherApplication mApp;
    private ImageView mBofang;
    private byte mIDRIVERENUM;
    private TextView mMTitle;
    private ImageView mMenuImg;
    public MusicHandlerx mMusicHandlerx;
    private TextView mNowTime;
    private View mRootView;
    private ImageView mShang;
    private ImageView mSingleLoopImg;
    private SpUtilK mSpUtilK;
    private ImageView mXiayiqu;
    private TextView mZongTime;
    private ImageView musicPic;
    private int needPlayIndex;
    private SeekBar seekBar;
    private TextView totalRecordTview;
    private Runnable updatePlayTimeRunnable;
    
    static {
        FragmentMusicShow.POS = "pos";
    }
    
    public FragmentMusicShow() {
        this.arr = new ImageView[5];
        this.count = 0;
        this.needPlayIndex = -1;
        this.loopType = -1;
        this.updatePlayTimeRunnable = new Runnable() {
            @Override
            public void run() {
                FragmentMusicShow.this.mMusicHandlerx.postDelayed(FragmentMusicShow.this.updatePlayTimeRunnable, 1000L);
                final int position = FragmentMusicShow.this.mApp.musicPlayControl.getPosition();
                FragmentMusicShow.this.seekBar.setProgress(position);
                final String secToTimeString = TimeUtils.secToTimeString(position / 1000);
                if (TextUtils.isEmpty((CharSequence)secToTimeString)) {
                    return;
                }
                FragmentMusicShow.this.mNowTime.setText((CharSequence)secToTimeString);
            }
        };
        this.mMusicHandlerx = new MusicHandlerx(this);
    }
    
    static /* synthetic */ void access$5(final FragmentMusicShow fragmentMusicShow, final int count) {
        fragmentMusicShow.count = count;
    }
    
    private void initListener() {
        this.mMenuImg.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicShow.access$5(FragmentMusicShow.this, 0);
                FragmentMusicShow.this.seclectTrue(0);
                FragmentMusicShow.mContext.handleBackAction();
            }
        });
        this.mSingleLoopImg.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicShow.access$5(FragmentMusicShow.this, 1);
                FragmentMusicShow.this.seclectTrue(1);
                FragmentMusicShow.this.pressItem(FragmentMusicShow.this.count);
            }
        });
        this.mShang.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicShow.access$5(FragmentMusicShow.this, 2);
                FragmentMusicShow.this.seclectTrue(FragmentMusicShow.this.count);
                FragmentMusicShow.this.pressItem(FragmentMusicShow.this.count);
            }
        });
        this.mBofang.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicShow.access$5(FragmentMusicShow.this, 3);
                FragmentMusicShow.this.seclectTrue(FragmentMusicShow.this.count);
                FragmentMusicShow.this.pressItem(FragmentMusicShow.this.count);
            }
        });
        this.mXiayiqu.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentMusicShow.access$5(FragmentMusicShow.this, 4);
                FragmentMusicShow.this.seclectTrue(FragmentMusicShow.this.count);
                FragmentMusicShow.this.pressItem(FragmentMusicShow.this.count);
            }
        });
        this.seekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener() {
            public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
            }
            
            public void onStartTrackingTouch(final SeekBar seekBar) {
            }
            
            public void onStopTrackingTouch(final SeekBar seekBar) {
                FragmentMusicShow.this.mApp.musicPlayControl.changeCurrentPlayProgress(seekBar.getProgress());
            }
        });
    }
    
    private void pressItem(final int n) {
        switch (n) {
            default: {}
            case 0: {
                FragmentMusicShow.mContext.handleBackAction();
            }
            case 1: {
                if (this.loopType == 1001) {
                    this.loopType = 1003;
                }
                else if (this.loopType == 1003) {
                    this.loopType = 1002;
                }
                else {
                    this.loopType = 1001;
                }
                this.mSpUtilK.putInt("musicloop", this.loopType);
                this.setLoopType();
            }
            case 2: {
                this.prev();
                this.setImageSeclect(n);
            }
            case 3: {
                if (this.mApp.musicPlayControl.isMusicPlaying()) {
                    this.mBofang.setImageResource(2130837506);
                    this.mApp.musicPlayControl.ipause = true;
                    this.mApp.musicPlayControl.pauseMusic();
                    this.mApp.musicPlayControl.settingPlayState(false);
                    return;
                }
                this.mBofang.setImageResource(2130837526);
                this.mApp.musicPlayControl.replayMusic();
                this.mApp.musicPlayControl.ipause = false;
                this.mApp.musicPlayControl.settingPlayState(true);
            }
            case 4: {
                this.Next();
                this.setImageSeclect(n);
            }
        }
    }
    
    private void seclectTrue(final int playBtnStatus) {
        this.setPlayBtnStatus(playBtnStatus);
        for (int i = 0; i < this.arr.length; ++i) {
            if (playBtnStatus == i) {
                this.arr[i].setSelected(true);
            }
            else {
                this.arr[i].setSelected(false);
            }
        }
    }
    
    private void setCurrentPlayInfo() {
        final MediaBean currentMusic = this.mApp.musicPlayControl.currentMusic;
        if (this.getActivity() == null || currentMusic == null) {
            return;
        }
        this.mMTitle.setText((CharSequence)currentMusic.getTitle().split("\\.")[0]);
        final String secToTimeString = TimeUtils.secToTimeString((int)currentMusic.getDuration() / 1000);
        this.seekBar.setMax((int)currentMusic.getDuration());
        this.mZongTime.setText((CharSequence)secToTimeString);
        this.mNowTime.setText((CharSequence)TimeUtils.secToTimeString(this.mApp.musicPlayControl.getPosition() / 1000));
        final int musicIndex = this.mApp.musicPlayControl.musicIndex;
        if (musicIndex >= 0) {
            this.curIndexTview.setText((CharSequence)new StringBuilder().append(musicIndex + 1).toString());
        }
        else {
            this.curIndexTview.setText((CharSequence)"");
        }
        if (this.mApp.musicPlayControl.getMusicList() != null) {
            this.totalRecordTview.setText((CharSequence)new StringBuilder().append(this.mApp.musicPlayControl.getMusicList().size()).toString());
        }
        else {
            this.totalRecordTview.setText((CharSequence)"");
        }
        if (currentMusic.getBitmap() == null) {
            this.musicPic.setTag((Object)currentMusic.getData());
            LoadLocalImageUtil.getInstance().loadDrawable(currentMusic, LoadLocalImageUtil.MUSIC_TYPE, (Context)FragmentMusicShow.mContext, currentMusic.getData(), (LoadLocalImageUtil.ImageCallback)new LoadLocalImageUtil.ImageCallback() {
                @Override
                public void imageLoaded(final Bitmap imageBitmap, final String s) {
                    if (FragmentMusicShow.this.musicPic.getTag().equals(s) && imageBitmap != null) {
                        FragmentMusicShow.this.musicPic.setImageBitmap(imageBitmap);
                        return;
                    }
                    FragmentMusicShow.this.musicPic.setImageResource(2130837976);
                }
            });
        }
        else {
            this.musicPic.setImageBitmap(currentMusic.getBitmap());
        }
        this.setLoopType();
    }
    
    private void setImageSeclect(final int n) {
        if (n == 2) {
            this.mShang.setImageResource(2130837518);
            this.mMusicHandlerx.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    FragmentMusicShow.this.mShang.setImageResource(2130837519);
                }
            }, 300L);
        }
        else if (n == 4) {
            this.mXiayiqu.setImageResource(2130837522);
            this.mMusicHandlerx.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    FragmentMusicShow.this.mXiayiqu.setImageResource(2130837523);
                }
            }, 300L);
        }
    }
    
    private void setLoopType() {
        switch (this.loopType) {
            default: {
                this.mSingleLoopImg.setImageResource(2130837504);
                break;
            }
            case 1001: {
                this.mSingleLoopImg.setImageResource(2130837504);
                break;
            }
            case 1003: {
                this.mSingleLoopImg.setImageResource(2130837520);
                break;
            }
            case 1002: {
                this.mSingleLoopImg.setImageResource(2130837510);
                break;
            }
        }
        this.mApp.musicPlayControl.setPlayLoopType(this.loopType);
    }
    
    private void setPlayBtnStatus(final int n) {
        if (n != 3) {
            if (this.mApp.musicPlayControl.isMusicPlaying()) {
                this.mBofang.setImageResource(2130837527);
                return;
            }
            this.mBofang.setImageResource(2130837507);
        }
        else {
            if (this.mApp.musicPlayControl.isMusicPlaying()) {
                this.mBofang.setImageResource(2130837526);
                return;
            }
            this.mBofang.setImageResource(2130837506);
        }
    }
    
    public void Next() {
        this.mApp.musicPlayControl.ipause = false;
        this.mApp.musicPlayControl.playNextMusic();
        this.setCurrentPlayInfo();
    }
    
    public void handlerMsgx(final Message message) {
        if (message.what == 6001) {
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                this.pressItem(this.count);
            }
            else {
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                    if (this.count < this.arr.length - 1) {
                        ++this.count;
                    }
                    this.seclectTrue(this.count);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    if (this.count > 0) {
                        --this.count;
                    }
                    this.seclectTrue(this.count);
                }
            }
        }
        else {
            if (message.what == 7001) {
                if (this.count != 3) {
                    this.mBofang.setImageResource(2130837527);
                }
                else {
                    this.mBofang.setImageResource(2130837526);
                }
                this.setCurrentPlayInfo();
                return;
            }
            if (message.what == 7003) {
                this.setPlayBtnStatus(this.count);
            }
        }
    }
    
    @Override
    public boolean onBack() {
        return LauncherApplication.iPlayingAuto = false;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2130903075, (ViewGroup)null);
        FragmentMusicShow.mContext = (Launcher)this.getActivity();
        this.mSpUtilK = new SpUtilK(FragmentMusicShow.mContext.getApplicationContext());
        if (bundle != null) {
            this.count = bundle.getInt(FragmentMusicShow.POS, 0);
        }
        this.loopType = this.mSpUtilK.getInt("musicloop", 1001);
        this.mApp = (LauncherApplication)FragmentMusicShow.mContext.getApplication();
        this.mMTitle = (MyTextView)this.mRootView.findViewById(2131427520);
        this.musicPic = (ImageView)this.mRootView.findViewById(2131427527);
        this.mNowTime = (TextView)this.mRootView.findViewById(2131427529);
        this.mZongTime = (TextView)this.mRootView.findViewById(2131427530);
        this.curIndexTview = (TextView)this.mRootView.findViewById(2131427525);
        this.totalRecordTview = (TextView)this.mRootView.findViewById(2131427526);
        this.seekBar = (SeekBar)this.mRootView.findViewById(2131427528);
        this.mShang = (ImageView)this.mRootView.findViewById(2131427517);
        this.mBofang = (ImageView)this.mRootView.findViewById(2131427518);
        this.mXiayiqu = (ImageView)this.mRootView.findViewById(2131427519);
        this.mMenuImg = (ImageView)this.mRootView.findViewById(2131427523);
        this.mSingleLoopImg = (ImageView)this.mRootView.findViewById(2131427524);
        this.arr[0] = this.mMenuImg;
        this.arr[1] = this.mSingleLoopImg;
        this.arr[2] = this.mShang;
        this.arr[3] = this.mBofang;
        this.arr[4] = this.mXiayiqu;
        this.needPlayIndex = this.getArguments().getInt("position");
        this.mMTitle.setFocusable(true);
        this.initListener();
        if (this.mApp.musicPlayControl == null) {
            UtilTools.sendKeyeventToSystem(4);
            this.mApp.musicPlayControl = new MusicPlayControl((Context)this.getActivity());
        }
        return this.mRootView;
    }
    
    @Override
    public void onDestroy() {
        this.seekBar = null;
        super.onDestroy();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        bundle.putInt(FragmentMusicShow.POS, this.count);
        super.onSaveInstanceState(bundle);
    }
    
    @Override
    public void onStart() {
        this.mApp.registerHandler(this.mMusicHandlerx);
        if (this.mApp.musicPlayControl == null) {
            UtilTools.sendKeyeventToSystem(4);
            this.mApp.musicPlayControl = new MusicPlayControl((Context)this.getActivity());
        }
        this.mApp.musicPlayControl.mHandler = this.mMusicHandlerx;
        if (this.needPlayIndex == this.mApp.musicPlayControl.musicIndex) {
            this.mApp.musicPlayControl.isMusicPlaying();
        }
        else if (this.needPlayIndex != -1) {
            this.mApp.musicPlayControl.playMusic(this.needPlayIndex);
        }
        this.mMusicHandlerx.post(this.updatePlayTimeRunnable);
        this.setCurrentPlayInfo();
        this.seclectTrue(this.count);
        super.onStart();
    }
    
    @Override
    public void onStop() {
        this.mApp.unregisterHandler(this.mMusicHandlerx);
        if (this.mApp.musicPlayControl != null) {
            this.mApp.musicPlayControl.mHandler = null;
        }
        this.mMusicHandlerx.removeCallbacks(this.updatePlayTimeRunnable);
        super.onStop();
    }
    
    public void prev() {
        this.mApp.musicPlayControl.ipause = false;
        this.mApp.musicPlayControl.playPreviousMusic();
        this.setCurrentPlayInfo();
    }
    
    static class MusicHandlerx extends Handler
    {
        private WeakReference<FragmentMusicShow> target;
        
        public MusicHandlerx(final FragmentMusicShow fragmentMusicShow) {
            this.target = new WeakReference<FragmentMusicShow>(fragmentMusicShow);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgx(message);
        }
    }
}
