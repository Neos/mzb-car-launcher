package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.touchus.publicutils.bean.*;
import java.util.*;
import android.graphics.*;
import java.io.*;
import com.squareup.picasso.*;
import android.content.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class FragmentPicShow extends BaseFragment implements View$OnClickListener
{
    private ImageView[] arr;
    private Bitmap bitmap;
    private int count;
    private float fScale;
    private ImageView fangda;
    Handler handler;
    private int indexCount;
    private boolean isBofang;
    private LauncherApplication mApp;
    private ImageView mBofangPic;
    private byte mIDRIVERENUM;
    private Bitmap mMBitmapda;
    private FrameLayout mMFl;
    private ImageView mMFliV;
    private Launcher mMMainActivity;
    private LinearLayout mMuenPic;
    public MusicLanyaHandler mMusicHandler;
    private TextView mPicName;
    private int mPosition;
    private View mRootView;
    private ImageView mShangPic;
    private ImageView mXiaPic;
    private int nBitmapWidth;
    private ImageView picCaidan;
    public List<MediaBean> picList;
    private ImageView suoxiao;
    TimerTask task;
    Timer timer;
    private int turnRotate;
    private String url;
    private ImageView youxuan;
    private ImageView zuoxuan;
    
    public FragmentPicShow() {
        this.count = 0;
        this.isBofang = false;
        this.turnRotate = 90;
        this.fScale = 1.0f;
        this.arr = new ImageView[8];
        this.handler = new Handler() {
            public void handleMessage(final Message message) {
                if (message.what == 1) {
                    if (FragmentPicShow.this.picList.size() == 0) {
                        return;
                    }
                    FragmentPicShow.this.pressItem(4);
                }
                super.handleMessage(message);
            }
        };
        this.timer = new Timer();
        this.task = new TimerTask() {
            @Override
            public void run() {
                final Message message = new Message();
                message.what = 1;
                FragmentPicShow.this.handler.sendMessage(message);
            }
        };
        this.mMusicHandler = new MusicLanyaHandler(this);
    }
    
    static /* synthetic */ void access$1(final FragmentPicShow fragmentPicShow, final int indexCount) {
        fragmentPicShow.indexCount = indexCount;
    }
    
    static /* synthetic */ void access$5(final FragmentPicShow fragmentPicShow, final int count) {
        fragmentPicShow.count = count;
    }
    
    public static int calculateInSampleSize(final BitmapFactory$Options bitmapFactory$Options, final int n, final int n2) {
        final int outHeight = bitmapFactory$Options.outHeight;
        final int outWidth = bitmapFactory$Options.outWidth;
        final int n3 = outHeight;
        final int n4 = outWidth;
        int n5 = 1;
        int n7;
        final int n6 = n7 = 1;
        int n8 = n3;
        int n9 = n4;
        if (n3 <= n2) {
            if (n4 <= n) {
                return n5;
            }
            n9 = n4;
            n8 = n3;
            n7 = n6;
        }
        while (true) {
            n5 = n7;
            if (n8 < n2) {
                break;
            }
            if (n9 < n) {
                n5 = n7;
                break;
            }
            ++n7;
            n8 = outHeight / n7;
            n9 = outWidth / n7;
        }
        return n5;
    }
    
    private void dealScale() {
        final Matrix matrix = new Matrix();
        matrix.setScale(this.fScale, this.fScale);
        this.mMFliV.setImageBitmap(Bitmap.createBitmap(this.bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), matrix, true));
    }
    
    public static Bitmap decodeSampledBitmapFromFile(final String s, final int n, final int n2) {
        final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(s, bitmapFactory$Options);
        bitmapFactory$Options.inSampleSize = calculateInSampleSize(bitmapFactory$Options, n, n2);
        bitmapFactory$Options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(s, bitmapFactory$Options);
    }
    
    private void hideFangDa() {
        if (this.fScale == 1.0f) {
            this.arr[0].setAlpha(0.5f);
            return;
        }
        this.arr[0].setAlpha(1.0f);
    }
    
    private void initData() {
        this.mPosition = this.getArguments().getInt("position");
        this.url = this.picList.get(this.mPosition).getData();
        final String title = this.picList.get(this.mPosition).getTitle();
        this.bitmap = decodeSampledBitmapFromFile(this.url, 799, 399);
        this.mMFliV.setImageBitmap(this.bitmap);
        this.mPicName.setText((CharSequence)title);
    }
    
    private void initListener() {
        this.picCaidan.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentPicShow.access$1(FragmentPicShow.this, 7);
                FragmentPicShow.this.seclectTrue(FragmentPicShow.this.indexCount);
                FragmentPicShow.this.pressItem(FragmentPicShow.this.indexCount);
            }
        });
        this.suoxiao.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentPicShow.access$1(FragmentPicShow.this, 1);
                FragmentPicShow.this.seclectTrue(FragmentPicShow.this.indexCount);
                FragmentPicShow.this.pressItem(FragmentPicShow.this.indexCount);
            }
        });
        this.fangda.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentPicShow.access$1(FragmentPicShow.this, 0);
                FragmentPicShow.this.seclectTrue(FragmentPicShow.this.indexCount);
                FragmentPicShow.this.pressItem(FragmentPicShow.this.indexCount);
            }
        });
        this.mBofangPic.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentPicShow.access$1(FragmentPicShow.this, 3);
                FragmentPicShow.this.seclectTrue(FragmentPicShow.this.indexCount);
                FragmentPicShow.this.pressItem(FragmentPicShow.this.indexCount);
            }
        });
        this.mXiaPic.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentPicShow.access$1(FragmentPicShow.this, 4);
                FragmentPicShow.this.seclectTrue(FragmentPicShow.this.indexCount);
                FragmentPicShow.this.pressItem(FragmentPicShow.this.indexCount);
            }
        });
        this.mShangPic.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                FragmentPicShow.access$1(FragmentPicShow.this, 2);
                FragmentPicShow.this.seclectTrue(FragmentPicShow.this.indexCount);
                FragmentPicShow.this.pressItem(FragmentPicShow.this.indexCount);
            }
        });
        this.mMFl.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                final FragmentPicShow this$0 = FragmentPicShow.this;
                FragmentPicShow.access$5(this$0, this$0.count + 1);
                if (FragmentPicShow.this.count % 2 == 0) {
                    FragmentPicShow.this.mMuenPic.setVisibility(8);
                    return;
                }
                FragmentPicShow.this.mMuenPic.setVisibility(0);
            }
        });
    }
    
    private void pressItem(int n) {
        switch (n) {
            case 0: {
                this.hideFangDa();
                if (this.bitmap != null) {
                    this.fScale += 0.2f;
                    if (this.fScale > 1.0f) {
                        this.fScale = 1.0f;
                    }
                    if (this.nBitmapWidth <= 0) {
                        this.nBitmapWidth = this.bitmap.getWidth();
                    }
                    this.dealScale();
                    return;
                }
                break;
            }
            case 1: {
                this.hideFangDa();
                if (this.bitmap != null) {
                    this.fScale -= 0.2f;
                    if (this.fScale < 0.2) {
                        this.fScale = 0.2f;
                    }
                    if (this.nBitmapWidth <= 0) {
                        this.nBitmapWidth = this.bitmap.getWidth();
                    }
                    this.dealScale();
                    return;
                }
                break;
            }
            case 2: {
                if (this.mPosition > 0) {
                    final List<MediaBean> picList = this.picList;
                    n = this.mPosition - 1;
                    this.mPosition = n;
                    final String data = picList.get(n).getData();
                    final File file = new File(data);
                    this.bitmap = decodeSampledBitmapFromFile(data, 799, 399);
                    Picasso.with((Context)this.getActivity()).load(file).into(this.mMFliV);
                    this.mPicName.setText((CharSequence)this.picList.get(this.mPosition).getTitle());
                    LauncherApplication.imageIndex = this.mPosition;
                    return;
                }
                break;
            }
            case 3: {
                if (!this.isBofang) {
                    if (this.task != null) {
                        this.task.cancel();
                        this.task = null;
                        this.task = new TimerTask() {
                            @Override
                            public void run() {
                                final Message message = new Message();
                                message.what = 1;
                                FragmentPicShow.this.handler.sendMessage(message);
                            }
                        };
                    }
                    this.timer.schedule(this.task, 1000L, 2000L);
                    this.isBofang = !this.isBofang;
                    this.mBofangPic.setImageResource(2130837973);
                    return;
                }
                this.task.cancel();
                this.isBofang = !this.isBofang;
                this.mBofangPic.setImageResource(2130837963);
            }
            case 4: {
                if (this.mPosition < this.picList.size() - 1) {
                    final List<MediaBean> picList2 = this.picList;
                    n = this.mPosition + 1;
                    this.mPosition = n;
                    final String data2 = picList2.get(n).getData();
                    this.mPicName.setText((CharSequence)this.picList.get(this.mPosition).getTitle());
                    this.bitmap = decodeSampledBitmapFromFile(data2, 799, 399);
                    Picasso.with((Context)this.getActivity()).load(new File(data2)).into(this.mMFliV);
                    LauncherApplication.imageIndex = this.mPosition;
                    return;
                }
                break;
            }
            case 5: {
                if (this.bitmap != null) {
                    this.turnRotate = -90;
                    this.bitmap = this.toturn(this.bitmap);
                    this.mMFliV.setImageBitmap(this.bitmap);
                    return;
                }
                break;
            }
            case 6: {
                if (this.bitmap != null) {
                    this.turnRotate = 90;
                    this.bitmap = this.toturn(this.bitmap);
                    this.mMFliV.setImageBitmap(this.bitmap);
                    return;
                }
                break;
            }
            case 7: {
                this.mMMainActivity.handleBackAction();
            }
        }
    }
    
    private void seclectTrue(final int n) {
        for (int i = 0; i < this.arr.length; ++i) {
            if (n == i) {
                this.arr[i].setSelected(true);
            }
            else {
                this.arr[i].setSelected(false);
            }
        }
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.mMuenPic.setVisibility(0);
                if (this.indexCount < 7) {
                    ++this.indexCount;
                }
                this.seclectTrue(this.indexCount);
            }
            else {
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.pressItem(this.indexCount);
                    return;
                }
                if (this.mIDRIVERENUM != Mainboard.EIdriverEnum.UP.getCode() && this.mIDRIVERENUM != Mainboard.EIdriverEnum.DOWN.getCode() && (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.LEFT.getCode())) {
                    this.mMuenPic.setVisibility(0);
                    if (this.indexCount > 0) {
                        --this.indexCount;
                    }
                    this.seclectTrue(this.indexCount);
                }
            }
        }
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427540) {
            this.indexCount = 5;
        }
        else if (view.getId() == 2131427541) {
            this.indexCount = 6;
        }
        this.seclectTrue(this.indexCount);
        this.pressItem(this.indexCount);
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2130903076, (ViewGroup)null);
        this.mMMainActivity = (Launcher)this.getActivity();
        (this.mApp = (LauncherApplication)this.mMMainActivity.getApplication()).registerHandler(this.mMusicHandler);
        this.mMFl = (FrameLayout)this.mRootView.findViewById(2131427532);
        this.mShangPic = (ImageView)this.mRootView.findViewById(2131427537);
        this.mXiaPic = (ImageView)this.mRootView.findViewById(2131427539);
        this.mBofangPic = (ImageView)this.mRootView.findViewById(2131427538);
        this.mMuenPic = (LinearLayout)this.mRootView.findViewById(2131427534);
        this.zuoxuan = (ImageView)this.mRootView.findViewById(2131427540);
        this.youxuan = (ImageView)this.mRootView.findViewById(2131427541);
        this.fangda = (ImageView)this.mRootView.findViewById(2131427535);
        this.suoxiao = (ImageView)this.mRootView.findViewById(2131427536);
        this.picCaidan = (ImageView)this.mRootView.findViewById(2131427542);
        this.mMFliV = (ImageView)this.mRootView.findViewById(2131427533);
        this.mPicName = (TextView)this.mRootView.findViewById(2131427543);
        this.arr[0] = this.fangda;
        this.arr[1] = this.suoxiao;
        this.arr[2] = this.mShangPic;
        this.arr[3] = this.mBofangPic;
        this.arr[4] = this.mXiaPic;
        this.arr[5] = this.zuoxuan;
        this.arr[6] = this.youxuan;
        this.arr[7] = this.picCaidan;
        this.initData();
        this.initListener();
        this.hideFangDa();
        this.zuoxuan.setOnClickListener((View$OnClickListener)this);
        this.youxuan.setOnClickListener((View$OnClickListener)this);
        return this.mRootView;
    }
    
    public void onDestroy() {
        this.mApp.unregisterHandler(this.mMusicHandler);
        if (this.task != null) {
            this.task.cancel();
        }
        super.onDestroy();
    }
    
    public Bitmap toturn(final Bitmap bitmap) {
        final Matrix matrix = new Matrix();
        matrix.reset();
        matrix.postRotate((float)this.turnRotate);
        return Bitmap.createBitmap(bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), matrix, true);
    }
    
    static class MusicLanyaHandler extends Handler
    {
        private WeakReference<FragmentPicShow> target;
        
        public MusicLanyaHandler(final FragmentPicShow fragmentPicShow) {
            this.target = new WeakReference<FragmentPicShow>(fragmentPicShow);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
