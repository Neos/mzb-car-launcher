package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.adapter.*;
import com.touchus.benchilauncher.*;
import com.touchus.publicutils.bean.*;
import android.content.*;
import java.util.*;
import com.touchus.benchilauncher.service.*;
import android.support.v4.app.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.widget.*;
import android.os.*;
import com.touchus.publicutils.utils.*;
import java.lang.ref.*;

public class FragmentMusicOutUSB extends BaseFragment implements AdapterView$OnItemClickListener
{
    boolean isUSB1;
    private MediaAdapter mAdapter;
    private LauncherApplication mApp;
    private Launcher mContext;
    private int mCount;
    private byte mIDRIVERENUM;
    private ListView mListView;
    public MusicLanyaHandler mMusicHandler;
    private View mRootView;
    private List<MediaBean> mediaBeans;
    private TextView noDataTv;
    private int type;
    
    public FragmentMusicOutUSB() {
        this.isUSB1 = false;
        this.type = 0;
        this.mCount = 0;
        this.mediaBeans = new ArrayList<MediaBean>();
        this.mMusicHandler = new MusicLanyaHandler(this);
    }
    
    private void artMusic() {
        if (this.getActivity() == null) {
            return;
        }
        if (this.mApp.musicPlayControl == null) {
            this.mApp.musicPlayControl = new MusicPlayControl((Context)this.getActivity());
        }
        if (LauncherApplication.musicList.size() == 0) {
            LauncherApplication.musicList = MusicLoader.instance(this.mContext.getContentResolver()).getMusicList();
        }
        this.mApp.musicPlayControl.setPlayList(LauncherApplication.musicList);
        this.mediaBeans.clear();
        this.mediaBeans.addAll(LauncherApplication.musicList);
        if (LauncherApplication.musicIndex >= this.mediaBeans.size()) {
            LauncherApplication.musicIndex = 0;
            LauncherApplication.mSpUtils.putInt("musicPos", 0);
        }
        this.mAdapter.setSeclectIndex(LauncherApplication.musicIndex);
        this.mListView.setSelection(LauncherApplication.musicIndex);
        this.mAdapter.notifyDataSetChanged(this.type);
    }
    
    private void artPic() {
        if (this.getActivity() == null) {
            return;
        }
        if (LauncherApplication.imageList.size() == 0) {
            LauncherApplication.imageList = new PictrueUtil((Context)this.mContext).getPicList();
        }
        this.mediaBeans.clear();
        this.mediaBeans.addAll(LauncherApplication.imageList);
        this.mAdapter.setSeclectIndex(LauncherApplication.imageIndex);
        this.mListView.setSelection(LauncherApplication.imageIndex);
        this.mAdapter.notifyDataSetChanged(this.type);
    }
    
    private void artVedio() {
        if (this.getActivity() == null) {
            return;
        }
        if (LauncherApplication.videoList.size() == 0) {
            LauncherApplication.videoList = new VideoService((Context)this.mContext).getVideoList();
        }
        this.mediaBeans.clear();
        this.mediaBeans.addAll(LauncherApplication.videoList);
        if (LauncherApplication.videoIndex >= this.mediaBeans.size()) {
            LauncherApplication.videoIndex = 0;
            LauncherApplication.mSpUtils.putInt("videoPos", 0);
        }
        this.mAdapter.setSeclectIndex(LauncherApplication.videoIndex);
        this.mListView.setSelection(LauncherApplication.videoIndex);
        this.mListView.smoothScrollToPositionFromTop(LauncherApplication.videoIndex, 0);
        this.mAdapter.notifyDataSetChanged(this.type);
    }
    
    private void changeToPicShowiew() {
        if (LauncherApplication.imageList.size() == 0) {
            return;
        }
        final FragmentPicShow fragmentPicShow = new FragmentPicShow();
        this.mContext.changeRightTo(fragmentPicShow);
        fragmentPicShow.picList = LauncherApplication.imageList;
        final Bundle arguments = new Bundle();
        arguments.putInt("position", LauncherApplication.imageIndex);
        fragmentPicShow.setArguments(arguments);
    }
    
    private void changeToPlayMusicView() {
        if (LauncherApplication.musicList.size() == 0) {
            return;
        }
        final FragmentMusicShow fragmentMusicShow = new FragmentMusicShow();
        this.mContext.changeRightTo(fragmentMusicShow);
        final Bundle arguments = new Bundle();
        arguments.putInt("position", LauncherApplication.musicIndex);
        fragmentMusicShow.setArguments(arguments);
    }
    
    private void changeToPlayVideoView() {
        if (LauncherApplication.videoList.size() == 0) {
            return;
        }
        final FragmentVideoPlay fragmentVideoPlay = new FragmentVideoPlay();
        fragmentVideoPlay.mediaBeans = this.mediaBeans;
        final Bundle arguments = new Bundle();
        arguments.putInt("position", LauncherApplication.videoIndex);
        fragmentVideoPlay.setArguments(arguments);
        this.mContext.changeToPlayVideo(fragmentVideoPlay);
    }
    
    private void initList() {
        if (LauncherApplication.menuSelectType == 1) {
            this.type = 1;
            this.artVedio();
        }
        else if (LauncherApplication.menuSelectType == 2) {
            this.type = 2;
            this.artPic();
        }
        else {
            this.type = 0;
            this.artMusic();
            if (LauncherApplication.iPlayingAuto && LauncherApplication.iPlaying) {
                this.changeToPlayMusicView();
            }
        }
        if (this.mediaBeans.size() == 0) {
            this.noDataTv.setVisibility(0);
            return;
        }
        this.noDataTv.setVisibility(8);
    }
    
    private void initView() {
        this.mListView = (ListView)this.mRootView.findViewById(2131427522);
        (this.noDataTv = (TextView)this.mRootView.findViewById(2131427468)).setText(this.getText(2131165304));
        this.mAdapter = new MediaAdapter((Context)this.mContext, this.mediaBeans);
        this.mListView.setAdapter((ListAdapter)this.mAdapter);
        this.mListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
    }
    
    public void down(int mCount) {
        final int lastVisiblePosition = this.mListView.getLastVisiblePosition();
        if (mCount == 0) {
            if (this.mCount < this.mediaBeans.size() - 1) {
                ++this.mCount;
            }
            mCount = this.mCount;
            if (mCount > lastVisiblePosition - 1) {
                this.mListView.smoothScrollToPositionFromTop(mCount, 0);
            }
        }
        else {
            final int firstVisiblePosition = this.mListView.getFirstVisiblePosition();
            this.mCount += mCount;
            if (this.mCount > this.mediaBeans.size() - 1) {
                this.mCount = this.mediaBeans.size() - 1;
            }
            this.mListView.smoothScrollToPositionFromTop(firstVisiblePosition + mCount, 0);
        }
        this.mAdapter.setSeclectIndex(this.mCount);
        this.mAdapter.notifyDataSetChanged(this.type);
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            if (this.type == 0) {
                this.mCount = LauncherApplication.musicIndex;
            }
            else if (this.type == 1) {
                this.mCount = LauncherApplication.videoIndex;
            }
            else if (this.type == 2) {
                this.mCount = LauncherApplication.imageIndex;
            }
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.DOWN.getCode()) {
                this.down(0);
            }
            else if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.UP.getCode()) {
                this.up(0);
            }
            else if (this.mIDRIVERENUM != Mainboard.EIdriverEnum.RIGHT.getCode() && this.mIDRIVERENUM != Mainboard.EIdriverEnum.LEFT.getCode() && this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                if (this.type == 0) {
                    this.changeToPlayMusicView();
                }
                else if (this.type == 1) {
                    this.changeToPlayVideoView();
                }
                else if (this.type == 2) {
                    this.changeToPicShowiew();
                }
            }
            if (this.type == 0) {
                LauncherApplication.musicIndex = this.mCount;
            }
            else {
                if (this.type == 1) {
                    LauncherApplication.videoIndex = this.mCount;
                    return;
                }
                if (this.type == 2) {
                    LauncherApplication.imageIndex = this.mCount;
                }
            }
        }
        else if (message.what == 7002) {
            final String string = message.getData().getString("usblistener");
            if (!"android.intent.action.MEDIA_CHECKING".equals(string) && !"android.intent.action.MEDIA_MOUNTED".equals(string)) {
                if ("android.intent.action.MEDIA_EJECT".equals(string)) {
                    this.mApp.service.removeLoadingFloatView();
                    this.mediaBeans.clear();
                    this.mAdapter.notifyDataSetChanged();
                    return;
                }
                if ("android.intent.action.MEDIA_SCANNER_STARTED".equals(string)) {
                    if (this.mApp.serviceHandler != null) {
                        this.mApp.service.createLoadingFloatView(this.getString(2131165295));
                    }
                }
                else if (!"android.intent.action.MEDIA_UNMOUNTED".equals(string) && "android.intent.action.MEDIA_SCANNER_FINISHED".equals(string)) {
                    this.mApp.service.removeLoadingFloatView();
                    this.initList();
                }
            }
        }
        else if (message.what == 1031) {
            this.initList();
        }
    }
    
    @Override
    public boolean onBack() {
        return LauncherApplication.iPlayingAuto = false;
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mRootView = layoutInflater.inflate(2130903074, (ViewGroup)null);
        this.mContext = (Launcher)this.getActivity();
        this.mApp = (LauncherApplication)this.mContext.getApplication();
        this.initView();
        return this.mRootView;
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int imageIndex, final long n) {
        view.setSelected(true);
        if (this.type == 0) {
            LauncherApplication.musicIndex = imageIndex;
            this.changeToPlayMusicView();
            return;
        }
        if (this.type == 1) {
            LauncherApplication.videoIndex = imageIndex;
            this.changeToPlayVideoView();
            return;
        }
        LauncherApplication.imageIndex = imageIndex;
        this.changeToPicShowiew();
    }
    
    public void onStart() {
        this.mApp.registerHandler(this.mMusicHandler);
        if (UtilTools.checkUSBExist() && this.mApp.isScanner) {
            this.mApp.service.createLoadingFloatView(this.getString(2131165295));
        }
        this.initList();
        super.onStart();
    }
    
    public void onStop() {
        this.mApp.unregisterHandler(this.mMusicHandler);
        super.onStop();
    }
    
    public void up(int mCount) {
        final int firstVisiblePosition = this.mListView.getFirstVisiblePosition();
        if (mCount == 0) {
            if (this.mCount > 0) {
                --this.mCount;
            }
            mCount = this.mCount;
            if (mCount < firstVisiblePosition + 1) {
                this.mListView.smoothScrollToPositionFromTop(mCount - 3, 0);
            }
        }
        else {
            final int lastVisiblePosition = this.mListView.getLastVisiblePosition();
            this.mCount += mCount;
            if (this.mCount < 0) {
                this.mCount = 0;
            }
            this.mListView.smoothScrollToPositionFromTop(lastVisiblePosition + mCount, 0);
        }
        this.mAdapter.setSeclectIndex(this.mCount);
        this.mAdapter.notifyDataSetChanged(this.type);
    }
    
    static class MusicLanyaHandler extends Handler
    {
        private WeakReference<FragmentMusicOutUSB> target;
        
        public MusicLanyaHandler(final FragmentMusicOutUSB fragmentMusicOutUSB) {
            this.target = new WeakReference<FragmentMusicOutUSB>(fragmentMusicOutUSB);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
