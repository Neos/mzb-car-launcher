package com.touchus.benchilauncher.fragment;

import com.touchus.benchilauncher.base.*;
import android.widget.*;
import com.touchus.benchilauncher.*;
import android.content.*;
import com.touchus.benchilauncher.views.*;
import com.backaudio.android.driver.beans.*;
import com.touchus.publicutils.sysconst.*;
import android.app.*;
import android.view.*;
import android.util.*;
import android.os.*;
import java.lang.ref.*;

public class YiBiaoFragment extends BaseFragment
{
    private ImageView car_body;
    private ImageView car_bonnet;
    private CarWarnInfoView car_brake;
    private ImageView car_headlamp;
    private ImageView car_lamplet;
    private ImageView car_lb_door;
    private ImageView car_lf_door;
    private CarWarnInfoView car_outsidetemp;
    private ImageView car_rb_door;
    private ImageView car_rearbox;
    private CarWarnInfoView car_remainkon;
    private ImageView car_rf_door;
    private CarWarnInfoView car_safety_belt;
    private ImageView car_stoplight;
    private boolean iUpdate;
    private Launcher launcher;
    private LauncherApplication mApp;
    private Context mContext;
    private View mView;
    private FragmentManager manager;
    private MeterHandler meterHandler;
    private float rSpeed;
    private float speed;
    private float suduAngle;
    private YiBiaoView suduYiBiao;
    private float zhuanSAngle;
    private YiBiaoView zhuanSuYiBiao;
    
    public YiBiaoFragment() {
        this.suduAngle = -129.0f;
        this.zhuanSAngle = -143.0f;
        this.speed = 0.0f;
        this.rSpeed = 0.0f;
        this.meterHandler = new MeterHandler(this);
    }
    
    private void handlerMsg(final Message message) {
        if (this.iUpdate) {
            if (message.what == 1008) {
                final CarRunInfo carRunInfo = (CarRunInfo)message.getData().getParcelable("runningState");
                if (carRunInfo != null) {
                    this.speed = carRunInfo.getCurSpeed();
                    this.rSpeed = carRunInfo.getRevolutions();
                    this.speedToAngle();
                    this.setYiBiaoAngle();
                    this.settingRunningInfo(carRunInfo, null);
                }
            }
            else if (message.what == 1007) {
                this.settingRunningInfo(null, (CarBaseInfo)message.getData().getParcelable("carBaseInfo"));
            }
        }
    }
    
    private void initData() {
        this.setYiBiaoAngle();
    }
    
    private void initView() {
        this.suduYiBiao = (YiBiaoView)this.mView.findViewById(2131427568);
        this.zhuanSuYiBiao = (YiBiaoView)this.mView.findViewById(2131427569);
        this.car_body = (ImageView)this.mView.findViewById(2131427497);
        this.car_headlamp = (ImageView)this.mView.findViewById(2131427495);
        this.car_lamplet = (ImageView)this.mView.findViewById(2131427496);
        this.car_bonnet = (ImageView)this.mView.findViewById(2131427498);
        this.car_lf_door = (ImageView)this.mView.findViewById(2131427499);
        this.car_rf_door = (ImageView)this.mView.findViewById(2131427500);
        this.car_lb_door = (ImageView)this.mView.findViewById(2131427501);
        this.car_rb_door = (ImageView)this.mView.findViewById(2131427502);
        this.car_rearbox = (ImageView)this.mView.findViewById(2131427503);
        this.car_stoplight = (ImageView)this.mView.findViewById(2131427504);
        this.car_remainkon = (CarWarnInfoView)this.mView.findViewById(2131427505);
        this.car_safety_belt = (CarWarnInfoView)this.mView.findViewById(2131427506);
        this.car_outsidetemp = (CarWarnInfoView)this.mView.findViewById(2131427507);
        this.car_brake = (CarWarnInfoView)this.mView.findViewById(2131427508);
        this.car_remainkon.setCarInfo(false, 2130837675, String.format(this.getString(2131165301), 0));
        this.car_brake.setCarInfo(true, 2130837664, this.getString(2131165297));
        this.car_safety_belt.setCarInfo(false, 2130837678, this.getString(2131165300));
        this.setImageState(this.car_headlamp, false);
        this.setImageState(this.car_lamplet, false);
        this.setImageState(this.car_bonnet, false);
        this.setImageState(this.car_lf_door, false);
        this.setImageState(this.car_rf_door, false);
        this.setImageState(this.car_lb_door, false);
        this.setImageState(this.car_rb_door, false);
        this.setImageState(this.car_rearbox, false);
        this.setImageState(this.car_stoplight, false);
        if (BenzModel.benzCan == BenzModel.EBenzCAN.XBS && BenzModel.benzTpye != BenzModel.EBenzTpye.GLA) {
            this.car_remainkon.setVisibility(0);
            this.car_safety_belt.setVisibility(8);
            return;
        }
        this.car_remainkon.setVisibility(8);
        this.car_safety_belt.setVisibility(0);
    }
    
    private void setBaseinfo(final CarBaseInfo carBaseInfo) {
        if (carBaseInfo.isiBrake()) {
            this.car_brake.setCarInfo(true, 2130837664, this.getString(2131165297));
        }
        else {
            this.car_brake.setCarInfo(false, 2130837665, this.getString(2131165298));
        }
        if (carBaseInfo.isiSafetyBelt()) {
            this.car_safety_belt.setCarInfo(true, 2130837677, this.getString(2131165300));
        }
        else {
            this.car_safety_belt.setCarInfo(false, 2130837678, this.getString(2131165299));
        }
        this.setImageState(this.car_headlamp, carBaseInfo.isiDistantLightOpen());
        this.setImageState(this.car_lamplet, carBaseInfo.isiNearLightOpen());
        this.setImageState(this.car_bonnet, carBaseInfo.isiFront());
        this.setImageState(this.car_lf_door, carBaseInfo.isiLeftFrontOpen());
        this.setImageState(this.car_rf_door, carBaseInfo.isiRightFrontOpen());
        this.setImageState(this.car_lb_door, carBaseInfo.isiLeftBackOpen());
        this.setImageState(this.car_rb_door, carBaseInfo.isiRightBackOpen());
        this.setImageState(this.car_rearbox, carBaseInfo.isiBack());
    }
    
    private void setImageState(final ImageView imageView, final boolean b) {
        if (b) {
            imageView.setVisibility(0);
            return;
        }
        imageView.setVisibility(4);
    }
    
    private void setRuningInfo(final CarRunInfo carRunInfo) {
        if (carRunInfo.getMileage() >= 2047 || carRunInfo.getMileage() <= 0) {
            this.car_remainkon.setCarInfo(false, 2130837675, String.format(this.getString(2131165301), "----"));
            return;
        }
        this.car_remainkon.setCarInfo(false, 2130837675, String.format(this.getString(2131165301), carRunInfo.getMileage()));
    }
    
    private void setSpeed() {
        this.speed = this.mApp.speed;
        this.rSpeed = this.mApp.rSpeed;
        this.speedToAngle();
        this.setYiBiaoAngle();
        this.settingRunningInfo(this.mApp.carRunInfo, this.mApp.carBaseInfo);
    }
    
    private void setYiBiaoAngle() {
        this.suduYiBiao.setAngle(this.suduAngle);
        this.zhuanSuYiBiao.setAngle(this.zhuanSAngle);
    }
    
    private void settingRunningInfo(final CarRunInfo runingInfo, final CarBaseInfo baseinfo) {
        if (this.getActivity() != null) {
            if (runingInfo != null) {
                this.setRuningInfo(runingInfo);
            }
            if (baseinfo != null) {
                this.setBaseinfo(baseinfo);
            }
        }
    }
    
    private void speedToAngle() {
        this.suduAngle = this.speed - 130.0f;
        if (this.rSpeed >= 0.0f && this.rSpeed <= 8000.0f) {
            this.zhuanSAngle = this.rSpeed * 360.0f / 10000.0f - 144.0f;
            return;
        }
        this.zhuanSAngle = -143.0f;
    }
    
    public void changeFragment(final int n, final android.app.Fragment fragment) {
        this.manager = this.launcher.getFragmentManager();
        final FragmentTransaction beginTransaction = this.manager.beginTransaction();
        beginTransaction.replace(n, fragment);
        beginTransaction.commit();
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Context)this.getActivity();
        this.launcher = (Launcher)this.getActivity();
        this.mApp = (LauncherApplication)this.launcher.getApplication();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Log.e("", String.valueOf(this.getClass().getName()) + " onCreateView ");
        this.mView = layoutInflater.inflate(2130903080, (ViewGroup)null);
        this.initView();
        return this.mView;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public void onHiddenChanged(final boolean b) {
        Log.e("", String.valueOf(this.getClass().getName()) + " onHiddenChanged = " + b);
        if (b) {
            this.iUpdate = false;
            this.mApp.unregisterHandler(this.meterHandler);
            this.setSpeed();
        }
        else {
            this.iUpdate = true;
            this.mApp.registerHandler(this.meterHandler);
            if (this.mApp.isSUV) {
                this.car_body.setImageResource(2130837919);
            }
            else {
                this.car_body.setImageResource(2130837686);
            }
        }
        super.onHiddenChanged(b);
    }
    
    @Override
    public void onResume() {
        Log.e("", String.valueOf(this.getClass().getName()) + " onResume ");
        super.onResume();
        this.initData();
    }
    
    @Override
    public void onStart() {
        Log.e("", String.valueOf(this.getClass().getName()) + " onStart ");
        if (this.mApp.isSUV) {
            this.car_body.setImageResource(2130837919);
        }
        else {
            this.car_body.setImageResource(2130837686);
        }
        super.onStart();
    }
    
    @Override
    public void onStop() {
        Log.e("", String.valueOf(this.getClass().getName()) + " onStop = ");
        super.onStop();
    }
    
    public static class MeterHandler extends Handler
    {
        private WeakReference<YiBiaoFragment> target;
        
        public MeterHandler(final YiBiaoFragment yiBiaoFragment) {
            this.target = new WeakReference<YiBiaoFragment>(yiBiaoFragment);
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
