package com.touchus.benchilauncher;

import com.touchus.publicutils.bean.*;
import com.backaudio.android.driver.beans.*;
import com.backaudio.android.driver.*;
import java.util.concurrent.atomic.*;
import cn.kuwo.autosdk.api.*;
import com.touchus.benchilauncher.service.*;
import org.slf4j.*;
import android.util.*;
import com.touchus.benchilauncher.views.*;
import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import de.mindpipe.android.logging.log4j.*;
import java.io.*;
import org.apache.log4j.*;
import android.media.*;
import android.provider.*;
import android.preference.*;
import android.text.*;
import com.touchus.publicutils.sysconst.*;
import com.touchus.publicutils.utils.*;
import com.touchus.benchilauncher.utils.*;
import android.os.*;
import com.touchus.benchilauncher.activity.main.left.*;
import com.touchus.benchilauncher.fragment.*;
import java.util.*;
import android.content.*;
import com.backaudio.android.driver.bluetooth.*;
import android.app.*;
import android.content.res.*;

public class LauncherApplication extends Application
{
    private static String FLAG_CITY_CODE;
    private static String FLAG_DEVICE_NAME;
    private static String FLAG_LAST_ENTER_PAIR_STATE;
    private static String FLAG_NAVI_APP;
    private static String FLAG_NEED_AUTO_START_NAVI;
    private static String FLAG_WIFI_AP_ID_KEY;
    private static String FLAG_WIFI_AP_STATE;
    private static String FLAG_WIFI_HAS_PWD;
    public static boolean iAccOff;
    public static boolean iPlaying;
    public static boolean iPlayingAuto;
    public static int imageIndex;
    public static List<MediaBean> imageList;
    public static boolean isBT;
    public static boolean isBlueConnectState;
    public static boolean isGPSLocation;
    public static Context mContext;
    public static LauncherApplication mInstance;
    public static SpUtilK mSpUtils;
    public static int menuSelectType;
    public static int musicIndex;
    public static List<MediaBean> musicList;
    public static int pageCount;
    public static boolean shutDoorNeedShowYibiao;
    public static int videoIndex;
    public static List<MediaBean> videoList;
    public final int BluetoothMusicType;
    public String address;
    public AirInfo airInfo;
    public int breakpos;
    public boolean btConnectDialog;
    public BluetoothService btservice;
    public CarBaseInfo carBaseInfo;
    public CarRunInfo carRunInfo;
    public int connectPos;
    private int countMusic;
    private int countMusicBluetooth;
    private int countRing;
    public String curCanboxVersion;
    public String curMcuVersion;
    public String currentCityName;
    public Dialog currentDialog1;
    public Dialog currentDialog2;
    public Dialog currentDialog3;
    public Dialog currentDialog4;
    public int day;
    public Mainboard.EReverserMediaSet eMediaSet;
    public String gpsCityName;
    public int hour;
    public boolean iCurrentInLauncher;
    public AtomicBoolean iIsScreenClose;
    public boolean iIsYibiaoShowing;
    public int iLanguageType;
    public boolean iMedeaDeviceClose;
    public boolean isAirhide;
    private boolean isBluetoothMusicMute;
    public boolean isCallDialog;
    public boolean isCalling;
    public boolean isComeFromRecord;
    public boolean isEasyBtConnect;
    public boolean isEasyView;
    public boolean isOriginalKeyOpen;
    public boolean isPhoneNumFromRecord;
    public boolean isSUV;
    public boolean isScanner;
    private boolean isStreamMusicMute;
    private boolean isStreamRingMute;
    public boolean isTestOpen;
    public boolean ismix;
    public boolean istop;
    public KWAPI kwapi;
    public Handler launcherHandler;
    Logger logger;
    private AudioManager$OnAudioFocusChangeListener mDVAudioFocusListener;
    public Handler mHandler;
    private ArrayList<Handler> mHandlers;
    public boolean mHomeAndBackEnable;
    private AudioManager$OnAudioFocusChangeListener mMainAudioFocusListener;
    public Handler mOriginalViewHandler;
    public int min;
    public int month;
    public MusicPlayControl musicPlayControl;
    public int musicType;
    public int naviPos;
    public String phoneName;
    public String phoneNumber;
    public int rSpeed;
    public int radioPos;
    public String recorPhoneNumber;
    public int screenPos;
    public int second;
    public MainService service;
    public Handler serviceHandler;
    private SharedPreferences share;
    public int speed;
    public int timeFormat;
    public int usbPos;
    public String weatherTemp;
    public String weatherTempDes;
    public int year;
    
    static {
        LauncherApplication.shutDoorNeedShowYibiao = false;
        LauncherApplication.FLAG_CITY_CODE = "cityCode";
        LauncherApplication.isBlueConnectState = false;
        LauncherApplication.isGPSLocation = false;
        LauncherApplication.musicList = new ArrayList<MediaBean>();
        LauncherApplication.videoList = new ArrayList<MediaBean>();
        LauncherApplication.imageList = new ArrayList<MediaBean>();
        LauncherApplication.iPlaying = false;
        LauncherApplication.iPlayingAuto = false;
        LauncherApplication.menuSelectType = -1;
        LauncherApplication.iAccOff = false;
        LauncherApplication.isBT = true;
        LauncherApplication.pageCount = 3;
        LauncherApplication.FLAG_LAST_ENTER_PAIR_STATE = "needEnterPair";
        LauncherApplication.FLAG_NEED_AUTO_START_NAVI = "needAutoStartNavi";
        LauncherApplication.FLAG_NAVI_APP = "naviapp";
        LauncherApplication.FLAG_WIFI_AP_STATE = "wifiApState";
        LauncherApplication.FLAG_WIFI_AP_ID_KEY = "wifiApIdAndKey";
        LauncherApplication.FLAG_WIFI_HAS_PWD = "hadpwd";
        LauncherApplication.FLAG_DEVICE_NAME = "lastDevice";
    }
    
    public LauncherApplication() {
        this.logger = LoggerFactory.getLogger(LauncherApplication.class);
        this.phoneNumber = "";
        this.phoneName = "";
        this.recorPhoneNumber = "";
        this.isPhoneNumFromRecord = false;
        this.isComeFromRecord = false;
        this.iCurrentInLauncher = true;
        this.mHomeAndBackEnable = true;
        this.curMcuVersion = "";
        this.curCanboxVersion = "";
        this.iLanguageType = 0;
        this.eMediaSet = Mainboard.EReverserMediaSet.MUTE;
        this.isOriginalKeyOpen = false;
        this.isAirhide = false;
        this.isTestOpen = false;
        this.year = 2017;
        this.month = 5;
        this.day = 20;
        this.timeFormat = 24;
        this.radioPos = 0;
        this.naviPos = 0;
        this.usbPos = 0;
        this.connectPos = 0;
        this.screenPos = 0;
        this.breakpos = 0;
        this.isCallDialog = false;
        this.currentDialog1 = null;
        this.currentDialog2 = null;
        this.currentDialog3 = null;
        this.currentDialog4 = null;
        this.btConnectDialog = false;
        this.musicPlayControl = null;
        this.isCalling = false;
        this.isEasyBtConnect = false;
        this.isEasyView = false;
        this.isSUV = false;
        this.mHandler = new Handler() {
            public void handleMessage(final Message message) {
                Log.e("", "FloatSystemCallDialog handleMessage  msg.what:" + message.what);
                switch (message.what) {
                    case 1415: {
                        LauncherApplication.this.interAndroidView();
                        LauncherApplication.this.isCalling = true;
                        if (BluetoothService.currentCallingType != 1412) {
                            break;
                        }
                        final FloatSystemCallDialog instance = FloatSystemCallDialog.getInstance();
                        if (instance.getShowStatus() == FloatSystemCallDialog.FloatShowST.INCOMING && LauncherApplication.this.phoneNumber != null && !LauncherApplication.this.phoneNumber.equals(instance.getCallingPhonenumber())) {
                            instance.setCallingPhonenumber(LauncherApplication.this.phoneNumber);
                            instance.clearView();
                            LauncherApplication.this.btservice.enterSystemFloatCallView();
                            return;
                        }
                        break;
                    }
                    case 1411: {
                        LauncherApplication.this.isCalling = true;
                        FloatSystemCallDialog.getInstance().updateTalkStatus();
                    }
                    case 1417: {
                        if (BluetoothService.talkingflag) {
                            LauncherApplication.this.isCalling = true;
                        }
                        else {
                            LauncherApplication.this.isCalling = false;
                        }
                        if (FloatSystemCallDialog.getInstance().getShowStatus() == FloatSystemCallDialog.FloatShowST.TALKING) {
                            FloatSystemCallDialog.getInstance().freshCallingTime();
                            return;
                        }
                        break;
                    }
                    case 1416: {
                        LauncherApplication.this.isPhoneNumFromRecord = false;
                        LauncherApplication.this.isCalling = false;
                        Mainboard.getInstance().openOrCloseRelay(false);
                    }
                    case 6002: {
                        if (LauncherApplication.this.isEasyView) {
                            if (LauncherApplication.isBlueConnectState && !LauncherApplication.this.isEasyBtConnect) {
                                LauncherApplication.this.isEasyBtConnect = true;
                                LauncherApplication.this.service.sendEasyConnectBroadcast("net.easyconn.bt.connected");
                                LauncherApplication.this.btservice.requestMusicAudioFocus();
                            }
                            else if (!LauncherApplication.isBlueConnectState && LauncherApplication.this.isEasyBtConnect) {
                                LauncherApplication.this.isEasyBtConnect = false;
                                LauncherApplication.this.openOrCloseBluetooth(true);
                                LauncherApplication.this.service.sendEasyConnectBroadcast("net.easyconn.bt.opened");
                                LauncherApplication.this.service.sendEasyConnectBroadcast("net.easyconn.bt.unconnected");
                                LauncherApplication.this.btservice.stopBTMusic();
                            }
                        }
                        LauncherApplication.this.isCalling = false;
                        if (!FloatSystemCallDialog.getInstance().isDestory()) {
                            FloatSystemCallDialog.getInstance().dissShow();
                            FloatSystemCallDialog.getInstance().clearView();
                            return;
                        }
                        break;
                    }
                    case 1422: {
                        if (BluetoothService.bluetoothStatus != EPhoneStatus.INCOMING_CALL && BluetoothService.bluetoothStatus != EPhoneStatus.TALKING && BluetoothService.bluetoothStatus != EPhoneStatus.CALLING_OUT) {
                            break;
                        }
                        if (LauncherApplication.this.btservice != null && !LauncherApplication.this.service.iIsInReversing) {
                            LauncherApplication.this.btservice.enterSystemFloatCallView();
                            return;
                        }
                        FloatSystemCallDialog.getInstance().clearView();
                    }
                }
            }
        };
        this.istop = false;
        this.isScanner = false;
        this.btservice = null;
        this.musicType = 0;
        this.iIsYibiaoShowing = false;
        this.mHandlers = new ArrayList<Handler>();
        this.iIsScreenClose = new AtomicBoolean(false);
        this.iMedeaDeviceClose = false;
        this.weatherTempDes = "";
        this.weatherTemp = "";
        this.currentCityName = "";
        this.mMainAudioFocusListener = (AudioManager$OnAudioFocusChangeListener)new AudioManager$OnAudioFocusChangeListener() {
            public void onAudioFocusChange(final int n) {
                switch (n) {
                    default: {}
                    case -1: {
                        LauncherApplication.this.logger.debug("requestAudioFocus main LOSS");
                    }
                    case -3:
                    case -2: {
                        LauncherApplication.this.logger.debug("requestAudioFocus main CAN_DUCK");
                    }
                    case 1: {
                        LauncherApplication.this.logger.debug("requestAudioFocus main GAIN");
                    }
                }
            }
        };
        this.mDVAudioFocusListener = (AudioManager$OnAudioFocusChangeListener)new AudioManager$OnAudioFocusChangeListener() {
            public void onAudioFocusChange(final int n) {
                switch (n) {
                    default: {}
                    case -1: {
                        LauncherApplication.this.logger.debug("requestAudioFocus DV LOSS");
                    }
                    case -3:
                    case -2: {
                        LauncherApplication.this.logger.debug("requestAudioFocus DV CAN_DUCK");
                    }
                    case 1: {
                        LauncherApplication.this.logger.debug("requestAudioFocus DV GAIN");
                    }
                }
            }
        };
        this.isBluetoothMusicMute = false;
        this.isStreamMusicMute = false;
        this.isStreamRingMute = false;
        this.BluetoothMusicType = 200;
        this.countMusic = 0;
        this.countMusicBluetooth = 0;
        this.countRing = 0;
    }
    
    private void closeScreen() {
        if (this.iIsScreenClose.compareAndSet(false, true)) {
            Mainboard.getInstance().closeOrOpenScreen(true);
        }
    }
    
    public static Context getContext() {
        return LauncherApplication.mContext;
    }
    
    public static LauncherApplication getInstance() {
        return LauncherApplication.mInstance;
    }
    
    private void initData() {
        LauncherApplication.musicIndex = LauncherApplication.mSpUtils.getInt("musicPos", 0);
        LauncherApplication.videoIndex = LauncherApplication.mSpUtils.getInt("videoPos", 0);
        Log.e("LauncherApplication", "musicIndex = " + LauncherApplication.musicIndex + ",videoIndex = " + LauncherApplication.videoIndex);
    }
    
    private void initLogger() {
        try {
            final LogConfigurator logConfigurator = new LogConfigurator();
            logConfigurator.setFileName(this.getApplicationContext().getFilesDir() + File.separator + "log.txt");
            logConfigurator.setRootLevel(Level.DEBUG);
            logConfigurator.setLevel("org.apache", Level.ERROR);
            logConfigurator.setFilePattern("%d - %F:%L - %m  [%t]%n");
            logConfigurator.setMaxFileSize(2097152L);
            logConfigurator.setMaxBackupSize(2);
            logConfigurator.setImmediateFlush(true);
            logConfigurator.configure();
            LoggerFactory.getLogger(LauncherApplication.class).info("init log4j");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void wakeupScreen() {
        if (this.iIsScreenClose.compareAndSet(true, false)) {
            Mainboard.getInstance().closeOrOpenScreen(false);
        }
    }
    
    public void abandonDVAudioFocus() {
        ((AudioManager)this.getSystemService("audio")).abandonAudioFocus(this.mDVAudioFocusListener);
        if (this.musicType == 4) {
            this.musicType = 0;
        }
    }
    
    public void abandonMainAudioFocus() {
        ((AudioManager)this.getSystemService("audio")).abandonAudioFocus(this.mMainAudioFocusListener);
    }
    
    public void changeAirplane(final boolean b) {
        final ContentResolver contentResolver = this.getContentResolver();
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 0;
        }
        Settings$Global.putInt(contentResolver, "airplane_mode_on", n);
        final Intent intent = new Intent("android.intent.action.AIRPLANE_MODE");
        intent.putExtra("state", b);
        this.sendBroadcastAsUser(intent, UserHandle.ALL);
    }
    
    public void closeOrWakeupScreen(final boolean b) {
        // monitorenter(this)
        Label_0013: {
            if (!b) {
                break Label_0013;
            }
            try {
                this.closeScreen();
                return;
                this.wakeupScreen();
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    public void dismissDialog() {
        if (this.currentDialog3 != null && this.currentDialog3.isShowing()) {
            this.currentDialog3.dismiss();
        }
        if (this.currentDialog2 != null && this.currentDialog2.isShowing()) {
            this.currentDialog2.dismiss();
        }
        if (this.currentDialog1 != null && this.currentDialog1.isShowing()) {
            this.currentDialog1.dismiss();
        }
        if (this.currentDialog4 != null && this.currentDialog4.isShowing()) {
            this.currentDialog4.dismiss();
        }
    }
    
    public void getLanguage() {
        final String string = Locale.getDefault().toString();
        if (string.contains("zh_CN")) {
            LauncherApplication.mSpUtils.putInt("languageType", 0);
            Mainboard.getInstance().sendLanguageSetToMcu(0);
            return;
        }
        if (string.contains("zh_TW")) {
            LauncherApplication.mSpUtils.putInt("languageType", 1);
            Mainboard.getInstance().sendLanguageSetToMcu(1);
            return;
        }
        LauncherApplication.mSpUtils.putInt("languageType", 2);
        Mainboard.getInstance().sendLanguageSetToMcu(2);
    }
    
    public String getNaviAPP() {
        return this.share.getString(LauncherApplication.FLAG_NAVI_APP, this.getString(2131165188));
    }
    
    public boolean getNeedStartNaviPkgs() {
        return this.share.getBoolean(LauncherApplication.FLAG_NEED_AUTO_START_NAVI, false);
    }
    
    public boolean getNeedToEnterPairMode() {
        return this.share.getBoolean(LauncherApplication.FLAG_LAST_ENTER_PAIR_STATE, false);
    }
    
    public boolean getWifiApHasPwd() {
        return this.share.getBoolean(LauncherApplication.FLAG_WIFI_HAS_PWD, true);
    }
    
    public String[] getWifiApIdAndKey() {
        final String string = this.share.getString(LauncherApplication.FLAG_WIFI_AP_ID_KEY, "11111111;" + (Object)this.getText(2131165212));
        final int index = string.indexOf(";");
        return new String[] { string.substring(index + 1), string.substring(0, index) };
    }
    
    public boolean getWifiApState() {
        return this.share.getBoolean(LauncherApplication.FLAG_WIFI_AP_STATE, false);
    }
    
    public boolean iNeedToChangeLocalContacts(final String s) {
        final String string = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()).getString(LauncherApplication.FLAG_DEVICE_NAME, "");
        return !TextUtils.isEmpty((CharSequence)string) && string.equals(s);
    }
    
    public void initModel() {
        final int n = 0;
        final int int1 = LauncherApplication.mSpUtils.getInt(BenzModel.KEY, 0);
        final BenzModel.EBenzTpye[] values = BenzModel.EBenzTpye.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            final BenzModel.EBenzTpye benzTpye = values[i];
            if (int1 == benzTpye.getCode()) {
                BenzModel.benzTpye = benzTpye;
                break;
            }
        }
        final int int2 = LauncherApplication.mSpUtils.getInt(BenzModel.SIZE_KEY, 0);
        final BenzModel.EBenzSize[] values2 = BenzModel.EBenzSize.values();
        for (int length2 = values2.length, j = n; j < length2; ++j) {
            final BenzModel.EBenzSize benzSize = values2[j];
            if (int2 == benzSize.getCode()) {
                BenzModel.benzSize = benzSize;
                break;
            }
        }
        this.isSUV = BenzModel.isSUV();
        if ("benz".equalsIgnoreCase(Build.MODEL) || "ajbenz".equalsIgnoreCase(Build.MODEL) || "c200_jly".equalsIgnoreCase(Build.MODEL) || "c200_jly_tw".equalsIgnoreCase(Build.MODEL) || "c200_psr".equalsIgnoreCase(Build.MODEL)) {
            BenzModel.benzCan = BenzModel.EBenzCAN.XBS;
        }
    }
    
    public void interAndroidView() {
        Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.ANDROID);
    }
    
    public void interBTConnectView() {
        Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.ORIGINAL);
    }
    
    public void interOriginalView() {
        Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.ORIGINAL);
    }
    
    public void onCreate() {
        CrashHandler.getInstance(this.getApplicationContext());
        this.initLogger();
        LauncherApplication.mContext = this.getApplicationContext();
        if (LauncherApplication.mInstance == null) {
            LauncherApplication.mInstance = this;
        }
        this.registerHandler(this.mHandler);
        LauncherApplication.mSpUtils = new SpUtilK(getContext());
        this.share = PreferenceManager.getDefaultSharedPreferences((Context)this);
        this.initModel();
        this.kwapi = KWAPI.createKWAPI((Context)this, "auto");
        if (this.getWifiApIdAndKey()[0].equals(this.getText(2131165212)) && this.getWifiApIdAndKey()[1].equals("11111111")) {
            WifiTool.setDefaultWifiAp((Context)this);
        }
        this.initData();
        super.onCreate();
    }
    
    public void onTerminate() {
        this.unregisterHandler(this.mHandler);
        super.onTerminate();
    }
    
    public void openOrCloseBluetooth(final boolean b) {
        if (this.btservice == null || this.mHandler == null) {
            return;
        }
        if (b) {
            this.btservice.enterPairingMode();
            return;
        }
        this.btservice.leavePairingMode();
    }
    
    public void registerHandler(final Handler handler) {
        if (!this.mHandlers.contains(handler)) {
            this.mHandlers.add(handler);
        }
        Log.d("launcherlog", "registerHandler =" + handler.getClass());
    }
    
    public void requestDVAudioFocus() {
        this.musicType = 4;
        Log.e("Main requestDVAudioFocus", "flag" + ((AudioManager)this.getSystemService("audio")).requestAudioFocus(this.mDVAudioFocusListener, 3, 1));
    }
    
    public void requestMainAudioFocus() {
        this.musicType = 0;
        Log.e("Main requestAudioFocus", "flag" + ((AudioManager)this.getSystemService("audio")).requestAudioFocus(this.mMainAudioFocusListener, 3, 1));
    }
    
    public void sendHideOrShowLocateViewEvent(final boolean b) {
        String s;
        if (b) {
            s = "com.touchus.showlocatefloat";
        }
        else {
            s = "com.touchus.hidelocatefloat";
        }
        this.sendBroadcast(new Intent(s));
    }
    
    public void sendHideOrShowNavigationBarEvent(final boolean b) {
        // monitorenter(this)
        Label_0025: {
            if (!b) {
                break Label_0025;
            }
            String s = "SHOW_NAVIGATION_BAR";
            try {
                while (true) {
                    this.sendBroadcast(new Intent(s));
                    return;
                    s = "HIDE_NAVIGATION_BAR";
                    continue;
                }
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    public void sendMessage(final int n, final Bundle bundle) {
        if (!LauncherApplication.iAccOff || n == 1007) {
            Log.d("templog", "mHandlers.size=" + this.mHandlers.size());
            if (n == 6001 && this.mHandlers.size() > 1) {
                final byte byte1 = bundle.getByte("idriver_enum");
                if (this.iIsScreenClose.get() && byte1 != Mainboard.EIdriverEnum.POWER_OFF.getCode()) {
                    this.wakeupScreen();
                    return;
                }
                if (byte1 != Mainboard.EIdriverEnum.BACK.getCode() && byte1 != Mainboard.EIdriverEnum.HOME.getCode() && byte1 != Mainboard.EIdriverEnum.STAR_BTN.getCode() && byte1 != Mainboard.EIdriverEnum.BT.getCode() && byte1 != Mainboard.EIdriverEnum.NAVI.getCode() && byte1 != Mainboard.EIdriverEnum.RADIO.getCode() && byte1 != Mainboard.EIdriverEnum.CARSET.getCode() && byte1 != Mainboard.EIdriverEnum.MEDIA.getCode() && byte1 != Mainboard.EIdriverEnum.HANG_UP.getCode() && byte1 != Mainboard.EIdriverEnum.PICK_UP.getCode()) {
                    if (this.iIsYibiaoShowing) {
                        return;
                    }
                    Handler handler = null;
                    Handler handler2;
                    for (int i = this.mHandlers.size() - 1; i >= 0; --i, handler = handler2) {
                        handler2 = this.mHandlers.get(i);
                        if ((!this.isCallDialog || handler2 instanceof FloatSystemCallDialog.BluetoothHandler) && !(handler2 instanceof RunningFrag.RunningHandler)) {
                            handler = handler2;
                            if (!(handler2 instanceof YiBiaoFragment.MeterHandler)) {
                                break;
                            }
                        }
                    }
                    Log.d("templog", "mHandlers.size=" + handler.toString());
                    if (handler != null) {
                        final Message obtainMessage = handler.obtainMessage(n);
                        obtainMessage.setData(bundle);
                        obtainMessage.sendToTarget();
                        return;
                    }
                }
            }
            final Iterator<Handler> iterator = this.mHandlers.iterator();
            while (iterator.hasNext()) {
                final Message obtainMessage2 = iterator.next().obtainMessage(n);
                obtainMessage2.setData(bundle);
                obtainMessage2.sendToTarget();
            }
        }
    }
    
    public void setLastDeviceName(final String s) {
        final SharedPreferences$Editor edit = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()).edit();
        edit.putString(LauncherApplication.FLAG_DEVICE_NAME, s);
        edit.commit();
    }
    
    public void setNaviAPP(final String s) {
        final SharedPreferences$Editor edit = this.share.edit();
        edit.putString(LauncherApplication.FLAG_NAVI_APP, s);
        edit.commit();
    }
    
    public void setNeedStartNaviPkg(final boolean b) {
        final SharedPreferences$Editor edit = this.share.edit();
        edit.putBoolean(LauncherApplication.FLAG_NEED_AUTO_START_NAVI, b);
        edit.commit();
    }
    
    public void setNeedToEnterPairMode(final boolean b) {
        final SharedPreferences$Editor edit = this.share.edit();
        edit.putBoolean(LauncherApplication.FLAG_LAST_ENTER_PAIR_STATE, b);
        edit.commit();
    }
    
    public void setTypeMute(final int n, final boolean b) {
    Label_0120_Outer:
        while (true) {
        Label_0309_Outer:
            while (true) {
                Label_0330: {
                    Label_0289: {
                        AudioManager audioManager = null;
                    Label_0228:
                        while (true) {
                            synchronized (this) {
                                this.logger.debug("setTypeMute type" + n + "  state==" + b);
                                Log.d("setTypeMute", "type" + n + "  state==" + b);
                                audioManager = (AudioManager)this.getSystemService("audio");
                                switch (n) {
                                    case 3: {
                                        if (this.isStreamMusicMute != b) {
                                            this.isStreamMusicMute = b;
                                            if (!b) {
                                                break;
                                            }
                                            audioManager.setStreamMute(30, true);
                                            ++this.countMusic;
                                        }
                                        Log.d("setTypeMute", "singular is mute : countMusic==" + this.countMusic + " countMusicBluetooth==" + this.countMusicBluetooth + " countRing==" + this.countRing);
                                        return;
                                    }
                                    case 2: {
                                        break Label_0228;
                                    }
                                    case 200: {
                                        break Label_0289;
                                    }
                                    default: {
                                        break Label_0330;
                                    }
                                }
                            }
                            final AudioManager audioManager2;
                            audioManager2.setStreamMute(30, false);
                            continue Label_0120_Outer;
                        }
                        if (this.isStreamRingMute != b) {
                            this.isStreamRingMute = b;
                            if (b) {
                                audioManager.setStreamMute(31, true);
                                audioManager.setStreamMute(32, true);
                            }
                            else {
                                audioManager.setStreamMute(31, false);
                                audioManager.setStreamMute(32, false);
                            }
                            ++this.countRing;
                            continue;
                        }
                        continue;
                    }
                    if (this.isBluetoothMusicMute != b) {
                        this.isBluetoothMusicMute = b;
                        while (true) {
                            try {
                                Bluetooth.getInstance().setBluetoothMusicMute(b);
                                ++this.countMusicBluetooth;
                                continue Label_0309_Outer;
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                                continue;
                            }
                            break;
                        }
                        break Label_0330;
                    }
                    continue;
                }
                continue;
            }
        }
    }
    
    public void setWifiApHasPwd(final boolean b) {
        final SharedPreferences$Editor edit = this.share.edit();
        edit.putBoolean(LauncherApplication.FLAG_WIFI_HAS_PWD, b);
        edit.commit();
    }
    
    public void setWifiApIdAndKey(final String[] array) {
        final SharedPreferences$Editor edit = this.share.edit();
        edit.putString(LauncherApplication.FLAG_WIFI_AP_ID_KEY, String.valueOf(array[1]) + ";" + array[0]);
        edit.commit();
    }
    
    public void setWifiApState(final boolean b) {
        final SharedPreferences$Editor edit = this.share.edit();
        edit.putBoolean(LauncherApplication.FLAG_WIFI_AP_STATE, b);
        edit.commit();
    }
    
    public boolean startAppByPkg(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final Intent launchIntentForPackage = this.getPackageManager().getLaunchIntentForPackage(s);
            if (launchIntentForPackage != null) {
                launchIntentForPackage.addFlags(272629760);
                this.startActivity(launchIntentForPackage);
                return true;
            }
        }
        return false;
    }
    
    public void startNavi() {
        final String naviAPP = this.getNaviAPP();
        if (!TextUtils.isEmpty((CharSequence)naviAPP)) {
            final Intent launchIntentForPackage = this.getPackageManager().getLaunchIntentForPackage(naviAPP);
            if (launchIntentForPackage != null) {
                launchIntentForPackage.addFlags(268435456);
                this.startActivity(launchIntentForPackage);
            }
        }
    }
    
    public boolean topIsBlueMainFragment() {
        return this.istop;
    }
    
    public void unregisterHandler(final Handler handler) {
        if (this.mHandlers.contains(handler)) {
            this.mHandlers.remove(handler);
            Log.d("launcherlog", "unregisterHandler =" + handler.getClass());
        }
    }
    
    public void updateLanguage(final Locale locale) {
        Log.d("ANDROID_LAB", locale.toString());
        try {
            final IActivityManager default1 = ActivityManagerNative.getDefault();
            final Configuration configuration = default1.getConfiguration();
            configuration.locale = locale;
            default1.updateConfiguration(configuration);
            this.getResources().updateConfiguration(configuration, this.getResources().getDisplayMetrics());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
