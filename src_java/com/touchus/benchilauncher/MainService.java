package com.touchus.benchilauncher;

import android.graphics.drawable.*;
import android.media.*;
import org.slf4j.*;
import android.util.*;
import com.touchus.benchilauncher.views.*;
import com.touchus.publicutils.utils.*;
import com.touchus.publicutils.sysconst.*;
import android.text.*;
import android.annotation.*;
import android.bluetooth.*;
import android.content.*;
import com.backaudio.android.driver.*;
import android.provider.*;
import com.touchus.benchilauncher.receiver.*;
import android.app.*;
import android.widget.*;
import android.view.*;
import com.touchus.benchilauncher.utils.*;
import com.touchus.benchilauncher.service.*;
import android.location.*;
import java.io.*;
import com.backaudio.android.driver.beans.*;
import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import java.util.*;
import android.os.*;
import java.lang.ref.*;

public class MainService extends Service
{
    public static String curChannel;
    private String MYLOG;
    public BluetoothAdapter adapter;
    private Runnable airplaneRunnable;
    private AnimationDrawable animationDrawable;
    private LauncherApplication app;
    public AudioManager audioManager;
    private BluetoothA2dp benzA2dp;
    private BluetoothDevice benzDevice;
    private BluetoothHeadset benzhHeadset;
    private BroadcastReceiver bluetoothReceiver;
    Thread canboxtThread;
    private String carBTname;
    public Runnable checkNavigationRunnable;
    private BroadcastReceiver easyConnectReceiver;
    long endTime;
    private Runnable getConnectCanboxState;
    private BroadcastReceiver getWeatherReceiver;
    private Runnable getWeatherRunnabale;
    private GpsLocationListener gpsLocatListener;
    private LocationManager gpsManager;
    private GpsStatus$Listener gpsStateListener;
    private boolean iAUX;
    private boolean iBenzA2dpConnected;
    private boolean iConnectCanbox;
    boolean iExist;
    boolean iGetWeather;
    private boolean iInAccState;
    public boolean iIsBTConnect;
    public boolean iIsDV;
    private boolean iIsEnterAirplaneMode;
    public boolean iIsInAndroid;
    public boolean iIsInOriginal;
    public boolean iIsInRecorder;
    public boolean iIsInReversing;
    public boolean iIsScrennOff;
    private boolean iNaviSound;
    private boolean iReadyUpdate;
    private long lastAirplaneTime;
    private FrameLayout loadingFloatLayout;
    private Logger logger;
    ActivityManager mActivityManager;
    public MainBinder mBinder;
    private DesktopLayout mDesktopLayout;
    private Mhandler mHandler;
    private BluetoothProfile$ServiceListener mProfileServiceListener;
    public ComponentName mRemoteControlClientReceiverComponent;
    private SpUtilK mSpUtilK;
    private WindowManager mWindowManager;
    private int mcuTotalDataIndex;
    public byte[] mcuUpgradeByteData;
    private BroadcastReceiver networkReceiver;
    private MyLinearlayout noTouchLayout;
    private BroadcastReceiver recoveryReceiver;
    public Runnable removeLoadingRunable;
    private int spaceTime;
    private Runnable startSettingRunnable;
    long startTime;
    private List<String> testBTname;
    private int totalDataIndex;
    private BroadcastReceiver unibroadAudioFocusReceiver;
    private byte[] upgradeByteData;
    
    public MainService() {
        this.logger = LoggerFactory.getLogger(MainService.class);
        this.iInAccState = false;
        this.iAUX = false;
        this.iConnectCanbox = false;
        this.getConnectCanboxState = new Runnable() {
            @Override
            public void run() {
                if (!MainService.this.iConnectCanbox) {
                    Mainboard.getInstance().connectOrDisConnectCanbox(true);
                    MainService.this.mHandler.postDelayed(MainService.this.getConnectCanboxState, 200L);
                    return;
                }
                MainService.this.mHandler.removeCallbacks(MainService.this.getConnectCanboxState);
            }
        };
        this.startSettingRunnable = new Runnable() {
            @Override
            public void run() {
                MainService.this.setAirplaneMode(false);
                MainService.this.setGpsStatus(true);
                MainService.this.app.getLanguage();
                MainService.this.app.isOriginalKeyOpen = MainService.this.mSpUtilK.getBoolean(MainService.this.getString(2131165345), false);
                MainService.this.app.ismix = MainService.this.mSpUtilK.getBoolean(MainService.this.getString(2131165346), false);
                MainService.this.app.isTestOpen = MainService.this.mSpUtilK.getBoolean("test", false);
                MainService.this.app.radioPos = MainService.this.mSpUtilK.getInt("radioType", 0);
                MainService.this.app.naviPos = MainService.this.mSpUtilK.getInt("naviExist", 0);
                MainService.this.app.usbPos = MainService.this.mSpUtilK.getInt("usbNum", 0);
                MainService.this.app.connectPos = MainService.this.mSpUtilK.getInt("connectType", 0);
                MainService.this.app.screenPos = MainService.this.mSpUtilK.getInt("screenType", 0);
                Log.e("", "app.radioPos = " + MainService.this.app.radioPos + ",app.naviPos = " + MainService.this.app.naviPos + ",app.usbPos = " + MainService.this.app.usbPos + ",app.connectPos = " + MainService.this.app.connectPos + ",app.screenPos = " + MainService.this.app.screenPos);
                if (MainService.this.mSpUtilK.getInt("voiceType", 0) == 1) {
                    MainService.this.app.eMediaSet = Mainboard.EReverserMediaSet.NOMAL;
                }
                else {
                    MainService.this.app.eMediaSet = Mainboard.EReverserMediaSet.MUTE;
                }
                if (MainService.this.app.connectPos == 0) {
                    LauncherApplication.isBT = true;
                }
                else {
                    LauncherApplication.isBT = false;
                }
                if (SysConst.isBT()) {
                    MainService.this.addBluetoothReceiver();
                }
                if (SysConst.isBT()) {
                    MainService.this.app.ismix = false;
                    MainService.this.mSpUtilK.putBoolean(MainService.this.getString(2131165346), false);
                    SysConst.musicDecVolume = 0.3f;
                }
                if (MainService.this.app.ismix) {
                    SysConst.musicDecVolume = (float)(MainService.this.mSpUtilK.getInt(SysConst.mixPro, 5) / 10.0);
                }
                if (SysConst.isBT()) {
                    MainService.this.startLocalBT();
                }
            }
        };
        this.upgradeByteData = null;
        this.iReadyUpdate = false;
        this.canboxtThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (!MainService.this.iReadyUpdate) {
                    Mainboard.getInstance().requestUpgradeCanbox(MainService.this.totalDataIndex);
                    MainService.this.mHandler.postDelayed((Runnable)MainService.this.canboxtThread, 200L);
                    return;
                }
                MainService.this.mHandler.removeCallbacks((Runnable)MainService.this.canboxtThread);
            }
        });
        this.mcuUpgradeByteData = null;
        this.mcuTotalDataIndex = -999;
        this.removeLoadingRunable = new Runnable() {
            @Override
            public void run() {
                if (MainService.this.loadingFloatLayout != null) {
                    MainService.this.removeLoadingFloatView();
                    MainService.this.mHandler.removeCallbacks(MainService.this.removeLoadingRunable);
                    if (!BenzModel.isBenzGLK() || !SysConst.isBT()) {
                        MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + " removeLoading isAUX = " + MainService.this.iAUX + ",getBTA2dpState = " + MainService.this.getBTA2dpState());
                        if (!MainService.this.getBTA2dpState()) {
                            BTConnectDialog.getInstance().showBTConnectView((Context)MainService.this);
                            return;
                        }
                        BTConnectDialog.getInstance().dissShow();
                        MainService.this.setConnectedMusicVoice();
                    }
                }
            }
        };
        this.lastAirplaneTime = -1L;
        this.iIsEnterAirplaneMode = false;
        this.spaceTime = 12000;
        this.airplaneRunnable = new Runnable() {
            @Override
            public void run() {
                MainService.this.dealwithAirplaneMode();
            }
        };
        this.iGetWeather = false;
        this.getWeatherRunnabale = new Runnable() {
            @Override
            public void run() {
                if (UtilTools.isNetworkConnected((Context)MainService.this)) {
                    MainService.this.getWeather();
                }
                MainService.this.mHandler.postDelayed(MainService.this.getWeatherRunnabale, 1200000L);
            }
        };
        this.getWeatherReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                MainService.this.app.currentCityName = intent.getStringExtra("cityName");
                MainService.this.app.weatherTempDes = intent.getStringExtra("cityWeather");
                MainService.this.app.weatherTemp = String.valueOf(intent.getStringExtra("lowTemperature")) + "-" + intent.getStringExtra("highTemperature") + "\u2103";
                Log.e("voice", "Weather app.weatherTempDes = " + MainService.this.app.weatherTempDes + ",app.weatherTemp = " + MainService.this.app.weatherTemp);
                MainService.this.app.sendMessage(1010, null);
            }
        };
        this.networkReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (action.equals("android.net.conn.CONNECTIVITY_CHANGE") || action.equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                    MainService.this.regainWeather();
                }
            }
        };
        this.recoveryReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (action.equals("com.touchus.factorytest.recovery")) {
                    MainService.this.forceStopApp("com.unibroad.usbcardvr");
                    MainService.this.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                }
                else {
                    if (action.equals("com.touchus.factorytest.sdcardreset")) {
                        Utiltools.resetSDCard();
                        return;
                    }
                    if (action.equals("com.touchus.factorytest.getMcu")) {
                        final Intent intent2 = new Intent();
                        intent2.setAction("com.touchus.factorytest.Mcu");
                        intent2.putExtra("mcu", MainService.this.app.curMcuVersion);
                        MainService.this.sendBroadcast(intent2);
                        return;
                    }
                    if (action.equals(PubSysConst.ACTION_FACTORY_BREAKSET)) {
                        final int intExtra = intent.getIntExtra(PubSysConst.KEY_BREAKPOS, 0);
                        MainService.this.mSpUtilK.putInt(PubSysConst.KEY_BREAKPOS, intExtra);
                        MainService.this.app.breakpos = intExtra;
                        SysConst.storeData[9] = (byte)intExtra;
                        Log.e("", "SysConst.storeData = " + SysConst.storeData);
                        Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
                        UtilTools.echoFile(new StringBuilder().append(intExtra).toString(), PubSysConst.GT9XX_INT_TRIGGER);
                        return;
                    }
                    if (action.equals("com.touchus.factorytest.benzetype")) {
                        final byte b = (byte)intent.getIntExtra(BenzModel.KEY, 0);
                        final BenzModel.EBenzTpye[] values = BenzModel.EBenzTpye.values();
                        for (int length = values.length, i = 0; i < length; ++i) {
                            final BenzModel.EBenzTpye benzTpye = values[i];
                            if (b == benzTpye.getCode()) {
                                BenzModel.benzTpye = benzTpye;
                                break;
                            }
                        }
                        MainService.this.app.isSUV = BenzModel.isSUV();
                        Mainboard.getInstance().setBenzType(BenzModel.benzTpye);
                        if (SysConst.isBT()) {
                            MainService.this.app.ismix = false;
                        }
                        MainService.this.mSpUtilK.putInt(BenzModel.KEY, BenzModel.benzTpye.getCode());
                    }
                }
            }
        };
        this.easyConnectReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if (action.equals("net.easyconn.bt.checkstatus")) {
                    Log.e("easyConnect", "easyConnect checkstatus");
                    MainService.this.app.isEasyView = true;
                    if (!LauncherApplication.isBlueConnectState) {
                        MainService.this.app.openOrCloseBluetooth(true);
                        MainService.this.sendEasyConnectBroadcast("net.easyconn.bt.opened");
                        MainService.this.sendEasyConnectBroadcast("net.easyconn.bt.unconnected");
                        return;
                    }
                    MainService.this.sendEasyConnectBroadcast("net.easyconn.bt.connected");
                    MainService.this.app.btservice.requestMusicAudioFocus();
                }
                else if (action.equals("net.easyconn.a2dp.acquire")) {
                    Log.e("easyConnect", "easyConnect a2dp acquire");
                    if (LauncherApplication.isBlueConnectState) {
                        MainService.this.app.btservice.requestMusicAudioFocus();
                    }
                }
                else {
                    if (action.equals("net.easyconn.a2dp.release")) {
                        Log.e("easyConnect", "easyConnect a2dp release");
                        MainService.this.app.btservice.stopBTMusic();
                        return;
                    }
                    if (action.equals("net.easyconn.app.quit")) {
                        Log.e("easyConnect", "easyConnect quit");
                        MainService.this.app.isEasyView = false;
                        return;
                    }
                    Log.e("easyConnect", "easyConnect no");
                }
            }
        };
        this.checkNavigationRunnable = new Runnable() {
            @Override
            public void run() {
                MainService.this.mHandler.postDelayed(MainService.this.checkNavigationRunnable, 2000L);
                if (LauncherApplication.iAccOff) {
                    return;
                }
                final String access$19 = MainService.this.getCurrentTopAppPkg();
                if (!TextUtils.isEmpty((CharSequence)access$19) && !access$19.equals(MainService.this.getPackageName()) && !access$19.equals(MainService.this.getString(2131165187)) && !access$19.equals("com.unibroad.notifyreceiverservice") && !access$19.equals("com.unibroad.benzuserguide") && !access$19.equals("com.touchus.amaplocation") && !access$19.equals("com.android.packageinstaller") && !access$19.contains("com.papago.s1OBU") && !access$19.contains("com.tima.carnet.vt") && !access$19.equals("cld.navi.kgomap") && !access$19.equals("com.baidu.navi") && !access$19.equals("net.easyconn")) {
                    MainService.this.app.sendHideOrShowNavigationBarEvent(false);
                    MainService.this.createDeskFloat();
                    return;
                }
                MainService.this.app.sendHideOrShowNavigationBarEvent(false);
            }
        };
        this.bluetoothReceiver = new BroadcastReceiver() {
            @SuppressLint({ "NewApi" })
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                Log.e("", "btstate Action = " + action);
                if (SysConst.isBT()) {
                    if ("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED".equals(action)) {
                        final int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                        switch (intExtra) {
                            case 0: {
                                Log.e("", "btstate a2dp STATE_DISCONNECTED");
                                MainService.access$20(MainService.this, false);
                                MainService.this.setMusicMute();
                                break;
                            }
                            case 1: {
                                Log.e("", "btstate a2dp STATE_CONNECTING");
                                MainService.access$20(MainService.this, false);
                                MainService.this.setMusicMute();
                                if (BenzModel.isBenzGLK()) {
                                    MainService.this.mHandler.postDelayed((Runnable)new Runnable() {
                                        @Override
                                        public void run() {
                                            Mainboard.getInstance().forcePress();
                                        }
                                    }, 4000L);
                                    break;
                                }
                                break;
                            }
                            case 2: {
                                Log.e("", "btstate a2dp STATE_CONNECTED");
                                if (BenzModel.isBenzGLK()) {
                                    MainService.this.mHandler.post(MainService.this.removeLoadingRunable);
                                }
                                MainService.access$20(MainService.this, true);
                                MainService.this.setConnectedMusicVoice();
                                break;
                            }
                        }
                        final Message message = new Message();
                        message.what = 1041;
                        message.obj = intExtra;
                        MainService.this.mHandler.sendMessage(message);
                        return;
                    }
                    if ("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED".equals(action)) {
                        final int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                        switch (intExtra2) {
                            case 0: {
                                Log.e("", "btstate headset STATE_DISCONNECTED");
                                break;
                            }
                            case 1: {
                                Log.e("", "btstate headset STATE_CONNECTING");
                                break;
                            }
                            case 2: {
                                Log.e("", "btstate headset STATE_CONNECTED");
                                break;
                            }
                        }
                        final Message message2 = new Message();
                        message2.what = 1042;
                        message2.obj = intExtra2;
                        MainService.this.mHandler.sendMessage(message2);
                        return;
                    }
                    if ("android.bluetooth.device.action.FOUND".equals(action)) {
                        final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                        Log.e("", "btstate ACTION_FOUND device.getName = " + bluetoothDevice.getName());
                        if (MainService.this.carBTname.equals(bluetoothDevice.getName())) {
                            MainService.access$23(MainService.this, bluetoothDevice);
                            MainService.this.adapter.cancelDiscovery();
                        }
                    }
                    else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                        Log.e("", "btstate ACTION_DISCOVERY_FINISHED");
                        if (BenzModel.isBenzGLK() && MainService.this.benzDevice != null) {
                            MainService.this.benzDevice.createBond();
                        }
                    }
                    else if ("android.bluetooth.device.action.BOND_STATE_CHANGED".equals(action)) {
                        final BluetoothDevice bluetoothDevice2 = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                        if (bluetoothDevice2 == null) {
                            Log.e("", "btstate bond failed ");
                            return;
                        }
                        if (MainService.this.carBTname.equals(bluetoothDevice2.getName()) || MainService.this.testBTname.contains(bluetoothDevice2.getName())) {
                            MainService.access$23(MainService.this, bluetoothDevice2);
                            Log.e("", "btstate bond state:" + bluetoothDevice2.getBondState());
                            switch (bluetoothDevice2.getBondState()) {
                                default: {}
                                case 11: {
                                    if (BenzModel.isBenzGLK()) {
                                        MainService.this.mHandler.postDelayed((Runnable)new Runnable() {
                                            @Override
                                            public void run() {
                                                Mainboard.getInstance().forcePress();
                                            }
                                        }, 4000L);
                                        return;
                                    }
                                    break;
                                }
                                case 12: {
                                    MainService.this.benzhHeadset.setPriority(bluetoothDevice2, 0);
                                    if (BenzModel.isBenzGLK() && !MainService.this.iExist) {
                                        MainService.this.mHandler.postDelayed((Runnable)new Runnable() {
                                            @Override
                                            public void run() {
                                                if (MainService.this.benzDevice != null && !MainService.this.getBTA2dpState()) {
                                                    Log.e("", "btstate a2dp connect = " + MainService.this.benzDevice.getName());
                                                    MainService.this.benzA2dp.connect(MainService.this.benzDevice);
                                                }
                                            }
                                        }, 2000L);
                                        return;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    else if ("android.bluetooth.device.action.PAIRING_REQUEST".equals(action)) {
                        final BluetoothDevice bluetoothDevice3 = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                        Log.e("", "btstate pairing request");
                        if (bluetoothDevice3 == null) {
                            Log.e("", "pairing failed ");
                            return;
                        }
                        if (MainService.this.carBTname.equals(bluetoothDevice3.getName()) || MainService.this.testBTname.contains(bluetoothDevice3.getName())) {
                            MainService.this.mHandler.postDelayed((Runnable)new Runnable() {
                                @Override
                                public void run() {
                                    Mainboard.getInstance().forcePress();
                                }
                            }, 1500L);
                        }
                    }
                    else if ("android.bluetooth.device.action.PAIRING_CANCEL".equals(action)) {
                        Log.e("", "btstate pairing cancel");
                    }
                }
            }
        };
        this.testBTname = new ArrayList<String>();
        this.carBTname = "MB Bluetooth";
        this.iBenzA2dpConnected = false;
        this.iExist = false;
        this.mProfileServiceListener = (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
            public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                while (true) {
                    if (n == 1) {
                        try {
                            MainService.access$28(MainService.this, (BluetoothHeadset)bluetoothProfile);
                            return;
                            while (true) {
                                MainService.access$29(MainService.this, (BluetoothA2dp)bluetoothProfile);
                                Log.e("", "btstate a2dp connect = " + MainService.this.benzDevice.getName());
                                MainService.this.benzA2dp.connect(MainService.this.benzDevice);
                                return;
                                continue;
                            }
                        }
                        // iftrue(Label_0133:, MainService.access$24(this.this$0) == null || MainService.access$15(this.this$0))
                        // iftrue(Label_0133:, n != 2)
                        catch (Exception ex) {
                            Log.e("debug", "uuid BOND_ BluetoothProfile error" + ex.toString());
                        }
                        Label_0133: {
                            return;
                        }
                    }
                    continue;
                }
            }
            
            public void onServiceDisconnected(final int n) {
            }
        };
        this.iNaviSound = false;
        this.unibroadAudioFocusReceiver = new BroadcastReceiver() {
            public void onReceive(final Context p0, final Intent p1) {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: invokevirtual   android/content/Intent.getAction:()Ljava/lang/String;
                //     4: astore          10
                //     6: ldc             "unibroadAudioFocus"
                //     8: new             Ljava/lang/StringBuilder;
                //    11: dup            
                //    12: ldc             "action = "
                //    14: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //    17: aload           10
                //    19: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //    22: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //    25: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //    28: pop            
                //    29: aload_2        
                //    30: ldc             "pkgName"
                //    32: invokevirtual   android/content/Intent.getStringExtra:(Ljava/lang/String;)Ljava/lang/String;
                //    35: astore_1       
                //    36: aload_2        
                //    37: ldc             "durationHint"
                //    39: iconst_0       
                //    40: invokevirtual   android/content/Intent.getIntExtra:(Ljava/lang/String;I)I
                //    43: istore_3       
                //    44: aload_2        
                //    45: ldc             "status"
                //    47: iconst_0       
                //    48: invokevirtual   android/content/Intent.getIntExtra:(Ljava/lang/String;I)I
                //    51: istore          4
                //    53: aload_2        
                //    54: invokevirtual   android/content/Intent.getAction:()Ljava/lang/String;
                //    57: ldc             "com.unibroad.AudioFocus.PAPAGO"
                //    59: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
                //    62: ifne            73
                //    65: aload_1        
                //    66: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
                //    69: ifeq            73
                //    72: return         
                //    73: iconst_0       
                //    74: istore          9
                //    76: iconst_0       
                //    77: istore          8
                //    79: getstatic       com/touchus/benchilauncher/SysConst.num:[I
                //    82: aload_0        
                //    83: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //    86: invokestatic    com/touchus/benchilauncher/MainService.access$6:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;
                //    89: getstatic       com/touchus/benchilauncher/SysConst.mediaVoice:Ljava/lang/String;
                //    92: iconst_5       
                //    93: invokevirtual   com/touchus/benchilauncher/utils/SpUtilK.getInt:(Ljava/lang/String;I)I
                //    96: iaload         
                //    97: istore          5
                //    99: getstatic       com/touchus/benchilauncher/SysConst.num:[I
                //   102: aload_0        
                //   103: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   106: invokestatic    com/touchus/benchilauncher/MainService.access$6:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;
                //   109: getstatic       com/touchus/benchilauncher/SysConst.dvVoice:Ljava/lang/String;
                //   112: iconst_5       
                //   113: invokevirtual   com/touchus/benchilauncher/utils/SpUtilK.getInt:(Ljava/lang/String;I)I
                //   116: iaload         
                //   117: istore          6
                //   119: getstatic       com/touchus/benchilauncher/SysConst.num:[I
                //   122: aload_0        
                //   123: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   126: invokestatic    com/touchus/benchilauncher/MainService.access$6:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/utils/SpUtilK;
                //   129: getstatic       com/touchus/benchilauncher/SysConst.naviVoice:Ljava/lang/String;
                //   132: iconst_3       
                //   133: invokevirtual   com/touchus/benchilauncher/utils/SpUtilK.getInt:(Ljava/lang/String;I)I
                //   136: iaload         
                //   137: istore          7
                //   139: aload_0        
                //   140: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   143: invokestatic    com/touchus/benchilauncher/MainService.access$14:(Lcom/touchus/benchilauncher/MainService;)Z
                //   146: ifne            192
                //   149: aload_0        
                //   150: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   153: invokestatic    com/touchus/benchilauncher/MainService.access$24:(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;
                //   156: ifnull          192
                //   159: aload_0        
                //   160: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   163: invokestatic    com/touchus/benchilauncher/MainService.access$25:(Lcom/touchus/benchilauncher/MainService;)Ljava/util/List;
                //   166: aload_0        
                //   167: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   170: invokestatic    com/touchus/benchilauncher/MainService.access$24:(Lcom/touchus/benchilauncher/MainService;)Landroid/bluetooth/BluetoothDevice;
                //   173: invokevirtual   android/bluetooth/BluetoothDevice.getName:()Ljava/lang/String;
                //   176: invokeinterface java/util/List.contains:(Ljava/lang/Object;)Z
                //   181: ifeq            192
                //   184: aload_0        
                //   185: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   188: iconst_1       
                //   189: invokestatic    com/touchus/benchilauncher/MainService.access$30:(Lcom/touchus/benchilauncher/MainService;Z)V
                //   192: aload           10
                //   194: ldc             "com.unibroad.AudioFocus.PAPAGOGAIN"
                //   196: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   199: ifeq            316
                //   202: iconst_1       
                //   203: istore          9
                //   205: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   208: getstatic       com/touchus/benchilauncher/SysConst.basicNum:I
                //   211: iload           7
                //   213: iconst_0       
                //   214: iconst_0       
                //   215: iconst_0       
                //   216: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   219: aload_0        
                //   220: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   223: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   226: iconst_2       
                //   227: iconst_0       
                //   228: invokevirtual   com/touchus/benchilauncher/LauncherApplication.setTypeMute:(IZ)V
                //   231: iload           9
                //   233: istore          8
                //   235: aload_0        
                //   236: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   239: invokestatic    com/touchus/benchilauncher/MainService.access$15:(Lcom/touchus/benchilauncher/MainService;)Z
                //   242: ifne            256
                //   245: aload_0        
                //   246: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   249: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   252: iload           9
                //   254: istore          8
                //   256: ldc             "unibroadAudioFocus"
                //   258: new             Ljava/lang/StringBuilder;
                //   261: dup            
                //   262: ldc             "openOrCloseRelay: "
                //   264: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   267: iload           8
                //   269: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //   272: ldc             ",isCalling : "
                //   274: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   277: aload_0        
                //   278: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   281: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   284: getfield        com/touchus/benchilauncher/LauncherApplication.isCalling:Z
                //   287: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //   290: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   293: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   296: pop            
                //   297: aload_0        
                //   298: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   301: iload           8
                //   303: invokestatic    com/touchus/benchilauncher/MainService.access$33:(Lcom/touchus/benchilauncher/MainService;Z)V
                //   306: aload_0        
                //   307: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   310: iload           8
                //   312: invokestatic    com/touchus/benchilauncher/MainService.access$31:(Lcom/touchus/benchilauncher/MainService;Z)V
                //   315: return         
                //   316: aload           10
                //   318: ldc             "com.unibroad.AudioFocus.PAPAGOLOSS"
                //   320: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   323: ifeq            391
                //   326: iconst_0       
                //   327: istore          8
                //   329: invokestatic    com/touchus/benchilauncher/SysConst.isBT:()Z
                //   332: ifeq            349
                //   335: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   338: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //   341: iload           7
                //   343: iconst_0       
                //   344: iconst_0       
                //   345: iconst_0       
                //   346: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   349: aload_0        
                //   350: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   353: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   356: iconst_2       
                //   357: iconst_0       
                //   358: invokevirtual   com/touchus/benchilauncher/LauncherApplication.setTypeMute:(IZ)V
                //   361: aload_0        
                //   362: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   365: invokestatic    com/touchus/benchilauncher/MainService.access$15:(Lcom/touchus/benchilauncher/MainService;)Z
                //   368: ifeq            381
                //   371: aload_0        
                //   372: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   375: invokestatic    com/touchus/benchilauncher/MainService.access$16:(Lcom/touchus/benchilauncher/MainService;)V
                //   378: goto            256
                //   381: aload_0        
                //   382: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   385: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   388: goto            256
                //   391: aload           10
                //   393: ldc             "com.unibroad.AudioFocus.REGAIN"
                //   395: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   398: ifeq            720
                //   401: ldc             "unibroadAudioFocus"
                //   403: new             Ljava/lang/StringBuilder;
                //   406: dup            
                //   407: ldc             "AUDIO_REGAIN pkgName:"
                //   409: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   412: aload_1        
                //   413: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   416: ldc             " , openOrCloseRelay: "
                //   418: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   421: iconst_0       
                //   422: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //   425: ldc             ",isCalling : "
                //   427: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   430: aload_0        
                //   431: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   434: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   437: getfield        com/touchus/benchilauncher/LauncherApplication.isCalling:Z
                //   440: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //   443: ldc             ",app.musicType = "
                //   445: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   448: aload_0        
                //   449: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   452: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   455: getfield        com/touchus/benchilauncher/LauncherApplication.musicType:I
                //   458: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   461: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   464: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   467: pop            
                //   468: aload_0        
                //   469: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   472: getfield        com/touchus/benchilauncher/MainService.iIsDV:Z
                //   475: ifeq            523
                //   478: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   481: getstatic       com/touchus/benchilauncher/SysConst.basicNum:I
                //   484: iload           6
                //   486: iconst_0       
                //   487: iconst_0       
                //   488: iconst_0       
                //   489: invokevirtual   com/backaudio/android/driver/Mainboard.setDVSoundValue:(IIIII)V
                //   492: ldc             "unibroadAudioFocus"
                //   494: new             Ljava/lang/StringBuilder;
                //   497: dup            
                //   498: ldc             " DV openOrCloseRelay: "
                //   500: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   503: iconst_0       
                //   504: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //   507: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   510: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   513: pop            
                //   514: aload_0        
                //   515: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   518: iconst_1       
                //   519: invokestatic    com/touchus/benchilauncher/MainService.access$31:(Lcom/touchus/benchilauncher/MainService;Z)V
                //   522: return         
                //   523: invokestatic    com/touchus/benchilauncher/SysConst.isBT:()Z
                //   526: ifeq            592
                //   529: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   532: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //   535: iload           7
                //   537: iconst_0       
                //   538: iconst_0       
                //   539: iconst_0       
                //   540: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   543: aload_0        
                //   544: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   547: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   550: iconst_2       
                //   551: iconst_0       
                //   552: invokevirtual   com/touchus/benchilauncher/LauncherApplication.setTypeMute:(IZ)V
                //   555: aload_1        
                //   556: aload_0        
                //   557: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   560: invokevirtual   com/touchus/benchilauncher/MainService.getPackageName:()Ljava/lang/String;
                //   563: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   566: ifeq            690
                //   569: aload_0        
                //   570: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   573: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   576: getfield        com/touchus/benchilauncher/LauncherApplication.musicType:I
                //   579: ifne            690
                //   582: aload_0        
                //   583: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   586: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   589: goto            256
                //   592: aload_0        
                //   593: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   596: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   599: getfield        com/touchus/benchilauncher/LauncherApplication.ismix:Z
                //   602: ifeq            627
                //   605: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   608: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //   611: iconst_2       
                //   612: idiv           
                //   613: iload           7
                //   615: iconst_0       
                //   616: iconst_0       
                //   617: iconst_0       
                //   618: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   621: ldc2_w          100
                //   624: invokestatic    java/lang/Thread.sleep:(J)V
                //   627: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   630: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //   633: iconst_2       
                //   634: idiv           
                //   635: iload           7
                //   637: iconst_0       
                //   638: iload           5
                //   640: iconst_2       
                //   641: idiv           
                //   642: iload           5
                //   644: iconst_2       
                //   645: idiv           
                //   646: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   649: ldc2_w          100
                //   652: invokestatic    java/lang/Thread.sleep:(J)V
                //   655: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   658: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //   661: iload           7
                //   663: iconst_0       
                //   664: iload           5
                //   666: iload           5
                //   668: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   671: goto            543
                //   674: astore_2       
                //   675: aload_2        
                //   676: invokevirtual   java/lang/InterruptedException.printStackTrace:()V
                //   679: goto            627
                //   682: astore_2       
                //   683: aload_2        
                //   684: invokevirtual   java/lang/InterruptedException.printStackTrace:()V
                //   687: goto            655
                //   690: aload_0        
                //   691: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   694: invokestatic    com/touchus/benchilauncher/MainService.access$15:(Lcom/touchus/benchilauncher/MainService;)Z
                //   697: ifeq            710
                //   700: aload_0        
                //   701: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   704: invokestatic    com/touchus/benchilauncher/MainService.access$16:(Lcom/touchus/benchilauncher/MainService;)V
                //   707: goto            256
                //   710: aload_0        
                //   711: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   714: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   717: goto            256
                //   720: ldc             "unibroadAudioFocus"
                //   722: new             Ljava/lang/StringBuilder;
                //   725: dup            
                //   726: ldc             "requestAudiofocus action : "
                //   728: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   731: aload           10
                //   733: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   736: ldc             " audiofocus pkgName:"
                //   738: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   741: aload_1        
                //   742: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   745: ldc             " , durationHint: "
                //   747: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   750: iload_3        
                //   751: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   754: ldc             ",status : "
                //   756: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   759: iload           4
                //   761: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   764: ldc             ", app.isCalling : "
                //   766: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   769: aload_0        
                //   770: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   773: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   776: getfield        com/touchus/benchilauncher/LauncherApplication.isCalling:Z
                //   779: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //   782: ldc             ",app.musicType = "
                //   784: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   787: aload_0        
                //   788: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   791: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   794: getfield        com/touchus/benchilauncher/LauncherApplication.musicType:I
                //   797: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   800: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   803: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   806: pop            
                //   807: aload_0        
                //   808: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   811: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   814: getfield        com/touchus/benchilauncher/LauncherApplication.isCalling:Z
                //   817: ifeq            845
                //   820: iconst_1       
                //   821: istore          8
                //   823: aload_0        
                //   824: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   827: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   830: iconst_2       
                //   831: iconst_1       
                //   832: invokevirtual   com/touchus/benchilauncher/LauncherApplication.setTypeMute:(IZ)V
                //   835: aload_0        
                //   836: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   839: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   842: goto            256
                //   845: aload_1        
                //   846: aload_0        
                //   847: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   850: ldc             2131165187
                //   852: invokevirtual   com/touchus/benchilauncher/MainService.getString:(I)Ljava/lang/String;
                //   855: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   858: ifne            894
                //   861: aload_1        
                //   862: ldc             "com.amap.navi.demo"
                //   864: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   867: ifne            894
                //   870: aload_1        
                //   871: aload_0        
                //   872: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   875: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   878: invokevirtual   com/touchus/benchilauncher/LauncherApplication.getNaviAPP:()Ljava/lang/String;
                //   881: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   884: ifne            894
                //   887: aload_1        
                //   888: invokestatic    com/touchus/publicutils/utils/APPSettings.isNavgation:(Ljava/lang/String;)Z
                //   891: ifeq            1042
                //   894: invokestatic    com/touchus/benchilauncher/SysConst.isBT:()Z
                //   897: ifeq            957
                //   900: iconst_1       
                //   901: istore          9
                //   903: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   906: getstatic       com/touchus/benchilauncher/SysConst.basicNum:I
                //   909: iload           7
                //   911: iconst_0       
                //   912: iconst_0       
                //   913: iconst_0       
                //   914: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   917: aload_0        
                //   918: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   921: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   924: iconst_2       
                //   925: iconst_0       
                //   926: invokevirtual   com/touchus/benchilauncher/LauncherApplication.setTypeMute:(IZ)V
                //   929: iload           9
                //   931: istore          8
                //   933: aload_0        
                //   934: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   937: invokestatic    com/touchus/benchilauncher/MainService.access$15:(Lcom/touchus/benchilauncher/MainService;)Z
                //   940: ifne            256
                //   943: aload_0        
                //   944: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   947: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   950: iload           9
                //   952: istore          8
                //   954: goto            256
                //   957: aload_0        
                //   958: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   961: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //   964: getfield        com/touchus/benchilauncher/LauncherApplication.ismix:Z
                //   967: ifeq            989
                //   970: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   973: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //   976: iload           7
                //   978: iconst_0       
                //   979: iload           5
                //   981: iload           5
                //   983: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //   986: goto            917
                //   989: iconst_1       
                //   990: istore          9
                //   992: aload_0        
                //   993: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //   996: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //   999: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //  1002: iconst_0       
                //  1003: iload           7
                //  1005: iconst_0       
                //  1006: iconst_0       
                //  1007: iconst_0       
                //  1008: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //  1011: ldc2_w          100
                //  1014: invokestatic    java/lang/Thread.sleep:(J)V
                //  1017: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //  1020: getstatic       com/touchus/benchilauncher/SysConst.basicNum:I
                //  1023: iload           7
                //  1025: iconst_0       
                //  1026: iconst_0       
                //  1027: iconst_0       
                //  1028: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //  1031: goto            917
                //  1034: astore_1       
                //  1035: aload_1        
                //  1036: invokevirtual   java/lang/InterruptedException.printStackTrace:()V
                //  1039: goto            1017
                //  1042: aload_1        
                //  1043: aload_0        
                //  1044: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1047: invokevirtual   com/touchus/benchilauncher/MainService.getPackageName:()Ljava/lang/String;
                //  1050: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //  1053: ifeq            1079
                //  1056: aload_0        
                //  1057: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1060: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //  1063: getfield        com/touchus/benchilauncher/LauncherApplication.musicType:I
                //  1066: ifne            1079
                //  1069: aload_0        
                //  1070: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1073: invokestatic    com/touchus/benchilauncher/MainService.access$21:(Lcom/touchus/benchilauncher/MainService;)V
                //  1076: goto            256
                //  1079: aload_1        
                //  1080: aload_0        
                //  1081: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1084: invokevirtual   com/touchus/benchilauncher/MainService.getPackageName:()Ljava/lang/String;
                //  1087: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //  1090: ifne            1104
                //  1093: aload_0        
                //  1094: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1097: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //  1100: iconst_5       
                //  1101: putfield        com/touchus/benchilauncher/LauncherApplication.musicType:I
                //  1104: aload_0        
                //  1105: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1108: getfield        com/touchus/benchilauncher/MainService.iIsDV:Z
                //  1111: ifeq            1159
                //  1114: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //  1117: getstatic       com/touchus/benchilauncher/SysConst.basicNum:I
                //  1120: iload           6
                //  1122: iconst_0       
                //  1123: iconst_0       
                //  1124: iconst_0       
                //  1125: invokevirtual   com/backaudio/android/driver/Mainboard.setDVSoundValue:(IIIII)V
                //  1128: ldc             "unibroadAudioFocus"
                //  1130: new             Ljava/lang/StringBuilder;
                //  1133: dup            
                //  1134: ldc             " DV openOrCloseRelay: "
                //  1136: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //  1139: iconst_0       
                //  1140: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //  1143: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //  1146: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //  1149: pop            
                //  1150: aload_0        
                //  1151: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1154: iconst_1       
                //  1155: invokestatic    com/touchus/benchilauncher/MainService.access$31:(Lcom/touchus/benchilauncher/MainService;Z)V
                //  1158: return         
                //  1159: invokestatic    com/touchus/benchilauncher/SysConst.isBT:()Z
                //  1162: ifeq            1267
                //  1165: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //  1168: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //  1171: iload           7
                //  1173: iconst_0       
                //  1174: iconst_0       
                //  1175: iconst_0       
                //  1176: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //  1179: aload_0        
                //  1180: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1183: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //  1186: iconst_2       
                //  1187: iconst_0       
                //  1188: invokevirtual   com/touchus/benchilauncher/LauncherApplication.setTypeMute:(IZ)V
                //  1191: ldc             "unibroadAudioFocus"
                //  1193: new             Ljava/lang/StringBuilder;
                //  1196: dup            
                //  1197: ldc_w           "iAUX = "
                //  1200: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //  1203: aload_0        
                //  1204: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1207: invokestatic    com/touchus/benchilauncher/MainService.access$14:(Lcom/touchus/benchilauncher/MainService;)Z
                //  1210: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                //  1213: ldc             ",app.musicType = "
                //  1215: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //  1218: aload_0        
                //  1219: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1222: invokestatic    com/touchus/benchilauncher/MainService.access$5:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/LauncherApplication;
                //  1225: getfield        com/touchus/benchilauncher/LauncherApplication.musicType:I
                //  1228: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //  1231: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //  1234: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //  1237: pop            
                //  1238: aload_0        
                //  1239: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1242: invokestatic    com/touchus/benchilauncher/MainService.access$32:(Lcom/touchus/benchilauncher/MainService;)Z
                //  1245: ifeq            1286
                //  1248: aload_0        
                //  1249: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1252: invokestatic    com/touchus/benchilauncher/MainService.access$16:(Lcom/touchus/benchilauncher/MainService;)V
                //  1255: ldc             "unibroadAudioFocus"
                //  1257: ldc_w           "MusicConnect"
                //  1260: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //  1263: pop            
                //  1264: goto            256
                //  1267: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //  1270: getstatic       com/touchus/benchilauncher/SysConst.mediaBasicNum:I
                //  1273: iload           7
                //  1275: iconst_0       
                //  1276: iload           5
                //  1278: iload           5
                //  1280: invokevirtual   com/backaudio/android/driver/Mainboard.setAllHornSoundValue:(IIIII)V
                //  1283: goto            1179
                //  1286: aload_0        
                //  1287: getfield        com/touchus/benchilauncher/MainService$14.this$0:Lcom/touchus/benchilauncher/MainService;
                //  1290: invokevirtual   com/touchus/benchilauncher/MainService.btMusicConnect:()V
                //  1293: ldc             "unibroadAudioFocus"
                //  1295: ldc_w           "MusicUnConnect"
                //  1298: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //  1301: pop            
                //  1302: goto            256
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                            
                //  -----  -----  -----  -----  --------------------------------
                //  621    627    674    682    Ljava/lang/InterruptedException;
                //  649    655    682    690    Ljava/lang/InterruptedException;
                //  1011   1017   1034   1042   Ljava/lang/InterruptedException;
                // 
                // The error that occurred was:
                // 
                // java.lang.IllegalStateException: Expression is linked from several locations: Label_0655:
                //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        };
        this.gpsLocatListener = new GpsLocationListener((GpsLocationListener)null);
        this.gpsStateListener = (GpsStatus$Listener)new GpsStatus$Listener() {
            public void onGpsStatusChanged(int n) {
                if (n == 2) {
                    if (LauncherApplication.isGPSLocation) {
                        LauncherApplication.isGPSLocation = false;
                    }
                    if (!LauncherApplication.iAccOff) {
                        MainService.this.setGpsStatus(true);
                    }
                }
                else if (n == 4) {
                    final GpsStatus gpsStatus = MainService.this.gpsManager.getGpsStatus((GpsStatus)null);
                    int maxSatellites;
                    Iterator iterator;
                    for (maxSatellites = gpsStatus.getMaxSatellites(), iterator = gpsStatus.getSatellites().iterator(), n = 0; iterator.hasNext() && n <= maxSatellites; ++n) {
                        if (iterator.next().usedInFix()) {}
                    }
                    if (n > 4) {
                        LauncherApplication.isGPSLocation = true;
                    }
                    else {
                        LauncherApplication.isGPSLocation = false;
                    }
                }
                MainService.this.app.sendMessage(1038, null);
            }
        };
        this.iIsInAndroid = true;
        this.iIsInReversing = false;
        this.iIsInOriginal = false;
        this.iIsInRecorder = false;
        this.iIsDV = false;
        this.iIsBTConnect = false;
        this.iIsScrennOff = false;
        this.totalDataIndex = -999;
        this.mHandler = new Mhandler(this);
        this.MYLOG = "listenlogread";
        this.mBinder = new MainBinder();
    }
    
    private void GLK_BTConnect() {
        if (!this.getIsBTState()) {
            final Set bondedDevices = this.adapter.getBondedDevices();
            for (final BluetoothDevice benzDevice : bondedDevices) {
                if (this.carBTname.equals(benzDevice.getName())) {
                    this.benzDevice = benzDevice;
                    Log.e("", "btstate benzDevice111 = " + this.benzDevice.getName());
                    if (this.benzDevice != null && !this.getBTA2dpState()) {
                        Log.e("", "btstate a2dp connect = " + this.benzDevice.getName());
                        this.benzA2dp.connect(this.benzDevice);
                    }
                    this.iExist = true;
                    break;
                }
            }
            if (bondedDevices.size() == 0) {
                this.iExist = false;
                this.adapter.startDiscovery();
            }
        }
    }
    
    static /* synthetic */ void access$20(final MainService mainService, final boolean iBenzA2dpConnected) {
        mainService.iBenzA2dpConnected = iBenzA2dpConnected;
    }
    
    static /* synthetic */ void access$23(final MainService mainService, final BluetoothDevice benzDevice) {
        mainService.benzDevice = benzDevice;
    }
    
    static /* synthetic */ void access$28(final MainService mainService, final BluetoothHeadset benzhHeadset) {
        mainService.benzhHeadset = benzhHeadset;
    }
    
    static /* synthetic */ void access$29(final MainService mainService, final BluetoothA2dp benzA2dp) {
        mainService.benzA2dp = benzA2dp;
    }
    
    static /* synthetic */ void access$30(final MainService mainService, final boolean iaux) {
        mainService.iAUX = iaux;
    }
    
    static /* synthetic */ void access$37(final MainService mainService, final boolean iReadyUpdate) {
        mainService.iReadyUpdate = iReadyUpdate;
    }
    
    static /* synthetic */ void access$39(final MainService mainService, final boolean iConnectCanbox) {
        mainService.iConnectCanbox = iConnectCanbox;
    }
    
    static /* synthetic */ void access$47(final MainService mainService, final AnimationDrawable animationDrawable) {
        mainService.animationDrawable = animationDrawable;
    }
    
    private void addBluetoothReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.FOUND");
        intentFilter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
        intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_CANCEL");
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        intentFilter.addAction("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
        this.registerReceiver(this.bluetoothReceiver, intentFilter);
    }
    
    private void addEasyConnectReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("net.easyconn.bt.checkstatus");
        intentFilter.addAction("net.easyconn.a2dp.acquire");
        intentFilter.addAction("net.easyconn.a2dp.release");
        intentFilter.addAction("net.easyconn.app.quit");
        this.registerReceiver(this.easyConnectReceiver, intentFilter);
    }
    
    private void addNetworkReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        this.registerReceiver(this.networkReceiver, intentFilter);
    }
    
    private void addRecoveryReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.touchus.factorytest.recovery");
        intentFilter.addAction("com.touchus.factorytest.sdcardreset");
        intentFilter.addAction("com.touchus.factorytest.getMcu");
        intentFilter.addAction("com.touchus.factorytest.benzetype");
        intentFilter.addAction(PubSysConst.ACTION_FACTORY_BREAKSET);
        this.registerReceiver(this.recoveryReceiver, intentFilter);
    }
    
    private void addUnibroadAudioFocusReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.unibroad.AudioFocus");
        intentFilter.addAction("com.unibroad.AudioFocus.REGAIN");
        this.registerReceiver(this.unibroadAudioFocusReceiver, intentFilter);
    }
    
    private void addWeatherReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.unibroad.weatherdata");
        this.registerReceiver(this.getWeatherReceiver, intentFilter);
    }
    
    private void autoStartApp() {
        this.app.startAppByPkg(this.getString(2131165188));
        this.app.setNeedStartNaviPkg(false);
    }
    
    private void autoStartNaviAPP() {
        if (!this.app.getNeedStartNaviPkgs()) {
            return;
        }
        try {
            this.mHandler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    MainService.this.autoStartApp();
                }
            }, 500L);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void broadcastEnterstandbyMode() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.mcu.enterStandbyMode");
        this.sendBroadcast(intent);
    }
    
    private void broadcastWakeUpEvent() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.mcu.wakeUp");
        intent.setFlags(32);
        this.sendBroadcast(intent);
    }
    
    private void closeRelay(final boolean b) {
        if (!b && this.iNaviSound) {
            return;
        }
        try {
            Mainboard.getInstance().openOrCloseRelay(false);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void connect(final BluetoothA2dp bluetoothA2dp) {
        if (bluetoothA2dp == null) {
            Log.e("", "btstate a2dp null");
        }
        else if (!this.iIsInOriginal) {
            this.changeAudioSource();
        }
    }
    
    private void dealwithAirplaneMode() {
        boolean b = true;
        this.lastAirplaneTime = System.currentTimeMillis();
        if (Settings$Global.getInt(this.getContentResolver(), "airplane_mode_on", 0) != 1) {
            b = false;
        }
        if ((b && this.iIsEnterAirplaneMode) || (!b && !this.iIsEnterAirplaneMode)) {
            return;
        }
        this.app.changeAirplane(this.iIsEnterAirplaneMode);
    }
    
    private void decMap(final boolean b) {
        int n = 1;
        final Intent intent = new Intent();
        intent.setAction("AUTONAVI_STANDARD_BROADCAST_RECV");
        intent.putExtra("KEY_TYPE", 10027);
        intent.putExtra("EXTRA_TYPE", 1);
        if (!b) {
            n = 0;
        }
        intent.putExtra("EXTRA_OPERA", n);
        this.sendBroadcast(intent);
    }
    
    private void enterAcc() {
        Mainboard.getInstance().readyToStandby();
        if (!LauncherApplication.iAccOff) {
            LauncherApplication.iAccOff = true;
            this.iNaviSound = false;
            this.iAUX = false;
            this.app.dismissDialog();
            this.app.btservice.setBTEnterACC(LauncherApplication.iAccOff);
            Log.e("ENTERACC", "enterAcc");
            if (this.loadingFloatLayout != null) {
                this.removeLoadingFloatView();
            }
            this.mHandler.removeCallbacks(this.getWeatherRunnabale);
            this.app.requestMainAudioFocus();
            this.broadcastEnterstandbyMode();
            this.checkHadNaviRunning();
            this.stopOtherAPP();
            if (this.app.launcherHandler != null) {
                this.app.launcherHandler.obtainMessage(1024).sendToTarget();
            }
        }
    }
    
    private boolean getBTA2dpState() {
        boolean b = false;
        boolean iaux = false;
        this.logger.debug("", "btstate SysConst.isBT = " + SysConst.isBT() + ",iAUX = " + this.iAUX);
        if (!SysConst.isBT()) {
            iaux = this.iAUX;
        }
        else if (this.benzA2dp != null) {
            Log.e("", "btstate iBenzA2dpConnected = " + this.iBenzA2dpConnected);
            Log.e("", "btstate  A2dpState = " + this.benzA2dp.getState(this.benzDevice));
            final boolean iBenzA2dpConnected = this.iBenzA2dpConnected;
            if (this.benzA2dp.getState(this.benzDevice) == 2) {
                b = true;
            }
            return b | iBenzA2dpConnected;
        }
        return iaux;
    }
    
    private int getBTHeadsetState() {
        Log.e("", "btstate  HeadsetState = " + this.benzhHeadset.getConnectionState(this.benzDevice));
        return this.benzhHeadset.getConnectionState(this.benzDevice);
    }
    
    private String getCurrentTopAppPkg() {
        final List runningTasks = this.mActivityManager.getRunningTasks(1);
        if (runningTasks == null || runningTasks.size() == 0) {
            return "";
        }
        return runningTasks.get(0).topActivity.getPackageName();
    }
    
    private boolean getIsBTState() {
        return this.getBTA2dpState() && this.iAUX;
    }
    
    private void handlerMsg(final Message message) {
        if (message.what == 2001) {
            this.createLoadingFloatView(this.getString(2131165296));
            this.mHandler.postDelayed(this.removeLoadingRunable, (long)((int)message.obj * 1000));
        }
        else if (message.what == 6001) {
            final Bundle data = message.getData();
            final byte byte1 = data.getByte("idriver_enum");
            final byte byte2 = data.getByte("idriver_state_enum");
            if (byte1 == Mainboard.EIdriverEnum.POWER_OFF.getCode() && byte2 == Mainboard.EIdriverEnum.DOWN.getCode()) {
                if (!this.iInAccState) {
                    this.iInAccState = true;
                    Mainboard.getInstance().enterIntoAcc();
                    return;
                }
                this.iInAccState = false;
                Mainboard.getInstance().wakeupMcu();
            }
        }
        else if (message.what == 1123) {
            final int arg1 = message.arg1;
            final int n = arg1 * 128;
            if (arg1 >= this.totalDataIndex) {
                return;
            }
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (arg1 < this.totalDataIndex - 1) {
                byteArrayOutputStream.write(this.upgradeByteData, n, 128);
            }
            else {
                byteArrayOutputStream.write(this.upgradeByteData, n, this.upgradeByteData.length - n);
            }
            Mainboard.getInstance().sendUpdateCanboxInfoByIndex(arg1, byteArrayOutputStream.toByteArray());
        }
        else {
            if (message.what == 1130) {
                Mainboard.getInstance().sendUpgradeMcuHeaderInfo(this.mcuTotalDataIndex);
                return;
            }
            if (message.what == 1133) {
                final int arg2 = message.arg1;
                final int n2 = arg2 * 128;
                if (arg2 >= this.mcuTotalDataIndex) {
                    return;
                }
                final ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                if (arg2 < this.mcuTotalDataIndex - 1) {
                    byteArrayOutputStream2.write(this.mcuUpgradeByteData, n2, 128);
                }
                else {
                    byteArrayOutputStream2.write(this.mcuUpgradeByteData, n2, this.mcuUpgradeByteData.length - n2);
                }
                Mainboard.getInstance().sendUpdateMcuInfoByIndex(arg2, byteArrayOutputStream2.toByteArray());
            }
            else if (message.what != 1033) {
                if (message.what == 1036) {
                    UtilTools.sendKeyeventToSystem(3);
                    this.mHandler.postDelayed((Runnable)new Runnable() {
                        private final /* synthetic */ int val$pos = (int)message.obj;
                        
                        @Override
                        public void run() {
                            MainService.this.app.launcherHandler.obtainMessage(1036, (Object)this.val$pos).sendToTarget();
                        }
                    }, 200L);
                    return;
                }
                if (message.what == 1041) {
                    if (this.benzA2dp != null) {
                        Log.e("", "btstate a2dp iBenzA2dpConnected = " + this.iBenzA2dpConnected);
                        if (this.getBTA2dpState()) {
                            this.mHandler.post(this.removeLoadingRunable);
                            BTConnectDialog.getInstance().dissShow();
                            this.setConnectedMusicVoice();
                            return;
                        }
                        this.setMusicMute();
                    }
                }
                else if (message.what == 1042 && this.benzhHeadset != null) {
                    switch (this.getBTHeadsetState()) {
                        case 1:
                        case 2: {
                            break;
                        }
                        default: {}
                        case 0: {
                            Log.e("", "btstate headset RECONNECTED");
                        }
                    }
                }
            }
        }
    }
    
    private void inOrOutAcc(final boolean b) {
        synchronized (this) {
            Log.e("mcu", "reciev:" + b);
            if (b) {
                this.enterAcc();
            }
            else {
                this.wakeUpFromAcc();
            }
        }
    }
    
    private void initMcuAndCanbox() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Mainboard.getInstance().setMainboardEventLisenner(new MainListenner());
                Mainboard.getInstance().readyToWork();
                Mainboard.getInstance().getMCUVersionNumber();
                Mainboard.getInstance().getReverseSetFromMcu();
                Mainboard.getInstance().getStoreDataFromMcu();
                Mainboard.getInstance().getReverseMediaSetFromMcu();
                Mainboard.getInstance().getBenzType();
                Log.e("", "listenlog QUERY CarLayer = " + (System.currentTimeMillis() - MainService.this.startTime));
                MainService.this.startTime = System.currentTimeMillis();
            }
        }).start();
        this.iConnectCanbox = false;
        this.mHandler.postDelayed(this.getConnectCanboxState, 200L);
    }
    
    private void openOrCloseRelay(final boolean b) {
        if (b) {
            this.mHandler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    MainService.this.openRelay();
                }
            }, 200L);
            return;
        }
        this.mHandler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                MainService.this.closeRelay(false);
            }
        }, 100L);
    }
    
    private void openRelay() {
        if (this.iIsScrennOff || !this.iNaviSound) {
            return;
        }
        try {
            Mainboard.getInstance().openOrCloseRelay(true);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void registerGPSListener() {
        (this.gpsManager = (LocationManager)this.getSystemService("location")).requestLocationUpdates("gps", 2000L, 6.0f, (LocationListener)this.gpsLocatListener);
        this.gpsManager.addGpsStatusListener(this.gpsStateListener);
    }
    
    private void requestBtAudio(final boolean b) {
        boolean b2 = true;
        final Mainboard instance = Mainboard.getInstance();
        final BenzModel.EBenzTpye benzTpye = BenzModel.benzTpye;
        final boolean b3 = this.app.radioPos != 0;
        final boolean b4 = this.app.naviPos == 0;
        if (LauncherApplication.isBT) {
            b2 = false;
        }
        instance.forceRequestBTAudio(b, benzTpye, b3, b4, b2, this.app.usbPos + 1);
    }
    
    private void setAirplaneMode(final boolean iIsEnterAirplaneMode) {
        while (true) {
            final long n;
            Label_0069: {
                synchronized (this) {
                    this.iIsEnterAirplaneMode = iIsEnterAirplaneMode;
                    if (this.lastAirplaneTime == -1L) {
                        this.dealwithAirplaneMode();
                    }
                    else {
                        n = System.currentTimeMillis() - this.lastAirplaneTime;
                        this.mHandler.removeCallbacks(this.airplaneRunnable);
                        if (n <= this.spaceTime) {
                            break Label_0069;
                        }
                        this.dealwithAirplaneMode();
                    }
                    return;
                }
            }
            this.mHandler.postDelayed(this.airplaneRunnable, this.spaceTime - n);
        }
    }
    
    private void setConnectedMusicVoice() {
        this.app.setTypeMute(3, false);
        if (this.app.btservice.blueMusicFocus) {
            final LauncherApplication app = this.app;
            this.app.getClass();
            app.setTypeMute(200, false);
            return;
        }
        final LauncherApplication app2 = this.app;
        this.app.getClass();
        app2.setTypeMute(200, true);
    }
    
    private void setGpsStatus(final boolean b) {
        Settings$Secure.setLocationProviderEnabled(this.getContentResolver(), "gps", b);
    }
    
    private void setMusicMute() {
        this.app.setTypeMute(3, true);
        final LauncherApplication app = this.app;
        this.app.getClass();
        app.setTypeMute(200, true);
    }
    
    private void startLocalBT() {
        if (this.adapter != null) {
            return;
        }
        this.adapter = BluetoothAdapter.getDefaultAdapter();
        if (!this.adapter.isEnabled()) {
            this.adapter.enable();
        }
        this.adapter.setScanMode(23);
        Log.e("", "btstate isEnabled = " + this.adapter.isEnabled());
        for (final BluetoothDevice bluetoothDevice : this.adapter.getBondedDevices()) {
            if (this.carBTname.equals(bluetoothDevice.getName())) {
                this.benzDevice = bluetoothDevice;
                Log.e("", "btstate benzDevice = " + this.benzDevice.getName());
            }
            if (this.testBTname.contains(bluetoothDevice.getName())) {
                this.benzDevice = bluetoothDevice;
                Log.e("", "btstate benzDevice = " + this.benzDevice.getName());
            }
        }
        this.adapter.getProfileProxy((Context)this, this.mProfileServiceListener, 2);
        this.adapter.getProfileProxy((Context)this, this.mProfileServiceListener, 1);
    }
    
    private void unregisterGPSListener() {
        this.gpsManager.removeGpsStatusListener(this.gpsStateListener);
        this.gpsManager.removeUpdates((LocationListener)this.gpsLocatListener);
    }
    
    private void wakeUpFromAcc() {
        Mainboard.getInstance().readyToWork();
        if (!LauncherApplication.iAccOff) {
            return;
        }
        Log.e("ENTERACC", "wakeUpFromAcc");
        LauncherApplication.iAccOff = false;
        this.app.btservice.setBTEnterACC(LauncherApplication.iAccOff);
        this.app.initModel();
        this.mHandler.postDelayed(this.startSettingRunnable, 1500L);
        this.initMcuAndCanbox();
        this.mHandler.removeCallbacks(this.getWeatherRunnabale);
        this.mHandler.postDelayed(this.getWeatherRunnabale, 40000L);
        this.autoStartNaviAPP();
        Log.e("ENTERACC", "wakeUpFromAcc end");
        this.broadcastWakeUpEvent();
    }
    
    public void addMusicReceiver() {
        if (this.mRemoteControlClientReceiverComponent != null) {
            return;
        }
        this.mRemoteControlClientReceiverComponent = new ComponentName(this.getPackageName(), MusicControlReceiver.class.getName());
        Log.e("", "musicControl addMusicReceiver " + this.mRemoteControlClientReceiverComponent);
        final Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
        intent.setComponent(this.mRemoteControlClientReceiverComponent);
        this.audioManager.registerMediaButtonIntent(PendingIntent.getBroadcast((Context)this, 0, intent, 0), this.mRemoteControlClientReceiverComponent);
    }
    
    public void btMusicConnect() {
        if (this.app.screenPos != 6) {
            if (SysConst.isBT()) {
                Log.e("", "Audio type is BT");
                if (this.adapter == null || !this.adapter.isEnabled()) {
                    this.startLocalBT();
                }
                else {
                    this.adapter.setScanMode(23);
                }
                this.mHandler.postDelayed((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        MainService.this.GLK_BTConnect();
                    }
                }, 30000L);
                this.connect(this.benzA2dp);
                return;
            }
            Log.e("", "Audio type is AUX");
            if (!this.iAUX) {
                final Message message = new Message();
                message.what = 2001;
                message.obj = 40;
                this.mHandler.sendMessage(message);
                this.requestBtAudio(false);
            }
        }
    }
    
    public void changeAudioSource() {
        final Message message = new Message();
        message.what = 2001;
        if (this.getBTA2dpState()) {
            if (!this.iAUX) {
                if (BenzModel.benzCan == BenzModel.EBenzCAN.ZMYT) {
                    message.obj = 40;
                }
                else {
                    message.obj = 20;
                }
                this.mHandler.sendMessage(message);
                this.requestBtAudio(false);
            }
            return;
        }
        message.obj = 40;
        this.mHandler.sendMessage(message);
        this.requestBtAudio(true);
    }
    
    public boolean checkHadNaviRunning() {
        final List runningTasks = this.mActivityManager.getRunningTasks(10);
        if (runningTasks == null) {
            return false;
        }
        final String string = this.getString(2131165188);
        final boolean b = false;
        final String string2 = runningTasks.get(0).topActivity.toString();
        boolean needStartNaviPkg = b;
        if (!TextUtils.isEmpty((CharSequence)string2)) {
            needStartNaviPkg = b;
            if (string2.contains(string)) {
                needStartNaviPkg = true;
            }
        }
        this.app.setNeedStartNaviPkg(needStartNaviPkg);
        return needStartNaviPkg;
    }
    
    public void closeNaviThread() {
        this.mHandler.removeCallbacks(this.checkNavigationRunnable);
        this.removeDesk();
    }
    
    public void createDeskFloat() {
        if (this.mDesktopLayout != null) {
            return;
        }
        this.mDesktopLayout = new DesktopLayout((Context)this);
        this.mWindowManager = (WindowManager)this.getApplicationContext().getSystemService("window");
        final WindowManager$LayoutParams windowManager$LayoutParams = new WindowManager$LayoutParams();
        windowManager$LayoutParams.type = 2003;
        windowManager$LayoutParams.flags = 262184;
        windowManager$LayoutParams.format = 1;
        windowManager$LayoutParams.gravity = 83;
        windowManager$LayoutParams.width = -2;
        windowManager$LayoutParams.height = -2;
        this.mDesktopLayout.setOnTouchListener((View$OnTouchListener)new View$OnTouchListener() {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                Log.e("DesktopLayout", "out MotionEvent event = " + motionEvent);
                return motionEvent.getAction() == 4 && MainService.this.mDesktopLayout.hideBtn();
            }
        });
        try {
            this.mWindowManager.addView((View)this.mDesktopLayout, (ViewGroup$LayoutParams)windowManager$LayoutParams);
        }
        catch (Exception ex) {
            Log.d("launcherlog", "canbox:error:mWindowManager.addView(mDesktopLayout)");
        }
    }
    
    public void createLoadingFloatView(final String s) {
        if (this.loadingFloatLayout != null) {
            return;
        }
        String string = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            string = this.getString(2131165294);
        }
        if (this.mWindowManager == null) {
            this.mWindowManager = (WindowManager)this.getSystemService("window");
        }
        final WindowManager$LayoutParams windowManager$LayoutParams = new WindowManager$LayoutParams();
        windowManager$LayoutParams.type = 2003;
        windowManager$LayoutParams.format = 1;
        windowManager$LayoutParams.flags = 131072;
        windowManager$LayoutParams.gravity = 51;
        windowManager$LayoutParams.x = 677;
        windowManager$LayoutParams.y = 160;
        windowManager$LayoutParams.width = 415;
        windowManager$LayoutParams.height = 147;
        this.loadingFloatLayout = (FrameLayout)LayoutInflater.from((Context)this.getApplication()).inflate(2130903103, (ViewGroup)null);
        final TextView textView = (TextView)this.loadingFloatLayout.findViewById(2131427677);
        final ImageView imageView = (ImageView)this.loadingFloatLayout.findViewById(2131427679);
        final TextView textView2 = (TextView)this.loadingFloatLayout.findViewById(2131427680);
        if (!string.equals(this.getString(2131165296))) {
            textView2.setVisibility(8);
        }
        else {
            textView2.setVisibility(0);
            textView2.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.ORIGINAL);
                    MainService.this.removeLoadingFloatView();
                }
            });
        }
        textView.setText((CharSequence)string);
        this.mWindowManager.addView((View)this.loadingFloatLayout, (ViewGroup$LayoutParams)windowManager$LayoutParams);
        this.mHandler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                imageView.setImageResource(2130837807);
                MainService.access$47(MainService.this, (AnimationDrawable)imageView.getDrawable());
                MainService.this.animationDrawable.start();
            }
        }, 100L);
    }
    
    public void createNoTouchScreens() {
        if (this.noTouchLayout != null) {
            return;
        }
        if (this.mWindowManager == null) {
            this.mWindowManager = (WindowManager)this.getSystemService("window");
        }
        final WindowManager$LayoutParams windowManager$LayoutParams = new WindowManager$LayoutParams();
        windowManager$LayoutParams.type = 2002;
        windowManager$LayoutParams.format = 1;
        windowManager$LayoutParams.flags = 40;
        windowManager$LayoutParams.gravity = 51;
        windowManager$LayoutParams.x = 0;
        windowManager$LayoutParams.y = 0;
        windowManager$LayoutParams.width = 1280;
        windowManager$LayoutParams.height = 480;
        (this.noTouchLayout = (MyLinearlayout)LayoutInflater.from((Context)this.getApplication()).inflate(2130903100, (ViewGroup)null)).setOnTouchListener((View$OnTouchListener)new View$OnTouchListener() {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                Log.e("", "listenlog MyLinearlayout x:" + motionEvent.getX() + " y:" + motionEvent.getY());
                Log.e("", "listenlog MyLinearlayout iIsInReversing = " + MainService.this.app.service.iIsInReversing + ",iIsInOriginal = " + MainService.this.app.service.iIsInOriginal + ",iIsBTConnect = " + MainService.this.app.service.iIsBTConnect + ",iIsInRecorder = " + MainService.this.app.service.iIsBTConnect + ",iIsScrennOff = " + MainService.this.app.service.iIsScrennOff);
                if (MainService.this.app.service != null && !MainService.this.iIsInAndroid) {
                    Log.e("", "noTouchLayout exist");
                    return true;
                }
                return false;
            }
        });
        Log.e("", "listenlog creat noTouchLayout  = " + (System.currentTimeMillis() - this.startTime));
        while (true) {
            try {
                this.mWindowManager.addView((View)this.noTouchLayout, (ViewGroup$LayoutParams)windowManager$LayoutParams);
                final Bundle bundle = new Bundle();
                bundle.putBoolean("isRed", true);
                this.app.sendMessage(1044, bundle);
            }
            catch (Exception ex) {
                Log.d("launcherlog", "canbox:error:mWindowManager.addView(onNoTouchLayout)");
                continue;
            }
            break;
        }
    }
    
    public void enterIntoUpgradeCanboxCheck() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     3: dup            
                //     4: aload_0        
                //     5: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //     8: ldc             2131165191
                //    10: invokevirtual   com/touchus/benchilauncher/MainService.getString:(I)Ljava/lang/String;
                //    13: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
                //    16: astore_3       
                //    17: aload_3        
                //    18: invokevirtual   java/io/File.exists:()Z
                //    21: ifne            25
                //    24: return         
                //    25: new             Ljava/io/ByteArrayOutputStream;
                //    28: dup            
                //    29: aload_3        
                //    30: invokevirtual   java/io/File.length:()J
                //    33: l2i            
                //    34: invokespecial   java/io/ByteArrayOutputStream.<init>:(I)V
                //    37: astore          6
                //    39: aconst_null    
                //    40: astore_2       
                //    41: aconst_null    
                //    42: astore          5
                //    44: new             Ljava/io/BufferedInputStream;
                //    47: dup            
                //    48: new             Ljava/io/FileInputStream;
                //    51: dup            
                //    52: aload_3        
                //    53: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
                //    56: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
                //    59: astore_3       
                //    60: sipush          1024
                //    63: newarray        B
                //    65: astore_2       
                //    66: aload_3        
                //    67: aload_2        
                //    68: iconst_0       
                //    69: sipush          1024
                //    72: invokevirtual   java/io/BufferedInputStream.read:([BII)I
                //    75: istore_1       
                //    76: iconst_m1      
                //    77: iload_1        
                //    78: if_icmpne       211
                //    81: aload_0        
                //    82: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //    85: aload           6
                //    87: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
                //    90: invokestatic    com/touchus/benchilauncher/MainService.access$43:(Lcom/touchus/benchilauncher/MainService;[B)V
                //    93: aload_3        
                //    94: invokevirtual   java/io/BufferedInputStream.close:()V
                //    97: aload           6
                //    99: invokevirtual   java/io/ByteArrayOutputStream.close:()V
                //   102: ldc             "driverlog"
                //   104: new             Ljava/lang/StringBuilder;
                //   107: dup            
                //   108: ldc             "canbox:upgradeByteData.length:"
                //   110: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   113: aload_0        
                //   114: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   117: invokestatic    com/touchus/benchilauncher/MainService.access$44:(Lcom/touchus/benchilauncher/MainService;)[B
                //   120: arraylength    
                //   121: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   124: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   127: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   130: pop            
                //   131: aload_0        
                //   132: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   135: aload_0        
                //   136: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   139: invokestatic    com/touchus/benchilauncher/MainService.access$44:(Lcom/touchus/benchilauncher/MainService;)[B
                //   142: arraylength    
                //   143: bipush          127
                //   145: iadd           
                //   146: sipush          128
                //   149: idiv           
                //   150: invokestatic    com/touchus/benchilauncher/MainService.access$45:(Lcom/touchus/benchilauncher/MainService;I)V
                //   153: ldc             "driverlog"
                //   155: new             Ljava/lang/StringBuilder;
                //   158: dup            
                //   159: ldc             "canbox:totalDataIndex:"
                //   161: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   164: aload_0        
                //   165: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   168: invokestatic    com/touchus/benchilauncher/MainService.access$10:(Lcom/touchus/benchilauncher/MainService;)I
                //   171: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   174: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   177: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   180: pop            
                //   181: aload_0        
                //   182: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   185: iconst_0       
                //   186: invokestatic    com/touchus/benchilauncher/MainService.access$37:(Lcom/touchus/benchilauncher/MainService;Z)V
                //   189: aload_0        
                //   190: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   193: invokestatic    com/touchus/benchilauncher/MainService.access$1:(Lcom/touchus/benchilauncher/MainService;)Lcom/touchus/benchilauncher/MainService$Mhandler;
                //   196: aload_0        
                //   197: getfield        com/touchus/benchilauncher/MainService$17.this$0:Lcom/touchus/benchilauncher/MainService;
                //   200: getfield        com/touchus/benchilauncher/MainService.canboxtThread:Ljava/lang/Thread;
                //   203: ldc2_w          200
                //   206: invokevirtual   com/touchus/benchilauncher/MainService$Mhandler.postDelayed:(Ljava/lang/Runnable;J)Z
                //   209: pop            
                //   210: return         
                //   211: aload           6
                //   213: aload_2        
                //   214: iconst_0       
                //   215: iload_1        
                //   216: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
                //   219: goto            66
                //   222: astore          4
                //   224: aload_3        
                //   225: astore_2       
                //   226: aload           4
                //   228: invokevirtual   java/io/IOException.printStackTrace:()V
                //   231: aload_3        
                //   232: invokevirtual   java/io/BufferedInputStream.close:()V
                //   235: aload           6
                //   237: invokevirtual   java/io/ByteArrayOutputStream.close:()V
                //   240: goto            102
                //   243: astore_2       
                //   244: aload_2        
                //   245: invokevirtual   java/io/IOException.printStackTrace:()V
                //   248: goto            102
                //   251: astore_2       
                //   252: aload_2        
                //   253: invokevirtual   java/io/IOException.printStackTrace:()V
                //   256: goto            235
                //   259: astore_3       
                //   260: aload_2        
                //   261: invokevirtual   java/io/BufferedInputStream.close:()V
                //   264: aload           6
                //   266: invokevirtual   java/io/ByteArrayOutputStream.close:()V
                //   269: aload_3        
                //   270: athrow         
                //   271: astore_2       
                //   272: aload_2        
                //   273: invokevirtual   java/io/IOException.printStackTrace:()V
                //   276: goto            264
                //   279: astore_2       
                //   280: aload_2        
                //   281: invokevirtual   java/io/IOException.printStackTrace:()V
                //   284: goto            269
                //   287: astore_2       
                //   288: aload_2        
                //   289: invokevirtual   java/io/IOException.printStackTrace:()V
                //   292: goto            97
                //   295: astore_2       
                //   296: aload_2        
                //   297: invokevirtual   java/io/IOException.printStackTrace:()V
                //   300: goto            102
                //   303: astore          4
                //   305: aload_3        
                //   306: astore_2       
                //   307: aload           4
                //   309: astore_3       
                //   310: goto            260
                //   313: astore          4
                //   315: aload           5
                //   317: astore_3       
                //   318: goto            224
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  44     60     313    321    Ljava/io/IOException;
                //  44     60     259    260    Any
                //  60     66     222    224    Ljava/io/IOException;
                //  60     66     303    313    Any
                //  66     76     222    224    Ljava/io/IOException;
                //  66     76     303    313    Any
                //  81     93     222    224    Ljava/io/IOException;
                //  81     93     303    313    Any
                //  93     97     287    295    Ljava/io/IOException;
                //  97     102    295    303    Ljava/io/IOException;
                //  211    219    222    224    Ljava/io/IOException;
                //  211    219    303    313    Any
                //  226    231    259    260    Any
                //  231    235    251    259    Ljava/io/IOException;
                //  235    240    243    251    Ljava/io/IOException;
                //  260    264    271    279    Ljava/io/IOException;
                //  264    269    279    287    Ljava/io/IOException;
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index: 159, Size: 159
                //     at java.util.ArrayList.rangeCheck(Unknown Source)
                //     at java.util.ArrayList.get(Unknown Source)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }).start();
    }
    
    public void enterIntoUpgradeMcuCheck() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     3: dup            
                //     4: aload_0        
                //     5: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //     8: ldc             2131165193
                //    10: invokevirtual   com/touchus/benchilauncher/MainService.getString:(I)Ljava/lang/String;
                //    13: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
                //    16: astore_3       
                //    17: aload_3        
                //    18: invokevirtual   java/io/File.exists:()Z
                //    21: ifne            25
                //    24: return         
                //    25: new             Ljava/io/ByteArrayOutputStream;
                //    28: dup            
                //    29: aload_3        
                //    30: invokevirtual   java/io/File.length:()J
                //    33: l2i            
                //    34: invokespecial   java/io/ByteArrayOutputStream.<init>:(I)V
                //    37: astore          6
                //    39: aconst_null    
                //    40: astore_2       
                //    41: aconst_null    
                //    42: astore          5
                //    44: new             Ljava/io/BufferedInputStream;
                //    47: dup            
                //    48: new             Ljava/io/FileInputStream;
                //    51: dup            
                //    52: aload_3        
                //    53: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
                //    56: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
                //    59: astore_3       
                //    60: sipush          1024
                //    63: newarray        B
                //    65: astore_2       
                //    66: aload_3        
                //    67: ldc2_w          10240
                //    70: invokevirtual   java/io/BufferedInputStream.skip:(J)J
                //    73: pop2           
                //    74: aload_3        
                //    75: aload_2        
                //    76: iconst_0       
                //    77: sipush          1024
                //    80: invokevirtual   java/io/BufferedInputStream.read:([BII)I
                //    83: istore_1       
                //    84: iconst_m1      
                //    85: iload_1        
                //    86: if_icmpne       221
                //    89: aload_0        
                //    90: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //    93: aload           6
                //    95: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
                //    98: putfield        com/touchus/benchilauncher/MainService.mcuUpgradeByteData:[B
                //   101: aload_3        
                //   102: invokevirtual   java/io/BufferedInputStream.close:()V
                //   105: aload           6
                //   107: invokevirtual   java/io/ByteArrayOutputStream.close:()V
                //   110: ldc             "driverlog"
                //   112: new             Ljava/lang/StringBuilder;
                //   115: dup            
                //   116: ldc             "mcu:upgradeByteData.length:"
                //   118: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   121: aload_0        
                //   122: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //   125: getfield        com/touchus/benchilauncher/MainService.mcuUpgradeByteData:[B
                //   128: arraylength    
                //   129: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   132: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   135: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   138: pop            
                //   139: aload_0        
                //   140: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //   143: aload_0        
                //   144: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //   147: getfield        com/touchus/benchilauncher/MainService.mcuUpgradeByteData:[B
                //   150: arraylength    
                //   151: bipush          127
                //   153: iadd           
                //   154: sipush          128
                //   157: idiv           
                //   158: invokestatic    com/touchus/benchilauncher/MainService.access$46:(Lcom/touchus/benchilauncher/MainService;I)V
                //   161: ldc             "driverlog"
                //   163: new             Ljava/lang/StringBuilder;
                //   166: dup            
                //   167: ldc             "mcu:totalDataIndex:"
                //   169: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
                //   172: aload_0        
                //   173: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //   176: invokestatic    com/touchus/benchilauncher/MainService.access$38:(Lcom/touchus/benchilauncher/MainService;)I
                //   179: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   182: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   185: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
                //   188: pop            
                //   189: new             Ljava/io/ByteArrayOutputStream;
                //   192: dup            
                //   193: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
                //   196: astore_2       
                //   197: aload_2        
                //   198: aload_0        
                //   199: getfield        com/touchus/benchilauncher/MainService$18.this$0:Lcom/touchus/benchilauncher/MainService;
                //   202: getfield        com/touchus/benchilauncher/MainService.mcuUpgradeByteData:[B
                //   205: iconst_0       
                //   206: iconst_4       
                //   207: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
                //   210: invokestatic    com/backaudio/android/driver/Mainboard.getInstance:()Lcom/backaudio/android/driver/Mainboard;
                //   213: aload_2        
                //   214: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
                //   217: invokevirtual   com/backaudio/android/driver/Mainboard.requestUpgradeMcu:([B)V
                //   220: return         
                //   221: aload           6
                //   223: aload_2        
                //   224: iconst_0       
                //   225: iload_1        
                //   226: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
                //   229: goto            74
                //   232: astore          4
                //   234: aload_3        
                //   235: astore_2       
                //   236: aload           4
                //   238: invokevirtual   java/io/IOException.printStackTrace:()V
                //   241: aload_3        
                //   242: invokevirtual   java/io/BufferedInputStream.close:()V
                //   245: aload           6
                //   247: invokevirtual   java/io/ByteArrayOutputStream.close:()V
                //   250: goto            110
                //   253: astore_2       
                //   254: aload_2        
                //   255: invokevirtual   java/io/IOException.printStackTrace:()V
                //   258: goto            110
                //   261: astore_2       
                //   262: aload_2        
                //   263: invokevirtual   java/io/IOException.printStackTrace:()V
                //   266: goto            245
                //   269: astore_3       
                //   270: aload_2        
                //   271: invokevirtual   java/io/BufferedInputStream.close:()V
                //   274: aload           6
                //   276: invokevirtual   java/io/ByteArrayOutputStream.close:()V
                //   279: aload_3        
                //   280: athrow         
                //   281: astore_2       
                //   282: aload_2        
                //   283: invokevirtual   java/io/IOException.printStackTrace:()V
                //   286: goto            274
                //   289: astore_2       
                //   290: aload_2        
                //   291: invokevirtual   java/io/IOException.printStackTrace:()V
                //   294: goto            279
                //   297: astore_2       
                //   298: aload_2        
                //   299: invokevirtual   java/io/IOException.printStackTrace:()V
                //   302: goto            105
                //   305: astore_2       
                //   306: aload_2        
                //   307: invokevirtual   java/io/IOException.printStackTrace:()V
                //   310: goto            110
                //   313: astore          4
                //   315: aload_3        
                //   316: astore_2       
                //   317: aload           4
                //   319: astore_3       
                //   320: goto            270
                //   323: astore          4
                //   325: aload           5
                //   327: astore_3       
                //   328: goto            234
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  44     60     323    331    Ljava/io/IOException;
                //  44     60     269    270    Any
                //  60     74     232    234    Ljava/io/IOException;
                //  60     74     313    323    Any
                //  74     84     232    234    Ljava/io/IOException;
                //  74     84     313    323    Any
                //  89     101    232    234    Ljava/io/IOException;
                //  89     101    313    323    Any
                //  101    105    297    305    Ljava/io/IOException;
                //  105    110    305    313    Ljava/io/IOException;
                //  221    229    232    234    Ljava/io/IOException;
                //  221    229    313    323    Any
                //  236    241    269    270    Any
                //  241    245    261    269    Ljava/io/IOException;
                //  245    250    253    261    Ljava/io/IOException;
                //  270    274    281    289    Ljava/io/IOException;
                //  274    279    289    297    Ljava/io/IOException;
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index: 165, Size: 165
                //     at java.util.ArrayList.rangeCheck(Unknown Source)
                //     at java.util.ArrayList.get(Unknown Source)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }).start();
    }
    
    public void forceStopApp(final String s) {
        this.mActivityManager.forceStopPackage(s);
    }
    
    public void getWeather() {
        final Intent intent = new Intent();
        intent.setAction("com.unibroad.getweather");
        intent.putExtra("cityName", this.app.gpsCityName);
        this.sendBroadcast(intent);
    }
    
    public IBinder onBind(final Intent intent) {
        return (IBinder)this.mBinder;
    }
    
    public void onCreate() {
        super.onCreate();
        this.app = (LauncherApplication)this.getApplication();
        this.app.serviceHandler = this.mHandler;
        this.mSpUtilK = new SpUtilK(this.getApplicationContext());
        this.audioManager = (AudioManager)this.getSystemService("audio");
        this.mActivityManager = (ActivityManager)this.getSystemService("activity");
        this.startTime = System.currentTimeMillis();
        Log.e("", "listenlog kill KernelMCU  = " + this.startTime);
        Utiltools.stopKernelMCU();
        this.addWeatherReceiver();
        this.addNetworkReceiver();
        this.addEasyConnectReceiver();
        this.addUnibroadAudioFocusReceiver();
        this.addRecoveryReceiver();
        this.addMusicReceiver();
        this.registerGPSListener();
        Log.e("", "listenlog initMCU  = " + (System.currentTimeMillis() - this.startTime));
        this.startTime = System.currentTimeMillis();
        this.broadcastWakeUpEvent();
        this.initMcuAndCanbox();
        this.app.requestMainAudioFocus();
        this.mHandler.postDelayed(this.startSettingRunnable, 1500L);
        this.mHandler.postDelayed(this.getWeatherRunnabale, 20000L);
        this.app.setTypeMute(3, true);
        this.app.setTypeMute(2, false);
        if (this.app.getWifiApState()) {
            WifiTool.openWifiAp((Context)this, WifiTool.getCurrentWifiApConfig((Context)this));
        }
        UtilTools.echoFile(new StringBuilder().append(this.app.breakpos).toString(), PubSysConst.GT9XX_INT_TRIGGER);
        this.testBTname.add("A2");
        this.testBTname.add("icar_500724");
    }
    
    public void onDestroy() {
        this.unregisterReceiver(this.getWeatherReceiver);
        this.unregisterReceiver(this.networkReceiver);
        this.unregisterReceiver(this.unibroadAudioFocusReceiver);
        this.unregisterReceiver(this.recoveryReceiver);
        this.unregisterReceiver(this.easyConnectReceiver);
        if (SysConst.isBT()) {
            this.unregisterReceiver(this.bluetoothReceiver);
        }
        this.unregisterMusicReceiver();
        this.unregisterGPSListener();
        this.app.abandonMainAudioFocus();
        super.onDestroy();
    }
    
    public void openNaviThread() {
        this.mHandler.postDelayed(this.checkNavigationRunnable, 500L);
    }
    
    public void regainWeather() {
        this.mHandler.removeCallbacks(this.getWeatherRunnabale);
        this.mHandler.postDelayed(this.getWeatherRunnabale, 2000L);
    }
    
    public void removeDesk() {
        if (this.mDesktopLayout == null) {
            return;
        }
        try {
            this.mWindowManager.removeViewImmediate((View)this.mDesktopLayout);
            this.mDesktopLayout = null;
        }
        catch (Exception ex) {
            Log.d("launcherlog", "canbox:error:mWindowManager.removeView(mDesktopLayout)");
        }
    }
    
    public void removeLoadingFloatView() {
        if (this.loadingFloatLayout == null) {
            return;
        }
        while (true) {
            try {
                this.animationDrawable.stop();
                this.animationDrawable = null;
                try {
                    this.mWindowManager.removeViewImmediate((View)this.loadingFloatLayout);
                    this.loadingFloatLayout = null;
                }
                catch (Exception ex) {
                    Log.d("launcherlog", "canbox:error:mWindowManager.removeView(bootFloatLayout)");
                }
            }
            catch (Exception ex2) {
                Log.d("launcherlog", "canbox:error:animationDrawable.stop()");
                continue;
            }
            break;
        }
    }
    
    public void removeNoTouchScreens() {
        if (this.noTouchLayout == null) {
            return;
        }
        while (true) {
            try {
                this.mWindowManager.removeView((View)this.noTouchLayout);
                final Bundle bundle = new Bundle();
                bundle.putBoolean("isRed", false);
                this.app.sendMessage(1044, bundle);
                this.noTouchLayout = null;
            }
            catch (Exception ex) {
                Log.d("launcherlog", "canbox:error:mWindowManager.removeView(onNoTouchLayout)");
                continue;
            }
            break;
        }
    }
    
    public void sendEasyConnectBroadcast(final String action) {
        final Intent intent = new Intent();
        intent.setAction(action);
        if (action.equals("net.easyconn.bt.opened")) {
            intent.putExtra("name", BluetoothService.deviceName);
        }
        this.sendBroadcast(intent);
    }
    
    public void stopOtherAPP() {
        final List runningTasks = this.mActivityManager.getRunningTasks(10);
        if (runningTasks != null) {
            Log.e("", "enteracc start stop app = " + runningTasks.size());
            for (int i = 0; i < runningTasks.size(); ++i) {
                final String packageName = runningTasks.get(i).topActivity.getPackageName();
                Log.e("", "enteracc start stop cmpNameTemp = " + packageName);
                if (!TextUtils.isEmpty((CharSequence)packageName) && !packageName.equals(this.getPackageName())) {
                    this.mActivityManager.forceStopPackage(packageName);
                }
            }
        }
    }
    
    public void unregisterMusicReceiver() {
        if (this.mRemoteControlClientReceiverComponent == null) {
            return;
        }
        Log.e("", "musicControl unregisterMusicReceiver " + this.mRemoteControlClientReceiverComponent);
        this.audioManager.unregisterMediaButtonEventReceiver(this.mRemoteControlClientReceiverComponent);
        this.mRemoteControlClientReceiverComponent = null;
    }
    
    private class GpsLocationListener implements LocationListener
    {
        public void onLocationChanged(final Location location) {
            if (location == null) {}
        }
        
        public void onProviderDisabled(final String s) {
            Log.d("launcherlog", "ProviderDisabled : " + s);
        }
        
        public void onProviderEnabled(final String s) {
            Log.d("launcherlog", "ProviderEnabled : " + s);
        }
        
        public void onStatusChanged(final String s, final int n, final Bundle bundle) {
            Log.d("launcherlog", "StatusChanged : " + s + n);
        }
    }
    
    public class MainBinder extends Binder
    {
        public MainService getService() {
            return MainService.this;
        }
    }
    
    class MainListenner implements IMainboardEventLisenner
    {
        static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer() {
            final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer = MainListenner.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer;
            if ($switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer != null) {
                return $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer;
            }
            final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2 = new int[Mainboard.ECarLayer.values().length];
            while (true) {
                try {
                    $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.ANDROID.ordinal()] = 1;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.BT_CONNECT.ordinal()] = 6;
                        try {
                            $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.DV.ordinal()] = 3;
                            try {
                                $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.ORIGINAL.ordinal()] = 5;
                                try {
                                    $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.ORIGINAL_REVERSE.ordinal()] = 7;
                                    try {
                                        $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.QUERY.ordinal()] = 10;
                                        try {
                                            $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.RECORDER.ordinal()] = 2;
                                            try {
                                                $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.REVERSE.ordinal()] = 9;
                                                try {
                                                    $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.REVERSE_360.ordinal()] = 4;
                                                    try {
                                                        $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2[Mainboard.ECarLayer.SCREEN_OFF.ordinal()] = 8;
                                                        return MainListenner.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer = $switch_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer2;
                                                    }
                                                    catch (NoSuchFieldError noSuchFieldError) {}
                                                }
                                                catch (NoSuchFieldError noSuchFieldError2) {}
                                            }
                                            catch (NoSuchFieldError noSuchFieldError3) {}
                                        }
                                        catch (NoSuchFieldError noSuchFieldError4) {}
                                    }
                                    catch (NoSuchFieldError noSuchFieldError5) {}
                                }
                                catch (NoSuchFieldError noSuchFieldError6) {}
                            }
                            catch (NoSuchFieldError noSuchFieldError7) {}
                        }
                        catch (NoSuchFieldError noSuchFieldError8) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError9) {}
                }
                catch (NoSuchFieldError noSuchFieldError10) {
                    continue;
                }
                break;
            }
        }
        
        static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye() {
            final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye = MainListenner.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye;
            if ($switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye != null) {
                return $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye;
            }
            final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye2 = new int[Mainboard.EReverseTpye.values().length];
            while (true) {
                try {
                    $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye2[Mainboard.EReverseTpye.ADD_REVERSE.ordinal()] = 1;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye2[Mainboard.EReverseTpye.ORIGINAL_REVERSE.ordinal()] = 2;
                        try {
                            $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye2[Mainboard.EReverseTpye.REVERSE_360.ordinal()] = 3;
                            return MainListenner.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye = $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye2;
                        }
                        catch (NoSuchFieldError noSuchFieldError) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError2) {}
                }
                catch (NoSuchFieldError noSuchFieldError3) {
                    continue;
                }
                break;
            }
        }
        
        static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet() {
            final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet = MainListenner.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet;
            if ($switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet != null) {
                return $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet;
            }
            final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet2 = new int[Mainboard.EReverserMediaSet.values().length];
            while (true) {
                try {
                    $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet2[Mainboard.EReverserMediaSet.FLAT.ordinal()] = 2;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet2[Mainboard.EReverserMediaSet.MUTE.ordinal()] = 1;
                        try {
                            $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet2[Mainboard.EReverserMediaSet.NOMAL.ordinal()] = 3;
                            try {
                                $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet2[Mainboard.EReverserMediaSet.QUERY.ordinal()] = 4;
                                return MainListenner.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet = $switch_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet2;
                            }
                            catch (NoSuchFieldError noSuchFieldError) {}
                        }
                        catch (NoSuchFieldError noSuchFieldError2) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError3) {}
                }
                catch (NoSuchFieldError noSuchFieldError4) {
                    continue;
                }
                break;
            }
        }
        
        @Override
        public void logcatCanbox(final String s) {
            final Intent intent = new Intent();
            intent.setAction("com.unibroad.logcatcanbox");
            intent.putExtra("canbox", s);
            MainService.this.sendBroadcast(intent);
        }
        
        @Override
        public void obtainBenzSize(final int screenPos) {
            MainService.this.app.screenPos = screenPos;
            MainService.this.mSpUtilK.putInt("screenType", screenPos);
            Log.d(MainService.this.MYLOG, "<<<\tobtainBenzSize:\ttype--" + screenPos);
        }
        
        @Override
        public void obtainBenzType(final BenzModel.EBenzTpye benzTpye) {
            Log.e("", "obtainBenzType before benzTpye = " + benzTpye);
            BenzModel.benzTpye = benzTpye;
            MainService.this.mSpUtilK.putInt(BenzModel.KEY, benzTpye.getCode());
            Log.d(MainService.this.MYLOG, "<<<\tobtainBenzType:\tEBenzTpye--" + benzTpye);
            final Bundle bundle = new Bundle();
            bundle.putParcelable("runningState", (Parcelable)MainService.this.app.carRunInfo);
            MainService.this.app.sendMessage(1008, bundle);
        }
        
        @Override
        public void obtainBrightness(final int n) {
        }
        
        @Override
        public void obtainLanguageMediaSet(final Mainboard.ELanguage eLanguage) {
            Log.d(MainService.this.MYLOG, "<<<\tobtainLanguageMediaSet: eLanguage--" + eLanguage);
        }
        
        @Override
        public void obtainReverseMediaSet(final Mainboard.EReverserMediaSet eMediaSet) {
            switch ($SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverserMediaSet()[eMediaSet.ordinal()]) {
                case 1: {
                    MainService.this.mSpUtilK.putInt("voiceType", 0);
                    break;
                }
                case 3: {
                    MainService.this.mSpUtilK.putInt("voiceType", 1);
                    break;
                }
            }
            MainService.this.app.eMediaSet = eMediaSet;
            Log.d(MainService.this.MYLOG, "<<<\tobtainReverseMediaSet:\tEReverserMediaSet--" + eMediaSet);
        }
        
        @Override
        public void obtainReverseType(final Mainboard.EReverseTpye eReverseTpye) {
            int n = 1;
            switch ($SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EReverseTpye()[eReverseTpye.ordinal()]) {
                case 2: {
                    n = 0;
                    break;
                }
                case 1: {
                    n = 1;
                    break;
                }
                case 3: {
                    n = 2;
                    break;
                }
            }
            MainService.this.mSpUtilK.putInt("reversingType", n);
            Log.d(MainService.this.MYLOG, "<<<\tobtainReverseType:\tEReverseTpye--" + eReverseTpye + "type = " + n);
        }
        
        @Override
        public void obtainStoreData(final List<Byte> list) {
            Log.d(MainService.this.MYLOG, "<<<\tobtainStoreData:\tdata--" + list);
            if (list.get(0) == 1) {
                MainService.this.app.isOriginalKeyOpen = true;
            }
            else {
                MainService.this.app.isOriginalKeyOpen = false;
            }
            MainService.this.mSpUtilK.putBoolean(MainService.this.getString(2131165345), MainService.this.app.isOriginalKeyOpen);
            SysConst.storeData[0] = list.get(0);
            SysConst.storeData[1] = list.get(1);
            SysConst.storeData[2] = list.get(2);
            if (list.get(3) == 1) {
                MainService.this.app.ismix = true;
            }
            else {
                MainService.this.app.ismix = false;
            }
            MainService.this.mSpUtilK.putBoolean(MainService.this.getString(2131165346), MainService.this.app.ismix);
            SysConst.storeData[3] = list.get(3);
            SysConst.storeData[4] = list.get(4);
            MainService.this.mSpUtilK.putInt("radioType", list.get(4));
            MainService.this.app.radioPos = list.get(4);
            SysConst.storeData[5] = list.get(5);
            MainService.this.mSpUtilK.putInt("naviExist", list.get(5));
            MainService.this.app.naviPos = list.get(5);
            SysConst.storeData[6] = list.get(6);
            MainService.this.mSpUtilK.putInt("connectType", list.get(6));
            MainService.this.app.connectPos = list.get(6);
            if (list.get(6) == 1) {
                LauncherApplication.isBT = false;
            }
            else {
                LauncherApplication.isBT = true;
            }
            SysConst.storeData[7] = list.get(7);
            MainService.this.mSpUtilK.putInt("usbNum", list.get(7));
            MainService.this.app.usbPos = list.get(7);
            if (!LauncherApplication.isBT) {
                SysConst.storeData[8] = list.get(8);
                MainService.this.app.isAirhide = (list.get(8) == 1);
                MainService.this.mSpUtilK.putBoolean(MainService.this.getString(2131165347), MainService.this.app.isAirhide);
            }
            else {
                MainService.this.app.ismix = false;
                MainService.this.mSpUtilK.putBoolean(MainService.this.getString(2131165346), false);
                SysConst.musicDecVolume = 0.3f;
            }
            SysConst.storeData[9] = list.get(9);
            MainService.this.mSpUtilK.putInt(PubSysConst.KEY_BREAKPOS, list.get(9));
            MainService.this.app.breakpos = list.get(9);
            UtilTools.echoFile(new StringBuilder().append(MainService.this.app.breakpos).toString(), PubSysConst.GT9XX_INT_TRIGGER);
        }
        
        @Override
        public void obtainVersionDate(final String s) {
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<obtainVersionDate\t\tversionDate--" + s);
        }
        
        @Override
        public void obtainVersionNumber(final String curMcuVersion) {
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<obtainVersionNumber\t\tversionNumber--" + curMcuVersion);
            MainService.this.app.curMcuVersion = curMcuVersion;
        }
        
        @Override
        public void onAUXActivateStutas(final Mainboard.EAUXStutas eauxStutas) {
            final Bundle bundle = new Bundle();
            bundle.putSerializable("aux_activate_stutas", (Serializable)eauxStutas);
            MainService.this.app.sendMessage(1048, bundle);
            Log.d(MainService.this.MYLOG, "<<<\tonAUXActivateStutas: eStutas--" + eauxStutas);
        }
        
        @Override
        public void onAirInfo(final AirInfo airInfo) {
            MainService.this.app.airInfo = airInfo;
            final Bundle bundle = new Bundle();
            bundle.putParcelable("airInfo", (Parcelable)airInfo);
            MainService.this.app.sendMessage(1012, bundle);
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onAirInfo\t\tAirInfo--" + airInfo.toString());
        }
        
        @Override
        public void onCanboxInfo(final String curCanboxVersion) {
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onCanboxInfo\t\tinfo--" + curCanboxVersion);
            MainService.this.app.curCanboxVersion = curCanboxVersion;
            MainService.access$39(MainService.this, true);
            if (TextUtils.isEmpty((CharSequence)MainService.this.app.curCanboxVersion)) {
                return;
            }
            Log.e("", "onCanboxInfo before BenzModel.benzTpye = " + BenzModel.benzTpye);
            if (curCanboxVersion.contains("BEZ") && BenzModel.benzCan != BenzModel.EBenzCAN.XBS) {
                BenzModel.benzCan = BenzModel.EBenzCAN.XBS;
            }
            else if ((curCanboxVersion.contains("YT") || curCanboxVersion.contains("YX")) && BenzModel.benzCan != BenzModel.EBenzCAN.ZMYT) {
                BenzModel.benzCan = BenzModel.EBenzCAN.ZMYT;
            }
            else if (curCanboxVersion.contains("Benz") && BenzModel.benzCan != BenzModel.EBenzCAN.ZHTD) {
                BenzModel.benzCan = BenzModel.EBenzCAN.ZHTD;
            }
            MainService.this.app.sendMessage(1127, null);
        }
        
        @Override
        public void onCanboxUpgradeForGetDataByIndex(int n) {
            Log.d("driverlog", "canbox:nextIndex:" + (n + 1));
            final Message obtainMessage = MainService.this.mHandler.obtainMessage(1123);
            obtainMessage.arg1 = n + 1;
            obtainMessage.sendToTarget();
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onCanboxUpgradeForGetDataByIndex\t\tnextIndex--" + (n + 1));
            n = (int)(n * 1.0 / MainService.this.totalDataIndex * 100.0);
            if (n <= 100) {
                final Bundle bundle = new Bundle();
                bundle.putString("upgradePer", String.valueOf(n) + "%");
                MainService.this.app.sendMessage(1113, bundle);
            }
        }
        
        @Override
        public void onCanboxUpgradeState(final Mainboard.ECanboxUpgrade eCanboxUpgrade) {
            if (eCanboxUpgrade == Mainboard.ECanboxUpgrade.READY_UPGRADING) {
                MainService.this.mHandler.removeCallbacks((Runnable)MainService.this.canboxtThread);
                MainService.access$37(MainService.this, true);
                final Message obtainMessage = MainService.this.mHandler.obtainMessage(1123);
                obtainMessage.arg1 = 0;
                obtainMessage.sendToTarget();
            }
            else if (eCanboxUpgrade == Mainboard.ECanboxUpgrade.FINISH) {
                MainService.this.app.sendMessage(1121, null);
                MainService.access$37(MainService.this, false);
            }
            else {
                MainService.this.app.sendMessage(1129, null);
            }
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onCanboxUpgradeState\t\tEUpgrade--" + eCanboxUpgrade.toString());
        }
        
        @Override
        public void onCarBaseInfo(final CarBaseInfo carBaseInfo) {
            MainService.this.app.carBaseInfo = carBaseInfo;
            final Bundle bundle = new Bundle();
            bundle.putParcelable("carBaseInfo", (Parcelable)carBaseInfo);
            MainService.this.app.sendMessage(1007, bundle);
            Log.d(MainService.this.MYLOG, "<<<onCarBaseInfo\t\tCarBaseInfo--" + carBaseInfo.toString());
        }
        
        @Override
        public void onCarRunningInfo(final CarRunInfo carRunInfo) {
            MainService.this.app.carRunInfo = carRunInfo;
            final Bundle bundle = new Bundle();
            bundle.putParcelable("runningState", (Parcelable)carRunInfo);
            MainService.this.app.speed = carRunInfo.getCurSpeed();
            MainService.this.app.rSpeed = carRunInfo.getRevolutions();
            MainService.this.app.sendMessage(1008, bundle);
            Log.d(MainService.this.MYLOG, "<<<\tonCarRunningInfo:\tCarRunInfo--" + carRunInfo.toString());
        }
        
        @Override
        public void onEnterStandbyMode() {
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onEnterStandbyMode");
            MainService.this.inOrOutAcc(true);
        }
        
        @Override
        public void onHandleIdriver(final Mainboard.EIdriverEnum eIdriverEnum, final Mainboard.EBtnStateEnum eBtnStateEnum) {
            if (MainService.this.loadingFloatLayout == null && eBtnStateEnum == Mainboard.EBtnStateEnum.BTN_DOWN) {
                MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onHandleIdriver\t\tEIdriverEnum--" + eIdriverEnum.toString() + ";\tEBtnStateEnum--" + eBtnStateEnum.toString());
                Log.e("systemtime", " recv " + System.currentTimeMillis());
                if (eIdriverEnum == Mainboard.EIdriverEnum.SPEECH) {
                    Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.ORIGINAL);
                    return;
                }
                if (eIdriverEnum == Mainboard.EIdriverEnum.SPEAKOFF) {
                    Mainboard.getInstance().showCarLayer(Mainboard.ECarLayer.ANDROID);
                    return;
                }
                if (!MainService.this.iIsInReversing && MainService.this.iIsInAndroid) {
                    if (MainService.this.app.iCurrentInLauncher) {
                        final Bundle bundle = new Bundle();
                        bundle.putByte("idriver_enum", eIdriverEnum.getCode());
                        bundle.putByte("idriver_state_enum", eBtnStateEnum.getCode());
                        MainService.this.app.sendMessage(6001, bundle);
                        return;
                    }
                    final int n = -1;
                    int n2 = 0;
                    Label_0182: {
                        if (eIdriverEnum == Mainboard.EIdriverEnum.LEFT) {
                            n2 = 21;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.TURN_LEFT) {
                            final int n3 = n2 = 21;
                            if (MainService.this.getCurrentTopAppPkg().equals(MainService.this.getString(2131165187))) {
                                MainService.this.decMap(true);
                                n2 = n3;
                            }
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.TURN_RIGHT) {
                            final int n4 = n2 = 22;
                            if (MainService.this.getCurrentTopAppPkg().equals(MainService.this.getString(2131165187))) {
                                MainService.this.decMap(false);
                                n2 = n4;
                            }
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.RIGHT) {
                            n2 = 22;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.UP) {
                            n2 = 19;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.DOWN) {
                            n2 = 20;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.PRESS) {
                            if (UtilTools.isFastDoubleClick()) {
                                return;
                            }
                            n2 = 23;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.BACK) {
                            n2 = 4;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.HOME) {
                            n2 = 3;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.STAR_BTN) {
                            n2 = 3;
                        }
                        else if (eIdriverEnum == Mainboard.EIdriverEnum.NAVI) {
                            n2 = 3;
                        }
                        else {
                            if (eIdriverEnum != Mainboard.EIdriverEnum.BT) {
                                n2 = n;
                                if (eIdriverEnum != Mainboard.EIdriverEnum.PICK_UP) {
                                    break Label_0182;
                                }
                            }
                            if (BluetoothService.bluetoothStatus == EPhoneStatus.UNCONNECT) {
                                return;
                            }
                            UtilTools.sendKeyeventToSystem(3);
                            final Bundle bundle2 = new Bundle();
                            bundle2.putByte("idriver_enum", eIdriverEnum.getCode());
                            bundle2.putByte("idriver_state_enum", eBtnStateEnum.getCode());
                            MainService.this.app.sendMessage(6001, bundle2);
                            n2 = n;
                        }
                    }
                    if (n2 != -1) {
                        UtilTools.sendKeyeventToSystem(n2);
                    }
                }
            }
        }
        
        @Override
        public void onHornSoundValue(final int n, final int n2, final int n3, final int n4) {
        }
        
        @Override
        public void onMcuUpgradeForGetDataByIndex(int arg1) {
            MainService.this.logger.debug("driverlog", "mcu:nextIndex:" + arg1);
            final Message obtainMessage = MainService.this.mHandler.obtainMessage(1133);
            obtainMessage.arg1 = arg1;
            obtainMessage.sendToTarget();
            arg1 = (int)(arg1 * 1.0 / MainService.this.mcuTotalDataIndex * 100.0);
            if (arg1 <= 100) {
                final Bundle bundle = new Bundle();
                bundle.putString("upgradePer", String.valueOf(arg1) + "%");
                MainService.this.app.sendMessage(1113, bundle);
            }
        }
        
        @Override
        public void onMcuUpgradeState(final Mainboard.EUpgrade eUpgrade) {
            if (eUpgrade == Mainboard.EUpgrade.GET_HERDER) {
                MainService.this.mHandler.obtainMessage(1130).sendToTarget();
            }
            else {
                if (eUpgrade == Mainboard.EUpgrade.FINISH) {
                    MainService.this.app.sendMessage(1131, null);
                    return;
                }
                if (eUpgrade == Mainboard.EUpgrade.ERROR) {
                    MainService.this.app.sendMessage(1129, null);
                }
            }
        }
        
        @Override
        public void onOriginalCarView(final Mainboard.EControlSource eControlSource, final boolean b) {
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onOriginalCarView:\tsource--" + eControlSource + ";\tiOriginal--" + b + ",source == EControlSource.AUX = " + (eControlSource == Mainboard.EControlSource.AUX));
            if (eControlSource != Mainboard.EControlSource.AUX) {
                if (MainService.this.iAUX) {
                    if (MainService.this.getIsBTState() && MainService.this.iIsInAndroid && !MainService.this.app.isTestOpen) {
                        MainService.this.app.interOriginalView();
                    }
                    MainService.access$30(MainService.this, false);
                }
                MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + " isAUX = " + MainService.this.iAUX);
                return;
            }
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + " isAUX = true");
            if (!MainService.this.iAUX) {
                MainService.access$30(MainService.this, true);
                if (MainService.this.getIsBTState() && !MainService.this.iIsInAndroid) {
                    MainService.this.app.interAndroidView();
                }
            }
            if (BenzModel.isBenzGLK()) {
                if (MainService.this.getIsBTState()) {
                    MainService.this.mHandler.post(MainService.this.removeLoadingRunable);
                }
                return;
            }
            MainService.this.mHandler.post(MainService.this.removeLoadingRunable);
        }
        
        @Override
        public void onShowOrHideCarLayer(final Mainboard.ECarLayer eCarLayer) {
            Log.d(MainService.this.MYLOG, "<<<\tonShowOrHideCarLayer:\tECarLayer--" + eCarLayer.toString() + ",onNoTouchLayout = " + MainService.this.noTouchLayout);
            final int n = SysConst.num[MainService.this.mSpUtilK.getInt(SysConst.mediaVoice, 5)];
            switch ($SWITCH_TABLE$com$backaudio$android$driver$Mainboard$ECarLayer()[eCarLayer.ordinal()]) {
                case 1: {
                    MainService.this.iIsInAndroid = true;
                    MainService.this.iIsInOriginal = false;
                    MainService.this.iIsInRecorder = false;
                    MainService.this.iIsDV = false;
                    MainService.this.iIsBTConnect = false;
                    MainService.this.iIsScrennOff = false;
                    MainService.this.iIsInReversing = false;
                    break;
                }
                case 2: {
                    MainService.this.iIsInAndroid = false;
                    MainService.this.iIsInOriginal = false;
                    MainService.this.iIsInRecorder = true;
                    MainService.this.iIsDV = false;
                    MainService.this.iIsBTConnect = false;
                    MainService.this.iIsScrennOff = false;
                    MainService.this.iIsInReversing = false;
                    break;
                }
                case 3: {
                    MainService.this.iIsInAndroid = false;
                    MainService.this.iIsInOriginal = false;
                    MainService.this.iIsInRecorder = false;
                    if (!MainService.this.iIsDV) {
                        Mainboard.getInstance().setDVSoundValue(SysConst.basicNum, SysConst.num[MainService.this.mSpUtilK.getInt("dvmun", 5)], 0, 0, 0);
                    }
                    MainService.this.iIsDV = true;
                    MainService.this.iIsBTConnect = false;
                    MainService.this.iIsScrennOff = false;
                    MainService.this.iIsInReversing = false;
                    break;
                }
                case 5: {
                    MainService.this.iIsInAndroid = false;
                    MainService.this.iIsInOriginal = true;
                    MainService.this.iIsDV = false;
                    MainService.this.iIsInRecorder = false;
                    MainService.this.iIsBTConnect = false;
                    MainService.this.iIsScrennOff = false;
                    MainService.this.iIsInReversing = false;
                    break;
                }
                case 4:
                case 7:
                case 9: {
                    MainService.this.iIsInAndroid = false;
                    MainService.this.iIsInOriginal = false;
                    MainService.this.iIsInRecorder = false;
                    MainService.this.iIsDV = false;
                    MainService.this.iIsBTConnect = false;
                    MainService.this.iIsScrennOff = false;
                    MainService.this.iIsInReversing = true;
                    if (MainService.this.app.eMediaSet != Mainboard.EReverserMediaSet.NOMAL && MainService.this.app.eMediaSet == Mainboard.EReverserMediaSet.MUTE) {
                        SysConst.basicNum = 0;
                        Mainboard.getInstance().setAllHornSoundValue(SysConst.basicNum, SysConst.num[MainService.this.mSpUtilK.getInt("mun", 5)], 0, n, n);
                        MainService.this.setMusicMute();
                        break;
                    }
                    break;
                }
                case 6: {
                    MainService.this.iIsInAndroid = false;
                    MainService.this.iIsInOriginal = false;
                    MainService.this.iIsInRecorder = false;
                    MainService.this.iIsDV = false;
                    MainService.this.iIsBTConnect = true;
                    MainService.this.iIsInReversing = false;
                    MainService.this.iIsScrennOff = false;
                    break;
                }
                case 8: {
                    MainService.this.iIsInAndroid = false;
                    MainService.this.iIsInOriginal = false;
                    MainService.this.iIsInRecorder = false;
                    MainService.this.iIsDV = false;
                    MainService.this.iIsBTConnect = false;
                    MainService.this.iIsScrennOff = true;
                    MainService.this.iIsInReversing = false;
                    break;
                }
            }
            if (!MainService.this.iIsInReversing && SysConst.basicNum != 23) {
                SysConst.basicNum = 23;
                if (MainService.this.iNaviSound && !MainService.this.app.ismix) {
                    Mainboard.getInstance().setAllHornSoundValue(SysConst.basicNum, SysConst.num[MainService.this.mSpUtilK.getInt(SysConst.naviVoice, 3)], 0, n, n);
                }
                else {
                    Mainboard.getInstance().setAllHornSoundValue(SysConst.mediaBasicNum, SysConst.num[MainService.this.mSpUtilK.getInt(SysConst.naviVoice, 3)], 0, n, n);
                }
                if (MainService.this.getBTA2dpState()) {
                    MainService.this.setConnectedMusicVoice();
                }
            }
            if (MainService.this.iIsInAndroid) {
                MainService.this.removeNoTouchScreens();
                MainService.this.app.abandonDVAudioFocus();
                return;
            }
            MainService.this.mHandler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    MainService.this.createNoTouchScreens();
                }
            }, 200L);
        }
        
        @Override
        public void onTime(final int year, final int month, final int day, final int n, final int hour, final int min, final int second) {
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<settingSystemOfTime:\tyear--" + year + ";\tmonth--" + month + ";\tday--" + day + ";\thour--" + hour + ";\tmin--" + min + ";\tsecond--" + second + ";\ttimeFormat--" + n);
            if (MainService.this.app.min == min) {
                return;
            }
            MainService.this.app.year = year;
            MainService.this.app.month = month;
            MainService.this.app.day = day;
            MainService.this.app.hour = hour;
            MainService.this.app.min = min;
            MainService.this.app.second = second;
            final Calendar instance = Calendar.getInstance();
            instance.set(year, month - 1, day, hour, min, second);
            SystemClock.setCurrentTimeMillis(instance.getTimeInMillis());
            MainService.this.app.sendMessage(1009, null);
        }
        
        @Override
        public void onWakeUp(final Mainboard.ECarLayer eCarLayer) {
            MainService.this.inOrOutAcc(false);
            this.onShowOrHideCarLayer(eCarLayer);
            MainService.this.logger.debug(String.valueOf(MainService.this.MYLOG) + "<<<onWakeUp ECarLayer = " + eCarLayer.toString());
        }
    }
    
    static class Mhandler extends Handler
    {
        private WeakReference<MainService> target;
        
        public Mhandler(final MainService mainService) {
            this.target = new WeakReference<MainService>(mainService);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
