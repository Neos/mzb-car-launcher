package com.touchus.benchilauncher.activity.main;

import com.touchus.benchilauncher.base.*;
import android.widget.*;
import com.backaudio.android.driver.beans.*;
import com.touchus.benchilauncher.*;
import android.util.*;
import com.backaudio.android.driver.*;
import com.touchus.publicutils.utils.*;
import android.view.*;
import android.os.*;
import android.content.*;

public class ButtomBar extends BaseHandlerFragment implements View$OnClickListener
{
    TextView air_state;
    private LauncherApplication app;
    private ImageButton backIbtn;
    private Runnable hideButtomBarRunnabler;
    private ImageButton homeIbtn;
    ImageView mAc;
    ImageView mAirfan;
    ImageView mAuto;
    Launcher mMainActivity;
    ImageView mMax;
    TextView mTempL;
    TextView mTempR;
    private ImageButton screenControlIbtn;
    private LinearLayout tempLayout;
    
    public ButtomBar() {
        this.hideButtomBarRunnabler = new Runnable() {
            @Override
            public void run() {
            }
        };
    }
    
    private void handleAirInfo(final AirInfo airInfo) {
        if (airInfo == null || this.getActivity() == null) {
            return;
        }
        if (this.app.isAirhide && !SysConst.isBT()) {
            this.air_state.setVisibility(4);
            return;
        }
        if (!airInfo.isiAirOpen()) {
            this.air_state.setVisibility(0);
            this.stateChanged();
            return;
        }
        this.air_state.setVisibility(8);
        this.setTempL(airInfo.getLeftTemp());
        this.setAc(airInfo.isiACOpen());
        this.setAirfan(airInfo.getLevel());
        this.setTempR(airInfo.getRightTemp());
        this.setAuto(airInfo.isiFlatWind(), airInfo.isiDownWind(), airInfo.isiMaxFrontWind(), airInfo.isiFrontWind());
        if (airInfo.isiAuto1()) {
            this.mAirfan.setImageResource(2130837787);
        }
        if (airInfo.isiAuto2()) {
            this.mAuto.setImageResource(2130837661);
        }
        if (airInfo.isiMaxFrontWind()) {
            this.mMax.setVisibility(0);
            this.mAuto.setVisibility(4);
            this.mAc.setVisibility(4);
            this.mAirfan.setVisibility(4);
            this.mTempL.setVisibility(4);
            this.mTempR.setVisibility(4);
        }
        else {
            this.mMax.setVisibility(8);
            this.mAuto.setVisibility(0);
            this.mAc.setVisibility(0);
            this.mAirfan.setVisibility(0);
            this.mTempL.setVisibility(0);
            this.mTempR.setVisibility(0);
        }
        this.stateChanged();
    }
    
    private void handleBackAction() {
        if (this.mMainActivity != null && LauncherApplication.getInstance().mHomeAndBackEnable) {
            this.mMainActivity.handleBackAction();
        }
    }
    
    private void handleHomeAction() {
        if (this.mMainActivity != null && LauncherApplication.getInstance().mHomeAndBackEnable) {
            Log.e("", "Menu start time = " + System.currentTimeMillis());
            this.mMainActivity.handHomeAction();
        }
    }
    
    private void setAc(final boolean b) {
        this.stateChanged();
        if (b) {
            this.mAc.setImageResource(2130837642);
            return;
        }
        this.mAc.setImageResource(2130837641);
    }
    
    private void setAirfan(final double n) {
        this.stateChanged();
        if (n == 0.0) {
            this.mAirfan.setImageResource(2130837787);
        }
        else {
            if (n == 0.5) {
                this.mAirfan.setImageResource(2130837772);
                return;
            }
            if (n == 1.0) {
                this.mAirfan.setImageResource(2130837773);
                return;
            }
            if (n == 1.5) {
                this.mAirfan.setImageResource(2130837774);
                return;
            }
            if (n == 2.0) {
                this.mAirfan.setImageResource(2130837775);
                return;
            }
            if (n == 2.5) {
                this.mAirfan.setImageResource(2130837776);
                return;
            }
            if (n == 3.0) {
                this.mAirfan.setImageResource(2130837777);
                return;
            }
            if (n == 3.5) {
                this.mAirfan.setImageResource(2130837778);
                return;
            }
            if (n == 4.0) {
                this.mAirfan.setImageResource(2130837779);
                return;
            }
            if (n == 4.5) {
                this.mAirfan.setImageResource(2130837780);
                return;
            }
            if (n == 5.0) {
                this.mAirfan.setImageResource(2130837781);
                return;
            }
            if (n == 5.5) {
                this.mAirfan.setImageResource(2130837782);
                return;
            }
            if (n == 6.0) {
                this.mAirfan.setImageResource(2130837783);
                return;
            }
            if (n == 6.5) {
                this.mAirfan.setImageResource(2130837784);
                return;
            }
            if (n >= 7.0) {
                this.mAirfan.setImageResource(2130837785);
            }
        }
    }
    
    private void setAuto(final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        this.stateChanged();
        if (b && b2 && b4) {
            this.mAuto.setImageResource(2130837656);
        }
        else {
            if (b && b2 && !b4) {
                this.mAuto.setImageResource(2130837658);
                return;
            }
            if (b && !b2 && !b4) {
                this.mAuto.setImageResource(2130837659);
                return;
            }
            if (!b && b2 && !b4) {
                this.mAuto.setImageResource(2130837653);
                return;
            }
            if (!b && b2 && b4) {
                this.mAuto.setImageResource(2130837655);
                return;
            }
            if (b && !b2 && b4) {
                this.mAuto.setImageResource(2130837657);
                return;
            }
            if (!b && !b2 && b4) {
                this.mAuto.setImageResource(2130837654);
            }
        }
    }
    
    private void setTempL(final double n) {
        this.stateChanged();
        if (n < 16.0) {
            this.mTempL.setText((CharSequence)"LO");
            return;
        }
        if (n > 28.0) {
            this.mTempL.setText((CharSequence)"HI");
            return;
        }
        this.mTempL.setText((CharSequence)(String.valueOf(n) + "\u2103"));
    }
    
    private void setTempR(final double n) {
        this.stateChanged();
        if (n < 16.0) {
            this.mTempR.setText((CharSequence)"LO");
            return;
        }
        if (n > 28.0) {
            this.mTempR.setText((CharSequence)"HI");
            return;
        }
        this.mTempR.setText((CharSequence)(String.valueOf(n) + "\u2103"));
    }
    
    @Override
    public void handlerMsg(final Message message) {
        switch (message.what) {
            case 1012: {
                this.handleAirInfo((AirInfo)message.getData().getParcelable("airInfo"));
            }
            case 6001: {
                final byte byte1 = message.getData().getByte("idriver_enum");
                if (byte1 == Mainboard.EIdriverEnum.BACK.getCode()) {
                    this.handleBackAction();
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.handleHomeAction();
                    return;
                }
                break;
            }
        }
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131427590: {
                this.handleHomeAction();
            }
            case 2131427591: {
                this.handleBackAction();
            }
            case 2131427592: {
                if (!UtilTools.isFastDoubleClick()) {
                    this.app.closeOrWakeupScreen(true);
                    return;
                }
                break;
            }
        }
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mMainActivity = (Launcher)this.getActivity();
        final View inflate = View.inflate((Context)this.mMainActivity, 2130903088, (ViewGroup)null);
        this.homeIbtn = (ImageButton)inflate.findViewById(2131427590);
        this.backIbtn = (ImageButton)inflate.findViewById(2131427591);
        this.screenControlIbtn = (ImageButton)inflate.findViewById(2131427592);
        this.mAuto = (ImageView)inflate.findViewById(2131427596);
        this.mAc = (ImageView)inflate.findViewById(2131427597);
        this.mAirfan = (ImageView)inflate.findViewById(2131427598);
        this.mMax = (ImageView)inflate.findViewById(2131427599);
        this.air_state = (TextView)inflate.findViewById(2131427593);
        this.mTempL = (TextView)inflate.findViewById(2131427595);
        this.mTempR = (TextView)inflate.findViewById(2131427600);
        this.tempLayout = (LinearLayout)inflate.findViewById(2131427594);
        this.app = (LauncherApplication)this.getActivity().getApplication();
        this.homeIbtn.setOnClickListener((View$OnClickListener)this);
        this.backIbtn.setOnClickListener((View$OnClickListener)this);
        this.screenControlIbtn.setOnClickListener((View$OnClickListener)this);
        return inflate;
    }
    
    @Override
    public void onStart() {
        this.handleAirInfo(this.app.airInfo);
        super.onStart();
    }
    
    public void settingTempLayoutHideOrShow(final boolean b) {
        if (this.getActivity() == null) {
            return;
        }
        if (!b) {
            int visibility;
            if (b) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            this.tempLayout.setVisibility(visibility);
            return;
        }
        this.mMyHandler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                int visibility;
                if (b) {
                    visibility = 0;
                }
                else {
                    visibility = 8;
                }
                ButtomBar.this.tempLayout.setVisibility(visibility);
            }
        }, 500L);
    }
    
    public void stateChanged() {
        if (this.mMainActivity == null) {
            return;
        }
        if (this.mMainActivity.playVideoLayout.getVisibility() != 0) {
            this.mMainActivity.showButtom();
        }
        this.mMyHandler.removeCallbacks(this.hideButtomBarRunnabler);
        this.mMyHandler.postDelayed(this.hideButtomBarRunnabler, 5000L);
    }
}
