package com.touchus.benchilauncher.activity.main.left;

import android.support.v4.app.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.backaudio.android.driver.beans.*;
import com.touchus.publicutils.sysconst.*;
import android.util.*;
import android.view.*;
import java.lang.ref.*;
import android.os.*;

public class RunningFrag extends Fragment
{
    private LinearLayout curSpeed_ll;
    private LauncherApplication mApp;
    private RunningHandler mHandler;
    private TextView mRemain;
    private TextView mSpeed;
    private LinearLayout mSpeedLayout;
    private TextView mSum;
    private TextView mTemperature;
    private LinearLayout mileage_ll;
    private TextView mzhuansu;
    private TextView running_speed_none;
    
    public RunningFrag() {
        this.mHandler = new RunningHandler(this);
    }
    
    private void findID(final View view) {
        this.mSpeed = (TextView)view.findViewById(2131427630);
        this.running_speed_none = (TextView)view.findViewById(2131427623);
        this.mRemain = (TextView)view.findViewById(2131427625);
        this.mSum = (TextView)view.findViewById(2131427626);
        this.mTemperature = (TextView)view.findViewById(2131427628);
        this.mzhuansu = (TextView)view.findViewById(2131427627);
        this.mSpeedLayout = (LinearLayout)view.findViewById(2131427629);
        this.curSpeed_ll = (LinearLayout)view.findViewById(2131427622);
        this.mileage_ll = (LinearLayout)view.findViewById(2131427624);
        this.mSpeedLayout.setVisibility(8);
    }
    
    private void init() {
        (this.mApp = (LauncherApplication)this.getActivity().getApplication()).registerHandler(this.mHandler);
    }
    
    private void settingRunningInfo(final CarRunInfo carRunInfo) {
        Log.e("", "BenzModel.benzTpye = " + BenzModel.benzTpye);
        if (BenzModel.benzCan == BenzModel.EBenzCAN.XBS && BenzModel.benzTpye != BenzModel.EBenzTpye.GLA) {
            this.mileage_ll.setVisibility(0);
            this.curSpeed_ll.setVisibility(8);
        }
        else {
            this.mileage_ll.setVisibility(8);
            this.curSpeed_ll.setVisibility(0);
        }
        if (carRunInfo == null) {
            this.running_speed_none.setText((CharSequence)"----");
            this.mRemain.setText((CharSequence)"----");
            this.mSum.setText((CharSequence)"----");
            this.mTemperature.setText((CharSequence)"----");
            return;
        }
        if (BenzModel.benzCan == BenzModel.EBenzCAN.XBS && BenzModel.benzTpye != BenzModel.EBenzTpye.GLA) {
            if (carRunInfo.getMileage() >= 2047 || carRunInfo.getMileage() <= 0) {
                this.running_speed_none.setText((CharSequence)"----");
                this.mRemain.setText((CharSequence)"----");
                this.mSum.setText((CharSequence)"----");
                this.mzhuansu.setText((CharSequence)"----");
                this.mTemperature.setText((CharSequence)"----");
                return;
            }
            this.mSpeed.setText((CharSequence)new StringBuilder(String.valueOf(carRunInfo.getCurSpeed())).toString());
            this.mRemain.setText((CharSequence)(String.valueOf(carRunInfo.getMileage()) + " km"));
            this.mzhuansu.setText((CharSequence)(String.valueOf(carRunInfo.getRevolutions()) + " rpm"));
            this.mSum.setText((CharSequence)(String.valueOf(carRunInfo.getTotalMileage() / 10L) + " km"));
            this.mTemperature.setText((CharSequence)(String.valueOf(carRunInfo.getOutsideTemp()) + " \u2103"));
        }
        else {
            if (carRunInfo.getCurSpeed() >= 0) {
                this.mSpeedLayout.setVisibility(0);
                this.running_speed_none.setVisibility(4);
                this.mSpeed.setText((CharSequence)new StringBuilder(String.valueOf(carRunInfo.getCurSpeed())).toString());
            }
            else {
                this.mSpeedLayout.setVisibility(4);
                this.running_speed_none.setVisibility(0);
                this.running_speed_none.setText((CharSequence)"----");
            }
            if (carRunInfo.getOutsideTemp() > -40.0 && carRunInfo.getOutsideTemp() < 87.5) {
                this.mTemperature.setText((CharSequence)(String.valueOf(carRunInfo.getOutsideTemp()) + " \u2103"));
            }
            else {
                this.mTemperature.setText((CharSequence)"----");
            }
            if (carRunInfo.getTotalMileage() > 0L) {
                this.mSum.setText((CharSequence)(String.valueOf(carRunInfo.getTotalMileage() / 10L) + " km"));
                return;
            }
            this.mSum.setText((CharSequence)"----");
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2130903092, viewGroup, false);
        this.findID(inflate);
        this.init();
        return inflate;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mApp.unregisterHandler(this.mHandler);
    }
    
    @Override
    public void onStart() {
        this.settingRunningInfo(this.mApp.carRunInfo);
        super.onStart();
    }
    
    public static class RunningHandler extends Handler
    {
        private WeakReference<RunningFrag> target;
        
        public RunningHandler(final RunningFrag runningFrag) {
            this.target = new WeakReference<RunningFrag>(runningFrag);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() != null && message.what == 1008) {
                this.target.get().settingRunningInfo((CarRunInfo)message.getData().getParcelable("runningState"));
            }
        }
    }
}
