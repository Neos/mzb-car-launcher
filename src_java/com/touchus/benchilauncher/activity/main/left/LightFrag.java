package com.touchus.benchilauncher.activity.main.left;

import android.support.v4.app.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.backaudio.android.driver.beans.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class LightFrag extends Fragment
{
    private LauncherApplication app;
    private LightHandler mHandler;
    private ImageView mImgCarLight;
    private TextView mLightDesc;
    
    public LightFrag() {
        this.mHandler = new LightHandler(this);
    }
    
    private void settingLightInfo(final CarBaseInfo carBaseInfo) {
        if (this.app.isSUV) {
            this.mImgCarLight.setImageResource(2130837924);
        }
        else {
            this.mImgCarLight.setImageResource(2130837694);
        }
        if (carBaseInfo == null) {
            return;
        }
        if (carBaseInfo.isiDistantLightOpen()) {
            if (this.app.isSUV) {
                this.mImgCarLight.setImageResource(2130837923);
            }
            else {
                this.mImgCarLight.setImageResource(2130837693);
            }
            this.mLightDesc.setText((CharSequence)this.getString(2131165288));
            return;
        }
        if (carBaseInfo.isiNearLightOpen()) {
            if (this.app.isSUV) {
                this.mImgCarLight.setImageResource(2130837925);
            }
            else {
                this.mImgCarLight.setImageResource(2130837695);
            }
            this.mLightDesc.setText((CharSequence)this.getString(2131165287));
            return;
        }
        this.mLightDesc.setText((CharSequence)this.getString(2131165286));
    }
    
    public void handMsg(final Message message) {
        if (this.getActivity() != null && message.what == 1007) {
            this.settingLightInfo((CarBaseInfo)message.getData().getParcelable("carBaseInfo"));
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2130903090, viewGroup);
        this.mImgCarLight = (ImageView)inflate.findViewById(2131427613);
        this.mLightDesc = (TextView)inflate.findViewById(2131427614);
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }
    
    @Override
    public void onStart() {
        (this.app = (LauncherApplication)this.getActivity().getApplication()).registerHandler(this.mHandler);
        this.settingLightInfo(this.app.carBaseInfo);
        super.onStart();
    }
    
    @Override
    public void onStop() {
        this.app.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    static class LightHandler extends Handler
    {
        private WeakReference<LightFrag> target;
        
        public LightHandler(final LightFrag lightFrag) {
            this.target = new WeakReference<LightFrag>(lightFrag);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() != null) {
                this.target.get().handMsg(message);
            }
        }
    }
}
