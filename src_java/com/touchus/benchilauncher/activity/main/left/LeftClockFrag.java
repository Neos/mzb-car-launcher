package com.touchus.benchilauncher.activity.main.left;

import android.support.v4.app.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.views.*;
import android.util.*;
import com.touchus.publicutils.utils.*;
import com.touchus.benchilauncher.utils.*;
import android.content.*;
import android.view.*;
import android.os.*;

public class LeftClockFrag extends Fragment implements View$OnClickListener
{
    private LauncherApplication app;
    private int clickCount;
    private ClockView clockView;
    Handler mHandler;
    Thread resetClickCountThread;
    
    public LeftClockFrag() {
        this.clickCount = 0;
        this.resetClickCountThread = new Thread(new Runnable() {
            @Override
            public void run() {
                LeftClockFrag.access$0(LeftClockFrag.this, 0);
            }
        });
        this.mHandler = new Handler();
    }
    
    static /* synthetic */ void access$0(final LeftClockFrag leftClockFrag, final int clickCount) {
        leftClockFrag.clickCount = clickCount;
    }
    
    public void onClick(final View view) {
        Log.e("test", "click");
        if (!UtilTools.isFastDoubleClick()) {
            this.clickCount = 0;
        }
        else {
            ++this.clickCount;
            if (this.clickCount >= 4) {
                this.clickCount = 0;
                if (!this.app.isTestOpen) {
                    ToastTool.showShortToast((Context)this.getActivity(), "test open");
                    this.app.isTestOpen = true;
                    LauncherApplication.mSpUtils.putBoolean("test", this.app.isTestOpen);
                }
                else {
                    ToastTool.showShortToast((Context)this.getActivity(), "test close");
                    this.app.isTestOpen = false;
                    LauncherApplication.mSpUtils.putBoolean("test", this.app.isTestOpen);
                }
            }
        }
        if (this.clickCount != 0) {
            this.mHandler.removeCallbacks((Runnable)this.resetClickCountThread);
            this.mHandler.postDelayed((Runnable)this.resetClickCountThread, 1000L);
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2130903071, viewGroup, false);
        (this.clockView = (ClockView)inflate.findViewById(2131427509)).setOnClickListener((View$OnClickListener)this);
        this.app = (LauncherApplication)this.getActivity().getApplication();
        return inflate;
    }
    
    @Override
    public void onStart() {
        this.clockView.setTime(true, this.app.hour, this.app.min);
        super.onStart();
    }
    
    public void updateCurrentTime() {
        if (this.getActivity() != null) {
            this.clockView.setTime(true, this.app.hour, this.app.min);
        }
    }
}
