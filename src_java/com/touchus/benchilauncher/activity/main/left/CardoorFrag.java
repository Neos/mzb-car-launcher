package com.touchus.benchilauncher.activity.main.left;

import android.support.v4.app.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.backaudio.android.driver.beans.*;
import com.touchus.benchilauncher.inface.*;
import java.util.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class CardoorFrag extends Fragment
{
    private LauncherApplication app;
    public ImageView mBehind;
    public TextView mDoorDes;
    public TextView mDoorsDes;
    public ImageView mFront;
    public CardoorHandler mHandler;
    public ImageView mImgState;
    public ImageView mLeftBehind;
    public ImageView mLeftFront;
    public ImageView mNormal;
    public ImageView mRightBehind;
    public ImageView mRightFront;
    public RelativeLayout mRlOverlook;
    public RelativeLayout mRlState;
    
    public CardoorFrag() {
        this.mHandler = new CardoorHandler(this);
    }
    
    private void handleMesage(final Message message) {
        if (this.getActivity() != null && message.what == 1007) {
            this.settingDoorView((CarBaseInfo)message.getData().getParcelable("carBaseInfo"));
        }
    }
    
    private void hideAll() {
        this.mNormal.setVisibility(8);
        this.mBehind.setVisibility(8);
        this.mFront.setVisibility(8);
        this.mLeftBehind.setVisibility(8);
        this.mLeftFront.setVisibility(8);
        this.mRightBehind.setVisibility(8);
        this.mRightFront.setVisibility(8);
        this.mImgState.setVisibility(8);
        this.mDoorDes.setVisibility(8);
        this.mDoorsDes.setVisibility(8);
    }
    
    private void initDoorView() {
        if (this.app.carBaseInfo == null) {
            this.hideAll();
            return;
        }
        this.settingDoorView(this.app.carBaseInfo);
    }
    
    private void openDoor(final int n) {
        if (this.getActivity() == null) {
            return;
        }
        this.mImgState.setVisibility(0);
        this.mDoorDes.setVisibility(0);
        if (this.app.isSUV) {
            switch (n) {
                default: {}
                case 0: {
                    this.mImgState.setImageResource(2130837920);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165215));
                }
                case 1: {
                    this.mImgState.setImageResource(2130837916);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165220));
                }
                case 2: {
                    this.mImgState.setImageResource(2130837917);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165218));
                }
                case 3: {
                    this.mImgState.setImageResource(2130837918);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165216));
                }
                case 4: {
                    this.mImgState.setImageResource(2130837921);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165219));
                }
                case 5: {
                    this.mImgState.setImageResource(2130837922);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165217));
                }
            }
        }
        else {
            switch (n) {
                default: {}
                case 0: {
                    this.mImgState.setImageResource(2130837688);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165215));
                }
                case 1: {
                    this.mImgState.setImageResource(2130837681);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165220));
                }
                case 2: {
                    this.mImgState.setImageResource(2130837683);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165218));
                }
                case 3: {
                    this.mImgState.setImageResource(2130837685);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165216));
                }
                case 4: {
                    this.mImgState.setImageResource(2130837690);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165219));
                }
                case 5: {
                    this.mImgState.setImageResource(2130837692);
                    this.mDoorDes.setText((CharSequence)this.getString(2131165217));
                }
            }
        }
    }
    
    private void openDoors(final int n) {
        this.mNormal.setVisibility(0);
        if (this.app.isSUV) {
            this.mNormal.setImageResource(2130837919);
        }
        else {
            this.mNormal.setImageResource(2130837686);
        }
        switch (n) {
            default: {}
            case 1: {
                if (this.app.isSUV) {
                    this.mBehind.setImageResource(2130837915);
                }
                else {
                    this.mBehind.setImageResource(2130837680);
                }
                this.mBehind.setVisibility(0);
            }
            case 0: {
                this.mFront.setVisibility(0);
            }
            case 2: {
                this.mLeftBehind.setVisibility(0);
            }
            case 3: {
                this.mLeftFront.setVisibility(0);
            }
            case 4: {
                this.mRightBehind.setVisibility(0);
            }
            case 5: {
                this.mRightFront.setVisibility(0);
            }
        }
    }
    
    private void resetLeftShowInfo() {
        if (this.getActivity() == null) {
            return;
        }
        ((IDoorStateParent)this.getActivity()).resetLeftLayoutToRightState();
    }
    
    private void settingDoorView(final CarBaseInfo carBaseInfo) {
        final ArrayList<Integer> list = new ArrayList<Integer>();
        if (carBaseInfo.isiLeftBackOpen()) {
            list.add(2);
        }
        if (carBaseInfo.isiLeftFrontOpen()) {
            list.add(3);
        }
        if (carBaseInfo.isiRightBackOpen()) {
            list.add(4);
        }
        if (carBaseInfo.isiRightFrontOpen()) {
            list.add(5);
        }
        if (carBaseInfo.isiFront()) {
            list.add(0);
        }
        if (carBaseInfo.isiBack()) {
            list.add(1);
        }
        this.hideAll();
        if (list.size() == 0) {
            this.resetLeftShowInfo();
        }
        else {
            if (list.size() == 1) {
                this.openDoor(list.get(0));
                this.showMe();
                return;
            }
            if (list.size() > 1) {
                this.mDoorsDes.setVisibility(0);
                for (int i = 0; i < list.size(); ++i) {
                    this.openDoors(list.get(i));
                }
                this.showMe();
            }
        }
    }
    
    private void showMe() {
        if (this.getActivity() == null) {
            return;
        }
        ((IDoorStateParent)this.getActivity()).showCardoor();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2130903089, viewGroup, false);
        this.mRlState = (RelativeLayout)inflate.findViewById(2131427610);
        this.mImgState = (ImageView)inflate.findViewById(2131427611);
        this.mRlOverlook = (RelativeLayout)inflate.findViewById(2131427601);
        this.mRightBehind = (ImageView)inflate.findViewById(2131427609);
        this.mRightFront = (ImageView)inflate.findViewById(2131427608);
        this.mLeftBehind = (ImageView)inflate.findViewById(2131427607);
        this.mLeftFront = (ImageView)inflate.findViewById(2131427606);
        this.mBehind = (ImageView)inflate.findViewById(2131427605);
        this.mFront = (ImageView)inflate.findViewById(2131427604);
        this.mNormal = (ImageView)inflate.findViewById(2131427602);
        this.mDoorDes = (TextView)inflate.findViewById(2131427612);
        this.mDoorsDes = (TextView)inflate.findViewById(2131427603);
        return inflate;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.app = (LauncherApplication)this.getActivity().getApplication();
        this.initDoorView();
        this.app.registerHandler(this.mHandler);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        (this.app = (LauncherApplication)this.getActivity().getApplication()).unregisterHandler(this.mHandler);
    }
    
    static class CardoorHandler extends Handler
    {
        private WeakReference<CardoorFrag> target;
        
        public CardoorHandler(final CardoorFrag cardoorFrag) {
            this.target = new WeakReference<CardoorFrag>(cardoorFrag);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handleMesage(message);
        }
    }
}
