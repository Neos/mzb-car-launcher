package com.touchus.benchilauncher.activity.main.right.Menu;

import android.widget.*;
import com.touchus.benchilauncher.views.*;
import android.content.*;
import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.*;
import android.graphics.*;
import java.util.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.os.*;
import android.util.*;

public class MenuFragment extends BaseHandlerFragment
{
    public static int mCurIndex;
    public static int mCurPage;
    ImageView mImgPoint0;
    ImageView mImgPoint1;
    ImageView mImgPoint2;
    Launcher mLaucher;
    LinearLayout mMenuContainer;
    MenuSlide mMenuSlide;
    LinearLayout$LayoutParams mParams;
    SlideRunnable mSlideRunnable;
    SparseArray<View> mSpaArrBigMenu;
    SparseArray<View> mSpaArrPoint;
    SparseArray<View> mSpaArrSmallMenu;
    
    public MenuFragment() {
        this.mSpaArrPoint = (SparseArray<View>)new SparseArray();
        this.mSpaArrBigMenu = (SparseArray<View>)new SparseArray();
        this.mSpaArrSmallMenu = (SparseArray<View>)new SparseArray();
        this.mSlideRunnable = new SlideRunnable();
    }
    
    private void init() {
        for (int i = 0; i < SysConst.MENU_LIST.length; ++i) {
            final MyMenuView myMenuView = new MyMenuView((Context)this.getActivity());
            myMenuView.setImageBitmap(SysConst.MENU_LIST[i], SysConst.MENU_SMALL_ICON[i], false);
            this.addView(myMenuView, 0);
            final MyMenuView myMenuView2 = new MyMenuView((Context)this.getActivity());
            myMenuView2.setImageBitmap(SysConst.MENU_LIST[i], SysConst.MENU_BIG_ICON[i], true);
            this.addView(myMenuView2, 1);
        }
        this.mSpaArrPoint.append(0, (Object)this.mImgPoint0);
        this.mSpaArrPoint.append(1, (Object)this.mImgPoint1);
        for (int length = SysConst.MENU_LIST.length, j = 0; j < length; ++j) {
            this.mSpaArrSmallMenu.append(j, (Object)this.mMenuContainer.getChildAt(j * 2));
            ((View)this.mSpaArrSmallMenu.get(j)).setVisibility(0);
            this.mMenuContainer.getChildAt(j * 2).setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    MenuFragment.this.select(j);
                    MenuFragment.this.mLaucher.goTo(j);
                }
            });
            this.mSpaArrBigMenu.append(j, (Object)this.mMenuContainer.getChildAt(j * 2 + 1));
            ((View)this.mSpaArrBigMenu.get(j)).setVisibility(8);
            ((View)this.mSpaArrBigMenu.get(j)).setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    MenuFragment.this.mLaucher.goTo(j);
                }
            });
        }
        this.selectMenu(MenuFragment.mCurIndex);
        this.mMenuSlide.setOnSlideListener((AbstraSlide.OnSlipeListener)new AbstraSlide.OnSlipeListener() {
            @Override
            public void lesten(final int n) {
                switch (n) {
                    default: {}
                    case 3: {
                        MenuFragment.this.selectDelay(5);
                    }
                    case 4: {
                        MenuFragment.this.selectDelay(4);
                    }
                }
            }
        });
    }
    
    public void addView(final MyMenuView myMenuView, final int n) {
        if (n == 0) {
            (this.mParams = new LinearLayout$LayoutParams(138, 148)).setMargins(0, 163, 0, 0);
        }
        else {
            (this.mParams = new LinearLayout$LayoutParams(230, 250)).setMargins(0, 119, 0, 0);
        }
        this.mMenuContainer.addView((View)myMenuView, (ViewGroup$LayoutParams)this.mParams);
    }
    
    public Bitmap createBmp(final String s, int n) {
        final Bitmap decodeResource = BitmapFactory.decodeResource(LauncherApplication.mContext.getResources(), n);
        n = decodeResource.getWidth();
        final int height = decodeResource.getHeight();
        final Bitmap bitmap = Bitmap.createBitmap(n, height, Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        final Paint paint = new Paint();
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawBitmap(decodeResource, new Rect(0, 0, n, height), new Rect(0, 0, n, height), paint);
        final Paint paint2 = new Paint(257);
        paint2.setTextSkewX(-0.18f);
        paint2.setTextSize(30.0f);
        paint2.setTypeface(Typeface.DEFAULT);
        paint2.setColor(-1);
        final Locale locale = this.getActivity().getResources().getConfiguration().locale;
        if (locale.getCountry().equals("UK") || locale.getCountry().equals("US")) {
            final int[] array2;
            final int[] array = array2 = new int[7];
            array2[0] = 100;
            array2[1] = 90;
            array2[2] = 80;
            array2[3] = 60;
            array2[5] = (array2[4] = 60);
            array2[6] = 40;
            if (s.length() - 3 >= array.length) {
                n = array.length - 1;
            }
            else {
                n = s.length() - 3;
            }
            n = array[n];
        }
        else {
            n = s.length();
            n = (new int[] { 90, 75, 60, 70 })[n - 2];
        }
        canvas.drawText(s, (float)n, 195.0f, paint2);
        return bitmap;
    }
    
    public Bitmap createBmp1(final String s, int n) {
        final Bitmap decodeResource = BitmapFactory.decodeResource(LauncherApplication.mContext.getResources(), n);
        n = decodeResource.getWidth();
        final int height = decodeResource.getHeight();
        final Bitmap bitmap = Bitmap.createBitmap(n, height, Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        final Paint paint = new Paint();
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawBitmap(decodeResource, new Rect(0, 0, n, height), new Rect(0, 0, n, height), paint);
        final Paint paint2 = new Paint(257);
        paint2.setTextSkewX(-0.18f);
        paint2.setTextSize(17.76f);
        paint2.setTypeface(Typeface.DEFAULT);
        paint2.setColor(-1);
        final Locale locale = this.getActivity().getResources().getConfiguration().locale;
        if (locale.getCountry().equals("UK") || locale.getCountry().equals("US")) {
            final int[] array2;
            final int[] array = array2 = new int[7];
            array2[0] = 95;
            array2[1] = 85;
            array2[2] = 75;
            array2[3] = 60;
            array2[5] = (array2[4] = 60);
            array2[6] = 40;
            if (s.length() - 3 >= array.length) {
                n = array.length - 1;
            }
            else {
                n = s.length() - 3;
            }
            n = array[n];
        }
        else {
            n = s.length();
            n = (new int[] { 90, 75, 60, 70 })[n - 2];
        }
        canvas.drawText(s, n * 0.592f, 115.44f, paint2);
        return bitmap;
    }
    
    @Override
    public void handlerMsg(final Message message) {
        switch (message.what) {
            case 6001: {
                final byte byte1 = message.getData().getByte("idriver_enum");
                if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                    this.next();
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    this.pre();
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.mLaucher.goTo(MenuFragment.mCurIndex);
                    return;
                }
                break;
            }
        }
    }
    
    public void next() {
        if (MenuFragment.mCurIndex == 9) {
            return;
        }
        this.select(MenuFragment.mCurIndex + 1);
        this.mMyHandler.postDelayed((Runnable)this.mSlideRunnable, 400L);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2130903094, viewGroup, false);
        this.mMenuSlide = (MenuSlide)inflate.findViewById(2131427641);
        this.mMenuContainer = (LinearLayout)inflate.findViewById(2131427642);
        this.mImgPoint0 = (ImageView)inflate.findViewById(2131427643);
        this.mImgPoint1 = (ImageView)inflate.findViewById(2131427644);
        this.mImgPoint2 = (ImageView)inflate.findViewById(2131427645);
        this.mLaucher = (Launcher)this.getActivity();
        this.init();
        Log.e("", "onCreateView");
        this.mLaucher.mMenuShow = true;
        return inflate;
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.mLaucher.mMenuShow = false;
    }
    
    @Override
    public void onStart() {
        super.onStart();
    }
    
    public void pre() {
        if (MenuFragment.mCurIndex == 0) {
            return;
        }
        this.select(MenuFragment.mCurIndex - 1);
        this.mMyHandler.postDelayed((Runnable)this.mSlideRunnable, 400L);
    }
    
    public void press() {
        this.mLaucher.goTo(MenuFragment.mCurIndex);
    }
    
    public void select(final int mCurIndex) {
        // monitorenter(this)
        Label_0030: {
            if (mCurIndex >= 5) {
                break Label_0030;
            }
            while (true) {
                while (true) {
                    try {
                        MenuFragment.mCurPage = 0;
                        this.showBig(mCurIndex);
                        MenuFragment.mCurIndex = mCurIndex;
                        this.selectPoint(MenuFragment.mCurPage);
                        return;
                        // iftrue(Label_0048:, mCurIndex >= 10)
                        MenuFragment.mCurPage = 1;
                        continue;
                    }
                    finally {
                    }
                    // monitorexit(this)
                    Label_0048: {
                        if (mCurIndex < 15) {
                            MenuFragment.mCurPage = 2;
                            continue;
                        }
                    }
                    continue;
                }
            }
        }
    }
    
    public void selectDelay(final int n) {
        this.mMyHandler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                MenuFragment.this.select(n);
            }
        }, 400L);
    }
    
    public void selectMenu(final int n) {
        if (n < 5) {
            MenuFragment.mCurPage = 0;
        }
        else if (n < 10) {
            MenuFragment.mCurPage = 1;
        }
        else if (n < 15) {
            MenuFragment.mCurPage = 2;
        }
        this.mMenuSlide.slide2Page(MenuFragment.mCurPage, 0);
        this.select(n);
    }
    
    public void selectPoint(final int n) {
        for (int i = 0; i < 2; ++i) {
            ((View)this.mSpaArrPoint.get(i)).setSelected(false);
        }
        ((View)this.mSpaArrPoint.get(n)).setSelected(true);
    }
    
    public void showBig(final int n) {
        for (int i = 0; i < this.mSpaArrBigMenu.size(); ++i) {
            if (i == n) {
                ((View)this.mSpaArrBigMenu.get(i)).setVisibility(0);
                ((View)this.mSpaArrSmallMenu.get(i)).setVisibility(8);
            }
            else {
                ((View)this.mSpaArrBigMenu.get(i)).setVisibility(8);
                ((View)this.mSpaArrSmallMenu.get(i)).setVisibility(0);
            }
        }
    }
    
    public void showSmall(final int n) {
        ((View)this.mSpaArrBigMenu.get(n)).setVisibility(8);
        ((View)this.mSpaArrSmallMenu.get(n)).setVisibility(0);
    }
    
    public void slideMenu(final int n) {
        this.mMenuSlide.slide2Page(n, 500);
    }
    
    class SlideRunnable implements Runnable
    {
        @Override
        public void run() {
            MenuFragment.this.slideMenu(MenuFragment.mCurPage);
        }
    }
}
