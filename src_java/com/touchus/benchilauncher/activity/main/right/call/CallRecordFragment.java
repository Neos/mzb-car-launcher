package com.touchus.benchilauncher.activity.main.right.call;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.adapter.*;
import com.touchus.benchilauncher.bean.*;
import android.content.*;
import com.touchus.benchilauncher.*;
import com.backaudio.android.driver.*;
import java.util.*;
import android.widget.*;
import android.support.v4.app.*;
import android.app.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class CallRecordFragment extends BaseFragment
{
    private String TAG;
    private LauncherApplication app;
    private CallRecordAdapter callRecordAdapter;
    private View callRecordView;
    private List<Person> callRecords;
    private Context context;
    private int currentPosition;
    private boolean isLogToggle;
    private boolean isLongPress;
    private Launcher launcher;
    private ListView listView_record;
    private RecordHandler mHandler;
    
    public CallRecordFragment() {
        this.TAG = this.getClass().getSimpleName();
        this.isLogToggle = true;
        this.callRecords = new ArrayList<Person>();
        this.currentPosition = 0;
        this.isLongPress = false;
        this.mHandler = new RecordHandler(this);
    }
    
    static /* synthetic */ void access$2(final CallRecordFragment callRecordFragment, final int currentPosition) {
        callRecordFragment.currentPosition = currentPosition;
    }
    
    private void handlerMsg(final Message message) {
        if (6001 == message.what) {
            this.handlerMsgIdr(message);
        }
    }
    
    private void handlerMsgIdr(final Message message) {
        final Bundle data = message.getData();
        if (data.getByte("idriver_state_enum") == Mainboard.EBtnStateEnum.BTN_LONG_PRESS.getCode()) {
            this.isLongPress = true;
            return;
        }
        if (data.getByte("idriver_state_enum") == Mainboard.EBtnStateEnum.BTN_DOWN.getCode()) {
            this.isLongPress = false;
            this.pressUpDown(data);
            return;
        }
        if (data.getByte("idriver_state_enum") == Mainboard.EBtnStateEnum.BTN_UP.getCode()) {
            this.isLongPress = false;
            return;
        }
        this.isLongPress = false;
    }
    
    private void initData() {
        this.callRecordAdapter = new CallRecordAdapter(this.context);
        this.callRecords.addAll(this.app.btservice.getHistoryList());
        this.callRecordAdapter.setData(this.callRecords);
        this.listView_record.setAdapter((ListAdapter)this.callRecordAdapter);
        this.listView_record.setEmptyView(this.callRecordView.findViewById(2131427468));
    }
    
    private void initEvent() {
        this.listView_record.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int selectItem, final long n) {
                CallRecordFragment.this.callRecordAdapter.setSelectItem(selectItem);
                CallRecordFragment.access$2(CallRecordFragment.this, selectItem);
                CallRecordFragment.this.callRecordAdapter.notifyDataSetChanged();
                if (LauncherApplication.isBlueConnectState) {
                    CallRecordFragment.this.app.isComeFromRecord = true;
                    CallRecordFragment.this.app.isPhoneNumFromRecord = true;
                    CallRecordFragment.this.app.recorPhoneNumber = CallRecordFragment.this.callRecords.get(selectItem).getPhone();
                    CallRecordFragment.this.launcher.changeRightTo(new CallPhoneFragment());
                }
            }
        });
    }
    
    private void initView() {
        this.listView_record = (ListView)this.callRecordView.findViewById(2131427467);
    }
    
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        this.context = (Context)this.getActivity();
        this.launcher = (Launcher)this.getActivity();
        this.app = (LauncherApplication)this.getActivity().getApplication();
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.callRecordView = layoutInflater.inflate(2130903069, (ViewGroup)null);
        this.initView();
        this.initData();
        this.initEvent();
        return this.callRecordView;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public void onStart() {
        this.app.registerHandler(this.mHandler);
        super.onStart();
    }
    
    @Override
    public void onStop() {
        this.app.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void pressUpDown(final Bundle bundle) {
        if (this.callRecords.size() != 0) {
            if (bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.UP.getCode() || bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_LEFT.getCode()) {
                --this.currentPosition;
                if (this.currentPosition == -1) {
                    this.currentPosition = 0;
                    this.isLongPress = false;
                }
                if (this.currentPosition < this.listView_record.getFirstVisiblePosition() + 1) {
                    this.listView_record.smoothScrollToPositionFromTop(this.currentPosition - 5, 0);
                }
            }
            else if (bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.DOWN.getCode() || bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_RIGHT.getCode()) {
                ++this.currentPosition;
                if (this.currentPosition == this.callRecordAdapter.getCount()) {
                    this.currentPosition = this.callRecordAdapter.getCount() - 1;
                    this.isLongPress = false;
                }
                if (this.currentPosition > this.listView_record.getLastVisiblePosition() - 1) {
                    this.listView_record.smoothScrollToPositionFromTop(this.currentPosition, 0);
                }
            }
            else {
                if (bundle.getByte("idriver_enum") != Mainboard.EIdriverEnum.PRESS.getCode()) {
                    return;
                }
                if (LauncherApplication.isBlueConnectState) {
                    final String phone = this.callRecords.get(this.currentPosition).getPhone();
                    this.app.isComeFromRecord = true;
                    this.app.isPhoneNumFromRecord = true;
                    this.app.recorPhoneNumber = phone;
                    this.launcher.changeRightTo(new CallPhoneFragment());
                }
            }
            this.callRecordAdapter.setSelectItem(this.currentPosition);
            this.callRecordAdapter.notifyDataSetChanged();
        }
    }
    
    static class RecordHandler extends Handler
    {
        private WeakReference<CallRecordFragment> target;
        
        public RecordHandler(final CallRecordFragment callRecordFragment) {
            this.target = new WeakReference<CallRecordFragment>(callRecordFragment);
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
