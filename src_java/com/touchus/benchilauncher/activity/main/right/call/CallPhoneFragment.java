package com.touchus.benchilauncher.activity.main.right.call;

import com.touchus.benchilauncher.base.*;
import com.touchus.benchilauncher.*;
import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import java.util.*;
import com.touchus.publicutils.utils.*;
import android.support.v4.app.*;
import android.text.*;
import com.backaudio.android.driver.*;
import com.touchus.benchilauncher.service.*;
import android.util.*;
import android.widget.*;
import android.content.*;
import com.touchus.benchilauncher.utils.*;
import com.backaudio.android.driver.bluetooth.*;
import android.app.*;
import android.view.*;
import android.os.*;
import com.touchus.benchilauncher.views.*;
import java.lang.ref.*;

public class CallPhoneFragment extends BaseFragment implements View$OnClickListener
{
    public static final int DOWN_AREA = 2;
    public static final int UP_AREA = 1;
    public static int mSelectArea;
    public static int mSelectedIndexInDown;
    private String TAG;
    private LauncherApplication app;
    private Button bt_callphone_xia_bluetooth;
    private Button bt_callphone_xia_connect;
    private Button bt_callphone_xia_record;
    private CallPhoneKeyDialog callPhoneKeyDialog;
    private View fragmentView;
    boolean isAlreadyOut;
    private boolean isLogToggle;
    private boolean isMargin;
    boolean isSendNum;
    private ImageButton iv_number0;
    private ImageButton iv_number1;
    private ImageButton iv_number2;
    private ImageButton iv_number3;
    private ImageButton iv_number4;
    private ImageButton iv_number5;
    private ImageButton iv_number6;
    private ImageButton iv_number7;
    private ImageButton iv_number8;
    private ImageButton iv_number9;
    private ImageButton iv_number_clear;
    private ImageButton iv_number_end;
    private ImageButton iv_number_jia;
    private ImageButton iv_number_jin;
    private ImageButton iv_number_send;
    private ImageButton iv_number_xing;
    private LinearLayout ll_callphone_number;
    private Launcher mContext;
    private List<Button> mDownBtns;
    private BluetoothHandler mHandler;
    private int mSelectedIndexInUp;
    private List<ImageButton> mUpBtns;
    private MyCustomDialog myCustomDialog;
    private ImageView soundswitch;
    private TextView tv_call_connect_state;
    private TextView tv_callphone_bluetooth_state;
    private ImageView tv_callphone_bluetooth_stateimg;
    private TextView tv_callphone_number_details;
    
    static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus() {
        final int[] $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus = CallPhoneFragment.$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus;
        if ($switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus != null) {
            return $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus;
        }
        final int[] $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2 = new int[EPhoneStatus.values().length];
        while (true) {
            try {
                $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.CALLING_OUT.ordinal()] = 5;
                try {
                    $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.CONNECTED.ordinal()] = 3;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.CONNECTING.ordinal()] = 2;
                        try {
                            $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.INCOMING_CALL.ordinal()] = 4;
                            try {
                                $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.INITIALIZING.ordinal()] = 8;
                                try {
                                    $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.MULTI_TALKING.ordinal()] = 7;
                                    try {
                                        $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.TALKING.ordinal()] = 6;
                                        try {
                                            $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2[EPhoneStatus.UNCONNECT.ordinal()] = 1;
                                            return CallPhoneFragment.$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus = $switch_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus2;
                                        }
                                        catch (NoSuchFieldError noSuchFieldError) {}
                                    }
                                    catch (NoSuchFieldError noSuchFieldError2) {}
                                }
                                catch (NoSuchFieldError noSuchFieldError3) {}
                            }
                            catch (NoSuchFieldError noSuchFieldError4) {}
                        }
                        catch (NoSuchFieldError noSuchFieldError5) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError6) {}
                }
                catch (NoSuchFieldError noSuchFieldError7) {}
            }
            catch (NoSuchFieldError noSuchFieldError8) {
                continue;
            }
            break;
        }
    }
    
    static {
        CallPhoneFragment.mSelectArea = 1;
        CallPhoneFragment.mSelectedIndexInDown = 0;
    }
    
    public CallPhoneFragment() {
        this.TAG = this.getClass().getSimpleName();
        this.isLogToggle = true;
        this.mUpBtns = new ArrayList<ImageButton>();
        this.mDownBtns = new ArrayList<Button>();
        this.isMargin = false;
        this.mSelectedIndexInUp = 0;
        this.mHandler = new BluetoothHandler(this);
        this.isSendNum = false;
        this.isAlreadyOut = false;
    }
    
    static /* synthetic */ void access$6(final CallPhoneFragment callPhoneFragment, final MyCustomDialog myCustomDialog) {
        callPhoneFragment.myCustomDialog = myCustomDialog;
    }
    
    private void afterSendBeforeConnectBtnState() {
        this.tv_callphone_bluetooth_state.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165228)) + " " + this.app.btservice.currentEquipName));
        this.tv_callphone_bluetooth_stateimg.setImageResource(2130837637);
        this.tv_callphone_number_details.setText((CharSequence)this.getName());
        this.tv_call_connect_state.setText((CharSequence)this.mContext.getString(2131165243));
        this.soundswitch.setVisibility(0);
        this.setAllUpBtnUnclickable();
        this.setUpBtnClickable(15);
        this.setAllDownBtnUnclickable();
        this.autoChangeUpSelectBtn();
    }
    
    private void answerBeforeConnectBtnState() {
        this.tv_callphone_bluetooth_state.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165228)) + " " + this.app.btservice.currentEquipName));
        this.tv_callphone_bluetooth_stateimg.setImageResource(2130837637);
        this.tv_callphone_number_details.setText((CharSequence)this.getName());
        this.tv_call_connect_state.setText((CharSequence)this.mContext.getString(2131165242));
        this.soundswitch.setVisibility(0);
        this.setAllUpBtnUnclickable();
        this.setUpBtnClickable(0);
        this.setUpBtnClickable(15);
        this.setAllDownBtnUnclickable();
        this.autoChangeUpSelectBtn();
    }
    
    private void autoChangeUpSelectBtn() {
        if (CallPhoneFragment.mSelectArea == 2 && this.downHaveBtnCanSelect(CallPhoneFragment.mSelectedIndexInDown)) {
            this.downSelectBtn(CallPhoneFragment.mSelectedIndexInDown);
        }
        else if (this.upHaveBtnCanSelect(this.mSelectedIndexInUp)) {
            this.upSelectBtn(this.mSelectedIndexInUp);
        }
    }
    
    private void bluetoothState() {
    }
    
    private void btConnetAndHavePhoneNumBtnState() {
        this.tv_callphone_bluetooth_state.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165228)) + " " + this.app.btservice.currentEquipName));
        this.tv_callphone_bluetooth_stateimg.setImageResource(2130837637);
        this.tv_call_connect_state.setText((CharSequence)"");
        this.soundswitch.setVisibility(4);
        this.setAllUpBtnClickable();
        this.setAllDownBtnUnclickable();
        this.autoChangeUpSelectBtn();
    }
    
    private void btConnetAndNoPhoneNumBtnState() {
        this.tv_callphone_bluetooth_state.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165228)) + " " + this.app.btservice.currentEquipName));
        this.tv_callphone_bluetooth_stateimg.setImageResource(2130837637);
        this.tv_call_connect_state.setText((CharSequence)"");
        this.tv_callphone_number_details.setText((CharSequence)"");
        this.bt_callphone_xia_connect.setText(2131165226);
        this.soundswitch.setVisibility(4);
        this.setAllUpBtnClickable();
        this.setUpBtnUnclickable(0);
        this.setUpBtnUnclickable(14);
        this.setUpBtnUnclickable(15);
        this.setAllDownBtnClickable();
        this.autoChangeUpSelectBtn();
        if (this.app.isPhoneNumFromRecord) {
            this.app.phoneNumber = this.app.recorPhoneNumber;
            this.afterSendBeforeConnectBtnState();
            this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
            if (this.isAlreadyOut) {
                this.sendPhoneNumFromRecord();
                this.isAlreadyOut = false;
            }
            return;
        }
        this.app.service.removeLoadingFloatView();
    }
    
    private void btUnconnectBtnState() {
        this.tv_callphone_bluetooth_state.setText((CharSequence)this.mContext.getString(2131165227));
        this.tv_callphone_bluetooth_stateimg.setImageResource(2130837638);
        this.tv_call_connect_state.setText((CharSequence)"");
        this.tv_callphone_number_details.setText((CharSequence)"");
        this.bt_callphone_xia_connect.setText(2131165225);
        this.soundswitch.setVisibility(4);
        this.app.phoneNumber = "";
        this.setAllUpBtnUnclickable();
        this.setAllDownBtnClickable();
        this.autoChangeUpSelectBtn();
    }
    
    private String clearNumber() {
        if (this.app.phoneNumber.length() > 0) {
            return this.app.phoneNumber.substring(0, this.app.phoneNumber.length() - 1);
        }
        return "";
    }
    
    private void clickDownBtn(final int n) {
        if (!UtilTools.isFastDoubleClick()) {
            switch (n) {
                default: {}
                case 0: {
                    if (this.myCustomDialog == null) {
                        if (LauncherApplication.isBlueConnectState) {
                            this.app.btservice.disconnectCurDevice();
                        }
                        this.showBluetoothDialog(this.mContext.getString(2131165230), this.app.btservice.getDeviceName(), null, null, true);
                        return;
                    }
                    break;
                }
                case 1: {
                    this.mContext.changeRightTo(new CallRecordFragment());
                }
                case 2: {
                    this.mContext.changeRightTo(new CallAddressBookFragment());
                }
            }
        }
    }
    
    private void dismissNumberKeyDialog() {
        if (this.callPhoneKeyDialog != null && this.callPhoneKeyDialog.isShowing()) {
            this.callPhoneKeyDialog.dismiss();
        }
        this.callPhoneKeyDialog = null;
    }
    
    private void downAllDisselect() {
        for (int i = 0; i < this.mDownBtns.size(); ++i) {
            this.mDownBtns.get(i).setSelected(false);
        }
    }
    
    private void downListenSelect(final int n) {
        this.downSelectBtn(n);
        this.clickDownBtn(n);
    }
    
    private void downSelectBtn(final int mSelectedIndexInDown) {
        CallPhoneFragment.mSelectArea = 2;
        this.upAllDisselect();
        this.downAllDisselect();
        this.mDownBtns.get(mSelectedIndexInDown).setSelected(true);
        this.isMargin = false;
        this.setMaringL();
        CallPhoneFragment.mSelectedIndexInDown = mSelectedIndexInDown;
    }
    
    private String getName() {
        if (TextUtils.isEmpty((CharSequence)this.app.phoneName)) {
            return this.app.phoneNumber;
        }
        return this.app.phoneName;
    }
    
    private void handlerCallConnState() {
        if (LauncherApplication.isBlueConnectState) {
            this.btConnetAndNoPhoneNumBtnState();
            if (this.myCustomDialog != null && this.myCustomDialog.isShowing()) {
                this.myCustomDialog.dismiss();
            }
        }
        else {
            this.btUnconnectBtnState();
        }
        this.soundswitch.setVisibility(4);
    }
    
    private void handlerHangup() {
        this.app.phoneNumber = "";
        this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
        if (LauncherApplication.isBlueConnectState) {
            this.btConnetAndNoPhoneNumBtnState();
            return;
        }
        this.btUnconnectBtnState();
    }
    
    private void handlerIdriver(final Bundle bundle) {
        final byte byte1 = bundle.getByte("idriver_enum");
        if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode()) {
            this.operateTurnRight();
        }
        else {
            if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode()) {
                this.operateTurnLeft();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.operateRight();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                this.operateLeft();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                this.operatePress();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.UP.getCode()) {
                this.operateUp();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.DOWN.getCode()) {
                this.operateDown();
                return;
            }
            if (byte1 == Mainboard.EIdriverEnum.PICK_UP.getCode()) {
                if (BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                    this.upListenSelectBtn(0);
                    return;
                }
                if (BluetoothService.bluetoothStatus == EPhoneStatus.CONNECTED) {
                    this.upListenSelectBtn(0);
                }
            }
            else if (byte1 == Mainboard.EIdriverEnum.HANG_UP.getCode()) {
                if (BluetoothService.bluetoothStatus == EPhoneStatus.TALKING || BluetoothService.bluetoothStatus == EPhoneStatus.CALLING_OUT || BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                    this.upListenSelectBtn(15);
                    return;
                }
                if (BluetoothService.bluetoothStatus == EPhoneStatus.CONNECTED) {
                    this.clickUpBtn(15);
                }
            }
        }
    }
    
    private void handlerMsg(final Message message) {
        final Bundle data = message.getData();
        switch (message.what) {
            case 6001: {
                this.handlerIdriver(data);
            }
            case 1411: {
                this.phoneConnectingBtnState();
            }
            case 6002: {
                this.handlerCallConnState();
            }
            case 1415: {
                if (BluetoothService.bluetoothStatus == EPhoneStatus.CALLING_OUT) {
                    this.handlerPhoneNowCallout();
                    return;
                }
                if (BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                    this.handlerPhoneNowCallin();
                    return;
                }
                break;
            }
            case 1416: {
                if (this.app.isPhoneNumFromRecord) {
                    this.app.isPhoneNumFromRecord = false;
                }
                this.handlerHangup();
            }
            case 1410: {
                if (this.app.btservice.iSoundInPhone) {
                    this.soundswitch.setImageResource(2130837611);
                    return;
                }
                this.soundswitch.setImageResource(2130837589);
            }
        }
    }
    
    private void handlerPhoneNowCallin() {
        if (this.app.phoneNumber == null) {
            this.app.phoneNumber = "";
        }
        this.answerBeforeConnectBtnState();
        this.upSelectBtn(0);
    }
    
    private void handlerPhoneNowCallout() {
        if (this.app.phoneNumber == null) {
            this.app.phoneNumber = "";
        }
        this.afterSendBeforeConnectBtnState();
        this.upSelectBtn(15);
    }
    
    private void initEvent() {
        this.iv_number_send.setOnClickListener((View$OnClickListener)this);
        this.iv_number0.setOnClickListener((View$OnClickListener)this);
        this.iv_number1.setOnClickListener((View$OnClickListener)this);
        this.iv_number2.setOnClickListener((View$OnClickListener)this);
        this.iv_number3.setOnClickListener((View$OnClickListener)this);
        this.iv_number4.setOnClickListener((View$OnClickListener)this);
        this.iv_number5.setOnClickListener((View$OnClickListener)this);
        this.iv_number6.setOnClickListener((View$OnClickListener)this);
        this.iv_number7.setOnClickListener((View$OnClickListener)this);
        this.iv_number8.setOnClickListener((View$OnClickListener)this);
        this.iv_number9.setOnClickListener((View$OnClickListener)this);
        this.iv_number_xing.setOnClickListener((View$OnClickListener)this);
        this.iv_number_jin.setOnClickListener((View$OnClickListener)this);
        this.iv_number_jia.setOnClickListener((View$OnClickListener)this);
        this.iv_number_clear.setOnClickListener((View$OnClickListener)this);
        this.iv_number_end.setOnClickListener((View$OnClickListener)this);
        this.soundswitch.setOnClickListener((View$OnClickListener)this);
        this.bt_callphone_xia_connect.setOnClickListener((View$OnClickListener)this);
        this.bt_callphone_xia_record.setOnClickListener((View$OnClickListener)this);
        this.bt_callphone_xia_bluetooth.setOnClickListener((View$OnClickListener)this);
    }
    
    private void initStae() {
        switch ($SWITCH_TABLE$com$backaudio$android$driver$bluetooth$bc8mpprotocol$EPhoneStatus()[BluetoothService.bluetoothStatus.ordinal()]) {
            default: {}
            case 3: {
                if (this.app.isComeFromRecord) {
                    this.isAlreadyOut = true;
                    this.app.phoneName = this.app.btservice.getbookName(this.app.phoneNumber);
                    this.afterSendBeforeConnectBtnState();
                    this.app.isComeFromRecord = false;
                    if (this.app.isPhoneNumFromRecord && !TextUtils.isEmpty((CharSequence)this.app.recorPhoneNumber)) {
                        this.sendPhoneNumFromRecord();
                    }
                }
                else {
                    this.app.phoneNumber = "";
                    this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
                    this.btConnetAndNoPhoneNumBtnState();
                }
                this.app.phoneNumber.equals("");
            }
            case 5: {
                this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
                this.afterSendBeforeConnectBtnState();
            }
            case 6: {
                this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
                this.phoneConnectingBtnState();
            }
            case 4: {
                this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
                this.handlerPhoneNowCallin();
            }
            case 1: {
                this.btUnconnectBtnState();
            }
        }
    }
    
    private void initView() {
        this.iv_number0 = (ImageButton)this.fragmentView.findViewById(2131427477);
        this.iv_number1 = (ImageButton)this.fragmentView.findViewById(2131427478);
        this.iv_number2 = (ImageButton)this.fragmentView.findViewById(2131427479);
        this.iv_number3 = (ImageButton)this.fragmentView.findViewById(2131427480);
        this.iv_number4 = (ImageButton)this.fragmentView.findViewById(2131427481);
        this.iv_number5 = (ImageButton)this.fragmentView.findViewById(2131427482);
        this.iv_number6 = (ImageButton)this.fragmentView.findViewById(2131427483);
        this.iv_number7 = (ImageButton)this.fragmentView.findViewById(2131427484);
        this.iv_number8 = (ImageButton)this.fragmentView.findViewById(2131427485);
        this.iv_number9 = (ImageButton)this.fragmentView.findViewById(2131427486);
        this.iv_number_send = (ImageButton)this.fragmentView.findViewById(2131427475);
        this.iv_number_xing = (ImageButton)this.fragmentView.findViewById(2131427487);
        this.iv_number_jin = (ImageButton)this.fragmentView.findViewById(2131427488);
        this.iv_number_jia = (ImageButton)this.fragmentView.findViewById(2131427489);
        this.iv_number_clear = (ImageButton)this.fragmentView.findViewById(2131427490);
        this.iv_number_end = (ImageButton)this.fragmentView.findViewById(2131427491);
        this.ll_callphone_number = (LinearLayout)this.fragmentView.findViewById(2131427476);
        this.bt_callphone_xia_connect = (Button)this.fragmentView.findViewById(2131427492);
        this.bt_callphone_xia_record = (Button)this.fragmentView.findViewById(2131427493);
        this.bt_callphone_xia_bluetooth = (Button)this.fragmentView.findViewById(2131427494);
        this.tv_callphone_number_details = (TextView)this.fragmentView.findViewById(2131427473);
        this.tv_callphone_bluetooth_state = (TextView)this.fragmentView.findViewById(2131427471);
        this.tv_callphone_bluetooth_stateimg = (ImageView)this.fragmentView.findViewById(2131427470);
        this.soundswitch = (ImageView)this.fragmentView.findViewById(2131427472);
        this.tv_call_connect_state = (TextView)this.fragmentView.findViewById(2131427474);
    }
    
    private void leftInDown() {
        int i = CallPhoneFragment.mSelectedIndexInDown;
        while (i > 0) {
            final int mSelectedIndexInDown = --i;
            if (this.mDownBtns.get(mSelectedIndexInDown).isEnabled()) {
                this.downSelectBtn(CallPhoneFragment.mSelectedIndexInDown = mSelectedIndexInDown);
            }
        }
    }
    
    private void leftInUp() {
        int i = this.mSelectedIndexInUp;
        while (i > 0) {
            final int mSelectedIndexInUp = --i;
            if (this.mUpBtns.get(mSelectedIndexInUp).isEnabled()) {
                this.upSelectBtn(this.mSelectedIndexInUp = mSelectedIndexInUp);
            }
        }
    }
    
    private void phoneConnectingBtnState() {
        this.tv_callphone_bluetooth_state.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165228)) + " " + this.app.btservice.currentEquipName));
        this.tv_callphone_bluetooth_stateimg.setImageResource(2130837637);
        this.tv_callphone_number_details.setText((CharSequence)this.getName());
        this.tv_call_connect_state.setText((CharSequence)this.mContext.getString(2131165244));
        this.soundswitch.setVisibility(0);
        this.setAllUpBtnClickable();
        this.setUpBtnUnclickable(0);
        this.setUpBtnUnclickable(14);
        this.setAllDownBtnUnclickable();
        this.autoChangeUpSelectBtn();
    }
    
    private void rightInDown() {
        int i = CallPhoneFragment.mSelectedIndexInDown;
        while (i < this.mDownBtns.size() - 1) {
            final int mSelectedIndexInDown = ++i;
            if (this.mDownBtns.get(mSelectedIndexInDown).isEnabled()) {
                this.downSelectBtn(CallPhoneFragment.mSelectedIndexInDown = mSelectedIndexInDown);
            }
        }
    }
    
    private void rightInUp() {
        int i = this.mSelectedIndexInUp;
        while (i < this.mUpBtns.size() - 1) {
            final int mSelectedIndexInUp = ++i;
            if (this.mUpBtns.get(mSelectedIndexInUp).isEnabled()) {
                this.upSelectBtn(this.mSelectedIndexInUp = mSelectedIndexInUp);
            }
        }
    }
    
    private void sendPhoneNumFromRecord() {
        Log.e("sendPhone", ":--\tsendPhoneNumFromRecord");
        if (this.isSendNum) {
            return;
        }
        this.isSendNum = true;
        this.tv_callphone_number_details.setText((CharSequence)this.app.recorPhoneNumber);
        this.app.btservice.CallOut(this.app.recorPhoneNumber);
    }
    
    private void setAllDownBtnClickable() {
        for (int i = 0; i < this.mDownBtns.size(); ++i) {
            this.setDownBtnClickable(i);
        }
    }
    
    private void setAllDownBtnUnclickable() {
        for (int i = 0; i < this.mDownBtns.size(); ++i) {
            this.setDownBtnUnclickable(i);
        }
    }
    
    private void setAllUpBtnClickable() {
        for (int i = 0; i < this.mUpBtns.size(); ++i) {
            this.setUpBtnClickable(i);
        }
    }
    
    private void setAllUpBtnUnclickable() {
        for (int i = 0; i < this.mUpBtns.size(); ++i) {
            this.setUpBtnUnclickable(i);
        }
    }
    
    private void setDialogLocation(final Dialog dialog) {
        final Window window = dialog.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        attributes.x = 256;
        attributes.y = -30;
        window.setAttributes(attributes);
    }
    
    private void setDownBtnClickable(final int n) {
        this.mDownBtns.get(n).setClickable(true);
        this.mDownBtns.get(n).setAlpha(1.0f);
        this.mDownBtns.get(n).setEnabled(true);
    }
    
    private void setDownBtnUnclickable(final int n) {
        this.mDownBtns.get(n).setClickable(false);
        this.mDownBtns.get(n).setAlpha(0.3f);
        this.mDownBtns.get(n).setEnabled(false);
    }
    
    private void setMaringL() {
        final RelativeLayout$LayoutParams layoutParams = (RelativeLayout$LayoutParams)this.ll_callphone_number.getLayoutParams();
        if (this.isMargin) {
            layoutParams.setMargins(123, 0, 0, 0);
        }
        else {
            layoutParams.setMargins(170, 0, 0, 0);
        }
        this.ll_callphone_number.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    private void setUpBtnClickable(final int n) {
        this.mUpBtns.get(n).setClickable(true);
        this.mUpBtns.get(n).setAlpha(1.0f);
        this.mUpBtns.get(n).setEnabled(true);
    }
    
    private void setUpBtnUnclickable(final int n) {
        this.mUpBtns.get(n).setClickable(false);
        this.mUpBtns.get(n).setAlpha(0.3f);
        this.mUpBtns.get(n).setEnabled(false);
        this.mUpBtns.get(n).setSelected(false);
    }
    
    private void showBluetoothDialog(final String message, final String nextMessage, final String s, final String s2, final boolean b) {
        final MyCustomDialog.Builder builder = new MyCustomDialog.Builder((Context)this.mContext);
        builder.setMessage(message);
        builder.setNextMessage(nextMessage);
        builder.setPositiveButton(s, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                if (b) {
                    CallPhoneFragment.this.app.btservice.disconnectCurDevice();
                    return;
                }
                CallPhoneFragment.this.bluetoothState();
            }
        });
        builder.setNegativeButton(s2, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                if (b) {
                    CallPhoneFragment.this.bt_callphone_xia_connect.setSelected(false);
                    return;
                }
                CallPhoneFragment.this.bt_callphone_xia_bluetooth.setSelected(false);
            }
        });
        this.myCustomDialog = builder.create();
        if (!LauncherApplication.isBlueConnectState && b) {
            this.app.btservice.enterPairingMode();
        }
        this.myCustomDialog.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                builder.unRegisterHandler();
                CallPhoneFragment.access$6(CallPhoneFragment.this, null);
            }
        });
        this.setDialogLocation(this.myCustomDialog);
        this.myCustomDialog.show();
    }
    
    private void showNumberKeyDialog() {
        if (this.callPhoneKeyDialog == null) {
            this.callPhoneKeyDialog = new CallPhoneKeyDialog((Context)this.getActivity(), 2131230740);
        }
        this.callPhoneKeyDialog.parent = this;
        this.callPhoneKeyDialog.show();
    }
    
    private void swithSound() {
        this.app.btservice.switchDevice();
        if (this.app.btservice.iSoundInPhone) {
            this.soundswitch.setImageResource(2130837611);
            return;
        }
        this.soundswitch.setImageResource(2130837589);
    }
    
    private void upAllDisselect() {
        for (int i = 0; i < this.mUpBtns.size(); ++i) {
            this.mUpBtns.get(i).setSelected(false);
        }
    }
    
    private void upClick2AddChar(final String s) {
        final LauncherApplication app = this.app;
        app.phoneNumber = String.valueOf(app.phoneNumber) + s;
        if (BluetoothService.bluetoothStatus == EPhoneStatus.TALKING) {
            this.phoneConnectingBtnState();
        }
        else {
            this.btConnetAndHavePhoneNumBtnState();
        }
        this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
    }
    
    private void upSelectBtn(final int mSelectedIndexInUp) {
        CallPhoneFragment.mSelectArea = 1;
        this.downAllDisselect();
        this.upAllDisselect();
        this.mUpBtns.get(mSelectedIndexInUp).setSelected(true);
        if (mSelectedIndexInUp < 1) {
            this.isMargin = false;
        }
        else {
            this.isMargin = true;
        }
        this.setMaringL();
        this.mSelectedIndexInUp = mSelectedIndexInUp;
    }
    
    public void clickUpBtn(final int n) {
        Log.e("bluetooth", new StringBuilder(String.valueOf(n)).toString());
        if (!this.mUpBtns.get(n).isEnabled()) {
            return;
        }
        switch (n) {
            default: {}
            case 0: {
                if (this.app.phoneNumber.length() < 1) {
                    ToastTool.showBigShortToast((Context)this.mContext, 2131165233);
                    return;
                }
                if (BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                    this.app.btservice.answerCalling();
                    return;
                }
                this.app.btservice.CallOut(this.app.phoneNumber);
                this.afterSendBeforeConnectBtnState();
            }
            case 1: {
                this.upClick2AddChar("0");
                this.app.btservice.pressVirutalButton(EVirtualButton.ZERO);
            }
            case 2: {
                this.upClick2AddChar("1");
                this.app.btservice.pressVirutalButton(EVirtualButton.ONE);
            }
            case 3: {
                this.upClick2AddChar("2");
                this.app.btservice.pressVirutalButton(EVirtualButton.TWO);
            }
            case 4: {
                this.upClick2AddChar("3");
                this.app.btservice.pressVirutalButton(EVirtualButton.THREE);
            }
            case 5: {
                this.upClick2AddChar("4");
                this.app.btservice.pressVirutalButton(EVirtualButton.FOUR);
            }
            case 6: {
                this.upClick2AddChar("5");
                this.app.btservice.pressVirutalButton(EVirtualButton.FIVE);
            }
            case 7: {
                this.upClick2AddChar("6");
                this.app.btservice.pressVirutalButton(EVirtualButton.SIX);
                this.btConnetAndHavePhoneNumBtnState();
            }
            case 8: {
                this.upClick2AddChar("7");
                this.app.btservice.pressVirutalButton(EVirtualButton.SEVEN);
            }
            case 9: {
                this.upClick2AddChar("8");
                this.app.btservice.pressVirutalButton(EVirtualButton.EIGHT);
            }
            case 10: {
                this.upClick2AddChar("9");
                this.app.btservice.pressVirutalButton(EVirtualButton.NINE);
            }
            case 11: {
                this.upClick2AddChar("*");
                this.app.btservice.pressVirutalButton(EVirtualButton.ASTERISK);
            }
            case 12: {
                this.upClick2AddChar("#");
                this.app.btservice.pressVirutalButton(EVirtualButton.WELL);
            }
            case 13: {
                this.upClick2AddChar("+");
            }
            case 14: {
                this.app.phoneNumber = this.clearNumber();
                if (this.app.phoneNumber.length() == 0) {
                    this.btConnetAndNoPhoneNumBtnState();
                }
                else {
                    this.btConnetAndHavePhoneNumBtnState();
                }
                this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
            }
            case 15: {
                this.app.phoneNumber = "";
                this.tv_callphone_number_details.setText((CharSequence)this.app.phoneNumber);
                this.btConnetAndNoPhoneNumBtnState();
                if (this.app.isPhoneNumFromRecord) {
                    this.app.isPhoneNumFromRecord = false;
                }
                this.app.btservice.cutdownCurrentCalling();
            }
        }
    }
    
    public void connectBluetooth() {
        this.app.btservice.enterPairingMode();
    }
    
    public boolean downHaveBtnCanSelect(int i) {
        for (int j = i; j < this.mDownBtns.size(); ++j) {
            if (this.mDownBtns.get(j).isEnabled()) {
                CallPhoneFragment.mSelectedIndexInDown = j;
                return true;
            }
        }
        for (--i; i > -1; --i) {
            if (this.mDownBtns.get(i).isEnabled()) {
                CallPhoneFragment.mSelectedIndexInDown = i;
                return true;
            }
        }
        return false;
    }
    
    public void initDataFromLauncher() {
        if (this.getActivity() == null) {}
    }
    
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
    }
    
    @Override
    public boolean onBack() {
        if (this.myCustomDialog != null && this.myCustomDialog.isShowing()) {
            this.myCustomDialog.dismiss();
            return true;
        }
        return false;
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131427475: {
                this.upListenSelectBtn(0);
            }
            case 2131427477: {
                this.upListenSelectBtn(1);
            }
            case 2131427478: {
                this.upListenSelectBtn(2);
            }
            case 2131427479: {
                this.upListenSelectBtn(3);
            }
            case 2131427480: {
                this.upListenSelectBtn(4);
            }
            case 2131427481: {
                this.upListenSelectBtn(5);
            }
            case 2131427482: {
                this.upListenSelectBtn(6);
            }
            case 2131427483: {
                this.upListenSelectBtn(7);
            }
            case 2131427484: {
                this.upListenSelectBtn(8);
            }
            case 2131427485: {
                this.upListenSelectBtn(9);
            }
            case 2131427486: {
                this.upListenSelectBtn(10);
            }
            case 2131427487: {
                this.upListenSelectBtn(11);
            }
            case 2131427488: {
                this.upListenSelectBtn(12);
            }
            case 2131427489: {
                this.upListenSelectBtn(13);
            }
            case 2131427490: {
                this.upListenSelectBtn(14);
            }
            case 2131427491: {
                this.upListenSelectBtn(15);
            }
            case 2131427492: {
                this.downListenSelect(0);
            }
            case 2131427493: {
                this.downListenSelect(1);
            }
            case 2131427494: {
                this.downListenSelect(2);
            }
            case 2131427472: {
                this.swithSound();
            }
        }
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = (Launcher)this.getActivity();
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.app = (LauncherApplication)this.getActivity().getApplication();
        this.fragmentView = layoutInflater.inflate(2130903068, (ViewGroup)null);
        this.initView();
        return this.fragmentView;
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    public void onPause() {
        super.onPause();
        this.app.unregisterHandler(this.mHandler);
        this.app.istop = false;
        if (this.app.btservice != null && (BluetoothService.talkingflag || this.app.btservice.showDialogflag) && !this.app.topIsBlueMainFragment()) {
            this.app.btservice.enterSystemFloatCallView();
        }
        if (this.myCustomDialog != null && this.myCustomDialog.isShowing()) {
            this.myCustomDialog.dismiss();
        }
        this.app.service.removeLoadingFloatView();
    }
    
    public void onResume() {
        super.onResume();
        this.app.istop = true;
        if (!FloatSystemCallDialog.getInstance().isDestory() && FloatSystemCallDialog.getInstance().getShowStatus() != FloatSystemCallDialog.FloatShowST.HIDEN) {
            FloatSystemCallDialog.getInstance().hiden();
        }
        this.initEvent();
        this.mUpBtns.add(this.iv_number_send);
        this.mUpBtns.add(this.iv_number0);
        this.mUpBtns.add(this.iv_number1);
        this.mUpBtns.add(this.iv_number2);
        this.mUpBtns.add(this.iv_number3);
        this.mUpBtns.add(this.iv_number4);
        this.mUpBtns.add(this.iv_number5);
        this.mUpBtns.add(this.iv_number6);
        this.mUpBtns.add(this.iv_number7);
        this.mUpBtns.add(this.iv_number8);
        this.mUpBtns.add(this.iv_number9);
        this.mUpBtns.add(this.iv_number_xing);
        this.mUpBtns.add(this.iv_number_jin);
        this.mUpBtns.add(this.iv_number_jia);
        this.mUpBtns.add(this.iv_number_clear);
        this.mUpBtns.add(this.iv_number_end);
        this.mDownBtns.add(this.bt_callphone_xia_connect);
        this.mDownBtns.add(this.bt_callphone_xia_record);
        this.mDownBtns.add(this.bt_callphone_xia_bluetooth);
        this.initStae();
        this.app.registerHandler(this.mHandler);
    }
    
    public void operateDown() {
        if (CallPhoneFragment.mSelectArea != 2 && this.downHaveBtnCanSelect(CallPhoneFragment.mSelectedIndexInDown)) {
            CallPhoneFragment.mSelectArea = 2;
            this.downSelectBtn(CallPhoneFragment.mSelectedIndexInDown);
        }
    }
    
    public void operateLeft() {
        if (CallPhoneFragment.mSelectArea == 1) {
            this.leftInUp();
            return;
        }
        this.leftInDown();
    }
    
    public void operatePress() {
        if (CallPhoneFragment.mSelectArea == 1) {
            this.clickUpBtn(this.mSelectedIndexInUp);
        }
        else if (CallPhoneFragment.mSelectArea == 2) {
            this.clickDownBtn(CallPhoneFragment.mSelectedIndexInDown);
        }
    }
    
    public void operateRight() {
        if (CallPhoneFragment.mSelectArea == 1) {
            this.rightInUp();
            return;
        }
        this.rightInDown();
    }
    
    public void operateTurnLeft() {
        this.operateLeft();
    }
    
    public void operateTurnRight() {
        this.operateRight();
    }
    
    public void operateUp() {
        if (CallPhoneFragment.mSelectArea != 1 && this.upHaveBtnCanSelect(this.mSelectedIndexInUp)) {
            CallPhoneFragment.mSelectArea = 1;
            this.upSelectBtn(this.mSelectedIndexInUp);
        }
    }
    
    public boolean upHaveBtnCanSelect(int i) {
        for (int j = i; j < this.mUpBtns.size(); ++j) {
            if (this.mUpBtns.get(j).isEnabled()) {
                this.mSelectedIndexInUp = j;
                return true;
            }
        }
        for (--i; i > -1; --i) {
            if (this.mUpBtns.get(i).isEnabled()) {
                this.mSelectedIndexInUp = i;
                return true;
            }
        }
        return false;
    }
    
    public void upListenSelectBtn(final int n) {
        this.upSelectBtn(n);
        this.clickUpBtn(n);
    }
    
    static class BluetoothHandler extends Handler
    {
        private WeakReference<CallPhoneFragment> target;
        
        public BluetoothHandler(final CallPhoneFragment callPhoneFragment) {
            this.target = new WeakReference<CallPhoneFragment>(callPhoneFragment);
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
