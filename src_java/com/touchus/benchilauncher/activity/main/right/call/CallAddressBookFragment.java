package com.touchus.benchilauncher.activity.main.right.call;

import com.touchus.benchilauncher.base.*;
import android.support.v4.widget.*;
import com.touchus.benchilauncher.adapter.*;
import com.touchus.benchilauncher.bean.*;
import android.content.*;
import com.touchus.benchilauncher.*;
import java.util.*;
import com.backaudio.android.driver.*;
import android.widget.*;
import android.support.v4.app.*;
import android.app.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class CallAddressBookFragment extends BaseFragment implements OnRefreshListener
{
    private String TAG;
    private LauncherApplication app;
    private CallRecordAdapter callRecordAdapter;
    private View callRecordView;
    private List<Person> callRecords;
    private Context context;
    private int currentPosition;
    private boolean isLogToggle;
    private boolean isLongPress;
    private Launcher launcher;
    private SwipeRefreshLayout layout;
    private ListView listView_record;
    private RecordHandler mHandler;
    private TextView noDataTv;
    
    public CallAddressBookFragment() {
        this.TAG = this.getClass().getSimpleName();
        this.isLogToggle = true;
        this.callRecords = new ArrayList<Person>();
        this.currentPosition = 0;
        this.isLongPress = false;
        this.mHandler = new RecordHandler(this);
    }
    
    static /* synthetic */ void access$2(final CallAddressBookFragment callAddressBookFragment, final int currentPosition) {
        callAddressBookFragment.currentPosition = currentPosition;
    }
    
    private void handlerMsg(final Message message) {
        switch (message.what) {
            default: {}
            case 1407: {
                this.app.service.createLoadingFloatView(this.getString(2131165241));
                this.callRecords.clear();
            }
            case 1408: {
                this.app.service.removeLoadingFloatView();
            }
            case 1409: {
                this.app.service.removeLoadingFloatView();
                this.callRecords.addAll(this.app.btservice.getBookList());
                if (this.callRecords.size() == 0) {
                    this.noDataTv.setVisibility(0);
                }
                else {
                    this.noDataTv.setVisibility(8);
                }
                this.callRecordAdapter.setData(this.callRecords);
                this.callRecordAdapter.notifyDataSetChanged();
                this.callRecordAdapter.notifyDataSetInvalidated();
                this.layout.setRefreshing(false);
            }
            case 6001: {
                this.handlerMsgIdr(message);
            }
        }
    }
    
    private void handlerMsgIdr(final Message message) {
        final Bundle data = message.getData();
        if (data.getByte("idriver_state_enum") == Mainboard.EBtnStateEnum.BTN_LONG_PRESS.getCode()) {
            this.isLongPress = true;
        }
        else if (data.getByte("idriver_state_enum") == Mainboard.EBtnStateEnum.BTN_DOWN.getCode()) {
            this.isLongPress = false;
            if (this.callRecords.size() > 0) {
                this.pressUpDown(data);
            }
        }
        else {
            if (data.getByte("idriver_state_enum") == Mainboard.EBtnStateEnum.BTN_UP.getCode()) {
                this.isLongPress = false;
                return;
            }
            this.isLongPress = false;
        }
    }
    
    private void initData() {
        this.callRecordAdapter = new CallRecordAdapter(this.context, 1);
        if (LauncherApplication.isBlueConnectState && this.app.btservice.getBookList().size() == 0) {
            this.app.btservice.downloadflag = 0;
            this.app.btservice.lastindex = 0;
            this.app.btservice.downloadType = 0;
            this.app.btservice.tryToDownloadPhoneBook();
        }
        else {
            this.callRecords.addAll(this.app.btservice.getBookList());
        }
        this.callRecordAdapter.setData(this.callRecords);
        this.listView_record.setAdapter((ListAdapter)this.callRecordAdapter);
        if (this.callRecords.size() == 0) {
            this.noDataTv.setVisibility(0);
            return;
        }
        this.noDataTv.setVisibility(8);
    }
    
    private void initEvent() {
        this.listView_record.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int selectItem, final long n) {
                CallAddressBookFragment.this.callRecordAdapter.setSelectItem(selectItem);
                CallAddressBookFragment.access$2(CallAddressBookFragment.this, selectItem);
                CallAddressBookFragment.this.callRecordAdapter.notifyDataSetChanged();
                if (LauncherApplication.isBlueConnectState) {
                    CallAddressBookFragment.this.app.isComeFromRecord = true;
                    CallAddressBookFragment.this.app.isPhoneNumFromRecord = true;
                    CallAddressBookFragment.this.app.recorPhoneNumber = CallAddressBookFragment.this.callRecords.get(selectItem).getPhone();
                    CallAddressBookFragment.this.launcher.changeRightTo(new CallPhoneFragment());
                }
            }
        });
        this.layout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener)this);
    }
    
    private void initView() {
        this.listView_record = (ListView)this.callRecordView.findViewById(2131427467);
        this.layout = (SwipeRefreshLayout)this.callRecordView.findViewById(2131427466);
        (this.noDataTv = (TextView)this.callRecordView.findViewById(2131427468)).setText(this.getText(2131165303));
    }
    
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        this.context = (Context)this.getActivity();
        this.launcher = (Launcher)this.getActivity();
        this.app = (LauncherApplication)this.getActivity().getApplication();
    }
    
    @Override
    public boolean onBack() {
        return false;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.callRecordView = layoutInflater.inflate(2130903067, (ViewGroup)null);
        this.initView();
        this.initData();
        this.initEvent();
        return this.callRecordView;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public void onRefresh() {
        if (LauncherApplication.isBlueConnectState) {
            this.app.btservice.downloadflag = 0;
            this.app.btservice.lastindex = 0;
            this.app.btservice.downloadType = 0;
            this.app.btservice.tryToDownloadPhoneBook();
        }
    }
    
    @Override
    public void onStart() {
        this.app.registerHandler(this.mHandler);
        super.onStart();
    }
    
    @Override
    public void onStop() {
        this.app.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void pressUpDown(final Bundle bundle) {
        if (this.callRecords.size() != 0) {
            if (bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.UP.getCode() || bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_LEFT.getCode()) {
                --this.currentPosition;
                if (this.currentPosition == -1) {
                    this.currentPosition = 0;
                    this.isLongPress = false;
                }
                if (this.currentPosition < this.listView_record.getFirstVisiblePosition() + 1) {
                    this.listView_record.smoothScrollToPositionFromTop(this.currentPosition - 5, 0);
                }
            }
            else if (bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.DOWN.getCode() || bundle.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_RIGHT.getCode()) {
                ++this.currentPosition;
                if (this.currentPosition == this.callRecordAdapter.getCount()) {
                    this.currentPosition = this.callRecordAdapter.getCount() - 1;
                    this.isLongPress = false;
                }
                if (this.currentPosition > this.listView_record.getLastVisiblePosition() - 1) {
                    this.listView_record.smoothScrollToPositionFromTop(this.currentPosition, 0);
                }
            }
            else {
                if (bundle.getByte("idriver_enum") != Mainboard.EIdriverEnum.PRESS.getCode()) {
                    return;
                }
                if (LauncherApplication.isBlueConnectState) {
                    final String phone = this.callRecords.get(this.currentPosition).getPhone();
                    this.app.isComeFromRecord = true;
                    this.app.isPhoneNumFromRecord = true;
                    this.app.recorPhoneNumber = phone;
                    this.launcher.changeRightTo(new CallPhoneFragment());
                }
            }
            this.callRecordAdapter.setSelectItem(this.currentPosition);
            this.callRecordAdapter.notifyDataSetChanged();
        }
    }
    
    static class RecordHandler extends Handler
    {
        private WeakReference<CallAddressBookFragment> target;
        
        public RecordHandler(final CallAddressBookFragment callAddressBookFragment) {
            this.target = new WeakReference<CallAddressBookFragment>(callAddressBookFragment);
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
