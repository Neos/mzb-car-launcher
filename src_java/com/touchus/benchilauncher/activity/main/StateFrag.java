package com.touchus.benchilauncher.activity.main;

import android.support.v4.app.*;
import android.widget.*;
import android.net.wifi.*;
import android.net.*;
import java.io.*;
import android.view.*;
import android.os.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.utils.*;
import android.telephony.*;
import java.lang.ref.*;
import android.content.*;

public class StateFrag extends Fragment
{
    Launcher activity;
    private LauncherApplication app;
    private Runnable checkWifiSignRunnable;
    ImageView mBt;
    TextView mDate;
    ImageView mGps;
    ImageView mSdcard;
    ImageView mSignal;
    StateHandler mStateHandler;
    TextView mTime;
    ImageView mUsb3;
    UsbBroadcastReceiver mUsbReceiver;
    ImageView mWifi;
    private TelephonyManager telManager;
    View view;
    
    public StateFrag() {
        this.checkWifiSignRunnable = new Runnable() {
            @Override
            public void run() {
                StateFrag.this.mStateHandler.removeCallbacks(StateFrag.this.checkWifiSignRunnable);
                StateFrag.this.mStateHandler.postDelayed(StateFrag.this.checkWifiSignRunnable, 2000L);
                StateFrag.this.checkWifiSign();
            }
        };
        this.mStateHandler = new StateHandler(this);
    }
    
    private void checkWifiSign() {
        if (!isWifiConnect((Context)this.getActivity())) {
            this.setWifi(0);
            return;
        }
        final int rssi = ((WifiManager)this.getActivity().getSystemService("wifi")).getConnectionInfo().getRssi();
        if (rssi <= 0 && rssi >= -50) {
            this.setWifi(4);
            return;
        }
        if (rssi < -50 && rssi >= -70) {
            this.setWifi(3);
            return;
        }
        if (rssi < -70 && rssi >= -80) {
            this.setWifi(2);
            return;
        }
        if (rssi < -80 && rssi >= -100) {
            this.setWifi(1);
            return;
        }
        this.setWifi(0);
    }
    
    private void findID() {
        this.activity = (Launcher)this.getActivity();
        this.mSignal = (ImageView)this.view.findViewById(2131427650);
        this.mWifi = (ImageView)this.view.findViewById(2131427651);
        this.mBt = (ImageView)this.view.findViewById(2131427656);
        this.mGps = (ImageView)this.view.findViewById(2131427657);
        this.mSdcard = (ImageView)this.view.findViewById(2131427655);
        this.mUsb3 = (ImageView)this.view.findViewById(2131427654);
        this.mTime = (TextView)this.view.findViewById(2131427659);
        this.mDate = (TextView)this.view.findViewById(2131427660);
    }
    
    private void handlerMsg(final Message message) {
        switch (message.what) {
            default: {}
            case 1038: {
                this.setGPS();
            }
            case 1009: {
                this.setTimeInfo(message.getData());
            }
            case 1044: {
                this.setTimeColor(message.getData().getBoolean("isRed", false));
            }
        }
    }
    
    private void initDate() {
        this.stateSignal(this.mSignal, 0);
        this.stateWifi(this.mWifi, 0);
        this.setBt();
        this.checkUsb3();
        this.setGPS();
    }
    
    public static boolean isWifiConnect(final Context context) {
        return ((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(1).isConnected();
    }
    
    private void registerReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addDataScheme("file");
        this.mUsbReceiver = new UsbBroadcastReceiver();
        this.getActivity().registerReceiver((BroadcastReceiver)this.mUsbReceiver, intentFilter);
    }
    
    private void setTimeInfo(final Bundle bundle) {
        final int year = this.app.year;
        final int month = this.app.month;
        final int day = this.app.day;
        final int hour = this.app.hour;
        final int min = this.app.min;
        String s = new StringBuilder().append(month).toString();
        if (month < 10) {
            s = "0" + month;
        }
        String s2 = new StringBuilder().append(day).toString();
        if (day < 10) {
            s2 = "0" + day;
        }
        String s3 = new StringBuilder().append(min).toString();
        if (min < 10) {
            s3 = "0" + min;
        }
        this.mDate.setText((CharSequence)(String.valueOf(year) + "/" + s + "/" + s2));
        if (this.app.timeFormat == 24) {
            String s4 = new StringBuilder().append(hour).toString();
            if (hour < 10) {
                s4 = "0" + hour;
            }
            this.mTime.setText((CharSequence)(String.valueOf(s4) + ":" + s3));
            return;
        }
        String s5;
        if (hour <= 12) {
            s5 = new StringBuilder().append(hour).toString();
        }
        else {
            s5 = new StringBuilder().append(hour - 12).toString();
        }
        this.mTime.setText((CharSequence)(String.valueOf(s5) + ":" + s3));
    }
    
    private void stateImg(final ImageView imageView, final int n) {
        switch (n) {
            default: {}
            case 0: {
                imageView.setSelected(false);
            }
            case 1: {
                imageView.setSelected(true);
            }
        }
    }
    
    private void unregisterReceiver() {
        this.getActivity().unregisterReceiver((BroadcastReceiver)this.mUsbReceiver);
    }
    
    public void checkUsb3() {
        final File file = new File("/mnt/usbotg");
        if (file == null || (file.isDirectory() && (file.list() == null || file.list().length == 0))) {
            this.stateImg(this.mUsb3, 0);
        }
        else if (file != null && file.list() != null && file.list().length > 0) {
            this.stateImg(this.mUsb3, 1);
        }
        final File file2 = new File("storage/sdcard1");
        if (file2 == null || (file2.isDirectory() && (file2.list() == null || file2.list().length == 0))) {
            this.stateImg(this.mSdcard, 0);
        }
        else if (file2 != null && file2.list() != null && file2.list().length > 0) {
            this.stateImg(this.mSdcard, 1);
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.view = layoutInflater.inflate(2130903096, viewGroup, false);
        this.app = (LauncherApplication)this.getActivity().getApplication();
        this.findID();
        this.initDate();
        (this.telManager = (TelephonyManager)this.getActivity().getSystemService("phone")).listen((PhoneStateListener)new PhoneStateMonitor(), 257);
        return this.view;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.app.registerHandler(this.mStateHandler);
        this.mStateHandler.postDelayed(this.checkWifiSignRunnable, 2000L);
        this.registerReceiver();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.unregisterReceiver();
        this.app.registerHandler(this.mStateHandler);
        this.mStateHandler.removeCallbacks(this.checkWifiSignRunnable);
    }
    
    public void setBt() {
        if (this.getActivity() == null) {
            return;
        }
        if (LauncherApplication.isBlueConnectState) {
            this.stateImg(this.mBt, 1);
            return;
        }
        this.stateImg(this.mBt, 0);
    }
    
    public void setDate(final String text) {
        this.mDate.setText((CharSequence)text);
    }
    
    public void setGPS() {
        if (this.getActivity() == null) {
            return;
        }
        if (LauncherApplication.isGPSLocation) {
            this.stateImg(this.mGps, 1);
            return;
        }
        this.stateImg(this.mGps, 0);
    }
    
    public void setSignal(final int n) {
        this.stateSignal(this.mSignal, n);
    }
    
    public void setTime(final String text) {
        this.mTime.setText((CharSequence)text);
    }
    
    public void setTimeColor(final boolean b) {
        if (SysConst.DEBUG) {
            if (!this.app.service.iIsInAndroid) {
                this.mTime.setTextColor(-65536);
                return;
            }
            this.mTime.setTextColor(-1);
        }
    }
    
    public void setWifi(final int n) {
        this.stateWifi(this.mWifi, n);
    }
    
    public void stateSignal(final ImageView imageView, final int n) {
        if (MobileDataSwitcher.getMobileDataState((Context)this.activity)) {
            switch (n) {
                default: {}
                case 0: {
                    imageView.setImageResource(2130837934);
                }
                case 1: {
                    imageView.setImageResource(2130837935);
                }
                case 2: {
                    imageView.setImageResource(2130837937);
                }
                case 3: {
                    imageView.setImageResource(2130837939);
                }
                case 4: {
                    imageView.setImageResource(2130837941);
                }
            }
        }
        else {
            switch (n) {
                default: {}
                case 0: {
                    imageView.setImageResource(2130837934);
                }
                case 1: {
                    imageView.setImageResource(2130837936);
                }
                case 2: {
                    imageView.setImageResource(2130837938);
                }
                case 3: {
                    imageView.setImageResource(2130837940);
                }
                case 4: {
                    imageView.setImageResource(2130837942);
                }
            }
        }
    }
    
    public void stateWifi(final ImageView imageView, final int n) {
        switch (n) {
            default: {}
            case 0: {
                imageView.setImageResource(2130837958);
            }
            case 1: {
                imageView.setImageResource(2130837959);
            }
            case 2: {
                imageView.setImageResource(2130837960);
            }
            case 3: {
                imageView.setImageResource(2130837961);
            }
            case 4: {
                imageView.setImageResource(2130837962);
            }
        }
    }
    
    public class PhoneStateMonitor extends PhoneStateListener
    {
        public void onServiceStateChanged(final ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);
            if (StateFrag.this.getActivity() != null) {
                int n = 5;
                switch (serviceState.getState()) {
                    case 2: {
                        n = 5;
                        break;
                    }
                    case 0: {
                        n = 3;
                        break;
                    }
                    case 1: {
                        n = 5;
                        break;
                    }
                    case 3: {
                        n = 5;
                        break;
                    }
                }
                if (n != 3) {
                    StateFrag.this.setSignal(0);
                }
            }
        }
        
        public void onSignalStrengthsChanged(final SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            if (StateFrag.this.getActivity() == null) {
                return;
            }
            final int networkType = ((TelephonyManager)StateFrag.this.getActivity().getSystemService("phone")).getNetworkType();
            if (networkType == 1 || networkType == 2) {
                StateFrag.this.setSignal(1);
                return;
            }
            if (networkType == 0) {
                StateFrag.this.setSignal(0);
                return;
            }
            final int gsmSignalStrengthDbm = signalStrength.getGsmSignalStrengthDbm();
            if (gsmSignalStrengthDbm >= -75) {
                StateFrag.this.setSignal(4);
                return;
            }
            if (gsmSignalStrengthDbm >= -85) {
                StateFrag.this.setSignal(4);
                return;
            }
            if (gsmSignalStrengthDbm >= -95) {
                StateFrag.this.setSignal(3);
                return;
            }
            if (gsmSignalStrengthDbm >= -100) {
                StateFrag.this.setSignal(2);
                return;
            }
            StateFrag.this.setSignal(1);
        }
    }
    
    static class StateHandler extends Handler
    {
        public Bundle mBundle;
        private WeakReference<StateFrag> target;
        
        public StateHandler(final StateFrag stateFrag) {
            this.target = new WeakReference<StateFrag>(stateFrag);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
    
    class UsbBroadcastReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction().equals("android.intent.action.MEDIA_MOUNTED")) {
                final File file = new File("/mnt/usbotg");
                if (file != null && file.list() != null && file.list().length > 0) {
                    StateFrag.this.stateImg(StateFrag.this.mUsb3, 1);
                }
                final File file2 = new File("storage/sdcard1");
                if (file2 != null && file2.list() != null && file2.list().length > 0) {
                    StateFrag.this.stateImg(StateFrag.this.mSdcard, 1);
                }
            }
            else if (intent.getAction().equals("android.intent.action.MEDIA_EJECT")) {
                final File file3 = new File("/mnt/usbotg");
                if (file3 == null || (file3.isDirectory() && (file3.list() == null || file3.list().length == 0))) {
                    StateFrag.this.stateImg(StateFrag.this.mUsb3, 0);
                }
                final File file4 = new File("storage/sdcard1");
                if (file4 == null || (file4.isDirectory() && (file4.list() == null || file4.list().length == 0))) {
                    StateFrag.this.stateImg(StateFrag.this.mSdcard, 0);
                }
            }
        }
    }
}
