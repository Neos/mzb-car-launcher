package com.touchus.benchilauncher.utils;

import android.content.*;
import javax.crypto.*;
import android.util.*;
import java.security.*;
import android.annotation.*;
import java.io.*;

public class DesTool
{
    public static String DESDecrypt(String s, final Context context) {
        final SecretKey desKeyFromFile = getDESKeyFromFile(context);
        try {
            final Cipher instance = Cipher.getInstance("DES");
            instance.init(2, desKeyFromFile);
            s = new String(instance.doFinal(hexStringToByteArray(s)));
            return s;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static String DESEncrypt(String byteArrayToHexString, final Context context) {
        final SecretKey desKeyFromFile = getDESKeyFromFile(context);
        try {
            final Cipher instance = Cipher.getInstance("DES");
            instance.init(1, desKeyFromFile);
            byteArrayToHexString = byteArrayToHexString(instance.doFinal(byteArrayToHexString.getBytes()));
            return byteArrayToHexString;
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    public static String byteArrayToHexString(final byte[] array) {
        final StringBuilder sb = new StringBuilder("");
        if (array == null || array.length <= 0) {
            return null;
        }
        for (int i = 0; i < array.length; ++i) {
            final String hexString = Integer.toHexString(array[i] & 0xFF);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString();
    }
    
    private static byte charToByte(final char c) {
        return (byte)"0123456789ABCDEF".indexOf(c);
    }
    
    public static SecretKey getDESKeyFromFile(final Context context) {
        try {
            return (SecretKey)new ObjectInputStream(context.getResources().openRawResource(2131034113)).readObject();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static String getKey(final Context context) {
        return byteArrayToHexString(getDESKeyFromFile(context).getEncoded());
    }
    
    public static String getMD5String(final String s, Context instance) {
    Label_0057_Outer:
        while (true) {
            Context context = null;
            instance = null;
            while (true) {
                byte[] digest;
                int n;
                while (true) {
                    try {
                        final Context context2 = context = (instance = (Context)MessageDigest.getInstance("MD5"));
                        ((MessageDigest)context2).reset();
                        instance = context2;
                        context = context2;
                        ((MessageDigest)context2).update(s.getBytes("UTF-8"));
                        instance = context2;
                        digest = ((MessageDigest)instance).digest();
                        instance = (Context)new StringBuffer();
                        n = 0;
                        if (n >= digest.length) {
                            return ((StringBuffer)instance).toString().toUpperCase();
                        }
                    }
                    catch (NoSuchAlgorithmException ex) {
                        Log.e("", ex.getMessage());
                        continue Label_0057_Outer;
                    }
                    catch (UnsupportedEncodingException ex2) {
                        Log.e("", ex2.getMessage());
                        instance = context;
                        continue Label_0057_Outer;
                    }
                    break;
                }
                if (Integer.toHexString(digest[n] & 0xFF).length() == 1) {
                    ((StringBuffer)instance).append("0").append(Integer.toHexString(digest[n] & 0xFF));
                }
                else {
                    ((StringBuffer)instance).append(Integer.toHexString(digest[n] & 0xFF));
                }
                ++n;
                continue;
            }
        }
    }
    
    @SuppressLint({ "DefaultLocale" })
    public static byte[] hexStringToByteArray(String upperCase) {
        byte[] array;
        if (upperCase == null || upperCase.equals("")) {
            array = null;
        }
        else {
            upperCase = upperCase.toUpperCase();
            final int n = upperCase.length() / 2;
            final char[] charArray = upperCase.toCharArray();
            final byte[] array2 = new byte[n];
            int n2 = 0;
            while (true) {
                array = array2;
                if (n2 >= n) {
                    break;
                }
                final int n3 = n2 * 2;
                array2[n2] = (byte)(charToByte(charArray[n3]) << 4 | charToByte(charArray[n3 + 1]));
                ++n2;
            }
        }
        return array;
    }
    
    public static byte[] objectToByteArray(final Object o) {
        try {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(o);
            objectOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        }
        catch (IOException ex) {
            Log.e("", ex.getMessage());
            return null;
        }
    }
    
    public static String objectToHexString(final Object o) {
        return byteArrayToHexString(objectToByteArray(o));
    }
}
