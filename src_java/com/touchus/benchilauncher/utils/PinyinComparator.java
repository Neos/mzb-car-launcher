package com.touchus.benchilauncher.utils;

import java.util.*;
import com.touchus.benchilauncher.bean.*;

public class PinyinComparator implements Comparator<Person>
{
    @Override
    public int compare(final Person person, final Person person2) {
        if (person.getRemark() == null) {
            return 0;
        }
        if (person.getRemark().equals("@") || person2.getRemark().equals("#")) {
            return -1;
        }
        if (person.getRemark().equals("#") || person2.getRemark().equals("@")) {
            return 1;
        }
        return person.getRemark().compareTo(person2.getRemark());
    }
}
