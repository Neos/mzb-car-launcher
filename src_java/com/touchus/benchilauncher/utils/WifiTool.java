package com.touchus.benchilauncher.utils;

import android.content.*;
import com.touchus.benchilauncher.bean.*;
import android.net.*;
import android.net.wifi.*;
import java.util.*;
import android.util.*;
import android.text.*;

public class WifiTool
{
    public static WifiConfiguration CreateWifiInfo(final Context context, final String s, final String s2, final String s3) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
        final WifiConfiguration wifiConfiguration = new WifiConfiguration();
        wifiConfiguration.allowedAuthAlgorithms.clear();
        wifiConfiguration.allowedGroupCiphers.clear();
        wifiConfiguration.allowedKeyManagement.clear();
        wifiConfiguration.allowedPairwiseCiphers.clear();
        wifiConfiguration.allowedProtocols.clear();
        wifiConfiguration.SSID = "\"" + s + "\"";
        final WifiConfiguration isExsits = IsExsits(context, s);
        if (isExsits != null) {
            wifiManager.removeNetwork(isExsits.networkId);
        }
        if (s3.equals("1")) {
            wifiConfiguration.wepKeys[0] = "\"\"";
            wifiConfiguration.allowedKeyManagement.set(0);
            wifiConfiguration.wepTxKeyIndex = 0;
        }
        if (s3.equals("2")) {
            wifiConfiguration.hiddenSSID = true;
            wifiConfiguration.wepKeys[0] = "\"" + s2 + "\"";
            wifiConfiguration.allowedAuthAlgorithms.set(1);
            wifiConfiguration.allowedGroupCiphers.set(3);
            wifiConfiguration.allowedGroupCiphers.set(2);
            wifiConfiguration.allowedGroupCiphers.set(0);
            wifiConfiguration.allowedGroupCiphers.set(1);
            wifiConfiguration.allowedKeyManagement.set(0);
            wifiConfiguration.wepTxKeyIndex = 0;
        }
        if (s3.equals("3")) {
            wifiConfiguration.preSharedKey = "\"" + s2 + "\"";
            wifiConfiguration.hiddenSSID = true;
            wifiConfiguration.allowedAuthAlgorithms.set(0);
            wifiConfiguration.allowedGroupCiphers.set(2);
            wifiConfiguration.allowedKeyManagement.set(1);
            wifiConfiguration.allowedPairwiseCiphers.set(1);
            wifiConfiguration.allowedGroupCiphers.set(3);
            wifiConfiguration.allowedPairwiseCiphers.set(2);
            wifiConfiguration.status = 2;
        }
        return wifiConfiguration;
    }
    
    private static WifiConfiguration IsExsits(final Context context, final String s) {
        for (final WifiConfiguration wifiConfiguration : ((WifiManager)context.getSystemService("wifi")).getConfiguredNetworks()) {
            if (wifiConfiguration.SSID.equals("\"" + s + "\"")) {
                return wifiConfiguration;
            }
        }
        return null;
    }
    
    public static void closeWifiAp(final Context context) {
        ((WifiManager)context.getSystemService("wifi")).setWifiApEnabled((WifiConfiguration)null, false);
    }
    
    public static void connectToHadSaveConfigWifi(final Context context, final int n) {
        ((WifiManager)context.getSystemService("wifi")).enableNetwork(n, true);
    }
    
    public static boolean connectToSpecifyWifi(final Context context, final WifiConfiguration wifiConfiguration) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
        final boolean enableNetwork = wifiManager.enableNetwork(wifiManager.addNetwork(wifiConfiguration), true);
        if (enableNetwork) {
            wifiManager.saveConfiguration();
        }
        return enableNetwork;
    }
    
    public static void disConnectWifi(final Context context) {
        try {
            final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
            wifiManager.disableNetwork(wifiManager.getConnectionInfo().getNetworkId());
        }
        catch (Exception ex) {}
    }
    
    public static ArrayList<BeanWifi> getAllCanConectWifi(final Context context) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
        final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        final boolean connected = ((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(1).isConnected();
        wifiManager.startScan();
        final List scanResults = wifiManager.getScanResults();
        final ArrayList<BeanWifi> list = new ArrayList<BeanWifi>();
        for (int i = 0; i < scanResults.size(); ++i) {
            final ScanResult scanResult = scanResults.get(i);
            final BeanWifi beanWifi = new BeanWifi();
            beanWifi.setName(scanResult.SSID);
            final boolean iHadPwd = iHadPwd(scanResult.capabilities);
            beanWifi.setCapabilities(scanResult.capabilities);
            beanWifi.setLevel(getSignLevel(scanResult.level, iHadPwd));
            beanWifi.setPwd(iHadPwd);
            beanWifi.setState(getConnectState(connectionInfo.getSSID(), scanResult.SSID, connected));
            if (beanWifi.getState() == 1) {
                list.add(0, beanWifi);
            }
            else {
                list.add(beanWifi);
            }
        }
        for (int j = 0; j < list.size(); ++j) {
            BeanWifi beanWifi2 = list.get(j);
            int n;
            BeanWifi beanWifi4;
            for (int k = j + 1; k < list.size() - 1; k = n + 1, beanWifi2 = beanWifi4) {
                final BeanWifi beanWifi3 = list.get(k);
                n = k;
                beanWifi4 = beanWifi2;
                if (beanWifi3.getName().equals(beanWifi2.getName())) {
                    beanWifi4 = beanWifi2;
                    if (beanWifi3.getLevel() > beanWifi2.getLevel()) {
                        beanWifi4 = beanWifi3;
                        list.remove(j);
                        list.add(j, beanWifi3);
                    }
                    list.remove(k);
                    n = k - 1;
                }
            }
        }
        final int size = list.size();
        if (size < 5) {
            for (int l = 5; l > size; --l) {
                list.add(new BeanWifi());
            }
        }
        return list;
    }
    
    private static int getConnectState(final String s, final String s2, final boolean b) {
        Log.d("launcherlog", "ssid " + s + " ; resultSsid " + s2);
        if (s == null || !s.equals("\"" + s2 + "\"")) {
            return 3;
        }
        if (b) {
            return 1;
        }
        return 2;
    }
    
    public static WifiConfiguration getCurrentWifiApConfig(final Context context) {
        return ((WifiManager)context.getSystemService("wifi")).getWifiApConfiguration();
    }
    
    public static String getCurrentWifiPwdType(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "3";
        }
        if (s.equals("[ESS]") || s.equals("[WPS][ESS]")) {
            return "1";
        }
        if (s.equals("[WEP][ESS]")) {
            return "2";
        }
        return "3";
    }
    
    public static WifiConfiguration getHadSaveWifiConfig(final Context context, final String s) {
        return IsExsits(context, s);
    }
    
    private static int getSignLevel(int n, final boolean b) {
        int n2;
        if (b) {
            n2 = 4;
        }
        else {
            n2 = 0;
        }
        if (n <= 0 && n >= -50) {
            n = 4;
        }
        else if (n < -50 && n >= -70) {
            n = 3;
        }
        else if (n < -70 && n >= -80) {
            n = 2;
        }
        else {
            n = 1;
        }
        return n2 + n;
    }
    
    private static boolean iHadPwd(final String s) {
        return s != null && !s.equals("[ESS]") && s.length() > 10;
    }
    
    public static boolean iWifiApIsOpen(final Context context) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
        return wifiManager.getWifiApState() == 13 || wifiManager.getWifiApState() == 12;
    }
    
    public static boolean iWifiState(final Context context) {
        final int wifiState = ((WifiManager)context.getSystemService("wifi")).getWifiState();
        boolean b = true;
        if (wifiState == 1 || wifiState == 0 || wifiState == 4) {
            b = false;
        }
        return b;
    }
    
    private static String intToIp(final int n) {
        return String.valueOf(n & 0xFF) + "." + (n >> 8 & 0xFF) + "." + (n >> 16 & 0xFF) + "." + (n >> 24 & 0xFF);
    }
    
    public static boolean isIp(final Context context) {
        final int ipAddress = ((WifiManager)context.getSystemService("wifi")).getConnectionInfo().getIpAddress();
        return ipAddress != 0 && !intToIp(ipAddress).equals("0.0.0.0");
    }
    
    public static void openWifiAp(final Context context, final WifiConfiguration wifiConfiguration) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
        wifiManager.setWifiEnabled(false);
        wifiManager.setWifiApEnabled(wifiConfiguration, true);
    }
    
    public static void removeWifiConfig(final Context context, final int n) {
        try {
            final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
            wifiManager.removeNetwork(n);
            wifiManager.saveConfiguration();
        }
        catch (Exception ex) {}
    }
    
    public static void setDefaultWifiAp(final Context context) {
        updateWifiAp(context, context.getString(2131165212), "11111111");
    }
    
    public static void setWifiState(final Context context, final boolean wifiEnabled) {
        final WifiManager wifiManager = (WifiManager)context.getSystemService("wifi");
        if (wifiEnabled && iWifiApIsOpen(context)) {
            closeWifiAp(context);
        }
        wifiManager.setWifiEnabled(wifiEnabled);
    }
    
    public static void updateWifiAp(final Context context, final String ssid, final String preSharedKey) {
        final WifiConfiguration currentWifiApConfig = getCurrentWifiApConfig(context);
        currentWifiApConfig.allowedAuthAlgorithms.clear();
        currentWifiApConfig.allowedGroupCiphers.clear();
        currentWifiApConfig.allowedKeyManagement.clear();
        currentWifiApConfig.allowedPairwiseCiphers.clear();
        currentWifiApConfig.allowedProtocols.clear();
        currentWifiApConfig.SSID = ssid;
        if (!TextUtils.isEmpty((CharSequence)preSharedKey)) {
            currentWifiApConfig.allowedKeyManagement.set(4);
            currentWifiApConfig.allowedAuthAlgorithms.set(0);
            currentWifiApConfig.preSharedKey = preSharedKey;
        }
        else {
            currentWifiApConfig.wepKeys[0] = "\"\"";
            currentWifiApConfig.allowedKeyManagement.set(0);
            currentWifiApConfig.wepTxKeyIndex = 0;
        }
        ((WifiManager)context.getSystemService("wifi")).setWifiApConfiguration(currentWifiApConfig);
    }
}
