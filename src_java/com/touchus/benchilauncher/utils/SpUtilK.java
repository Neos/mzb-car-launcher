package com.touchus.benchilauncher.utils;

import android.content.*;
import com.touchus.benchilauncher.*;

public class SpUtilK
{
    private SharedPreferences$Editor mEditor;
    private SharedPreferences mSp;
    
    public SpUtilK(final Context context) {
        this.mSp = context.getSharedPreferences("launcher", 0);
        this.mEditor = this.mSp.edit();
    }
    
    public SpUtilK(final Context context, final String s) {
        this.mSp = context.getSharedPreferences(s, 0);
        this.mEditor = this.mSp.edit();
    }
    
    public SpUtilK(final String s) {
        this.mSp = LauncherApplication.getContext().getSharedPreferences(s, 0);
        this.mEditor = this.mSp.edit();
    }
    
    public boolean getBoolean(final String s, final boolean b) {
        return this.mSp.getBoolean(s, b);
    }
    
    public int getInt(final String s, final int n) {
        return this.mSp.getInt(s, n);
    }
    
    public String getString(final String s, final String s2) {
        return this.mSp.getString(s, s2);
    }
    
    public void putBoolean(final String s, final boolean b) {
        this.mEditor.putBoolean(s, b);
        this.mEditor.commit();
    }
    
    public void putInt(final String s, final int n) {
        this.mEditor.putInt(s, n);
        this.mEditor.commit();
    }
    
    public void putString(final String s, final String s2) {
        this.mEditor.putString(s, s2);
        this.mEditor.commit();
    }
}
