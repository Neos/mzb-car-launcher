package com.touchus.benchilauncher.utils;

public class VideoType
{
    private static String[] videoTpye;
    
    static {
        VideoType.videoTpye = new String[] { ".rm", ".rmvb", ".avi", ".pmeg", ".wmv", ".3gp", ".mp4", ".dat", ".vob", ".flv" };
    }
    
    public static boolean isVideo(final String s) {
        final String[] videoTpye = VideoType.videoTpye;
        for (int length = videoTpye.length, i = 0; i < length; ++i) {
            if (videoTpye[i].equals(s.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}
