package com.touchus.benchilauncher.utils;

import java.io.*;

public class QuotedPrintable
{
    private static final byte CR = 13;
    private static final byte EQUALS = 61;
    private static final byte LF = 10;
    private static final byte LIT_END = 126;
    private static final byte LIT_START = 33;
    private static final int MAX_LINE_LENGTH = 76;
    private static final byte TAB = 9;
    private static int mCurrentLineLength;
    
    static {
        QuotedPrintable.mCurrentLineLength = 0;
    }
    
    private static void checkLineLength(final int mCurrentLineLength, final StringBuilder sb) {
        if (QuotedPrintable.mCurrentLineLength + mCurrentLineLength > 75) {
            sb.append("=/r/n");
            QuotedPrintable.mCurrentLineLength = mCurrentLineLength;
            return;
        }
        QuotedPrintable.mCurrentLineLength += mCurrentLineLength;
    }
    
    public static int decode(final byte[] array) {
        final int length = array.length;
        int i = 0;
        int n = 0;
    Label_0056_Outer:
        while (i < length) {
            while (true) {
                Label_0156: {
                    if (array[i] != 61 || length - i <= 2) {
                        break Label_0156;
                    }
                    if (array[i + 1] == 13 && array[i + 2] == 10) {
                        i += 2;
                    }
                    else {
                        if (!isHexDigit(array[i + 1]) || !isHexDigit(array[i + 2])) {
                            System.err.println("decode: Invalid sequence = " + array[i + 1] + array[i + 2]);
                            break Label_0156;
                        }
                        final int n2 = n + 1;
                        array[n] = (byte)(getHexValue(array[i + 1]) * 16 + getHexValue(array[i + 2]));
                        i += 2;
                        n = n2;
                    }
                    ++i;
                    continue Label_0056_Outer;
                }
                if ((array[i] >= 32 && array[i] <= 127) || array[i] == 9 || array[i] == 13 || array[i] == 10) {
                    final int n3 = n + 1;
                    array[n] = array[i];
                    n = n3;
                }
                continue;
            }
        }
        return n;
    }
    
    public static String decode(final byte[] array, String s) {
        final int decode = decode(array);
        try {
            s = new String(array, 0, decode, s);
            return s;
        }
        catch (UnsupportedEncodingException ex) {
            return new String(array, 0, decode);
        }
    }
    
    public static String encode(String o, final String s) {
        if (o == null) {
            return null;
        }
        try {
            o = ((String)o).getBytes(s);
            return encode((byte[])o);
        }
        catch (UnsupportedEncodingException ex) {
            o = ((String)o).getBytes();
            return encode((byte[])o);
        }
    }
    
    public static String encode(final byte[] array) {
        if (array == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        QuotedPrintable.mCurrentLineLength = 0;
        for (int i = 0; i < array.length; ++i) {
            final byte b = array[i];
            if (b >= 33 && b <= 126 && b != 61) {
                checkLineLength(1, sb);
                sb.append((char)b);
            }
            else {
                checkLineLength(3, sb);
                sb.append('=');
                sb.append(String.format("%02X", b));
            }
        }
        return sb.toString();
    }
    
    private static byte getHexValue(final byte b) {
        return (byte)Character.digit((char)b, 16);
    }
    
    private static boolean isHexDigit(final byte b) {
        return (b >= 48 && b <= 57) || (b >= 65 && b <= 70);
    }
}
