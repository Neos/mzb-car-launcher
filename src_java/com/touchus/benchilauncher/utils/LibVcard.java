package com.touchus.benchilauncher.utils;

import java.util.*;
import android.util.*;
import java.util.regex.*;

public class LibVcard
{
    boolean isgetend;
    boolean isgethead;
    private List<VcardUnit> list;
    private VcardUnit unit;
    
    public LibVcard(final byte[] array) throws Exception {
        this.list = new ArrayList<VcardUnit>();
        this.unit = null;
        this.isgethead = false;
        this.isgetend = false;
        if (array != null) {
            int n = 0;
            int n2;
            for (int i = 0; i < array.length; ++i, n = n2) {
                n2 = n;
                if (array[i] == 10) {
                    n2 = n;
                    if (i - 1 >= 0) {
                        n2 = n;
                        if (i - 1 < array.length) {
                            n2 = n;
                            if (array[i - 1] == 13) {
                                n2 = n;
                                if (i - 2 >= 0) {
                                    n2 = n;
                                    if (i - 2 < array.length) {
                                        n2 = n;
                                        if (array[i - 2] != 61) {
                                            final int n3 = i - 2;
                                            final byte[] array2 = new byte[n3 - n + 1];
                                            for (int n4 = n, n5 = 0; n4 <= n3 && n5 < array2.length; ++n4, ++n5) {
                                                array2[n5] = array[n4];
                                            }
                                            this.processLine(array2);
                                            n2 = n3 + 3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void processLine(final byte[] array) throws Exception {
        final String s = new String(array);
        Log.e("VcardUnit", s);
        if (!this.isgethead) {
            if ("BEGIN:VCARD".equals(s)) {
                this.unit = new VcardUnit();
                this.isgethead = true;
            }
        }
        else if (!this.isgetend) {
            if ("END:VCARD".equals(s)) {
                if (this.unit != null && this.unit.isvalid()) {
                    this.list.add(this.unit);
                }
                this.isgethead = false;
                this.isgetend = false;
                return;
            }
            if (s.startsWith("N") || s.startsWith("FN")) {
                final Matcher matcher = Pattern.compile("CHARSET=([0-9A-Za-z\\-]+)").matcher(s);
                String group = "UTF-8";
                if (matcher.find()) {
                    group = matcher.group(1);
                }
                final String s2 = new String(array, group);
                final String substring = s2.substring(s2.indexOf(":") + 1);
                String name;
                if (s2.indexOf("ENCODING=QUOTED-PRINTABLE") != -1) {
                    final String[] split = substring.split(";");
                    String string = substring;
                    if (split != null) {
                        string = substring;
                        if (split.length >= 3) {
                            string = String.valueOf(split[0]) + split[2] + split[1];
                        }
                    }
                    name = QuotedPrintable.decode(string.replace(";", "").getBytes(), group);
                }
                else {
                    final String[] split2 = substring.split(";");
                    String string2 = substring;
                    if (split2 != null) {
                        string2 = substring;
                        if (split2.length >= 3) {
                            string2 = String.valueOf(split2[0]) + split2[2] + split2[1];
                        }
                    }
                    name = string2.replace(";", "");
                }
                this.unit.setName(name);
            }
            if (s.indexOf("TEL") == 0) {
                final Matcher matcher2 = Pattern.compile("TEL[;a-zA-Z\\=\\-]*:([0-9\\-\\+ ]+)").matcher(s);
                if (matcher2.find()) {
                    String name2;
                    final String s3 = name2 = matcher2.group(1).replace("-", "").replace(" ", "");
                    if (s3.startsWith("+86")) {
                        name2 = s3.substring(3);
                    }
                    this.unit.appendNumber(name2);
                    if (this.unit.getName() == null || "".equals(this.unit.getName())) {
                        this.unit.setName(name2);
                    }
                }
            }
            if (s.indexOf("X-IRMC-CALL-DATETIME") == 0) {
                final String substring2 = s.substring(s.indexOf(";") + 1, s.indexOf(":"));
                if ("MISSED".equals(substring2)) {
                    this.unit.setFlag(1414);
                }
                else if ("DIALED".equals(substring2)) {
                    this.unit.setFlag(1413);
                }
                else {
                    this.unit.setFlag(1412);
                }
                final String substring3 = s.substring(s.indexOf(":") + 1, s.lastIndexOf("T"));
                final String substring4 = s.substring(s.lastIndexOf("T") + 1);
                this.unit.setRemark(substring3.substring(0, 4) + "/" + substring3.substring(4, 6) + "/" + substring3.substring(6) + " " + substring4.substring(0, 2) + ":" + substring4.substring(2, 4) + ":" + substring4.substring(4));
            }
        }
    }
    
    public List<VcardUnit> getList() {
        return this.list;
    }
}
