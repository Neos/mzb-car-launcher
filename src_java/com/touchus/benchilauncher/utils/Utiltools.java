package com.touchus.benchilauncher.utils;

import android.content.*;
import android.content.pm.*;
import java.util.*;
import android.os.*;

public class Utiltools
{
    public static int getResId(final Context context, final String s) {
        try {
            return context.getResources().getIdentifier(s, "drawable", context.getPackageName());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return 2130837897;
        }
    }
    
    public static boolean isAvilible(final Context context, final String s) {
        final List installedPackages = context.getPackageManager().getInstalledPackages(0);
        final ArrayList<String> list = new ArrayList<String>();
        if (installedPackages != null) {
            for (int i = 0; i < installedPackages.size(); ++i) {
                list.add(installedPackages.get(i).packageName);
            }
        }
        return list.contains(s);
    }
    
    public static void onlineOtaUpdate() {
        SystemProperties.set("ctl.start", "ota_update");
    }
    
    public static void resetSDCard() {
        SystemProperties.set("ctl.start", "mkfs");
    }
    
    public static void stopKernelMCU() {
        SystemProperties.set("ctl.stop", "kernel_mcu");
    }
    
    public static void updateLauncherAPK() {
        SystemProperties.set("ctl.start", "update_apk");
    }
}
