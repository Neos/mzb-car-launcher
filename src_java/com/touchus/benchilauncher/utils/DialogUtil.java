package com.touchus.benchilauncher.utils;

import android.app.*;
import android.view.*;

public class DialogUtil
{
    public static void setDialogLocation(final Dialog dialog, final int x, final int y) {
        final Window window = dialog.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        attributes.x = x;
        attributes.y = y;
        window.setAttributes(attributes);
    }
}
