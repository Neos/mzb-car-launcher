package com.touchus.benchilauncher.utils;

import android.content.*;
import android.widget.*;
import android.view.*;

public class ToastTool
{
    public static void showBigShortToast(final Context context, final int n) {
        final View inflate = LayoutInflater.from(context).inflate(2130903106, (ViewGroup)Toast.makeText(context, (CharSequence)"", 0).getView().findViewById(2131427681));
        ((TextView)inflate.findViewById(2131427682)).setText((CharSequence)context.getString(n));
        final Toast toast = new Toast(context);
        toast.setGravity(17, 200, 10);
        toast.setDuration(0);
        toast.setView(inflate);
        toast.show();
    }
    
    public static void showBigShortToast(final Context context, final String text) {
        final View inflate = LayoutInflater.from(context).inflate(2130903106, (ViewGroup)Toast.makeText(context, (CharSequence)"", 0).getView().findViewById(2131427681));
        ((TextView)inflate.findViewById(2131427682)).setText((CharSequence)text);
        final Toast toast = new Toast(context);
        toast.setGravity(17, 200, 10);
        toast.setDuration(0);
        toast.setView(inflate);
        toast.show();
    }
    
    public static void showLongToast(final Context context, final String s) {
        try {
            final Toast text = Toast.makeText(context, (CharSequence)s, 1);
            text.setGravity(17, 0, 30);
            text.show();
        }
        catch (Exception ex) {}
    }
    
    public static void showShortToast(final Context context, final String s) {
        try {
            final Toast text = Toast.makeText(context, (CharSequence)s, 0);
            text.setGravity(17, 0, 30);
            text.show();
        }
        catch (Exception ex) {}
    }
}
