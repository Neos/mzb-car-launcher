package com.touchus.benchilauncher.utils;

import java.util.*;

public class VcardUnit
{
    private int flag;
    private String name;
    private List<String> numbers;
    private String remark;
    
    public VcardUnit() {
        this.numbers = new ArrayList<String>();
    }
    
    public void appendNumber(final String s) {
        if (s != null) {
            this.numbers.add(s);
        }
    }
    
    public int getFlag() {
        return this.flag;
    }
    
    public String getName() {
        return this.name;
    }
    
    public List<String> getNumbers() {
        return this.numbers;
    }
    
    public String getRemark() {
        return this.remark;
    }
    
    public boolean isvalid() {
        return this.name != null && this.numbers.size() > 0;
    }
    
    public void setFlag(final int flag) {
        this.flag = flag;
    }
    
    public void setName(final String name) {
        if (name != null) {
            this.name = name;
        }
    }
    
    public void setNumbers(final List<String> numbers) {
        this.numbers = numbers;
    }
    
    public void setRemark(final String remark) {
        this.remark = remark;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VcardUnit [name=");
        sb.append(this.name);
        sb.append(", flag=");
        sb.append(this.flag);
        sb.append(", remark=");
        sb.append(this.remark);
        sb.append(", numbers=");
        sb.append(this.numbers);
        sb.append("]");
        return sb.toString();
    }
}
