package com.touchus.benchilauncher.utils;

import com.touchus.benchilauncher.bean.*;
import android.util.*;
import java.io.*;
import org.xmlpull.v1.*;
import java.util.*;

public class PersonParse
{
    public static String getFileName(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: astore_2       
        //     3: new             Ljava/io/File;
        //     6: dup            
        //     7: aload_0        
        //     8: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    11: astore_1       
        //    12: aconst_null    
        //    13: astore_3       
        //    14: aconst_null    
        //    15: astore_0       
        //    16: new             Ljava/io/FileInputStream;
        //    19: dup            
        //    20: aload_1        
        //    21: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    24: astore_1       
        //    25: aload_2        
        //    26: astore_0       
        //    27: aload_1        
        //    28: ifnull          62
        //    31: new             Ljava/io/BufferedReader;
        //    34: dup            
        //    35: new             Ljava/io/InputStreamReader;
        //    38: dup            
        //    39: aload_1        
        //    40: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    43: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    46: astore_3       
        //    47: aload_2        
        //    48: astore_0       
        //    49: aload_3        
        //    50: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    53: astore_2       
        //    54: aload_2        
        //    55: ifnonnull       72
        //    58: aload_1        
        //    59: invokevirtual   java/io/InputStream.close:()V
        //    62: aload_1        
        //    63: ifnull          70
        //    66: aload_1        
        //    67: invokevirtual   java/io/InputStream.close:()V
        //    70: aload_0        
        //    71: areturn        
        //    72: new             Ljava/lang/StringBuilder;
        //    75: dup            
        //    76: aload_0        
        //    77: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    80: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    83: aload_2        
        //    84: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    87: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    90: astore_0       
        //    91: goto            49
        //    94: astore_1       
        //    95: aload_0        
        //    96: ifnull          103
        //    99: aload_0        
        //   100: invokevirtual   java/io/InputStream.close:()V
        //   103: aconst_null    
        //   104: areturn        
        //   105: astore_0       
        //   106: aload_0        
        //   107: invokevirtual   java/io/IOException.printStackTrace:()V
        //   110: goto            103
        //   113: astore_0       
        //   114: aload_3        
        //   115: astore_1       
        //   116: aload_1        
        //   117: ifnull          124
        //   120: aload_1        
        //   121: invokevirtual   java/io/InputStream.close:()V
        //   124: aload_0        
        //   125: athrow         
        //   126: astore_1       
        //   127: aload_1        
        //   128: invokevirtual   java/io/IOException.printStackTrace:()V
        //   131: goto            124
        //   134: astore_1       
        //   135: aload_1        
        //   136: invokevirtual   java/io/IOException.printStackTrace:()V
        //   139: goto            70
        //   142: astore_0       
        //   143: goto            116
        //   146: astore_0       
        //   147: aload_1        
        //   148: astore_0       
        //   149: goto            95
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  16     25     94     95     Ljava/lang/Exception;
        //  16     25     113    116    Any
        //  31     47     146    152    Ljava/lang/Exception;
        //  31     47     142    146    Any
        //  49     54     146    152    Ljava/lang/Exception;
        //  49     54     142    146    Any
        //  58     62     146    152    Ljava/lang/Exception;
        //  58     62     142    146    Any
        //  66     70     134    142    Ljava/io/IOException;
        //  72     91     146    152    Ljava/lang/Exception;
        //  72     91     142    146    Any
        //  99     103    105    113    Ljava/io/IOException;
        //  120    124    126    134    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0049:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static ArrayList<Person> getPersons(final InputStream inputStream) throws Exception {
        ArrayList<Person> list = null;
        Person person = null;
        final XmlPullParser pullParser = Xml.newPullParser();
        pullParser.setInput(inputStream, "UTF-8");
        int i = pullParser.getEventType();
    Label_0107_Outer:
        while (i != 1) {
            Person person2 = person;
            ArrayList<Person> list2 = list;
            while (true) {
                switch (i) {
                    default: {
                        list2 = list;
                        person2 = person;
                        break Label_0107;
                    }
                    case 0: {
                        list2 = new ArrayList<Person>();
                        person2 = person;
                    }
                    case 1: {
                        i = pullParser.next();
                        person = person2;
                        list = list2;
                        continue Label_0107_Outer;
                    }
                    case 2: {
                        if ("person".equals(pullParser.getName())) {
                            final long longValue = Long.valueOf(pullParser.getAttributeValue(0));
                            person = new Person();
                            person.setId(String.valueOf(longValue));
                        }
                        if ("name".equals(pullParser.getName())) {
                            person.setName(pullParser.nextText());
                        }
                        if ("phone".equals(pullParser.getName())) {
                            person.setPhone(new String(pullParser.nextText()));
                        }
                        if ("flag".equals(pullParser.getName())) {
                            person.setFlag(Integer.valueOf(pullParser.nextText()));
                        }
                        person2 = person;
                        list2 = list;
                        if ("remark".equals(pullParser.getName())) {
                            person.setRemark(new String(pullParser.nextText()));
                            person2 = person;
                            list2 = list;
                        }
                        continue;
                    }
                    case 3: {
                        person2 = person;
                        list2 = list;
                        if ("person".equals(pullParser.getName())) {
                            list.add(person);
                            person2 = null;
                            list2 = list;
                        }
                        continue;
                    }
                }
                break;
            }
        }
        Log.d("onPhoneBookList::", "getHistoryList::" + list.size());
        return list;
    }
    
    public static void save(final ArrayList<Person> list, final OutputStream outputStream) throws Exception {
        try {
            final XmlSerializer serializer = Xml.newSerializer();
            serializer.setOutput(outputStream, "UTF-8");
            serializer.startDocument("UTF-8", true);
            serializer.startTag((String)null, "persons");
            for (final Person person : list) {
                Log.e("onPhoneBookList", "onPhoneBookList persons ===== " + list);
                serializer.startTag((String)null, "person");
                serializer.attribute((String)null, "id", person.getId().toString());
                serializer.startTag((String)null, "name");
                serializer.text(person.getName());
                serializer.endTag((String)null, "name");
                serializer.startTag((String)null, "phone");
                serializer.text(person.getPhone());
                serializer.endTag((String)null, "phone");
                serializer.startTag((String)null, "flag");
                serializer.text(new StringBuilder(String.valueOf(person.getFlag())).toString());
                serializer.endTag((String)null, "flag");
                serializer.startTag((String)null, "remark");
                serializer.text(person.getRemark());
                serializer.endTag((String)null, "remark");
                serializer.endTag((String)null, "person");
            }
            serializer.endTag((String)null, "persons");
            serializer.endDocument();
            outputStream.flush();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
}
