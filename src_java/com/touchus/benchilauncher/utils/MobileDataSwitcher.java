package com.touchus.benchilauncher.utils;

import android.content.*;
import android.net.*;

public class MobileDataSwitcher
{
    public static boolean getMobileDataState(final Context context) {
        try {
            return Boolean.valueOf(((ConnectivityManager)context.getSystemService("connectivity")).getMobileDataEnabled());
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    public static void setMobileData(final Context context, final boolean mobileDataEnabled) {
        ((ConnectivityManager)context.getSystemService("connectivity")).setMobileDataEnabled(mobileDataEnabled);
    }
}
