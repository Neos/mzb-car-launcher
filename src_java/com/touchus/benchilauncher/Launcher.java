package com.touchus.benchilauncher;

import com.touchus.benchilauncher.inface.*;
import java.util.*;
import com.touchus.benchilauncher.activity.main.*;
import com.touchus.benchilauncher.activity.main.right.Menu.*;
import com.backaudio.android.driver.*;
import com.touchus.benchilauncher.service.*;
import com.touchus.benchilauncher.utils.*;
import android.app.*;
import com.touchus.benchilauncher.activity.main.right.call.*;
import android.util.*;
import android.widget.*;
import com.touchus.publicutils.utils.*;
import com.touchus.benchilauncher.activity.main.left.*;
import android.support.v4.app.*;
import android.content.*;
import android.view.*;
import com.touchus.benchilauncher.fragment.*;
import com.touchus.benchilauncher.views.*;
import com.touchus.benchilauncher.base.*;
import android.os.*;
import java.lang.ref.*;

public class Launcher extends FragmentActivity implements IDoorStateParent
{
    private LauncherApplication app;
    private ArrayList<Fragment> backList;
    private final ServiceConnection blueconn;
    private ServiceConnection conn;
    public long lastChangeTimne;
    public byte lastChangeView;
    ButtomBar mButtomFrag;
    public ButtomSlide mButtomSlide;
    private Mhandler mHandler;
    public boolean mIsButtomShowed;
    public boolean mIsTopShowed;
    public LeftStateSlide mLeftStateSlide;
    public boolean mMenuShow;
    StateFrag mStateFra;
    public ButtomSlide mTopSlide;
    public FragmentManager manager;
    public MenuFragment menuFragment;
    public FrameLayout playVideoLayout;
    private FrameLayout rightLayout;
    private Fragment yibiaoFragment;
    private LinearLayout yibiaoLayout;
    
    public Launcher() {
        this.manager = this.getSupportFragmentManager();
        this.mIsButtomShowed = true;
        this.mIsTopShowed = true;
        this.mMenuShow = true;
        this.backList = new ArrayList<Fragment>();
        this.yibiaoFragment = new YiBiaoFragment();
        this.lastChangeView = Mainboard.EControlSource.MENU.getCode();
        this.lastChangeTimne = -1L;
        this.conn = (ServiceConnection)new ServiceConnection() {
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                Launcher.this.app.service = ((MainService.MainBinder)binder).getService();
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                Launcher.this.app.service = null;
            }
        };
        this.blueconn = (ServiceConnection)new ServiceConnection() {
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                Launcher.this.app.btservice = ((BluetoothService.PlayerBinder)binder).getService();
                Launcher.this.initBluetooth();
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                Launcher.this.app.btservice = null;
            }
        };
        this.mHandler = new Mhandler(this);
    }
    
    private void artSound(final int n) {
        if (this.app.currentDialog4 != null) {
            return;
        }
        final SoundDialog.Builder builder = new SoundDialog.Builder((Context)this);
        final SoundDialog create = builder.create(n);
        DialogUtil.setDialogLocation(create, 0, -180);
        create.show();
        ((SoundDialog)(this.app.currentDialog4 = create)).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                builder.unregisterHandlerr();
                Launcher.this.app.currentDialog4 = null;
            }
        });
    }
    
    private void changeRightLayoutFragmentByHistory(final Fragment fragment) {
        this.changeFragment(2131427331, fragment);
    }
    
    private void changeViewFromIdriverCar(final Bundle bundle) {
        final byte byte1 = bundle.getByte("idriver_enum");
        if (byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
            this.app.dismissDialog();
        }
        if (this.lastChangeView != byte1 || System.currentTimeMillis() - this.lastChangeTimne >= 2000L) {
            this.lastChangeView = byte1;
            this.lastChangeTimne = System.currentTimeMillis();
            if (byte1 == Mainboard.EIdriverEnum.BT.getCode() || byte1 == Mainboard.EIdriverEnum.PICK_UP.getCode()) {
                if (!(this.getCurrentFragment() instanceof CallPhoneFragment) || !this.app.isCallDialog) {
                    this.changeRightTo(new CallPhoneFragment());
                    this.app.dismissDialog();
                }
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.HOME.getCode()) {
                    this.handHomeAction();
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.NAVI.getCode()) {
                    this.app.startNavi();
                }
            }
        }
    }
    
    private void handlerMsg(final Message message) {
        if (!this.isDestroyed()) {
            Log.e("msg.what", "::" + message.what);
            if (message.what == 6001) {
                this.changeViewFromIdriverCar(message.getData());
                return;
            }
            if (message.what == 6002) {
                final StateFrag stateFrag = (StateFrag)this.getSupportFragmentManager().findFragmentById(2131427343);
                if (stateFrag != null) {
                    stateFrag.setBt();
                }
            }
            else if (message.what == 1013) {
                if (!(this.getCurrentFragment() instanceof CallPhoneFragment)) {
                    this.changeRightTo(new CallPhoneFragment());
                }
            }
            else {
                if (message.what == 1009) {
                    this.setLeftClockTime(message.getData());
                    return;
                }
                if (message.what == 1121 || message.what == 1131) {
                    this.app.mHomeAndBackEnable = true;
                    this.handleBackAction();
                    Toast.makeText((Context)this, (CharSequence)this.getString(2131165285), 0).show();
                    return;
                }
                if (message.what == 1024) {
                    this.handHomeAction();
                    return;
                }
                if (message.what == 1030) {
                    this.handHomeAction();
                    this.menuFragment.selectMenu(1);
                    return;
                }
                if (message.what == 1036) {
                    UtilTools.sendKeyeventToSystem(3);
                    this.goTo((int)message.obj);
                    return;
                }
                if (message.what == 1040) {
                    this.artSound(1);
                    return;
                }
                if (message.what == 1046) {
                    this.showNaviView();
                }
            }
        }
    }
    
    private void inflate() {
        this.changeFragment(2131427339, this.yibiaoFragment);
        this.showFragment(false, this.yibiaoFragment);
        this.changeFragment(2131427334, new RunningFrag());
        this.changeFragment(2131427335, new LeftClockFrag());
        this.changeFragment(2131427336, new LightFrag());
        this.changeFragment(2131427337, new CardoorFrag());
        this.changeFragment(2131427343, this.mStateFra);
        this.changeFragment(2131427341, this.mButtomFrag);
        this.changeFragment(2131427331, this.menuFragment);
        this.hideButtom();
    }
    
    private void initBluetooth() {
        try {
            new Thread(new InitBluetoothThread()).start();
            this.app.btservice.accFlag = false;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void initObject() {
        this.menuFragment = new MenuFragment();
        this.mStateFra = new StateFrag();
        this.mButtomFrag = new ButtomBar();
        this.mButtomSlide = (ButtomSlide)this.findViewById(2131427340);
        this.mTopSlide = (ButtomSlide)this.findViewById(2131427342);
        this.mLeftStateSlide = (LeftStateSlide)this.findViewById(2131427333);
        this.rightLayout = (FrameLayout)this.findViewById(2131427331);
        this.yibiaoLayout = (LinearLayout)this.findViewById(2131427339);
        this.playVideoLayout = (FrameLayout)this.findViewById(2131427338);
        this.yibiaoLayout.setVisibility(4);
        this.app.iIsYibiaoShowing = false;
        this.playVideoLayout.setVisibility(4);
    }
    
    private void removePlayVideo() {
        try {
            this.showButtom();
            this.showTop();
            final FragmentTransaction beginTransaction = this.getSupportFragmentManager().beginTransaction();
            final FragmentVideoPlay fragmentVideoPlay = (FragmentVideoPlay)this.getSupportFragmentManager().findFragmentById(2131427338);
            if (fragmentVideoPlay != null) {
                beginTransaction.remove(fragmentVideoPlay);
                beginTransaction.commit();
            }
            this.app.sendMessage(1031, null);
        }
        catch (Exception ex) {}
    }
    
    private void setLeftClockTime(final Bundle bundle) {
        final Fragment fragmentById = this.getSupportFragmentManager().findFragmentById(2131427335);
        if (fragmentById instanceof LeftClockFrag) {
            ((LeftClockFrag)fragmentById).updateCurrentTime();
        }
    }
    
    private void startBluetoothService() {
        if (this.app.btservice == null) {
            final Intent intent = new Intent((Context)this, (Class)BluetoothService.class);
            this.startService(intent);
            this.bindService(intent, this.blueconn, 1);
            return;
        }
        this.initBluetooth();
    }
    
    public void changeCenterTo() {
        this.yibiaoLayout.setVisibility(0);
        this.showFragment(true, this.yibiaoFragment);
        this.app.iIsYibiaoShowing = true;
        this.showButtom();
        this.rightLayout.setVisibility(4);
        this.mLeftStateSlide.setVisibility(4);
    }
    
    public void changeFragment(final int n, final Fragment fragment) {
        if (this.isDestroyed()) {
            return;
        }
        final FragmentTransaction beginTransaction = this.manager.beginTransaction();
        beginTransaction.replace(n, fragment);
        beginTransaction.commitAllowingStateLoss();
    }
    
    public void changeRightTo(final Fragment fragment) {
        if (this.yibiaoLayout.getVisibility() == 0) {
            this.rightLayout.setVisibility(0);
            this.mLeftStateSlide.setVisibility(0);
            this.yibiaoLayout.setVisibility(4);
            this.showFragment(false, this.yibiaoFragment);
            this.app.iIsYibiaoShowing = false;
        }
        if (this.playVideoLayout.getVisibility() == 0) {
            this.removePlayVideo();
            this.rightLayout.setVisibility(0);
            this.mLeftStateSlide.setVisibility(0);
            this.playVideoLayout.setVisibility(8);
        }
        if (!(fragment instanceof MenuFragment)) {
            Label_0132: {
                if (!(fragment instanceof CallPhoneFragment)) {
                    break Label_0132;
                }
                if (this.backList != null) {
                    this.backList.clear();
                }
                if (!(this.getCurrentFragment() instanceof CallPhoneFragment)) {
                    break Label_0132;
                }
                return;
            }
            if (fragment instanceof FragmentMediaMenu) {
                if (this.backList != null) {
                    this.backList.clear();
                }
                if (this.getCurrentFragment() instanceof FragmentMediaMenu) {
                    return;
                }
            }
            this.backList.add(fragment);
            this.showButtom();
        }
        else {
            this.backList.clear();
            this.hideButtom();
        }
        this.changeFragment(2131427331, fragment);
    }
    
    public void changeToPlayVideo(final FragmentVideoPlay fragmentVideoPlay) {
        final FragmentTransaction beginTransaction = this.getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(2131427338, fragmentVideoPlay);
        beginTransaction.commit();
        this.yibiaoLayout.setVisibility(4);
        this.showFragment(false, this.yibiaoFragment);
        this.app.iIsYibiaoShowing = false;
        this.rightLayout.setVisibility(4);
        this.playVideoLayout.setVisibility(0);
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        if (LauncherApplication.iAccOff) {
            return true;
        }
        if (this.app.service != null && this.app.service.iIsInAndroid && this.app.iIsScreenClose.get()) {
            this.app.closeOrWakeupScreen(false);
            return true;
        }
        return super.dispatchTouchEvent(motionEvent);
    }
    
    public Fragment getCurrentFragment() {
        return this.getSupportFragmentManager().findFragmentById(2131427331);
    }
    
    public void goTo(final int mCurIndex) {
        if (!UtilTools.isFastDoubleClick()) {
            switch (mCurIndex) {
                case 0: {
                    this.app.startNavi();
                    break;
                }
                case 1: {
                    this.app.interOriginalView();
                    break;
                }
                case 2: {
                    this.changeRightTo(new FragmentMediaMenu());
                    break;
                }
                case 3: {
                    this.changeRightTo(new CallPhoneFragment());
                    break;
                }
                case 4: {
                    this.changeCenterTo();
                    LauncherApplication.shutDoorNeedShowYibiao = true;
                    break;
                }
                case 5: {
                    this.app.startAppByPkg("net.easyconn");
                    break;
                }
                case 6: {
                    this.app.startAppByPkg("com.android.chrome");
                    break;
                }
                case 7: {
                    this.changeRightTo(new FragmentYingyong());
                    break;
                }
                case 8: {
                    final SettingDialog.Builder builder = new SettingDialog.Builder((Context)this);
                    final SettingDialog create = builder.create();
                    DialogUtil.setDialogLocation(this.app.currentDialog1 = create, 100, 0);
                    create.show();
                    create.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                        public void onDismiss(final DialogInterface dialogInterface) {
                            builder.unRigisterHandler();
                            Launcher.this.app.currentDialog1 = null;
                        }
                    });
                    break;
                }
                case 9: {
                    LauncherApplication.getInstance().closeOrWakeupScreen(true);
                    break;
                }
            }
            if (mCurIndex > -1 || mCurIndex < 11) {
                MenuFragment.mCurIndex = mCurIndex;
            }
        }
    }
    
    public void handHomeAction() {
        this.app.dismissDialog();
        this.showTop();
        this.app.sendHideOrShowLocateViewEvent(false);
        LauncherApplication.shutDoorNeedShowYibiao = false;
        final Fragment currentFragment = this.getCurrentFragment();
        this.rightLayout.setVisibility(0);
        this.mLeftStateSlide.setVisibility(0);
        if (this.yibiaoLayout.getVisibility() == 0) {
            this.yibiaoLayout.setVisibility(4);
            this.showFragment(false, this.yibiaoFragment);
            this.app.iIsYibiaoShowing = false;
        }
        if (this.playVideoLayout.getVisibility() == 0) {
            this.removePlayVideo();
            this.playVideoLayout.setVisibility(4);
        }
        if (currentFragment instanceof MenuFragment && MenuFragment.mCurIndex != 0) {
            this.menuFragment.selectMenu(0);
        }
        else {
            this.backList.clear();
            this.changeFragment(2131427331, this.menuFragment);
        }
        this.hideButtom();
    }
    
    public void handleBackAction() {
        this.showTop();
        LauncherApplication.shutDoorNeedShowYibiao = false;
        final Fragment currentFragment = this.getCurrentFragment();
        if (!(currentFragment instanceof MenuFragment) && this.backList.size() == 0) {
            this.changeRightTo(this.menuFragment);
            this.hideButtom();
        }
        else {
            if (this.yibiaoLayout.getVisibility() == 0) {
                this.rightLayout.setVisibility(0);
                this.mLeftStateSlide.setVisibility(0);
                this.yibiaoLayout.setVisibility(4);
                this.showFragment(false, this.yibiaoFragment);
                this.app.iIsYibiaoShowing = false;
                this.hideButtom();
                return;
            }
            if (this.playVideoLayout.getVisibility() == 0) {
                this.removePlayVideo();
                this.rightLayout.setVisibility(0);
                this.mLeftStateSlide.setVisibility(0);
                this.playVideoLayout.setVisibility(8);
                return;
            }
            if (currentFragment instanceof BaseFragment && !((BaseFragment)currentFragment).onBack()) {
                if (this.backList.size() > 1) {
                    this.backList.remove(this.backList.size() - 1);
                    Fragment fragment;
                    if ((fragment = this.backList.get(this.backList.size() - 1)) instanceof CallPhoneFragment) {
                        fragment = new CallPhoneFragment();
                    }
                    this.changeRightLayoutFragmentByHistory(fragment);
                    return;
                }
                this.backList.clear();
                this.changeRightTo(this.menuFragment);
                this.hideButtom();
            }
        }
    }
    
    public void hideButtom() {
        if (!this.mIsButtomShowed) {
            return;
        }
        this.mIsButtomShowed = false;
        this.mButtomSlide.smoothScrollYTo(-60, 500);
    }
    
    public void hideTop() {
        if (!this.mIsTopShowed) {
            return;
        }
        this.mIsTopShowed = false;
        this.mTopSlide.smoothScrollYTo(40, 300);
    }
    
    @Override
    public void onBackPressed() {
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        Log.e("", "NAVIGATION_BAR----------------onCreate");
        this.requestWindowFeature(1);
        this.setContentView(2130903041);
        (this.app = (LauncherApplication)this.getApplication()).registerHandler(this.mHandler);
        this.app.launcherHandler = this.mHandler;
        final Intent intent = new Intent((Context)this, (Class)MainService.class);
        this.startService(intent);
        if (this.app.service == null) {
            this.bindService(intent, this.conn, 1);
        }
        this.startBluetoothService();
        this.initObject();
        this.inflate();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.app.unregisterHandler(this.mHandler);
        try {
            this.unbindService(this.conn);
            this.unbindService(this.blueconn);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    protected void onPause() {
        this.app.iCurrentInLauncher = false;
        if (this.app.service != null) {
            this.app.service.openNaviThread();
        }
        super.onPause();
    }
    
    @Override
    protected void onResume() {
        Log.e("", "NAVIGATION_BAR----------------onResume");
        if (this.app.service != null) {
            this.app.service.closeNaviThread();
        }
        this.app.sendHideOrShowNavigationBarEvent(false);
        this.app.iCurrentInLauncher = true;
        this.mStateFra.checkUsb3();
        super.onResume();
    }
    
    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
    
    @Override
    protected void onStart() {
        Log.e("", "NAVIGATION_BAR----------------onStart== ");
        super.onStart();
    }
    
    @Override
    protected void onStop() {
        this.app.dismissDialog();
        super.onStop();
    }
    
    @Override
    public void resetLeftLayoutToRightState() {
        this.mLeftStateSlide.iHandleTouchEvent = true;
        this.mLeftStateSlide.changeToCurrentView();
        if (LauncherApplication.shutDoorNeedShowYibiao) {
            this.yibiaoLayout.setVisibility(0);
            this.showFragment(true, this.yibiaoFragment);
            this.mLeftStateSlide.setVisibility(8);
            this.rightLayout.setVisibility(8);
            this.app.iIsYibiaoShowing = true;
        }
    }
    
    public void showButtom() {
        if (this.mIsButtomShowed) {
            return;
        }
        this.mIsButtomShowed = true;
        this.mButtomSlide.smoothScrollYTo(0, 300);
    }
    
    @Override
    public void showCardoor() {
        if (this.yibiaoLayout.getVisibility() != 0) {
            this.mLeftStateSlide.iHandleTouchEvent = false;
            this.mLeftStateSlide.scrollTo(LeftStateSlide.mChildWith * 3, 0);
            if (this.yibiaoLayout.getVisibility() == 0) {
                this.yibiaoLayout.setVisibility(8);
                this.showFragment(false, this.yibiaoFragment);
                this.rightLayout.setVisibility(0);
                this.mLeftStateSlide.setVisibility(0);
                LauncherApplication.shutDoorNeedShowYibiao = true;
                this.app.iIsYibiaoShowing = false;
            }
        }
    }
    
    public void showFragment(final Boolean b, final Fragment fragment) {
        if (this.isDestroyed()) {
            return;
        }
        final FragmentTransaction beginTransaction = this.manager.beginTransaction();
        if (b) {
            beginTransaction.show(fragment);
        }
        else {
            beginTransaction.hide(fragment);
        }
        beginTransaction.commitAllowingStateLoss();
    }
    
    public void showNaviView() {
    }
    
    public void showTop() {
        if (this.mIsTopShowed) {
            return;
        }
        this.mIsTopShowed = true;
        this.mTopSlide.smoothScrollYTo(0, 500);
    }
    
    class InitBluetoothThread implements Runnable
    {
        @Override
        public void run() {
            Launcher.this.app.btservice.readPairingList();
            Launcher.this.app.btservice.readPhoneStatus();
            Launcher.this.app.btservice.readDeviceAddr();
        }
    }
    
    static class Mhandler extends Handler
    {
        private WeakReference<Launcher> target;
        
        public Mhandler(final Launcher launcher) {
            this.target = new WeakReference<Launcher>(launcher);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
