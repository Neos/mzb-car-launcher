package com.touchus.benchilauncher.views;

import java.util.concurrent.atomic.*;
import com.touchus.benchilauncher.*;
import android.content.*;
import android.widget.*;
import org.slf4j.*;
import com.backaudio.android.driver.*;
import com.touchus.benchilauncher.service.*;
import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import com.touchus.publicutils.utils.*;
import android.app.*;
import android.view.*;
import android.text.*;
import java.lang.ref.*;
import android.os.*;

public class FloatSystemCallDialog implements View$OnClickListener
{
    private static FloatSystemCallDialog instance;
    private static AtomicBoolean isAdded;
    private static Logger logger;
    private static AtomicInteger showingStatus;
    private LauncherApplication app;
    private String callingPhoneNumber;
    private ImageButton leftButton;
    private View mCallFloatView;
    private Context mContext;
    private BluetoothHandler mHandler;
    private WindowManager$LayoutParams params;
    private LinearLayout phoneLayout;
    private ImageButton rightButton;
    private TextView talkingPhoneNumber;
    private TextView talkingphoneStatus;
    private WindowManager wm;
    
    static {
        FloatSystemCallDialog.logger = LoggerFactory.getLogger(FloatSystemCallDialog.class);
        FloatSystemCallDialog.instance = null;
        FloatSystemCallDialog.showingStatus = new AtomicInteger(FloatShowST.DEFAULT.ordinal());
        FloatSystemCallDialog.isAdded = new AtomicBoolean(false);
    }
    
    public FloatSystemCallDialog() {
        this.mCallFloatView = null;
        this.callingPhoneNumber = "-1";
        this.mHandler = new BluetoothHandler(this);
        FloatSystemCallDialog.logger.debug("FloatSystemCallDialog FloatSystemCallDialog()");
    }
    
    public static FloatSystemCallDialog getInstance() {
        if (FloatSystemCallDialog.instance == null) {
            FloatSystemCallDialog.instance = new FloatSystemCallDialog();
        }
        return FloatSystemCallDialog.instance;
    }
    
    private void handlerMsg(final Message message) {
        final Bundle data = message.getData();
        switch (message.what) {
            case 6001: {
                final byte byte1 = data.getByte("idriver_enum");
                if (byte1 == Mainboard.EIdriverEnum.PICK_UP.getCode()) {
                    if (BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                        this.leftButton.performClick();
                        return;
                    }
                    break;
                }
                else if (byte1 == Mainboard.EIdriverEnum.HANG_UP.getCode()) {
                    if (BluetoothService.bluetoothStatus == EPhoneStatus.TALKING || BluetoothService.bluetoothStatus == EPhoneStatus.CALLING_OUT || BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                        this.rightButton.performClick();
                        return;
                    }
                    break;
                }
                else if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                    if (this.rightButton.isEnabled()) {
                        this.rightButton.setSelected(true);
                        this.leftButton.setSelected(false);
                        return;
                    }
                    break;
                }
                else if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    if (this.leftButton.isEnabled()) {
                        this.leftButton.setSelected(true);
                        this.rightButton.setSelected(false);
                        return;
                    }
                    break;
                }
                else {
                    if (byte1 != Mainboard.EIdriverEnum.PRESS.getCode()) {
                        break;
                    }
                    if (this.leftButton.isSelected()) {
                        this.leftButton.performClick();
                        return;
                    }
                    if (this.rightButton.isSelected()) {
                        this.rightButton.performClick();
                        return;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    private void modifyName() {
        if (this.mCallFloatView == null) {
            return;
        }
        this.talkingPhoneNumber.setText((CharSequence)this.app.phoneName);
    }
    
    private void setButtonState(final boolean b) {
        if (b) {
            this.leftButton.setClickable(false);
            this.leftButton.setSelected(false);
            this.leftButton.setEnabled(false);
            this.rightButton.setSelected(true);
            return;
        }
        this.leftButton.setClickable(true);
        this.leftButton.setSelected(true);
        this.leftButton.setEnabled(true);
        this.rightButton.setSelected(false);
    }
    
    public void clearView() {
        if (this.mCallFloatView == null) {
            return;
        }
        while (true) {
            try {
                if (FloatSystemCallDialog.isAdded.get()) {
                    this.wm.removeViewImmediate(this.mCallFloatView);
                    FloatSystemCallDialog.isAdded.set(false);
                }
                this.mCallFloatView = null;
                FloatSystemCallDialog.isAdded.set(false);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void dissShow() {
        if (this.app.currentDialog4 != null) {
            this.app.currentDialog4.dismiss();
        }
        if (this.mCallFloatView == null) {
            return;
        }
        this.app.unregisterHandler(this.mHandler);
        this.app.isCallDialog = false;
        if (FloatSystemCallDialog.showingStatus.get() == FloatShowST.HIDEN.ordinal()) {
            FloatSystemCallDialog.logger.debug("FloatSystemCallDialog HIDEN  return");
            while (true) {
                try {
                    if (FloatSystemCallDialog.isAdded.get()) {
                        this.wm.removeViewImmediate(this.mCallFloatView);
                        FloatSystemCallDialog.isAdded.set(false);
                    }
                    FloatSystemCallDialog.showingStatus.set(FloatShowST.DEFAULT.ordinal());
                    this.mCallFloatView = null;
                    return;
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    continue;
                }
                break;
            }
        }
        FloatSystemCallDialog.showingStatus.set(FloatShowST.DEFAULT.ordinal());
        this.app.btservice.histalkflag = false;
        this.app.btservice.accFlag = false;
        this.app.btservice.exitsource();
        while (true) {
            try {
                if (FloatSystemCallDialog.isAdded.get()) {
                    this.wm.removeViewImmediate(this.mCallFloatView);
                    FloatSystemCallDialog.isAdded.set(false);
                }
                this.mCallFloatView = null;
                FloatSystemCallDialog.isAdded.set(false);
            }
            catch (Exception ex2) {
                ex2.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void freshCallingTime() {
        if (this.mCallFloatView == null) {
            return;
        }
        FloatSystemCallDialog.logger.debug("TIME_FLAG" + TimeUtils.secToTimeString(this.app.btservice.calltime));
        this.talkingphoneStatus.setText((CharSequence)String.format(this.mContext.getString(2131165235), TimeUtils.secToTimeString(this.app.btservice.calltime)));
        this.updateTalkStatus();
    }
    
    public String getCallingPhonenumber() {
        return this.callingPhoneNumber;
    }
    
    public FloatShowST getShowStatus() {
        return FloatShowST.values()[FloatSystemCallDialog.showingStatus.get()];
    }
    
    public void hiden() {
        if (this.app.currentDialog4 != null) {
            this.app.currentDialog4.dismiss();
        }
        if (this.mCallFloatView != null) {
            this.app.unregisterHandler(this.mHandler);
            this.app.isCallDialog = false;
            FloatSystemCallDialog.showingStatus.set(FloatShowST.HIDEN.ordinal());
            try {
                if (FloatSystemCallDialog.isAdded.get()) {
                    this.wm.removeViewImmediate(this.mCallFloatView);
                    FloatSystemCallDialog.isAdded.set(false);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public boolean isDestory() {
        return this.mCallFloatView == null;
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131427362: {
                if (BluetoothService.currentCallingType == 1412) {
                    this.app.btservice.answerCalling();
                    FloatSystemCallDialog.showingStatus.set(FloatShowST.TALKING.ordinal());
                    return;
                }
                break;
            }
            case 2131427361: {
                if (!UtilTools.isForeground(LauncherApplication.mContext, "com.touchus.benchilauncher.Launcher")) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            new Instrumentation().sendKeyDownUpSync(3);
                            FloatSystemCallDialog.this.app.sendMessage(1013, null);
                        }
                    }).start();
                    return;
                }
                this.app.sendMessage(1013, null);
            }
            case 2131427365: {
                if (UtilTools.isFastDoubleClick()) {
                    break;
                }
                FloatSystemCallDialog.logger.debug("FloatSystemCallDialog cutdownCurrentCalling");
                while (true) {
                    try {
                        this.app.btservice.cutdownCurrentCalling();
                        this.dissShow();
                        this.app.mHandler.postDelayed((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                FloatSystemCallDialog.this.setCallingPhonenumber("-1");
                            }
                        }, 1000L);
                        return;
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    public void setCallingPhonenumber(final String callingPhoneNumber) {
        synchronized (this) {
            this.callingPhoneNumber = callingPhoneNumber;
        }
    }
    
    public void setShowStatus(final FloatShowST floatShowST) {
        FloatSystemCallDialog.logger.debug("FloatSystemCallDialog setShowStatus showingStatus: " + floatShowST);
        FloatSystemCallDialog.showingStatus.set(floatShowST.ordinal());
        FloatSystemCallDialog.logger.debug("FloatSystemCallDialog setShowStatus showingStatus: " + this.getShowStatus());
    }
    
    public void show() {
        FloatSystemCallDialog.logger.debug("FloatSystemCallDialog show()");
        if (this.mCallFloatView == null) {
            FloatSystemCallDialog.logger.debug("FloatSystemCallDialog show  mCallFloatView = null");
            return;
        }
        this.app.registerHandler(this.mHandler);
        this.app.isCallDialog = true;
        this.updateTalkStatus();
        while (true) {
            try {
                if (!FloatSystemCallDialog.isAdded.get()) {
                    this.wm.addView(this.mCallFloatView, (ViewGroup$LayoutParams)this.params);
                    FloatSystemCallDialog.isAdded.set(true);
                }
                this.modifyName();
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void showFloatCallView(final Context context) {
        Label_0405_Outer:Label_0434_Outer:
        while (true) {
            while (true) {
            Label_0577:
                while (true) {
                Label_0524:
                    while (true) {
                        Label_0507: {
                            while (true) {
                                try {
                                    this.mContext = context.getApplicationContext();
                                    this.app = (LauncherApplication)context.getApplicationContext();
                                    FloatSystemCallDialog.logger.debug("FloatSystemCallDialog showFloatCallView()  PhoneNumber=" + this.app.phoneNumber);
                                    this.wm = (WindowManager)this.mContext.getSystemService("window");
                                    this.params = new WindowManager$LayoutParams();
                                    this.mContext.getResources().getDisplayMetrics();
                                    this.params.height = 235;
                                    this.params.width = 700;
                                    this.params.format = 1;
                                    this.params.flags = 40;
                                    this.params.type = 2002;
                                    Label_0170: {
                                        if (this.mCallFloatView == null) {
                                            break Label_0170;
                                        }
                                        try {
                                            FloatSystemCallDialog.isAdded.set(false);
                                            this.wm.removeView(this.mCallFloatView);
                                            this.mCallFloatView = null;
                                            this.mCallFloatView = LayoutInflater.from(this.mContext).inflate(2130903049, (ViewGroup)null);
                                            this.talkingPhoneNumber = (TextView)this.mCallFloatView.findViewById(2131427364);
                                            this.talkingphoneStatus = (TextView)this.mCallFloatView.findViewById(2131427363);
                                            this.rightButton = (ImageButton)this.mCallFloatView.findViewById(2131427365);
                                            this.leftButton = (ImageButton)this.mCallFloatView.findViewById(2131427362);
                                            this.phoneLayout = (LinearLayout)this.mCallFloatView.findViewById(2131427361);
                                            if (!FloatSystemCallDialog.isAdded.get()) {
                                                this.wm.addView(this.mCallFloatView, (ViewGroup$LayoutParams)this.params);
                                                FloatSystemCallDialog.isAdded.set(true);
                                            }
                                            if (!TextUtils.isEmpty((CharSequence)this.app.phoneNumber)) {
                                                break Label_0507;
                                            }
                                            this.talkingPhoneNumber.setText((CharSequence)this.mContext.getString(2131165240));
                                            this.leftButton.setOnClickListener((View$OnClickListener)this);
                                            this.rightButton.setOnClickListener((View$OnClickListener)this);
                                            this.phoneLayout.setOnClickListener((View$OnClickListener)this);
                                            if (BluetoothService.bluetoothStatus != EPhoneStatus.INCOMING_CALL) {
                                                break Label_0524;
                                            }
                                            this.talkingphoneStatus.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165237)) + " "));
                                            if (BluetoothService.currentCallingType == 1412 && !TextUtils.isEmpty((CharSequence)this.app.phoneNumber)) {
                                                this.setButtonState(BluetoothService.talkingflag);
                                                this.app.registerHandler(this.mHandler);
                                                this.app.isCallDialog = true;
                                                return;
                                            }
                                            break Label_0577;
                                        }
                                        catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                }
                                catch (Exception ex2) {
                                    FloatSystemCallDialog.logger.debug("FloatSystemCallDialog Exception:" + ex2);
                                    ex2.printStackTrace();
                                    FloatSystemCallDialog.showingStatus.set(FloatShowST.DEFAULT.ordinal());
                                    continue Label_0405_Outer;
                                }
                                break;
                            }
                        }
                        this.talkingPhoneNumber.setText((CharSequence)this.app.phoneName);
                        continue Label_0405_Outer;
                    }
                    if (BluetoothService.bluetoothStatus == EPhoneStatus.CALLING_OUT) {
                        this.talkingphoneStatus.setText((CharSequence)(String.valueOf(this.mContext.getString(2131165238)) + " "));
                        this.setButtonState(true);
                        continue Label_0434_Outer;
                    }
                    continue Label_0434_Outer;
                }
                if (BluetoothService.currentCallingType == 1413) {
                    this.setButtonState(true);
                    continue;
                }
                continue;
            }
        }
    }
    
    public void updateTalkStatus() {
        if (this.mCallFloatView == null) {
            return;
        }
        if (BluetoothService.talkingflag) {
            this.setButtonState(true);
            FloatSystemCallDialog.showingStatus.set(FloatShowST.TALKING.ordinal());
            return;
        }
        this.setButtonState(false);
        if (BluetoothService.currentCallingType == 1412) {
            FloatSystemCallDialog.showingStatus.set(FloatShowST.INCOMING.ordinal());
            return;
        }
        if (BluetoothService.currentCallingType == 1413) {
            FloatSystemCallDialog.showingStatus.set(FloatShowST.OUTCALLING.ordinal());
            return;
        }
        FloatSystemCallDialog.showingStatus.set(FloatShowST.DEFAULT.ordinal());
    }
    
    public static class BluetoothHandler extends Handler
    {
        private WeakReference<FloatSystemCallDialog> target;
        
        public BluetoothHandler(final FloatSystemCallDialog floatSystemCallDialog) {
            this.target = new WeakReference<FloatSystemCallDialog>(floatSystemCallDialog);
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
    
    public enum FloatShowST
    {
        DEFAULT("DEFAULT", 0), 
        HIDEN("HIDEN", 4), 
        INCOMING("INCOMING", 1), 
        OUTCALLING("OUTCALLING", 3), 
        TALKING("TALKING", 2);
        
        private FloatShowST(final String s, final int n) {
        }
    }
    
    class PhoneNumPraseTask extends AsyncTask<String, String, String>
    {
        protected String doInBackground(final String... array) {
            try {
                if (UtilTools.isNetworkConnected(FloatSystemCallDialog.this.mContext)) {
                    FloatSystemCallDialog.logger.debug("FloatSystemCallDialog PhoneNumPraseTask city: " + "");
                    return "";
                }
                FloatSystemCallDialog.logger.debug("FloatSystemCallDialog PhoneNumPraseTask not net error ! ");
                return FloatSystemCallDialog.this.mContext.getString(2131165236);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                FloatSystemCallDialog.logger.debug("FloatSystemCallDialog get city error !");
                return "";
            }
        }
        
        protected void onPostExecute(final String s) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                if (BluetoothService.currentCallingType != 1412) {
                    if (BluetoothService.currentCallingType != 1413) {
                        return;
                    }
                }
                try {
                    if (BluetoothService.bluetoothStatus == EPhoneStatus.INCOMING_CALL) {
                        FloatSystemCallDialog.this.talkingphoneStatus.setText((CharSequence)(String.valueOf(FloatSystemCallDialog.this.mContext.getString(2131165237)) + " " + s));
                        return;
                    }
                    if (BluetoothService.bluetoothStatus == EPhoneStatus.CALLING_OUT) {
                        FloatSystemCallDialog.this.talkingphoneStatus.setText((CharSequence)(String.valueOf(FloatSystemCallDialog.this.mContext.getString(2131165238)) + " " + s));
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
