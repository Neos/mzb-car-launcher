package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.bean.*;
import android.content.*;
import com.touchus.benchilauncher.adapter.*;
import android.content.pm.*;
import com.touchus.publicutils.utils.*;
import java.util.*;
import com.backaudio.android.driver.*;
import android.widget.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class AppSettingDialog extends Dialog implements AdapterView$OnItemClickListener
{
    private LauncherApplication app;
    private List<AppInfo> appInfos;
    private Context context;
    int currentSelectIndex;
    private AppsetItemAdapter mAdapter;
    public MHandler mHandler;
    private ListView mListView;
    
    public AppSettingDialog(final Context context) {
        super(context);
        this.appInfos = new ArrayList<AppInfo>();
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    public AppSettingDialog(final Context context, final int n) {
        super(context, n);
        this.appInfos = new ArrayList<AppInfo>();
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    private void initData() {
        this.appInfos.clear();
        final List installedPackages = this.context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < installedPackages.size(); ++i) {
            final PackageInfo packageInfo = installedPackages.get(i);
            if (APPSettings.isNavgation(packageInfo.packageName)) {
                final AppInfo appInfo = new AppInfo();
                appInfo.setAppName(packageInfo.applicationInfo.loadLabel(this.context.getPackageManager()).toString());
                appInfo.setPackageName(packageInfo.packageName);
                appInfo.setVersionName(packageInfo.versionName);
                appInfo.setVersionCode(packageInfo.versionCode);
                appInfo.setAppIcon(packageInfo.applicationInfo.loadIcon(this.context.getPackageManager()));
                appInfo.setiCheck(this.app.getNaviAPP().equals(packageInfo.packageName));
                this.appInfos.add(appInfo);
            }
        }
        for (int j = 0; j < this.appInfos.size(); ++j) {
            if (this.appInfos.get(j).isiCheck()) {
                Collections.swap(this.appInfos, 0, j);
            }
        }
    }
    
    private void pressItem(final int n) {
        this.currentSelectIndex = n;
        this.app.setNaviAPP(this.appInfos.get(n).getPackageName());
        this.mAdapter.setSeclectIndex(n);
    }
    
    private void seclectTrue(final int seclectIndex) {
        this.mAdapter.setSeclectIndex(seclectIndex);
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.DOWN.getCode()) {
                if (this.currentSelectIndex < this.appInfos.size() - 1) {
                    ++this.currentSelectIndex;
                }
                this.seclectTrue(this.currentSelectIndex);
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.currentSelectIndex > 0) {
                        --this.currentSelectIndex;
                    }
                    this.seclectTrue(this.currentSelectIndex);
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.pressItem(this.currentSelectIndex);
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setContentView(2130903047);
        this.app = (LauncherApplication)this.context.getApplicationContext();
        this.initData();
        this.mListView = (ListView)this.findViewById(2131427356);
        this.mAdapter = new AppsetItemAdapter(this.app, this.context, this.appInfos);
        this.mListView.setAdapter((ListAdapter)this.mAdapter);
        this.mListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        super.onCreate(bundle);
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        this.pressItem(n);
    }
    
    protected void onStart() {
        this.app.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mHandler.removeCallbacksAndMessages((Object)null);
        this.app.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<AppSettingDialog> target;
        
        public MHandler(final AppSettingDialog appSettingDialog) {
            this.target = new WeakReference<AppSettingDialog>(appSettingDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
