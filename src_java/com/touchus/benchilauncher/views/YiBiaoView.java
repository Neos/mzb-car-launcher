package com.touchus.benchilauncher.views;

import android.view.*;
import android.graphics.drawable.*;
import android.content.*;
import android.util.*;
import com.touchus.benchilauncher.*;
import android.content.res.*;
import android.graphics.*;

public class YiBiaoView extends View
{
    private float angle;
    private Drawable bgImage;
    private boolean isChange;
    private Drawable neiYuanView;
    private Paint paint;
    private Drawable pointView;
    
    public YiBiaoView(final Context context) {
        this(context, null);
    }
    
    public YiBiaoView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public YiBiaoView(final Context mContext, final AttributeSet set, final int n) {
        super(mContext, set, n);
        this.mContext = mContext;
        final TypedArray obtainStyledAttributes = mContext.obtainStyledAttributes(set, R.styleable.MyYibiaoStyleable, n, 0);
        this.bgImage = obtainStyledAttributes.getDrawable(0);
        this.pointView = obtainStyledAttributes.getDrawable(2);
        this.neiYuanView = obtainStyledAttributes.getDrawable(1);
        obtainStyledAttributes.recycle();
        (this.paint = new Paint()).setColor(Color.parseColor("#000000"));
        this.paint.setTypeface(Typeface.DEFAULT_BOLD);
        this.paint.setFakeBoldText(true);
        this.paint.setAntiAlias(true);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.isChange = true;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.isChange = false;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final int n = (this.getRight() - this.getLeft()) / 2;
        final int n2 = (this.getBottom() - this.getTop()) / 2;
        final Drawable bgImage = this.bgImage;
        final int intrinsicHeight = bgImage.getIntrinsicHeight();
        final int intrinsicWidth = bgImage.getIntrinsicWidth();
        if (this.getRight() - this.getLeft() < intrinsicWidth || this.getBottom() - this.getTop() < intrinsicHeight) {
            final float min = Math.min((this.getRight() - this.getLeft()) / intrinsicWidth, (this.getBottom() - this.getTop()) / intrinsicHeight);
            canvas.save();
            canvas.scale(min, min, (float)n, (float)n2);
        }
        if (this.isChange) {
            bgImage.setBounds(n - intrinsicWidth / 2, n2 - intrinsicHeight / 2, intrinsicWidth / 2 + n, intrinsicHeight / 2 + n2);
        }
        bgImage.draw(canvas);
        canvas.save();
        canvas.rotate(this.angle, (float)n, (float)n2);
        final Drawable pointView = this.pointView;
        final int intrinsicHeight2 = pointView.getIntrinsicHeight();
        final int intrinsicWidth2 = pointView.getIntrinsicWidth();
        if (this.isChange) {
            pointView.setBounds(n - intrinsicWidth2 / 2, n2 - intrinsicHeight2 / 2, intrinsicWidth2 / 2 + n, intrinsicHeight2 / 2 + n2);
        }
        pointView.draw(canvas);
        canvas.restore();
        canvas.save();
        final Drawable neiYuanView = this.neiYuanView;
        final int intrinsicHeight3 = neiYuanView.getIntrinsicHeight();
        final int intrinsicWidth3 = neiYuanView.getIntrinsicWidth();
        neiYuanView.setBounds(n - intrinsicWidth3 / 2, n2 - intrinsicHeight3 / 2, intrinsicWidth3 / 2 + n, intrinsicHeight3 / 2 + n2);
        neiYuanView.draw(canvas);
        canvas.restore();
        canvas.save();
    }
    
    public void setAngle(final float angle) {
        this.angle = angle;
        this.postInvalidate();
    }
}
