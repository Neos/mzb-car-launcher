package com.touchus.benchilauncher.views;

import android.widget.*;
import android.util.*;
import com.touchus.benchilauncher.utils.*;
import android.content.*;
import android.net.*;
import android.app.*;
import com.touchus.benchilauncher.*;
import android.text.*;
import java.io.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class UpdateDialog extends Dialog implements View$OnClickListener
{
    public static final int APP_UPDATE = 4;
    public static final int CANBOX_UPDATE = 3;
    public static final int MCU_UPDATE = 2;
    public static final int SN_UPDATE = 5;
    public static final int SYSTEM_UPDATE = 1;
    private LauncherApplication app;
    private Button cancelBtn;
    private Context context;
    private TextView curInfoTview;
    private TextView curTypeTview;
    public int currentType;
    private String currentUpdatePath;
    private boolean iFinishUpdate;
    private boolean iInCancelState;
    public MHandler mHandler;
    private BroadcastReceiver onlineDownloadReceiver;
    private Button submitBtn;
    public String url;
    
    public UpdateDialog(final Context context) {
        super(context);
        this.iInCancelState = true;
        this.iFinishUpdate = false;
        this.currentType = 1;
        this.url = "http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh";
        this.onlineDownloadReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                Log.e("UpdateDialog", "onlineDownloadReceiver  getAction " + intent.getAction());
                if (intent.getAction().equals("android.intent.action.DOWNLOAD_COMPLETE")) {
                    Utiltools.onlineOtaUpdate();
                }
            }
        };
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    public UpdateDialog(final Context context, final int n) {
        super(context, n);
        this.iInCancelState = true;
        this.iFinishUpdate = false;
        this.currentType = 1;
        this.url = "http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh";
        this.onlineDownloadReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                Log.e("UpdateDialog", "onlineDownloadReceiver  getAction " + intent.getAction());
                if (intent.getAction().equals("android.intent.action.DOWNLOAD_COMPLETE")) {
                    Utiltools.onlineOtaUpdate();
                }
            }
        };
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    private void addOnlineDownloadReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DOWNLOAD_COMPLETE");
        this.context.registerReceiver(this.onlineDownloadReceiver, intentFilter);
    }
    
    private void androidUpdate() {
        new Thread("Reboot") {
            @Override
            public void run() {
                try {
                    final File file = new File("/SDCARD/update.zip");
                    RecoverySystem.handleAftermath();
                    RecoverySystem.installPackage(UpdateDialog.this.context, file);
                }
                catch (IOException ex) {}
            }
        }.start();
    }
    
    private void checkUpdateFile() {
        this.submitBtn.setEnabled(false);
        if (this.currentType == 1) {
            this.startSystemCheckThread();
        }
        else {
            if (this.currentType == 3) {
                this.startCanboxCheckThread();
                return;
            }
            if (this.currentType == 2) {
                this.startMcuCheckThread();
                return;
            }
            if (this.currentType == 4) {
                this.startAPPCheckThread();
                return;
            }
            if (this.currentType == 5) {
                this.startSNCheckThread();
            }
        }
    }
    
    private void deleteFile(final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     3: invokestatic    java/lang/Thread.sleep:(J)V
                //     6: new             Ljava/io/File;
                //     9: dup            
                //    10: aload_0        
                //    11: getfield        com/touchus/benchilauncher/views/UpdateDialog$10.val$path:Ljava/lang/String;
                //    14: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
                //    17: astore_1       
                //    18: aload_1        
                //    19: invokevirtual   java/io/File.exists:()Z
                //    22: ifeq            30
                //    25: aload_1        
                //    26: invokevirtual   java/io/File.delete:()Z
                //    29: pop            
                //    30: return         
                //    31: astore_1       
                //    32: aload_1        
                //    33: invokevirtual   java/lang/Exception.printStackTrace:()V
                //    36: goto            6
                //    39: astore_1       
                //    40: aload_1        
                //    41: invokevirtual   java/lang/Exception.printStackTrace:()V
                //    44: return         
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  0      6      31     39     Ljava/lang/Exception;
                //  6      30     39     45     Ljava/lang/Exception;
                // 
                // The error that occurred was:
                // 
                // java.lang.IllegalStateException: Expression is linked from several locations: Label_0006:
                //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }).start();
    }
    
    private boolean getFile() {
        final File file = new File("update");
        return (file.exists() && file.isDirectory()) || file.mkdirs();
    }
    
    private void getOnlineConfig() {
        this.setCanceledOnTouchOutside(false);
        this.submitBtn.setVisibility(8);
        this.cancelBtn.setVisibility(8);
        this.curInfoTview.setText((CharSequence)this.context.getString(2131165294));
        this.app.mHomeAndBackEnable = false;
        this.addOnlineDownloadReceiver();
        final DownloadManager downloadManager = (DownloadManager)this.context.getSystemService("download");
        final DownloadManager$Request downloadManager$Request = new DownloadManager$Request(Uri.parse(this.url));
        this.getFile();
        downloadManager$Request.setDestinationInExternalPublicDir("update", "update_config.sh");
        downloadManager.enqueue(downloadManager$Request);
    }
    
    private void scanUsbAndSdcard(final String s) {
        if (!this.scanUsbDiskUpdateFile(new File(this.context.getString(2131165198)), s)) {
            this.scanUsbDiskUpdateFile(new File("mnt/usbotg"), s);
        }
    }
    
    private boolean scanUsbDiskUpdateFile(final File file, final String s) {
        final String[] list = file.list();
        if (list != null) {
            for (int length = list.length, i = 0; i < length; ++i) {
                final String s2 = list[i];
                try {
                    final File file2 = new File(file, s2);
                    if (file2.isFile()) {
                        final String absolutePath = file2.getAbsolutePath();
                        if (absolutePath.contains(s)) {
                            this.currentUpdatePath = absolutePath;
                            return true;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }
    
    private void startAPPCheckThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                UpdateDialog.this.scanUsbAndSdcard(ProjectConfig.APPSoft);
                UpdateDialog.this.mHandler.obtainMessage(1111).sendToTarget();
            }
        }).start();
    }
    
    private void startCanboxCheckThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                UpdateDialog.this.scanUsbAndSdcard(ProjectConfig.CANSoft);
                UpdateDialog.this.mHandler.obtainMessage(1111).sendToTarget();
            }
        }).start();
    }
    
    private void startMcuCheckThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                UpdateDialog.this.scanUsbAndSdcard(ProjectConfig.MCUSoft);
                UpdateDialog.this.mHandler.obtainMessage(1111).sendToTarget();
            }
        }).start();
    }
    
    private void startSNCheckThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                UpdateDialog.this.mHandler.obtainMessage(1124).sendToTarget();
            }
        }).start();
    }
    
    private void startSystemCheckThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                UpdateDialog.this.scanUsbAndSdcard(ProjectConfig.systemSoft);
                UpdateDialog.this.mHandler.obtainMessage(1111).sendToTarget();
            }
        }).start();
    }
    
    private void startUpdate() {
        if (!TextUtils.isEmpty((CharSequence)this.currentUpdatePath)) {
            this.setCanceledOnTouchOutside(false);
            this.submitBtn.setVisibility(8);
            this.cancelBtn.setVisibility(8);
            this.curInfoTview.setText((CharSequence)this.context.getString(2131165283));
            this.app.mHomeAndBackEnable = false;
            if (this.currentType == 1) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (UpdateDialog.this.copyFile(UpdateDialog.this.currentUpdatePath, UpdateDialog.this.context.getString(2131165195))) {
                            UpdateDialog.this.androidUpdate();
                            return;
                        }
                        UpdateDialog.this.mHandler.obtainMessage(1128).sendToTarget();
                    }
                }).start();
                return;
            }
            if (this.currentType == 3) {
                this.copyFile(this.currentUpdatePath, this.context.getString(2131165191));
                this.deleteFile(this.context.getString(2131165192));
                this.app.service.enterIntoUpgradeCanboxCheck();
                return;
            }
            if (this.currentType == 2) {
                this.copyFile(this.currentUpdatePath, this.context.getString(2131165193));
                this.deleteFile(this.context.getString(2131165194));
                this.app.service.enterIntoUpgradeMcuCheck();
                return;
            }
            if (this.currentType == 4) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (UpdateDialog.this.copyFile(UpdateDialog.this.currentUpdatePath, UpdateDialog.this.context.getString(2131165196))) {
                            Utiltools.updateLauncherAPK();
                            return;
                        }
                        UpdateDialog.this.mHandler.obtainMessage(1128).sendToTarget();
                    }
                }).start();
            }
        }
    }
    
    public Boolean copyFile(final String s, final String s2) {
        Label_0143: {
            try {
                if (new File(s).exists()) {
                    final FileInputStream fileInputStream = new FileInputStream(s);
                    final FileOutputStream fileOutputStream = new FileOutputStream(s2);
                    final int available = fileInputStream.available();
                    final byte[] array = new byte[8192];
                    while (true) {
                        final int read = fileInputStream.read(array);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(array, 0, read);
                        final int n = (int)(fileInputStream.available() * 1.0 / available * 100.0);
                        if (n > 100 || n < 0) {
                            continue;
                        }
                        this.mHandler.obtainMessage(1112, (Object)(100 - n)).sendToTarget();
                    }
                    fileInputStream.close();
                    fileOutputStream.close();
                    return true;
                }
                break Label_0143;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            return false;
        }
        Log.d("", "file not  exist...........");
        return false;
    }
    
    public void dismiss() {
        this.app.currentDialog3 = null;
        this.app.mHomeAndBackEnable = true;
        super.dismiss();
    }
    
    public void handlerMsgUSB(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.iInCancelState = true;
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    this.iInCancelState = false;
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    if (this.iInCancelState) {
                        this.dismiss();
                        return;
                    }
                    if (this.currentType == 5) {
                        this.getOnlineConfig();
                        return;
                    }
                    this.startUpdate();
                }
                else if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
            return;
        }
        if (message.what == 1111) {
            if (TextUtils.isEmpty((CharSequence)this.currentUpdatePath)) {
                this.curInfoTview.setText((CharSequence)this.context.getString(2131165281));
                return;
            }
            this.curInfoTview.setText((CharSequence)this.context.getString(2131165282));
            this.submitBtn.setEnabled(true);
        }
        else {
            if (message.what == 1124) {
                this.curInfoTview.setText((CharSequence)this.context.getString(2131165282));
                this.submitBtn.setEnabled(true);
                return;
            }
            if (message.what == 1128) {
                this.curInfoTview.setText((CharSequence)this.context.getString(2131165280));
                this.setCanceledOnTouchOutside(true);
                this.cancelBtn.setVisibility(0);
                return;
            }
            if (message.what == 1129) {
                this.curInfoTview.setText((CharSequence)this.context.getString(2131165279));
                this.setCanceledOnTouchOutside(true);
                this.cancelBtn.setVisibility(0);
                return;
            }
            if (message.what == 1131 || message.what == 1121) {
                this.app.mHomeAndBackEnable = true;
                this.setCanceledOnTouchOutside(this.iFinishUpdate = true);
                this.submitBtn.setEnabled(true);
                this.cancelBtn.setEnabled(true);
                this.submitBtn.setVisibility(8);
                this.cancelBtn.setText(2131165275);
                this.cancelBtn.setVisibility(0);
                this.curInfoTview.setText((CharSequence)this.context.getString(2131165285));
                if (this.currentType == 2) {
                    Mainboard.getInstance().getMCUVersionNumber();
                    this.deleteFile(this.context.getString(2131165193));
                }
                else if (this.currentType == 3) {
                    Mainboard.getInstance().getModeInfo(Mainboard.EModeInfo.CANBOX_INFO);
                    this.deleteFile(this.context.getString(2131165191));
                }
                this.dismiss();
                return;
            }
            if (message.what == 1112) {
                this.curInfoTview.setText((CharSequence)(String.valueOf(this.context.getString(2131165283)) + "(" + message.obj + "%)"));
                return;
            }
            if (message.what == 1113) {
                this.curInfoTview.setText((CharSequence)(String.valueOf(this.context.getString(2131165284)) + message.getData().getString("upgradePer")));
                return;
            }
            final int what = message.what;
        }
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427405) {
            this.dismiss();
            return;
        }
        if (this.iFinishUpdate) {
            this.dismiss();
            return;
        }
        if (this.currentType == 5) {
            this.getOnlineConfig();
            return;
        }
        this.startUpdate();
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setContentView(2130903062);
        this.app = (LauncherApplication)this.context.getApplicationContext();
        this.curInfoTview = (TextView)this.findViewById(2131427450);
        this.curTypeTview = (TextView)this.findViewById(2131427449);
        this.submitBtn = (Button)this.findViewById(2131427404);
        this.cancelBtn = (Button)this.findViewById(2131427405);
        this.submitBtn.setOnClickListener((View$OnClickListener)this);
        this.cancelBtn.setOnClickListener((View$OnClickListener)this);
        final Window window = this.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        attributes.x = 400;
        attributes.y = 0;
        window.setAttributes(attributes);
        this.submitBtn.setVisibility(0);
        this.cancelBtn.setVisibility(0);
        super.onCreate(bundle);
    }
    
    protected void onStart() {
        this.checkUpdateFile();
        if (this.currentType == 1) {
            this.curTypeTview.setText((CharSequence)"SYSTEM UPDATE");
        }
        else if (this.currentType == 3) {
            this.curTypeTview.setText((CharSequence)"CANBOX UPDATE");
        }
        else if (this.currentType == 2) {
            this.curTypeTview.setText((CharSequence)"MCU UPDATE");
        }
        else if (this.currentType == 4) {
            this.curTypeTview.setText((CharSequence)"APP UPDATE");
        }
        else if (this.currentType == 5) {
            this.curTypeTview.setText((CharSequence)"ONLINE CONFIG UPDATE");
        }
        this.app.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.app.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<UpdateDialog> target;
        
        public MHandler(final UpdateDialog updateDialog) {
            this.target = new WeakReference<UpdateDialog>(updateDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
