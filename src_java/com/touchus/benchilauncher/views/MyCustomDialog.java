package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.*;
import com.backaudio.android.driver.*;
import android.os.*;
import android.view.*;
import android.content.*;
import android.text.*;
import android.widget.*;
import java.lang.ref.*;

public class MyCustomDialog extends Dialog
{
    public MyCustomDialog(final Context context) {
        super(context);
    }
    
    public MyCustomDialog(final Context context, final int n) {
        super(context, n);
    }
    
    public static class Builder
    {
        private LauncherApplication app;
        private LinearLayout button_layout;
        private View contentView;
        private Context context;
        private MyCustomDialog dialog;
        private MyCustomHandler mHandler;
        private String message;
        private String messageNext;
        private Button negativeButton;
        private DialogInterface$OnClickListener negativeButtonClickListener;
        private String negativeButtonText;
        private Button positiveButton;
        private DialogInterface$OnClickListener positiveButtonClickListener;
        private String positiveButtonText;
        
        public Builder(final Context context) {
            this.mHandler = new MyCustomHandler(this);
            this.context = context;
            if (context instanceof Launcher) {
                this.app = (LauncherApplication)((Launcher)context).getApplication();
            }
            if (this.app != null) {
                this.app.registerHandler(this.mHandler);
            }
        }
        
        private void handlerMsg(final Message message) {
            final Bundle data = message.getData();
            if (data.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || data.getByte("idriver_enum") == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.right();
            }
            else {
                if (data.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || data.getByte("idriver_enum") == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    this.left();
                    return;
                }
                if (data.getByte("idriver_enum") == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.press();
                }
            }
        }
        
        private void left() {
            if (this.negativeButton.isSelected()) {
                this.negativeButton.setSelected(false);
                this.positiveButton.setSelected(true);
            }
        }
        
        private void press() {
            if (this.positiveButton.isSelected()) {
                if (this.positiveButtonClickListener != null) {
                    this.positiveButton.performClick();
                }
            }
            else if (this.negativeButton.isSelected()) {
                if (this.negativeButtonClickListener != null) {
                    this.negativeButton.performClick();
                }
            }
            else if (this.dialog != null) {
                this.dialog.dismiss();
            }
            this.unRegisterHandler();
        }
        
        private void right() {
            if (this.positiveButton.isSelected()) {
                this.negativeButton.setSelected(true);
                this.positiveButton.setSelected(false);
            }
        }
        
        public MyCustomDialog create() {
            final LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService("layout_inflater");
            this.dialog = new MyCustomDialog(this.context, 2131230726);
            final View inflate = layoutInflater.inflate(2130903057, (ViewGroup)null);
            this.dialog.addContentView(inflate, new ViewGroup$LayoutParams(-1, -2));
            this.positiveButton = (Button)inflate.findViewById(2131427359);
            this.negativeButton = (Button)inflate.findViewById(2131427360);
            this.button_layout = (LinearLayout)inflate.findViewById(2131427433);
            this.positiveButton.setSelected(true);
            if (this.positiveButtonText != null) {
                this.button_layout.setVisibility(0);
                this.positiveButton.setText((CharSequence)this.positiveButtonText);
                if (this.positiveButtonClickListener != null) {
                    this.positiveButton.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                        public void onClick(final View view) {
                            Builder.this.negativeButton.setSelected(false);
                            Builder.this.positiveButton.setSelected(true);
                            Builder.this.positiveButtonClickListener.onClick((DialogInterface)Builder.this.dialog, -1);
                            Builder.this.unRegisterHandler();
                        }
                    });
                }
            }
            else {
                this.button_layout.setVisibility(8);
                this.positiveButton.setVisibility(8);
            }
            if (this.negativeButtonText != null) {
                this.negativeButton.setText((CharSequence)this.negativeButtonText);
                if (this.negativeButtonClickListener != null) {
                    this.negativeButton.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                        public void onClick(final View view) {
                            Builder.this.negativeButton.setSelected(true);
                            Builder.this.positiveButton.setSelected(false);
                            Builder.this.negativeButtonClickListener.onClick((DialogInterface)Builder.this.dialog, -2);
                            Builder.this.unRegisterHandler();
                        }
                    });
                }
            }
            else {
                this.negativeButton.setVisibility(8);
            }
            if (this.message != null) {
                if (TextUtils.isEmpty((CharSequence)this.message)) {
                    ((TextView)inflate.findViewById(2131427358)).setVisibility(8);
                }
                ((TextView)inflate.findViewById(2131427358)).setText((CharSequence)this.message);
                if (this.messageNext != null) {
                    ((TextView)inflate.findViewById(2131427432)).setText((CharSequence)this.messageNext);
                }
            }
            else if (this.contentView != null) {
                ((LinearLayout)inflate.findViewById(2131427431)).removeAllViews();
                ((LinearLayout)inflate.findViewById(2131427431)).addView(this.contentView, new ViewGroup$LayoutParams(-1, -1));
            }
            this.dialog.setContentView(inflate);
            return this.dialog;
        }
        
        public Builder setContentView(final View contentView) {
            this.contentView = contentView;
            return this;
        }
        
        public Builder setMessage(final int n) {
            this.message = (String)this.context.getText(n);
            return this;
        }
        
        public Builder setMessage(final String message) {
            this.message = message;
            return this;
        }
        
        public Builder setNegativeButton(final int n, final DialogInterface$OnClickListener negativeButtonClickListener) {
            this.negativeButtonText = (String)this.context.getText(n);
            this.negativeButtonClickListener = negativeButtonClickListener;
            return this;
        }
        
        public Builder setNegativeButton(final String negativeButtonText, final DialogInterface$OnClickListener negativeButtonClickListener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = negativeButtonClickListener;
            return this;
        }
        
        public Builder setNextMessage(final String messageNext) {
            this.messageNext = messageNext;
            return this;
        }
        
        public Builder setPositiveButton(final int n, final DialogInterface$OnClickListener positiveButtonClickListener) {
            this.positiveButtonText = (String)this.context.getText(n);
            this.positiveButtonClickListener = positiveButtonClickListener;
            return this;
        }
        
        public Builder setPositiveButton(final String positiveButtonText, final DialogInterface$OnClickListener positiveButtonClickListener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = positiveButtonClickListener;
            return this;
        }
        
        public void unRegisterHandler() {
            if (this.app != null) {
                this.app.unregisterHandler(this.mHandler);
            }
        }
        
        static class MyCustomHandler extends Handler
        {
            private WeakReference<Builder> target;
            
            public MyCustomHandler(final Builder builder) {
                this.target = new WeakReference<Builder>(builder);
            }
            
            public void handleMessage(final Message message) {
                super.handleMessage(message);
                if (this.target.get() != null && message.what == 6001) {
                    this.target.get().handlerMsg(message);
                }
            }
        }
    }
}
