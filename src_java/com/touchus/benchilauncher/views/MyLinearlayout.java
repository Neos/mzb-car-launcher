package com.touchus.benchilauncher.views;

import android.widget.*;
import com.touchus.benchilauncher.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class MyLinearlayout extends LinearLayout
{
    private LauncherApplication app;
    private Context mcontext;
    
    public MyLinearlayout(final Context context) {
        this(context, null);
    }
    
    public MyLinearlayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MyLinearlayout(final Context mcontext, final AttributeSet set, final int n) {
        super(mcontext, set, n);
        this.mcontext = mcontext;
        this.app = (LauncherApplication)this.mcontext.getApplicationContext();
        Log.e("", "listenlog MyLinearlayout creat:");
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        return true;
    }
}
