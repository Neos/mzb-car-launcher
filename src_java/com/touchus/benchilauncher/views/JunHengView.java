package com.touchus.benchilauncher.views;

import android.view.*;
import android.annotation.*;
import android.graphics.drawable.*;
import android.text.format.*;
import android.content.*;
import android.util.*;
import com.touchus.benchilauncher.*;
import android.content.res.*;
import android.graphics.*;

@SuppressLint({ "HandlerLeak" })
public class JunHengView extends View
{
    private Drawable clockDrawable;
    private Thread clockThread;
    private Drawable hourDrawable;
    private boolean isChange;
    public boolean istuodong;
    private int mViewCenterX;
    private int mViewCenterY;
    int mun;
    private Paint paint;
    private Time time;
    
    public JunHengView(final Context context) {
        this(context, null);
    }
    
    public JunHengView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public JunHengView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.istuodong = false;
        this.mun = 0;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.MyClockStyleable, n, 0);
        this.clockDrawable = obtainStyledAttributes.getDrawable(0);
        this.hourDrawable = obtainStyledAttributes.getDrawable(1);
        obtainStyledAttributes.recycle();
        (this.paint = new Paint()).setColor(Color.parseColor("#000000"));
        this.paint.setTypeface(Typeface.DEFAULT_BOLD);
        this.paint.setFakeBoldText(true);
        this.paint.setAntiAlias(true);
        this.time = new Time();
        this.clockThread = new Thread() {
            @Override
            public void run() {
                while (JunHengView.this.isChange) {
                    JunHengView.this.postInvalidate();
                    try {
                        Thread.sleep(1000L);
                    }
                    catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.isChange = true;
        this.clockThread.start();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.isChange = false;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        this.time.setToNow();
        this.mViewCenterX = (this.getRight() - this.getLeft()) / 2;
        this.mViewCenterY = (this.getBottom() - this.getTop()) / 2;
        final Drawable clockDrawable = this.clockDrawable;
        final int intrinsicHeight = clockDrawable.getIntrinsicHeight();
        final int intrinsicWidth = clockDrawable.getIntrinsicWidth();
        if (this.getRight() - this.getLeft() < intrinsicWidth || this.getBottom() - this.getTop() < intrinsicHeight) {
            final float min = Math.min((this.getRight() - this.getLeft()) / intrinsicWidth, (this.getBottom() - this.getTop()) / intrinsicHeight);
            canvas.save();
            canvas.scale(min, min, (float)this.mViewCenterX, (float)this.mViewCenterY);
        }
        if (this.isChange) {
            clockDrawable.setBounds(this.mViewCenterX - intrinsicWidth / 2, this.mViewCenterY - intrinsicHeight / 2, this.mViewCenterX + intrinsicWidth / 2, this.mViewCenterY + intrinsicHeight / 2);
        }
        clockDrawable.draw(canvas);
        canvas.save();
        canvas.rotate(15.0f * this.setMun(this.mun), (float)this.mViewCenterX, (float)this.mViewCenterY);
        final Drawable hourDrawable = this.hourDrawable;
        final int intrinsicHeight2 = hourDrawable.getIntrinsicHeight();
        final int intrinsicWidth2 = hourDrawable.getIntrinsicWidth();
        if (this.isChange) {
            hourDrawable.setBounds(this.mViewCenterX - intrinsicWidth2 / 2, this.mViewCenterY - intrinsicHeight2 + 70, this.mViewCenterX + intrinsicWidth2 / 2, this.mViewCenterY + 70);
        }
        hourDrawable.draw(canvas);
        canvas.restore();
        canvas.save();
    }
    
    public int setMun(final int mun) {
        if (mun >= 0 && mun <= 10) {
            this.mun = mun;
        }
        else if (mun >= -10 && mun <= -1) {
            this.mun = mun + 24;
        }
        return this.mun;
    }
    
    public void settingCurrentClockPic(final boolean b) {
        if (b) {
            this.clockDrawable = this.getResources().getDrawable(2130837749);
            this.hourDrawable = this.getResources().getDrawable(2130837751);
            return;
        }
        this.clockDrawable = this.getResources().getDrawable(2130837750);
        this.hourDrawable = this.getResources().getDrawable(2130837752);
    }
}
