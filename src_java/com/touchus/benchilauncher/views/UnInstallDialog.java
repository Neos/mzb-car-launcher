package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.*;
import android.view.*;
import android.text.*;
import android.util.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.widget.*;

public class UnInstallDialog extends Dialog
{
    private LauncherApplication app;
    private String appName;
    private Button btnCancel;
    private Button btnConfirm;
    public int height;
    private Context mContext;
    private View$OnClickListener onClickListener;
    private String pkgName;
    public int width;
    
    public UnInstallDialog(final Context mContext) {
        super(mContext);
        this.width = 500;
        this.height = 200;
        this.pkgName = "";
        this.appName = "";
        this.onClickListener = (View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                UnInstallDialog.this.clickEvent(view);
            }
        };
        this.mContext = mContext;
        this.app = (LauncherApplication)mContext.getApplicationContext();
    }
    
    public UnInstallDialog(final Context mContext, final int n) {
        super(mContext, n);
        this.width = 500;
        this.height = 200;
        this.pkgName = "";
        this.appName = "";
        this.onClickListener = (View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                UnInstallDialog.this.clickEvent(view);
            }
        };
        this.mContext = mContext;
        this.app = (LauncherApplication)mContext.getApplicationContext();
    }
    
    private void clickEvent(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131427427: {
                this.btnCancel.setSelected(false);
                this.btnConfirm.setSelected(true);
                this.unstallApp();
                this.dismiss();
            }
            case 2131427459: {
                this.btnCancel.setSelected(true);
                this.btnConfirm.setSelected(false);
                this.dismiss();
            }
        }
    }
    
    private void unstallApp() {
        if (TextUtils.isEmpty((CharSequence)this.pkgName)) {
            Log.d("uninstall", "pkgName is empty.");
            return;
        }
        final Intent intent = new Intent();
        intent.setAction("android.intent.action.DELETE");
        intent.setData(Uri.parse("package:" + this.pkgName));
        this.mContext.startActivity(intent);
    }
    
    public void dismiss() {
        super.dismiss();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903065);
        final TextView textView = (TextView)this.findViewById(2131427454);
        this.btnConfirm = (Button)this.findViewById(2131427427);
        this.btnCancel = (Button)this.findViewById(2131427459);
        this.btnConfirm.setOnClickListener(this.onClickListener);
        this.btnCancel.setOnClickListener(this.onClickListener);
        this.btnConfirm.setText((CharSequence)this.mContext.getString(2131165277));
        textView.setText((CharSequence)String.format(this.mContext.getString(2131165278), this.appName));
        this.btnCancel.setText((CharSequence)this.mContext.getString(2131165274));
        this.btnCancel.setSelected(true);
        this.getWindow().setLayout(this.width, this.height);
    }
    
    public void setPkgName(final String pkgName, final String appName) {
        this.pkgName = pkgName;
        this.appName = appName;
    }
}
