package com.touchus.benchilauncher.views;

import android.os.*;

public abstract class LoopRotarySwitchViewHandler extends Handler
{
    public static final int msgid = 1000;
    private boolean loop;
    public long loopTime;
    private Message msg;
    
    public LoopRotarySwitchViewHandler(final int n) {
        this.loop = false;
        this.loopTime = 3000L;
        this.msg = this.createMsg();
        this.loopTime = n;
    }
    
    private void sendMsg() {
        while (true) {
            try {
                this.removeMessages(1000);
                this.sendMessageDelayed(this.msg = this.createMsg(), this.loopTime);
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
    
    public Message createMsg() {
        final Message message = new Message();
        message.what = 1000;
        return message;
    }
    
    public abstract void doScroll();
    
    public long getLoopTime() {
        return this.loopTime;
    }
    
    public void handleMessage(final Message message) {
        switch (message.what = 1000) {
            case 1000: {
                if (this.loop) {
                    this.doScroll();
                    this.sendMsg();
                    break;
                }
                break;
            }
        }
        super.handleMessage(message);
    }
    
    public boolean isLoop() {
        return this.loop;
    }
    
    public void setLoop(final boolean loop) {
        this.loop = loop;
        if (loop) {
            this.sendMsg();
            return;
        }
        try {
            this.removeMessages(1000);
        }
        catch (Exception ex) {}
    }
    
    public void setLoopTime(final long loopTime) {
        this.loopTime = loopTime;
    }
}
