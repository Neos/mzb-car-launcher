package com.touchus.benchilauncher.views;

import android.content.*;
import android.graphics.*;
import android.view.*;
import android.util.*;
import com.touchus.benchilauncher.utils.*;

public class CrossView extends View
{
    private Point cp;
    private boolean drawLine;
    private Boolean isCilkshizi;
    private Bitmap mBitmap;
    private Context mContext;
    private float[] mLineA;
    private float[] mLineB;
    private Paint paint;
    public float parantX;
    public float parantY;
    int sen;
    
    public CrossView(final Context mContext, final View view) {
        super(mContext);
        this.drawLine = true;
        this.sen = 50;
        this.isCilkshizi = false;
        this.mContext = mContext;
        this.init(view);
    }
    
    public float getPravX() {
        return this.cp.x;
    }
    
    public float getPravY() {
        return this.cp.y;
    }
    
    public void init(final View view) {
        if (this.paint == null || this.cp == null) {
            this.paint = new Paint();
            this.cp = new Point();
        }
        this.setFocusable(true);
        this.parantY = view.getHeight();
        this.parantX = view.getWidth();
        this.mLineA = new float[] { 0.0f, this.parantY / 2.0f, this.parantX, this.parantY / 2.0f };
        this.mLineB = new float[] { this.parantX / 2.0f, this.parantY, this.parantX / 2.0f, 0.0f };
        this.cp.x = (int)(this.parantX / 2.0f + 0.5f);
        this.cp.y = (int)(this.parantY / 2.0f + 0.5f);
    }
    
    public void onCilkCrossView(int x, int y) {
        final int n = (int)(this.parantX / 2.0f + x * 7);
        final int n2 = (int)(this.parantY / 2.0f + y * 9);
        if (n >= this.parantX) {
            x = (int)(this.parantX + 0.5f);
        }
        else if ((x = n) <= 0) {
            x = 0;
        }
        if (n2 >= this.parantY) {
            y = (int)(this.parantY + 0.5f);
        }
        else if ((y = n2) <= 0) {
            y = 0;
        }
        this.drawLine = true;
        this.cp.x = x;
        this.cp.y = y;
        this.mLineA[1] = y;
        this.mLineA[3] = y;
        this.mLineB[0] = x;
        this.mLineB[2] = x;
        this.invalidate();
    }
    
    protected void onDraw(final Canvas canvas) {
        this.paint.setAntiAlias(true);
        this.paint.setDither(true);
        this.paint.setStyle(Paint$Style.FILL);
        this.paint.setStrokeJoin(Paint$Join.ROUND);
        if (this.drawLine) {
            this.paint.setColor(-1);
            this.paint.setStrokeWidth(5.0f);
        }
        canvas.drawLine(this.mLineA[0], this.mLineA[1], this.mLineA[2], this.mLineA[3], this.paint);
        canvas.drawLine(this.mLineB[0], this.mLineB[1], this.mLineB[2], this.mLineB[3], this.paint);
        this.paint.setColor(-1);
        if (this.isCilkshizi) {
            this.mBitmap = BitmapFactory.decodeResource(this.getResources(), 2130838035);
        }
        else {
            this.mBitmap = BitmapFactory.decodeResource(this.getResources(), 2130838036);
        }
        canvas.drawBitmap(this.mBitmap, (float)(this.cp.x - 16), (float)(this.cp.y - 17), this.paint);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final int n = (int)(motionEvent.getX() + 0.5f);
        final int n2 = (int)(motionEvent.getY() + 0.5f);
        switch (motionEvent.getAction()) {
            case 0:
            case 2: {
                this.isCilkshizi = true;
                Log.d("XY: ", "X" + n + "-----" + "Y" + n2);
                int x;
                if (n >= this.parantX) {
                    x = (int)this.parantX;
                }
                else if ((x = n) <= 0) {
                    x = 0;
                }
                int y;
                if (n2 >= this.parantY) {
                    y = (int)this.parantY;
                }
                else if ((y = n2) <= 0) {
                    y = 0;
                }
                this.drawLine = false;
                this.cp.x = x;
                this.cp.y = y;
                this.mLineA[1] = y;
                this.mLineA[3] = y;
                this.mLineB[0] = x;
                this.mLineB[2] = x;
                break;
            }
            case 1: {
                this.drawLine = true;
                this.isCilkshizi = false;
                break;
            }
        }
        this.invalidate();
        return true;
    }
    
    public void setIsCilkshizi(final Boolean isCilkshizi) {
        this.isCilkshizi = isCilkshizi;
        new SpUtilK(this.mContext).putBoolean("isCilkshizi", this.isCilkshizi);
    }
}
