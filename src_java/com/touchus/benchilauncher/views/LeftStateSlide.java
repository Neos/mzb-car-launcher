package com.touchus.benchilauncher.views;

import com.touchus.benchilauncher.base.*;
import android.content.*;
import android.util.*;
import android.view.*;
import a_vcard.android.util.*;
import com.touchus.benchilauncher.*;

public class LeftStateSlide extends AbstraSlide
{
    public static int mChildWith;
    public static int mPage;
    public boolean iHandleTouchEvent;
    public float mDownX;
    public float mDownY;
    private boolean mScrolling;
    public int mSpace;
    public float mStartX;
    
    static {
        LeftStateSlide.mChildWith = 475;
        LeftStateSlide.mPage = 0;
    }
    
    public LeftStateSlide(final Context context, final AttributeSet set) {
        super(context, set);
        this.mSpace = 20;
        this.iHandleTouchEvent = true;
    }
    
    public void changeToCurrentView() {
        this.scrollTo(LeftStateSlide.mChildWith * LeftStateSlide.mPage, 0);
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0: {
                this.mStartX = motionEvent.getX();
                this.mDownX = motionEvent.getX();
                this.mScrolling = false;
                break;
            }
            case 2: {
                if (Math.abs(this.mStartX - motionEvent.getX()) >= ViewConfiguration.get(this.getContext()).getScaledTouchSlop()) {
                    this.mScrolling = true;
                    break;
                }
                this.mScrolling = false;
                break;
            }
            case 1: {
                this.mScrolling = false;
                break;
            }
        }
        return this.mScrolling;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        Label_0007: {
            if (this.iHandleTouchEvent) {
                switch (motionEvent.getAction()) {
                    default: {
                        return true;
                    }
                    case 0: {
                        this.mStartX = motionEvent.getX();
                        this.mDownX = motionEvent.getX();
                        this.mDownY = motionEvent.getY();
                        return true;
                    }
                    case 2: {
                        final float x = motionEvent.getX();
                        final int n = (int)(this.mDownX - x + 0.5f);
                        final int n2 = this.getScrollX() + n;
                        Log.e("ACTION_MOVE", "ACTION_ moveX = " + x + ",diffX = " + n + ",finalX = " + n2);
                        if (n2 < -20) {
                            this.scrollTo(-20, 0);
                            return true;
                        }
                        if (n2 > LeftStateSlide.mChildWith * (LauncherApplication.pageCount - 1) + 20) {
                            this.scrollTo(LeftStateSlide.mChildWith * (LauncherApplication.pageCount - 1) + 20, 0);
                            return true;
                        }
                        this.scrollBy(n, 0);
                        this.mDownX = x;
                        return true;
                    }
                    case 1: {
                        final float n3 = this.mDownX - this.mStartX;
                        Log.e("ACTION_UP", "ACTION_ distance = " + n3 + ",mPage = " + LeftStateSlide.mPage);
                        switch (LeftStateSlide.mPage) {
                            default: {
                                return true;
                            }
                            case 0: {
                                if (n3 > -this.mSpace) {
                                    this.smoothScrollXTo(0, 100);
                                    return true;
                                }
                                if (n3 < -this.mSpace) {
                                    this.smoothScrollXTo(LeftStateSlide.mChildWith * 1, 300);
                                    LeftStateSlide.mPage = 1;
                                    return true;
                                }
                                break Label_0007;
                            }
                            case 1: {
                                if (n3 > this.mSpace) {
                                    this.smoothScrollXTo(0, 300);
                                    LeftStateSlide.mPage = 0;
                                    return true;
                                }
                                if (n3 < -this.mSpace) {
                                    LeftStateSlide.mPage = 2;
                                    this.smoothScrollXTo(LeftStateSlide.mChildWith * 2, 100);
                                    return true;
                                }
                                if (n3 < this.mSpace) {
                                    this.smoothScrollXTo(LeftStateSlide.mChildWith * 1, 100);
                                    return true;
                                }
                                break Label_0007;
                            }
                            case 2: {
                                if (n3 > this.mSpace) {
                                    this.smoothScrollXTo(LeftStateSlide.mChildWith * 1, 300);
                                    LeftStateSlide.mPage = 1;
                                    return true;
                                }
                                if (n3 < this.mSpace) {
                                    this.smoothScrollXTo(LeftStateSlide.mChildWith * 2, 100);
                                    return true;
                                }
                                break Label_0007;
                            }
                        }
                        break;
                    }
                }
            }
        }
        return true;
    }
}
