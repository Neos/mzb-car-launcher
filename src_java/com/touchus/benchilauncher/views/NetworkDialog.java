package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.adapter.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.bean.*;
import java.util.*;
import com.backaudio.android.driver.*;
import android.view.*;
import com.touchus.publicutils.utils.*;
import com.touchus.benchilauncher.utils.*;
import android.text.*;
import android.widget.*;
import android.os.*;
import android.util.*;
import android.net.wifi.*;
import java.lang.ref.*;
import android.content.*;

public class NetworkDialog extends Dialog implements View$OnClickListener, AdapterView$OnItemClickListener, CompoundButton$OnCheckedChangeListener
{
    private int CHECK_WIFI_SPACE;
    private AdpWifiAdapter adapter;
    private LauncherApplication app;
    private int clickCount;
    private Context context;
    private String currentApName;
    private String currentConnectingWifiSsid;
    private String currentPwd;
    private TextView data;
    private TextView dataTview;
    private ImageView data_switch;
    private CheckBox hadPwdCbox;
    private boolean iDataOpen;
    private boolean iWifiOpen;
    private boolean iWlanOpen;
    private ArrayList<BeanWifi> list;
    public MHandler mHandler;
    private ListView mListView;
    private TextView noListDataTview;
    private Runnable refreshWifiListRunnable;
    private Button submitBtn;
    private WifiBroadCastReceiver wifiReceiver;
    private TextView wifi_ap;
    private ImageView wifi_ap_switch;
    private RelativeLayout wifi_apset_rl;
    private EditText wifi_name_et;
    private TextView wifi_pwd;
    private EditText wifi_pwd_et;
    private TextView wlan;
    private ImageView wlan_switch;
    
    public NetworkDialog(final Context context) {
        super(context);
        this.clickCount = 0;
        this.iDataOpen = false;
        this.iWlanOpen = false;
        this.iWifiOpen = false;
        this.refreshWifiListRunnable = new Runnable() {
            @Override
            public void run() {
                NetworkDialog.this.refreshWifiListViewByConnected();
                if (NetworkDialog.this.iWlanOpen) {
                    NetworkDialog.this.mHandler.postDelayed(NetworkDialog.this.refreshWifiListRunnable, 5000L);
                    return;
                }
                NetworkDialog.this.mHandler.removeCallbacks(NetworkDialog.this.refreshWifiListRunnable);
            }
        };
        this.CHECK_WIFI_SPACE = 20000;
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    public NetworkDialog(final Context context, final int n) {
        super(context, n);
        this.clickCount = 0;
        this.iDataOpen = false;
        this.iWlanOpen = false;
        this.iWifiOpen = false;
        this.refreshWifiListRunnable = new Runnable() {
            @Override
            public void run() {
                NetworkDialog.this.refreshWifiListViewByConnected();
                if (NetworkDialog.this.iWlanOpen) {
                    NetworkDialog.this.mHandler.postDelayed(NetworkDialog.this.refreshWifiListRunnable, 5000L);
                    return;
                }
                NetworkDialog.this.mHandler.removeCallbacks(NetworkDialog.this.refreshWifiListRunnable);
            }
        };
        this.CHECK_WIFI_SPACE = 20000;
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    private void enterToWifiSettingView() {
        this.mHandler.removeCallbacks(this.refreshWifiListRunnable);
        this.mHandler.postDelayed(this.refreshWifiListRunnable, 200L);
    }
    
    private void refreshWifiListViewByConnected() {
        this.list.clear();
        this.list.addAll(WifiTool.getAllCanConectWifi(this.context));
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
    }
    
    private void refreshWifiListViewByConnecting(final String currentConnectingWifiSsid) {
        final int firstVisiblePosition = this.mListView.getFirstVisiblePosition();
        this.list = this.adapter.getListData();
        for (final BeanWifi beanWifi : this.list) {
            if (currentConnectingWifiSsid.equals(beanWifi.getName())) {
                beanWifi.setState(2);
            }
            else {
                beanWifi.setState(3);
            }
        }
        this.adapter.notifyDataSetInvalidated();
        this.mHandler.postDelayed((Runnable)new CheckWifiRunnable(currentConnectingWifiSsid), (long)this.CHECK_WIFI_SPACE);
        this.currentConnectingWifiSsid = currentConnectingWifiSsid;
        this.mListView.setSelection(firstVisiblePosition);
    }
    
    private void refreshWifiListViewByDisconnect() {
        final int firstVisiblePosition = this.mListView.getFirstVisiblePosition();
        this.list = this.adapter.getListData();
        for (final BeanWifi beanWifi : this.list) {
            if (beanWifi.getState() != 3) {
                beanWifi.setState(3);
            }
        }
        this.adapter.notifyDataSetInvalidated();
        this.mListView.setSelection(firstVisiblePosition);
    }
    
    private void refreshWifiSettingView() {
        if (!this.iWlanOpen || this.context == null || this.mListView == null || this.list.size() == 0) {
            return;
        }
    Label_0076:
        while (true) {
            if (!(this.mListView.getAdapter().getItem(0) instanceof BeanWifi)) {
                break Label_0076;
            }
            while (true) {
                try {
                    if (((WifiManager)this.context.getSystemService("wifi")).getConnectionInfo().getNetworkId() < 0) {
                        this.refreshWifiListViewByDisconnect();
                    }
                    else {
                        this.refreshWifiListViewByConnected();
                    }
                    this.currentConnectingWifiSsid = "";
                    return;
                }
                catch (Exception ex) {
                    continue Label_0076;
                }
                continue Label_0076;
            }
            break;
        }
    }
    
    private void registerWifiReceiver() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.wifiReceiver = new WifiBroadCastReceiver();
        this.context.registerReceiver((BroadcastReceiver)this.wifiReceiver, intentFilter);
    }
    
    private void setSwitchImage(final ImageView imageView, final boolean b) {
        if (b) {
            imageView.setImageResource(2130837754);
            return;
        }
        imageView.setImageResource(2130837753);
    }
    
    private void showWifiConfigDialog(final BeanWifi currentWifiInfo) {
        final DlgWifiRelaDialog dlgWifiRelaDialog = new DlgWifiRelaDialog(this.context, 2131230726);
        dlgWifiRelaDialog.parent = this;
        dlgWifiRelaDialog.width = 500;
        dlgWifiRelaDialog.height = 200;
        dlgWifiRelaDialog.currentWifiInfo = currentWifiInfo;
        dlgWifiRelaDialog.show();
    }
    
    private void unregisterWifiReceiver() {
        try {
            this.context.unregisterReceiver((BroadcastReceiver)this.wifiReceiver);
        }
        catch (Exception ex) {}
    }
    
    public void connectingToSpecifyWifi(final String s) {
        this.refreshWifiListViewByConnecting(s);
    }
    
    public void disConnectCurWifi() {
        WifiTool.disConnectWifi(this.context);
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.BACK.getCode()) {
                this.dismiss();
            }
            else if (byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                this.dismiss();
            }
        }
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        if (b) {
            this.wifi_pwd_et.setEnabled(false);
            this.currentPwd = this.wifi_pwd_et.getText().toString();
            this.wifi_pwd_et.setHint((CharSequence)"");
            this.wifi_pwd_et.setText((CharSequence)"");
            this.wifi_pwd.setVisibility(8);
            this.wifi_pwd_et.setVisibility(8);
            return;
        }
        this.wifi_pwd.setVisibility(0);
        this.wifi_pwd_et.setVisibility(0);
        this.wifi_pwd_et.setEnabled(true);
        this.wifi_pwd_et.setHint((CharSequence)this.context.getString(2131165316));
        this.wifi_pwd_et.setText((CharSequence)this.currentPwd);
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131427415: {
                this.dataTview.setVisibility(0);
                if (this.iDataOpen) {
                    this.dataTview.setText((CharSequence)this.context.getString(2131165338));
                }
                else {
                    this.dataTview.setText((CharSequence)this.context.getString(2131165337));
                }
                this.wifi_apset_rl.setVisibility(8);
                this.mListView.setVisibility(8);
                this.noListDataTview.setVisibility(8);
            }
            case 2131427417: {
                if (this.iWlanOpen) {
                    this.mListView.setVisibility(0);
                    this.noListDataTview.setVisibility(8);
                }
                else {
                    this.mListView.setVisibility(8);
                    this.noListDataTview.setVisibility(0);
                }
                this.wifi_apset_rl.setVisibility(8);
                this.dataTview.setVisibility(8);
            }
            case 2131427419: {
                if (this.iWifiOpen) {
                    this.wifi_apset_rl.setVisibility(0);
                    this.noListDataTview.setVisibility(8);
                }
                else {
                    this.wifi_apset_rl.setVisibility(8);
                    this.noListDataTview.setVisibility(0);
                }
                this.mListView.setVisibility(8);
                this.dataTview.setVisibility(8);
            }
            case 2131427416: {
                this.dataTview.setVisibility(0);
                if (this.iDataOpen) {
                    this.iDataOpen = false;
                    this.dataTview.setText((CharSequence)this.context.getString(2131165337));
                }
                else {
                    this.iDataOpen = true;
                    this.dataTview.setText((CharSequence)this.context.getString(2131165338));
                }
                MobileDataSwitcher.setMobileData(this.context, this.iDataOpen);
                this.setSwitchImage(this.data_switch, this.iDataOpen);
                this.wifi_apset_rl.setVisibility(8);
                this.mListView.setVisibility(8);
                this.noListDataTview.setVisibility(8);
                this.wifi_apset_rl.setVisibility(8);
            }
            case 2131427418: {
                if (this.iWlanOpen) {
                    this.iWlanOpen = false;
                    this.mListView.setVisibility(8);
                    this.noListDataTview.setVisibility(0);
                    this.unregisterWifiReceiver();
                }
                else {
                    this.iWlanOpen = true;
                    this.iWifiOpen = false;
                    this.mListView.setVisibility(0);
                    this.noListDataTview.setVisibility(8);
                    this.registerWifiReceiver();
                    this.app.setWifiApState(false);
                }
                WifiTool.setWifiState(this.context, this.iWlanOpen);
                this.enterToWifiSettingView();
                this.wifi_apset_rl.setVisibility(8);
                this.dataTview.setVisibility(8);
                this.setSwitchImage(this.wlan_switch, this.iWlanOpen);
                this.setSwitchImage(this.wifi_ap_switch, this.iWifiOpen);
            }
            case 2131427420: {
                if (this.iWifiOpen) {
                    this.iWifiOpen = false;
                    WifiTool.closeWifiAp(this.context);
                    this.wifi_apset_rl.setVisibility(8);
                    this.noListDataTview.setVisibility(0);
                }
                else {
                    this.iWifiOpen = true;
                    this.iWlanOpen = false;
                    this.unregisterWifiReceiver();
                    this.wifi_apset_rl.setVisibility(0);
                    this.noListDataTview.setVisibility(8);
                    WifiTool.openWifiAp(this.context, WifiTool.getCurrentWifiApConfig(this.context));
                    WifiTool.setWifiState(this.context, this.iWlanOpen);
                }
                this.app.setWifiApState(this.iWifiOpen);
                this.mListView.setVisibility(8);
                this.dataTview.setVisibility(8);
                this.setSwitchImage(this.wlan_switch, this.iWlanOpen);
                this.setSwitchImage(this.wifi_ap_switch, this.iWifiOpen);
            }
            case 2131427427: {
                final String string = this.wifi_name_et.getText().toString();
                String string2 = this.wifi_pwd_et.getText().toString();
                if (!UtilTools.stringFilter(string)) {
                    ToastTool.showLongToast(this.context, this.context.getString(2131165311));
                    return;
                }
                if (TextUtils.isEmpty((CharSequence)string)) {
                    ToastTool.showLongToast(this.context, this.context.getString(2131165312));
                    return;
                }
                if (!this.hadPwdCbox.isChecked() && string2.length() < 8) {
                    ToastTool.showLongToast(this.context, this.context.getString(2131165325));
                    return;
                }
                if (this.hadPwdCbox.isChecked()) {
                    this.app.setWifiApHasPwd(true);
                    string2 = "";
                }
                else {
                    this.app.setWifiApHasPwd(false);
                }
                this.app.setWifiApIdAndKey(new String[] { string, string2 });
                if (WifiTool.iWifiApIsOpen(this.context)) {
                    WifiTool.closeWifiAp(this.context);
                    WifiTool.updateWifiAp(this.context, string, string2);
                    WifiTool.openWifiAp(this.context, WifiTool.getCurrentWifiApConfig(this.context));
                }
                else {
                    WifiTool.updateWifiAp(this.context, string, string2);
                }
                ToastTool.showLongToast(this.context, this.context.getString(2131165310));
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setContentView(2130903056);
        this.app = (LauncherApplication)this.context.getApplicationContext();
        this.wlan = (TextView)this.findViewById(2131427417);
        this.wifi_ap = (TextView)this.findViewById(2131427419);
        this.data = (TextView)this.findViewById(2131427415);
        this.wlan_switch = (ImageView)this.findViewById(2131427418);
        this.wifi_ap_switch = (ImageView)this.findViewById(2131427420);
        this.data_switch = (ImageView)this.findViewById(2131427416);
        this.wifi_apset_rl = (RelativeLayout)this.findViewById(2131427421);
        this.wifi_name_et = (EditText)this.findViewById(2131427423);
        this.wifi_pwd = (TextView)this.findViewById(2131427424);
        this.wifi_pwd_et = (EditText)this.findViewById(2131427425);
        this.hadPwdCbox = (CheckBox)this.findViewById(2131427426);
        this.submitBtn = (Button)this.findViewById(2131427427);
        this.mListView = (ListView)this.findViewById(2131427428);
        this.noListDataTview = (TextView)this.findViewById(2131427429);
        this.dataTview = (TextView)this.findViewById(2131427430);
        this.wlan.setOnClickListener((View$OnClickListener)this);
        this.wifi_ap.setOnClickListener((View$OnClickListener)this);
        this.data.setOnClickListener((View$OnClickListener)this);
        this.wlan_switch.setOnClickListener((View$OnClickListener)this);
        this.wifi_ap_switch.setOnClickListener((View$OnClickListener)this);
        this.data_switch.setOnClickListener((View$OnClickListener)this);
        this.wifi_name_et.setOnClickListener((View$OnClickListener)this);
        this.wifi_pwd_et.setOnClickListener((View$OnClickListener)this);
        this.hadPwdCbox.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
        this.submitBtn.setOnClickListener((View$OnClickListener)this);
        this.iDataOpen = MobileDataSwitcher.getMobileDataState(this.context);
        if (this.iDataOpen) {
            this.dataTview.setText((CharSequence)this.context.getString(2131165338));
        }
        else {
            this.dataTview.setText((CharSequence)this.context.getString(2131165337));
        }
        this.setSwitchImage(this.data_switch, this.iDataOpen);
        final String[] wifiApIdAndKey = this.app.getWifiApIdAndKey();
        this.wifi_name_et.setText((CharSequence)wifiApIdAndKey[0]);
        this.wifi_pwd_et.setText((CharSequence)wifiApIdAndKey[1]);
        this.iWlanOpen = WifiTool.iWifiState(this.context);
        if (this.iWlanOpen) {
            this.app.setWifiApState(false);
        }
        this.iWifiOpen = this.app.getWifiApState();
        this.setSwitchImage(this.wlan_switch, this.iWlanOpen);
        this.setSwitchImage(this.wifi_ap_switch, this.iWifiOpen);
        this.noListDataTview.setVisibility(0);
        if (this.iWlanOpen) {
            this.wifi_apset_rl.setVisibility(8);
            this.mListView.setVisibility(0);
            this.noListDataTview.setVisibility(8);
            this.dataTview.setVisibility(8);
        }
        else if (this.iWifiOpen) {
            this.wifi_apset_rl.setVisibility(0);
            this.mListView.setVisibility(8);
            this.noListDataTview.setVisibility(8);
            this.dataTview.setVisibility(8);
        }
        else if (this.iDataOpen) {
            this.wifi_apset_rl.setVisibility(8);
            this.mListView.setVisibility(8);
            this.noListDataTview.setVisibility(8);
            this.dataTview.setVisibility(0);
        }
        if (this.app.getWifiApHasPwd()) {
            this.hadPwdCbox.setChecked(true);
            this.wifi_pwd.setVisibility(8);
            this.wifi_pwd_et.setVisibility(8);
        }
        else {
            this.hadPwdCbox.setChecked(false);
            this.wifi_pwd.setVisibility(0);
            this.wifi_pwd_et.setVisibility(0);
        }
        this.mListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        this.list = new ArrayList<BeanWifi>();
        this.adapter = new AdpWifiAdapter(this.context, this.list);
        this.mListView.setAdapter((ListAdapter)this.adapter);
        this.registerWifiReceiver();
        this.enterToWifiSettingView();
        this.getWindow().setSoftInputMode(18);
        super.onCreate(bundle);
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        final BeanWifi beanWifi = (BeanWifi)this.mListView.getAdapter().getItem(n);
        if (TextUtils.isEmpty((CharSequence)beanWifi.getName()) || beanWifi.getState() == 2) {
            return;
        }
        if (beanWifi.getState() != 3 || beanWifi.isPwd()) {
            this.showWifiConfigDialog(beanWifi);
            return;
        }
        this.disConnectCurWifi();
        final WifiConfiguration createWifiInfo = WifiTool.CreateWifiInfo(this.context, beanWifi.getName(), "", WifiTool.getCurrentWifiPwdType(beanWifi.getCapabilities()));
        if (!WifiTool.connectToSpecifyWifi(this.context, createWifiInfo)) {
            ToastTool.showBigShortToast(this.context, String.valueOf(this.context.getString(2131165318)) + createWifiInfo.SSID);
            return;
        }
        this.connectingToSpecifyWifi(beanWifi.getName());
    }
    
    protected void onStart() {
        this.app.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mHandler.removeCallbacksAndMessages((Object)null);
        this.app.unregisterHandler(this.mHandler);
        this.unregisterWifiReceiver();
        super.onStop();
    }
    
    public void updateWifiList() {
        this.refreshWifiListViewByDisconnect();
    }
    
    class CheckWifiRunnable implements Runnable
    {
        private String ConnectingWifiSsid;
        
        public CheckWifiRunnable(final String connectingWifiSsid) {
            this.ConnectingWifiSsid = connectingWifiSsid;
        }
        
        @Override
        public void run() {
            if (NetworkDialog.this.context != null && this.ConnectingWifiSsid.equals(NetworkDialog.this.currentConnectingWifiSsid)) {
                final WifiInfo connectionInfo = ((WifiManager)NetworkDialog.this.context.getSystemService("wifi")).getConnectionInfo();
                Log.d("launcherlog", "CheckWifiRunnable1 " + connectionInfo.getSSID());
                if (connectionInfo.getSSID().length() > 2) {
                    NetworkDialog.this.mHandler.postDelayed((Runnable)new CheckWifiRunnable(this.ConnectingWifiSsid), 2000L);
                    return;
                }
                final WifiConfiguration hadSaveWifiConfig = WifiTool.getHadSaveWifiConfig(NetworkDialog.this.context, NetworkDialog.this.currentConnectingWifiSsid);
                if (hadSaveWifiConfig != null) {
                    Log.d("launcherlog", "CheckWifiRunnable1 " + hadSaveWifiConfig.status + " " + hadSaveWifiConfig.disableReason);
                    NetworkDialog.this.refreshWifiListViewByDisconnect();
                    if (hadSaveWifiConfig.disableReason == 3) {
                        ToastTool.showLongToast(NetworkDialog.this.context, String.valueOf(NetworkDialog.this.context.getString(2131165314)) + NetworkDialog.this.currentConnectingWifiSsid);
                        WifiTool.removeWifiConfig(NetworkDialog.this.context, hadSaveWifiConfig.networkId);
                        return;
                    }
                    ToastTool.showLongToast(NetworkDialog.this.context, String.valueOf(NetworkDialog.this.context.getString(2131165315)) + this.ConnectingWifiSsid);
                }
            }
        }
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<NetworkDialog> target;
        
        public MHandler(final NetworkDialog networkDialog) {
            this.target = new WeakReference<NetworkDialog>(networkDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
    
    class WifiBroadCastReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            NetworkDialog.this.refreshWifiSettingView();
        }
    }
}
