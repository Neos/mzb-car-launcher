package com.touchus.benchilauncher.views;

import com.touchus.benchilauncher.*;
import android.content.*;
import com.touchus.benchilauncher.inface.*;
import android.util.*;
import android.widget.*;
import android.view.*;

public class SoundSetView extends RelativeLayout implements View$OnClickListener
{
    public static int MIXPRO;
    public static int SOUND;
    private LauncherApplication app;
    private String baseType;
    private Context context;
    private SeekBar mSoundSeekBar;
    private TextView mSoundTv;
    private int max;
    private int num;
    private OnSoundClickListener onSoundClickListener;
    private ImageView sound_add;
    private ImageView sound_jianjian;
    private int type;
    
    static {
        SoundSetView.SOUND = 0;
        SoundSetView.MIXPRO = 1;
    }
    
    public SoundSetView(final Context context) {
        this(context, null);
    }
    
    public SoundSetView(final Context context, final AttributeSet set) {
        super(context, set);
        this.max = 10;
        this.num = 10;
        this.type = 0;
        this.baseType = "\u97f3\u91cf";
        this.context = context;
        this.app = (LauncherApplication)context.getApplicationContext();
        ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(2130903104, (ViewGroup)this);
        this.mSoundTv = (TextView)this.findViewById(2131427435);
        this.sound_jianjian = (ImageView)this.findViewById(2131427436);
        this.mSoundSeekBar = (SeekBar)this.findViewById(2131427437);
        this.sound_add = (ImageView)this.findViewById(2131427438);
        this.sound_jianjian.setOnClickListener((View$OnClickListener)this);
        this.sound_add.setOnClickListener((View$OnClickListener)this);
        this.mSoundSeekBar.setMax(this.max);
        this.mSoundSeekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener() {
            public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
                if (n < 1) {
                    SoundSetView.this.mSoundSeekBar.setProgress(1);
                }
                else {
                    SoundSetView.access$1(SoundSetView.this, n);
                    if (SoundSetView.this.type == SoundSetView.SOUND) {
                        SoundSetView.this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(SoundSetView.this.num)).toString());
                    }
                    else {
                        SoundSetView.this.mSoundTv.setText((CharSequence)(String.valueOf(SoundSetView.this.num * 10) + "%"));
                    }
                    if (SoundSetView.this.onSoundClickListener != null) {
                        SoundSetView.this.onSoundClickListener.click(SoundSetView.this.num);
                    }
                }
            }
            
            public void onStartTrackingTouch(final SeekBar seekBar) {
            }
            
            public void onStopTrackingTouch(final SeekBar seekBar) {
            }
        });
    }
    
    static /* synthetic */ void access$1(final SoundSetView soundSetView, final int num) {
        soundSetView.num = num;
    }
    
    public void addSoundValue() {
        this.sound_add.performClick();
    }
    
    public void decSoundValue() {
        this.sound_jianjian.performClick();
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131427436: {
                if (this.num > 1) {
                    --this.num;
                    break;
                }
                break;
            }
            case 2131427438: {
                if (this.num < this.max) {
                    ++this.num;
                    break;
                }
                break;
            }
        }
        this.setSoundValue(this.num, this.type);
        this.onSoundClickListener.click(this.num);
    }
    
    public void setOnSoundClickListener(final OnSoundClickListener onSoundClickListener) {
        this.onSoundClickListener = onSoundClickListener;
    }
    
    public void setSoundValue(final int num, final int type) {
        this.type = type;
        this.num = num;
        if (type == SoundSetView.SOUND) {
            this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(this.num)).toString());
        }
        else {
            this.mSoundTv.setText((CharSequence)(String.valueOf(this.num * 10) + "%"));
        }
        this.mSoundSeekBar.setProgress(this.num);
    }
    
    public void setSpeakText(final String baseType) {
        this.baseType = baseType;
    }
}
