package com.touchus.benchilauncher.views;

import android.app.*;
import android.widget.*;
import android.content.*;
import com.backaudio.android.driver.*;
import android.view.*;
import com.touchus.publicutils.utils.*;
import android.util.*;
import android.text.*;
import com.touchus.benchilauncher.*;
import com.touchus.publicutils.sysconst.*;
import android.os.*;
import java.lang.ref.*;

public class XitongDialog extends Dialog implements View$OnClickListener
{
    private TextView androidTview;
    private LauncherApplication app;
    private TextView canboxTview;
    private int clickCount;
    private LinearLayout clickLayout;
    private Context context;
    public MHandler mHandler;
    private TextView mcuTview;
    Thread resetClickCountThread;
    private TextView snTview;
    private TextView systemTview;
    private TextView typeTview;
    private String url;
    
    public XitongDialog(final Context context) {
        super(context);
        this.clickCount = 0;
        this.url = "";
        this.resetClickCountThread = new Thread(new Runnable() {
            @Override
            public void run() {
                XitongDialog.access$0(XitongDialog.this, 0);
            }
        });
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    public XitongDialog(final Context context, final int n) {
        super(context, n);
        this.clickCount = 0;
        this.url = "";
        this.resetClickCountThread = new Thread(new Runnable() {
            @Override
            public void run() {
                XitongDialog.access$0(XitongDialog.this, 0);
            }
        });
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    static /* synthetic */ void access$0(final XitongDialog xitongDialog, final int clickCount) {
        xitongDialog.clickCount = clickCount;
    }
    
    static /* synthetic */ void access$3(final XitongDialog xitongDialog, final String url) {
        xitongDialog.url = url;
    }
    
    private void showDialog() {
        ((FeedbackDialog)(this.app.currentDialog3 = new FeedbackDialog(this.context, 2131230726))).show();
    }
    
    private void showDialog(final int currentType) {
        final UpdateDialog currentDialog3 = new UpdateDialog(this.context, 2131230726);
        currentDialog3.currentType = currentType;
        ((UpdateDialog)(this.app.currentDialog3 = currentDialog3)).show();
    }
    
    private void showDialog(final int currentType, final String url) {
        final UpdateDialog currentDialog3 = new UpdateDialog(this.context, 2131230726);
        currentDialog3.currentType = currentType;
        currentDialog3.url = url;
        ((UpdateDialog)(this.app.currentDialog3 = currentDialog3)).show();
    }
    
    public String getVersion() {
        return "SYSTEM\uff1aV" + Build.DISPLAY;
    }
    
    public void handlerMsgUSB(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 != Mainboard.EIdriverEnum.TURN_RIGHT.getCode() && byte1 != Mainboard.EIdriverEnum.RIGHT.getCode() && byte1 != Mainboard.EIdriverEnum.TURN_LEFT.getCode() && byte1 != Mainboard.EIdriverEnum.LEFT.getCode() && (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode())) {
                this.dismiss();
            }
        }
        else {
            if (message.what == 1131) {
                this.mcuTview.setText((CharSequence)("MCU\uff1a" + this.app.curMcuVersion));
                return;
            }
            if (message.what == 1121 || message.what == 1127) {
                this.canboxTview.setText((CharSequence)("CAN\uff1a" + this.app.curCanboxVersion));
                return;
            }
            if (message.what == 1125) {
                this.showDialog(5, this.url);
            }
        }
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427460) {
            this.clickCount = 0;
            this.showDialog();
        }
        else {
            if (view.getId() == 2131427461) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (UtilTools.checkURL(XitongDialog.this.context, String.format("http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh", XitongDialog.this.app.curMcuVersion))) {
                            XitongDialog.access$3(XitongDialog.this, String.format("http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh", XitongDialog.this.app.curMcuVersion));
                        }
                        else if (UtilTools.checkURL(XitongDialog.this.context, String.format("http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh", UtilTools.getIMEI(XitongDialog.this.context)))) {
                            XitongDialog.access$3(XitongDialog.this, String.format("http://smartpie-update.gz.bcebos.com/c200_en_app/%1$s.sh", UtilTools.getIMEI(XitongDialog.this.context)));
                        }
                        else {
                            Log.e("UpdateDialog", "error url");
                        }
                        if (!TextUtils.isEmpty((CharSequence)XitongDialog.this.url)) {
                            XitongDialog.this.mHandler.sendEmptyMessage(1125);
                        }
                    }
                }).start();
                return;
            }
            if (view.getId() != 2131427401) {
                if (view.getId() == 2131427462) {
                    this.clickCount = 0;
                    this.showDialog(4);
                    return;
                }
                if (view.getId() == 2131427463) {
                    this.clickCount = 0;
                    this.showDialog(1);
                    return;
                }
                if (view.getId() == 2131427464) {
                    this.clickCount = 0;
                    this.showDialog(2);
                    return;
                }
                if (view.getId() == 2131427465) {
                    this.clickCount = 0;
                    this.showDialog(3);
                }
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setContentView(2130903066);
        this.app = (LauncherApplication)this.context.getApplicationContext();
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131427401);
        this.typeTview = (TextView)this.findViewById(2131427460);
        this.snTview = (TextView)this.findViewById(2131427461);
        this.androidTview = (TextView)this.findViewById(2131427462);
        this.systemTview = (TextView)this.findViewById(2131427463);
        this.mcuTview = (TextView)this.findViewById(2131427464);
        this.canboxTview = (TextView)this.findViewById(2131427465);
        this.typeTview.setText((CharSequence)(String.valueOf(ProjectConfig.projectName) + BenzModel.benzName()));
        final String imei = UtilTools.getIMEI(this.context);
        if (TextUtils.isEmpty((CharSequence)imei)) {
            this.snTview.setVisibility(8);
        }
        this.snTview.setText((CharSequence)("SN\uff1a" + imei));
        this.androidTview.setText((CharSequence)("APP\uff1a" + UtilTools.getVersion(this.context)));
        this.systemTview.setText((CharSequence)this.getVersion());
        this.mcuTview.setText((CharSequence)("MCU\uff1a" + this.app.curMcuVersion));
        this.canboxTview.setText((CharSequence)("CAN\uff1a" + this.app.curCanboxVersion));
        linearLayout.setOnClickListener((View$OnClickListener)this);
        this.typeTview.setOnClickListener((View$OnClickListener)this);
        this.snTview.setOnClickListener((View$OnClickListener)this);
        this.androidTview.setOnClickListener((View$OnClickListener)this);
        this.systemTview.setOnClickListener((View$OnClickListener)this);
        this.mcuTview.setOnClickListener((View$OnClickListener)this);
        this.canboxTview.setOnClickListener((View$OnClickListener)this);
        this.app.registerHandler(this.mHandler);
        Mainboard.getInstance().getMCUVersionNumber();
        Mainboard.getInstance().getModeInfo(Mainboard.EModeInfo.CANBOX_INFO);
        super.onCreate(bundle);
    }
    
    protected void onStart() {
        this.clickCount = 0;
        this.mcuTview.setText((CharSequence)("MCU\uff1a" + this.app.curMcuVersion));
        this.canboxTview.setText((CharSequence)("CAN\uff1a" + this.app.curCanboxVersion));
        super.onStart();
    }
    
    protected void onStop() {
        super.onStop();
    }
    
    public void unregisterHandlerr() {
        this.app.unregisterHandler(this.mHandler);
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<XitongDialog> target;
        
        public MHandler(final XitongDialog xitongDialog) {
            this.target = new WeakReference<XitongDialog>(xitongDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
