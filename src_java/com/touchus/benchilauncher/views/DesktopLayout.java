package com.touchus.benchilauncher.views;

import android.content.*;
import android.os.*;
import android.widget.*;
import android.view.*;
import android.util.*;
import android.app.*;

public class DesktopLayout extends LinearLayout implements View$OnClickListener
{
    private ImageView back;
    Context context;
    Runnable hideBgRun;
    Runnable hideBtnRun;
    private ImageView home;
    boolean isHide;
    Handler mHandler;
    private ImageView right_push;
    private View view;
    
    public DesktopLayout(final Context context) {
        super(context);
        this.mHandler = new Handler();
        this.hideBtnRun = new Runnable() {
            @Override
            public void run() {
                DesktopLayout.this.hideBtn();
            }
        };
        this.hideBgRun = new Runnable() {
            @Override
            public void run() {
                DesktopLayout.this.view.setBackgroundResource(2130837710);
            }
        };
        this.isHide = false;
        this.context = context;
        this.setLayoutParams((ViewGroup$LayoutParams)new LinearLayout$LayoutParams(-2, -2));
        this.view = LayoutInflater.from(context).inflate(2130903045, (ViewGroup)null);
        this.right_push = (ImageView)this.view.findViewById(2131427349);
        this.home = (ImageView)this.view.findViewById(2131427350);
        this.back = (ImageView)this.view.findViewById(2131427351);
        this.right_push.setOnTouchListener((View$OnTouchListener)new View$OnTouchListener() {
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                Log.e("DesktopLayout", "MotionEvent event = " + motionEvent);
                if (motionEvent.getAction() == 0) {
                    DesktopLayout.this.view.setBackgroundResource(2130837709);
                    DesktopLayout.this.right_push.setVisibility(8);
                    DesktopLayout.this.home.setVisibility(0);
                    DesktopLayout.this.back.setVisibility(0);
                    DesktopLayout.this.isHide = false;
                    DesktopLayout.this.setBtnhide();
                }
                else {
                    motionEvent.getAction();
                }
                return true;
            }
        });
        this.right_push.setVisibility(8);
        this.home.setOnClickListener((View$OnClickListener)this);
        this.back.setOnClickListener((View$OnClickListener)this);
        this.setBtnhide();
        this.addView(this.view);
    }
    
    private void setBtnhide() {
        this.mHandler.removeCallbacks(this.hideBgRun);
        this.mHandler.removeCallbacks(this.hideBtnRun);
        this.mHandler.postDelayed(this.hideBgRun, 3000L);
        this.mHandler.postDelayed(this.hideBtnRun, 3000L);
    }
    
    public boolean hideBtn() {
        Log.e("DesktopLayout", "isHide = " + this.isHide);
        if (this.isHide) {
            return false;
        }
        this.mHandler.removeCallbacks(this.hideBtnRun);
        this.right_push.setVisibility(0);
        this.home.setVisibility(8);
        this.back.setVisibility(8);
        return this.isHide = true;
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131427350: {
                this.sendKeyeventToSystem(3);
                this.setBtnhide();
            }
            case 2131427351: {
                this.sendKeyeventToSystem(4);
                this.setBtnhide();
            }
        }
    }
    
    public void sendKeyeventToSystem(final int n) {
        if (n < 0) {
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                new Instrumentation().sendKeyDownUpSync(n);
            }
        }).start();
    }
}
