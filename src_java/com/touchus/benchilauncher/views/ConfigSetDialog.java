package com.touchus.benchilauncher.views;

import android.app.*;
import com.backaudio.android.driver.*;
import com.touchus.benchilauncher.utils.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import android.util.*;
import android.view.*;
import android.content.*;
import android.os.*;
import java.lang.ref.*;

public class ConfigSetDialog extends Dialog implements View$OnClickListener
{
    private Spinner configure_model;
    private RadioButton connect_AUX;
    private RadioButton connect_BT;
    private RadioGroup connect_type;
    private Context context;
    private TextView help;
    private LauncherApplication mApp;
    public ReverseialogHandler mHandler;
    private Launcher mMMainActivity;
    private SpUtilK mSpUtilK;
    private MyCustomDialog myCustomDialog;
    private RadioButton navi_have;
    private RadioButton navi_no;
    private RadioGroup original_navi;
    private RadioGroup radioset;
    private String[] screenType;
    private RadioButton square;
    private RelativeLayout square_layout;
    private RadioButton stripe;
    private LinearLayout stripe_layout;
    private int type;
    private String[] usbNum;
    private Spinner usb_configure;
    
    static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas() {
        final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas = ConfigSetDialog.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas;
        if ($switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas != null) {
            return $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas;
        }
        final int[] $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas2 = new int[Mainboard.EAUXStutas.values().length];
        while (true) {
            try {
                $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas2[Mainboard.EAUXStutas.ACTIVATING.ordinal()] = 1;
                try {
                    $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas2[Mainboard.EAUXStutas.FAILED.ordinal()] = 3;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas2[Mainboard.EAUXStutas.SUCCEED.ordinal()] = 2;
                        return ConfigSetDialog.$SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas = $switch_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas2;
                    }
                    catch (NoSuchFieldError noSuchFieldError) {}
                }
                catch (NoSuchFieldError noSuchFieldError2) {}
            }
            catch (NoSuchFieldError noSuchFieldError3) {
                continue;
            }
            break;
        }
    }
    
    public ConfigSetDialog(final Context context) {
        super(context);
        this.type = 1;
        this.screenType = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
        this.usbNum = new String[] { "1", "2" };
        this.mHandler = new ReverseialogHandler(this);
    }
    
    public ConfigSetDialog(final Context context, final int n) {
        super(context, n);
        this.type = 1;
        this.screenType = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
        this.usbNum = new String[] { "1", "2" };
        this.mHandler = new ReverseialogHandler(this);
        this.context = context;
    }
    
    static /* synthetic */ void access$2(final ConfigSetDialog configSetDialog, final int type) {
        configSetDialog.type = type;
    }
    
    static /* synthetic */ void access$7(final ConfigSetDialog configSetDialog, final MyCustomDialog myCustomDialog) {
        configSetDialog.myCustomDialog = myCustomDialog;
    }
    
    private void initData() {
        this.mMMainActivity = (Launcher)this.context;
        this.mSpUtilK = new SpUtilK((Context)this.mMMainActivity);
        final ArrayAdapter adapter = new ArrayAdapter(this.context, 2130903105, (Object[])this.screenType);
        adapter.setDropDownViewResource(2130903105);
        this.configure_model.setAdapter((SpinnerAdapter)adapter);
        final ArrayAdapter adapter2 = new ArrayAdapter(this.context, 2130903105, (Object[])this.usbNum);
        adapter2.setDropDownViewResource(2130903105);
        this.usb_configure.setAdapter((SpinnerAdapter)adapter2);
        this.mApp.radioPos = this.mSpUtilK.getInt("radioType", 0);
        this.mApp.naviPos = this.mSpUtilK.getInt("naviExist", 0);
        this.mApp.usbPos = this.mSpUtilK.getInt("usbNum", 0);
        this.mApp.connectPos = this.mSpUtilK.getInt("connectType", 0);
        this.mApp.screenPos = this.mSpUtilK.getInt("screenType", 0);
    }
    
    private void initSetup() {
        this.stripe.setOnClickListener((View$OnClickListener)this);
        this.square.setOnClickListener((View$OnClickListener)this);
        if (!Utiltools.isAvilible(this.context, "com.unibroad.benzuserguide")) {
            this.help.setVisibility(8);
        }
        this.help.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                final Intent intent = new Intent();
                intent.setAction("com.unibroad.help");
                intent.putExtra("page", 10);
                ConfigSetDialog.this.context.startActivity(intent);
            }
        });
        this.radioset.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427380: {
                        ConfigSetDialog.this.mApp.radioPos = 0;
                        break;
                    }
                    case 2131427381: {
                        ConfigSetDialog.this.mApp.radioPos = 1;
                        break;
                    }
                }
                ConfigSetDialog.access$2(ConfigSetDialog.this, 1);
                ConfigSetDialog.this.seclectTrue(ConfigSetDialog.this.mApp.radioPos);
            }
        });
        this.original_navi.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427384: {
                        ConfigSetDialog.this.mApp.naviPos = 0;
                        break;
                    }
                    case 2131427385: {
                        ConfigSetDialog.this.mApp.naviPos = 1;
                        break;
                    }
                }
                SysConst.storeData[5] = (byte)ConfigSetDialog.this.mApp.naviPos;
                ConfigSetDialog.this.mSpUtilK.putInt("naviExist", ConfigSetDialog.this.mApp.naviPos);
                Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
            }
        });
        this.connect_type.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427387: {
                        ConfigSetDialog.this.mApp.connectPos = 0;
                        LauncherApplication.isBT = true;
                        SysConst.storeData[6] = (byte)ConfigSetDialog.this.mApp.connectPos;
                        ConfigSetDialog.this.mSpUtilK.putInt("connectType", ConfigSetDialog.this.mApp.connectPos);
                        Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
                    }
                    case 2131427388: {
                        if (ConfigSetDialog.this.mApp.connectPos != 1) {
                            ConfigSetDialog.this.showBluetoothDialog(ConfigSetDialog.this.context.getString(2131165364), ConfigSetDialog.this.context.getString(2131165365), ConfigSetDialog.this.context.getString(2131165359), ConfigSetDialog.this.context.getString(2131165360), true);
                            return;
                        }
                        break;
                    }
                }
            }
        });
        this.configure_model.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int benzSize, final long n) {
                Mainboard.getInstance().setBenzSize(benzSize);
                if (benzSize == 6) {
                    Mainboard.getInstance().setOldCBTAudio();
                }
                ConfigSetDialog.this.mSpUtilK.putInt("screenType", benzSize);
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        });
        this.usb_configure.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                SysConst.storeData[7] = (byte)n;
                ConfigSetDialog.this.mSpUtilK.putInt("usbNum", n);
                Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        });
        this.seclectTrue(this.mApp.radioPos);
        if (this.mApp.naviPos < 0 || this.mApp.naviPos > 1) {
            this.mApp.naviPos = 0;
        }
        ((RadioButton)this.original_navi.getChildAt(this.mApp.naviPos)).setChecked(true);
        if (this.mApp.connectPos < 0 || this.mApp.connectPos > 1) {
            this.mApp.connectPos = 0;
        }
        Log.e("", "connect_type.size = " + this.connect_type.getChildCount());
        ((RadioButton)this.connect_type.getChildAt(this.mApp.connectPos)).setChecked(true);
        if (this.mApp.usbPos < 0 || this.mApp.usbPos > this.usbNum.length) {
            this.mApp.usbPos = 0;
        }
        this.usb_configure.setSelection(this.mApp.usbPos);
        this.configure_model.setSelection(this.mApp.screenPos);
    }
    
    private void initView() {
        this.help = (TextView)this.findViewById(2131427378);
        this.radioset = (RadioGroup)this.findViewById(2131427379);
        this.original_navi = (RadioGroup)this.findViewById(2131427383);
        this.connect_type = (RadioGroup)this.findViewById(2131427386);
        this.stripe = (RadioButton)this.findViewById(2131427380);
        this.square = (RadioButton)this.findViewById(2131427381);
        this.navi_have = (RadioButton)this.findViewById(2131427384);
        this.navi_no = (RadioButton)this.findViewById(2131427385);
        this.connect_BT = (RadioButton)this.findViewById(2131427387);
        this.connect_AUX = (RadioButton)this.findViewById(2131427388);
        this.stripe_layout = (LinearLayout)this.findViewById(2131427382);
        this.square_layout = (RelativeLayout)this.findViewById(2131427389);
        this.configure_model = (Spinner)this.findViewById(2131427393);
        this.usb_configure = (Spinner)this.findViewById(2131427391);
    }
    
    private void pressItem(final int n) {
    }
    
    private void seclectTrue(final int n) {
        if (this.type != 2) {
            if (this.mApp.radioPos == 0) {
                this.square_layout.setVisibility(8);
                this.stripe_layout.setVisibility(0);
            }
            else {
                this.square_layout.setVisibility(0);
                this.stripe_layout.setVisibility(8);
            }
            if (this.mApp.radioPos < 0 || this.mApp.radioPos >= this.radioset.getChildCount()) {
                this.mApp.radioPos = 0;
            }
            SysConst.storeData[4] = (byte)this.mApp.radioPos;
            Log.e("", "SysConst.storeData = " + SysConst.storeData);
            Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
            this.mSpUtilK.putInt("radioType", this.mApp.radioPos);
            ((RadioButton)this.radioset.getChildAt(this.mApp.radioPos)).setChecked(true);
        }
    }
    
    private void setConnectType() {
        this.mApp.connectPos = 1;
        LauncherApplication.isBT = false;
        SysConst.storeData[6] = (byte)this.mApp.connectPos;
        this.mSpUtilK.putInt("connectType", this.mApp.connectPos);
        Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
    }
    
    private void setDialogLocation(final Dialog dialog) {
        final Window window = dialog.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        attributes.x = 256;
        attributes.y = -30;
        window.setAttributes(attributes);
    }
    
    private void showBluetoothDialog(final String message, final String nextMessage, final String s, final String s2, final boolean b) {
        final MyCustomDialog.Builder builder = new MyCustomDialog.Builder(this.context);
        builder.setMessage(message);
        builder.setNextMessage(nextMessage);
        builder.setPositiveButton(s, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                if (b) {
                    ConfigSetDialog.this.setConnectType();
                    return;
                }
                Mainboard.getInstance().requestAUXOperate(Mainboard.EAUXOperate.ACTIVATE);
                ConfigSetDialog.this.showBluetoothDialog(ConfigSetDialog.this.context.getString(2131165369), ConfigSetDialog.this.context.getString(2131165370), null, null, false);
            }
        });
        builder.setNegativeButton(s2, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                if (b) {
                    ConfigSetDialog.this.showBluetoothDialog("", ConfigSetDialog.this.context.getString(2131165366), ConfigSetDialog.this.context.getString(2131165367), ConfigSetDialog.this.context.getString(2131165368), false);
                }
            }
        });
        if (this.myCustomDialog != null) {
            this.myCustomDialog.dismiss();
        }
        (this.myCustomDialog = builder.create()).setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            public void onDismiss(final DialogInterface dialogInterface) {
                builder.unRegisterHandler();
                ConfigSetDialog.access$7(ConfigSetDialog.this, null);
            }
        });
        this.setDialogLocation(this.myCustomDialog);
        this.myCustomDialog.show();
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                this.dismiss();
            }
        }
        else if (message.what == 1048) {
            switch ($SWITCH_TABLE$com$backaudio$android$driver$Mainboard$EAUXStutas()[((Mainboard.EAUXStutas)message.getData().getSerializable("aux_activate_stutas")).ordinal()]) {
                default: {}
                case 1: {
                    if (this.myCustomDialog != null) {
                        this.myCustomDialog.dismiss();
                    }
                    this.showBluetoothDialog(this.context.getString(2131165369), this.context.getString(2131165370), null, null, false);
                }
                case 2: {
                    if (this.myCustomDialog != null) {
                        this.myCustomDialog.dismiss();
                    }
                    this.showBluetoothDialog(this.context.getString(2131165371), this.context.getString(2131165372), null, null, false);
                    this.mHandler.postDelayed((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            if (ConfigSetDialog.this.myCustomDialog != null) {
                                ConfigSetDialog.this.myCustomDialog.dismiss();
                            }
                        }
                    }, 2000L);
                }
                case 3: {
                    if (this.myCustomDialog != null) {
                        this.myCustomDialog.dismiss();
                    }
                    this.showBluetoothDialog(this.context.getString(2131165373), "", null, null, true);
                }
            }
        }
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131427380:
            case 2131427381: {
                this.type = 1;
                this.seclectTrue(this.mApp.radioPos);
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903051);
        this.mApp = (LauncherApplication)this.context.getApplicationContext();
        this.initView();
        this.initData();
        this.initSetup();
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void unregisterHandlerr() {
        this.mApp.unregisterHandler(this.mHandler);
    }
    
    static class ReverseialogHandler extends Handler
    {
        private WeakReference<ConfigSetDialog> target;
        
        public ReverseialogHandler(final ConfigSetDialog configSetDialog) {
            this.target = new WeakReference<ConfigSetDialog>(configSetDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
