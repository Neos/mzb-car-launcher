package com.touchus.benchilauncher.views;

import android.app.*;
import android.content.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.utils.*;
import com.backaudio.android.driver.*;
import android.widget.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class ReverseDialog extends Dialog implements View$OnClickListener
{
    private RadioButton addreverse360;
    private Context context;
    private LauncherApplication mApp;
    public ReverseialogHandler mHandler;
    private byte mIDRIVERENUM;
    private Launcher mMMainActivity;
    private SpUtilK mSpUtilK;
    private RadioButton mute;
    private RadioButton newaddreverse;
    private RadioButton normal;
    private RadioButton originalreverse;
    private int reversePos;
    private RadioGroup reverseType;
    private int selectPos;
    private int type;
    private RadioGroup voiceSet;
    private int voicesetPos;
    
    public ReverseDialog(final Context context) {
        super(context);
        this.reversePos = 0;
        this.voicesetPos = 0;
        this.type = 1;
        this.mHandler = new ReverseialogHandler(this);
        this.selectPos = 0;
    }
    
    public ReverseDialog(final Context context, final int n) {
        super(context, n);
        this.reversePos = 0;
        this.voicesetPos = 0;
        this.type = 1;
        this.mHandler = new ReverseialogHandler(this);
        this.selectPos = 0;
        this.context = context;
    }
    
    static /* synthetic */ void access$0(final ReverseDialog reverseDialog, final int reversePos) {
        reverseDialog.reversePos = reversePos;
    }
    
    static /* synthetic */ void access$1(final ReverseDialog reverseDialog, final int type) {
        reverseDialog.type = type;
    }
    
    static /* synthetic */ void access$5(final ReverseDialog reverseDialog, final int voicesetPos) {
        reverseDialog.voicesetPos = voicesetPos;
    }
    
    private void initData() {
        this.mMMainActivity = (Launcher)this.context;
        this.mSpUtilK = new SpUtilK((Context)this.mMMainActivity);
        this.reversePos = this.mSpUtilK.getInt("reversingType", 1);
        this.voicesetPos = this.mSpUtilK.getInt("voiceType", 0);
        if (this.voicesetPos == 1) {
            this.mApp.eMediaSet = Mainboard.EReverserMediaSet.NOMAL;
            return;
        }
        this.mApp.eMediaSet = Mainboard.EReverserMediaSet.MUTE;
    }
    
    private void initSetup() {
        this.originalreverse.setOnClickListener((View$OnClickListener)this);
        this.newaddreverse.setOnClickListener((View$OnClickListener)this);
        this.addreverse360.setOnClickListener((View$OnClickListener)this);
        this.normal.setOnClickListener((View$OnClickListener)this);
        this.mute.setOnClickListener((View$OnClickListener)this);
        ((RadioButton)this.voiceSet.getChildAt(this.voicesetPos)).setChecked(true);
        ((RadioButton)this.reverseType.getChildAt(this.reversePos)).setChecked(true);
        this.reverseType.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427395: {
                        ReverseDialog.access$0(ReverseDialog.this, 0);
                        break;
                    }
                    case 2131427396: {
                        ReverseDialog.access$0(ReverseDialog.this, 1);
                        break;
                    }
                    case 2131427397: {
                        ReverseDialog.access$0(ReverseDialog.this, 2);
                        break;
                    }
                }
                ReverseDialog.access$1(ReverseDialog.this, 1);
                ReverseDialog.this.seclectTrue(ReverseDialog.this.reversePos);
                ReverseDialog.this.press();
            }
        });
        this.voiceSet.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427400: {
                        ReverseDialog.access$5(ReverseDialog.this, 1);
                        break;
                    }
                    case 2131427399: {
                        ReverseDialog.access$5(ReverseDialog.this, 0);
                        break;
                    }
                }
                ReverseDialog.access$1(ReverseDialog.this, 2);
                ReverseDialog.this.seclectTrue(ReverseDialog.this.voicesetPos);
                ReverseDialog.this.press();
            }
        });
        this.seclectTrue(this.reversePos);
    }
    
    private void initView() {
        this.reverseType = (RadioGroup)this.findViewById(2131427394);
        this.voiceSet = (RadioGroup)this.findViewById(2131427398);
        this.originalreverse = (RadioButton)this.findViewById(2131427395);
        this.newaddreverse = (RadioButton)this.findViewById(2131427396);
        this.addreverse360 = (RadioButton)this.findViewById(2131427397);
        this.normal = (RadioButton)this.findViewById(2131427400);
        this.mute = (RadioButton)this.findViewById(2131427399);
    }
    
    private void press() {
        if (this.type == 2) {
            this.mSpUtilK.putInt("voiceType", this.voicesetPos);
            if (this.voicesetPos == 1) {
                this.mApp.eMediaSet = Mainboard.EReverserMediaSet.NOMAL;
                Mainboard.getInstance().sendReverseMediaSetToMcu(2);
            }
            else {
                this.mApp.eMediaSet = Mainboard.EReverserMediaSet.MUTE;
                Mainboard.getInstance().sendReverseMediaSetToMcu(0);
            }
            ((RadioButton)this.voiceSet.getChildAt(this.voicesetPos)).setChecked(true);
            return;
        }
        this.mSpUtilK.putInt("reversingType", this.reversePos);
        Mainboard.getInstance().sendReverseSetToMcu(this.reversePos);
        ((RadioButton)this.reverseType.getChildAt(this.reversePos)).setChecked(true);
    }
    
    private void seclectTrue(final int n) {
        RadioGroup radioGroup;
        if (this.type == 2) {
            radioGroup = this.voiceSet;
            this.reverseType.getChildAt(this.reversePos).setSelected(false);
        }
        else {
            radioGroup = this.reverseType;
            this.voiceSet.getChildAt(this.voicesetPos).setSelected(false);
        }
        for (int i = 0; i < radioGroup.getChildCount(); ++i) {
            if (n == i) {
                radioGroup.getChildAt(i).setSelected(true);
            }
            else {
                radioGroup.getChildAt(i).setSelected(false);
            }
        }
    }
    
    public void handlerMsgUSB(final Message message) {
        if (message.what == 6001) {
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.DOWN.getCode()) {
                if (this.type == 1 && this.reversePos < this.reverseType.getChildCount() - 1) {
                    ++this.reversePos;
                    this.selectPos = this.reversePos;
                }
                else if (this.type == 1 && this.reversePos == this.reverseType.getChildCount() - 1) {
                    ++this.type;
                    this.voicesetPos = 0;
                    this.selectPos = this.voicesetPos;
                }
                else if (this.type == 2 && this.voicesetPos < this.voiceSet.getChildCount() - 1) {
                    ++this.voicesetPos;
                    this.selectPos = this.voicesetPos;
                }
                this.seclectTrue(this.selectPos);
            }
            else {
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.type == 2 && this.voicesetPos > 0) {
                        --this.voicesetPos;
                        this.selectPos = this.voicesetPos;
                    }
                    else if (this.type == 2 && this.voicesetPos == 0) {
                        --this.type;
                        this.reversePos = this.reverseType.getChildCount() - 1;
                        this.selectPos = this.reversePos;
                    }
                    else if (this.type == 1 && this.reversePos > 0) {
                        --this.reversePos;
                        this.selectPos = this.reversePos;
                    }
                    this.seclectTrue(this.selectPos);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.press();
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.BACK.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.HOME.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
        }
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131427395:
            case 2131427396:
            case 2131427397: {
                this.type = 1;
                this.seclectTrue(this.reversePos);
            }
            case 2131427399:
            case 2131427400: {
                this.type = 2;
                this.seclectTrue(this.voicesetPos);
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903052);
        this.mApp = (LauncherApplication)this.context.getApplicationContext();
        this.initView();
        this.initData();
        this.initSetup();
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void unregisterHandlerr() {
        this.mApp.unregisterHandler(this.mHandler);
    }
    
    static class ReverseialogHandler extends Handler
    {
        private WeakReference<ReverseDialog> target;
        
        public ReverseialogHandler(final ReverseDialog reverseDialog) {
            this.target = new WeakReference<ReverseDialog>(reverseDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
