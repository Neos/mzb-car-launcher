package com.touchus.benchilauncher.views;

import android.widget.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class CarWarnInfoView extends LinearLayout
{
    private ImageView infoImg;
    private TextView infoText;
    
    public CarWarnInfoView(final Context context) {
        this(context, null);
    }
    
    public CarWarnInfoView(final Context context, final AttributeSet set) {
        super(context, set);
        LayoutInflater.from(context).inflate(2130903044, (ViewGroup)this, true);
        this.infoImg = (ImageView)this.findViewById(2131427347);
        this.infoText = (TextView)this.findViewById(2131427348);
    }
    
    public void setCarInfo(final boolean b, final int n, final String s) {
        if (b) {
            this.infoImg.setImageResource(n);
            this.infoText.setTextColor(-65536);
            this.infoText.setText((CharSequence)s);
            return;
        }
        this.infoImg.setImageResource(n);
        this.infoText.setTextColor(-16777216);
        this.infoText.setText((CharSequence)s);
    }
}
