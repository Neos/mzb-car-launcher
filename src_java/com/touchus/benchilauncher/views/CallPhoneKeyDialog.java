package com.touchus.benchilauncher.views;

import android.app.*;
import android.widget.*;
import com.touchus.benchilauncher.activity.main.right.call.*;
import android.content.*;
import android.os.*;
import android.view.*;

public class CallPhoneKeyDialog extends Dialog implements View$OnClickListener
{
    View lastview;
    private ImageView number0Img;
    private ImageView number1Img;
    private ImageView number2Img;
    private ImageView number3Img;
    private ImageView number4Img;
    private ImageView number5Img;
    private ImageView number6Img;
    private ImageView number7Img;
    private ImageView number8Img;
    private ImageView number9Img;
    private ImageView numberOtherImg;
    private ImageView numberStarImg;
    public CallPhoneFragment parent;
    
    public CallPhoneKeyDialog(final Context context) {
        super(context);
        this.lastview = null;
    }
    
    public CallPhoneKeyDialog(final Context context, final int n) {
        super(context, n);
        this.lastview = null;
    }
    
    private void initListener() {
        this.number0Img.setOnClickListener((View$OnClickListener)this);
        this.number1Img.setOnClickListener((View$OnClickListener)this);
        this.number2Img.setOnClickListener((View$OnClickListener)this);
        this.number3Img.setOnClickListener((View$OnClickListener)this);
        this.number4Img.setOnClickListener((View$OnClickListener)this);
        this.number5Img.setOnClickListener((View$OnClickListener)this);
        this.number6Img.setOnClickListener((View$OnClickListener)this);
        this.number7Img.setOnClickListener((View$OnClickListener)this);
        this.number8Img.setOnClickListener((View$OnClickListener)this);
        this.number9Img.setOnClickListener((View$OnClickListener)this);
        this.numberStarImg.setOnClickListener((View$OnClickListener)this);
        this.numberOtherImg.setOnClickListener((View$OnClickListener)this);
    }
    
    private void initView() {
        this.number1Img = (ImageView)this.findViewById(2131427366);
        this.number2Img = (ImageView)this.findViewById(2131427367);
        this.number3Img = (ImageView)this.findViewById(2131427368);
        this.number4Img = (ImageView)this.findViewById(2131427369);
        this.number5Img = (ImageView)this.findViewById(2131427370);
        this.number6Img = (ImageView)this.findViewById(2131427371);
        this.number7Img = (ImageView)this.findViewById(2131427372);
        this.number8Img = (ImageView)this.findViewById(2131427373);
        this.number9Img = (ImageView)this.findViewById(2131427374);
        this.number0Img = (ImageView)this.findViewById(2131427376);
        this.numberStarImg = (ImageView)this.findViewById(2131427375);
        this.numberOtherImg = (ImageView)this.findViewById(2131427377);
    }
    
    public void onClick(final View lastview) {
        if (this.parent != null) {
            if (this.lastview != null && this.lastview != lastview) {
                this.lastview.setSelected(false);
            }
            (this.lastview = lastview).setSelected(true);
            if (lastview.getId() == 2131427376) {
                this.parent.upListenSelectBtn(1);
                return;
            }
            if (lastview.getId() == 2131427366) {
                this.parent.upListenSelectBtn(2);
                return;
            }
            if (lastview.getId() == 2131427367) {
                this.parent.upListenSelectBtn(3);
                return;
            }
            if (lastview.getId() == 2131427368) {
                this.parent.upListenSelectBtn(4);
                return;
            }
            if (lastview.getId() == 2131427369) {
                this.parent.upListenSelectBtn(5);
                return;
            }
            if (lastview.getId() == 2131427370) {
                this.parent.upListenSelectBtn(6);
                return;
            }
            if (lastview.getId() == 2131427371) {
                this.parent.upListenSelectBtn(7);
                return;
            }
            if (lastview.getId() == 2131427372) {
                this.parent.upListenSelectBtn(8);
                return;
            }
            if (lastview.getId() == 2131427373) {
                this.parent.upListenSelectBtn(9);
                return;
            }
            if (lastview.getId() == 2131427374) {
                this.parent.upListenSelectBtn(10);
                return;
            }
            if (lastview.getId() == 2131427375) {
                this.parent.upListenSelectBtn(11);
                return;
            }
            if (lastview.getId() == 2131427377) {
                this.parent.upListenSelectBtn(12);
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903050);
        this.initView();
        this.initListener();
        final Window window = this.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        window.setGravity(19);
        attributes.x = 0;
        attributes.y = -50;
        window.setAttributes(attributes);
    }
    
    protected void onStart() {
        super.onStart();
    }
}
