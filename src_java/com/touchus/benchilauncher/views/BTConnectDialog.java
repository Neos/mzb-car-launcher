package com.touchus.benchilauncher.views;

import java.util.concurrent.atomic.*;
import com.touchus.benchilauncher.*;
import android.content.*;
import java.util.*;
import org.slf4j.*;
import com.backaudio.android.driver.*;
import android.widget.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class BTConnectDialog
{
    private static BTConnectDialog instance;
    private static AtomicBoolean isAdded;
    private static Logger logger;
    private LauncherApplication app;
    private View contentView;
    private Context mContext;
    private MyCustomHandler mHandler;
    private String message;
    private Button negativeButton;
    private String negativeButtonText;
    private View outside;
    private WindowManager$LayoutParams params;
    private Button positiveButton;
    private String positiveButtonText;
    int time;
    Timer timer;
    Runnable update;
    private WindowManager wm;
    
    static {
        BTConnectDialog.logger = LoggerFactory.getLogger(BTConnectDialog.class);
        BTConnectDialog.instance = null;
        BTConnectDialog.isAdded = new AtomicBoolean(false);
    }
    
    public BTConnectDialog() {
        this.contentView = null;
        this.time = 8;
        this.timer = new Timer();
        this.update = new Runnable() {
            @Override
            public void run() {
                BTConnectDialog.logger.debug("BTConnectDialog time:" + BTConnectDialog.this.time);
                if (BTConnectDialog.this.time == 0) {
                    if (BTConnectDialog.this.app.btConnectDialog) {
                        BTConnectDialog.this.app.service.btMusicConnect();
                        BTConnectDialog.this.dissShow();
                    }
                    return;
                }
                final BTConnectDialog this$0 = BTConnectDialog.this;
                --this$0.time;
                BTConnectDialog.this.mHandler.obtainMessage(0).sendToTarget();
                BTConnectDialog.this.mHandler.postDelayed(BTConnectDialog.this.update, 1000L);
            }
        };
        this.mHandler = new MyCustomHandler(this);
        BTConnectDialog.logger.debug("BTConnectDialog BTConnectDialog()");
    }
    
    private void autoConnect() {
        this.mHandler.post(this.update);
    }
    
    public static BTConnectDialog getInstance() {
        if (BTConnectDialog.instance == null) {
            BTConnectDialog.instance = new BTConnectDialog();
        }
        return BTConnectDialog.instance;
    }
    
    private void handlerMsg(final Message message) {
        final Bundle data = message.getData();
        if (data.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || data.getByte("idriver_enum") == Mainboard.EIdriverEnum.RIGHT.getCode()) {
            this.right();
        }
        else {
            if (data.getByte("idriver_enum") == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || data.getByte("idriver_enum") == Mainboard.EIdriverEnum.LEFT.getCode()) {
                this.left();
                return;
            }
            if (data.getByte("idriver_enum") == Mainboard.EIdriverEnum.PRESS.getCode()) {
                this.press();
                return;
            }
            if (message.what == 0) {
                this.positiveButton.setText((CharSequence)String.format(this.positiveButtonText, this.time));
            }
        }
    }
    
    private void left() {
        if (this.negativeButton.isSelected()) {
            this.negativeButton.setSelected(false);
            this.positiveButton.setSelected(true);
        }
    }
    
    private void press() {
        if (this.positiveButton.isSelected()) {
            this.positiveButton.performClick();
        }
        else if (this.negativeButton.isSelected()) {
            this.negativeButton.performClick();
        }
        this.unRegisterHandler();
    }
    
    private void right() {
        if (this.positiveButton.isSelected()) {
            this.negativeButton.setSelected(true);
            this.positiveButton.setSelected(false);
        }
    }
    
    public void dissShow() {
        if (this.contentView == null) {
            return;
        }
        if (this.app.btConnectDialog) {
            this.app.btConnectDialog = false;
        }
        this.timer.cancel();
        this.unRegisterHandler();
        while (true) {
            try {
                if (BTConnectDialog.isAdded.get()) {
                    this.wm.removeView(this.contentView);
                    BTConnectDialog.isAdded.set(false);
                }
                this.contentView = null;
                BTConnectDialog.isAdded.set(false);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void showBTConnectView(final Context context) {
        if (this.contentView != null) {
            BTConnectDialog.logger.debug("BTConnectDialog show  contentView != null");
            return;
        }
        while (true) {
            try {
                this.mContext = context.getApplicationContext();
                this.app = (LauncherApplication)context.getApplicationContext();
                this.message = this.mContext.getString(2131165296);
                this.positiveButtonText = this.mContext.getString(2131165335);
                this.negativeButtonText = this.mContext.getString(2131165336);
                this.timer = new Timer();
                this.time = 8;
                this.wm = (WindowManager)this.mContext.getSystemService("window");
                this.params = new WindowManager$LayoutParams();
                this.params.format = 1;
                this.params.flags = 40;
                this.params.type = 2003;
                this.contentView = LayoutInflater.from(this.mContext).inflate(2130903048, (ViewGroup)null);
                this.outside = this.contentView.findViewById(2131427357);
                this.positiveButton = (Button)this.contentView.findViewById(2131427359);
                this.negativeButton = (Button)this.contentView.findViewById(2131427360);
                this.positiveButton.setSelected(true);
                this.positiveButton.setText((CharSequence)this.positiveButtonText);
                this.positiveButton.setText((CharSequence)String.format(this.positiveButtonText, this.time));
                this.positiveButton.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                    public void onClick(final View view) {
                        BTConnectDialog.this.negativeButton.setSelected(false);
                        BTConnectDialog.this.positiveButton.setSelected(true);
                        BTConnectDialog.this.app.service.btMusicConnect();
                        BTConnectDialog.this.dissShow();
                    }
                });
                this.negativeButton.setText((CharSequence)this.negativeButtonText);
                this.negativeButton.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                    public void onClick(final View view) {
                        BTConnectDialog.this.negativeButton.setSelected(true);
                        BTConnectDialog.this.positiveButton.setSelected(false);
                        BTConnectDialog.this.app.interBTConnectView();
                        BTConnectDialog.this.dissShow();
                    }
                });
                ((TextView)this.contentView.findViewById(2131427358)).setText((CharSequence)this.message);
                if (!BTConnectDialog.isAdded.get()) {
                    this.wm.addView(this.contentView, (ViewGroup$LayoutParams)this.params);
                    BTConnectDialog.isAdded.set(true);
                }
                this.outside.setOnTouchListener((View$OnTouchListener)new View$OnTouchListener() {
                    public boolean onTouch(final View view, final MotionEvent motionEvent) {
                        if (motionEvent.getAction() == 0) {
                            BTConnectDialog.this.dissShow();
                            return true;
                        }
                        return false;
                    }
                });
                this.autoConnect();
                this.app.registerHandler(this.mHandler);
                this.app.btConnectDialog = true;
            }
            catch (Exception ex) {
                BTConnectDialog.logger.debug("BTConnectDialog Exception:" + ex);
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public void unRegisterHandler() {
        if (this.app != null) {
            this.app.unregisterHandler(this.mHandler);
        }
    }
    
    static class MyCustomHandler extends Handler
    {
        private WeakReference<BTConnectDialog> target;
        
        public MyCustomHandler(final BTConnectDialog btConnectDialog) {
            this.target = new WeakReference<BTConnectDialog>(btConnectDialog);
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            if (this.target.get() != null) {
                if (message.what == 6001) {
                    this.target.get().handlerMsg(message);
                    return;
                }
                if (message.what == 0) {
                    this.target.get().handlerMsg(message);
                }
            }
        }
    }
}
