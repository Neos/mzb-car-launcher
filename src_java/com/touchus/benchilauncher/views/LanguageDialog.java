package com.touchus.benchilauncher.views;

import android.app.*;
import android.content.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.utils.*;
import android.widget.*;
import java.util.*;
import com.backaudio.android.driver.*;
import android.os.*;
import java.lang.ref.*;

public class LanguageDialog extends Dialog
{
    private Context context;
    private RadioGroup languageType;
    private LauncherApplication mApp;
    public ReverseialogHandler mHandler;
    private byte mIDRIVERENUM;
    private Launcher mMMainActivity;
    private SpUtilK mSpUtilK;
    private int selectPos;
    
    public LanguageDialog(final Context context) {
        super(context);
        this.selectPos = 0;
        this.mHandler = new ReverseialogHandler(this);
    }
    
    public LanguageDialog(final Context context, final int n) {
        super(context, n);
        this.selectPos = 0;
        this.mHandler = new ReverseialogHandler(this);
        this.context = context;
    }
    
    static /* synthetic */ void access$0(final LanguageDialog languageDialog, final int selectPos) {
        languageDialog.selectPos = selectPos;
    }
    
    private void initData() {
        this.mMMainActivity = (Launcher)this.context;
        this.mSpUtilK = new SpUtilK((Context)this.mMMainActivity);
        this.selectPos = this.mSpUtilK.getInt("languageType", 0);
        ((RadioButton)this.languageType.getChildAt(this.selectPos)).setChecked(true);
    }
    
    private void initSetup() {
        this.languageType.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427407: {
                        LanguageDialog.access$0(LanguageDialog.this, 0);
                        break;
                    }
                    case 2131427408: {
                        LanguageDialog.access$0(LanguageDialog.this, 1);
                        break;
                    }
                    case 2131427409: {
                        LanguageDialog.access$0(LanguageDialog.this, 2);
                        break;
                    }
                    case 2131427410: {
                        LanguageDialog.access$0(LanguageDialog.this, 3);
                        break;
                    }
                    case 2131427411: {
                        LanguageDialog.access$0(LanguageDialog.this, 4);
                        break;
                    }
                    case 2131427412: {
                        LanguageDialog.access$0(LanguageDialog.this, 5);
                        break;
                    }
                    case 2131427413: {
                        LanguageDialog.access$0(LanguageDialog.this, 6);
                        break;
                    }
                    case 2131427414: {
                        LanguageDialog.access$0(LanguageDialog.this, 7);
                        break;
                    }
                }
                LanguageDialog.this.seclectTrue(LanguageDialog.this.selectPos);
                LanguageDialog.this.press();
                LanguageDialog.this.dismiss();
            }
        });
        this.seclectTrue(this.selectPos);
    }
    
    private void initView() {
        this.languageType = (RadioGroup)this.findViewById(2131427406);
    }
    
    private void press() {
        this.mApp.iLanguageType = this.selectPos;
        switch (this.selectPos) {
            case 0: {
                this.mApp.updateLanguage(Locale.SIMPLIFIED_CHINESE);
                break;
            }
            case 1: {
                this.mApp.updateLanguage(Locale.TRADITIONAL_CHINESE);
                break;
            }
            case 2: {
                this.mApp.updateLanguage(Locale.US);
                break;
            }
            case 3: {
                this.mApp.updateLanguage(new Locale("de", "DE"));
                break;
            }
            case 4: {
                this.mApp.updateLanguage(new Locale("pl", "PL"));
                break;
            }
            case 5: {
                this.mApp.updateLanguage(new Locale("sr", "RS"));
                break;
            }
            case 6: {
                this.mApp.updateLanguage(new Locale("tr", "TR"));
                break;
            }
            case 7: {
                this.mApp.updateLanguage(new Locale("sv", "SE"));
                break;
            }
        }
        this.mSpUtilK.putInt("languageType", this.selectPos);
        Mainboard.getInstance().sendLanguageSetToMcu(this.selectPos);
        Mainboard.getInstance().getStoreDataFromMcu();
    }
    
    private void seclectTrue(final int n) {
        for (int i = 0; i < this.languageType.getChildCount(); ++i) {
            if (n == i) {
                this.languageType.getChildAt(i).setSelected(true);
            }
            else {
                this.languageType.getChildAt(i).setSelected(false);
            }
        }
    }
    
    public void handlerMsgUSB(final Message message) {
        if (message.what == 6001) {
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.DOWN.getCode()) {
                if (this.selectPos < this.languageType.getChildCount() - 1) {
                    ++this.selectPos;
                }
                this.seclectTrue(this.selectPos);
            }
            else {
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.selectPos > 0) {
                        --this.selectPos;
                    }
                    this.seclectTrue(this.selectPos);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.press();
                    this.dismiss();
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.BACK.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.HOME.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903055);
        this.mApp = (LauncherApplication)this.context.getApplicationContext();
        this.initView();
        this.initSetup();
        this.initData();
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void unregisterHandlerr() {
        this.mApp.unregisterHandler(this.mHandler);
    }
    
    static class ReverseialogHandler extends Handler
    {
        private WeakReference<LanguageDialog> target;
        
        public ReverseialogHandler(final LanguageDialog languageDialog) {
            this.target = new WeakReference<LanguageDialog>(languageDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
