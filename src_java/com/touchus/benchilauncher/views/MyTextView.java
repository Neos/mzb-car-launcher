package com.touchus.benchilauncher.views;

import android.widget.*;
import android.content.*;
import android.util.*;
import android.graphics.*;

public class MyTextView extends TextView
{
    public MyTextView(final Context context) {
        super(context);
    }
    
    public MyTextView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public MyTextView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public boolean isFocused() {
        return true;
    }
    
    protected void onFocusChanged(final boolean b, final int n, final Rect rect) {
    }
}
