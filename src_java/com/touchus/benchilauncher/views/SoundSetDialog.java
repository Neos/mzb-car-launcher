package com.touchus.benchilauncher.views;

import android.app.*;
import android.content.*;
import android.widget.*;
import com.touchus.benchilauncher.utils.*;
import java.util.*;
import com.touchus.benchilauncher.*;
import android.view.*;
import com.touchus.benchilauncher.inface.*;
import com.backaudio.android.driver.*;
import android.os.*;
import java.lang.ref.*;

public class SoundSetDialog extends Dialog
{
    public SoundSetDialog(final Context context) {
        super(context);
    }
    
    public SoundSetDialog(final Context context, final int n) {
        super(context, n);
    }
    
    public static class Builder
    {
        private List<TextView> arr;
        SettingDialog.Builder builder;
        private Context context;
        private int count;
        private SoundSetView dvSetView;
        private TextView dv_tv;
        private LauncherApplication mApp;
        private SoundSetDialog mDialog;
        private byte mIDRIVERENUM;
        private Launcher mMMainActivity;
        public YuyingDialogHandler mSettingHandler;
        private SpUtilK mSpUtilK;
        private SoundSetView mediaSetView;
        private TextView media_tv;
        private SoundSetView mixSetView;
        private TextView mix_tv;
        private SoundSetView naviSetView;
        private TextView navi_tv;
        private int size;
        private List<SoundSetView> soundViews;
        
        public Builder(final Context context) {
            this.builder = new SettingDialog.Builder(this.context);
            this.size = 4;
            this.arr = new ArrayList<TextView>();
            this.soundViews = new ArrayList<SoundSetView>();
            this.count = 0;
            this.mSettingHandler = new YuyingDialogHandler(this);
            this.context = context;
        }
        
        static /* synthetic */ void access$0(final Builder builder, final int count) {
            builder.count = count;
        }
        
        private void seclectTrue(final int n) {
            for (int i = 0; i < this.arr.size(); ++i) {
                if (n == i) {
                    this.arr.get(i).setSelected(true);
                }
                else {
                    this.arr.get(i).setSelected(false);
                }
            }
        }
        
        public SoundSetDialog create() {
            this.mMMainActivity = (Launcher)this.context;
            this.mSpUtilK = new SpUtilK(this.context);
            (this.mApp = (LauncherApplication)this.mMMainActivity.getApplication()).registerHandler(this.mSettingHandler);
            this.mDialog = new SoundSetDialog(this.context, 2131230726);
            final View inflate = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130903060, (ViewGroup)null);
            this.mDialog.addContentView(inflate, new ViewGroup$LayoutParams(-1, -2));
            this.navi_tv = (TextView)inflate.findViewById(2131427439);
            this.media_tv = (TextView)inflate.findViewById(2131427441);
            this.dv_tv = (TextView)inflate.findViewById(2131427445);
            this.mix_tv = (TextView)inflate.findViewById(2131427443);
            this.naviSetView = (SoundSetView)inflate.findViewById(2131427440);
            this.mediaSetView = (SoundSetView)inflate.findViewById(2131427442);
            this.dvSetView = (SoundSetView)inflate.findViewById(2131427446);
            this.mixSetView = (SoundSetView)inflate.findViewById(2131427444);
            this.arr.add(this.navi_tv);
            this.soundViews.add(this.naviSetView);
            if (!SysConst.isBT()) {
                this.arr.add(this.media_tv);
                this.soundViews.add(this.mediaSetView);
                this.media_tv.setVisibility(0);
                this.mediaSetView.setVisibility(0);
                if (this.mApp.ismix) {
                    this.arr.add(this.mix_tv);
                    this.soundViews.add(this.mixSetView);
                    this.mix_tv.setVisibility(0);
                    this.mixSetView.setVisibility(0);
                }
                else {
                    this.mix_tv.setVisibility(8);
                    this.mixSetView.setVisibility(8);
                }
            }
            else {
                this.media_tv.setVisibility(8);
                this.mediaSetView.setVisibility(8);
                this.mix_tv.setVisibility(8);
                this.mixSetView.setVisibility(8);
            }
            if (SysConst.DV_CUSTOM) {
                this.arr.add(this.dv_tv);
                this.soundViews.add(this.dvSetView);
                this.dv_tv.setVisibility(0);
                this.dvSetView.setVisibility(0);
            }
            else {
                this.dv_tv.setVisibility(8);
                this.dvSetView.setVisibility(8);
            }
            this.navi_tv.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    Builder.access$0(Builder.this, 0);
                    Builder.this.seclectTrue(Builder.this.count);
                }
            });
            this.media_tv.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    Builder.access$0(Builder.this, 1);
                    Builder.this.seclectTrue(Builder.this.count);
                }
            });
            this.mix_tv.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    Builder.access$0(Builder.this, 2);
                    Builder.this.seclectTrue(Builder.this.count);
                }
            });
            this.dv_tv.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    Builder.access$0(Builder.this, 3);
                    Builder.this.seclectTrue(Builder.this.count);
                }
            });
            this.naviSetView.setOnSoundClickListener(new OnSoundClickListener() {
                @Override
                public void click(final int n) {
                    Builder.access$0(Builder.this, 0);
                    Builder.this.seclectTrue(Builder.this.count);
                    Builder.this.mSpUtilK.putInt(SysConst.naviVoice, n);
                }
            });
            this.mediaSetView.setOnSoundClickListener(new OnSoundClickListener() {
                @Override
                public void click(final int n) {
                    Builder.access$0(Builder.this, 1);
                    Builder.this.seclectTrue(Builder.this.count);
                    Builder.this.mSpUtilK.putInt(SysConst.mediaVoice, n);
                    Mainboard.getInstance().setAllHornSoundValue(SysConst.mediaBasicNum, SysConst.num[n], 0, SysConst.num[n], SysConst.num[n]);
                }
            });
            this.mixSetView.setOnSoundClickListener(new OnSoundClickListener() {
                @Override
                public void click(final int n) {
                    Builder.access$0(Builder.this, 2);
                    Builder.this.seclectTrue(Builder.this.count);
                    Builder.this.mSpUtilK.putInt(SysConst.mixPro, n);
                    SysConst.musicDecVolume = (float)(n / 10.0);
                }
            });
            this.dvSetView.setOnSoundClickListener(new OnSoundClickListener() {
                @Override
                public void click(final int n) {
                    Builder.access$0(Builder.this, 3);
                    Builder.this.seclectTrue(Builder.this.count);
                    Builder.this.mSpUtilK.putInt(SysConst.dvVoice, n);
                }
            });
            if (SysConst.DV_CUSTOM) {
                this.dvSetView.setSoundValue(this.mSpUtilK.getInt(SysConst.dvVoice, 5), SoundSetView.SOUND);
            }
            this.mediaSetView.setSoundValue(this.mSpUtilK.getInt(SysConst.mediaVoice, 5), SoundSetView.SOUND);
            this.mediaSetView.setSpeakText(this.context.getString(2131165355));
            this.mixSetView.setSoundValue(this.mSpUtilK.getInt(SysConst.mixPro, 5), SoundSetView.MIXPRO);
            this.mixSetView.setSpeakText(this.context.getString(2131165354));
            this.naviSetView.setSoundValue(this.mSpUtilK.getInt(SysConst.naviVoice, 3), SoundSetView.SOUND);
            this.naviSetView.setSpeakText(this.context.getString(2131165352));
            this.mDialog.setContentView(inflate);
            this.seclectTrue(this.count);
            return this.mDialog;
        }
        
        public void handlerMsgUSB(final Message message) {
            if (message.what == 6001) {
                this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.DOWN.getCode()) {
                    if (this.count < this.arr.size() - 1) {
                        ++this.count;
                    }
                    this.seclectTrue(this.count);
                }
                else {
                    if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.UP.getCode()) {
                        if (this.count > 0) {
                            --this.count;
                        }
                        this.seclectTrue(this.count);
                        return;
                    }
                    if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode()) {
                        this.soundViews.get(this.count).addSoundValue();
                        return;
                    }
                    if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode()) {
                        this.soundViews.get(this.count).decSoundValue();
                        return;
                    }
                    if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.BACK.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.HOME.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                        this.mDialog.dismiss();
                    }
                }
            }
        }
        
        public void unregisterHandlerr() {
            this.mApp.unregisterHandler(this.mSettingHandler);
        }
        
        static class YuyingDialogHandler extends Handler
        {
            private WeakReference<Builder> target;
            
            public YuyingDialogHandler(final Builder builder) {
                this.target = new WeakReference<Builder>(builder);
            }
            
            public void handleMessage(final Message message) {
                if (this.target.get() == null) {
                    return;
                }
                this.target.get().handlerMsgUSB(message);
            }
        }
    }
}
