package com.touchus.benchilauncher.views;

import android.view.*;
import android.annotation.*;
import android.graphics.drawable.*;
import android.text.format.*;
import android.content.*;
import android.util.*;
import com.touchus.benchilauncher.*;
import android.content.res.*;
import android.graphics.*;

@SuppressLint({ "HandlerLeak" })
public class ClockView extends View
{
    private Drawable clockDrawable;
    private Thread clockThread;
    private int hour;
    private Drawable hourDrawable;
    private boolean isChange;
    private boolean isSetTime;
    private int minute;
    private Drawable minuteDrawable;
    private Paint paint;
    private Time time;
    
    public ClockView(final Context context) {
        this(context, null);
    }
    
    public ClockView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ClockView(final Context mContext, final AttributeSet set, final int n) {
        super(mContext, set, n);
        this.isSetTime = false;
        this.mContext = mContext;
        final TypedArray obtainStyledAttributes = mContext.obtainStyledAttributes(set, R.styleable.MyClockStyleable, n, 0);
        this.clockDrawable = obtainStyledAttributes.getDrawable(0);
        this.hourDrawable = obtainStyledAttributes.getDrawable(1);
        this.minuteDrawable = obtainStyledAttributes.getDrawable(2);
        obtainStyledAttributes.recycle();
        (this.paint = new Paint()).setColor(Color.parseColor("#000000"));
        this.paint.setTypeface(Typeface.DEFAULT_BOLD);
        this.paint.setFakeBoldText(true);
        this.paint.setAntiAlias(true);
        this.time = new Time();
        this.clockThread = new Thread() {
            @Override
            public void run() {
                while (ClockView.this.isChange) {
                    ClockView.this.postInvalidate();
                    try {
                        Thread.sleep(1000L);
                    }
                    catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.isChange = true;
        this.clockThread.start();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.isChange = false;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.isSetTime) {
            this.time.hour = this.hour;
            this.time.minute = this.minute;
        }
        final int n = (this.getRight() - this.getLeft()) / 2;
        final int n2 = (this.getBottom() - this.getTop()) / 2;
        final Drawable clockDrawable = this.clockDrawable;
        final int intrinsicHeight = clockDrawable.getIntrinsicHeight();
        final int intrinsicWidth = clockDrawable.getIntrinsicWidth();
        if (this.getRight() - this.getLeft() < intrinsicWidth || this.getBottom() - this.getTop() < intrinsicHeight) {
            final float min = Math.min((this.getRight() - this.getLeft()) / intrinsicWidth, (this.getBottom() - this.getTop()) / intrinsicHeight);
            canvas.save();
            canvas.scale(min, min, (float)n, (float)n2);
        }
        if (this.isChange) {
            clockDrawable.setBounds(n - intrinsicWidth / 2, n2 - intrinsicHeight / 2, intrinsicWidth / 2 + n, intrinsicHeight / 2 + n2);
        }
        clockDrawable.draw(canvas);
        canvas.save();
        canvas.rotate((this.time.hour + this.time.minute / 60.0f) / 12.0f * 360.0f, (float)n, (float)n2);
        final Drawable hourDrawable = this.hourDrawable;
        final int intrinsicHeight2 = hourDrawable.getIntrinsicHeight();
        final int intrinsicWidth2 = hourDrawable.getIntrinsicWidth();
        if (this.isChange) {
            hourDrawable.setBounds(n - intrinsicWidth2 / 2, n2 - intrinsicHeight2 / 2, intrinsicWidth2 / 2 + n, intrinsicHeight2 / 2 + n2);
        }
        hourDrawable.draw(canvas);
        canvas.restore();
        canvas.save();
        canvas.rotate((this.time.minute + this.time.second / 60.0f) / 60.0f * 360.0f, (float)n, (float)n2);
        final Drawable minuteDrawable = this.minuteDrawable;
        if (this.isChange) {
            final int intrinsicWidth3 = minuteDrawable.getIntrinsicWidth();
            final int intrinsicHeight3 = minuteDrawable.getIntrinsicHeight();
            minuteDrawable.setBounds(n - intrinsicWidth3 / 2, n2 - intrinsicHeight3 / 2, intrinsicWidth3 / 2 + n, intrinsicHeight3 / 2 + n2);
        }
        minuteDrawable.draw(canvas);
        canvas.restore();
        canvas.save();
    }
    
    public void setTime(final boolean isSetTime, final int hour, final int minute) {
        this.isSetTime = isSetTime;
        this.hour = hour;
        this.minute = minute;
    }
}
