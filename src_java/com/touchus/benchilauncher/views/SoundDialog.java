package com.touchus.benchilauncher.views;

import android.app.*;
import android.content.*;
import com.touchus.benchilauncher.utils.*;
import android.widget.*;
import com.touchus.benchilauncher.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class SoundDialog extends Dialog
{
    public SoundDialog(final Context context, final int n) {
        super(context, n);
    }
    
    public static class Builder
    {
        private Context context;
        private LauncherApplication mApp;
        private SoundDialog mDialog;
        private byte mIDRIVERENUM;
        private byte mIDRIVERSTATEENUM;
        public DaohangDialogHandler mSettingHandler;
        private ImageView mSoundAdd;
        private ImageView mSoundJianjian;
        private SeekBar mSoundSeekBar;
        private TextView mSoundTv;
        private int max;
        private int mun;
        private SpUtilK spUtilK;
        private int type;
        
        public Builder(final Context context) {
            this.mun = 0;
            this.max = 10;
            this.mSettingHandler = new DaohangDialogHandler(this);
            this.context = context;
        }
        
        static /* synthetic */ void access$0(final Builder builder, final int mun) {
            builder.mun = mun;
        }
        
        public SoundDialog create(final int type) {
            this.spUtilK = new SpUtilK(this.context);
            this.type = type;
            if (this.type == 0) {
                this.mun = this.spUtilK.getInt("mun", 5);
            }
            else {
                this.mun = this.spUtilK.getInt("callmun", 5);
            }
            this.mDialog = new SoundDialog(this.context, 2131230726);
            final View inflate = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130903059, (ViewGroup)null);
            this.mDialog.addContentView(inflate, new ViewGroup$LayoutParams(-1, -2));
            this.mSoundSeekBar = (SeekBar)inflate.findViewById(2131427437);
            this.mSoundAdd = (ImageView)inflate.findViewById(2131427438);
            this.mSoundJianjian = (ImageView)inflate.findViewById(2131427436);
            this.mSoundTv = (TextView)inflate.findViewById(2131427435);
            this.mDialog.setContentView(inflate);
            this.mSoundSeekBar.setProgress(this.mun);
            this.mSoundSeekBar.setMax(this.max);
            this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(this.mun)).toString());
            this.mApp = (LauncherApplication)this.context.getApplicationContext();
            if (this.type == 0) {
                this.mApp.registerHandler(this.mSettingHandler);
            }
            this.mSoundSeekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener() {
                public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
                    Builder.access$0(Builder.this, n);
                    Builder.this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(Builder.this.mun)).toString());
                    if (Builder.this.type == 0) {
                        Builder.this.spUtilK.putInt(SysConst.naviVoice, Builder.this.mun);
                        return;
                    }
                    Builder.this.spUtilK.putInt(SysConst.callVoice, Builder.this.mun);
                    Mainboard.getInstance().setAllHornSoundValue(SysConst.basicNum, SysConst.callnum[Builder.this.mun], 0, 0, 0);
                }
                
                public void onStartTrackingTouch(final SeekBar seekBar) {
                }
                
                public void onStopTrackingTouch(final SeekBar seekBar) {
                }
            });
            this.mSoundAdd.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    if (Builder.this.mun < Builder.this.max) {
                        final Builder this$1 = Builder.this;
                        Builder.access$0(this$1, this$1.mun + 1);
                    }
                    Builder.this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(Builder.this.mun)).toString());
                    Builder.this.mSoundSeekBar.setProgress(Builder.this.mun);
                }
            });
            this.mSoundJianjian.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    if (Builder.this.mun > 0) {
                        final Builder this$1 = Builder.this;
                        Builder.access$0(this$1, this$1.mun - 1);
                    }
                    Builder.this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(Builder.this.mun)).toString());
                    Builder.this.mSoundSeekBar.setProgress(Builder.this.mun);
                }
            });
            return this.mDialog;
        }
        
        public void handlerMsgUSB(final Message message) {
            if (message.what == 6001) {
                final Bundle data = message.getData();
                this.mIDRIVERENUM = data.getByte("idriver_enum");
                this.mIDRIVERSTATEENUM = data.getByte("idriver_state_enum");
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                    if (this.mun < this.max) {
                        ++this.mun;
                    }
                    this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(this.mun)).toString());
                    this.mSoundSeekBar.setProgress(this.mun);
                }
                else {
                    if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.LEFT.getCode()) {
                        if (this.mun > 0) {
                            --this.mun;
                        }
                        this.mSoundTv.setText((CharSequence)new StringBuilder(String.valueOf(this.mun)).toString());
                        this.mSoundSeekBar.setProgress(this.mun);
                        return;
                    }
                    if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.BACK.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.HOME.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                        this.mDialog.dismiss();
                    }
                }
            }
        }
        
        public void unregisterHandlerr() {
            if (this.type == 0) {
                this.mApp.unregisterHandler(this.mSettingHandler);
            }
        }
        
        static class DaohangDialogHandler extends Handler
        {
            private WeakReference<Builder> target;
            
            public DaohangDialogHandler(final Builder builder) {
                this.target = new WeakReference<Builder>(builder);
            }
            
            public void handleMessage(final Message message) {
                if (this.target.get() == null) {
                    return;
                }
                this.target.get().handlerMsgUSB(message);
            }
        }
    }
}
