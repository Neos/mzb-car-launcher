package com.touchus.benchilauncher.views;

import com.touchus.benchilauncher.base.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class MenuSlide extends AbstraSlide
{
    public static final int STATE_DOWN0 = 0;
    public static final int STATE_DOWN1 = 1;
    public static final int STATE_HIDE_NEXT5V = 8;
    public static final int STATE_UP020 = 2;
    public static final int STATE_UP021 = 3;
    public static final int STATE_UP120 = 4;
    public static final int STATE_UP121 = 5;
    public static final int STATE_UP122 = 9;
    public static final int STATE_UP221 = 10;
    public static final int STATE_UP222 = 11;
    public static final int STATE_V421 = 6;
    public static final int STATE_V520 = 7;
    public static final int mChildWithB = 230;
    public static final int mChildWithS = 138;
    int childNum;
    public float mDownX;
    public float mDownY;
    public int mPage;
    PageListener mPageListener;
    public float mStartX;
    
    public MenuSlide(final Context context, final AttributeSet set) {
        super(context, set);
        this.childNum = 10;
        this.mPage = 0;
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0: {
                this.mStartX = motionEvent.getX();
                this.mDownX = motionEvent.getX();
                this.mDownY = motionEvent.getY();
                break;
            }
            case 2: {
                if (Math.abs(motionEvent.getX() - this.mDownX) > Math.abs(motionEvent.getY() - this.mDownY)) {
                    return true;
                }
                break;
            }
        }
        return false;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        Label_0028: {
            switch (motionEvent.getAction()) {
                case 2: {
                    final float x = motionEvent.getX();
                    final int n = (int)(this.mDownX - x + 0.5f);
                    final int n2 = this.getScrollX() + n;
                    if (n2 > 710) {
                        this.scrollTo(710, 0);
                        return true;
                    }
                    if (n2 < -20) {
                        this.scrollTo(-20, 0);
                        return true;
                    }
                    this.scrollBy(n, 0);
                    this.mDownX = x;
                    return true;
                }
                case 1: {
                    final float n3 = motionEvent.getX() - this.mStartX;
                    switch (this.mPage) {
                        default: {
                            return true;
                        }
                        case 0: {
                            if (n3 < -80.0f) {
                                this.smoothScrollXTo(690, 500);
                                this.mPage = 1;
                                if (this.mOnSlipeListener != null) {
                                    this.mOnSlipeListener.lesten(3);
                                }
                                if (this.mPageListener != null) {
                                    this.mPageListener.pageListener(1);
                                    return true;
                                }
                                break Label_0028;
                            }
                            else {
                                this.smoothScrollXTo(0, 100);
                                if (this.mOnSlipeListener != null) {
                                    this.mOnSlipeListener.lesten(2);
                                    return true;
                                }
                                break Label_0028;
                            }
                            break;
                        }
                        case 1: {
                            if (n3 > 80.0f) {
                                this.smoothScrollXTo(0, 500);
                                this.mPage = 0;
                                if (this.mOnSlipeListener != null) {
                                    this.mOnSlipeListener.lesten(4);
                                }
                                if (this.mPageListener != null) {
                                    this.mPageListener.pageListener(0);
                                    return true;
                                }
                                break Label_0028;
                            }
                            else {
                                this.smoothScrollXTo(690, 100);
                                this.mPage = 1;
                                if (this.mOnSlipeListener != null) {
                                    this.mOnSlipeListener.lesten(5);
                                    return true;
                                }
                                break Label_0028;
                            }
                            break;
                        }
                        case 2: {
                            if (n3 > 80.0f) {
                                this.smoothScrollXTo(690, 500);
                                this.mPage = 1;
                                if (this.mOnSlipeListener != null) {
                                    this.mOnSlipeListener.lesten(10);
                                }
                                if (this.mPageListener != null) {
                                    this.mPageListener.pageListener(1);
                                    return true;
                                }
                                break Label_0028;
                            }
                            else {
                                this.smoothScrollXTo(1380, 100);
                                if (this.mOnSlipeListener != null) {
                                    this.mOnSlipeListener.lesten(11);
                                    return true;
                                }
                                break Label_0028;
                            }
                            break;
                        }
                    }
                    break;
                }
            }
        }
        return true;
    }
    
    public void setPageListener(final PageListener mPageListener) {
        this.mPageListener = mPageListener;
    }
    
    public void slide2Page(final int mPage, final int n) {
        switch (mPage) {
            case 0: {
                this.smoothScrollXTo(0, n);
                break;
            }
            case 1: {
                this.smoothScrollXTo(690, n);
                break;
            }
            case 2: {
                this.smoothScrollXTo(1380, n);
                break;
            }
        }
        this.mPage = mPage;
    }
    
    public interface PageListener
    {
        void pageListener(final int p0);
    }
}
