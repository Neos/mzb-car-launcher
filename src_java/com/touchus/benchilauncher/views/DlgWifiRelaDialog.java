package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.bean.*;
import android.content.*;
import android.view.*;
import com.touchus.benchilauncher.utils.*;
import android.text.*;
import android.net.wifi.*;
import android.os.*;
import android.widget.*;

public class DlgWifiRelaDialog extends Dialog implements View$OnClickListener
{
    public BeanWifi currentWifiInfo;
    public int height;
    private Button joinBtn;
    private Context mContext;
    public NetworkDialog parent;
    private EditText pwdEdit;
    private TextWatcher pwdTextWatcher;
    public String showMessage;
    public String title;
    public int width;
    
    public DlgWifiRelaDialog(final Context mContext) {
        super(mContext);
        this.width = 415;
        this.height = 150;
        this.showMessage = "";
        this.title = "";
        this.pwdTextWatcher = (TextWatcher)new TextWatcher() {
            public void afterTextChanged(final Editable editable) {
            }
            
            public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
            
            public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                if (charSequence.length() >= 8 && !DlgWifiRelaDialog.this.joinBtn.isEnabled()) {
                    DlgWifiRelaDialog.this.joinBtn.setEnabled(true);
                }
            }
        };
        this.mContext = mContext;
    }
    
    public DlgWifiRelaDialog(final Context mContext, final int n) {
        super(mContext, n);
        this.width = 415;
        this.height = 150;
        this.showMessage = "";
        this.title = "";
        this.pwdTextWatcher = (TextWatcher)new TextWatcher() {
            public void afterTextChanged(final Editable editable) {
            }
            
            public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
            
            public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                if (charSequence.length() >= 8 && !DlgWifiRelaDialog.this.joinBtn.isEnabled()) {
                    DlgWifiRelaDialog.this.joinBtn.setEnabled(true);
                }
            }
        };
        this.mContext = mContext;
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427427) {
            if (this.currentWifiInfo.getState() == 1) {
                WifiTool.disConnectWifi(this.mContext);
                this.parent.updateWifiList();
            }
            else {
                final WifiConfiguration hadSaveWifiConfig = WifiTool.getHadSaveWifiConfig(this.mContext, this.currentWifiInfo.getName());
                if (hadSaveWifiConfig != null) {
                    WifiTool.connectToHadSaveConfigWifi(this.mContext, hadSaveWifiConfig.networkId);
                    this.parent.disConnectCurWifi();
                    this.parent.connectingToSpecifyWifi(this.currentWifiInfo.getName());
                }
                else {
                    ToastTool.showLongToast(this.mContext, this.mContext.getString(2131165313));
                }
            }
        }
        else if (view.getId() == 2131427459) {
            final WifiConfiguration hadSaveWifiConfig2 = WifiTool.getHadSaveWifiConfig(this.mContext, this.currentWifiInfo.getName());
            if (hadSaveWifiConfig2 != null) {
                WifiTool.removeWifiConfig(this.mContext, hadSaveWifiConfig2.networkId);
                if (this.currentWifiInfo.getState() == 1) {
                    this.parent.updateWifiList();
                }
            }
        }
        else if (view.getId() == 2131427457) {
            final String string = this.pwdEdit.getText().toString();
            if (TextUtils.isEmpty((CharSequence)string)) {
                ToastTool.showLongToast(this.mContext, this.mContext.getString(2131165319));
                return;
            }
            if (string.length() < 8) {
                ToastTool.showLongToast(this.mContext, this.mContext.getString(2131165325));
                return;
            }
            this.parent.connectingToSpecifyWifi(this.currentWifiInfo.getName());
            if (!WifiTool.connectToSpecifyWifi(this.mContext, WifiTool.CreateWifiInfo(this.mContext, this.currentWifiInfo.getName(), string, WifiTool.getCurrentWifiPwdType(this.currentWifiInfo.getCapabilities())))) {
                ToastTool.showLongToast(this.mContext, String.valueOf(this.mContext.getString(2131165314)) + this.currentWifiInfo.getName());
                this.parent.updateWifiList();
            }
        }
        this.dismiss();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final WifiConfiguration hadSaveWifiConfig = WifiTool.getHadSaveWifiConfig(this.mContext, this.currentWifiInfo.getName());
        if (this.currentWifiInfo.getState() == 3 && hadSaveWifiConfig == null) {
            this.setContentView(2130903064);
            final TextView textView = (TextView)this.findViewById(2131427455);
            this.joinBtn = (Button)this.findViewById(2131427457);
            this.pwdEdit = (EditText)this.findViewById(2131427456);
            this.joinBtn.setOnClickListener((View$OnClickListener)this);
            this.joinBtn.setEnabled(true);
            this.joinBtn.setText((CharSequence)this.mContext.getString(2131165321));
            if (TextUtils.isEmpty((CharSequence)this.showMessage)) {
                this.showMessage = this.mContext.getString(2131165320);
            }
            textView.setText((CharSequence)this.showMessage);
        }
        else {
            this.setContentView(2130903065);
            final Button button = (Button)this.findViewById(2131427459);
            final Button button2 = (Button)this.findViewById(2131427427);
            button2.setOnClickListener((View$OnClickListener)this);
            button.setOnClickListener((View$OnClickListener)this);
            if (this.currentWifiInfo.getState() == 1) {
                button2.setText((CharSequence)this.mContext.getString(2131165323));
            }
            else if (this.currentWifiInfo.getState() == 3) {
                button2.setText((CharSequence)this.mContext.getString(2131165322));
            }
            button.setText((CharSequence)this.mContext.getString(2131165324));
            this.height *= (int)0.7;
        }
        ((TextView)this.findViewById(2131427454)).setText((CharSequence)this.currentWifiInfo.getName());
        this.getWindow().setLayout(this.width, this.height);
    }
}
