package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.adapter.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.utils.*;
import android.content.*;
import com.touchus.publicutils.utils.*;
import java.util.*;
import android.widget.*;
import android.view.*;
import com.touchus.publicutils.sysconst.*;
import android.os.*;
import com.backaudio.android.driver.*;
import java.lang.ref.*;

public class SettingDialog extends Dialog
{
    public SettingDialog(final Context context) {
        super(context);
    }
    
    public SettingDialog(final Context context, final int n) {
        super(context, n);
    }
    
    protected void onStart() {
        super.onStart();
    }
    
    public static class Builder
    {
        private TextView benzconfig;
        private int clickCount;
        private Context context;
        private int currentSelectIndex;
        private DialogItemAdapter mAdapter;
        private LauncherApplication mApp;
        private SettingDialog mDialog;
        private Launcher mLauncher;
        public SettingHandler mSettingHandler;
        private ListView mlistView;
        Thread resetClickCountThread;
        private List<String> strings;
        
        public Builder(final Context context) {
            this.currentSelectIndex = 0;
            this.clickCount = 0;
            this.resetClickCountThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Builder.access$0(Builder.this, 0);
                }
            });
            this.mSettingHandler = new SettingHandler(this);
            this.context = context;
        }
        
        static /* synthetic */ void access$0(final Builder builder, final int clickCount) {
            builder.clickCount = clickCount;
        }
        
        static /* synthetic */ void access$1(final Builder builder, final int currentSelectIndex) {
            builder.currentSelectIndex = currentSelectIndex;
        }
        
        private void artAppset() {
            final AppSettingDialog currentDialog2 = new AppSettingDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artConfig() {
            final ConfigSetDialog currentDialog2 = new ConfigSetDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artDaoche() {
            final ReverseDialog currentDialog2 = new ReverseDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artLanguage() {
            final LanguageDialog currentDialog2 = new LanguageDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artNetwork() {
            final NetworkDialog currentDialog2 = new NetworkDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artSound() {
            final SoundSetDialog.Builder builder = new SoundSetDialog.Builder(this.context);
            final SoundSetDialog create = builder.create();
            DialogUtil.setDialogLocation(create, 300, -20);
            create.show();
            this.mApp.currentDialog2 = create;
            this.unRigisterHandler();
            create.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    builder.unregisterHandlerr();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artSystem() {
            final SystemSetDialog currentDialog2 = new SystemSetDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void artVersion() {
            final XitongDialog currentDialog2 = new XitongDialog(this.context, 2131230726);
            DialogUtil.setDialogLocation(this.mApp.currentDialog2 = currentDialog2, 400, 0);
            currentDialog2.show();
            this.unRigisterHandler();
            currentDialog2.setOnDismissListener((DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
                public void onDismiss(final DialogInterface dialogInterface) {
                    Builder.this.RigisterHandler();
                    currentDialog2.unregisterHandlerr();
                    Builder.this.mApp.currentDialog2 = null;
                }
            });
        }
        
        private void clickItem(final int currentSelectIndex) {
            this.currentSelectIndex = currentSelectIndex;
            this.seclectTrue();
            this.pressBTItem(this.currentSelectIndex);
        }
        
        private void pressAMPItem(final int n) {
            if (n != 0) {
                this.pressBTItem(n - 1);
            }
        }
        
        private void pressBTItem(final int n) {
            if (UtilTools.isFastDoubleClick()) {
                return;
            }
            switch (n) {
                default: {}
                case 0: {
                    this.artAppset();
                }
                case 1: {
                    this.artSound();
                }
                case 2: {
                    this.artLanguage();
                }
                case 3: {
                    this.artDaoche();
                }
                case 4: {
                    this.artNetwork();
                }
                case 5: {
                    this.artSystem();
                }
                case 6: {
                    this.artVersion();
                }
            }
        }
        
        private void seclectTrue() {
            this.mAdapter.setSeclectIndex(this.currentSelectIndex);
            this.mAdapter.notifyDataSetChanged();
        }
        
        public void RigisterHandler() {
            if (this.mApp != null) {
                this.mApp.registerHandler(this.mSettingHandler);
            }
        }
        
        public SettingDialog create() {
            this.mLauncher = (Launcher)this.context;
            this.mDialog = new SettingDialog(this.context, 2131230726);
            final View inflate = ((LayoutInflater)this.context.getSystemService("layout_inflater")).inflate(2130903058, (ViewGroup)null);
            this.mDialog.addContentView(inflate, new ViewGroup$LayoutParams(-1, -2));
            this.mlistView = (ListView)inflate.findViewById(2131427356);
            this.benzconfig = (TextView)inflate.findViewById(2131427434);
            (this.strings = new ArrayList<String>()).add(this.context.getString(2131165252));
            this.strings.add(this.context.getString(2131165257));
            this.strings.add(this.context.getString(2131165253));
            this.strings.add(this.context.getString(2131165254));
            this.strings.add(this.context.getString(2131165256));
            this.strings.add(this.context.getString(2131165258));
            this.strings.add(this.context.getString(2131165255));
            (this.mAdapter = new DialogItemAdapter(this.context, this.strings)).setSeclectIndex(this.currentSelectIndex);
            this.mlistView.setAdapter((ListAdapter)this.mAdapter);
            this.mlistView.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
                public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                    Builder.access$1(Builder.this, n);
                    Builder.this.clickItem(Builder.this.currentSelectIndex);
                }
            });
            this.benzconfig.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    if (BenzModel.benzCan == BenzModel.EBenzCAN.ZMYT) {
                        if (!UtilTools.isFastDoubleClick()) {
                            Builder.access$0(Builder.this, 0);
                        }
                        else {
                            final Builder this$1 = Builder.this;
                            Builder.access$0(this$1, this$1.clickCount + 1);
                            if (Builder.this.clickCount >= 2) {
                                Builder.access$0(Builder.this, 0);
                                Builder.this.artConfig();
                            }
                        }
                        if (Builder.this.clickCount != 0) {
                            Builder.this.mSettingHandler.removeCallbacks((Runnable)Builder.this.resetClickCountThread);
                            Builder.this.mSettingHandler.postDelayed((Runnable)Builder.this.resetClickCountThread, 1000L);
                        }
                    }
                }
            });
            this.currentSelectIndex = 0;
            this.seclectTrue();
            this.mApp = (LauncherApplication)this.mLauncher.getApplication();
            this.RigisterHandler();
            this.mDialog.setContentView(inflate);
            return this.mDialog;
        }
        
        public void handlerMsgUSB(final Message message) {
            if (message.what == 6001) {
                final byte byte1 = message.getData().getByte("idriver_enum");
                if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode()) {
                    if (this.currentSelectIndex < this.strings.size() - 1) {
                        ++this.currentSelectIndex;
                    }
                    this.seclectTrue();
                }
                else {
                    if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode()) {
                        if (this.currentSelectIndex > 0) {
                            --this.currentSelectIndex;
                        }
                        this.seclectTrue();
                        return;
                    }
                    if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                        this.pressBTItem(this.currentSelectIndex);
                        return;
                    }
                    if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                        this.mDialog.dismiss();
                        return;
                    }
                    if (byte1 == Mainboard.EIdriverEnum.DOWN.getCode()) {
                        if (this.currentSelectIndex < this.strings.size() - 1) {
                            ++this.currentSelectIndex;
                        }
                        this.seclectTrue();
                        return;
                    }
                    if (byte1 == Mainboard.EIdriverEnum.UP.getCode()) {
                        if (this.currentSelectIndex > 0) {
                            --this.currentSelectIndex;
                        }
                        this.seclectTrue();
                    }
                }
            }
        }
        
        public void unRigisterHandler() {
            if (this.mApp != null) {
                this.mApp.unregisterHandler(this.mSettingHandler);
            }
        }
        
        static class SettingHandler extends Handler
        {
            private WeakReference<Builder> target;
            
            public SettingHandler(final Builder builder) {
                this.target = new WeakReference<Builder>(builder);
            }
            
            public void handleMessage(final Message message) {
                if (this.target.get() == null) {
                    return;
                }
                this.target.get().handlerMsgUSB(message);
            }
        }
    }
}
