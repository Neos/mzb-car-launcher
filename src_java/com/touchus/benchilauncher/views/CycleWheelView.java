package com.touchus.benchilauncher.views;

import android.os.*;
import android.content.*;
import android.util.*;
import android.graphics.drawable.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;
import java.util.*;

public class CycleWheelView extends ListView
{
    private static final int COLOR_DIVIDER_DEFALUT;
    private static final int COLOR_SOLID_DEFAULT;
    private static final int COLOR_SOLID_SELET_DEFAULT;
    private static final int HEIGHT_DIVIDER_DEFAULT = 2;
    public static final String TAG;
    private static final int WHEEL_SIZE_DEFAULT = 3;
    private boolean cylceEnable;
    private int dividerColor;
    private int dividerHeight;
    private CycleWheelViewAdapter mAdapter;
    private float mAlphaGradual;
    private int mCurrentPositon;
    private Handler mHandler;
    private int mItemHeight;
    private int mItemLabelTvId;
    private int mItemLayoutId;
    private WheelItemSelectedListener mItemSelectedListener;
    private int mLabelColor;
    private int mLabelSelectColor;
    private List<String> mLabels;
    private int mWheelSize;
    private int seletedSolidColor;
    private int solidColor;
    
    static {
        TAG = CycleWheelView.class.getSimpleName();
        COLOR_DIVIDER_DEFALUT = Color.parseColor("#747474");
        COLOR_SOLID_DEFAULT = Color.parseColor("#3e4043");
        COLOR_SOLID_SELET_DEFAULT = Color.parseColor("#323335");
    }
    
    public CycleWheelView(final Context context) {
        super(context);
        this.mLabelSelectColor = -1;
        this.mLabelColor = -7829368;
        this.mAlphaGradual = 0.7f;
        this.dividerColor = CycleWheelView.COLOR_DIVIDER_DEFALUT;
        this.dividerHeight = 2;
        this.seletedSolidColor = CycleWheelView.COLOR_SOLID_SELET_DEFAULT;
        this.solidColor = CycleWheelView.COLOR_SOLID_DEFAULT;
        this.mWheelSize = 3;
    }
    
    public CycleWheelView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mLabelSelectColor = -1;
        this.mLabelColor = -7829368;
        this.mAlphaGradual = 0.7f;
        this.dividerColor = CycleWheelView.COLOR_DIVIDER_DEFALUT;
        this.dividerHeight = 2;
        this.seletedSolidColor = CycleWheelView.COLOR_SOLID_SELET_DEFAULT;
        this.solidColor = CycleWheelView.COLOR_SOLID_DEFAULT;
        this.mWheelSize = 3;
        this.init();
    }
    
    public CycleWheelView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mLabelSelectColor = -1;
        this.mLabelColor = -7829368;
        this.mAlphaGradual = 0.7f;
        this.dividerColor = CycleWheelView.COLOR_DIVIDER_DEFALUT;
        this.dividerHeight = 2;
        this.seletedSolidColor = CycleWheelView.COLOR_SOLID_SELET_DEFAULT;
        this.solidColor = CycleWheelView.COLOR_SOLID_DEFAULT;
        this.mWheelSize = 3;
    }
    
    static /* synthetic */ void access$8(final CycleWheelView cycleWheelView, final int selection) {
        cycleWheelView.setSelection(selection);
    }
    
    private int getDistance(final float n) {
        if (Math.abs(n) <= 2.0f) {
            return (int)n;
        }
        if (Math.abs(n) >= 12.0f) {
            return (int)(n / 6.0f);
        }
        if (n > 0.0f) {
            return 2;
        }
        return -2;
    }
    
    private int getPosition(final int n) {
        int n2;
        if (this.mLabels == null || this.mLabels.size() == 0) {
            n2 = 0;
        }
        else {
            n2 = n;
            if (this.cylceEnable) {
                return n + this.mLabels.size() * (1073741823 / this.mLabels.size());
            }
        }
        return n2;
    }
    
    private void init() {
        this.mHandler = new Handler();
        this.mItemLayoutId = 2130903084;
        this.mItemLabelTvId = 2131427579;
        this.mAdapter = new CycleWheelViewAdapter();
        this.setVerticalScrollBarEnabled(false);
        this.setScrollingCacheEnabled(false);
        this.setCacheColorHint(0);
        this.setFadingEdgeLength(0);
        this.setOverScrollMode(2);
        this.setDividerHeight(0);
        this.setAdapter((ListAdapter)this.mAdapter);
        this.setOnScrollListener((AbsListView$OnScrollListener)new AbsListView$OnScrollListener() {
            public void onScroll(final AbsListView absListView, final int n, final int n2, final int n3) {
                CycleWheelView.this.refreshItems();
            }
            
            public void onScrollStateChanged(final AbsListView absListView, final int n) {
                if (n == 0) {
                    final View child = CycleWheelView.this.getChildAt(0);
                    if (child != null) {
                        final float y = child.getY();
                        if (y != 0.0f) {
                            if (Math.abs(y) < CycleWheelView.this.mItemHeight / 2) {
                                CycleWheelView.this.smoothScrollBy(CycleWheelView.this.getDistance(y), 50);
                                return;
                            }
                            CycleWheelView.this.smoothScrollBy(CycleWheelView.this.getDistance(CycleWheelView.this.mItemHeight + y), 50);
                        }
                    }
                }
            }
        });
    }
    
    private void initView() {
        this.mItemHeight = this.measureHeight();
        this.getLayoutParams().height = this.mItemHeight * this.mWheelSize;
        this.mAdapter.setData(this.mLabels);
        this.mAdapter.notifyDataSetChanged();
        this.setBackgroundDrawable((Drawable)new Drawable() {
            public void draw(final Canvas canvas) {
                final int width = CycleWheelView.this.getWidth();
                final Paint paint = new Paint();
                paint.setColor(CycleWheelView.this.dividerColor);
                paint.setStrokeWidth((float)CycleWheelView.this.dividerHeight);
                final Paint paint2 = new Paint();
                paint2.setColor(CycleWheelView.this.seletedSolidColor);
                final Paint paint3 = new Paint();
                paint3.setColor(CycleWheelView.this.solidColor);
                canvas.drawRect(0.0f, 0.0f, (float)width, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2)), paint3);
                canvas.drawRect(0.0f, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2 + 1)), (float)width, (float)(CycleWheelView.this.mItemHeight * CycleWheelView.this.mWheelSize), paint3);
                canvas.drawRect(0.0f, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2)), (float)width, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2 + 1)), paint2);
                canvas.drawLine(0.0f, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2)), (float)width, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2)), paint);
                canvas.drawLine(0.0f, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2 + 1)), (float)width, (float)(CycleWheelView.this.mItemHeight * (CycleWheelView.this.mWheelSize / 2 + 1)), paint);
            }
            
            public int getOpacity() {
                return 0;
            }
            
            public void setAlpha(final int n) {
            }
            
            public void setColorFilter(final ColorFilter colorFilter) {
            }
        });
    }
    
    private int measureHeight() {
        final View inflate = LayoutInflater.from(this.getContext()).inflate(this.mItemLayoutId, (ViewGroup)null);
        inflate.setLayoutParams(new ViewGroup$LayoutParams(-1, -2));
        inflate.measure(View$MeasureSpec.makeMeasureSpec(0, 0), View$MeasureSpec.makeMeasureSpec(0, 0));
        return inflate.getMeasuredHeight();
    }
    
    private void refreshItems() {
        final int n = this.mWheelSize / 2;
        final int firstVisiblePosition = this.getFirstVisiblePosition();
        if (this.getChildAt(0) != null) {
            int mCurrentPositon;
            if (Math.abs(this.getChildAt(0).getY()) <= this.mItemHeight / 2) {
                mCurrentPositon = firstVisiblePosition + n;
            }
            else {
                mCurrentPositon = firstVisiblePosition + n + 1;
            }
            if (mCurrentPositon != this.mCurrentPositon) {
                this.mCurrentPositon = mCurrentPositon;
                if (this.mItemSelectedListener != null) {
                    this.mItemSelectedListener.onItemSelected(this.getSelection(), this.getSelectLabel());
                }
                this.resetItems(firstVisiblePosition, mCurrentPositon, n);
            }
        }
    }
    
    private void resetItems(final int n, final int n2, final int n3) {
        for (int i = n2 - n3 - 1; i < n2 + n3 + 1; ++i) {
            final View child = this.getChildAt(i - n);
            if (child != null) {
                final TextView textView = (TextView)child.findViewById(this.mItemLabelTvId);
                if (n2 == i) {
                    textView.setTextColor(this.mLabelSelectColor);
                    child.setAlpha(1.0f);
                }
                else {
                    textView.setTextColor(this.mLabelColor);
                    child.setAlpha((float)Math.pow(this.mAlphaGradual, Math.abs(i - n2)));
                }
            }
        }
    }
    
    public List<String> getLabels() {
        return this.mLabels;
    }
    
    public String getSelectLabel() {
        int selection;
        if ((selection = this.getSelection()) < 0) {
            selection = 0;
        }
        try {
            return this.mLabels.get(selection);
        }
        catch (Exception ex) {
            return "";
        }
    }
    
    public int getSelection() {
        if (this.mCurrentPositon == 0) {
            this.mCurrentPositon = this.mWheelSize / 2;
        }
        return (this.mCurrentPositon - this.mWheelSize / 2) % this.mLabels.size();
    }
    
    public void setAlphaGradual(final float mAlphaGradual) {
        this.mAlphaGradual = mAlphaGradual;
        this.resetItems(this.getFirstVisiblePosition(), this.mCurrentPositon, this.mWheelSize / 2);
    }
    
    public void setCycleEnable(final boolean cylceEnable) {
        if (this.cylceEnable != cylceEnable) {
            this.cylceEnable = cylceEnable;
            this.mAdapter.notifyDataSetChanged();
            this.setSelection(this.getSelection());
        }
    }
    
    public void setDivider(final int dividerColor, final int dividerHeight) {
        this.dividerColor = dividerColor;
        this.dividerHeight = dividerHeight;
    }
    
    public void setLabelColor(final int mLabelColor) {
        this.mLabelColor = mLabelColor;
        this.resetItems(this.getFirstVisiblePosition(), this.mCurrentPositon, this.mWheelSize / 2);
    }
    
    public void setLabelSelectColor(final int mLabelSelectColor) {
        this.mLabelSelectColor = mLabelSelectColor;
        this.resetItems(this.getFirstVisiblePosition(), this.mCurrentPositon, this.mWheelSize / 2);
    }
    
    public void setLabels(final List<String> mLabels) {
        this.mLabels = mLabels;
        this.mAdapter.setData(this.mLabels);
        this.mAdapter.notifyDataSetChanged();
        this.initView();
    }
    
    public void setOnWheelItemSelectedListener(final WheelItemSelectedListener mItemSelectedListener) {
        this.mItemSelectedListener = mItemSelectedListener;
    }
    
    public void setSelection(final int n) {
        this.mHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                CycleWheelView.access$8(CycleWheelView.this, CycleWheelView.this.getPosition(n));
            }
        });
    }
    
    public void setSolid(final int solidColor, final int seletedSolidColor) {
        this.solidColor = solidColor;
        this.seletedSolidColor = seletedSolidColor;
        this.initView();
    }
    
    public void setWheelItemLayout(final int mItemLayoutId, final int mItemLabelTvId) {
        this.mItemLayoutId = mItemLayoutId;
        this.mItemLabelTvId = mItemLabelTvId;
        (this.mAdapter = new CycleWheelViewAdapter()).setData(this.mLabels);
        this.setAdapter((ListAdapter)this.mAdapter);
        this.initView();
    }
    
    public void setWheelSize(final int mWheelSize) throws CycleWheelViewException {
        if (mWheelSize < 3 || mWheelSize % 2 != 1) {
            throw new CycleWheelViewException("Wheel Size Error , Must Be 3,5,7,9...");
        }
        this.mWheelSize = mWheelSize;
        this.initView();
    }
    
    public class CycleWheelViewAdapter extends BaseAdapter
    {
        private List<String> mData;
        
        public CycleWheelViewAdapter() {
            this.mData = new ArrayList<String>();
        }
        
        public int getCount() {
            if (CycleWheelView.this.cylceEnable) {
                return Integer.MAX_VALUE;
            }
            return this.mData.size() + CycleWheelView.this.mWheelSize - 1;
        }
        
        public Object getItem(final int n) {
            return "";
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            View inflate = view;
            if (view == null) {
                inflate = LayoutInflater.from(CycleWheelView.this.getContext()).inflate(CycleWheelView.this.mItemLayoutId, (ViewGroup)null);
            }
            final TextView textView = (TextView)inflate.findViewById(CycleWheelView.this.mItemLabelTvId);
            if (n < CycleWheelView.this.mWheelSize / 2 || (!CycleWheelView.this.cylceEnable && n >= this.mData.size() + CycleWheelView.this.mWheelSize / 2)) {
                textView.setText((CharSequence)"");
                inflate.setVisibility(4);
                return inflate;
            }
            textView.setText((CharSequence)this.mData.get((n - CycleWheelView.this.mWheelSize / 2) % this.mData.size()));
            inflate.setVisibility(0);
            return inflate;
        }
        
        public boolean isEnabled(final int n) {
            return false;
        }
        
        public void setData(final List<String> list) {
            this.mData.clear();
            this.mData.addAll(list);
        }
    }
    
    public class CycleWheelViewException extends Exception
    {
        private static final long serialVersionUID = 1L;
        
        public CycleWheelViewException(final String s) {
            super(s);
        }
    }
    
    public interface WheelItemSelectedListener
    {
        void onItemSelected(final int p0, final String p1);
    }
}
