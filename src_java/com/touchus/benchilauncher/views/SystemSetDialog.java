package com.touchus.benchilauncher.views;

import android.app.*;
import android.content.*;
import com.touchus.benchilauncher.adapter.*;
import com.touchus.benchilauncher.utils.*;
import com.touchus.benchilauncher.bean.*;
import java.util.*;
import android.util.*;
import com.touchus.benchilauncher.*;
import com.backaudio.android.driver.*;
import android.widget.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class SystemSetDialog extends Dialog implements AdapterView$OnItemClickListener
{
    private Context context;
    int currentSelectIndex;
    private LauncherApplication mApp;
    public MHandler mHandler;
    private SystemSetItemAdapter mSetAdapter;
    private ListView mSetListView;
    private SpUtilK mSpUtilK;
    private SystemSetItemAdapter mSysAdapter;
    private ListView mSysListView;
    private List<SettingInfo> setInfos;
    private List<SettingInfo> sysInfos;
    private int type;
    
    public SystemSetDialog(final Context context) {
        super(context);
        this.setInfos = new ArrayList<SettingInfo>();
        this.sysInfos = new ArrayList<SettingInfo>();
        this.type = 1;
        this.currentSelectIndex = 0;
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    public SystemSetDialog(final Context context, final int n) {
        super(context, n);
        this.setInfos = new ArrayList<SettingInfo>();
        this.sysInfos = new ArrayList<SettingInfo>();
        this.type = 1;
        this.currentSelectIndex = 0;
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    private void initData() {
        this.mApp.ismix = this.mSpUtilK.getBoolean(this.context.getString(2131165346), false);
        this.mApp.isAirhide = this.mSpUtilK.getBoolean(this.context.getString(2131165347), false);
        this.mApp.isOriginalKeyOpen = this.mSpUtilK.getBoolean(this.context.getString(2131165345), false);
        Log.e("", "SystemSetDialog mApp.ismix = " + this.mApp.ismix + ",mApp.isAirhide = " + this.mApp.isAirhide);
        if (!SysConst.isBT()) {
            this.setInfos.add(new SettingInfo(this.context.getString(2131165346), true, this.mApp.ismix));
        }
        this.setInfos.add(new SettingInfo(this.context.getString(2131165345), true, this.mApp.isOriginalKeyOpen));
        if (!SysConst.isBT()) {
            this.setInfos.add(new SettingInfo(this.context.getString(2131165347), true, this.mApp.isAirhide));
        }
        this.sysInfos.add(new SettingInfo(this.context.getString(2131165326), false));
        this.sysInfos.add(new SettingInfo(this.context.getString(2131165348), false));
        this.sysInfos.add(new SettingInfo(this.context.getString(2131165349), false));
    }
    
    private void initView() {
        this.mSetListView = (ListView)this.findViewById(2131427447);
        this.mSetAdapter = new SystemSetItemAdapter(this.context, this.setInfos);
        this.mSetListView.setAdapter((ListAdapter)this.mSetAdapter);
        this.mSetListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        this.mSysListView = (ListView)this.findViewById(2131427448);
        this.mSysAdapter = new SystemSetItemAdapter(this.context, this.sysInfos);
        this.mSysListView.setAdapter((ListAdapter)this.mSysAdapter);
        this.mSysListView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        this.mSetAdapter.setSeclectIndex(0);
        this.mSysAdapter.setSeclectIndex(-1);
    }
    
    private void pressItem(final int seclectIndex) {
        final boolean b = false;
        this.currentSelectIndex = seclectIndex;
        Log.e("", "SystemSetDialog type = " + this.type + ",mApp.isOriginalKeyOpen = " + this.mApp.isOriginalKeyOpen);
        if (this.type == 1) {
            final String item = this.setInfos.get(seclectIndex).getItem();
            final boolean boolean1 = this.mSpUtilK.getBoolean(item, false);
            if (item.equals(this.context.getString(2131165345))) {
                this.mApp.isOriginalKeyOpen = !boolean1;
                this.mSpUtilK.putBoolean(item, this.mApp.isOriginalKeyOpen);
                final byte[] storeData = SysConst.storeData;
                boolean b2;
                if (this.mApp.isOriginalKeyOpen) {
                    b2 = true;
                }
                else {
                    b2 = false;
                }
                storeData[0] = (byte)(b2 ? 1 : 0);
            }
            else if (item.equals(this.context.getString(2131165346))) {
                this.mApp.ismix = !boolean1;
                this.mSpUtilK.putBoolean(item, this.mApp.ismix);
                final byte[] storeData2 = SysConst.storeData;
                boolean b3;
                if (this.mApp.ismix) {
                    b3 = true;
                }
                else {
                    b3 = false;
                }
                storeData2[3] = (byte)(b3 ? 1 : 0);
            }
            else if (item.equals(this.context.getString(2131165347))) {
                this.mApp.isAirhide = !boolean1;
                this.mSpUtilK.putBoolean(item, this.mApp.isAirhide);
                final byte[] storeData3 = SysConst.storeData;
                boolean b4;
                if (this.mApp.isAirhide) {
                    b4 = true;
                }
                else {
                    b4 = false;
                }
                storeData3[8] = (byte)(b4 ? 1 : 0);
            }
            this.setInfos.get(seclectIndex).setChecked(!boolean1 || b);
            Mainboard.getInstance().sendStoreDataToMcu(SysConst.storeData);
            this.mSetAdapter.setSeclectIndex(seclectIndex);
            this.mSysAdapter.setSeclectIndex(-1);
            return;
        }
        final String item2 = this.sysInfos.get(seclectIndex).getItem();
        if (item2.equals(this.context.getString(2131165348))) {
            this.showRecoveryDlg(item2);
        }
        else if (item2.equals(this.context.getString(2131165349))) {
            this.showRecoveryDlg(item2);
        }
        else if (item2.equals(this.context.getString(2131165326))) {
            final FactorySetDialog factorySetDialog = new FactorySetDialog(this.context, 2131230726);
            factorySetDialog.width = 500;
            factorySetDialog.height = 200;
            factorySetDialog.show();
        }
        this.mSetAdapter.setSeclectIndex(-1);
        this.mSysAdapter.setSeclectIndex(seclectIndex);
    }
    
    private void seclectTrue(final int n) {
        if (this.type == 1) {
            this.mSetAdapter.setSeclectIndex(n);
            this.mSysAdapter.setSeclectIndex(-1);
        }
        else if (this.type == 2) {
            this.mSetAdapter.setSeclectIndex(-1);
            this.mSysAdapter.setSeclectIndex(n);
        }
    }
    
    private void showRecoveryDlg(final String title) {
        final SystemOperateDialog systemOperateDialog = new SystemOperateDialog(this.context, 2131230726);
        systemOperateDialog.setTitle(title);
        systemOperateDialog.show();
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.DOWN.getCode()) {
                if (this.type == 1 && this.currentSelectIndex < this.setInfos.size() - 1) {
                    ++this.currentSelectIndex;
                }
                else if (this.type == 1 && this.currentSelectIndex == this.setInfos.size() - 1) {
                    ++this.type;
                    this.currentSelectIndex = 0;
                }
                else if (this.type == 2 && this.currentSelectIndex < this.sysInfos.size() - 1) {
                    ++this.currentSelectIndex;
                }
                this.seclectTrue(this.currentSelectIndex);
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.type == 2 && this.currentSelectIndex > 0) {
                        --this.currentSelectIndex;
                    }
                    else if (this.type == 2 && this.currentSelectIndex == 0) {
                        --this.type;
                        this.currentSelectIndex = this.setInfos.size() - 1;
                    }
                    else if (this.type == 1 && this.currentSelectIndex > 0) {
                        --this.currentSelectIndex;
                    }
                    this.seclectTrue(this.currentSelectIndex);
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.pressItem(this.currentSelectIndex);
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        this.setContentView(2130903061);
        this.mApp = (LauncherApplication)this.context.getApplicationContext();
        this.mSpUtilK = new SpUtilK(this.context);
        this.initData();
        this.initView();
        super.onCreate(bundle);
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        switch (adapterView.getId()) {
            default: {}
            case 2131427447: {
                this.type = 1;
                this.pressItem(n);
            }
            case 2131427448: {
                this.type = 2;
                this.pressItem(n);
            }
        }
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mHandler.removeCallbacksAndMessages((Object)null);
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<SystemSetDialog> target;
        
        public MHandler(final SystemSetDialog systemSetDialog) {
            this.target = new WeakReference<SystemSetDialog>(systemSetDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
