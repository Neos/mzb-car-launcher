package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.*;
import android.content.*;
import android.widget.*;
import java.io.*;
import android.util.*;
import com.touchus.publicutils.sysconst.*;
import java.util.*;
import com.backaudio.android.driver.*;
import com.touchus.benchilauncher.utils.*;
import android.text.*;
import com.touchus.publicutils.utils.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public class FeedbackDialog extends Dialog implements View$OnClickListener
{
    private LauncherApplication app;
    private Button cancelBtn;
    private Context context;
    private boolean iInCancelState;
    public MHandler mHandler;
    private EditText phoneNum;
    private Button submitBtn;
    private EditText suggest;
    private String suggestStr;
    
    public FeedbackDialog(final Context context) {
        super(context);
        this.iInCancelState = true;
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    public FeedbackDialog(final Context context, final int n) {
        super(context, n);
        this.iInCancelState = true;
        this.mHandler = new MHandler(this);
        this.context = context;
    }
    
    private void submit() {
        final String string = this.context.getApplicationContext().getFilesDir() + File.separator;
        Log.e("", "path = " + string);
        final File[] listFiles = new File(string).listFiles();
        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < listFiles.length; ++i) {
            Log.e("", "files[i].getAbsolutePath() = " + listFiles[i].getAbsolutePath());
            list.add(listFiles[i].getAbsolutePath());
        }
        final String string2 = "/data/anr" + File.separator;
        Log.e("", "path = " + string2);
        final File[] listFiles2 = new File(string2).listFiles();
        for (int j = 0; j < listFiles2.length; ++j) {
            Log.e("", "files[i].getAbsolutePath() = " + listFiles2[j].getAbsolutePath());
            if (!listFiles2[j].isDirectory()) {
                list.add(listFiles2[j].getAbsolutePath());
            }
        }
        if (list.size() == 0) {
            return;
        }
        MailManager.getInstance(this.context).sendMailWithMultiFile(String.valueOf(BenzModel.benzName()) + "\u5954\u9a70\u53cd\u9988\u6536\u96c6" + TimeUtils.getSimpleDate(), "\u53cd\u9988\u610f\u89c1\uff1a" + this.suggestStr + "\n\u8054\u7cfb\u53f7\u7801\uff1a" + this.phoneNum.getText().toString() + "\u5730\u5740\uff1a" + this.app.address, list);
        this.dismiss();
    }
    
    public void dismiss() {
        this.app.currentDialog3 = null;
        super.dismiss();
    }
    
    public void handlerMsgUSB(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.iInCancelState = true;
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    this.iInCancelState = false;
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    if (this.iInCancelState) {
                        this.dismiss();
                        return;
                    }
                    this.submit();
                }
                else {
                    if (byte1 == Mainboard.EIdriverEnum.BACK.getCode()) {
                        this.dismiss();
                        return;
                    }
                    if (byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                        this.dismiss();
                    }
                }
            }
        }
        else if (message.what == 1039) {
            if (message.getData().getBoolean("FEEDBACK_RESULT")) {
                ToastTool.showShortToast(this.context, "send successful");
                return;
            }
            ToastTool.showShortToast(this.context, "send failed");
        }
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427405) {
            this.dismiss();
            return;
        }
        this.suggestStr = this.suggest.getText().toString();
        if (TextUtils.isEmpty((CharSequence)this.suggestStr)) {
            this.suggest.setError((CharSequence)this.context.getString(2131165331));
            return;
        }
        if (UtilTools.isNetworkConnected(this.context)) {
            this.submit();
            return;
        }
        ToastTool.showShortToast(this.context, this.context.getString(2131165334));
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903053);
        this.app = (LauncherApplication)this.context.getApplicationContext();
        this.suggest = (EditText)this.findViewById(2131427402);
        this.phoneNum = (EditText)this.findViewById(2131427403);
        this.submitBtn = (Button)this.findViewById(2131427404);
        this.cancelBtn = (Button)this.findViewById(2131427405);
        this.submitBtn.setOnClickListener((View$OnClickListener)this);
        this.cancelBtn.setOnClickListener((View$OnClickListener)this);
        final Window window = this.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        attributes.x = 400;
        attributes.y = 0;
        window.setAttributes(attributes);
        this.submitBtn.setVisibility(0);
        this.cancelBtn.setVisibility(0);
        this.submitBtn.setEnabled(true);
    }
    
    protected void onStart() {
        this.app.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.app.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<FeedbackDialog> target;
        
        public MHandler(final FeedbackDialog feedbackDialog) {
            this.target = new WeakReference<FeedbackDialog>(feedbackDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
