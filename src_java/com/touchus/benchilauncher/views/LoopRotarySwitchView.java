package com.touchus.benchilauncher.views;

import android.content.*;
import com.touchus.benchilauncher.inface.*;
import android.widget.*;
import com.touchus.benchilauncher.*;
import android.content.res.*;
import android.view.animation.*;
import android.animation.*;
import java.util.*;
import android.view.*;
import android.util.*;

public class LoopRotarySwitchView extends RelativeLayout
{
    private static final int LoopR = 200;
    private static final float RADIO_DEFAULT_CHILD_DIMENSION = 0.25f;
    private static final int horizontal = 1;
    private static final int vertical = 0;
    private float RADIO_DEFAULT_CENTERITEM_DIMENSION;
    private float angle;
    private AutoScrollDirection autoRotatinDirection;
    private boolean autoRotation;
    protected boolean clickRotation;
    private float distance;
    private boolean isCanClickListener;
    private boolean isFirst;
    private float last_angle;
    private float limitX;
    LoopRotarySwitchViewHandler loopHandler;
    private int loopRotationX;
    private int loopRotationZ;
    private Context mContext;
    private GestureDetector mGestureDetector;
    private int mOrientation;
    private float multiple;
    private OnItemClickListener onItemClickListener;
    private OnItemSelectedListener onItemSelectedListener;
    private OnLoopViewTouchListener onLoopViewTouchListener;
    private float r;
    private ValueAnimator rAnimation;
    private ValueAnimator restAnimator;
    private int selectItem;
    private int size;
    private List<TextView> textViews;
    private String[] texts;
    private boolean touching;
    private List<View> views;
    private float x;
    private ValueAnimator xAnimation;
    private ValueAnimator zAnimation;
    
    static /* synthetic */ int[] $SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection() {
        final int[] $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection = LoopRotarySwitchView.$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection;
        if ($switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection != null) {
            return $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection;
        }
        final int[] $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2 = new int[AutoScrollDirection.values().length];
        while (true) {
            try {
                $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2[AutoScrollDirection.left.ordinal()] = 1;
                try {
                    $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2[AutoScrollDirection.right.ordinal()] = 2;
                    return LoopRotarySwitchView.$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection = $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2;
                }
                catch (NoSuchFieldError noSuchFieldError) {}
            }
            catch (NoSuchFieldError noSuchFieldError2) {
                continue;
            }
            break;
        }
    }
    
    public LoopRotarySwitchView(final Context context) {
        this(context, null);
    }
    
    public LoopRotarySwitchView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public LoopRotarySwitchView(final Context mContext, final AttributeSet set, int int1) {
        super(mContext, set, int1);
        this.mOrientation = 1;
        this.restAnimator = null;
        this.rAnimation = null;
        this.zAnimation = null;
        this.xAnimation = null;
        this.loopRotationX = 0;
        this.loopRotationZ = 0;
        this.mGestureDetector = null;
        this.selectItem = 0;
        this.size = 5;
        this.r = 200.0f;
        this.multiple = 2.0f;
        this.distance = this.multiple * this.r;
        this.angle = 0.0f;
        this.last_angle = 0.0f;
        this.clickRotation = true;
        this.autoRotation = false;
        this.touching = false;
        this.isFirst = false;
        this.autoRotatinDirection = AutoScrollDirection.right;
        this.views = new ArrayList<View>();
        this.onItemSelectedListener = null;
        this.onLoopViewTouchListener = null;
        this.onItemClickListener = null;
        this.isCanClickListener = true;
        this.limitX = 30.0f;
        this.RADIO_DEFAULT_CENTERITEM_DIMENSION = 0.5f;
        this.loopHandler = new LoopRotarySwitchViewHandler(3000) {
            static /* synthetic */ int[] $SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection() {
                final int[] $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection = LoopRotarySwitchView$1.$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection;
                if ($switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection != null) {
                    return $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection;
                }
                final int[] $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2 = new int[AutoScrollDirection.values().length];
                while (true) {
                    try {
                        $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2[AutoScrollDirection.left.ordinal()] = 1;
                        try {
                            $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2[AutoScrollDirection.right.ordinal()] = 2;
                            return LoopRotarySwitchView$1.$SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection = $switch_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection2;
                        }
                        catch (NoSuchFieldError noSuchFieldError) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError2) {
                        continue;
                    }
                    break;
                }
            }
            
            @Override
            public void doScroll() {
                while (true) {
                    while (true) {
                        Label_0124: {
                            try {
                                if (LoopRotarySwitchView.this.size != 0) {
                                    int n = 0;
                                    switch ($SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection()[LoopRotarySwitchView.this.autoRotatinDirection.ordinal()]) {
                                        case 1: {
                                            n = 360 / LoopRotarySwitchView.this.size;
                                            break;
                                        }
                                        case 2: {
                                            n = -360 / LoopRotarySwitchView.this.size;
                                            break;
                                        }
                                        default: {
                                            break Label_0124;
                                        }
                                    }
                                    if (LoopRotarySwitchView.this.angle == 360.0f) {
                                        LoopRotarySwitchView.access$3(LoopRotarySwitchView.this, 0.0f);
                                    }
                                    LoopRotarySwitchView.this.AnimRotationTo(LoopRotarySwitchView.this.angle + n, null);
                                    return;
                                }
                                break;
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                                return;
                            }
                        }
                        continue;
                    }
                }
            }
        };
        this.textViews = new ArrayList<TextView>();
        this.mContext = mContext;
        final TypedArray obtainStyledAttributes = mContext.obtainStyledAttributes(set, R.styleable.LoopRotarySwitchView);
        this.mOrientation = obtainStyledAttributes.getInt(0, 1);
        this.autoRotation = obtainStyledAttributes.getBoolean(1, false);
        this.r = obtainStyledAttributes.getDimension(2, 200.0f);
        int1 = obtainStyledAttributes.getInt(3, 0);
        obtainStyledAttributes.recycle();
        this.mGestureDetector = new GestureDetector(mContext, (GestureDetector$OnGestureListener)this.getGeomeryController());
        if (this.mOrientation == 1) {
            this.loopRotationZ = 0;
        }
        else {
            this.loopRotationZ = 90;
        }
        if (int1 == 0) {
            this.autoRotatinDirection = AutoScrollDirection.left;
        }
        else {
            this.autoRotatinDirection = AutoScrollDirection.right;
        }
        this.loopHandler.setLoop(this.autoRotation);
    }
    
    private void AnimRotationTo(final float n, final Runnable runnable) {
        if (this.angle == n) {
            return;
        }
        (this.restAnimator = ValueAnimator.ofFloat(new float[] { this.angle, n })).setInterpolator((TimeInterpolator)new DecelerateInterpolator());
        this.restAnimator.setDuration(300L);
        this.restAnimator.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                if (!LoopRotarySwitchView.this.touching) {
                    LoopRotarySwitchView.access$3(LoopRotarySwitchView.this, (float)valueAnimator.getAnimatedValue());
                    LoopRotarySwitchView.this.initView();
                }
            }
        });
        this.restAnimator.addListener((Animator$AnimatorListener)new Animator$AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                if (!LoopRotarySwitchView.this.touching) {
                    LoopRotarySwitchView.access$15(LoopRotarySwitchView.this, LoopRotarySwitchView.this.calculateItem());
                    if (LoopRotarySwitchView.this.selectItem < 0) {
                        LoopRotarySwitchView.access$15(LoopRotarySwitchView.this, LoopRotarySwitchView.this.size + LoopRotarySwitchView.this.selectItem);
                    }
                    if (LoopRotarySwitchView.this.onItemSelectedListener != null) {
                        if (LoopRotarySwitchView.this.views.size() == 0) {
                            return;
                        }
                        LoopRotarySwitchView.this.onItemSelectedListener.selected(LoopRotarySwitchView.this.selectItem, LoopRotarySwitchView.this.views.get(LoopRotarySwitchView.this.selectItem));
                    }
                }
                LoopRotarySwitchView.this.clickRotation = true;
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
            }
        });
        if (runnable != null) {
            this.restAnimator.addListener((Animator$AnimatorListener)new Animator$AnimatorListener() {
                public void onAnimationCancel(final Animator animator) {
                }
                
                public void onAnimationEnd(final Animator animator) {
                    runnable.run();
                }
                
                public void onAnimationRepeat(final Animator animator) {
                }
                
                public void onAnimationStart(final Animator animator) {
                }
            });
        }
        this.restAnimator.start();
    }
    
    static /* synthetic */ void access$15(final LoopRotarySwitchView loopRotarySwitchView, final int selectItem) {
        loopRotarySwitchView.selectItem = selectItem;
    }
    
    static /* synthetic */ void access$18(final LoopRotarySwitchView loopRotarySwitchView, final int loopRotationX) {
        loopRotarySwitchView.loopRotationX = loopRotationX;
    }
    
    static /* synthetic */ void access$19(final LoopRotarySwitchView loopRotarySwitchView, final int loopRotationZ) {
        loopRotarySwitchView.loopRotationZ = loopRotationZ;
    }
    
    static /* synthetic */ void access$3(final LoopRotarySwitchView loopRotarySwitchView, final float angle) {
        loopRotarySwitchView.angle = angle;
    }
    
    static /* synthetic */ void access$9(final LoopRotarySwitchView loopRotarySwitchView, final float r) {
        loopRotarySwitchView.r = r;
    }
    
    private int calculateItem() {
        return (int)(this.angle / (360 / this.size)) % this.size;
    }
    
    private int calculateItemPosition(final int n) {
        switch (this.calculateItem()) {
            default: {
                return n;
            }
            case -1: {
                return 4;
            }
            case -2: {
                return 3;
            }
            case -3: {
                return 2;
            }
            case -4: {
                return 1;
            }
        }
    }
    
    private void initView() {
        for (int i = 0; i < this.views.size(); ++i) {
            final double n = this.angle + 180.0f - i * 360 / this.size;
            final float n2 = (float)Math.sin(Math.toRadians(n)) * this.r;
            final float n3 = (this.distance - (float)Math.cos(Math.toRadians(n)) * this.r) / (this.distance + this.r);
            this.views.get(i).setScaleX(n3);
            this.views.get(i).setScaleY(n3);
            final float n4 = (float)Math.sin(Math.toRadians(this.loopRotationX * Math.cos(Math.toRadians(n))));
            final float r = this.r;
            final float n5 = -(float)Math.sin(Math.toRadians(-this.loopRotationZ));
            this.views.get(i).setTranslationX(n2 + ((float)Math.cos(Math.toRadians(-this.loopRotationZ)) * n2 - n2));
            this.views.get(i).setTranslationY(n4 * r + n5 * n2);
        }
        final ArrayList<View> list = new ArrayList<View>();
        list.clear();
        for (int j = 0; j < this.views.size(); ++j) {
            list.add(this.views.get(j));
        }
        this.sortList(list);
        this.postInvalidate();
    }
    
    private boolean onTouch(final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.last_angle = this.angle;
            this.touching = true;
        }
        if (this.mGestureDetector.onTouchEvent(motionEvent)) {
            this.getParent().requestDisallowInterceptTouchEvent(true);
        }
        if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            this.touching = false;
            this.restPosition();
        }
        return true;
    }
    
    private <T> void sortList(final List<View> list) {
        final SortComparator sortComparator = new SortComparator((SortComparator)null);
        final Object[] array = list.toArray(new Object[list.size()]);
        Arrays.sort(array, (Comparator<? super Object>)sortComparator);
        int n = 0;
        final ListIterator<View> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            listIterator.next();
            listIterator.set((View)array[n]);
            ++n;
        }
        for (int i = 0; i < list.size(); ++i) {
            list.get(i).bringToFront();
        }
    }
    
    public void RAnimation() {
        this.RAnimation(1.0f, 200.0f);
    }
    
    public void RAnimation(final float n, final float n2) {
        (this.rAnimation = ValueAnimator.ofFloat(new float[] { n, n2 })).addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                LoopRotarySwitchView.access$9(LoopRotarySwitchView.this, (float)valueAnimator.getAnimatedValue());
                LoopRotarySwitchView.this.initView();
            }
        });
        this.rAnimation.setInterpolator((TimeInterpolator)new DecelerateInterpolator());
        this.rAnimation.setDuration(2000L);
        this.rAnimation.start();
    }
    
    public void RAnimation(final boolean b) {
        if (b) {
            this.RAnimation(1.0f, 200.0f);
            return;
        }
        this.RAnimation(200.0f, 1.0f);
    }
    
    public void checkChildView() {
        for (int i = 0; i < this.views.size(); ++i) {
            this.views.remove(i);
        }
        this.textViews.clear();
        final int childCount = this.getChildCount();
        this.size = childCount;
        for (int j = 0; j < childCount; ++j) {
            final View child = this.getChildAt(j);
            child.setPivotY(165.0f);
            child.setPivotX(100.0f);
            if (this.texts != null && this.texts.length == childCount) {
                final TextView textView = (TextView)child.findViewById(2131427589);
                textView.setText((CharSequence)String.valueOf(this.texts[j]));
                this.textViews.add(textView);
            }
            this.views.add(child);
            child.setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    if (j != LoopRotarySwitchView.this.calculateItemPosition(LoopRotarySwitchView.this.calculateItem())) {
                        LoopRotarySwitchView.this.setSelectItem(j);
                    }
                    else if (LoopRotarySwitchView.this.isCanClickListener && LoopRotarySwitchView.this.onItemClickListener != null) {
                        LoopRotarySwitchView.this.onItemClickListener.onItemClick(j, LoopRotarySwitchView.this.views.get(j));
                    }
                }
            });
        }
    }
    
    public void createXAnimation(final int n, final int n2, final boolean b) {
        if (this.xAnimation != null && this.xAnimation.isRunning()) {
            this.xAnimation.cancel();
        }
        (this.xAnimation = ValueAnimator.ofInt(new int[] { n, n2 })).addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                LoopRotarySwitchView.access$18(LoopRotarySwitchView.this, (int)valueAnimator.getAnimatedValue());
                LoopRotarySwitchView.this.initView();
            }
        });
        this.xAnimation.setInterpolator((TimeInterpolator)new DecelerateInterpolator());
        this.xAnimation.setDuration(2000L);
        if (b) {
            this.xAnimation.start();
        }
    }
    
    public ValueAnimator createZAnimation(final int n, final int n2, final boolean b) {
        if (this.zAnimation != null && this.zAnimation.isRunning()) {
            this.zAnimation.cancel();
        }
        (this.zAnimation = ValueAnimator.ofInt(new int[] { n, n2 })).addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                LoopRotarySwitchView.access$19(LoopRotarySwitchView.this, (int)valueAnimator.getAnimatedValue());
                LoopRotarySwitchView.this.initView();
            }
        });
        this.zAnimation.setInterpolator((TimeInterpolator)new DecelerateInterpolator());
        this.zAnimation.setDuration(2000L);
        if (b) {
            this.zAnimation.start();
        }
        return this.zAnimation;
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        this.onTouch(motionEvent);
        if (this.onLoopViewTouchListener != null) {
            this.onLoopViewTouchListener.onTouch(motionEvent);
        }
        this.isCanClickListener(motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }
    
    public void doRotain() {
        if (this.clickRotation) {
            while (true) {
                this.clickRotation = false;
                while (true) {
                    Label_0120: {
                        try {
                            if (this.size != 0) {
                                int n = 0;
                                switch ($SWITCH_TABLE$com$touchus$benchilauncher$views$LoopRotarySwitchView$AutoScrollDirection()[this.autoRotatinDirection.ordinal()]) {
                                    case 1: {
                                        n = 360 / this.size;
                                        break;
                                    }
                                    case 2: {
                                        n = -360 / this.size;
                                        break;
                                    }
                                    default: {
                                        break Label_0120;
                                    }
                                }
                                if (this.angle == 360.0f) {
                                    this.angle = 0.0f;
                                }
                                this.AnimRotationTo(this.angle + n, null);
                                return;
                            }
                            break;
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            this.clickRotation = true;
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }
    
    public float getAngle() {
        return this.angle;
    }
    
    public long getAutoRotationTime() {
        return this.loopHandler.loopTime;
    }
    
    public float getDistance() {
        return this.distance;
    }
    
    public GestureDetector$SimpleOnGestureListener getGeomeryController() {
        return new GestureDetector$SimpleOnGestureListener() {
            public boolean onScroll(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                final LoopRotarySwitchView this$0 = LoopRotarySwitchView.this;
                LoopRotarySwitchView.access$3(this$0, (float)(this$0.angle + (Math.cos(Math.toRadians(LoopRotarySwitchView.this.loopRotationZ)) * (n / LoopRotarySwitchView.this.views.size()) + Math.sin(Math.toRadians(LoopRotarySwitchView.this.loopRotationZ)) * (n2 / LoopRotarySwitchView.this.views.size()))));
                LoopRotarySwitchView.this.initView();
                return true;
            }
        };
    }
    
    public int getLoopRotationX() {
        return this.loopRotationX;
    }
    
    public int getLoopRotationZ() {
        return this.loopRotationZ;
    }
    
    public float getR() {
        return this.r;
    }
    
    public ValueAnimator getRestAnimator() {
        return this.restAnimator;
    }
    
    public int getSelectItem() {
        return this.selectItem;
    }
    
    public List<TextView> getTextViews() {
        return this.textViews;
    }
    
    public List<View> getViews() {
        return this.views;
    }
    
    public ValueAnimator getrAnimation() {
        return this.rAnimation;
    }
    
    public ValueAnimator getxAnimation() {
        return this.xAnimation;
    }
    
    public ValueAnimator getzAnimation() {
        return this.zAnimation;
    }
    
    public boolean isAutoRotation() {
        return this.autoRotation;
    }
    
    public void isCanClickListener(final MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0: {
                this.x = motionEvent.getX();
                if (this.autoRotation) {
                    this.loopHandler.removeMessages(1000);
                    return;
                }
                break;
            }
            case 1:
            case 3: {
                if (this.autoRotation) {
                    this.loopHandler.sendEmptyMessageDelayed(1000, this.loopHandler.loopTime);
                }
                if (motionEvent.getX() - this.x > this.limitX || this.x - motionEvent.getX() > this.limitX) {
                    this.isCanClickListener = false;
                    return;
                }
                this.isCanClickListener = true;
            }
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (b && !this.isFirst) {
            this.isFirst = true;
            this.checkChildView();
            if (this.onItemSelectedListener != null) {
                this.isCanClickListener = true;
                this.onItemSelectedListener.selected(this.selectItem, this.views.get(this.selectItem));
            }
            this.RAnimation();
        }
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        this.initView();
        if (this.autoRotation) {
            this.loopHandler.sendEmptyMessageDelayed(1000, this.loopHandler.loopTime);
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.onLoopViewTouchListener != null) {
            this.onLoopViewTouchListener.onTouch(motionEvent);
        }
        this.isCanClickListener(motionEvent);
        return true;
    }
    
    public void removeview(final View view) {
        this.views.remove(view);
    }
    
    public void restPosition() {
        if (this.size == 0) {
            return;
        }
        float n2;
        final float n = n2 = 360 / this.size;
        if (this.angle < 0.0f) {
            n2 = -n;
        }
        final float n3 = (int)(this.angle / n2) * n2;
        float n4 = (int)(this.angle / n2) * n2 + n2;
        if (this.angle >= 0.0f) {
            if (this.angle - this.last_angle <= 0.0f) {
                n4 = n3;
            }
        }
        else if (this.angle - this.last_angle >= 0.0f) {
            n4 = n3;
        }
        this.AnimRotationTo(n4, null);
    }
    
    public void setAngle(final float angle) {
        this.angle = angle;
    }
    
    public LoopRotarySwitchView setAutoRotation(final boolean b) {
        this.autoRotation = b;
        this.loopHandler.setLoop(b);
        return this;
    }
    
    public LoopRotarySwitchView setAutoRotationTime(final long loopTime) {
        this.loopHandler.setLoopTime(loopTime);
        return this;
    }
    
    public LoopRotarySwitchView setAutoScrollDirection(final AutoScrollDirection autoRotatinDirection) {
        this.autoRotatinDirection = autoRotatinDirection;
        return this;
    }
    
    public void setChildViewText(final String[] texts) {
        this.texts = texts;
    }
    
    public void setDistance(final float distance) {
        this.distance = distance;
    }
    
    public LoopRotarySwitchView setHorizontal(final boolean b, final boolean b2) {
        if (!b2) {
            if (b) {
                this.setLoopRotationZ(0);
            }
            else {
                this.setLoopRotationZ(90);
            }
            this.initView();
            return this;
        }
        if (b) {
            this.createZAnimation(this.getLoopRotationZ(), 0, true);
            return this;
        }
        this.createZAnimation(this.getLoopRotationZ(), 90, true);
        return this;
    }
    
    public LoopRotarySwitchView setLoopRotationX(final int loopRotationX) {
        this.loopRotationX = loopRotationX;
        return this;
    }
    
    public LoopRotarySwitchView setLoopRotationZ(final int loopRotationZ) {
        this.loopRotationZ = loopRotationZ;
        return this;
    }
    
    public LoopRotarySwitchView setMultiple(final float multiple) {
        this.multiple = multiple;
        return this;
    }
    
    public void setOnItemClickListener(final OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    
    public void setOnItemSelectedListener(final OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }
    
    public void setOnLoopViewTouchListener(final OnLoopViewTouchListener onLoopViewTouchListener) {
        this.onLoopViewTouchListener = onLoopViewTouchListener;
    }
    
    public LoopRotarySwitchView setOrientation(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        this.setHorizontal(b, false);
        return this;
    }
    
    public LoopRotarySwitchView setR(final float r) {
        this.r = r;
        this.distance = this.multiple * r;
        return this;
    }
    
    public void setSelectItem(final int n) {
        if (n != this.getSelectItem() && n >= 0) {
            float n2;
            if (this.getSelectItem() == 0) {
                if (n == this.views.size() - 1) {
                    n2 = this.angle - 360 / this.size;
                    Log.d("iii", new StringBuilder(String.valueOf(this.size)).toString());
                }
                else {
                    Log.d("iii", new StringBuilder(String.valueOf(this.size)).toString());
                    n2 = this.angle + 360 / this.size;
                }
            }
            else if (this.getSelectItem() == this.views.size() - 1) {
                if (n == 0) {
                    n2 = this.angle + 360 / this.size;
                }
                else {
                    n2 = this.angle - 360 / this.size;
                }
            }
            else if (n > this.getSelectItem()) {
                n2 = this.angle + 360 / this.size;
            }
            else {
                n2 = this.angle - 360 / this.size;
            }
            float n4;
            final float n3 = n4 = 360 / this.size;
            if (n2 < 0.0f) {
                n4 = -n3;
            }
            final float n5 = (int)(n2 / n4) * n4;
            final float n6 = (int)(n2 / n4) * n4;
            float n7;
            if (n2 >= 0.0f) {
                if (n2 - this.last_angle > 0.0f) {
                    n7 = n6;
                }
                else {
                    n7 = n5;
                }
            }
            else if (n2 - this.last_angle < 0.0f) {
                n7 = n6;
            }
            else {
                n7 = n5;
            }
            if (this.size > 0) {
                this.AnimRotationTo(n7, null);
            }
        }
    }
    
    public void setSelectItemm(final int n) {
        if (n >= 0) {
            final float n2 = this.angle + 360 / this.size * n;
            float n4;
            final float n3 = n4 = 360 / this.size;
            if (n2 < 0.0f) {
                n4 = -n3;
            }
            final float n5 = (int)(n2 / n4) * n4;
            float n6 = (int)(n2 / n4) * n4;
            if (n2 >= 0.0f) {
                if (n2 - this.last_angle <= 0.0f) {
                    n6 = n5;
                }
            }
            else if (n2 - this.last_angle >= 0.0f) {
                n6 = n5;
            }
            if (this.size > 0) {
                this.AnimRotationTo(n6, null);
            }
        }
    }
    
    public void setSize(final int size) {
        this.size = size;
    }
    
    public void setTextViewText() {
        final int childCount = this.getChildCount();
        if (this.texts != null && this.texts.length != 0 && this.textViews.size() != 0) {
            for (int i = 0; i < childCount; ++i) {
                if (this.texts != null && this.texts.length == childCount) {
                    if (this.texts.length == 0) {
                        break;
                    }
                    switch (i) {
                        case 0: {
                            this.textViews.get(i).setText((CharSequence)String.valueOf(this.texts[i]));
                            break;
                        }
                        case 1: {
                            this.textViews.get(i).setText((CharSequence)String.valueOf(this.texts[i]));
                            break;
                        }
                        case 2: {
                            this.textViews.get(i).setText((CharSequence)String.valueOf(this.texts[i]));
                            break;
                        }
                        case 3: {
                            this.textViews.get(i).setText((CharSequence)String.valueOf(this.texts[i]));
                            break;
                        }
                        case 4: {
                            this.textViews.get(i).setText((CharSequence)String.valueOf(this.texts[i]));
                            break;
                        }
                    }
                }
            }
        }
    }
    
    public void setxAnimation(final ValueAnimator xAnimation) {
        this.xAnimation = xAnimation;
    }
    
    public void setzAnimation(final ValueAnimator zAnimation) {
        this.zAnimation = zAnimation;
    }
    
    public enum AutoScrollDirection
    {
        left("left", 0), 
        right("right", 1);
        
        private AutoScrollDirection(final String s, final int n) {
        }
    }
    
    private class SortComparator implements Comparator<View>
    {
        @Override
        public int compare(final View view, final View view2) {
            try {
                return (int)(view.getScaleX() * 1000.0f - view2.getScaleX() * 1000.0f);
            }
            catch (Exception ex) {
                return 0;
            }
        }
    }
}
