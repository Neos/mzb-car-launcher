package com.touchus.benchilauncher.views;

import android.app.*;
import android.content.*;
import com.touchus.benchilauncher.*;
import com.touchus.benchilauncher.utils.*;
import android.widget.*;
import com.backaudio.android.driver.*;
import android.os.*;
import java.lang.ref.*;

public class ReverseSetDialog extends Dialog
{
    private Context context;
    private RadioButton english;
    private RadioGroup languageType;
    private LauncherApplication mApp;
    public ReverseialogHandler mHandler;
    private byte mIDRIVERENUM;
    private Launcher mMMainActivity;
    private SpUtilK mSpUtilK;
    private int selectPos;
    private RadioButton simplified;
    private RadioButton traditional;
    
    public ReverseSetDialog(final Context context) {
        super(context);
        this.selectPos = 0;
        this.mHandler = new ReverseialogHandler(this);
    }
    
    public ReverseSetDialog(final Context context, final int n) {
        super(context, n);
        this.selectPos = 0;
        this.mHandler = new ReverseialogHandler(this);
        this.context = context;
    }
    
    static /* synthetic */ void access$0(final ReverseSetDialog reverseSetDialog, final int selectPos) {
        reverseSetDialog.selectPos = selectPos;
    }
    
    private void initData() {
        this.mMMainActivity = (Launcher)this.context;
        this.mSpUtilK = new SpUtilK((Context)this.mMMainActivity);
        this.selectPos = this.mSpUtilK.getInt("reversingType", 0);
        this.simplified.setText((CharSequence)this.context.getString(2131165259));
        this.traditional.setText((CharSequence)this.context.getString(2131165260));
        this.english.setText((CharSequence)this.context.getString(2131165261));
        ((RadioButton)this.languageType.getChildAt(this.selectPos)).setChecked(true);
    }
    
    private void initSetup() {
        this.languageType.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener() {
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                switch (n) {
                    case 2131427407: {
                        ReverseSetDialog.access$0(ReverseSetDialog.this, 0);
                        break;
                    }
                    case 2131427408: {
                        ReverseSetDialog.access$0(ReverseSetDialog.this, 1);
                        break;
                    }
                    case 2131427409: {
                        ReverseSetDialog.access$0(ReverseSetDialog.this, 2);
                        break;
                    }
                }
                ReverseSetDialog.this.seclectTrue(ReverseSetDialog.this.selectPos);
                ReverseSetDialog.this.press();
            }
        });
        this.seclectTrue(this.selectPos);
    }
    
    private void initView() {
        this.languageType = (RadioGroup)this.findViewById(2131427406);
        this.simplified = (RadioButton)this.findViewById(2131427407);
        this.traditional = (RadioButton)this.findViewById(2131427408);
        this.english = (RadioButton)this.findViewById(2131427409);
    }
    
    private void press() {
        this.mApp.iLanguageType = this.selectPos;
        this.mSpUtilK.putInt("reversingType", this.selectPos);
        Mainboard.getInstance().sendReverseSetToMcu(this.selectPos);
        this.dismiss();
    }
    
    private void seclectTrue(final int n) {
        for (int i = 0; i < this.languageType.getChildCount(); ++i) {
            if (n == i) {
                this.languageType.getChildAt(i).setSelected(true);
            }
            else {
                this.languageType.getChildAt(i).setSelected(false);
            }
        }
    }
    
    public void handlerMsgUSB(final Message message) {
        if (message.what == 6001) {
            this.mIDRIVERENUM = message.getData().getByte("idriver_enum");
            if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.DOWN.getCode()) {
                if (this.selectPos < this.languageType.getChildCount() - 1) {
                    ++this.selectPos;
                }
                this.seclectTrue(this.selectPos);
            }
            else {
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.UP.getCode()) {
                    if (this.selectPos > 0) {
                        --this.selectPos;
                    }
                    this.seclectTrue(this.selectPos);
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    this.press();
                    return;
                }
                if (this.mIDRIVERENUM == Mainboard.EIdriverEnum.BACK.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.HOME.getCode() || this.mIDRIVERENUM == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903055);
        this.mApp = (LauncherApplication)this.context.getApplicationContext();
        this.initView();
        this.initSetup();
        this.initData();
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void unregisterHandlerr() {
        this.mApp.unregisterHandler(this.mHandler);
    }
    
    static class ReverseialogHandler extends Handler
    {
        private WeakReference<ReverseSetDialog> target;
        
        public ReverseialogHandler(final ReverseSetDialog reverseSetDialog) {
            this.target = new WeakReference<ReverseSetDialog>(reverseSetDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsgUSB(message);
        }
    }
}
