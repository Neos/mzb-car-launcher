package com.touchus.benchilauncher.views;

import android.app.*;
import com.touchus.benchilauncher.*;
import android.widget.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.content.*;
import android.os.*;
import java.lang.ref.*;

public class SystemOperateDialog extends Dialog implements View$OnClickListener
{
    private Button canceBtn;
    public int height;
    private String itemString;
    private LauncherApplication mApp;
    private Context mContext;
    public MHandler mHandler;
    public String showMessage;
    private Button submitBtn;
    public String title;
    private TextView titleName;
    public int width;
    
    public SystemOperateDialog(final Context mContext) {
        super(mContext);
        this.width = 415;
        this.height = 150;
        this.showMessage = "";
        this.title = "";
        this.mHandler = new MHandler(this);
        this.mContext = mContext;
    }
    
    public SystemOperateDialog(final Context mContext, final int n) {
        super(mContext, n);
        this.width = 415;
        this.height = 150;
        this.showMessage = "";
        this.title = "";
        this.mHandler = new MHandler(this);
        this.mContext = mContext;
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.TURN_RIGHT.getCode() || byte1 == Mainboard.EIdriverEnum.RIGHT.getCode()) {
                this.canceBtn.setSelected(true);
                this.submitBtn.setSelected(false);
            }
            else {
                if (byte1 == Mainboard.EIdriverEnum.TURN_LEFT.getCode() || byte1 == Mainboard.EIdriverEnum.LEFT.getCode()) {
                    this.canceBtn.setSelected(false);
                    this.submitBtn.setSelected(true);
                    return;
                }
                if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                    if (this.submitBtn.isSelected()) {
                        this.submitBtn.performClick();
                        return;
                    }
                    this.canceBtn.performClick();
                }
                else if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                    this.dismiss();
                }
            }
        }
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427427) {
            if (this.itemString.equals(this.mContext.getString(2131165348))) {
                ((PowerManager)this.mContext.getSystemService("power")).reboot("reboot");
            }
            else if (this.itemString.equals(this.mContext.getString(2131165349))) {
                this.mContext.sendBroadcast(new Intent("com.touchus.factorytest.recovery"));
            }
        }
        else {
            view.getId();
        }
        this.dismiss();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903065);
        this.mApp = (LauncherApplication)this.mContext.getApplicationContext();
        this.canceBtn = (Button)this.findViewById(2131427459);
        (this.submitBtn = (Button)this.findViewById(2131427427)).setOnClickListener((View$OnClickListener)this);
        this.canceBtn.setOnClickListener((View$OnClickListener)this);
        this.submitBtn.setText((CharSequence)this.mContext.getString(2131165272));
        this.canceBtn.setText((CharSequence)this.mContext.getString(2131165274));
        this.height *= (int)0.9;
        this.titleName = (TextView)this.findViewById(2131427454);
        if (this.itemString.equals(this.mContext.getString(2131165348))) {
            this.titleName.setText(2131165350);
        }
        else if (this.itemString.equals(this.mContext.getString(2131165349))) {
            this.titleName.setText(2131165351);
        }
        this.canceBtn.setOnClickListener((View$OnClickListener)this);
        this.submitBtn.setOnClickListener((View$OnClickListener)this);
        this.getWindow().setLayout(this.width, this.height);
        this.canceBtn.setSelected(true);
        this.submitBtn.setSelected(false);
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    public void setTitle(final String itemString) {
        this.itemString = itemString;
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<SystemOperateDialog> target;
        
        public MHandler(final SystemOperateDialog systemOperateDialog) {
            this.target = new WeakReference<SystemOperateDialog>(systemOperateDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
