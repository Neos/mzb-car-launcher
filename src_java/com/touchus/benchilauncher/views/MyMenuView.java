package com.touchus.benchilauncher.views;

import android.graphics.drawable.*;
import android.widget.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class MyMenuView extends RelativeLayout
{
    private Drawable iconDrawable;
    private boolean isBig;
    LinearLayout$LayoutParams layoutParams;
    private ImageView menuIcon;
    private String menuName;
    private TextView menuTv;
    
    public MyMenuView(final Context context) {
        this(context, null);
    }
    
    public MyMenuView(final Context context, final AttributeSet set) {
        super(context, set);
        this.layoutParams = new LinearLayout$LayoutParams(-2, -2);
        ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(2130903099, (ViewGroup)this);
        this.menuIcon = (ImageView)this.findViewById(2131427673);
        this.menuTv = (TextView)this.findViewById(2131427674);
    }
    
    public void setImageBitmap(final int text, final int imageResource, final boolean b) {
        this.menuIcon.setImageResource(imageResource);
        this.menuTv.setText(text);
        if (b) {
            this.menuTv.setTextSize(30.0f);
            this.layoutParams.setMargins(0, 160, 0, 0);
            this.menuTv.setLayoutParams((ViewGroup$LayoutParams)this.layoutParams);
            return;
        }
        this.menuTv.setTextSize(18.0f);
        this.layoutParams.setMargins(0, 97, 0, 0);
        this.menuTv.setLayoutParams((ViewGroup$LayoutParams)this.layoutParams);
    }
}
