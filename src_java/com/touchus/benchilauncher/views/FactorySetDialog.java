package com.touchus.benchilauncher.views;

import android.app.*;
import com.backaudio.android.driver.*;
import android.view.*;
import android.text.*;
import com.touchus.benchilauncher.utils.*;
import com.touchus.benchilauncher.*;
import com.touchus.publicutils.sysconst.*;
import android.content.*;
import android.widget.*;
import android.os.*;
import java.lang.ref.*;

public class FactorySetDialog extends Dialog implements View$OnClickListener
{
    public int height;
    private Button joinBtn;
    private LauncherApplication mApp;
    private Context mContext;
    public MHandler mHandler;
    private EditText pwdEdit;
    public String showMessage;
    public String title;
    public int width;
    
    public FactorySetDialog(final Context mContext) {
        super(mContext);
        this.width = 415;
        this.height = 150;
        this.showMessage = "";
        this.title = "";
        this.mHandler = new MHandler(this);
        this.mContext = mContext;
    }
    
    public FactorySetDialog(final Context mContext, final int n) {
        super(mContext, n);
        this.width = 415;
        this.height = 150;
        this.showMessage = "";
        this.title = "";
        this.mHandler = new MHandler(this);
        this.mContext = mContext;
    }
    
    public void handlerMsg(final Message message) {
        if (message.what == 6001) {
            final byte byte1 = message.getData().getByte("idriver_enum");
            if (byte1 == Mainboard.EIdriverEnum.PRESS.getCode()) {
                this.joinBtn.performClick();
            }
            else if (byte1 == Mainboard.EIdriverEnum.BACK.getCode() || byte1 == Mainboard.EIdriverEnum.HOME.getCode() || byte1 == Mainboard.EIdriverEnum.STAR_BTN.getCode()) {
                this.dismiss();
            }
        }
    }
    
    public void onClick(final View view) {
        if (view.getId() == 2131427457) {
            final String string = this.pwdEdit.getText().toString();
            if (TextUtils.isEmpty((CharSequence)string)) {
                ToastTool.showLongToast(this.mContext, this.mContext.getString(2131165319));
                return;
            }
            if (!ProjectConfig.FactoryPwd.contains(string)) {
                ToastTool.showLongToast(this.mContext, this.mContext.getString(2131165328));
                return;
            }
            final Intent launchIntentForPackage = this.mContext.getPackageManager().getLaunchIntentForPackage("com.touchus.factorytest");
            if (launchIntentForPackage != null) {
                launchIntentForPackage.addFlags(272629760);
                launchIntentForPackage.putExtra(BenzModel.KEY, (int)BenzModel.benzTpye.getCode());
                launchIntentForPackage.putExtra(BenzModel.SIZE_KEY, (int)BenzModel.benzSize.getCode());
                launchIntentForPackage.putExtra(PubSysConst.KEY_BREAKPOS, this.mApp.breakpos);
                this.mContext.startActivity(launchIntentForPackage);
            }
        }
        this.dismiss();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903064);
        this.mApp = (LauncherApplication)this.mContext.getApplicationContext();
        final TextView textView = (TextView)this.findViewById(2131427455);
        this.joinBtn = (Button)this.findViewById(2131427457);
        this.pwdEdit = (EditText)this.findViewById(2131427456);
        this.joinBtn.setOnClickListener((View$OnClickListener)this);
        this.joinBtn.setEnabled(true);
        this.joinBtn.setText((CharSequence)this.mContext.getString(2131165229));
        if (TextUtils.isEmpty((CharSequence)this.showMessage)) {
            this.showMessage = this.mContext.getString(2131165327);
        }
        textView.setText((CharSequence)this.showMessage);
        ((TextView)this.findViewById(2131427454)).setText((CharSequence)this.mContext.getString(2131165326));
        this.getWindow().setLayout(this.width, this.height);
    }
    
    protected void onStart() {
        this.mApp.registerHandler(this.mHandler);
        super.onStart();
    }
    
    protected void onStop() {
        this.mApp.unregisterHandler(this.mHandler);
        super.onStop();
    }
    
    static class MHandler extends Handler
    {
        private WeakReference<FactorySetDialog> target;
        
        public MHandler(final FactorySetDialog factorySetDialog) {
            this.target = new WeakReference<FactorySetDialog>(factorySetDialog);
        }
        
        public void handleMessage(final Message message) {
            if (this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
