package com.touchus.benchilauncher.base;

import android.widget.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class AbstraSlide extends ViewGroup
{
    public View mChild;
    public OnSlipeListener mOnSlipeListener;
    public Scroller mScroller;
    
    public AbstraSlide(final Context context, final AttributeSet set) {
        super(context, set);
        this.mScroller = new Scroller(context);
    }
    
    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            this.scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            this.invalidate();
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        this.mChild.layout(0, 0, this.mChild.getMeasuredWidth(), this.mChild.getMeasuredHeight());
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.mChild = this.getChildAt(0);
        final ViewGroup$LayoutParams layoutParams = this.mChild.getLayoutParams();
        this.mChild.measure(View$MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View$MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
        this.setMeasuredDimension(View$MeasureSpec.getSize(n), View$MeasureSpec.getSize(n2));
    }
    
    public void setOnSlideListener(final OnSlipeListener mOnSlipeListener) {
        this.mOnSlipeListener = mOnSlipeListener;
    }
    
    public void smoothScrollTo(final int n, final int n2, final int n3) {
        final int scrollX = this.getScrollX();
        final int scrollY = this.getScrollY();
        this.mScroller.startScroll(scrollX, scrollY, n - scrollX, n2 - scrollY, n3);
        this.invalidate();
    }
    
    public void smoothScrollXTo(final int n, final int n2) {
        this.smoothScrollTo(n, 0, n2);
    }
    
    public void smoothScrollYTo(final int n, final int n2) {
        this.smoothScrollTo(0, n, n2);
    }
    
    public interface OnSlipeListener
    {
        void lesten(final int p0);
    }
}
