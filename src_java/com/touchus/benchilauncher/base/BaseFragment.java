package com.touchus.benchilauncher.base;

import android.support.v4.app.*;
import android.view.*;
import android.os.*;

public abstract class BaseFragment extends Fragment
{
    public abstract boolean onBack();
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
    }
}
