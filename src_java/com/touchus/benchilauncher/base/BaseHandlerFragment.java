package com.touchus.benchilauncher.base;

import android.support.v4.app.*;
import com.touchus.benchilauncher.*;
import android.view.*;
import android.os.*;
import java.lang.ref.*;

public abstract class BaseHandlerFragment extends Fragment
{
    public MyHandler mMyHandler;
    
    public BaseHandlerFragment() {
        this.mMyHandler = new MyHandler(this);
    }
    
    public abstract void handlerMsg(final Message p0);
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        LauncherApplication.getInstance().registerHandler(this.mMyHandler);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        LauncherApplication.getInstance().unregisterHandler(this.mMyHandler);
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        super.onViewCreated(view, bundle);
    }
    
    public static class MyHandler extends Handler
    {
        private WeakReference<BaseHandlerFragment> target;
        
        public MyHandler(final BaseHandlerFragment baseHandlerFragment) {
            this.target = new WeakReference<BaseHandlerFragment>(baseHandlerFragment);
        }
        
        public void handleMessage(final Message message) {
            if (this.target == null || this.target.get() == null) {
                return;
            }
            this.target.get().handlerMsg(message);
        }
    }
}
