package com.sun.mail.util;

import java.io.*;

public class CRLFOutputStream extends FilterOutputStream
{
    private static final byte[] newline;
    protected boolean atBOL;
    protected int lastb;
    
    static {
        newline = new byte[] { 13, 10 };
    }
    
    public CRLFOutputStream(final OutputStream outputStream) {
        super(outputStream);
        this.lastb = -1;
        this.atBOL = true;
    }
    
    @Override
    public void write(final int lastb) throws IOException {
        if (lastb == 13) {
            this.writeln();
        }
        else if (lastb == 10) {
            if (this.lastb != 13) {
                this.writeln();
            }
        }
        else {
            this.out.write(lastb);
            this.atBOL = false;
        }
        this.lastb = lastb;
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, int n, int i) throws IOException {
        int n2 = n;
        int n3;
        for (n3 = i + n, i = n2; i < n3; ++i, n2 = n) {
            if (array[i] == 13) {
                this.out.write(array, n2, i - n2);
                this.writeln();
                n = i + 1;
            }
            else {
                n = n2;
                if (array[i] == 10) {
                    if (this.lastb != 13) {
                        this.out.write(array, n2, i - n2);
                        this.writeln();
                    }
                    n = i + 1;
                }
            }
            this.lastb = array[i];
        }
        if (n3 - n2 > 0) {
            this.out.write(array, n2, n3 - n2);
            this.atBOL = false;
        }
    }
    
    public void writeln() throws IOException {
        this.out.write(CRLFOutputStream.newline);
        this.atBOL = true;
    }
}
