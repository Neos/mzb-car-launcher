package com.sun.mail.util;

import java.io.*;

public class ASCIIUtility
{
    public static byte[] getBytes(final InputStream inputStream) throws IOException {
        if (inputStream instanceof ByteArrayInputStream) {
            final int available = inputStream.available();
            final byte[] array = new byte[available];
            inputStream.read(array, 0, available);
            return array;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array2 = new byte[1024];
        while (true) {
            final int read = inputStream.read(array2, 0, 1024);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(array2, 0, read);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    public static byte[] getBytes(final String s) {
        final char[] charArray = s.toCharArray();
        final int length = charArray.length;
        final byte[] array = new byte[length];
        for (int i = 0; i < length; ++i) {
            array[i] = (byte)charArray[i];
        }
        return array;
    }
    
    public static int parseInt(final byte[] array, final int n, final int n2) throws NumberFormatException {
        return parseInt(array, n, n2, 10);
    }
    
    public static int parseInt(final byte[] array, final int n, final int n2, final int n3) throws NumberFormatException {
        if (array == null) {
            throw new NumberFormatException("null");
        }
        int n4 = 0;
        boolean b = false;
        int i;
        if (n2 <= (i = n)) {
            throw new NumberFormatException("illegal number");
        }
        int n5;
        if (array[i] == 45) {
            b = true;
            n5 = Integer.MIN_VALUE;
            ++i;
        }
        else {
            n5 = -2147483647;
        }
        final int n6 = n5 / n3;
        if (i < n2) {
            final int n7 = i + 1;
            final int digit = Character.digit((char)array[i], n3);
            if (digit < 0) {
                throw new NumberFormatException("illegal number: " + toString(array, n, n2));
            }
            n4 = -digit;
            i = n7;
        }
        while (i < n2) {
            final int digit2 = Character.digit((char)array[i], n3);
            if (digit2 < 0) {
                throw new NumberFormatException("illegal number");
            }
            if (n4 < n6) {
                throw new NumberFormatException("illegal number");
            }
            final int n8 = n4 * n3;
            if (n8 < n5 + digit2) {
                throw new NumberFormatException("illegal number");
            }
            n4 = n8 - digit2;
            ++i;
        }
        if (!b) {
            return -n4;
        }
        if (i > n + 1) {
            return n4;
        }
        throw new NumberFormatException("illegal number");
    }
    
    public static long parseLong(final byte[] array, final int n, final int n2) throws NumberFormatException {
        return parseLong(array, n, n2, 10);
    }
    
    public static long parseLong(final byte[] array, final int n, final int n2, final int n3) throws NumberFormatException {
        if (array == null) {
            throw new NumberFormatException("null");
        }
        long n4 = 0L;
        boolean b = false;
        int i;
        if (n2 <= (i = n)) {
            throw new NumberFormatException("illegal number");
        }
        long n5;
        if (array[i] == 45) {
            b = true;
            n5 = Long.MIN_VALUE;
            ++i;
        }
        else {
            n5 = -9223372036854775807L;
        }
        final long n6 = n5 / n3;
        if (i < n2) {
            final int n7 = i + 1;
            final int digit = Character.digit((char)array[i], n3);
            if (digit < 0) {
                throw new NumberFormatException("illegal number: " + toString(array, n, n2));
            }
            n4 = -digit;
            i = n7;
        }
        while (i < n2) {
            final int digit2 = Character.digit((char)array[i], n3);
            if (digit2 < 0) {
                throw new NumberFormatException("illegal number");
            }
            if (n4 < n6) {
                throw new NumberFormatException("illegal number");
            }
            final long n8 = n4 * n3;
            if (n8 < digit2 + n5) {
                throw new NumberFormatException("illegal number");
            }
            n4 = n8 - digit2;
            ++i;
        }
        if (!b) {
            return -n4;
        }
        if (i > n + 1) {
            return n4;
        }
        throw new NumberFormatException("illegal number");
    }
    
    public static String toString(final ByteArrayInputStream byteArrayInputStream) {
        final int available = byteArrayInputStream.available();
        final char[] array = new char[available];
        final byte[] array2 = new byte[available];
        byteArrayInputStream.read(array2, 0, available);
        for (int i = 0; i < available; ++i) {
            array[i] = (char)(array2[i] & 0xFF);
        }
        return new String(array);
    }
    
    public static String toString(final byte[] array, int i, int n) {
        final int n2 = n - i;
        final char[] array2 = new char[n2];
        final int n3 = 0;
        n = i;
        for (i = n3; i < n2; ++i) {
            array2[i] = (char)(array[n] & 0xFF);
            ++n;
        }
        return new String(array2);
    }
}
