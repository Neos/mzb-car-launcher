package com.sun.mail.util;

import java.io.*;

public class UUDecoderStream extends FilterInputStream
{
    private byte[] buffer;
    private int bufsize;
    private boolean gotEnd;
    private boolean gotPrefix;
    private int index;
    private LineInputStream lin;
    private int mode;
    private String name;
    
    public UUDecoderStream(final InputStream inputStream) {
        super(inputStream);
        this.bufsize = 0;
        this.index = 0;
        this.gotPrefix = false;
        this.gotEnd = false;
        this.lin = new LineInputStream(inputStream);
        this.buffer = new byte[45];
    }
    
    private boolean decode() throws IOException {
        if (this.gotEnd) {
            return false;
        }
        this.bufsize = 0;
        String line;
        do {
            line = this.lin.readLine();
            if (line == null) {
                throw new IOException("Missing End");
            }
            if (line.regionMatches(true, 0, "end", 0, 3)) {
                this.gotEnd = true;
                return false;
            }
        } while (line.length() == 0);
        final char char1 = line.charAt(0);
        if (char1 < ' ') {
            throw new IOException("Buffer format error");
        }
        final char c = (char)(char1 - ' ' & '?');
        if (c == '\0') {
            final String line2 = this.lin.readLine();
            if (line2 == null || !line2.regionMatches(true, 0, "end", 0, 3)) {
                throw new IOException("Missing End");
            }
            this.gotEnd = true;
            return false;
        }
        else {
            if (line.length() < (c * '\b' + '\u0005') / '\u0006' + '\u0001') {
                throw new IOException("Short buffer error");
            }
            int n = 1;
            while (this.bufsize < c) {
                final int n2 = n + 1;
                final byte b = (byte)(line.charAt(n) - ' ' & '?');
                final int n3 = n2 + 1;
                final byte b2 = (byte)(line.charAt(n2) - ' ' & '?');
                this.buffer[this.bufsize++] = (byte)((b << 2 & 0xFC) | (b2 >>> 4 & 0x3));
                byte b3 = b2;
                int n4 = n3;
                if (this.bufsize < c) {
                    b3 = (byte)(line.charAt(n3) - ' ' & '?');
                    this.buffer[this.bufsize++] = (byte)((b2 << 4 & 0xF0) | (b3 >>> 2 & 0xF));
                    n4 = n3 + 1;
                }
                n = n4;
                if (this.bufsize < c) {
                    this.buffer[this.bufsize++] = (byte)((b3 << 6 & 0xC0) | ((byte)(line.charAt(n4) - ' ' & '?') & 0x3F));
                    n = n4 + 1;
                }
            }
            return true;
        }
    }
    
    private void readPrefix() throws IOException {
        if (this.gotPrefix) {
            return;
        }
        String line;
        do {
            line = this.lin.readLine();
            if (line == null) {
                throw new IOException("UUDecoder error: No Begin");
            }
        } while (!line.regionMatches(true, 0, "begin", 0, 5));
        try {
            this.mode = Integer.parseInt(line.substring(6, 9));
            this.name = line.substring(10);
            this.gotPrefix = true;
        }
        catch (NumberFormatException ex) {
            throw new IOException("UUDecoder error: " + ex.toString());
        }
    }
    
    @Override
    public int available() throws IOException {
        return this.in.available() * 3 / 4 + (this.bufsize - this.index);
    }
    
    public int getMode() throws IOException {
        this.readPrefix();
        return this.mode;
    }
    
    public String getName() throws IOException {
        this.readPrefix();
        return this.name;
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int read() throws IOException {
        if (this.index >= this.bufsize) {
            this.readPrefix();
            if (!this.decode()) {
                return -1;
            }
            this.index = 0;
        }
        return this.buffer[this.index++] & 0xFF;
    }
    
    @Override
    public int read(final byte[] array, final int n, final int n2) throws IOException {
        int i = 0;
        while (i < n2) {
            final int read = this.read();
            if (read == -1) {
                if (i == 0) {
                    return -1;
                }
                break;
            }
            else {
                array[n + i] = (byte)read;
                ++i;
            }
        }
        return i;
    }
}
