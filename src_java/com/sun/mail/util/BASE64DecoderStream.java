package com.sun.mail.util;

import java.io.*;

public class BASE64DecoderStream extends FilterInputStream
{
    private static final char[] pem_array;
    private static final byte[] pem_convert_array;
    private byte[] buffer;
    private int bufsize;
    private boolean ignoreErrors;
    private int index;
    private byte[] input_buffer;
    private int input_len;
    private int input_pos;
    
    static {
        pem_array = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };
        pem_convert_array = new byte[256];
        for (int i = 0; i < 255; ++i) {
            BASE64DecoderStream.pem_convert_array[i] = -1;
        }
        for (int j = 0; j < BASE64DecoderStream.pem_array.length; ++j) {
            BASE64DecoderStream.pem_convert_array[BASE64DecoderStream.pem_array[j]] = (byte)j;
        }
    }
    
    public BASE64DecoderStream(final InputStream inputStream) {
        final boolean b = false;
        super(inputStream);
        this.buffer = new byte[3];
        this.bufsize = 0;
        this.index = 0;
        this.input_buffer = new byte[8190];
        this.input_pos = 0;
        this.input_len = 0;
        this.ignoreErrors = false;
        try {
            final String property = System.getProperty("mail.mime.base64.ignoreerrors");
            boolean ignoreErrors = b;
            if (property != null) {
                ignoreErrors = b;
                if (!property.equalsIgnoreCase("false")) {
                    ignoreErrors = true;
                }
            }
            this.ignoreErrors = ignoreErrors;
        }
        catch (SecurityException ex) {}
    }
    
    public BASE64DecoderStream(final InputStream inputStream, final boolean ignoreErrors) {
        super(inputStream);
        this.buffer = new byte[3];
        this.bufsize = 0;
        this.index = 0;
        this.input_buffer = new byte[8190];
        this.input_pos = 0;
        this.input_len = 0;
        this.ignoreErrors = false;
        this.ignoreErrors = ignoreErrors;
    }
    
    private int decode(final byte[] array, final int n, int i) throws IOException {
        int n2 = i;
        i = n;
        while (true) {
            final int n3 = i;
            if (n2 < 3) {
                return n3 - n;
            }
            int j;
            int byte1;
            boolean b;
            int n4;
            int n5;
            int n6;
            int byte2;
            for (j = 0, i = 0; j < 4; ++j, i = (i << 6 | byte1)) {
                byte1 = this.getByte();
                if (byte1 == -1 || byte1 == -2) {
                    if (byte1 == -1) {
                        if (j == 0) {
                            return n3 - n;
                        }
                        if (!this.ignoreErrors) {
                            throw new IOException("Error in encoded stream: needed 4 valid base64 characters but only got " + j + " before EOF" + this.recentChars());
                        }
                        b = true;
                    }
                    else {
                        if (j < 2 && !this.ignoreErrors) {
                            throw new IOException("Error in encoded stream: needed at least 2 valid base64 characters, but only got " + j + " before padding character (=)" + this.recentChars());
                        }
                        if (j == 0) {
                            return n3 - n;
                        }
                        b = false;
                    }
                    if ((n4 = j - 1) == 0) {
                        n4 = 1;
                    }
                    n5 = j + 1;
                    n6 = i << 6;
                    for (i = n5; i < 4; ++i) {
                        if (!b) {
                            byte2 = this.getByte();
                            if (byte2 == -1) {
                                if (!this.ignoreErrors) {
                                    throw new IOException("Error in encoded stream: hit EOF while looking for padding characters (=)" + this.recentChars());
                                }
                            }
                            else if (byte2 != -2 && !this.ignoreErrors) {
                                throw new IOException("Error in encoded stream: found valid base64 character after a padding character (=)" + this.recentChars());
                            }
                        }
                        n6 <<= 6;
                    }
                    i = n6 >> 8;
                    if (n4 == 2) {
                        array[n3 + 1] = (byte)(i & 0xFF);
                    }
                    array[n3] = (byte)(i >> 8 & 0xFF);
                    return n3 + n4 - n;
                }
            }
            array[n3 + 2] = (byte)(i & 0xFF);
            i >>= 8;
            array[n3 + 1] = (byte)(i & 0xFF);
            array[n3] = (byte)(i >> 8 & 0xFF);
            n2 -= 3;
            i = n3 + 3;
        }
    }
    
    public static byte[] decode(final byte[] array) {
        final int n = array.length / 4 * 3;
        if (n == 0) {
            return array;
        }
        int n2 = n;
        if (array[array.length - 1] == 61) {
            n2 = n - 1;
            if (array[array.length - 2] == 61) {
                --n2;
            }
        }
        final byte[] array2 = new byte[n2];
        int n3 = 0;
        int i = array.length;
        int n4 = 0;
        while (i > 0) {
            final int n5 = 3;
            final byte[] pem_convert_array = BASE64DecoderStream.pem_convert_array;
            final int n6 = n4 + 1;
            final byte b = pem_convert_array[array[n4] & 0xFF];
            final byte[] pem_convert_array2 = BASE64DecoderStream.pem_convert_array;
            final int n7 = n6 + 1;
            int n8 = (b << 6 | pem_convert_array2[array[n6] & 0xFF]) << 6;
            int n10;
            int n11;
            if (array[n7] != 61) {
                final byte[] pem_convert_array3 = BASE64DecoderStream.pem_convert_array;
                final int n9 = n7 + 1;
                n8 |= pem_convert_array3[array[n7] & 0xFF];
                n10 = n5;
                n11 = n9;
            }
            else {
                final int n12 = 3 - 1;
                n11 = n7;
                n10 = n12;
            }
            int n13 = n8 << 6;
            if (array[n11] != 61) {
                n13 |= BASE64DecoderStream.pem_convert_array[array[n11] & 0xFF];
                ++n11;
            }
            else {
                --n10;
            }
            if (n10 > 2) {
                array2[n3 + 2] = (byte)(n13 & 0xFF);
            }
            final int n14 = n13 >> 8;
            if (n10 > 1) {
                array2[n3 + 1] = (byte)(n14 & 0xFF);
            }
            array2[n3] = (byte)(n14 >> 8 & 0xFF);
            n3 += n10;
            i -= 4;
            n4 = n11;
        }
        return array2;
    }
    
    private int getByte() throws IOException {
        byte b;
        do {
            if (this.input_pos >= this.input_len) {
                try {
                    this.input_len = this.in.read(this.input_buffer);
                    if (this.input_len <= 0) {
                        return -1;
                    }
                }
                catch (EOFException ex) {
                    return -1;
                }
                this.input_pos = 0;
            }
            final int n = this.input_buffer[this.input_pos++] & 0xFF;
            if (n == 61) {
                return -2;
            }
            b = BASE64DecoderStream.pem_convert_array[n];
        } while (b == -1);
        return b;
    }
    
    private String recentChars() {
        int input_pos = 10;
        String string = "";
        if (this.input_pos <= 10) {
            input_pos = this.input_pos;
        }
        if (input_pos > 0) {
            String s = String.valueOf("") + ", the " + input_pos + " most recent characters were: \"";
            for (int i = this.input_pos - input_pos; i < this.input_pos; ++i) {
                final char c = (char)(this.input_buffer[i] & 0xFF);
                switch (c) {
                    default: {
                        if (c >= ' ' && c < '\u007f') {
                            s = String.valueOf(s) + c;
                            break;
                        }
                        s = String.valueOf(s) + "\\" + (int)c;
                        break;
                    }
                    case 13: {
                        s = String.valueOf(s) + "\\r";
                        break;
                    }
                    case 10: {
                        s = String.valueOf(s) + "\\n";
                        break;
                    }
                    case 9: {
                        s = String.valueOf(s) + "\\t";
                        break;
                    }
                }
            }
            string = String.valueOf(s) + "\"";
        }
        return string;
    }
    
    @Override
    public int available() throws IOException {
        return this.in.available() * 3 / 4 + (this.bufsize - this.index);
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int read() throws IOException {
        if (this.index >= this.bufsize) {
            this.bufsize = this.decode(this.buffer, 0, this.buffer.length);
            if (this.bufsize <= 0) {
                return -1;
            }
            this.index = 0;
        }
        return this.buffer[this.index++] & 0xFF;
    }
    
    @Override
    public int read(final byte[] array, final int n, int n2) throws IOException {
        int n3 = n;
        int n4;
        while (true) {
            n4 = n3;
            if (this.index >= this.bufsize || n2 <= 0) {
                break;
            }
            array[n4] = this.buffer[this.index++];
            --n2;
            n3 = n4 + 1;
        }
        if (this.index >= this.bufsize) {
            this.index = 0;
            this.bufsize = 0;
        }
        final int n5 = n2 / 3 * 3;
        int n6 = n4;
        int i = n2;
        if (n5 > 0) {
            final int decode = this.decode(array, n4, n5);
            final int n7 = n4 + decode;
            i = n2 - decode;
            n6 = n7;
            if (decode != n5) {
                if (n7 == n) {
                    return -1;
                }
                return n7 - n;
            }
        }
        n2 = n6;
        while (true) {
            while (i > 0) {
                final int read = this.read();
                if (read == -1) {
                    if (n2 == n) {
                        return -1;
                    }
                    return n2 - n;
                }
                else {
                    array[n2] = (byte)read;
                    --i;
                    ++n2;
                }
            }
            continue;
        }
    }
}
