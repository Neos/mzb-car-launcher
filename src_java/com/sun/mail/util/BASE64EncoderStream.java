package com.sun.mail.util;

import java.io.*;

public class BASE64EncoderStream extends FilterOutputStream
{
    private static byte[] newline;
    private static final char[] pem_array;
    private byte[] buffer;
    private int bufsize;
    private int bytesPerLine;
    private int count;
    private int lineLimit;
    private boolean noCRLF;
    private byte[] outbuf;
    
    static {
        BASE64EncoderStream.newline = new byte[] { 13, 10 };
        pem_array = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };
    }
    
    public BASE64EncoderStream(final OutputStream outputStream) {
        this(outputStream, 76);
    }
    
    public BASE64EncoderStream(final OutputStream outputStream, int bytesPerLine) {
        super(outputStream);
        this.bufsize = 0;
        this.count = 0;
        this.noCRLF = false;
        this.buffer = new byte[3];
        int n;
        if (bytesPerLine == Integer.MAX_VALUE || (n = bytesPerLine) < 4) {
            this.noCRLF = true;
            n = 76;
        }
        bytesPerLine = n / 4 * 4;
        this.bytesPerLine = bytesPerLine;
        this.lineLimit = bytesPerLine / 4 * 3;
        if (this.noCRLF) {
            this.outbuf = new byte[bytesPerLine];
            return;
        }
        (this.outbuf = new byte[bytesPerLine + 2])[bytesPerLine] = 13;
        this.outbuf[bytesPerLine + 1] = 10;
    }
    
    private void encode() throws IOException {
        final int encodedSize = encodedSize(this.bufsize);
        this.out.write(encode(this.buffer, 0, this.bufsize, this.outbuf), 0, encodedSize);
        this.count += encodedSize;
        if (this.count >= this.bytesPerLine) {
            if (!this.noCRLF) {
                this.out.write(BASE64EncoderStream.newline);
            }
            this.count = 0;
        }
    }
    
    public static byte[] encode(final byte[] array) {
        if (array.length == 0) {
            return array;
        }
        return encode(array, 0, array.length, null);
    }
    
    private static byte[] encode(final byte[] array, int n, int n2, final byte[] array2) {
        byte[] array3 = array2;
        if (array2 == null) {
            array3 = new byte[encodedSize(n2)];
        }
        final int n3 = 0;
        int i;
        int n4;
        byte b;
        int n5;
        int n6;
        int n7;
        for (i = n2, n2 = n3; i >= 3; i -= 3, n2 += 4, ++n) {
            n4 = n + 1;
            b = array[n];
            n = n4 + 1;
            n5 = (((b & 0xFF) << 8 | (array[n4] & 0xFF)) << 8 | (array[n] & 0xFF));
            array3[n2 + 3] = (byte)BASE64EncoderStream.pem_array[n5 & 0x3F];
            n6 = n5 >> 6;
            array3[n2 + 2] = (byte)BASE64EncoderStream.pem_array[n6 & 0x3F];
            n7 = n6 >> 6;
            array3[n2 + 1] = (byte)BASE64EncoderStream.pem_array[n7 & 0x3F];
            array3[n2 + 0] = (byte)BASE64EncoderStream.pem_array[n7 >> 6 & 0x3F];
        }
        if (i == 1) {
            n = (array[n] & 0xFF) << 4;
            array3[n2 + 2] = (array3[n2 + 3] = 61);
            array3[n2 + 1] = (byte)BASE64EncoderStream.pem_array[n & 0x3F];
            array3[n2 + 0] = (byte)BASE64EncoderStream.pem_array[n >> 6 & 0x3F];
            return array3;
        }
        if (i == 2) {
            final int n8 = n + 1;
            n = array[n];
            n = ((n & 0xFF) << 8 | (array[n8] & 0xFF)) << 2;
            array3[n2 + 3] = 61;
            array3[n2 + 2] = (byte)BASE64EncoderStream.pem_array[n & 0x3F];
            n >>= 6;
            array3[n2 + 1] = (byte)BASE64EncoderStream.pem_array[n & 0x3F];
            array3[n2 + 0] = (byte)BASE64EncoderStream.pem_array[n >> 6 & 0x3F];
        }
        return array3;
    }
    
    private static int encodedSize(final int n) {
        return (n + 2) / 3 * 4;
    }
    
    @Override
    public void close() throws IOException {
        synchronized (this) {
            this.flush();
            if (this.count > 0 && !this.noCRLF) {
                this.out.write(BASE64EncoderStream.newline);
                this.out.flush();
            }
            this.out.close();
        }
    }
    
    @Override
    public void flush() throws IOException {
        synchronized (this) {
            if (this.bufsize > 0) {
                this.encode();
                this.bufsize = 0;
            }
            this.out.flush();
        }
    }
    
    @Override
    public void write(final int n) throws IOException {
        synchronized (this) {
            this.buffer[this.bufsize++] = (byte)n;
            if (this.bufsize == 3) {
                this.encode();
                this.bufsize = 0;
            }
        }
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] p0, final int p1, final int p2) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: iload_2        
        //     3: iload_3        
        //     4: iadd           
        //     5: istore          5
        //     7: aload_0        
        //     8: getfield        com/sun/mail/util/BASE64EncoderStream.bufsize:I
        //    11: ifeq            20
        //    14: iload_2        
        //    15: iload           5
        //    17: if_icmplt       208
        //    20: aload_0        
        //    21: getfield        com/sun/mail/util/BASE64EncoderStream.bytesPerLine:I
        //    24: aload_0        
        //    25: getfield        com/sun/mail/util/BASE64EncoderStream.count:I
        //    28: isub           
        //    29: iconst_4       
        //    30: idiv           
        //    31: iconst_3       
        //    32: imul           
        //    33: istore          6
        //    35: iload_2        
        //    36: iload           6
        //    38: iadd           
        //    39: iload           5
        //    41: if_icmpge       275
        //    44: iload           6
        //    46: invokestatic    com/sun/mail/util/BASE64EncoderStream.encodedSize:(I)I
        //    49: istore          4
        //    51: iload           4
        //    53: istore_3       
        //    54: aload_0        
        //    55: getfield        com/sun/mail/util/BASE64EncoderStream.noCRLF:Z
        //    58: ifne            98
        //    61: aload_0        
        //    62: getfield        com/sun/mail/util/BASE64EncoderStream.outbuf:[B
        //    65: astore          8
        //    67: iload           4
        //    69: iconst_1       
        //    70: iadd           
        //    71: istore          7
        //    73: aload           8
        //    75: iload           4
        //    77: bipush          13
        //    79: bastore        
        //    80: aload_0        
        //    81: getfield        com/sun/mail/util/BASE64EncoderStream.outbuf:[B
        //    84: astore          8
        //    86: iload           7
        //    88: iconst_1       
        //    89: iadd           
        //    90: istore_3       
        //    91: aload           8
        //    93: iload           7
        //    95: bipush          10
        //    97: bastore        
        //    98: aload_0        
        //    99: getfield        com/sun/mail/util/BASE64EncoderStream.out:Ljava/io/OutputStream;
        //   102: aload_1        
        //   103: iload_2        
        //   104: iload           6
        //   106: aload_0        
        //   107: getfield        com/sun/mail/util/BASE64EncoderStream.outbuf:[B
        //   110: invokestatic    com/sun/mail/util/BASE64EncoderStream.encode:([BII[B)[B
        //   113: iconst_0       
        //   114: iload_3        
        //   115: invokevirtual   java/io/OutputStream.write:([BII)V
        //   118: iload_2        
        //   119: iload           6
        //   121: iadd           
        //   122: istore_2       
        //   123: aload_0        
        //   124: iconst_0       
        //   125: putfield        com/sun/mail/util/BASE64EncoderStream.count:I
        //   128: aload_0        
        //   129: getfield        com/sun/mail/util/BASE64EncoderStream.lineLimit:I
        //   132: iload_2        
        //   133: iadd           
        //   134: iload           5
        //   136: if_icmplt       222
        //   139: iload_2        
        //   140: istore_3       
        //   141: iload_2        
        //   142: iconst_3       
        //   143: iadd           
        //   144: iload           5
        //   146: if_icmpge       199
        //   149: iload           5
        //   151: iload_2        
        //   152: isub           
        //   153: iconst_3       
        //   154: idiv           
        //   155: iconst_3       
        //   156: imul           
        //   157: istore_3       
        //   158: iload_3        
        //   159: invokestatic    com/sun/mail/util/BASE64EncoderStream.encodedSize:(I)I
        //   162: istore          4
        //   164: aload_0        
        //   165: getfield        com/sun/mail/util/BASE64EncoderStream.out:Ljava/io/OutputStream;
        //   168: aload_1        
        //   169: iload_2        
        //   170: iload_3        
        //   171: aload_0        
        //   172: getfield        com/sun/mail/util/BASE64EncoderStream.outbuf:[B
        //   175: invokestatic    com/sun/mail/util/BASE64EncoderStream.encode:([BII[B)[B
        //   178: iconst_0       
        //   179: iload           4
        //   181: invokevirtual   java/io/OutputStream.write:([BII)V
        //   184: iload_2        
        //   185: iload_3        
        //   186: iadd           
        //   187: istore_3       
        //   188: aload_0        
        //   189: aload_0        
        //   190: getfield        com/sun/mail/util/BASE64EncoderStream.count:I
        //   193: iload           4
        //   195: iadd           
        //   196: putfield        com/sun/mail/util/BASE64EncoderStream.count:I
        //   199: iload_3        
        //   200: iload           5
        //   202: if_icmplt       252
        //   205: aload_0        
        //   206: monitorexit    
        //   207: return         
        //   208: aload_0        
        //   209: aload_1        
        //   210: iload_2        
        //   211: baload         
        //   212: invokevirtual   com/sun/mail/util/BASE64EncoderStream.write:(I)V
        //   215: iload_2        
        //   216: iconst_1       
        //   217: iadd           
        //   218: istore_2       
        //   219: goto            7
        //   222: aload_0        
        //   223: getfield        com/sun/mail/util/BASE64EncoderStream.out:Ljava/io/OutputStream;
        //   226: aload_1        
        //   227: iload_2        
        //   228: aload_0        
        //   229: getfield        com/sun/mail/util/BASE64EncoderStream.lineLimit:I
        //   232: aload_0        
        //   233: getfield        com/sun/mail/util/BASE64EncoderStream.outbuf:[B
        //   236: invokestatic    com/sun/mail/util/BASE64EncoderStream.encode:([BII[B)[B
        //   239: invokevirtual   java/io/OutputStream.write:([B)V
        //   242: iload_2        
        //   243: aload_0        
        //   244: getfield        com/sun/mail/util/BASE64EncoderStream.lineLimit:I
        //   247: iadd           
        //   248: istore_2       
        //   249: goto            128
        //   252: aload_0        
        //   253: aload_1        
        //   254: iload_3        
        //   255: baload         
        //   256: invokevirtual   com/sun/mail/util/BASE64EncoderStream.write:(I)V
        //   259: iload_3        
        //   260: iconst_1       
        //   261: iadd           
        //   262: istore_3       
        //   263: goto            199
        //   266: astore_1       
        //   267: aload_0        
        //   268: monitorexit    
        //   269: aload_1        
        //   270: athrow         
        //   271: astore_1       
        //   272: goto            267
        //   275: goto            128
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  7      14     266    267    Any
        //  20     35     266    267    Any
        //  44     51     266    267    Any
        //  54     67     266    267    Any
        //  80     86     266    267    Any
        //  98     118    266    267    Any
        //  123    128    271    275    Any
        //  128    139    271    275    Any
        //  149    184    271    275    Any
        //  188    199    271    275    Any
        //  208    215    271    275    Any
        //  222    249    271    275    Any
        //  252    259    271    275    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0128:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
