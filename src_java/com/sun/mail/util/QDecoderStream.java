package com.sun.mail.util;

import java.io.*;

public class QDecoderStream extends QPDecoderStream
{
    public QDecoderStream(final InputStream inputStream) {
        super(inputStream);
    }
    
    @Override
    public int read() throws IOException {
        final int read = this.in.read();
        int n;
        if (read == 95) {
            n = 32;
        }
        else if ((n = read) == 61) {
            this.ba[0] = (byte)this.in.read();
            this.ba[1] = (byte)this.in.read();
            try {
                return ASCIIUtility.parseInt(this.ba, 0, 2, 16);
            }
            catch (NumberFormatException ex) {
                throw new IOException("Error in QP stream " + ex.getMessage());
            }
        }
        return n;
    }
}
