package com.sun.mail.util;

import java.io.*;

public class TraceInputStream extends FilterInputStream
{
    private boolean quote;
    private boolean trace;
    private OutputStream traceOut;
    
    public TraceInputStream(final InputStream inputStream, final OutputStream traceOut) {
        super(inputStream);
        this.trace = false;
        this.quote = false;
        this.traceOut = traceOut;
    }
    
    private final void writeByte(int n) throws IOException {
        final int n2 = n &= 0xFF;
        if (n2 > 127) {
            this.traceOut.write(77);
            this.traceOut.write(45);
            n = (n2 & 0x7F);
        }
        if (n == 13) {
            this.traceOut.write(92);
            this.traceOut.write(114);
            return;
        }
        if (n == 10) {
            this.traceOut.write(92);
            this.traceOut.write(110);
            this.traceOut.write(10);
            return;
        }
        if (n == 9) {
            this.traceOut.write(92);
            this.traceOut.write(116);
            return;
        }
        if (n < 32) {
            this.traceOut.write(94);
            this.traceOut.write(n + 64);
            return;
        }
        this.traceOut.write(n);
    }
    
    @Override
    public int read() throws IOException {
        final int read = this.in.read();
        if (this.trace && read != -1) {
            if (!this.quote) {
                this.traceOut.write(read);
                return read;
            }
            this.writeByte(read);
        }
        return read;
    }
    
    @Override
    public int read(final byte[] array, final int n, int i) throws IOException {
        final int read = this.in.read(array, n, i);
        if (this.trace && read != -1) {
            if (!this.quote) {
                this.traceOut.write(array, n, read);
                return read;
            }
            for (i = 0; i < read; ++i) {
                this.writeByte(array[n + i]);
            }
        }
        return read;
    }
    
    public void setQuote(final boolean quote) {
        this.quote = quote;
    }
    
    public void setTrace(final boolean trace) {
        this.trace = trace;
    }
}
