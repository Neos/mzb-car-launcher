package com.sun.mail.util;

import java.io.*;

public class TraceOutputStream extends FilterOutputStream
{
    private boolean quote;
    private boolean trace;
    private OutputStream traceOut;
    
    public TraceOutputStream(final OutputStream outputStream, final OutputStream traceOut) {
        super(outputStream);
        this.trace = false;
        this.quote = false;
        this.traceOut = traceOut;
    }
    
    private final void writeByte(int n) throws IOException {
        final int n2 = n &= 0xFF;
        if (n2 > 127) {
            this.traceOut.write(77);
            this.traceOut.write(45);
            n = (n2 & 0x7F);
        }
        if (n == 13) {
            this.traceOut.write(92);
            this.traceOut.write(114);
            return;
        }
        if (n == 10) {
            this.traceOut.write(92);
            this.traceOut.write(110);
            this.traceOut.write(10);
            return;
        }
        if (n == 9) {
            this.traceOut.write(92);
            this.traceOut.write(116);
            return;
        }
        if (n < 32) {
            this.traceOut.write(94);
            this.traceOut.write(n + 64);
            return;
        }
        this.traceOut.write(n);
    }
    
    public void setQuote(final boolean quote) {
        this.quote = quote;
    }
    
    public void setTrace(final boolean trace) {
        this.trace = trace;
    }
    
    @Override
    public void write(final int n) throws IOException {
        if (this.trace) {
            if (this.quote) {
                this.writeByte(n);
            }
            else {
                this.traceOut.write(n);
            }
        }
        this.out.write(n);
    }
    
    @Override
    public void write(final byte[] array, final int n, final int n2) throws IOException {
        if (this.trace) {
            if (this.quote) {
                for (int i = 0; i < n2; ++i) {
                    this.writeByte(array[n + i]);
                }
            }
            else {
                this.traceOut.write(array, n, n2);
            }
        }
        this.out.write(array, n, n2);
    }
}
