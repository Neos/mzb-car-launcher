package com.sun.mail.util;

import java.io.*;

public class BEncoderStream extends BASE64EncoderStream
{
    public BEncoderStream(final OutputStream outputStream) {
        super(outputStream, Integer.MAX_VALUE);
    }
    
    public static int encodedLength(final byte[] array) {
        return (array.length + 2) / 3 * 4;
    }
}
