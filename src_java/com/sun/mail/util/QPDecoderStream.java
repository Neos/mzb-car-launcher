package com.sun.mail.util;

import java.io.*;

public class QPDecoderStream extends FilterInputStream
{
    protected byte[] ba;
    protected int spaces;
    
    public QPDecoderStream(final InputStream inputStream) {
        super(new PushbackInputStream(inputStream, 2));
        this.ba = new byte[2];
        this.spaces = 0;
    }
    
    @Override
    public int available() throws IOException {
        return this.in.available();
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int read() throws IOException {
        int n;
        if (this.spaces > 0) {
            --this.spaces;
            n = 32;
        }
        else {
            final int read = this.in.read();
            if (read == 32) {
                int read2;
                while (true) {
                    read2 = this.in.read();
                    if (read2 != 32) {
                        break;
                    }
                    ++this.spaces;
                }
                if (read2 == 13 || read2 == 10 || read2 == -1) {
                    this.spaces = 0;
                    return read2;
                }
                ((PushbackInputStream)this.in).unread(read2);
                return 32;
            }
            else if ((n = read) == 61) {
                final int read3 = this.in.read();
                if (read3 == 10) {
                    return this.read();
                }
                if (read3 == 13) {
                    final int read4 = this.in.read();
                    if (read4 != 10) {
                        ((PushbackInputStream)this.in).unread(read4);
                    }
                    return this.read();
                }
                if (read3 == -1) {
                    return -1;
                }
                this.ba[0] = (byte)read3;
                this.ba[1] = (byte)this.in.read();
                try {
                    return ASCIIUtility.parseInt(this.ba, 0, 2, 16);
                }
                catch (NumberFormatException ex) {
                    ((PushbackInputStream)this.in).unread(this.ba);
                    return read;
                }
            }
        }
        return n;
    }
    
    @Override
    public int read(final byte[] array, final int n, final int n2) throws IOException {
        int i = 0;
        while (i < n2) {
            final int read = this.read();
            if (read == -1) {
                if (i == 0) {
                    return -1;
                }
                break;
            }
            else {
                array[n + i] = (byte)read;
                ++i;
            }
        }
        return i;
    }
}
