package com.sun.mail.util;

import java.io.*;

public class LineInputStream extends FilterInputStream
{
    private char[] lineBuffer;
    
    public LineInputStream(final InputStream inputStream) {
        super(inputStream);
        this.lineBuffer = null;
    }
    
    public String readLine() throws IOException {
        final InputStream in = this.in;
        char[] lineBuffer;
        if ((lineBuffer = this.lineBuffer) == null) {
            lineBuffer = new char[128];
            this.lineBuffer = lineBuffer;
        }
        int length = lineBuffer.length;
        int n = 0;
        while (true) {
            Block_5: {
                int read;
                while (true) {
                    read = in.read();
                    if (read == -1 || read == 10) {
                        break;
                    }
                    if (read == 13) {
                        break Block_5;
                    }
                    if (--length < 0) {
                        lineBuffer = new char[n + 128];
                        length = lineBuffer.length - n - 1;
                        System.arraycopy(this.lineBuffer, 0, lineBuffer, 0, n);
                        this.lineBuffer = lineBuffer;
                    }
                    lineBuffer[n] = (char)read;
                    ++n;
                }
                if (read == -1 && n == 0) {
                    return null;
                }
                return String.copyValueOf(lineBuffer, 0, n);
            }
            int n2;
            if ((n2 = in.read()) == 13) {
                n2 = in.read();
            }
            if (n2 != 10) {
                InputStream in2 = in;
                if (!(in instanceof PushbackInputStream)) {
                    in2 = new PushbackInputStream(in);
                    this.in = in2;
                }
                ((PushbackInputStream)in2).unread(n2);
            }
            continue;
        }
    }
}
