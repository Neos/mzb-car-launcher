package com.sun.mail.util;

import java.io.*;

public class UUEncoderStream extends FilterOutputStream
{
    private byte[] buffer;
    private int bufsize;
    protected int mode;
    protected String name;
    private boolean wrotePrefix;
    
    public UUEncoderStream(final OutputStream outputStream) {
        this(outputStream, "encoder.buf", 644);
    }
    
    public UUEncoderStream(final OutputStream outputStream, final String s) {
        this(outputStream, s, 644);
    }
    
    public UUEncoderStream(final OutputStream outputStream, final String name, final int mode) {
        super(outputStream);
        this.bufsize = 0;
        this.wrotePrefix = false;
        this.name = name;
        this.mode = mode;
        this.buffer = new byte[45];
    }
    
    private void encode() throws IOException {
        int i = 0;
        this.out.write((this.bufsize & 0x3F) + 32);
        while (i < this.bufsize) {
            final byte[] buffer = this.buffer;
            final int n = i + 1;
            final byte b = buffer[i];
            byte b2;
            byte b3;
            if (n < this.bufsize) {
                final byte[] buffer2 = this.buffer;
                i = n + 1;
                b2 = buffer2[n];
                if (i < this.bufsize) {
                    b3 = this.buffer[i];
                    ++i;
                }
                else {
                    b3 = 1;
                }
            }
            else {
                b2 = 1;
                final byte b4 = 1;
                i = n;
                b3 = b4;
            }
            this.out.write((b >>> 2 & 0x3F) + 32);
            this.out.write(((b << 4 & 0x30) | (b2 >>> 4 & 0xF)) + 32);
            this.out.write(((b2 << 2 & 0x3C) | (b3 >>> 6 & 0x3)) + 32);
            this.out.write((b3 & 0x3F) + 32);
        }
        this.out.write(10);
    }
    
    private void writePrefix() throws IOException {
        if (!this.wrotePrefix) {
            final PrintStream printStream = new PrintStream(this.out);
            printStream.println("begin " + this.mode + " " + this.name);
            printStream.flush();
            this.wrotePrefix = true;
        }
    }
    
    private void writeSuffix() throws IOException {
        final PrintStream printStream = new PrintStream(this.out);
        printStream.println(" \nend");
        printStream.flush();
    }
    
    @Override
    public void close() throws IOException {
        this.flush();
        this.out.close();
    }
    
    @Override
    public void flush() throws IOException {
        if (this.bufsize > 0) {
            this.writePrefix();
            this.encode();
        }
        this.writeSuffix();
        this.out.flush();
    }
    
    public void setNameMode(final String name, final int mode) {
        this.name = name;
        this.mode = mode;
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.buffer[this.bufsize++] = (byte)n;
        if (this.bufsize == 45) {
            this.writePrefix();
            this.encode();
            this.bufsize = 0;
        }
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, final int n, final int n2) throws IOException {
        for (int i = 0; i < n2; ++i) {
            this.write(array[n + i]);
        }
    }
}
