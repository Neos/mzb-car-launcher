package com.sun.mail.util;

import java.io.*;

public class QEncoderStream extends QPEncoderStream
{
    private static String TEXT_SPECIALS;
    private static String WORD_SPECIALS;
    private String specials;
    
    static {
        QEncoderStream.WORD_SPECIALS = "=_?\"#$%&'(),.:;<>@[\\]^`{|}~";
        QEncoderStream.TEXT_SPECIALS = "=_?";
    }
    
    public QEncoderStream(final OutputStream outputStream, final boolean b) {
        super(outputStream, Integer.MAX_VALUE);
        String specials;
        if (b) {
            specials = QEncoderStream.WORD_SPECIALS;
        }
        else {
            specials = QEncoderStream.TEXT_SPECIALS;
        }
        this.specials = specials;
    }
    
    public static int encodedLength(final byte[] array, final boolean b) {
        int n = 0;
        String s;
        if (b) {
            s = QEncoderStream.WORD_SPECIALS;
        }
        else {
            s = QEncoderStream.TEXT_SPECIALS;
        }
        for (int i = 0; i < array.length; ++i) {
            final int n2 = array[i] & 0xFF;
            if (n2 < 32 || n2 >= 127 || s.indexOf(n2) >= 0) {
                n += 3;
            }
            else {
                ++n;
            }
        }
        return n;
    }
    
    @Override
    public void write(int n) throws IOException {
        n &= 0xFF;
        if (n == 32) {
            this.output(95, false);
            return;
        }
        if (n < 32 || n >= 127 || this.specials.indexOf(n) >= 0) {
            this.output(n, true);
            return;
        }
        this.output(n, false);
    }
}
