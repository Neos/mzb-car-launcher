package com.sun.mail.util;

import javax.net.*;
import javax.net.ssl.*;
import java.net.*;
import java.io.*;
import java.security.*;
import java.lang.reflect.*;
import java.util.*;

public class SocketFetcher
{
    private static void configureSSLSocket(final Socket socket, final Properties properties, final String s) {
        if (socket instanceof SSLSocket) {
            final SSLSocket sslSocket = (SSLSocket)socket;
            final String property = properties.getProperty(String.valueOf(s) + ".ssl.protocols", null);
            if (property != null) {
                sslSocket.setEnabledProtocols(stringArray(property));
            }
            else {
                sslSocket.setEnabledProtocols(new String[] { "TLSv1" });
            }
            final String property2 = properties.getProperty(String.valueOf(s) + ".ssl.ciphersuites", null);
            if (property2 != null) {
                sslSocket.setEnabledCipherSuites(stringArray(property2));
            }
        }
    }
    
    private static Socket createSocket(final InetAddress inetAddress, final int n, final String s, final int n2, final int n3, final SocketFactory socketFactory, final boolean b) throws IOException {
        Socket socket;
        if (socketFactory != null) {
            socket = socketFactory.createSocket();
        }
        else if (b) {
            socket = SSLSocketFactory.getDefault().createSocket();
        }
        else {
            socket = new Socket();
        }
        if (inetAddress != null) {
            socket.bind(new InetSocketAddress(inetAddress, n));
        }
        if (n3 >= 0) {
            socket.connect(new InetSocketAddress(s, n2), n3);
            return socket;
        }
        socket.connect(new InetSocketAddress(s, n2));
        return socket;
    }
    
    private static ClassLoader getContextClassLoader() {
        return AccessController.doPrivileged((PrivilegedAction<ClassLoader>)new PrivilegedAction() {
            @Override
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                }
                catch (SecurityException ex) {
                    return null;
                }
            }
        });
    }
    
    public static Socket getSocket(final String s, final int n, final Properties properties, final String s2) throws IOException {
        return getSocket(s, n, properties, s2, false);
    }
    
    public static Socket getSocket(final String p0, final int p1, final Properties p2, final String p3, final boolean p4) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          11
        //     3: aload_3        
        //     4: ifnonnull       11
        //     7: ldc             "socket"
        //     9: astore          11
        //    11: aload_2        
        //    12: astore_3       
        //    13: aload_2        
        //    14: ifnonnull       25
        //    17: new             Ljava/util/Properties;
        //    20: dup            
        //    21: invokespecial   java/util/Properties.<init>:()V
        //    24: astore_3       
        //    25: aload_3        
        //    26: new             Ljava/lang/StringBuilder;
        //    29: dup            
        //    30: aload           11
        //    32: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    35: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    38: ldc             ".connectiontimeout"
        //    40: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    43: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    46: aconst_null    
        //    47: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    50: astore_2       
        //    51: iconst_m1      
        //    52: istore          5
        //    54: iload           5
        //    56: istore          7
        //    58: aload_2        
        //    59: ifnull          68
        //    62: aload_2        
        //    63: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    66: istore          7
        //    68: aconst_null    
        //    69: astore          13
        //    71: aload_3        
        //    72: new             Ljava/lang/StringBuilder;
        //    75: dup            
        //    76: aload           11
        //    78: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    81: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    84: ldc             ".timeout"
        //    86: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    89: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    92: aconst_null    
        //    93: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    96: astore          15
        //    98: aload_3        
        //    99: new             Ljava/lang/StringBuilder;
        //   102: dup            
        //   103: aload           11
        //   105: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   108: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   111: ldc             ".localaddress"
        //   113: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   116: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   119: aconst_null    
        //   120: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   123: astore_2       
        //   124: aconst_null    
        //   125: astore          12
        //   127: aload_2        
        //   128: ifnull          137
        //   131: aload_2        
        //   132: invokestatic    java/net/InetAddress.getByName:(Ljava/lang/String;)Ljava/net/InetAddress;
        //   135: astore          12
        //   137: aload_3        
        //   138: new             Ljava/lang/StringBuilder;
        //   141: dup            
        //   142: aload           11
        //   144: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   147: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   150: ldc             ".localport"
        //   152: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   155: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   158: aconst_null    
        //   159: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   162: astore_2       
        //   163: iconst_0       
        //   164: istore          5
        //   166: iload           5
        //   168: istore          8
        //   170: aload_2        
        //   171: ifnull          180
        //   174: aload_2        
        //   175: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   178: istore          8
        //   180: aload_3        
        //   181: new             Ljava/lang/StringBuilder;
        //   184: dup            
        //   185: aload           11
        //   187: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   190: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   193: ldc             ".socketFactory.fallback"
        //   195: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   198: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   201: aconst_null    
        //   202: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   205: astore_2       
        //   206: aload_2        
        //   207: ifnull          418
        //   210: aload_2        
        //   211: ldc             "false"
        //   213: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   216: ifeq            418
        //   219: iconst_0       
        //   220: istore          9
        //   222: aload_3        
        //   223: new             Ljava/lang/StringBuilder;
        //   226: dup            
        //   227: aload           11
        //   229: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   232: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   235: ldc             ".socketFactory.class"
        //   237: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   240: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   243: aconst_null    
        //   244: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   247: astore          16
        //   249: iconst_m1      
        //   250: istore          6
        //   252: iload           6
        //   254: istore          5
        //   256: aload           16
        //   258: invokestatic    com/sun/mail/util/SocketFetcher.getSocketFactory:(Ljava/lang/String;)Ljavax/net/SocketFactory;
        //   261: astore          14
        //   263: aload           13
        //   265: astore_2       
        //   266: aload           14
        //   268: ifnull          357
        //   271: iload           6
        //   273: istore          5
        //   275: aload_3        
        //   276: new             Ljava/lang/StringBuilder;
        //   279: dup            
        //   280: aload           11
        //   282: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   285: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   288: ldc             ".socketFactory.port"
        //   290: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   293: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   296: aconst_null    
        //   297: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   300: astore_2       
        //   301: iload           6
        //   303: istore          5
        //   305: aload_2        
        //   306: ifnull          323
        //   309: iload           6
        //   311: istore          5
        //   313: aload_2        
        //   314: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   317: istore          10
        //   319: iload           10
        //   321: istore          5
        //   323: iload           5
        //   325: istore          6
        //   327: iload           5
        //   329: iconst_m1      
        //   330: if_icmpne       336
        //   333: iload_1        
        //   334: istore          6
        //   336: iload           6
        //   338: istore          5
        //   340: aload           12
        //   342: iload           8
        //   344: aload_0        
        //   345: iload           6
        //   347: iload           7
        //   349: aload           14
        //   351: iload           4
        //   353: invokestatic    com/sun/mail/util/SocketFetcher.createSocket:(Ljava/net/InetAddress;ILjava/lang/String;IILjavax/net/SocketFactory;Z)Ljava/net/Socket;
        //   356: astore_2       
        //   357: aload_2        
        //   358: astore          13
        //   360: aload_2        
        //   361: ifnonnull       380
        //   364: aload           12
        //   366: iload           8
        //   368: aload_0        
        //   369: iload_1        
        //   370: iload           7
        //   372: aconst_null    
        //   373: iload           4
        //   375: invokestatic    com/sun/mail/util/SocketFetcher.createSocket:(Ljava/net/InetAddress;ILjava/lang/String;IILjavax/net/SocketFactory;Z)Ljava/net/Socket;
        //   378: astore          13
        //   380: iconst_m1      
        //   381: istore          5
        //   383: iload           5
        //   385: istore_1       
        //   386: aload           15
        //   388: ifnull          397
        //   391: aload           15
        //   393: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   396: istore_1       
        //   397: iload_1        
        //   398: iflt            407
        //   401: aload           13
        //   403: iload_1        
        //   404: invokevirtual   java/net/Socket.setSoTimeout:(I)V
        //   407: aload           13
        //   409: aload_3        
        //   410: aload           11
        //   412: invokestatic    com/sun/mail/util/SocketFetcher.configureSSLSocket:(Ljava/net/Socket;Ljava/util/Properties;Ljava/lang/String;)V
        //   415: aload           13
        //   417: areturn        
        //   418: iconst_1       
        //   419: istore          9
        //   421: goto            222
        //   424: astore_0       
        //   425: aload_0        
        //   426: athrow         
        //   427: astore          14
        //   429: aload           13
        //   431: astore_2       
        //   432: iload           9
        //   434: ifne            357
        //   437: aload           14
        //   439: astore_2       
        //   440: aload           14
        //   442: instanceof      Ljava/lang/reflect/InvocationTargetException;
        //   445: ifeq            472
        //   448: aload           14
        //   450: checkcast       Ljava/lang/reflect/InvocationTargetException;
        //   453: invokevirtual   java/lang/reflect/InvocationTargetException.getTargetException:()Ljava/lang/Throwable;
        //   456: astore_3       
        //   457: aload           14
        //   459: astore_2       
        //   460: aload_3        
        //   461: instanceof      Ljava/lang/Exception;
        //   464: ifeq            472
        //   467: aload_3        
        //   468: checkcast       Ljava/lang/Exception;
        //   471: astore_2       
        //   472: aload_2        
        //   473: instanceof      Ljava/io/IOException;
        //   476: ifeq            484
        //   479: aload_2        
        //   480: checkcast       Ljava/io/IOException;
        //   483: athrow         
        //   484: new             Ljava/io/IOException;
        //   487: dup            
        //   488: new             Ljava/lang/StringBuilder;
        //   491: dup            
        //   492: ldc             "Couldn't connect using \""
        //   494: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   497: aload           16
        //   499: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   502: ldc             "\" socket factory to host, port: "
        //   504: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   507: aload_0        
        //   508: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   511: ldc             ", "
        //   513: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   516: iload           5
        //   518: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   521: ldc             "; Exception: "
        //   523: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   526: aload_2        
        //   527: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   530: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   533: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   536: astore_0       
        //   537: aload_0        
        //   538: aload_2        
        //   539: invokevirtual   java/io/IOException.initCause:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //   542: pop            
        //   543: aload_0        
        //   544: athrow         
        //   545: astore_2       
        //   546: iload           5
        //   548: istore          7
        //   550: goto            68
        //   553: astore_2       
        //   554: iload           5
        //   556: istore          8
        //   558: goto            180
        //   561: astore_2       
        //   562: iload           6
        //   564: istore          5
        //   566: goto            323
        //   569: astore_0       
        //   570: iload           5
        //   572: istore_1       
        //   573: goto            397
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  62     68     545    553    Ljava/lang/NumberFormatException;
        //  174    180    553    561    Ljava/lang/NumberFormatException;
        //  256    263    424    427    Ljava/net/SocketTimeoutException;
        //  256    263    427    545    Ljava/lang/Exception;
        //  275    301    424    427    Ljava/net/SocketTimeoutException;
        //  275    301    427    545    Ljava/lang/Exception;
        //  313    319    561    569    Ljava/lang/NumberFormatException;
        //  313    319    424    427    Ljava/net/SocketTimeoutException;
        //  313    319    427    545    Ljava/lang/Exception;
        //  340    357    424    427    Ljava/net/SocketTimeoutException;
        //  340    357    427    545    Ljava/lang/Exception;
        //  391    397    569    576    Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 287, Size: 287
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static SocketFactory getSocketFactory(final String s) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (s == null || s.length() == 0) {
            return null;
        }
        final ClassLoader contextClassLoader = getContextClassLoader();
        Class<?> loadClass;
        Class<?> forName = loadClass = null;
        while (true) {
            if (contextClassLoader == null) {
                break Label_0031;
            }
            try {
                loadClass = contextClassLoader.loadClass(s);
                if ((forName = loadClass) == null) {
                    forName = Class.forName(s);
                }
                return (SocketFactory)forName.getMethod("getDefault", (Class<?>[])new Class[0]).invoke(new Object(), new Object[0]);
            }
            catch (ClassNotFoundException ex) {
                loadClass = forName;
                continue;
            }
            break;
        }
    }
    
    public static Socket startTLS(final Socket socket) throws IOException {
        return startTLS(socket, new Properties(), "socket");
    }
    
    public static Socket startTLS(Socket socket, final Properties properties, final String s) throws IOException {
        final String hostName = socket.getInetAddress().getHostName();
        final int port = socket.getPort();
        try {
            final SocketFactory socketFactory = getSocketFactory(properties.getProperty(String.valueOf(s) + ".socketFactory.class", null));
            SSLSocketFactory sslSocketFactory;
            if (socketFactory != null && socketFactory instanceof SSLSocketFactory) {
                sslSocketFactory = (SSLSocketFactory)socketFactory;
            }
            else {
                sslSocketFactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
            }
            socket = sslSocketFactory.createSocket(socket, hostName, port, true);
            configureSSLSocket(socket, properties, s);
            return socket;
        }
        catch (Exception ex2) {
            Exception ex = ex2;
            if (ex2 instanceof InvocationTargetException) {
                final Throwable targetException = ((InvocationTargetException)ex2).getTargetException();
                ex = ex2;
                if (targetException instanceof Exception) {
                    ex = (Exception)targetException;
                }
            }
            if (ex instanceof IOException) {
                throw (IOException)ex;
            }
            final IOException ex3 = new IOException("Exception in startTLS: host " + hostName + ", port " + port + "; Exception: " + ex);
            ex3.initCause(ex);
            throw ex3;
        }
    }
    
    private static String[] stringArray(final String s) {
        final StringTokenizer stringTokenizer = new StringTokenizer(s);
        final ArrayList<String> list = new ArrayList<String>();
        while (stringTokenizer.hasMoreTokens()) {
            list.add(stringTokenizer.nextToken());
        }
        return list.toArray(new String[list.size()]);
    }
}
