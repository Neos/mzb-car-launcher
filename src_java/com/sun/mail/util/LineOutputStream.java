package com.sun.mail.util;

import java.io.*;
import javax.mail.*;

public class LineOutputStream extends FilterOutputStream
{
    private static byte[] newline;
    
    static {
        (LineOutputStream.newline = new byte[2])[0] = 13;
        LineOutputStream.newline[1] = 10;
    }
    
    public LineOutputStream(final OutputStream outputStream) {
        super(outputStream);
    }
    
    public void writeln() throws MessagingException {
        try {
            this.out.write(LineOutputStream.newline);
        }
        catch (Exception ex) {
            throw new MessagingException("IOException", ex);
        }
    }
    
    public void writeln(final String s) throws MessagingException {
        try {
            this.out.write(ASCIIUtility.getBytes(s));
            this.out.write(LineOutputStream.newline);
        }
        catch (Exception ex) {
            throw new MessagingException("IOException", ex);
        }
    }
}
