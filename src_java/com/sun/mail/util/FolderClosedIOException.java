package com.sun.mail.util;

import java.io.*;
import javax.mail.*;

public class FolderClosedIOException extends IOException
{
    private static final long serialVersionUID = 4281122580365555735L;
    private transient Folder folder;
    
    public FolderClosedIOException(final Folder folder) {
        this(folder, null);
    }
    
    public FolderClosedIOException(final Folder folder, final String s) {
        super(s);
        this.folder = folder;
    }
    
    public Folder getFolder() {
        return this.folder;
    }
}
