package com.sun.mail.util;

import java.io.*;

public class QPEncoderStream extends FilterOutputStream
{
    private static final char[] hex;
    private int bytesPerLine;
    private int count;
    private boolean gotCR;
    private boolean gotSpace;
    
    static {
        hex = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    
    public QPEncoderStream(final OutputStream outputStream) {
        this(outputStream, 76);
    }
    
    public QPEncoderStream(final OutputStream outputStream, final int n) {
        super(outputStream);
        this.count = 0;
        this.gotSpace = false;
        this.gotCR = false;
        this.bytesPerLine = n - 1;
    }
    
    private void outputCRLF() throws IOException {
        this.out.write(13);
        this.out.write(10);
        this.count = 0;
    }
    
    @Override
    public void close() throws IOException {
        this.out.close();
    }
    
    @Override
    public void flush() throws IOException {
        this.out.flush();
    }
    
    protected void output(final int n, final boolean b) throws IOException {
        if (b) {
            if ((this.count += 3) > this.bytesPerLine) {
                this.out.write(61);
                this.out.write(13);
                this.out.write(10);
                this.count = 3;
            }
            this.out.write(61);
            this.out.write(QPEncoderStream.hex[n >> 4]);
            this.out.write(QPEncoderStream.hex[n & 0xF]);
            return;
        }
        if (++this.count > this.bytesPerLine) {
            this.out.write(61);
            this.out.write(13);
            this.out.write(10);
            this.count = 1;
        }
        this.out.write(n);
    }
    
    @Override
    public void write(int n) throws IOException {
        n &= 0xFF;
        if (this.gotSpace) {
            if (n == 13 || n == 10) {
                this.output(32, true);
            }
            else {
                this.output(32, false);
            }
            this.gotSpace = false;
        }
        if (n == 13) {
            this.gotCR = true;
            this.outputCRLF();
            return;
        }
        if (n == 10) {
            if (!this.gotCR) {
                this.outputCRLF();
            }
        }
        else if (n == 32) {
            this.gotSpace = true;
        }
        else if (n < 32 || n >= 127 || n == 61) {
            this.output(n, true);
        }
        else {
            this.output(n, false);
        }
        this.gotCR = false;
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, final int n, final int n2) throws IOException {
        for (int i = 0; i < n2; ++i) {
            this.write(array[n + i]);
        }
    }
}
