package com.sun.mail.handlers;

import javax.activation.*;
import java.util.*;
import javax.mail.internet.*;
import javax.mail.*;
import myjava.awt.datatransfer.*;
import java.io.*;

public class message_rfc822 implements DataContentHandler
{
    ActivationDataFlavor ourDataFlavor;
    
    public message_rfc822() {
        this.ourDataFlavor = new ActivationDataFlavor(Message.class, "message/rfc822", "Message");
    }
    
    @Override
    public Object getContent(final DataSource dataSource) throws IOException {
        try {
            Session session;
            if (dataSource instanceof MessageAware) {
                session = ((MessageAware)dataSource).getMessageContext().getSession();
            }
            else {
                session = Session.getDefaultInstance(new Properties(), null);
            }
            return new MimeMessage(session, dataSource.getInputStream());
        }
        catch (MessagingException ex) {
            throw new IOException("Exception creating MimeMessage in message/rfc822 DataContentHandler: " + ex.toString());
        }
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (this.ourDataFlavor.equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { this.ourDataFlavor };
    }
    
    @Override
    public void writeTo(final Object o, final String s, final OutputStream outputStream) throws IOException {
        if (o instanceof Message) {
            final Message message = (Message)o;
            try {
                message.writeTo(outputStream);
                return;
            }
            catch (MessagingException ex) {
                throw new IOException(ex.toString());
            }
        }
        throw new IOException("unsupported object");
    }
}
