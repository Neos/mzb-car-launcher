package com.sun.mail.handlers;

import javax.mail.internet.*;
import javax.activation.*;
import javax.mail.*;
import myjava.awt.datatransfer.*;
import java.io.*;

public class multipart_mixed implements DataContentHandler
{
    private ActivationDataFlavor myDF;
    
    public multipart_mixed() {
        this.myDF = new ActivationDataFlavor(MimeMultipart.class, "multipart/mixed", "Multipart");
    }
    
    @Override
    public Object getContent(final DataSource dataSource) throws IOException {
        try {
            return new MimeMultipart(dataSource);
        }
        catch (MessagingException ex2) {
            final IOException ex = new IOException("Exception while constructing MimeMultipart");
            ex.initCause(ex2);
            throw ex;
        }
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (this.myDF.equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { this.myDF };
    }
    
    @Override
    public void writeTo(final Object o, final String s, final OutputStream outputStream) throws IOException {
        if (!(o instanceof MimeMultipart)) {
            return;
        }
        try {
            ((MimeMultipart)o).writeTo(outputStream);
        }
        catch (MessagingException ex) {
            throw new IOException(ex.toString());
        }
    }
}
