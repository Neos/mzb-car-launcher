package com.sun.mail.handlers;

import javax.mail.internet.*;
import javax.activation.*;
import myjava.awt.datatransfer.*;
import java.io.*;

public class text_plain implements DataContentHandler
{
    private static ActivationDataFlavor myDF;
    
    static {
        text_plain.myDF = new ActivationDataFlavor(String.class, "text/plain", "Text String");
    }
    
    private String getCharset(String s) {
        try {
            if ((s = new ContentType(s).getParameter("charset")) == null) {
                s = "us-ascii";
            }
            s = MimeUtility.javaCharset(s);
            return s;
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    @Override
    public Object getContent(final DataSource p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          4
        //     3: aload_0        
        //     4: aload_1        
        //     5: invokeinterface javax/activation/DataSource.getContentType:()Ljava/lang/String;
        //    10: invokespecial   com/sun/mail/handlers/text_plain.getCharset:(Ljava/lang/String;)Ljava/lang/String;
        //    13: astore          5
        //    15: aload           5
        //    17: astore          4
        //    19: new             Ljava/io/InputStreamReader;
        //    22: dup            
        //    23: aload_1        
        //    24: invokeinterface javax/activation/DataSource.getInputStream:()Ljava/io/InputStream;
        //    29: aload           5
        //    31: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    34: astore          5
        //    36: iconst_0       
        //    37: istore_2       
        //    38: sipush          1024
        //    41: newarray        C
        //    43: astore_1       
        //    44: aload           5
        //    46: aload_1        
        //    47: iload_2        
        //    48: aload_1        
        //    49: arraylength    
        //    50: iload_2        
        //    51: isub           
        //    52: invokevirtual   java/io/InputStreamReader.read:([CII)I
        //    55: istore_3       
        //    56: iload_3        
        //    57: iconst_m1      
        //    58: if_icmpne       90
        //    61: new             Ljava/lang/String;
        //    64: dup            
        //    65: aload_1        
        //    66: iconst_0       
        //    67: iload_2        
        //    68: invokespecial   java/lang/String.<init>:([CII)V
        //    71: astore_1       
        //    72: aload           5
        //    74: invokevirtual   java/io/InputStreamReader.close:()V
        //    77: aload_1        
        //    78: areturn        
        //    79: astore_1       
        //    80: new             Ljava/io/UnsupportedEncodingException;
        //    83: dup            
        //    84: aload           4
        //    86: invokespecial   java/io/UnsupportedEncodingException.<init>:(Ljava/lang/String;)V
        //    89: athrow         
        //    90: iload_2        
        //    91: iload_3        
        //    92: iadd           
        //    93: istore_3       
        //    94: iload_3        
        //    95: istore_2       
        //    96: iload_3        
        //    97: aload_1        
        //    98: arraylength    
        //    99: if_icmplt       44
        //   102: aload_1        
        //   103: arraylength    
        //   104: istore_2       
        //   105: iload_2        
        //   106: ldc             262144
        //   108: if_icmpge       137
        //   111: iload_2        
        //   112: iload_2        
        //   113: iadd           
        //   114: istore_2       
        //   115: iload_2        
        //   116: newarray        C
        //   118: astore          4
        //   120: aload_1        
        //   121: iconst_0       
        //   122: aload           4
        //   124: iconst_0       
        //   125: iload_3        
        //   126: invokestatic    java/lang/System.arraycopy:(Ljava/lang/Object;ILjava/lang/Object;II)V
        //   129: aload           4
        //   131: astore_1       
        //   132: iload_3        
        //   133: istore_2       
        //   134: goto            44
        //   137: iload_2        
        //   138: ldc             262144
        //   140: iadd           
        //   141: istore_2       
        //   142: goto            115
        //   145: astore_1       
        //   146: aload           5
        //   148: invokevirtual   java/io/InputStreamReader.close:()V
        //   151: aload_1        
        //   152: athrow         
        //   153: astore          4
        //   155: aload_1        
        //   156: areturn        
        //   157: astore          4
        //   159: goto            151
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  3      15     79     90     Ljava/lang/IllegalArgumentException;
        //  19     36     79     90     Ljava/lang/IllegalArgumentException;
        //  38     44     145    153    Any
        //  44     56     145    153    Any
        //  61     72     145    153    Any
        //  72     77     153    157    Ljava/io/IOException;
        //  96     105    145    153    Any
        //  115    129    145    153    Any
        //  146    151    157    162    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 99, Size: 99
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected ActivationDataFlavor getDF() {
        return text_plain.myDF;
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (this.getDF().equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { this.getDF() };
    }
    
    @Override
    public void writeTo(final Object o, String s, final OutputStream outputStream) throws IOException {
        if (!(o instanceof String)) {
            throw new IOException("\"" + this.getDF().getMimeType() + "\" DataContentHandler requires String object, " + "was given object of type " + o.getClass().toString());
        }
        String charset = null;
        try {
            s = (charset = this.getCharset(s));
            final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, s);
            final String s2 = (String)o;
            outputStreamWriter.write(s2, 0, s2.length());
            outputStreamWriter.flush();
        }
        catch (IllegalArgumentException ex) {
            throw new UnsupportedEncodingException(charset);
        }
    }
}
