package com.sun.mail.iap;

import java.io.*;

public class ByteArray
{
    private byte[] bytes;
    private int count;
    private int start;
    
    public ByteArray(final int n) {
        this(new byte[n], 0, n);
    }
    
    public ByteArray(final byte[] bytes, final int start, final int count) {
        this.bytes = bytes;
        this.start = start;
        this.count = count;
    }
    
    public byte[] getBytes() {
        return this.bytes;
    }
    
    public int getCount() {
        return this.count;
    }
    
    public byte[] getNewBytes() {
        final byte[] array = new byte[this.count];
        System.arraycopy(this.bytes, this.start, array, 0, this.count);
        return array;
    }
    
    public int getStart() {
        return this.start;
    }
    
    public void grow(final int n) {
        final byte[] bytes = new byte[this.bytes.length + n];
        System.arraycopy(this.bytes, 0, bytes, 0, this.bytes.length);
        this.bytes = bytes;
    }
    
    public void setCount(final int count) {
        this.count = count;
    }
    
    public ByteArrayInputStream toByteArrayInputStream() {
        return new ByteArrayInputStream(this.bytes, this.start, this.count);
    }
}
