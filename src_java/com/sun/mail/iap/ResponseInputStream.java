package com.sun.mail.iap;

import java.io.*;

public class ResponseInputStream
{
    private static final int incrementSlop = 16;
    private static final int maxIncrement = 262144;
    private static final int minIncrement = 256;
    private BufferedInputStream bin;
    
    public ResponseInputStream(final InputStream inputStream) {
        this.bin = new BufferedInputStream(inputStream, 2048);
    }
    
    public ByteArray readResponse() throws IOException {
        return this.readResponse(null);
    }
    
    public ByteArray readResponse(final ByteArray p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          8
        //     3: aload_1        
        //     4: ifnonnull       25
        //     7: new             Lcom/sun/mail/iap/ByteArray;
        //    10: dup            
        //    11: sipush          128
        //    14: newarray        B
        //    16: iconst_0       
        //    17: sipush          128
        //    20: invokespecial   com/sun/mail/iap/ByteArray.<init>:([BII)V
        //    23: astore          8
        //    25: aload           8
        //    27: invokevirtual   com/sun/mail/iap/ByteArray.getBytes:()[B
        //    30: astore_1       
        //    31: iconst_0       
        //    32: istore_3       
        //    33: iconst_0       
        //    34: istore          4
        //    36: iconst_0       
        //    37: istore          5
        //    39: iload_3        
        //    40: istore_2       
        //    41: iload           4
        //    43: istore_3       
        //    44: iload           5
        //    46: ifne            62
        //    49: aload_0        
        //    50: getfield        com/sun/mail/iap/ResponseInputStream.bin:Ljava/io/BufferedInputStream;
        //    53: invokevirtual   java/io/BufferedInputStream.read:()I
        //    56: istore_3       
        //    57: iload_3        
        //    58: iconst_m1      
        //    59: if_icmpne       75
        //    62: iload_3        
        //    63: iconst_m1      
        //    64: if_icmpne       190
        //    67: new             Ljava/io/IOException;
        //    70: dup            
        //    71: invokespecial   java/io/IOException.<init>:()V
        //    74: athrow         
        //    75: iload_3        
        //    76: tableswitch {
        //               20: 162
        //          default: 96
        //        }
        //    96: iload           5
        //    98: istore          4
        //   100: aload_1        
        //   101: astore          7
        //   103: iload_2        
        //   104: aload_1        
        //   105: arraylength    
        //   106: if_icmplt       142
        //   109: aload_1        
        //   110: arraylength    
        //   111: istore          6
        //   113: iload           6
        //   115: istore          5
        //   117: iload           6
        //   119: ldc             262144
        //   121: if_icmple       128
        //   124: ldc             262144
        //   126: istore          5
        //   128: aload           8
        //   130: iload           5
        //   132: invokevirtual   com/sun/mail/iap/ByteArray.grow:(I)V
        //   135: aload           8
        //   137: invokevirtual   com/sun/mail/iap/ByteArray.getBytes:()[B
        //   140: astore          7
        //   142: aload           7
        //   144: iload_2        
        //   145: iload_3        
        //   146: i2b            
        //   147: bastore        
        //   148: iload_2        
        //   149: iconst_1       
        //   150: iadd           
        //   151: istore_2       
        //   152: aload           7
        //   154: astore_1       
        //   155: iload           4
        //   157: istore          5
        //   159: goto            44
        //   162: iload           5
        //   164: istore          4
        //   166: iload_2        
        //   167: ifle            100
        //   170: iload           5
        //   172: istore          4
        //   174: aload_1        
        //   175: iload_2        
        //   176: iconst_1       
        //   177: isub           
        //   178: baload         
        //   179: bipush          13
        //   181: if_icmpne       100
        //   184: iconst_1       
        //   185: istore          4
        //   187: goto            100
        //   190: iload_2        
        //   191: iconst_5       
        //   192: if_icmplt       205
        //   195: aload_1        
        //   196: iload_2        
        //   197: iconst_3       
        //   198: isub           
        //   199: baload         
        //   200: bipush          125
        //   202: if_icmpeq       214
        //   205: aload           8
        //   207: iload_2        
        //   208: invokevirtual   com/sun/mail/iap/ByteArray.setCount:(I)V
        //   211: aload           8
        //   213: areturn        
        //   214: iload_2        
        //   215: iconst_4       
        //   216: isub           
        //   217: istore_3       
        //   218: iload_3        
        //   219: ifge            327
        //   222: iload_3        
        //   223: iflt            205
        //   226: aload_1        
        //   227: iload_3        
        //   228: iconst_1       
        //   229: iadd           
        //   230: iload_2        
        //   231: iconst_3       
        //   232: isub           
        //   233: invokestatic    com/sun/mail/util/ASCIIUtility.parseInt:([BII)I
        //   236: istore_3       
        //   237: iload_3        
        //   238: ifle            367
        //   241: aload_1        
        //   242: arraylength    
        //   243: iload_2        
        //   244: isub           
        //   245: istore          4
        //   247: iload_3        
        //   248: bipush          16
        //   250: iadd           
        //   251: iload           4
        //   253: if_icmple       358
        //   256: sipush          256
        //   259: iload_3        
        //   260: bipush          16
        //   262: iadd           
        //   263: iload           4
        //   265: isub           
        //   266: if_icmple       346
        //   269: sipush          256
        //   272: istore          4
        //   274: aload           8
        //   276: iload           4
        //   278: invokevirtual   com/sun/mail/iap/ByteArray.grow:(I)V
        //   281: aload           8
        //   283: invokevirtual   com/sun/mail/iap/ByteArray.getBytes:()[B
        //   286: astore          7
        //   288: iload_3        
        //   289: istore          4
        //   291: aload           7
        //   293: astore_1       
        //   294: iload_2        
        //   295: istore_3       
        //   296: iload           4
        //   298: ifle            33
        //   301: aload_0        
        //   302: getfield        com/sun/mail/iap/ResponseInputStream.bin:Ljava/io/BufferedInputStream;
        //   305: aload           7
        //   307: iload_2        
        //   308: iload           4
        //   310: invokevirtual   java/io/BufferedInputStream.read:([BII)I
        //   313: istore_3       
        //   314: iload           4
        //   316: iload_3        
        //   317: isub           
        //   318: istore          4
        //   320: iload_2        
        //   321: iload_3        
        //   322: iadd           
        //   323: istore_2       
        //   324: goto            291
        //   327: aload_1        
        //   328: iload_3        
        //   329: baload         
        //   330: bipush          123
        //   332: if_icmpeq       222
        //   335: iload_3        
        //   336: iconst_1       
        //   337: isub           
        //   338: istore_3       
        //   339: goto            218
        //   342: astore_1       
        //   343: goto            205
        //   346: iload_3        
        //   347: bipush          16
        //   349: iadd           
        //   350: iload           4
        //   352: isub           
        //   353: istore          4
        //   355: goto            274
        //   358: aload_1        
        //   359: astore          7
        //   361: iload_3        
        //   362: istore          4
        //   364: goto            291
        //   367: iload_2        
        //   368: istore_3       
        //   369: goto            33
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  226    237    342    346    Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: cmpeq:boolean(loadelement:byte(var_1_1E:byte[], var_3_D9:int), ldc:byte(123))
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
