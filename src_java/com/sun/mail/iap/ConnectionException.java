package com.sun.mail.iap;

public class ConnectionException extends ProtocolException
{
    private static final long serialVersionUID = 5749739604257464727L;
    private transient Protocol p;
    
    public ConnectionException() {
    }
    
    public ConnectionException(final Protocol p2, final Response response) {
        super(response);
        this.p = p2;
    }
    
    public ConnectionException(final String s) {
        super(s);
    }
    
    public Protocol getProtocol() {
        return this.p;
    }
}
