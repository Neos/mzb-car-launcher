package com.sun.mail.iap;

import java.util.*;
import java.net.*;
import java.io.*;
import com.sun.mail.util.*;

public class Protocol
{
    private static final byte[] CRLF;
    private boolean connected;
    protected boolean debug;
    private volatile Vector handlers;
    protected String host;
    private volatile ResponseInputStream input;
    protected PrintStream out;
    private volatile DataOutputStream output;
    protected String prefix;
    protected Properties props;
    protected boolean quote;
    private Socket socket;
    private int tagCounter;
    private volatile long timestamp;
    private TraceInputStream traceInput;
    private TraceOutputStream traceOutput;
    
    static {
        CRLF = new byte[] { 13, 10 };
    }
    
    public Protocol(final InputStream inputStream, final OutputStream outputStream, final boolean trace) throws IOException {
        this.connected = false;
        this.tagCounter = 0;
        this.handlers = null;
        this.host = "localhost";
        this.debug = trace;
        this.quote = false;
        this.out = System.out;
        (this.traceInput = new TraceInputStream(inputStream, System.out)).setTrace(trace);
        this.traceInput.setQuote(this.quote);
        this.input = new ResponseInputStream(this.traceInput);
        (this.traceOutput = new TraceOutputStream(outputStream, System.out)).setTrace(trace);
        this.traceOutput.setQuote(this.quote);
        this.output = new DataOutputStream(new BufferedOutputStream(this.traceOutput));
        this.timestamp = System.currentTimeMillis();
    }
    
    public Protocol(String property, final int n, final boolean debug, final PrintStream out, final Properties props, final String prefix, final boolean b) throws IOException, ProtocolException {
        final boolean b2 = true;
        this.connected = false;
        this.tagCounter = 0;
        this.handlers = null;
        try {
            this.host = property;
            this.debug = debug;
            this.out = out;
            this.props = props;
            this.prefix = prefix;
            this.socket = SocketFetcher.getSocket(property, n, props, prefix, b);
            property = props.getProperty("mail.debug.quote");
            this.quote = (property != null && property.equalsIgnoreCase("true") && b2);
            this.initStreams(out);
            this.processGreeting(this.readResponse());
            this.timestamp = System.currentTimeMillis();
            this.connected = true;
        }
        finally {
            if (!this.connected) {
                this.disconnect();
            }
        }
    }
    
    private void initStreams(final PrintStream printStream) throws IOException {
        (this.traceInput = new TraceInputStream(this.socket.getInputStream(), printStream)).setTrace(this.debug);
        this.traceInput.setQuote(this.quote);
        this.input = new ResponseInputStream(this.traceInput);
        (this.traceOutput = new TraceOutputStream(this.socket.getOutputStream(), printStream)).setTrace(this.debug);
        this.traceOutput.setQuote(this.quote);
        this.output = new DataOutputStream(new BufferedOutputStream(this.traceOutput));
    }
    
    public void addResponseHandler(final ResponseHandler responseHandler) {
        synchronized (this) {
            if (this.handlers == null) {
                this.handlers = new Vector();
            }
            this.handlers.addElement(responseHandler);
        }
    }
    
    public Response[] command(String writeCommand, final Argument argument) {
        synchronized (this) {
            while (true) {
                final Vector<Response> vector = new Vector<Response>();
                int n = 0;
                final String s = null;
                while (true) {
                    try {
                        writeCommand = this.writeCommand(writeCommand, argument);
                        if (n != 0) {
                            final Response[] array = new Response[vector.size()];
                            vector.copyInto(array);
                            this.timestamp = System.currentTimeMillis();
                            return array;
                        }
                    }
                    catch (LiteralException ex) {
                        vector.addElement(ex.getResponse());
                        n = 1;
                        writeCommand = s;
                        continue;
                    }
                    catch (Exception ex2) {
                        vector.addElement(Response.byeResponse(ex2));
                        n = 1;
                        writeCommand = s;
                        continue;
                    }
                    try {
                        final Response response = this.readResponse();
                        vector.addElement(response);
                        int n2 = n;
                        if (response.isBYE()) {
                            n2 = 1;
                        }
                        n = n2;
                        if (!response.isTagged()) {
                            continue;
                        }
                        n = n2;
                        if (response.getTag().equals(writeCommand)) {
                            n = 1;
                            continue;
                        }
                        continue;
                    }
                    catch (IOException ex3) {
                        final Response response = Response.byeResponse(ex3);
                    }
                    catch (ProtocolException ex4) {}
                    break;
                }
            }
        }
    }
    
    protected void disconnect() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        com/sun/mail/iap/Protocol.socket:Ljava/net/Socket;
        //     6: astore_1       
        //     7: aload_1        
        //     8: ifnull          23
        //    11: aload_0        
        //    12: getfield        com/sun/mail/iap/Protocol.socket:Ljava/net/Socket;
        //    15: invokevirtual   java/net/Socket.close:()V
        //    18: aload_0        
        //    19: aconst_null    
        //    20: putfield        com/sun/mail/iap/Protocol.socket:Ljava/net/Socket;
        //    23: aload_0        
        //    24: monitorexit    
        //    25: return         
        //    26: astore_1       
        //    27: aload_0        
        //    28: monitorexit    
        //    29: aload_1        
        //    30: athrow         
        //    31: astore_1       
        //    32: goto            18
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      7      26     31     Any
        //  11     18     31     35     Ljava/io/IOException;
        //  11     18     26     31     Any
        //  18     23     26     31     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0018:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.disconnect();
    }
    
    protected ResponseInputStream getInputStream() {
        return this.input;
    }
    
    protected OutputStream getOutputStream() {
        return this.output;
    }
    
    protected ByteArray getResponseBuffer() {
        return null;
    }
    
    public long getTimestamp() {
        return this.timestamp;
    }
    
    public void handleResult(final Response response) throws ProtocolException {
        if (!response.isOK()) {
            if (response.isNO()) {
                throw new CommandFailedException(response);
            }
            if (response.isBAD()) {
                throw new BadCommandException(response);
            }
            if (response.isBYE()) {
                this.disconnect();
                throw new ConnectionException(this, response);
            }
        }
    }
    
    public void notifyResponseHandlers(final Response[] array) {
        if (this.handlers != null) {
            for (int i = 0; i < array.length; ++i) {
                final Response response = array[i];
                if (response != null) {
                    final int size = this.handlers.size();
                    if (size == 0) {
                        break;
                    }
                    final Object[] array2 = new Object[size];
                    this.handlers.copyInto(array2);
                    for (int j = 0; j < size; ++j) {
                        ((ResponseHandler)array2[j]).handleResponse(response);
                    }
                }
            }
        }
    }
    
    protected void processGreeting(final Response response) throws ProtocolException {
        if (response.isBYE()) {
            throw new ConnectionException(this, response);
        }
    }
    
    public Response readResponse() throws IOException, ProtocolException {
        return new Response(this);
    }
    
    public void removeResponseHandler(final ResponseHandler responseHandler) {
        synchronized (this) {
            if (this.handlers != null) {
                this.handlers.removeElement(responseHandler);
            }
        }
    }
    
    public void simpleCommand(final String s, final Argument argument) throws ProtocolException {
        final Response[] command = this.command(s, argument);
        this.notifyResponseHandlers(command);
        this.handleResult(command[command.length - 1]);
    }
    
    public void startTLS(final String s) throws IOException, ProtocolException {
        synchronized (this) {
            this.simpleCommand(s, null);
            this.socket = SocketFetcher.startTLS(this.socket, this.props, this.prefix);
            this.initStreams(this.out);
        }
    }
    
    protected boolean supportsNonSyncLiterals() {
        // monitorenter(this)
        // monitorexit(this)
        return false;
    }
    
    public String writeCommand(final String s, final Argument argument) throws IOException, ProtocolException {
        final String string = "A" + Integer.toString(this.tagCounter++, 10);
        this.output.writeBytes(String.valueOf(string) + " " + s);
        if (argument != null) {
            this.output.write(32);
            argument.write(this);
        }
        this.output.write(Protocol.CRLF);
        this.output.flush();
        return string;
    }
}
