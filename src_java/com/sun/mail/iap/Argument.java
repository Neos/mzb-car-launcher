package com.sun.mail.iap;

import java.util.*;
import com.sun.mail.util.*;
import java.io.*;

public class Argument
{
    protected Vector items;
    
    public Argument() {
        this.items = new Vector(1);
    }
    
    private void astring(final byte[] array, final Protocol protocol) throws IOException, ProtocolException {
        final DataOutputStream dataOutputStream = (DataOutputStream)protocol.getOutputStream();
        final int length = array.length;
        if (length > 1024) {
            this.literal(array, protocol);
        }
        else {
            boolean b;
            if (length == 0) {
                b = true;
            }
            else {
                b = false;
            }
            int n = 0;
            int n2;
            for (int i = 0; i < length; ++i, n = n2) {
                final byte b2 = array[i];
                if (b2 == 0 || b2 == 13 || b2 == 10 || (b2 & 0xFF) > 127) {
                    this.literal(array, protocol);
                    return;
                }
                if (b2 != 42 && b2 != 37 && b2 != 40 && b2 != 41 && b2 != 123 && b2 != 34 && b2 != 92) {
                    n2 = n;
                    if ((b2 & 0xFF) > 32) {
                        continue;
                    }
                }
                final boolean b3 = true;
                if (b2 != 34) {
                    n2 = n;
                    b = b3;
                    if (b2 != 92) {
                        continue;
                    }
                }
                n2 = 1;
                b = b3;
            }
            if (b) {
                dataOutputStream.write(34);
            }
            if (n != 0) {
                for (int j = 0; j < length; ++j) {
                    final byte b4 = array[j];
                    if (b4 == 34 || b4 == 92) {
                        dataOutputStream.write(92);
                    }
                    dataOutputStream.write(b4);
                }
            }
            else {
                dataOutputStream.write(array);
            }
            if (b) {
                dataOutputStream.write(34);
            }
        }
    }
    
    private void literal(final Literal literal, final Protocol protocol) throws IOException, ProtocolException {
        literal.writeTo(this.startLiteral(protocol, literal.size()));
    }
    
    private void literal(final ByteArrayOutputStream byteArrayOutputStream, final Protocol protocol) throws IOException, ProtocolException {
        byteArrayOutputStream.writeTo(this.startLiteral(protocol, byteArrayOutputStream.size()));
    }
    
    private void literal(final byte[] array, final Protocol protocol) throws IOException, ProtocolException {
        this.startLiteral(protocol, array.length).write(array);
    }
    
    private OutputStream startLiteral(final Protocol protocol, final int n) throws IOException, ProtocolException {
        final DataOutputStream dataOutputStream = (DataOutputStream)protocol.getOutputStream();
        final boolean supportsNonSyncLiterals = protocol.supportsNonSyncLiterals();
        dataOutputStream.write(123);
        dataOutputStream.writeBytes(Integer.toString(n));
        if (supportsNonSyncLiterals) {
            dataOutputStream.writeBytes("+}\r\n");
        }
        else {
            dataOutputStream.writeBytes("}\r\n");
        }
        dataOutputStream.flush();
        if (!supportsNonSyncLiterals) {
            Response response;
            do {
                response = protocol.readResponse();
                if (response.isContinuation()) {
                    return dataOutputStream;
                }
            } while (!response.isTagged());
            throw new LiteralException(response);
        }
        return dataOutputStream;
    }
    
    public void append(final Argument argument) {
        this.items.ensureCapacity(this.items.size() + argument.items.size());
        for (int i = 0; i < argument.items.size(); ++i) {
            this.items.addElement(argument.items.elementAt(i));
        }
    }
    
    public void write(final Protocol protocol) throws IOException, ProtocolException {
        int size;
        if (this.items != null) {
            size = this.items.size();
        }
        else {
            size = 0;
        }
        final DataOutputStream dataOutputStream = (DataOutputStream)protocol.getOutputStream();
        for (int i = 0; i < size; ++i) {
            if (i > 0) {
                dataOutputStream.write(32);
            }
            final Object element = this.items.elementAt(i);
            if (element instanceof Atom) {
                dataOutputStream.writeBytes(((Atom)element).string);
            }
            else if (element instanceof Number) {
                dataOutputStream.writeBytes(((Number)element).toString());
            }
            else if (element instanceof AString) {
                this.astring(((AString)element).bytes, protocol);
            }
            else if (element instanceof byte[]) {
                this.literal((byte[])element, protocol);
            }
            else if (element instanceof ByteArrayOutputStream) {
                this.literal((ByteArrayOutputStream)element, protocol);
            }
            else if (element instanceof Literal) {
                this.literal((Literal)element, protocol);
            }
            else if (element instanceof Argument) {
                dataOutputStream.write(40);
                ((Argument)element).write(protocol);
                dataOutputStream.write(41);
            }
        }
    }
    
    public void writeArgument(final Argument argument) {
        this.items.addElement(argument);
    }
    
    public void writeAtom(final String s) {
        this.items.addElement(new Atom(s));
    }
    
    public void writeBytes(final Literal literal) {
        this.items.addElement(literal);
    }
    
    public void writeBytes(final ByteArrayOutputStream byteArrayOutputStream) {
        this.items.addElement(byteArrayOutputStream);
    }
    
    public void writeBytes(final byte[] array) {
        this.items.addElement(array);
    }
    
    public void writeNumber(final int n) {
        this.items.addElement(new Integer(n));
    }
    
    public void writeNumber(final long n) {
        this.items.addElement(new Long(n));
    }
    
    public void writeString(final String s) {
        this.items.addElement(new AString(ASCIIUtility.getBytes(s)));
    }
    
    public void writeString(final String s, final String s2) throws UnsupportedEncodingException {
        if (s2 == null) {
            this.writeString(s);
            return;
        }
        this.items.addElement(new AString(s.getBytes(s2)));
    }
}
