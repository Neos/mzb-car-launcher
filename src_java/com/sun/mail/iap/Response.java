package com.sun.mail.iap;

import com.sun.mail.util.*;
import java.io.*;
import java.util.*;

public class Response
{
    public static final int BAD = 12;
    public static final int BYE = 16;
    public static final int CONTINUATION = 1;
    public static final int NO = 8;
    public static final int OK = 4;
    public static final int SYNTHETIC = 32;
    public static final int TAGGED = 2;
    public static final int TAG_MASK = 3;
    public static final int TYPE_MASK = 28;
    public static final int UNTAGGED = 3;
    private static final int increment = 100;
    protected byte[] buffer;
    protected int index;
    protected int pindex;
    protected int size;
    protected String tag;
    protected int type;
    
    public Response(final Protocol protocol) throws IOException, ProtocolException {
        this.buffer = null;
        this.type = 0;
        this.tag = null;
        final ByteArray response = protocol.getInputStream().readResponse(protocol.getResponseBuffer());
        this.buffer = response.getBytes();
        this.size = response.getCount() - 2;
        this.parse();
    }
    
    public Response(final Response response) {
        this.buffer = null;
        this.type = 0;
        this.tag = null;
        this.index = response.index;
        this.size = response.size;
        this.buffer = response.buffer;
        this.type = response.type;
        this.tag = response.tag;
    }
    
    public Response(final String s) {
        this.buffer = null;
        this.type = 0;
        this.tag = null;
        this.buffer = ASCIIUtility.getBytes(s);
        this.size = this.buffer.length;
        this.parse();
    }
    
    public static Response byeResponse(final Exception ex) {
        final Response response = new Response(("* BYE JavaMail Exception: " + ex.toString()).replace('\r', ' ').replace('\n', ' '));
        response.type |= 0x20;
        return response;
    }
    
    private void parse() {
        this.index = 0;
        if (this.buffer[this.index] == 43) {
            this.type |= 0x1;
            ++this.index;
            return;
        }
        if (this.buffer[this.index] == 42) {
            this.type |= 0x3;
            ++this.index;
        }
        else {
            this.type |= 0x2;
            this.tag = this.readAtom();
        }
        final int index = this.index;
        String atom;
        if ((atom = this.readAtom()) == null) {
            atom = "";
        }
        if (atom.equalsIgnoreCase("OK")) {
            this.type |= 0x4;
        }
        else if (atom.equalsIgnoreCase("NO")) {
            this.type |= 0x8;
        }
        else if (atom.equalsIgnoreCase("BAD")) {
            this.type |= 0xC;
        }
        else if (atom.equalsIgnoreCase("BYE")) {
            this.type |= 0x10;
        }
        else {
            this.index = index;
        }
        this.pindex = this.index;
    }
    
    private Object parseString(final boolean b, final boolean b2) {
        Object o = null;
        this.skipSpaces();
        final byte b3 = this.buffer[this.index];
        if (b3 == 34) {
            ++this.index;
            final int index = this.index;
            int index2 = this.index;
            while (true) {
                final byte b4 = this.buffer[this.index];
                if (b4 == 34) {
                    break;
                }
                if (b4 == 92) {
                    ++this.index;
                }
                if (this.index != index2) {
                    this.buffer[index2] = this.buffer[this.index];
                }
                ++index2;
                ++this.index;
            }
            ++this.index;
            if (!b2) {
                return new ByteArray(this.buffer, index, index2 - index);
            }
            o = ASCIIUtility.toString(this.buffer, index, index2);
        }
        else {
            if (b3 == 123) {
                final int index3 = this.index + 1;
                this.index = index3;
                int int1 = 0;
                int n = 0;
            Label_0245_Outer:
                while (true) {
                    while (true) {
                        if (this.buffer[this.index] == 125) {
                            try {
                                int1 = ASCIIUtility.parseInt(this.buffer, index3, this.index);
                                n = this.index + 3;
                                this.index = n + int1;
                                if (b2) {
                                    return ASCIIUtility.toString(this.buffer, n, n + int1);
                                }
                                break;
                                ++this.index;
                                continue Label_0245_Outer;
                            }
                            catch (NumberFormatException ex) {
                                return null;
                            }
                            break;
                        }
                        continue;
                    }
                }
                return new ByteArray(this.buffer, n, int1);
            }
            if (b) {
                final int index4 = this.index;
                o = this.readAtom();
                if (!b2) {
                    return new ByteArray(this.buffer, index4, this.index);
                }
            }
            else if (b3 == 78 || b3 == 110) {
                this.index += 3;
                return null;
            }
        }
        return o;
    }
    
    public String getRest() {
        this.skipSpaces();
        return ASCIIUtility.toString(this.buffer, this.index, this.size);
    }
    
    public String getTag() {
        return this.tag;
    }
    
    public int getType() {
        return this.type;
    }
    
    public boolean isBAD() {
        return (this.type & 0x1C) == 0xC;
    }
    
    public boolean isBYE() {
        return (this.type & 0x1C) == 0x10;
    }
    
    public boolean isContinuation() {
        return (this.type & 0x3) == 0x1;
    }
    
    public boolean isNO() {
        return (this.type & 0x1C) == 0x8;
    }
    
    public boolean isOK() {
        return (this.type & 0x1C) == 0x4;
    }
    
    public boolean isSynthetic() {
        return (this.type & 0x20) == 0x20;
    }
    
    public boolean isTagged() {
        return (this.type & 0x3) == 0x2;
    }
    
    public boolean isUnTagged() {
        return (this.type & 0x3) == 0x3;
    }
    
    public byte peekByte() {
        if (this.index < this.size) {
            return this.buffer[this.index];
        }
        return 0;
    }
    
    public String readAtom() {
        return this.readAtom('\0');
    }
    
    public String readAtom(final char c) {
        this.skipSpaces();
        if (this.index >= this.size) {
            return null;
        }
        final int index = this.index;
        while (this.index < this.size) {
            final byte b = this.buffer[this.index];
            if (b <= 32 || b == 40 || b == 41 || b == 37 || b == 42 || b == 34 || b == 92 || b == 127 || (c != '\0' && b == c)) {
                break;
            }
            ++this.index;
        }
        return ASCIIUtility.toString(this.buffer, index, this.index);
    }
    
    public String readAtomString() {
        return (String)this.parseString(true, true);
    }
    
    public byte readByte() {
        if (this.index < this.size) {
            return this.buffer[this.index++];
        }
        return 0;
    }
    
    public ByteArray readByteArray() {
        if (this.isContinuation()) {
            this.skipSpaces();
            return new ByteArray(this.buffer, this.index, this.size - this.index);
        }
        return (ByteArray)this.parseString(false, false);
    }
    
    public ByteArrayInputStream readBytes() {
        final ByteArray byteArray = this.readByteArray();
        if (byteArray != null) {
            return byteArray.toByteArrayInputStream();
        }
        return null;
    }
    
    public long readLong() {
        this.skipSpaces();
        final int index = this.index;
        while (true) {
            Label_0059: {
                if (this.index < this.size && Character.isDigit((char)this.buffer[this.index])) {
                    break Label_0059;
                }
                if (this.index <= index) {
                    break;
                }
                try {
                    return ASCIIUtility.parseLong(this.buffer, index, this.index);
                    ++this.index;
                    continue;
                }
                catch (NumberFormatException ex) {}
            }
            break;
        }
        return -1L;
    }
    
    public int readNumber() {
        this.skipSpaces();
        final int index = this.index;
        while (true) {
            Label_0059: {
                if (this.index < this.size && Character.isDigit((char)this.buffer[this.index])) {
                    break Label_0059;
                }
                if (this.index <= index) {
                    break;
                }
                try {
                    return ASCIIUtility.parseInt(this.buffer, index, this.index);
                    ++this.index;
                    continue;
                }
                catch (NumberFormatException ex) {}
            }
            break;
        }
        return -1;
    }
    
    public String readString() {
        return (String)this.parseString(false, true);
    }
    
    public String readString(final char c) {
        this.skipSpaces();
        if (this.index >= this.size) {
            return null;
        }
        final int index = this.index;
        while (this.index < this.size && this.buffer[this.index] != c) {
            ++this.index;
        }
        return ASCIIUtility.toString(this.buffer, index, this.index);
    }
    
    public String[] readStringList() {
        this.skipSpaces();
        if (this.buffer[this.index] == 40) {
            ++this.index;
            final Vector<String> vector = new Vector<String>();
            do {
                vector.addElement(this.readString());
            } while (this.buffer[this.index++] != 41);
            final int size = vector.size();
            if (size > 0) {
                final String[] array = new String[size];
                vector.copyInto(array);
                return array;
            }
        }
        return null;
    }
    
    public void reset() {
        this.index = this.pindex;
    }
    
    public void skip(final int n) {
        this.index += n;
    }
    
    public void skipSpaces() {
        while (this.index < this.size && this.buffer[this.index] == 32) {
            ++this.index;
        }
    }
    
    public void skipToken() {
        while (this.index < this.size && this.buffer[this.index] != 32) {
            ++this.index;
        }
    }
    
    @Override
    public String toString() {
        return ASCIIUtility.toString(this.buffer, 0, this.size);
    }
}
