package com.sun.mail.pop3;

import javax.mail.internet.*;
import java.util.*;
import java.io.*;
import javax.mail.*;

public class POP3Message extends MimeMessage
{
    static final String UNKNOWN = "UNKNOWN";
    private POP3Folder folder;
    private int hdrSize;
    private int msgSize;
    String uid;
    
    public POP3Message(final Folder folder, final int n) throws MessagingException {
        super(folder, n);
        this.hdrSize = -1;
        this.msgSize = -1;
        this.uid = "UNKNOWN";
        this.folder = (POP3Folder)folder;
    }
    
    private void loadHeaders() throws MessagingException {
        try {
            synchronized (this) {
                if (this.headers != null) {
                    return;
                }
                if (((POP3Store)this.folder.getStore()).disableTop || this.folder.getProtocol().top(this.msgnum, 0) == null) {
                    this.getContentStream().close();
                    return;
                }
                goto Label_0088;
            }
        }
        catch (EOFException ex) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, ex.toString());
        }
        catch (IOException ex2) {
            throw new MessagingException("error loading POP3 headers", ex2);
        }
    }
    
    @Override
    public void addHeader(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }
    
    @Override
    public void addHeaderLine(final String s) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }
    
    @Override
    public Enumeration getAllHeaderLines() throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getAllHeaderLines();
    }
    
    @Override
    public Enumeration getAllHeaders() throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getAllHeaders();
    }
    
    @Override
    protected InputStream getContentStream() throws MessagingException {
        try {
            synchronized (this) {
                if (this.contentStream != null) {
                    goto Label_0168;
                }
                final Protocol protocol = this.folder.getProtocol();
                final int msgnum = this.msgnum;
                if (this.msgSize > 0 && protocol.retr(msgnum, this.msgSize + this.hdrSize) == null) {
                    this.expunged = true;
                    throw new MessageRemovedException();
                }
                goto Label_0093;
            }
        }
        catch (EOFException ex) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, ex.toString());
        }
        catch (IOException ex2) {
            throw new MessagingException("error fetching POP3 content", ex2);
        }
    }
    
    @Override
    public String getHeader(final String s, final String s2) throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getHeader(s, s2);
    }
    
    @Override
    public String[] getHeader(final String s) throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getHeader(s);
    }
    
    @Override
    public Enumeration getMatchingHeaderLines(final String[] array) throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getMatchingHeaders(final String[] array) throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getMatchingHeaders(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaderLines(final String[] array) throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getNonMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaders(final String[] array) throws MessagingException {
        if (this.headers == null) {
            this.loadHeaders();
        }
        return this.headers.getNonMatchingHeaders(array);
    }
    
    @Override
    public int getSize() throws MessagingException {
        try {
            synchronized (this) {
                if (this.msgSize >= 0) {
                    return this.msgSize;
                }
                if (this.msgSize < 0) {
                    if (this.headers == null) {
                        this.loadHeaders();
                    }
                    if (this.contentStream == null) {
                        goto Label_0093;
                    }
                    this.msgSize = this.contentStream.available();
                }
                return this.msgSize;
            }
        }
        catch (EOFException ex) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, ex.toString());
        }
        catch (IOException ex2) {
            throw new MessagingException("error getting size", ex2);
        }
    }
    
    public void invalidate(final boolean b) {
        synchronized (this) {
            this.content = null;
            this.contentStream = null;
            this.msgSize = -1;
            if (b) {
                this.headers = null;
                this.hdrSize = -1;
            }
        }
    }
    
    @Override
    public void removeHeader(final String s) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }
    
    @Override
    public void saveChanges() throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }
    
    @Override
    public void setFlags(final Flags flags, final boolean b) throws MessagingException {
        final Flags flags2 = (Flags)this.flags.clone();
        super.setFlags(flags, b);
        if (!this.flags.equals(flags2)) {
            this.folder.notifyMessageChangedListeners(1, this);
        }
    }
    
    @Override
    public void setHeader(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }
    
    public InputStream top(final int n) throws MessagingException {
        try {
            synchronized (this) {
                return this.folder.getProtocol().top(this.msgnum, n);
            }
        }
        catch (EOFException ex) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, ex.toString());
        }
        catch (IOException ex2) {
            throw new MessagingException("error getting size", ex2);
        }
    }
}
