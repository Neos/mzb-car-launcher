package com.sun.mail.pop3;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.mail.*;

public class POP3Folder extends Folder
{
    private boolean doneUidl;
    private boolean exists;
    private Vector message_cache;
    private String name;
    private boolean opened;
    private Protocol port;
    private int size;
    private int total;
    
    POP3Folder(final POP3Store pop3Store, final String name) {
        super(pop3Store);
        this.exists = false;
        this.opened = false;
        this.doneUidl = false;
        this.name = name;
        if (name.equalsIgnoreCase("INBOX")) {
            this.exists = true;
        }
    }
    
    @Override
    public void appendMessages(final Message[] array) throws MessagingException {
        throw new MethodNotSupportedException("Append not supported");
    }
    
    void checkClosed() throws IllegalStateException {
        if (this.opened) {
            throw new IllegalStateException("Folder is Open");
        }
    }
    
    void checkOpen() throws IllegalStateException {
        if (!this.opened) {
            throw new IllegalStateException("Folder is not Open");
        }
    }
    
    void checkReadable() throws IllegalStateException {
        if (!this.opened || (this.mode != 1 && this.mode != 2)) {
            throw new IllegalStateException("Folder is not Readable");
        }
    }
    
    void checkWritable() throws IllegalStateException {
        if (!this.opened || this.mode != 2) {
            throw new IllegalStateException("Folder is not Writable");
        }
    }
    
    @Override
    public void close(final boolean b) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpen();
            try {
                if (((POP3Store)this.store).rsetBeforeQuit) {
                    this.port.rset();
                }
                if (b && this.mode == 2) {
                    int i = 0;
                    while (i < this.message_cache.size()) {
                        final POP3Message pop3Message = this.message_cache.elementAt(i);
                        Label_0133: {
                            if (pop3Message == null || !pop3Message.isSet(Flags.Flag.DELETED)) {
                                break Label_0133;
                            }
                            try {
                                this.port.dele(i + 1);
                                ++i;
                            }
                            catch (IOException ex) {
                                throw new MessagingException("Exception deleting messages during close", ex);
                            }
                        }
                    }
                }
                this.port.quit();
                this.port = null;
                ((POP3Store)this.store).closePort(this);
                this.message_cache = null;
                this.opened = false;
                this.notifyConnectionListeners(3);
            }
            catch (IOException ex2) {
                this.port = null;
                ((POP3Store)this.store).closePort(this);
                this.message_cache = null;
                this.opened = false;
                this.notifyConnectionListeners(3);
            }
            finally {
                this.port = null;
                ((POP3Store)this.store).closePort(this);
                this.message_cache = null;
                this.opened = false;
                this.notifyConnectionListeners(3);
            }
        }
        finally {}
    }
    
    @Override
    public boolean create(final int n) throws MessagingException {
        return false;
    }
    
    protected POP3Message createMessage(final Folder folder, final int n) throws MessagingException {
        POP3Message pop3Message = null;
        final Constructor messageConstructor = ((POP3Store)this.store).messageConstructor;
        POP3Message pop3Message2 = pop3Message;
        while (true) {
            if (messageConstructor == null) {
                break Label_0049;
            }
            try {
                pop3Message2 = messageConstructor.newInstance(this, new Integer(n));
                if ((pop3Message = pop3Message2) == null) {
                    pop3Message = new POP3Message(this, n);
                }
                return pop3Message;
            }
            catch (Exception ex) {
                pop3Message2 = pop3Message;
                continue;
            }
            break;
        }
    }
    
    @Override
    public boolean delete(final boolean b) throws MessagingException {
        throw new MethodNotSupportedException("delete");
    }
    
    @Override
    public boolean exists() {
        return this.exists;
    }
    
    @Override
    public Message[] expunge() throws MessagingException {
        throw new MethodNotSupportedException("Expunge not supported");
    }
    
    @Override
    public void fetch(final Message[] p0, final FetchProfile p1) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokevirtual   com/sun/mail/pop3/POP3Folder.checkReadable:()V
        //     6: aload_0        
        //     7: getfield        com/sun/mail/pop3/POP3Folder.doneUidl:Z
        //    10: ifne            102
        //    13: aload_2        
        //    14: getstatic       javax/mail/UIDFolder$FetchProfileItem.UID:Ljavax/mail/UIDFolder$FetchProfileItem;
        //    17: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //    20: ifeq            102
        //    23: aload_0        
        //    24: getfield        com/sun/mail/pop3/POP3Folder.message_cache:Ljava/util/Vector;
        //    27: invokevirtual   java/util/Vector.size:()I
        //    30: anewarray       Ljava/lang/String;
        //    33: astore          6
        //    35: aload_0        
        //    36: getfield        com/sun/mail/pop3/POP3Folder.port:Lcom/sun/mail/pop3/Protocol;
        //    39: aload           6
        //    41: invokevirtual   com/sun/mail/pop3/Protocol.uidl:([Ljava/lang/String;)Z
        //    44: istore          5
        //    46: iload           5
        //    48: ifne            184
        //    51: aload_0        
        //    52: monitorexit    
        //    53: return         
        //    54: astore_1       
        //    55: aload_0        
        //    56: iconst_0       
        //    57: invokevirtual   com/sun/mail/pop3/POP3Folder.close:(Z)V
        //    60: new             Ljavax/mail/FolderClosedException;
        //    63: dup            
        //    64: aload_0        
        //    65: aload_1        
        //    66: invokevirtual   java/io/EOFException.toString:()Ljava/lang/String;
        //    69: invokespecial   javax/mail/FolderClosedException.<init>:(Ljavax/mail/Folder;Ljava/lang/String;)V
        //    72: athrow         
        //    73: astore_1       
        //    74: aload_0        
        //    75: monitorexit    
        //    76: aload_1        
        //    77: athrow         
        //    78: astore_1       
        //    79: new             Ljavax/mail/MessagingException;
        //    82: dup            
        //    83: ldc             "error getting UIDL"
        //    85: aload_1        
        //    86: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //    89: athrow         
        //    90: iload_3        
        //    91: aload           6
        //    93: arraylength    
        //    94: if_icmplt       150
        //    97: aload_0        
        //    98: iconst_1       
        //    99: putfield        com/sun/mail/pop3/POP3Folder.doneUidl:Z
        //   102: aload_2        
        //   103: getstatic       javax/mail/FetchProfile$Item.ENVELOPE:Ljavax/mail/FetchProfile$Item;
        //   106: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //   109: ifeq            51
        //   112: iconst_0       
        //   113: istore_3       
        //   114: aload_1        
        //   115: arraylength    
        //   116: istore          4
        //   118: iload_3        
        //   119: iload           4
        //   121: if_icmpge       51
        //   124: aload_1        
        //   125: iload_3        
        //   126: aaload         
        //   127: checkcast       Lcom/sun/mail/pop3/POP3Message;
        //   130: astore_2       
        //   131: aload_2        
        //   132: ldc             ""
        //   134: invokevirtual   com/sun/mail/pop3/POP3Message.getHeader:(Ljava/lang/String;)[Ljava/lang/String;
        //   137: pop            
        //   138: aload_2        
        //   139: invokevirtual   com/sun/mail/pop3/POP3Message.getSize:()I
        //   142: pop            
        //   143: iload_3        
        //   144: iconst_1       
        //   145: iadd           
        //   146: istore_3       
        //   147: goto            114
        //   150: aload           6
        //   152: iload_3        
        //   153: aaload         
        //   154: ifnonnull       160
        //   157: goto            189
        //   160: aload_0        
        //   161: iload_3        
        //   162: iconst_1       
        //   163: iadd           
        //   164: invokevirtual   com/sun/mail/pop3/POP3Folder.getMessage:(I)Ljavax/mail/Message;
        //   167: checkcast       Lcom/sun/mail/pop3/POP3Message;
        //   170: aload           6
        //   172: iload_3        
        //   173: aaload         
        //   174: putfield        com/sun/mail/pop3/POP3Message.uid:Ljava/lang/String;
        //   177: goto            189
        //   180: astore_2       
        //   181: goto            143
        //   184: iconst_0       
        //   185: istore_3       
        //   186: goto            90
        //   189: iload_3        
        //   190: iconst_1       
        //   191: iadd           
        //   192: istore_3       
        //   193: goto            90
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      35     73     78     Any
        //  35     46     54     73     Ljava/io/EOFException;
        //  35     46     78     90     Ljava/io/IOException;
        //  35     46     73     78     Any
        //  55     73     73     78     Any
        //  79     90     73     78     Any
        //  90     102    73     78     Any
        //  102    112    73     78     Any
        //  114    118    73     78     Any
        //  124    143    180    184    Ljavax/mail/MessageRemovedException;
        //  124    143    73     78     Any
        //  160    177    73     78     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.close(false);
    }
    
    @Override
    public Folder getFolder(final String s) throws MessagingException {
        throw new MessagingException("not a directory");
    }
    
    @Override
    public String getFullName() {
        return this.name;
    }
    
    @Override
    public Message getMessage(final int n) throws MessagingException {
        synchronized (this) {
            this.checkOpen();
            POP3Message message;
            if ((message = this.message_cache.elementAt(n - 1)) == null) {
                message = this.createMessage(this, n);
                this.message_cache.setElementAt(message, n - 1);
            }
            return message;
        }
    }
    
    @Override
    public int getMessageCount() throws MessagingException {
        synchronized (this) {
            int total;
            if (!this.opened) {
                total = -1;
            }
            else {
                this.checkReadable();
                total = this.total;
            }
            return total;
        }
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    @Override
    public Folder getParent() {
        return new DefaultFolder((POP3Store)this.store);
    }
    
    @Override
    public Flags getPermanentFlags() {
        return new Flags();
    }
    
    Protocol getProtocol() throws MessagingException {
        this.checkOpen();
        return this.port;
    }
    
    @Override
    public char getSeparator() {
        return '\0';
    }
    
    public int getSize() throws MessagingException {
        synchronized (this) {
            this.checkOpen();
            return this.size;
        }
    }
    
    public int[] getSizes() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokevirtual   com/sun/mail/pop3/POP3Folder.checkOpen:()V
        //     6: aload_0        
        //     7: getfield        com/sun/mail/pop3/POP3Folder.total:I
        //    10: newarray        I
        //    12: astore          9
        //    14: aconst_null    
        //    15: astore          5
        //    17: aconst_null    
        //    18: astore          4
        //    20: aconst_null    
        //    21: astore          7
        //    23: aconst_null    
        //    24: astore          8
        //    26: aload_0        
        //    27: getfield        com/sun/mail/pop3/POP3Folder.port:Lcom/sun/mail/pop3/Protocol;
        //    30: invokevirtual   com/sun/mail/pop3/Protocol.list:()Ljava/io/InputStream;
        //    33: astore_3       
        //    34: aload_3        
        //    35: astore          4
        //    37: aload_3        
        //    38: astore          5
        //    40: new             Lcom/sun/mail/util/LineInputStream;
        //    43: dup            
        //    44: aload_3        
        //    45: invokespecial   com/sun/mail/util/LineInputStream.<init>:(Ljava/io/InputStream;)V
        //    48: astore          6
        //    50: aload           6
        //    52: invokevirtual   com/sun/mail/util/LineInputStream.readLine:()Ljava/lang/String;
        //    55: astore          4
        //    57: aload           4
        //    59: ifnonnull       85
        //    62: aload           6
        //    64: ifnull          72
        //    67: aload           6
        //    69: invokevirtual   com/sun/mail/util/LineInputStream.close:()V
        //    72: aload_3        
        //    73: ifnull          254
        //    76: aload_3        
        //    77: invokevirtual   java/io/InputStream.close:()V
        //    80: aload_0        
        //    81: monitorexit    
        //    82: aload           9
        //    84: areturn        
        //    85: new             Ljava/util/StringTokenizer;
        //    88: dup            
        //    89: aload           4
        //    91: invokespecial   java/util/StringTokenizer.<init>:(Ljava/lang/String;)V
        //    94: astore          4
        //    96: aload           4
        //    98: invokevirtual   java/util/StringTokenizer.nextToken:()Ljava/lang/String;
        //   101: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   104: istore_1       
        //   105: aload           4
        //   107: invokevirtual   java/util/StringTokenizer.nextToken:()Ljava/lang/String;
        //   110: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   113: istore_2       
        //   114: iload_1        
        //   115: ifle            50
        //   118: iload_1        
        //   119: aload_0        
        //   120: getfield        com/sun/mail/pop3/POP3Folder.total:I
        //   123: if_icmpgt       50
        //   126: aload           9
        //   128: iload_1        
        //   129: iconst_1       
        //   130: isub           
        //   131: iload_2        
        //   132: iastore        
        //   133: goto            50
        //   136: astore          4
        //   138: goto            50
        //   141: astore_3       
        //   142: aload           8
        //   144: astore          5
        //   146: aload           4
        //   148: astore_3       
        //   149: aload           5
        //   151: ifnull          159
        //   154: aload           5
        //   156: invokevirtual   com/sun/mail/util/LineInputStream.close:()V
        //   159: aload_3        
        //   160: ifnull          80
        //   163: aload_3        
        //   164: invokevirtual   java/io/InputStream.close:()V
        //   167: goto            80
        //   170: astore_3       
        //   171: goto            80
        //   174: astore_3       
        //   175: aload           7
        //   177: astore          4
        //   179: aload           4
        //   181: ifnull          189
        //   184: aload           4
        //   186: invokevirtual   com/sun/mail/util/LineInputStream.close:()V
        //   189: aload           5
        //   191: ifnull          199
        //   194: aload           5
        //   196: invokevirtual   java/io/InputStream.close:()V
        //   199: aload_3        
        //   200: athrow         
        //   201: astore_3       
        //   202: aload_0        
        //   203: monitorexit    
        //   204: aload_3        
        //   205: athrow         
        //   206: astore_3       
        //   207: goto            80
        //   210: astore          4
        //   212: goto            159
        //   215: astore          4
        //   217: goto            189
        //   220: astore          4
        //   222: goto            199
        //   225: astore          4
        //   227: goto            72
        //   230: astore          7
        //   232: aload           6
        //   234: astore          4
        //   236: aload_3        
        //   237: astore          5
        //   239: aload           7
        //   241: astore_3       
        //   242: goto            179
        //   245: astore          4
        //   247: aload           6
        //   249: astore          5
        //   251: goto            149
        //   254: goto            80
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      14     201    206    Any
        //  26     34     141    149    Ljava/io/IOException;
        //  26     34     174    179    Any
        //  40     50     141    149    Ljava/io/IOException;
        //  40     50     174    179    Any
        //  50     57     245    254    Ljava/io/IOException;
        //  50     57     230    245    Any
        //  67     72     225    230    Ljava/io/IOException;
        //  67     72     201    206    Any
        //  76     80     206    210    Ljava/io/IOException;
        //  76     80     201    206    Any
        //  85     114    136    141    Ljava/lang/Exception;
        //  85     114    245    254    Ljava/io/IOException;
        //  85     114    230    245    Any
        //  118    126    136    141    Ljava/lang/Exception;
        //  118    126    245    254    Ljava/io/IOException;
        //  118    126    230    245    Any
        //  154    159    210    215    Ljava/io/IOException;
        //  154    159    201    206    Any
        //  163    167    170    174    Ljava/io/IOException;
        //  163    167    201    206    Any
        //  184    189    215    220    Ljava/io/IOException;
        //  184    189    201    206    Any
        //  194    199    220    225    Ljava/io/IOException;
        //  194    199    201    206    Any
        //  199    201    201    206    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 131, Size: 131
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public int getType() {
        return 1;
    }
    
    public String getUID(final Message message) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpen();
            final POP3Message pop3Message = (POP3Message)message;
            try {
                if (pop3Message.uid == "UNKNOWN") {
                    pop3Message.uid = this.port.uidl(pop3Message.getMessageNumber());
                }
                return pop3Message.uid;
            }
            catch (EOFException ex) {
                this.close(false);
                throw new FolderClosedException(this, ex.toString());
            }
            catch (IOException ex2) {
                throw new MessagingException("error getting UIDL", ex2);
            }
        }
        finally {}
    }
    
    @Override
    public boolean hasNewMessages() throws MessagingException {
        return false;
    }
    
    @Override
    public boolean isOpen() {
        if (!this.opened) {
            return false;
        }
        if (this.store.isConnected()) {
            return true;
        }
        try {
            this.close(false);
            return false;
        }
        catch (MessagingException ex) {
            return false;
        }
    }
    
    @Override
    public Folder[] list(final String s) throws MessagingException {
        throw new MessagingException("not a directory");
    }
    
    public InputStream listCommand() throws MessagingException, IOException {
        synchronized (this) {
            this.checkOpen();
            return this.port.list();
        }
    }
    
    @Override
    protected void notifyMessageChangedListeners(final int n, final Message message) {
        super.notifyMessageChangedListeners(n, message);
    }
    
    @Override
    public void open(final int mode) throws MessagingException {
        synchronized (this) {
            this.checkClosed();
            if (!this.exists) {
                throw new FolderNotFoundException(this, "folder is not INBOX");
            }
        }
        try {
            this.port = ((POP3Store)this.store).getPort(this);
            final Status stat = this.port.stat();
            this.total = stat.total;
            this.size = stat.size;
            this.mode = mode;
            this.opened = true;
            (this.message_cache = new Vector(this.total)).setSize(this.total);
            this.doneUidl = false;
            this.notifyConnectionListeners(1);
        }
        // monitorexit(this)
        catch (IOException ex) {
            try {
                if (this.port != null) {
                    this.port.quit();
                }
                this.port = null;
                ((POP3Store)this.store).closePort(this);
                throw new MessagingException("Open failed", ex);
            }
            catch (IOException ex2) {
                this.port = null;
                ((POP3Store)this.store).closePort(this);
            }
            finally {
                this.port = null;
                ((POP3Store)this.store).closePort(this);
            }
        }
    }
    
    @Override
    public boolean renameTo(final Folder folder) throws MessagingException {
        throw new MethodNotSupportedException("renameTo");
    }
}
