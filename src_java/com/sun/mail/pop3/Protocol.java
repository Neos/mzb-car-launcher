package com.sun.mail.pop3;

import java.net.*;
import java.util.*;
import java.security.*;
import java.io.*;
import com.sun.mail.util.*;

class Protocol
{
    private static final String CRLF = "\r\n";
    private static final int POP3_PORT = 110;
    private static char[] digits;
    private String apopChallenge;
    private boolean debug;
    private DataInputStream input;
    private PrintStream out;
    private PrintWriter output;
    private Socket socket;
    
    static {
        Protocol.digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    }
    
    Protocol(final String p0, final int p1, final boolean p2, final PrintStream p3, final Properties p4, final String p5, final boolean p6) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: aload_0        
        //     5: iconst_0       
        //     6: putfield        com/sun/mail/pop3/Protocol.debug:Z
        //     9: aload_0        
        //    10: aconst_null    
        //    11: putfield        com/sun/mail/pop3/Protocol.apopChallenge:Ljava/lang/String;
        //    14: aload_0        
        //    15: iload_3        
        //    16: putfield        com/sun/mail/pop3/Protocol.debug:Z
        //    19: aload_0        
        //    20: aload           4
        //    22: putfield        com/sun/mail/pop3/Protocol.out:Ljava/io/PrintStream;
        //    25: aload           5
        //    27: new             Ljava/lang/StringBuilder;
        //    30: dup            
        //    31: aload           6
        //    33: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    36: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    39: ldc             ".apop.enable"
        //    41: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    44: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    47: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    50: astore          10
        //    52: aload           10
        //    54: ifnull          232
        //    57: aload           10
        //    59: ldc             "true"
        //    61: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //    64: ifeq            232
        //    67: iconst_1       
        //    68: istore          8
        //    70: iload_2        
        //    71: istore          9
        //    73: iload_2        
        //    74: iconst_m1      
        //    75: if_icmpne       82
        //    78: bipush          110
        //    80: istore          9
        //    82: iload_3        
        //    83: ifeq            127
        //    86: aload           4
        //    88: new             Ljava/lang/StringBuilder;
        //    91: dup            
        //    92: ldc             "DEBUG POP3: connecting to host \""
        //    94: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    97: aload_1        
        //    98: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   101: ldc             "\", port "
        //   103: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   106: iload           9
        //   108: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   111: ldc             ", isSSL "
        //   113: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   116: iload           7
        //   118: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   121: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   124: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   127: aload_0        
        //   128: aload_1        
        //   129: iload           9
        //   131: aload           5
        //   133: aload           6
        //   135: iload           7
        //   137: invokestatic    com/sun/mail/util/SocketFetcher.getSocket:(Ljava/lang/String;ILjava/util/Properties;Ljava/lang/String;Z)Ljava/net/Socket;
        //   140: putfield        com/sun/mail/pop3/Protocol.socket:Ljava/net/Socket;
        //   143: aload_0        
        //   144: new             Ljava/io/DataInputStream;
        //   147: dup            
        //   148: new             Ljava/io/BufferedInputStream;
        //   151: dup            
        //   152: aload_0        
        //   153: getfield        com/sun/mail/pop3/Protocol.socket:Ljava/net/Socket;
        //   156: invokevirtual   java/net/Socket.getInputStream:()Ljava/io/InputStream;
        //   159: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   162: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
        //   165: putfield        com/sun/mail/pop3/Protocol.input:Ljava/io/DataInputStream;
        //   168: aload_0        
        //   169: new             Ljava/io/PrintWriter;
        //   172: dup            
        //   173: new             Ljava/io/BufferedWriter;
        //   176: dup            
        //   177: new             Ljava/io/OutputStreamWriter;
        //   180: dup            
        //   181: aload_0        
        //   182: getfield        com/sun/mail/pop3/Protocol.socket:Ljava/net/Socket;
        //   185: invokevirtual   java/net/Socket.getOutputStream:()Ljava/io/OutputStream;
        //   188: ldc             "iso-8859-1"
        //   190: invokespecial   java/io/OutputStreamWriter.<init>:(Ljava/io/OutputStream;Ljava/lang/String;)V
        //   193: invokespecial   java/io/BufferedWriter.<init>:(Ljava/io/Writer;)V
        //   196: invokespecial   java/io/PrintWriter.<init>:(Ljava/io/Writer;)V
        //   199: putfield        com/sun/mail/pop3/Protocol.output:Ljava/io/PrintWriter;
        //   202: aload_0        
        //   203: aconst_null    
        //   204: invokespecial   com/sun/mail/pop3/Protocol.simpleCommand:(Ljava/lang/String;)Lcom/sun/mail/pop3/Response;
        //   207: astore_1       
        //   208: aload_1        
        //   209: getfield        com/sun/mail/pop3/Response.ok:Z
        //   212: ifne            248
        //   215: aload_0        
        //   216: getfield        com/sun/mail/pop3/Protocol.socket:Ljava/net/Socket;
        //   219: invokevirtual   java/net/Socket.close:()V
        //   222: new             Ljava/io/IOException;
        //   225: dup            
        //   226: ldc             "Connect failed"
        //   228: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   231: athrow         
        //   232: iconst_0       
        //   233: istore          8
        //   235: goto            70
        //   238: astore_1       
        //   239: aload_0        
        //   240: getfield        com/sun/mail/pop3/Protocol.socket:Ljava/net/Socket;
        //   243: invokevirtual   java/net/Socket.close:()V
        //   246: aload_1        
        //   247: athrow         
        //   248: iload           8
        //   250: ifeq            330
        //   253: aload_1        
        //   254: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //   257: bipush          60
        //   259: invokevirtual   java/lang/String.indexOf:(I)I
        //   262: istore_2       
        //   263: aload_1        
        //   264: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //   267: bipush          62
        //   269: iload_2        
        //   270: invokevirtual   java/lang/String.indexOf:(II)I
        //   273: istore          8
        //   275: iload_2        
        //   276: iconst_m1      
        //   277: if_icmpeq       302
        //   280: iload           8
        //   282: iconst_m1      
        //   283: if_icmpeq       302
        //   286: aload_0        
        //   287: aload_1        
        //   288: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //   291: iload_2        
        //   292: iload           8
        //   294: iconst_1       
        //   295: iadd           
        //   296: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   299: putfield        com/sun/mail/pop3/Protocol.apopChallenge:Ljava/lang/String;
        //   302: iload_3        
        //   303: ifeq            330
        //   306: aload           4
        //   308: new             Ljava/lang/StringBuilder;
        //   311: dup            
        //   312: ldc             "DEBUG POP3: APOP challenge: "
        //   314: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   317: aload_0        
        //   318: getfield        com/sun/mail/pop3/Protocol.apopChallenge:Ljava/lang/String;
        //   321: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   324: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   327: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   330: return         
        //   331: astore_1       
        //   332: goto            222
        //   335: astore          4
        //   337: goto            246
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  86     127    238    248    Ljava/io/IOException;
        //  127    208    238    248    Ljava/io/IOException;
        //  215    222    331    335    Any
        //  239    246    335    340    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 162, Size: 162
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private String getDigest(String string) {
        string = String.valueOf(this.apopChallenge) + string;
        try {
            return toHex(MessageDigest.getInstance("MD5").digest(string.getBytes("iso-8859-1")));
        }
        catch (NoSuchAlgorithmException ex) {
            return null;
        }
        catch (UnsupportedEncodingException ex2) {
            return null;
        }
    }
    
    private Response multilineCommand(final String s, int n) throws IOException {
        final Response simpleCommand = this.simpleCommand(s);
        if (!simpleCommand.ok) {
            return simpleCommand;
        }
        final SharedByteArrayOutputStream sharedByteArrayOutputStream = new SharedByteArrayOutputStream(n);
        int n2 = 10;
        while (true) {
            int read = 0;
            Block_6: {
                int n3;
                while (true) {
                    n3 = this.input.read();
                    if (n3 < 0) {
                        break;
                    }
                    n = n3;
                    if (n2 == 10 && (n = n3) == 46) {
                        if (this.debug) {
                            this.out.write(n3);
                        }
                        read = this.input.read();
                        if ((n = read) == 13) {
                            break Block_6;
                        }
                    }
                    sharedByteArrayOutputStream.write(n);
                    if (this.debug) {
                        this.out.write(n);
                    }
                    n2 = n;
                }
                if (n3 < 0) {
                    throw new EOFException("EOF on socket");
                }
                simpleCommand.bytes = sharedByteArrayOutputStream.toStream();
                return simpleCommand;
            }
            if (this.debug) {
                this.out.write(read);
            }
            int n3;
            n = (n3 = this.input.read());
            if (this.debug) {
                this.out.write(n);
                n3 = n;
            }
            continue;
        }
    }
    
    private Response simpleCommand(String s) throws IOException {
        if (this.socket == null) {
            throw new IOException("Folder is closed");
        }
        if (s != null) {
            if (this.debug) {
                this.out.println("C: " + s);
            }
            s = String.valueOf(s) + "\r\n";
            this.output.print(s);
            this.output.flush();
        }
        s = this.input.readLine();
        if (s == null) {
            if (this.debug) {
                this.out.println("S: EOF");
            }
            throw new EOFException("EOF on socket");
        }
        if (this.debug) {
            this.out.println("S: " + s);
        }
        final Response response = new Response();
        if (s.startsWith("+OK")) {
            response.ok = true;
        }
        else {
            if (!s.startsWith("-ERR")) {
                throw new IOException("Unexpected response: " + s);
            }
            response.ok = false;
        }
        final int index = s.indexOf(32);
        if (index >= 0) {
            response.data = s.substring(index + 1);
        }
        return response;
    }
    
    private static String toHex(final byte[] array) {
        final char[] array2 = new char[array.length * 2];
        int i = 0;
        int n = 0;
        while (i < array.length) {
            final int n2 = array[i] & 0xFF;
            final int n3 = n + 1;
            array2[n] = Protocol.digits[n2 >> 4];
            n = n3 + 1;
            array2[n3] = Protocol.digits[n2 & 0xF];
            ++i;
        }
        return new String(array2);
    }
    
    boolean dele(final int n) throws IOException {
        synchronized (this) {
            return this.simpleCommand("DELE " + n).ok;
        }
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (this.socket != null) {
            this.quit();
        }
    }
    
    int list(final int p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: aload_0        
        //     3: new             Ljava/lang/StringBuilder;
        //     6: dup            
        //     7: ldc_w           "LIST "
        //    10: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    13: iload_1        
        //    14: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    17: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    20: invokespecial   com/sun/mail/pop3/Protocol.simpleCommand:(Ljava/lang/String;)Lcom/sun/mail/pop3/Response;
        //    23: astore_3       
        //    24: iconst_m1      
        //    25: istore_2       
        //    26: iload_2        
        //    27: istore_1       
        //    28: aload_3        
        //    29: getfield        com/sun/mail/pop3/Response.ok:Z
        //    32: ifeq            73
        //    35: aload_3        
        //    36: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //    39: astore          4
        //    41: iload_2        
        //    42: istore_1       
        //    43: aload           4
        //    45: ifnull          73
        //    48: new             Ljava/util/StringTokenizer;
        //    51: dup            
        //    52: aload_3        
        //    53: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //    56: invokespecial   java/util/StringTokenizer.<init>:(Ljava/lang/String;)V
        //    59: astore_3       
        //    60: aload_3        
        //    61: invokevirtual   java/util/StringTokenizer.nextToken:()Ljava/lang/String;
        //    64: pop            
        //    65: aload_3        
        //    66: invokevirtual   java/util/StringTokenizer.nextToken:()Ljava/lang/String;
        //    69: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    72: istore_1       
        //    73: aload_0        
        //    74: monitorexit    
        //    75: iload_1        
        //    76: ireturn        
        //    77: astore_3       
        //    78: aload_0        
        //    79: monitorexit    
        //    80: aload_3        
        //    81: athrow         
        //    82: astore_3       
        //    83: iload_2        
        //    84: istore_1       
        //    85: goto            73
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      24     77     82     Any
        //  28     41     77     82     Any
        //  48     73     82     88     Ljava/lang/Exception;
        //  48     73     77     82     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0073:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    InputStream list() throws IOException {
        synchronized (this) {
            return this.multilineCommand("LIST", 128).bytes;
        }
    }
    
    String login(String s, final String s2) throws IOException {
        while (true) {
            // monitorenter(this)
            String digest = null;
            try {
                if (this.apopChallenge != null) {
                    digest = this.getDigest(s2);
                }
                Response response;
                if (this.apopChallenge != null && digest != null) {
                    response = this.simpleCommand("APOP " + s + " " + digest);
                }
                else {
                    final Response simpleCommand = this.simpleCommand("USER " + s);
                    if (!simpleCommand.ok) {
                        if (simpleCommand.data != null) {
                            s = simpleCommand.data;
                            return s;
                        }
                        return "USER command failed";
                    }
                    else {
                        response = this.simpleCommand("PASS " + s2);
                    }
                }
                if (!response.ok) {
                    if (response.data != null) {
                        s = response.data;
                    }
                    else {
                        s = "login failed";
                    }
                }
                else {
                    s = null;
                }
                return s;
            }
            finally {
            }
            // monitorexit(this)
            s = "USER command failed";
            return s;
        }
    }
    
    boolean noop() throws IOException {
        synchronized (this) {
            return this.simpleCommand("NOOP").ok;
        }
    }
    
    boolean quit() throws IOException {
        // monitorenter(this)
        boolean ok;
        try {
            ok = this.simpleCommand("QUIT").ok;
            final Protocol protocol = this;
            final Socket socket = protocol.socket;
            socket.close();
            final Protocol protocol2 = this;
            final Socket socket2 = null;
            protocol2.socket = socket2;
            final Protocol protocol3 = this;
            final DataInputStream dataInputStream = null;
            protocol3.input = dataInputStream;
            final Protocol protocol4 = this;
            final PrintWriter printWriter = null;
            protocol4.output = printWriter;
            return ok;
        }
        finally {
            try {
                this.socket.close();
                this.socket = null;
                this.input = null;
                this.output = null;
            }
            finally {
                final Protocol protocol5 = this;
                final Socket socket3 = null;
                protocol5.socket = socket3;
                final Protocol protocol6 = this;
                final DataInputStream dataInputStream2 = null;
                protocol6.input = dataInputStream2;
                final Protocol protocol7 = this;
                final PrintWriter printWriter2 = null;
                protocol7.output = printWriter2;
            }
        }
        try {
            final Protocol protocol = this;
            final Socket socket = protocol.socket;
            socket.close();
            try {
                final Protocol protocol2 = this;
                final Socket socket2 = null;
                protocol2.socket = socket2;
                final Protocol protocol3 = this;
                final DataInputStream dataInputStream = null;
                protocol3.input = dataInputStream;
                final Protocol protocol4 = this;
                final PrintWriter printWriter = null;
                protocol4.output = printWriter;
                return ok;
            }
            finally {
            }
            // monitorexit(this)
            final Protocol protocol5 = this;
            final Socket socket3 = null;
            protocol5.socket = socket3;
            final Protocol protocol6 = this;
            final DataInputStream dataInputStream2 = null;
            protocol6.input = dataInputStream2;
            final Protocol protocol7 = this;
            final PrintWriter printWriter2 = null;
            protocol7.output = printWriter2;
        }
        finally {
            this.socket = null;
            this.input = null;
            this.output = null;
        }
    }
    
    InputStream retr(final int n, final int n2) throws IOException {
        synchronized (this) {
            return this.multilineCommand("RETR " + n, n2).bytes;
        }
    }
    
    boolean rset() throws IOException {
        synchronized (this) {
            return this.simpleCommand("RSET").ok;
        }
    }
    
    Status stat() throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: aload_0        
        //     3: ldc_w           "STAT"
        //     6: invokespecial   com/sun/mail/pop3/Protocol.simpleCommand:(Ljava/lang/String;)Lcom/sun/mail/pop3/Response;
        //     9: astore_2       
        //    10: new             Lcom/sun/mail/pop3/Status;
        //    13: dup            
        //    14: invokespecial   com/sun/mail/pop3/Status.<init>:()V
        //    17: astore_1       
        //    18: aload_2        
        //    19: getfield        com/sun/mail/pop3/Response.ok:Z
        //    22: ifeq            68
        //    25: aload_2        
        //    26: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //    29: astore_3       
        //    30: aload_3        
        //    31: ifnull          68
        //    34: new             Ljava/util/StringTokenizer;
        //    37: dup            
        //    38: aload_2        
        //    39: getfield        com/sun/mail/pop3/Response.data:Ljava/lang/String;
        //    42: invokespecial   java/util/StringTokenizer.<init>:(Ljava/lang/String;)V
        //    45: astore_2       
        //    46: aload_1        
        //    47: aload_2        
        //    48: invokevirtual   java/util/StringTokenizer.nextToken:()Ljava/lang/String;
        //    51: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    54: putfield        com/sun/mail/pop3/Status.total:I
        //    57: aload_1        
        //    58: aload_2        
        //    59: invokevirtual   java/util/StringTokenizer.nextToken:()Ljava/lang/String;
        //    62: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    65: putfield        com/sun/mail/pop3/Status.size:I
        //    68: aload_0        
        //    69: monitorexit    
        //    70: aload_1        
        //    71: areturn        
        //    72: astore_1       
        //    73: aload_0        
        //    74: monitorexit    
        //    75: aload_1        
        //    76: athrow         
        //    77: astore_2       
        //    78: goto            68
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      30     72     77     Any
        //  34     68     77     81     Ljava/lang/Exception;
        //  34     68     72     77     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0068:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    InputStream top(final int n, final int n2) throws IOException {
        synchronized (this) {
            return this.multilineCommand("TOP " + n + " " + n2, 0).bytes;
        }
    }
    
    String uidl(int index) throws IOException {
        String substring = null;
        synchronized (this) {
            final Response simpleCommand = this.simpleCommand("UIDL " + index);
            if (simpleCommand.ok) {
                index = simpleCommand.data.indexOf(32);
                if (index > 0) {
                    substring = simpleCommand.data.substring(index + 1);
                }
            }
            return substring;
        }
    }
    
    boolean uidl(final String[] array) throws IOException {
        boolean b = false;
        synchronized (this) {
            final Response multilineCommand = this.multilineCommand("UIDL", array.length * 15);
            if (multilineCommand.ok) {
                final LineInputStream lineInputStream = new LineInputStream(multilineCommand.bytes);
                while (true) {
                    final String line = lineInputStream.readLine();
                    if (line == null) {
                        break;
                    }
                    final int index = line.indexOf(32);
                    if (index < 1 || index >= line.length()) {
                        continue;
                    }
                    final int int1 = Integer.parseInt(line.substring(0, index));
                    if (int1 <= 0 || int1 > array.length) {
                        continue;
                    }
                    array[int1 - 1] = line.substring(index + 1);
                }
                b = true;
            }
            return b;
        }
    }
}
