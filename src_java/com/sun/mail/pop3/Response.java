package com.sun.mail.pop3;

import java.io.*;

class Response
{
    InputStream bytes;
    String data;
    boolean ok;
    
    Response() {
        this.ok = false;
        this.data = null;
        this.bytes = null;
    }
}
