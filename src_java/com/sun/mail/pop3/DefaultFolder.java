package com.sun.mail.pop3;

import javax.mail.*;

public class DefaultFolder extends Folder
{
    DefaultFolder(final POP3Store pop3Store) {
        super(pop3Store);
    }
    
    @Override
    public void appendMessages(final Message[] array) throws MessagingException {
        throw new MethodNotSupportedException("Append not supported");
    }
    
    @Override
    public void close(final boolean b) throws MessagingException {
        throw new MethodNotSupportedException("close");
    }
    
    @Override
    public boolean create(final int n) throws MessagingException {
        return false;
    }
    
    @Override
    public boolean delete(final boolean b) throws MessagingException {
        throw new MethodNotSupportedException("delete");
    }
    
    @Override
    public boolean exists() {
        return true;
    }
    
    @Override
    public Message[] expunge() throws MessagingException {
        throw new MethodNotSupportedException("expunge");
    }
    
    @Override
    public Folder getFolder(final String s) throws MessagingException {
        if (!s.equalsIgnoreCase("INBOX")) {
            throw new MessagingException("only INBOX supported");
        }
        return this.getInbox();
    }
    
    @Override
    public String getFullName() {
        return "";
    }
    
    protected Folder getInbox() throws MessagingException {
        return this.getStore().getFolder("INBOX");
    }
    
    @Override
    public Message getMessage(final int n) throws MessagingException {
        throw new MethodNotSupportedException("getMessage");
    }
    
    @Override
    public int getMessageCount() throws MessagingException {
        return 0;
    }
    
    @Override
    public String getName() {
        return "";
    }
    
    @Override
    public Folder getParent() {
        return null;
    }
    
    @Override
    public Flags getPermanentFlags() {
        return new Flags();
    }
    
    @Override
    public char getSeparator() {
        return '/';
    }
    
    @Override
    public int getType() {
        return 2;
    }
    
    @Override
    public boolean hasNewMessages() throws MessagingException {
        return false;
    }
    
    @Override
    public boolean isOpen() {
        return false;
    }
    
    @Override
    public Folder[] list(final String s) throws MessagingException {
        return new Folder[] { this.getInbox() };
    }
    
    @Override
    public void open(final int n) throws MessagingException {
        throw new MethodNotSupportedException("open");
    }
    
    @Override
    public boolean renameTo(final Folder folder) throws MessagingException {
        throw new MethodNotSupportedException("renameTo");
    }
}
