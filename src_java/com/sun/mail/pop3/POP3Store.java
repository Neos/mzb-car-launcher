package com.sun.mail.pop3;

import java.lang.reflect.*;
import javax.mail.*;
import java.io.*;

public class POP3Store extends Store
{
    private int defaultPort;
    boolean disableTop;
    boolean forgetTopHeaders;
    private String host;
    private boolean isSSL;
    Constructor messageConstructor;
    private String name;
    private String passwd;
    private Protocol port;
    private int portNum;
    private POP3Folder portOwner;
    boolean rsetBeforeQuit;
    private String user;
    
    public POP3Store(final Session session, final URLName urlName) {
        this(session, urlName, "pop3", 110, false);
    }
    
    public POP3Store(final Session session, final URLName urlName, String name, final int defaultPort, final boolean isSSL) {
        super(session, urlName);
        this.name = "pop3";
        this.defaultPort = 110;
        this.isSSL = false;
        this.port = null;
        this.portOwner = null;
        this.host = null;
        this.portNum = -1;
        this.user = null;
        this.passwd = null;
        this.rsetBeforeQuit = false;
        this.disableTop = false;
        this.forgetTopHeaders = false;
        this.messageConstructor = null;
        if (urlName != null) {
            name = urlName.getProtocol();
        }
        this.name = name;
        this.defaultPort = defaultPort;
        this.isSSL = isSSL;
        final String property = session.getProperty("mail." + name + ".rsetbeforequit");
        if (property != null && property.equalsIgnoreCase("true")) {
            this.rsetBeforeQuit = true;
        }
        final String property2 = session.getProperty("mail." + name + ".disabletop");
        if (property2 != null && property2.equalsIgnoreCase("true")) {
            this.disableTop = true;
        }
        final String property3 = session.getProperty("mail." + name + ".forgettopheaders");
        if (property3 != null && property3.equalsIgnoreCase("true")) {
            this.forgetTopHeaders = true;
        }
        name = session.getProperty("mail." + name + ".message.class");
        if (name != null) {
            if (session.getDebug()) {
                session.getDebugOut().println("DEBUG: POP3 message class: " + name);
            }
            try {
                final ClassLoader classLoader = this.getClass().getClassLoader();
                try {
                    final Class<?> clazz = classLoader.loadClass(name);
                    this.messageConstructor = clazz.getConstructor(Folder.class, Integer.TYPE);
                }
                catch (ClassNotFoundException ex2) {
                    final Class<?> clazz = Class.forName(name);
                }
            }
            catch (Exception ex) {
                if (session.getDebug()) {
                    session.getDebugOut().println("DEBUG: failed to load POP3 message class: " + ex);
                }
            }
        }
    }
    
    private void checkConnected() throws MessagingException {
        if (!super.isConnected()) {
            throw new MessagingException("Not connected");
        }
    }
    
    @Override
    public void close() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //     6: ifnull          17
        //     9: aload_0        
        //    10: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    13: invokevirtual   com/sun/mail/pop3/Protocol.quit:()Z
        //    16: pop            
        //    17: aload_0        
        //    18: aconst_null    
        //    19: putfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    22: aload_0        
        //    23: invokespecial   javax/mail/Store.close:()V
        //    26: aload_0        
        //    27: monitorexit    
        //    28: return         
        //    29: astore_1       
        //    30: aload_0        
        //    31: aconst_null    
        //    32: putfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    35: aload_0        
        //    36: invokespecial   javax/mail/Store.close:()V
        //    39: goto            26
        //    42: astore_1       
        //    43: aload_0        
        //    44: monitorexit    
        //    45: aload_1        
        //    46: athrow         
        //    47: astore_1       
        //    48: aload_0        
        //    49: aconst_null    
        //    50: putfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    53: aload_0        
        //    54: invokespecial   javax/mail/Store.close:()V
        //    57: aload_1        
        //    58: athrow         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      17     29     42     Ljava/io/IOException;
        //  2      17     47     59     Any
        //  17     26     42     47     Any
        //  30     39     42     47     Any
        //  48     59     42     47     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0017:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    void closePort(final POP3Folder pop3Folder) {
        synchronized (this) {
            if (this.portOwner == pop3Folder) {
                this.port = null;
                this.portOwner = null;
            }
        }
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (this.port != null) {
            this.close();
        }
    }
    
    @Override
    public Folder getDefaultFolder() throws MessagingException {
        this.checkConnected();
        return new DefaultFolder(this);
    }
    
    @Override
    public Folder getFolder(final String s) throws MessagingException {
        this.checkConnected();
        return new POP3Folder(this, s);
    }
    
    @Override
    public Folder getFolder(final URLName urlName) throws MessagingException {
        this.checkConnected();
        return new POP3Folder(this, urlName.getFile());
    }
    
    Protocol getPort(final POP3Folder p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: aload_0        
        //     3: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //     6: ifnull          30
        //     9: aload_0        
        //    10: getfield        com/sun/mail/pop3/POP3Store.portOwner:Lcom/sun/mail/pop3/POP3Folder;
        //    13: ifnonnull       30
        //    16: aload_0        
        //    17: aload_1        
        //    18: putfield        com/sun/mail/pop3/POP3Store.portOwner:Lcom/sun/mail/pop3/POP3Folder;
        //    21: aload_0        
        //    22: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    25: astore_2       
        //    26: aload_0        
        //    27: monitorexit    
        //    28: aload_2        
        //    29: areturn        
        //    30: new             Lcom/sun/mail/pop3/Protocol;
        //    33: dup            
        //    34: aload_0        
        //    35: getfield        com/sun/mail/pop3/POP3Store.host:Ljava/lang/String;
        //    38: aload_0        
        //    39: getfield        com/sun/mail/pop3/POP3Store.portNum:I
        //    42: aload_0        
        //    43: getfield        com/sun/mail/pop3/POP3Store.session:Ljavax/mail/Session;
        //    46: invokevirtual   javax/mail/Session.getDebug:()Z
        //    49: aload_0        
        //    50: getfield        com/sun/mail/pop3/POP3Store.session:Ljavax/mail/Session;
        //    53: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //    56: aload_0        
        //    57: getfield        com/sun/mail/pop3/POP3Store.session:Ljavax/mail/Session;
        //    60: invokevirtual   javax/mail/Session.getProperties:()Ljava/util/Properties;
        //    63: new             Ljava/lang/StringBuilder;
        //    66: dup            
        //    67: ldc             "mail."
        //    69: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    72: aload_0        
        //    73: getfield        com/sun/mail/pop3/POP3Store.name:Ljava/lang/String;
        //    76: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    79: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    82: aload_0        
        //    83: getfield        com/sun/mail/pop3/POP3Store.isSSL:Z
        //    86: invokespecial   com/sun/mail/pop3/Protocol.<init>:(Ljava/lang/String;IZLjava/io/PrintStream;Ljava/util/Properties;Ljava/lang/String;Z)V
        //    89: astore_3       
        //    90: aload_3        
        //    91: aload_0        
        //    92: getfield        com/sun/mail/pop3/POP3Store.user:Ljava/lang/String;
        //    95: aload_0        
        //    96: getfield        com/sun/mail/pop3/POP3Store.passwd:Ljava/lang/String;
        //    99: invokevirtual   com/sun/mail/pop3/Protocol.login:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   102: astore_2       
        //   103: aload_2        
        //   104: ifnull          126
        //   107: aload_3        
        //   108: invokevirtual   com/sun/mail/pop3/Protocol.quit:()Z
        //   111: pop            
        //   112: new             Ljava/io/EOFException;
        //   115: dup            
        //   116: aload_2        
        //   117: invokespecial   java/io/EOFException.<init>:(Ljava/lang/String;)V
        //   120: athrow         
        //   121: astore_1       
        //   122: aload_0        
        //   123: monitorexit    
        //   124: aload_1        
        //   125: athrow         
        //   126: aload_0        
        //   127: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //   130: ifnonnull       147
        //   133: aload_1        
        //   134: ifnull          147
        //   137: aload_0        
        //   138: aload_3        
        //   139: putfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //   142: aload_0        
        //   143: aload_1        
        //   144: putfield        com/sun/mail/pop3/POP3Store.portOwner:Lcom/sun/mail/pop3/POP3Folder;
        //   147: aload_3        
        //   148: astore_2       
        //   149: aload_0        
        //   150: getfield        com/sun/mail/pop3/POP3Store.portOwner:Lcom/sun/mail/pop3/POP3Folder;
        //   153: ifnonnull       26
        //   156: aload_0        
        //   157: aload_1        
        //   158: putfield        com/sun/mail/pop3/POP3Store.portOwner:Lcom/sun/mail/pop3/POP3Folder;
        //   161: aload_3        
        //   162: astore_2       
        //   163: goto            26
        //   166: astore_1       
        //   167: goto            112
        //   170: astore_1       
        //   171: goto            112
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      26     121    126    Any
        //  30     103    121    126    Any
        //  107    112    166    170    Ljava/io/IOException;
        //  107    112    170    174    Any
        //  112    121    121    126    Any
        //  126    133    121    126    Any
        //  137    147    121    126    Any
        //  149    161    121    126    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0112:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean isConnected() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_1       
        //     2: aload_0        
        //     3: monitorenter   
        //     4: aload_0        
        //     5: invokespecial   javax/mail/Store.isConnected:()Z
        //     8: istore_2       
        //     9: iload_2        
        //    10: ifne            17
        //    13: aload_0        
        //    14: monitorexit    
        //    15: iload_1        
        //    16: ireturn        
        //    17: aload_0        
        //    18: monitorenter   
        //    19: aload_0        
        //    20: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    23: ifnonnull       42
        //    26: aload_0        
        //    27: aload_0        
        //    28: aconst_null    
        //    29: invokevirtual   com/sun/mail/pop3/POP3Store.getPort:(Lcom/sun/mail/pop3/POP3Folder;)Lcom/sun/mail/pop3/Protocol;
        //    32: putfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    35: aload_0        
        //    36: monitorexit    
        //    37: iconst_1       
        //    38: istore_1       
        //    39: goto            13
        //    42: aload_0        
        //    43: getfield        com/sun/mail/pop3/POP3Store.port:Lcom/sun/mail/pop3/Protocol;
        //    46: invokevirtual   com/sun/mail/pop3/Protocol.noop:()Z
        //    49: pop            
        //    50: goto            35
        //    53: astore_3       
        //    54: aload_0        
        //    55: invokespecial   javax/mail/Store.close:()V
        //    58: aload_0        
        //    59: monitorexit    
        //    60: goto            13
        //    63: astore_3       
        //    64: aload_0        
        //    65: monitorexit    
        //    66: aload_3        
        //    67: athrow         
        //    68: astore_3       
        //    69: aload_0        
        //    70: monitorexit    
        //    71: aload_3        
        //    72: athrow         
        //    73: astore_3       
        //    74: goto            58
        //    77: astore_3       
        //    78: goto            58
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  4      9      68     73     Any
        //  17     19     68     73     Any
        //  19     35     53     81     Ljava/io/IOException;
        //  19     35     63     68     Any
        //  35     37     63     68     Any
        //  42     50     53     81     Ljava/io/IOException;
        //  42     50     63     68     Any
        //  54     58     73     77     Ljavax/mail/MessagingException;
        //  54     58     77     81     Any
        //  58     60     63     68     Any
        //  64     66     63     68     Any
        //  66     68     68     73     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0058:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected boolean protocolConnect(final String host, int defaultPort, final String user, final String passwd) throws MessagingException {
        // monitorenter(this)
        boolean b = false;
        if (host == null || passwd == null || user == null) {
            b = false;
        }
        else {
            Label_0079: {
                int int1;
                if ((int1 = defaultPort) != -1) {
                    break Label_0079;
                }
                try {
                    final String property = this.session.getProperty("mail." + this.name + ".port");
                    int1 = defaultPort;
                    if (property != null) {
                        int1 = Integer.parseInt(property);
                    }
                    if ((defaultPort = int1) == -1) {
                        defaultPort = this.defaultPort;
                    }
                    this.host = host;
                    this.portNum = defaultPort;
                    this.user = user;
                    this.passwd = passwd;
                    try {
                        this.port = this.getPort(null);
                        b = true;
                    }
                    catch (EOFException ex) {
                        throw new AuthenticationFailedException(ex.getMessage());
                    }
                    catch (IOException ex2) {
                        throw new MessagingException("Connect failed", ex2);
                    }
                }
                finally {}
            }
        }
        // monitorexit(this)
        return b;
    }
}
