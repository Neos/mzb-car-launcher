package com.sun.mail.imap;

import java.io.*;
import javax.mail.*;
import java.util.*;
import com.sun.mail.iap.*;
import com.sun.mail.imap.protocol.*;
import javax.mail.search.*;

public class IMAPFolder extends Folder implements UIDFolder, ResponseHandler
{
    private static final int ABORTING = 2;
    private static final int IDLE = 1;
    private static final int RUNNING = 0;
    protected static final char UNKNOWN_SEPARATOR = '\uffff';
    protected String[] attributes;
    protected Flags availableFlags;
    private Status cachedStatus;
    private long cachedStatusTime;
    private boolean connectionPoolDebug;
    private boolean debug;
    private boolean doExpungeNotification;
    protected boolean exists;
    protected String fullName;
    private int idleState;
    protected boolean isNamespace;
    protected Vector messageCache;
    protected Object messageCacheLock;
    protected String name;
    private boolean opened;
    private PrintStream out;
    protected Flags permanentFlags;
    protected IMAPProtocol protocol;
    private int realTotal;
    private boolean reallyClosed;
    private int recent;
    protected char separator;
    private int total;
    protected int type;
    protected Hashtable uidTable;
    private long uidnext;
    private long uidvalidity;
    
    protected IMAPFolder(final ListInfo listInfo, final IMAPStore imapStore) {
        this(listInfo.name, listInfo.separator, imapStore);
        if (listInfo.hasInferiors) {
            this.type |= 0x2;
        }
        if (listInfo.canOpen) {
            this.type |= 0x1;
        }
        this.exists = true;
        this.attributes = listInfo.attrs;
    }
    
    protected IMAPFolder(final String fullName, final char separator, final IMAPStore imapStore) {
        super(imapStore);
        this.exists = false;
        this.isNamespace = false;
        this.opened = false;
        this.reallyClosed = true;
        this.idleState = 0;
        this.total = -1;
        this.recent = -1;
        this.realTotal = -1;
        this.uidvalidity = -1L;
        this.uidnext = -1L;
        this.doExpungeNotification = true;
        this.cachedStatus = null;
        this.cachedStatusTime = 0L;
        this.debug = false;
        if (fullName == null) {
            throw new NullPointerException("Folder name is null");
        }
        this.fullName = fullName;
        this.separator = separator;
        this.messageCacheLock = new Object();
        this.debug = imapStore.getSession().getDebug();
        this.connectionPoolDebug = imapStore.getConnectionPoolDebug();
        this.out = imapStore.getSession().getDebugOut();
        if (this.out == null) {
            this.out = System.out;
        }
        this.isNamespace = false;
        if (separator != '\uffff' && separator != '\0') {
            final int index = this.fullName.indexOf(separator);
            if (index > 0 && index == this.fullName.length() - 1) {
                this.fullName = this.fullName.substring(0, index);
                this.isNamespace = true;
            }
        }
    }
    
    protected IMAPFolder(final String s, final char c, final IMAPStore imapStore, final boolean isNamespace) {
        this(s, c, imapStore);
        this.isNamespace = isNamespace;
    }
    
    private void checkClosed() {
        if (this.opened) {
            throw new IllegalStateException("This operation is not allowed on an open folder");
        }
    }
    
    private void checkExists() throws MessagingException {
        if (!this.exists && !this.exists()) {
            throw new FolderNotFoundException(this, String.valueOf(this.fullName) + " not found");
        }
    }
    
    private void checkFlags(final Flags flags) throws MessagingException {
        assert Thread.holdsLock(this);
        if (this.mode != 2) {
            throw new IllegalStateException("Cannot change flags on READ_ONLY folder: " + this.fullName);
        }
    }
    
    private void checkOpened() throws FolderClosedException {
        assert Thread.holdsLock(this);
        if (this.opened) {
            return;
        }
        if (this.reallyClosed) {
            throw new IllegalStateException("This operation is not allowed on a closed folder");
        }
        throw new FolderClosedException(this, "Lost folder connection to server");
    }
    
    private void checkRange(final int n) throws MessagingException {
        if (n < 1) {
            throw new IndexOutOfBoundsException();
        }
        if (n > this.total) {
            final Object messageCacheLock = this.messageCacheLock;
            // monitorenter(messageCacheLock)
            try {
                this.keepConnectionAlive(false);
                // monitorexit(messageCacheLock)
                if (n > this.total) {
                    throw new IndexOutOfBoundsException();
                }
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this, ex.getMessage());
                try {}
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
    }
    
    private void cleanup(final boolean b) {
        this.releaseProtocol(b);
        this.protocol = null;
        this.messageCache = null;
        this.uidTable = null;
        this.exists = false;
        this.attributes = null;
        this.opened = false;
        this.idleState = 0;
        this.notifyConnectionListeners(3);
    }
    
    private void close(final boolean b, final boolean b2) throws MessagingException {
        assert Thread.holdsLock(this);
        synchronized (this.messageCacheLock) {
            if (!this.opened && this.reallyClosed) {
                throw new IllegalStateException("This operation is not allowed on a closed folder");
            }
        }
        this.reallyClosed = true;
        if (!this.opened) {
            // monitorexit(o)
            return;
        }
        while (true) {
        Label_0275_Outer:
            while (true) {
                Label_0249: {
                    try {
                        this.waitIfIdle();
                        if (b2) {
                            if (this.debug) {
                                this.out.println("DEBUG: forcing folder " + this.fullName + " to close");
                            }
                            if (this.protocol != null) {
                                this.protocol.disconnect();
                            }
                        }
                        else {
                            if (!((IMAPStore)this.store).isConnectionPoolFull()) {
                                break Label_0249;
                            }
                            if (this.debug) {
                                this.out.println("DEBUG: pool is full, not adding an Authenticated connection");
                            }
                            if (b) {
                                this.protocol.close();
                            }
                            if (this.protocol != null) {
                                this.protocol.logout();
                            }
                        }
                        if (this.opened) {
                            this.cleanup(true);
                        }
                        // monitorexit(o)
                        return;
                    }
                    catch (ProtocolException ex) {
                        throw new MessagingException(ex.getMessage(), ex);
                    }
                    finally {
                        if (this.opened) {
                            this.cleanup(true);
                        }
                    }
                }
                while (true) {
                    if (b || this.mode != 2) {
                        break Label_0275;
                    }
                    try {
                        this.protocol.examine(this.fullName);
                        if (this.protocol != null) {
                            this.protocol.close();
                            continue Label_0275_Outer;
                        }
                        continue Label_0275_Outer;
                    }
                    catch (ProtocolException ex2) {
                        if (this.protocol != null) {
                            this.protocol.disconnect();
                        }
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    private Folder[] doList(final String s, final boolean b) throws MessagingException {
        synchronized (this) {
            this.checkExists();
            Folder[] array;
            if (!this.isDirectory()) {
                array = new Folder[0];
            }
            else {
                final char separator = this.getSeparator();
                final ListInfo[] array2 = (ListInfo[])this.doCommandIgnoreFailure((ProtocolCommand)new ProtocolCommand() {
                    @Override
                    public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                        if (b) {
                            return imapProtocol.lsub("", String.valueOf(IMAPFolder.this.fullName) + separator + s);
                        }
                        return imapProtocol.list("", String.valueOf(IMAPFolder.this.fullName) + separator + s);
                    }
                });
                if (array2 == null) {
                    array = new Folder[0];
                }
                else {
                    int n2;
                    final int n = n2 = 0;
                    if (array2.length > 0) {
                        n2 = n;
                        if (array2[0].name.equals(String.valueOf(this.fullName) + separator)) {
                            n2 = 1;
                        }
                    }
                    final IMAPFolder[] array3 = new IMAPFolder[array2.length - n2];
                    int n3 = n2;
                    while (true) {
                        array = array3;
                        if (n3 >= array2.length) {
                            break;
                        }
                        array3[n3 - n2] = new IMAPFolder(array2[n3], (IMAPStore)this.store);
                        ++n3;
                    }
                }
            }
            return array;
        }
    }
    
    private int findName(final ListInfo[] array, final String s) {
        int n;
        for (n = 0; n < array.length && !array[n].name.equals(s); ++n) {}
        int n2;
        if ((n2 = n) >= array.length) {
            n2 = 0;
        }
        return n2;
    }
    
    private IMAPProtocol getProtocol() throws ProtocolException {
        assert Thread.holdsLock(this.messageCacheLock);
        this.waitIfIdle();
        return this.protocol;
    }
    
    private Status getStatus() throws ProtocolException {
        final int statusCacheTimeout = ((IMAPStore)this.store).getStatusCacheTimeout();
        if (statusCacheTimeout > 0 && this.cachedStatus != null && System.currentTimeMillis() - this.cachedStatusTime < statusCacheTimeout) {
            return this.cachedStatus;
        }
        IMAPProtocol storeProtocol = null;
        try {
            final IMAPProtocol imapProtocol = storeProtocol = this.getStoreProtocol();
            final Status status = imapProtocol.status(this.fullName, null);
            if (statusCacheTimeout > 0) {
                storeProtocol = imapProtocol;
                this.cachedStatus = status;
                storeProtocol = imapProtocol;
                this.cachedStatusTime = System.currentTimeMillis();
            }
            return status;
        }
        finally {
            this.releaseStoreProtocol(storeProtocol);
        }
    }
    
    private boolean isDirectory() {
        return (this.type & 0x2) != 0x0;
    }
    
    private void keepConnectionAlive(final boolean b) throws ProtocolException {
        if (System.currentTimeMillis() - this.protocol.getTimestamp() > 1000L) {
            this.waitIfIdle();
            this.protocol.noop();
        }
        if (!b || !((IMAPStore)this.store).hasSeparateStoreConnection()) {
            return;
        }
        IMAPProtocol storeProtocol = null;
        try {
            final IMAPProtocol imapProtocol = storeProtocol = ((IMAPStore)this.store).getStoreProtocol();
            if (System.currentTimeMillis() - imapProtocol.getTimestamp() > 1000L) {
                storeProtocol = imapProtocol;
                imapProtocol.noop();
            }
        }
        finally {
            ((IMAPStore)this.store).releaseStoreProtocol(storeProtocol);
        }
    }
    
    private void releaseProtocol(final boolean b) {
        if (this.protocol != null) {
            this.protocol.removeResponseHandler(this);
            if (!b) {
                ((IMAPStore)this.store).releaseProtocol(this, null);
                return;
            }
            ((IMAPStore)this.store).releaseProtocol(this, this.protocol);
        }
    }
    
    private void setACL(final ACL acl, final char c) throws MessagingException {
        this.doOptionalCommand("ACL not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                imapProtocol.setACL(IMAPFolder.this.fullName, c, acl);
                return null;
            }
        });
    }
    
    private void throwClosedException(final ConnectionException ex) throws FolderClosedException, StoreClosedException {
        synchronized (this) {
            if ((this.protocol != null && ex.getProtocol() == this.protocol) || (this.protocol == null && !this.reallyClosed)) {
                throw new FolderClosedException(this, ex.getMessage());
            }
        }
        final Throwable t;
        throw new StoreClosedException(this.store, t.getMessage());
    }
    
    public void addACL(final ACL acl) throws MessagingException {
        this.setACL(acl, '\0');
    }
    
    public Message[] addMessages(final Message[] p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/sun/mail/imap/IMAPFolder.checkOpened:()V
        //     6: aload_1        
        //     7: arraylength    
        //     8: anewarray       Ljavax/mail/internet/MimeMessage;
        //    11: astore          8
        //    13: aload_0        
        //    14: aload_1        
        //    15: invokevirtual   com/sun/mail/imap/IMAPFolder.appendUIDMessages:([Ljavax/mail/Message;)[Lcom/sun/mail/imap/AppendUID;
        //    18: astore_1       
        //    19: iconst_0       
        //    20: istore_2       
        //    21: aload_1        
        //    22: arraylength    
        //    23: istore_3       
        //    24: iload_2        
        //    25: iload_3        
        //    26: if_icmplt       34
        //    29: aload_0        
        //    30: monitorexit    
        //    31: aload           8
        //    33: areturn        
        //    34: aload_1        
        //    35: iload_2        
        //    36: aaload         
        //    37: astore          9
        //    39: aload           9
        //    41: ifnull          78
        //    44: aload           9
        //    46: getfield        com/sun/mail/imap/AppendUID.uidvalidity:J
        //    49: lstore          4
        //    51: aload_0        
        //    52: getfield        com/sun/mail/imap/IMAPFolder.uidvalidity:J
        //    55: lstore          6
        //    57: lload           4
        //    59: lload           6
        //    61: lcmp           
        //    62: ifne            78
        //    65: aload           8
        //    67: iload_2        
        //    68: aload_0        
        //    69: aload           9
        //    71: getfield        com/sun/mail/imap/AppendUID.uid:J
        //    74: invokevirtual   com/sun/mail/imap/IMAPFolder.getMessageByUID:(J)Ljavax/mail/Message;
        //    77: aastore        
        //    78: iload_2        
        //    79: iconst_1       
        //    80: iadd           
        //    81: istore_2       
        //    82: goto            21
        //    85: astore_1       
        //    86: aload_0        
        //    87: monitorexit    
        //    88: aload_1        
        //    89: athrow         
        //    90: astore          9
        //    92: goto            78
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  2      19     85     90     Any
        //  21     24     85     90     Any
        //  44     57     85     90     Any
        //  65     78     90     95     Ljavax/mail/MessagingException;
        //  65     78     85     90     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void addRights(final ACL acl) throws MessagingException {
        this.setACL(acl, '+');
    }
    
    @Override
    public void appendMessages(final Message[] p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/sun/mail/imap/IMAPFolder.checkExists:()V
        //     6: aload_0        
        //     7: getfield        com/sun/mail/imap/IMAPFolder.store:Ljavax/mail/Store;
        //    10: checkcast       Lcom/sun/mail/imap/IMAPStore;
        //    13: invokevirtual   com/sun/mail/imap/IMAPStore.getAppendBufferSize:()I
        //    16: istore          4
        //    18: iconst_0       
        //    19: istore_2       
        //    20: aload_1        
        //    21: arraylength    
        //    22: istore_3       
        //    23: iload_2        
        //    24: iload_3        
        //    25: if_icmplt       31
        //    28: aload_0        
        //    29: monitorexit    
        //    30: return         
        //    31: aload_1        
        //    32: iload_2        
        //    33: aaload         
        //    34: astore          7
        //    36: aload           7
        //    38: invokevirtual   javax/mail/Message.getSize:()I
        //    41: iload           4
        //    43: if_icmple       135
        //    46: iconst_0       
        //    47: istore_3       
        //    48: new             Lcom/sun/mail/imap/MessageLiteral;
        //    51: dup            
        //    52: aload           7
        //    54: iload_3        
        //    55: invokespecial   com/sun/mail/imap/MessageLiteral.<init>:(Ljavax/mail/Message;I)V
        //    58: astore          8
        //    60: aload           7
        //    62: invokevirtual   javax/mail/Message.getReceivedDate:()Ljava/util/Date;
        //    65: astore          6
        //    67: aload           6
        //    69: astore          5
        //    71: aload           6
        //    73: ifnonnull       83
        //    76: aload           7
        //    78: invokevirtual   javax/mail/Message.getSentDate:()Ljava/util/Date;
        //    81: astore          5
        //    83: aload_0        
        //    84: new             Lcom/sun/mail/imap/IMAPFolder$10;
        //    87: dup            
        //    88: aload_0        
        //    89: aload           7
        //    91: invokevirtual   javax/mail/Message.getFlags:()Ljavax/mail/Flags;
        //    94: aload           5
        //    96: aload           8
        //    98: invokespecial   com/sun/mail/imap/IMAPFolder$10.<init>:(Lcom/sun/mail/imap/IMAPFolder;Ljavax/mail/Flags;Ljava/util/Date;Lcom/sun/mail/imap/MessageLiteral;)V
        //   101: invokevirtual   com/sun/mail/imap/IMAPFolder.doCommand:(Lcom/sun/mail/imap/IMAPFolder$ProtocolCommand;)Ljava/lang/Object;
        //   104: pop            
        //   105: goto            128
        //   108: astore_1       
        //   109: new             Ljavax/mail/MessagingException;
        //   112: dup            
        //   113: ldc_w           "IOException while appending messages"
        //   116: aload_1        
        //   117: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   120: athrow         
        //   121: astore_1       
        //   122: aload_0        
        //   123: monitorexit    
        //   124: aload_1        
        //   125: athrow         
        //   126: astore          5
        //   128: iload_2        
        //   129: iconst_1       
        //   130: iadd           
        //   131: istore_2       
        //   132: goto            20
        //   135: iload           4
        //   137: istore_3       
        //   138: goto            48
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      18     121    126    Any
        //  20     23     121    126    Any
        //  36     46     108    121    Ljava/io/IOException;
        //  36     46     126    128    Ljavax/mail/MessageRemovedException;
        //  36     46     121    126    Any
        //  48     60     108    121    Ljava/io/IOException;
        //  48     60     126    128    Ljavax/mail/MessageRemovedException;
        //  48     60     121    126    Any
        //  60     67     121    126    Any
        //  76     83     121    126    Any
        //  83     105    121    126    Any
        //  109    121    121    126    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public AppendUID[] appendUIDMessages(final Message[] p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/sun/mail/imap/IMAPFolder.checkExists:()V
        //     6: aload_0        
        //     7: getfield        com/sun/mail/imap/IMAPFolder.store:Ljavax/mail/Store;
        //    10: checkcast       Lcom/sun/mail/imap/IMAPStore;
        //    13: invokevirtual   com/sun/mail/imap/IMAPStore.getAppendBufferSize:()I
        //    16: istore          4
        //    18: aload_1        
        //    19: arraylength    
        //    20: anewarray       Lcom/sun/mail/imap/AppendUID;
        //    23: astore          7
        //    25: iconst_0       
        //    26: istore_2       
        //    27: aload_1        
        //    28: arraylength    
        //    29: istore_3       
        //    30: iload_2        
        //    31: iload_3        
        //    32: if_icmplt       40
        //    35: aload_0        
        //    36: monitorexit    
        //    37: aload           7
        //    39: areturn        
        //    40: aload_1        
        //    41: iload_2        
        //    42: aaload         
        //    43: astore          8
        //    45: aload           8
        //    47: invokevirtual   javax/mail/Message.getSize:()I
        //    50: iload           4
        //    52: if_icmple       150
        //    55: iconst_0       
        //    56: istore_3       
        //    57: new             Lcom/sun/mail/imap/MessageLiteral;
        //    60: dup            
        //    61: aload           8
        //    63: iload_3        
        //    64: invokespecial   com/sun/mail/imap/MessageLiteral.<init>:(Ljavax/mail/Message;I)V
        //    67: astore          9
        //    69: aload           8
        //    71: invokevirtual   javax/mail/Message.getReceivedDate:()Ljava/util/Date;
        //    74: astore          6
        //    76: aload           6
        //    78: astore          5
        //    80: aload           6
        //    82: ifnonnull       92
        //    85: aload           8
        //    87: invokevirtual   javax/mail/Message.getSentDate:()Ljava/util/Date;
        //    90: astore          5
        //    92: aload           7
        //    94: iload_2        
        //    95: aload_0        
        //    96: new             Lcom/sun/mail/imap/IMAPFolder$11;
        //    99: dup            
        //   100: aload_0        
        //   101: aload           8
        //   103: invokevirtual   javax/mail/Message.getFlags:()Ljavax/mail/Flags;
        //   106: aload           5
        //   108: aload           9
        //   110: invokespecial   com/sun/mail/imap/IMAPFolder$11.<init>:(Lcom/sun/mail/imap/IMAPFolder;Ljavax/mail/Flags;Ljava/util/Date;Lcom/sun/mail/imap/MessageLiteral;)V
        //   113: invokevirtual   com/sun/mail/imap/IMAPFolder.doCommand:(Lcom/sun/mail/imap/IMAPFolder$ProtocolCommand;)Ljava/lang/Object;
        //   116: checkcast       Lcom/sun/mail/imap/AppendUID;
        //   119: aastore        
        //   120: goto            143
        //   123: astore_1       
        //   124: new             Ljavax/mail/MessagingException;
        //   127: dup            
        //   128: ldc_w           "IOException while appending messages"
        //   131: aload_1        
        //   132: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   135: athrow         
        //   136: astore_1       
        //   137: aload_0        
        //   138: monitorexit    
        //   139: aload_1        
        //   140: athrow         
        //   141: astore          5
        //   143: iload_2        
        //   144: iconst_1       
        //   145: iadd           
        //   146: istore_2       
        //   147: goto            27
        //   150: iload           4
        //   152: istore_3       
        //   153: goto            57
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      25     136    141    Any
        //  27     30     136    141    Any
        //  45     55     123    136    Ljava/io/IOException;
        //  45     55     141    143    Ljavax/mail/MessageRemovedException;
        //  45     55     136    141    Any
        //  57     69     123    136    Ljava/io/IOException;
        //  57     69     141    143    Ljavax/mail/MessageRemovedException;
        //  57     69     136    141    Any
        //  69     76     136    141    Any
        //  85     92     136    141    Any
        //  92     120    136    141    Any
        //  124    136    136    141    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void close(final boolean b) throws MessagingException {
        synchronized (this) {
            this.close(b, false);
        }
    }
    
    @Override
    public void copyMessages(final Message[] array, final Folder folder) throws MessagingException {
        while (true) {
            // monitorenter(this)
            try {
                this.checkOpened();
                if (array.length == 0) {
                    return;
                }
                if (folder.getStore() == this.store) {
                    final Object messageCacheLock = this.messageCacheLock;
                    // monitorenter(messageCacheLock)
                    try {
                        this.getProtocol();
                        if (Utility.toMessageSet(array, null) == null) {
                            throw new MessageRemovedException("Messages have been removed");
                        }
                        goto Label_0121;
                    }
                    catch (CommandFailedException ex) {
                        try {
                            if (ex.getMessage().indexOf("TRYCREATE") != -1) {
                                throw new FolderNotFoundException(folder, String.valueOf(folder.getFullName()) + " does not exist");
                            }
                            goto Label_0137;
                        }
                        finally {
                        }
                        // monitorexit(messageCacheLock)
                    }
                    catch (ConnectionException ex2) {
                        throw new FolderClosedException(this, ex2.getMessage());
                    }
                    catch (ProtocolException ex3) {
                        throw new MessagingException(ex3.getMessage(), ex3);
                    }
                }
            }
            finally {}
            final Message[] array2;
            super.copyMessages(array2, folder);
        }
    }
    
    @Override
    public boolean create(final int n) throws MessagingException {
        // monitorenter(this)
        char separator = '\0';
        Label_0015: {
            if ((n & 0x1) != 0x0) {
                break Label_0015;
            }
            try {
                separator = this.getSeparator();
                boolean exists;
                if (this.doCommandIgnoreFailure((ProtocolCommand)new ProtocolCommand() {
                    @Override
                    public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                        if ((n & 0x1) == 0x0) {
                            imapProtocol.create(String.valueOf(IMAPFolder.this.fullName) + separator);
                        }
                        else {
                            imapProtocol.create(IMAPFolder.this.fullName);
                            if ((n & 0x2) != 0x0) {
                                final ListInfo[] list = imapProtocol.list("", IMAPFolder.this.fullName);
                                if (list != null && !list[0].hasInferiors) {
                                    imapProtocol.delete(IMAPFolder.this.fullName);
                                    throw new ProtocolException("Unsupported type");
                                }
                            }
                        }
                        return Boolean.TRUE;
                    }
                }) == null) {
                    exists = false;
                }
                else {
                    final boolean b = exists = this.exists();
                    if (b) {
                        this.notifyFolderListeners(1);
                        exists = b;
                    }
                }
                return exists;
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    @Override
    public boolean delete(final boolean b) throws MessagingException {
        final boolean b2 = false;
        synchronized (this) {
            this.checkClosed();
            if (b) {
                final Folder[] list = this.list();
                for (int i = 0; i < list.length; ++i) {
                    list[i].delete(b);
                }
            }
            boolean b3;
            if (this.doCommandIgnoreFailure((ProtocolCommand)new ProtocolCommand() {
                @Override
                public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                    imapProtocol.delete(IMAPFolder.this.fullName);
                    return Boolean.TRUE;
                }
            }) == null) {
                b3 = b2;
            }
            else {
                this.exists = false;
                this.attributes = null;
                this.notifyFolderListeners(2);
                b3 = true;
            }
            return b3;
        }
    }
    
    public Object doCommand(final ProtocolCommand protocolCommand) throws MessagingException {
        try {
            return this.doProtocolCommand(protocolCommand);
        }
        catch (ConnectionException ex) {
            this.throwClosedException(ex);
            return null;
        }
        catch (ProtocolException ex2) {
            throw new MessagingException(ex2.getMessage(), ex2);
        }
    }
    
    public Object doCommandIgnoreFailure(final ProtocolCommand protocolCommand) throws MessagingException {
        try {
            return this.doProtocolCommand(protocolCommand);
        }
        catch (CommandFailedException ex3) {
            return null;
        }
        catch (ConnectionException ex) {
            this.throwClosedException(ex);
            return null;
        }
        catch (ProtocolException ex2) {
            throw new MessagingException(ex2.getMessage(), ex2);
        }
    }
    
    public Object doOptionalCommand(final String s, final ProtocolCommand protocolCommand) throws MessagingException {
        try {
            return this.doProtocolCommand(protocolCommand);
        }
        catch (BadCommandException ex) {
            throw new MessagingException(s, ex);
        }
        catch (ConnectionException ex2) {
            this.throwClosedException(ex2);
            return null;
        }
        catch (ProtocolException ex3) {
            throw new MessagingException(ex3.getMessage(), ex3);
        }
    }
    
    protected Object doProtocolCommand(final ProtocolCommand protocolCommand) throws ProtocolException {
        synchronized (this) {
            if (this.opened && !((IMAPStore)this.store).hasSeparateStoreConnection()) {
                synchronized (this.messageCacheLock) {
                    return protocolCommand.doCommand(this.getProtocol());
                }
            }
        }
        // monitorexit(this)
        IMAPProtocol storeProtocol = null;
        try {
            final ProtocolCommand protocolCommand2;
            return protocolCommand2.doCommand(storeProtocol = this.getStoreProtocol());
        }
        finally {
            this.releaseStoreProtocol(storeProtocol);
        }
    }
    
    @Override
    public boolean exists() throws MessagingException {
        synchronized (this) {
            final ListInfo[] array = null;
            String s;
            if (this.isNamespace && this.separator != '\0') {
                s = String.valueOf(this.fullName) + this.separator;
            }
            else {
                s = this.fullName;
            }
            final ListInfo[] array2 = (ListInfo[])this.doCommand((ProtocolCommand)new ProtocolCommand() {
                @Override
                public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                    return imapProtocol.list("", s);
                }
            });
            if (array2 != null) {
                final int name = this.findName(array2, s);
                this.fullName = array2[name].name;
                this.separator = array2[name].separator;
                final int length = this.fullName.length();
                if (this.separator != '\0' && length > 0 && this.fullName.charAt(length - 1) == this.separator) {
                    this.fullName = this.fullName.substring(0, length - 1);
                }
                this.type = 0;
                if (array2[name].hasInferiors) {
                    this.type |= 0x2;
                }
                if (array2[name].canOpen) {
                    this.type |= 0x1;
                }
                this.exists = true;
                this.attributes = array2[name].attrs;
            }
            else {
                this.exists = this.opened;
                this.attributes = null;
            }
            return this.exists;
        }
    }
    
    @Override
    public Message[] expunge() throws MessagingException {
        synchronized (this) {
            return this.expunge(null);
        }
    }
    
    public Message[] expunge(Message[] array) throws MessagingException {
        while (true) {
            // monitorenter(this)
            while (true) {
                Vector<IMAPMessage> vector;
                int n;
                try {
                    this.checkOpened();
                    vector = new Vector<IMAPMessage>();
                    if (array != null) {
                        final Object messageCacheLock = new FetchProfile();
                        ((FetchProfile)messageCacheLock).add((FetchProfile.Item)UIDFolder.FetchProfileItem.UID);
                        this.fetch(array, (FetchProfile)messageCacheLock);
                    }
                    final Object messageCacheLock = this.messageCacheLock;
                    // monitorenter(messageCacheLock)
                    try {
                        this.doExpungeNotification = false;
                        try {
                            final IMAPProtocol protocol = this.getProtocol();
                            if (array != null) {
                                protocol.uidexpunge(Utility.toUIDSet(array));
                            }
                            else {
                                protocol.expunge();
                            }
                            this.doExpungeNotification = true;
                            n = 0;
                            if (n >= this.messageCache.size()) {
                                // monitorexit(messageCacheLock)
                                this.total = this.messageCache.size();
                                array = new Message[vector.size()];
                                vector.copyInto(array);
                                if (array.length > 0) {
                                    this.notifyMessageRemovedListeners(true, array);
                                }
                                return array;
                            }
                        }
                        catch (CommandFailedException ex4) {
                            if (this.mode != 2) {
                                throw new IllegalStateException("Cannot expunge READ_ONLY folder: " + this.fullName);
                            }
                            final Exception ex;
                            throw new MessagingException(ex.getMessage(), ex);
                        }
                        catch (ConnectionException ex5) {
                            final ConnectionException ex2;
                            throw new FolderClosedException(this, ex2.getMessage());
                        }
                        catch (ProtocolException ex3) {
                            throw new MessagingException(ex3.getMessage(), ex3);
                        }
                    }
                    finally {}
                }
                finally {}
                final IMAPMessage imapMessage = this.messageCache.elementAt(n);
                if (!imapMessage.isExpunged()) {
                    imapMessage.setMessageNumber(imapMessage.getSequenceNumber());
                    ++n;
                    continue;
                }
                vector.addElement(imapMessage);
                this.messageCache.removeElementAt(n);
                if (this.uidTable == null) {
                    continue;
                }
                final long uid = imapMessage.getUID();
                if (uid != -1L) {
                    this.uidTable.remove(new Long(uid));
                    continue;
                }
                continue;
            }
        }
    }
    
    @Override
    public void fetch(final Message[] array, final FetchProfile fetchProfile) throws MessagingException {
        synchronized (this) {
            this.checkOpened();
            IMAPMessage.fetch(this, array, fetchProfile);
        }
    }
    
    public void forceClose() throws MessagingException {
        synchronized (this) {
            this.close(false, true);
        }
    }
    
    public ACL[] getACL() throws MessagingException {
        return (ACL[])this.doOptionalCommand("ACL not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                return imapProtocol.getACL(IMAPFolder.this.fullName);
            }
        });
    }
    
    public String[] getAttributes() throws MessagingException {
        if (this.attributes == null) {
            this.exists();
        }
        return this.attributes.clone();
    }
    
    @Override
    public int getDeletedMessageCount() throws MessagingException {
        // monitorenter(this)
        try {
            int length;
            if (!this.opened) {
                this.checkExists();
                length = -1;
            }
            else {
                final Flags flags = new Flags();
                flags.add(Flags.Flag.DELETED);
                try {
                    synchronized (this.messageCacheLock) {
                        length = this.getProtocol().search(new FlagTerm(flags, true)).length;
                    }
                }
                catch (ConnectionException ex) {
                    throw new FolderClosedException(this, ex.getMessage());
                }
                catch (ProtocolException ex2) {
                    throw new MessagingException(ex2.getMessage(), ex2);
                }
            }
            return length;
        }
        finally {}
    }
    
    @Override
    public Folder getFolder(final String s) throws MessagingException {
        if (this.attributes != null && !this.isDirectory()) {
            throw new MessagingException("Cannot contain subfolders");
        }
        final char separator = this.getSeparator();
        return new IMAPFolder(String.valueOf(this.fullName) + separator + s, separator, (IMAPStore)this.store);
    }
    
    @Override
    public String getFullName() {
        synchronized (this) {
            return this.fullName;
        }
    }
    
    @Override
    public Message getMessage(final int n) throws MessagingException {
        synchronized (this) {
            this.checkOpened();
            this.checkRange(n);
            return this.messageCache.elementAt(n - 1);
        }
    }
    
    IMAPMessage getMessageBySeqNumber(final int n) {
        for (int i = n - 1; i < this.total; ++i) {
            final IMAPMessage imapMessage;
            if ((imapMessage = this.messageCache.elementAt(i)).getSequenceNumber() == n) {
                return imapMessage;
            }
        }
        return null;
    }
    
    @Override
    public Message getMessageByUID(final long n) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpened();
            IMAPMessage imapMessage = null;
            try {
                synchronized (this.messageCacheLock) {
                    final Long n2 = new Long(n);
                    Label_0075: {
                        if (this.uidTable == null) {
                            this.uidTable = new Hashtable();
                            break Label_0075;
                        }
                        final IMAPMessage messageBySeqNumber = this.uidTable.get(n2);
                        if ((imapMessage = messageBySeqNumber) == null) {
                            break Label_0075;
                        }
                        return messageBySeqNumber;
                    }
                    final UID fetchSequenceNumber = this.getProtocol().fetchSequenceNumber(n);
                    IMAPMessage messageBySeqNumber = imapMessage;
                    if (fetchSequenceNumber != null) {
                        messageBySeqNumber = imapMessage;
                        if (fetchSequenceNumber.seqnum <= this.total) {
                            messageBySeqNumber = this.getMessageBySeqNumber(fetchSequenceNumber.seqnum);
                            messageBySeqNumber.setUID(fetchSequenceNumber.uid);
                            this.uidTable.put(n2, messageBySeqNumber);
                        }
                    }
                }
                // monitorexit(this.messageCacheLock)
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this, ex.getMessage());
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
        finally {}
    }
    
    @Override
    public int getMessageCount() throws MessagingException {
        while (true) {
            // monitorenter(this)
            Label_0148: {
                try {
                    if (this.opened) {
                        break Label_0148;
                    }
                    this.checkExists();
                    try {
                        return this.getStatus().total;
                    }
                    catch (BadCommandException ex5) {
                        final IMAPFolder imapFolder = this;
                        final IMAPProtocol imapProtocol = imapFolder.getStoreProtocol();
                        final IMAPProtocol imapProtocol5;
                        final IMAPProtocol imapProtocol4;
                        final IMAPProtocol imapProtocol3;
                        final IMAPProtocol imapProtocol2 = imapProtocol3 = (imapProtocol4 = (imapProtocol5 = imapProtocol));
                        final IMAPFolder imapFolder2 = this;
                        final String s = imapFolder2.fullName;
                        final MailboxInfo mailboxInfo = imapProtocol3.examine(s);
                        final IMAPProtocol imapProtocol6 = imapProtocol2;
                        imapProtocol6.close();
                        final MailboxInfo mailboxInfo2 = mailboxInfo;
                        final int n;
                        final int n2 = n = mailboxInfo2.total;
                    }
                    catch (ConnectionException ex) {
                        throw new StoreClosedException(this.store, ex.getMessage());
                    }
                    catch (ProtocolException ex2) {
                        throw new MessagingException(ex2.getMessage(), ex2);
                    }
                }
                finally {}
                try {
                    final IMAPFolder imapFolder = this;
                    final IMAPProtocol imapProtocol = imapFolder.getStoreProtocol();
                    final IMAPProtocol imapProtocol5;
                    final IMAPProtocol imapProtocol4;
                    final IMAPProtocol imapProtocol3;
                    final IMAPProtocol imapProtocol2 = imapProtocol3 = (imapProtocol4 = (imapProtocol5 = imapProtocol));
                    final IMAPFolder imapFolder2 = this;
                    final String s = imapFolder2.fullName;
                    final MailboxInfo mailboxInfo = imapProtocol3.examine(s);
                    final IMAPProtocol imapProtocol6 = imapProtocol2;
                    imapProtocol6.close();
                    final MailboxInfo mailboxInfo2 = mailboxInfo;
                    return mailboxInfo2.total;
                }
                catch (ProtocolException ex6) {}
            }
            final Object messageCacheLock = this.messageCacheLock;
            // monitorenter(messageCacheLock)
            try {
                try {
                    this.keepConnectionAlive(true);
                    final int n = this.total;
                }
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ConnectionException ex3) {
                throw new FolderClosedException(this, ex3.getMessage());
            }
            catch (ProtocolException ex4) {
                throw new MessagingException(ex4.getMessage(), ex4);
            }
        }
    }
    
    @Override
    public Message[] getMessagesByUID(final long n, final long n2) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpened();
            try {
                synchronized (this.messageCacheLock) {
                    if (this.uidTable == null) {
                        this.uidTable = new Hashtable();
                    }
                    final UID[] fetchSequenceNumbers = this.getProtocol().fetchSequenceNumbers(n, n2);
                    final Message[] array = new Message[fetchSequenceNumbers.length];
                    for (int i = 0; i < fetchSequenceNumbers.length; ++i) {
                        final IMAPMessage messageBySeqNumber = this.getMessageBySeqNumber(fetchSequenceNumbers[i].seqnum);
                        messageBySeqNumber.setUID(fetchSequenceNumbers[i].uid);
                        array[i] = messageBySeqNumber;
                        this.uidTable.put(new Long(fetchSequenceNumbers[i].uid), messageBySeqNumber);
                    }
                    return array;
                }
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this, ex.getMessage());
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
        finally {}
    }
    
    @Override
    public Message[] getMessagesByUID(final long[] p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/sun/mail/imap/IMAPFolder.checkOpened:()V
        //     6: aload_0        
        //     7: getfield        com/sun/mail/imap/IMAPFolder.messageCacheLock:Ljava/lang/Object;
        //    10: astore          5
        //    12: aload           5
        //    14: monitorenter   
        //    15: aload_1        
        //    16: astore          4
        //    18: aload_0        
        //    19: getfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //    22: ifnull          168
        //    25: new             Ljava/util/Vector;
        //    28: dup            
        //    29: invokespecial   java/util/Vector.<init>:()V
        //    32: astore          6
        //    34: iconst_0       
        //    35: istore_2       
        //    36: iload_2        
        //    37: aload_1        
        //    38: arraylength    
        //    39: if_icmplt       107
        //    42: aload           6
        //    44: invokevirtual   java/util/Vector.size:()I
        //    47: istore_3       
        //    48: iload_3        
        //    49: newarray        J
        //    51: astore          4
        //    53: iconst_0       
        //    54: istore_2       
        //    55: goto            308
        //    58: aload           4
        //    60: arraylength    
        //    61: ifle            84
        //    64: aload_0        
        //    65: invokespecial   com/sun/mail/imap/IMAPFolder.getProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    68: aload           4
        //    70: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.fetchSequenceNumbers:([J)[Lcom/sun/mail/imap/protocol/UID;
        //    73: astore          4
        //    75: iconst_0       
        //    76: istore_2       
        //    77: iload_2        
        //    78: aload           4
        //    80: arraylength    
        //    81: if_icmplt       207
        //    84: aload_1        
        //    85: arraylength    
        //    86: anewarray       Ljavax/mail/Message;
        //    89: astore          4
        //    91: iconst_0       
        //    92: istore_2       
        //    93: iload_2        
        //    94: aload_1        
        //    95: arraylength    
        //    96: if_icmplt       263
        //    99: aload           5
        //   101: monitorexit    
        //   102: aload_0        
        //   103: monitorexit    
        //   104: aload           4
        //   106: areturn        
        //   107: aload_0        
        //   108: getfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   111: astore          4
        //   113: new             Ljava/lang/Long;
        //   116: dup            
        //   117: aload_1        
        //   118: iload_2        
        //   119: laload         
        //   120: invokespecial   java/lang/Long.<init>:(J)V
        //   123: astore          7
        //   125: aload           4
        //   127: aload           7
        //   129: invokevirtual   java/util/Hashtable.containsKey:(Ljava/lang/Object;)Z
        //   132: ifne            316
        //   135: aload           6
        //   137: aload           7
        //   139: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   142: goto            316
        //   145: aload           4
        //   147: iload_2        
        //   148: aload           6
        //   150: iload_2        
        //   151: invokevirtual   java/util/Vector.elementAt:(I)Ljava/lang/Object;
        //   154: checkcast       Ljava/lang/Long;
        //   157: invokevirtual   java/lang/Long.longValue:()J
        //   160: lastore        
        //   161: iload_2        
        //   162: iconst_1       
        //   163: iadd           
        //   164: istore_2       
        //   165: goto            308
        //   168: aload_0        
        //   169: new             Ljava/util/Hashtable;
        //   172: dup            
        //   173: invokespecial   java/util/Hashtable.<init>:()V
        //   176: putfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   179: goto            58
        //   182: astore_1       
        //   183: aload           5
        //   185: monitorexit    
        //   186: aload_1        
        //   187: athrow         
        //   188: astore_1       
        //   189: new             Ljavax/mail/FolderClosedException;
        //   192: dup            
        //   193: aload_0        
        //   194: aload_1        
        //   195: invokevirtual   com/sun/mail/iap/ConnectionException.getMessage:()Ljava/lang/String;
        //   198: invokespecial   javax/mail/FolderClosedException.<init>:(Ljavax/mail/Folder;Ljava/lang/String;)V
        //   201: athrow         
        //   202: astore_1       
        //   203: aload_0        
        //   204: monitorexit    
        //   205: aload_1        
        //   206: athrow         
        //   207: aload_0        
        //   208: aload           4
        //   210: iload_2        
        //   211: aaload         
        //   212: getfield        com/sun/mail/imap/protocol/UID.seqnum:I
        //   215: invokevirtual   com/sun/mail/imap/IMAPFolder.getMessageBySeqNumber:(I)Lcom/sun/mail/imap/IMAPMessage;
        //   218: astore          6
        //   220: aload           6
        //   222: aload           4
        //   224: iload_2        
        //   225: aaload         
        //   226: getfield        com/sun/mail/imap/protocol/UID.uid:J
        //   229: invokevirtual   com/sun/mail/imap/IMAPMessage.setUID:(J)V
        //   232: aload_0        
        //   233: getfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   236: new             Ljava/lang/Long;
        //   239: dup            
        //   240: aload           4
        //   242: iload_2        
        //   243: aaload         
        //   244: getfield        com/sun/mail/imap/protocol/UID.uid:J
        //   247: invokespecial   java/lang/Long.<init>:(J)V
        //   250: aload           6
        //   252: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   255: pop            
        //   256: iload_2        
        //   257: iconst_1       
        //   258: iadd           
        //   259: istore_2       
        //   260: goto            77
        //   263: aload           4
        //   265: iload_2        
        //   266: aload_0        
        //   267: getfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   270: new             Ljava/lang/Long;
        //   273: dup            
        //   274: aload_1        
        //   275: iload_2        
        //   276: laload         
        //   277: invokespecial   java/lang/Long.<init>:(J)V
        //   280: invokevirtual   java/util/Hashtable.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   283: checkcast       Ljavax/mail/Message;
        //   286: aastore        
        //   287: iload_2        
        //   288: iconst_1       
        //   289: iadd           
        //   290: istore_2       
        //   291: goto            93
        //   294: astore_1       
        //   295: new             Ljavax/mail/MessagingException;
        //   298: dup            
        //   299: aload_1        
        //   300: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   303: aload_1        
        //   304: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   307: athrow         
        //   308: iload_2        
        //   309: iload_3        
        //   310: if_icmplt       145
        //   313: goto            58
        //   316: iload_2        
        //   317: iconst_1       
        //   318: iadd           
        //   319: istore_2       
        //   320: goto            36
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  2      6      202    207    Any
        //  6      15     188    202    Lcom/sun/mail/iap/ConnectionException;
        //  6      15     294    308    Lcom/sun/mail/iap/ProtocolException;
        //  6      15     202    207    Any
        //  18     34     182    188    Any
        //  36     53     182    188    Any
        //  58     75     182    188    Any
        //  77     84     182    188    Any
        //  84     91     182    188    Any
        //  93     102    182    188    Any
        //  107    142    182    188    Any
        //  145    161    182    188    Any
        //  168    179    182    188    Any
        //  183    186    182    188    Any
        //  186    188    188    202    Lcom/sun/mail/iap/ConnectionException;
        //  186    188    294    308    Lcom/sun/mail/iap/ProtocolException;
        //  186    188    202    207    Any
        //  189    202    202    207    Any
        //  207    256    182    188    Any
        //  263    287    182    188    Any
        //  295    308    202    207    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public String getName() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        com/sun/mail/imap/IMAPFolder.name:Ljava/lang/String;
        //     6: astore_1       
        //     7: aload_1        
        //     8: ifnonnull       35
        //    11: aload_0        
        //    12: aload_0        
        //    13: getfield        com/sun/mail/imap/IMAPFolder.fullName:Ljava/lang/String;
        //    16: aload_0        
        //    17: getfield        com/sun/mail/imap/IMAPFolder.fullName:Ljava/lang/String;
        //    20: aload_0        
        //    21: invokevirtual   com/sun/mail/imap/IMAPFolder.getSeparator:()C
        //    24: invokevirtual   java/lang/String.lastIndexOf:(I)I
        //    27: iconst_1       
        //    28: iadd           
        //    29: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //    32: putfield        com/sun/mail/imap/IMAPFolder.name:Ljava/lang/String;
        //    35: aload_0        
        //    36: getfield        com/sun/mail/imap/IMAPFolder.name:Ljava/lang/String;
        //    39: astore_1       
        //    40: aload_0        
        //    41: monitorexit    
        //    42: aload_1        
        //    43: areturn        
        //    44: astore_1       
        //    45: aload_0        
        //    46: monitorexit    
        //    47: aload_1        
        //    48: athrow         
        //    49: astore_1       
        //    50: goto            35
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  2      7      44     49     Any
        //  11     35     49     53     Ljavax/mail/MessagingException;
        //  11     35     44     49     Any
        //  35     40     44     49     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0035:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public int getNewMessageCount() throws MessagingException {
        while (true) {
            // monitorenter(this)
            Label_0148: {
                try {
                    if (this.opened) {
                        break Label_0148;
                    }
                    this.checkExists();
                    try {
                        return this.getStatus().recent;
                    }
                    catch (BadCommandException ex5) {
                        final IMAPFolder imapFolder = this;
                        final IMAPProtocol imapProtocol = imapFolder.getStoreProtocol();
                        final IMAPProtocol imapProtocol5;
                        final IMAPProtocol imapProtocol4;
                        final IMAPProtocol imapProtocol3;
                        final IMAPProtocol imapProtocol2 = imapProtocol3 = (imapProtocol4 = (imapProtocol5 = imapProtocol));
                        final IMAPFolder imapFolder2 = this;
                        final String s = imapFolder2.fullName;
                        final MailboxInfo mailboxInfo = imapProtocol3.examine(s);
                        final IMAPProtocol imapProtocol6 = imapProtocol2;
                        imapProtocol6.close();
                        final MailboxInfo mailboxInfo2 = mailboxInfo;
                        final int n;
                        final int n2 = n = mailboxInfo2.recent;
                    }
                    catch (ConnectionException ex) {
                        throw new StoreClosedException(this.store, ex.getMessage());
                    }
                    catch (ProtocolException ex2) {
                        throw new MessagingException(ex2.getMessage(), ex2);
                    }
                }
                finally {}
                try {
                    final IMAPFolder imapFolder = this;
                    final IMAPProtocol imapProtocol = imapFolder.getStoreProtocol();
                    final IMAPProtocol imapProtocol5;
                    final IMAPProtocol imapProtocol4;
                    final IMAPProtocol imapProtocol3;
                    final IMAPProtocol imapProtocol2 = imapProtocol3 = (imapProtocol4 = (imapProtocol5 = imapProtocol));
                    final IMAPFolder imapFolder2 = this;
                    final String s = imapFolder2.fullName;
                    final MailboxInfo mailboxInfo = imapProtocol3.examine(s);
                    final IMAPProtocol imapProtocol6 = imapProtocol2;
                    imapProtocol6.close();
                    final MailboxInfo mailboxInfo2 = mailboxInfo;
                    return mailboxInfo2.recent;
                }
                catch (ProtocolException ex6) {}
            }
            final Object messageCacheLock = this.messageCacheLock;
            // monitorenter(messageCacheLock)
            try {
                try {
                    this.keepConnectionAlive(true);
                    final int n = this.recent;
                }
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ConnectionException ex3) {
                throw new FolderClosedException(this, ex3.getMessage());
            }
            catch (ProtocolException ex4) {
                throw new MessagingException(ex4.getMessage(), ex4);
            }
        }
    }
    
    @Override
    public Folder getParent() throws MessagingException {
        synchronized (this) {
            final char separator = this.getSeparator();
            final int lastIndex = this.fullName.lastIndexOf(separator);
            IMAPFolder imapFolder;
            if (lastIndex != -1) {
                imapFolder = new IMAPFolder(this.fullName.substring(0, lastIndex), separator, (IMAPStore)this.store);
            }
            else {
                imapFolder = new DefaultFolder((IMAPStore)this.store);
            }
            return imapFolder;
        }
    }
    
    @Override
    public Flags getPermanentFlags() {
        synchronized (this) {
            return (Flags)this.permanentFlags.clone();
        }
    }
    
    public Quota[] getQuota() throws MessagingException {
        return (Quota[])this.doOptionalCommand("QUOTA not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                return imapProtocol.getQuotaRoot(IMAPFolder.this.fullName);
            }
        });
    }
    
    @Override
    public char getSeparator() throws MessagingException {
        synchronized (this) {
            if (this.separator == '\uffff') {
                final ListInfo[] array = null;
                final ListInfo[] array2 = (ListInfo[])this.doCommand((ProtocolCommand)new ProtocolCommand() {
                    @Override
                    public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                        if (imapProtocol.isREV1()) {
                            return imapProtocol.list(IMAPFolder.this.fullName, "");
                        }
                        return imapProtocol.list("", IMAPFolder.this.fullName);
                    }
                });
                if (array2 != null) {
                    this.separator = array2[0].separator;
                }
                else {
                    this.separator = '/';
                }
            }
            return this.separator;
        }
    }
    
    protected IMAPProtocol getStoreProtocol() throws ProtocolException {
        synchronized (this) {
            if (this.connectionPoolDebug) {
                this.out.println("DEBUG: getStoreProtocol() - borrowing a connection");
            }
            return ((IMAPStore)this.store).getStoreProtocol();
        }
    }
    
    @Override
    public int getType() throws MessagingException {
        synchronized (this) {
            if (this.opened) {
                if (this.attributes == null) {
                    this.exists();
                }
            }
            else {
                this.checkExists();
            }
            return this.type;
        }
    }
    
    @Override
    public long getUID(Message messageCacheLock) throws MessagingException {
        synchronized (this) {
            if (messageCacheLock.getFolder() != this) {
                throw new NoSuchElementException("Message does not belong to this folder");
            }
        }
        this.checkOpened();
        final IMAPMessage imapMessage2;
        final IMAPMessage imapMessage = imapMessage2;
        long uid = imapMessage.getUID();
        if (uid == -1L) {
            messageCacheLock = (Message)this.messageCacheLock;
            // monitorenter(messageCacheLock)
            try {
                final IMAPProtocol protocol = this.getProtocol();
                imapMessage.checkExpunged();
                final UID fetchUID = protocol.fetchUID(imapMessage.getSequenceNumber());
                if (fetchUID != null) {
                    uid = fetchUID.uid;
                    imapMessage.setUID(uid);
                    if (this.uidTable == null) {
                        this.uidTable = new Hashtable();
                    }
                    this.uidTable.put(new Long(uid), imapMessage);
                }
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this, ex.getMessage());
                try {}
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
        // monitorexit(this)
        return uid;
    }
    
    public long getUIDNext() throws MessagingException {
        // monitorenter(this)
        try {
            long n = 0L;
            if (this.opened) {
                n = this.uidnext;
            }
            else {
                Object status = null;
                IMAPProtocol imapProtocol = null;
                IMAPProtocol imapProtocol2 = null;
                IMAPProtocol storeProtocol = null;
                final IMAPProtocol imapProtocol3 = null;
                try {
                    final IMAPProtocol imapProtocol4 = imapProtocol2 = (imapProtocol = (IMAPProtocol)(status = (storeProtocol = this.getStoreProtocol())));
                    status = imapProtocol4.status(this.fullName, new String[] { "UIDNEXT" });
                    this.releaseStoreProtocol(imapProtocol4);
                    n = ((Status)status).uidnext;
                }
                catch (BadCommandException ex) {
                    status = storeProtocol;
                    throw new MessagingException("Cannot obtain UIDNext", ex);
                }
                catch (ConnectionException ex2) {
                    status = imapProtocol;
                    this.throwClosedException(ex2);
                    this.releaseStoreProtocol(imapProtocol);
                    status = imapProtocol3;
                }
                catch (ProtocolException ex3) {
                    status = imapProtocol2;
                    throw new MessagingException(ex3.getMessage(), ex3);
                }
                finally {
                    this.releaseStoreProtocol((IMAPProtocol)status);
                }
            }
            return n;
        }
        finally {}
    }
    
    @Override
    public long getUIDValidity() throws MessagingException {
        // monitorenter(this)
        try {
            long n = 0L;
            if (this.opened) {
                n = this.uidvalidity;
            }
            else {
                Object status = null;
                IMAPProtocol imapProtocol = null;
                IMAPProtocol imapProtocol2 = null;
                IMAPProtocol storeProtocol = null;
                final IMAPProtocol imapProtocol3 = null;
                try {
                    final IMAPProtocol imapProtocol4 = imapProtocol2 = (imapProtocol = (IMAPProtocol)(status = (storeProtocol = this.getStoreProtocol())));
                    status = imapProtocol4.status(this.fullName, new String[] { "UIDVALIDITY" });
                    this.releaseStoreProtocol(imapProtocol4);
                    n = ((Status)status).uidvalidity;
                }
                catch (BadCommandException ex) {
                    status = storeProtocol;
                    throw new MessagingException("Cannot obtain UIDValidity", ex);
                }
                catch (ConnectionException ex2) {
                    status = imapProtocol;
                    this.throwClosedException(ex2);
                    this.releaseStoreProtocol(imapProtocol);
                    status = imapProtocol3;
                }
                catch (ProtocolException ex3) {
                    status = imapProtocol2;
                    throw new MessagingException(ex3.getMessage(), ex3);
                }
                finally {
                    this.releaseStoreProtocol((IMAPProtocol)status);
                }
            }
            return n;
        }
        finally {}
    }
    
    @Override
    public int getUnreadMessageCount() throws MessagingException {
        while (true) {
            // monitorenter(this)
            try {
                if (!this.opened) {
                    this.checkExists();
                    try {
                        return this.getStatus().unseen;
                    }
                    catch (BadCommandException ex5) {
                        final int n = -1;
                    }
                    catch (ConnectionException ex) {
                        throw new StoreClosedException(this.store, ex.getMessage());
                    }
                    catch (ProtocolException ex2) {
                        throw new MessagingException(ex2.getMessage(), ex2);
                    }
                }
            }
            finally {}
            final Flags flags = new Flags();
            flags.add(Flags.Flag.SEEN);
            try {
                synchronized (this.messageCacheLock) {
                    final int n = this.getProtocol().search(new FlagTerm(flags, false)).length;
                }
            }
            catch (ConnectionException ex3) {
                throw new FolderClosedException(this, ex3.getMessage());
            }
            catch (ProtocolException ex4) {
                throw new MessagingException(ex4.getMessage(), ex4);
            }
        }
    }
    
    @Override
    public void handleResponse(final Response response) {
        assert Thread.holdsLock(this.messageCacheLock);
        if (response.isOK() || response.isNO() || response.isBAD() || response.isBYE()) {
            ((IMAPStore)this.store).handleResponseCode(response);
        }
        if (response.isBYE()) {
            if (this.opened) {
                this.cleanup(false);
            }
        }
        else if (!response.isOK() && response.isUnTagged()) {
            if (!(response instanceof IMAPResponse)) {
                this.out.println("UNEXPECTED RESPONSE : " + response.toString());
                this.out.println("CONTACT javamail@sun.com");
                return;
            }
            final IMAPResponse imapResponse = (IMAPResponse)response;
            if (imapResponse.keyEquals("EXISTS")) {
                final int number = imapResponse.getNumber();
                if (number > this.realTotal) {
                    final int n = number - this.realTotal;
                    final Message[] array = new Message[n];
                    for (int i = 0; i < n; ++i) {
                        final int total = this.total + 1;
                        this.total = total;
                        final int realTotal = this.realTotal + 1;
                        this.realTotal = realTotal;
                        final IMAPMessage imapMessage = new IMAPMessage(this, total, realTotal);
                        array[i] = imapMessage;
                        this.messageCache.addElement(imapMessage);
                    }
                    this.notifyMessageAddedListeners(array);
                }
            }
            else if (imapResponse.keyEquals("EXPUNGE")) {
                final IMAPMessage messageBySeqNumber = this.getMessageBySeqNumber(imapResponse.getNumber());
                messageBySeqNumber.setExpunged(true);
                for (int j = messageBySeqNumber.getMessageNumber(); j < this.total; ++j) {
                    final IMAPMessage imapMessage2 = this.messageCache.elementAt(j);
                    if (!imapMessage2.isExpunged()) {
                        imapMessage2.setSequenceNumber(imapMessage2.getSequenceNumber() - 1);
                    }
                }
                --this.realTotal;
                if (this.doExpungeNotification) {
                    this.notifyMessageRemovedListeners(false, new Message[] { messageBySeqNumber });
                }
            }
            else if (imapResponse.keyEquals("FETCH")) {
                assert imapResponse instanceof FetchResponse : "!ir instanceof FetchResponse";
                final FetchResponse fetchResponse = (FetchResponse)imapResponse;
                final Flags flags = (Flags)fetchResponse.getItem(Flags.class);
                if (flags != null) {
                    final IMAPMessage messageBySeqNumber2 = this.getMessageBySeqNumber(fetchResponse.getNumber());
                    if (messageBySeqNumber2 != null) {
                        messageBySeqNumber2._setFlags(flags);
                        this.notifyMessageChangedListeners(1, messageBySeqNumber2);
                    }
                }
            }
            else if (imapResponse.keyEquals("RECENT")) {
                this.recent = imapResponse.getNumber();
            }
        }
    }
    
    void handleResponses(final Response[] array) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] != null) {
                this.handleResponse(array[i]);
            }
        }
    }
    
    @Override
    public boolean hasNewMessages() throws MessagingException {
        while (true) {
            final boolean b = false;
            boolean booleanValue = false;
            // monitorenter(this)
            try {
                if (this.opened) {
                    final Object messageCacheLock = this.messageCacheLock;
                    // monitorenter(messageCacheLock)
                    try {
                        this.keepConnectionAlive(true);
                        if (this.recent > 0) {
                            booleanValue = true;
                        }
                        return booleanValue;
                    }
                    catch (ConnectionException ex) {
                        throw new FolderClosedException(this, ex.getMessage());
                        try {}
                        finally {
                        }
                        // monitorexit(messageCacheLock)
                    }
                    catch (ProtocolException ex2) {
                        throw new MessagingException(ex2.getMessage(), ex2);
                    }
                }
            }
            finally {}
            this.checkExists();
            final Object messageCacheLock = this.doCommandIgnoreFailure((ProtocolCommand)new ProtocolCommand() {
                @Override
                public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                    final ListInfo[] list = imapProtocol.list("", IMAPFolder.this.fullName);
                    if (list != null) {
                        if (list[0].changeState == 1) {
                            return Boolean.TRUE;
                        }
                        if (list[0].changeState == 2) {
                            return Boolean.FALSE;
                        }
                    }
                    if (IMAPFolder.this.getStatus().recent > 0) {
                        return Boolean.TRUE;
                    }
                    return Boolean.FALSE;
                }
            });
            booleanValue = b;
            if (messageCacheLock != null) {
                booleanValue = (boolean)messageCacheLock;
                return booleanValue;
            }
            return booleanValue;
        }
    }
    
    public void idle() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            21
        //     6: aload_0        
        //     7: invokestatic    java/lang/Thread.holdsLock:(Ljava/lang/Object;)Z
        //    10: ifeq            21
        //    13: new             Ljava/lang/AssertionError;
        //    16: dup            
        //    17: invokespecial   java/lang/AssertionError.<init>:()V
        //    20: athrow         
        //    21: aload_0        
        //    22: monitorenter   
        //    23: aload_0        
        //    24: invokespecial   com/sun/mail/imap/IMAPFolder.checkOpened:()V
        //    27: aload_0        
        //    28: ldc_w           "IDLE not supported"
        //    31: new             Lcom/sun/mail/imap/IMAPFolder$19;
        //    34: dup            
        //    35: aload_0        
        //    36: invokespecial   com/sun/mail/imap/IMAPFolder$19.<init>:(Lcom/sun/mail/imap/IMAPFolder;)V
        //    39: invokevirtual   com/sun/mail/imap/IMAPFolder.doOptionalCommand:(Ljava/lang/String;Lcom/sun/mail/imap/IMAPFolder$ProtocolCommand;)Ljava/lang/Object;
        //    42: checkcast       Ljava/lang/Boolean;
        //    45: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //    48: ifne            54
        //    51: aload_0        
        //    52: monitorexit    
        //    53: return         
        //    54: aload_0        
        //    55: monitorexit    
        //    56: aload_0        
        //    57: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    60: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.readIdleResponse:()Lcom/sun/mail/iap/Response;
        //    63: astore          5
        //    65: aload_0        
        //    66: getfield        com/sun/mail/imap/IMAPFolder.messageCacheLock:Ljava/lang/Object;
        //    69: astore          4
        //    71: aload           4
        //    73: monitorenter   
        //    74: aload           5
        //    76: ifnull          98
        //    79: aload_0        
        //    80: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    83: ifnull          98
        //    86: aload_0        
        //    87: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    90: aload           5
        //    92: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.processIdleResponse:(Lcom/sun/mail/iap/Response;)Z
        //    95: ifne            146
        //    98: aload_0        
        //    99: iconst_0       
        //   100: putfield        com/sun/mail/imap/IMAPFolder.idleState:I
        //   103: aload_0        
        //   104: getfield        com/sun/mail/imap/IMAPFolder.messageCacheLock:Ljava/lang/Object;
        //   107: invokevirtual   java/lang/Object.notifyAll:()V
        //   110: aload           4
        //   112: monitorexit    
        //   113: aload_0        
        //   114: getfield        com/sun/mail/imap/IMAPFolder.store:Ljavax/mail/Store;
        //   117: checkcast       Lcom/sun/mail/imap/IMAPStore;
        //   120: invokevirtual   com/sun/mail/imap/IMAPStore.getMinIdleTime:()I
        //   123: istore_1       
        //   124: iload_1        
        //   125: ifle            188
        //   128: iload_1        
        //   129: i2l            
        //   130: lstore_2       
        //   131: lload_2        
        //   132: invokestatic    java/lang/Thread.sleep:(J)V
        //   135: return         
        //   136: astore          4
        //   138: return         
        //   139: astore          4
        //   141: aload_0        
        //   142: monitorexit    
        //   143: aload           4
        //   145: athrow         
        //   146: aload           4
        //   148: monitorexit    
        //   149: goto            56
        //   152: astore          5
        //   154: aload           4
        //   156: monitorexit    
        //   157: aload           5
        //   159: athrow         
        //   160: astore          4
        //   162: aload_0        
        //   163: aload           4
        //   165: invokespecial   com/sun/mail/imap/IMAPFolder.throwClosedException:(Lcom/sun/mail/iap/ConnectionException;)V
        //   168: goto            56
        //   171: astore          4
        //   173: new             Ljavax/mail/MessagingException;
        //   176: dup            
        //   177: aload           4
        //   179: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   182: aload           4
        //   184: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   187: athrow         
        //   188: return         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  23     53     139    146    Any
        //  54     56     139    146    Any
        //  65     74     160    171    Lcom/sun/mail/iap/ConnectionException;
        //  65     74     171    188    Lcom/sun/mail/iap/ProtocolException;
        //  79     98     152    160    Any
        //  98     113    152    160    Any
        //  131    135    136    139    Ljava/lang/InterruptedException;
        //  141    143    139    146    Any
        //  146    149    152    160    Any
        //  154    157    152    160    Any
        //  157    160    160    171    Lcom/sun/mail/iap/ConnectionException;
        //  157    160    171    188    Lcom/sun/mail/iap/ProtocolException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0098:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean isOpen() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        com/sun/mail/imap/IMAPFolder.messageCacheLock:Ljava/lang/Object;
        //     6: astore_2       
        //     7: aload_2        
        //     8: monitorenter   
        //     9: aload_0        
        //    10: getfield        com/sun/mail/imap/IMAPFolder.opened:Z
        //    13: istore_1       
        //    14: iload_1        
        //    15: ifeq            23
        //    18: aload_0        
        //    19: iconst_0       
        //    20: invokespecial   com/sun/mail/imap/IMAPFolder.keepConnectionAlive:(Z)V
        //    23: aload_2        
        //    24: monitorexit    
        //    25: aload_0        
        //    26: getfield        com/sun/mail/imap/IMAPFolder.opened:Z
        //    29: istore_1       
        //    30: aload_0        
        //    31: monitorexit    
        //    32: iload_1        
        //    33: ireturn        
        //    34: astore_3       
        //    35: aload_2        
        //    36: monitorexit    
        //    37: aload_3        
        //    38: athrow         
        //    39: astore_2       
        //    40: aload_0        
        //    41: monitorexit    
        //    42: aload_2        
        //    43: athrow         
        //    44: astore_3       
        //    45: goto            23
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      9      39     44     Any
        //  9      14     34     39     Any
        //  18     23     44     48     Lcom/sun/mail/iap/ProtocolException;
        //  18     23     34     39     Any
        //  23     25     34     39     Any
        //  25     30     39     44     Any
        //  35     37     34     39     Any
        //  37     39     39     44     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0023:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean isSubscribed() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aconst_null    
        //     3: checkcast       [Lcom/sun/mail/imap/protocol/ListInfo;
        //     6: astore_3       
        //     7: aload_0        
        //     8: getfield        com/sun/mail/imap/IMAPFolder.isNamespace:Z
        //    11: ifeq            87
        //    14: aload_0        
        //    15: getfield        com/sun/mail/imap/IMAPFolder.separator:C
        //    18: ifeq            87
        //    21: new             Ljava/lang/StringBuilder;
        //    24: dup            
        //    25: aload_0        
        //    26: getfield        com/sun/mail/imap/IMAPFolder.fullName:Ljava/lang/String;
        //    29: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //    32: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    35: aload_0        
        //    36: getfield        com/sun/mail/imap/IMAPFolder.separator:C
        //    39: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    42: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    45: astore_2       
        //    46: aload_0        
        //    47: new             Lcom/sun/mail/imap/IMAPFolder$4;
        //    50: dup            
        //    51: aload_0        
        //    52: aload_2        
        //    53: invokespecial   com/sun/mail/imap/IMAPFolder$4.<init>:(Lcom/sun/mail/imap/IMAPFolder;Ljava/lang/String;)V
        //    56: invokevirtual   com/sun/mail/imap/IMAPFolder.doProtocolCommand:(Lcom/sun/mail/imap/IMAPFolder$ProtocolCommand;)Ljava/lang/Object;
        //    59: checkcast       [Lcom/sun/mail/imap/protocol/ListInfo;
        //    62: astore          4
        //    64: aload           4
        //    66: astore_3       
        //    67: aload_3        
        //    68: ifnull          95
        //    71: aload_3        
        //    72: aload_0        
        //    73: aload_3        
        //    74: aload_2        
        //    75: invokespecial   com/sun/mail/imap/IMAPFolder.findName:([Lcom/sun/mail/imap/protocol/ListInfo;Ljava/lang/String;)I
        //    78: aaload         
        //    79: getfield        com/sun/mail/imap/protocol/ListInfo.canOpen:Z
        //    82: istore_1       
        //    83: aload_0        
        //    84: monitorexit    
        //    85: iload_1        
        //    86: ireturn        
        //    87: aload_0        
        //    88: getfield        com/sun/mail/imap/IMAPFolder.fullName:Ljava/lang/String;
        //    91: astore_2       
        //    92: goto            46
        //    95: iconst_0       
        //    96: istore_1       
        //    97: goto            83
        //   100: astore_2       
        //   101: aload_0        
        //   102: monitorexit    
        //   103: aload_2        
        //   104: athrow         
        //   105: astore          4
        //   107: goto            67
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      46     100    105    Any
        //  46     64     105    110    Lcom/sun/mail/iap/ProtocolException;
        //  46     64     100    105    Any
        //  71     83     100    105    Any
        //  87     92     100    105    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0046:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Folder[] list(final String s) throws MessagingException {
        return this.doList(s, false);
    }
    
    public Rights[] listRights(final String s) throws MessagingException {
        return (Rights[])this.doOptionalCommand("ACL not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                return imapProtocol.listRights(IMAPFolder.this.fullName, s);
            }
        });
    }
    
    @Override
    public Folder[] listSubscribed(final String s) throws MessagingException {
        return this.doList(s, true);
    }
    
    public Rights myRights() throws MessagingException {
        return (Rights)this.doOptionalCommand("ACL not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                return imapProtocol.myRights(IMAPFolder.this.fullName);
            }
        });
    }
    
    @Override
    public void open(final int p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/sun/mail/imap/IMAPFolder.checkClosed:()V
        //     6: aload_0        
        //     7: aload_0        
        //     8: getfield        com/sun/mail/imap/IMAPFolder.store:Ljavax/mail/Store;
        //    11: checkcast       Lcom/sun/mail/imap/IMAPStore;
        //    14: aload_0        
        //    15: invokevirtual   com/sun/mail/imap/IMAPStore.getProtocol:(Lcom/sun/mail/imap/IMAPFolder;)Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    18: putfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    21: aconst_null    
        //    22: astore_3       
        //    23: aload_0        
        //    24: getfield        com/sun/mail/imap/IMAPFolder.messageCacheLock:Ljava/lang/Object;
        //    27: astore          4
        //    29: aload           4
        //    31: monitorenter   
        //    32: aload_0        
        //    33: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    36: aload_0        
        //    37: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.addResponseHandler:(Lcom/sun/mail/iap/ResponseHandler;)V
        //    40: iload_1        
        //    41: iconst_1       
        //    42: if_icmpne       227
        //    45: aload_0        
        //    46: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    49: aload_0        
        //    50: getfield        com/sun/mail/imap/IMAPFolder.fullName:Ljava/lang/String;
        //    53: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.examine:(Ljava/lang/String;)Lcom/sun/mail/imap/protocol/MailboxInfo;
        //    56: astore_2       
        //    57: aload_2        
        //    58: getfield        com/sun/mail/imap/protocol/MailboxInfo.mode:I
        //    61: iload_1        
        //    62: if_icmpeq       91
        //    65: iload_1        
        //    66: iconst_2       
        //    67: if_icmpne       296
        //    70: aload_2        
        //    71: getfield        com/sun/mail/imap/protocol/MailboxInfo.mode:I
        //    74: iconst_1       
        //    75: if_icmpne       296
        //    78: aload_0        
        //    79: getfield        com/sun/mail/imap/IMAPFolder.store:Ljavax/mail/Store;
        //    82: checkcast       Lcom/sun/mail/imap/IMAPStore;
        //    85: invokevirtual   com/sun/mail/imap/IMAPStore.allowReadOnlySelect:()Z
        //    88: ifeq            296
        //    91: aload_0        
        //    92: iconst_1       
        //    93: putfield        com/sun/mail/imap/IMAPFolder.opened:Z
        //    96: aload_0        
        //    97: iconst_0       
        //    98: putfield        com/sun/mail/imap/IMAPFolder.reallyClosed:Z
        //   101: aload_0        
        //   102: aload_2        
        //   103: getfield        com/sun/mail/imap/protocol/MailboxInfo.mode:I
        //   106: putfield        com/sun/mail/imap/IMAPFolder.mode:I
        //   109: aload_0        
        //   110: aload_2        
        //   111: getfield        com/sun/mail/imap/protocol/MailboxInfo.availableFlags:Ljavax/mail/Flags;
        //   114: putfield        com/sun/mail/imap/IMAPFolder.availableFlags:Ljavax/mail/Flags;
        //   117: aload_0        
        //   118: aload_2        
        //   119: getfield        com/sun/mail/imap/protocol/MailboxInfo.permanentFlags:Ljavax/mail/Flags;
        //   122: putfield        com/sun/mail/imap/IMAPFolder.permanentFlags:Ljavax/mail/Flags;
        //   125: aload_2        
        //   126: getfield        com/sun/mail/imap/protocol/MailboxInfo.total:I
        //   129: istore_1       
        //   130: aload_0        
        //   131: iload_1        
        //   132: putfield        com/sun/mail/imap/IMAPFolder.realTotal:I
        //   135: aload_0        
        //   136: iload_1        
        //   137: putfield        com/sun/mail/imap/IMAPFolder.total:I
        //   140: aload_0        
        //   141: aload_2        
        //   142: getfield        com/sun/mail/imap/protocol/MailboxInfo.recent:I
        //   145: putfield        com/sun/mail/imap/IMAPFolder.recent:I
        //   148: aload_0        
        //   149: aload_2        
        //   150: getfield        com/sun/mail/imap/protocol/MailboxInfo.uidvalidity:J
        //   153: putfield        com/sun/mail/imap/IMAPFolder.uidvalidity:J
        //   156: aload_0        
        //   157: aload_2        
        //   158: getfield        com/sun/mail/imap/protocol/MailboxInfo.uidnext:J
        //   161: putfield        com/sun/mail/imap/IMAPFolder.uidnext:J
        //   164: aload_0        
        //   165: new             Ljava/util/Vector;
        //   168: dup            
        //   169: aload_0        
        //   170: getfield        com/sun/mail/imap/IMAPFolder.total:I
        //   173: invokespecial   java/util/Vector.<init>:(I)V
        //   176: putfield        com/sun/mail/imap/IMAPFolder.messageCache:Ljava/util/Vector;
        //   179: iconst_0       
        //   180: istore_1       
        //   181: iload_1        
        //   182: aload_0        
        //   183: getfield        com/sun/mail/imap/IMAPFolder.total:I
        //   186: if_icmplt       358
        //   189: aload           4
        //   191: monitorexit    
        //   192: aload_3        
        //   193: astore_2       
        //   194: aload_2        
        //   195: ifnull          399
        //   198: aload_0        
        //   199: invokespecial   com/sun/mail/imap/IMAPFolder.checkExists:()V
        //   202: aload_0        
        //   203: getfield        com/sun/mail/imap/IMAPFolder.type:I
        //   206: iconst_1       
        //   207: iand           
        //   208: ifne            386
        //   211: new             Ljavax/mail/MessagingException;
        //   214: dup            
        //   215: ldc_w           "folder cannot contain messages"
        //   218: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   221: athrow         
        //   222: astore_2       
        //   223: aload_0        
        //   224: monitorexit    
        //   225: aload_2        
        //   226: athrow         
        //   227: aload_0        
        //   228: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   231: aload_0        
        //   232: getfield        com/sun/mail/imap/IMAPFolder.fullName:Ljava/lang/String;
        //   235: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.select:(Ljava/lang/String;)Lcom/sun/mail/imap/protocol/MailboxInfo;
        //   238: astore_2       
        //   239: goto            57
        //   242: astore_2       
        //   243: aload_0        
        //   244: iconst_1       
        //   245: invokespecial   com/sun/mail/imap/IMAPFolder.releaseProtocol:(Z)V
        //   248: aload_0        
        //   249: aconst_null    
        //   250: putfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   253: aload           4
        //   255: monitorexit    
        //   256: goto            194
        //   259: astore_2       
        //   260: aload           4
        //   262: monitorexit    
        //   263: aload_2        
        //   264: athrow         
        //   265: astore_2       
        //   266: aload_0        
        //   267: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   270: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.logout:()V
        //   273: aload_0        
        //   274: iconst_0       
        //   275: invokespecial   com/sun/mail/imap/IMAPFolder.releaseProtocol:(Z)V
        //   278: aload_0        
        //   279: aconst_null    
        //   280: putfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   283: new             Ljavax/mail/MessagingException;
        //   286: dup            
        //   287: aload_2        
        //   288: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   291: aload_2        
        //   292: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   295: athrow         
        //   296: aload_0        
        //   297: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   300: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.close:()V
        //   303: aload_0        
        //   304: iconst_1       
        //   305: invokespecial   com/sun/mail/imap/IMAPFolder.releaseProtocol:(Z)V
        //   308: aload_0        
        //   309: aconst_null    
        //   310: putfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   313: new             Ljavax/mail/ReadOnlyFolderException;
        //   316: dup            
        //   317: aload_0        
        //   318: ldc_w           "Cannot open in desired mode"
        //   321: invokespecial   javax/mail/ReadOnlyFolderException.<init>:(Ljavax/mail/Folder;Ljava/lang/String;)V
        //   324: athrow         
        //   325: astore_2       
        //   326: aload_0        
        //   327: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   330: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.logout:()V
        //   333: aload_0        
        //   334: iconst_0       
        //   335: invokespecial   com/sun/mail/imap/IMAPFolder.releaseProtocol:(Z)V
        //   338: goto            308
        //   341: astore_2       
        //   342: aload_0        
        //   343: iconst_0       
        //   344: invokespecial   com/sun/mail/imap/IMAPFolder.releaseProtocol:(Z)V
        //   347: goto            308
        //   350: astore_2       
        //   351: aload_0        
        //   352: iconst_0       
        //   353: invokespecial   com/sun/mail/imap/IMAPFolder.releaseProtocol:(Z)V
        //   356: aload_2        
        //   357: athrow         
        //   358: aload_0        
        //   359: getfield        com/sun/mail/imap/IMAPFolder.messageCache:Ljava/util/Vector;
        //   362: new             Lcom/sun/mail/imap/IMAPMessage;
        //   365: dup            
        //   366: aload_0        
        //   367: iload_1        
        //   368: iconst_1       
        //   369: iadd           
        //   370: iload_1        
        //   371: iconst_1       
        //   372: iadd           
        //   373: invokespecial   com/sun/mail/imap/IMAPMessage.<init>:(Lcom/sun/mail/imap/IMAPFolder;II)V
        //   376: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   379: iload_1        
        //   380: iconst_1       
        //   381: iadd           
        //   382: istore_1       
        //   383: goto            181
        //   386: new             Ljavax/mail/MessagingException;
        //   389: dup            
        //   390: aload_2        
        //   391: invokevirtual   com/sun/mail/iap/CommandFailedException.getMessage:()Ljava/lang/String;
        //   394: aload_2        
        //   395: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   398: athrow         
        //   399: aload_0        
        //   400: iconst_1       
        //   401: putfield        com/sun/mail/imap/IMAPFolder.exists:Z
        //   404: aload_0        
        //   405: aconst_null    
        //   406: putfield        com/sun/mail/imap/IMAPFolder.attributes:[Ljava/lang/String;
        //   409: aload_0        
        //   410: iconst_1       
        //   411: putfield        com/sun/mail/imap/IMAPFolder.type:I
        //   414: aload_0        
        //   415: iconst_1       
        //   416: invokevirtual   com/sun/mail/imap/IMAPFolder.notifyConnectionListeners:(I)V
        //   419: aload_0        
        //   420: monitorexit    
        //   421: return         
        //   422: astore_3       
        //   423: goto            273
        //   426: astore_3       
        //   427: goto            273
        //   430: astore_2       
        //   431: goto            308
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  2      21     222    227    Any
        //  23     32     222    227    Any
        //  32     40     259    265    Any
        //  45     57     242    259    Lcom/sun/mail/iap/CommandFailedException;
        //  45     57     265    430    Lcom/sun/mail/iap/ProtocolException;
        //  45     57     259    265    Any
        //  57     65     259    265    Any
        //  70     91     259    265    Any
        //  91     179    259    265    Any
        //  181    192    259    265    Any
        //  198    222    222    227    Any
        //  227    239    242    259    Lcom/sun/mail/iap/CommandFailedException;
        //  227    239    265    430    Lcom/sun/mail/iap/ProtocolException;
        //  227    239    259    265    Any
        //  243    253    259    265    Any
        //  253    256    259    265    Any
        //  260    263    259    265    Any
        //  263    265    222    227    Any
        //  266    273    426    430    Lcom/sun/mail/iap/ProtocolException;
        //  266    273    422    426    Any
        //  273    296    259    265    Any
        //  296    308    325    358    Lcom/sun/mail/iap/ProtocolException;
        //  296    308    430    434    Any
        //  308    325    259    265    Any
        //  326    333    341    350    Lcom/sun/mail/iap/ProtocolException;
        //  326    333    350    358    Any
        //  333    338    430    434    Any
        //  342    347    430    434    Any
        //  351    358    430    434    Any
        //  358    379    259    265    Any
        //  386    399    222    227    Any
        //  399    419    222    227    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 241, Size: 241
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void releaseStoreProtocol(final IMAPProtocol imapProtocol) {
        synchronized (this) {
            if (imapProtocol != this.protocol) {
                ((IMAPStore)this.store).releaseStoreProtocol(imapProtocol);
            }
        }
    }
    
    public void removeACL(final String s) throws MessagingException {
        this.doOptionalCommand("ACL not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                imapProtocol.deleteACL(IMAPFolder.this.fullName, s);
                return null;
            }
        });
    }
    
    public void removeRights(final ACL acl) throws MessagingException {
        this.setACL(acl, '-');
    }
    
    @Override
    public boolean renameTo(final Folder folder) throws MessagingException {
        boolean b = false;
        synchronized (this) {
            this.checkClosed();
            this.checkExists();
            if (folder.getStore() != this.store) {
                throw new MessagingException("Can't rename across Stores");
            }
        }
        final Folder folder2;
        if (this.doCommandIgnoreFailure((ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                imapProtocol.rename(IMAPFolder.this.fullName, folder2.getFullName());
                return Boolean.TRUE;
            }
        }) != null) {
            this.exists = false;
            this.attributes = null;
            this.notifyFolderRenamedListeners(folder2);
            b = true;
        }
        // monitorexit(this)
        return b;
    }
    
    @Override
    public Message[] search(final SearchTerm searchTerm) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpened();
            try {
                Message[] array = null;
                synchronized (this.messageCacheLock) {
                    final int[] search = this.getProtocol().search(searchTerm);
                    if (search != null) {
                        array = new IMAPMessage[search.length];
                        for (int i = 0; i < search.length; ++i) {
                            array[i] = this.getMessageBySeqNumber(search[i]);
                        }
                    }
                    return array;
                }
            }
            catch (CommandFailedException array) {
                array = super.search(searchTerm);
            }
            catch (SearchException array) {
                array = super.search(searchTerm);
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this, ex.getMessage());
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
        finally {}
    }
    
    @Override
    public Message[] search(final SearchTerm searchTerm, Message[] o) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpened();
            if (o.length != 0) {
                try {
                    final Message[] array = null;
                    synchronized (this.messageCacheLock) {
                        this.getProtocol();
                        if (Utility.toMessageSet((Message[])o, null) == null) {
                            throw new MessageRemovedException("Messages have been removed");
                        }
                        goto Label_0081;
                    }
                }
                catch (CommandFailedException ex3) {
                    o = super.search(searchTerm, (Message[])o);
                }
                catch (SearchException ex4) {
                    o = super.search(searchTerm, (Message[])o);
                }
                catch (ConnectionException ex) {
                    throw new FolderClosedException(this, ex.getMessage());
                }
                catch (ProtocolException ex2) {
                    throw new MessagingException(ex2.getMessage(), ex2);
                }
            }
            return (Message[])o;
        }
        finally {}
    }
    
    @Override
    public void setFlags(final Message[] array, final Flags flags, final boolean b) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkOpened();
            this.checkFlags(flags);
            if (array.length == 0) {
                return;
            }
            final Object messageCacheLock = this.messageCacheLock;
            // monitorenter(messageCacheLock)
            try {
                this.getProtocol();
                if (Utility.toMessageSet(array, null) == null) {
                    throw new MessageRemovedException("Messages have been removed");
                }
                goto Label_0084;
            }
            catch (ConnectionException ex) {
                try {
                    throw new FolderClosedException(this, ex.getMessage());
                }
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
        finally {}
    }
    
    public void setQuota(final Quota quota) throws MessagingException {
        this.doOptionalCommand("QUOTA not supported", (ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                imapProtocol.setQuota(quota);
                return null;
            }
        });
    }
    
    @Override
    public void setSubscribed(final boolean b) throws MessagingException {
        synchronized (this) {
            this.doCommandIgnoreFailure((ProtocolCommand)new ProtocolCommand() {
                @Override
                public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                    if (b) {
                        imapProtocol.subscribe(IMAPFolder.this.fullName);
                    }
                    else {
                        imapProtocol.unsubscribe(IMAPFolder.this.fullName);
                    }
                    return null;
                }
            });
        }
    }
    
    void waitIfIdle() throws ProtocolException {
        assert Thread.holdsLock(this.messageCacheLock);
    Label_0051:
        while (true) {
            break Label_0051;
            while (true) {
                try {
                    this.messageCacheLock.wait();
                    if (this.idleState == 0) {
                        return;
                    }
                }
                catch (InterruptedException ex) {
                    continue Label_0051;
                }
                if (this.idleState == 1) {
                    this.protocol.idleAbort();
                    this.idleState = 2;
                }
            }
            break;
        }
    }
    
    public static class FetchProfileItem extends Item
    {
        public static final FetchProfileItem HEADERS;
        public static final FetchProfileItem SIZE;
        
        static {
            HEADERS = new FetchProfileItem("HEADERS");
            SIZE = new FetchProfileItem("SIZE");
        }
        
        protected FetchProfileItem(final String s) {
            super(s);
        }
    }
    
    public interface ProtocolCommand
    {
        Object doCommand(final IMAPProtocol p0) throws ProtocolException;
    }
}
