package com.sun.mail.imap;

import java.io.*;

class LengthCounter extends OutputStream
{
    private byte[] buf;
    private int maxsize;
    private int size;
    
    public LengthCounter(final int maxsize) {
        this.size = 0;
        this.buf = new byte[8192];
        this.maxsize = maxsize;
    }
    
    public byte[] getBytes() {
        return this.buf;
    }
    
    public int getSize() {
        return this.size;
    }
    
    @Override
    public void write(final int n) {
        final int size = this.size + 1;
        if (this.buf != null) {
            if (size > this.maxsize && this.maxsize >= 0) {
                this.buf = null;
            }
            else if (size > this.buf.length) {
                final byte[] buf = new byte[Math.max(this.buf.length << 1, size)];
                System.arraycopy(this.buf, 0, buf, 0, this.size);
                (this.buf = buf)[this.size] = (byte)n;
            }
            else {
                this.buf[this.size] = (byte)n;
            }
        }
        this.size = size;
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, final int n, final int n2) {
        if (n < 0 || n > array.length || n2 < 0 || n + n2 > array.length || n + n2 < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (n2 == 0) {
            return;
        }
        final int size = this.size + n2;
        if (this.buf != null) {
            if (size > this.maxsize && this.maxsize >= 0) {
                this.buf = null;
            }
            else if (size > this.buf.length) {
                final byte[] buf = new byte[Math.max(this.buf.length << 1, size)];
                System.arraycopy(this.buf, 0, buf, 0, this.size);
                System.arraycopy(array, n, this.buf = buf, this.size, n2);
            }
            else {
                System.arraycopy(array, n, this.buf, this.size, n2);
            }
        }
        this.size = size;
    }
}
