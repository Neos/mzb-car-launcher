package com.sun.mail.imap;

import java.util.*;

public class Rights implements Cloneable
{
    private boolean[] rights;
    
    public Rights() {
        this.rights = new boolean[128];
    }
    
    public Rights(final Right right) {
        (this.rights = new boolean[128])[right.right] = true;
    }
    
    public Rights(final Rights rights) {
        this.rights = new boolean[128];
        System.arraycopy(rights.rights, 0, this.rights, 0, this.rights.length);
    }
    
    public Rights(final String s) {
        this.rights = new boolean[128];
        for (int i = 0; i < s.length(); ++i) {
            this.add(Right.getInstance(s.charAt(i)));
        }
    }
    
    public void add(final Right right) {
        this.rights[right.right] = true;
    }
    
    public void add(final Rights rights) {
        for (int i = 0; i < rights.rights.length; ++i) {
            if (rights.rights[i]) {
                this.rights[i] = true;
            }
        }
    }
    
    public Object clone() {
        Object o = null;
        try {
            final Rights rights = (Rights)(o = super.clone());
            rights.rights = new boolean[128];
            o = rights;
            System.arraycopy(this.rights, 0, rights.rights, 0, this.rights.length);
            return rights;
        }
        catch (CloneNotSupportedException ex) {
            return o;
        }
    }
    
    public boolean contains(final Right right) {
        return this.rights[right.right];
    }
    
    public boolean contains(final Rights rights) {
        for (int i = 0; i < rights.rights.length; ++i) {
            if (rights.rights[i] && !this.rights[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Rights) {
            final Rights rights = (Rights)o;
            for (int i = 0; i < rights.rights.length; ++i) {
                if (rights.rights[i] != this.rights[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public Right[] getRights() {
        final Vector<Right> vector = new Vector<Right>();
        for (int i = 0; i < this.rights.length; ++i) {
            if (this.rights[i]) {
                vector.addElement(Right.getInstance((char)i));
            }
        }
        final Right[] array = new Right[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    @Override
    public int hashCode() {
        int n = 0;
        int n2;
        for (int i = 0; i < this.rights.length; ++i, n = n2) {
            n2 = n;
            if (this.rights[i]) {
                n2 = n + 1;
            }
        }
        return n;
    }
    
    public void remove(final Right right) {
        this.rights[right.right] = false;
    }
    
    public void remove(final Rights rights) {
        for (int i = 0; i < rights.rights.length; ++i) {
            if (rights.rights[i]) {
                this.rights[i] = false;
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < this.rights.length; ++i) {
            if (this.rights[i]) {
                sb.append((char)i);
            }
        }
        return sb.toString();
    }
    
    public static final class Right
    {
        public static final Right ADMINISTER;
        public static final Right CREATE;
        public static final Right DELETE;
        public static final Right INSERT;
        public static final Right KEEP_SEEN;
        public static final Right LOOKUP;
        public static final Right POST;
        public static final Right READ;
        public static final Right WRITE;
        private static Right[] cache;
        char right;
        
        static {
            Right.cache = new Right[128];
            LOOKUP = getInstance('l');
            READ = getInstance('r');
            KEEP_SEEN = getInstance('s');
            WRITE = getInstance('w');
            INSERT = getInstance('i');
            POST = getInstance('p');
            CREATE = getInstance('c');
            DELETE = getInstance('d');
            ADMINISTER = getInstance('a');
        }
        
        private Right(final char right) {
            if (right >= '\u0080') {
                throw new IllegalArgumentException("Right must be ASCII");
            }
            this.right = right;
        }
        
        public static Right getInstance(final char c) {
            // monitorenter(Right.class)
            if (c >= '\u0080') {
                try {
                    throw new IllegalArgumentException("Right must be ASCII");
                }
                finally {
                }
                // monitorexit(Right.class)
            }
            if (Right.cache[c] == null) {
                Right.cache[c] = new Right(c);
            }
            // monitorexit(Right.class)
            return Right.cache[c];
        }
        
        @Override
        public String toString() {
            return String.valueOf(this.right);
        }
    }
}
