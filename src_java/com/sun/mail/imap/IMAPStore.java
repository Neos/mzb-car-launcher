package com.sun.mail.imap;

import java.io.*;
import java.util.*;
import com.sun.mail.imap.protocol.*;
import javax.mail.*;
import com.sun.mail.iap.*;

public class IMAPStore extends Store implements QuotaAwareStore, ResponseHandler
{
    public static final int RESPONSE = 1000;
    private int appendBufferSize;
    private String authorizationID;
    private int blksize;
    private volatile boolean connected;
    private int defaultPort;
    private boolean disableAuthLogin;
    private boolean disableAuthPlain;
    private boolean enableImapEvents;
    private boolean enableSASL;
    private boolean enableStartTLS;
    private boolean forcePasswordRefresh;
    private String host;
    private boolean isSSL;
    private int minIdleTime;
    private String name;
    private Namespaces namespaces;
    private PrintStream out;
    private String password;
    private ConnectionPool pool;
    private int port;
    private String proxyAuthUser;
    private String[] saslMechanisms;
    private String saslRealm;
    private int statusCacheTimeout;
    private String user;
    
    public IMAPStore(final Session session, final URLName urlName) {
        this(session, urlName, "imap", 143, false);
    }
    
    protected IMAPStore(Session property, final URLName urlName, String protocol, int defaultPort, final boolean isSSL) {
        super(property, urlName);
        this.name = "imap";
        this.defaultPort = 143;
        this.isSSL = false;
        this.port = -1;
        this.blksize = 16384;
        this.statusCacheTimeout = 1000;
        this.appendBufferSize = -1;
        this.minIdleTime = 10;
        this.disableAuthLogin = false;
        this.disableAuthPlain = false;
        this.enableStartTLS = false;
        this.enableSASL = false;
        this.forcePasswordRefresh = false;
        this.enableImapEvents = false;
        this.connected = false;
        this.pool = new ConnectionPool();
        if (urlName != null) {
            protocol = urlName.getProtocol();
        }
        this.name = protocol;
        this.defaultPort = defaultPort;
        this.isSSL = isSSL;
        ConnectionPool.access$0(this.pool, System.currentTimeMillis());
        this.debug = property.getDebug();
        this.out = property.getDebugOut();
        if (this.out == null) {
            this.out = System.out;
        }
        final String property2 = property.getProperty("mail." + protocol + ".connectionpool.debug");
        if (property2 != null && property2.equalsIgnoreCase("true")) {
            ConnectionPool.access$1(this.pool, true);
        }
        final String property3 = property.getProperty("mail." + protocol + ".partialfetch");
        Label_1530: {
            if (property3 == null || !property3.equalsIgnoreCase("false")) {
                break Label_1530;
            }
            this.blksize = -1;
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.partialfetch: false");
            }
            final String property4;
            Label_0273: {
                property4 = property.getProperty("mail." + protocol + ".statuscachetimeout");
            }
            if (property4 != null) {
                this.statusCacheTimeout = Integer.parseInt(property4);
                if (this.debug) {
                    this.out.println("DEBUG: mail.imap.statuscachetimeout: " + this.statusCacheTimeout);
                }
            }
            final String property5 = property.getProperty("mail." + protocol + ".appendbuffersize");
            if (property5 != null) {
                this.appendBufferSize = Integer.parseInt(property5);
                if (this.debug) {
                    this.out.println("DEBUG: mail.imap.appendbuffersize: " + this.appendBufferSize);
                }
            }
            final String property6 = property.getProperty("mail." + protocol + ".minidletime");
            if (property6 != null) {
                this.minIdleTime = Integer.parseInt(property6);
                if (this.debug) {
                    this.out.println("DEBUG: mail.imap.minidletime: " + this.minIdleTime);
                }
            }
            final String property7 = property.getProperty("mail." + protocol + ".connectionpoolsize");
            Label_0575: {
                if (property7 == null) {
                    break Label_0575;
                }
                while (true) {
                    try {
                        defaultPort = Integer.parseInt(property7);
                        if (defaultPort > 0) {
                            ConnectionPool.access$2(this.pool, defaultPort);
                        }
                        if (this.pool.debug) {
                            this.out.println("DEBUG: mail.imap.connectionpoolsize: " + this.pool.poolSize);
                        }
                        final String property8 = property.getProperty("mail." + protocol + ".connectionpooltimeout");
                        Label_0665: {
                            if (property8 == null) {
                                break Label_0665;
                            }
                            try {
                                defaultPort = Integer.parseInt(property8);
                                if (defaultPort > 0) {
                                    ConnectionPool.access$5(this.pool, defaultPort);
                                }
                                if (this.pool.debug) {
                                    this.out.println("DEBUG: mail.imap.connectionpooltimeout: " + this.pool.clientTimeoutInterval);
                                }
                                final String property9 = property.getProperty("mail." + protocol + ".servertimeout");
                                Label_0755: {
                                    if (property9 == null) {
                                        break Label_0755;
                                    }
                                    try {
                                        defaultPort = Integer.parseInt(property9);
                                        if (defaultPort > 0) {
                                            ConnectionPool.access$7(this.pool, defaultPort);
                                        }
                                        if (this.pool.debug) {
                                            this.out.println("DEBUG: mail.imap.servertimeout: " + this.pool.serverTimeoutInterval);
                                        }
                                        final String property10 = property.getProperty("mail." + protocol + ".separatestoreconnection");
                                        if (property10 != null && property10.equalsIgnoreCase("true")) {
                                            if (this.pool.debug) {
                                                this.out.println("DEBUG: dedicate a store connection");
                                            }
                                            ConnectionPool.access$9(this.pool, true);
                                        }
                                        final String property11 = property.getProperty("mail." + protocol + ".proxyauth.user");
                                        if (property11 != null) {
                                            this.proxyAuthUser = property11;
                                            if (this.debug) {
                                                this.out.println("DEBUG: mail.imap.proxyauth.user: " + this.proxyAuthUser);
                                            }
                                        }
                                        final String property12 = property.getProperty("mail." + protocol + ".auth.login.disable");
                                        if (property12 != null && property12.equalsIgnoreCase("true")) {
                                            if (this.debug) {
                                                this.out.println("DEBUG: disable AUTH=LOGIN");
                                            }
                                            this.disableAuthLogin = true;
                                        }
                                        final String property13 = property.getProperty("mail." + protocol + ".auth.plain.disable");
                                        if (property13 != null && property13.equalsIgnoreCase("true")) {
                                            if (this.debug) {
                                                this.out.println("DEBUG: disable AUTH=PLAIN");
                                            }
                                            this.disableAuthPlain = true;
                                        }
                                        final String property14 = property.getProperty("mail." + protocol + ".starttls.enable");
                                        if (property14 != null && property14.equalsIgnoreCase("true")) {
                                            if (this.debug) {
                                                this.out.println("DEBUG: enable STARTTLS");
                                            }
                                            this.enableStartTLS = true;
                                        }
                                        final String property15 = property.getProperty("mail." + protocol + ".sasl.enable");
                                        if (property15 != null && property15.equalsIgnoreCase("true")) {
                                            if (this.debug) {
                                                this.out.println("DEBUG: enable SASL");
                                            }
                                            this.enableSASL = true;
                                        }
                                        if (this.enableSASL) {
                                            final String property16 = property.getProperty("mail." + protocol + ".sasl.mechanisms");
                                            if (property16 != null && property16.length() > 0) {
                                                if (this.debug) {
                                                    this.out.println("DEBUG: SASL mechanisms allowed: " + property16);
                                                }
                                                final Vector<String> vector = new Vector<String>(5);
                                                final StringTokenizer stringTokenizer = new StringTokenizer(property16, " ,");
                                                while (stringTokenizer.hasMoreTokens()) {
                                                    final String nextToken = stringTokenizer.nextToken();
                                                    if (nextToken.length() > 0) {
                                                        vector.addElement(nextToken);
                                                    }
                                                }
                                                vector.copyInto(this.saslMechanisms = new String[vector.size()]);
                                            }
                                        }
                                        final String property17 = property.getProperty("mail." + protocol + ".sasl.authorizationid");
                                        if (property17 != null) {
                                            this.authorizationID = property17;
                                            if (this.debug) {
                                                this.out.println("DEBUG: mail.imap.sasl.authorizationid: " + this.authorizationID);
                                            }
                                        }
                                        final String property18 = property.getProperty("mail." + protocol + ".sasl.realm");
                                        if (property18 != null) {
                                            this.saslRealm = property18;
                                            if (this.debug) {
                                                this.out.println("DEBUG: mail.imap.sasl.realm: " + this.saslRealm);
                                            }
                                        }
                                        final String property19 = property.getProperty("mail." + protocol + ".forcepasswordrefresh");
                                        if (property19 != null && property19.equalsIgnoreCase("true")) {
                                            if (this.debug) {
                                                this.out.println("DEBUG: enable forcePasswordRefresh");
                                            }
                                            this.forcePasswordRefresh = true;
                                        }
                                        property = (Session)property.getProperty("mail." + protocol + ".enableimapevents");
                                        if (property != null && ((String)property).equalsIgnoreCase("true")) {
                                            if (this.debug) {
                                                this.out.println("DEBUG: enable IMAP events");
                                            }
                                            this.enableImapEvents = true;
                                        }
                                        return;
                                        // iftrue(Label_1569:, property20 == null)
                                    Label_1569:
                                        while (true) {
                                            final String property20;
                                            this.blksize = Integer.parseInt(property20);
                                            break Label_1569;
                                            property20 = property.getProperty("mail." + protocol + ".fetchsize");
                                            continue;
                                        }
                                        // iftrue(Label_0273:, !this.debug)
                                        this.out.println("DEBUG: mail.imap.fetchsize: " + this.blksize);
                                    }
                                    catch (NumberFormatException ex) {}
                                }
                            }
                            catch (NumberFormatException ex2) {}
                        }
                    }
                    catch (NumberFormatException ex3) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    private void checkConnected() {
        assert Thread.holdsLock(this);
        if (!this.connected) {
            super.setConnected(false);
            throw new IllegalStateException("Not connected");
        }
    }
    
    private void cleanup() {
        this.cleanup(false);
    }
    
    private void cleanup(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //     4: ifeq            31
        //     7: aload_0        
        //     8: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    11: new             Ljava/lang/StringBuilder;
        //    14: dup            
        //    15: ldc_w           "DEBUG: IMAPStore cleanup, force "
        //    18: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    21: iload_1        
        //    22: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //    25: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    28: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    31: aconst_null    
        //    32: astore          5
        //    34: aload_0        
        //    35: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    38: astore          6
        //    40: aload           6
        //    42: monitorenter   
        //    43: aload_0        
        //    44: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    47: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$13:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    50: ifnull          124
        //    53: iconst_0       
        //    54: istore_2       
        //    55: aload_0        
        //    56: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    59: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$13:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    62: astore          4
        //    64: aload_0        
        //    65: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    68: aconst_null    
        //    69: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$14:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Ljava/util/Vector;)V
        //    72: aload           6
        //    74: monitorexit    
        //    75: iload_2        
        //    76: ifeq            141
        //    79: aload_0        
        //    80: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    83: astore          4
        //    85: aload           4
        //    87: monitorenter   
        //    88: aload_0        
        //    89: iload_1        
        //    90: invokespecial   com/sun/mail/imap/IMAPStore.emptyConnectionPool:(Z)V
        //    93: aload           4
        //    95: monitorexit    
        //    96: aload_0        
        //    97: iconst_0       
        //    98: putfield        com/sun/mail/imap/IMAPStore.connected:Z
        //   101: aload_0        
        //   102: iconst_3       
        //   103: invokevirtual   com/sun/mail/imap/IMAPStore.notifyConnectionListeners:(I)V
        //   106: aload_0        
        //   107: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //   110: ifeq            123
        //   113: aload_0        
        //   114: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   117: ldc_w           "DEBUG: IMAPStore cleanup done"
        //   120: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   123: return         
        //   124: iconst_1       
        //   125: istore_2       
        //   126: aload           5
        //   128: astore          4
        //   130: goto            72
        //   133: astore          4
        //   135: aload           6
        //   137: monitorexit    
        //   138: aload           4
        //   140: athrow         
        //   141: iconst_0       
        //   142: istore_2       
        //   143: aload           4
        //   145: invokevirtual   java/util/Vector.size:()I
        //   148: istore_3       
        //   149: aload           4
        //   151: astore          5
        //   153: iload_2        
        //   154: iload_3        
        //   155: if_icmpge       34
        //   158: aload           4
        //   160: iload_2        
        //   161: invokevirtual   java/util/Vector.elementAt:(I)Ljava/lang/Object;
        //   164: checkcast       Lcom/sun/mail/imap/IMAPFolder;
        //   167: astore          5
        //   169: iload_1        
        //   170: ifeq            198
        //   173: aload_0        
        //   174: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //   177: ifeq            190
        //   180: aload_0        
        //   181: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   184: ldc_w           "DEBUG: force folder to close"
        //   187: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   190: aload           5
        //   192: invokevirtual   com/sun/mail/imap/IMAPFolder.forceClose:()V
        //   195: goto            239
        //   198: aload_0        
        //   199: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //   202: ifeq            215
        //   205: aload_0        
        //   206: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   209: ldc_w           "DEBUG: close folder"
        //   212: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   215: aload           5
        //   217: iconst_0       
        //   218: invokevirtual   com/sun/mail/imap/IMAPFolder.close:(Z)V
        //   221: goto            239
        //   224: astore          5
        //   226: goto            239
        //   229: astore          5
        //   231: aload           4
        //   233: monitorexit    
        //   234: aload           5
        //   236: athrow         
        //   237: astore          5
        //   239: iload_2        
        //   240: iconst_1       
        //   241: iadd           
        //   242: istore_2       
        //   243: goto            149
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  43     53     133    141    Any
        //  55     72     133    141    Any
        //  72     75     133    141    Any
        //  88     96     229    237    Any
        //  135    138    133    141    Any
        //  173    190    224    229    Ljavax/mail/MessagingException;
        //  173    190    237    239    Ljava/lang/IllegalStateException;
        //  190    195    224    229    Ljavax/mail/MessagingException;
        //  190    195    237    239    Ljava/lang/IllegalStateException;
        //  198    215    224    229    Ljavax/mail/MessagingException;
        //  198    215    237    239    Ljava/lang/IllegalStateException;
        //  215    221    224    229    Ljavax/mail/MessagingException;
        //  215    221    237    239    Ljava/lang/IllegalStateException;
        //  231    234    229    237    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0123:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void emptyConnectionPool(final boolean b) {
        while (true) {
            while (true) {
                int n;
                synchronized (this.pool) {
                    n = this.pool.authenticatedConnections.size() - 1;
                    if (n < 0) {
                        this.pool.authenticatedConnections.removeAllElements();
                        // monitorexit(this.pool)
                        if (this.pool.debug) {
                            this.out.println("DEBUG: removed all authenticated connections");
                        }
                        return;
                    }
                    try {
                        final IMAPProtocol imapProtocol = this.pool.authenticatedConnections.elementAt(n);
                        imapProtocol.removeResponseHandler(this);
                        if (b) {
                            imapProtocol.disconnect();
                        }
                        else {
                            imapProtocol.logout();
                        }
                    }
                    catch (ProtocolException ex) {}
                }
                --n;
                continue;
            }
        }
    }
    
    private Namespaces getNamespaces() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/sun/mail/imap/IMAPStore.checkConnected:()V
        //     6: aconst_null    
        //     7: astore_3       
        //     8: aconst_null    
        //     9: astore_1       
        //    10: aconst_null    
        //    11: astore          4
        //    13: aconst_null    
        //    14: astore          5
        //    16: aload_0        
        //    17: getfield        com/sun/mail/imap/IMAPStore.namespaces:Lcom/sun/mail/imap/protocol/Namespaces;
        //    20: astore_2       
        //    21: aload_2        
        //    22: ifnonnull       61
        //    25: aload_0        
        //    26: invokevirtual   com/sun/mail/imap/IMAPStore.getStoreProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    29: astore_2       
        //    30: aload_2        
        //    31: astore          5
        //    33: aload_2        
        //    34: astore_3       
        //    35: aload_2        
        //    36: astore_1       
        //    37: aload_2        
        //    38: astore          4
        //    40: aload_0        
        //    41: aload_2        
        //    42: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.namespace:()Lcom/sun/mail/imap/protocol/Namespaces;
        //    45: putfield        com/sun/mail/imap/IMAPStore.namespaces:Lcom/sun/mail/imap/protocol/Namespaces;
        //    48: aload_0        
        //    49: aload_2        
        //    50: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    53: aload_2        
        //    54: ifnonnull       61
        //    57: aload_0        
        //    58: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //    61: aload_0        
        //    62: getfield        com/sun/mail/imap/IMAPStore.namespaces:Lcom/sun/mail/imap/protocol/Namespaces;
        //    65: astore_1       
        //    66: aload_0        
        //    67: monitorexit    
        //    68: aload_1        
        //    69: areturn        
        //    70: astore_1       
        //    71: aload_0        
        //    72: aload           5
        //    74: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    77: aload           5
        //    79: ifnonnull       61
        //    82: aload_0        
        //    83: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //    86: goto            61
        //    89: astore_1       
        //    90: aload_0        
        //    91: monitorexit    
        //    92: aload_1        
        //    93: athrow         
        //    94: astore_2       
        //    95: aload_3        
        //    96: astore_1       
        //    97: new             Ljavax/mail/StoreClosedException;
        //   100: dup            
        //   101: aload_0        
        //   102: aload_2        
        //   103: invokevirtual   com/sun/mail/iap/ConnectionException.getMessage:()Ljava/lang/String;
        //   106: invokespecial   javax/mail/StoreClosedException.<init>:(Ljavax/mail/Store;Ljava/lang/String;)V
        //   109: athrow         
        //   110: astore_2       
        //   111: aload_0        
        //   112: aload_1        
        //   113: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   116: aload_1        
        //   117: ifnonnull       124
        //   120: aload_0        
        //   121: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //   124: aload_2        
        //   125: athrow         
        //   126: astore_2       
        //   127: aload           4
        //   129: astore_1       
        //   130: new             Ljavax/mail/MessagingException;
        //   133: dup            
        //   134: aload_2        
        //   135: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   138: aload_2        
        //   139: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   142: athrow         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  2      6      89     94     Any
        //  16     21     89     94     Any
        //  25     30     70     89     Lcom/sun/mail/iap/BadCommandException;
        //  25     30     94     110    Lcom/sun/mail/iap/ConnectionException;
        //  25     30     126    143    Lcom/sun/mail/iap/ProtocolException;
        //  25     30     110    126    Any
        //  40     48     70     89     Lcom/sun/mail/iap/BadCommandException;
        //  40     48     94     110    Lcom/sun/mail/iap/ConnectionException;
        //  40     48     126    143    Lcom/sun/mail/iap/ProtocolException;
        //  40     48     110    126    Any
        //  48     53     89     94     Any
        //  57     61     89     94     Any
        //  61     66     89     94     Any
        //  71     77     89     94     Any
        //  82     86     89     94     Any
        //  97     110    110    126    Any
        //  111    116    89     94     Any
        //  120    124    89     94     Any
        //  124    126    89     94     Any
        //  130    143    110    126    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0061:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void login(final IMAPProtocol imapProtocol, final String s, final String s2) throws ProtocolException {
        if (this.enableStartTLS && imapProtocol.hasCapability("STARTTLS")) {
            imapProtocol.startTLS();
            imapProtocol.capability();
        }
        if (!imapProtocol.isAuthenticated()) {
            imapProtocol.getCapabilities().put("__PRELOGIN__", "");
            if (this.authorizationID == null) {
                goto Label_0125;
            }
            final String authorizationID = this.authorizationID;
            if (this.enableSASL) {
                imapProtocol.sasllogin(this.saslMechanisms, this.saslRealm, authorizationID, s, s2);
            }
            if (!imapProtocol.isAuthenticated()) {
                goto Label_0147;
            }
            if (this.proxyAuthUser != null) {
                imapProtocol.proxyauth(this.proxyAuthUser);
            }
            if (imapProtocol.hasCapability("__PRELOGIN__")) {
                try {
                    imapProtocol.capability();
                }
                catch (ConnectionException ex) {
                    throw ex;
                }
                catch (ProtocolException ex2) {}
            }
        }
    }
    
    private Folder[] namespaceToFolders(final Namespaces.Namespace[] array, final String s) {
        final Folder[] array2 = new Folder[array.length];
        for (int i = 0; i < array2.length; ++i) {
            final String prefix = array[i].prefix;
            String s2;
            if (s == null) {
                final int length = prefix.length();
                s2 = prefix;
                if (length > 0) {
                    s2 = prefix;
                    if (prefix.charAt(length - 1) == array[i].delimiter) {
                        s2 = prefix.substring(0, length - 1);
                    }
                }
            }
            else {
                s2 = String.valueOf(prefix) + s;
            }
            array2[i] = new IMAPFolder(s2, array[i].delimiter, this, s == null);
        }
        return array2;
    }
    
    private void timeoutConnections() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //     4: astore_2       
        //     5: aload_2        
        //     6: monitorenter   
        //     7: invokestatic    java/lang/System.currentTimeMillis:()J
        //    10: aload_0        
        //    11: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    14: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$16:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)J
        //    17: lsub           
        //    18: aload_0        
        //    19: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    22: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$17:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)J
        //    25: lcmp           
        //    26: ifle            144
        //    29: aload_0        
        //    30: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    33: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    36: invokevirtual   java/util/Vector.size:()I
        //    39: iconst_1       
        //    40: if_icmple       144
        //    43: aload_0        
        //    44: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    47: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //    50: ifeq            117
        //    53: aload_0        
        //    54: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    57: new             Ljava/lang/StringBuilder;
        //    60: dup            
        //    61: ldc_w           "DEBUG: checking for connections to prune: "
        //    64: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    67: invokestatic    java/lang/System.currentTimeMillis:()J
        //    70: aload_0        
        //    71: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    74: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$16:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)J
        //    77: lsub           
        //    78: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //    81: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    84: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    87: aload_0        
        //    88: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    91: new             Ljava/lang/StringBuilder;
        //    94: dup            
        //    95: ldc_w           "DEBUG: clientTimeoutInterval: "
        //    98: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   101: aload_0        
        //   102: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   105: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$6:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)J
        //   108: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   111: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   114: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   117: aload_0        
        //   118: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   121: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   124: invokevirtual   java/util/Vector.size:()I
        //   127: iconst_1       
        //   128: isub           
        //   129: istore_1       
        //   130: iload_1        
        //   131: ifgt            147
        //   134: aload_0        
        //   135: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   138: invokestatic    java/lang/System.currentTimeMillis:()J
        //   141: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$0:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;J)V
        //   144: aload_2        
        //   145: monitorexit    
        //   146: return         
        //   147: aload_0        
        //   148: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   151: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   154: iload_1        
        //   155: invokevirtual   java/util/Vector.elementAt:(I)Ljava/lang/Object;
        //   158: checkcast       Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   161: astore_3       
        //   162: aload_0        
        //   163: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   166: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //   169: ifeq            203
        //   172: aload_0        
        //   173: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   176: new             Ljava/lang/StringBuilder;
        //   179: dup            
        //   180: ldc_w           "DEBUG: protocol last used: "
        //   183: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   186: invokestatic    java/lang/System.currentTimeMillis:()J
        //   189: aload_3        
        //   190: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.getTimestamp:()J
        //   193: lsub           
        //   194: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   197: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   200: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   203: invokestatic    java/lang/System.currentTimeMillis:()J
        //   206: aload_3        
        //   207: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.getTimestamp:()J
        //   210: lsub           
        //   211: aload_0        
        //   212: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   215: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$6:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)J
        //   218: lcmp           
        //   219: ifle            272
        //   222: aload_0        
        //   223: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   226: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //   229: ifeq            252
        //   232: aload_0        
        //   233: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   236: ldc_w           "DEBUG: authenticated connection timed out"
        //   239: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   242: aload_0        
        //   243: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   246: ldc_w           "DEBUG: logging out the connection"
        //   249: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   252: aload_3        
        //   253: aload_0        
        //   254: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.removeResponseHandler:(Lcom/sun/mail/iap/ResponseHandler;)V
        //   257: aload_0        
        //   258: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   261: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   264: iload_1        
        //   265: invokevirtual   java/util/Vector.removeElementAt:(I)V
        //   268: aload_3        
        //   269: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.logout:()V
        //   272: iload_1        
        //   273: iconst_1       
        //   274: isub           
        //   275: istore_1       
        //   276: goto            130
        //   279: astore_3       
        //   280: aload_2        
        //   281: monitorexit    
        //   282: aload_3        
        //   283: athrow         
        //   284: astore_3       
        //   285: goto            272
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  7      117    279    284    Any
        //  117    130    279    284    Any
        //  134    144    279    284    Any
        //  144    146    279    284    Any
        //  147    203    279    284    Any
        //  203    252    279    284    Any
        //  252    268    279    284    Any
        //  268    272    284    288    Lcom/sun/mail/iap/ProtocolException;
        //  268    272    279    284    Any
        //  280    282    279    284    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0272:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void waitIfIdle() throws ProtocolException {
        assert Thread.holdsLock(this.pool);
    Label_0060:
        while (true) {
            break Label_0060;
            while (true) {
                try {
                    this.pool.wait();
                    if (this.pool.idleState == 0) {
                        return;
                    }
                }
                catch (InterruptedException ex) {
                    continue Label_0060;
                }
                if (this.pool.idleState == 1) {
                    this.pool.idleProtocol.idleAbort();
                    ConnectionPool.access$20(this.pool, 2);
                }
            }
            break;
        }
    }
    
    boolean allowReadOnlySelect() {
        final String property = this.session.getProperty("mail." + this.name + ".allowreadonlyselect");
        return property != null && property.equalsIgnoreCase("true");
    }
    
    @Override
    public void close() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   javax/mail/Store.isConnected:()Z
        //     6: istore_1       
        //     7: iload_1        
        //     8: ifne            14
        //    11: aload_0        
        //    12: monitorexit    
        //    13: return         
        //    14: aconst_null    
        //    15: astore          5
        //    17: aconst_null    
        //    18: astore          4
        //    20: aload           4
        //    22: astore_3       
        //    23: aload           5
        //    25: astore_2       
        //    26: aload_0        
        //    27: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    30: astore          6
        //    32: aload           4
        //    34: astore_3       
        //    35: aload           5
        //    37: astore_2       
        //    38: aload           6
        //    40: monitorenter   
        //    41: aload_0        
        //    42: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    45: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    48: invokevirtual   java/util/Vector.isEmpty:()Z
        //    51: istore_1       
        //    52: aload           6
        //    54: monitorexit    
        //    55: iload_1        
        //    56: ifeq            161
        //    59: aload           4
        //    61: astore_3       
        //    62: aload           5
        //    64: astore_2       
        //    65: aload_0        
        //    66: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    69: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //    72: ifeq            91
        //    75: aload           4
        //    77: astore_3       
        //    78: aload           5
        //    80: astore_2       
        //    81: aload_0        
        //    82: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    85: ldc_w           "DEBUG: close() - no connections "
        //    88: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    91: aload           4
        //    93: astore_3       
        //    94: aload           5
        //    96: astore_2       
        //    97: aload_0        
        //    98: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //   101: aload_0        
        //   102: aconst_null    
        //   103: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   106: goto            11
        //   109: astore_2       
        //   110: aload_0        
        //   111: monitorexit    
        //   112: aload_2        
        //   113: athrow         
        //   114: astore          7
        //   116: aload           6
        //   118: monitorexit    
        //   119: aload           4
        //   121: astore_3       
        //   122: aload           5
        //   124: astore_2       
        //   125: aload           7
        //   127: athrow         
        //   128: astore          4
        //   130: aload_3        
        //   131: astore_2       
        //   132: aload_0        
        //   133: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //   136: aload_3        
        //   137: astore_2       
        //   138: new             Ljavax/mail/MessagingException;
        //   141: dup            
        //   142: aload           4
        //   144: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   147: aload           4
        //   149: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   152: athrow         
        //   153: astore_3       
        //   154: aload_0        
        //   155: aload_2        
        //   156: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   159: aload_3        
        //   160: athrow         
        //   161: aload           4
        //   163: astore_3       
        //   164: aload           5
        //   166: astore_2       
        //   167: aload_0        
        //   168: invokevirtual   com/sun/mail/imap/IMAPStore.getStoreProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   171: astore          4
        //   173: aload           4
        //   175: astore_3       
        //   176: aload           4
        //   178: astore_2       
        //   179: aload_0        
        //   180: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   183: astore          5
        //   185: aload           4
        //   187: astore_3       
        //   188: aload           4
        //   190: astore_2       
        //   191: aload           5
        //   193: monitorenter   
        //   194: aload_0        
        //   195: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   198: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   201: aload           4
        //   203: invokevirtual   java/util/Vector.removeElement:(Ljava/lang/Object;)Z
        //   206: pop            
        //   207: aload           5
        //   209: monitorexit    
        //   210: aload           4
        //   212: astore_3       
        //   213: aload           4
        //   215: astore_2       
        //   216: aload           4
        //   218: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.logout:()V
        //   221: aload_0        
        //   222: aload           4
        //   224: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   227: goto            11
        //   230: astore          6
        //   232: aload           5
        //   234: monitorexit    
        //   235: aload           4
        //   237: astore_3       
        //   238: aload           4
        //   240: astore_2       
        //   241: aload           6
        //   243: athrow         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      7      109    114    Any
        //  26     32     128    153    Lcom/sun/mail/iap/ProtocolException;
        //  26     32     153    161    Any
        //  38     41     128    153    Lcom/sun/mail/iap/ProtocolException;
        //  38     41     153    161    Any
        //  41     55     114    128    Any
        //  65     75     128    153    Lcom/sun/mail/iap/ProtocolException;
        //  65     75     153    161    Any
        //  81     91     128    153    Lcom/sun/mail/iap/ProtocolException;
        //  81     91     153    161    Any
        //  97     101    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  97     101    153    161    Any
        //  101    106    109    114    Any
        //  116    119    114    128    Any
        //  125    128    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  125    128    153    161    Any
        //  132    136    153    161    Any
        //  138    153    153    161    Any
        //  154    161    109    114    Any
        //  167    173    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  167    173    153    161    Any
        //  179    185    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  179    185    153    161    Any
        //  191    194    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  191    194    153    161    Any
        //  194    210    230    244    Any
        //  216    221    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  216    221    153    161    Any
        //  221    227    109    114    Any
        //  232    235    230    244    Any
        //  241    244    128    153    Lcom/sun/mail/iap/ProtocolException;
        //  241    244    153    161    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0091:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.close();
    }
    
    int getAppendBufferSize() {
        return this.appendBufferSize;
    }
    
    boolean getConnectionPoolDebug() {
        return this.pool.debug;
    }
    
    @Override
    public Folder getDefaultFolder() throws MessagingException {
        synchronized (this) {
            this.checkConnected();
            return new DefaultFolder(this);
        }
    }
    
    int getFetchBlockSize() {
        return this.blksize;
    }
    
    @Override
    public Folder getFolder(final String s) throws MessagingException {
        synchronized (this) {
            this.checkConnected();
            return new IMAPFolder(s, '\uffff', this);
        }
    }
    
    @Override
    public Folder getFolder(final URLName urlName) throws MessagingException {
        synchronized (this) {
            this.checkConnected();
            return new IMAPFolder(urlName.getFile(), '\uffff', this);
        }
    }
    
    int getMinIdleTime() {
        return this.minIdleTime;
    }
    
    @Override
    public Folder[] getPersonalNamespaces() throws MessagingException {
        final Namespaces namespaces = this.getNamespaces();
        if (namespaces == null || namespaces.personal == null) {
            return super.getPersonalNamespaces();
        }
        return this.namespaceToFolders(namespaces.personal, null);
    }
    
    IMAPProtocol getProtocol(final IMAPFolder p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aload           9
        //     5: ifnull          11
        //     8: aload           9
        //    10: areturn        
        //    11: aload_0        
        //    12: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    15: astore          11
        //    17: aload           11
        //    19: monitorenter   
        //    20: aload_0        
        //    21: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    24: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    27: invokevirtual   java/util/Vector.isEmpty:()Z
        //    30: ifne            67
        //    33: aload_0        
        //    34: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    37: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    40: invokevirtual   java/util/Vector.size:()I
        //    43: iconst_1       
        //    44: if_icmpne       264
        //    47: aload_0        
        //    48: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    51: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$11:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //    54: ifne            67
        //    57: aload_0        
        //    58: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    61: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$12:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //    64: ifeq            264
        //    67: aload_0        
        //    68: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //    71: ifeq            84
        //    74: aload_0        
        //    75: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    78: ldc_w           "DEBUG: no connections in the pool, creating a new one"
        //    81: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    84: aload_0        
        //    85: getfield        com/sun/mail/imap/IMAPStore.forcePasswordRefresh:Z
        //    88: istore_2       
        //    89: iload_2        
        //    90: ifeq            149
        //    93: aload_0        
        //    94: getfield        com/sun/mail/imap/IMAPStore.host:Ljava/lang/String;
        //    97: invokestatic    java/net/InetAddress.getByName:(Ljava/lang/String;)Ljava/net/InetAddress;
        //   100: astore          10
        //   102: aload_0        
        //   103: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   106: aload           10
        //   108: aload_0        
        //   109: getfield        com/sun/mail/imap/IMAPStore.port:I
        //   112: aload_0        
        //   113: getfield        com/sun/mail/imap/IMAPStore.name:Ljava/lang/String;
        //   116: aconst_null    
        //   117: aload_0        
        //   118: getfield        com/sun/mail/imap/IMAPStore.user:Ljava/lang/String;
        //   121: invokevirtual   javax/mail/Session.requestPasswordAuthentication:(Ljava/net/InetAddress;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljavax/mail/PasswordAuthentication;
        //   124: astore          10
        //   126: aload           10
        //   128: ifnull          149
        //   131: aload_0        
        //   132: aload           10
        //   134: invokevirtual   javax/mail/PasswordAuthentication.getUserName:()Ljava/lang/String;
        //   137: putfield        com/sun/mail/imap/IMAPStore.user:Ljava/lang/String;
        //   140: aload_0        
        //   141: aload           10
        //   143: invokevirtual   javax/mail/PasswordAuthentication.getPassword:()Ljava/lang/String;
        //   146: putfield        com/sun/mail/imap/IMAPStore.password:Ljava/lang/String;
        //   149: new             Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   152: dup            
        //   153: aload_0        
        //   154: getfield        com/sun/mail/imap/IMAPStore.name:Ljava/lang/String;
        //   157: aload_0        
        //   158: getfield        com/sun/mail/imap/IMAPStore.host:Ljava/lang/String;
        //   161: aload_0        
        //   162: getfield        com/sun/mail/imap/IMAPStore.port:I
        //   165: aload_0        
        //   166: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   169: invokevirtual   javax/mail/Session.getDebug:()Z
        //   172: aload_0        
        //   173: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   176: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //   179: aload_0        
        //   180: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   183: invokevirtual   javax/mail/Session.getProperties:()Ljava/util/Properties;
        //   186: aload_0        
        //   187: getfield        com/sun/mail/imap/IMAPStore.isSSL:Z
        //   190: invokespecial   com/sun/mail/imap/protocol/IMAPProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;IZLjava/io/PrintStream;Ljava/util/Properties;Z)V
        //   193: astore          10
        //   195: aload_0        
        //   196: aload           10
        //   198: aload_0        
        //   199: getfield        com/sun/mail/imap/IMAPStore.user:Ljava/lang/String;
        //   202: aload_0        
        //   203: getfield        com/sun/mail/imap/IMAPStore.password:Ljava/lang/String;
        //   206: invokespecial   com/sun/mail/imap/IMAPStore.login:(Lcom/sun/mail/imap/protocol/IMAPProtocol;Ljava/lang/String;Ljava/lang/String;)V
        //   209: aload           10
        //   211: astore          9
        //   213: aload           9
        //   215: astore          10
        //   217: aload           9
        //   219: ifnonnull       373
        //   222: new             Ljavax/mail/MessagingException;
        //   225: dup            
        //   226: ldc_w           "connection failure"
        //   229: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   232: athrow         
        //   233: aload           11
        //   235: monitorexit    
        //   236: aload_1        
        //   237: athrow         
        //   238: astore          10
        //   240: aconst_null    
        //   241: astore          10
        //   243: goto            102
        //   246: astore          10
        //   248: aload           9
        //   250: ifnull          258
        //   253: aload           9
        //   255: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.disconnect:()V
        //   258: aconst_null    
        //   259: astore          9
        //   261: goto            213
        //   264: aload_0        
        //   265: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //   268: ifeq            304
        //   271: aload_0        
        //   272: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   275: new             Ljava/lang/StringBuilder;
        //   278: dup            
        //   279: ldc_w           "DEBUG: connection available -- size: "
        //   282: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   285: aload_0        
        //   286: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   289: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   292: invokevirtual   java/util/Vector.size:()I
        //   295: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   298: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   301: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   304: aload_0        
        //   305: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   308: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   311: invokevirtual   java/util/Vector.lastElement:()Ljava/lang/Object;
        //   314: checkcast       Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   317: astore          10
        //   319: aload_0        
        //   320: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   323: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   326: aload           10
        //   328: invokevirtual   java/util/Vector.removeElement:(Ljava/lang/Object;)Z
        //   331: pop            
        //   332: invokestatic    java/lang/System.currentTimeMillis:()J
        //   335: lstore_3       
        //   336: aload           10
        //   338: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.getTimestamp:()J
        //   341: lstore          5
        //   343: aload_0        
        //   344: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   347: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$8:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)J
        //   350: lstore          7
        //   352: lload_3        
        //   353: lload           5
        //   355: lsub           
        //   356: lload           7
        //   358: lcmp           
        //   359: ifle            367
        //   362: aload           10
        //   364: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.noop:()V
        //   367: aload           10
        //   369: aload_0        
        //   370: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.removeResponseHandler:(Lcom/sun/mail/iap/ResponseHandler;)V
        //   373: aload_0        
        //   374: invokespecial   com/sun/mail/imap/IMAPStore.timeoutConnections:()V
        //   377: aload_1        
        //   378: ifnull          416
        //   381: aload_0        
        //   382: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   385: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$13:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   388: ifnonnull       405
        //   391: aload_0        
        //   392: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   395: new             Ljava/util/Vector;
        //   398: dup            
        //   399: invokespecial   java/util/Vector.<init>:()V
        //   402: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$14:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Ljava/util/Vector;)V
        //   405: aload_0        
        //   406: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   409: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$13:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   412: aload_1        
        //   413: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   416: aload           11
        //   418: monitorexit    
        //   419: aload           10
        //   421: astore          9
        //   423: goto            3
        //   426: astore          9
        //   428: aload           10
        //   430: aload_0        
        //   431: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.removeResponseHandler:(Lcom/sun/mail/iap/ResponseHandler;)V
        //   434: aload           10
        //   436: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.disconnect:()V
        //   439: aload           11
        //   441: monitorexit    
        //   442: aconst_null    
        //   443: astore          9
        //   445: goto            3
        //   448: astore          9
        //   450: goto            258
        //   453: astore_1       
        //   454: goto            233
        //   457: astore          9
        //   459: goto            439
        //   462: astore          9
        //   464: aload           10
        //   466: astore          9
        //   468: goto            248
        //   471: astore_1       
        //   472: goto            233
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  20     67     453    457    Any
        //  67     84     453    457    Any
        //  84     89     246    248    Ljava/lang/Exception;
        //  84     89     453    457    Any
        //  93     102    238    246    Ljava/net/UnknownHostException;
        //  93     102    246    248    Ljava/lang/Exception;
        //  93     102    453    457    Any
        //  102    126    246    248    Ljava/lang/Exception;
        //  102    126    453    457    Any
        //  131    149    246    248    Ljava/lang/Exception;
        //  131    149    453    457    Any
        //  149    195    246    248    Ljava/lang/Exception;
        //  149    195    453    457    Any
        //  195    209    462    471    Ljava/lang/Exception;
        //  195    209    471    475    Any
        //  222    233    471    475    Any
        //  233    236    471    475    Any
        //  253    258    448    453    Ljava/lang/Exception;
        //  253    258    471    475    Any
        //  264    304    453    457    Any
        //  304    319    453    457    Any
        //  319    352    471    475    Any
        //  362    367    426    448    Lcom/sun/mail/iap/ProtocolException;
        //  362    367    471    475    Any
        //  367    373    471    475    Any
        //  373    377    471    475    Any
        //  381    405    471    475    Any
        //  405    416    471    475    Any
        //  416    419    471    475    Any
        //  428    439    457    462    Any
        //  439    442    471    475    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0213:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Quota[] getQuota(final String s) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkConnected();
            final Quota[] array = null;
            IMAPProtocol imapProtocol = null;
            IMAPProtocol imapProtocol2 = null;
            IMAPProtocol imapProtocol3 = null;
            IMAPProtocol storeProtocol = null;
            try {
                return (imapProtocol3 = (imapProtocol2 = (imapProtocol = (storeProtocol = this.getStoreProtocol())))).getQuotaRoot(s);
            }
            catch (BadCommandException ex) {
                imapProtocol = storeProtocol;
                throw new MessagingException("QUOTA not supported", ex);
            }
            catch (ConnectionException ex2) {
                imapProtocol = imapProtocol2;
                throw new StoreClosedException(this, ex2.getMessage());
            }
            catch (ProtocolException ex3) {
                imapProtocol = imapProtocol3;
                throw new MessagingException(ex3.getMessage(), ex3);
            }
            finally {
                this.releaseStoreProtocol(imapProtocol);
                if (imapProtocol == null) {
                    this.cleanup();
                }
            }
        }
        finally {}
    }
    
    Session getSession() {
        return this.session;
    }
    
    @Override
    public Folder[] getSharedNamespaces() throws MessagingException {
        final Namespaces namespaces = this.getNamespaces();
        if (namespaces == null || namespaces.shared == null) {
            return super.getSharedNamespaces();
        }
        return this.namespaceToFolders(namespaces.shared, null);
    }
    
    int getStatusCacheTimeout() {
        return this.statusCacheTimeout;
    }
    
    IMAPProtocol getStoreProtocol() throws ProtocolException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: aload_2        
        //     3: ifnull          8
        //     6: aload_2        
        //     7: areturn        
        //     8: aload_0        
        //     9: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    12: astore          4
        //    14: aload           4
        //    16: monitorenter   
        //    17: aload_0        
        //    18: invokespecial   com/sun/mail/imap/IMAPStore.waitIfIdle:()V
        //    21: aload_0        
        //    22: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    25: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //    28: invokevirtual   java/util/Vector.isEmpty:()Z
        //    31: ifeq            197
        //    34: aload_0        
        //    35: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    38: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //    41: ifeq            54
        //    44: aload_0        
        //    45: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    48: ldc_w           "DEBUG: getStoreProtocol() - no connections in the pool, creating a new one"
        //    51: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    54: new             Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    57: dup            
        //    58: aload_0        
        //    59: getfield        com/sun/mail/imap/IMAPStore.name:Ljava/lang/String;
        //    62: aload_0        
        //    63: getfield        com/sun/mail/imap/IMAPStore.host:Ljava/lang/String;
        //    66: aload_0        
        //    67: getfield        com/sun/mail/imap/IMAPStore.port:I
        //    70: aload_0        
        //    71: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //    74: invokevirtual   javax/mail/Session.getDebug:()Z
        //    77: aload_0        
        //    78: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //    81: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //    84: aload_0        
        //    85: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //    88: invokevirtual   javax/mail/Session.getProperties:()Ljava/util/Properties;
        //    91: aload_0        
        //    92: getfield        com/sun/mail/imap/IMAPStore.isSSL:Z
        //    95: invokespecial   com/sun/mail/imap/protocol/IMAPProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;IZLjava/io/PrintStream;Ljava/util/Properties;Z)V
        //    98: astore_3       
        //    99: aload_0        
        //   100: aload_3        
        //   101: aload_0        
        //   102: getfield        com/sun/mail/imap/IMAPStore.user:Ljava/lang/String;
        //   105: aload_0        
        //   106: getfield        com/sun/mail/imap/IMAPStore.password:Ljava/lang/String;
        //   109: invokespecial   com/sun/mail/imap/IMAPStore.login:(Lcom/sun/mail/imap/protocol/IMAPProtocol;Ljava/lang/String;Ljava/lang/String;)V
        //   112: aload_3        
        //   113: astore_2       
        //   114: aload_2        
        //   115: ifnonnull       148
        //   118: new             Lcom/sun/mail/iap/ConnectionException;
        //   121: dup            
        //   122: ldc_w           "failed to create new store connection"
        //   125: invokespecial   com/sun/mail/iap/ConnectionException.<init>:(Ljava/lang/String;)V
        //   128: athrow         
        //   129: aload           4
        //   131: monitorexit    
        //   132: aload_2        
        //   133: athrow         
        //   134: astore_3       
        //   135: aload_2        
        //   136: ifnull          143
        //   139: aload_2        
        //   140: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.logout:()V
        //   143: aconst_null    
        //   144: astore_2       
        //   145: goto            114
        //   148: aload_2        
        //   149: aload_0        
        //   150: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.addResponseHandler:(Lcom/sun/mail/iap/ResponseHandler;)V
        //   153: aload_0        
        //   154: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   157: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   160: aload_2        
        //   161: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   164: aload_0        
        //   165: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   168: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$12:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //   171: istore_1       
        //   172: iload_1        
        //   173: ifeq            257
        //   176: aconst_null    
        //   177: astore_3       
        //   178: aload_0        
        //   179: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   182: invokevirtual   java/lang/Object.wait:()V
        //   185: aload_0        
        //   186: invokespecial   com/sun/mail/imap/IMAPStore.timeoutConnections:()V
        //   189: aload           4
        //   191: monitorexit    
        //   192: aload_3        
        //   193: astore_2       
        //   194: goto            2
        //   197: aload_0        
        //   198: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   201: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //   204: ifeq            240
        //   207: aload_0        
        //   208: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   211: new             Ljava/lang/StringBuilder;
        //   214: dup            
        //   215: ldc_w           "DEBUG: getStoreProtocol() - connection available -- size: "
        //   218: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   221: aload_0        
        //   222: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   225: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   228: invokevirtual   java/util/Vector.size:()I
        //   231: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   234: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   237: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   240: aload_0        
        //   241: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   244: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   247: invokevirtual   java/util/Vector.firstElement:()Ljava/lang/Object;
        //   250: checkcast       Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   253: astore_2       
        //   254: goto            164
        //   257: aload_0        
        //   258: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   261: iconst_1       
        //   262: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$15:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Z)V
        //   265: aload_2        
        //   266: astore_3       
        //   267: aload_0        
        //   268: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   271: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$3:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Z
        //   274: ifeq            185
        //   277: aload_0        
        //   278: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   281: ldc_w           "DEBUG: getStoreProtocol() -- storeConnectionInUse"
        //   284: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   287: aload_2        
        //   288: astore_3       
        //   289: goto            185
        //   292: astore_2       
        //   293: goto            143
        //   296: astore_2       
        //   297: goto            129
        //   300: astore_2       
        //   301: goto            185
        //   304: astore_2       
        //   305: aload_3        
        //   306: astore_2       
        //   307: goto            135
        //   310: astore_2       
        //   311: goto            129
        //    Exceptions:
        //  throws com.sun.mail.iap.ProtocolException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  17     54     296    300    Any
        //  54     99     134    135    Ljava/lang/Exception;
        //  54     99     296    300    Any
        //  99     112    304    310    Ljava/lang/Exception;
        //  99     112    310    314    Any
        //  118    129    310    314    Any
        //  129    132    310    314    Any
        //  139    143    292    296    Ljava/lang/Exception;
        //  139    143    310    314    Any
        //  148    164    310    314    Any
        //  164    172    310    314    Any
        //  178    185    300    304    Ljava/lang/InterruptedException;
        //  178    185    310    314    Any
        //  185    192    310    314    Any
        //  197    240    296    300    Any
        //  240    254    296    300    Any
        //  257    265    310    314    Any
        //  267    287    310    314    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0114:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Folder[] getUserNamespaces(final String s) throws MessagingException {
        final Namespaces namespaces = this.getNamespaces();
        if (namespaces == null || namespaces.otherUsers == null) {
            return super.getUserNamespaces(s);
        }
        return this.namespaceToFolders(namespaces.otherUsers, s);
    }
    
    @Override
    public void handleResponse(final Response response) {
        if (response.isOK() || response.isNO() || response.isBAD() || response.isBYE()) {
            this.handleResponseCode(response);
        }
        if (response.isBYE()) {
            if (this.debug) {
                this.out.println("DEBUG: IMAPStore connection dead");
            }
            if (this.connected) {
                this.cleanup(response.isSynthetic());
            }
        }
    }
    
    void handleResponseCode(final Response response) {
        final String rest = response.getRest();
        boolean b = false;
        final boolean b2 = false;
        String trim = rest;
        if (rest.startsWith("[")) {
            final int index = rest.indexOf(93);
            b = b2;
            if (index > 0) {
                b = b2;
                if (rest.substring(0, index + 1).equalsIgnoreCase("[ALERT]")) {
                    b = true;
                }
            }
            trim = rest.substring(index + 1).trim();
        }
        if (b) {
            this.notifyStoreListeners(1, trim);
        }
        else if (response.isUnTagged() && trim.length() > 0) {
            this.notifyStoreListeners(2, trim);
        }
    }
    
    public boolean hasCapability(final String p0) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aconst_null    
        //     3: astore          4
        //     5: aconst_null    
        //     6: astore_3       
        //     7: aload_0        
        //     8: invokevirtual   com/sun/mail/imap/IMAPStore.getStoreProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    11: astore          5
        //    13: aload           5
        //    15: astore_3       
        //    16: aload           5
        //    18: astore          4
        //    20: aload           5
        //    22: aload_1        
        //    23: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.hasCapability:(Ljava/lang/String;)Z
        //    26: istore_2       
        //    27: aload_0        
        //    28: aload           5
        //    30: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    33: aload_0        
        //    34: monitorexit    
        //    35: iload_2        
        //    36: ireturn        
        //    37: astore_1       
        //    38: aload_3        
        //    39: ifnonnull       49
        //    42: aload_3        
        //    43: astore          4
        //    45: aload_0        
        //    46: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //    49: aload_3        
        //    50: astore          4
        //    52: new             Ljavax/mail/MessagingException;
        //    55: dup            
        //    56: aload_1        
        //    57: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //    60: aload_1        
        //    61: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //    64: athrow         
        //    65: astore_1       
        //    66: aload_0        
        //    67: aload           4
        //    69: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    72: aload_1        
        //    73: athrow         
        //    74: astore_1       
        //    75: aload_0        
        //    76: monitorexit    
        //    77: aload_1        
        //    78: athrow         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  7      13     37     65     Lcom/sun/mail/iap/ProtocolException;
        //  7      13     65     74     Any
        //  20     27     37     65     Lcom/sun/mail/iap/ProtocolException;
        //  20     27     65     74     Any
        //  27     33     74     79     Any
        //  45     49     65     74     Any
        //  52     65     65     74     Any
        //  66     74     74     79     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0049:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    boolean hasSeparateStoreConnection() {
        return this.pool.separateStoreConnection;
    }
    
    public void idle() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          9
        //     3: aconst_null    
        //     4: astore          10
        //     6: aconst_null    
        //     7: astore          11
        //     9: aconst_null    
        //    10: astore          12
        //    12: aconst_null    
        //    13: astore          6
        //    15: getstatic       com/sun/mail/imap/IMAPStore.$assertionsDisabled:Z
        //    18: ifne            39
        //    21: aload_0        
        //    22: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    25: invokestatic    java/lang/Thread.holdsLock:(Ljava/lang/Object;)Z
        //    28: ifeq            39
        //    31: new             Ljava/lang/AssertionError;
        //    34: dup            
        //    35: invokespecial   java/lang/AssertionError.<init>:()V
        //    38: athrow         
        //    39: aload_0        
        //    40: monitorenter   
        //    41: aload_0        
        //    42: invokespecial   com/sun/mail/imap/IMAPStore.checkConnected:()V
        //    45: aload_0        
        //    46: monitorexit    
        //    47: aload           9
        //    49: astore          7
        //    51: aload           10
        //    53: astore          4
        //    55: aload           11
        //    57: astore          8
        //    59: aload           12
        //    61: astore          5
        //    63: aload_0        
        //    64: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //    67: astore          13
        //    69: aload           9
        //    71: astore          7
        //    73: aload           10
        //    75: astore          4
        //    77: aload           11
        //    79: astore          8
        //    81: aload           12
        //    83: astore          5
        //    85: aload           13
        //    87: monitorenter   
        //    88: aload           6
        //    90: astore          5
        //    92: aload_0        
        //    93: invokevirtual   com/sun/mail/imap/IMAPStore.getStoreProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    96: astore          6
        //    98: aload           6
        //   100: astore          5
        //   102: aload_0        
        //   103: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   106: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$19:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)I
        //   109: ifne            341
        //   112: aload           6
        //   114: astore          5
        //   116: aload           6
        //   118: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.idleStart:()V
        //   121: aload           6
        //   123: astore          5
        //   125: aload_0        
        //   126: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   129: iconst_1       
        //   130: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$20:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;I)V
        //   133: aload           6
        //   135: astore          5
        //   137: aload_0        
        //   138: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   141: aload           6
        //   143: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$18:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   146: aload           6
        //   148: astore          5
        //   150: aload           13
        //   152: monitorexit    
        //   153: aload           6
        //   155: astore          7
        //   157: aload           6
        //   159: astore          4
        //   161: aload           6
        //   163: astore          8
        //   165: aload           6
        //   167: astore          5
        //   169: aload           6
        //   171: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.readIdleResponse:()Lcom/sun/mail/iap/Response;
        //   174: astore          10
        //   176: aload           6
        //   178: astore          7
        //   180: aload           6
        //   182: astore          4
        //   184: aload           6
        //   186: astore          8
        //   188: aload           6
        //   190: astore          5
        //   192: aload_0        
        //   193: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   196: astore          9
        //   198: aload           6
        //   200: astore          7
        //   202: aload           6
        //   204: astore          4
        //   206: aload           6
        //   208: astore          8
        //   210: aload           6
        //   212: astore          5
        //   214: aload           9
        //   216: monitorenter   
        //   217: aload           10
        //   219: ifnull          232
        //   222: aload           6
        //   224: aload           10
        //   226: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.processIdleResponse:(Lcom/sun/mail/iap/Response;)Z
        //   229: ifne            482
        //   232: aload_0        
        //   233: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   236: iconst_0       
        //   237: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$20:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;I)V
        //   240: aload_0        
        //   241: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   244: invokevirtual   java/lang/Object.notifyAll:()V
        //   247: aload           9
        //   249: monitorexit    
        //   250: aload           6
        //   252: astore          7
        //   254: aload           6
        //   256: astore          4
        //   258: aload           6
        //   260: astore          8
        //   262: aload           6
        //   264: astore          5
        //   266: aload_0        
        //   267: invokevirtual   com/sun/mail/imap/IMAPStore.getMinIdleTime:()I
        //   270: istore_1       
        //   271: iload_1        
        //   272: ifle            298
        //   275: iload_1        
        //   276: i2l            
        //   277: lstore_2       
        //   278: aload           6
        //   280: astore          7
        //   282: aload           6
        //   284: astore          4
        //   286: aload           6
        //   288: astore          8
        //   290: aload           6
        //   292: astore          5
        //   294: lload_2        
        //   295: invokestatic    java/lang/Thread.sleep:(J)V
        //   298: aload_0        
        //   299: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   302: astore          4
        //   304: aload           4
        //   306: monitorenter   
        //   307: aload_0        
        //   308: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   311: aconst_null    
        //   312: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$18:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   315: aload           4
        //   317: monitorexit    
        //   318: aload_0        
        //   319: aload           6
        //   321: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   324: aload           6
        //   326: ifnonnull       333
        //   329: aload_0        
        //   330: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //   333: return         
        //   334: astore          4
        //   336: aload_0        
        //   337: monitorexit    
        //   338: aload           4
        //   340: athrow         
        //   341: aload           6
        //   343: astore          5
        //   345: aload_0        
        //   346: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   349: invokevirtual   java/lang/Object.wait:()V
        //   352: aload           6
        //   354: astore          5
        //   356: aload           13
        //   358: monitorexit    
        //   359: aload_0        
        //   360: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   363: astore          4
        //   365: aload           4
        //   367: monitorenter   
        //   368: aload_0        
        //   369: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   372: aconst_null    
        //   373: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$18:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   376: aload           4
        //   378: monitorexit    
        //   379: aload_0        
        //   380: aload           6
        //   382: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   385: aload           6
        //   387: ifnonnull       333
        //   390: aload_0        
        //   391: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //   394: return         
        //   395: astore          5
        //   397: aload           4
        //   399: monitorexit    
        //   400: aload           5
        //   402: athrow         
        //   403: astore          6
        //   405: aload           13
        //   407: monitorexit    
        //   408: aload           5
        //   410: astore          7
        //   412: aload           5
        //   414: astore          4
        //   416: aload           5
        //   418: astore          8
        //   420: aload           6
        //   422: athrow         
        //   423: astore          5
        //   425: aload           7
        //   427: astore          4
        //   429: new             Ljavax/mail/MessagingException;
        //   432: dup            
        //   433: ldc_w           "IDLE not supported"
        //   436: aload           5
        //   438: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   441: athrow         
        //   442: astore          6
        //   444: aload_0        
        //   445: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   448: astore          5
        //   450: aload           5
        //   452: monitorenter   
        //   453: aload_0        
        //   454: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   457: aconst_null    
        //   458: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$18:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   461: aload           5
        //   463: monitorexit    
        //   464: aload_0        
        //   465: aload           4
        //   467: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //   470: aload           4
        //   472: ifnonnull       479
        //   475: aload_0        
        //   476: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //   479: aload           6
        //   481: athrow         
        //   482: aload           9
        //   484: monitorexit    
        //   485: aload           6
        //   487: astore          7
        //   489: aload           6
        //   491: astore          4
        //   493: aload           6
        //   495: astore          8
        //   497: aload           6
        //   499: astore          5
        //   501: aload_0        
        //   502: getfield        com/sun/mail/imap/IMAPStore.enableImapEvents:Z
        //   505: ifeq            153
        //   508: aload           6
        //   510: astore          7
        //   512: aload           6
        //   514: astore          4
        //   516: aload           6
        //   518: astore          8
        //   520: aload           6
        //   522: astore          5
        //   524: aload           10
        //   526: invokevirtual   com/sun/mail/iap/Response.isUnTagged:()Z
        //   529: ifeq            153
        //   532: aload           6
        //   534: astore          7
        //   536: aload           6
        //   538: astore          4
        //   540: aload           6
        //   542: astore          8
        //   544: aload           6
        //   546: astore          5
        //   548: aload_0        
        //   549: sipush          1000
        //   552: aload           10
        //   554: invokevirtual   com/sun/mail/iap/Response.toString:()Ljava/lang/String;
        //   557: invokevirtual   com/sun/mail/imap/IMAPStore.notifyStoreListeners:(ILjava/lang/String;)V
        //   560: goto            153
        //   563: astore          5
        //   565: aload           8
        //   567: astore          4
        //   569: new             Ljavax/mail/StoreClosedException;
        //   572: dup            
        //   573: aload_0        
        //   574: aload           5
        //   576: invokevirtual   com/sun/mail/iap/ConnectionException.getMessage:()Ljava/lang/String;
        //   579: invokespecial   javax/mail/StoreClosedException.<init>:(Ljavax/mail/Store;Ljava/lang/String;)V
        //   582: athrow         
        //   583: astore          10
        //   585: aload           9
        //   587: monitorexit    
        //   588: aload           6
        //   590: astore          7
        //   592: aload           6
        //   594: astore          4
        //   596: aload           6
        //   598: astore          8
        //   600: aload           6
        //   602: astore          5
        //   604: aload           10
        //   606: athrow         
        //   607: astore          6
        //   609: aload           5
        //   611: astore          4
        //   613: new             Ljavax/mail/MessagingException;
        //   616: dup            
        //   617: aload           6
        //   619: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   622: aload           6
        //   624: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   627: athrow         
        //   628: astore          4
        //   630: aload           5
        //   632: monitorexit    
        //   633: aload           4
        //   635: athrow         
        //   636: astore          5
        //   638: aload           4
        //   640: monitorexit    
        //   641: aload           5
        //   643: athrow         
        //   644: astore          4
        //   646: goto            298
        //   649: astore          4
        //   651: goto            352
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  41     47     334    341    Any
        //  63     69     423    442    Lcom/sun/mail/iap/BadCommandException;
        //  63     69     563    583    Lcom/sun/mail/iap/ConnectionException;
        //  63     69     607    628    Lcom/sun/mail/iap/ProtocolException;
        //  63     69     442    636    Any
        //  85     88     423    442    Lcom/sun/mail/iap/BadCommandException;
        //  85     88     563    583    Lcom/sun/mail/iap/ConnectionException;
        //  85     88     607    628    Lcom/sun/mail/iap/ProtocolException;
        //  85     88     442    636    Any
        //  92     98     403    423    Any
        //  102    112    403    423    Any
        //  116    121    403    423    Any
        //  125    133    403    423    Any
        //  137    146    403    423    Any
        //  150    153    403    423    Any
        //  169    176    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  169    176    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  169    176    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  169    176    442    636    Any
        //  192    198    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  192    198    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  192    198    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  192    198    442    636    Any
        //  214    217    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  214    217    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  214    217    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  214    217    442    636    Any
        //  222    232    583    607    Any
        //  232    250    583    607    Any
        //  266    271    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  266    271    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  266    271    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  266    271    442    636    Any
        //  294    298    644    649    Ljava/lang/InterruptedException;
        //  294    298    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  294    298    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  294    298    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  294    298    442    636    Any
        //  307    318    636    644    Any
        //  336    338    334    341    Any
        //  345    352    649    654    Ljava/lang/InterruptedException;
        //  345    352    403    423    Any
        //  356    359    403    423    Any
        //  368    379    395    403    Any
        //  397    400    395    403    Any
        //  405    408    403    423    Any
        //  420    423    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  420    423    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  420    423    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  420    423    442    636    Any
        //  429    442    442    636    Any
        //  453    464    628    636    Any
        //  482    485    583    607    Any
        //  501    508    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  501    508    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  501    508    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  501    508    442    636    Any
        //  524    532    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  524    532    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  524    532    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  524    532    442    636    Any
        //  548    560    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  548    560    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  548    560    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  548    560    442    636    Any
        //  569    583    442    636    Any
        //  585    588    583    607    Any
        //  604    607    423    442    Lcom/sun/mail/iap/BadCommandException;
        //  604    607    563    583    Lcom/sun/mail/iap/ConnectionException;
        //  604    607    607    628    Lcom/sun/mail/iap/ProtocolException;
        //  604    607    442    636    Any
        //  613    628    442    636    Any
        //  630    633    628    636    Any
        //  638    641    636    644    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 334, Size: 334
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean isConnected() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_1       
        //     2: aload_0        
        //     3: monitorenter   
        //     4: aload_0        
        //     5: getfield        com/sun/mail/imap/IMAPStore.connected:Z
        //     8: ifne            20
        //    11: aload_0        
        //    12: iconst_0       
        //    13: invokespecial   javax/mail/Store.setConnected:(Z)V
        //    16: aload_0        
        //    17: monitorexit    
        //    18: iload_1        
        //    19: ireturn        
        //    20: aconst_null    
        //    21: astore_3       
        //    22: aconst_null    
        //    23: astore_2       
        //    24: aload_0        
        //    25: invokevirtual   com/sun/mail/imap/IMAPStore.getStoreProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    28: astore          4
        //    30: aload           4
        //    32: astore_2       
        //    33: aload           4
        //    35: astore_3       
        //    36: aload           4
        //    38: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.noop:()V
        //    41: aload_0        
        //    42: aload           4
        //    44: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    47: aload_0        
        //    48: invokespecial   javax/mail/Store.isConnected:()Z
        //    51: istore_1       
        //    52: goto            16
        //    55: astore_3       
        //    56: aload_2        
        //    57: ifnonnull       66
        //    60: aload_2        
        //    61: astore_3       
        //    62: aload_0        
        //    63: invokespecial   com/sun/mail/imap/IMAPStore.cleanup:()V
        //    66: aload_0        
        //    67: aload_2        
        //    68: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    71: goto            47
        //    74: astore_2       
        //    75: aload_0        
        //    76: monitorexit    
        //    77: aload_2        
        //    78: athrow         
        //    79: astore_2       
        //    80: aload_0        
        //    81: aload_3        
        //    82: invokevirtual   com/sun/mail/imap/IMAPStore.releaseStoreProtocol:(Lcom/sun/mail/imap/protocol/IMAPProtocol;)V
        //    85: aload_2        
        //    86: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  4      16     74     79     Any
        //  24     30     55     74     Lcom/sun/mail/iap/ProtocolException;
        //  24     30     79     87     Any
        //  36     41     55     74     Lcom/sun/mail/iap/ProtocolException;
        //  36     41     79     87     Any
        //  41     47     74     79     Any
        //  47     52     74     79     Any
        //  62     66     79     87     Any
        //  66     71     74     79     Any
        //  80     87     74     79     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0047:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    boolean isConnectionPoolFull() {
        while (true) {
            synchronized (this.pool) {
                if (this.pool.debug) {
                    this.out.println("DEBUG: current size: " + this.pool.authenticatedConnections.size() + "   pool size: " + this.pool.poolSize);
                }
                if (this.pool.authenticatedConnections.size() >= this.pool.poolSize) {
                    return true;
                }
            }
            return false;
        }
    }
    
    @Override
    protected boolean protocolConnect(final String p0, final int p1, final String p2, final String p3) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_1        
        //     3: ifnull          15
        //     6: aload           4
        //     8: ifnull          15
        //    11: aload_3        
        //    12: ifnonnull       96
        //    15: aload_0        
        //    16: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //    19: ifeq            81
        //    22: aload_0        
        //    23: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //    26: astore          6
        //    28: new             Ljava/lang/StringBuilder;
        //    31: dup            
        //    32: ldc_w           "DEBUG: protocolConnect returning false, host="
        //    35: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    38: aload_1        
        //    39: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    42: ldc_w           ", user="
        //    45: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    48: aload_3        
        //    49: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    52: ldc_w           ", password="
        //    55: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    58: astore_3       
        //    59: aload           4
        //    61: ifnull          89
        //    64: ldc_w           "<non-null>"
        //    67: astore_1       
        //    68: aload           6
        //    70: aload_3        
        //    71: aload_1        
        //    72: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    75: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    78: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    81: iconst_0       
        //    82: istore          5
        //    84: aload_0        
        //    85: monitorexit    
        //    86: iload           5
        //    88: ireturn        
        //    89: ldc_w           "<null>"
        //    92: astore_1       
        //    93: goto            68
        //    96: iload_2        
        //    97: iconst_m1      
        //    98: if_icmpeq       304
        //   101: aload_0        
        //   102: iload_2        
        //   103: putfield        com/sun/mail/imap/IMAPStore.port:I
        //   106: aload_0        
        //   107: getfield        com/sun/mail/imap/IMAPStore.port:I
        //   110: iconst_m1      
        //   111: if_icmpne       122
        //   114: aload_0        
        //   115: aload_0        
        //   116: getfield        com/sun/mail/imap/IMAPStore.defaultPort:I
        //   119: putfield        com/sun/mail/imap/IMAPStore.port:I
        //   122: aload_0        
        //   123: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   126: astore          6
        //   128: aload           6
        //   130: monitorenter   
        //   131: aload_0        
        //   132: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   135: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   138: invokevirtual   java/util/Vector.isEmpty:()Z
        //   141: istore          5
        //   143: aload           6
        //   145: monitorexit    
        //   146: iload           5
        //   148: ifeq            443
        //   151: new             Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   154: dup            
        //   155: aload_0        
        //   156: getfield        com/sun/mail/imap/IMAPStore.name:Ljava/lang/String;
        //   159: aload_1        
        //   160: aload_0        
        //   161: getfield        com/sun/mail/imap/IMAPStore.port:I
        //   164: aload_0        
        //   165: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   168: invokevirtual   javax/mail/Session.getDebug:()Z
        //   171: aload_0        
        //   172: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   175: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //   178: aload_0        
        //   179: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   182: invokevirtual   javax/mail/Session.getProperties:()Ljava/util/Properties;
        //   185: aload_0        
        //   186: getfield        com/sun/mail/imap/IMAPStore.isSSL:Z
        //   189: invokespecial   com/sun/mail/imap/protocol/IMAPProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;IZLjava/io/PrintStream;Ljava/util/Properties;Z)V
        //   192: astore          6
        //   194: aload_0        
        //   195: getfield        com/sun/mail/imap/IMAPStore.debug:Z
        //   198: ifeq            241
        //   201: aload_0        
        //   202: getfield        com/sun/mail/imap/IMAPStore.out:Ljava/io/PrintStream;
        //   205: new             Ljava/lang/StringBuilder;
        //   208: dup            
        //   209: ldc_w           "DEBUG: protocolConnect login, host="
        //   212: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   215: aload_1        
        //   216: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   219: ldc_w           ", user="
        //   222: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   225: aload_3        
        //   226: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   229: ldc_w           ", password=<non-null>"
        //   232: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   235: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   238: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   241: aload_0        
        //   242: aload           6
        //   244: aload_3        
        //   245: aload           4
        //   247: invokespecial   com/sun/mail/imap/IMAPStore.login:(Lcom/sun/mail/imap/protocol/IMAPProtocol;Ljava/lang/String;Ljava/lang/String;)V
        //   250: aload           6
        //   252: aload_0        
        //   253: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.addResponseHandler:(Lcom/sun/mail/iap/ResponseHandler;)V
        //   256: aload_0        
        //   257: aload_1        
        //   258: putfield        com/sun/mail/imap/IMAPStore.host:Ljava/lang/String;
        //   261: aload_0        
        //   262: aload_3        
        //   263: putfield        com/sun/mail/imap/IMAPStore.user:Ljava/lang/String;
        //   266: aload_0        
        //   267: aload           4
        //   269: putfield        com/sun/mail/imap/IMAPStore.password:Ljava/lang/String;
        //   272: aload_0        
        //   273: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   276: astore_1       
        //   277: aload_1        
        //   278: monitorenter   
        //   279: aload_0        
        //   280: getfield        com/sun/mail/imap/IMAPStore.pool:Lcom/sun/mail/imap/IMAPStore$ConnectionPool;
        //   283: invokestatic    com/sun/mail/imap/IMAPStore$ConnectionPool.access$10:(Lcom/sun/mail/imap/IMAPStore$ConnectionPool;)Ljava/util/Vector;
        //   286: aload           6
        //   288: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   291: aload_1        
        //   292: monitorexit    
        //   293: aload_0        
        //   294: iconst_1       
        //   295: putfield        com/sun/mail/imap/IMAPStore.connected:Z
        //   298: iconst_1       
        //   299: istore          5
        //   301: goto            84
        //   304: aload_0        
        //   305: getfield        com/sun/mail/imap/IMAPStore.session:Ljavax/mail/Session;
        //   308: new             Ljava/lang/StringBuilder;
        //   311: dup            
        //   312: ldc             "mail."
        //   314: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   317: aload_0        
        //   318: getfield        com/sun/mail/imap/IMAPStore.name:Ljava/lang/String;
        //   321: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   324: ldc_w           ".port"
        //   327: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   330: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   333: invokevirtual   javax/mail/Session.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   336: astore          6
        //   338: aload           6
        //   340: ifnull          106
        //   343: aload_0        
        //   344: aload           6
        //   346: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   349: putfield        com/sun/mail/imap/IMAPStore.port:I
        //   352: goto            106
        //   355: astore_1       
        //   356: aload_0        
        //   357: monitorexit    
        //   358: aload_1        
        //   359: athrow         
        //   360: astore_1       
        //   361: aload           6
        //   363: monitorexit    
        //   364: aload_1        
        //   365: athrow         
        //   366: astore_3       
        //   367: aconst_null    
        //   368: astore_1       
        //   369: aload_1        
        //   370: ifnull          377
        //   373: aload_1        
        //   374: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.disconnect:()V
        //   377: new             Ljavax/mail/AuthenticationFailedException;
        //   380: dup            
        //   381: aload_3        
        //   382: invokevirtual   com/sun/mail/iap/CommandFailedException.getResponse:()Lcom/sun/mail/iap/Response;
        //   385: invokevirtual   com/sun/mail/iap/Response.getRest:()Ljava/lang/String;
        //   388: invokespecial   javax/mail/AuthenticationFailedException.<init>:(Ljava/lang/String;)V
        //   391: athrow         
        //   392: astore_1       
        //   393: goto            356
        //   396: astore_3       
        //   397: aload_1        
        //   398: monitorexit    
        //   399: aload_3        
        //   400: athrow         
        //   401: astore_3       
        //   402: aload           6
        //   404: astore_1       
        //   405: goto            369
        //   408: astore_1       
        //   409: new             Ljavax/mail/MessagingException;
        //   412: dup            
        //   413: aload_1        
        //   414: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   417: aload_1        
        //   418: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   421: athrow         
        //   422: new             Ljavax/mail/MessagingException;
        //   425: dup            
        //   426: aload_1        
        //   427: invokevirtual   java/io/IOException.getMessage:()Ljava/lang/String;
        //   430: aload_1        
        //   431: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   434: athrow         
        //   435: astore_1       
        //   436: goto            422
        //   439: astore_1       
        //   440: goto            409
        //   443: goto            293
        //   446: astore_1       
        //   447: goto            422
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  15     59     355    356    Any
        //  68     81     355    356    Any
        //  101    106    355    356    Any
        //  106    122    355    356    Any
        //  122    131    366    369    Lcom/sun/mail/iap/CommandFailedException;
        //  122    131    408    409    Lcom/sun/mail/iap/ProtocolException;
        //  122    131    446    450    Ljava/io/IOException;
        //  122    131    355    356    Any
        //  131    146    360    366    Any
        //  151    194    366    369    Lcom/sun/mail/iap/CommandFailedException;
        //  151    194    408    409    Lcom/sun/mail/iap/ProtocolException;
        //  151    194    446    450    Ljava/io/IOException;
        //  151    194    355    356    Any
        //  194    241    401    408    Lcom/sun/mail/iap/CommandFailedException;
        //  194    241    439    443    Lcom/sun/mail/iap/ProtocolException;
        //  194    241    435    439    Ljava/io/IOException;
        //  194    241    392    396    Any
        //  241    279    401    408    Lcom/sun/mail/iap/CommandFailedException;
        //  241    279    439    443    Lcom/sun/mail/iap/ProtocolException;
        //  241    279    435    439    Ljava/io/IOException;
        //  241    279    392    396    Any
        //  279    293    396    401    Any
        //  293    298    392    396    Any
        //  304    338    355    356    Any
        //  343    352    355    356    Any
        //  361    364    360    366    Any
        //  364    366    366    369    Lcom/sun/mail/iap/CommandFailedException;
        //  364    366    408    409    Lcom/sun/mail/iap/ProtocolException;
        //  364    366    446    450    Ljava/io/IOException;
        //  364    366    355    356    Any
        //  373    377    392    396    Any
        //  377    392    392    396    Any
        //  397    399    396    401    Any
        //  399    401    401    408    Lcom/sun/mail/iap/CommandFailedException;
        //  399    401    439    443    Lcom/sun/mail/iap/ProtocolException;
        //  399    401    435    439    Ljava/io/IOException;
        //  399    401    392    396    Any
        //  409    422    392    396    Any
        //  422    435    392    396    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 226, Size: 226
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    void releaseProtocol(final IMAPFolder imapFolder, final IMAPProtocol imapProtocol) {
        final ConnectionPool pool = this.pool;
        // monitorenter(pool)
        Label_0074: {
            if (imapProtocol == null) {
                break Label_0074;
            }
            try {
                if (!this.isConnectionPoolFull()) {
                    imapProtocol.addResponseHandler(this);
                    this.pool.authenticatedConnections.addElement(imapProtocol);
                    if (this.debug) {
                        this.out.println("DEBUG: added an Authenticated connection -- size: " + this.pool.authenticatedConnections.size());
                    }
                }
                else {
                    if (this.debug) {
                        this.out.println("DEBUG: pool is full, not adding an Authenticated connection");
                    }
                    try {
                        imapProtocol.logout();
                    }
                    catch (ProtocolException ex) {}
                }
                if (this.pool.folders != null) {
                    this.pool.folders.removeElement(imapFolder);
                }
                this.timeoutConnections();
            }
            finally {
            }
            // monitorexit(pool)
        }
    }
    
    void releaseStoreProtocol(final IMAPProtocol imapProtocol) {
        if (imapProtocol == null) {
            return;
        }
        synchronized (this.pool) {
            ConnectionPool.access$15(this.pool, false);
            this.pool.notifyAll();
            if (this.pool.debug) {
                this.out.println("DEBUG: releaseStoreProtocol()");
            }
            this.timeoutConnections();
        }
    }
    
    public void setPassword(final String password) {
        synchronized (this) {
            this.password = password;
        }
    }
    
    @Override
    public void setQuota(final Quota quota) throws MessagingException {
        // monitorenter(this)
        try {
            this.checkConnected();
            IMAPProtocol imapProtocol = null;
            IMAPProtocol imapProtocol2 = null;
            IMAPProtocol imapProtocol3 = null;
            IMAPProtocol storeProtocol = null;
            try {
                (imapProtocol3 = (imapProtocol2 = (imapProtocol = (storeProtocol = this.getStoreProtocol())))).setQuota(quota);
            }
            catch (BadCommandException ex) {
                imapProtocol = storeProtocol;
                throw new MessagingException("QUOTA not supported", ex);
            }
            catch (ConnectionException ex2) {
                imapProtocol = imapProtocol2;
                throw new StoreClosedException(this, ex2.getMessage());
            }
            catch (ProtocolException ex3) {
                imapProtocol = imapProtocol3;
                throw new MessagingException(ex3.getMessage(), ex3);
            }
            finally {
                this.releaseStoreProtocol(imapProtocol);
                if (imapProtocol == null) {
                    this.cleanup();
                }
            }
        }
        finally {}
    }
    
    public void setUsername(final String user) {
        synchronized (this) {
            this.user = user;
        }
    }
    
    static class ConnectionPool
    {
        private static final int ABORTING = 2;
        private static final int IDLE = 1;
        private static final int RUNNING = 0;
        private Vector authenticatedConnections;
        private long clientTimeoutInterval;
        private boolean debug;
        private Vector folders;
        private IMAPProtocol idleProtocol;
        private int idleState;
        private long lastTimePruned;
        private int poolSize;
        private long pruningInterval;
        private boolean separateStoreConnection;
        private long serverTimeoutInterval;
        private boolean storeConnectionInUse;
        
        ConnectionPool() {
            this.authenticatedConnections = new Vector();
            this.separateStoreConnection = false;
            this.storeConnectionInUse = false;
            this.clientTimeoutInterval = 45000L;
            this.serverTimeoutInterval = 1800000L;
            this.poolSize = 1;
            this.pruningInterval = 60000L;
            this.debug = false;
            this.idleState = 0;
        }
        
        static /* synthetic */ void access$0(final ConnectionPool connectionPool, final long lastTimePruned) {
            connectionPool.lastTimePruned = lastTimePruned;
        }
        
        static /* synthetic */ void access$1(final ConnectionPool connectionPool, final boolean debug) {
            connectionPool.debug = debug;
        }
        
        static /* synthetic */ void access$15(final ConnectionPool connectionPool, final boolean storeConnectionInUse) {
            connectionPool.storeConnectionInUse = storeConnectionInUse;
        }
        
        static /* synthetic */ void access$2(final ConnectionPool connectionPool, final int poolSize) {
            connectionPool.poolSize = poolSize;
        }
        
        static /* synthetic */ void access$20(final ConnectionPool connectionPool, final int idleState) {
            connectionPool.idleState = idleState;
        }
        
        static /* synthetic */ void access$5(final ConnectionPool connectionPool, final long clientTimeoutInterval) {
            connectionPool.clientTimeoutInterval = clientTimeoutInterval;
        }
        
        static /* synthetic */ void access$7(final ConnectionPool connectionPool, final long serverTimeoutInterval) {
            connectionPool.serverTimeoutInterval = serverTimeoutInterval;
        }
        
        static /* synthetic */ void access$9(final ConnectionPool connectionPool, final boolean separateStoreConnection) {
            connectionPool.separateStoreConnection = separateStoreConnection;
        }
    }
}
