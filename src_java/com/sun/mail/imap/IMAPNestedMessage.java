package com.sun.mail.imap;

import com.sun.mail.imap.protocol.*;
import com.sun.mail.iap.*;
import javax.mail.*;

public class IMAPNestedMessage extends IMAPMessage
{
    private IMAPMessage msg;
    
    IMAPNestedMessage(final IMAPMessage msg, final BODYSTRUCTURE bs, final ENVELOPE envelope, final String sectionId) {
        super(msg._getSession());
        this.msg = msg;
        this.bs = bs;
        this.envelope = envelope;
        this.sectionId = sectionId;
    }
    
    @Override
    protected void checkExpunged() throws MessageRemovedException {
        this.msg.checkExpunged();
    }
    
    @Override
    protected int getFetchBlockSize() {
        return this.msg.getFetchBlockSize();
    }
    
    @Override
    protected Object getMessageCacheLock() {
        return this.msg.getMessageCacheLock();
    }
    
    @Override
    protected IMAPProtocol getProtocol() throws ProtocolException, FolderClosedException {
        return this.msg.getProtocol();
    }
    
    @Override
    protected int getSequenceNumber() {
        return this.msg.getSequenceNumber();
    }
    
    @Override
    public int getSize() throws MessagingException {
        return this.bs.size;
    }
    
    @Override
    public boolean isExpunged() {
        return this.msg.isExpunged();
    }
    
    @Override
    protected boolean isREV1() throws FolderClosedException {
        return this.msg.isREV1();
    }
    
    @Override
    public void setFlags(final Flags flags, final boolean b) throws MessagingException {
        synchronized (this) {
            throw new MethodNotSupportedException("Cannot set flags on this nested message");
        }
    }
}
