package com.sun.mail.imap;

import java.io.*;
import com.sun.mail.util.*;
import javax.mail.*;
import com.sun.mail.iap.*;

public class IMAPInputStream extends InputStream
{
    private static final int slop = 64;
    private int blksize;
    private byte[] buf;
    private int bufcount;
    private int bufpos;
    private int max;
    private IMAPMessage msg;
    private boolean peek;
    private int pos;
    private ByteArray readbuf;
    private String section;
    
    public IMAPInputStream(final IMAPMessage msg, final String section, final int max, final boolean peek) {
        this.msg = msg;
        this.section = section;
        this.max = max;
        this.peek = peek;
        this.pos = 0;
        this.blksize = msg.getFetchBlockSize();
    }
    
    private void checkSeen() {
        if (!this.peek) {
            try {
                final Folder folder = this.msg.getFolder();
                if (folder != null && folder.getMode() != 1 && !this.msg.isSet(Flags.Flag.SEEN)) {
                    this.msg.setFlag(Flags.Flag.SEEN, true);
                }
            }
            catch (MessagingException ex) {}
        }
    }
    
    private void fill() throws IOException {
        if (this.max != -1 && this.pos >= this.max) {
            if (this.pos == 0) {
                this.checkSeen();
            }
            this.readbuf = null;
            return;
        }
        if (this.readbuf == null) {
            this.readbuf = new ByteArray(this.blksize + 64);
        }
        final Object messageCacheLock = this.msg.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        try {
            this.msg.getProtocol();
            if (this.msg.isExpunged()) {
                throw new MessageRemovedIOException("No content for expunged message");
            }
            goto Label_0129;
        }
        catch (ProtocolException ex) {
            try {
                this.forceCheckExpunged();
                throw new IOException(ex.getMessage());
            }
            finally {
            }
            // monitorexit(messageCacheLock)
        }
        catch (FolderClosedException ex2) {
            throw new FolderClosedIOException(ex2.getFolder(), ex2.getMessage());
        }
        // monitorexit(messageCacheLock)
        if (this.pos == 0) {
            this.checkSeen();
        }
        final ByteArray byteArray;
        this.buf = byteArray.getBytes();
        this.bufpos = byteArray.getStart();
        final int count = byteArray.getCount();
        this.bufcount = this.bufpos + count;
        this.pos += count;
    }
    
    private void forceCheckExpunged() throws MessageRemovedIOException, FolderClosedIOException {
        final Object messageCacheLock = this.msg.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        while (true) {
            try {
                this.msg.getProtocol().noop();
                // monitorexit(messageCacheLock)
                if (this.msg.isExpunged()) {
                    throw new MessageRemovedIOException();
                }
            }
            catch (ConnectionException ex) {
                throw new FolderClosedIOException(this.msg.getFolder(), ex.getMessage());
                try {}
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (FolderClosedException ex2) {
                throw new FolderClosedIOException(ex2.getFolder(), ex2.getMessage());
            }
            catch (ProtocolException ex3) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public int available() throws IOException {
        synchronized (this) {
            return this.bufcount - this.bufpos;
        }
    }
    
    @Override
    public int read() throws IOException {
        synchronized (this) {
            if (this.bufpos < this.bufcount) {
                return this.buf[this.bufpos++] & 0xFF;
            }
            this.fill();
            if (this.bufpos < this.bufcount) {
                return this.buf[this.bufpos++] & 0xFF;
            }
            return -1;
            n = (this.buf[this.bufpos++] & 0xFF);
            return n;
        }
    }
    
    @Override
    public int read(final byte[] array) throws IOException {
        return this.read(array, 0, array.length);
    }
    
    @Override
    public int read(final byte[] array, final int n, int n2) throws IOException {
        while (true) {
            while (true) {
                Label_0101: {
                    synchronized (this) {
                        int n3 = 0;
                        Label_0060: {
                            if ((n3 = this.bufcount - this.bufpos) > 0) {
                                break Label_0060;
                            }
                            this.fill();
                            if ((n3 = this.bufcount - this.bufpos) > 0) {
                                break Label_0060;
                            }
                            n2 = -1;
                            return n2;
                        }
                        if (n3 < n2) {
                            n2 = n3;
                            System.arraycopy(this.buf, this.bufpos, array, n, n2);
                            this.bufpos += n2;
                            return n2;
                        }
                        break Label_0101;
                    }
                }
                continue;
            }
        }
    }
}
