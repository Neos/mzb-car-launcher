package com.sun.mail.imap.protocol;

import java.util.*;

public class MessageSet
{
    public int end;
    public int start;
    
    public MessageSet() {
    }
    
    public MessageSet(final int start, final int end) {
        this.start = start;
        this.end = end;
    }
    
    public static MessageSet[] createMessageSets(final int[] array) {
        final Vector<MessageSet> vector = new Vector<MessageSet>();
        int n;
        for (int i = 0; i < array.length; i = n - 1 + 1) {
            final MessageSet set = new MessageSet();
            set.start = array[i];
            for (n = i + 1; n < array.length && array[n] == array[n - 1] + 1; ++n) {}
            set.end = array[n - 1];
            vector.addElement(set);
        }
        final MessageSet[] array2 = new MessageSet[vector.size()];
        vector.copyInto(array2);
        return array2;
    }
    
    public static int size(final MessageSet[] array) {
        int n = 0;
        if (array == null) {
            return 0;
        }
        for (int i = 0; i < array.length; ++i) {
            n += array[i].size();
        }
        return n;
    }
    
    public static String toString(final MessageSet[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int n = 0;
        final StringBuffer sb = new StringBuffer();
        final int length = array.length;
        while (true) {
            final int start = array[n].start;
            final int end = array[n].end;
            if (end > start) {
                sb.append(start).append(':').append(end);
            }
            else {
                sb.append(start);
            }
            ++n;
            if (n >= length) {
                break;
            }
            sb.append(',');
        }
        return sb.toString();
    }
    
    public int size() {
        return this.end - this.start + 1;
    }
}
