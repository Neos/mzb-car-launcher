package com.sun.mail.imap.protocol;

import javax.mail.internet.*;
import javax.mail.search.*;
import javax.mail.*;
import java.io.*;
import com.sun.mail.util.*;
import com.sun.mail.imap.*;
import java.util.*;
import com.sun.mail.iap.*;
import java.lang.reflect.*;

public class IMAPProtocol extends Protocol
{
    private static final byte[] CRLF;
    private static final byte[] DONE;
    private boolean authenticated;
    private List authmechs;
    private ByteArray ba;
    private Map capabilities;
    private boolean connected;
    private String idleTag;
    private String name;
    private boolean rev1;
    private SaslAuthenticator saslAuthenticator;
    private String[] searchCharsets;
    
    static {
        CRLF = new byte[] { 13, 10 };
        DONE = new byte[] { 68, 79, 78, 69, 13, 10 };
    }
    
    public IMAPProtocol(final String name, final String s, final int n, final boolean b, final PrintStream printStream, final Properties properties, final boolean b2) throws IOException, ProtocolException {
        super(s, n, b, printStream, properties, "mail." + name, b2);
        this.connected = false;
        this.rev1 = false;
        this.capabilities = null;
        this.authmechs = null;
        try {
            this.name = name;
            if (this.capabilities == null) {
                this.capability();
            }
            if (this.hasCapability("IMAP4rev1")) {
                this.rev1 = true;
            }
            (this.searchCharsets = new String[2])[0] = "UTF-8";
            this.searchCharsets[1] = MimeUtility.mimeCharset(MimeUtility.getDefaultJavaCharset());
            this.connected = true;
        }
        finally {
            if (!this.connected) {
                this.disconnect();
            }
        }
    }
    
    private void copy(final String s, String encode) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeAtom(s);
        argument.writeString(encode);
        this.simpleCommand("COPY", argument);
    }
    
    private String createFlagList(final Flags flags) {
        final StringBuffer sb = new StringBuffer();
        sb.append("(");
        final Flags.Flag[] systemFlags = flags.getSystemFlags();
        int n = 1;
        int n2;
        for (int i = 0; i < systemFlags.length; ++i, n = n2) {
            final Flags.Flag flag = systemFlags[i];
            String s;
            if (flag == Flags.Flag.ANSWERED) {
                s = "\\Answered";
            }
            else if (flag == Flags.Flag.DELETED) {
                s = "\\Deleted";
            }
            else if (flag == Flags.Flag.DRAFT) {
                s = "\\Draft";
            }
            else if (flag == Flags.Flag.FLAGGED) {
                s = "\\Flagged";
            }
            else if (flag == Flags.Flag.RECENT) {
                s = "\\Recent";
            }
            else {
                n2 = n;
                if (flag != Flags.Flag.SEEN) {
                    continue;
                }
                s = "\\Seen";
            }
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(' ');
            }
            sb.append(s);
            n2 = n;
        }
        final String[] userFlags = flags.getUserFlags();
        for (int j = 0; j < userFlags.length; ++j) {
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(' ');
            }
            sb.append(userFlags[j]);
        }
        sb.append(")");
        return sb.toString();
    }
    
    private ListInfo[] doList(final String s, String encode, String encode2) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        encode2 = BASE64MailboxEncoder.encode(encode2);
        final Argument argument = new Argument();
        argument.writeString(encode);
        argument.writeString(encode2);
        final Response[] command = this.command(s, argument);
        final ListInfo[] array = null;
        final Response response = command[command.length - 1];
        ListInfo[] array2 = array;
        if (response.isOK()) {
            final Vector<ListInfo> vector = new Vector<ListInfo>(1);
            for (int i = 0; i < command.length; ++i) {
                if (command[i] instanceof IMAPResponse) {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    if (imapResponse.keyEquals(s)) {
                        vector.addElement(new ListInfo(imapResponse));
                        command[i] = null;
                    }
                }
            }
            array2 = array;
            if (vector.size() > 0) {
                array2 = new ListInfo[vector.size()];
                vector.copyInto(array2);
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        return array2;
    }
    
    private Response[] fetch(final String s, final String s2, final boolean b) throws ProtocolException {
        if (b) {
            return this.command("UID FETCH " + s + " (" + s2 + ")", null);
        }
        return this.command("FETCH " + s + " (" + s2 + ")", null);
    }
    
    private AppendUID getAppendUID(final Response response) {
        if (response.isOK()) {
            byte byte1;
            do {
                byte1 = response.readByte();
            } while (byte1 > 0 && byte1 != 91);
            if (byte1 != 0 && response.readAtom().equalsIgnoreCase("APPENDUID")) {
                return new AppendUID(response.readLong(), response.readLong());
            }
        }
        return null;
    }
    
    private int[] issueSearch(final String s, final SearchTerm searchTerm, final String s2) throws ProtocolException, SearchException, IOException {
        String javaCharset;
        if (s2 == null) {
            javaCharset = null;
        }
        else {
            javaCharset = MimeUtility.javaCharset(s2);
        }
        final Argument generateSequence = SearchSequence.generateSequence(searchTerm, javaCharset);
        generateSequence.writeAtom(s);
        Response[] array;
        if (s2 == null) {
            array = this.command("SEARCH", generateSequence);
        }
        else {
            array = this.command("SEARCH CHARSET " + s2, generateSequence);
        }
        final Response response = array[array.length - 1];
        int[] array2 = null;
        if (response.isOK()) {
            final Vector<Integer> vector = new Vector<Integer>();
            for (int i = 0; i < array.length; ++i) {
                if (array[i] instanceof IMAPResponse) {
                    final IMAPResponse imapResponse = (IMAPResponse)array[i];
                    if (imapResponse.keyEquals("SEARCH")) {
                        while (true) {
                            final int number = imapResponse.readNumber();
                            if (number == -1) {
                                break;
                            }
                            vector.addElement(new Integer(number));
                        }
                        array[i] = null;
                    }
                }
            }
            final int size = vector.size();
            array2 = new int[size];
            for (int j = 0; j < size; ++j) {
                array2[j] = vector.elementAt(j);
            }
        }
        this.notifyResponseHandlers(array);
        this.handleResult(response);
        return array2;
    }
    
    private Quota parseQuota(final Response response) throws ParsingException {
        final Quota quota = new Quota(response.readAtomString());
        response.skipSpaces();
        if (response.readByte() != 40) {
            throw new ParsingException("parse error in QUOTA");
        }
        final Vector<Quota.Resource> vector = new Vector<Quota.Resource>();
        while (response.peekByte() != 41) {
            final String atom = response.readAtom();
            if (atom != null) {
                vector.addElement(new Quota.Resource(atom, response.readLong(), response.readLong()));
            }
        }
        response.readByte();
        vector.copyInto(quota.resources = new Quota.Resource[vector.size()]);
        return quota;
    }
    
    private int[] search(final String s, final SearchTerm searchTerm) throws ProtocolException, SearchException {
        if (SearchSequence.isAscii(searchTerm)) {
            try {
                return this.issueSearch(s, searchTerm, null);
            }
            catch (IOException ex3) {}
        }
        for (int i = 0; i < this.searchCharsets.length; ++i) {
            if (this.searchCharsets[i] != null) {
                try {
                    return this.issueSearch(s, searchTerm, this.searchCharsets[i]);
                }
                catch (CommandFailedException ex4) {
                    this.searchCharsets[i] = null;
                }
                catch (IOException ex5) {}
                catch (ProtocolException ex) {
                    throw ex;
                }
                catch (SearchException ex2) {
                    throw ex2;
                }
            }
        }
        throw new SearchException("Search failed");
    }
    
    private void storeFlags(final String s, final Flags flags, final boolean b) throws ProtocolException {
        Response[] array;
        if (b) {
            array = this.command("STORE " + s + " +FLAGS " + this.createFlagList(flags), null);
        }
        else {
            array = this.command("STORE " + s + " -FLAGS " + this.createFlagList(flags), null);
        }
        this.notifyResponseHandlers(array);
        this.handleResult(array[array.length - 1]);
    }
    
    public void append(final String s, final Flags flags, final Date date, final Literal literal) throws ProtocolException {
        this.appenduid(s, flags, date, literal, false);
    }
    
    public AppendUID appenduid(final String s, final Flags flags, final Date date, final Literal literal) throws ProtocolException {
        return this.appenduid(s, flags, date, literal, true);
    }
    
    public AppendUID appenduid(String encode, final Flags flags, final Date date, final Literal literal, final boolean b) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        if (flags != null) {
            Flags flags2 = flags;
            if (flags.contains(Flags.Flag.RECENT)) {
                flags2 = new Flags(flags);
                flags2.remove(Flags.Flag.RECENT);
            }
            argument.writeAtom(this.createFlagList(flags2));
        }
        if (date != null) {
            argument.writeString(INTERNALDATE.format(date));
        }
        argument.writeBytes(literal);
        final Response[] command = this.command("APPEND", argument);
        this.notifyResponseHandlers(command);
        this.handleResult(command[command.length - 1]);
        if (b) {
            return this.getAppendUID(command[command.length - 1]);
        }
        return null;
    }
    
    public void authlogin(String s, final String s2) throws ProtocolException {
        synchronized (this) {
        Label_0065_Outer:
            while (true) {
                final Vector<Response> vector = new Vector<Response>();
                Object writeCommand = null;
                Response capabilities = null;
                int n = 0;
            Label_0065:
                while (true) {
                    OutputStream outputStream;
                    ByteArrayOutputStream byteArrayOutputStream;
                    BASE64EncoderStream base64EncoderStream;
                    int n2;
                    while (true) {
                        try {
                            writeCommand = this.writeCommand("AUTHENTICATE LOGIN", null);
                            outputStream = this.getOutputStream();
                            byteArrayOutputStream = new ByteArrayOutputStream();
                            base64EncoderStream = new BASE64EncoderStream(byteArrayOutputStream, Integer.MAX_VALUE);
                            n2 = 1;
                            if (n != 0) {
                                s = (String)(Object)new Response[vector.size()];
                                vector.copyInto((Object[])(Object)s);
                                this.notifyResponseHandlers((Response[])(Object)s);
                                this.handleResult(capabilities);
                                this.setCapabilities(capabilities);
                                this.authenticated = true;
                                return;
                            }
                        }
                        catch (Exception ex) {
                            capabilities = Response.byeResponse(ex);
                            n = 1;
                            continue Label_0065_Outer;
                        }
                        break;
                    }
                    Response response;
                    while (true) {
                        int n3 = n2;
                        while (true) {
                            Label_0248: {
                                try {
                                    response = this.readResponse();
                                    n3 = n2;
                                    if (!response.isContinuation()) {
                                        break;
                                    }
                                    if (n2 != 0) {
                                        final String s3 = s;
                                        n2 = 0;
                                        n3 = n2;
                                        base64EncoderStream.write(ASCIIUtility.getBytes(s3));
                                        n3 = n2;
                                        base64EncoderStream.flush();
                                        n3 = n2;
                                        byteArrayOutputStream.write(IMAPProtocol.CRLF);
                                        n3 = n2;
                                        outputStream.write(byteArrayOutputStream.toByteArray());
                                        n3 = n2;
                                        outputStream.flush();
                                        n3 = n2;
                                        byteArrayOutputStream.reset();
                                        capabilities = response;
                                        continue Label_0065;
                                    }
                                    break Label_0248;
                                }
                                catch (Exception ex2) {
                                    capabilities = Response.byeResponse(ex2);
                                    n = 1;
                                    n2 = n3;
                                    continue Label_0065;
                                }
                                continue Label_0065;
                            }
                            final String s3 = s2;
                            continue;
                        }
                    }
                    if (response.isTagged() && response.getTag().equals(writeCommand)) {
                        n = 1;
                        capabilities = response;
                        continue Label_0065;
                    }
                    if (response.isBYE()) {
                        n = 1;
                        capabilities = response;
                        continue Label_0065;
                    }
                    vector.addElement(response);
                    capabilities = response;
                    continue Label_0065;
                }
            }
        }
    }
    
    public void authplain(String s, final String s2, final String s3) throws ProtocolException {
        synchronized (this) {
        Label_0063_Outer:
            while (true) {
                final Vector<Response> vector = new Vector<Response>();
                Object writeCommand = null;
                Response capabilities = null;
                int n = 0;
                while (true) {
                    OutputStream outputStream;
                    ByteArrayOutputStream byteArrayOutputStream;
                    BASE64EncoderStream base64EncoderStream;
                    while (true) {
                        try {
                            writeCommand = this.writeCommand("AUTHENTICATE PLAIN", null);
                            outputStream = this.getOutputStream();
                            byteArrayOutputStream = new ByteArrayOutputStream();
                            base64EncoderStream = new BASE64EncoderStream(byteArrayOutputStream, Integer.MAX_VALUE);
                            if (n != 0) {
                                s = (String)(Object)new Response[vector.size()];
                                vector.copyInto((Object[])(Object)s);
                                this.notifyResponseHandlers((Response[])(Object)s);
                                this.handleResult(capabilities);
                                this.setCapabilities(capabilities);
                                this.authenticated = true;
                                return;
                            }
                        }
                        catch (Exception ex) {
                            capabilities = Response.byeResponse(ex);
                            n = 1;
                            continue Label_0063_Outer;
                        }
                        break;
                    }
                    Label_0230: {
                        try {
                            capabilities = this.readResponse();
                            if (capabilities.isContinuation()) {
                                base64EncoderStream.write(ASCIIUtility.getBytes(String.valueOf(s) + "\u0000" + s2 + "\u0000" + s3));
                                base64EncoderStream.flush();
                                byteArrayOutputStream.write(IMAPProtocol.CRLF);
                                outputStream.write(byteArrayOutputStream.toByteArray());
                                outputStream.flush();
                                byteArrayOutputStream.reset();
                                continue;
                            }
                            break Label_0230;
                        }
                        catch (Exception ex2) {
                            capabilities = Response.byeResponse(ex2);
                            n = 1;
                            continue;
                        }
                        continue;
                    }
                    if (capabilities.isTagged() && capabilities.getTag().equals(writeCommand)) {
                        n = 1;
                        continue;
                    }
                    if (capabilities.isBYE()) {
                        n = 1;
                        continue;
                    }
                    vector.addElement(capabilities);
                    continue;
                }
            }
        }
    }
    
    public void capability() throws ProtocolException {
        final Response[] command = this.command("CAPABILITY", null);
        if (!command[command.length - 1].isOK()) {
            throw new ProtocolException(command[command.length - 1].toString());
        }
        this.capabilities = new HashMap(10);
        this.authmechs = new ArrayList(5);
        for (int i = 0; i < command.length; ++i) {
            if (command[i] instanceof IMAPResponse) {
                final IMAPResponse imapResponse = (IMAPResponse)command[i];
                if (imapResponse.keyEquals("CAPABILITY")) {
                    this.parseCapabilities(imapResponse);
                }
            }
        }
    }
    
    public void check() throws ProtocolException {
        this.simpleCommand("CHECK", null);
    }
    
    public void close() throws ProtocolException {
        this.simpleCommand("CLOSE", null);
    }
    
    public void copy(final int n, final int n2, final String s) throws ProtocolException {
        this.copy(String.valueOf(String.valueOf(n)) + ":" + String.valueOf(n2), s);
    }
    
    public void copy(final MessageSet[] array, final String s) throws ProtocolException {
        this.copy(MessageSet.toString(array), s);
    }
    
    public void create(String encode) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        this.simpleCommand("CREATE", argument);
    }
    
    public void delete(String encode) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        this.simpleCommand("DELETE", argument);
    }
    
    public void deleteACL(String encode, final String s) throws ProtocolException {
        if (!this.hasCapability("ACL")) {
            throw new BadCommandException("ACL not supported");
        }
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        argument.writeString(s);
        final Response[] command = this.command("DELETEACL", argument);
        final Response response = command[command.length - 1];
        this.notifyResponseHandlers(command);
        this.handleResult(response);
    }
    
    public void disconnect() {
        super.disconnect();
        this.authenticated = false;
    }
    
    public MailboxInfo examine(String encode) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        final Response[] command = this.command("EXAMINE", argument);
        final MailboxInfo mailboxInfo = new MailboxInfo(command);
        mailboxInfo.mode = 1;
        this.notifyResponseHandlers(command);
        this.handleResult(command[command.length - 1]);
        return mailboxInfo;
    }
    
    public void expunge() throws ProtocolException {
        this.simpleCommand("EXPUNGE", null);
    }
    
    public Response[] fetch(final int n, final int n2, final String s) throws ProtocolException {
        return this.fetch(String.valueOf(String.valueOf(n)) + ":" + String.valueOf(n2), s, false);
    }
    
    public Response[] fetch(final int n, final String s) throws ProtocolException {
        return this.fetch(String.valueOf(n), s, false);
    }
    
    public Response[] fetch(final MessageSet[] array, final String s) throws ProtocolException {
        return this.fetch(MessageSet.toString(array), s, false);
    }
    
    public BODY fetchBody(final int n, final String s) throws ProtocolException {
        return this.fetchBody(n, s, false);
    }
    
    public BODY fetchBody(final int n, final String s, final int n2, final int n3) throws ProtocolException {
        return this.fetchBody(n, s, n2, n3, false, null);
    }
    
    public BODY fetchBody(final int n, final String s, final int n2, final int n3, final ByteArray byteArray) throws ProtocolException {
        return this.fetchBody(n, s, n2, n3, false, byteArray);
    }
    
    protected BODY fetchBody(final int n, String string, final int n2, final int n3, final boolean b, final ByteArray ba) throws ProtocolException {
        this.ba = ba;
        String s;
        if (b) {
            s = "BODY.PEEK[";
        }
        else {
            s = "BODY[";
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s));
        if (string == null) {
            string = "]<";
        }
        else {
            string = String.valueOf(string) + "]<";
        }
        final Response[] fetch = this.fetch(n, sb.append(string).append(String.valueOf(n2)).append(".").append(String.valueOf(n3)).append(">").toString());
        this.notifyResponseHandlers(fetch);
        final Response response = fetch[fetch.length - 1];
        if (response.isOK()) {
            return (BODY)FetchResponse.getItem(fetch, n, BODY.class);
        }
        if (response.isNO()) {
            return null;
        }
        this.handleResult(response);
        return null;
    }
    
    protected BODY fetchBody(final int n, String s, final boolean b) throws ProtocolException {
        Response[] array;
        if (b) {
            final StringBuilder sb = new StringBuilder("BODY.PEEK[");
            if (s == null) {
                s = "]";
            }
            else {
                s = String.valueOf(s) + "]";
            }
            array = this.fetch(n, sb.append(s).toString());
        }
        else {
            final StringBuilder sb2 = new StringBuilder("BODY[");
            if (s == null) {
                s = "]";
            }
            else {
                s = String.valueOf(s) + "]";
            }
            array = this.fetch(n, sb2.append(s).toString());
        }
        this.notifyResponseHandlers(array);
        final Response response = array[array.length - 1];
        if (response.isOK()) {
            return (BODY)FetchResponse.getItem(array, n, BODY.class);
        }
        if (response.isNO()) {
            return null;
        }
        this.handleResult(response);
        return null;
    }
    
    public BODYSTRUCTURE fetchBodyStructure(final int n) throws ProtocolException {
        BODYSTRUCTURE bodystructure = null;
        final Response[] fetch = this.fetch(n, "BODYSTRUCTURE");
        this.notifyResponseHandlers(fetch);
        final Response response = fetch[fetch.length - 1];
        if (response.isOK()) {
            bodystructure = (BODYSTRUCTURE)FetchResponse.getItem(fetch, n, BODYSTRUCTURE.class);
        }
        else if (!response.isNO()) {
            this.handleResult(response);
            return null;
        }
        return bodystructure;
    }
    
    public Flags fetchFlags(final int n) throws ProtocolException {
        Flags flags = null;
        final Response[] fetch = this.fetch(n, "FLAGS");
        Flags flags2;
        for (int i = 0; i < fetch.length; ++i, flags = flags2) {
            flags2 = flags;
            if (fetch[i] != null) {
                flags2 = flags;
                if (fetch[i] instanceof FetchResponse) {
                    if (((FetchResponse)fetch[i]).getNumber() != n) {
                        flags2 = flags;
                    }
                    else {
                        flags = (Flags)((FetchResponse)fetch[i]).getItem(Flags.class);
                        if ((flags2 = flags) != null) {
                            fetch[i] = null;
                            break;
                        }
                    }
                }
            }
        }
        this.notifyResponseHandlers(fetch);
        this.handleResult(fetch[fetch.length - 1]);
        return flags;
    }
    
    public RFC822DATA fetchRFC822(final int n, String string) throws ProtocolException {
        if (string == null) {
            string = "RFC822";
        }
        else {
            string = "RFC822." + string;
        }
        final Response[] fetch = this.fetch(n, string);
        this.notifyResponseHandlers(fetch);
        final Response response = fetch[fetch.length - 1];
        if (response.isOK()) {
            return (RFC822DATA)FetchResponse.getItem(fetch, n, RFC822DATA.class);
        }
        if (response.isNO()) {
            return null;
        }
        this.handleResult(response);
        return null;
    }
    
    public UID fetchSequenceNumber(final long n) throws ProtocolException {
        UID uid = null;
        final Response[] fetch = this.fetch(String.valueOf(n), "UID", true);
        while (true) {
            UID uid2;
            for (int i = 0; i < fetch.length; ++i, uid = uid2) {
                uid2 = uid;
                if (fetch[i] != null) {
                    if (!(fetch[i] instanceof FetchResponse)) {
                        uid2 = uid;
                    }
                    else {
                        final UID uid3 = (UID)((FetchResponse)fetch[i]).getItem(UID.class);
                        if ((uid2 = uid3) != null) {
                            final UID uid4 = uid3;
                            if (uid3.uid == n) {
                                this.notifyResponseHandlers(fetch);
                                this.handleResult(fetch[fetch.length - 1]);
                                return uid4;
                            }
                            uid2 = null;
                        }
                    }
                }
            }
            final UID uid4 = uid;
            continue;
        }
    }
    
    public UID[] fetchSequenceNumbers(final long n, final long n2) throws ProtocolException {
        final StringBuilder append = new StringBuilder(String.valueOf(String.valueOf(n))).append(":");
        String value;
        if (n2 == -1L) {
            value = "*";
        }
        else {
            value = String.valueOf(n2);
        }
        final Response[] fetch = this.fetch(append.append(value).toString(), "UID", true);
        final Vector<UID> vector = new Vector<UID>();
        for (int i = 0; i < fetch.length; ++i) {
            if (fetch[i] != null && fetch[i] instanceof FetchResponse) {
                final UID uid = (UID)((FetchResponse)fetch[i]).getItem(UID.class);
                if (uid != null) {
                    vector.addElement(uid);
                }
            }
        }
        this.notifyResponseHandlers(fetch);
        this.handleResult(fetch[fetch.length - 1]);
        final UID[] array = new UID[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    public UID[] fetchSequenceNumbers(final long[] array) throws ProtocolException {
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(String.valueOf(array[i]));
        }
        final Response[] fetch = this.fetch(sb.toString(), "UID", true);
        final Vector<UID> vector = new Vector<UID>();
        for (int j = 0; j < fetch.length; ++j) {
            if (fetch[j] != null && fetch[j] instanceof FetchResponse) {
                final UID uid = (UID)((FetchResponse)fetch[j]).getItem(UID.class);
                if (uid != null) {
                    vector.addElement(uid);
                }
            }
        }
        this.notifyResponseHandlers(fetch);
        this.handleResult(fetch[fetch.length - 1]);
        final UID[] array2 = new UID[vector.size()];
        vector.copyInto(array2);
        return array2;
    }
    
    public UID fetchUID(final int n) throws ProtocolException {
        UID uid = null;
        final Response[] fetch = this.fetch(n, "UID");
        this.notifyResponseHandlers(fetch);
        final Response response = fetch[fetch.length - 1];
        if (response.isOK()) {
            uid = (UID)FetchResponse.getItem(fetch, n, UID.class);
        }
        else if (!response.isNO()) {
            this.handleResult(response);
            return null;
        }
        return uid;
    }
    
    public ACL[] getACL(String encode) throws ProtocolException {
        if (!this.hasCapability("ACL")) {
            throw new BadCommandException("ACL not supported");
        }
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        final Response[] command = this.command("GETACL", argument);
        final Response response = command[command.length - 1];
        final Vector<ACL> vector = new Vector<ACL>();
        if (response.isOK()) {
            for (int i = 0; i < command.length; ++i) {
                if (command[i] instanceof IMAPResponse) {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    if (imapResponse.keyEquals("ACL")) {
                        imapResponse.readAtomString();
                        while (true) {
                            final String atomString = imapResponse.readAtomString();
                            if (atomString == null) {
                                break;
                            }
                            final String atomString2 = imapResponse.readAtomString();
                            if (atomString2 == null) {
                                break;
                            }
                            vector.addElement(new ACL(atomString, new Rights(atomString2)));
                        }
                        command[i] = null;
                    }
                }
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        final ACL[] array = new ACL[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    public Map getCapabilities() {
        return this.capabilities;
    }
    
    OutputStream getIMAPOutputStream() {
        return this.getOutputStream();
    }
    
    public Quota[] getQuota(final String s) throws ProtocolException {
        if (!this.hasCapability("QUOTA")) {
            throw new BadCommandException("QUOTA not supported");
        }
        final Argument argument = new Argument();
        argument.writeString(s);
        final Response[] command = this.command("GETQUOTA", argument);
        final Vector<Quota> vector = new Vector<Quota>();
        final Response response = command[command.length - 1];
        if (response.isOK()) {
            for (int i = 0; i < command.length; ++i) {
                if (command[i] instanceof IMAPResponse) {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    if (imapResponse.keyEquals("QUOTA")) {
                        vector.addElement(this.parseQuota(imapResponse));
                        command[i] = null;
                    }
                }
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        final Quota[] array = new Quota[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    public Quota[] getQuotaRoot(String encode) throws ProtocolException {
        if (!this.hasCapability("QUOTA")) {
            throw new BadCommandException("GETQUOTAROOT not supported");
        }
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        final Response[] command = this.command("GETQUOTAROOT", argument);
        final Response response = command[command.length - 1];
        final Hashtable<Object, Quota> hashtable = new Hashtable<Object, Quota>();
        if (response.isOK()) {
            for (int i = 0; i < command.length; ++i) {
                if (command[i] instanceof IMAPResponse) {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    if (imapResponse.keyEquals("QUOTAROOT")) {
                        imapResponse.readAtomString();
                        while (true) {
                            final String atomString = imapResponse.readAtomString();
                            if (atomString == null) {
                                break;
                            }
                            hashtable.put(atomString, new Quota(atomString));
                        }
                        command[i] = null;
                    }
                    else if (imapResponse.keyEquals("QUOTA")) {
                        final Quota quota = this.parseQuota(imapResponse);
                        final Quota quota2 = hashtable.get(quota.quotaRoot);
                        if (quota2 != null) {
                            final Quota.Resource[] resources = quota2.resources;
                        }
                        hashtable.put(quota.quotaRoot, quota);
                        command[i] = null;
                    }
                }
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        final Quota[] array = new Quota[hashtable.size()];
        final Enumeration<Quota> elements = hashtable.elements();
        int n = 0;
        while (elements.hasMoreElements()) {
            array[n] = elements.nextElement();
            ++n;
        }
        return array;
    }
    
    @Override
    protected ByteArray getResponseBuffer() {
        final ByteArray ba = this.ba;
        this.ba = null;
        return ba;
    }
    
    public boolean hasCapability(final String s) {
        return this.capabilities.containsKey(s.toUpperCase(Locale.ENGLISH));
    }
    
    public void idleAbort() throws ProtocolException {
        final OutputStream outputStream = this.getOutputStream();
        try {
            outputStream.write(IMAPProtocol.DONE);
            outputStream.flush();
        }
        catch (IOException ex) {}
    }
    
    public void idleStart() throws ProtocolException {
        synchronized (this) {
            if (!this.hasCapability("IDLE")) {
                throw new BadCommandException("IDLE not supported");
            }
        }
        while (true) {
            try {
                this.idleTag = this.writeCommand("IDLE", null);
                final Response response = this.readResponse();
                if (!response.isContinuation()) {
                    this.handleResult(response);
                }
            }
            // monitorexit(this)
            catch (LiteralException ex) {
                final Response response = ex.getResponse();
                continue;
            }
            catch (Exception ex2) {
                final Response response = Response.byeResponse(ex2);
                continue;
            }
            break;
        }
    }
    
    public boolean isAuthenticated() {
        return this.authenticated;
    }
    
    public boolean isREV1() {
        return this.rev1;
    }
    
    public ListInfo[] list(final String s, final String s2) throws ProtocolException {
        return this.doList("LIST", s, s2);
    }
    
    public Rights[] listRights(String encode, final String s) throws ProtocolException {
        if (!this.hasCapability("ACL")) {
            throw new BadCommandException("ACL not supported");
        }
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        argument.writeString(s);
        final Response[] command = this.command("LISTRIGHTS", argument);
        final Response response = command[command.length - 1];
        final Vector<Rights> vector = new Vector<Rights>();
        if (response.isOK()) {
            for (int i = 0; i < command.length; ++i) {
                if (command[i] instanceof IMAPResponse) {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    if (imapResponse.keyEquals("LISTRIGHTS")) {
                        imapResponse.readAtomString();
                        imapResponse.readAtomString();
                        while (true) {
                            final String atomString = imapResponse.readAtomString();
                            if (atomString == null) {
                                break;
                            }
                            vector.addElement(new Rights(atomString));
                        }
                        command[i] = null;
                    }
                }
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        final Rights[] array = new Rights[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    public void login(final String s, final String s2) throws ProtocolException {
        final Argument argument = new Argument();
        argument.writeString(s);
        argument.writeString(s2);
        final Response[] command = this.command("LOGIN", argument);
        this.notifyResponseHandlers(command);
        this.handleResult(command[command.length - 1]);
        this.setCapabilities(command[command.length - 1]);
        this.authenticated = true;
    }
    
    public void logout() throws ProtocolException {
        final Response[] command = this.command("LOGOUT", null);
        this.authenticated = false;
        this.notifyResponseHandlers(command);
        this.disconnect();
    }
    
    public ListInfo[] lsub(final String s, final String s2) throws ProtocolException {
        return this.doList("LSUB", s, s2);
    }
    
    public Rights myRights(String encode) throws ProtocolException {
        if (!this.hasCapability("ACL")) {
            throw new BadCommandException("ACL not supported");
        }
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        final Response[] command = this.command("MYRIGHTS", argument);
        final Response response = command[command.length - 1];
        Rights rights = null;
        final Rights rights2 = null;
        if (response.isOK()) {
            int i = 0;
            final int length = command.length;
            rights = rights2;
            while (i < length) {
                Rights rights3;
                if (!(command[i] instanceof IMAPResponse)) {
                    rights3 = rights;
                }
                else {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    rights3 = rights;
                    if (imapResponse.keyEquals("MYRIGHTS")) {
                        imapResponse.readAtomString();
                        final String atomString = imapResponse.readAtomString();
                        if ((rights3 = rights) == null) {
                            rights3 = new Rights(atomString);
                        }
                        command[i] = null;
                    }
                }
                ++i;
                rights = rights3;
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        return rights;
    }
    
    public Namespaces namespace() throws ProtocolException {
        if (!this.hasCapability("NAMESPACE")) {
            throw new BadCommandException("NAMESPACE not supported");
        }
        final Response[] command = this.command("NAMESPACE", null);
        Namespaces namespaces = null;
        final Namespaces namespaces2 = null;
        final Response response = command[command.length - 1];
        if (response.isOK()) {
            int i = 0;
            final int length = command.length;
            namespaces = namespaces2;
            while (i < length) {
                Namespaces namespaces3;
                if (!(command[i] instanceof IMAPResponse)) {
                    namespaces3 = namespaces;
                }
                else {
                    final IMAPResponse imapResponse = (IMAPResponse)command[i];
                    namespaces3 = namespaces;
                    if (imapResponse.keyEquals("NAMESPACE")) {
                        if ((namespaces3 = namespaces) == null) {
                            namespaces3 = new Namespaces(imapResponse);
                        }
                        command[i] = null;
                    }
                }
                ++i;
                namespaces = namespaces3;
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        return namespaces;
    }
    
    public void noop() throws ProtocolException {
        if (this.debug) {
            this.out.println("IMAP DEBUG: IMAPProtocol noop");
        }
        this.simpleCommand("NOOP", null);
    }
    
    protected void parseCapabilities(final Response response) {
        while (true) {
            final String atom = response.readAtom(']');
            if (atom == null) {
                break;
            }
            if (atom.length() == 0) {
                if (response.peekByte() == 93) {
                    break;
                }
                response.skipToken();
            }
            else {
                this.capabilities.put(atom.toUpperCase(Locale.ENGLISH), atom);
                if (!atom.regionMatches(true, 0, "AUTH=", 0, 5)) {
                    continue;
                }
                this.authmechs.add(atom.substring(5));
                if (!this.debug) {
                    continue;
                }
                this.out.println("IMAP DEBUG: AUTH: " + atom.substring(5));
            }
        }
    }
    
    public BODY peekBody(final int n, final String s) throws ProtocolException {
        return this.fetchBody(n, s, true);
    }
    
    public BODY peekBody(final int n, final String s, final int n2, final int n3) throws ProtocolException {
        return this.fetchBody(n, s, n2, n3, true, null);
    }
    
    public BODY peekBody(final int n, final String s, final int n2, final int n3, final ByteArray byteArray) throws ProtocolException {
        return this.fetchBody(n, s, n2, n3, true, byteArray);
    }
    
    @Override
    protected void processGreeting(final Response response) throws ProtocolException {
        super.processGreeting(response);
        if (response.isOK()) {
            this.setCapabilities(response);
            return;
        }
        if (((IMAPResponse)response).keyEquals("PREAUTH")) {
            this.authenticated = true;
            this.setCapabilities(response);
            return;
        }
        throw new ConnectionException(this, response);
    }
    
    public boolean processIdleResponse(final Response response) throws ProtocolException {
        boolean b = false;
        this.notifyResponseHandlers(new Response[] { response });
        if (response.isBYE()) {
            b = true;
        }
        boolean b2 = b;
        if (response.isTagged()) {
            b2 = b;
            if (response.getTag().equals(this.idleTag)) {
                b2 = true;
            }
        }
        if (b2) {
            this.idleTag = null;
        }
        this.handleResult(response);
        return !b2;
    }
    
    public void proxyauth(final String s) throws ProtocolException {
        final Argument argument = new Argument();
        argument.writeString(s);
        this.simpleCommand("PROXYAUTH", argument);
    }
    
    public Response readIdleResponse() {
        synchronized (this) {
            Response response;
            if (this.idleTag == null) {
                response = null;
            }
            else {
                try {
                    response = this.readResponse();
                }
                catch (IOException ex) {
                    response = Response.byeResponse(ex);
                }
                catch (ProtocolException ex2) {
                    response = Response.byeResponse(ex2);
                }
            }
            return response;
        }
    }
    
    @Override
    public Response readResponse() throws IOException, ProtocolException {
        return IMAPResponse.readResponse(this);
    }
    
    public void rename(String encode, String encode2) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        encode2 = BASE64MailboxEncoder.encode(encode2);
        final Argument argument = new Argument();
        argument.writeString(encode);
        argument.writeString(encode2);
        this.simpleCommand("RENAME", argument);
    }
    
    public void sasllogin(String[] array, final String s, final String s2, final String s3, final String s4) throws ProtocolException {
        Label_0130: {
            if (this.saslAuthenticator != null) {
                break Label_0130;
            }
        Label_0163_Outer:
            while (true) {
                while (true) {
                    Label_0285: {
                        while (true) {
                            ArrayList<String> list;
                            int n;
                            try {
                                final Constructor<?> constructor = Class.forName("com.sun.mail.imap.protocol.IMAPSaslAuthenticator").getConstructor(IMAPProtocol.class, String.class, Properties.class, Boolean.TYPE, PrintStream.class, String.class);
                                final String name = this.name;
                                final Properties props = this.props;
                                Boolean b;
                                if (this.debug) {
                                    b = Boolean.TRUE;
                                }
                                else {
                                    b = Boolean.FALSE;
                                }
                                this.saslAuthenticator = (SaslAuthenticator)constructor.newInstance(this, name, props, b, this.out, this.host);
                                if (array == null || array.length <= 0) {
                                    break Label_0285;
                                }
                                list = new ArrayList<String>(array.length);
                                n = 0;
                                if (n >= array.length) {
                                    final List<String> authmechs = list;
                                    array = authmechs.toArray(new String[authmechs.size()]);
                                    if (this.saslAuthenticator.authenticate(array, s, s2, s3, s4)) {
                                        this.authenticated = true;
                                    }
                                    return;
                                }
                            }
                            catch (Exception ex) {
                                if (this.debug) {
                                    this.out.println("IMAP DEBUG: Can't load SASL authenticator: " + ex);
                                }
                                return;
                            }
                            if (this.authmechs.contains(array[n])) {
                                list.add(array[n]);
                            }
                            ++n;
                            continue Label_0163_Outer;
                        }
                    }
                    final List<String> authmechs = (List<String>)this.authmechs;
                    continue;
                }
            }
        }
    }
    
    public int[] search(final SearchTerm searchTerm) throws ProtocolException, SearchException {
        return this.search("ALL", searchTerm);
    }
    
    public int[] search(final MessageSet[] array, final SearchTerm searchTerm) throws ProtocolException, SearchException {
        return this.search(MessageSet.toString(array), searchTerm);
    }
    
    public MailboxInfo select(String encode) throws ProtocolException {
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        final Response[] command = this.command("SELECT", argument);
        final MailboxInfo mailboxInfo = new MailboxInfo(command);
        this.notifyResponseHandlers(command);
        final Response response = command[command.length - 1];
        if (response.isOK()) {
            if (response.toString().indexOf("READ-ONLY") != -1) {
                mailboxInfo.mode = 1;
            }
            else {
                mailboxInfo.mode = 2;
            }
        }
        this.handleResult(response);
        return mailboxInfo;
    }
    
    public void setACL(String s, final char c, final ACL acl) throws ProtocolException {
        if (!this.hasCapability("ACL")) {
            throw new BadCommandException("ACL not supported");
        }
        s = BASE64MailboxEncoder.encode(s);
        final Argument argument = new Argument();
        argument.writeString(s);
        argument.writeString(acl.getName());
        final String string = acl.getRights().toString();
        Label_0091: {
            if (c != '+') {
                s = string;
                if (c != '-') {
                    break Label_0091;
                }
            }
            s = String.valueOf(c) + string;
        }
        argument.writeString(s);
        final Response[] command = this.command("SETACL", argument);
        final Response response = command[command.length - 1];
        this.notifyResponseHandlers(command);
        this.handleResult(response);
    }
    
    protected void setCapabilities(final Response response) {
        byte byte1;
        do {
            byte1 = response.readByte();
        } while (byte1 > 0 && byte1 != 91);
        if (byte1 != 0 && response.readAtom().equalsIgnoreCase("CAPABILITY")) {
            this.capabilities = new HashMap(10);
            this.authmechs = new ArrayList(5);
            this.parseCapabilities(response);
        }
    }
    
    public void setQuota(final Quota quota) throws ProtocolException {
        if (!this.hasCapability("QUOTA")) {
            throw new BadCommandException("QUOTA not supported");
        }
        final Argument argument = new Argument();
        argument.writeString(quota.quotaRoot);
        final Argument argument2 = new Argument();
        if (quota.resources != null) {
            for (int i = 0; i < quota.resources.length; ++i) {
                argument2.writeAtom(quota.resources[i].name);
                argument2.writeNumber(quota.resources[i].limit);
            }
        }
        argument.writeArgument(argument2);
        final Response[] command = this.command("SETQUOTA", argument);
        final Response response = command[command.length - 1];
        this.notifyResponseHandlers(command);
        this.handleResult(response);
    }
    
    public void startTLS() throws ProtocolException {
        try {
            super.startTLS("STARTTLS");
        }
        catch (ProtocolException ex) {
            throw ex;
        }
        catch (Exception ex2) {
            this.notifyResponseHandlers(new Response[] { Response.byeResponse(ex2) });
            this.disconnect();
        }
    }
    
    public Status status(String encode, final String[] array) throws ProtocolException {
        if (!this.isREV1() && !this.hasCapability("IMAP4SUNVERSION")) {
            throw new BadCommandException("STATUS not supported");
        }
        encode = BASE64MailboxEncoder.encode(encode);
        final Argument argument = new Argument();
        argument.writeString(encode);
        final Argument argument2 = new Argument();
        String[] standardItems;
        if ((standardItems = array) == null) {
            standardItems = Status.standardItems;
        }
        for (int i = 0; i < standardItems.length; ++i) {
            argument2.writeAtom(standardItems[i]);
        }
        argument.writeArgument(argument2);
        final Response[] command = this.command("STATUS", argument);
        Status status = null;
        final Status status2 = null;
        final Response response = command[command.length - 1];
        if (response.isOK()) {
            int j = 0;
            final int length = command.length;
            status = status2;
            while (j < length) {
                Status status3;
                if (!(command[j] instanceof IMAPResponse)) {
                    status3 = status;
                }
                else {
                    final IMAPResponse imapResponse = (IMAPResponse)command[j];
                    status3 = status;
                    if (imapResponse.keyEquals("STATUS")) {
                        if (status == null) {
                            status = new Status(imapResponse);
                        }
                        else {
                            Status.add(status, new Status(imapResponse));
                        }
                        command[j] = null;
                        status3 = status;
                    }
                }
                ++j;
                status = status3;
            }
        }
        this.notifyResponseHandlers(command);
        this.handleResult(response);
        return status;
    }
    
    public void storeFlags(final int n, final int n2, final Flags flags, final boolean b) throws ProtocolException {
        this.storeFlags(String.valueOf(String.valueOf(n)) + ":" + String.valueOf(n2), flags, b);
    }
    
    public void storeFlags(final int n, final Flags flags, final boolean b) throws ProtocolException {
        this.storeFlags(String.valueOf(n), flags, b);
    }
    
    public void storeFlags(final MessageSet[] array, final Flags flags, final boolean b) throws ProtocolException {
        this.storeFlags(MessageSet.toString(array), flags, b);
    }
    
    public void subscribe(final String s) throws ProtocolException {
        final Argument argument = new Argument();
        argument.writeString(BASE64MailboxEncoder.encode(s));
        this.simpleCommand("SUBSCRIBE", argument);
    }
    
    @Override
    protected boolean supportsNonSyncLiterals() {
        return this.hasCapability("LITERAL+");
    }
    
    public void uidexpunge(final UIDSet[] array) throws ProtocolException {
        if (!this.hasCapability("UIDPLUS")) {
            throw new BadCommandException("UID EXPUNGE not supported");
        }
        this.simpleCommand("UID EXPUNGE " + UIDSet.toString(array), null);
    }
    
    public void unsubscribe(final String s) throws ProtocolException {
        final Argument argument = new Argument();
        argument.writeString(BASE64MailboxEncoder.encode(s));
        this.simpleCommand("UNSUBSCRIBE", argument);
    }
}
