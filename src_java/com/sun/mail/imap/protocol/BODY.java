package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;
import java.io.*;

public class BODY implements Item
{
    static final char[] name;
    public ByteArray data;
    public int msgno;
    public int origin;
    public String section;
    
    static {
        name = new char[] { 'B', 'O', 'D', 'Y' };
    }
    
    public BODY(final FetchResponse fetchResponse) throws ParsingException {
        this.origin = 0;
        this.msgno = fetchResponse.getNumber();
        fetchResponse.skipSpaces();
        byte byte1;
        do {
            byte1 = fetchResponse.readByte();
            if (byte1 == 93) {
                if (fetchResponse.readByte() == 60) {
                    this.origin = fetchResponse.readNumber();
                    fetchResponse.skip(1);
                }
                this.data = fetchResponse.readByteArray();
                return;
            }
        } while (byte1 != 0);
        throw new ParsingException("BODY parse error: missing ``]'' at section end");
    }
    
    public ByteArray getByteArray() {
        return this.data;
    }
    
    public ByteArrayInputStream getByteArrayInputStream() {
        if (this.data != null) {
            return this.data.toByteArrayInputStream();
        }
        return null;
    }
}
