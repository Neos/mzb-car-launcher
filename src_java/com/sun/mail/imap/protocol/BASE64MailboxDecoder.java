package com.sun.mail.imap.protocol;

import java.text.*;

public class BASE64MailboxDecoder
{
    static final char[] pem_array;
    private static final byte[] pem_convert_array;
    
    static {
        pem_array = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', ',' };
        pem_convert_array = new byte[256];
        for (int i = 0; i < 255; ++i) {
            BASE64MailboxDecoder.pem_convert_array[i] = -1;
        }
        for (int j = 0; j < BASE64MailboxDecoder.pem_array.length; ++j) {
            BASE64MailboxDecoder.pem_convert_array[BASE64MailboxDecoder.pem_array[j]] = (byte)j;
        }
    }
    
    protected static int base64decode(final char[] array, int n, final CharacterIterator characterIterator) {
        boolean b = true;
        int n2 = -1;
        Block_2: {
            int n3;
            while (true) {
                final byte b2 = (byte)characterIterator.next();
                if (b2 == -1) {
                    n3 = n;
                    break;
                }
                if (b2 == 45) {
                    break Block_2;
                }
                final boolean b3 = false;
                final byte b4 = (byte)characterIterator.next();
                n3 = n;
                if (b4 == -1) {
                    break;
                }
                n3 = n;
                if (b4 == 45) {
                    break;
                }
                final byte b5 = BASE64MailboxDecoder.pem_convert_array[b2 & 0xFF];
                final byte b6 = BASE64MailboxDecoder.pem_convert_array[b4 & 0xFF];
                final byte b7 = (byte)((b5 << 2 & 0xFC) | (b6 >>> 4 & 0x3));
                int n4;
                int n5;
                if (n2 != -1) {
                    array[n] = (char)(n2 << 8 | (b7 & 0xFF));
                    n4 = -1;
                    n5 = n + 1;
                }
                else {
                    n4 = (b7 & 0xFF);
                    n5 = n;
                }
                final byte b8 = (byte)characterIterator.next();
                b = b3;
                n2 = n4;
                n = n5;
                if (b8 == 61) {
                    continue;
                }
                n3 = n5;
                if (b8 == -1) {
                    break;
                }
                n3 = n5;
                if (b8 == 45) {
                    break;
                }
                final byte b9 = BASE64MailboxDecoder.pem_convert_array[b8 & 0xFF];
                n = (byte)((b6 << 4 & 0xF0) | (b9 >>> 2 & 0xF));
                int n6;
                if (n4 != -1) {
                    array[n5] = (char)(n4 << 8 | (n & 0xFF));
                    n6 = -1;
                    ++n5;
                }
                else {
                    n6 = (n & 0xFF);
                }
                final byte b10 = (byte)characterIterator.next();
                b = b3;
                n2 = n6;
                n = n5;
                if (b10 == 61) {
                    continue;
                }
                n3 = n5;
                if (b10 == -1) {
                    break;
                }
                n3 = n5;
                if (b10 == 45) {
                    break;
                }
                n = (byte)((b9 << 6 & 0xC0) | (BASE64MailboxDecoder.pem_convert_array[b10 & 0xFF] & 0x3F));
                if (n6 != -1) {
                    final char c = (char)(n6 << 8 | (n & 0xFF));
                    array[n5] = (char)(n6 << 8 | (n & 0xFF));
                    n2 = -1;
                    n = n5 + 1;
                    b = b3;
                }
                else {
                    n2 = (n & 0xFF);
                    b = b3;
                    n = n5;
                }
            }
            return n3;
        }
        int n3 = n;
        if (b) {
            array[n] = '&';
            return n + 1;
        }
        return n3;
    }
    
    public static String decode(final String s) {
        if (s != null && s.length() != 0) {
            int n = 0;
            final char[] array = new char[s.length()];
            final StringCharacterIterator stringCharacterIterator = new StringCharacterIterator(s);
            char c = stringCharacterIterator.first();
            int base64decode = 0;
            while (c != '\uffff') {
                if (c == '&') {
                    n = 1;
                    base64decode = base64decode(array, base64decode, stringCharacterIterator);
                }
                else {
                    final int n2 = base64decode + 1;
                    array[base64decode] = c;
                    base64decode = n2;
                }
                c = stringCharacterIterator.next();
            }
            if (n != 0) {
                return new String(array, 0, base64decode);
            }
        }
        return s;
    }
}
