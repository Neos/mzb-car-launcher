package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;

public class UID implements Item
{
    static final char[] name;
    public int seqnum;
    public long uid;
    
    static {
        name = new char[] { 'U', 'I', 'D' };
    }
    
    public UID(final FetchResponse fetchResponse) throws ParsingException {
        this.seqnum = fetchResponse.getNumber();
        fetchResponse.skipSpaces();
        this.uid = fetchResponse.readLong();
    }
}
