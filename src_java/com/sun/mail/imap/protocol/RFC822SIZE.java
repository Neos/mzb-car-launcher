package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;

public class RFC822SIZE implements Item
{
    static final char[] name;
    public int msgno;
    public int size;
    
    static {
        name = new char[] { 'R', 'F', 'C', '8', '2', '2', '.', 'S', 'I', 'Z', 'E' };
    }
    
    public RFC822SIZE(final FetchResponse fetchResponse) throws ParsingException {
        this.msgno = fetchResponse.getNumber();
        fetchResponse.skipSpaces();
        this.size = fetchResponse.readNumber();
    }
}
