package com.sun.mail.imap.protocol;

import java.io.*;
import com.sun.mail.iap.*;
import java.util.*;

public class FetchResponse extends IMAPResponse
{
    private static final char[] HEADER;
    private static final char[] TEXT;
    private Item[] items;
    
    static {
        HEADER = new char[] { '.', 'H', 'E', 'A', 'D', 'E', 'R' };
        TEXT = new char[] { '.', 'T', 'E', 'X', 'T' };
    }
    
    public FetchResponse(final Protocol protocol) throws IOException, ProtocolException {
        super(protocol);
        this.parse();
    }
    
    public FetchResponse(final IMAPResponse imapResponse) throws IOException, ProtocolException {
        super(imapResponse);
        this.parse();
    }
    
    public static Item getItem(final Response[] array, final int n, final Class clazz) {
        if (array == null) {
            return null;
        }
        for (int i = 0; i < array.length; ++i) {
            if (array[i] != null && array[i] instanceof FetchResponse && ((FetchResponse)array[i]).getNumber() == n) {
                final FetchResponse fetchResponse = (FetchResponse)array[i];
                for (int j = 0; j < fetchResponse.items.length; ++j) {
                    if (clazz.isInstance(fetchResponse.items[j])) {
                        return fetchResponse.items[j];
                    }
                }
            }
        }
        return null;
    }
    
    private boolean match(final char[] array) {
        final int length = array.length;
        int index = this.index;
        for (int i = 0; i < length; ++i) {
            if (Character.toUpperCase((char)this.buffer[index]) != array[i]) {
                return false;
            }
            ++index;
        }
        return true;
    }
    
    private void parse() throws ParsingException {
        this.skipSpaces();
        if (this.buffer[this.index] != 40) {
            throw new ParsingException("error in FETCH parsing, missing '(' at index " + this.index);
        }
        final Vector<Object> vector = new Vector<Object>();
        Object o = null;
        while (true) {
            ++this.index;
            if (this.index >= this.size) {
                throw new ParsingException("error in FETCH parsing, ran off end of buffer, size " + this.size);
            }
            switch (this.buffer[this.index]) {
                case 69: {
                    if (this.match(ENVELOPE.name)) {
                        this.index += ENVELOPE.name.length;
                        o = new ENVELOPE(this);
                        break;
                    }
                    break;
                }
                case 70: {
                    if (this.match(FLAGS.name)) {
                        this.index += FLAGS.name.length;
                        o = new FLAGS(this);
                        break;
                    }
                    break;
                }
                case 73: {
                    if (this.match(INTERNALDATE.name)) {
                        this.index += INTERNALDATE.name.length;
                        o = new INTERNALDATE(this);
                        break;
                    }
                    break;
                }
                case 66: {
                    if (!this.match(BODY.name)) {
                        break;
                    }
                    if (this.buffer[this.index + 4] == 91) {
                        this.index += BODY.name.length;
                        o = new BODY(this);
                        break;
                    }
                    if (this.match(BODYSTRUCTURE.name)) {
                        this.index += BODYSTRUCTURE.name.length;
                    }
                    else {
                        this.index += BODY.name.length;
                    }
                    o = new BODYSTRUCTURE(this);
                    break;
                }
                case 82: {
                    if (this.match(RFC822SIZE.name)) {
                        this.index += RFC822SIZE.name.length;
                        o = new RFC822SIZE(this);
                        break;
                    }
                    if (this.match(RFC822DATA.name)) {
                        this.index += RFC822DATA.name.length;
                        if (this.match(FetchResponse.HEADER)) {
                            this.index += FetchResponse.HEADER.length;
                        }
                        else if (this.match(FetchResponse.TEXT)) {
                            this.index += FetchResponse.TEXT.length;
                        }
                        o = new RFC822DATA(this);
                        break;
                    }
                    break;
                }
                case 85: {
                    if (this.match(UID.name)) {
                        this.index += UID.name.length;
                        o = new UID(this);
                        break;
                    }
                    break;
                }
            }
            if (o != null) {
                vector.addElement(o);
            }
            if (this.buffer[this.index] == 41) {
                ++this.index;
                vector.copyInto(this.items = new Item[vector.size()]);
            }
        }
    }
    
    public Item getItem(final int n) {
        return this.items[n];
    }
    
    public Item getItem(final Class clazz) {
        for (int i = 0; i < this.items.length; ++i) {
            if (clazz.isInstance(this.items[i])) {
                return this.items[i];
            }
        }
        return null;
    }
    
    public int getItemCount() {
        return this.items.length;
    }
}
