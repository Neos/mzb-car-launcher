package com.sun.mail.imap.protocol;

import javax.mail.*;
import com.sun.mail.iap.*;

public class FLAGS extends Flags implements Item
{
    static final char[] name;
    private static final long serialVersionUID = 439049847053756670L;
    public int msgno;
    
    static {
        name = new char[] { 'F', 'L', 'A', 'G', 'S' };
    }
    
    public FLAGS(final IMAPResponse imapResponse) throws ParsingException {
        this.msgno = imapResponse.getNumber();
        imapResponse.skipSpaces();
        final String[] simpleList = imapResponse.readSimpleList();
        if (simpleList != null) {
            for (int i = 0; i < simpleList.length; ++i) {
                final String s = simpleList[i];
                if (s.length() >= 2 && s.charAt(0) == '\\') {
                    switch (Character.toUpperCase(s.charAt(1))) {
                        default: {
                            this.add(s);
                            break;
                        }
                        case 'S': {
                            this.add(Flag.SEEN);
                            break;
                        }
                        case 'R': {
                            this.add(Flag.RECENT);
                            break;
                        }
                        case 'D': {
                            if (s.length() < 3) {
                                this.add(s);
                                break;
                            }
                            final char char1 = s.charAt(2);
                            if (char1 == 'e' || char1 == 'E') {
                                this.add(Flag.DELETED);
                                break;
                            }
                            if (char1 == 'r' || char1 == 'R') {
                                this.add(Flag.DRAFT);
                                break;
                            }
                            break;
                        }
                        case 'A': {
                            this.add(Flag.ANSWERED);
                            break;
                        }
                        case 'F': {
                            this.add(Flag.FLAGGED);
                            break;
                        }
                        case '*': {
                            this.add(Flag.USER);
                            break;
                        }
                    }
                }
                else {
                    this.add(s);
                }
            }
        }
    }
}
