package com.sun.mail.imap.protocol;

import javax.mail.*;
import com.sun.mail.iap.*;

public class MailboxInfo
{
    public Flags availableFlags;
    public int first;
    public int mode;
    public Flags permanentFlags;
    public int recent;
    public int total;
    public long uidnext;
    public long uidvalidity;
    
    public MailboxInfo(final Response[] array) throws ParsingException {
        this.availableFlags = null;
        this.permanentFlags = null;
        this.total = -1;
        this.recent = -1;
        this.first = -1;
        this.uidvalidity = -1L;
        this.uidnext = -1L;
        for (int i = 0; i < array.length; ++i) {
            if (array[i] != null && array[i] instanceof IMAPResponse) {
                final IMAPResponse imapResponse = (IMAPResponse)array[i];
                if (imapResponse.keyEquals("EXISTS")) {
                    this.total = imapResponse.getNumber();
                    array[i] = null;
                }
                else if (imapResponse.keyEquals("RECENT")) {
                    this.recent = imapResponse.getNumber();
                    array[i] = null;
                }
                else if (imapResponse.keyEquals("FLAGS")) {
                    this.availableFlags = new FLAGS(imapResponse);
                    array[i] = null;
                }
                else if (imapResponse.isUnTagged() && imapResponse.isOK()) {
                    imapResponse.skipSpaces();
                    if (imapResponse.readByte() != 91) {
                        imapResponse.reset();
                    }
                    else {
                        int n = 1;
                        final String atom = imapResponse.readAtom();
                        if (atom.equalsIgnoreCase("UNSEEN")) {
                            this.first = imapResponse.readNumber();
                        }
                        else if (atom.equalsIgnoreCase("UIDVALIDITY")) {
                            this.uidvalidity = imapResponse.readLong();
                        }
                        else if (atom.equalsIgnoreCase("PERMANENTFLAGS")) {
                            this.permanentFlags = new FLAGS(imapResponse);
                        }
                        else if (atom.equalsIgnoreCase("UIDNEXT")) {
                            this.uidnext = imapResponse.readLong();
                        }
                        else {
                            n = 0;
                        }
                        if (n != 0) {
                            array[i] = null;
                        }
                        else {
                            imapResponse.reset();
                        }
                    }
                }
            }
        }
        if (this.permanentFlags == null) {
            if (this.availableFlags == null) {
                this.permanentFlags = new Flags();
                return;
            }
            this.permanentFlags = new Flags(this.availableFlags);
        }
    }
}
