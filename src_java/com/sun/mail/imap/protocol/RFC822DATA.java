package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;
import java.io.*;

public class RFC822DATA implements Item
{
    static final char[] name;
    public ByteArray data;
    public int msgno;
    
    static {
        name = new char[] { 'R', 'F', 'C', '8', '2', '2' };
    }
    
    public RFC822DATA(final FetchResponse fetchResponse) throws ParsingException {
        this.msgno = fetchResponse.getNumber();
        fetchResponse.skipSpaces();
        this.data = fetchResponse.readByteArray();
    }
    
    public ByteArray getByteArray() {
        return this.data;
    }
    
    public ByteArrayInputStream getByteArrayInputStream() {
        if (this.data != null) {
            return this.data.toByteArrayInputStream();
        }
        return null;
    }
}
