package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;
import java.util.*;
import javax.mail.internet.*;

class IMAPAddress extends InternetAddress
{
    private static final long serialVersionUID = -3835822029483122232L;
    private boolean group;
    private InternetAddress[] grouplist;
    private String groupname;
    
    IMAPAddress(final Response response) throws ParsingException {
        this.group = false;
        response.skipSpaces();
        if (response.readByte() != 40) {
            throw new ParsingException("ADDRESS parse error");
        }
        this.encodedPersonal = response.readString();
        response.readString();
        final String string = response.readString();
        final String string2 = response.readString();
        if (response.readByte() != 41) {
            throw new ParsingException("ADDRESS parse error");
        }
        if (string2 == null) {
            this.group = true;
            this.groupname = string;
            if (this.groupname == null) {
                return;
            }
            final StringBuffer sb = new StringBuffer();
            sb.append(this.groupname).append(':');
            final Vector<IMAPAddress> vector = new Vector<IMAPAddress>();
            while (response.peekByte() != 41) {
                final IMAPAddress imapAddress = new IMAPAddress(response);
                if (imapAddress.isEndOfGroup()) {
                    break;
                }
                if (vector.size() != 0) {
                    sb.append(',');
                }
                sb.append(imapAddress.toString());
                vector.addElement(imapAddress);
            }
            sb.append(';');
            this.address = sb.toString();
            vector.copyInto(this.grouplist = new IMAPAddress[vector.size()]);
        }
        else {
            if (string == null || string.length() == 0) {
                this.address = string2;
                return;
            }
            if (string2.length() == 0) {
                this.address = string;
                return;
            }
            this.address = String.valueOf(string) + "@" + string2;
        }
    }
    
    @Override
    public InternetAddress[] getGroup(final boolean b) throws AddressException {
        if (this.grouplist == null) {
            return null;
        }
        return this.grouplist.clone();
    }
    
    boolean isEndOfGroup() {
        return this.group && this.groupname == null;
    }
    
    @Override
    public boolean isGroup() {
        return this.group;
    }
}
