package com.sun.mail.imap.protocol;

import javax.security.auth.callback.*;
import java.util.*;
import com.sun.mail.util.*;
import java.io.*;
import com.sun.mail.iap.*;
import javax.security.sasl.*;

public class IMAPSaslAuthenticator implements SaslAuthenticator
{
    private boolean debug;
    private String host;
    private String name;
    private PrintStream out;
    private IMAPProtocol pr;
    private Properties props;
    
    public IMAPSaslAuthenticator(final IMAPProtocol pr, final String name, final Properties props, final boolean debug, final PrintStream out, final String host) {
        this.pr = pr;
        this.name = name;
        this.props = props;
        this.debug = debug;
        this.out = out;
        this.host = host;
    }
    
    @Override
    public boolean authenticate(final String[] array, String response, final String s, String saslClient, String writeCommand) throws ProtocolException {
        Vector<String> vector = null;
    Label_0313_Outer:
        while (true) {
        Label_0313:
            while (true) {
                synchronized (this.pr) {
                    vector = new Vector<String>();
                    final int n = 0;
                Label_0188:
                    while (true) {
                        Label_0117: {
                            Label_0057: {
                                if (!this.debug) {
                                    break Label_0057;
                                }
                                this.out.print("IMAP SASL DEBUG: Mechanisms:");
                                final int n2 = 0;
                                if (n2 < array.length) {
                                    break Label_0117;
                                }
                                this.out.println();
                            }
                            response = (String)new CallbackHandler() {
                                @Override
                                public void handle(final Callback[] array) {
                                    if (IMAPSaslAuthenticator.this.debug) {
                                        IMAPSaslAuthenticator.this.out.println("IMAP SASL DEBUG: callback length: " + array.length);
                                    }
                                    for (int i = 0; i < array.length; ++i) {
                                        if (IMAPSaslAuthenticator.this.debug) {
                                            IMAPSaslAuthenticator.this.out.println("IMAP SASL DEBUG: callback " + i + ": " + array[i]);
                                        }
                                        if (array[i] instanceof NameCallback) {
                                            ((NameCallback)array[i]).setName(saslClient);
                                        }
                                        else if (array[i] instanceof PasswordCallback) {
                                            ((PasswordCallback)array[i]).setPassword(writeCommand.toCharArray());
                                        }
                                        else if (array[i] instanceof RealmCallback) {
                                            final RealmCallback realmCallback = (RealmCallback)array[i];
                                            String text;
                                            if (response != null) {
                                                text = response;
                                            }
                                            else {
                                                text = realmCallback.getDefaultText();
                                            }
                                            realmCallback.setText(text);
                                        }
                                        else if (array[i] instanceof RealmChoiceCallback) {
                                            final RealmChoiceCallback realmChoiceCallback = (RealmChoiceCallback)array[i];
                                            if (response == null) {
                                                realmChoiceCallback.setSelectedIndex(realmChoiceCallback.getDefaultChoice());
                                            }
                                            else {
                                                final String[] choices = realmChoiceCallback.getChoices();
                                                for (int j = 0; j < choices.length; ++j) {
                                                    if (choices[j].equals(response)) {
                                                        realmChoiceCallback.setSelectedIndex(j);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            };
                            try {
                                saslClient = (String)Sasl.createSaslClient(array, s, this.name, this.host, (Map<String, ?>)this.props, (CallbackHandler)response);
                                if (saslClient == null) {
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: No SASL support");
                                    }
                                    return false;
                                }
                                break Label_0188;
                                final int n2;
                                this.out.print(" " + array[n2]);
                                ++n2;
                                continue Label_0313_Outer;
                            }
                            catch (SaslException ex) {
                                if (this.debug) {
                                    this.out.println("IMAP SASL DEBUG: Failed to create SASL client: " + ex);
                                }
                                return false;
                            }
                        }
                        break;
                    }
                    if (this.debug) {
                        this.out.println("IMAP SASL DEBUG: SASL client " + ((SaslClient)saslClient).getMechanismName());
                    }
                    while (true) {
                        OutputStream imapOutputStream;
                        ByteArrayOutputStream byteArrayOutputStream;
                        byte[] array2;
                        boolean equals;
                        try {
                            writeCommand = this.pr.writeCommand("AUTHENTICATE " + ((SaslClient)saslClient).getMechanismName(), null);
                            imapOutputStream = this.pr.getIMAPOutputStream();
                            byteArrayOutputStream = new ByteArrayOutputStream();
                            final byte[] array3;
                            array2 = (array3 = new byte[2]);
                            array3[0] = 13;
                            array3[1] = 10;
                            equals = ((SaslClient)saslClient).getMechanismName().equals("XGWTRUSTEDAPP");
                            final int n2 = n;
                            if (n2 != 0) {
                                if (!((SaslClient)saslClient).isComplete()) {
                                    break Label_0313_Outer;
                                }
                                response = (String)((SaslClient)saslClient).getNegotiatedProperty("javax.security.sasl.qop");
                                if (response != null && (response.equalsIgnoreCase("auth-int") || response.equalsIgnoreCase("auth-conf"))) {
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: Mechanism requires integrity or confidentiality");
                                    }
                                    return false;
                                }
                                break Label_0313_Outer;
                            }
                        }
                        catch (Exception ex2) {
                            if (this.debug) {
                                this.out.println("IMAP SASL DEBUG: AUTHENTICATE Exception: " + ex2);
                            }
                            return false;
                        }
                        byte[] evaluateChallenge = null;
                        Label_0585: {
                            try {
                                response = (String)this.pr.readResponse();
                                if (!((Response)response).isContinuation()) {
                                    break;
                                }
                                evaluateChallenge = null;
                                if (!((SaslClient)saslClient).isComplete()) {
                                    byte[] array5;
                                    final byte[] array4 = array5 = ((Response)response).readByteArray().getNewBytes();
                                    if (array4.length > 0) {
                                        array5 = BASE64DecoderStream.decode(array4);
                                    }
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: challenge: " + ASCIIUtility.toString(array5, 0, array5.length) + " :");
                                    }
                                    evaluateChallenge = ((SaslClient)saslClient).evaluateChallenge(array5);
                                }
                                if (evaluateChallenge == null) {
                                    if (this.debug) {
                                        this.out.println("IMAP SASL DEBUG: no response");
                                    }
                                    imapOutputStream.write(array2);
                                    imapOutputStream.flush();
                                    byteArrayOutputStream.reset();
                                    continue Label_0313;
                                }
                                break Label_0585;
                            }
                            catch (Exception ex3) {
                                if (this.debug) {
                                    ex3.printStackTrace();
                                }
                                Response.byeResponse(ex3);
                                final int n2 = 1;
                                continue Label_0313;
                            }
                            continue Label_0313;
                        }
                        if (this.debug) {
                            this.out.println("IMAP SASL DEBUG: response: " + ASCIIUtility.toString(evaluateChallenge, 0, evaluateChallenge.length) + " :");
                        }
                        final byte[] encode = BASE64EncoderStream.encode(evaluateChallenge);
                        if (equals) {
                            byteArrayOutputStream.write("XGWTRUSTEDAPP ".getBytes());
                        }
                        byteArrayOutputStream.write(encode);
                        byteArrayOutputStream.write(array2);
                        imapOutputStream.write(byteArrayOutputStream.toByteArray());
                        imapOutputStream.flush();
                        byteArrayOutputStream.reset();
                        continue Label_0313;
                    }
                }
                if (((Response)response).isTagged() && ((Response)response).getTag().equals(writeCommand)) {
                    final int n2 = 1;
                    continue Label_0313;
                }
                if (((Response)response).isBYE()) {
                    final int n2 = 1;
                    continue Label_0313;
                }
                vector.addElement(response);
                continue Label_0313;
            }
        }
        final Response[] array6 = new Response[vector.size()];
        vector.copyInto(array6);
        this.pr.notifyResponseHandlers(array6);
        final Response capabilities;
        this.pr.handleResult(capabilities);
        this.pr.setCapabilities(capabilities);
        // monitorexit(imapProtocol)
        return true;
    }
}
