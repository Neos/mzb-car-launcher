package com.sun.mail.imap.protocol;

import java.io.*;
import com.sun.mail.iap.*;
import java.util.*;
import com.sun.mail.util.*;

public class IMAPResponse extends Response
{
    private String key;
    private int number;
    
    public IMAPResponse(final Protocol protocol) throws IOException, ProtocolException {
        super(protocol);
        if (!this.isUnTagged() || this.isOK() || this.isNO() || this.isBAD() || this.isBYE()) {
            return;
        }
        this.key = this.readAtom();
        try {
            this.number = Integer.parseInt(this.key);
            this.key = this.readAtom();
        }
        catch (NumberFormatException ex) {}
    }
    
    public IMAPResponse(final IMAPResponse imapResponse) {
        super(imapResponse);
        this.key = imapResponse.key;
        this.number = imapResponse.number;
    }
    
    public static IMAPResponse readResponse(final Protocol protocol) throws IOException, ProtocolException {
        IMAPResponse imapResponse2;
        final IMAPResponse imapResponse = imapResponse2 = new IMAPResponse(protocol);
        if (imapResponse.keyEquals("FETCH")) {
            imapResponse2 = new FetchResponse(imapResponse);
        }
        return imapResponse2;
    }
    
    public String getKey() {
        return this.key;
    }
    
    public int getNumber() {
        return this.number;
    }
    
    public boolean keyEquals(final String s) {
        return this.key != null && this.key.equalsIgnoreCase(s);
    }
    
    public String[] readSimpleList() {
        this.skipSpaces();
        if (this.buffer[this.index] == 40) {
            ++this.index;
            final Vector<String> vector = new Vector<String>();
            int index = this.index;
            while (this.buffer[this.index] != 41) {
                int n = index;
                if (this.buffer[this.index] == 32) {
                    vector.addElement(ASCIIUtility.toString(this.buffer, index, this.index));
                    n = this.index + 1;
                }
                ++this.index;
                index = n;
            }
            if (this.index > index) {
                vector.addElement(ASCIIUtility.toString(this.buffer, index, this.index));
            }
            ++this.index;
            final int size = vector.size();
            if (size > 0) {
                final String[] array = new String[size];
                vector.copyInto(array);
                return array;
            }
        }
        return null;
    }
}
