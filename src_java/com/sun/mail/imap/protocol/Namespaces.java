package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;
import java.util.*;

public class Namespaces
{
    public Namespace[] otherUsers;
    public Namespace[] personal;
    public Namespace[] shared;
    
    public Namespaces(final Response response) throws ProtocolException {
        this.personal = this.getNamespaces(response);
        this.otherUsers = this.getNamespaces(response);
        this.shared = this.getNamespaces(response);
    }
    
    private Namespace[] getNamespaces(final Response response) throws ProtocolException {
        response.skipSpaces();
        if (response.peekByte() == 40) {
            final Vector<Namespace> vector = new Vector<Namespace>();
            response.readByte();
            do {
                vector.addElement(new Namespace(response));
            } while (response.peekByte() != 41);
            response.readByte();
            final Namespace[] array = new Namespace[vector.size()];
            vector.copyInto(array);
            return array;
        }
        final String atom = response.readAtom();
        if (atom == null) {
            throw new ProtocolException("Expected NIL, got null");
        }
        if (!atom.equalsIgnoreCase("NIL")) {
            throw new ProtocolException("Expected NIL, got " + atom);
        }
        return null;
    }
    
    public static class Namespace
    {
        public char delimiter;
        public String prefix;
        
        public Namespace(final Response response) throws ProtocolException {
            if (response.readByte() != 40) {
                throw new ProtocolException("Missing '(' at start of Namespace");
            }
            this.prefix = BASE64MailboxDecoder.decode(response.readString());
            response.skipSpaces();
            if (response.peekByte() == 34) {
                response.readByte();
                this.delimiter = (char)response.readByte();
                if (this.delimiter == '\\') {
                    this.delimiter = (char)response.readByte();
                }
                if (response.readByte() != 34) {
                    throw new ProtocolException("Missing '\"' at end of QUOTED_CHAR");
                }
            }
            else {
                final String atom = response.readAtom();
                if (atom == null) {
                    throw new ProtocolException("Expected NIL, got null");
                }
                if (!atom.equalsIgnoreCase("NIL")) {
                    throw new ProtocolException("Expected NIL, got " + atom);
                }
                this.delimiter = '\0';
            }
            if (response.peekByte() != 41) {
                response.skipSpaces();
                response.readString();
                response.skipSpaces();
                response.readStringList();
            }
            if (response.readByte() != 41) {
                throw new ProtocolException("Missing ')' at end of Namespace");
            }
        }
    }
}
