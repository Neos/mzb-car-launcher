package com.sun.mail.imap.protocol;

import com.sun.mail.iap.*;
import java.io.*;
import javax.mail.search.*;
import javax.mail.*;
import java.util.*;

class SearchSequence
{
    private static Calendar cal;
    private static String[] monthTable;
    
    static {
        SearchSequence.monthTable = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        SearchSequence.cal = new GregorianCalendar();
    }
    
    private static Argument and(final AndTerm andTerm, final String s) throws SearchException, IOException {
        final SearchTerm[] terms = andTerm.getTerms();
        final Argument generateSequence = generateSequence(terms[0], s);
        for (int i = 1; i < terms.length; ++i) {
            generateSequence.append(generateSequence(terms[i], s));
        }
        return generateSequence;
    }
    
    private static Argument body(final BodyTerm bodyTerm, final String s) throws SearchException, IOException {
        final Argument argument = new Argument();
        argument.writeAtom("BODY");
        argument.writeString(bodyTerm.getPattern(), s);
        return argument;
    }
    
    private static Argument flag(final FlagTerm flagTerm) throws SearchException {
        final boolean testSet = flagTerm.getTestSet();
        final Argument argument = new Argument();
        final Flags flags = flagTerm.getFlags();
        final Flags.Flag[] systemFlags = flags.getSystemFlags();
        final String[] userFlags = flags.getUserFlags();
        if (systemFlags.length == 0 && userFlags.length == 0) {
            throw new SearchException("Invalid FlagTerm");
        }
        for (int i = 0; i < systemFlags.length; ++i) {
            if (systemFlags[i] == Flags.Flag.DELETED) {
                String s;
                if (testSet) {
                    s = "DELETED";
                }
                else {
                    s = "UNDELETED";
                }
                argument.writeAtom(s);
            }
            else if (systemFlags[i] == Flags.Flag.ANSWERED) {
                String s2;
                if (testSet) {
                    s2 = "ANSWERED";
                }
                else {
                    s2 = "UNANSWERED";
                }
                argument.writeAtom(s2);
            }
            else if (systemFlags[i] == Flags.Flag.DRAFT) {
                String s3;
                if (testSet) {
                    s3 = "DRAFT";
                }
                else {
                    s3 = "UNDRAFT";
                }
                argument.writeAtom(s3);
            }
            else if (systemFlags[i] == Flags.Flag.FLAGGED) {
                String s4;
                if (testSet) {
                    s4 = "FLAGGED";
                }
                else {
                    s4 = "UNFLAGGED";
                }
                argument.writeAtom(s4);
            }
            else if (systemFlags[i] == Flags.Flag.RECENT) {
                String s5;
                if (testSet) {
                    s5 = "RECENT";
                }
                else {
                    s5 = "OLD";
                }
                argument.writeAtom(s5);
            }
            else if (systemFlags[i] == Flags.Flag.SEEN) {
                String s6;
                if (testSet) {
                    s6 = "SEEN";
                }
                else {
                    s6 = "UNSEEN";
                }
                argument.writeAtom(s6);
            }
        }
        for (int j = 0; j < userFlags.length; ++j) {
            String s7;
            if (testSet) {
                s7 = "KEYWORD";
            }
            else {
                s7 = "UNKEYWORD";
            }
            argument.writeAtom(s7);
            argument.writeAtom(userFlags[j]);
        }
        return argument;
    }
    
    private static Argument from(final String s, final String s2) throws SearchException, IOException {
        final Argument argument = new Argument();
        argument.writeAtom("FROM");
        argument.writeString(s, s2);
        return argument;
    }
    
    static Argument generateSequence(final SearchTerm searchTerm, final String s) throws SearchException, IOException {
        if (searchTerm instanceof AndTerm) {
            return and((AndTerm)searchTerm, s);
        }
        if (searchTerm instanceof OrTerm) {
            return or((OrTerm)searchTerm, s);
        }
        if (searchTerm instanceof NotTerm) {
            return not((NotTerm)searchTerm, s);
        }
        if (searchTerm instanceof HeaderTerm) {
            return header((HeaderTerm)searchTerm, s);
        }
        if (searchTerm instanceof FlagTerm) {
            return flag((FlagTerm)searchTerm);
        }
        if (searchTerm instanceof FromTerm) {
            return from(((FromTerm)searchTerm).getAddress().toString(), s);
        }
        if (searchTerm instanceof FromStringTerm) {
            return from(((FromStringTerm)searchTerm).getPattern(), s);
        }
        if (searchTerm instanceof RecipientTerm) {
            final RecipientTerm recipientTerm = (RecipientTerm)searchTerm;
            return recipient(recipientTerm.getRecipientType(), recipientTerm.getAddress().toString(), s);
        }
        if (searchTerm instanceof RecipientStringTerm) {
            final RecipientStringTerm recipientStringTerm = (RecipientStringTerm)searchTerm;
            return recipient(recipientStringTerm.getRecipientType(), recipientStringTerm.getPattern(), s);
        }
        if (searchTerm instanceof SubjectTerm) {
            return subject((SubjectTerm)searchTerm, s);
        }
        if (searchTerm instanceof BodyTerm) {
            return body((BodyTerm)searchTerm, s);
        }
        if (searchTerm instanceof SizeTerm) {
            return size((SizeTerm)searchTerm);
        }
        if (searchTerm instanceof SentDateTerm) {
            return sentdate((DateTerm)searchTerm);
        }
        if (searchTerm instanceof ReceivedDateTerm) {
            return receiveddate((DateTerm)searchTerm);
        }
        if (searchTerm instanceof MessageIDTerm) {
            return messageid((MessageIDTerm)searchTerm, s);
        }
        throw new SearchException("Search too complex");
    }
    
    private static Argument header(final HeaderTerm headerTerm, final String s) throws SearchException, IOException {
        final Argument argument = new Argument();
        argument.writeAtom("HEADER");
        argument.writeString(headerTerm.getHeaderName());
        argument.writeString(headerTerm.getPattern(), s);
        return argument;
    }
    
    private static boolean isAscii(final String s) {
        for (int length = s.length(), i = 0; i < length; ++i) {
            if (s.charAt(i) > '\u007f') {
                return false;
            }
        }
        return true;
    }
    
    static boolean isAscii(final SearchTerm searchTerm) {
        if (searchTerm instanceof AndTerm || searchTerm instanceof OrTerm) {
            SearchTerm[] array;
            if (searchTerm instanceof AndTerm) {
                array = ((AndTerm)searchTerm).getTerms();
            }
            else {
                array = ((OrTerm)searchTerm).getTerms();
            }
            for (int i = 0; i < array.length; ++i) {
                if (!isAscii(array[i])) {
                    return false;
                }
            }
        }
        else {
            if (searchTerm instanceof NotTerm) {
                return isAscii(((NotTerm)searchTerm).getTerm());
            }
            if (searchTerm instanceof StringTerm) {
                return isAscii(((StringTerm)searchTerm).getPattern());
            }
            if (searchTerm instanceof AddressTerm) {
                return isAscii(((AddressTerm)searchTerm).getAddress().toString());
            }
        }
        return true;
    }
    
    private static Argument messageid(final MessageIDTerm messageIDTerm, final String s) throws SearchException, IOException {
        final Argument argument = new Argument();
        argument.writeAtom("HEADER");
        argument.writeString("Message-ID");
        argument.writeString(messageIDTerm.getPattern(), s);
        return argument;
    }
    
    private static Argument not(final NotTerm notTerm, final String s) throws SearchException, IOException {
        final Argument argument = new Argument();
        argument.writeAtom("NOT");
        final SearchTerm term = notTerm.getTerm();
        if (term instanceof AndTerm || term instanceof FlagTerm) {
            argument.writeArgument(generateSequence(term, s));
            return argument;
        }
        argument.append(generateSequence(term, s));
        return argument;
    }
    
    private static Argument or(final OrTerm orTerm, final String s) throws SearchException, IOException {
        SearchTerm[] array2;
        final SearchTerm[] array = array2 = orTerm.getTerms();
        if (array.length > 2) {
            SearchTerm searchTerm = array[0];
            for (int i = 1; i < array.length; ++i) {
                searchTerm = new OrTerm(searchTerm, array[i]);
            }
            array2 = ((OrTerm)searchTerm).getTerms();
        }
        final Argument argument = new Argument();
        if (array2.length > 1) {
            argument.writeAtom("OR");
        }
        if (array2[0] instanceof AndTerm || array2[0] instanceof FlagTerm) {
            argument.writeArgument(generateSequence(array2[0], s));
        }
        else {
            argument.append(generateSequence(array2[0], s));
        }
        if (array2.length > 1) {
            if (!(array2[1] instanceof AndTerm) && !(array2[1] instanceof FlagTerm)) {
                argument.append(generateSequence(array2[1], s));
                return argument;
            }
            argument.writeArgument(generateSequence(array2[1], s));
        }
        return argument;
    }
    
    private static Argument receiveddate(final DateTerm dateTerm) throws SearchException {
        final Argument argument = new Argument();
        final String imapDate = toIMAPDate(dateTerm.getDate());
        switch (dateTerm.getComparison()) {
            default: {
                throw new SearchException("Cannot handle Date Comparison");
            }
            case 5: {
                argument.writeAtom("SINCE " + imapDate);
                return argument;
            }
            case 3: {
                argument.writeAtom("ON " + imapDate);
                return argument;
            }
            case 2: {
                argument.writeAtom("BEFORE " + imapDate);
                return argument;
            }
            case 6: {
                argument.writeAtom("OR SINCE " + imapDate + " ON " + imapDate);
                return argument;
            }
            case 1: {
                argument.writeAtom("OR BEFORE " + imapDate + " ON " + imapDate);
                return argument;
            }
            case 4: {
                argument.writeAtom("NOT ON " + imapDate);
                return argument;
            }
        }
    }
    
    private static Argument recipient(final Message.RecipientType recipientType, final String s, final String s2) throws SearchException, IOException {
        final Argument argument = new Argument();
        if (recipientType == Message.RecipientType.TO) {
            argument.writeAtom("TO");
        }
        else if (recipientType == Message.RecipientType.CC) {
            argument.writeAtom("CC");
        }
        else {
            if (recipientType != Message.RecipientType.BCC) {
                throw new SearchException("Illegal Recipient type");
            }
            argument.writeAtom("BCC");
        }
        argument.writeString(s, s2);
        return argument;
    }
    
    private static Argument sentdate(final DateTerm dateTerm) throws SearchException {
        final Argument argument = new Argument();
        final String imapDate = toIMAPDate(dateTerm.getDate());
        switch (dateTerm.getComparison()) {
            default: {
                throw new SearchException("Cannot handle Date Comparison");
            }
            case 5: {
                argument.writeAtom("SENTSINCE " + imapDate);
                return argument;
            }
            case 3: {
                argument.writeAtom("SENTON " + imapDate);
                return argument;
            }
            case 2: {
                argument.writeAtom("SENTBEFORE " + imapDate);
                return argument;
            }
            case 6: {
                argument.writeAtom("OR SENTSINCE " + imapDate + " SENTON " + imapDate);
                return argument;
            }
            case 1: {
                argument.writeAtom("OR SENTBEFORE " + imapDate + " SENTON " + imapDate);
                return argument;
            }
            case 4: {
                argument.writeAtom("NOT SENTON " + imapDate);
                return argument;
            }
        }
    }
    
    private static Argument size(final SizeTerm sizeTerm) throws SearchException {
        final Argument argument = new Argument();
        switch (sizeTerm.getComparison()) {
            default: {
                throw new SearchException("Cannot handle Comparison");
            }
            case 5: {
                argument.writeAtom("LARGER");
                break;
            }
            case 2: {
                argument.writeAtom("SMALLER");
                break;
            }
        }
        argument.writeNumber(sizeTerm.getNumber());
        return argument;
    }
    
    private static Argument subject(final SubjectTerm subjectTerm, final String s) throws SearchException, IOException {
        final Argument argument = new Argument();
        argument.writeAtom("SUBJECT");
        argument.writeString(subjectTerm.getPattern(), s);
        return argument;
    }
    
    private static String toIMAPDate(final Date time) {
        final StringBuffer sb = new StringBuffer();
        SearchSequence.cal.setTime(time);
        sb.append(SearchSequence.cal.get(5)).append("-");
        sb.append(SearchSequence.monthTable[SearchSequence.cal.get(2)]).append('-');
        sb.append(SearchSequence.cal.get(1));
        return sb.toString();
    }
}
