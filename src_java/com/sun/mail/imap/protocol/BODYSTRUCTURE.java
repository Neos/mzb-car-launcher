package com.sun.mail.imap.protocol;

import javax.mail.internet.*;
import java.util.*;
import com.sun.mail.iap.*;

public class BODYSTRUCTURE implements Item
{
    private static int MULTI;
    private static int NESTED;
    private static int SINGLE;
    static final char[] name;
    private static boolean parseDebug;
    public String attachment;
    public BODYSTRUCTURE[] bodies;
    public ParameterList cParams;
    public ParameterList dParams;
    public String description;
    public String disposition;
    public String encoding;
    public ENVELOPE envelope;
    public String id;
    public String[] language;
    public int lines;
    public String md5;
    public int msgno;
    private int processedType;
    public int size;
    public String subtype;
    public String type;
    
    static {
        boolean parseDebug = true;
        name = new char[] { 'B', 'O', 'D', 'Y', 'S', 'T', 'R', 'U', 'C', 'T', 'U', 'R', 'E' };
        BODYSTRUCTURE.SINGLE = 1;
        BODYSTRUCTURE.MULTI = 2;
        BODYSTRUCTURE.NESTED = 3;
        BODYSTRUCTURE.parseDebug = false;
        try {
            final String property = System.getProperty("mail.imap.parse.debug");
            if (property == null || !property.equalsIgnoreCase("true")) {
                parseDebug = false;
            }
            BODYSTRUCTURE.parseDebug = parseDebug;
        }
        catch (SecurityException ex) {}
    }
    
    public BODYSTRUCTURE(final FetchResponse fetchResponse) throws ParsingException {
        this.lines = -1;
        this.size = -1;
        if (BODYSTRUCTURE.parseDebug) {
            System.out.println("DEBUG IMAP: parsing BODYSTRUCTURE");
        }
        this.msgno = fetchResponse.getNumber();
        if (BODYSTRUCTURE.parseDebug) {
            System.out.println("DEBUG IMAP: msgno " + this.msgno);
        }
        fetchResponse.skipSpaces();
        if (fetchResponse.readByte() != 40) {
            throw new ParsingException("BODYSTRUCTURE parse error: missing ``('' at start");
        }
        if (fetchResponse.peekByte() == 40) {
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: parsing multipart");
            }
            this.type = "multipart";
            this.processedType = BODYSTRUCTURE.MULTI;
            final Vector<BODYSTRUCTURE> vector = new Vector<BODYSTRUCTURE>(1);
            do {
                vector.addElement(new BODYSTRUCTURE(fetchResponse));
                fetchResponse.skipSpaces();
            } while (fetchResponse.peekByte() == 40);
            vector.copyInto(this.bodies = new BODYSTRUCTURE[vector.size()]);
            this.subtype = fetchResponse.readString();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: subtype " + this.subtype);
            }
            if (fetchResponse.readByte() == 41) {
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: parse DONE");
                }
            }
            else {
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: parsing extension data");
                }
                this.cParams = this.parseParameters(fetchResponse);
                if (fetchResponse.readByte() == 41) {
                    if (BODYSTRUCTURE.parseDebug) {
                        System.out.println("DEBUG IMAP: body parameters DONE");
                    }
                }
                else {
                    final byte byte1 = fetchResponse.readByte();
                    if (byte1 == 40) {
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: parse disposition");
                        }
                        this.disposition = fetchResponse.readString();
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: disposition " + this.disposition);
                        }
                        this.dParams = this.parseParameters(fetchResponse);
                        if (fetchResponse.readByte() != 41) {
                            throw new ParsingException("BODYSTRUCTURE parse error: missing ``)'' at end of disposition in multipart");
                        }
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: disposition DONE");
                        }
                    }
                    else {
                        if (byte1 != 78 && byte1 != 110) {
                            throw new ParsingException("BODYSTRUCTURE parse error: " + this.type + "/" + this.subtype + ": " + "bad multipart disposition, b " + byte1);
                        }
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: disposition NIL");
                        }
                        fetchResponse.skip(2);
                    }
                    final byte byte2 = fetchResponse.readByte();
                    if (byte2 == 41) {
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: no body-fld-lang");
                        }
                    }
                    else {
                        if (byte2 != 32) {
                            throw new ParsingException("BODYSTRUCTURE parse error: missing space after disposition");
                        }
                        if (fetchResponse.peekByte() == 40) {
                            this.language = fetchResponse.readStringList();
                            if (BODYSTRUCTURE.parseDebug) {
                                System.out.println("DEBUG IMAP: language len " + this.language.length);
                            }
                        }
                        else {
                            final String string = fetchResponse.readString();
                            if (string != null) {
                                this.language = new String[] { string };
                                if (BODYSTRUCTURE.parseDebug) {
                                    System.out.println("DEBUG IMAP: language " + string);
                                }
                            }
                        }
                        while (fetchResponse.readByte() == 32) {
                            this.parseBodyExtension(fetchResponse);
                        }
                    }
                }
            }
        }
        else {
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: single part");
            }
            this.type = fetchResponse.readString();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: type " + this.type);
            }
            this.processedType = BODYSTRUCTURE.SINGLE;
            this.subtype = fetchResponse.readString();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: subtype " + this.subtype);
            }
            if (this.type == null) {
                this.type = "application";
                this.subtype = "octet-stream";
            }
            this.cParams = this.parseParameters(fetchResponse);
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: cParams " + this.cParams);
            }
            this.id = fetchResponse.readString();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: id " + this.id);
            }
            this.description = fetchResponse.readString();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: description " + this.description);
            }
            this.encoding = fetchResponse.readString();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: encoding " + this.encoding);
            }
            this.size = fetchResponse.readNumber();
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: size " + this.size);
            }
            if (this.size < 0) {
                throw new ParsingException("BODYSTRUCTURE parse error: bad ``size'' element");
            }
            if (this.type.equalsIgnoreCase("text")) {
                this.lines = fetchResponse.readNumber();
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: lines " + this.lines);
                }
                if (this.lines < 0) {
                    throw new ParsingException("BODYSTRUCTURE parse error: bad ``lines'' element");
                }
            }
            else if (this.type.equalsIgnoreCase("message") && this.subtype.equalsIgnoreCase("rfc822")) {
                this.processedType = BODYSTRUCTURE.NESTED;
                this.envelope = new ENVELOPE(fetchResponse);
                this.bodies = new BODYSTRUCTURE[] { new BODYSTRUCTURE(fetchResponse) };
                this.lines = fetchResponse.readNumber();
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: lines " + this.lines);
                }
                if (this.lines < 0) {
                    throw new ParsingException("BODYSTRUCTURE parse error: bad ``lines'' element");
                }
            }
            else {
                fetchResponse.skipSpaces();
                if (Character.isDigit((char)fetchResponse.peekByte())) {
                    throw new ParsingException("BODYSTRUCTURE parse error: server erroneously included ``lines'' element with type " + this.type + "/" + this.subtype);
                }
            }
            if (fetchResponse.peekByte() == 41) {
                fetchResponse.readByte();
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: parse DONE");
                }
            }
            else {
                this.md5 = fetchResponse.readString();
                if (fetchResponse.readByte() == 41) {
                    if (BODYSTRUCTURE.parseDebug) {
                        System.out.println("DEBUG IMAP: no MD5 DONE");
                    }
                }
                else {
                    final byte byte3 = fetchResponse.readByte();
                    if (byte3 == 40) {
                        this.disposition = fetchResponse.readString();
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: disposition " + this.disposition);
                        }
                        this.dParams = this.parseParameters(fetchResponse);
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: dParams " + this.dParams);
                        }
                        if (fetchResponse.readByte() != 41) {
                            throw new ParsingException("BODYSTRUCTURE parse error: missing ``)'' at end of disposition");
                        }
                    }
                    else {
                        if (byte3 != 78 && byte3 != 110) {
                            throw new ParsingException("BODYSTRUCTURE parse error: " + this.type + "/" + this.subtype + ": " + "bad single part disposition, b " + byte3);
                        }
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: disposition NIL");
                        }
                        fetchResponse.skip(2);
                    }
                    if (fetchResponse.readByte() == 41) {
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: disposition DONE");
                        }
                    }
                    else {
                        if (fetchResponse.peekByte() == 40) {
                            this.language = fetchResponse.readStringList();
                            if (BODYSTRUCTURE.parseDebug) {
                                System.out.println("DEBUG IMAP: language len " + this.language.length);
                            }
                        }
                        else {
                            final String string2 = fetchResponse.readString();
                            if (string2 != null) {
                                this.language = new String[] { string2 };
                                if (BODYSTRUCTURE.parseDebug) {
                                    System.out.println("DEBUG IMAP: language " + string2);
                                }
                            }
                        }
                        while (fetchResponse.readByte() == 32) {
                            this.parseBodyExtension(fetchResponse);
                        }
                        if (BODYSTRUCTURE.parseDebug) {
                            System.out.println("DEBUG IMAP: all DONE");
                        }
                    }
                }
            }
        }
    }
    
    private void parseBodyExtension(final Response response) throws ParsingException {
        response.skipSpaces();
        final byte peekByte = response.peekByte();
        if (peekByte == 40) {
            response.skip(1);
            do {
                this.parseBodyExtension(response);
            } while (response.readByte() != 41);
            return;
        }
        if (Character.isDigit((char)peekByte)) {
            response.readNumber();
            return;
        }
        response.readString();
    }
    
    private ParameterList parseParameters(final Response response) throws ParsingException {
        response.skipSpaces();
        final byte byte1 = response.readByte();
        if (byte1 == 40) {
            final ParameterList list = new ParameterList();
            do {
                final String string = response.readString();
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: parameter name " + string);
                }
                if (string == null) {
                    throw new ParsingException("BODYSTRUCTURE parse error: " + this.type + "/" + this.subtype + ": " + "null name in parameter list");
                }
                final String string2 = response.readString();
                if (BODYSTRUCTURE.parseDebug) {
                    System.out.println("DEBUG IMAP: parameter value " + string2);
                }
                list.set(string, string2);
            } while (response.readByte() != 41);
            list.set(null, "DONE");
            return list;
        }
        if (byte1 == 78 || byte1 == 110) {
            if (BODYSTRUCTURE.parseDebug) {
                System.out.println("DEBUG IMAP: parameter list NIL");
            }
            response.skip(2);
            return null;
        }
        throw new ParsingException("Parameter list parse error");
    }
    
    public boolean isMulti() {
        return this.processedType == BODYSTRUCTURE.MULTI;
    }
    
    public boolean isNested() {
        return this.processedType == BODYSTRUCTURE.NESTED;
    }
    
    public boolean isSingle() {
        return this.processedType == BODYSTRUCTURE.SINGLE;
    }
}
