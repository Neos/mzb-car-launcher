package com.sun.mail.imap.protocol;

import java.util.*;

public class UIDSet
{
    public long end;
    public long start;
    
    public UIDSet() {
    }
    
    public UIDSet(final long start, final long end) {
        this.start = start;
        this.end = end;
    }
    
    public static UIDSet[] createUIDSets(final long[] array) {
        final Vector<UIDSet> vector = new Vector<UIDSet>();
        int n;
        for (int i = 0; i < array.length; i = n - 1 + 1) {
            final UIDSet set = new UIDSet();
            set.start = array[i];
            for (n = i + 1; n < array.length && array[n] == array[n - 1] + 1L; ++n) {}
            set.end = array[n - 1];
            vector.addElement(set);
        }
        final UIDSet[] array2 = new UIDSet[vector.size()];
        vector.copyInto(array2);
        return array2;
    }
    
    public static long size(final UIDSet[] array) {
        long n = 0L;
        if (array == null) {
            return 0L;
        }
        for (int i = 0; i < array.length; ++i) {
            n += array[i].size();
        }
        return n;
    }
    
    public static String toString(final UIDSet[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        int n = 0;
        final StringBuffer sb = new StringBuffer();
        final int length = array.length;
        while (true) {
            final long start = array[n].start;
            final long end = array[n].end;
            if (end > start) {
                sb.append(start).append(':').append(end);
            }
            else {
                sb.append(start);
            }
            ++n;
            if (n >= length) {
                break;
            }
            sb.append(',');
        }
        return sb.toString();
    }
    
    public long size() {
        return this.end - this.start + 1L;
    }
}
