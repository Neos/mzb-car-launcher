package com.sun.mail.imap.protocol;

import java.util.*;
import com.sun.mail.iap.*;

public class ListInfo
{
    public static final int CHANGED = 1;
    public static final int INDETERMINATE = 3;
    public static final int UNCHANGED = 2;
    public String[] attrs;
    public boolean canOpen;
    public int changeState;
    public boolean hasInferiors;
    public String name;
    public char separator;
    
    public ListInfo(final IMAPResponse imapResponse) throws ParsingException {
        this.name = null;
        this.separator = '/';
        this.hasInferiors = true;
        this.canOpen = true;
        this.changeState = 3;
        final String[] simpleList = imapResponse.readSimpleList();
        final Vector<String> vector = new Vector<String>();
        if (simpleList != null) {
            for (int i = 0; i < simpleList.length; ++i) {
                if (simpleList[i].equalsIgnoreCase("\\Marked")) {
                    this.changeState = 1;
                }
                else if (simpleList[i].equalsIgnoreCase("\\Unmarked")) {
                    this.changeState = 2;
                }
                else if (simpleList[i].equalsIgnoreCase("\\Noselect")) {
                    this.canOpen = false;
                }
                else if (simpleList[i].equalsIgnoreCase("\\Noinferiors")) {
                    this.hasInferiors = false;
                }
                vector.addElement(simpleList[i]);
            }
        }
        vector.copyInto(this.attrs = new String[vector.size()]);
        imapResponse.skipSpaces();
        if (imapResponse.readByte() == 34) {
            if ((this.separator = (char)imapResponse.readByte()) == '\\') {
                this.separator = (char)imapResponse.readByte();
            }
            imapResponse.skip(1);
        }
        else {
            imapResponse.skip(2);
        }
        imapResponse.skipSpaces();
        this.name = imapResponse.readAtomString();
        this.name = BASE64MailboxDecoder.decode(this.name);
    }
}
