package com.sun.mail.imap.protocol;

import javax.mail.internet.*;
import com.sun.mail.iap.*;
import java.util.*;

public class ENVELOPE implements Item
{
    private static MailDateFormat mailDateFormat;
    static final char[] name;
    public InternetAddress[] bcc;
    public InternetAddress[] cc;
    public Date date;
    public InternetAddress[] from;
    public String inReplyTo;
    public String messageId;
    public int msgno;
    public InternetAddress[] replyTo;
    public InternetAddress[] sender;
    public String subject;
    public InternetAddress[] to;
    
    static {
        name = new char[] { 'E', 'N', 'V', 'E', 'L', 'O', 'P', 'E' };
        ENVELOPE.mailDateFormat = new MailDateFormat();
    }
    
    public ENVELOPE(final FetchResponse fetchResponse) throws ParsingException {
        this.date = null;
        this.msgno = fetchResponse.getNumber();
        fetchResponse.skipSpaces();
        if (fetchResponse.readByte() != 40) {
            throw new ParsingException("ENVELOPE parse error");
        }
        final String string = fetchResponse.readString();
        while (true) {
            if (string == null) {
                break Label_0060;
            }
            try {
                this.date = ENVELOPE.mailDateFormat.parse(string);
                this.subject = fetchResponse.readString();
                this.from = this.parseAddressList(fetchResponse);
                this.sender = this.parseAddressList(fetchResponse);
                this.replyTo = this.parseAddressList(fetchResponse);
                this.to = this.parseAddressList(fetchResponse);
                this.cc = this.parseAddressList(fetchResponse);
                this.bcc = this.parseAddressList(fetchResponse);
                this.inReplyTo = fetchResponse.readString();
                this.messageId = fetchResponse.readString();
                if (fetchResponse.readByte() != 41) {
                    throw new ParsingException("ENVELOPE parse error");
                }
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
    
    private InternetAddress[] parseAddressList(final Response response) throws ParsingException {
        response.skipSpaces();
        final byte byte1 = response.readByte();
        if (byte1 == 40) {
            final Vector<IMAPAddress> vector = new Vector<IMAPAddress>();
            do {
                final IMAPAddress imapAddress = new IMAPAddress(response);
                if (!imapAddress.isEndOfGroup()) {
                    vector.addElement(imapAddress);
                }
            } while (response.peekByte() != 41);
            response.skip(1);
            final InternetAddress[] array = new InternetAddress[vector.size()];
            vector.copyInto(array);
            return array;
        }
        if (byte1 == 78 || byte1 == 110) {
            response.skip(2);
            return null;
        }
        throw new ParsingException("ADDRESS parse error");
    }
}
