package com.sun.mail.imap.protocol;

import java.io.*;

public class BASE64MailboxEncoder
{
    private static final char[] pem_array;
    protected byte[] buffer;
    protected int bufsize;
    protected Writer out;
    protected boolean started;
    
    static {
        pem_array = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', ',' };
    }
    
    public BASE64MailboxEncoder(final Writer out) {
        this.buffer = new byte[4];
        this.bufsize = 0;
        this.started = false;
        this.out = null;
        this.out = out;
    }
    
    public static String encode(String string) {
        BASE64MailboxEncoder base64MailboxEncoder = null;
        final char[] charArray = string.toCharArray();
        final int length = charArray.length;
        int n = 0;
        final CharArrayWriter charArrayWriter = new CharArrayWriter(length);
        for (int i = 0; i < length; ++i) {
            final char c = charArray[i];
            if (c >= ' ' && c <= '~') {
                if (base64MailboxEncoder != null) {
                    base64MailboxEncoder.flush();
                }
                if (c == '&') {
                    n = 1;
                    charArrayWriter.write(38);
                    charArrayWriter.write(45);
                }
                else {
                    charArrayWriter.write(c);
                }
            }
            else {
                BASE64MailboxEncoder base64MailboxEncoder2;
                if ((base64MailboxEncoder2 = base64MailboxEncoder) == null) {
                    base64MailboxEncoder2 = new BASE64MailboxEncoder(charArrayWriter);
                    n = 1;
                }
                base64MailboxEncoder2.write(c);
                base64MailboxEncoder = base64MailboxEncoder2;
            }
        }
        if (base64MailboxEncoder != null) {
            base64MailboxEncoder.flush();
        }
        if (n != 0) {
            string = charArrayWriter.toString();
        }
        return string;
    }
    
    protected void encode() throws IOException {
        if (this.bufsize == 1) {
            final byte b = this.buffer[0];
            this.out.write(BASE64MailboxEncoder.pem_array[b >>> 2 & 0x3F]);
            this.out.write(BASE64MailboxEncoder.pem_array[(b << 4 & 0x30) + 0]);
        }
        else {
            if (this.bufsize == 2) {
                final byte b2 = this.buffer[0];
                final byte b3 = this.buffer[1];
                this.out.write(BASE64MailboxEncoder.pem_array[b2 >>> 2 & 0x3F]);
                this.out.write(BASE64MailboxEncoder.pem_array[(b2 << 4 & 0x30) + (b3 >>> 4 & 0xF)]);
                this.out.write(BASE64MailboxEncoder.pem_array[(b3 << 2 & 0x3C) + 0]);
                return;
            }
            final byte b4 = this.buffer[0];
            final byte b5 = this.buffer[1];
            final byte b6 = this.buffer[2];
            this.out.write(BASE64MailboxEncoder.pem_array[b4 >>> 2 & 0x3F]);
            this.out.write(BASE64MailboxEncoder.pem_array[(b4 << 4 & 0x30) + (b5 >>> 4 & 0xF)]);
            this.out.write(BASE64MailboxEncoder.pem_array[(b5 << 2 & 0x3C) + (b6 >>> 6 & 0x3)]);
            this.out.write(BASE64MailboxEncoder.pem_array[b6 & 0x3F]);
            if (this.bufsize == 4) {
                this.buffer[0] = this.buffer[3];
            }
        }
    }
    
    public void flush() {
        try {
            if (this.bufsize > 0) {
                this.encode();
                this.bufsize = 0;
            }
            if (this.started) {
                this.out.write(45);
                this.started = false;
            }
        }
        catch (IOException ex) {}
    }
    
    public void write(final int n) {
        try {
            if (!this.started) {
                this.started = true;
                this.out.write(38);
            }
            this.buffer[this.bufsize++] = (byte)(n >> 8);
            this.buffer[this.bufsize++] = (byte)(n & 0xFF);
            if (this.bufsize >= 3) {
                this.encode();
                this.bufsize -= 3;
            }
        }
        catch (IOException ex) {}
    }
}
