package com.sun.mail.imap.protocol;

import javax.mail.internet.*;
import java.util.*;
import com.sun.mail.iap.*;
import java.text.*;

public class INTERNALDATE implements Item
{
    private static SimpleDateFormat df;
    private static MailDateFormat mailDateFormat;
    static final char[] name;
    protected Date date;
    public int msgno;
    
    static {
        name = new char[] { 'I', 'N', 'T', 'E', 'R', 'N', 'A', 'L', 'D', 'A', 'T', 'E' };
        INTERNALDATE.mailDateFormat = new MailDateFormat();
        INTERNALDATE.df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss ", Locale.US);
    }
    
    public INTERNALDATE(final FetchResponse fetchResponse) throws ParsingException {
        this.msgno = fetchResponse.getNumber();
        fetchResponse.skipSpaces();
        final String string = fetchResponse.readString();
        if (string == null) {
            throw new ParsingException("INTERNALDATE is NIL");
        }
        try {
            this.date = INTERNALDATE.mailDateFormat.parse(string);
        }
        catch (ParseException ex) {
            throw new ParsingException("INTERNALDATE parse error");
        }
    }
    
    public static String format(final Date date) {
        while (true) {
            final StringBuffer sb = new StringBuffer();
            while (true) {
                synchronized (INTERNALDATE.df) {
                    INTERNALDATE.df.format(date, sb, new FieldPosition(0));
                    // monitorexit(INTERNALDATE.df)
                    int n = -date.getTimezoneOffset();
                    if (n < 0) {
                        sb.append('-');
                        n = -n;
                        final int n2 = n / 60;
                        n %= 60;
                        sb.append(Character.forDigit(n2 / 10, 10));
                        sb.append(Character.forDigit(n2 % 10, 10));
                        sb.append(Character.forDigit(n / 10, 10));
                        sb.append(Character.forDigit(n % 10, 10));
                        return sb.toString();
                    }
                }
                sb.append('+');
                continue;
            }
        }
    }
    
    public Date getDate() {
        return this.date;
    }
}
