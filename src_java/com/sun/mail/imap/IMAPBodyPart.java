package com.sun.mail.imap;

import com.sun.mail.iap.*;
import java.util.*;
import com.sun.mail.imap.protocol.*;
import javax.activation.*;
import javax.mail.internet.*;
import java.io.*;
import javax.mail.*;

public class IMAPBodyPart extends MimeBodyPart
{
    private BODYSTRUCTURE bs;
    private String description;
    private boolean headersLoaded;
    private IMAPMessage message;
    private String sectionId;
    private String type;
    
    protected IMAPBodyPart(final BODYSTRUCTURE bs, final String sectionId, final IMAPMessage message) {
        this.headersLoaded = false;
        this.bs = bs;
        this.sectionId = sectionId;
        this.message = message;
        this.type = new ContentType(bs.type, bs.subtype, bs.cParams).toString();
    }
    
    private void loadHeaders() throws MessagingException {
        while (true) {
            // monitorenter(this)
            // monitorexit(t)
            while (true) {
                Label_0193: {
                    try {
                        if (this.headersLoaded) {
                            return;
                        }
                        if (this.headers == null) {
                            this.headers = new InternetHeaders();
                        }
                        final Object messageCacheLock = this.message.getMessageCacheLock();
                        // monitorenter(messageCacheLock)
                        try {
                            final IMAPProtocol protocol = this.message.getProtocol();
                            this.message.checkExpunged();
                            if (!protocol.isREV1()) {
                                break Label_0193;
                            }
                            if (protocol.peekBody(this.message.getSequenceNumber(), String.valueOf(this.sectionId) + ".MIME") == null) {
                                throw new MessagingException("Failed to fetch headers");
                            }
                            goto Label_0142;
                        }
                        catch (ConnectionException ex) {
                            try {
                                throw new FolderClosedException(this.message.getFolder(), ex.getMessage());
                            }
                            finally {
                            }
                            // monitorexit(messageCacheLock)
                        }
                        catch (ProtocolException ex2) {
                            throw new MessagingException(ex2.getMessage(), ex2);
                        }
                    }
                    finally {}
                    final ConnectionException ex;
                    this.headers.load((InputStream)ex);
                    this.headersLoaded = true;
                    return;
                }
                this.headers.addHeader("Content-Type", this.type);
                this.headers.addHeader("Content-Transfer-Encoding", this.bs.encoding);
                if (this.bs.description != null) {
                    this.headers.addHeader("Content-Description", this.bs.description);
                }
                if (this.bs.id != null) {
                    this.headers.addHeader("Content-ID", this.bs.id);
                }
                if (this.bs.md5 != null) {
                    this.headers.addHeader("Content-MD5", this.bs.md5);
                }
                continue;
            }
        }
    }
    
    @Override
    public void addHeader(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void addHeaderLine(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public Enumeration getAllHeaderLines() throws MessagingException {
        this.loadHeaders();
        return super.getAllHeaderLines();
    }
    
    @Override
    public Enumeration getAllHeaders() throws MessagingException {
        this.loadHeaders();
        return super.getAllHeaders();
    }
    
    @Override
    public String getContentID() throws MessagingException {
        return this.bs.id;
    }
    
    @Override
    public String getContentMD5() throws MessagingException {
        return this.bs.md5;
    }
    
    @Override
    protected InputStream getContentStream() throws MessagingException {
        InputStream byteArrayInputStream = null;
        final boolean peek = this.message.getPeek();
        final Object messageCacheLock = this.message.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        try {
            final IMAPProtocol protocol = this.message.getProtocol();
            this.message.checkExpunged();
            if (protocol.isREV1() && this.message.getFetchBlockSize() != -1) {
                return new IMAPInputStream(this.message, this.sectionId, this.bs.size, peek);
            }
            final int sequenceNumber = this.message.getSequenceNumber();
            BODY body;
            if (peek) {
                body = protocol.peekBody(sequenceNumber, this.sectionId);
            }
            else {
                body = protocol.fetchBody(sequenceNumber, this.sectionId);
            }
            if (body != null) {
                byteArrayInputStream = body.getByteArrayInputStream();
            }
            // monitorexit(messageCacheLock)
            if (byteArrayInputStream == null) {
                throw new MessagingException("No content");
            }
        }
        catch (ConnectionException ex) {
            throw new FolderClosedException(this.message.getFolder(), ex.getMessage());
            try {}
            finally {
            }
            // monitorexit(messageCacheLock)
        }
        catch (ProtocolException ex2) {
            throw new MessagingException(ex2.getMessage(), ex2);
        }
        return byteArrayInputStream;
    }
    
    @Override
    public String getContentType() throws MessagingException {
        return this.type;
    }
    
    @Override
    public DataHandler getDataHandler() throws MessagingException {
        synchronized (this) {
            if (this.dh == null) {
                if (this.bs.isMulti()) {
                    this.dh = new DataHandler(new IMAPMultipartDataSource(this, this.bs.bodies, this.sectionId, this.message));
                }
                else if (this.bs.isNested() && this.message.isREV1()) {
                    this.dh = new DataHandler(new IMAPNestedMessage(this.message, this.bs.bodies[0], this.bs.envelope, this.sectionId), this.type);
                }
            }
            return super.getDataHandler();
        }
    }
    
    @Override
    public String getDescription() throws MessagingException {
        if (this.description != null) {
            return this.description;
        }
        if (this.bs.description == null) {
            return null;
        }
        try {
            this.description = MimeUtility.decodeText(this.bs.description);
            return this.description;
        }
        catch (UnsupportedEncodingException ex) {
            this.description = this.bs.description;
            return this.description;
        }
    }
    
    @Override
    public String getDisposition() throws MessagingException {
        return this.bs.disposition;
    }
    
    @Override
    public String getEncoding() throws MessagingException {
        return this.bs.encoding;
    }
    
    @Override
    public String getFileName() throws MessagingException {
        String value = null;
        if (this.bs.dParams != null) {
            value = this.bs.dParams.get("filename");
        }
        String value2;
        if ((value2 = value) == null) {
            value2 = value;
            if (this.bs.cParams != null) {
                value2 = this.bs.cParams.get("name");
            }
        }
        return value2;
    }
    
    @Override
    public String[] getHeader(final String s) throws MessagingException {
        this.loadHeaders();
        return super.getHeader(s);
    }
    
    @Override
    public int getLineCount() throws MessagingException {
        return this.bs.lines;
    }
    
    @Override
    public Enumeration getMatchingHeaderLines(final String[] array) throws MessagingException {
        this.loadHeaders();
        return super.getMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getMatchingHeaders(final String[] array) throws MessagingException {
        this.loadHeaders();
        return super.getMatchingHeaders(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaderLines(final String[] array) throws MessagingException {
        this.loadHeaders();
        return super.getNonMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaders(final String[] array) throws MessagingException {
        this.loadHeaders();
        return super.getNonMatchingHeaders(array);
    }
    
    @Override
    public int getSize() throws MessagingException {
        return this.bs.size;
    }
    
    @Override
    public void removeHeader(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setContent(final Object o, final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setContent(final Multipart multipart) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setContentMD5(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setDataHandler(final DataHandler dataHandler) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setDescription(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setDisposition(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setFileName(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    public void setHeader(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPBodyPart is read-only");
    }
    
    @Override
    protected void updateHeaders() {
    }
}
