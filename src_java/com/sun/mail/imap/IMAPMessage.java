package com.sun.mail.imap;

import com.sun.mail.iap.*;
import java.util.*;
import com.sun.mail.imap.protocol.*;
import javax.activation.*;
import javax.mail.internet.*;
import java.io.*;
import javax.mail.*;

public class IMAPMessage extends MimeMessage
{
    private static String EnvelopeCmd;
    protected BODYSTRUCTURE bs;
    private String description;
    protected ENVELOPE envelope;
    private boolean headersLoaded;
    private Hashtable loadedHeaders;
    private boolean peek;
    private Date receivedDate;
    protected String sectionId;
    private int seqnum;
    private int size;
    private String subject;
    private String type;
    private long uid;
    
    static {
        IMAPMessage.EnvelopeCmd = "ENVELOPE INTERNALDATE RFC822.SIZE";
    }
    
    protected IMAPMessage(final IMAPFolder imapFolder, final int n, final int seqnum) {
        super(imapFolder, n);
        this.size = -1;
        this.uid = -1L;
        this.headersLoaded = false;
        this.seqnum = seqnum;
        this.flags = null;
    }
    
    protected IMAPMessage(final Session session) {
        super(session);
        this.size = -1;
        this.uid = -1L;
        this.headersLoaded = false;
    }
    
    private BODYSTRUCTURE _getBodyStructure() {
        return this.bs;
    }
    
    private ENVELOPE _getEnvelope() {
        return this.envelope;
    }
    
    private Flags _getFlags() {
        return this.flags;
    }
    
    private InternetAddress[] aaclone(final InternetAddress[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    private boolean areHeadersLoaded() {
        synchronized (this) {
            return this.headersLoaded;
        }
    }
    
    private static String craftHeaderCmd(final IMAPProtocol imapProtocol, final String[] array) {
        StringBuffer sb;
        if (imapProtocol.isREV1()) {
            sb = new StringBuffer("BODY.PEEK[HEADER.FIELDS (");
        }
        else {
            sb = new StringBuffer("RFC822.HEADER.LINES (");
        }
        for (int i = 0; i < array.length; ++i) {
            if (i > 0) {
                sb.append(" ");
            }
            sb.append(array[i]);
        }
        if (imapProtocol.isREV1()) {
            sb.append(")]");
        }
        else {
            sb.append(")");
        }
        return sb.toString();
    }
    
    static void fetch(final IMAPFolder p0, final Message[] p1, final FetchProfile p2) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/StringBuffer.<init>:()V
        //     7: astore          12
        //     9: iconst_1       
        //    10: istore          4
        //    12: iconst_0       
        //    13: istore          5
        //    15: aload_2        
        //    16: getstatic       javax/mail/FetchProfile$Item.ENVELOPE:Ljavax/mail/FetchProfile$Item;
        //    19: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //    22: ifeq            37
        //    25: aload           12
        //    27: getstatic       com/sun/mail/imap/IMAPMessage.EnvelopeCmd:Ljava/lang/String;
        //    30: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    33: pop            
        //    34: iconst_0       
        //    35: istore          4
        //    37: iload           4
        //    39: istore_3       
        //    40: aload_2        
        //    41: getstatic       javax/mail/FetchProfile$Item.FLAGS:Ljavax/mail/FetchProfile$Item;
        //    44: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //    47: ifeq            69
        //    50: iload           4
        //    52: ifeq            304
        //    55: ldc             "FLAGS"
        //    57: astore          10
        //    59: aload           12
        //    61: aload           10
        //    63: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    66: pop            
        //    67: iconst_0       
        //    68: istore_3       
        //    69: iload_3        
        //    70: istore          4
        //    72: aload_2        
        //    73: getstatic       javax/mail/FetchProfile$Item.CONTENT_INFO:Ljavax/mail/FetchProfile$Item;
        //    76: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //    79: ifeq            101
        //    82: iload_3        
        //    83: ifeq            311
        //    86: ldc             "BODYSTRUCTURE"
        //    88: astore          10
        //    90: aload           12
        //    92: aload           10
        //    94: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    97: pop            
        //    98: iconst_0       
        //    99: istore          4
        //   101: iload           4
        //   103: istore_3       
        //   104: aload_2        
        //   105: getstatic       javax/mail/UIDFolder$FetchProfileItem.UID:Ljavax/mail/UIDFolder$FetchProfileItem;
        //   108: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //   111: ifeq            133
        //   114: iload           4
        //   116: ifeq            318
        //   119: ldc             "UID"
        //   121: astore          10
        //   123: aload           12
        //   125: aload           10
        //   127: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   130: pop            
        //   131: iconst_0       
        //   132: istore_3       
        //   133: iload_3        
        //   134: istore          4
        //   136: aload_2        
        //   137: getstatic       com/sun/mail/imap/IMAPFolder$FetchProfileItem.HEADERS:Lcom/sun/mail/imap/IMAPFolder$FetchProfileItem;
        //   140: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //   143: ifeq            178
        //   146: iconst_1       
        //   147: istore          5
        //   149: aload_0        
        //   150: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   153: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.isREV1:()Z
        //   156: ifeq            332
        //   159: iload_3        
        //   160: ifeq            325
        //   163: ldc             "BODY.PEEK[HEADER]"
        //   165: astore          10
        //   167: aload           12
        //   169: aload           10
        //   171: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   174: pop            
        //   175: iconst_0       
        //   176: istore          4
        //   178: iload           4
        //   180: istore_3       
        //   181: aload_2        
        //   182: getstatic       com/sun/mail/imap/IMAPFolder$FetchProfileItem.SIZE:Lcom/sun/mail/imap/IMAPFolder$FetchProfileItem;
        //   185: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //   188: ifeq            210
        //   191: iload           4
        //   193: ifeq            358
        //   196: ldc             "RFC822.SIZE"
        //   198: astore          10
        //   200: aload           12
        //   202: aload           10
        //   204: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   207: pop            
        //   208: iconst_0       
        //   209: istore_3       
        //   210: aconst_null    
        //   211: checkcast       [Ljava/lang/String;
        //   214: astore          10
        //   216: iload           5
        //   218: ifne            268
        //   221: aload_2        
        //   222: invokevirtual   javax/mail/FetchProfile.getHeaderNames:()[Ljava/lang/String;
        //   225: astore          11
        //   227: aload           11
        //   229: astore          10
        //   231: aload           11
        //   233: arraylength    
        //   234: ifle            268
        //   237: iload_3        
        //   238: ifne            249
        //   241: aload           12
        //   243: ldc             " "
        //   245: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   248: pop            
        //   249: aload           12
        //   251: aload_0        
        //   252: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   255: aload           11
        //   257: invokestatic    com/sun/mail/imap/IMAPMessage.craftHeaderCmd:(Lcom/sun/mail/imap/protocol/IMAPProtocol;[Ljava/lang/String;)Ljava/lang/String;
        //   260: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   263: pop            
        //   264: aload           11
        //   266: astore          10
        //   268: new             Lcom/sun/mail/imap/IMAPMessage$1FetchProfileCondition;
        //   271: dup            
        //   272: aload_2        
        //   273: invokespecial   com/sun/mail/imap/IMAPMessage$1FetchProfileCondition.<init>:(Ljavax/mail/FetchProfile;)V
        //   276: astore          11
        //   278: aload_0        
        //   279: getfield        com/sun/mail/imap/IMAPFolder.messageCacheLock:Ljava/lang/Object;
        //   282: astore          13
        //   284: aload           13
        //   286: monitorenter   
        //   287: aload_1        
        //   288: aload           11
        //   290: invokestatic    com/sun/mail/imap/Utility.toMessageSet:([Ljavax/mail/Message;Lcom/sun/mail/imap/Utility$Condition;)[Lcom/sun/mail/imap/protocol/MessageSet;
        //   293: astore          11
        //   295: aload           11
        //   297: ifnonnull       365
        //   300: aload           13
        //   302: monitorexit    
        //   303: return         
        //   304: ldc             " FLAGS"
        //   306: astore          10
        //   308: goto            59
        //   311: ldc             " BODYSTRUCTURE"
        //   313: astore          10
        //   315: goto            90
        //   318: ldc             " UID"
        //   320: astore          10
        //   322: goto            123
        //   325: ldc             " BODY.PEEK[HEADER]"
        //   327: astore          10
        //   329: goto            167
        //   332: iload_3        
        //   333: ifeq            351
        //   336: ldc             "RFC822.HEADER"
        //   338: astore          10
        //   340: aload           12
        //   342: aload           10
        //   344: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   347: pop            
        //   348: goto            175
        //   351: ldc             " RFC822.HEADER"
        //   353: astore          10
        //   355: goto            340
        //   358: ldc             " RFC822.SIZE"
        //   360: astore          10
        //   362: goto            200
        //   365: aconst_null    
        //   366: checkcast       [Lcom/sun/mail/iap/Response;
        //   369: astore_1       
        //   370: new             Ljava/util/Vector;
        //   373: dup            
        //   374: invokespecial   java/util/Vector.<init>:()V
        //   377: astore          14
        //   379: aload_0        
        //   380: getfield        com/sun/mail/imap/IMAPFolder.protocol:Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //   383: aload           11
        //   385: aload           12
        //   387: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   390: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.fetch:([Lcom/sun/mail/imap/protocol/MessageSet;Ljava/lang/String;)[Lcom/sun/mail/iap/Response;
        //   393: astore          11
        //   395: aload           11
        //   397: astore_1       
        //   398: aload_1        
        //   399: ifnonnull       984
        //   402: aload           13
        //   404: monitorexit    
        //   405: return         
        //   406: astore_0       
        //   407: aload           13
        //   409: monitorexit    
        //   410: aload_0        
        //   411: athrow         
        //   412: astore_1       
        //   413: new             Ljavax/mail/FolderClosedException;
        //   416: dup            
        //   417: aload_0        
        //   418: aload_1        
        //   419: invokevirtual   com/sun/mail/iap/ConnectionException.getMessage:()Ljava/lang/String;
        //   422: invokespecial   javax/mail/FolderClosedException.<init>:(Ljavax/mail/Folder;Ljava/lang/String;)V
        //   425: athrow         
        //   426: astore_0       
        //   427: new             Ljavax/mail/MessagingException;
        //   430: dup            
        //   431: aload_0        
        //   432: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   435: aload_0        
        //   436: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   439: athrow         
        //   440: iload_3        
        //   441: aload_1        
        //   442: arraylength    
        //   443: if_icmplt       989
        //   446: aload           14
        //   448: invokevirtual   java/util/Vector.size:()I
        //   451: istore_3       
        //   452: iload_3        
        //   453: ifeq            472
        //   456: iload_3        
        //   457: anewarray       Lcom/sun/mail/iap/Response;
        //   460: astore_1       
        //   461: aload           14
        //   463: aload_1        
        //   464: invokevirtual   java/util/Vector.copyInto:([Ljava/lang/Object;)V
        //   467: aload_0        
        //   468: aload_1        
        //   469: invokevirtual   com/sun/mail/imap/IMAPFolder.handleResponses:([Lcom/sun/mail/iap/Response;)V
        //   472: aload           13
        //   474: monitorexit    
        //   475: return         
        //   476: aload_1        
        //   477: iload_3        
        //   478: aaload         
        //   479: instanceof      Lcom/sun/mail/imap/protocol/FetchResponse;
        //   482: ifne            496
        //   485: aload           14
        //   487: aload_1        
        //   488: iload_3        
        //   489: aaload         
        //   490: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   493: goto            995
        //   496: aload_1        
        //   497: iload_3        
        //   498: aaload         
        //   499: checkcast       Lcom/sun/mail/imap/protocol/FetchResponse;
        //   502: astore          12
        //   504: aload_0        
        //   505: aload           12
        //   507: invokevirtual   com/sun/mail/imap/protocol/FetchResponse.getNumber:()I
        //   510: invokevirtual   com/sun/mail/imap/IMAPFolder.getMessageBySeqNumber:(I)Lcom/sun/mail/imap/IMAPMessage;
        //   513: astore          15
        //   515: aload           12
        //   517: invokevirtual   com/sun/mail/imap/protocol/FetchResponse.getItemCount:()I
        //   520: istore          9
        //   522: iconst_0       
        //   523: istore          6
        //   525: iconst_0       
        //   526: istore          4
        //   528: iload           4
        //   530: iload           9
        //   532: if_icmplt       550
        //   535: iload           6
        //   537: ifeq            995
        //   540: aload           14
        //   542: aload           12
        //   544: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   547: goto            995
        //   550: aload           12
        //   552: iload           4
        //   554: invokevirtual   com/sun/mail/imap/protocol/FetchResponse.getItem:(I)Lcom/sun/mail/imap/protocol/Item;
        //   557: astore          11
        //   559: aload           11
        //   561: instanceof      Ljavax/mail/Flags;
        //   564: ifeq            602
        //   567: aload_2        
        //   568: getstatic       javax/mail/FetchProfile$Item.FLAGS:Ljavax/mail/FetchProfile$Item;
        //   571: invokevirtual   javax/mail/FetchProfile.contains:(Ljavax/mail/FetchProfile$Item;)Z
        //   574: ifeq            1002
        //   577: aload           15
        //   579: ifnonnull       585
        //   582: goto            1002
        //   585: aload           15
        //   587: aload           11
        //   589: checkcast       Ljavax/mail/Flags;
        //   592: putfield        com/sun/mail/imap/IMAPMessage.flags:Ljavax/mail/Flags;
        //   595: iload           6
        //   597: istore          7
        //   599: goto            1005
        //   602: aload           11
        //   604: instanceof      Lcom/sun/mail/imap/protocol/ENVELOPE;
        //   607: ifeq            627
        //   610: aload           15
        //   612: aload           11
        //   614: checkcast       Lcom/sun/mail/imap/protocol/ENVELOPE;
        //   617: putfield        com/sun/mail/imap/IMAPMessage.envelope:Lcom/sun/mail/imap/protocol/ENVELOPE;
        //   620: iload           6
        //   622: istore          7
        //   624: goto            1005
        //   627: aload           11
        //   629: instanceof      Lcom/sun/mail/imap/protocol/INTERNALDATE;
        //   632: ifeq            655
        //   635: aload           15
        //   637: aload           11
        //   639: checkcast       Lcom/sun/mail/imap/protocol/INTERNALDATE;
        //   642: invokevirtual   com/sun/mail/imap/protocol/INTERNALDATE.getDate:()Ljava/util/Date;
        //   645: putfield        com/sun/mail/imap/IMAPMessage.receivedDate:Ljava/util/Date;
        //   648: iload           6
        //   650: istore          7
        //   652: goto            1005
        //   655: aload           11
        //   657: instanceof      Lcom/sun/mail/imap/protocol/RFC822SIZE;
        //   660: ifeq            683
        //   663: aload           15
        //   665: aload           11
        //   667: checkcast       Lcom/sun/mail/imap/protocol/RFC822SIZE;
        //   670: getfield        com/sun/mail/imap/protocol/RFC822SIZE.size:I
        //   673: putfield        com/sun/mail/imap/IMAPMessage.size:I
        //   676: iload           6
        //   678: istore          7
        //   680: goto            1005
        //   683: aload           11
        //   685: instanceof      Lcom/sun/mail/imap/protocol/BODYSTRUCTURE;
        //   688: ifeq            708
        //   691: aload           15
        //   693: aload           11
        //   695: checkcast       Lcom/sun/mail/imap/protocol/BODYSTRUCTURE;
        //   698: putfield        com/sun/mail/imap/IMAPMessage.bs:Lcom/sun/mail/imap/protocol/BODYSTRUCTURE;
        //   701: iload           6
        //   703: istore          7
        //   705: goto            1005
        //   708: aload           11
        //   710: instanceof      Lcom/sun/mail/imap/protocol/UID;
        //   713: ifeq            780
        //   716: aload           11
        //   718: checkcast       Lcom/sun/mail/imap/protocol/UID;
        //   721: astore          11
        //   723: aload           15
        //   725: aload           11
        //   727: getfield        com/sun/mail/imap/protocol/UID.uid:J
        //   730: putfield        com/sun/mail/imap/IMAPMessage.uid:J
        //   733: aload_0        
        //   734: getfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   737: ifnonnull       751
        //   740: aload_0        
        //   741: new             Ljava/util/Hashtable;
        //   744: dup            
        //   745: invokespecial   java/util/Hashtable.<init>:()V
        //   748: putfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   751: aload_0        
        //   752: getfield        com/sun/mail/imap/IMAPFolder.uidTable:Ljava/util/Hashtable;
        //   755: new             Ljava/lang/Long;
        //   758: dup            
        //   759: aload           11
        //   761: getfield        com/sun/mail/imap/protocol/UID.uid:J
        //   764: invokespecial   java/lang/Long.<init>:(J)V
        //   767: aload           15
        //   769: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   772: pop            
        //   773: iload           6
        //   775: istore          7
        //   777: goto            1005
        //   780: aload           11
        //   782: instanceof      Lcom/sun/mail/imap/protocol/RFC822DATA;
        //   785: ifne            800
        //   788: iload           6
        //   790: istore          7
        //   792: aload           11
        //   794: instanceof      Lcom/sun/mail/imap/protocol/BODY;
        //   797: ifeq            1005
        //   800: aload           11
        //   802: instanceof      Lcom/sun/mail/imap/protocol/RFC822DATA;
        //   805: ifeq            872
        //   808: aload           11
        //   810: checkcast       Lcom/sun/mail/imap/protocol/RFC822DATA;
        //   813: invokevirtual   com/sun/mail/imap/protocol/RFC822DATA.getByteArrayInputStream:()Ljava/io/ByteArrayInputStream;
        //   816: astore          11
        //   818: new             Ljavax/mail/internet/InternetHeaders;
        //   821: dup            
        //   822: invokespecial   javax/mail/internet/InternetHeaders.<init>:()V
        //   825: astore          16
        //   827: aload           16
        //   829: aload           11
        //   831: invokevirtual   javax/mail/internet/InternetHeaders.load:(Ljava/io/InputStream;)V
        //   834: aload           15
        //   836: getfield        com/sun/mail/imap/IMAPMessage.headers:Ljavax/mail/internet/InternetHeaders;
        //   839: ifnull          847
        //   842: iload           5
        //   844: ifeq            885
        //   847: aload           15
        //   849: aload           16
        //   851: putfield        com/sun/mail/imap/IMAPMessage.headers:Ljavax/mail/internet/InternetHeaders;
        //   854: iload           5
        //   856: ifeq            1018
        //   859: aload           15
        //   861: iconst_1       
        //   862: invokespecial   com/sun/mail/imap/IMAPMessage.setHeadersLoaded:(Z)V
        //   865: iload           6
        //   867: istore          7
        //   869: goto            1005
        //   872: aload           11
        //   874: checkcast       Lcom/sun/mail/imap/protocol/BODY;
        //   877: invokevirtual   com/sun/mail/imap/protocol/BODY.getByteArrayInputStream:()Ljava/io/ByteArrayInputStream;
        //   880: astore          11
        //   882: goto            818
        //   885: aload           16
        //   887: invokevirtual   javax/mail/internet/InternetHeaders.getAllHeaders:()Ljava/util/Enumeration;
        //   890: astore          11
        //   892: aload           11
        //   894: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //   899: ifeq            854
        //   902: aload           11
        //   904: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //   909: checkcast       Ljavax/mail/Header;
        //   912: astore          16
        //   914: aload           15
        //   916: aload           16
        //   918: invokevirtual   javax/mail/Header.getName:()Ljava/lang/String;
        //   921: invokespecial   com/sun/mail/imap/IMAPMessage.isHeaderLoaded:(Ljava/lang/String;)Z
        //   924: ifne            892
        //   927: aload           15
        //   929: getfield        com/sun/mail/imap/IMAPMessage.headers:Ljavax/mail/internet/InternetHeaders;
        //   932: aload           16
        //   934: invokevirtual   javax/mail/Header.getName:()Ljava/lang/String;
        //   937: aload           16
        //   939: invokevirtual   javax/mail/Header.getValue:()Ljava/lang/String;
        //   942: invokevirtual   javax/mail/internet/InternetHeaders.addHeader:(Ljava/lang/String;Ljava/lang/String;)V
        //   945: goto            892
        //   948: iload           6
        //   950: istore          7
        //   952: iload           8
        //   954: aload           10
        //   956: arraylength    
        //   957: if_icmpge       1005
        //   960: aload           15
        //   962: aload           10
        //   964: iload           8
        //   966: aaload         
        //   967: invokespecial   com/sun/mail/imap/IMAPMessage.setHeaderLoaded:(Ljava/lang/String;)V
        //   970: iload           8
        //   972: iconst_1       
        //   973: iadd           
        //   974: istore          8
        //   976: goto            948
        //   979: astore          11
        //   981: goto            398
        //   984: iconst_0       
        //   985: istore_3       
        //   986: goto            440
        //   989: aload_1        
        //   990: iload_3        
        //   991: aaload         
        //   992: ifnonnull       476
        //   995: iload_3        
        //   996: iconst_1       
        //   997: iadd           
        //   998: istore_3       
        //   999: goto            440
        //  1002: iconst_1       
        //  1003: istore          7
        //  1005: iload           4
        //  1007: iconst_1       
        //  1008: iadd           
        //  1009: istore          4
        //  1011: iload           7
        //  1013: istore          6
        //  1015: goto            528
        //  1018: iconst_0       
        //  1019: istore          8
        //  1021: goto            948
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  287    295    406    412    Any
        //  300    303    406    412    Any
        //  365    379    406    412    Any
        //  379    395    412    426    Lcom/sun/mail/iap/ConnectionException;
        //  379    395    979    984    Lcom/sun/mail/iap/CommandFailedException;
        //  379    395    426    440    Lcom/sun/mail/iap/ProtocolException;
        //  379    395    406    412    Any
        //  402    405    406    412    Any
        //  407    410    406    412    Any
        //  413    426    406    412    Any
        //  427    440    406    412    Any
        //  440    452    406    412    Any
        //  456    472    406    412    Any
        //  472    475    406    412    Any
        //  476    493    406    412    Any
        //  496    522    406    412    Any
        //  540    547    406    412    Any
        //  550    577    406    412    Any
        //  585    595    406    412    Any
        //  602    620    406    412    Any
        //  627    648    406    412    Any
        //  655    676    406    412    Any
        //  683    701    406    412    Any
        //  708    751    406    412    Any
        //  751    773    406    412    Any
        //  780    788    406    412    Any
        //  792    800    406    412    Any
        //  800    818    406    412    Any
        //  818    842    406    412    Any
        //  847    854    406    412    Any
        //  859    865    406    412    Any
        //  872    882    406    412    Any
        //  885    892    406    412    Any
        //  892    945    406    412    Any
        //  952    970    406    412    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean isHeaderLoaded(final String s) {
        synchronized (this) {
            return this.headersLoaded || (this.loadedHeaders != null && this.loadedHeaders.containsKey(s.toUpperCase(Locale.ENGLISH)));
        }
    }
    
    private void loadBODYSTRUCTURE() throws MessagingException {
        while (true) {
            Label_0109: {
                synchronized (this) {
                    if (this.bs != null) {
                        return;
                    }
                    final Object messageCacheLock = this.getMessageCacheLock();
                    synchronized (messageCacheLock) {
                        final IMAPMessage imapMessage = this;
                        final IMAPProtocol imapProtocol = imapMessage.getProtocol();
                        final IMAPMessage imapMessage2 = this;
                        imapMessage2.checkExpunged();
                        final IMAPMessage imapMessage3 = this;
                        final IMAPProtocol imapProtocol2 = imapProtocol;
                        final IMAPMessage imapMessage4 = this;
                        final int n = imapMessage4.getSequenceNumber();
                        final BODYSTRUCTURE bodystructure = imapProtocol2.fetchBodyStructure(n);
                        imapMessage3.bs = bodystructure;
                        final IMAPMessage imapMessage5 = this;
                        final BODYSTRUCTURE bodystructure2 = imapMessage5.bs;
                        if (bodystructure2 == null) {
                            final IMAPMessage imapMessage6 = this;
                            imapMessage6.forceCheckExpunged();
                            final String s = "Unable to load BODYSTRUCTURE";
                            throw new MessagingException(s);
                        }
                        break Label_0109;
                    }
                }
                try {
                    final IMAPMessage imapMessage = this;
                    final IMAPProtocol imapProtocol = imapMessage.getProtocol();
                    final IMAPMessage imapMessage2 = this;
                    imapMessage2.checkExpunged();
                    final IMAPMessage imapMessage3 = this;
                    final IMAPProtocol imapProtocol2 = imapProtocol;
                    final IMAPMessage imapMessage4 = this;
                    final int n = imapMessage4.getSequenceNumber();
                    final BODYSTRUCTURE bodystructure = imapProtocol2.fetchBodyStructure(n);
                    imapMessage3.bs = bodystructure;
                    final IMAPMessage imapMessage5 = this;
                    final BODYSTRUCTURE bodystructure2 = imapMessage5.bs;
                    if (bodystructure2 == null) {
                        final IMAPMessage imapMessage6 = this;
                        imapMessage6.forceCheckExpunged();
                        final String s = "Unable to load BODYSTRUCTURE";
                        throw new MessagingException(s);
                    }
                }
                catch (ConnectionException ex2) {
                    throw new FolderClosedException(this.folder, ex2.getMessage());
                }
                catch (ProtocolException ex3) {
                    this.forceCheckExpunged();
                    throw new MessagingException(ex3.getMessage(), ex3);
                }
            }
        }
        // monitorexit(messageCacheLock)
    }
    
    private void loadEnvelope() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        com/sun/mail/imap/IMAPMessage.envelope:Lcom/sun/mail/imap/protocol/ENVELOPE;
        //     6: astore          5
        //     8: aload           5
        //    10: ifnull          16
        //    13: aload_0        
        //    14: monitorexit    
        //    15: return         
        //    16: aconst_null    
        //    17: checkcast       [Lcom/sun/mail/iap/Response;
        //    20: astore          5
        //    22: aload_0        
        //    23: invokevirtual   com/sun/mail/imap/IMAPMessage.getMessageCacheLock:()Ljava/lang/Object;
        //    26: astore          5
        //    28: aload           5
        //    30: monitorenter   
        //    31: aload_0        
        //    32: invokevirtual   com/sun/mail/imap/IMAPMessage.getProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    35: astore          6
        //    37: aload_0        
        //    38: invokevirtual   com/sun/mail/imap/IMAPMessage.checkExpunged:()V
        //    41: aload_0        
        //    42: invokevirtual   com/sun/mail/imap/IMAPMessage.getSequenceNumber:()I
        //    45: istore_3       
        //    46: aload           6
        //    48: iload_3        
        //    49: getstatic       com/sun/mail/imap/IMAPMessage.EnvelopeCmd:Ljava/lang/String;
        //    52: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.fetch:(ILjava/lang/String;)[Lcom/sun/mail/iap/Response;
        //    55: astore          7
        //    57: iconst_0       
        //    58: istore_1       
        //    59: iload_1        
        //    60: aload           7
        //    62: arraylength    
        //    63: if_icmplt       114
        //    66: aload           6
        //    68: aload           7
        //    70: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.notifyResponseHandlers:([Lcom/sun/mail/iap/Response;)V
        //    73: aload           6
        //    75: aload           7
        //    77: aload           7
        //    79: arraylength    
        //    80: iconst_1       
        //    81: isub           
        //    82: aaload         
        //    83: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.handleResult:(Lcom/sun/mail/iap/Response;)V
        //    86: aload           5
        //    88: monitorexit    
        //    89: aload_0        
        //    90: getfield        com/sun/mail/imap/IMAPMessage.envelope:Lcom/sun/mail/imap/protocol/ENVELOPE;
        //    93: ifnonnull       13
        //    96: new             Ljavax/mail/MessagingException;
        //    99: dup            
        //   100: ldc_w           "Failed to load IMAP envelope"
        //   103: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   106: athrow         
        //   107: astore          5
        //   109: aload_0        
        //   110: monitorexit    
        //   111: aload           5
        //   113: athrow         
        //   114: aload           7
        //   116: iload_1        
        //   117: aaload         
        //   118: ifnull          294
        //   121: aload           7
        //   123: iload_1        
        //   124: aaload         
        //   125: instanceof      Lcom/sun/mail/imap/protocol/FetchResponse;
        //   128: ifeq            294
        //   131: aload           7
        //   133: iload_1        
        //   134: aaload         
        //   135: checkcast       Lcom/sun/mail/imap/protocol/FetchResponse;
        //   138: invokevirtual   com/sun/mail/imap/protocol/FetchResponse.getNumber:()I
        //   141: iload_3        
        //   142: if_icmpeq       148
        //   145: goto            294
        //   148: aload           7
        //   150: iload_1        
        //   151: aaload         
        //   152: checkcast       Lcom/sun/mail/imap/protocol/FetchResponse;
        //   155: astore          8
        //   157: aload           8
        //   159: invokevirtual   com/sun/mail/imap/protocol/FetchResponse.getItemCount:()I
        //   162: istore          4
        //   164: iconst_0       
        //   165: istore_2       
        //   166: iload_2        
        //   167: iload           4
        //   169: if_icmpge       294
        //   172: aload           8
        //   174: iload_2        
        //   175: invokevirtual   com/sun/mail/imap/protocol/FetchResponse.getItem:(I)Lcom/sun/mail/imap/protocol/Item;
        //   178: astore          9
        //   180: aload           9
        //   182: instanceof      Lcom/sun/mail/imap/protocol/ENVELOPE;
        //   185: ifeq            200
        //   188: aload_0        
        //   189: aload           9
        //   191: checkcast       Lcom/sun/mail/imap/protocol/ENVELOPE;
        //   194: putfield        com/sun/mail/imap/IMAPMessage.envelope:Lcom/sun/mail/imap/protocol/ENVELOPE;
        //   197: goto            301
        //   200: aload           9
        //   202: instanceof      Lcom/sun/mail/imap/protocol/INTERNALDATE;
        //   205: ifeq            250
        //   208: aload_0        
        //   209: aload           9
        //   211: checkcast       Lcom/sun/mail/imap/protocol/INTERNALDATE;
        //   214: invokevirtual   com/sun/mail/imap/protocol/INTERNALDATE.getDate:()Ljava/util/Date;
        //   217: putfield        com/sun/mail/imap/IMAPMessage.receivedDate:Ljava/util/Date;
        //   220: goto            301
        //   223: astore          6
        //   225: new             Ljavax/mail/FolderClosedException;
        //   228: dup            
        //   229: aload_0        
        //   230: getfield        com/sun/mail/imap/IMAPMessage.folder:Ljavax/mail/Folder;
        //   233: aload           6
        //   235: invokevirtual   com/sun/mail/iap/ConnectionException.getMessage:()Ljava/lang/String;
        //   238: invokespecial   javax/mail/FolderClosedException.<init>:(Ljavax/mail/Folder;Ljava/lang/String;)V
        //   241: athrow         
        //   242: astore          6
        //   244: aload           5
        //   246: monitorexit    
        //   247: aload           6
        //   249: athrow         
        //   250: aload           9
        //   252: instanceof      Lcom/sun/mail/imap/protocol/RFC822SIZE;
        //   255: ifeq            301
        //   258: aload_0        
        //   259: aload           9
        //   261: checkcast       Lcom/sun/mail/imap/protocol/RFC822SIZE;
        //   264: getfield        com/sun/mail/imap/protocol/RFC822SIZE.size:I
        //   267: putfield        com/sun/mail/imap/IMAPMessage.size:I
        //   270: goto            301
        //   273: astore          6
        //   275: aload_0        
        //   276: invokevirtual   com/sun/mail/imap/IMAPMessage.forceCheckExpunged:()V
        //   279: new             Ljavax/mail/MessagingException;
        //   282: dup            
        //   283: aload           6
        //   285: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   288: aload           6
        //   290: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   293: athrow         
        //   294: iload_1        
        //   295: iconst_1       
        //   296: iadd           
        //   297: istore_1       
        //   298: goto            59
        //   301: iload_2        
        //   302: iconst_1       
        //   303: iadd           
        //   304: istore_2       
        //   305: goto            166
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  2      8      107    114    Any
        //  16     31     107    114    Any
        //  31     57     223    242    Lcom/sun/mail/iap/ConnectionException;
        //  31     57     273    294    Lcom/sun/mail/iap/ProtocolException;
        //  31     57     242    250    Any
        //  59     86     223    242    Lcom/sun/mail/iap/ConnectionException;
        //  59     86     273    294    Lcom/sun/mail/iap/ProtocolException;
        //  59     86     242    250    Any
        //  86     89     242    250    Any
        //  89     107    107    114    Any
        //  121    145    223    242    Lcom/sun/mail/iap/ConnectionException;
        //  121    145    273    294    Lcom/sun/mail/iap/ProtocolException;
        //  121    145    242    250    Any
        //  148    164    223    242    Lcom/sun/mail/iap/ConnectionException;
        //  148    164    273    294    Lcom/sun/mail/iap/ProtocolException;
        //  148    164    242    250    Any
        //  172    197    223    242    Lcom/sun/mail/iap/ConnectionException;
        //  172    197    273    294    Lcom/sun/mail/iap/ProtocolException;
        //  172    197    242    250    Any
        //  200    220    223    242    Lcom/sun/mail/iap/ConnectionException;
        //  200    220    273    294    Lcom/sun/mail/iap/ProtocolException;
        //  200    220    242    250    Any
        //  225    242    242    250    Any
        //  244    247    242    250    Any
        //  247    250    107    114    Any
        //  250    270    223    242    Lcom/sun/mail/iap/ConnectionException;
        //  250    270    273    294    Lcom/sun/mail/iap/ProtocolException;
        //  250    270    242    250    Any
        //  275    294    242    250    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0059:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void loadFlags() throws MessagingException {
        synchronized (this) {
            if (this.flags == null) {
                final Object messageCacheLock = this.getMessageCacheLock();
                synchronized (messageCacheLock) {
                    final IMAPMessage imapMessage = this;
                    final IMAPProtocol imapProtocol = imapMessage.getProtocol();
                    final IMAPMessage imapMessage2 = this;
                    imapMessage2.checkExpunged();
                    final IMAPMessage imapMessage3 = this;
                    final IMAPProtocol imapProtocol2 = imapProtocol;
                    final IMAPMessage imapMessage4 = this;
                    final int n = imapMessage4.getSequenceNumber();
                    final Flags flags = imapProtocol2.fetchFlags(n);
                    imapMessage3.flags = flags;
                }
            }
            return;
        }
        try {
            final IMAPMessage imapMessage = this;
            final IMAPProtocol imapProtocol = imapMessage.getProtocol();
            final IMAPMessage imapMessage2 = this;
            imapMessage2.checkExpunged();
            final IMAPMessage imapMessage3 = this;
            final IMAPProtocol imapProtocol2 = imapProtocol;
            final IMAPMessage imapMessage4 = this;
            final int n = imapMessage4.getSequenceNumber();
            final Flags flags = imapProtocol2.fetchFlags(n);
            imapMessage3.flags = flags;
        }
        catch (ConnectionException ex) {
            throw new FolderClosedException(this.folder, ex.getMessage());
        }
        catch (ProtocolException ex2) {
            this.forceCheckExpunged();
            throw new MessagingException(ex2.getMessage(), ex2);
        }
    }
    
    private void loadHeaders() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        com/sun/mail/imap/IMAPMessage.headersLoaded:Z
        //     6: istore_1       
        //     7: iload_1        
        //     8: ifeq            14
        //    11: aload_0        
        //    12: monitorexit    
        //    13: return         
        //    14: aconst_null    
        //    15: astore_2       
        //    16: aload_0        
        //    17: invokevirtual   com/sun/mail/imap/IMAPMessage.getMessageCacheLock:()Ljava/lang/Object;
        //    20: astore_3       
        //    21: aload_3        
        //    22: monitorenter   
        //    23: aload_0        
        //    24: invokevirtual   com/sun/mail/imap/IMAPMessage.getProtocol:()Lcom/sun/mail/imap/protocol/IMAPProtocol;
        //    27: astore          4
        //    29: aload_0        
        //    30: invokevirtual   com/sun/mail/imap/IMAPMessage.checkExpunged:()V
        //    33: aload           4
        //    35: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.isREV1:()Z
        //    38: ifeq            92
        //    41: aload           4
        //    43: aload_0        
        //    44: invokevirtual   com/sun/mail/imap/IMAPMessage.getSequenceNumber:()I
        //    47: aload_0        
        //    48: ldc_w           "HEADER"
        //    51: invokespecial   com/sun/mail/imap/IMAPMessage.toSection:(Ljava/lang/String;)Ljava/lang/String;
        //    54: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.peekBody:(ILjava/lang/String;)Lcom/sun/mail/imap/protocol/BODY;
        //    57: astore          4
        //    59: aload           4
        //    61: ifnull          70
        //    64: aload           4
        //    66: invokevirtual   com/sun/mail/imap/protocol/BODY.getByteArrayInputStream:()Ljava/io/ByteArrayInputStream;
        //    69: astore_2       
        //    70: aload_3        
        //    71: monitorexit    
        //    72: aload_2        
        //    73: ifnonnull       160
        //    76: new             Ljavax/mail/MessagingException;
        //    79: dup            
        //    80: ldc_w           "Cannot load header"
        //    83: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //    86: athrow         
        //    87: astore_2       
        //    88: aload_0        
        //    89: monitorexit    
        //    90: aload_2        
        //    91: athrow         
        //    92: aload           4
        //    94: aload_0        
        //    95: invokevirtual   com/sun/mail/imap/IMAPMessage.getSequenceNumber:()I
        //    98: ldc_w           "HEADER"
        //   101: invokevirtual   com/sun/mail/imap/protocol/IMAPProtocol.fetchRFC822:(ILjava/lang/String;)Lcom/sun/mail/imap/protocol/RFC822DATA;
        //   104: astore          4
        //   106: aload           4
        //   108: ifnull          70
        //   111: aload           4
        //   113: invokevirtual   com/sun/mail/imap/protocol/RFC822DATA.getByteArrayInputStream:()Ljava/io/ByteArrayInputStream;
        //   116: astore_2       
        //   117: goto            70
        //   120: astore_2       
        //   121: new             Ljavax/mail/FolderClosedException;
        //   124: dup            
        //   125: aload_0        
        //   126: getfield        com/sun/mail/imap/IMAPMessage.folder:Ljavax/mail/Folder;
        //   129: aload_2        
        //   130: invokevirtual   com/sun/mail/iap/ConnectionException.getMessage:()Ljava/lang/String;
        //   133: invokespecial   javax/mail/FolderClosedException.<init>:(Ljavax/mail/Folder;Ljava/lang/String;)V
        //   136: athrow         
        //   137: astore_2       
        //   138: aload_3        
        //   139: monitorexit    
        //   140: aload_2        
        //   141: athrow         
        //   142: astore_2       
        //   143: aload_0        
        //   144: invokevirtual   com/sun/mail/imap/IMAPMessage.forceCheckExpunged:()V
        //   147: new             Ljavax/mail/MessagingException;
        //   150: dup            
        //   151: aload_2        
        //   152: invokevirtual   com/sun/mail/iap/ProtocolException.getMessage:()Ljava/lang/String;
        //   155: aload_2        
        //   156: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   159: athrow         
        //   160: aload_0        
        //   161: new             Ljavax/mail/internet/InternetHeaders;
        //   164: dup            
        //   165: aload_2        
        //   166: invokespecial   javax/mail/internet/InternetHeaders.<init>:(Ljava/io/InputStream;)V
        //   169: putfield        com/sun/mail/imap/IMAPMessage.headers:Ljavax/mail/internet/InternetHeaders;
        //   172: aload_0        
        //   173: iconst_1       
        //   174: putfield        com/sun/mail/imap/IMAPMessage.headersLoaded:Z
        //   177: goto            11
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  2      7      87     92     Any
        //  16     23     87     92     Any
        //  23     59     120    137    Lcom/sun/mail/iap/ConnectionException;
        //  23     59     142    160    Lcom/sun/mail/iap/ProtocolException;
        //  23     59     137    142    Any
        //  64     70     120    137    Lcom/sun/mail/iap/ConnectionException;
        //  64     70     142    160    Lcom/sun/mail/iap/ProtocolException;
        //  64     70     137    142    Any
        //  70     72     137    142    Any
        //  76     87     87     92     Any
        //  92     106    120    137    Lcom/sun/mail/iap/ConnectionException;
        //  92     106    142    160    Lcom/sun/mail/iap/ProtocolException;
        //  92     106    137    142    Any
        //  111    117    120    137    Lcom/sun/mail/iap/ConnectionException;
        //  111    117    142    160    Lcom/sun/mail/iap/ProtocolException;
        //  111    117    137    142    Any
        //  121    137    137    142    Any
        //  138    140    137    142    Any
        //  140    142    87     92     Any
        //  143    160    137    142    Any
        //  160    177    87     92     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0070:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void setHeaderLoaded(final String s) {
        synchronized (this) {
            if (this.loadedHeaders == null) {
                this.loadedHeaders = new Hashtable(1);
            }
            this.loadedHeaders.put(s.toUpperCase(Locale.ENGLISH), s);
        }
    }
    
    private void setHeadersLoaded(final boolean headersLoaded) {
        synchronized (this) {
            this.headersLoaded = headersLoaded;
        }
    }
    
    private String toSection(final String s) {
        if (this.sectionId == null) {
            return s;
        }
        return String.valueOf(this.sectionId) + "." + s;
    }
    
    Session _getSession() {
        return this.session;
    }
    
    void _setFlags(final Flags flags) {
        this.flags = flags;
    }
    
    @Override
    public void addFrom(final Address[] array) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void addHeader(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void addHeaderLine(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void addRecipients(final Message.RecipientType recipientType, final Address[] array) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    protected void checkExpunged() throws MessageRemovedException {
        if (this.expunged) {
            throw new MessageRemovedException();
        }
    }
    
    protected void forceCheckExpunged() throws MessageRemovedException, FolderClosedException {
        final Object messageCacheLock = this.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        while (true) {
            try {
                this.getProtocol().noop();
                // monitorexit(messageCacheLock)
                if (this.expunged) {
                    throw new MessageRemovedException();
                }
                goto Label_0053;
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this.folder, ex.getMessage());
                try {}
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ProtocolException ex2) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public Enumeration getAllHeaderLines() throws MessagingException {
        this.checkExpunged();
        this.loadHeaders();
        return super.getAllHeaderLines();
    }
    
    @Override
    public Enumeration getAllHeaders() throws MessagingException {
        this.checkExpunged();
        this.loadHeaders();
        return super.getAllHeaders();
    }
    
    @Override
    public String getContentID() throws MessagingException {
        this.checkExpunged();
        this.loadBODYSTRUCTURE();
        return this.bs.id;
    }
    
    @Override
    public String[] getContentLanguage() throws MessagingException {
        this.checkExpunged();
        this.loadBODYSTRUCTURE();
        if (this.bs.language != null) {
            return this.bs.language.clone();
        }
        return null;
    }
    
    @Override
    public String getContentMD5() throws MessagingException {
        this.checkExpunged();
        this.loadBODYSTRUCTURE();
        return this.bs.md5;
    }
    
    @Override
    protected InputStream getContentStream() throws MessagingException {
        int size = -1;
        ByteArrayInputStream byteArrayInputStream = null;
        final boolean peek = this.getPeek();
        final Object messageCacheLock = this.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        try {
            final IMAPProtocol protocol = this.getProtocol();
            this.checkExpunged();
            if (protocol.isREV1() && this.getFetchBlockSize() != -1) {
                final String section = this.toSection("TEXT");
                if (this.bs != null) {
                    size = this.bs.size;
                }
                return new IMAPInputStream(this, section, size, peek);
            }
            if (protocol.isREV1()) {
                BODY body;
                if (peek) {
                    body = protocol.peekBody(this.getSequenceNumber(), this.toSection("TEXT"));
                }
                else {
                    body = protocol.fetchBody(this.getSequenceNumber(), this.toSection("TEXT"));
                }
                if (body != null) {
                    byteArrayInputStream = body.getByteArrayInputStream();
                }
            }
            else {
                final RFC822DATA fetchRFC822 = protocol.fetchRFC822(this.getSequenceNumber(), "TEXT");
                if (fetchRFC822 != null) {
                    byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                }
            }
            // monitorexit(messageCacheLock)
            if (byteArrayInputStream == null) {
                throw new MessagingException("No content");
            }
        }
        catch (ConnectionException ex) {
            throw new FolderClosedException(this.folder, ex.getMessage());
            try {}
            finally {
            }
            // monitorexit(messageCacheLock)
        }
        catch (ProtocolException ex2) {
            this.forceCheckExpunged();
            throw new MessagingException(ex2.getMessage(), ex2);
        }
        return;
    }
    
    @Override
    public String getContentType() throws MessagingException {
        this.checkExpunged();
        if (this.type == null) {
            this.loadBODYSTRUCTURE();
            this.type = new ContentType(this.bs.type, this.bs.subtype, this.bs.cParams).toString();
        }
        return this.type;
    }
    
    @Override
    public DataHandler getDataHandler() throws MessagingException {
        while (true) {
            while (true) {
                Label_0189: {
                    synchronized (this) {
                        this.checkExpunged();
                        if (this.dh == null) {
                            this.loadBODYSTRUCTURE();
                            if (this.type == null) {
                                this.type = new ContentType(this.bs.type, this.bs.subtype, this.bs.cParams).toString();
                            }
                            if (this.bs.isMulti()) {
                                this.dh = new DataHandler(new IMAPMultipartDataSource(this, this.bs.bodies, this.sectionId, this));
                            }
                            else if (this.bs.isNested() && this.isREV1()) {
                                final BODYSTRUCTURE bodystructure = this.bs.bodies[0];
                                final ENVELOPE envelope = this.bs.envelope;
                                if (this.sectionId != null) {
                                    break Label_0189;
                                }
                                final String string = "1";
                                this.dh = new DataHandler(new IMAPNestedMessage(this, bodystructure, envelope, string), this.type);
                            }
                        }
                        return super.getDataHandler();
                    }
                }
                final String string = String.valueOf(this.sectionId) + ".1";
                continue;
            }
        }
    }
    
    @Override
    public String getDescription() throws MessagingException {
        this.checkExpunged();
        if (this.description != null) {
            return this.description;
        }
        this.loadBODYSTRUCTURE();
        if (this.bs.description == null) {
            return null;
        }
        try {
            this.description = MimeUtility.decodeText(this.bs.description);
            return this.description;
        }
        catch (UnsupportedEncodingException ex) {
            this.description = this.bs.description;
            return this.description;
        }
    }
    
    @Override
    public String getDisposition() throws MessagingException {
        this.checkExpunged();
        this.loadBODYSTRUCTURE();
        return this.bs.disposition;
    }
    
    @Override
    public String getEncoding() throws MessagingException {
        this.checkExpunged();
        this.loadBODYSTRUCTURE();
        return this.bs.encoding;
    }
    
    protected int getFetchBlockSize() {
        return ((IMAPStore)this.folder.getStore()).getFetchBlockSize();
    }
    
    @Override
    public String getFileName() throws MessagingException {
        this.checkExpunged();
        String value = null;
        this.loadBODYSTRUCTURE();
        if (this.bs.dParams != null) {
            value = this.bs.dParams.get("filename");
        }
        String value2;
        if ((value2 = value) == null) {
            value2 = value;
            if (this.bs.cParams != null) {
                value2 = this.bs.cParams.get("name");
            }
        }
        return value2;
    }
    
    @Override
    public Flags getFlags() throws MessagingException {
        synchronized (this) {
            this.checkExpunged();
            this.loadFlags();
            return super.getFlags();
        }
    }
    
    @Override
    public Address[] getFrom() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        return this.aaclone(this.envelope.from);
    }
    
    @Override
    public String getHeader(final String s, final String s2) throws MessagingException {
        this.checkExpunged();
        if (this.getHeader(s) == null) {
            return null;
        }
        return this.headers.getHeader(s, s2);
    }
    
    @Override
    public String[] getHeader(final String s) throws MessagingException {
        this.checkExpunged();
        if (this.isHeaderLoaded(s)) {
            return this.headers.getHeader(s);
        }
        InputStream inputStream = null;
        final Object messageCacheLock = this.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        try {
            final IMAPProtocol protocol = this.getProtocol();
            this.checkExpunged();
            if (protocol.isREV1()) {
                final BODY peekBody = protocol.peekBody(this.getSequenceNumber(), this.toSection("HEADER.FIELDS (" + s + ")"));
                if (peekBody != null) {
                    inputStream = peekBody.getByteArrayInputStream();
                }
            }
            else {
                final RFC822DATA fetchRFC822 = protocol.fetchRFC822(this.getSequenceNumber(), "HEADER.LINES (" + s + ")");
                if (fetchRFC822 != null) {
                    inputStream = fetchRFC822.getByteArrayInputStream();
                }
            }
            // monitorexit(messageCacheLock)
            if (inputStream == null) {
                return null;
            }
        }
        catch (ConnectionException ex) {
            throw new FolderClosedException(this.folder, ex.getMessage());
            try {}
            finally {
            }
            // monitorexit(messageCacheLock)
        }
        catch (ProtocolException ex2) {
            this.forceCheckExpunged();
            throw new MessagingException(ex2.getMessage(), ex2);
        }
        if (this.headers == null) {
            this.headers = new InternetHeaders();
        }
        this.headers.load(inputStream);
        final String headerLoaded;
        this.setHeaderLoaded(headerLoaded);
        return this.headers.getHeader(headerLoaded);
    }
    
    public String getInReplyTo() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        return this.envelope.inReplyTo;
    }
    
    @Override
    public int getLineCount() throws MessagingException {
        this.checkExpunged();
        this.loadBODYSTRUCTURE();
        return this.bs.lines;
    }
    
    @Override
    public Enumeration getMatchingHeaderLines(final String[] array) throws MessagingException {
        this.checkExpunged();
        this.loadHeaders();
        return super.getMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getMatchingHeaders(final String[] array) throws MessagingException {
        this.checkExpunged();
        this.loadHeaders();
        return super.getMatchingHeaders(array);
    }
    
    protected Object getMessageCacheLock() {
        return ((IMAPFolder)this.folder).messageCacheLock;
    }
    
    @Override
    public String getMessageID() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        return this.envelope.messageId;
    }
    
    @Override
    public Enumeration getNonMatchingHeaderLines(final String[] array) throws MessagingException {
        this.checkExpunged();
        this.loadHeaders();
        return super.getNonMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaders(final String[] array) throws MessagingException {
        this.checkExpunged();
        this.loadHeaders();
        return super.getNonMatchingHeaders(array);
    }
    
    public boolean getPeek() {
        synchronized (this) {
            return this.peek;
        }
    }
    
    protected IMAPProtocol getProtocol() throws ProtocolException, FolderClosedException {
        ((IMAPFolder)this.folder).waitIfIdle();
        final IMAPProtocol protocol = ((IMAPFolder)this.folder).protocol;
        if (protocol == null) {
            throw new FolderClosedException(this.folder);
        }
        return protocol;
    }
    
    @Override
    public Date getReceivedDate() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        if (this.receivedDate == null) {
            return null;
        }
        return new Date(this.receivedDate.getTime());
    }
    
    @Override
    public Address[] getRecipients(final Message.RecipientType recipientType) throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        if (recipientType == Message.RecipientType.TO) {
            return this.aaclone(this.envelope.to);
        }
        if (recipientType == Message.RecipientType.CC) {
            return this.aaclone(this.envelope.cc);
        }
        if (recipientType == Message.RecipientType.BCC) {
            return this.aaclone(this.envelope.bcc);
        }
        return super.getRecipients(recipientType);
    }
    
    @Override
    public Address[] getReplyTo() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        return this.aaclone(this.envelope.replyTo);
    }
    
    @Override
    public Address getSender() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        if (this.envelope.sender != null) {
            return this.envelope.sender[0];
        }
        return null;
    }
    
    @Override
    public Date getSentDate() throws MessagingException {
        this.checkExpunged();
        this.loadEnvelope();
        if (this.envelope.date == null) {
            return null;
        }
        return new Date(this.envelope.date.getTime());
    }
    
    protected int getSequenceNumber() {
        return this.seqnum;
    }
    
    @Override
    public int getSize() throws MessagingException {
        this.checkExpunged();
        if (this.size == -1) {
            this.loadEnvelope();
        }
        return this.size;
    }
    
    @Override
    public String getSubject() throws MessagingException {
        this.checkExpunged();
        if (this.subject != null) {
            return this.subject;
        }
        this.loadEnvelope();
        if (this.envelope.subject == null) {
            return null;
        }
        try {
            this.subject = MimeUtility.decodeText(this.envelope.subject);
            return this.subject;
        }
        catch (UnsupportedEncodingException ex) {
            this.subject = this.envelope.subject;
            return this.subject;
        }
    }
    
    protected long getUID() {
        return this.uid;
    }
    
    public void invalidateHeaders() {
        synchronized (this) {
            this.headersLoaded = false;
            this.loadedHeaders = null;
            this.envelope = null;
            this.bs = null;
            this.receivedDate = null;
            this.size = -1;
            this.type = null;
            this.subject = null;
            this.description = null;
        }
    }
    
    protected boolean isREV1() throws FolderClosedException {
        final IMAPProtocol protocol = ((IMAPFolder)this.folder).protocol;
        if (protocol == null) {
            throw new FolderClosedException(this.folder);
        }
        return protocol.isREV1();
    }
    
    @Override
    public boolean isSet(final Flags.Flag flag) throws MessagingException {
        synchronized (this) {
            this.checkExpunged();
            this.loadFlags();
            return super.isSet(flag);
        }
    }
    
    @Override
    public void removeHeader(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setContentID(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setContentLanguage(final String[] array) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setContentMD5(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setDataHandler(final DataHandler dataHandler) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setDescription(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setDisposition(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    protected void setExpunged(final boolean expunged) {
        super.setExpunged(expunged);
        this.seqnum = -1;
    }
    
    @Override
    public void setFileName(final String s) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setFlags(final Flags flags, final boolean b) throws MessagingException {
        // monitorenter(this)
        try {
            final Object messageCacheLock = this.getMessageCacheLock();
            // monitorenter(messageCacheLock)
            try {
                final IMAPProtocol protocol = this.getProtocol();
                this.checkExpunged();
                protocol.storeFlags(this.getSequenceNumber(), flags, b);
            }
            catch (ConnectionException ex) {
                throw new FolderClosedException(this.folder, ex.getMessage());
                try {}
                finally {
                }
                // monitorexit(messageCacheLock)
            }
            catch (ProtocolException ex2) {
                throw new MessagingException(ex2.getMessage(), ex2);
            }
        }
        finally {}
    }
    
    @Override
    public void setFrom(final Address address) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setHeader(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    protected void setMessageNumber(final int messageNumber) {
        super.setMessageNumber(messageNumber);
    }
    
    public void setPeek(final boolean peek) {
        synchronized (this) {
            this.peek = peek;
        }
    }
    
    @Override
    public void setRecipients(final Message.RecipientType recipientType, final Address[] array) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setReplyTo(final Address[] array) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setSender(final Address address) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    @Override
    public void setSentDate(final Date date) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    protected void setSequenceNumber(final int seqnum) {
        this.seqnum = seqnum;
    }
    
    @Override
    public void setSubject(final String s, final String s2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }
    
    protected void setUID(final long uid) {
        this.uid = uid;
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) throws IOException, MessagingException {
        InputStream inputStream = null;
        final boolean peek = this.getPeek();
        final Object messageCacheLock = this.getMessageCacheLock();
        // monitorenter(messageCacheLock)
        try {
            final IMAPProtocol protocol = this.getProtocol();
            this.checkExpunged();
            if (protocol.isREV1()) {
                BODY body;
                if (peek) {
                    body = protocol.peekBody(this.getSequenceNumber(), this.sectionId);
                }
                else {
                    body = protocol.fetchBody(this.getSequenceNumber(), this.sectionId);
                }
                if (body != null) {
                    inputStream = body.getByteArrayInputStream();
                }
            }
            else {
                final RFC822DATA fetchRFC822 = protocol.fetchRFC822(this.getSequenceNumber(), null);
                if (fetchRFC822 != null) {
                    inputStream = fetchRFC822.getByteArrayInputStream();
                }
            }
            // monitorexit(messageCacheLock)
            if (inputStream == null) {
                throw new MessagingException("No content");
            }
        }
        catch (ConnectionException ex) {
            throw new FolderClosedException(this.folder, ex.getMessage());
            try {}
            finally {
            }
            // monitorexit(messageCacheLock)
        }
        catch (ProtocolException ex2) {
            this.forceCheckExpunged();
            throw new MessagingException(ex2.getMessage(), ex2);
        }
        final byte[] array = new byte[1024];
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            final OutputStream outputStream2;
            outputStream2.write(array, 0, read);
        }
    }
    
    class FetchProfileCondition implements Condition
    {
        private String[] hdrs;
        private boolean needBodyStructure;
        private boolean needEnvelope;
        private boolean needFlags;
        private boolean needHeaders;
        private boolean needSize;
        private boolean needUID;
        
        public FetchProfileCondition(final FetchProfile fetchProfile) {
            this.needEnvelope = false;
            this.needFlags = false;
            this.needBodyStructure = false;
            this.needUID = false;
            this.needHeaders = false;
            this.needSize = false;
            this.hdrs = null;
            if (fetchProfile.contains(FetchProfile.Item.ENVELOPE)) {
                this.needEnvelope = true;
            }
            if (fetchProfile.contains(FetchProfile.Item.FLAGS)) {
                this.needFlags = true;
            }
            if (fetchProfile.contains(FetchProfile.Item.CONTENT_INFO)) {
                this.needBodyStructure = true;
            }
            if (fetchProfile.contains((FetchProfile.Item)UIDFolder.FetchProfileItem.UID)) {
                this.needUID = true;
            }
            if (fetchProfile.contains((FetchProfile.Item)IMAPFolder.FetchProfileItem.HEADERS)) {
                this.needHeaders = true;
            }
            if (fetchProfile.contains((FetchProfile.Item)IMAPFolder.FetchProfileItem.SIZE)) {
                this.needSize = true;
            }
            this.hdrs = fetchProfile.getHeaderNames();
        }
        
        @Override
        public boolean test(final IMAPMessage imapMessage) {
            if ((!this.needEnvelope || imapMessage._getEnvelope() != null) && (!this.needFlags || imapMessage._getFlags() != null) && (!this.needBodyStructure || imapMessage._getBodyStructure() != null) && (!this.needUID || imapMessage.getUID() != -1L) && (!this.needHeaders || imapMessage.areHeadersLoaded()) && (!this.needSize || imapMessage.size != -1)) {
                for (int i = 0; i < this.hdrs.length; ++i) {
                    if (!imapMessage.isHeaderLoaded(this.hdrs[i])) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }
    }
}
