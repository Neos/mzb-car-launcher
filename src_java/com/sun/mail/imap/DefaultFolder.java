package com.sun.mail.imap;

import javax.mail.*;
import com.sun.mail.imap.protocol.*;
import com.sun.mail.iap.*;

public class DefaultFolder extends IMAPFolder
{
    protected DefaultFolder(final IMAPStore imapStore) {
        super("", '\uffff', imapStore);
        this.exists = true;
        this.type = 2;
    }
    
    @Override
    public void appendMessages(final Message[] array) throws MessagingException {
        throw new MethodNotSupportedException("Cannot append to Default Folder");
    }
    
    @Override
    public boolean delete(final boolean b) throws MessagingException {
        throw new MethodNotSupportedException("Cannot delete Default Folder");
    }
    
    @Override
    public Message[] expunge() throws MessagingException {
        throw new MethodNotSupportedException("Cannot expunge Default Folder");
    }
    
    @Override
    public Folder getFolder(final String s) throws MessagingException {
        return new IMAPFolder(s, '\uffff', (IMAPStore)this.store);
    }
    
    @Override
    public String getName() {
        return this.fullName;
    }
    
    @Override
    public Folder getParent() {
        return null;
    }
    
    @Override
    public boolean hasNewMessages() throws MessagingException {
        return false;
    }
    
    @Override
    public Folder[] list(final String s) throws MessagingException {
        final ListInfo[] array = null;
        final ListInfo[] array2 = (ListInfo[])this.doCommand((ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                return imapProtocol.list("", s);
            }
        });
        Folder[] array3;
        if (array2 == null) {
            array3 = new Folder[0];
        }
        else {
            final IMAPFolder[] array4 = new IMAPFolder[array2.length];
            int n = 0;
            while (true) {
                array3 = array4;
                if (n >= array4.length) {
                    break;
                }
                array4[n] = new IMAPFolder(array2[n], (IMAPStore)this.store);
                ++n;
            }
        }
        return array3;
    }
    
    @Override
    public Folder[] listSubscribed(final String s) throws MessagingException {
        final ListInfo[] array = null;
        final ListInfo[] array2 = (ListInfo[])this.doCommand((ProtocolCommand)new ProtocolCommand() {
            @Override
            public Object doCommand(final IMAPProtocol imapProtocol) throws ProtocolException {
                return imapProtocol.lsub("", s);
            }
        });
        Folder[] array3;
        if (array2 == null) {
            array3 = new Folder[0];
        }
        else {
            final IMAPFolder[] array4 = new IMAPFolder[array2.length];
            int n = 0;
            while (true) {
                array3 = array4;
                if (n >= array4.length) {
                    break;
                }
                array4[n] = new IMAPFolder(array2[n], (IMAPStore)this.store);
                ++n;
            }
        }
        return array3;
    }
    
    @Override
    public boolean renameTo(final Folder folder) throws MessagingException {
        throw new MethodNotSupportedException("Cannot rename Default Folder");
    }
}
