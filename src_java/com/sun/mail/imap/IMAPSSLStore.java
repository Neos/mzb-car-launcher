package com.sun.mail.imap;

import javax.mail.*;

public class IMAPSSLStore extends IMAPStore
{
    public IMAPSSLStore(final Session session, final URLName urlName) {
        super(session, urlName, "imaps", 993, true);
    }
}
