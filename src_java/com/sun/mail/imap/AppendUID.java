package com.sun.mail.imap;

public class AppendUID
{
    public long uid;
    public long uidvalidity;
    
    public AppendUID(final long uidvalidity, final long uid) {
        this.uidvalidity = -1L;
        this.uid = -1L;
        this.uidvalidity = uidvalidity;
        this.uid = uid;
    }
}
