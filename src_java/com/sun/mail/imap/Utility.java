package com.sun.mail.imap;

import javax.mail.*;
import java.util.*;
import com.sun.mail.imap.protocol.*;

public final class Utility
{
    public static MessageSet[] toMessageSet(final Message[] array, final Condition condition) {
        final Vector<MessageSet> vector = new Vector<MessageSet>(1);
        int n;
        for (int i = 0; i < array.length; i = n + 1) {
            final IMAPMessage imapMessage = (IMAPMessage)array[i];
            if (imapMessage.isExpunged()) {
                n = i;
            }
            else {
                final int sequenceNumber = imapMessage.getSequenceNumber();
                if (condition != null) {
                    n = i;
                    if (!condition.test(imapMessage)) {
                        continue;
                    }
                }
                final MessageSet set = new MessageSet();
                set.start = sequenceNumber;
                int j = i + 1;
                int end = sequenceNumber;
                while (j < array.length) {
                    final IMAPMessage imapMessage2 = (IMAPMessage)array[j];
                    int n2 = 0;
                    Label_0143: {
                        if (imapMessage2.isExpunged()) {
                            n2 = end;
                        }
                        else {
                            final int sequenceNumber2 = imapMessage2.getSequenceNumber();
                            if (condition != null) {
                                n2 = end;
                                if (!condition.test(imapMessage2)) {
                                    break Label_0143;
                                }
                            }
                            if (sequenceNumber2 != end + 1) {
                                --j;
                                break;
                            }
                            n2 = sequenceNumber2;
                        }
                    }
                    ++j;
                    end = n2;
                }
                set.end = end;
                vector.addElement(set);
                n = j;
            }
        }
        if (vector.isEmpty()) {
            return null;
        }
        final MessageSet[] array2 = new MessageSet[vector.size()];
        vector.copyInto(array2);
        return array2;
    }
    
    public static UIDSet[] toUIDSet(final Message[] array) {
        final Vector<UIDSet> vector = new Vector<UIDSet>(1);
        for (int i = 0; i < array.length; ++i) {
            final IMAPMessage imapMessage = (IMAPMessage)array[i];
            if (!imapMessage.isExpunged()) {
                long uid = imapMessage.getUID();
                final UIDSet set = new UIDSet();
                set.start = uid;
                for (++i; i < array.length; ++i) {
                    final IMAPMessage imapMessage2 = (IMAPMessage)array[i];
                    if (!imapMessage2.isExpunged()) {
                        final long uid2 = imapMessage2.getUID();
                        if (uid2 != 1L + uid) {
                            --i;
                            break;
                        }
                        uid = uid2;
                    }
                }
                set.end = uid;
                vector.addElement(set);
            }
        }
        if (vector.isEmpty()) {
            return null;
        }
        final UIDSet[] array2 = new UIDSet[vector.size()];
        vector.copyInto(array2);
        return array2;
    }
    
    public interface Condition
    {
        boolean test(final IMAPMessage p0);
    }
}
