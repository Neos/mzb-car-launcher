package com.sun.mail.imap;

import java.util.*;
import javax.mail.internet.*;
import com.sun.mail.imap.protocol.*;
import javax.mail.*;

public class IMAPMultipartDataSource extends MimePartDataSource implements MultipartDataSource
{
    private Vector parts;
    
    protected IMAPMultipartDataSource(final MimePart mimePart, final BODYSTRUCTURE[] array, final String s, final IMAPMessage imapMessage) {
        super(mimePart);
        this.parts = new Vector(array.length);
        for (int i = 0; i < array.length; ++i) {
            final Vector parts = this.parts;
            final BODYSTRUCTURE bodystructure = array[i];
            String s2;
            if (s == null) {
                s2 = Integer.toString(i + 1);
            }
            else {
                s2 = String.valueOf(s) + "." + Integer.toString(i + 1);
            }
            parts.addElement(new IMAPBodyPart(bodystructure, s2, imapMessage));
        }
    }
    
    @Override
    public BodyPart getBodyPart(final int n) throws MessagingException {
        return this.parts.elementAt(n);
    }
    
    @Override
    public int getCount() {
        return this.parts.size();
    }
}
