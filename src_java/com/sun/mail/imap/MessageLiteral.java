package com.sun.mail.imap;

import com.sun.mail.iap.*;
import com.sun.mail.util.*;
import javax.mail.*;
import java.io.*;

class MessageLiteral implements Literal
{
    private byte[] buf;
    private Message msg;
    private int msgSize;
    
    public MessageLiteral(final Message msg, final int n) throws MessagingException, IOException {
        this.msgSize = -1;
        this.msg = msg;
        final LengthCounter lengthCounter = new LengthCounter(n);
        final CRLFOutputStream crlfOutputStream = new CRLFOutputStream(lengthCounter);
        msg.writeTo(crlfOutputStream);
        crlfOutputStream.flush();
        this.msgSize = lengthCounter.getSize();
        this.buf = lengthCounter.getBytes();
    }
    
    @Override
    public int size() {
        return this.msgSize;
    }
    
    @Override
    public void writeTo(OutputStream ex) throws IOException {
        try {
            if (this.buf != null) {
                ((OutputStream)ex).write(this.buf, 0, this.msgSize);
                return;
            }
            ex = (MessagingException)new CRLFOutputStream((OutputStream)ex);
            final MessageLiteral messageLiteral = this;
            final Message message = messageLiteral.msg;
            final MessagingException ex2 = ex;
            message.writeTo((OutputStream)ex2);
            return;
        }
        catch (MessagingException ex3) {}
        try {
            final MessageLiteral messageLiteral = this;
            final Message message = messageLiteral.msg;
            final MessagingException ex2 = ex;
            message.writeTo((OutputStream)ex2);
        }
        catch (MessagingException ex) {
            throw new IOException("MessagingException while appending message: " + ex);
        }
    }
}
