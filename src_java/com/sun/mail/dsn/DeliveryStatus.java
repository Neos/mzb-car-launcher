package com.sun.mail.dsn;

import javax.mail.internet.*;
import javax.mail.*;
import com.sun.mail.util.*;
import java.util.*;
import java.io.*;

public class DeliveryStatus
{
    private static boolean debug;
    protected InternetHeaders messageDSN;
    protected InternetHeaders[] recipientDSN;
    
    static {
        final boolean b = false;
        DeliveryStatus.debug = false;
        try {
            final String property = System.getProperty("mail.dsn.debug");
            boolean debug = b;
            if (property != null) {
                debug = b;
                if (!property.equalsIgnoreCase("false")) {
                    debug = true;
                }
            }
            DeliveryStatus.debug = debug;
        }
        catch (SecurityException ex) {}
    }
    
    public DeliveryStatus() throws MessagingException {
        this.messageDSN = new InternetHeaders();
        this.recipientDSN = new InternetHeaders[0];
    }
    
    public DeliveryStatus(final InputStream inputStream) throws MessagingException, IOException {
        this.messageDSN = new InternetHeaders(inputStream);
        if (DeliveryStatus.debug) {
            System.out.println("DSN: got messageDSN");
        }
        final Vector<InternetHeaders> vector = new Vector<InternetHeaders>();
        while (true) {
            try {
                while (inputStream.available() > 0) {
                    final InternetHeaders internetHeaders = new InternetHeaders(inputStream);
                    if (DeliveryStatus.debug) {
                        System.out.println("DSN: got recipientDSN");
                    }
                    vector.addElement(internetHeaders);
                }
                if (DeliveryStatus.debug) {
                    System.out.println("DSN: recipientDSN size " + vector.size());
                }
                vector.copyInto(this.recipientDSN = new InternetHeaders[vector.size()]);
            }
            catch (EOFException ex) {
                if (DeliveryStatus.debug) {
                    System.out.println("DSN: got EOFException");
                }
                continue;
            }
            break;
        }
    }
    
    private static void writeInternetHeaders(final InternetHeaders internetHeaders, final LineOutputStream lineOutputStream) throws IOException {
        final Enumeration allHeaderLines = internetHeaders.getAllHeaderLines();
        try {
            while (allHeaderLines.hasMoreElements()) {
                lineOutputStream.writeln(allHeaderLines.nextElement());
            }
        }
        catch (MessagingException ex) {
            final Exception nextException = ex.getNextException();
            if (nextException instanceof IOException) {
                throw (IOException)nextException;
            }
            throw new IOException("Exception writing headers: " + ex);
        }
    }
    
    public void addRecipientDSN(final InternetHeaders internetHeaders) {
        final InternetHeaders[] recipientDSN = new InternetHeaders[this.recipientDSN.length + 1];
        System.arraycopy(this.recipientDSN, 0, recipientDSN, 0, this.recipientDSN.length);
        (this.recipientDSN = recipientDSN)[this.recipientDSN.length - 1] = internetHeaders;
    }
    
    public InternetHeaders getMessageDSN() {
        return this.messageDSN;
    }
    
    public InternetHeaders getRecipientDSN(final int n) {
        return this.recipientDSN[n];
    }
    
    public int getRecipientDSNCount() {
        return this.recipientDSN.length;
    }
    
    public void setMessageDSN(final InternetHeaders messageDSN) {
        this.messageDSN = messageDSN;
    }
    
    @Override
    public String toString() {
        return "DeliveryStatus: Reporting-MTA=" + this.messageDSN.getHeader("Reporting-MTA", null) + ", #Recipients=" + this.recipientDSN.length;
    }
    
    public void writeTo(final OutputStream outputStream) throws IOException, MessagingException {
        LineOutputStream lineOutputStream;
        if (outputStream instanceof LineOutputStream) {
            lineOutputStream = (LineOutputStream)outputStream;
        }
        else {
            lineOutputStream = new LineOutputStream(outputStream);
        }
        writeInternetHeaders(this.messageDSN, lineOutputStream);
        lineOutputStream.writeln();
        for (int i = 0; i < this.recipientDSN.length; ++i) {
            writeInternetHeaders(this.recipientDSN[i], lineOutputStream);
            lineOutputStream.writeln();
        }
    }
}
