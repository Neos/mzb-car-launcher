package com.sun.mail.dsn;

import javax.activation.*;
import javax.mail.*;
import myjava.awt.datatransfer.*;
import java.io.*;

public class multipart_report implements DataContentHandler
{
    private ActivationDataFlavor myDF;
    
    public multipart_report() {
        this.myDF = new ActivationDataFlavor(MultipartReport.class, "multipart/report", "Multipart Report");
    }
    
    @Override
    public Object getContent(final DataSource dataSource) throws IOException {
        try {
            return new MultipartReport(dataSource);
        }
        catch (MessagingException ex2) {
            final IOException ex = new IOException("Exception while constructing MultipartReport");
            ex.initCause(ex2);
            throw ex;
        }
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (this.myDF.equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { this.myDF };
    }
    
    @Override
    public void writeTo(final Object o, final String s, final OutputStream outputStream) throws IOException {
        if (!(o instanceof MultipartReport)) {
            return;
        }
        try {
            ((MultipartReport)o).writeTo(outputStream);
        }
        catch (MessagingException ex) {
            throw new IOException(ex.toString());
        }
    }
}
