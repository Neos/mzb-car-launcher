package com.sun.mail.dsn;

import javax.mail.internet.*;
import javax.activation.*;
import java.util.*;
import java.io.*;
import javax.mail.*;

public class MultipartReport extends MimeMultipart
{
    protected boolean constructed;
    
    public MultipartReport() throws MessagingException {
        super("report");
        this.setBodyPart(new MimeBodyPart(), 0);
        this.setBodyPart(new MimeBodyPart(), 1);
        this.constructed = true;
    }
    
    public MultipartReport(final String text, final DeliveryStatus deliveryStatus) throws MessagingException {
        super("report");
        final ContentType contentType = new ContentType(this.contentType);
        contentType.setParameter("report-type", "delivery-status");
        this.contentType = contentType.toString();
        final MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setText(text);
        this.setBodyPart(mimeBodyPart, 0);
        final MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
        mimeBodyPart2.setContent(deliveryStatus, "message/delivery-status");
        this.setBodyPart(mimeBodyPart2, 1);
        this.constructed = true;
    }
    
    public MultipartReport(final String s, final DeliveryStatus deliveryStatus, final InternetHeaders internetHeaders) throws MessagingException {
        this(s, deliveryStatus);
        if (internetHeaders != null) {
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(new MessageHeaders(internetHeaders), "text/rfc822-headers");
            this.setBodyPart(mimeBodyPart, 2);
        }
    }
    
    public MultipartReport(final String s, final DeliveryStatus deliveryStatus, final MimeMessage mimeMessage) throws MessagingException {
        this(s, deliveryStatus);
        if (mimeMessage != null) {
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(mimeMessage, "message/rfc822");
            this.setBodyPart(mimeBodyPart, 2);
        }
    }
    
    public MultipartReport(final DataSource dataSource) throws MessagingException {
        super(dataSource);
        this.parse();
        this.constructed = true;
    }
    
    private void setBodyPart(final BodyPart bodyPart, final int n) throws MessagingException {
        synchronized (this) {
            if (this.parts == null) {
                this.parts = new Vector();
            }
            if (n < this.parts.size()) {
                super.removeBodyPart(n);
            }
            super.addBodyPart(bodyPart, n);
        }
    }
    
    @Override
    public void addBodyPart(final BodyPart bodyPart) throws MessagingException {
        synchronized (this) {
            if (!this.constructed) {
                super.addBodyPart(bodyPart);
                return;
            }
            throw new MessagingException("Can't add body parts to multipart/report 1");
        }
    }
    
    @Override
    public void addBodyPart(final BodyPart bodyPart, final int n) throws MessagingException {
        synchronized (this) {
            throw new MessagingException("Can't add body parts to multipart/report 2");
        }
    }
    
    public DeliveryStatus getDeliveryStatus() throws MessagingException {
        DeliveryStatus deliveryStatus = null;
        synchronized (this) {
            if (this.getCount() >= 2) {
                final BodyPart bodyPart = this.getBodyPart(1);
                if (bodyPart.isMimeType("message/delivery-status")) {
                    try {
                        deliveryStatus = (DeliveryStatus)bodyPart.getContent();
                    }
                    catch (IOException ex) {
                        throw new MessagingException("IOException getting DeliveryStatus", ex);
                    }
                }
            }
            return deliveryStatus;
        }
    }
    
    public MimeMessage getReturnedMessage() throws MessagingException {
        MimeMessage mimeMessage = null;
        synchronized (this) {
            if (this.getCount() >= 3) {
                final BodyPart bodyPart = this.getBodyPart(2);
                if (!bodyPart.isMimeType("message/rfc822")) {
                    if (!bodyPart.isMimeType("text/rfc822-headers")) {
                        return mimeMessage;
                    }
                }
                try {
                    mimeMessage = (MimeMessage)bodyPart.getContent();
                }
                catch (IOException ex) {
                    throw new MessagingException("IOException getting ReturnedMessage", ex);
                }
            }
            return mimeMessage;
        }
    }
    
    public String getText() throws MessagingException {
        while (true) {
            synchronized (this) {
                try {
                    final BodyPart bodyPart = this.getBodyPart(0);
                    String s;
                    if (bodyPart.isMimeType("text/plain")) {
                        s = (String)bodyPart.getContent();
                    }
                    else {
                        if (bodyPart.isMimeType("multipart/alternative")) {
                            final Multipart multipart = (Multipart)bodyPart.getContent();
                            for (int i = 0; i < multipart.getCount(); ++i) {
                                final BodyPart bodyPart2 = multipart.getBodyPart(i);
                                if (bodyPart2.isMimeType("text/plain")) {
                                    s = (String)bodyPart2.getContent();
                                    return s;
                                }
                            }
                        }
                        return null;
                    }
                    return s;
                }
                catch (IOException ex) {
                    throw new MessagingException("Exception getting text content", ex);
                }
            }
            return null;
        }
    }
    
    public MimeBodyPart getTextBodyPart() throws MessagingException {
        synchronized (this) {
            return (MimeBodyPart)this.getBodyPart(0);
        }
    }
    
    @Override
    public void removeBodyPart(final int n) throws MessagingException {
        throw new MessagingException("Can't remove body parts from multipart/report");
    }
    
    @Override
    public boolean removeBodyPart(final BodyPart bodyPart) throws MessagingException {
        throw new MessagingException("Can't remove body parts from multipart/report");
    }
    
    public void setDeliveryStatus(final DeliveryStatus deliveryStatus) throws MessagingException {
        synchronized (this) {
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(deliveryStatus, "message/delivery-status");
            this.setBodyPart(mimeBodyPart, 2);
            final ContentType contentType = new ContentType(this.contentType);
            contentType.setParameter("report-type", "delivery-status");
            this.contentType = contentType.toString();
        }
    }
    
    public void setReturnedMessage(final MimeMessage mimeMessage) throws MessagingException {
        // monitorenter(this)
        Label_0026: {
            if (mimeMessage != null) {
                break Label_0026;
            }
            while (true) {
                while (true) {
                    MimeBodyPart mimeBodyPart;
                    try {
                        final BodyPart bodyPart = this.parts.elementAt(2);
                        super.removeBodyPart(2);
                        return;
                        this.setBodyPart(mimeBodyPart, 2);
                        return;
                        mimeBodyPart = new MimeBodyPart();
                        // iftrue(Label_0062:, !mimeMessage instanceof MessageHeaders)
                        mimeBodyPart.setContent(mimeMessage, "text/rfc822-headers");
                        continue;
                    }
                    finally {
                    }
                    // monitorexit(this)
                    Label_0062: {
                        final Throwable t;
                        mimeBodyPart.setContent(t, "message/rfc822");
                    }
                    continue;
                }
            }
        }
    }
    
    @Override
    public void setSubType(final String s) throws MessagingException {
        synchronized (this) {
            throw new MessagingException("Can't change subtype of MultipartReport");
        }
    }
    
    public void setText(final String text) throws MessagingException {
        synchronized (this) {
            final MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setText(text);
            this.setBodyPart(mimeBodyPart, 0);
        }
    }
    
    public void setTextBodyPart(final MimeBodyPart mimeBodyPart) throws MessagingException {
        synchronized (this) {
            this.setBodyPart(mimeBodyPart, 0);
        }
    }
}
