package com.sun.mail.dsn;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.*;
import javax.activation.*;

public class MessageHeaders extends MimeMessage
{
    public MessageHeaders() throws MessagingException {
        super((Session)null);
        this.content = new byte[0];
    }
    
    public MessageHeaders(final InputStream inputStream) throws MessagingException {
        super(null, inputStream);
        this.content = new byte[0];
    }
    
    public MessageHeaders(final InternetHeaders headers) throws MessagingException {
        super((Session)null);
        this.headers = headers;
        this.content = new byte[0];
    }
    
    @Override
    protected InputStream getContentStream() {
        return new ByteArrayInputStream(this.content);
    }
    
    @Override
    public InputStream getInputStream() {
        return new ByteArrayInputStream(this.content);
    }
    
    @Override
    public int getSize() {
        return 0;
    }
    
    @Override
    public void setDataHandler(final DataHandler dataHandler) throws MessagingException {
        throw new MessagingException("Can't set content for MessageHeaders");
    }
}
