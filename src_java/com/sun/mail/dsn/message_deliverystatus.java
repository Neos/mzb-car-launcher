package com.sun.mail.dsn;

import javax.activation.*;
import javax.mail.*;
import myjava.awt.datatransfer.*;
import java.io.*;

public class message_deliverystatus implements DataContentHandler
{
    ActivationDataFlavor ourDataFlavor;
    
    public message_deliverystatus() {
        this.ourDataFlavor = new ActivationDataFlavor(DeliveryStatus.class, "message/delivery-status", "Delivery Status");
    }
    
    @Override
    public Object getContent(final DataSource dataSource) throws IOException {
        try {
            return new DeliveryStatus(dataSource.getInputStream());
        }
        catch (MessagingException ex) {
            throw new IOException("Exception creating DeliveryStatus in message/devliery-status DataContentHandler: " + ex.toString());
        }
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (this.ourDataFlavor.equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { this.ourDataFlavor };
    }
    
    @Override
    public void writeTo(final Object o, final String s, final OutputStream outputStream) throws IOException {
        if (o instanceof DeliveryStatus) {
            final DeliveryStatus deliveryStatus = (DeliveryStatus)o;
            try {
                deliveryStatus.writeTo(outputStream);
                return;
            }
            catch (MessagingException ex) {
                throw new IOException(ex.toString());
            }
        }
        throw new IOException("unsupported object");
    }
}
