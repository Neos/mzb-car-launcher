package com.sun.mail.dsn;

import javax.mail.internet.*;
import javax.activation.*;
import javax.mail.*;
import myjava.awt.datatransfer.*;
import java.io.*;

public class text_rfc822headers implements DataContentHandler
{
    private static ActivationDataFlavor myDF;
    private static ActivationDataFlavor myDFs;
    
    static {
        text_rfc822headers.myDF = new ActivationDataFlavor(MessageHeaders.class, "text/rfc822-headers", "RFC822 headers");
        text_rfc822headers.myDFs = new ActivationDataFlavor(String.class, "text/rfc822-headers", "RFC822 headers");
    }
    
    private String getCharset(String s) {
        try {
            if ((s = new ContentType(s).getParameter("charset")) == null) {
                s = "us-ascii";
            }
            s = MimeUtility.javaCharset(s);
            return s;
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    private Object getStringContent(final DataSource dataSource) throws IOException {
        while (true) {
            Object charset = null;
            while (true) {
                int n;
                Object o;
                int read;
                try {
                    final InputStreamReader inputStreamReader = new InputStreamReader(dataSource.getInputStream(), (String)(charset = this.getCharset(dataSource.getContentType())));
                    n = 0;
                    o = new char[1024];
                    read = inputStreamReader.read((char[])o, n, ((char[])o).length - n);
                    if (read == -1) {
                        return new String((char[])o, 0, n);
                    }
                }
                catch (IllegalArgumentException ex) {
                    throw new UnsupportedEncodingException((String)charset);
                }
                final int n2 = n += read;
                if (n2 >= ((char[])o).length) {
                    final int length = ((char[])o).length;
                    int n3;
                    if (length < 262144) {
                        n3 = length + length;
                    }
                    else {
                        n3 = length + 262144;
                    }
                    charset = new char[n3];
                    System.arraycopy(o, 0, charset, 0, n2);
                    o = charset;
                    n = n2;
                    continue;
                }
                continue;
            }
        }
    }
    
    @Override
    public Object getContent(final DataSource dataSource) throws IOException {
        try {
            return new MessageHeaders(dataSource.getInputStream());
        }
        catch (MessagingException ex) {
            throw new IOException("Exception creating MessageHeaders: " + ex);
        }
    }
    
    public Object getTransferData(final DataFlavor dataFlavor, final DataSource dataSource) throws IOException {
        if (text_rfc822headers.myDF.equals(dataFlavor)) {
            return this.getContent(dataSource);
        }
        if (text_rfc822headers.myDFs.equals(dataFlavor)) {
            return this.getStringContent(dataSource);
        }
        return null;
    }
    
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[] { text_rfc822headers.myDF, text_rfc822headers.myDFs };
    }
    
    @Override
    public void writeTo(final Object o, String s, final OutputStream outputStream) throws IOException {
        if (o instanceof MessageHeaders) {
            final MessageHeaders messageHeaders = (MessageHeaders)o;
            try {
                messageHeaders.writeTo(outputStream);
                return;
            }
            catch (MessagingException ex) {
                final Exception nextException = ex.getNextException();
                if (nextException instanceof IOException) {
                    throw (IOException)nextException;
                }
                throw new IOException("Exception writing headers: " + ex);
            }
        }
        if (!(o instanceof String)) {
            throw new IOException("\"" + text_rfc822headers.myDFs.getMimeType() + "\" DataContentHandler requires String object, " + "was given object of type " + o.getClass().toString());
        }
        String charset = null;
        try {
            s = (charset = this.getCharset(s));
            final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, s);
            final String s2 = (String)o;
            outputStreamWriter.write(s2, 0, s2.length());
            outputStreamWriter.flush();
        }
        catch (IllegalArgumentException ex2) {
            throw new UnsupportedEncodingException(charset);
        }
    }
}
