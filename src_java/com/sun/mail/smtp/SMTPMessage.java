package com.sun.mail.smtp;

import javax.mail.internet.*;
import java.io.*;
import javax.mail.*;

public class SMTPMessage extends MimeMessage
{
    public static final int NOTIFY_DELAY = 4;
    public static final int NOTIFY_FAILURE = 2;
    public static final int NOTIFY_NEVER = -1;
    public static final int NOTIFY_SUCCESS = 1;
    public static final int RETURN_FULL = 1;
    public static final int RETURN_HDRS = 2;
    private static final String[] returnOptionString;
    private boolean allow8bitMIME;
    private String envelopeFrom;
    private String extension;
    private int notifyOptions;
    private int returnOption;
    private boolean sendPartial;
    private String submitter;
    
    static {
        returnOptionString = new String[] { null, "FULL", "HDRS" };
    }
    
    public SMTPMessage(final Session session) {
        super(session);
        this.notifyOptions = 0;
        this.returnOption = 0;
        this.sendPartial = false;
        this.allow8bitMIME = false;
        this.submitter = null;
        this.extension = null;
    }
    
    public SMTPMessage(final Session session, final InputStream inputStream) throws MessagingException {
        super(session, inputStream);
        this.notifyOptions = 0;
        this.returnOption = 0;
        this.sendPartial = false;
        this.allow8bitMIME = false;
        this.submitter = null;
        this.extension = null;
    }
    
    public SMTPMessage(final MimeMessage mimeMessage) throws MessagingException {
        super(mimeMessage);
        this.notifyOptions = 0;
        this.returnOption = 0;
        this.sendPartial = false;
        this.allow8bitMIME = false;
        this.submitter = null;
        this.extension = null;
    }
    
    public boolean getAllow8bitMIME() {
        return this.allow8bitMIME;
    }
    
    String getDSNNotify() {
        if (this.notifyOptions == 0) {
            return null;
        }
        if (this.notifyOptions == -1) {
            return "NEVER";
        }
        final StringBuffer sb = new StringBuffer();
        if ((this.notifyOptions & 0x1) != 0x0) {
            sb.append("SUCCESS");
        }
        if ((this.notifyOptions & 0x2) != 0x0) {
            if (sb.length() != 0) {
                sb.append(',');
            }
            sb.append("FAILURE");
        }
        if ((this.notifyOptions & 0x4) != 0x0) {
            if (sb.length() != 0) {
                sb.append(',');
            }
            sb.append("DELAY");
        }
        return sb.toString();
    }
    
    String getDSNRet() {
        return SMTPMessage.returnOptionString[this.returnOption];
    }
    
    public String getEnvelopeFrom() {
        return this.envelopeFrom;
    }
    
    public String getMailExtension() {
        return this.extension;
    }
    
    public int getNotifyOptions() {
        return this.notifyOptions;
    }
    
    public int getReturnOption() {
        return this.returnOption;
    }
    
    public boolean getSendPartial() {
        return this.sendPartial;
    }
    
    public String getSubmitter() {
        return this.submitter;
    }
    
    public void setAllow8bitMIME(final boolean allow8bitMIME) {
        this.allow8bitMIME = allow8bitMIME;
    }
    
    public void setEnvelopeFrom(final String envelopeFrom) {
        this.envelopeFrom = envelopeFrom;
    }
    
    public void setMailExtension(final String extension) {
        this.extension = extension;
    }
    
    public void setNotifyOptions(final int notifyOptions) {
        if (notifyOptions < -1 || notifyOptions >= 8) {
            throw new IllegalArgumentException("Bad return option");
        }
        this.notifyOptions = notifyOptions;
    }
    
    public void setReturnOption(final int returnOption) {
        if (returnOption < 0 || returnOption > 2) {
            throw new IllegalArgumentException("Bad return option");
        }
        this.returnOption = returnOption;
    }
    
    public void setSendPartial(final boolean sendPartial) {
        this.sendPartial = sendPartial;
    }
    
    public void setSubmitter(final String submitter) {
        this.submitter = submitter;
    }
}
