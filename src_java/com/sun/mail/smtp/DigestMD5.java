package com.sun.mail.smtp;

import java.io.*;
import java.util.*;
import com.sun.mail.util.*;
import java.security.*;

public class DigestMD5
{
    private static char[] digits;
    private String clientResponse;
    private PrintStream debugout;
    private MessageDigest md5;
    private String uri;
    
    static {
        DigestMD5.digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    }
    
    public DigestMD5(final PrintStream debugout) {
        this.debugout = debugout;
        if (debugout != null) {
            debugout.println("DEBUG DIGEST-MD5: Loaded");
        }
    }
    
    private static String toHex(final byte[] array) {
        final char[] array2 = new char[array.length * 2];
        int i = 0;
        int n = 0;
        while (i < array.length) {
            final int n2 = array[i] & 0xFF;
            final int n3 = n + 1;
            array2[n] = DigestMD5.digits[n2 >> 4];
            n = n3 + 1;
            array2[n3] = DigestMD5.digits[n2 & 0xF];
            ++i;
        }
        return new String(array2);
    }
    
    private Hashtable tokenize(String sval) throws IOException {
        final Hashtable<String, String> hashtable = new Hashtable<String, String>();
        final byte[] bytes = sval.getBytes();
        sval = null;
        final StreamTokenizer streamTokenizer = new StreamTokenizer(new InputStreamReader(new BASE64DecoderStream(new ByteArrayInputStream(bytes, 4, bytes.length - 4))));
        streamTokenizer.ordinaryChars(48, 57);
        streamTokenizer.wordChars(48, 57);
        while (true) {
            final int nextToken = streamTokenizer.nextToken();
            if (nextToken == -1) {
                break;
            }
            switch (nextToken) {
                default: {
                    continue;
                }
                case -3: {
                    if (sval == null) {
                        sval = streamTokenizer.sval;
                        continue;
                    }
                }
                case 34: {
                    if (this.debugout != null) {
                        this.debugout.println("DEBUG DIGEST-MD5: Received => " + sval + "='" + streamTokenizer.sval + "'");
                    }
                    if (hashtable.containsKey(sval)) {
                        hashtable.put(sval, (Object)hashtable.get(sval) + "," + streamTokenizer.sval);
                    }
                    else {
                        hashtable.put(sval, streamTokenizer.sval);
                    }
                    sval = null;
                    continue;
                }
            }
        }
        return hashtable;
    }
    
    public byte[] authClient(String s, final String s2, final String s3, String string, String nextToken) throws IOException {
        while (true) {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final BASE64EncoderStream base64EncoderStream = new BASE64EncoderStream(byteArrayOutputStream, Integer.MAX_VALUE);
            while (true) {
                Label_0728: {
                    try {
                        final SecureRandom secureRandom = new SecureRandom();
                        this.md5 = MessageDigest.getInstance("MD5");
                        final StringBuffer sb = new StringBuffer();
                        this.uri = "smtp/" + s;
                        final byte[] array = new byte[32];
                        if (this.debugout != null) {
                            this.debugout.println("DEBUG DIGEST-MD5: Begin authentication ...");
                        }
                        final Hashtable tokenize = this.tokenize(nextToken);
                        if ((nextToken = string) == null) {
                            string = tokenize.get("realm");
                            if (string == null) {
                                break Label_0728;
                            }
                            nextToken = new StringTokenizer(string, ",").nextToken();
                        }
                        s = tokenize.get("nonce");
                        secureRandom.nextBytes(array);
                        base64EncoderStream.write(array);
                        base64EncoderStream.flush();
                        string = byteArrayOutputStream.toString();
                        byteArrayOutputStream.reset();
                        this.md5.update(this.md5.digest(ASCIIUtility.getBytes(String.valueOf(s2) + ":" + nextToken + ":" + s3)));
                        this.md5.update(ASCIIUtility.getBytes(":" + s + ":" + string));
                        this.clientResponse = String.valueOf(toHex(this.md5.digest())) + ":" + s + ":" + "00000001" + ":" + string + ":" + "auth" + ":";
                        this.md5.update(ASCIIUtility.getBytes("AUTHENTICATE:" + this.uri));
                        this.md5.update(ASCIIUtility.getBytes(String.valueOf(this.clientResponse) + toHex(this.md5.digest())));
                        sb.append("username=\"" + s2 + "\"");
                        sb.append(",realm=\"" + nextToken + "\"");
                        sb.append(",qop=" + "auth");
                        sb.append(",nc=" + "00000001");
                        sb.append(",nonce=\"" + s + "\"");
                        sb.append(",cnonce=\"" + string + "\"");
                        sb.append(",digest-uri=\"" + this.uri + "\"");
                        sb.append(",response=" + toHex(this.md5.digest()));
                        if (this.debugout != null) {
                            this.debugout.println("DEBUG DIGEST-MD5: Response => " + sb.toString());
                        }
                        base64EncoderStream.write(ASCIIUtility.getBytes(sb.toString()));
                        base64EncoderStream.flush();
                        return byteArrayOutputStream.toByteArray();
                    }
                    catch (NoSuchAlgorithmException ex) {
                        if (this.debugout != null) {
                            this.debugout.println("DEBUG DIGEST-MD5: " + ex);
                        }
                        throw new IOException(ex.toString());
                    }
                }
                nextToken = s;
                continue;
            }
        }
    }
    
    public boolean authServer(final String s) throws IOException {
        final Hashtable tokenize = this.tokenize(s);
        this.md5.update(ASCIIUtility.getBytes(":" + this.uri));
        this.md5.update(ASCIIUtility.getBytes(String.valueOf(this.clientResponse) + toHex(this.md5.digest())));
        final String hex = toHex(this.md5.digest());
        if (!hex.equals(tokenize.get("rspauth"))) {
            if (this.debugout != null) {
                this.debugout.println("DEBUG DIGEST-MD5: Expected => rspauth=" + hex);
            }
            return false;
        }
        return true;
    }
}
