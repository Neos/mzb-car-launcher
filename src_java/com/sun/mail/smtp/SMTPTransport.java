package com.sun.mail.smtp;

import javax.mail.internet.*;
import java.net.*;
import java.io.*;
import com.sun.mail.util.*;
import javax.mail.*;
import java.util.*;

public class SMTPTransport extends Transport
{
    private static final byte[] CRLF;
    private static final String UNKNOWN = "UNKNOWN";
    private static char[] hexchar;
    private static final String[] ignoreList;
    private Address[] addresses;
    private SMTPOutputStream dataStream;
    private int defaultPort;
    private MessagingException exception;
    private Hashtable extMap;
    private Address[] invalidAddr;
    private boolean isSSL;
    private int lastReturnCode;
    private String lastServerResponse;
    private LineInputStream lineInputStream;
    private String localHostName;
    private DigestMD5 md5support;
    private MimeMessage message;
    private String name;
    private PrintStream out;
    private boolean quitWait;
    private boolean reportSuccess;
    private String saslRealm;
    private boolean sendPartiallyFailed;
    private BufferedInputStream serverInput;
    private OutputStream serverOutput;
    private Socket serverSocket;
    private boolean useRset;
    private boolean useStartTLS;
    private Address[] validSentAddr;
    private Address[] validUnsentAddr;
    
    static {
        ignoreList = new String[] { "Bcc", "Content-Length" };
        CRLF = new byte[] { 13, 10 };
        SMTPTransport.hexchar = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    
    public SMTPTransport(final Session session, final URLName urlName) {
        this(session, urlName, "smtp", 25, false);
    }
    
    protected SMTPTransport(final Session session, final URLName urlName, String protocol, final int defaultPort, final boolean isSSL) {
        final boolean b = true;
        super(session, urlName);
        this.name = "smtp";
        this.defaultPort = 25;
        this.isSSL = false;
        this.sendPartiallyFailed = false;
        this.quitWait = false;
        this.saslRealm = "UNKNOWN";
        if (urlName != null) {
            protocol = urlName.getProtocol();
        }
        this.name = protocol;
        this.defaultPort = defaultPort;
        this.isSSL = isSSL;
        this.out = session.getDebugOut();
        final String property = session.getProperty("mail." + protocol + ".quitwait");
        this.quitWait = (property == null || property.equalsIgnoreCase("true"));
        final String property2 = session.getProperty("mail." + protocol + ".reportsuccess");
        this.reportSuccess = (property2 != null && property2.equalsIgnoreCase("true"));
        final String property3 = session.getProperty("mail." + protocol + ".starttls.enable");
        this.useStartTLS = (property3 != null && property3.equalsIgnoreCase("true"));
        final String property4 = session.getProperty("mail." + protocol + ".userset");
        this.useRset = (property4 != null && property4.equalsIgnoreCase("true") && b);
    }
    
    private void closeConnection() throws MessagingException {
        try {
            if (this.serverSocket != null) {
                this.serverSocket.close();
            }
        }
        catch (IOException ex) {
            throw new MessagingException("Server Close Failed", ex);
        }
        finally {
            this.serverSocket = null;
            this.serverOutput = null;
            this.serverInput = null;
            this.lineInputStream = null;
            if (super.isConnected()) {
                super.close();
            }
        }
    }
    
    private boolean convertTo8Bit(final MimePart mimePart) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = false;
        boolean b5 = b;
        boolean b6 = b2;
        boolean b7;
        try {
            if (mimePart.isMimeType("text/*")) {
                b5 = b;
                b6 = b2;
                final String encoding = mimePart.getEncoding();
                b7 = b3;
                if (encoding != null) {
                    b5 = b;
                    b6 = b2;
                    if (!encoding.equalsIgnoreCase("quoted-printable")) {
                        b5 = b;
                        b6 = b2;
                        b7 = b3;
                        if (!encoding.equalsIgnoreCase("base64")) {
                            return b7;
                        }
                    }
                    b5 = b;
                    b6 = b2;
                    b7 = b3;
                    if (this.is8Bit(mimePart.getInputStream())) {
                        b5 = b;
                        b6 = b2;
                        mimePart.setContent(mimePart.getContent(), mimePart.getContentType());
                        b5 = b;
                        b6 = b2;
                        mimePart.setHeader("Content-Transfer-Encoding", "8bit");
                        return true;
                    }
                }
            }
            else {
                b5 = b;
                b6 = b2;
                b7 = b3;
                if (mimePart.isMimeType("multipart/*")) {
                    b5 = b;
                    b6 = b2;
                    final MimeMultipart mimeMultipart = (MimeMultipart)mimePart.getContent();
                    b5 = b;
                    b6 = b2;
                    final int count = mimeMultipart.getCount();
                    int n = 0;
                    while (true) {
                        b7 = b4;
                        if (n >= count) {
                            break;
                        }
                        b5 = b4;
                        b6 = b4;
                        if (this.convertTo8Bit((MimePart)mimeMultipart.getBodyPart(n))) {
                            b4 = true;
                        }
                        ++n;
                    }
                }
            }
        }
        catch (MessagingException ex) {
            return b5;
        }
        catch (IOException ex2) {
            b7 = b6;
        }
        return b7;
    }
    
    private void expandGroups() {
        Vector<InternetAddress> vector = null;
        int i = 0;
    Label_0122_Outer:
        while (i < this.addresses.length) {
            final InternetAddress internetAddress = (InternetAddress)this.addresses[i];
            while (true) {
                while (true) {
                    int n = 0;
                Label_0111:
                    while (true) {
                        Vector<InternetAddress> vector2;
                        if (internetAddress.isGroup()) {
                            if ((vector2 = vector) == null) {
                                vector2 = new Vector<InternetAddress>();
                                n = 0;
                                break Label_0111;
                            }
                        }
                        else {
                            if ((vector2 = vector) != null) {
                                vector.addElement(internetAddress);
                                vector2 = vector;
                                break Label_0111;
                            }
                            break Label_0111;
                        }
                        while (true) {
                            try {
                                final InternetAddress[] group = internetAddress.getGroup(true);
                                if (group != null) {
                                    for (int j = 0; j < group.length; ++j) {
                                        vector2.addElement(group[j]);
                                    }
                                }
                                else {
                                    vector2.addElement(internetAddress);
                                }
                                ++i;
                                vector = vector2;
                                continue Label_0122_Outer;
                                vector2.addElement((InternetAddress)this.addresses[n]);
                                ++n;
                                break;
                            }
                            catch (ParseException ex) {
                                vector2.addElement(internetAddress);
                                continue Label_0111;
                            }
                            continue Label_0111;
                        }
                        break;
                    }
                    if (n >= i) {
                        continue Label_0122_Outer;
                    }
                    break;
                }
                continue;
            }
        }
        if (vector != null) {
            final InternetAddress[] addresses = new InternetAddress[vector.size()];
            vector.copyInto(addresses);
            this.addresses = addresses;
        }
    }
    
    private DigestMD5 getMD5() {
        synchronized (this) {
            if (this.md5support == null) {
                PrintStream out;
                if (this.debug) {
                    out = this.out;
                }
                else {
                    out = null;
                }
                this.md5support = new DigestMD5(out);
            }
            return this.md5support;
        }
    }
    
    private void initStreams() throws IOException {
        final Properties properties = this.session.getProperties();
        final PrintStream debugOut = this.session.getDebugOut();
        final boolean debug = this.session.getDebug();
        final String property = properties.getProperty("mail.debug.quote");
        final boolean b = property != null && property.equalsIgnoreCase("true");
        final TraceInputStream traceInputStream = new TraceInputStream(this.serverSocket.getInputStream(), debugOut);
        traceInputStream.setTrace(debug);
        traceInputStream.setQuote(b);
        final TraceOutputStream traceOutputStream = new TraceOutputStream(this.serverSocket.getOutputStream(), debugOut);
        traceOutputStream.setTrace(debug);
        traceOutputStream.setQuote(b);
        this.serverOutput = new BufferedOutputStream(traceOutputStream);
        this.serverInput = new BufferedInputStream(traceInputStream);
        this.lineInputStream = new LineInputStream(this.serverInput);
    }
    
    private boolean is8Bit(final InputStream inputStream) {
        int n = 0;
        boolean b = false;
        try {
            while (true) {
                final int read = inputStream.read();
                if (read < 0) {
                    if (this.debug && b) {
                        this.out.println("DEBUG SMTP: found an 8bit part");
                    }
                    return b;
                }
                final int n2 = read & 0xFF;
                int n3;
                if (n2 == 13 || n2 == 10) {
                    n3 = 0;
                }
                else {
                    if (n2 == 0) {
                        return false;
                    }
                    if ((n3 = n + 1) > 998) {
                        return false;
                    }
                }
                n = n3;
                if (n2 <= 127) {
                    continue;
                }
                b = true;
                n = n3;
            }
        }
        catch (IOException ex) {
            return false;
        }
    }
    
    private boolean isNotLastLine(final String s) {
        return s != null && s.length() >= 4 && s.charAt(3) == '-';
    }
    
    private void issueSendCommand(final String s, int lastReturnCode) throws MessagingException {
        this.sendCommand(s);
        final int serverResponse = this.readServerResponse();
        if (serverResponse != lastReturnCode) {
            if (this.validSentAddr == null) {
                lastReturnCode = 0;
            }
            else {
                lastReturnCode = this.validSentAddr.length;
            }
            int length;
            if (this.validUnsentAddr == null) {
                length = 0;
            }
            else {
                length = this.validUnsentAddr.length;
            }
            final Address[] validUnsentAddr = new Address[lastReturnCode + length];
            if (lastReturnCode > 0) {
                System.arraycopy(this.validSentAddr, 0, validUnsentAddr, 0, lastReturnCode);
            }
            if (length > 0) {
                System.arraycopy(this.validUnsentAddr, 0, validUnsentAddr, lastReturnCode, length);
            }
            this.validSentAddr = null;
            this.validUnsentAddr = validUnsentAddr;
            if (this.debug) {
                this.out.println("DEBUG SMTP: got response code " + serverResponse + ", with response: " + this.lastServerResponse);
            }
            final String lastServerResponse = this.lastServerResponse;
            lastReturnCode = this.lastReturnCode;
            if (this.serverSocket != null) {
                this.issueCommand("RSET", 250);
            }
            this.lastServerResponse = lastServerResponse;
            this.lastReturnCode = lastReturnCode;
            throw new SMTPSendFailedException(s, serverResponse, this.lastServerResponse, this.exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
        }
    }
    
    private String normalizeAddress(final String s) {
        String string = s;
        if (!s.startsWith("<")) {
            string = s;
            if (!s.endsWith(">")) {
                string = "<" + s + ">";
            }
        }
        return string;
    }
    
    private void openServer() throws MessagingException {
        int port = -1;
        String s = "UNKNOWN";
        int n;
        String hostName;
        try {
            n = (port = this.serverSocket.getPort());
            s = s;
            hostName = this.serverSocket.getInetAddress().getHostName();
            port = n;
            s = hostName;
            if (this.debug) {
                port = n;
                s = hostName;
                this.out.println("DEBUG SMTP: starting protocol to host \"" + hostName + "\", port " + n);
            }
            port = n;
            s = hostName;
            this.initStreams();
            port = n;
            s = hostName;
            final int serverResponse = this.readServerResponse();
            if (serverResponse != 220) {
                port = n;
                s = hostName;
                this.serverSocket.close();
                port = n;
                s = hostName;
                this.serverSocket = null;
                port = n;
                s = hostName;
                this.serverOutput = null;
                port = n;
                s = hostName;
                this.serverInput = null;
                port = n;
                s = hostName;
                this.lineInputStream = null;
                port = n;
                s = hostName;
                if (this.debug) {
                    port = n;
                    s = hostName;
                    this.out.println("DEBUG SMTP: got bad greeting from host \"" + hostName + "\", port: " + n + ", response: " + serverResponse + "\n");
                }
                port = n;
                s = hostName;
                throw new MessagingException("Got bad greeting from SMTP host: " + hostName + ", port: " + n + ", response: " + serverResponse);
            }
        }
        catch (IOException ex) {
            throw new MessagingException("Could not start protocol to SMTP host: " + s + ", port: " + port, ex);
        }
        if (this.debug) {
            this.out.println("DEBUG SMTP: protocol started to host \"" + hostName + "\", port: " + n + "\n");
        }
    }
    
    private void openServer(final String s, int n) throws MessagingException {
        if (this.debug) {
            this.out.println("DEBUG SMTP: trying to connect to host \"" + s + "\", port " + n + ", isSSL " + this.isSSL);
        }
        int port = n;
        try {
            this.serverSocket = SocketFetcher.getSocket(s, n, this.session.getProperties(), "mail." + this.name, this.isSSL);
            port = n;
            n = (port = this.serverSocket.getPort());
            this.initStreams();
            port = n;
            final int serverResponse = this.readServerResponse();
            if (serverResponse != 220) {
                port = n;
                this.serverSocket.close();
                port = n;
                this.serverSocket = null;
                port = n;
                this.serverOutput = null;
                port = n;
                this.serverInput = null;
                port = n;
                this.lineInputStream = null;
                port = n;
                if (this.debug) {
                    port = n;
                    this.out.println("DEBUG SMTP: could not connect to host \"" + s + "\", port: " + n + ", response: " + serverResponse + "\n");
                }
                port = n;
                throw new MessagingException("Could not connect to SMTP host: " + s + ", port: " + n + ", response: " + serverResponse);
            }
            goto Label_0303;
        }
        catch (UnknownHostException ex) {
            throw new MessagingException("Unknown SMTP host: " + s, ex);
        }
        catch (IOException ex2) {
            throw new MessagingException("Could not connect to SMTP host: " + s + ", port: " + port, ex2);
        }
    }
    
    private void sendCommand(final byte[] array) throws MessagingException {
        assert Thread.holdsLock(this);
        try {
            this.serverOutput.write(array);
            this.serverOutput.write(SMTPTransport.CRLF);
            this.serverOutput.flush();
        }
        catch (IOException ex) {
            throw new MessagingException("Can't send command to SMTP host", ex);
        }
    }
    
    protected static String xtext(String string) {
        StringBuffer sb = null;
        StringBuffer sb2;
        for (int i = 0; i < string.length(); ++i, sb = sb2) {
            final char char1 = string.charAt(i);
            if (char1 >= '\u0080') {
                throw new IllegalArgumentException("Non-ASCII character in SMTP submitter: " + string);
            }
            if (char1 < '!' || char1 > '~' || char1 == '+' || char1 == '=') {
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuffer(string.length() + 4);
                    sb2.append(string.substring(0, i));
                }
                sb2.append('+');
                sb2.append(SMTPTransport.hexchar[(char1 & '\u00f0') >> 4]);
                sb2.append(SMTPTransport.hexchar[char1 & '\u000f']);
            }
            else if ((sb2 = sb) != null) {
                sb.append(char1);
                sb2 = sb;
            }
        }
        if (sb != null) {
            string = sb.toString();
        }
        return string;
    }
    
    protected void checkConnected() {
        if (!super.isConnected()) {
            throw new IllegalStateException("Not connected");
        }
    }
    
    @Override
    public void close() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   javax/mail/Transport.isConnected:()Z
        //     6: istore_2       
        //     7: iload_2        
        //     8: ifne            14
        //    11: aload_0        
        //    12: monitorexit    
        //    13: return         
        //    14: aload_0        
        //    15: getfield        com/sun/mail/smtp/SMTPTransport.serverSocket:Ljava/net/Socket;
        //    18: ifnull          76
        //    21: aload_0        
        //    22: ldc_w           "QUIT"
        //    25: invokevirtual   com/sun/mail/smtp/SMTPTransport.sendCommand:(Ljava/lang/String;)V
        //    28: aload_0        
        //    29: getfield        com/sun/mail/smtp/SMTPTransport.quitWait:Z
        //    32: ifeq            76
        //    35: aload_0        
        //    36: invokevirtual   com/sun/mail/smtp/SMTPTransport.readServerResponse:()I
        //    39: istore_1       
        //    40: iload_1        
        //    41: sipush          221
        //    44: if_icmpeq       76
        //    47: iload_1        
        //    48: iconst_m1      
        //    49: if_icmpeq       76
        //    52: aload_0        
        //    53: getfield        com/sun/mail/smtp/SMTPTransport.out:Ljava/io/PrintStream;
        //    56: new             Ljava/lang/StringBuilder;
        //    59: dup            
        //    60: ldc_w           "DEBUG SMTP: QUIT failed with "
        //    63: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    66: iload_1        
        //    67: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    70: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    73: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //    76: aload_0        
        //    77: invokespecial   com/sun/mail/smtp/SMTPTransport.closeConnection:()V
        //    80: goto            11
        //    83: astore_3       
        //    84: aload_0        
        //    85: monitorexit    
        //    86: aload_3        
        //    87: athrow         
        //    88: astore_3       
        //    89: aload_0        
        //    90: invokespecial   com/sun/mail/smtp/SMTPTransport.closeConnection:()V
        //    93: aload_3        
        //    94: athrow         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  2      7      83     88     Any
        //  14     40     88     95     Any
        //  52     76     88     95     Any
        //  76     80     83     88     Any
        //  89     95     83     88     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0014:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void connect(final Socket serverSocket) throws MessagingException {
        synchronized (this) {
            this.serverSocket = serverSocket;
            super.connect();
        }
    }
    
    protected OutputStream data() throws MessagingException {
        assert Thread.holdsLock(this);
        this.issueSendCommand("DATA", 354);
        return this.dataStream = new SMTPOutputStream(this.serverOutput);
    }
    
    protected boolean ehlo(String s) throws MessagingException {
        boolean b = false;
        Label_0098: {
            if (s == null) {
                break Label_0098;
            }
            s = "EHLO " + s;
        Label_0085_Outer:
            while (true) {
                this.sendCommand(s);
                final int serverResponse = this.readServerResponse();
                while (true) {
                    if (serverResponse != 250) {
                        break Label_0085;
                    }
                    final BufferedReader bufferedReader = new BufferedReader(new StringReader(this.lastServerResponse));
                    this.extMap = new Hashtable();
                    int n = 1;
                    try {
                        while (true) {
                            s = bufferedReader.readLine();
                            if (s == null) {
                                break;
                            }
                            if (n != 0) {
                                n = 0;
                            }
                            else {
                                if (s.length() < 5) {
                                    continue Label_0085_Outer;
                                }
                                final String substring = s.substring(4);
                                final int index = substring.indexOf(32);
                                String substring2 = "";
                                s = substring;
                                if (index > 0) {
                                    substring2 = substring.substring(index + 1);
                                    s = substring.substring(0, index);
                                }
                                if (this.debug) {
                                    this.out.println("DEBUG SMTP: Found extension \"" + s + "\", arg \"" + substring2 + "\"");
                                }
                                this.extMap.put(s.toUpperCase(Locale.ENGLISH), substring2);
                            }
                        }
                        if (serverResponse == 250) {
                            b = true;
                        }
                        return b;
                        s = "EHLO";
                        continue Label_0085_Outer;
                    }
                    catch (IOException ex) {
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        try {
            this.closeConnection();
        }
        catch (MessagingException ex) {}
    }
    
    protected void finishData() throws IOException, MessagingException {
        assert Thread.holdsLock(this);
        this.dataStream.ensureAtBOL();
        this.issueSendCommand(".", 250);
    }
    
    public String getExtensionParameter(final String s) {
        if (this.extMap == null) {
            return null;
        }
        return this.extMap.get(s.toUpperCase(Locale.ENGLISH));
    }
    
    public int getLastReturnCode() {
        synchronized (this) {
            return this.lastReturnCode;
        }
    }
    
    public String getLastServerResponse() {
        synchronized (this) {
            return this.lastServerResponse;
        }
    }
    
    public String getLocalHost() {
        // monitorenter(this)
        while (true) {
            try {
                try {
                    if (this.localHostName == null || this.localHostName.length() <= 0) {
                        this.localHostName = this.session.getProperty("mail." + this.name + ".localhost");
                    }
                    if (this.localHostName == null || this.localHostName.length() <= 0) {
                        this.localHostName = this.session.getProperty("mail." + this.name + ".localaddress");
                    }
                    if (this.localHostName == null || this.localHostName.length() <= 0) {
                        final InetAddress localHost = InetAddress.getLocalHost();
                        this.localHostName = localHost.getHostName();
                        if (this.localHostName == null) {
                            this.localHostName = "[" + localHost.getHostAddress() + "]";
                        }
                    }
                    return this.localHostName;
                }
                finally {
                }
                // monitorexit(this)
            }
            catch (UnknownHostException ex) {
                continue;
            }
            break;
        }
    }
    
    public boolean getReportSuccess() {
        synchronized (this) {
            return this.reportSuccess;
        }
    }
    
    public String getSASLRealm() {
        synchronized (this) {
            if (this.saslRealm == "UNKNOWN") {
                this.saslRealm = this.session.getProperty("mail." + this.name + ".sasl.realm");
                if (this.saslRealm == null) {
                    this.saslRealm = this.session.getProperty("mail." + this.name + ".saslrealm");
                }
            }
            return this.saslRealm;
        }
    }
    
    public boolean getStartTLS() {
        synchronized (this) {
            return this.useStartTLS;
        }
    }
    
    public boolean getUseRset() {
        synchronized (this) {
            return this.useRset;
        }
    }
    
    protected void helo(final String s) throws MessagingException {
        if (s != null) {
            this.issueCommand("HELO " + s, 250);
            return;
        }
        this.issueCommand("HELO", 250);
    }
    
    @Override
    public boolean isConnected() {
        boolean b = false;
        synchronized (this) {
            if (super.isConnected()) {
                try {
                    if (this.useRset) {
                        this.sendCommand("RSET");
                    }
                    else {
                        this.sendCommand("NOOP");
                    }
                    final int serverResponse = this.readServerResponse();
                    if (serverResponse >= 0 && serverResponse != 421) {
                        b = true;
                        return b;
                    }
                }
                catch (Exception ex) {
                    try {
                        this.closeConnection();
                    }
                    catch (MessagingException ex2) {}
                }
                try {
                    this.closeConnection();
                }
                catch (MessagingException ex3) {}
            }
            return b;
        }
    }
    
    public void issueCommand(final String s, final int n) throws MessagingException {
        synchronized (this) {
            this.sendCommand(s);
            if (this.readServerResponse() != n) {
                throw new MessagingException(this.lastServerResponse);
            }
        }
    }
    // monitorexit(this)
    
    protected void mailFrom() throws MessagingException {
        Serializable envelope = null;
        if (this.message instanceof SMTPMessage) {
            envelope = ((SMTPMessage)this.message).getEnvelopeFrom();
        }
        Object o = null;
        Label_0069: {
            if (envelope != null) {
                o = envelope;
                if (((String)envelope).length() > 0) {
                    break Label_0069;
                }
            }
            o = this.session.getProperty("mail." + this.name + ".from");
        }
    Label_0359_Outer:
        while (true) {
            Label_0481: {
                Object address = null;
                Label_0122: {
                    if (o != null) {
                        address = o;
                        if (((String)o).length() > 0) {
                            break Label_0122;
                        }
                    }
                    if (this.message == null) {
                        break Label_0359_Outer;
                    }
                    o = this.message.getFrom();
                    if (o == null || ((String)o).length <= 0) {
                        break Label_0359_Outer;
                    }
                    o = o[0];
                    if (o == null) {
                        break Label_0481;
                    }
                    address = ((InternetAddress)o).getAddress();
                }
                Serializable s = (Serializable)(o = "MAIL FROM:" + this.normalizeAddress((String)address));
                if (this.supportsExtension("DSN")) {
                    String dsnRet = null;
                    if (this.message instanceof SMTPMessage) {
                        dsnRet = ((SMTPMessage)this.message).getDSNRet();
                    }
                    String property;
                    if ((property = dsnRet) == null) {
                        property = this.session.getProperty("mail." + this.name + ".dsn.ret");
                    }
                    o = s;
                    if (property != null) {
                        o = String.valueOf(s) + " RET=" + property;
                    }
                }
                Serializable string = (Serializable)o;
                while (true) {
                    if (!this.supportsExtension("AUTH")) {
                        break Label_0359;
                    }
                    String submitter = null;
                    if (this.message instanceof SMTPMessage) {
                        submitter = ((SMTPMessage)this.message).getSubmitter();
                    }
                    if ((s = submitter) == null) {
                        s = this.session.getProperty("mail." + this.name + ".submitter");
                    }
                    string = (Serializable)o;
                    if (s == null) {
                        break Label_0359;
                    }
                    try {
                        string = String.valueOf(o) + " AUTH=" + xtext((String)s);
                        o = null;
                        if (this.message instanceof SMTPMessage) {
                            o = ((SMTPMessage)this.message).getMailExtension();
                        }
                        if ((s = (Serializable)o) == null) {
                            s = this.session.getProperty("mail." + this.name + ".mailextension");
                        }
                        o = string;
                        if (s != null) {
                            o = string;
                            if (((String)s).length() > 0) {
                                o = String.valueOf(string) + " " + (String)s;
                            }
                        }
                        this.issueSendCommand((String)o, 250);
                        return;
                        o = InternetAddress.getLocalAddress(this.session);
                        continue Label_0359_Outer;
                        throw new MessagingException("can't determine local email address");
                    }
                    catch (IllegalArgumentException ex) {
                        string = (Serializable)o;
                        if (this.debug) {
                            this.out.println("DEBUG SMTP: ignoring invalid submitter: " + (String)s + ", Exception: " + ex);
                            string = (Serializable)o;
                        }
                        continue;
                    }
                    break;
                }
            }
            break;
        }
    }
    
    @Override
    protected boolean protocolConnect(final String s, int n, final String s2, final String s3) throws MessagingException {
        final String property = this.session.getProperty("mail." + this.name + ".ehlo");
        final boolean b = property == null || !property.equalsIgnoreCase("false");
        final String property2 = this.session.getProperty("mail." + this.name + ".auth");
        final boolean b2 = property2 != null && property2.equalsIgnoreCase("true");
        if (this.debug) {
            this.out.println("DEBUG SMTP: useEhlo " + b + ", useAuth " + b2);
        }
        if (b2 && (s2 == null || s3 == null)) {
            return false;
        }
        Object property3 = null;
        Label_0633: {
            int n2;
            if ((n2 = n) == -1) {
                property3 = this.session.getProperty("mail." + this.name + ".port");
                if (property3 == null) {
                    break Label_0633;
                }
                n2 = Integer.parseInt((String)property3);
            }
        Label_0263_Outer:
            while (true) {
                Label_0249: {
                    if (s != null) {
                        property3 = s;
                        if (s.length() != 0) {
                            break Label_0249;
                        }
                    }
                    property3 = "localhost";
                }
                boolean ehlo = false;
                Label_0642: {
                    if (this.serverSocket == null) {
                        break Label_0642;
                    }
                    this.openServer();
                    while (true) {
                        if (b) {
                            ehlo = this.ehlo(this.getLocalHost());
                        }
                        if (!ehlo) {
                            this.helo(this.getLocalHost());
                        }
                        if (this.useStartTLS && this.supportsExtension("STARTTLS")) {
                            this.startTLS();
                            this.ehlo(this.getLocalHost());
                        }
                        if ((!b2 && (s2 == null || s3 == null)) || (!this.supportsExtension("AUTH") && !this.supportsExtension("AUTH=LOGIN"))) {
                            return true;
                        }
                        if (this.debug) {
                            this.out.println("DEBUG SMTP: Attempt to authenticate");
                            if (!this.supportsAuthentication("LOGIN") && this.supportsExtension("AUTH=LOGIN")) {
                                this.out.println("DEBUG SMTP: use AUTH=LOGIN hack");
                            }
                        }
                        if (!this.supportsAuthentication("LOGIN") && !this.supportsExtension("AUTH=LOGIN")) {
                            break Label_0633;
                        }
                        n2 = this.simpleCommand("AUTH LOGIN");
                        if ((n = n2) == 530) {
                            this.startTLS();
                            n = this.simpleCommand("AUTH LOGIN");
                        }
                        int n3 = n;
                        int n4 = n;
                        try {
                            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            n3 = n;
                            n4 = n;
                            property3 = new BASE64EncoderStream(byteArrayOutputStream, Integer.MAX_VALUE);
                            n2 = n;
                            if (n == 334) {
                                n3 = n;
                                n4 = n;
                                ((OutputStream)property3).write(ASCIIUtility.getBytes(s2));
                                n3 = n;
                                n4 = n;
                                ((OutputStream)property3).flush();
                                n3 = n;
                                n4 = n;
                                n2 = (n4 = (n3 = this.simpleCommand(byteArrayOutputStream.toByteArray())));
                                byteArrayOutputStream.reset();
                            }
                            if ((n = n2) == 334) {
                                n3 = n2;
                                n4 = n2;
                                ((OutputStream)property3).write(ASCIIUtility.getBytes(s3));
                                n3 = n2;
                                n4 = n2;
                                ((OutputStream)property3).flush();
                                n3 = n2;
                                n4 = n2;
                                n = (n4 = (n3 = this.simpleCommand(byteArrayOutputStream.toByteArray())));
                                byteArrayOutputStream.reset();
                            }
                            if (n != 235) {
                                this.closeConnection();
                                return false;
                            }
                            return true;
                            this.openServer((String)property3, n2);
                            continue;
                            n2 = this.defaultPort;
                            continue Label_0263_Outer;
                        }
                        catch (IOException ex2) {
                            if (n3 != 235) {
                                this.closeConnection();
                                return false;
                            }
                            return true;
                        }
                        finally {
                            if (n4 != 235) {
                                this.closeConnection();
                                return false;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        if (this.supportsAuthentication("PLAIN")) {
            final int simpleCommand = this.simpleCommand("AUTH PLAIN");
            try {
                final ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                final BASE64EncoderStream base64EncoderStream = new BASE64EncoderStream(byteArrayOutputStream2, Integer.MAX_VALUE);
                n = simpleCommand;
                if (simpleCommand == 334) {
                    base64EncoderStream.write(0);
                    base64EncoderStream.write(ASCIIUtility.getBytes(s2));
                    base64EncoderStream.write(0);
                    base64EncoderStream.write(ASCIIUtility.getBytes(s3));
                    base64EncoderStream.flush();
                    n = this.simpleCommand(byteArrayOutputStream2.toByteArray());
                }
                if (n != 235) {
                    this.closeConnection();
                    return false;
                }
                return true;
            }
            catch (IOException ex3) {
                if (simpleCommand != 235) {
                    this.closeConnection();
                    return false;
                }
                return true;
            }
            finally {
                if (simpleCommand != 235) {
                    this.closeConnection();
                    return false;
                }
            }
        }
        if (this.supportsAuthentication("DIGEST-MD5")) {
            final DigestMD5 md5 = this.getMD5();
            if (md5 != null) {
                int simpleCommand2 = this.simpleCommand("AUTH DIGEST-MD5");
                Label_0926: {
                    if ((n = simpleCommand2) != 334) {
                        break Label_0926;
                    }
                    n = simpleCommand2;
                    try {
                        final int n5 = n = this.simpleCommand(md5.authClient((String)property3, s2, s3, this.getSASLRealm(), this.lastServerResponse));
                        if (n5 == 334) {
                            n = n5;
                            simpleCommand2 = n5;
                            if (!md5.authServer(this.lastServerResponse)) {
                                n = -1;
                            }
                            else {
                                n = n5;
                                simpleCommand2 = n5;
                                n = this.simpleCommand(new byte[0]);
                            }
                        }
                        if (n != 235) {
                            this.closeConnection();
                            return false;
                        }
                    }
                    catch (Exception ex) {
                        simpleCommand2 = n;
                        if (this.debug) {
                            simpleCommand2 = n;
                            this.out.println("DEBUG SMTP: DIGEST-MD5: " + ex);
                        }
                        if (n != 235) {
                            this.closeConnection();
                            return false;
                        }
                    }
                    finally {
                        if (simpleCommand2 != 235) {
                            this.closeConnection();
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    
    protected void rcptTo() throws MessagingException {
        final Vector<InternetAddress> vector = new Vector<InternetAddress>();
        final Vector<InternetAddress> vector2 = new Vector<InternetAddress>();
        final Vector<InternetAddress> vector3 = new Vector<InternetAddress>();
        Serializable exception = null;
        int n = 0;
        this.invalidAddr = null;
        this.validUnsentAddr = null;
        this.validSentAddr = null;
        boolean sendPartial = false;
        if (this.message instanceof SMTPMessage) {
            sendPartial = ((SMTPMessage)this.message).getSendPartial();
        }
        int n2 = sendPartial ? 1 : 0;
        Label_0581: {
            if (!sendPartial) {
                final Serializable s = this.session.getProperty("mail." + this.name + ".sendpartial");
                if (s == null || !((String)s).equalsIgnoreCase("true")) {
                    break Label_0581;
                }
                n2 = 1;
            }
        Label_0264_Outer:
            while (true) {
                if (this.debug && n2 != 0) {
                    this.out.println("DEBUG SMTP: sendPartial set");
                }
                int n3 = 0;
                String s2 = null;
                Serializable s = null;
                int n4 = n3;
                if (this.supportsExtension("DSN")) {
                    String dsnNotify = (String)s;
                    if (this.message instanceof SMTPMessage) {
                        dsnNotify = ((SMTPMessage)this.message).getDSNNotify();
                    }
                    if ((s = dsnNotify) == null) {
                        s = this.session.getProperty("mail." + this.name + ".dsn.notify");
                    }
                    n4 = n3;
                    if ((s2 = (String)s) != null) {
                        n4 = 1;
                        s2 = (String)s;
                    }
                }
                int n5 = 0;
            Label_0340_Outer:
                while (true) {
                    Label_0587: {
                        if (n5 < this.addresses.length) {
                            break Label_0587;
                        }
                        n3 = n;
                        if (n2 != 0) {
                            n3 = n;
                            if (vector.size() == 0) {
                                n3 = 1;
                            }
                        }
                        Label_1228: {
                            if (n3 == 0) {
                                break Label_1228;
                            }
                            vector3.copyInto(this.invalidAddr = new Address[vector3.size()]);
                            this.validUnsentAddr = new Address[vector.size() + vector2.size()];
                            n = 0;
                            n4 = 0;
                        Label_0351_Outer:
                            while (true) {
                                Label_1176: {
                                    if (n4 < vector.size()) {
                                        break Label_1176;
                                    }
                                    n4 = 0;
                                Label_0360_Outer:
                                    while (true) {
                                        Label_1202: {
                                            if (n4 < vector2.size()) {
                                                break Label_1202;
                                            }
                                        Label_0394_Outer:
                                            while (true) {
                                            Label_0430_Outer:
                                                while (true) {
                                                Label_0466_Outer:
                                                    while (true) {
                                                        InternetAddress internetAddress;
                                                        String s3;
                                                        int serverResponse;
                                                        Label_0832_Outer:Block_40_Outer:
                                                        while (true) {
                                                            Label_1416: {
                                                                Label_0475: {
                                                                    if (!this.debug) {
                                                                        break Label_0475;
                                                                    }
                                                                    Label_0403: {
                                                                        if (this.validSentAddr == null || this.validSentAddr.length <= 0) {
                                                                            break Label_0403;
                                                                        }
                                                                        this.out.println("DEBUG SMTP: Verified Addresses");
                                                                        n = 0;
                                                                        if (n < this.validSentAddr.length) {
                                                                            break Label_0466_Outer;
                                                                        }
                                                                    }
                                                                    Label_0439: {
                                                                        if (this.validUnsentAddr == null || this.validUnsentAddr.length <= 0) {
                                                                            break Label_0439;
                                                                        }
                                                                        this.out.println("DEBUG SMTP: Valid Unsent Addresses");
                                                                        n = 0;
                                                                        if (n < this.validUnsentAddr.length) {
                                                                            break Label_0832_Outer;
                                                                        }
                                                                    }
                                                                    if (this.invalidAddr == null || this.invalidAddr.length <= 0) {
                                                                        break Label_0475;
                                                                    }
                                                                    this.out.println("DEBUG SMTP: Invalid Addresses");
                                                                    n = 0;
                                                                    if (n < this.invalidAddr.length) {
                                                                        break Label_1416;
                                                                    }
                                                                }
                                                                if (n3 == 0) {
                                                                    return;
                                                                }
                                                                if (this.debug) {
                                                                    this.out.println("DEBUG SMTP: Sending failed because of invalid destination addresses");
                                                                }
                                                                this.notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                                                                s = this.lastServerResponse;
                                                                n = this.lastReturnCode;
                                                                try {
                                                                    if (this.serverSocket != null) {
                                                                        this.issueCommand("RSET", 250);
                                                                    }
                                                                    this.lastServerResponse = (String)s;
                                                                    this.lastReturnCode = n;
                                                                    throw new SendFailedException("Invalid Addresses", (Exception)exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
                                                                    internetAddress = (InternetAddress)this.addresses[n5];
                                                                    s = (s3 = "RCPT TO:" + this.normalizeAddress(internetAddress.getAddress()));
                                                                    // iftrue(Label_0662:, n4 == 0)
                                                                    // iftrue(Label_0832:, !this.reportSuccess)
                                                                    // iftrue(Label_0980:, n2 != 0)
                                                                    // iftrue(Label_1016:, exception != null)
                                                                    // iftrue(Label_1159:, exception != null)
                                                                    // iftrue(Label_0896:, exception != null)
                                                                    // iftrue(Label_1104:, !this.debug)
                                                                    // iftrue(Label_0806:, n2 != 0)
                                                                    // iftrue(Label_1333:, !this.reportSuccess && n2 == 0 || vector3.size() <= 0 && vector2.size() <= 0)
                                                                    // iftrue(Label_0920:, n2 != 0)
                                                                    // iftrue(Label_1132:, this.serverSocket == null)
                                                                    // switch([Lcom.strobel.decompiler.ast.Label;@48f2793, serverResponse)
                                                                    // iftrue(Label_1033:, serverResponse < 400 || serverResponse > 499)
                                                                    // iftrue(Label_0956:, exception != null)
                                                                Label_1132_Outer:
                                                                    while (true) {
                                                                        while (true) {
                                                                            Label_0980_Outer:Block_36_Outer:
                                                                            while (true) {
                                                                                while (true) {
                                                                                    Label_0920: {
                                                                                        while (true) {
                                                                                            while (true) {
                                                                                                Block_37: {
                                                                                                    Label_0662: {
                                                                                                        Label_1104: {
                                                                                                        Label_0799:
                                                                                                            while (true) {
                                                                                                            Block_31_Outer:
                                                                                                                while (true) {
                                                                                                                Block_41_Outer:
                                                                                                                    while (true) {
                                                                                                                        while (true) {
                                                                                                                            while (true) {
                                                                                                                                Block_33: {
                                                                                                                                    Block_28: {
                                                                                                                                        break Block_28;
                                                                                                                                        Label_0847: {
                                                                                                                                            vector.addElement(internetAddress);
                                                                                                                                        }
                                                                                                                                        s = exception;
                                                                                                                                        n3 = n;
                                                                                                                                        break Block_33;
                                                                                                                                        Label_0973:
                                                                                                                                        break Block_37;
                                                                                                                                        this.validUnsentAddr[n] = vector2.elementAt(n4);
                                                                                                                                        ++n4;
                                                                                                                                        ++n;
                                                                                                                                        continue Label_0360_Outer;
                                                                                                                                    }
                                                                                                                                    s3 = String.valueOf(s) + " NOTIFY=" + s2;
                                                                                                                                    break Label_0662;
                                                                                                                                    Label_0956: {
                                                                                                                                        ((MessagingException)exception).setNextException((Exception)s);
                                                                                                                                    }
                                                                                                                                    s = exception;
                                                                                                                                    n3 = n;
                                                                                                                                    break Label_0832;
                                                                                                                                    this.sendPartiallyFailed = true;
                                                                                                                                    this.exception = (MessagingException)exception;
                                                                                                                                    vector3.copyInto(this.invalidAddr = new Address[vector3.size()]);
                                                                                                                                    vector2.copyInto(this.validUnsentAddr = new Address[vector2.size()]);
                                                                                                                                    vector.copyInto(this.validSentAddr = new Address[vector.size()]);
                                                                                                                                    continue Label_0394_Outer;
                                                                                                                                    n = 1;
                                                                                                                                    Label_0806: {
                                                                                                                                        break Label_0806;
                                                                                                                                        Label_1333:
                                                                                                                                        this.validSentAddr = this.addresses;
                                                                                                                                        continue Label_0394_Outer;
                                                                                                                                        vector2.addElement(internetAddress);
                                                                                                                                        s = new SMTPAddressFailedException(internetAddress, s3, serverResponse, this.lastServerResponse);
                                                                                                                                        n3 = n;
                                                                                                                                        break Label_0832;
                                                                                                                                        Label_1016:
                                                                                                                                        ((MessagingException)exception).setNextException((Exception)s);
                                                                                                                                        s = exception;
                                                                                                                                        n3 = n;
                                                                                                                                        break Label_0832;
                                                                                                                                    }
                                                                                                                                    s = new SMTPAddressFailedException(internetAddress, s3, serverResponse, this.lastServerResponse);
                                                                                                                                    break Label_0799;
                                                                                                                                    n3 = n;
                                                                                                                                    break Label_0832;
                                                                                                                                    n3 = n;
                                                                                                                                    break Label_0832;
                                                                                                                                    this.out.println("DEBUG SMTP: got response code " + serverResponse + ", with response: " + this.lastServerResponse);
                                                                                                                                    break Label_1104;
                                                                                                                                }
                                                                                                                                s = new SMTPAddressSucceededException(internetAddress, s3, serverResponse, this.lastServerResponse);
                                                                                                                                continue Block_41_Outer;
                                                                                                                            }
                                                                                                                            vector3.addElement(internetAddress);
                                                                                                                            break Label_0799;
                                                                                                                            Label_1059: {
                                                                                                                                continue Block_40_Outer;
                                                                                                                            }
                                                                                                                        }
                                                                                                                        this.out.println("DEBUG SMTP:   " + this.validUnsentAddr[n]);
                                                                                                                        ++n;
                                                                                                                        continue Label_0466_Outer;
                                                                                                                        continue Label_0980_Outer;
                                                                                                                    }
                                                                                                                    this.out.println("DEBUG SMTP:   " + this.validSentAddr[n]);
                                                                                                                    ++n;
                                                                                                                    continue Label_0430_Outer;
                                                                                                                    continue Block_31_Outer;
                                                                                                                }
                                                                                                                vector2.addElement(internetAddress);
                                                                                                                continue Label_0799;
                                                                                                            }
                                                                                                            n3 = n;
                                                                                                            break Label_0832;
                                                                                                            while (true) {
                                                                                                                n = 1;
                                                                                                                break Label_0920;
                                                                                                                Label_0913: {
                                                                                                                    continue Label_1132_Outer;
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        s = this.lastServerResponse;
                                                                                                        n = this.lastReturnCode;
                                                                                                        break Label_0980_Outer;
                                                                                                    }
                                                                                                    this.sendCommand(s3);
                                                                                                    serverResponse = this.readServerResponse();
                                                                                                }
                                                                                                n = 1;
                                                                                                continue Block_36_Outer;
                                                                                            }
                                                                                            Label_1159: {
                                                                                                ((MessagingException)exception).setNextException((Exception)s);
                                                                                            }
                                                                                            s = exception;
                                                                                            n3 = n;
                                                                                            break Label_0832;
                                                                                            this.out.println("DEBUG SMTP:   " + this.invalidAddr[n]);
                                                                                            ++n;
                                                                                            continue Label_0832_Outer;
                                                                                            Label_0776:
                                                                                            continue Label_1132_Outer;
                                                                                        }
                                                                                        this.lastServerResponse = (String)s;
                                                                                        this.lastReturnCode = n;
                                                                                        throw new SMTPAddressFailedException(internetAddress, s3, serverResponse, (String)s);
                                                                                    }
                                                                                    vector3.addElement(internetAddress);
                                                                                    s = new SMTPAddressFailedException(internetAddress, s3, serverResponse, this.lastServerResponse);
                                                                                    continue Block_40_Outer;
                                                                                }
                                                                                ++n5;
                                                                                exception = s;
                                                                                n = n3;
                                                                                continue Label_0340_Outer;
                                                                                this.validUnsentAddr[n] = vector.elementAt(n4);
                                                                                ++n4;
                                                                                ++n;
                                                                                continue Label_0351_Outer;
                                                                                Label_0896: {
                                                                                    ((MessagingException)exception).setNextException((Exception)s);
                                                                                }
                                                                                s = exception;
                                                                                n3 = n;
                                                                                continue Label_0980_Outer;
                                                                            }
                                                                            this.issueCommand("RSET", 250);
                                                                            continue;
                                                                        }
                                                                        n2 = 0;
                                                                        continue Label_0264_Outer;
                                                                        Label_1033: {
                                                                            continue Label_1132_Outer;
                                                                        }
                                                                    }
                                                                }
                                                                // iftrue(Label_1059:, serverResponse < 500 || serverResponse > 599)
                                                                catch (MessagingException ex2) {
                                                                    while (true) {
                                                                        try {
                                                                            this.close();
                                                                            this.lastServerResponse = (String)s;
                                                                            this.lastReturnCode = n;
                                                                        }
                                                                        catch (MessagingException ex) {
                                                                            if (this.debug) {
                                                                                ex.printStackTrace(this.out);
                                                                            }
                                                                            continue;
                                                                        }
                                                                        break;
                                                                    }
                                                                }
                                                                finally {
                                                                    this.lastServerResponse = (String)s;
                                                                    this.lastReturnCode = n;
                                                                }
                                                            }
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    break;
                                                }
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
                break;
            }
        }
    }
    
    protected int readServerResponse() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            21
        //     6: aload_0        
        //     7: invokestatic    java/lang/Thread.holdsLock:(Ljava/lang/Object;)Z
        //    10: ifne            21
        //    13: new             Ljava/lang/AssertionError;
        //    16: dup            
        //    17: invokespecial   java/lang/AssertionError.<init>:()V
        //    20: athrow         
        //    21: new             Ljava/lang/StringBuffer;
        //    24: dup            
        //    25: bipush          100
        //    27: invokespecial   java/lang/StringBuffer.<init>:(I)V
        //    30: astore_2       
        //    31: aload_0        
        //    32: getfield        com/sun/mail/smtp/SMTPTransport.lineInputStream:Lcom/sun/mail/util/LineInputStream;
        //    35: invokevirtual   com/sun/mail/util/LineInputStream.readLine:()Ljava/lang/String;
        //    38: astore_3       
        //    39: aload_3        
        //    40: ifnonnull       104
        //    43: aload_2        
        //    44: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    47: astore_3       
        //    48: aload_3        
        //    49: astore_2       
        //    50: aload_3        
        //    51: invokevirtual   java/lang/String.length:()I
        //    54: ifne            61
        //    57: ldc_w           "[EOF]"
        //    60: astore_2       
        //    61: aload_0        
        //    62: aload_2        
        //    63: putfield        com/sun/mail/smtp/SMTPTransport.lastServerResponse:Ljava/lang/String;
        //    66: aload_0        
        //    67: iconst_m1      
        //    68: putfield        com/sun/mail/smtp/SMTPTransport.lastReturnCode:I
        //    71: aload_0        
        //    72: getfield        com/sun/mail/smtp/SMTPTransport.debug:Z
        //    75: ifeq            320
        //    78: aload_0        
        //    79: getfield        com/sun/mail/smtp/SMTPTransport.out:Ljava/io/PrintStream;
        //    82: new             Ljava/lang/StringBuilder;
        //    85: dup            
        //    86: ldc_w           "DEBUG SMTP: EOF: "
        //    89: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    92: aload_2        
        //    93: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    96: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    99: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   102: iconst_m1      
        //   103: ireturn        
        //   104: aload_2        
        //   105: aload_3        
        //   106: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   109: pop            
        //   110: aload_2        
        //   111: ldc_w           "\n"
        //   114: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   117: pop            
        //   118: aload_0        
        //   119: aload_3        
        //   120: invokespecial   com/sun/mail/smtp/SMTPTransport.isNotLastLine:(Ljava/lang/String;)Z
        //   123: ifne            31
        //   126: aload_2        
        //   127: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   130: astore_2       
        //   131: aload_2        
        //   132: ifnull          315
        //   135: aload_2        
        //   136: invokevirtual   java/lang/String.length:()I
        //   139: iconst_3       
        //   140: if_icmplt       315
        //   143: aload_2        
        //   144: iconst_0       
        //   145: iconst_3       
        //   146: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   149: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   152: istore_1       
        //   153: iload_1        
        //   154: iconst_m1      
        //   155: if_icmpne       189
        //   158: aload_0        
        //   159: getfield        com/sun/mail/smtp/SMTPTransport.debug:Z
        //   162: ifeq            189
        //   165: aload_0        
        //   166: getfield        com/sun/mail/smtp/SMTPTransport.out:Ljava/io/PrintStream;
        //   169: new             Ljava/lang/StringBuilder;
        //   172: dup            
        //   173: ldc_w           "DEBUG SMTP: bad server response: "
        //   176: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   179: aload_2        
        //   180: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   183: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   186: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   189: aload_0        
        //   190: aload_2        
        //   191: putfield        com/sun/mail/smtp/SMTPTransport.lastServerResponse:Ljava/lang/String;
        //   194: aload_0        
        //   195: iload_1        
        //   196: putfield        com/sun/mail/smtp/SMTPTransport.lastReturnCode:I
        //   199: iload_1        
        //   200: ireturn        
        //   201: astore_2       
        //   202: aload_0        
        //   203: getfield        com/sun/mail/smtp/SMTPTransport.debug:Z
        //   206: ifeq            233
        //   209: aload_0        
        //   210: getfield        com/sun/mail/smtp/SMTPTransport.out:Ljava/io/PrintStream;
        //   213: new             Ljava/lang/StringBuilder;
        //   216: dup            
        //   217: ldc_w           "DEBUG SMTP: exception reading response: "
        //   220: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   223: aload_2        
        //   224: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   227: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   230: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   233: aload_0        
        //   234: ldc_w           ""
        //   237: putfield        com/sun/mail/smtp/SMTPTransport.lastServerResponse:Ljava/lang/String;
        //   240: aload_0        
        //   241: iconst_0       
        //   242: putfield        com/sun/mail/smtp/SMTPTransport.lastReturnCode:I
        //   245: new             Ljavax/mail/MessagingException;
        //   248: dup            
        //   249: ldc_w           "Exception reading response"
        //   252: aload_2        
        //   253: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   256: athrow         
        //   257: astore_3       
        //   258: aload_0        
        //   259: invokevirtual   com/sun/mail/smtp/SMTPTransport.close:()V
        //   262: iconst_m1      
        //   263: istore_1       
        //   264: goto            153
        //   267: astore_3       
        //   268: aload_0        
        //   269: getfield        com/sun/mail/smtp/SMTPTransport.debug:Z
        //   272: ifeq            262
        //   275: aload_3        
        //   276: aload_0        
        //   277: getfield        com/sun/mail/smtp/SMTPTransport.out:Ljava/io/PrintStream;
        //   280: invokevirtual   javax/mail/MessagingException.printStackTrace:(Ljava/io/PrintStream;)V
        //   283: goto            262
        //   286: astore_3       
        //   287: aload_0        
        //   288: invokevirtual   com/sun/mail/smtp/SMTPTransport.close:()V
        //   291: iconst_m1      
        //   292: istore_1       
        //   293: goto            153
        //   296: astore_3       
        //   297: aload_0        
        //   298: getfield        com/sun/mail/smtp/SMTPTransport.debug:Z
        //   301: ifeq            291
        //   304: aload_3        
        //   305: aload_0        
        //   306: getfield        com/sun/mail/smtp/SMTPTransport.out:Ljava/io/PrintStream;
        //   309: invokevirtual   javax/mail/MessagingException.printStackTrace:(Ljava/io/PrintStream;)V
        //   312: goto            291
        //   315: iconst_m1      
        //   316: istore_1       
        //   317: goto            153
        //   320: iconst_m1      
        //   321: ireturn        
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                       
        //  -----  -----  -----  -----  -------------------------------------------
        //  31     39     201    257    Ljava/io/IOException;
        //  43     48     201    257    Ljava/io/IOException;
        //  50     57     201    257    Ljava/io/IOException;
        //  61     102    201    257    Ljava/io/IOException;
        //  104    131    201    257    Ljava/io/IOException;
        //  143    153    257    286    Ljava/lang/NumberFormatException;
        //  143    153    286    315    Ljava/lang/StringIndexOutOfBoundsException;
        //  258    262    267    286    Ljavax/mail/MessagingException;
        //  287    291    296    315    Ljavax/mail/MessagingException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0153:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void sendCommand(final String s) throws MessagingException {
        this.sendCommand(ASCIIUtility.getBytes(s));
    }
    
    @Override
    public void sendMessage(final Message ex, final Address[] array) throws MessagingException, SendFailedException {
        synchronized (this) {
            this.checkConnected();
            if (!(ex instanceof MimeMessage)) {
                if (this.debug) {
                    this.out.println("DEBUG SMTP: Can only send RFC822 msgs");
                }
                throw new MessagingException("SMTP can only send RFC822 messages");
            }
        }
        if (0 < array.length) {
            goto Label_0405;
        }
        final SMTPMessage smtpMessage;
        this.message = smtpMessage;
        this.addresses = array;
        this.validUnsentAddr = array;
        this.expandGroups();
        boolean allow8bitMIME = false;
        if (smtpMessage instanceof SMTPMessage) {
            allow8bitMIME = smtpMessage.getAllow8bitMIME();
        }
        boolean b = allow8bitMIME;
        if (!allow8bitMIME) {
            final String property = this.session.getProperty("mail." + this.name + ".allow8bitmime");
            if (property == null || !property.equalsIgnoreCase("true")) {
                goto Label_0451;
            }
            b = true;
        }
        if (this.debug) {
            this.out.println("DEBUG SMTP: use8bit " + b);
        }
        while (true) {
            if (!b || !this.supportsExtension("8BITMIME") || !this.convertTo8Bit(this.message)) {
                break Label_0222;
            }
            try {
                this.message.saveChanges();
                try {
                    this.mailFrom();
                    this.rcptTo();
                    this.message.writeTo(this.data(), SMTPTransport.ignoreList);
                    this.finishData();
                    if (this.sendPartiallyFailed) {
                        if (this.debug) {
                            this.out.println("DEBUG SMTP: Sending partially failed because of invalid destination addresses");
                        }
                        this.notifyTransportListeners(3, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                        throw new SMTPSendFailedException(".", this.lastReturnCode, this.lastServerResponse, this.exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
                    }
                    goto Label_0457;
                }
                catch (MessagingException ex2) {
                    try {
                        if (this.debug) {
                            ex2.printStackTrace(this.out);
                        }
                        this.notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                        throw ex2;
                    }
                    finally {
                        this.invalidAddr = null;
                        this.validUnsentAddr = null;
                        this.validSentAddr = null;
                        this.addresses = null;
                        this.message = null;
                        this.exception = null;
                        this.sendPartiallyFailed = false;
                    }
                }
                catch (IOException ex) {
                    if (this.debug) {
                        ex.printStackTrace(this.out);
                    }
                    try {
                        this.closeConnection();
                        this.notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                        throw new MessagingException("IOException while sending message", ex);
                    }
                    catch (MessagingException ex3) {}
                }
            }
            catch (MessagingException ex4) {
                continue;
            }
            break;
        }
    }
    
    public void setLocalHost(final String localHostName) {
        synchronized (this) {
            this.localHostName = localHostName;
        }
    }
    
    public void setReportSuccess(final boolean reportSuccess) {
        synchronized (this) {
            this.reportSuccess = reportSuccess;
        }
    }
    
    public void setSASLRealm(final String saslRealm) {
        synchronized (this) {
            this.saslRealm = saslRealm;
        }
    }
    
    public void setStartTLS(final boolean useStartTLS) {
        synchronized (this) {
            this.useStartTLS = useStartTLS;
        }
    }
    
    public void setUseRset(final boolean useRset) {
        synchronized (this) {
            this.useRset = useRset;
        }
    }
    
    public int simpleCommand(final String s) throws MessagingException {
        synchronized (this) {
            this.sendCommand(s);
            return this.readServerResponse();
        }
    }
    
    protected int simpleCommand(final byte[] array) throws MessagingException {
        assert Thread.holdsLock(this);
        this.sendCommand(array);
        return this.readServerResponse();
    }
    
    protected void startTLS() throws MessagingException {
        this.issueCommand("STARTTLS", 220);
        try {
            this.serverSocket = SocketFetcher.startTLS(this.serverSocket, this.session.getProperties(), "mail." + this.name);
            this.initStreams();
        }
        catch (IOException ex) {
            this.closeConnection();
            throw new MessagingException("Could not convert socket to TLS", ex);
        }
    }
    
    protected boolean supportsAuthentication(final String s) {
        assert Thread.holdsLock(this);
        if (this.extMap != null) {
            final String s2 = this.extMap.get("AUTH");
            if (s2 != null) {
                final StringTokenizer stringTokenizer = new StringTokenizer(s2);
                while (stringTokenizer.hasMoreTokens()) {
                    if (stringTokenizer.nextToken().equalsIgnoreCase(s)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public boolean supportsExtension(final String s) {
        return this.extMap != null && this.extMap.get(s.toUpperCase(Locale.ENGLISH)) != null;
    }
}
