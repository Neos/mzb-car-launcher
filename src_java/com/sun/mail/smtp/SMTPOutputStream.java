package com.sun.mail.smtp;

import com.sun.mail.util.*;
import java.io.*;

public class SMTPOutputStream extends CRLFOutputStream
{
    public SMTPOutputStream(final OutputStream outputStream) {
        super(outputStream);
    }
    
    public void ensureAtBOL() throws IOException {
        if (!this.atBOL) {
            super.writeln();
        }
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public void write(final int n) throws IOException {
        if ((this.lastb == 10 || this.lastb == 13 || this.lastb == -1) && n == 46) {
            this.out.write(46);
        }
        super.write(n);
    }
    
    @Override
    public void write(final byte[] array, int i, int n) throws IOException {
        int lastb;
        if (this.lastb == -1) {
            lastb = 10;
        }
        else {
            lastb = this.lastb;
        }
        final int n2 = i;
        final int n3 = n + i;
        n = n2;
        int n4 = lastb;
        while (i < n3) {
            int n5 = 0;
            Label_0109: {
                if (n4 != 10) {
                    n5 = n;
                    if (n4 != 13) {
                        break Label_0109;
                    }
                }
                n5 = n;
                if (array[i] == 46) {
                    super.write(array, n, i - n);
                    this.out.write(46);
                    n5 = i;
                }
            }
            n4 = array[i];
            ++i;
            n = n5;
        }
        if (n3 - n > 0) {
            super.write(array, n, n3 - n);
        }
    }
}
