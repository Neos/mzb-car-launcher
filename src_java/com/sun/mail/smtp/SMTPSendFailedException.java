package com.sun.mail.smtp;

import javax.mail.internet.*;
import javax.mail.*;

public class SMTPSendFailedException extends SendFailedException
{
    private static final long serialVersionUID = 8049122628728932894L;
    protected InternetAddress addr;
    protected String cmd;
    protected int rc;
    
    public SMTPSendFailedException(final String cmd, final int rc, final String s, final Exception ex, final Address[] array, final Address[] array2, final Address[] array3) {
        super(s, ex, array, array2, array3);
        this.cmd = cmd;
        this.rc = rc;
    }
    
    public String getCommand() {
        return this.cmd;
    }
    
    public int getReturnCode() {
        return this.rc;
    }
}
