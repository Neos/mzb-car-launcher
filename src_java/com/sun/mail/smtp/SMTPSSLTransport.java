package com.sun.mail.smtp;

import javax.mail.*;

public class SMTPSSLTransport extends SMTPTransport
{
    public SMTPSSLTransport(final Session session, final URLName urlName) {
        super(session, urlName, "smtps", 465, true);
    }
}
