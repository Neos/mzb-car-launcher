package com.unibroad.c2py;

import java.util.*;
import java.io.*;

public class Parser
{
    private static boolean sInitialized = false;
    private static final String sName = "unicode_to_hanyu_pinyin.txt";
    protected static Properties sProp;
    public static Map<String, String> specialHanzi;
    
    static {
        Parser.sInitialized = false;
        (Parser.specialHanzi = new HashMap<String, String>()).put("\u8584", "\u640f");
        Parser.specialHanzi.put("\u79d8", "\u95ed");
        Parser.specialHanzi.put("\u7987", "\u695a");
        Parser.specialHanzi.put("\u91cd", "\u5d07");
        Parser.specialHanzi.put("\u76d6", "\u8238");
        Parser.specialHanzi.put("\u8d3e", "\u7532");
        Parser.specialHanzi.put("\u89e3", "\u8c22");
        Parser.specialHanzi.put("\u7f2a", "\u5999");
        Parser.specialHanzi.put("\u90a3", "\u7eb3");
        Parser.specialHanzi.put("\u533a", "\u6b27");
        Parser.specialHanzi.put("\u8983", "\u52e4");
        Parser.specialHanzi.put("\u77bf", "\u6b0b");
        Parser.specialHanzi.put("\u4ec7", "\u7403");
        Parser.specialHanzi.put("\u76db", "\u5269");
        Parser.specialHanzi.put("\u77f3", "\u65f6");
        Parser.specialHanzi.put("\u6c88", "\u5ba1");
        Parser.specialHanzi.put("\u6298", "\u86c7");
        Parser.specialHanzi.put("\u5355", "\u5584");
        Parser.specialHanzi.put("\u5854", "\u5b83");
        Parser.specialHanzi.put("\u590f", "\u4e0b");
        Parser.specialHanzi.put("\u51bc", "\u9669");
        Parser.specialHanzi.put("\u4fde", "\u4e8e");
        Parser.specialHanzi.put("\u65bc", "\u6de4");
        Parser.specialHanzi.put("\u66fe", "\u589e");
        Parser.specialHanzi.put("\u67e5", "\u5412");
        Parser.specialHanzi.put("\u85cf", "\u846c");
        Parser.specialHanzi.put("\u7fdf", "\u5b85");
        Parser.specialHanzi.put("\u4e07\u4fdf", "\u83ab\u5947");
        Parser.specialHanzi.put("\u5c09\u8fdf", "\u7389\u8fdf");
    }
    
    public static final String getAllPinyin(final String s) {
        if (s == null || "".equals(s)) {
            return "#";
        }
        return getPinyin(isPolyphone(s));
    }
    
    public static final String getPinyin(final char c) {
        if (c >= '\0' && c < '\u0100') {
            return String.valueOf(c).toLowerCase(Locale.US);
        }
        while (true) {
            if (!Parser.sInitialized) {
                String property;
                try {
                    initialize();
                    property = Parser.sProp.getProperty(Integer.toHexString(c).toUpperCase(Locale.US));
                    if (property == null) {
                        return "#";
                    }
                }
                catch (IOException ex) {
                    System.err.println("PinyinParser initialize error: " + ex.getMessage());
                    continue;
                }
                return property.split(",")[0];
            }
            continue;
        }
    }
    
    public static final String getPinyin(final String s) {
        if (s == null || "".equals(s)) {
            return "#";
        }
        final char[] charArray = s.toCharArray();
        final StringBuilder sb = new StringBuilder();
        for (int length = charArray.length, i = 0; i < length; ++i) {
            sb.append(getPinyin(charArray[i]));
        }
        return sb.toString();
    }
    
    public static final String getPinyinAllFirstLetters(final String s) {
        if (s == null || "".equals(s)) {
            return "#";
        }
        final char[] charArray = s.toCharArray();
        final StringBuilder sb = new StringBuilder();
        for (int length = charArray.length, i = 0; i < length; ++i) {
            sb.append(getPinyinFirstLetter(charArray[i]));
        }
        return sb.toString();
    }
    
    public static final String getPinyinFirstLetter(final char c) {
        return String.valueOf(getPinyin(c).charAt(0));
    }
    
    public static final String getPinyinFirstLetter(final String s) {
        if (s == null || "".equals(s)) {
            return "#";
        }
        return getPinyinFirstLetter(s.charAt(0));
    }
    
    public static final String getPinyinFirstWord(final String s) {
        if (s == null || "".equals(s)) {
            return "#";
        }
        return getPinyin(s.charAt(0));
    }
    
    public static final String getPinyinSecondWords(final String s) {
        String pinyin;
        if (s == null || "".equals(s)) {
            pinyin = "#";
        }
        else {
            pinyin = getPinyin(s.charAt(0));
            if (s.length() > 1) {
                return String.valueOf(pinyin) + getPinyin(s.charAt(1));
            }
        }
        return pinyin;
    }
    
    public static void initialize() throws IOException {
        if (Parser.sInitialized) {
            return;
        }
        synchronized (Parser.class) {
            if (Parser.sInitialized) {
                return;
            }
            Parser.sProp = new Properties();
            final InputStream resourceAsStream = Parser.class.getResourceAsStream("unicode_to_hanyu_pinyin.txt");
            try {
                Parser.sProp.load(resourceAsStream);
                if (resourceAsStream != null) {
                    resourceAsStream.close();
                }
                Parser.sInitialized = true;
            }
            catch (Exception ex) {
                throw new IOException(ex);
            }
            finally {
                if (resourceAsStream != null) {
                    resourceAsStream.close();
                }
            }
        }
    }
    
    private static String isPolyphone(String s) {
        final String substring = s.substring(0, 1);
        Label_0082: {
            if (s.length() <= 2) {
                break Label_0082;
            }
            final String substring2 = s.substring(0, 2);
            if (!"\u4e07\u4fdf".equals(substring2) && !"\u5c09\u8fdf".equals(substring2)) {
                break Label_0082;
            }
            s = s.substring(2, s.length());
            return String.valueOf(Parser.specialHanzi.get(substring2)) + s;
        }
        final String string = s;
        if (Parser.specialHanzi.get(substring) != null) {
            s = s.substring(1, s.length());
            return String.valueOf(Parser.specialHanzi.get(substring)) + s;
        }
        return string;
    }
    
    public static final void release() {
        if (Parser.sInitialized) {
            synchronized (Parser.class) {
                if (Parser.sInitialized) {
                    if (Parser.sProp != null) {
                        Parser.sProp.clear();
                        Parser.sProp = null;
                    }
                    Parser.sInitialized = false;
                }
            }
        }
    }
}
