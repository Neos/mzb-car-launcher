package com.backaudio.android.driver;

import java.io.*;
import java.util.*;

public class LibSerialPort
{
    private static List<File> openedSerialPort;
    private FileDescriptor mFd;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;
    
    static {
        System.loadLibrary("SerialPort");
        LibSerialPort.openedSerialPort = new ArrayList<File>();
    }
    
    public LibSerialPort(final File file, int n, final int n2) throws Exception {
        final int n3 = 0;
        Label_0134: {
            if (!LibSerialPort.openedSerialPort.contains(file)) {
                // monitorexit(LibSerialPort.class)
                while (true) {
                    Label_0148: {
                        synchronized (LibSerialPort.class) {
                            if (LibSerialPort.openedSerialPort.contains(file)) {
                                break Label_0148;
                            }
                            this.mFd = this.open(file.getAbsolutePath(), n, n2);
                            if (this.mFd == null) {
                                throw new Exception("cannot open serialport:" + file.getAbsolutePath());
                            }
                        }
                        this.mFileInputStream = new FileInputStream(this.mFd);
                        this.mFileOutputStream = new FileOutputStream(this.mFd);
                        final File file2;
                        LibSerialPort.openedSerialPort.add(file2);
                        n = n3;
                        break Label_0134;
                    }
                    n = 1;
                    continue;
                }
            }
            n = 1;
        }
        if (n != 0) {
            throw new Exception("serialport is already opened! cannot open now");
        }
    }
    
    private native FileDescriptor open(final String p0, final int p1, final int p2);
    
    public native void close(final FileDescriptor p0);
    
    public FileInputStream getmFileInputStream() {
        return this.mFileInputStream;
    }
    
    public FileOutputStream getmFileOutputStream() {
        return this.mFileOutputStream;
    }
}
