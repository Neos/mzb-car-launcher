package com.backaudio.android.driver.beans;

import android.os.*;

public class CarRunInfo implements Parcelable
{
    public static final Parcelable$Creator<CarRunInfo> CREATOR;
    private int averSpeed;
    private int curSpeed;
    private int mileage;
    private double outsideTemp;
    private int revolutions;
    private long totalMileage;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<CarRunInfo>() {
            public CarRunInfo createFromParcel(final Parcel parcel) {
                return new CarRunInfo(parcel);
            }
            
            public CarRunInfo[] newArray(final int n) {
                return new CarRunInfo[n];
            }
        };
    }
    
    public CarRunInfo() {
        this.averSpeed = 0;
        this.mileage = 0;
        this.totalMileage = 0L;
        this.outsideTemp = 0.0;
        this.revolutions = 0;
        this.curSpeed = 0;
    }
    
    protected CarRunInfo(final Parcel parcel) {
        this.averSpeed = 0;
        this.mileage = 0;
        this.totalMileage = 0L;
        this.outsideTemp = 0.0;
        this.revolutions = 0;
        this.curSpeed = 0;
        this.averSpeed = parcel.readInt();
        this.mileage = parcel.readInt();
        this.totalMileage = parcel.readLong();
        this.outsideTemp = parcel.readDouble();
        this.revolutions = parcel.readInt();
        this.curSpeed = parcel.readInt();
    }
    
    public int describeContents() {
        return 0;
    }
    
    public int getAverSpeed() {
        return this.averSpeed;
    }
    
    public int getCurSpeed() {
        return this.curSpeed;
    }
    
    public int getMileage() {
        return this.mileage;
    }
    
    public double getOutsideTemp() {
        return this.outsideTemp;
    }
    
    public int getRevolutions() {
        return this.revolutions;
    }
    
    public long getTotalMileage() {
        return this.totalMileage;
    }
    
    public void setAverSpeed(final int averSpeed) {
        this.averSpeed = averSpeed;
    }
    
    public void setCurSpeed(final int curSpeed) {
        this.curSpeed = curSpeed;
    }
    
    public void setMileage(final int mileage) {
        this.mileage = mileage;
    }
    
    public void setOutsideTemp(final double outsideTemp) {
        this.outsideTemp = outsideTemp;
    }
    
    public void setRevolutions(final int revolutions) {
        this.revolutions = revolutions;
    }
    
    public void setTotalMileage(final long totalMileage) {
        this.totalMileage = totalMileage;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CarRunInfo [averSpeed=");
        sb.append(this.averSpeed);
        sb.append(", mileage=");
        sb.append(this.mileage);
        sb.append(", totalMileage=");
        sb.append(this.totalMileage);
        sb.append(", outsideTemp=");
        sb.append(this.outsideTemp);
        sb.append(", revolutions=");
        sb.append(this.revolutions);
        sb.append(", curSpeed=");
        sb.append(this.curSpeed);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.averSpeed);
        parcel.writeInt(this.mileage);
        parcel.writeLong(this.totalMileage);
        parcel.writeDouble(this.outsideTemp);
        parcel.writeInt(this.revolutions);
        parcel.writeInt(this.curSpeed);
    }
}
