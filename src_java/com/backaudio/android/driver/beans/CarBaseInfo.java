package com.backaudio.android.driver.beans;

import android.os.*;

public class CarBaseInfo implements Parcelable
{
    public static final Parcelable$Creator<CarBaseInfo> CREATOR;
    private boolean foglight;
    private boolean iAccOpen;
    private boolean iBack;
    private boolean iBrake;
    private boolean iDistantLightOpen;
    private boolean iFlash;
    private boolean iFootBrake;
    private boolean iFront;
    private boolean iInP;
    private boolean iInRevering;
    private boolean iLeftBackOpen;
    private boolean iLeftFrontOpen;
    private int iLightValue;
    private boolean iNearLightOpen;
    private boolean iPowerOn;
    private boolean iRightBackOpen;
    private boolean iRightFrontOpen;
    private boolean iSafetyBelt;
    public boolean ichange;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<CarBaseInfo>() {
            public CarBaseInfo createFromParcel(final Parcel parcel) {
                return new CarBaseInfo(parcel);
            }
            
            public CarBaseInfo[] newArray(final int n) {
                return new CarBaseInfo[n];
            }
        };
    }
    
    public CarBaseInfo() {
        this.iDistantLightOpen = false;
        this.iPowerOn = false;
        this.iAccOpen = false;
        this.iBrake = false;
        this.iNearLightOpen = false;
        this.iInP = false;
        this.iInRevering = false;
        this.iRightBackOpen = false;
        this.iLeftBackOpen = false;
        this.iRightFrontOpen = false;
        this.iLeftFrontOpen = false;
        this.iSafetyBelt = false;
        this.iFootBrake = false;
        this.iFront = false;
        this.iBack = false;
        this.iLightValue = 0;
        this.foglight = false;
        this.iFlash = false;
        this.ichange = false;
    }
    
    protected CarBaseInfo(final Parcel parcel) {
        final boolean b = true;
        this.iDistantLightOpen = false;
        this.iPowerOn = false;
        this.iAccOpen = false;
        this.iBrake = false;
        this.iNearLightOpen = false;
        this.iInP = false;
        this.iInRevering = false;
        this.iRightBackOpen = false;
        this.iLeftBackOpen = false;
        this.iRightFrontOpen = false;
        this.iLeftFrontOpen = false;
        this.iSafetyBelt = false;
        this.iFootBrake = false;
        this.iFront = false;
        this.iBack = false;
        this.iLightValue = 0;
        this.foglight = false;
        this.iFlash = false;
        this.ichange = false;
        this.iDistantLightOpen = (parcel.readByte() != 0);
        this.iPowerOn = (parcel.readByte() != 0);
        this.iAccOpen = (parcel.readByte() != 0);
        this.iBrake = (parcel.readByte() != 0);
        this.iNearLightOpen = (parcel.readByte() != 0);
        this.iInP = (parcel.readByte() != 0);
        this.iInRevering = (parcel.readByte() != 0);
        this.iRightBackOpen = (parcel.readByte() != 0);
        this.iLeftBackOpen = (parcel.readByte() != 0);
        this.iRightFrontOpen = (parcel.readByte() != 0);
        this.iLeftFrontOpen = (parcel.readByte() != 0);
        this.iFront = (parcel.readByte() != 0);
        this.iBack = (parcel.readByte() != 0);
        this.iFlash = (parcel.readByte() != 0);
        this.iSafetyBelt = (parcel.readByte() != 0);
        this.iFootBrake = (parcel.readByte() != 0 && b);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public int getiLightValue() {
        return this.iLightValue;
    }
    
    public boolean isFoglight() {
        return this.foglight;
    }
    
    public boolean isiAccOpen() {
        return this.iAccOpen;
    }
    
    public boolean isiBack() {
        return this.iBack;
    }
    
    public boolean isiBrake() {
        return this.iBrake;
    }
    
    public boolean isiDistantLightOpen() {
        return this.iDistantLightOpen;
    }
    
    public boolean isiFlash() {
        return this.iFlash;
    }
    
    public boolean isiFootBrake() {
        return this.iFootBrake;
    }
    
    public boolean isiFront() {
        return this.iFront;
    }
    
    public boolean isiInP() {
        return this.iInP;
    }
    
    public boolean isiInRevering() {
        return this.iInRevering;
    }
    
    public boolean isiLeftBackOpen() {
        return this.iLeftBackOpen;
    }
    
    public boolean isiLeftFrontOpen() {
        return this.iLeftFrontOpen;
    }
    
    public boolean isiNearLightOpen() {
        return this.iNearLightOpen;
    }
    
    public boolean isiPowerOn() {
        return this.iPowerOn;
    }
    
    public boolean isiRightBackOpen() {
        return this.iRightBackOpen;
    }
    
    public boolean isiRightFrontOpen() {
        return this.iRightFrontOpen;
    }
    
    public boolean isiSafetyBelt() {
        return this.iSafetyBelt;
    }
    
    public void setFoglight(final boolean foglight) {
        this.ichange |= (this.foglight ^ foglight);
        this.foglight = foglight;
    }
    
    public void setiAccOpen(final boolean iAccOpen) {
        this.iAccOpen = iAccOpen;
    }
    
    public void setiBack(final boolean iBack) {
        this.iBack = iBack;
    }
    
    public void setiBrake(final boolean iBrake) {
        this.ichange |= (this.iBrake ^ iBrake);
        this.iBrake = iBrake;
    }
    
    public void setiDistantLightOpen(final boolean iDistantLightOpen) {
        this.ichange |= (this.iDistantLightOpen ^ iDistantLightOpen);
        this.iDistantLightOpen = iDistantLightOpen;
    }
    
    public void setiFlash(final boolean iFlash) {
        this.iFlash = iFlash;
    }
    
    public void setiFootBrake(final boolean iFootBrake) {
        this.ichange |= (this.iFootBrake ^ iFootBrake);
        this.iFootBrake = iFootBrake;
    }
    
    public void setiFront(final boolean iFront) {
        this.iFront = iFront;
    }
    
    public void setiInP(final boolean iInP) {
        this.iInP = iInP;
    }
    
    public void setiInRevering(final boolean iInRevering) {
        this.iInRevering = iInRevering;
    }
    
    public void setiLeftBackOpen(final boolean iLeftBackOpen) {
        this.iLeftBackOpen = iLeftBackOpen;
    }
    
    public void setiLeftFrontOpen(final boolean iLeftFrontOpen) {
        this.iLeftFrontOpen = iLeftFrontOpen;
    }
    
    public void setiLightValue(final int iLightValue) {
        this.iLightValue = iLightValue;
    }
    
    public void setiNearLightOpen(final boolean iNearLightOpen) {
        this.ichange = (this.iNearLightOpen ^ iNearLightOpen);
        this.iNearLightOpen = iNearLightOpen;
    }
    
    public void setiPowerOn(final boolean iPowerOn) {
        this.iPowerOn = iPowerOn;
    }
    
    public void setiRightBackOpen(final boolean iRightBackOpen) {
        this.iRightBackOpen = iRightBackOpen;
    }
    
    public void setiRightFrontOpen(final boolean iRightFrontOpen) {
        this.iRightFrontOpen = iRightFrontOpen;
    }
    
    public void setiSafetyBelt(final boolean iSafetyBelt) {
        this.iSafetyBelt = iSafetyBelt;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CarBaseInfo [iDistantLightOpen=");
        sb.append(this.iDistantLightOpen);
        sb.append(", iPowerOn=");
        sb.append(this.iPowerOn);
        sb.append(", iAccOpen=");
        sb.append(this.iAccOpen);
        sb.append(", iBrake=");
        sb.append(this.iBrake);
        sb.append(", iNearLightOpen=");
        sb.append(this.iNearLightOpen);
        sb.append(", iInP=");
        sb.append(this.iInP);
        sb.append(", iInRevering=");
        sb.append(this.iInRevering);
        sb.append(", iRightBackOpen=");
        sb.append(this.iRightBackOpen);
        sb.append(", iLeftBackOpen=");
        sb.append(this.iLeftBackOpen);
        sb.append(", iRightFrontOpen=");
        sb.append(this.iRightFrontOpen);
        sb.append(", iLeftFrontOpen=");
        sb.append(this.iLeftFrontOpen);
        sb.append(", iFront=");
        sb.append(this.iFront);
        sb.append(", iBack=");
        sb.append(this.iBack);
        sb.append(", iFlash=");
        sb.append(this.iFlash);
        sb.append(", iSafetyBelt=");
        sb.append(this.iSafetyBelt);
        sb.append(", iFootBrake=");
        sb.append(this.iFootBrake);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int n) {
        final int n2 = 1;
        if (this.iDistantLightOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iPowerOn) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iAccOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iBrake) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iNearLightOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iInP) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iInRevering) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iRightBackOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iLeftBackOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iRightFrontOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iLeftFrontOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iFront) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iBack) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iFlash) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iSafetyBelt) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iFootBrake) {
            n = n2;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
    }
}
