package com.backaudio.android.driver.beans;

import android.os.*;

public class AirInfo implements Parcelable
{
    public static final Parcelable$Creator<AirInfo> CREATOR;
    private boolean iACOpen;
    private boolean iAirOpen;
    private boolean iAuto1;
    private boolean iAuto2;
    private boolean iBackWind;
    private boolean iDownWind;
    private boolean iDual;
    private boolean iFlatWind;
    private boolean iFront;
    private boolean iFrontWind;
    private boolean iHadChange;
    private boolean iInnerLoop;
    private boolean iMaxFrontWind;
    private boolean iRear;
    private boolean iSync;
    private boolean iWindDirAuto;
    private double leftTemp;
    private double level;
    private double rightTemp;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<AirInfo>() {
            public AirInfo createFromParcel(final Parcel parcel) {
                return new AirInfo(parcel);
            }
            
            public AirInfo[] newArray(final int n) {
                return new AirInfo[n];
            }
        };
    }
    
    public AirInfo() {
        this.iAirOpen = false;
        this.iACOpen = false;
        this.iInnerLoop = false;
        this.iSync = false;
        this.iAuto1 = false;
        this.iAuto2 = false;
        this.iDual = false;
        this.iFront = false;
        this.iRear = false;
        this.iWindDirAuto = false;
        this.iFrontWind = false;
        this.iMaxFrontWind = false;
        this.iBackWind = false;
        this.iFlatWind = false;
        this.iDownWind = false;
        this.iHadChange = false;
        this.level = 0.0;
        this.leftTemp = 15.0;
        this.rightTemp = 15.0;
    }
    
    protected AirInfo(final Parcel parcel) {
        final boolean b = true;
        this.iAirOpen = false;
        this.iACOpen = false;
        this.iInnerLoop = false;
        this.iSync = false;
        this.iAuto1 = false;
        this.iAuto2 = false;
        this.iDual = false;
        this.iFront = false;
        this.iRear = false;
        this.iWindDirAuto = false;
        this.iFrontWind = false;
        this.iMaxFrontWind = false;
        this.iBackWind = false;
        this.iFlatWind = false;
        this.iDownWind = false;
        this.iHadChange = false;
        this.level = 0.0;
        this.leftTemp = 15.0;
        this.rightTemp = 15.0;
        this.iAirOpen = (parcel.readByte() != 0);
        this.iACOpen = (parcel.readByte() != 0);
        this.iInnerLoop = (parcel.readByte() != 0);
        this.iSync = (parcel.readByte() != 0);
        this.iAuto1 = (parcel.readByte() != 0);
        this.iAuto2 = (parcel.readByte() != 0);
        this.iDual = (parcel.readByte() != 0);
        this.iFront = (parcel.readByte() != 0);
        this.iRear = (parcel.readByte() != 0);
        this.iWindDirAuto = (parcel.readByte() != 0);
        this.iFrontWind = (parcel.readByte() != 0);
        this.iMaxFrontWind = (parcel.readByte() != 0);
        this.iBackWind = (parcel.readByte() != 0);
        this.iFlatWind = (parcel.readByte() != 0);
        this.iDownWind = (parcel.readByte() != 0);
        this.iHadChange = (parcel.readByte() != 0 && b);
        this.level = parcel.readInt();
        this.leftTemp = parcel.readInt();
        this.rightTemp = parcel.readInt();
    }
    
    public int describeContents() {
        return 0;
    }
    
    public double getLeftTemp() {
        return this.leftTemp;
    }
    
    public double getLevel() {
        return this.level;
    }
    
    public double getRightTemp() {
        return this.rightTemp;
    }
    
    public boolean isiACOpen() {
        return this.iACOpen;
    }
    
    public boolean isiAirOpen() {
        return this.iAirOpen;
    }
    
    public boolean isiAuto1() {
        return this.iAuto1;
    }
    
    public boolean isiAuto2() {
        return this.iAuto2;
    }
    
    public boolean isiBackWind() {
        return this.iBackWind;
    }
    
    public boolean isiDownWind() {
        return this.iDownWind;
    }
    
    public boolean isiDual() {
        return this.iDual;
    }
    
    public boolean isiFlatWind() {
        return this.iFlatWind;
    }
    
    public boolean isiFront() {
        return this.iFront;
    }
    
    public boolean isiFrontWind() {
        return this.iFrontWind;
    }
    
    public boolean isiHadChange() {
        return this.iHadChange;
    }
    
    public boolean isiInnerLoop() {
        return this.iInnerLoop;
    }
    
    public boolean isiMaxFrontWind() {
        return this.iMaxFrontWind;
    }
    
    public boolean isiRear() {
        return this.iRear;
    }
    
    public boolean isiSync() {
        return this.iSync;
    }
    
    public boolean isiWindDirAuto() {
        return this.iWindDirAuto;
    }
    
    public void setLeftTemp(final double leftTemp) {
        this.leftTemp = leftTemp;
    }
    
    public void setLevel(final double level) {
        this.level = level;
    }
    
    public void setRightTemp(final double rightTemp) {
        this.rightTemp = rightTemp;
    }
    
    public void setiACOpen(final boolean iacOpen) {
        this.iACOpen = iacOpen;
    }
    
    public void setiAirOpen(final boolean iAirOpen) {
        this.iAirOpen = iAirOpen;
    }
    
    public void setiAuto1(final boolean iAuto1) {
        this.iAuto1 = iAuto1;
    }
    
    public void setiAuto2(final boolean iAuto2) {
        this.iAuto2 = iAuto2;
    }
    
    public void setiBackWind(final boolean iBackWind) {
        this.iBackWind = iBackWind;
    }
    
    public void setiDownWind(final boolean iDownWind) {
        this.iDownWind = iDownWind;
    }
    
    public void setiDual(final boolean iDual) {
        this.iDual = iDual;
    }
    
    public void setiFlatWind(final boolean iFlatWind) {
        this.iFlatWind = iFlatWind;
    }
    
    public void setiFront(final boolean iFront) {
        this.iFront = iFront;
    }
    
    public void setiFrontWind(final boolean iFrontWind) {
        this.iFrontWind = iFrontWind;
    }
    
    public void setiHadChange(final boolean iHadChange) {
        this.iHadChange = iHadChange;
    }
    
    public void setiInnerLoop(final boolean iInnerLoop) {
        this.iInnerLoop = iInnerLoop;
    }
    
    public void setiMaxFrontWind(final boolean iMaxFrontWind) {
        this.iMaxFrontWind = iMaxFrontWind;
    }
    
    public void setiRear(final boolean iRear) {
        this.iRear = iRear;
    }
    
    public void setiSync(final boolean iSync) {
        this.iSync = iSync;
    }
    
    public void setiWindDirAuto(final boolean iWindDirAuto) {
        this.iWindDirAuto = iWindDirAuto;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AirInfo [iAirOpen=");
        sb.append(this.iAirOpen);
        sb.append(", iACOpen=");
        sb.append(this.iACOpen);
        sb.append(", iInnerLoop=");
        sb.append(this.iInnerLoop);
        sb.append(", iSync=");
        sb.append(this.iSync);
        sb.append(", iAuto1=");
        sb.append(this.iAuto1);
        sb.append(", iAuto2=");
        sb.append(this.iAuto2);
        sb.append(", iDual=");
        sb.append(this.iDual);
        sb.append(", iFront=");
        sb.append(this.iFront);
        sb.append(", iRear=");
        sb.append(this.iRear);
        sb.append(", iWindDirAuto=");
        sb.append(this.iWindDirAuto);
        sb.append(", iFrontWind=");
        sb.append(this.iFrontWind);
        sb.append(", iMaxFrontWind=");
        sb.append(this.iMaxFrontWind);
        sb.append(", iBackWind=");
        sb.append(this.iBackWind);
        sb.append(", iFlatWind=");
        sb.append(this.iFlatWind);
        sb.append(", iDownWind=");
        sb.append(this.iDownWind);
        sb.append(", iHadChange=");
        sb.append(this.iHadChange);
        sb.append(", level=");
        sb.append(this.level);
        sb.append(", leftTemp=");
        sb.append(this.leftTemp);
        sb.append(", rightTemp=");
        sb.append(this.rightTemp);
        sb.append("]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int n) {
        final int n2 = 1;
        if (this.iAirOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iACOpen) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iInnerLoop) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iSync) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iAuto1) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iAuto2) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iDual) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iFront) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iRear) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iWindDirAuto) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iFrontWind) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iMaxFrontWind) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iBackWind) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iFlatWind) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iDownWind) {
            n = 1;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        if (this.iHadChange) {
            n = n2;
        }
        else {
            n = 0;
        }
        parcel.writeByte((byte)n);
        parcel.writeDouble(this.level);
        parcel.writeDouble(this.leftTemp);
        parcel.writeDouble(this.rightTemp);
    }
}
