package com.backaudio.android.driver;

import java.io.*;
import org.slf4j.*;
import android.util.*;

public class ProtocolAnalyzer
{
    static final Object eventLock;
    private final String TAG;
    int canCurrentProtocolLength;
    ByteArrayOutputStream canProtocolBuffer;
    int canProtocolLength;
    int currentProtocolLength;
    boolean iDown;
    private Logger logger;
    ByteArrayOutputStream protocolBuffer;
    int protocolLength;
    private final byte[] tmp;
    int touch_x;
    int touch_y;
    
    static {
        eventLock = new Object();
    }
    
    public ProtocolAnalyzer() {
        this.TAG = "driverlog";
        this.protocolBuffer = new ByteArrayOutputStream();
        this.canProtocolBuffer = new ByteArrayOutputStream();
        this.canProtocolLength = 0;
        this.canCurrentProtocolLength = 0;
        this.protocolLength = 0;
        this.currentProtocolLength = 0;
        this.logger = LoggerFactory.getLogger(ProtocolAnalyzer.class);
        this.tmp = new byte[8];
    }
    
    private void analyzeCanbox(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_3       
        //     2: iload_3        
        //     3: aload_1        
        //     4: arraylength    
        //     5: iconst_1       
        //     6: isub           
        //     7: if_icmplt       11
        //    10: return         
        //    11: aload_1        
        //    12: iload_3        
        //    13: baload         
        //    14: istore_2       
        //    15: aload_0        
        //    16: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //    19: tableswitch {
        //                0: 131
        //                1: 209
        //                2: 238
        //          default: 394
        //        }
        //    44: aload_0        
        //    45: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //    48: aload_0        
        //    49: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //    52: if_icmpge       350
        //    55: aload_0        
        //    56: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //    59: iconst_1       
        //    60: newarray        B
        //    62: dup            
        //    63: iconst_0       
        //    64: iload_2        
        //    65: bastore        
        //    66: iconst_0       
        //    67: iconst_1       
        //    68: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //    71: aload_0        
        //    72: aload_0        
        //    73: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //    76: iconst_1       
        //    77: iadd           
        //    78: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //    81: aload_0        
        //    82: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //    85: istore          4
        //    87: aload_0        
        //    88: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //    91: istore          5
        //    93: iload           4
        //    95: iload           5
        //    97: if_icmpne       397
        //   100: aload_0        
        //   101: aload_0        
        //   102: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   105: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   108: invokespecial   com/backaudio/android/driver/ProtocolAnalyzer.newProtocol:([B)V
        //   111: aload_0        
        //   112: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   115: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   118: aload_0        
        //   119: iconst_0       
        //   120: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   123: aload_0        
        //   124: iconst_0       
        //   125: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   128: goto            397
        //   131: iload_2        
        //   132: bipush          46
        //   134: if_icmpne       176
        //   137: aload_0        
        //   138: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   141: iconst_1       
        //   142: newarray        B
        //   144: dup            
        //   145: iconst_0       
        //   146: iload_2        
        //   147: bastore        
        //   148: iconst_0       
        //   149: iconst_1       
        //   150: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   153: aload_0        
        //   154: aload_0        
        //   155: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   158: iconst_1       
        //   159: iadd           
        //   160: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   163: goto            397
        //   166: astore          6
        //   168: aload           6
        //   170: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   173: goto            397
        //   176: ldc             "driverlog"
        //   178: new             Ljava/lang/StringBuilder;
        //   181: dup            
        //   182: ldc             "canbox: require 0x2E or 0xFF but "
        //   184: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   187: iload_2        
        //   188: invokestatic    java/lang/Integer.toHexString:(I)Ljava/lang/String;
        //   191: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   194: ldc             "\n"
        //   196: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   199: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   202: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   205: pop            
        //   206: goto            397
        //   209: aload_0        
        //   210: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   213: iconst_1       
        //   214: newarray        B
        //   216: dup            
        //   217: iconst_0       
        //   218: iload_2        
        //   219: bastore        
        //   220: iconst_0       
        //   221: iconst_1       
        //   222: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   225: aload_0        
        //   226: aload_0        
        //   227: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   230: iconst_1       
        //   231: iadd           
        //   232: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   235: goto            397
        //   238: aload_0        
        //   239: iload_2        
        //   240: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   243: aload_0        
        //   244: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   247: ifge            262
        //   250: aload_0        
        //   251: aload_0        
        //   252: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   255: sipush          256
        //   258: iadd           
        //   259: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   262: aload_0        
        //   263: aload_0        
        //   264: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   267: iconst_4       
        //   268: iadd           
        //   269: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   272: aload_0        
        //   273: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   276: iconst_1       
        //   277: newarray        B
        //   279: dup            
        //   280: iconst_0       
        //   281: iload_2        
        //   282: bastore        
        //   283: iconst_0       
        //   284: iconst_1       
        //   285: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   288: aload_0        
        //   289: aload_0        
        //   290: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   293: iconst_1       
        //   294: iadd           
        //   295: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   298: goto            397
        //   301: astore          6
        //   303: aload           6
        //   305: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   308: aload_0        
        //   309: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   312: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   315: aload_0        
        //   316: iconst_0       
        //   317: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   320: aload_0        
        //   321: iconst_0       
        //   322: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   325: goto            397
        //   328: astore          6
        //   330: aload_0        
        //   331: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   334: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   337: aload_0        
        //   338: iconst_0       
        //   339: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   342: aload_0        
        //   343: iconst_0       
        //   344: putfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   347: aload           6
        //   349: athrow         
        //   350: aload_0        
        //   351: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canCurrentProtocolLength:I
        //   354: aload_0        
        //   355: getfield        com/backaudio/android/driver/ProtocolAnalyzer.canProtocolLength:I
        //   358: if_icmple       397
        //   361: ldc             "driverlog"
        //   363: new             Ljava/lang/StringBuilder;
        //   366: dup            
        //   367: ldc             "canbox: shit:"
        //   369: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   372: iload_2        
        //   373: invokestatic    java/lang/Integer.toHexString:(I)Ljava/lang/String;
        //   376: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   379: ldc             "\n"
        //   381: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   384: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   387: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   390: pop            
        //   391: goto            397
        //   394: goto            44
        //   397: iload_3        
        //   398: iconst_1       
        //   399: iadd           
        //   400: istore_3       
        //   401: goto            2
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  15     44     166    176    Ljava/lang/Exception;
        //  44     93     166    176    Ljava/lang/Exception;
        //  100    111    301    328    Ljava/lang/Exception;
        //  100    111    328    350    Any
        //  111    128    166    176    Ljava/lang/Exception;
        //  137    163    166    176    Ljava/lang/Exception;
        //  176    206    166    176    Ljava/lang/Exception;
        //  209    235    166    176    Ljava/lang/Exception;
        //  238    262    166    176    Ljava/lang/Exception;
        //  262    298    166    176    Ljava/lang/Exception;
        //  303    308    328    350    Any
        //  308    325    166    176    Ljava/lang/Exception;
        //  330    350    166    176    Ljava/lang/Exception;
        //  350    391    166    176    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0131:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void analyzeMcu(final byte[] p0, final int p1, final int p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore          5
        //     3: iconst_0       
        //     4: istore_2       
        //     5: iload           5
        //     7: aload_1        
        //     8: arraylength    
        //     9: if_icmpge       17
        //    12: iload_2        
        //    13: iload_3        
        //    14: if_icmplt       18
        //    17: return         
        //    18: aload_1        
        //    19: iload           5
        //    21: baload         
        //    22: istore          4
        //    24: aload_0        
        //    25: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //    28: tableswitch {
        //                0: 144
        //                1: 225
        //                2: 303
        //          default: 462
        //        }
        //    56: aload_0        
        //    57: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //    60: aload_0        
        //    61: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //    64: if_icmpge       417
        //    67: aload_0        
        //    68: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //    71: iconst_1       
        //    72: newarray        B
        //    74: dup            
        //    75: iconst_0       
        //    76: iload           4
        //    78: bastore        
        //    79: iconst_0       
        //    80: iconst_1       
        //    81: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //    84: aload_0        
        //    85: aload_0        
        //    86: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //    89: iconst_1       
        //    90: iadd           
        //    91: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //    94: aload_0        
        //    95: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //    98: istore          6
        //   100: aload_0        
        //   101: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   104: istore          7
        //   106: iload           6
        //   108: iload           7
        //   110: if_icmpne       465
        //   113: aload_0        
        //   114: aload_0        
        //   115: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   118: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   121: invokespecial   com/backaudio/android/driver/ProtocolAnalyzer.newMcuProtocol:([B)V
        //   124: aload_0        
        //   125: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   128: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   131: aload_0        
        //   132: iconst_0       
        //   133: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   136: aload_0        
        //   137: iconst_0       
        //   138: putfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   141: goto            465
        //   144: iload           4
        //   146: bipush          -86
        //   148: if_icmpne       191
        //   151: aload_0        
        //   152: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   155: iconst_1       
        //   156: newarray        B
        //   158: dup            
        //   159: iconst_0       
        //   160: iload           4
        //   162: bastore        
        //   163: iconst_0       
        //   164: iconst_1       
        //   165: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   168: aload_0        
        //   169: aload_0        
        //   170: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   173: iconst_1       
        //   174: iadd           
        //   175: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   178: goto            465
        //   181: astore          8
        //   183: aload           8
        //   185: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   188: goto            465
        //   191: ldc             "driverlog"
        //   193: new             Ljava/lang/StringBuilder;
        //   196: dup            
        //   197: ldc             "mcu: require 0xAA but "
        //   199: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   202: iload           4
        //   204: invokestatic    java/lang/Integer.toHexString:(I)Ljava/lang/String;
        //   207: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   210: ldc             "\n"
        //   212: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   215: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   218: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   221: pop            
        //   222: goto            465
        //   225: iload           4
        //   227: bipush          85
        //   229: if_icmpne       262
        //   232: aload_0        
        //   233: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   236: iconst_1       
        //   237: newarray        B
        //   239: dup            
        //   240: iconst_0       
        //   241: iload           4
        //   243: bastore        
        //   244: iconst_0       
        //   245: iconst_1       
        //   246: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   249: aload_0        
        //   250: aload_0        
        //   251: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   254: iconst_1       
        //   255: iadd           
        //   256: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   259: goto            465
        //   262: ldc             "driverlog"
        //   264: new             Ljava/lang/StringBuilder;
        //   267: dup            
        //   268: ldc             "require 0x55 but "
        //   270: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   273: iload           4
        //   275: invokestatic    java/lang/Integer.toHexString:(I)Ljava/lang/String;
        //   278: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   281: ldc             "\n"
        //   283: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   286: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   289: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   292: pop            
        //   293: aload_0        
        //   294: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   297: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   300: goto            465
        //   303: aload_0        
        //   304: iload           4
        //   306: putfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   309: aload_0        
        //   310: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   313: ifge            328
        //   316: aload_0        
        //   317: aload_0        
        //   318: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   321: sipush          256
        //   324: iadd           
        //   325: putfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   328: aload_0        
        //   329: aload_0        
        //   330: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   333: iconst_4       
        //   334: iadd           
        //   335: putfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   338: aload_0        
        //   339: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   342: iconst_1       
        //   343: newarray        B
        //   345: dup            
        //   346: iconst_0       
        //   347: iload           4
        //   349: bastore        
        //   350: iconst_0       
        //   351: iconst_1       
        //   352: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   355: aload_0        
        //   356: aload_0        
        //   357: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   360: iconst_1       
        //   361: iadd           
        //   362: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   365: goto            465
        //   368: astore          8
        //   370: aload           8
        //   372: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   375: aload_0        
        //   376: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   379: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   382: aload_0        
        //   383: iconst_0       
        //   384: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   387: aload_0        
        //   388: iconst_0       
        //   389: putfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   392: goto            465
        //   395: astore          8
        //   397: aload_0        
        //   398: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolBuffer:Ljava/io/ByteArrayOutputStream;
        //   401: invokevirtual   java/io/ByteArrayOutputStream.reset:()V
        //   404: aload_0        
        //   405: iconst_0       
        //   406: putfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   409: aload_0        
        //   410: iconst_0       
        //   411: putfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   414: aload           8
        //   416: athrow         
        //   417: aload_0        
        //   418: getfield        com/backaudio/android/driver/ProtocolAnalyzer.currentProtocolLength:I
        //   421: aload_0        
        //   422: getfield        com/backaudio/android/driver/ProtocolAnalyzer.protocolLength:I
        //   425: if_icmple       465
        //   428: ldc             "driverlog"
        //   430: new             Ljava/lang/StringBuilder;
        //   433: dup            
        //   434: ldc             "mcu: shit:"
        //   436: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   439: iload           4
        //   441: invokestatic    java/lang/Integer.toHexString:(I)Ljava/lang/String;
        //   444: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   447: ldc             "\n"
        //   449: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   452: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   455: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   458: pop            
        //   459: goto            465
        //   462: goto            56
        //   465: iload           5
        //   467: iconst_1       
        //   468: iadd           
        //   469: istore          5
        //   471: iload_2        
        //   472: iconst_1       
        //   473: iadd           
        //   474: istore_2       
        //   475: goto            5
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  24     56     181    191    Ljava/lang/Exception;
        //  56     106    181    191    Ljava/lang/Exception;
        //  113    124    368    395    Ljava/lang/Exception;
        //  113    124    395    417    Any
        //  124    141    181    191    Ljava/lang/Exception;
        //  151    178    181    191    Ljava/lang/Exception;
        //  191    222    181    191    Ljava/lang/Exception;
        //  232    259    181    191    Ljava/lang/Exception;
        //  262    300    181    191    Ljava/lang/Exception;
        //  303    328    181    191    Ljava/lang/Exception;
        //  328    365    181    191    Ljava/lang/Exception;
        //  370    375    395    417    Any
        //  375    392    181    191    Ljava/lang/Exception;
        //  397    417    181    191    Ljava/lang/Exception;
        //  417    459    181    191    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0144:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean checkMcu(final byte[] array) {
        if (array.length < 4) {
            Log.e("driverlog", "mcuParse_checkMcu buffer too short");
        }
        else if (array[2] == (byte)(array.length - 4)) {
            byte b = array[2];
            for (int i = 3; i < array.length - 1; ++i) {
                b += array[i];
            }
            if (b == array[array.length - 1]) {
                return true;
            }
        }
        return false;
    }
    
    private void handleEvent(final byte[] array, int n, int n2) {
        n = ((array[1] & 0xFF) << 8) + (array[0] & 0xFF);
        n2 = ((array[3] & 0xFF) << 8) + (array[2] & 0xFF);
        final int n3 = ((array[5] & 0xFF) << 8) + (array[4] & 0xFF);
        if (n == ETouch.EV_ABS.getCode()) {
            if (n2 == ETouch.ABS_X.getCode()) {
                this.touch_x = n3;
            }
            else if (n2 == ETouch.ABS_Y.getCode()) {
                this.touch_y = n3;
            }
        }
        else {
            if (n != ETouch.EV_KEY.getCode()) {
                ETouch.EV_SYN.getCode();
                return;
            }
            if (n2 == ETouch.BTN_TOUCH.getCode() && n3 == 0) {
                this.logger.debug("button handleEvent touch_x = " + this.touch_x + ",touch_y = " + this.touch_y);
                Mainboard.getInstance().sendCoordinateToMcu(this.touch_x, this.touch_y, 1);
            }
        }
    }
    
    private void newMcuProtocol(final byte[] array) {
        Log.d("driverlog", "mcuParse:[" + Utils.byteArrayToHexString(array) + "]");
        if (array == null || array.length < 5) {
            Log.d("driverlog", "mcu: ProtocolAnalyzer invalid mcu buffer, drop");
        }
        else {
            if (!this.checkMcu(array)) {
                Log.e("driverlog", "checkMcu failed");
                return;
            }
            if (array[3] == 1 && array[4] == 0) {
                Mainboard.getInstance().analyzeMcuEnterAccOrWakeup(array);
                return;
            }
            if (array[4] == 17) {
                Mainboard.getInstance().analyzeMcuInfoPro(array);
                return;
            }
            if (array[3] != 1 || array[4] != 62) {
                if (array[3] == -30) {
                    Mainboard.getInstance().analyzeMcuUpgradeStatePro(array);
                    return;
                }
                if (array[3] == -28) {
                    Mainboard.getInstance().analyzeMcuUpgradeDateByindexPro(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 80) {
                    this.pushCanbox(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 75) {
                    Mainboard.getInstance().analyzeShowOrHideCarLayer(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 91) {
                    Mainboard.getInstance().analyzeReverseTpye(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 98) {
                    Mainboard.getInstance().analyzeStoreDateFromMCU(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 102) {
                    Mainboard.getInstance().analyzeBenzTpye(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 106) {
                    Mainboard.getInstance().analyzeBenzSize(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 104) {
                    Mainboard.getInstance().analyzeReverseMediaSet(array);
                    return;
                }
                if (array[3] == 1 && array[4] == 111) {
                    Mainboard.getInstance().analyzeLanguageMediaSet(array);
                }
            }
        }
    }
    
    private void newProtocol(final byte[] array) {
        Log.d("driverlog", "canboxParse:[" + Utils.byteArrayToHexString(array) + "]");
        if (array == null || array.length < 5) {
            Log.e("driverlog", "canboxParse invalid canbox buffer, drop");
        }
        else {
            if (!this.checkCanbox(array)) {
                Log.e("driverlog", "error check");
                return;
            }
            Mainboard.getInstance().logcatCanbox(Utils.byteArrayToHexString(array));
            switch (array[1]) {
                case 10: {
                    break;
                }
                default: {}
                case -30: {
                    Mainboard.getInstance().analyzeUpgradeStatePro(array);
                }
                case 1: {
                    Mainboard.getInstance().analyzeCarBasePro(array);
                }
                case 2: {
                    Mainboard.getInstance().analyzeIdriverPro(array);
                }
                case 3: {
                    Mainboard.getInstance().analyzeAirInfo(array);
                }
                case 6: {
                    Mainboard.getInstance().analyzeCarBasePro1(array);
                }
                case 7: {
                    Mainboard.getInstance().analyzeCarRunningInfoPro1(array);
                }
                case 8: {
                    Mainboard.getInstance().analyzeTimePro(array);
                }
                case 18: {
                    Mainboard.getInstance().analyzeOriginalCarPro(array);
                }
                case 44: {
                    Mainboard.getInstance().analyzeAUXStatus(array);
                }
                case 53: {
                    Mainboard.getInstance().analyzeCarRunningInfoPro(array);
                }
                case 54: {
                    Mainboard.getInstance().analyzeCarRunningInfoPro2(array);
                }
                case Byte.MAX_VALUE: {
                    Mainboard.getInstance().analyzeCanboxPro(array);
                }
                case -28: {
                    Mainboard.getInstance().analyzeUpgradeDateByindexPro(array);
                }
            }
        }
    }
    
    private void pushCanbox(final byte[] array) {
        synchronized (ProtocolAnalyzer.class) {
            final String string = "canbox:recv:<[" + Utils.byteArrayToHexString(array) + "]>";
            Log.d("driverlog", string);
            this.logger.debug(string);
            this.analyzeCanbox(array);
        }
    }
    
    public boolean checkCanbox(final byte[] array) {
        if (array.length < 5) {
            Log.e("driverlog", "canboxParse_checkMcu buffer too short");
        }
        else if (array[2] == (byte)(array.length - 4)) {
            byte b = array[1];
            for (int i = 2; i < array.length - 1; ++i) {
                b += array[i];
            }
            if ((byte)(b ^ 0xFF) == array[array.length - 1]) {
                return true;
            }
        }
        return false;
    }
    
    public void pushEvent(final byte[] array, final int n, final int n2) {
        final int n3 = array.length / 2;
        synchronized (ProtocolAnalyzer.eventLock) {
            System.arraycopy(array, n3, this.tmp, 0, this.tmp.length);
            this.handleEvent(this.tmp, n, n2);
        }
    }
    
    public void pushMcu(final byte[] array, final int n, final int n2) {
        final String string = "mcu:recv:<[" + Utils.byteArrayToHexString(array, n, n2) + "]>";
        Log.d("driverlog", string);
        this.logger.debug(string);
        this.analyzeMcu(array, n, n2);
    }
    
    public enum ETouch
    {
        ABS_X("ABS_X", 4, 53), 
        ABS_Y("ABS_Y", 5, 54), 
        BTN_TOUCH("BTN_TOUCH", 3, 330), 
        EV_ABS("EV_ABS", 2, 3), 
        EV_KEY("EV_KEY", 1, 1), 
        EV_SYN("EV_SYN", 0, 0), 
        SYN_REPORT("SYN_REPORT", 6, 2);
        
        private int code;
        
        private ETouch(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public int getCode() {
            return this.code;
        }
    }
}
