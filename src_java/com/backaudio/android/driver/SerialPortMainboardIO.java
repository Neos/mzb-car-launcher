package com.backaudio.android.driver;

import org.slf4j.*;
import android.util.*;
import java.io.*;

public class SerialPortMainboardIO
{
    private static SerialPortMainboardIO instance;
    private final String TAG;
    private Logger logger;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;
    
    static {
        SerialPortMainboardIO.instance = null;
    }
    
    public SerialPortMainboardIO() {
        this.TAG = "driverlog";
        this.logger = LoggerFactory.getLogger(SerialPortMainboardIO.class);
        this.mFileInputStream = null;
        this.mFileOutputStream = null;
        this.init(this);
    }
    
    public static SerialPortMainboardIO getInstance() {
        Label_0028: {
            if (SerialPortMainboardIO.instance != null) {
                break Label_0028;
            }
            synchronized (SerialPortMainboardIO.class) {
                if (SerialPortMainboardIO.instance == null) {
                    SerialPortMainboardIO.instance = new SerialPortMainboardIO();
                }
                return SerialPortMainboardIO.instance;
            }
        }
    }
    
    private void init(final SerialPortMainboardIO serialPortMainboardIO) {
        int n = 0;
        while (true) {
            try {
                final LibSerialPort libSerialPort = new LibSerialPort(new File("/dev/BT_serial"), 115200, 0);
                serialPortMainboardIO.mFileInputStream = libSerialPort.getmFileInputStream();
                serialPortMainboardIO.mFileOutputStream = libSerialPort.getmFileOutputStream();
            }
            catch (Exception ex2) {
                ++n;
                if (n <= 2) {
                    try {
                        Thread.sleep(1000L);
                    }
                    catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    continue;
                }
            }
            break;
        }
    }
    
    public int read(final byte[] array) throws IOException {
        if (this.mFileInputStream == null) {
            while (this.mFileInputStream == null) {
                this.init(this);
                if (this.mFileInputStream == null) {
                    Log.e("driverlog", "/dev/BT_serial cannot open");
                }
            }
        }
        return this.mFileInputStream.read(array);
    }
    
    public void write(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: new             Ljava/lang/StringBuilder;
        //     5: dup            
        //     6: ldc             "bluetoothprot: write->["
        //     8: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    11: aload_1        
        //    12: iconst_0       
        //    13: aload_1        
        //    14: arraylength    
        //    15: invokestatic    com/backaudio/android/driver/Utils.byteArrayToHexString:([BII)Ljava/lang/String;
        //    18: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    21: ldc             "]>"
        //    23: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    26: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    29: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //    32: pop            
        //    33: ldc             Lcom/backaudio/android/driver/SerialPortMainboardIO;.class
        //    35: monitorenter   
        //    36: aload_0        
        //    37: getfield        com/backaudio/android/driver/SerialPortMainboardIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    40: ifnonnull       67
        //    43: aload_0        
        //    44: aload_0        
        //    45: invokespecial   com/backaudio/android/driver/SerialPortMainboardIO.init:(Lcom/backaudio/android/driver/SerialPortMainboardIO;)V
        //    48: aload_0        
        //    49: getfield        com/backaudio/android/driver/SerialPortMainboardIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    52: ifnonnull       67
        //    55: ldc             "driverlog"
        //    57: ldc             "/dev/BT_serial cannot open"
        //    59: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //    62: pop            
        //    63: ldc             Lcom/backaudio/android/driver/SerialPortMainboardIO;.class
        //    65: monitorexit    
        //    66: return         
        //    67: aload_0        
        //    68: getfield        com/backaudio/android/driver/SerialPortMainboardIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    71: aload_1        
        //    72: invokevirtual   java/io/FileOutputStream.write:([B)V
        //    75: aload_0        
        //    76: getfield        com/backaudio/android/driver/SerialPortMainboardIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    79: invokevirtual   java/io/FileOutputStream.flush:()V
        //    82: ldc2_w          40
        //    85: invokestatic    java/lang/Thread.sleep:(J)V
        //    88: ldc             Lcom/backaudio/android/driver/SerialPortMainboardIO;.class
        //    90: monitorexit    
        //    91: return         
        //    92: astore_1       
        //    93: ldc             Lcom/backaudio/android/driver/SerialPortMainboardIO;.class
        //    95: monitorexit    
        //    96: aload_1        
        //    97: athrow         
        //    98: astore_1       
        //    99: ldc             "driverlog"
        //   101: new             Ljava/lang/StringBuilder;
        //   104: dup            
        //   105: ldc             "/dev/BT_serial write error "
        //   107: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   110: aload_1        
        //   111: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   114: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   117: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   120: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   123: pop            
        //   124: goto            82
        //   127: astore_1       
        //   128: goto            88
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  36     66     92     98     Any
        //  67     82     98     127    Ljava/lang/Exception;
        //  67     82     92     98     Any
        //  82     88     127    131    Ljava/lang/InterruptedException;
        //  82     88     92     98     Any
        //  88     91     92     98     Any
        //  93     96     92     98     Any
        //  99     124    92     98     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 66, Size: 66
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
