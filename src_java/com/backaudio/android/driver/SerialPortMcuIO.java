package com.backaudio.android.driver;

import org.slf4j.*;
import android.util.*;
import java.io.*;

public class SerialPortMcuIO
{
    private static SerialPortMcuIO instance;
    private final String TAG;
    private Logger logger;
    private FileInputStream mFileInputStream;
    private FileOutputStream mFileOutputStream;
    
    static {
        SerialPortMcuIO.instance = null;
    }
    
    public SerialPortMcuIO() {
        this.TAG = "driverlog";
        this.logger = LoggerFactory.getLogger(SerialPortMcuIO.class);
        this.mFileInputStream = null;
        this.mFileOutputStream = null;
        this.init();
    }
    
    public static SerialPortMcuIO getInstance() {
        Label_0028: {
            if (SerialPortMcuIO.instance != null) {
                break Label_0028;
            }
            synchronized (SerialPortMcuIO.class) {
                if (SerialPortMcuIO.instance == null) {
                    SerialPortMcuIO.instance = new SerialPortMcuIO();
                }
                return SerialPortMcuIO.instance;
            }
        }
    }
    
    private void init() {
        int n = 0;
        while (true) {
            try {
                while (true) {
                    final LibSerialPort libSerialPort = new LibSerialPort(new File("/dev/ttyMT3"), 115200, 0);
                    this.mFileInputStream = libSerialPort.getmFileInputStream();
                    this.mFileOutputStream = libSerialPort.getmFileOutputStream();
                }
            }
            catch (Exception ex2) {
                ++n;
                if (n > 2) {
                    return;
                }
                try {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                continue;
            }
            break;
        }
    }
    
    public int read(final byte[] array) throws IOException {
        if (this.mFileInputStream == null) {
            while (this.mFileInputStream == null) {
                this.init();
                if (this.mFileInputStream == null) {
                    Log.e("driverlog", "/dev/ttyMT3 cannot open serial port");
                }
            }
        }
        return this.mFileInputStream.read(array);
    }
    
    public void write(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: ldc             "mcu: write->["
        //     6: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //     9: aload_1        
        //    10: iconst_0       
        //    11: aload_1        
        //    12: arraylength    
        //    13: invokestatic    com/backaudio/android/driver/Utils.byteArrayToHexString:([BII)Ljava/lang/String;
        //    16: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    19: ldc             "]>"
        //    21: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    24: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    27: astore_2       
        //    28: aload_0        
        //    29: getfield        com/backaudio/android/driver/SerialPortMcuIO.logger:Lorg/slf4j/Logger;
        //    32: aload_2        
        //    33: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //    38: ldc             Lcom/backaudio/android/driver/SerialPortMcuIO;.class
        //    40: monitorenter   
        //    41: aload_0        
        //    42: getfield        com/backaudio/android/driver/SerialPortMcuIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    45: ifnonnull       71
        //    48: aload_0        
        //    49: invokespecial   com/backaudio/android/driver/SerialPortMcuIO.init:()V
        //    52: aload_0        
        //    53: getfield        com/backaudio/android/driver/SerialPortMcuIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    56: ifnonnull       71
        //    59: ldc             "driverlog"
        //    61: ldc             "/dev/ttyMT3 cannot open serial port"
        //    63: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //    66: pop            
        //    67: ldc             Lcom/backaudio/android/driver/SerialPortMcuIO;.class
        //    69: monitorexit    
        //    70: return         
        //    71: aload_0        
        //    72: getfield        com/backaudio/android/driver/SerialPortMcuIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    75: aload_1        
        //    76: invokevirtual   java/io/FileOutputStream.write:([B)V
        //    79: aload_0        
        //    80: getfield        com/backaudio/android/driver/SerialPortMcuIO.mFileOutputStream:Ljava/io/FileOutputStream;
        //    83: invokevirtual   java/io/FileOutputStream.flush:()V
        //    86: ldc2_w          50
        //    89: invokestatic    java/lang/Thread.sleep:(J)V
        //    92: ldc             Lcom/backaudio/android/driver/SerialPortMcuIO;.class
        //    94: monitorexit    
        //    95: return         
        //    96: astore_1       
        //    97: ldc             Lcom/backaudio/android/driver/SerialPortMcuIO;.class
        //    99: monitorexit    
        //   100: aload_1        
        //   101: athrow         
        //   102: astore_1       
        //   103: ldc             "driverlog"
        //   105: new             Ljava/lang/StringBuilder;
        //   108: dup            
        //   109: ldc             "/dev/ttyMT3 cannot write error: "
        //   111: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   114: aload_1        
        //   115: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   118: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   121: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   124: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   127: pop            
        //   128: goto            86
        //   131: astore_1       
        //   132: goto            92
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  41     70     96     102    Any
        //  71     86     102    131    Ljava/lang/Exception;
        //  71     86     96     102    Any
        //  86     92     131    135    Ljava/lang/InterruptedException;
        //  86     92     96     102    Any
        //  92     95     96     102    Any
        //  97     100    96     102    Any
        //  103    128    96     102    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 67, Size: 67
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
