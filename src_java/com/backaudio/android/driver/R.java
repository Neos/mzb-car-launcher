package com.backaudio.android.driver;

public final class R
{
    public static final class dimen
    {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }
    
    public static final class drawable
    {
        public static final int auto_navi_speed_bg_day = 2130837528;
        public static final int auto_navi_view_bus_day = 2130837529;
        public static final int auto_navi_view_camera_day = 2130837530;
        public static final int auto_navi_view_traffic_day = 2130837531;
        public static final int ic_launcher = 2130837720;
        public static final int sou10 = 2130837897;
        public static final int sou11 = 2130837898;
        public static final int sou12 = 2130837899;
        public static final int sou13 = 2130837900;
        public static final int sou14 = 2130837901;
        public static final int sou15 = 2130837902;
        public static final int sou16 = 2130837903;
        public static final int sou2 = 2130837904;
        public static final int sou3 = 2130837905;
        public static final int sou4 = 2130837906;
        public static final int sou5 = 2130837907;
        public static final int sou6 = 2130837908;
        public static final int sou7 = 2130837909;
        public static final int sou8 = 2130837910;
        public static final int sou9 = 2130837911;
        public static final int weather_daxuebaoxue = 2130837982;
        public static final int weather_dayubaoyu = 2130837983;
        public static final int weather_dongyu = 2130837984;
        public static final int weather_duoyun_baitian = 2130837985;
        public static final int weather_duoyun_yejian = 2130837986;
        public static final int weather_leizhenyu = 2130837987;
        public static final int weather_mai = 2130837988;
        public static final int weather_no = 2130837989;
        public static final int weather_qing_baitian = 2130837990;
        public static final int weather_qing_yejian = 2130837991;
        public static final int weather_shachenbao = 2130837992;
        public static final int weather_wu = 2130837993;
        public static final int weather_xiaoxuezhongxue = 2130837994;
        public static final int weather_xiaoyuzhongyu = 2130837995;
        public static final int weather_yin = 2130837996;
        public static final int weather_yujiaxue = 2130837997;
        public static final int weather_zhenxue_baitian = 2130837998;
        public static final int weather_zhenxue_yejian = 2130837999;
        public static final int weather_zhenyu_baitian = 2130838000;
        public static final int weather_zhenyu_yejian = 2130838001;
    }
    
    public static final class id
    {
        public static final int action_settings = 2131427684;
    }
    
    public static final class layout
    {
        public static final int activity_driver_main = 2130903040;
        public static final int activity_main = 2130903041;
    }
    
    public static final class menu
    {
        public static final int main = 2131361792;
    }
    
    public static final class string
    {
        public static final int action_settings = 2131165186;
        public static final int app_name = 2131165184;
        public static final int hello_world = 2131165185;
    }
    
    public static final class style
    {
        public static final int AppBaseTheme = 2131230720;
        public static final int AppTheme = 2131230721;
    }
}
