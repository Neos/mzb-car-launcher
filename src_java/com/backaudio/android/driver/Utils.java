package com.backaudio.android.driver;

import android.os.*;
import java.text.*;
import java.util.*;
import java.io.*;

public class Utils
{
    public static String AUDIO_SOURCE;
    public static String PATH;
    public static final int TIME_OUT = 1000;
    public static String VIDEO_SOURCE;
    public static final String aux_connect_flag = "/sys/bus/platform/drivers/image_sensor/tw8836_reg";
    public static final String collison_happen = "/sys/bus/i2c/drivers/DA380/da380_flag";
    public static final String collison_level = "/sys/bus/i2c/drivers/DA380/da380_sensitivity";
    private static final String droid_up = "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_droid_up";
    private static boolean flag = false;
    public static final String gpio_radar = "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_rada";
    private static String path;
    public static final String radio_frequency = "/sys/bus/i2c/drivers/qn8027/qn8027_mode";
    public static final String usb_protocol = "/sys/bus/platform/drivers/mt_usb/mt_usb/usb_protocol";
    
    static {
        Utils.PATH = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/unibroad/driver";
        Utils.AUDIO_SOURCE = String.valueOf(Utils.PATH) + "/audio_source.prop";
        Utils.VIDEO_SOURCE = String.valueOf(Utils.PATH) + "/video_source.prop";
        Utils.path = Environment.getExternalStorageDirectory().getAbsolutePath();
    }
    
    public static void bash(final String p0) throws Exception {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aload_0        
        //     4: aconst_null    
        //     5: new             Ljava/io/File;
        //     8: dup            
        //     9: ldc             "/system/bin"
        //    11: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    14: invokevirtual   java/lang/Runtime.exec:(Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;
        //    17: astore          7
        //    19: aload           7
        //    21: invokevirtual   java/lang/Process.getInputStream:()Ljava/io/InputStream;
        //    24: astore          6
        //    26: aconst_null    
        //    27: astore_3       
        //    28: aconst_null    
        //    29: astore_2       
        //    30: new             Ljava/io/BufferedReader;
        //    33: dup            
        //    34: new             Ljava/io/InputStreamReader;
        //    37: dup            
        //    38: aload           7
        //    40: invokevirtual   java/lang/Process.getErrorStream:()Ljava/io/InputStream;
        //    43: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    46: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    49: astore          4
        //    51: aload           4
        //    53: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    56: astore_2       
        //    57: aload_2        
        //    58: ifnonnull       184
        //    61: new             Ljava/io/BufferedReader;
        //    64: dup            
        //    65: new             Ljava/io/InputStreamReader;
        //    68: dup            
        //    69: aload           6
        //    71: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    74: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    77: astore          5
        //    79: aload           5
        //    81: astore_2       
        //    82: aload           5
        //    84: astore_3       
        //    85: aload           5
        //    87: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    90: astore          4
        //    92: aload           4
        //    94: ifnonnull       223
        //    97: aload           5
        //    99: astore_2       
        //   100: aload           5
        //   102: astore_3       
        //   103: aload           7
        //   105: invokevirtual   java/lang/Process.waitFor:()I
        //   108: pop            
        //   109: aload           5
        //   111: astore_2       
        //   112: aload           5
        //   114: astore_3       
        //   115: aload           7
        //   117: invokevirtual   java/lang/Process.exitValue:()I
        //   120: istore_1       
        //   121: iload_1        
        //   122: ifeq            283
        //   125: aload           5
        //   127: astore_2       
        //   128: aload           5
        //   130: astore_3       
        //   131: new             Ljava/lang/Exception;
        //   134: dup            
        //   135: new             Ljava/lang/StringBuilder;
        //   138: dup            
        //   139: ldc             "cmd ["
        //   141: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   144: aload_0        
        //   145: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   148: ldc             "]exit:"
        //   150: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   153: iload_1        
        //   154: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   157: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   160: invokespecial   java/lang/Exception.<init>:(Ljava/lang/String;)V
        //   163: athrow         
        //   164: astore_0       
        //   165: aload           6
        //   167: ifnull          175
        //   170: aload           6
        //   172: invokevirtual   java/io/InputStream.close:()V
        //   175: aload_2        
        //   176: ifnull          183
        //   179: aload_2        
        //   180: invokevirtual   java/io/BufferedReader.close:()V
        //   183: return         
        //   184: ldc             ""
        //   186: new             Ljava/lang/StringBuilder;
        //   189: dup            
        //   190: aload_0        
        //   191: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   194: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   197: ldc             "\r\n"
        //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   202: aload_2        
        //   203: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   206: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   209: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   212: pop            
        //   213: goto            51
        //   216: astore_0       
        //   217: aload           4
        //   219: astore_2       
        //   220: goto            165
        //   223: aload           5
        //   225: astore_2       
        //   226: aload           5
        //   228: astore_3       
        //   229: ldc             ""
        //   231: new             Ljava/lang/StringBuilder;
        //   234: dup            
        //   235: aload_0        
        //   236: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   239: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   242: ldc             "\r\n"
        //   244: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   247: aload           4
        //   249: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   252: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   255: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   258: pop            
        //   259: goto            79
        //   262: astore_0       
        //   263: aload           6
        //   265: ifnull          273
        //   268: aload           6
        //   270: invokevirtual   java/io/InputStream.close:()V
        //   273: aload_3        
        //   274: ifnull          281
        //   277: aload_3        
        //   278: invokevirtual   java/io/BufferedReader.close:()V
        //   281: aload_0        
        //   282: athrow         
        //   283: aload           6
        //   285: ifnull          293
        //   288: aload           6
        //   290: invokevirtual   java/io/InputStream.close:()V
        //   293: aload           5
        //   295: ifnull          183
        //   298: aload           5
        //   300: invokevirtual   java/io/BufferedReader.close:()V
        //   303: return         
        //   304: astore_0       
        //   305: aload           4
        //   307: astore_3       
        //   308: goto            263
        //    Exceptions:
        //  throws java.lang.Exception
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  30     51     164    165    Ljava/lang/Exception;
        //  30     51     262    263    Any
        //  51     57     216    223    Ljava/lang/Exception;
        //  51     57     304    311    Any
        //  61     79     216    223    Ljava/lang/Exception;
        //  61     79     304    311    Any
        //  85     92     164    165    Ljava/lang/Exception;
        //  85     92     262    263    Any
        //  103    109    164    165    Ljava/lang/Exception;
        //  103    109    262    263    Any
        //  115    121    164    165    Ljava/lang/Exception;
        //  115    121    262    263    Any
        //  131    164    164    165    Ljava/lang/Exception;
        //  131    164    262    263    Any
        //  184    213    216    223    Ljava/lang/Exception;
        //  184    213    304    311    Any
        //  229    259    164    165    Ljava/lang/Exception;
        //  229    259    262    263    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0051:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String byteArrayToHexString(final byte[] array) {
        final StringBuilder sb = new StringBuilder("");
        if (array == null || array.length <= 0) {
            return null;
        }
        for (int i = 0; i < array.length; ++i) {
            final String hexString = Integer.toHexString(array[i] & 0xFF);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString();
    }
    
    public static String byteArrayToHexString(final byte[] array, int n, final int n2) {
        final StringBuilder sb = new StringBuilder("");
        if (array == null || array.length <= 0) {
            return null;
        }
        int n3;
        String hexString;
        for (n3 = n, n = 0; n3 < array.length && n < n2; ++n3, ++n) {
            hexString = Integer.toHexString(array[n3] & 0xFF);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString();
    }
    
    public static byte[] calcCheckSum(final byte[] array) {
        if (array.length < 5) {
            System.out.println("invalid buffer length:" + byteArrayToHexString(array));
        }
        byte b = array[2];
        for (int i = 3; i < array.length - 1; ++i) {
            b += array[i];
        }
        array[array.length - 1] = b;
        return array;
    }
    
    public static String catFile(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aconst_null    
        //     3: astore_2       
        //     4: new             Ljava/io/File;
        //     7: dup            
        //     8: aload_0        
        //     9: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    12: astore_0       
        //    13: aload_0        
        //    14: invokevirtual   java/io/File.exists:()Z
        //    17: ifeq            95
        //    20: aload_0        
        //    21: invokevirtual   java/io/File.canRead:()Z
        //    24: ifeq            95
        //    27: new             Ljava/io/FileInputStream;
        //    30: dup            
        //    31: aload_0        
        //    32: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    35: astore_0       
        //    36: aload_0        
        //    37: invokevirtual   java/io/FileInputStream.available:()I
        //    40: newarray        B
        //    42: astore_1       
        //    43: new             Ljava/lang/String;
        //    46: dup            
        //    47: aload_1        
        //    48: iconst_0       
        //    49: aload_0        
        //    50: aload_1        
        //    51: iconst_0       
        //    52: aload_1        
        //    53: arraylength    
        //    54: invokevirtual   java/io/FileInputStream.read:([BII)I
        //    57: invokespecial   java/lang/String.<init>:([BII)V
        //    60: astore_1       
        //    61: aload_0        
        //    62: ifnull          69
        //    65: aload_0        
        //    66: invokevirtual   java/io/FileInputStream.close:()V
        //    69: aload_1        
        //    70: areturn        
        //    71: astore_0       
        //    72: aload_2        
        //    73: astore_0       
        //    74: aload_0        
        //    75: ifnull          82
        //    78: aload_0        
        //    79: invokevirtual   java/io/FileInputStream.close:()V
        //    82: aconst_null    
        //    83: areturn        
        //    84: astore_0       
        //    85: aload_1        
        //    86: ifnull          93
        //    89: aload_1        
        //    90: invokevirtual   java/io/FileInputStream.close:()V
        //    93: aload_0        
        //    94: athrow         
        //    95: iconst_0       
        //    96: ifeq            82
        //    99: new             Ljava/lang/NullPointerException;
        //   102: dup            
        //   103: invokespecial   java/lang/NullPointerException.<init>:()V
        //   106: athrow         
        //   107: astore_0       
        //   108: goto            82
        //   111: astore_0       
        //   112: goto            69
        //   115: astore_0       
        //   116: goto            82
        //   119: astore_1       
        //   120: goto            93
        //   123: astore_2       
        //   124: aload_0        
        //   125: astore_1       
        //   126: aload_2        
        //   127: astore_0       
        //   128: goto            85
        //   131: astore_1       
        //   132: goto            74
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      36     71     74     Ljava/lang/Exception;
        //  4      36     84     85     Any
        //  36     61     131    135    Ljava/lang/Exception;
        //  36     61     123    131    Any
        //  65     69     111    115    Ljava/lang/Exception;
        //  78     82     115    119    Ljava/lang/Exception;
        //  89     93     119    123    Ljava/lang/Exception;
        //  99     107    107    111    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 80, Size: 80
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static final void closeEdog() {
        echoFile("0", "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_rada");
    }
    
    public static final void closeRadioFrequency() {
        echoFile("0", "/sys/bus/i2c/drivers/qn8027/qn8027_mode");
    }
    
    public static boolean deleteDirectory(final String s) {
        String string = s;
        if (!s.endsWith(File.separator)) {
            string = String.valueOf(s) + File.separator;
        }
        final File file = new File(string);
        if (file.exists() && file.isDirectory()) {
            Utils.flag = true;
            final File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; ++i) {
                if (listFiles[i].isFile()) {
                    Utils.flag = deleteFile(listFiles[i].getAbsolutePath());
                    if (!Utils.flag) {
                        break;
                    }
                }
                else if (!(Utils.flag = deleteDirectory(listFiles[i].getAbsolutePath()))) {
                    break;
                }
            }
            if (Utils.flag && file.delete()) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean deleteFile(final String s) {
        final File file = new File(s);
        if (file.isFile() && file.exists()) {
            file.delete();
            Utils.flag = true;
        }
        return Utils.flag;
    }
    
    public static boolean echoFile(final String p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: new             Ljava/lang/StringBuilder;
        //     5: dup            
        //     6: ldc             "echoFile: "
        //     8: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    11: aload_0        
        //    12: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    15: ldc             " path: "
        //    17: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    20: aload_1        
        //    21: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    24: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    27: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //    30: pop            
        //    31: aconst_null    
        //    32: astore          4
        //    34: aconst_null    
        //    35: astore_3       
        //    36: aload           4
        //    38: astore_2       
        //    39: new             Ljava/io/File;
        //    42: dup            
        //    43: aload_1        
        //    44: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    47: astore_1       
        //    48: aload           4
        //    50: astore_2       
        //    51: aload_1        
        //    52: invokevirtual   java/io/File.exists:()Z
        //    55: ifeq            137
        //    58: aload           4
        //    60: astore_2       
        //    61: aload_1        
        //    62: invokevirtual   java/io/File.canWrite:()Z
        //    65: ifeq            137
        //    68: aload           4
        //    70: astore_2       
        //    71: new             Ljava/io/FileOutputStream;
        //    74: dup            
        //    75: aload_1        
        //    76: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //    79: astore_1       
        //    80: aload_1        
        //    81: aload_0        
        //    82: invokevirtual   java/lang/String.getBytes:()[B
        //    85: invokevirtual   java/io/FileOutputStream.write:([B)V
        //    88: aload_1        
        //    89: invokevirtual   java/io/FileOutputStream.flush:()V
        //    92: aload_1        
        //    93: ifnull          100
        //    96: aload_1        
        //    97: invokevirtual   java/io/FileOutputStream.close:()V
        //   100: iconst_1       
        //   101: ireturn        
        //   102: astore_0       
        //   103: aload_3        
        //   104: astore_0       
        //   105: aload_0        
        //   106: astore_2       
        //   107: ldc             "driverlog"
        //   109: ldc_w           "echofile err----------"
        //   112: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   115: pop            
        //   116: aload_0        
        //   117: ifnull          124
        //   120: aload_0        
        //   121: invokevirtual   java/io/FileOutputStream.close:()V
        //   124: iconst_0       
        //   125: ireturn        
        //   126: astore_0       
        //   127: aload_2        
        //   128: ifnull          135
        //   131: aload_2        
        //   132: invokevirtual   java/io/FileOutputStream.close:()V
        //   135: aload_0        
        //   136: athrow         
        //   137: iconst_0       
        //   138: ifeq            124
        //   141: new             Ljava/lang/NullPointerException;
        //   144: dup            
        //   145: invokespecial   java/lang/NullPointerException.<init>:()V
        //   148: athrow         
        //   149: astore_0       
        //   150: goto            124
        //   153: astore_0       
        //   154: goto            100
        //   157: astore_0       
        //   158: goto            124
        //   161: astore_1       
        //   162: goto            135
        //   165: astore_0       
        //   166: aload_1        
        //   167: astore_2       
        //   168: goto            127
        //   171: astore_0       
        //   172: aload_1        
        //   173: astore_0       
        //   174: goto            105
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  39     48     102    105    Ljava/lang/Exception;
        //  39     48     126    127    Any
        //  51     58     102    105    Ljava/lang/Exception;
        //  51     58     126    127    Any
        //  61     68     102    105    Ljava/lang/Exception;
        //  61     68     126    127    Any
        //  71     80     102    105    Ljava/lang/Exception;
        //  71     80     126    127    Any
        //  80     92     171    177    Ljava/lang/Exception;
        //  80     92     165    171    Any
        //  96     100    153    157    Ljava/lang/Exception;
        //  107    116    126    127    Any
        //  120    124    157    161    Ljava/lang/Exception;
        //  131    135    161    165    Ljava/lang/Exception;
        //  141    149    149    153    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 98, Size: 98
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static final String getAuxConnect() {
        return catFile("/sys/bus/platform/drivers/image_sensor/tw8836_reg");
    }
    
    public static final String getCollisonHappenState() {
        return catFile("/sys/bus/i2c/drivers/DA380/da380_flag");
    }
    
    public static String getSource(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_3       
        //     2: aconst_null    
        //     3: astore          4
        //     5: new             Ljava/io/File;
        //     8: dup            
        //     9: aload_0        
        //    10: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    13: astore_0       
        //    14: aload_0        
        //    15: invokevirtual   java/io/File.exists:()Z
        //    18: istore_2       
        //    19: iload_2        
        //    20: ifne            38
        //    23: iconst_0       
        //    24: ifeq            35
        //    27: new             Ljava/lang/NullPointerException;
        //    30: dup            
        //    31: invokespecial   java/lang/NullPointerException.<init>:()V
        //    34: athrow         
        //    35: ldc             ""
        //    37: areturn        
        //    38: new             Ljava/io/FileInputStream;
        //    41: dup            
        //    42: aload_0        
        //    43: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    46: astore_0       
        //    47: aload_0        
        //    48: invokevirtual   java/io/FileInputStream.available:()I
        //    51: newarray        B
        //    53: astore_3       
        //    54: aload_0        
        //    55: aload_3        
        //    56: invokevirtual   java/io/FileInputStream.read:([B)I
        //    59: istore_1       
        //    60: iload_1        
        //    61: ifne            75
        //    64: aload_0        
        //    65: ifnull          72
        //    68: aload_0        
        //    69: invokevirtual   java/io/FileInputStream.close:()V
        //    72: ldc             ""
        //    74: areturn        
        //    75: new             Ljava/lang/String;
        //    78: dup            
        //    79: aload_3        
        //    80: invokespecial   java/lang/String.<init>:([B)V
        //    83: astore_3       
        //    84: aload_0        
        //    85: ifnull          92
        //    88: aload_0        
        //    89: invokevirtual   java/io/FileInputStream.close:()V
        //    92: aload_3        
        //    93: areturn        
        //    94: astore_0       
        //    95: aload           4
        //    97: astore_0       
        //    98: aload_0        
        //    99: ifnull          106
        //   102: aload_0        
        //   103: invokevirtual   java/io/FileInputStream.close:()V
        //   106: ldc             ""
        //   108: areturn        
        //   109: astore_0       
        //   110: aload_3        
        //   111: ifnull          118
        //   114: aload_3        
        //   115: invokevirtual   java/io/FileInputStream.close:()V
        //   118: aload_0        
        //   119: athrow         
        //   120: astore_0       
        //   121: goto            35
        //   124: astore_0       
        //   125: goto            72
        //   128: astore_0       
        //   129: goto            92
        //   132: astore_0       
        //   133: goto            106
        //   136: astore_3       
        //   137: goto            118
        //   140: astore          4
        //   142: aload_0        
        //   143: astore_3       
        //   144: aload           4
        //   146: astore_0       
        //   147: goto            110
        //   150: astore_3       
        //   151: goto            98
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  5      19     94     98     Ljava/lang/Exception;
        //  5      19     109    110    Any
        //  27     35     120    124    Ljava/lang/Exception;
        //  38     47     94     98     Ljava/lang/Exception;
        //  38     47     109    110    Any
        //  47     60     150    154    Ljava/lang/Exception;
        //  47     60     140    150    Any
        //  68     72     124    128    Ljava/lang/Exception;
        //  75     84     150    154    Ljava/lang/Exception;
        //  75     84     140    150    Any
        //  88     92     128    132    Ljava/lang/Exception;
        //  102    106    132    136    Ljava/lang/Exception;
        //  114    118    136    140    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 88, Size: 88
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int hex2int(final char c) {
        return "0123456789abcdef".indexOf(c);
    }
    
    public static double logFileSize(final File file) {
        if (!file.exists()) {
            return 0.0;
        }
        if (file.isDirectory()) {
            final File[] listFiles = file.listFiles();
            double n = 0.0;
            for (int length = listFiles.length, i = 0; i < length; ++i) {
                n += logFileSize(listFiles[i]);
            }
            return n;
        }
        return file.length() / 1024.0 / 1024.0;
    }
    
    public static final void openEdog() {
        echoFile("1", "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_rada");
    }
    
    public static final void openRadioFrequency() {
        echoFile("1", "/sys/bus/i2c/drivers/qn8027/qn8027_mode");
    }
    
    public static final void readToWork() {
        echoFile("1", "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_droid_up");
    }
    
    public static String saveLogLine(final String s, final boolean b) throws IOException {
        final String string = "log-" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()).substring(0, 10) + "-" + System.currentTimeMillis() / 1000L / 60L / 60L + ".log";
        String s2;
        if (b) {
            s2 = String.valueOf(Utils.path) + "/unibroad/benzbluetoothlog_byte/";
        }
        else {
            s2 = String.valueOf(Utils.path) + "/unibroad/benzbluetoothlog_line/";
        }
        if (Environment.getExternalStorageState().equals("mounted")) {
            final File file = new File(s2);
            if (logFileSize(file) > 5.0) {
                deleteDirectory(s2);
            }
            if (!file.exists()) {
                file.mkdirs();
            }
            final FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(s2) + string, true);
            fileOutputStream.write(s.toString().getBytes());
            fileOutputStream.write(new byte[] { 13, 10 });
            fileOutputStream.close();
        }
        return string;
    }
    
    public static boolean saveSource(final String p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aconst_null    
        //     3: astore_3       
        //     4: new             Ljava/io/File;
        //     7: dup            
        //     8: aload_0        
        //     9: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    12: astore_0       
        //    13: aload_0        
        //    14: invokevirtual   java/io/File.exists:()Z
        //    17: ifne            33
        //    20: aload_0        
        //    21: invokevirtual   java/io/File.getParentFile:()Ljava/io/File;
        //    24: invokevirtual   java/io/File.mkdirs:()Z
        //    27: pop            
        //    28: aload_0        
        //    29: invokevirtual   java/io/File.createNewFile:()Z
        //    32: pop            
        //    33: new             Ljava/io/FileOutputStream;
        //    36: dup            
        //    37: aload_0        
        //    38: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //    41: astore_0       
        //    42: aload_0        
        //    43: aload_1        
        //    44: invokevirtual   java/lang/String.getBytes:()[B
        //    47: invokevirtual   java/io/FileOutputStream.write:([B)V
        //    50: aload_0        
        //    51: invokevirtual   java/io/FileOutputStream.flush:()V
        //    54: aload_0        
        //    55: ifnull          62
        //    58: aload_0        
        //    59: invokevirtual   java/io/FileOutputStream.close:()V
        //    62: iconst_1       
        //    63: ireturn        
        //    64: astore_0       
        //    65: aload_3        
        //    66: astore_0       
        //    67: aload_0        
        //    68: ifnull          75
        //    71: aload_0        
        //    72: invokevirtual   java/io/FileOutputStream.close:()V
        //    75: iconst_0       
        //    76: ireturn        
        //    77: astore_0       
        //    78: aload_2        
        //    79: astore_1       
        //    80: aload_1        
        //    81: ifnull          88
        //    84: aload_1        
        //    85: invokevirtual   java/io/FileOutputStream.close:()V
        //    88: aload_0        
        //    89: athrow         
        //    90: astore_0       
        //    91: goto            75
        //    94: astore_1       
        //    95: goto            88
        //    98: astore_0       
        //    99: goto            62
        //   102: astore_2       
        //   103: aload_0        
        //   104: astore_1       
        //   105: aload_2        
        //   106: astore_0       
        //   107: goto            80
        //   110: astore_1       
        //   111: goto            67
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      33     64     67     Ljava/lang/Exception;
        //  4      33     77     80     Any
        //  33     42     64     67     Ljava/lang/Exception;
        //  33     42     77     80     Any
        //  42     54     110    114    Ljava/lang/Exception;
        //  42     54     102    110    Any
        //  58     62     98     102    Ljava/lang/Exception;
        //  71     75     90     94     Ljava/lang/Exception;
        //  84     88     94     98     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0062:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static final void setCollisonCheckLevel(final String s) {
        echoFile(s, "/sys/bus/i2c/drivers/DA380/da380_sensitivity");
    }
    
    public static final void setCollisonHappenState(final String s) {
        echoFile(s, "/sys/bus/i2c/drivers/DA380/da380_flag");
    }
    
    public static void settingReveringLight(final String s) {
        echoFile("ff 00", "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
        echoFile(s, "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
    }
    
    public static int stringToByte(final String s, final byte[] array) throws Exception {
        if (array.length < s.length() / 2) {
            throw new Exception("byte array too small");
        }
        int n = 0;
        final StringBuffer sb = new StringBuffer(2);
        for (int i = 0; i < s.length(); i = i + 1 + 1, ++n) {
            sb.insert(0, s.charAt(i));
            sb.insert(1, s.charAt(i + 1));
            final int int1 = Integer.parseInt(sb.toString(), 16);
            System.out.println("byte hex value:" + int1);
            array[n] = (byte)int1;
            sb.delete(0, 2);
        }
        return n;
    }
    
    public static void switch2AUXSource() {
        echoFile("ff 01", "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
        echoFile("02 4c", "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
    }
    
    public static void switch2NoSource() {
        echoFile("ff 01", "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
    }
    
    public static void switch2ReversingSource() {
        echoFile("ff 01", "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
        echoFile("02 44", "/sys/bus/platform/drivers/image_sensor/tw8836_reg");
    }
    
    public static void system(final String p0) throws Exception {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aload_0        
        //     4: invokevirtual   java/lang/Runtime.exec:(Ljava/lang/String;)Ljava/lang/Process;
        //     7: astore          7
        //     9: aload           7
        //    11: invokevirtual   java/lang/Process.getInputStream:()Ljava/io/InputStream;
        //    14: astore          6
        //    16: aconst_null    
        //    17: astore_3       
        //    18: aconst_null    
        //    19: astore_2       
        //    20: new             Ljava/io/BufferedReader;
        //    23: dup            
        //    24: new             Ljava/io/InputStreamReader;
        //    27: dup            
        //    28: aload           7
        //    30: invokevirtual   java/lang/Process.getErrorStream:()Ljava/io/InputStream;
        //    33: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    36: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    39: astore          4
        //    41: aload           4
        //    43: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    46: astore_2       
        //    47: aload_2        
        //    48: ifnonnull       174
        //    51: new             Ljava/io/BufferedReader;
        //    54: dup            
        //    55: new             Ljava/io/InputStreamReader;
        //    58: dup            
        //    59: aload           6
        //    61: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    64: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    67: astore          5
        //    69: aload           5
        //    71: astore_2       
        //    72: aload           5
        //    74: astore_3       
        //    75: aload           5
        //    77: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    80: astore          4
        //    82: aload           4
        //    84: ifnonnull       213
        //    87: aload           5
        //    89: astore_2       
        //    90: aload           5
        //    92: astore_3       
        //    93: aload           7
        //    95: invokevirtual   java/lang/Process.waitFor:()I
        //    98: pop            
        //    99: aload           5
        //   101: astore_2       
        //   102: aload           5
        //   104: astore_3       
        //   105: aload           7
        //   107: invokevirtual   java/lang/Process.exitValue:()I
        //   110: istore_1       
        //   111: iload_1        
        //   112: ifeq            273
        //   115: aload           5
        //   117: astore_2       
        //   118: aload           5
        //   120: astore_3       
        //   121: new             Ljava/lang/Exception;
        //   124: dup            
        //   125: new             Ljava/lang/StringBuilder;
        //   128: dup            
        //   129: ldc             "cmd ["
        //   131: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   134: aload_0        
        //   135: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   138: ldc             "]exit:"
        //   140: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   143: iload_1        
        //   144: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   147: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   150: invokespecial   java/lang/Exception.<init>:(Ljava/lang/String;)V
        //   153: athrow         
        //   154: astore_0       
        //   155: aload           6
        //   157: ifnull          165
        //   160: aload           6
        //   162: invokevirtual   java/io/InputStream.close:()V
        //   165: aload_2        
        //   166: ifnull          173
        //   169: aload_2        
        //   170: invokevirtual   java/io/BufferedReader.close:()V
        //   173: return         
        //   174: ldc             ""
        //   176: new             Ljava/lang/StringBuilder;
        //   179: dup            
        //   180: aload_0        
        //   181: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   184: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   187: ldc             "\r\n"
        //   189: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   192: aload_2        
        //   193: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   196: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   199: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   202: pop            
        //   203: goto            41
        //   206: astore_0       
        //   207: aload           4
        //   209: astore_2       
        //   210: goto            155
        //   213: aload           5
        //   215: astore_2       
        //   216: aload           5
        //   218: astore_3       
        //   219: ldc             ""
        //   221: new             Ljava/lang/StringBuilder;
        //   224: dup            
        //   225: aload_0        
        //   226: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   229: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   232: ldc             "\r\n"
        //   234: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   237: aload           4
        //   239: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   242: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   245: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   248: pop            
        //   249: goto            69
        //   252: astore_0       
        //   253: aload           6
        //   255: ifnull          263
        //   258: aload           6
        //   260: invokevirtual   java/io/InputStream.close:()V
        //   263: aload_3        
        //   264: ifnull          271
        //   267: aload_3        
        //   268: invokevirtual   java/io/BufferedReader.close:()V
        //   271: aload_0        
        //   272: athrow         
        //   273: aload           6
        //   275: ifnull          283
        //   278: aload           6
        //   280: invokevirtual   java/io/InputStream.close:()V
        //   283: aload           5
        //   285: ifnull          173
        //   288: aload           5
        //   290: invokevirtual   java/io/BufferedReader.close:()V
        //   293: return         
        //   294: astore_0       
        //   295: aload           4
        //   297: astore_3       
        //   298: goto            253
        //    Exceptions:
        //  throws java.lang.Exception
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  20     41     154    155    Ljava/lang/Exception;
        //  20     41     252    253    Any
        //  41     47     206    213    Ljava/lang/Exception;
        //  41     47     294    301    Any
        //  51     69     206    213    Ljava/lang/Exception;
        //  51     69     294    301    Any
        //  75     82     154    155    Ljava/lang/Exception;
        //  75     82     252    253    Any
        //  93     99     154    155    Ljava/lang/Exception;
        //  93     99     252    253    Any
        //  105    111    154    155    Ljava/lang/Exception;
        //  105    111    252    253    Any
        //  121    154    154    155    Ljava/lang/Exception;
        //  121    154    252    253    Any
        //  174    203    206    213    Ljava/lang/Exception;
        //  174    203    294    301    Any
        //  219    249    154    155    Ljava/lang/Exception;
        //  219    249    252    253    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0041:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
