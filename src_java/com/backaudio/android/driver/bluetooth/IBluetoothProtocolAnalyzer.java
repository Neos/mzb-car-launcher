package com.backaudio.android.driver.bluetooth;

public interface IBluetoothProtocolAnalyzer
{
    void push(final byte[] p0, final int p1, final int p2);
    
    void reset();
    
    void setEventHandler(final IBluetoothEventHandler p0);
}
