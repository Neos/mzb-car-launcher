package com.backaudio.android.driver.bluetooth;

import java.text.*;
import org.slf4j.*;
import java.util.*;
import com.backaudio.android.driver.*;

public class Bluetooth
{
    private static final String GPIO_BT = "/sys/bus/platform/drivers/unibroad_gpio_control/gpio_bt";
    private static Bluetooth instance;
    private static boolean isRenGao;
    private static Logger logger;
    private static SimpleDateFormat sdf;
    private IBluetoothProtocolAnalyzer bluetoothProtocolAnalyzer;
    private List<IBluetoothEventHandler> eventHandlers;
    
    static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton() {
        final int[] $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton = Bluetooth.$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton;
        if ($switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton != null) {
            return $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton;
        }
        final int[] $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2 = new int[EVirtualButton.values().length];
        while (true) {
            try {
                $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.ASTERISK.ordinal()] = 11;
                try {
                    $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.EIGHT.ordinal()] = 8;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.FIVE.ordinal()] = 5;
                        try {
                            $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.FOUR.ordinal()] = 4;
                            try {
                                $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.NINE.ordinal()] = 9;
                                try {
                                    $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.ONE.ordinal()] = 1;
                                    try {
                                        $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.SEVEN.ordinal()] = 7;
                                        try {
                                            $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.SIX.ordinal()] = 6;
                                            try {
                                                $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.THREE.ordinal()] = 3;
                                                try {
                                                    $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.TWO.ordinal()] = 2;
                                                    try {
                                                        $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.WELL.ordinal()] = 12;
                                                        try {
                                                            $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.ZERO.ordinal()] = 10;
                                                            return Bluetooth.$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton = $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2;
                                                        }
                                                        catch (NoSuchFieldError noSuchFieldError) {}
                                                    }
                                                    catch (NoSuchFieldError noSuchFieldError2) {}
                                                }
                                                catch (NoSuchFieldError noSuchFieldError3) {}
                                            }
                                            catch (NoSuchFieldError noSuchFieldError4) {}
                                        }
                                        catch (NoSuchFieldError noSuchFieldError5) {}
                                    }
                                    catch (NoSuchFieldError noSuchFieldError6) {}
                                }
                                catch (NoSuchFieldError noSuchFieldError7) {}
                            }
                            catch (NoSuchFieldError noSuchFieldError8) {}
                        }
                        catch (NoSuchFieldError noSuchFieldError9) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError10) {}
                }
                catch (NoSuchFieldError noSuchFieldError11) {}
            }
            catch (NoSuchFieldError noSuchFieldError12) {
                continue;
            }
            break;
        }
    }
    
    static {
        Bluetooth.logger = LoggerFactory.getLogger(Bluetooth.class);
        Bluetooth.isRenGao = false;
        Bluetooth.instance = null;
        Bluetooth.sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
    }
    
    private Bluetooth() throws Exception {
        this.eventHandlers = null;
        this.bluetoothProtocolAnalyzer = null;
        this.eventHandlers = new ArrayList<IBluetoothEventHandler>();
        if (Bluetooth.isRenGao) {
            this.bluetoothProtocolAnalyzer = new Bc8mpBluetoothProtocolAnalyzer();
            return;
        }
        this.bluetoothProtocolAnalyzer = new BluetoothProtocolAnalyzer2();
    }
    
    public static Bluetooth getInstance() throws Exception {
        synchronized (Bluetooth.class) {
            if (Bluetooth.instance == null) {
                Bluetooth.instance = new Bluetooth();
            }
            return Bluetooth.instance;
        }
    }
    
    private void ioWrite(final String s) throws Exception {
        final byte[] bytes = s.getBytes();
        Bluetooth.logger.debug("send cmd:" + new String(bytes));
        this.writeBluetooth(bytes);
    }
    
    private void sendVirutalButton(final EVirtualButton eVirtualButton, final String s, final String s2) throws Exception {
        switch ($SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton()[eVirtualButton.ordinal()]) {
            default: {}
            case 10: {
                this.ioWrite(String.valueOf(s) + "0" + s2);
            }
            case 1: {
                this.ioWrite(String.valueOf(s) + "1" + s2);
            }
            case 2: {
                this.ioWrite(String.valueOf(s) + "2" + s2);
            }
            case 3: {
                this.ioWrite(String.valueOf(s) + "3" + s2);
            }
            case 4: {
                this.ioWrite(String.valueOf(s) + "4" + s2);
            }
            case 5: {
                this.ioWrite(String.valueOf(s) + "5" + s2);
            }
            case 6: {
                this.ioWrite(String.valueOf(s) + "6" + s2);
            }
            case 7: {
                this.ioWrite(String.valueOf(s) + "7" + s2);
            }
            case 8: {
                this.ioWrite(String.valueOf(s) + "8" + s2);
            }
            case 9: {
                this.ioWrite(String.valueOf(s) + "9" + s2);
            }
            case 11: {
                this.ioWrite(String.valueOf(s) + "*" + s2);
            }
            case 12: {
                this.ioWrite(String.valueOf(s) + "#" + s2);
            }
        }
    }
    
    public void SetBoot() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+BOOT\r\n");
            return;
        }
        this.ioWrite("AT#CC\r\n");
    }
    
    public void addEventHandler(final IBluetoothEventHandler eventHandler) {
        if (eventHandler != null && eventHandler != null && !this.eventHandlers.contains(eventHandler)) {
            this.eventHandlers.add(eventHandler);
            this.bluetoothProtocolAnalyzer.setEventHandler(eventHandler);
        }
    }
    
    public void answerThePhone() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFANSW\r\n");
            return;
        }
        this.ioWrite("AT#CE\r\n");
    }
    
    public void call(final String s) throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFDIAL=" + s + "\r\n");
            return;
        }
        this.ioWrite("AT#CW" + s + "\r\n");
    }
    
    public void cancelDownloadPhoneBook() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+PBABORT\r\n");
            return;
        }
        this.ioWrite("AT#PS\r\n");
    }
    
    public void clearPairingList() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+DLPD=000000000000");
            return;
        }
        this.ioWrite("AT#CV\r\n");
    }
    
    public void connectDeviceSync(String replace) throws Exception {
        replace = replace.replace("\r\n", "");
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+CPDL=" + replace + "\r\n");
            return;
        }
        this.ioWrite("AT#CC" + replace + "\r\n");
    }
    
    public void decVolume() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+VDN\r\n");
            return;
        }
        this.ioWrite("AT#CL\r\n");
    }
    
    public void disconnectCurrentDevice() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+DSCA\r\n");
            return;
        }
        this.ioWrite("AT#CD\r\n");
    }
    
    public void downloadPhoneBookSync(final int n) throws Exception {
        if (Bluetooth.isRenGao) {
            switch (n) {
                default: {
                    this.ioWrite("AT+PBDOWN=1\r\n");
                }
                case 1:
                case 2:
                case 3:
                case 4:
                case 5: {
                    this.ioWrite("AT+PBDOWN=" + n + "\r\n");
                }
            }
        }
        else {
            switch (n) {
                default: {
                    this.ioWrite("AT#PA\r\n");
                }
                case 1: {
                    this.ioWrite("AT#PA\r\n");
                }
                case 2: {
                    this.ioWrite("AT#PH\r\n");
                }
                case 3: {
                    this.ioWrite("AT#PI\r\n");
                }
                case 4: {
                    this.ioWrite("AT#PJ\r\n");
                }
                case 5: {
                    this.ioWrite("AT#PX\r\n");
                }
            }
        }
    }
    
    public void enterPairingModeSync() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+EPRM=1\r\n");
            return;
        }
        this.ioWrite("AT#CA\r\n");
    }
    
    public void hangUpThePhone() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFCHUP\r\n");
            return;
        }
        this.ioWrite("AT#CF\r\n");
    }
    
    public void incVolume() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+VUP\r\n");
            return;
        }
        this.ioWrite("AT#CK\r\n");
    }
    
    public void leavePairingModeSync() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+EPRM=0\r\n");
            return;
        }
        this.ioWrite("AT#CB\r\n");
    }
    
    public void pausePlaySync(final boolean b) throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+PP\r\n");
            return;
        }
        if (b) {
            this.ioWrite("AT#MS\r\n");
            return;
        }
        this.ioWrite("AT#MB\r\n");
    }
    
    public void pauseSync() throws Exception {
        if (!Bluetooth.isRenGao) {
            this.ioWrite("AT#MB\r\n");
        }
    }
    
    public void playNext() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+FWD\r\n");
            return;
        }
        this.ioWrite("AT#MD\r\n");
    }
    
    public void playPrev() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+BACK\r\n");
            return;
        }
        this.ioWrite("AT#ME\r\n");
    }
    
    public void pressVirutalButton(final EVirtualButton eVirtualButton) throws Exception {
        if (Bluetooth.isRenGao) {
            this.sendVirutalButton(eVirtualButton, "AT+HFDTMF=", "\r\n");
            return;
        }
        this.sendVirutalButton(eVirtualButton, "AT#CX", "\r\n");
    }
    
    public void push(final byte[] array) {
        Bluetooth.logger.debug("rec cmd:" + Utils.byteArrayToHexString(array, 0, array.length));
        Bluetooth.logger.debug("push--------------");
    }
    
    public void queryPhoneStatus() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFPSTAT\r\n");
            return;
        }
        this.ioWrite("AT#CY\r\n");
    }
    
    public void readDeviceAddr() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+GLBA\r\n");
            return;
        }
        this.ioWrite("AT#DF\r\n");
    }
    
    public void readDeviceNameSync() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+GLDN\r\n");
            return;
        }
        this.ioWrite("AT#MM\r\n");
    }
    
    public void readDevicePIN() throws Exception {
        if (!Bluetooth.isRenGao) {
            this.ioWrite("AT#MN\r\n");
        }
    }
    
    public void readMediaInfo() throws Exception {
        if (!Bluetooth.isRenGao) {
            this.ioWrite("AT#MK\r\n");
        }
    }
    
    public void readMediaStatus() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+A2DPSTAT\r\n");
            return;
        }
        this.ioWrite("AT#MV\r\n");
    }
    
    public void readPairingListSync() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+LSPD\r\n");
            return;
        }
        this.ioWrite("AT#MX\r\n");
    }
    
    public void readPhoneStatusSync() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFPSTAT\r\n");
            return;
        }
        this.ioWrite("AT#CY\r\n");
    }
    
    public void readVersionSync() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+GVER\r\n");
            return;
        }
        this.ioWrite("AT#MY\r\n");
    }
    
    public void removeDevice(String replace) throws Exception {
        if (Bluetooth.isRenGao) {
            replace = replace.replace("\r\n", "");
            this.ioWrite("AT+DLPD=" + replace + "\r\n");
            return;
        }
        this.ioWrite("AT#CV\r\n");
    }
    
    public void setBTEnterACC(final boolean b) throws Exception {
        if (!Bluetooth.isRenGao) {
            if (!b) {
                this.ioWrite("AT#CZ1\r\n");
                return;
            }
            this.ioWrite("AT#CZ0\r\n");
        }
    }
    
    public void setBTVolume(final int n) throws Exception {
        if (!Bluetooth.isRenGao) {
            this.ioWrite("AT#VF" + n + "\r\n");
        }
    }
    
    public void setBluetoothMusicMute(final boolean b) throws Exception {
        if (!Bluetooth.isRenGao) {
            if (!b) {
                this.ioWrite("AT#VB\r\n");
                return;
            }
            this.ioWrite("AT#VA\r\n");
        }
    }
    
    public void setDeviceName(String replace) throws Exception {
        replace = replace.replace("\r\n", "");
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+SLDN=" + replace + "\r\n");
            return;
        }
        this.ioWrite("AT#MM" + replace + "\r\n");
    }
    
    public void stopPlay() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+STOP\r\n");
            return;
        }
        this.ioWrite("AT#MC\r\n");
    }
    
    public void switchBtDevice() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFADTS\r\n");
            return;
        }
        this.ioWrite("AT#CP\r\n");
    }
    
    public void switchDevice(final boolean b) throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFADTS\r\n");
            return;
        }
        if (b) {
            this.ioWrite("AT#CO\r\n");
            return;
        }
        this.ioWrite("AT#CP\r\n");
    }
    
    public void switchPhoneDevice() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+HFADTS\r\n");
            return;
        }
        this.ioWrite("AT#CO\r\n");
    }
    
    public void tryToDownloadPhoneBook() throws Exception {
        if (Bluetooth.isRenGao) {
            this.ioWrite("AT+PBCONN\r\n");
            return;
        }
        this.ioWrite("AT#PA\r\n");
    }
    
    protected byte[] wrap(final byte[] array) {
        final byte[] array2 = new byte[array.length + 4];
        array2[0] = (byte)(array.length + 2);
        array2[1] = 1;
        array2[2] = 15;
        byte b = (byte)((byte)(array2[1] + array2[2]) + array2[0]);
        for (int i = 0; i < array.length; ++i) {
            array2[i + 3] = array[i];
            b += array[i];
        }
        array2[array.length + 3] = b;
        return array2;
    }
    
    public void writeBluetooth(final byte[] array) throws Exception {
        Mainboard.getInstance().writeBlueTooth(array);
        Bluetooth.logger.debug("bluetoothprotocal write::" + new String(array));
    }
}
