package com.backaudio.android.driver.bluetooth;

public class StandardSerialPort
{
    static {
        try {
            System.loadLibrary("StandardSerialPort");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public native int close();
    
    public native int open(final String p0, final int p1, final int p2, final int p3, final int p4, final int p5, final int p6);
    
    public native int read(final byte[] p0, final int p1);
    
    public native int write(final byte[] p0);
    
    public native int writeLength(final byte[] p0, final int p1);
}
