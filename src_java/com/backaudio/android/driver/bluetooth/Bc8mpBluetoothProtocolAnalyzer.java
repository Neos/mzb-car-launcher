package com.backaudio.android.driver.bluetooth;

import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import org.slf4j.*;
import java.util.*;
import com.backaudio.android.driver.*;
import java.io.*;

public class Bc8mpBluetoothProtocolAnalyzer implements IBluetoothProtocolAnalyzer
{
    private static Logger logger;
    private static List<String> playStatusTag;
    private IBluetoothEventHandler handler;
    private boolean isLineEndDetected;
    private boolean isLineStartDeteceted;
    private MediaInfoProtocol mediaInfoProtocol;
    private MediaInfoUnitProtocol mediaInfoUnitProtocol;
    private BaseMultilineProtocol mutilineProtocol;
    private byte nextExceptedValue;
    private PhoneBookListProtocol phoneBookListProtocol;
    private PhoneBookListProtocol phoneBookUnit;
    private ByteArrayOutputStream protocolBuffer;
    
    static {
        Bc8mpBluetoothProtocolAnalyzer.logger = LoggerFactory.getLogger(Bc8mpBluetoothProtocolAnalyzer.class);
        Bc8mpBluetoothProtocolAnalyzer.playStatusTag = null;
    }
    
    public Bc8mpBluetoothProtocolAnalyzer() {
        this.handler = null;
        this.protocolBuffer = new ByteArrayOutputStream();
        this.isLineStartDeteceted = false;
        this.isLineEndDetected = false;
        this.nextExceptedValue = 0;
        this.mutilineProtocol = null;
        this.mediaInfoProtocol = null;
        this.mediaInfoUnitProtocol = null;
        this.phoneBookListProtocol = null;
        this.phoneBookUnit = null;
        (Bc8mpBluetoothProtocolAnalyzer.playStatusTag = new ArrayList<String>()).add("PP");
        Bc8mpBluetoothProtocolAnalyzer.playStatusTag.add("STOP");
        Bc8mpBluetoothProtocolAnalyzer.playStatusTag.add("FWD");
        Bc8mpBluetoothProtocolAnalyzer.playStatusTag.add("BACK");
    }
    
    private boolean newLineProtocol(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_1        
        //     5: invokespecial   java/lang/String.<init>:([B)V
        //     8: astore_3       
        //     9: aload_3        
        //    10: ifnull          20
        //    13: aload_3        
        //    14: invokevirtual   java/lang/String.length:()I
        //    17: ifne            22
        //    20: iconst_0       
        //    21: ireturn        
        //    22: aload_3        
        //    23: ldc             "HFPSTAT"
        //    25: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //    28: ifne            37
        //    31: aload_3        
        //    32: iconst_0       
        //    33: invokestatic    com/backaudio/android/driver/Utils.saveLogLine:(Ljava/lang/String;Z)Ljava/lang/String;
        //    36: pop            
        //    37: aconst_null    
        //    38: astore_2       
        //    39: aload_3        
        //    40: bipush          61
        //    42: invokevirtual   java/lang/String.indexOf:(I)I
        //    45: iconst_m1      
        //    46: if_icmpeq       188
        //    49: aload_3        
        //    50: aload_3        
        //    51: ldc             "+"
        //    53: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //    56: iconst_1       
        //    57: iadd           
        //    58: aload_3        
        //    59: ldc             "="
        //    61: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //    64: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    67: astore_1       
        //    68: aload_3        
        //    69: aload_3        
        //    70: ldc             "="
        //    72: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //    75: iconst_1       
        //    76: iadd           
        //    77: aload_3        
        //    78: bipush          13
        //    80: invokevirtual   java/lang/String.lastIndexOf:(I)I
        //    83: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    86: astore_2       
        //    87: getstatic       com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.logger:Lorg/slf4j/Logger;
        //    90: new             Ljava/lang/StringBuilder;
        //    93: dup            
        //    94: ldc             "bluetoothprotocal line recv::"
        //    96: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    99: aload_1        
        //   100: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   103: ldc             "="
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   108: aload_2        
        //   109: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   112: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   115: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   120: aload_1        
        //   121: ldc             "PBDN"
        //   123: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   126: ifeq            396
        //   129: aload_1        
        //   130: ldc             "PBDNDATA"
        //   132: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   135: ifeq            287
        //   138: aload_0        
        //   139: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   142: ifnonnull       158
        //   145: aload_0        
        //   146: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   149: dup            
        //   150: aload_1        
        //   151: aload_2        
        //   152: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   155: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   158: aload_0        
        //   159: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   162: ifnonnull       210
        //   165: aload_0        
        //   166: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   169: dup            
        //   170: aload_1        
        //   171: aload_2        
        //   172: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   175: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   178: iconst_1       
        //   179: ireturn        
        //   180: astore_1       
        //   181: aload_1        
        //   182: invokevirtual   java/io/IOException.printStackTrace:()V
        //   185: goto            37
        //   188: aload_3        
        //   189: aload_3        
        //   190: ldc             "+"
        //   192: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //   195: iconst_1       
        //   196: iadd           
        //   197: aload_3        
        //   198: bipush          13
        //   200: invokevirtual   java/lang/String.lastIndexOf:(I)I
        //   203: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   206: astore_1       
        //   207: goto            120
        //   210: aload_0        
        //   211: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   214: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.getNeedDataLength:()I
        //   217: ifne            254
        //   220: aload_0        
        //   221: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   224: aload_0        
        //   225: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   228: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.addUnit:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;)V
        //   231: aload_0        
        //   232: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   235: dup            
        //   236: aload_1        
        //   237: aload_2        
        //   238: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   241: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   244: goto            178
        //   247: astore_1       
        //   248: aload_1        
        //   249: invokevirtual   java/lang/Exception.printStackTrace:()V
        //   252: iconst_0       
        //   253: ireturn        
        //   254: getstatic       com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.logger:Lorg/slf4j/Logger;
        //   257: new             Ljava/lang/StringBuilder;
        //   260: dup            
        //   261: ldc             "phonebook error,needData:"
        //   263: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   266: aload_0        
        //   267: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   270: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.getNeedDataLength:()I
        //   273: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   276: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   279: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   284: goto            178
        //   287: aload_1        
        //   288: ldc             "PBDNEND"
        //   290: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   293: ifeq            396
        //   296: aload_0        
        //   297: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   300: ifnull          363
        //   303: aload_0        
        //   304: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   307: ifnull          363
        //   310: aload_0        
        //   311: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   314: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.getNeedDataLength:()I
        //   317: ifeq            375
        //   320: getstatic       com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.logger:Lorg/slf4j/Logger;
        //   323: new             Ljava/lang/StringBuilder;
        //   326: dup            
        //   327: ldc             "phonebook error,needData:"
        //   329: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   332: aload_0        
        //   333: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   336: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.getNeedDataLength:()I
        //   339: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   342: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   345: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   350: aload_0        
        //   351: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   354: aload_0        
        //   355: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   358: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPhoneBookList:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;)V
        //   363: aload_0        
        //   364: aconst_null    
        //   365: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   368: aload_0        
        //   369: aconst_null    
        //   370: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   373: iconst_1       
        //   374: ireturn        
        //   375: aload_0        
        //   376: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookListProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   379: aload_0        
        //   380: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   383: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.addUnit:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AbstractPlayLoadProtocol;)V
        //   386: aload_0        
        //   387: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.phoneBookUnit:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol;
        //   390: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookListProtocol.reset:()V
        //   393: goto            350
        //   396: aload_1        
        //   397: ldc             "PREND"
        //   399: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   402: ifeq            416
        //   405: aload_0        
        //   406: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   409: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPairingModeEnd:()V
        //   414: iconst_1       
        //   415: ireturn        
        //   416: aload_1        
        //   417: ldc             "HFPOCID"
        //   419: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   422: ifeq            447
        //   425: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;
        //   428: dup            
        //   429: aload_1        
        //   430: aload_2        
        //   431: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   434: astore_1       
        //   435: aload_0        
        //   436: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   439: aload_1        
        //   440: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPhoneCallingOut:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallingOutProtocol;)V
        //   445: iconst_1       
        //   446: ireturn        
        //   447: aload_1        
        //   448: ldc             "HFPAUDO"
        //   450: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   453: ifeq            478
        //   456: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
        //   459: dup            
        //   460: aload_1        
        //   461: aload_2        
        //   462: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   465: astore_1       
        //   466: aload_0        
        //   467: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   470: aload_1        
        //   471: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.ondeviceSwitchedProtocol:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V
        //   476: iconst_1       
        //   477: ireturn        
        //   478: aload_1        
        //   479: ldc             "GVER"
        //   481: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   484: ifeq            509
        //   487: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;
        //   490: dup            
        //   491: aload_1        
        //   492: aload_2        
        //   493: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   496: astore_1       
        //   497: aload_0        
        //   498: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   501: aload_1        
        //   502: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onVersion:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;)V
        //   507: iconst_1       
        //   508: ireturn        
        //   509: aload_1        
        //   510: ldc             "GLDN"
        //   512: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   515: ifeq            540
        //   518: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
        //   521: dup            
        //   522: aload_1        
        //   523: aload_2        
        //   524: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   527: astore_1       
        //   528: aload_0        
        //   529: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   532: aload_1        
        //   533: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onDeviceName:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V
        //   538: iconst_1       
        //   539: ireturn        
        //   540: aload_1        
        //   541: ldc             "EPRM"
        //   543: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   546: ifeq            571
        //   549: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;
        //   552: dup            
        //   553: aload_1        
        //   554: aload_2        
        //   555: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   558: astore_1       
        //   559: aload_0        
        //   560: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   563: aload_1        
        //   564: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPairingModeResult:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EnterPairingModeResult;)V
        //   569: iconst_1       
        //   570: ireturn        
        //   571: getstatic       com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.playStatusTag:Ljava/util/List;
        //   574: aload_1        
        //   575: invokeinterface java/util/List.contains:(Ljava/lang/Object;)Z
        //   580: ifeq            605
        //   583: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;
        //   586: dup            
        //   587: aload_1        
        //   588: aload_2        
        //   589: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   592: astore_1       
        //   593: aload_0        
        //   594: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   597: aload_1        
        //   598: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onSetPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/SetPlayStatusProtocol;)V
        //   603: iconst_1       
        //   604: ireturn        
        //   605: aload_1        
        //   606: ldc             "HFPCLID"
        //   608: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   611: ifeq            636
        //   614: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;
        //   617: dup            
        //   618: aload_1        
        //   619: aload_2        
        //   620: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   623: astore_1       
        //   624: aload_0        
        //   625: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   628: aload_1        
        //   629: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onIncomingCall:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/IncomingCallProtocol;)V
        //   634: iconst_1       
        //   635: ireturn        
        //   636: aload_1        
        //   637: ldc             "HFPSTAT"
        //   639: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   642: ifeq            667
        //   645: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
        //   648: dup            
        //   649: aload_1        
        //   650: aload_2        
        //   651: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   654: astore_1       
        //   655: aload_0        
        //   656: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   659: aload_1        
        //   660: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPhoneStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V
        //   665: iconst_1       
        //   666: ireturn        
        //   667: aload_1        
        //   668: ldc_w           "HFANSW"
        //   671: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   674: ifeq            699
        //   677: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;
        //   680: dup            
        //   681: aload_1        
        //   682: aload_2        
        //   683: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   686: astore_1       
        //   687: aload_0        
        //   688: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   691: aload_1        
        //   692: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onAnswerPhone:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/AnswerPhoneResult;)V
        //   697: iconst_1       
        //   698: ireturn        
        //   699: aload_1        
        //   700: ldc_w           "HFCHUP"
        //   703: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   706: ifeq            731
        //   709: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;
        //   712: dup            
        //   713: aload_1        
        //   714: aload_2        
        //   715: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   718: astore_1       
        //   719: aload_0        
        //   720: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   723: aload_1        
        //   724: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onHangUpPhone:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/HangUpPhoneResult;)V
        //   729: iconst_1       
        //   730: ireturn        
        //   731: aload_1        
        //   732: ldc_w           "HFDIAL"
        //   735: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   738: ifeq            763
        //   741: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;
        //   744: dup            
        //   745: aload_1        
        //   746: aload_2        
        //   747: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   750: astore_1       
        //   751: aload_0        
        //   752: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   755: aload_1        
        //   756: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onCallOut:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/CallOutResult;)V
        //   761: iconst_1       
        //   762: ireturn        
        //   763: aload_1        
        //   764: ldc_w           "AVRCPSTAT"
        //   767: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   770: ifeq            795
        //   773: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;
        //   776: dup            
        //   777: aload_1        
        //   778: aload_2        
        //   779: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   782: astore_1       
        //   783: aload_0        
        //   784: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   787: aload_1        
        //   788: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaStatusProtocol;)V
        //   793: iconst_1       
        //   794: ireturn        
        //   795: aload_1        
        //   796: ldc_w           "A2DPSTAT"
        //   799: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   802: ifne            815
        //   805: aload_1        
        //   806: ldc_w           "PLAYSTAT"
        //   809: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   812: ifeq            837
        //   815: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
        //   818: dup            
        //   819: aload_1        
        //   820: aload_2        
        //   821: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   824: astore_1       
        //   825: aload_0        
        //   826: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   829: aload_1        
        //   830: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
        //   835: iconst_1       
        //   836: ireturn        
        //   837: aload_1        
        //   838: ldc_w           "PBSTAT"
        //   841: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   844: ifeq            869
        //   847: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;
        //   850: dup            
        //   851: aload_1        
        //   852: aload_2        
        //   853: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   856: astore_1       
        //   857: aload_0        
        //   858: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   861: aload_1        
        //   862: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPhoneBookCtrlStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneBookCtrlStatusProtocol;)V
        //   867: iconst_1       
        //   868: ireturn        
        //   869: aload_1        
        //   870: ldc_w           "CCDA"
        //   873: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   876: ifeq            901
        //   879: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
        //   882: dup            
        //   883: aload_1        
        //   884: aload_2        
        //   885: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   888: astore_1       
        //   889: aload_0        
        //   890: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   893: aload_1        
        //   894: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onConnectedDevice:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;)V
        //   899: iconst_1       
        //   900: ireturn        
        //   901: aload_1        
        //   902: ldc_w           "DLPD"
        //   905: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   908: ifeq            933
        //   911: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;
        //   914: dup            
        //   915: aload_1        
        //   916: aload_2        
        //   917: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   920: astore_1       
        //   921: aload_0        
        //   922: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   925: aload_1        
        //   926: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onDeviceRemoved:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceRemovedProtocol;)V
        //   931: iconst_1       
        //   932: ireturn        
        //   933: aload_1        
        //   934: ldc_w           "LSPD"
        //   937: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   940: ifeq            1051
        //   943: aload_1        
        //   944: ldc_w           "LSPDSTART"
        //   947: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   950: ifeq            966
        //   953: aload_0        
        //   954: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //   957: dup            
        //   958: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol.<init>:()V
        //   961: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //   964: iconst_1       
        //   965: ireturn        
        //   966: aload_1        
        //   967: ldc_w           "LSPD"
        //   970: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   973: ifeq            1004
        //   976: aload_0        
        //   977: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //   980: ifnull          1163
        //   983: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol;
        //   986: dup            
        //   987: aload_1        
        //   988: aload_2        
        //   989: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListUnitProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   992: astore_1       
        //   993: aload_0        
        //   994: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //   997: aload_1        
        //   998: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol.addUnit:(Ljava/lang/Object;)V
        //  1001: goto            1163
        //  1004: aload_1        
        //  1005: ldc_w           "LSPDEND"
        //  1008: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1011: ifeq            1165
        //  1014: aload_0        
        //  1015: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //  1018: ifnull          1165
        //  1021: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;
        //  1024: dup            
        //  1025: aload_0        
        //  1026: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //  1029: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol.<init>:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;)V
        //  1032: astore_1       
        //  1033: aload_0        
        //  1034: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //  1037: aload_1        
        //  1038: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPairingList:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PairingListProtocol;)V
        //  1043: aload_0        
        //  1044: aconst_null    
        //  1045: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mutilineProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/BaseMultilineProtocol;
        //  1048: goto            1165
        //  1051: aload_1        
        //  1052: ldc_w           "MEDIAINFO"
        //  1055: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //  1058: ifeq            252
        //  1061: aload_1        
        //  1062: ldc_w           "MEDIAINFOSTART"
        //  1065: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1068: ifeq            1084
        //  1071: aload_0        
        //  1072: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //  1075: dup            
        //  1076: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.<init>:()V
        //  1079: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //  1082: iconst_1       
        //  1083: ireturn        
        //  1084: aload_1        
        //  1085: ldc_w           "MEDIAINFOEND"
        //  1088: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1091: ifeq            1127
        //  1094: aload_0        
        //  1095: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //  1098: ifnull          1167
        //  1101: aload_0        
        //  1102: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.handler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //  1105: aload_0        
        //  1106: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //  1109: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaInfo:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;)V
        //  1114: aload_0        
        //  1115: aconst_null    
        //  1116: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //  1119: aload_0        
        //  1120: aconst_null    
        //  1121: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
        //  1124: goto            1167
        //  1127: aload_1        
        //  1128: ldc_w           "MEDIAINFO"
        //  1131: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1134: ifeq            252
        //  1137: aload_0        
        //  1138: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
        //  1141: dup            
        //  1142: aload_1        
        //  1143: aload_2        
        //  1144: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1147: putfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
        //  1150: aload_0        
        //  1151: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //  1154: aload_0        
        //  1155: getfield        com/backaudio/android/driver/bluetooth/Bc8mpBluetoothProtocolAnalyzer.mediaInfoUnitProtocol:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoUnitProtocol;
        //  1158: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.addUnit:(Ljava/lang/Object;)V
        //  1161: iconst_1       
        //  1162: ireturn        
        //  1163: iconst_1       
        //  1164: ireturn        
        //  1165: iconst_1       
        //  1166: ireturn        
        //  1167: iconst_1       
        //  1168: ireturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  31     37     180    188    Ljava/io/IOException;
        //  39     120    247    252    Ljava/lang/Exception;
        //  120    158    247    252    Ljava/lang/Exception;
        //  158    178    247    252    Ljava/lang/Exception;
        //  188    207    247    252    Ljava/lang/Exception;
        //  210    244    247    252    Ljava/lang/Exception;
        //  254    284    247    252    Ljava/lang/Exception;
        //  287    350    247    252    Ljava/lang/Exception;
        //  350    363    247    252    Ljava/lang/Exception;
        //  363    373    247    252    Ljava/lang/Exception;
        //  375    393    247    252    Ljava/lang/Exception;
        //  396    414    247    252    Ljava/lang/Exception;
        //  416    445    247    252    Ljava/lang/Exception;
        //  447    476    247    252    Ljava/lang/Exception;
        //  478    507    247    252    Ljava/lang/Exception;
        //  509    538    247    252    Ljava/lang/Exception;
        //  540    569    247    252    Ljava/lang/Exception;
        //  571    603    247    252    Ljava/lang/Exception;
        //  605    634    247    252    Ljava/lang/Exception;
        //  636    665    247    252    Ljava/lang/Exception;
        //  667    697    247    252    Ljava/lang/Exception;
        //  699    729    247    252    Ljava/lang/Exception;
        //  731    761    247    252    Ljava/lang/Exception;
        //  763    793    247    252    Ljava/lang/Exception;
        //  795    815    247    252    Ljava/lang/Exception;
        //  815    835    247    252    Ljava/lang/Exception;
        //  837    867    247    252    Ljava/lang/Exception;
        //  869    899    247    252    Ljava/lang/Exception;
        //  901    931    247    252    Ljava/lang/Exception;
        //  933    964    247    252    Ljava/lang/Exception;
        //  966    1001   247    252    Ljava/lang/Exception;
        //  1004   1048   247    252    Ljava/lang/Exception;
        //  1051   1082   247    252    Ljava/lang/Exception;
        //  1084   1124   247    252    Ljava/lang/Exception;
        //  1127   1161   247    252    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0120:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void push(final byte[] array) {
        final int n = 0;
        int n2 = 0;
        if (this.phoneBookUnit != null && this.phoneBookUnit.getNeedDataLength() > 0) {
            if (array.length > this.phoneBookUnit.getNeedDataLength()) {
                final int needDataLength = this.phoneBookUnit.getNeedDataLength();
                final int n3 = array.length - this.phoneBookUnit.getNeedDataLength();
                final byte[] array2 = new byte[n3];
                this.phoneBookUnit.push(array, 0, this.phoneBookUnit.getNeedDataLength());
                for (int i = 0; i < n3; ++i) {
                    array2[i] = array[i + needDataLength];
                }
                this.push(array2);
                return;
            }
            this.phoneBookUnit.push(array, 0, array.length);
        }
        else {
            int j = n;
            if (this.mediaInfoUnitProtocol != null) {
                j = n;
                if (this.mediaInfoUnitProtocol.getNeedDataLength() > 0) {
                    while ((j = n2) < array.length) {
                        if (this.mediaInfoUnitProtocol.getNeedDataLength() <= 0) {
                            j = n2;
                            break;
                        }
                        this.mediaInfoUnitProtocol.push(array, n2, 1);
                        ++n2;
                    }
                }
            }
            while (j < array.length) {
                if (!this.isLineStartDeteceted) {
                    if (array[j] == 13) {
                        this.isLineStartDeteceted = true;
                        this.protocolBuffer.write(array, j, 1);
                        this.nextExceptedValue = 10;
                    }
                    else {
                        this.isLineStartDeteceted = false;
                        final Logger logger = Bc8mpBluetoothProtocolAnalyzer.logger;
                        final StringBuilder sb = new StringBuilder("skip:");
                        int n4;
                        if (array[j] > 0) {
                            n4 = array[j];
                        }
                        else {
                            n4 = array[j] + 256;
                        }
                        logger.error(sb.append(Integer.toHexString(n4)).toString());
                        this.protocolBuffer.reset();
                    }
                }
                else if (this.nextExceptedValue != 0 && !this.isLineEndDetected) {
                    if (this.nextExceptedValue == array[j]) {
                        this.protocolBuffer.write(array, j, 1);
                        this.nextExceptedValue = 0;
                    }
                    else if (array[j] != 13) {
                        this.isLineStartDeteceted = false;
                        final Logger logger2 = Bc8mpBluetoothProtocolAnalyzer.logger;
                        final StringBuilder sb2 = new StringBuilder("skip:");
                        int n5;
                        if (array[j] > 0) {
                            n5 = array[j];
                        }
                        else {
                            n5 = array[j] + 256;
                        }
                        logger2.error(sb2.append(Integer.toHexString(n5)).toString());
                        this.protocolBuffer.reset();
                    }
                }
                else if (!this.isLineEndDetected) {
                    if (array[j] != 13) {
                        this.protocolBuffer.write(array, j, 1);
                    }
                    else {
                        this.isLineEndDetected = true;
                        this.protocolBuffer.write(array, j, 1);
                        this.nextExceptedValue = 10;
                    }
                }
                else if (this.nextExceptedValue == 10) {
                    this.protocolBuffer.write(array, j, 1);
                    if (new String(this.protocolBuffer.toByteArray()).equals("\r\n\r\n")) {
                        Bc8mpBluetoothProtocolAnalyzer.logger.error("0d0a0d0a error protocol,try to repare");
                        this.protocolBuffer.reset();
                        this.protocolBuffer.write(new byte[] { 13, 10 }, 0, 2);
                        this.isLineEndDetected = false;
                        this.isLineStartDeteceted = true;
                        this.nextExceptedValue = 0;
                    }
                    else {
                        this.newLineProtocol(this.protocolBuffer.toByteArray());
                        this.isLineStartDeteceted = false;
                        this.isLineEndDetected = false;
                        this.nextExceptedValue = 0;
                        this.protocolBuffer.reset();
                        final int n6 = array.length - j - 1;
                        if (n6 > 0) {
                            final byte[] array3 = new byte[n6];
                            for (int k = 0; k < n6; ++k) {
                                array3[k] = array[k + j + 1];
                            }
                            this.push(array3);
                            return;
                        }
                    }
                }
                else {
                    Bc8mpBluetoothProtocolAnalyzer.logger.debug("protocol error, drop:" + Integer.toHexString(array[j]));
                    this.isLineStartDeteceted = false;
                    this.isLineEndDetected = false;
                    this.nextExceptedValue = 0;
                    this.protocolBuffer.reset();
                }
                ++j;
            }
        }
    }
    
    @Override
    public void push(byte[] copy, final int n, final int n2) {
        copy = Arrays.copyOf(copy, n2);
        Bc8mpBluetoothProtocolAnalyzer.logger.debug("bluetoothprot recv::" + Utils.byteArrayToHexString(copy, 0, copy.length));
        while (true) {
            try {
                Utils.saveLogLine(Utils.byteArrayToHexString(copy, 0, copy.length), true);
                this.push(copy);
            }
            catch (IOException ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    @Override
    public void reset() {
    }
    
    @Override
    public void setEventHandler(final IBluetoothEventHandler handler) {
        this.handler = handler;
    }
}
