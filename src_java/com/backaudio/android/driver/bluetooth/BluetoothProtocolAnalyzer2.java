package com.backaudio.android.driver.bluetooth;

import java.text.*;
import org.slf4j.*;
import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;
import java.util.*;
import com.backaudio.android.driver.*;
import java.io.*;

public class BluetoothProtocolAnalyzer2 implements IBluetoothProtocolAnalyzer
{
    private static final String TAG = "tag-bt2";
    private static Logger logger;
    public static SimpleDateFormat sdf;
    private ByteArrayOutputStream baos;
    DeviceNameProtocol dnp;
    private IBluetoothEventHandler eventHandler;
    private byte mNextExpectedValue;
    private boolean mValidProtocol;
    public FileOutputStream out;
    
    static {
        BluetoothProtocolAnalyzer2.logger = LoggerFactory.getLogger(BluetoothProtocolAnalyzer2.class);
        BluetoothProtocolAnalyzer2.sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
    }
    
    public BluetoothProtocolAnalyzer2() {
        this.eventHandler = null;
        this.mNextExpectedValue = -1;
        this.mValidProtocol = false;
        this.baos = new ByteArrayOutputStream();
        this.out = null;
        this.dnp = new DeviceNameProtocol(null, "");
        try {
            this.out = new FileOutputStream("/data/data/com.touchus.benchilauncher/bluetooth2.txt", true);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void analyze(final byte[] array) {
        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2 hexstring: " + new String(array));
        Label_0108: {
            if (this.out == null) {
                break Label_0108;
            }
            while (true) {
                synchronized (this.out) {
                    while (true) {
                        try {
                            this.out.write(array);
                            this.out.write(("\t" + BluetoothProtocolAnalyzer2.sdf.format(new Date()) + "\r\n").getBytes());
                            this.out.flush();
                            // monitorexit(this.out)
                            if (this.eventHandler == null || array == null || array.length < 4) {
                                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2--------invalid--------");
                                return;
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            continue;
                        }
                        break;
                    }
                }
                final char c = (char)array[0];
                final char c2 = (char)array[1];
                switch (c) {
                    default: {}
                    case 68: {
                        this.handleStartD(c2, array);
                    }
                    case 73: {
                        this.handleStartI(c2, array);
                    }
                    case 77: {
                        this.handleStartM(c2, array);
                    }
                    case 80: {
                        this.handleStartP(c2, array);
                    }
                    case 84: {
                        if (c2 == '1' || c2 == '0') {
                            BluetoothProtocolAnalyzer2.logger.debug("tag-bt2T: \u8f66\u673a\uff08\u84dd\u7259/LOCAL\uff09\u51fa\u58f0\u97f3");
                            this.eventHandler.ondeviceSwitchedProtocol(new DeviceSwitchedProtocol(null, String.valueOf(c2)));
                        }
                    }
                    case 83: {
                        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2---start S----" + new String(array, 0, array.length - 2));
                    }
                    case 72: {
                        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2---start H----" + new String(array, 0, array.length - 2));
                    }
                    case 79: {
                        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2---start O----" + new String(array, 0, array.length - 2));
                    }
                    case 78: {
                        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2---start N----" + new String(array, 0, array.length - 2));
                    }
                }
            }
        }
    }
    
    private void handleStartD(final char c, final byte[] array) {
        final int length = array.length;
        switch (c) {
            default: {}
            case 'B': {
                final String macAddr = new String(array, 2, length - 4);
                this.dnp.setMACAddr(macAddr);
                this.eventHandler.onDeviceName(this.dnp);
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2MN: MACAddr() PIN = " + macAddr);
            }
        }
    }
    
    private void handleStartI(final char c, final byte[] array) {
        final int length = array.length;
        switch (c) {
            default: {}
            case 'A': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IA: HFP\u65ad\u5f00");
                final PhoneStatusProtocol phoneStatusProtocol = new PhoneStatusProtocol(null, "");
                phoneStatusProtocol.setPhoneStatus(EPhoneStatus.UNCONNECT);
                this.eventHandler.onPhoneStatus(phoneStatusProtocol);
            }
            case 'B': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IB: HFP\u8fde\u63a5\u6210\u529f");
                final PhoneStatusProtocol phoneStatusProtocol2 = new PhoneStatusProtocol(null, "");
                phoneStatusProtocol2.setPhoneStatus(EPhoneStatus.CONNECTED);
                this.eventHandler.onPhoneStatus(phoneStatusProtocol2);
            }
            case 'C': {
                this.eventHandler.onCallOut(new CallOutResult(null, "0"));
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IC: onCallOut() ");
            }
            case 'D': {
                final String s = new String(array, 2, length - 4);
                this.eventHandler.onIncomingCall(new IncomingCallProtocol(null, s));
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2ID: onIncomingCall() " + s);
            }
            case 'E': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2\u4e09\u65b9\u6765\u7535\uff1a" + new String(array, 2, length - 4));
            }
            case 'F': {
                this.eventHandler.onHangUpPhone(new HangUpPhoneResult(null, "0"));
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IF: \u6302\u673a------------");
            }
            case 'G': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IG: \u5df2\u63a5\u901a-----------");
                final PhoneStatusProtocol phoneStatusProtocol3 = new PhoneStatusProtocol(null, "");
                phoneStatusProtocol3.setPhoneStatus(EPhoneStatus.TALKING);
                this.eventHandler.onPhoneStatus(phoneStatusProtocol3);
            }
            case 'R': {
                final String s2 = new String(array, 2, length - 4);
                this.eventHandler.onPhoneCallingOut(new CallingOutProtocol(null, s2));
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IR: onPhoneCallingOut() " + s2);
            }
            case 'V': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IV: HFP\u8fde\u63a5\u4e2d");
                final PhoneStatusProtocol phoneStatusProtocol4 = new PhoneStatusProtocol(null, "");
                phoneStatusProtocol4.setPhoneStatus(EPhoneStatus.CONNECTING);
                this.eventHandler.onPhoneStatus(phoneStatusProtocol4);
            }
            case 'I': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2II: \u8fdb\u5165\u914d\u5bf9");
                this.eventHandler.onPairingModeResult(new EnterPairingModeResult(null, "0"));
            }
            case 'J': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IJ: \u9000\u51fa\u914d\u5bf9");
                this.eventHandler.onPairingModeResult(new EnterPairingModeResult(null, "1"));
            }
            case 'S': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2IS: \u84dd\u7259\u521d\u59cb\u5316\u5b8c\u6210");
            }
            case 'O': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2\u6253\u5f00/\u5173\u95ed\u54aa\u5934");
            }
        }
    }
    
    private void handleStartM(final char p0, final byte[] p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: arraylength    
        //     2: istore_3       
        //     3: iload_1        
        //     4: tableswitch {
        //              130: 121
        //              131: 162
        //              132: 203
        //              133: 236
        //              134: 120
        //              135: 120
        //              136: 270
        //              137: 120
        //              138: 782
        //              139: 120
        //              140: 120
        //              141: 120
        //              142: 489
        //              143: 549
        //              144: 120
        //              145: 120
        //              146: 120
        //              147: 120
        //              148: 120
        //              149: 120
        //              150: 335
        //              151: 120
        //              152: 428
        //              153: 609
        //              154: 393
        //          default: 120
        //        }
        //   120: return         
        //   121: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   124: ldc_w           "tag-bt2MA: \u97f3\u4e50\u6682\u505c\u4e2d/\u8fde\u63a5\u6210\u529f"
        //   127: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   132: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
        //   135: dup            
        //   136: ldc             ""
        //   138: ldc             ""
        //   140: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   143: astore_2       
        //   144: aload_2        
        //   145: getstatic       com/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus.PAUSE:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;
        //   148: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.setMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;)V
        //   151: aload_0        
        //   152: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   155: aload_2        
        //   156: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
        //   161: return         
        //   162: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   165: ldc_w           "tag-bt2MB: \u97f3\u4e50\u64ad\u653e\u4e2d"
        //   168: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   173: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
        //   176: dup            
        //   177: ldc             ""
        //   179: ldc             ""
        //   181: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   184: astore_2       
        //   185: aload_2        
        //   186: getstatic       com/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus.PLAYING:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;
        //   189: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.setMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EMediaPlayStatus;)V
        //   192: aload_0        
        //   193: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   196: aload_2        
        //   197: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
        //   202: return         
        //   203: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   206: ldc_w           "tag-bt2MC: \u8f66\u673a\uff08\u84dd\u7259/LOCAL\uff09\u51fa\u58f0\u97f3"
        //   209: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   214: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
        //   217: dup            
        //   218: aconst_null    
        //   219: ldc             "0"
        //   221: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   224: astore_2       
        //   225: aload_0        
        //   226: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   229: aload_2        
        //   230: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.ondeviceSwitchedProtocol:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V
        //   235: return         
        //   236: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   239: ldc_w           "tag-bt2MD: \u624b\u673a\uff08REMOTE\uff09\u51fa\u58f0\u97f3"
        //   242: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   247: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;
        //   250: dup            
        //   251: aconst_null    
        //   252: ldc_w           "1"
        //   255: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   258: astore_2       
        //   259: aload_0        
        //   260: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   263: aload_2        
        //   264: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.ondeviceSwitchedProtocol:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceSwitchedProtocol;)V
        //   269: return         
        //   270: aload_2        
        //   271: iconst_2       
        //   272: baload         
        //   273: istore_1       
        //   274: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;
        //   277: dup            
        //   278: aconst_null    
        //   279: ldc             ""
        //   281: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   284: astore_2       
        //   285: aload_2        
        //   286: iload_1        
        //   287: bipush          48
        //   289: isub           
        //   290: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //   293: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol.setPhoneStatus:(Ljava/lang/String;)V
        //   296: aload_0        
        //   297: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   300: aload_2        
        //   301: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPhoneStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol;)V
        //   306: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   309: new             Ljava/lang/StringBuilder;
        //   312: dup            
        //   313: ldc_w           "tag-bt2MG: onPhoneStatus() "
        //   316: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   319: aload_2        
        //   320: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/PhoneStatusProtocol.getPhoneStatus:()Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/EPhoneStatus;
        //   323: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   326: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   329: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   334: return         
        //   335: aload_2        
        //   336: iconst_2       
        //   337: baload         
        //   338: bipush          48
        //   340: isub           
        //   341: istore_1       
        //   342: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   345: new             Ljava/lang/StringBuilder;
        //   348: dup            
        //   349: ldc_w           "tag-bt2MU: "
        //   352: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   355: iload_1        
        //   356: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   359: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   362: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   367: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
        //   370: dup            
        //   371: ldc_w           "A2DPSTAT"
        //   374: iload_1        
        //   375: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //   378: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   381: astore_2       
        //   382: aload_0        
        //   383: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   386: aload_2        
        //   387: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
        //   392: return         
        //   393: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   396: ldc_w           "tag-bt2MY: A2DP\u65ad\u5f00"
        //   399: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   404: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;
        //   407: dup            
        //   408: ldc_w           "A2DPSTAT"
        //   411: ldc             "0"
        //   413: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   416: astore_2       
        //   417: aload_0        
        //   418: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   421: aload_2        
        //   422: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaPlayStatus:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaPlayStatusProtocol;)V
        //   427: return         
        //   428: new             Ljava/lang/String;
        //   431: dup            
        //   432: aload_2        
        //   433: iconst_2       
        //   434: iload_3        
        //   435: iconst_4       
        //   436: isub           
        //   437: invokespecial   java/lang/String.<init>:([BII)V
        //   440: astore_2       
        //   441: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;
        //   444: dup            
        //   445: aconst_null    
        //   446: aload_2        
        //   447: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   450: astore          7
        //   452: aload_0        
        //   453: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   456: aload           7
        //   458: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onVersion:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/VersionProtocol;)V
        //   463: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   466: new             Ljava/lang/StringBuilder;
        //   469: dup            
        //   470: ldc_w           "tag-bt2MW: onVersion() "
        //   473: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   476: aload_2        
        //   477: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   480: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   483: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   488: return         
        //   489: new             Ljava/lang/String;
        //   492: dup            
        //   493: aload_2        
        //   494: iconst_2       
        //   495: iload_3        
        //   496: iconst_4       
        //   497: isub           
        //   498: invokespecial   java/lang/String.<init>:([BII)V
        //   501: astore_2       
        //   502: aload_0        
        //   503: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
        //   506: aload_2        
        //   507: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol.setDeviceName:(Ljava/lang/String;)V
        //   510: aload_0        
        //   511: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   514: aload_0        
        //   515: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
        //   518: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onDeviceName:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V
        //   523: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   526: new             Ljava/lang/StringBuilder;
        //   529: dup            
        //   530: ldc_w           "tag-bt2MM: onDeviceName() name = "
        //   533: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   536: aload_2        
        //   537: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   540: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   543: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   548: return         
        //   549: new             Ljava/lang/String;
        //   552: dup            
        //   553: aload_2        
        //   554: iconst_2       
        //   555: iload_3        
        //   556: iconst_4       
        //   557: isub           
        //   558: invokespecial   java/lang/String.<init>:([BII)V
        //   561: astore_2       
        //   562: aload_0        
        //   563: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
        //   566: aload_2        
        //   567: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol.setPIN:(Ljava/lang/String;)V
        //   570: aload_0        
        //   571: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   574: aload_0        
        //   575: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.dnp:Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;
        //   578: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onDeviceName:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/DeviceNameProtocol;)V
        //   583: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   586: new             Ljava/lang/StringBuilder;
        //   589: dup            
        //   590: ldc_w           "tag-bt2MN: onDeviceName() PIN = "
        //   593: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   596: aload_2        
        //   597: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   600: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   603: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   608: return         
        //   609: aload_2        
        //   610: iconst_2       
        //   611: baload         
        //   612: ifge            711
        //   615: aload_2        
        //   616: iconst_2       
        //   617: baload         
        //   618: sipush          256
        //   621: iadd           
        //   622: istore_1       
        //   623: new             Ljava/lang/String;
        //   626: dup            
        //   627: aload_2        
        //   628: iconst_3       
        //   629: bipush          12
        //   631: invokespecial   java/lang/String.<init>:([BII)V
        //   634: astore          7
        //   636: iload_1        
        //   637: bipush          48
        //   639: if_icmpne       718
        //   642: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;
        //   645: dup            
        //   646: aconst_null    
        //   647: aload           7
        //   649: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   652: astore          8
        //   654: aload           8
        //   656: new             Ljava/lang/String;
        //   659: dup            
        //   660: aload_2        
        //   661: bipush          15
        //   663: iload_3        
        //   664: bipush          17
        //   666: isub           
        //   667: invokespecial   java/lang/String.<init>:([BII)V
        //   670: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol.setDeviceName:(Ljava/lang/String;)V
        //   673: aload_0        
        //   674: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   677: aload           8
        //   679: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onConnectedDevice:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/ConnectedDeviceProtocol;)V
        //   684: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   687: new             Ljava/lang/StringBuilder;
        //   690: dup            
        //   691: ldc_w           "tag-bt2MX: address="
        //   694: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   697: aload           7
        //   699: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   702: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   705: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   710: return         
        //   711: aload_2        
        //   712: iconst_2       
        //   713: baload         
        //   714: istore_1       
        //   715: goto            623
        //   718: new             Ljava/lang/String;
        //   721: dup            
        //   722: aload_2        
        //   723: bipush          15
        //   725: iload_3        
        //   726: bipush          17
        //   728: isub           
        //   729: invokespecial   java/lang/String.<init>:([BII)V
        //   732: astore_2       
        //   733: aload_0        
        //   734: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   737: aload           7
        //   739: aload_2        
        //   740: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onPairedDevice:(Ljava/lang/String;Ljava/lang/String;)V
        //   745: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   748: new             Ljava/lang/StringBuilder;
        //   751: dup            
        //   752: ldc_w           "tag-bt2MX: address="
        //   755: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   758: aload           7
        //   760: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   763: ldc_w           " name="
        //   766: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   769: aload_2        
        //   770: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   773: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   776: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   781: return         
        //   782: new             Ljava/lang/String;
        //   785: dup            
        //   786: aload_2        
        //   787: iconst_2       
        //   788: iload_3        
        //   789: iconst_4       
        //   790: isub           
        //   791: invokespecial   java/lang/String.<init>:([BII)V
        //   794: ldc_w           "\n"
        //   797: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   800: astore          7
        //   802: aload           7
        //   804: arraylength    
        //   805: istore          6
        //   807: new             Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;
        //   810: dup            
        //   811: invokespecial   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.<init>:()V
        //   814: astore          8
        //   816: aload           8
        //   818: iconst_1       
        //   819: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.setAnalyzed:(Z)V
        //   822: iload           6
        //   824: ifle            993
        //   827: aload           7
        //   829: iconst_0       
        //   830: aaload         
        //   831: astore_2       
        //   832: aload           8
        //   834: aload_2        
        //   835: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.setTitle:(Ljava/lang/String;)V
        //   838: iload           6
        //   840: iconst_1       
        //   841: if_icmple       1000
        //   844: aload           7
        //   846: iconst_1       
        //   847: aaload         
        //   848: astore_2       
        //   849: aload           8
        //   851: aload_2        
        //   852: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.setArtist:(Ljava/lang/String;)V
        //   855: iconst_0       
        //   856: istore_1       
        //   857: iconst_0       
        //   858: istore_3       
        //   859: iconst_0       
        //   860: istore          4
        //   862: iload           6
        //   864: iconst_2       
        //   865: if_icmple       1007
        //   868: aload           7
        //   870: iconst_2       
        //   871: aaload         
        //   872: astore_2       
        //   873: aload_2        
        //   874: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   877: istore          5
        //   879: iload           5
        //   881: istore_1       
        //   882: iload           6
        //   884: iconst_3       
        //   885: if_icmple       1022
        //   888: aload           7
        //   890: iconst_3       
        //   891: aaload         
        //   892: astore_2       
        //   893: aload_2        
        //   894: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   897: istore          5
        //   899: iload           5
        //   901: istore_3       
        //   902: iload           6
        //   904: iconst_4       
        //   905: if_icmple       1037
        //   908: aload           7
        //   910: iconst_4       
        //   911: aaload         
        //   912: astore_2       
        //   913: aload_2        
        //   914: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   917: istore          5
        //   919: iload           5
        //   921: istore          4
        //   923: aload           8
        //   925: iload_1        
        //   926: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.setPlayTime:(I)V
        //   929: aload           8
        //   931: iload_3        
        //   932: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.setNumber:(I)V
        //   935: aload           8
        //   937: iload           4
        //   939: invokevirtual   com/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol.setTotalNumber:(I)V
        //   942: aload_0        
        //   943: getfield        com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.eventHandler:Lcom/backaudio/android/driver/bluetooth/IBluetoothEventHandler;
        //   946: aload           8
        //   948: invokeinterface com/backaudio/android/driver/bluetooth/IBluetoothEventHandler.onMediaInfo:(Lcom/backaudio/android/driver/bluetooth/bc8mpprotocol/MediaInfoProtocol;)V
        //   953: ldc             ""
        //   955: astore_2       
        //   956: aload           7
        //   958: arraylength    
        //   959: istore_3       
        //   960: iconst_0       
        //   961: istore_1       
        //   962: iload_1        
        //   963: iload_3        
        //   964: if_icmplt       1052
        //   967: getstatic       com/backaudio/android/driver/bluetooth/BluetoothProtocolAnalyzer2.logger:Lorg/slf4j/Logger;
        //   970: new             Ljava/lang/StringBuilder;
        //   973: dup            
        //   974: ldc_w           "tag-bt2MI: -+- "
        //   977: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   980: aload_2        
        //   981: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   984: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   987: invokeinterface org/slf4j/Logger.debug:(Ljava/lang/String;)V
        //   992: return         
        //   993: ldc_w           "UNKNOWN"
        //   996: astore_2       
        //   997: goto            832
        //  1000: ldc_w           "UNKNOWN"
        //  1003: astore_2       
        //  1004: goto            849
        //  1007: ldc_w           "-1"
        //  1010: astore_2       
        //  1011: goto            873
        //  1014: astore_2       
        //  1015: aload_2        
        //  1016: invokevirtual   java/lang/NumberFormatException.printStackTrace:()V
        //  1019: goto            882
        //  1022: ldc_w           "-1"
        //  1025: astore_2       
        //  1026: goto            893
        //  1029: astore_2       
        //  1030: aload_2        
        //  1031: invokevirtual   java/lang/NumberFormatException.printStackTrace:()V
        //  1034: goto            902
        //  1037: ldc_w           "-1"
        //  1040: astore_2       
        //  1041: goto            913
        //  1044: astore_2       
        //  1045: aload_2        
        //  1046: invokevirtual   java/lang/NumberFormatException.printStackTrace:()V
        //  1049: goto            923
        //  1052: aload           7
        //  1054: iload_1        
        //  1055: aaload         
        //  1056: astore          8
        //  1058: new             Ljava/lang/StringBuilder;
        //  1061: dup            
        //  1062: aload_2        
        //  1063: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //  1066: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //  1069: aload           8
        //  1071: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1074: ldc_w           " "
        //  1077: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1080: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1083: astore_2       
        //  1084: iload_1        
        //  1085: iconst_1       
        //  1086: iadd           
        //  1087: istore_1       
        //  1088: goto            962
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  873    879    1014   1022   Ljava/lang/NumberFormatException;
        //  893    899    1029   1037   Ljava/lang/NumberFormatException;
        //  913    919    1044   1052   Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 470, Size: 470
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void handleStartP(final char c, final byte[] array) {
        final int length = array.length;
        switch (c) {
            default: {}
            case '1': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2P1: onPairingModeResult() success");
                this.eventHandler.onPairingModeResult(new EnterPairingModeResult(null, "0"));
            }
            case '0': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2P0: onPairingModeResult() fail");
                this.eventHandler.onPairingModeResult(new EnterPairingModeResult(null, null));
            }
            case 'A': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2PA: " + new String(array));
            }
            case 'B': {
                final String[] split = new String(array, 2, length - 4).split("\n");
                if (split == null || split.length < 2) {
                    BluetoothProtocolAnalyzer2.logger.debug("tag-bt2PB: invalid protocol name=" + split[0]);
                    return;
                }
                final String s = split[0];
                final String s2 = split[1];
                this.eventHandler.onPhoneBook(s, s2);
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2PB: name=" + s + " number=" + s2);
            }
            case 'C': {
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2PC: onFinishDownloadPhoneBook()");
                this.eventHandler.onFinishDownloadPhoneBook();
            }
            case 'D': {
                final String[] split2 = new String(array, 2, length - 4).split("\n");
                if (split2 == null || split2.length < 2) {
                    BluetoothProtocolAnalyzer2.logger.debug("tag-bt2PD: invalid protocol name=" + split2[0]);
                    return;
                }
                final String s3 = split2[0];
                final String s4 = split2[1];
                this.eventHandler.onPhoneBook(null, null);
                BluetoothProtocolAnalyzer2.logger.debug("tag-bt2PD: name=" + s3 + " number=" + s4);
            }
        }
    }
    
    public void push(final byte[] array) {
        final int length = array.length;
        int i = 0;
    Label_0025_Outer:
        while (i < length) {
            Label_0076: {
                if (array[i] != 13) {
                    break Label_0076;
                }
                this.mNextExpectedValue = 10;
            Label_0069_Outer:
                while (true) {
                    if (array[i] == -1) {
                        array[i] = 10;
                    }
                    this.baos.write(array, i, 1);
                    while (true) {
                        if (!this.mValidProtocol) {
                            break Label_0069;
                        }
                        try {
                            this.analyze(this.baos.toByteArray());
                            this.reset();
                            ++i;
                            continue Label_0025_Outer;
                            while (true) {
                                this.mValidProtocol = true;
                                continue Label_0069_Outer;
                                Block_6: {
                                    break Block_6;
                                    Label_0101: {
                                        this.mNextExpectedValue = -1;
                                    }
                                    continue Label_0069_Outer;
                                }
                                continue;
                            }
                        }
                        // iftrue(Label_0101:, array[i] != 10)
                        // iftrue(Label_0025:, this.mNextExpectedValue != 10)
                        catch (Exception ex) {
                            BluetoothProtocolAnalyzer2.logger.debug("tag-bt2analyze error" + ex);
                            this.reset();
                            continue;
                        }
                        finally {
                            this.reset();
                        }
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public void push(byte[] copy, final int n, final int n2) {
        copy = Arrays.copyOf(copy, n2);
        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2 bluetoothprot recv::" + Utils.byteArrayToHexString(copy, 0, copy.length));
        while (true) {
            try {
                Utils.saveLogLine(Utils.byteArrayToHexString(copy, 0, copy.length), true);
                this.push(copy);
            }
            catch (IOException ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    @Override
    public void reset() {
        this.mNextExpectedValue = -1;
        this.mValidProtocol = false;
        this.baos.reset();
    }
    
    @Override
    public void setEventHandler(final IBluetoothEventHandler eventHandler) {
        BluetoothProtocolAnalyzer2.logger.debug("tag-bt2            handler");
        this.eventHandler = eventHandler;
    }
}
