package com.backaudio.android.driver.bluetooth;

import com.backaudio.android.driver.bluetooth.bc8mpprotocol.*;

public interface IBluetoothEventHandler
{
    void onAnswerPhone(final AnswerPhoneResult p0);
    
    void onCallOut(final CallOutResult p0);
    
    void onConnectedDevice(final ConnectedDeviceProtocol p0);
    
    void onDeviceName(final DeviceNameProtocol p0);
    
    void onDeviceRemoved(final DeviceRemovedProtocol p0);
    
    void onFinishDownloadPhoneBook();
    
    void onHangUpPhone(final HangUpPhoneResult p0);
    
    void onIncomingCall(final IncomingCallProtocol p0);
    
    void onMediaInfo(final MediaInfoProtocol p0);
    
    void onMediaPlayStatus(final MediaPlayStatusProtocol p0);
    
    void onMediaStatus(final MediaStatusProtocol p0);
    
    void onPairedDevice(final String p0, final String p1);
    
    void onPairingList(final PairingListProtocol p0);
    
    void onPairingModeEnd();
    
    void onPairingModeResult(final EnterPairingModeResult p0);
    
    void onPhoneBook(final String p0, final String p1);
    
    void onPhoneBookCtrlStatus(final PhoneBookCtrlStatusProtocol p0);
    
    void onPhoneBookList(final PhoneBookListProtocol p0);
    
    void onPhoneCallingOut(final CallingOutProtocol p0);
    
    void onPhoneStatus(final PhoneStatusProtocol p0);
    
    void onSetPlayStatus(final SetPlayStatusProtocol p0);
    
    void onVersion(final VersionProtocol p0);
    
    void ondeviceSwitchedProtocol(final DeviceSwitchedProtocol p0);
}
