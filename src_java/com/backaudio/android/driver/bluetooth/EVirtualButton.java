package com.backaudio.android.driver.bluetooth;

public enum EVirtualButton
{
    ASTERISK("ASTERISK", 10), 
    EIGHT("EIGHT", 7), 
    FIVE("FIVE", 4), 
    FOUR("FOUR", 3), 
    NINE("NINE", 8), 
    ONE("ONE", 0), 
    SEVEN("SEVEN", 6), 
    SIX("SIX", 5), 
    THREE("THREE", 2), 
    TWO("TWO", 1), 
    WELL("WELL", 11), 
    ZERO("ZERO", 9);
    
    static /* synthetic */ int[] $SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton() {
        final int[] $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton = EVirtualButton.$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton;
        if ($switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton != null) {
            return $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton;
        }
        final int[] $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2 = new int[values().length];
        while (true) {
            try {
                $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.ASTERISK.ordinal()] = 11;
                try {
                    $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.EIGHT.ordinal()] = 8;
                    try {
                        $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.FIVE.ordinal()] = 5;
                        try {
                            $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.FOUR.ordinal()] = 4;
                            try {
                                $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.NINE.ordinal()] = 9;
                                try {
                                    $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.ONE.ordinal()] = 1;
                                    try {
                                        $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.SEVEN.ordinal()] = 7;
                                        try {
                                            $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.SIX.ordinal()] = 6;
                                            try {
                                                $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.THREE.ordinal()] = 3;
                                                try {
                                                    $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.TWO.ordinal()] = 2;
                                                    try {
                                                        $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.WELL.ordinal()] = 12;
                                                        try {
                                                            $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2[EVirtualButton.ZERO.ordinal()] = 10;
                                                            return EVirtualButton.$SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton = $switch_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton2;
                                                        }
                                                        catch (NoSuchFieldError noSuchFieldError) {}
                                                    }
                                                    catch (NoSuchFieldError noSuchFieldError2) {}
                                                }
                                                catch (NoSuchFieldError noSuchFieldError3) {}
                                            }
                                            catch (NoSuchFieldError noSuchFieldError4) {}
                                        }
                                        catch (NoSuchFieldError noSuchFieldError5) {}
                                    }
                                    catch (NoSuchFieldError noSuchFieldError6) {}
                                }
                                catch (NoSuchFieldError noSuchFieldError7) {}
                            }
                            catch (NoSuchFieldError noSuchFieldError8) {}
                        }
                        catch (NoSuchFieldError noSuchFieldError9) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError10) {}
                }
                catch (NoSuchFieldError noSuchFieldError11) {}
            }
            catch (NoSuchFieldError noSuchFieldError12) {
                continue;
            }
            break;
        }
    }
    
    private EVirtualButton(final String s, final int n) {
    }
    
    public static final String parse(final char c) {
        if (c == '*' || c == '#' || (c >= '0' && c <= '9')) {
            return String.valueOf(c);
        }
        return null;
    }
    
    public static final String parse(final EVirtualButton eVirtualButton) {
        switch ($SWITCH_TABLE$com$backaudio$android$driver$bluetooth$EVirtualButton()[eVirtualButton.ordinal()]) {
            default: {
                return null;
            }
            case 1: {
                return "1";
            }
            case 2: {
                return "2";
            }
            case 3: {
                return "3";
            }
            case 4: {
                return "4";
            }
            case 5: {
                return "5";
            }
            case 6: {
                return "6";
            }
            case 7: {
                return "7";
            }
            case 8: {
                return "8";
            }
            case 9: {
                return "9";
            }
            case 10: {
                return "0";
            }
            case 11: {
                return "*";
            }
            case 12: {
                return "#";
            }
        }
    }
}
