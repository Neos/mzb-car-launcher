package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

import java.util.*;

public class MediaInfoProtocol extends BaseMultilineProtocol
{
    private String album;
    private String artist;
    private String genre;
    private boolean isAnalyzed;
    private int number;
    private int playTime;
    private String title;
    private int totalNumber;
    
    public MediaInfoProtocol() {
        this.isAnalyzed = false;
    }
    
    private void analyze() {
        for (final MediaInfoUnitProtocol next : super.getUnits()) {
            byte[] playload = null;
            String charSet = null;
            Label_0228: {
                Label_0207: {
                    Label_0183: {
                        Label_0159: {
                            Label_0138: {
                                Label_0117: {
                                    try {
                                        playload = next.getPlayload();
                                        charSet = this.getCharSet(playload);
                                        switch (playload[0]) {
                                            case 1: {
                                                this.title = new String(playload, 3, playload.length - 3, charSet);
                                                continue;
                                            }
                                            case 2: {
                                                break Label_0117;
                                            }
                                            case 3: {
                                                break Label_0138;
                                            }
                                            case 4: {
                                                break Label_0159;
                                            }
                                            case 5: {
                                                break Label_0183;
                                            }
                                            case 6: {
                                                break Label_0207;
                                            }
                                            case 7: {
                                                break Label_0228;
                                            }
                                            default: {
                                                continue;
                                            }
                                        }
                                    }
                                    catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                    continue;
                                }
                                this.artist = new String(playload, 3, playload.length - 3, charSet);
                                continue;
                            }
                            this.album = new String(playload, 3, playload.length - 3, charSet);
                            continue;
                        }
                        this.number = Integer.parseInt(new String(playload, 3, playload.length - 3, charSet));
                        continue;
                    }
                    this.totalNumber = Integer.parseInt(new String(playload, 3, playload.length - 3, charSet));
                    continue;
                }
                this.genre = new String(playload, 3, playload.length - 3, charSet);
                continue;
            }
            this.playTime = Integer.parseInt(new String(playload, 3, playload.length - 3, charSet));
        }
    }
    
    private String getCharSet(final byte[] array) {
        switch (array[1] + (array[2] << 8)) {
            default: {
                return null;
            }
            case 3: {
                return "ASCII";
            }
            case 4: {
                return "ISO-8859-1";
            }
            case 15: {
                return "JIS-X0201";
            }
            case 17: {
                return "SHIFT-JIS";
            }
            case 36: {
                return "KS-C-5601-1987";
            }
            case 106: {
                return "UTF-8";
            }
            case 1000: {
                return "UCS2";
            }
            case 1013: {
                return "UTF-16BE";
            }
            case 2025: {
                return "GB2312";
            }
            case 2026: {
                return "BIG5";
            }
        }
    }
    
    public String getAlbum() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.album;
    }
    
    public String getArtist() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.artist;
    }
    
    public String getGenre() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.genre;
    }
    
    public int getNumber() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.number;
    }
    
    public int getPlayTime() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.playTime;
    }
    
    public String getTitle() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.title;
    }
    
    public int getTotalNumber() {
        if (!this.isAnalyzed) {
            this.analyze();
        }
        return this.totalNumber;
    }
    
    public void setAnalyzed(final boolean isAnalyzed) {
        this.isAnalyzed = isAnalyzed;
    }
    
    public void setArtist(final String artist) {
        this.artist = artist;
    }
    
    public void setNumber(final int number) {
        this.number = number;
    }
    
    public void setPlayTime(final int playTime) {
        this.playTime = playTime;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public void setTotalNumber(final int totalNumber) {
        this.totalNumber = totalNumber;
    }
}
