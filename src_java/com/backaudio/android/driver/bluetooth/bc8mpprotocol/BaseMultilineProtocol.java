package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

import java.util.*;

public class BaseMultilineProtocol
{
    private List<Object> units;
    
    public BaseMultilineProtocol() {
        this.units = new ArrayList<Object>();
    }
    
    public void addUnit(final Object o) {
        if (!this.units.contains(o)) {
            this.units.add(o);
        }
    }
    
    public List getUnits() {
        return this.units;
    }
}
