package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class EnterPairingModeResult extends AbstractLineProtocol
{
    private boolean isSuccess;
    
    public EnterPairingModeResult(final String s, final String s2) {
        super(s, s2);
        this.isSuccess = false;
        try {
            if (s2.equals("0")) {
                this.isSuccess = true;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public boolean isSuccess() {
        return this.isSuccess;
    }
}
