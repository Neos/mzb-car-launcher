package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

import java.util.*;

public class PairingListProtocol
{
    private List<PairingListUnitProtocol> units;
    
    public PairingListProtocol(final BaseMultilineProtocol baseMultilineProtocol) {
        try {
            this.units = (List<PairingListUnitProtocol>)baseMultilineProtocol.getUnits();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public List<PairingListUnitProtocol> getUnits() {
        return this.units;
    }
}
