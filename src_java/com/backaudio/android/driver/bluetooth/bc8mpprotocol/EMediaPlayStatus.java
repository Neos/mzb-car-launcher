package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public enum EMediaPlayStatus
{
    PAUSE("PAUSE", 2), 
    PLAYING("PLAYING", 1), 
    STOP("STOP", 0);
    
    private EMediaPlayStatus(final String s, final int n) {
    }
}
