package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class IncomingCallProtocol extends AbstractLineProtocol
{
    private String phone;
    
    public IncomingCallProtocol(final String s, final String phone) {
        super(s, phone);
        this.phone = phone;
    }
    
    public String getPhone() {
        return this.phone;
    }
}
