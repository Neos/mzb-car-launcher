package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class MediaStatusProtocol extends AbstractLineProtocol
{
    private EMediaStatus mediaStatus;
    
    public MediaStatusProtocol(final String s, final String s2) {
        super(s, s2);
        if ("0".equals(s2)) {
            this.mediaStatus = EMediaStatus.UNCONNECT;
        }
        else {
            if ("1".equals(s2)) {
                this.mediaStatus = EMediaStatus.CONNECTIING;
                return;
            }
            if ("2".equals(s2)) {
                this.mediaStatus = EMediaStatus.CONNECTED;
            }
        }
    }
    
    public EMediaStatus getMediaStatus() {
        return this.mediaStatus;
    }
    
    public void setMediaStatus(final EMediaStatus mediaStatus) {
        this.mediaStatus = mediaStatus;
    }
}
