package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class DeviceRemovedProtocol extends AbstractLineProtocol
{
    private String address;
    
    public DeviceRemovedProtocol(final String s, final String s2) {
        super(s, s2);
        try {
            this.address = s2.substring(44);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public String getAddress() {
        return this.address;
    }
}
