package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class PairingListUnitProtocol extends AbstractLineProtocol
{
    private String deviceAddress;
    private String deviceName;
    
    public PairingListUnitProtocol(final String s, final String s2) {
        super(s, s2);
        try {
            this.deviceAddress = s2.substring(s2.indexOf(44) + 1, s2.lastIndexOf(44));
            this.deviceName = s2.substring(s2.lastIndexOf(",\"") + 2, s2.lastIndexOf(34));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public String getDeviceAddress() {
        return this.deviceAddress;
    }
    
    public String getDeviceName() {
        return this.deviceName;
    }
}
