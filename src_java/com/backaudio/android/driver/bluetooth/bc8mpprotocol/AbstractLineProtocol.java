package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public abstract class AbstractLineProtocol
{
    private String key;
    private String value;
    
    public AbstractLineProtocol(final String key, final String value) {
        this.key = key;
        this.value = value;
    }
    
    public String getKey() {
        return this.key;
    }
    
    public String getValue() {
        return this.value;
    }
}
