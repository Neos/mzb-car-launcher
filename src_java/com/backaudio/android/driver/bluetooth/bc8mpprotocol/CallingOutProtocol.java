package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class CallingOutProtocol extends AbstractLineProtocol
{
    private String phoneNumber;
    
    public CallingOutProtocol(final String s, final String phoneNumber) {
        super(s, phoneNumber);
        this.phoneNumber = phoneNumber;
    }
    
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
}
