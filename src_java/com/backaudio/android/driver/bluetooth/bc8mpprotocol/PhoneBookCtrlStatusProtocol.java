package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class PhoneBookCtrlStatusProtocol extends AbstractLineProtocol
{
    private EPhoneBookCtrlStatus phoneBookCtrlStatus;
    
    public PhoneBookCtrlStatusProtocol(final String s, final String s2) {
        super(s, s2);
        if (s2.equals("0")) {
            this.phoneBookCtrlStatus = EPhoneBookCtrlStatus.UNCONNECT;
        }
        else {
            if (s2.equals("1")) {
                this.phoneBookCtrlStatus = EPhoneBookCtrlStatus.CONECTING;
                return;
            }
            if (s2.equals("2")) {
                this.phoneBookCtrlStatus = EPhoneBookCtrlStatus.CONNECTED;
                return;
            }
            if (s2.equals("3")) {
                this.phoneBookCtrlStatus = EPhoneBookCtrlStatus.DOWNLOADING;
            }
        }
    }
    
    public EPhoneBookCtrlStatus getPhoneBookCtrlStatus() {
        return this.phoneBookCtrlStatus;
    }
}
