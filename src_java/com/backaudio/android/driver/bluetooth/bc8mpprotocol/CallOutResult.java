package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class CallOutResult extends AbstractLineProtocol
{
    private boolean isSuccess;
    
    public CallOutResult(final String s, final String s2) {
        super(s, s2);
        this.isSuccess = false;
        if (s2.equals("0")) {
            this.isSuccess = true;
        }
    }
    
    public boolean isSuccess() {
        return this.isSuccess;
    }
}
