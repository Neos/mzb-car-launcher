package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class PhoneStatusProtocol extends AbstractLineProtocol
{
    private EPhoneStatus phoneStatus;
    
    public PhoneStatusProtocol(final String s, final String s2) {
        super(s, s2);
        if (s2.equals("0")) {
            this.phoneStatus = EPhoneStatus.UNCONNECT;
        }
        else {
            if (s2.equals("1")) {
                this.phoneStatus = EPhoneStatus.CONNECTING;
                return;
            }
            if (s2.equals("2")) {
                this.phoneStatus = EPhoneStatus.CONNECTED;
                return;
            }
            if (s2.equals("3")) {
                this.phoneStatus = EPhoneStatus.INCOMING_CALL;
                return;
            }
            if (s2.equals("4")) {
                this.phoneStatus = EPhoneStatus.CALLING_OUT;
                return;
            }
            if (s2.equals("5")) {
                this.phoneStatus = EPhoneStatus.TALKING;
                return;
            }
            if (s2.equals("6")) {
                this.phoneStatus = EPhoneStatus.MULTI_TALKING;
            }
        }
    }
    
    public EPhoneStatus getPhoneStatus() {
        return this.phoneStatus;
    }
    
    public void setPhoneStatus(final EPhoneStatus phoneStatus) {
        this.phoneStatus = phoneStatus;
    }
    
    public void setPhoneStatus(final String s) {
        if (s == null) {
            this.phoneStatus = EPhoneStatus.UNCONNECT;
            return;
        }
        if (s.equals("0")) {
            this.phoneStatus = EPhoneStatus.INITIALIZING;
            return;
        }
        if (s.equals("1")) {
            this.phoneStatus = EPhoneStatus.UNCONNECT;
            return;
        }
        if (s.equals("2")) {
            this.phoneStatus = EPhoneStatus.CONNECTING;
            return;
        }
        if (s.equals("3")) {
            this.phoneStatus = EPhoneStatus.CONNECTED;
            return;
        }
        if (s.equals("4")) {
            this.phoneStatus = EPhoneStatus.CALLING_OUT;
            return;
        }
        if (s.equals("5")) {
            this.phoneStatus = EPhoneStatus.INCOMING_CALL;
            return;
        }
        if (s.equals("6")) {
            this.phoneStatus = EPhoneStatus.TALKING;
            return;
        }
        this.phoneStatus = EPhoneStatus.UNCONNECT;
    }
}
