package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class DeviceNameProtocol extends AbstractLineProtocol
{
    private String MACAddr;
    private String PIN;
    private String deviceName;
    private boolean success;
    
    public DeviceNameProtocol(final String s, final String s2) {
        super(s, s2);
        this.success = false;
        try {
            if (s2.charAt(0) == '0') {
                this.deviceName = s2.substring(s2.indexOf(44) + 1);
                this.success = true;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public String getDeviceName() {
        return this.deviceName;
    }
    
    public String getMACAdress() {
        return this.MACAddr;
    }
    
    public String getPIN() {
        return this.PIN;
    }
    
    public boolean isSuccess() {
        return this.success;
    }
    
    public void setDeviceName(final String deviceName) {
        this.success = true;
        this.deviceName = deviceName;
    }
    
    public void setMACAddr(final String macAddr) {
        this.MACAddr = macAddr;
    }
    
    public void setPIN(final String pin) {
        this.PIN = pin;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DeviceNameProtocol [deviceName=");
        sb.append(this.deviceName);
        sb.append(", PIN=");
        sb.append(this.PIN);
        sb.append(", MACAddr=");
        sb.append(this.MACAddr);
        sb.append("]");
        return sb.toString();
    }
}
