package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class VersionProtocol extends AbstractLineProtocol
{
    public VersionProtocol(final String s, final String s2) {
        super(s, s2);
    }
    
    public String getVersion() {
        return super.getValue();
    }
}
