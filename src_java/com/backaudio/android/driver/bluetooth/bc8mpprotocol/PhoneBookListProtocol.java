package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class PhoneBookListProtocol extends AbstractPlayLoadProtocol
{
    public PhoneBookListProtocol(final String s, final String s2) {
        super(s, s2);
    }
    
    public void addUnit(final AbstractPlayLoadProtocol abstractPlayLoadProtocol) {
        super.push(abstractPlayLoadProtocol.getPlayload(), 0, abstractPlayLoadProtocol.getPlayload().length);
    }
}
