package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public enum EPhoneStatus
{
    CALLING_OUT("CALLING_OUT", 4), 
    CONNECTED("CONNECTED", 2), 
    CONNECTING("CONNECTING", 1), 
    INCOMING_CALL("INCOMING_CALL", 3), 
    INITIALIZING("INITIALIZING", 7), 
    MULTI_TALKING("MULTI_TALKING", 6), 
    TALKING("TALKING", 5), 
    UNCONNECT("UNCONNECT", 0);
    
    private EPhoneStatus(final String s, final int n) {
    }
}
