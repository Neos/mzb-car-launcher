package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

import java.util.*;

public class BaseMutilineProtocol
{
    private List<Object> units;
    
    public BaseMutilineProtocol() {
        this.units = new ArrayList<Object>();
    }
    
    public void addUnit(final PairingListUnitProtocol pairingListUnitProtocol) {
        if (!this.units.contains(pairingListUnitProtocol)) {
            this.units.add(pairingListUnitProtocol);
        }
    }
    
    public List getUnits() {
        return this.units;
    }
}
