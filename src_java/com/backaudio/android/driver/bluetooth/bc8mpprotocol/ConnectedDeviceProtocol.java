package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class ConnectedDeviceProtocol extends AbstractLineProtocol
{
    private String deviceAddress;
    private String deviceName;
    
    public ConnectedDeviceProtocol(final String s, final String deviceAddress) {
        super(s, deviceAddress);
        this.deviceAddress = deviceAddress;
    }
    
    public String getDeviceAddress() {
        return this.deviceAddress;
    }
    
    public String getDeviceName() {
        return this.deviceName;
    }
    
    public void setDeviceName(final String deviceName) {
        this.deviceName = deviceName;
    }
}
