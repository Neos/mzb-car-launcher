package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

import java.io.*;

public class AbstractPlayLoadProtocol extends AbstractLineProtocol
{
    private int needDataLength;
    private ByteArrayOutputStream playload;
    
    public AbstractPlayLoadProtocol(final String s, final String s2) {
        super(s, s2);
        this.needDataLength = 0;
        try {
            this.needDataLength = Integer.parseInt(s2);
            this.playload = new ByteArrayOutputStream(this.needDataLength);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public int getNeedDataLength() {
        return this.needDataLength;
    }
    
    public byte[] getPlayload() {
        return this.playload.toByteArray();
    }
    
    public void push(final byte[] array, final int n, final int n2) {
        this.playload.write(array, n, n2);
        this.needDataLength -= n2;
    }
    
    public void reset() {
        this.playload.reset();
    }
}
