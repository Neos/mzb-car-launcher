package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class DeviceSwitchedProtocol extends AbstractLineProtocol
{
    private EConnectedDevice connectedDevice;
    
    public DeviceSwitchedProtocol(final String s, final String s2) {
        super(s, s2);
        if (s2.equals("0")) {
            this.connectedDevice = EConnectedDevice.LOCAL;
            return;
        }
        this.connectedDevice = EConnectedDevice.REMOTE;
    }
    
    public EConnectedDevice getConnectedDevice() {
        return this.connectedDevice;
    }
}
