package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public enum ESetPlayStatus
{
    PALY_OR_PAUSE("PALY_OR_PAUSE", 1), 
    PLAY_NEXT("PLAY_NEXT", 2), 
    PLAY_PREV("PLAY_PREV", 3), 
    STOP("STOP", 0);
    
    private ESetPlayStatus(final String s, final int n) {
    }
}
