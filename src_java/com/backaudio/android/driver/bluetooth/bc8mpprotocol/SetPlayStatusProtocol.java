package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class SetPlayStatusProtocol extends AbstractLineProtocol
{
    private static final String PAUSE = "PP";
    private static final String PLAY_NEXT = "FWD";
    private static final String PLAY_PREV = "BACK";
    private static final String STOP = "STOP";
    private boolean isSuccess;
    private ESetPlayStatus playStatus;
    
    public SetPlayStatusProtocol(final String s, final String s2) {
        super(s, s2);
        this.playStatus = null;
        this.isSuccess = false;
        this.isSuccess = s2.equals("0");
        if (s.equals("PP")) {
            this.playStatus = ESetPlayStatus.PALY_OR_PAUSE;
        }
        else {
            if (s.equals("STOP")) {
                this.playStatus = ESetPlayStatus.STOP;
                return;
            }
            if (s.equals("FWD")) {
                this.playStatus = ESetPlayStatus.PLAY_NEXT;
                return;
            }
            if (s.equals("BACK")) {
                this.playStatus = ESetPlayStatus.PLAY_PREV;
            }
        }
    }
    
    public ESetPlayStatus getPlayStatus() {
        return this.playStatus;
    }
    
    public boolean isSuccess() {
        return this.isSuccess;
    }
}
