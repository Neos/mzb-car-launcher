package com.backaudio.android.driver.bluetooth.bc8mpprotocol;

public class MediaPlayStatusProtocol extends AbstractLineProtocol
{
    private EMediaPlayStatus mediaPlayStatus;
    
    public MediaPlayStatusProtocol(final String s, final String s2) {
        super(s, s2);
        if (s.equals("A2DPSTAT")) {
            if (s2.equals("0")) {
                this.mediaPlayStatus = EMediaPlayStatus.STOP;
            }
            else {
                if (s2.equals("3")) {
                    this.mediaPlayStatus = EMediaPlayStatus.PLAYING;
                    return;
                }
                if (s2.equals("2")) {
                    this.mediaPlayStatus = EMediaPlayStatus.PAUSE;
                    return;
                }
                if (this.mediaPlayStatus == null) {
                    this.mediaPlayStatus = EMediaPlayStatus.STOP;
                }
            }
        }
        else if (s.equals("PLAYSTAT")) {
            if (s2.equals("0")) {
                this.mediaPlayStatus = EMediaPlayStatus.STOP;
                return;
            }
            if (s2.equals("1")) {
                this.mediaPlayStatus = EMediaPlayStatus.PLAYING;
                return;
            }
            if (s2.equals("2")) {
                this.mediaPlayStatus = EMediaPlayStatus.PAUSE;
            }
        }
    }
    
    public EMediaPlayStatus getMediaPlayStatus() {
        return this.mediaPlayStatus;
    }
    
    public void setMediaPlayStatus(final EMediaPlayStatus mediaPlayStatus) {
        this.mediaPlayStatus = mediaPlayStatus;
    }
}
