package com.backaudio.android.driver;

import com.touchus.publicutils.sysconst.*;
import java.util.*;
import com.backaudio.android.driver.beans.*;

public interface IMainboardEventLisenner
{
    void logcatCanbox(final String p0);
    
    void obtainBenzSize(final int p0);
    
    void obtainBenzType(final BenzModel.EBenzTpye p0);
    
    void obtainBrightness(final int p0);
    
    void obtainLanguageMediaSet(final Mainboard.ELanguage p0);
    
    void obtainReverseMediaSet(final Mainboard.EReverserMediaSet p0);
    
    void obtainReverseType(final Mainboard.EReverseTpye p0);
    
    void obtainStoreData(final List<Byte> p0);
    
    void obtainVersionDate(final String p0);
    
    void obtainVersionNumber(final String p0);
    
    void onAUXActivateStutas(final Mainboard.EAUXStutas p0);
    
    void onAirInfo(final AirInfo p0);
    
    void onCanboxInfo(final String p0);
    
    void onCanboxUpgradeForGetDataByIndex(final int p0);
    
    void onCanboxUpgradeState(final Mainboard.ECanboxUpgrade p0);
    
    void onCarBaseInfo(final CarBaseInfo p0);
    
    void onCarRunningInfo(final CarRunInfo p0);
    
    void onEnterStandbyMode();
    
    void onHandleIdriver(final Mainboard.EIdriverEnum p0, final Mainboard.EBtnStateEnum p1);
    
    void onHornSoundValue(final int p0, final int p1, final int p2, final int p3);
    
    void onMcuUpgradeForGetDataByIndex(final int p0);
    
    void onMcuUpgradeState(final Mainboard.EUpgrade p0);
    
    void onOriginalCarView(final Mainboard.EControlSource p0, final boolean p1);
    
    void onShowOrHideCarLayer(final Mainboard.ECarLayer p0);
    
    void onTime(final int p0, final int p1, final int p2, final int p3, final int p4, final int p5, final int p6);
    
    void onWakeUp(final Mainboard.ECarLayer p0);
}
