package javax.mail;

import javax.mail.event.*;
import java.util.*;

class EventQueue implements Runnable
{
    private QueueElement head;
    private Thread qThread;
    private QueueElement tail;
    
    public EventQueue() {
        this.head = null;
        this.tail = null;
        (this.qThread = new Thread(this, "JavaMail-EventQueue")).setDaemon(true);
        this.qThread.start();
    }
    
    private QueueElement dequeue() throws InterruptedException {
        // monitorenter(this)
        while (true) {
            while (true) {
                try {
                    while (this.tail == null) {
                        this.wait();
                    }
                    final QueueElement tail = this.tail;
                    this.tail = tail.prev;
                    if (this.tail == null) {
                        this.head = null;
                        tail.next = null;
                        tail.prev = null;
                        return tail;
                    }
                }
                finally {
                }
                // monitorexit(this)
                this.tail.next = null;
                continue;
            }
        }
    }
    
    public void enqueue(final MailEvent mailEvent, final Vector vector) {
        synchronized (this) {
            final QueueElement queueElement = new QueueElement(mailEvent, vector);
            if (this.head == null) {
                this.head = queueElement;
                this.tail = queueElement;
            }
            else {
                queueElement.next = this.head;
                this.head.prev = queueElement;
                this.head = queueElement;
            }
            this.notifyAll();
        }
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                final QueueElement dequeue = this.dequeue();
                if (dequeue == null) {
                    break;
                }
                final MailEvent event = dequeue.event;
                final Vector vector = dequeue.vector;
                int i = 0;
                while (i < vector.size()) {
                    while (true) {
                        try {
                            event.dispatch(vector.elementAt(i));
                            ++i;
                        }
                        catch (Throwable t) {
                            if (t instanceof InterruptedException) {
                                return;
                            }
                            continue;
                        }
                        break;
                    }
                }
            }
        }
        catch (InterruptedException ex) {}
    }
    
    void stop() {
        if (this.qThread != null) {
            this.qThread.interrupt();
            this.qThread = null;
        }
    }
    
    static class QueueElement
    {
        MailEvent event;
        QueueElement next;
        QueueElement prev;
        Vector vector;
        
        QueueElement(final MailEvent event, final Vector vector) {
            this.next = null;
            this.prev = null;
            this.event = null;
            this.vector = null;
            this.event = event;
            this.vector = vector;
        }
    }
}
