package javax.mail;

import java.security.*;
import java.io.*;
import com.sun.mail.util.*;
import java.util.*;
import java.net.*;

public final class Session
{
    private static Session defaultSession;
    private final Properties addressMap;
    private final Hashtable authTable;
    private final Authenticator authenticator;
    private boolean debug;
    private PrintStream out;
    private final Properties props;
    private final Vector providers;
    private final Hashtable providersByClassName;
    private final Hashtable providersByProtocol;
    
    static {
        Session.defaultSession = null;
    }
    
    private Session(final Properties props, final Authenticator authenticator) {
        this.authTable = new Hashtable();
        this.debug = false;
        this.providers = new Vector();
        this.providersByProtocol = new Hashtable();
        this.providersByClassName = new Hashtable();
        this.addressMap = new Properties();
        this.props = props;
        this.authenticator = authenticator;
        if (Boolean.valueOf(props.getProperty("mail.debug"))) {
            this.debug = true;
        }
        if (this.debug) {
            this.pr("DEBUG: JavaMail version 1.4.1");
        }
        Serializable s;
        if (authenticator != null) {
            s = authenticator.getClass();
        }
        else {
            s = this.getClass();
        }
        this.loadProviders((Class)s);
        this.loadAddressMap((Class)s);
    }
    
    private static ClassLoader getContextClassLoader() {
        return AccessController.doPrivileged((PrivilegedAction<ClassLoader>)new PrivilegedAction() {
            @Override
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                }
                catch (SecurityException ex) {
                    return null;
                }
            }
        });
    }
    
    public static Session getDefaultInstance(final Properties properties) {
        return getDefaultInstance(properties, null);
    }
    
    public static Session getDefaultInstance(final Properties properties, final Authenticator authenticator) {
        synchronized (Session.class) {
            if (Session.defaultSession == null) {
                Session.defaultSession = new Session(properties, authenticator);
            }
            else if (Session.defaultSession.authenticator != authenticator && (Session.defaultSession.authenticator == null || authenticator == null || Session.defaultSession.authenticator.getClass().getClassLoader() != authenticator.getClass().getClassLoader())) {
                throw new SecurityException("Access to default session denied");
            }
            return Session.defaultSession;
        }
    }
    
    public static Session getInstance(final Properties properties) {
        return new Session(properties, null);
    }
    
    public static Session getInstance(final Properties properties, final Authenticator authenticator) {
        return new Session(properties, authenticator);
    }
    
    private static InputStream getResourceAsStream(final Class clazz, final String s) throws IOException {
        try {
            return AccessController.doPrivileged((PrivilegedExceptionAction<InputStream>)new PrivilegedExceptionAction() {
                @Override
                public Object run() throws IOException {
                    return clazz.getResourceAsStream(s);
                }
            });
        }
        catch (PrivilegedActionException ex) {
            throw (IOException)ex.getException();
        }
    }
    
    private static URL[] getResources(final ClassLoader classLoader, final String s) {
        return AccessController.doPrivileged((PrivilegedAction<URL[]>)new PrivilegedAction() {
            @Override
            public Object run() {
                URL[] array3;
                URL[] array2;
                final URL[] array = array2 = (array3 = null);
                URL[] array4;
                try {
                    final Vector<URL> vector = new Vector<URL>();
                    array3 = array;
                    array2 = array;
                    final Enumeration<URL> resources = classLoader.getResources(s);
                    while (resources != null) {
                        array3 = array;
                        array2 = array;
                        if (!resources.hasMoreElements()) {
                            break;
                        }
                        array3 = array;
                        array2 = array;
                        final URL url = resources.nextElement();
                        if (url == null) {
                            continue;
                        }
                        array3 = array;
                        array2 = array;
                        vector.addElement(url);
                    }
                    array3 = array;
                    array2 = array;
                    array4 = array;
                    if (vector.size() > 0) {
                        array3 = array;
                        array2 = array;
                        final URL[] array5 = array2 = (array3 = new URL[vector.size()]);
                        vector.copyInto(array5);
                        return array5;
                    }
                }
                catch (IOException ex) {
                    return array3;
                }
                catch (SecurityException ex2) {
                    array4 = array2;
                }
                return array4;
            }
        });
    }
    
    private Object getService(final Provider p0, final URLName p1) throws NoSuchProviderException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnonnull       14
        //     4: new             Ljavax/mail/NoSuchProviderException;
        //     7: dup            
        //     8: ldc             "null"
        //    10: invokespecial   javax/mail/NoSuchProviderException.<init>:(Ljava/lang/String;)V
        //    13: athrow         
        //    14: aload_2        
        //    15: astore          4
        //    17: aload_2        
        //    18: ifnonnull       39
        //    21: new             Ljavax/mail/URLName;
        //    24: dup            
        //    25: aload_1        
        //    26: invokevirtual   javax/mail/Provider.getProtocol:()Ljava/lang/String;
        //    29: aconst_null    
        //    30: iconst_m1      
        //    31: aconst_null    
        //    32: aconst_null    
        //    33: aconst_null    
        //    34: invokespecial   javax/mail/URLName.<init>:(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //    37: astore          4
        //    39: aload_0        
        //    40: getfield        javax/mail/Session.authenticator:Ljavax/mail/Authenticator;
        //    43: ifnull          135
        //    46: aload_0        
        //    47: getfield        javax/mail/Session.authenticator:Ljavax/mail/Authenticator;
        //    50: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //    53: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //    56: astore          5
        //    58: aconst_null    
        //    59: astore_3       
        //    60: invokestatic    javax/mail/Session.getContextClassLoader:()Ljava/lang/ClassLoader;
        //    63: astore          6
        //    65: aload_3        
        //    66: astore_2       
        //    67: aload           6
        //    69: ifnull          82
        //    72: aload           6
        //    74: aload_1        
        //    75: invokevirtual   javax/mail/Provider.getClassName:()Ljava/lang/String;
        //    78: invokevirtual   java/lang/ClassLoader.loadClass:(Ljava/lang/String;)Ljava/lang/Class;
        //    81: astore_2       
        //    82: aload_2        
        //    83: astore_3       
        //    84: aload_2        
        //    85: ifnonnull       98
        //    88: aload           5
        //    90: aload_1        
        //    91: invokevirtual   javax/mail/Provider.getClassName:()Ljava/lang/String;
        //    94: invokevirtual   java/lang/ClassLoader.loadClass:(Ljava/lang/String;)Ljava/lang/Class;
        //    97: astore_3       
        //    98: aload_3        
        //    99: iconst_2       
        //   100: anewarray       Ljava/lang/Class;
        //   103: dup            
        //   104: iconst_0       
        //   105: ldc             Ljavax/mail/Session;.class
        //   107: aastore        
        //   108: dup            
        //   109: iconst_1       
        //   110: ldc             Ljavax/mail/URLName;.class
        //   112: aastore        
        //   113: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   116: iconst_2       
        //   117: anewarray       Ljava/lang/Object;
        //   120: dup            
        //   121: iconst_0       
        //   122: aload_0        
        //   123: aastore        
        //   124: dup            
        //   125: iconst_1       
        //   126: aload           4
        //   128: aastore        
        //   129: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   132: astore_2       
        //   133: aload_2        
        //   134: areturn        
        //   135: aload_0        
        //   136: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   139: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   142: astore          5
        //   144: goto            58
        //   147: astore_2       
        //   148: aload_1        
        //   149: invokevirtual   javax/mail/Provider.getClassName:()Ljava/lang/String;
        //   152: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   155: astore_3       
        //   156: goto            98
        //   159: astore_2       
        //   160: aload_0        
        //   161: getfield        javax/mail/Session.debug:Z
        //   164: ifeq            175
        //   167: aload_2        
        //   168: aload_0        
        //   169: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //   172: invokevirtual   java/lang/Exception.printStackTrace:(Ljava/io/PrintStream;)V
        //   175: new             Ljavax/mail/NoSuchProviderException;
        //   178: dup            
        //   179: aload_1        
        //   180: invokevirtual   javax/mail/Provider.getProtocol:()Ljava/lang/String;
        //   183: invokespecial   javax/mail/NoSuchProviderException.<init>:(Ljava/lang/String;)V
        //   186: athrow         
        //   187: astore_2       
        //   188: aload_0        
        //   189: getfield        javax/mail/Session.debug:Z
        //   192: ifeq            203
        //   195: aload_2        
        //   196: aload_0        
        //   197: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //   200: invokevirtual   java/lang/Exception.printStackTrace:(Ljava/io/PrintStream;)V
        //   203: new             Ljavax/mail/NoSuchProviderException;
        //   206: dup            
        //   207: aload_1        
        //   208: invokevirtual   javax/mail/Provider.getProtocol:()Ljava/lang/String;
        //   211: invokespecial   javax/mail/NoSuchProviderException.<init>:(Ljava/lang/String;)V
        //   214: athrow         
        //   215: astore_2       
        //   216: aload_3        
        //   217: astore_2       
        //   218: goto            82
        //    Exceptions:
        //  throws javax.mail.NoSuchProviderException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  60     65     147    187    Ljava/lang/Exception;
        //  72     82     215    221    Ljava/lang/ClassNotFoundException;
        //  72     82     147    187    Ljava/lang/Exception;
        //  88     98     147    187    Ljava/lang/Exception;
        //  98     133    187    215    Ljava/lang/Exception;
        //  148    156    159    187    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0082:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private Store getStore(final Provider provider, final URLName urlName) throws NoSuchProviderException {
        if (provider == null || provider.getType() != Provider.Type.STORE) {
            throw new NoSuchProviderException("invalid provider");
        }
        try {
            return (Store)this.getService(provider, urlName);
        }
        catch (ClassCastException ex) {
            throw new NoSuchProviderException("incorrect class");
        }
    }
    
    private static URL[] getSystemResources(final String s) {
        return AccessController.doPrivileged((PrivilegedAction<URL[]>)new PrivilegedAction() {
            @Override
            public Object run() {
                URL[] array3;
                URL[] array2;
                final URL[] array = array2 = (array3 = null);
                URL[] array4;
                try {
                    final Vector<URL> vector = new Vector<URL>();
                    array3 = array;
                    array2 = array;
                    final Enumeration<URL> systemResources = ClassLoader.getSystemResources(s);
                    while (systemResources != null) {
                        array3 = array;
                        array2 = array;
                        if (!systemResources.hasMoreElements()) {
                            break;
                        }
                        array3 = array;
                        array2 = array;
                        final URL url = systemResources.nextElement();
                        if (url == null) {
                            continue;
                        }
                        array3 = array;
                        array2 = array;
                        vector.addElement(url);
                    }
                    array3 = array;
                    array2 = array;
                    array4 = array;
                    if (vector.size() > 0) {
                        array3 = array;
                        array2 = array;
                        final URL[] array5 = array2 = (array3 = new URL[vector.size()]);
                        vector.copyInto(array5);
                        return array5;
                    }
                }
                catch (IOException ex) {
                    return array3;
                }
                catch (SecurityException ex2) {
                    array4 = array2;
                }
                return array4;
            }
        });
    }
    
    private Transport getTransport(final Provider provider, final URLName urlName) throws NoSuchProviderException {
        if (provider == null || provider.getType() != Provider.Type.TRANSPORT) {
            throw new NoSuchProviderException("invalid provider");
        }
        try {
            return (Transport)this.getService(provider, urlName);
        }
        catch (ClassCastException ex) {
            throw new NoSuchProviderException("incorrect class");
        }
    }
    
    private void loadAddressMap(final Class clazz) {
        final StreamLoader streamLoader = new StreamLoader() {
            @Override
            public void load(final InputStream inputStream) throws IOException {
                Session.this.addressMap.load(inputStream);
            }
        };
        this.loadResource("/META-INF/javamail.default.address.map", clazz, streamLoader);
        this.loadAllResources("META-INF/javamail.address.map", clazz, streamLoader);
        while (true) {
            try {
                this.loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.address.map", streamLoader);
                if (this.addressMap.isEmpty()) {
                    if (this.debug) {
                        this.pr("DEBUG: failed to load address map, using defaults");
                    }
                    ((Hashtable<String, String>)this.addressMap).put("rfc822", "smtp");
                }
            }
            catch (SecurityException ex) {
                if (this.debug) {
                    this.pr("DEBUG: can't get java.home: " + ex);
                }
                continue;
            }
            break;
        }
    }
    
    private void loadAllResources(final String p0, final Class p1, final StreamLoader p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore          4
        //     3: iconst_0       
        //     4: istore          7
        //     6: iconst_0       
        //     7: istore          6
        //     9: iload           7
        //    11: istore          5
        //    13: invokestatic    javax/mail/Session.getContextClassLoader:()Ljava/lang/ClassLoader;
        //    16: astore          14
        //    18: aload           14
        //    20: astore          13
        //    22: aload           14
        //    24: ifnonnull       37
        //    27: iload           7
        //    29: istore          5
        //    31: aload_2        
        //    32: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //    35: astore          13
        //    37: aload           13
        //    39: ifnull          116
        //    42: iload           7
        //    44: istore          5
        //    46: aload           13
        //    48: aload_1        
        //    49: invokestatic    javax/mail/Session.getResources:(Ljava/lang/ClassLoader;Ljava/lang/String;)[Ljava/net/URL;
        //    52: astore          17
        //    54: goto            695
        //    57: iload           4
        //    59: istore          5
        //    61: aload           17
        //    63: arraylength    
        //    64: istore          6
        //    66: iload           10
        //    68: iload           6
        //    70: if_icmplt       129
        //    73: iload           4
        //    75: ifne            115
        //    78: aload_0        
        //    79: getfield        javax/mail/Session.debug:Z
        //    82: ifeq            92
        //    85: aload_0        
        //    86: ldc_w           "DEBUG: !anyLoaded"
        //    89: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //    92: aload_0        
        //    93: new             Ljava/lang/StringBuilder;
        //    96: dup            
        //    97: ldc_w           "/"
        //   100: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   103: aload_1        
        //   104: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   107: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   110: aload_2        
        //   111: aload_3        
        //   112: invokespecial   javax/mail/Session.loadResource:(Ljava/lang/String;Ljava/lang/Class;Ljavax/mail/StreamLoader;)V
        //   115: return         
        //   116: iload           7
        //   118: istore          5
        //   120: aload_1        
        //   121: invokestatic    javax/mail/Session.getSystemResources:(Ljava/lang/String;)[Ljava/net/URL;
        //   124: astore          17
        //   126: goto            695
        //   129: aload           17
        //   131: iload           10
        //   133: aaload         
        //   134: astore          18
        //   136: aconst_null    
        //   137: astore          15
        //   139: aconst_null    
        //   140: astore          13
        //   142: aconst_null    
        //   143: astore          14
        //   145: iload           4
        //   147: istore          5
        //   149: aload_0        
        //   150: getfield        javax/mail/Session.debug:Z
        //   153: ifeq            182
        //   156: iload           4
        //   158: istore          5
        //   160: aload_0        
        //   161: new             Ljava/lang/StringBuilder;
        //   164: dup            
        //   165: ldc_w           "DEBUG: URL "
        //   168: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   171: aload           18
        //   173: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   176: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   179: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   182: iload           4
        //   184: istore          6
        //   186: iload           4
        //   188: istore          7
        //   190: iload           4
        //   192: istore          8
        //   194: aload           18
        //   196: invokestatic    javax/mail/Session.openStream:(Ljava/net/URL;)Ljava/io/InputStream;
        //   199: astore          16
        //   201: aload           16
        //   203: ifnull          366
        //   206: iload           4
        //   208: istore          6
        //   210: aload           16
        //   212: astore          14
        //   214: iload           4
        //   216: istore          7
        //   218: aload           16
        //   220: astore          15
        //   222: iload           4
        //   224: istore          8
        //   226: aload           16
        //   228: astore          13
        //   230: aload_3        
        //   231: aload           16
        //   233: invokeinterface javax/mail/StreamLoader.load:(Ljava/io/InputStream;)V
        //   238: iconst_1       
        //   239: istore          5
        //   241: iconst_1       
        //   242: istore          11
        //   244: iconst_1       
        //   245: istore          12
        //   247: iconst_1       
        //   248: istore          4
        //   250: iload           4
        //   252: istore          9
        //   254: iload           5
        //   256: istore          6
        //   258: aload           16
        //   260: astore          14
        //   262: iload           11
        //   264: istore          7
        //   266: aload           16
        //   268: astore          15
        //   270: iload           12
        //   272: istore          8
        //   274: aload           16
        //   276: astore          13
        //   278: aload_0        
        //   279: getfield        javax/mail/Session.debug:Z
        //   282: ifeq            335
        //   285: iload           5
        //   287: istore          6
        //   289: aload           16
        //   291: astore          14
        //   293: iload           11
        //   295: istore          7
        //   297: aload           16
        //   299: astore          15
        //   301: iload           12
        //   303: istore          8
        //   305: aload           16
        //   307: astore          13
        //   309: aload_0        
        //   310: new             Ljava/lang/StringBuilder;
        //   313: dup            
        //   314: ldc_w           "DEBUG: successfully loaded resource: "
        //   317: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   320: aload           18
        //   322: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   325: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   328: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   331: iload           4
        //   333: istore          9
        //   335: iload           9
        //   337: istore          4
        //   339: aload           16
        //   341: ifnull          357
        //   344: iload           9
        //   346: istore          5
        //   348: aload           16
        //   350: invokevirtual   java/io/InputStream.close:()V
        //   353: iload           9
        //   355: istore          4
        //   357: iload           10
        //   359: iconst_1       
        //   360: iadd           
        //   361: istore          10
        //   363: goto            57
        //   366: iload           4
        //   368: istore          9
        //   370: iload           4
        //   372: istore          6
        //   374: aload           16
        //   376: astore          14
        //   378: iload           4
        //   380: istore          7
        //   382: aload           16
        //   384: astore          15
        //   386: iload           4
        //   388: istore          8
        //   390: aload           16
        //   392: astore          13
        //   394: aload_0        
        //   395: getfield        javax/mail/Session.debug:Z
        //   398: ifeq            335
        //   401: iload           4
        //   403: istore          6
        //   405: aload           16
        //   407: astore          14
        //   409: iload           4
        //   411: istore          7
        //   413: aload           16
        //   415: astore          15
        //   417: iload           4
        //   419: istore          8
        //   421: aload           16
        //   423: astore          13
        //   425: aload_0        
        //   426: new             Ljava/lang/StringBuilder;
        //   429: dup            
        //   430: ldc_w           "DEBUG: not loading resource: "
        //   433: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   436: aload           18
        //   438: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   441: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   444: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   447: iload           4
        //   449: istore          9
        //   451: goto            335
        //   454: astore          15
        //   456: iload           6
        //   458: istore          8
        //   460: aload           14
        //   462: astore          13
        //   464: aload_0        
        //   465: getfield        javax/mail/Session.debug:Z
        //   468: ifeq            501
        //   471: iload           6
        //   473: istore          8
        //   475: aload           14
        //   477: astore          13
        //   479: aload_0        
        //   480: new             Ljava/lang/StringBuilder;
        //   483: dup            
        //   484: ldc_w           "DEBUG: "
        //   487: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   490: aload           15
        //   492: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   495: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   498: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   501: iload           6
        //   503: istore          4
        //   505: aload           14
        //   507: ifnull          357
        //   510: iload           6
        //   512: istore          5
        //   514: aload           14
        //   516: invokevirtual   java/io/InputStream.close:()V
        //   519: iload           6
        //   521: istore          4
        //   523: goto            357
        //   526: astore          13
        //   528: iload           6
        //   530: istore          4
        //   532: goto            357
        //   535: astore          14
        //   537: iload           7
        //   539: istore          8
        //   541: aload           15
        //   543: astore          13
        //   545: aload_0        
        //   546: getfield        javax/mail/Session.debug:Z
        //   549: ifeq            582
        //   552: iload           7
        //   554: istore          8
        //   556: aload           15
        //   558: astore          13
        //   560: aload_0        
        //   561: new             Ljava/lang/StringBuilder;
        //   564: dup            
        //   565: ldc_w           "DEBUG: "
        //   568: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   571: aload           14
        //   573: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   576: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   579: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   582: iload           7
        //   584: istore          4
        //   586: aload           15
        //   588: ifnull          357
        //   591: iload           7
        //   593: istore          5
        //   595: aload           15
        //   597: invokevirtual   java/io/InputStream.close:()V
        //   600: iload           7
        //   602: istore          4
        //   604: goto            357
        //   607: astore          13
        //   609: iload           7
        //   611: istore          4
        //   613: goto            357
        //   616: astore          14
        //   618: aload           13
        //   620: ifnull          632
        //   623: iload           8
        //   625: istore          5
        //   627: aload           13
        //   629: invokevirtual   java/io/InputStream.close:()V
        //   632: iload           8
        //   634: istore          5
        //   636: aload           14
        //   638: athrow         
        //   639: astore          13
        //   641: iload           5
        //   643: istore          4
        //   645: aload_0        
        //   646: getfield        javax/mail/Session.debug:Z
        //   649: ifeq            73
        //   652: aload_0        
        //   653: new             Ljava/lang/StringBuilder;
        //   656: dup            
        //   657: ldc_w           "DEBUG: "
        //   660: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   663: aload           13
        //   665: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   668: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   671: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   674: iload           5
        //   676: istore          4
        //   678: goto            73
        //   681: astore          13
        //   683: goto            632
        //   686: astore          13
        //   688: iload           9
        //   690: istore          4
        //   692: goto            357
        //   695: aload           17
        //   697: ifnull          73
        //   700: iconst_0       
        //   701: istore          10
        //   703: iload           6
        //   705: istore          4
        //   707: goto            57
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  13     18     639    681    Ljava/lang/Exception;
        //  31     37     639    681    Ljava/lang/Exception;
        //  46     54     639    681    Ljava/lang/Exception;
        //  61     66     639    681    Ljava/lang/Exception;
        //  120    126    639    681    Ljava/lang/Exception;
        //  149    156    639    681    Ljava/lang/Exception;
        //  160    182    639    681    Ljava/lang/Exception;
        //  194    201    454    535    Ljava/io/IOException;
        //  194    201    535    616    Ljava/lang/SecurityException;
        //  194    201    616    639    Any
        //  230    238    454    535    Ljava/io/IOException;
        //  230    238    535    616    Ljava/lang/SecurityException;
        //  230    238    616    639    Any
        //  278    285    454    535    Ljava/io/IOException;
        //  278    285    535    616    Ljava/lang/SecurityException;
        //  278    285    616    639    Any
        //  309    331    454    535    Ljava/io/IOException;
        //  309    331    535    616    Ljava/lang/SecurityException;
        //  309    331    616    639    Any
        //  348    353    686    695    Ljava/io/IOException;
        //  348    353    639    681    Ljava/lang/Exception;
        //  394    401    454    535    Ljava/io/IOException;
        //  394    401    535    616    Ljava/lang/SecurityException;
        //  394    401    616    639    Any
        //  425    447    454    535    Ljava/io/IOException;
        //  425    447    535    616    Ljava/lang/SecurityException;
        //  425    447    616    639    Any
        //  464    471    616    639    Any
        //  479    501    616    639    Any
        //  514    519    526    535    Ljava/io/IOException;
        //  514    519    639    681    Ljava/lang/Exception;
        //  545    552    616    639    Any
        //  560    582    616    639    Any
        //  595    600    607    616    Ljava/io/IOException;
        //  595    600    639    681    Ljava/lang/Exception;
        //  627    632    681    686    Ljava/io/IOException;
        //  627    632    639    681    Ljava/lang/Exception;
        //  636    639    639    681    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0357:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void loadFile(final String p0, final StreamLoader p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          6
        //     3: aconst_null    
        //     4: astore_3       
        //     5: aconst_null    
        //     6: astore          5
        //     8: new             Ljava/io/BufferedInputStream;
        //    11: dup            
        //    12: new             Ljava/io/FileInputStream;
        //    15: dup            
        //    16: aload_1        
        //    17: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //    20: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //    23: astore          4
        //    25: aload_2        
        //    26: aload           4
        //    28: invokeinterface javax/mail/StreamLoader.load:(Ljava/io/InputStream;)V
        //    33: aload_0        
        //    34: getfield        javax/mail/Session.debug:Z
        //    37: ifeq            61
        //    40: aload_0        
        //    41: new             Ljava/lang/StringBuilder;
        //    44: dup            
        //    45: ldc_w           "DEBUG: successfully loaded file: "
        //    48: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    51: aload_1        
        //    52: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    55: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    58: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //    61: aload           4
        //    63: ifnull          260
        //    66: aload           4
        //    68: invokevirtual   java/io/InputStream.close:()V
        //    71: return         
        //    72: astore          4
        //    74: aload           5
        //    76: astore_2       
        //    77: aload_2        
        //    78: astore_3       
        //    79: aload_0        
        //    80: getfield        javax/mail/Session.debug:Z
        //    83: ifeq            133
        //    86: aload_2        
        //    87: astore_3       
        //    88: aload_0        
        //    89: new             Ljava/lang/StringBuilder;
        //    92: dup            
        //    93: ldc_w           "DEBUG: not loading file: "
        //    96: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    99: aload_1        
        //   100: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   103: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   106: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   109: aload_2        
        //   110: astore_3       
        //   111: aload_0        
        //   112: new             Ljava/lang/StringBuilder;
        //   115: dup            
        //   116: ldc_w           "DEBUG: "
        //   119: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   122: aload           4
        //   124: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   127: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   130: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   133: aload_2        
        //   134: ifnull          71
        //   137: aload_2        
        //   138: invokevirtual   java/io/InputStream.close:()V
        //   141: return         
        //   142: astore_1       
        //   143: return         
        //   144: astore          4
        //   146: aload           6
        //   148: astore_2       
        //   149: aload_2        
        //   150: astore_3       
        //   151: aload_0        
        //   152: getfield        javax/mail/Session.debug:Z
        //   155: ifeq            205
        //   158: aload_2        
        //   159: astore_3       
        //   160: aload_0        
        //   161: new             Ljava/lang/StringBuilder;
        //   164: dup            
        //   165: ldc_w           "DEBUG: not loading file: "
        //   168: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   171: aload_1        
        //   172: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   175: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   178: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   181: aload_2        
        //   182: astore_3       
        //   183: aload_0        
        //   184: new             Ljava/lang/StringBuilder;
        //   187: dup            
        //   188: ldc_w           "DEBUG: "
        //   191: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   194: aload           4
        //   196: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   199: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   202: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   205: aload_2        
        //   206: ifnull          71
        //   209: aload_2        
        //   210: invokevirtual   java/io/InputStream.close:()V
        //   213: return         
        //   214: astore_1       
        //   215: return         
        //   216: astore_1       
        //   217: aload_3        
        //   218: ifnull          225
        //   221: aload_3        
        //   222: invokevirtual   java/io/InputStream.close:()V
        //   225: aload_1        
        //   226: athrow         
        //   227: astore_1       
        //   228: return         
        //   229: astore_2       
        //   230: goto            225
        //   233: astore_1       
        //   234: aload           4
        //   236: astore_3       
        //   237: goto            217
        //   240: astore_3       
        //   241: aload           4
        //   243: astore_2       
        //   244: aload_3        
        //   245: astore          4
        //   247: goto            149
        //   250: astore_3       
        //   251: aload           4
        //   253: astore_2       
        //   254: aload_3        
        //   255: astore          4
        //   257: goto            77
        //   260: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  8      25     72     77     Ljava/io/IOException;
        //  8      25     144    149    Ljava/lang/SecurityException;
        //  8      25     216    217    Any
        //  25     61     250    260    Ljava/io/IOException;
        //  25     61     240    250    Ljava/lang/SecurityException;
        //  25     61     233    240    Any
        //  66     71     227    229    Ljava/io/IOException;
        //  79     86     216    217    Any
        //  88     109    216    217    Any
        //  111    133    216    217    Any
        //  137    141    142    144    Ljava/io/IOException;
        //  151    158    216    217    Any
        //  160    181    216    217    Any
        //  183    205    216    217    Any
        //  209    213    214    216    Ljava/io/IOException;
        //  221    225    229    233    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0061:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void loadProviders(final Class clazz) {
        final StreamLoader streamLoader = new StreamLoader() {
            @Override
            public void load(final InputStream inputStream) throws IOException {
                Session.this.loadProvidersFromStream(inputStream);
            }
        };
        while (true) {
            try {
                this.loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.providers", streamLoader);
                this.loadAllResources("META-INF/javamail.providers", clazz, streamLoader);
                this.loadResource("/META-INF/javamail.default.providers", clazz, streamLoader);
                if (this.providers.size() == 0) {
                    if (this.debug) {
                        this.pr("DEBUG: failed to load any providers, using defaults");
                    }
                    this.addProvider(new Provider(Provider.Type.STORE, "imap", "com.sun.mail.imap.IMAPStore", "Sun Microsystems, Inc.", "1.4.1"));
                    this.addProvider(new Provider(Provider.Type.STORE, "imaps", "com.sun.mail.imap.IMAPSSLStore", "Sun Microsystems, Inc.", "1.4.1"));
                    this.addProvider(new Provider(Provider.Type.STORE, "pop3", "com.sun.mail.pop3.POP3Store", "Sun Microsystems, Inc.", "1.4.1"));
                    this.addProvider(new Provider(Provider.Type.STORE, "pop3s", "com.sun.mail.pop3.POP3SSLStore", "Sun Microsystems, Inc.", "1.4.1"));
                    this.addProvider(new Provider(Provider.Type.TRANSPORT, "smtp", "com.sun.mail.smtp.SMTPTransport", "Sun Microsystems, Inc.", "1.4.1"));
                    this.addProvider(new Provider(Provider.Type.TRANSPORT, "smtps", "com.sun.mail.smtp.SMTPSSLTransport", "Sun Microsystems, Inc.", "1.4.1"));
                }
                if (this.debug) {
                    this.pr("DEBUG: Tables of loaded providers");
                    this.pr("DEBUG: Providers Listed By Class Name: " + this.providersByClassName.toString());
                    this.pr("DEBUG: Providers Listed By Protocol: " + this.providersByProtocol.toString());
                }
            }
            catch (SecurityException ex) {
                if (this.debug) {
                    this.pr("DEBUG: can't get java.home: " + ex);
                }
                continue;
            }
            break;
        }
    }
    
    private void loadProvidersFromStream(final InputStream inputStream) throws IOException {
        if (inputStream != null) {
            final LineInputStream lineInputStream = new LineInputStream(inputStream);
            while (true) {
                final String line = lineInputStream.readLine();
                if (line == null) {
                    break;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                Provider.Type type = null;
                String substring = null;
                String substring2 = null;
                String substring3 = null;
                String substring4 = null;
                final StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
                while (stringTokenizer.hasMoreTokens()) {
                    final String trim = stringTokenizer.nextToken().trim();
                    final int index = trim.indexOf("=");
                    if (trim.startsWith("protocol=")) {
                        substring = trim.substring(index + 1);
                    }
                    else if (trim.startsWith("type=")) {
                        final String substring5 = trim.substring(index + 1);
                        if (substring5.equalsIgnoreCase("store")) {
                            type = Provider.Type.STORE;
                        }
                        else {
                            if (!substring5.equalsIgnoreCase("transport")) {
                                continue;
                            }
                            type = Provider.Type.TRANSPORT;
                        }
                    }
                    else if (trim.startsWith("class=")) {
                        substring2 = trim.substring(index + 1);
                    }
                    else if (trim.startsWith("vendor=")) {
                        substring3 = trim.substring(index + 1);
                    }
                    else {
                        if (!trim.startsWith("version=")) {
                            continue;
                        }
                        substring4 = trim.substring(index + 1);
                    }
                }
                if (type == null || substring == null || substring2 == null || substring.length() <= 0 || substring2.length() <= 0) {
                    if (!this.debug) {
                        continue;
                    }
                    this.pr("DEBUG: Bad provider entry: " + line);
                }
                else {
                    this.addProvider(new Provider(type, substring, substring2, substring3, substring4));
                }
            }
        }
    }
    
    private void loadResource(final String p0, final Class p1, final StreamLoader p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          6
        //     3: aconst_null    
        //     4: astore          4
        //     6: aconst_null    
        //     7: astore          5
        //     9: aload_2        
        //    10: aload_1        
        //    11: invokestatic    javax/mail/Session.getResourceAsStream:(Ljava/lang/Class;Ljava/lang/String;)Ljava/io/InputStream;
        //    14: astore_2       
        //    15: aload_2        
        //    16: ifnull          90
        //    19: aload_2        
        //    20: astore          5
        //    22: aload_2        
        //    23: astore          6
        //    25: aload_2        
        //    26: astore          4
        //    28: aload_3        
        //    29: aload_2        
        //    30: invokeinterface javax/mail/StreamLoader.load:(Ljava/io/InputStream;)V
        //    35: aload_2        
        //    36: astore          5
        //    38: aload_2        
        //    39: astore          6
        //    41: aload_2        
        //    42: astore          4
        //    44: aload_0        
        //    45: getfield        javax/mail/Session.debug:Z
        //    48: ifeq            81
        //    51: aload_2        
        //    52: astore          5
        //    54: aload_2        
        //    55: astore          6
        //    57: aload_2        
        //    58: astore          4
        //    60: aload_0        
        //    61: new             Ljava/lang/StringBuilder;
        //    64: dup            
        //    65: ldc_w           "DEBUG: successfully loaded resource: "
        //    68: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //    71: aload_1        
        //    72: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    75: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    78: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //    81: aload_2        
        //    82: ifnull          89
        //    85: aload_2        
        //    86: invokevirtual   java/io/InputStream.close:()V
        //    89: return         
        //    90: aload_2        
        //    91: astore          5
        //    93: aload_2        
        //    94: astore          6
        //    96: aload_2        
        //    97: astore          4
        //    99: aload_0        
        //   100: getfield        javax/mail/Session.debug:Z
        //   103: ifeq            81
        //   106: aload_2        
        //   107: astore          5
        //   109: aload_2        
        //   110: astore          6
        //   112: aload_2        
        //   113: astore          4
        //   115: aload_0        
        //   116: new             Ljava/lang/StringBuilder;
        //   119: dup            
        //   120: ldc_w           "DEBUG: not loading resource: "
        //   123: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   126: aload_1        
        //   127: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   130: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   133: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   136: goto            81
        //   139: astore_1       
        //   140: aload           5
        //   142: astore          4
        //   144: aload_0        
        //   145: getfield        javax/mail/Session.debug:Z
        //   148: ifeq            176
        //   151: aload           5
        //   153: astore          4
        //   155: aload_0        
        //   156: new             Ljava/lang/StringBuilder;
        //   159: dup            
        //   160: ldc_w           "DEBUG: "
        //   163: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   166: aload_1        
        //   167: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   170: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   173: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   176: aload           5
        //   178: ifnull          89
        //   181: aload           5
        //   183: invokevirtual   java/io/InputStream.close:()V
        //   186: return         
        //   187: astore_1       
        //   188: return         
        //   189: astore_1       
        //   190: aload           6
        //   192: astore          4
        //   194: aload_0        
        //   195: getfield        javax/mail/Session.debug:Z
        //   198: ifeq            226
        //   201: aload           6
        //   203: astore          4
        //   205: aload_0        
        //   206: new             Ljava/lang/StringBuilder;
        //   209: dup            
        //   210: ldc_w           "DEBUG: "
        //   213: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   216: aload_1        
        //   217: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   220: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   223: invokespecial   javax/mail/Session.pr:(Ljava/lang/String;)V
        //   226: aload           6
        //   228: ifnull          89
        //   231: aload           6
        //   233: invokevirtual   java/io/InputStream.close:()V
        //   236: return         
        //   237: astore_1       
        //   238: return         
        //   239: astore_1       
        //   240: aload           4
        //   242: ifnull          250
        //   245: aload           4
        //   247: invokevirtual   java/io/InputStream.close:()V
        //   250: aload_1        
        //   251: athrow         
        //   252: astore_2       
        //   253: goto            250
        //   256: astore_1       
        //   257: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  9      15     139    189    Ljava/io/IOException;
        //  9      15     189    239    Ljava/lang/SecurityException;
        //  9      15     239    256    Any
        //  28     35     139    189    Ljava/io/IOException;
        //  28     35     189    239    Ljava/lang/SecurityException;
        //  28     35     239    256    Any
        //  44     51     139    189    Ljava/io/IOException;
        //  44     51     189    239    Ljava/lang/SecurityException;
        //  44     51     239    256    Any
        //  60     81     139    189    Ljava/io/IOException;
        //  60     81     189    239    Ljava/lang/SecurityException;
        //  60     81     239    256    Any
        //  85     89     256    258    Ljava/io/IOException;
        //  99     106    139    189    Ljava/io/IOException;
        //  99     106    189    239    Ljava/lang/SecurityException;
        //  99     106    239    256    Any
        //  115    136    139    189    Ljava/io/IOException;
        //  115    136    189    239    Ljava/lang/SecurityException;
        //  115    136    239    256    Any
        //  144    151    239    256    Any
        //  155    176    239    256    Any
        //  181    186    187    189    Ljava/io/IOException;
        //  194    201    239    256    Any
        //  205    226    239    256    Any
        //  231    236    237    239    Ljava/io/IOException;
        //  245    250    252    256    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0089:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static InputStream openStream(final URL url) throws IOException {
        try {
            return AccessController.doPrivileged((PrivilegedExceptionAction<InputStream>)new PrivilegedExceptionAction() {
                @Override
                public Object run() throws IOException {
                    return url.openStream();
                }
            });
        }
        catch (PrivilegedActionException ex) {
            throw (IOException)ex.getException();
        }
    }
    
    private void pr(final String s) {
        this.getDebugOut().println(s);
    }
    
    public void addProvider(final Provider provider) {
        synchronized (this) {
            this.providers.addElement(provider);
            this.providersByClassName.put(provider.getClassName(), provider);
            if (!this.providersByProtocol.containsKey(provider.getProtocol())) {
                this.providersByProtocol.put(provider.getProtocol(), provider);
            }
        }
    }
    
    public boolean getDebug() {
        synchronized (this) {
            return this.debug;
        }
    }
    
    public PrintStream getDebugOut() {
        synchronized (this) {
            PrintStream printStream;
            if (this.out == null) {
                printStream = System.out;
            }
            else {
                printStream = this.out;
            }
            return printStream;
        }
    }
    
    public Folder getFolder(final URLName urlName) throws MessagingException {
        final Store store = this.getStore(urlName);
        store.connect();
        return store.getFolder(urlName);
    }
    
    public PasswordAuthentication getPasswordAuthentication(final URLName urlName) {
        return this.authTable.get(urlName);
    }
    
    public Properties getProperties() {
        return this.props;
    }
    
    public String getProperty(final String s) {
        return this.props.getProperty(s);
    }
    
    public Provider getProvider(final String s) throws NoSuchProviderException {
        // monitorenter(this)
        while (true) {
            if (s != null) {
                try {
                    if (s.length() <= 0) {
                        throw new NoSuchProviderException("Invalid protocol: null");
                    }
                }
                finally {
                }
                // monitorexit(this)
                Provider provider = null;
                final String s2;
                final String property = this.props.getProperty("mail." + s2 + ".class");
                if (property != null) {
                    if (this.debug) {
                        this.pr("DEBUG: mail." + s2 + ".class property exists and points to " + property);
                    }
                    provider = (Provider)this.providersByClassName.get(property);
                }
                if (provider == null) {
                    provider = this.providersByProtocol.get(s2);
                    if (provider == null) {
                        throw new NoSuchProviderException("No provider for " + s2);
                    }
                    if (this.debug) {
                        this.pr("DEBUG: getProvider() returning " + provider.toString());
                    }
                }
                // monitorexit(this)
                return provider;
            }
            continue;
        }
    }
    
    public Provider[] getProviders() {
        synchronized (this) {
            final Provider[] array = new Provider[this.providers.size()];
            this.providers.copyInto(array);
            return array;
        }
    }
    
    public Store getStore() throws NoSuchProviderException {
        return this.getStore(this.getProperty("mail.store.protocol"));
    }
    
    public Store getStore(final String s) throws NoSuchProviderException {
        return this.getStore(new URLName(s, null, -1, null, null, null));
    }
    
    public Store getStore(final Provider provider) throws NoSuchProviderException {
        return this.getStore(provider, null);
    }
    
    public Store getStore(final URLName urlName) throws NoSuchProviderException {
        return this.getStore(this.getProvider(urlName.getProtocol()), urlName);
    }
    
    public Transport getTransport() throws NoSuchProviderException {
        return this.getTransport(this.getProperty("mail.transport.protocol"));
    }
    
    public Transport getTransport(final String s) throws NoSuchProviderException {
        return this.getTransport(new URLName(s, null, -1, null, null, null));
    }
    
    public Transport getTransport(final Address address) throws NoSuchProviderException {
        final String s = ((Hashtable<K, String>)this.addressMap).get(address.getType());
        if (s == null) {
            throw new NoSuchProviderException("No provider for Address type: " + address.getType());
        }
        return this.getTransport(s);
    }
    
    public Transport getTransport(final Provider provider) throws NoSuchProviderException {
        return this.getTransport(provider, null);
    }
    
    public Transport getTransport(final URLName urlName) throws NoSuchProviderException {
        return this.getTransport(this.getProvider(urlName.getProtocol()), urlName);
    }
    
    public PasswordAuthentication requestPasswordAuthentication(final InetAddress inetAddress, final int n, final String s, final String s2, final String s3) {
        if (this.authenticator != null) {
            return this.authenticator.requestPasswordAuthentication(inetAddress, n, s, s2, s3);
        }
        return null;
    }
    
    public void setDebug(final boolean debug) {
        synchronized (this) {
            this.debug = debug;
            if (debug) {
                this.pr("DEBUG: setDebug: JavaMail version 1.4.1");
            }
        }
    }
    
    public void setDebugOut(final PrintStream out) {
        synchronized (this) {
            this.out = out;
        }
    }
    
    public void setPasswordAuthentication(final URLName urlName, final PasswordAuthentication passwordAuthentication) {
        if (passwordAuthentication == null) {
            this.authTable.remove(urlName);
            return;
        }
        this.authTable.put(urlName, passwordAuthentication);
    }
    
    public void setProtocolForAddress(final String s, final String s2) {
        // monitorenter(this)
        Label_0018: {
            if (s2 != null) {
                break Label_0018;
            }
            try {
                this.addressMap.remove(s);
                return;
                ((Hashtable<String, String>)this.addressMap).put(s, s2);
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    public void setProvider(final Provider provider) throws NoSuchProviderException {
        // monitorenter(this)
        if (provider == null) {
            try {
                throw new NoSuchProviderException("Can't set null provider");
            }
            finally {
            }
            // monitorexit(this)
        }
        this.providersByProtocol.put(provider.getProtocol(), provider);
        ((Hashtable<String, String>)this.props).put("mail." + provider.getProtocol() + ".class", provider.getClassName());
    }
    // monitorexit(this)
}
