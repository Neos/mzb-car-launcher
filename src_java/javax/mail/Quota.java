package javax.mail;

public class Quota
{
    public String quotaRoot;
    public Resource[] resources;
    
    public Quota(final String quotaRoot) {
        this.quotaRoot = quotaRoot;
    }
    
    public void setResourceLimit(final String s, final long limit) {
        if (this.resources == null) {
            (this.resources = new Resource[1])[0] = new Resource(s, 0L, limit);
            return;
        }
        for (int i = 0; i < this.resources.length; ++i) {
            if (this.resources[i].name.equalsIgnoreCase(s)) {
                this.resources[i].limit = limit;
                return;
            }
        }
        final Resource[] resources = new Resource[this.resources.length + 1];
        System.arraycopy(this.resources, 0, resources, 0, this.resources.length);
        resources[resources.length - 1] = new Resource(s, 0L, limit);
        this.resources = resources;
    }
    
    public static class Resource
    {
        public long limit;
        public String name;
        public long usage;
        
        public Resource(final String name, final long usage, final long limit) {
            this.name = name;
            this.usage = usage;
            this.limit = limit;
        }
    }
}
