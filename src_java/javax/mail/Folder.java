package javax.mail;

import java.util.*;
import javax.mail.event.*;
import javax.mail.search.*;

public abstract class Folder
{
    public static final int HOLDS_FOLDERS = 2;
    public static final int HOLDS_MESSAGES = 1;
    public static final int READ_ONLY = 1;
    public static final int READ_WRITE = 2;
    private volatile Vector connectionListeners;
    private volatile Vector folderListeners;
    private volatile Vector messageChangedListeners;
    private volatile Vector messageCountListeners;
    protected int mode;
    private EventQueue q;
    private Object qLock;
    protected Store store;
    
    protected Folder(final Store store) {
        this.mode = -1;
        this.connectionListeners = null;
        this.folderListeners = null;
        this.messageCountListeners = null;
        this.messageChangedListeners = null;
        this.qLock = new Object();
        this.store = store;
    }
    
    private void queueEvent(final MailEvent mailEvent, Vector vector) {
        synchronized (this.qLock) {
            if (this.q == null) {
                this.q = new EventQueue();
            }
            // monitorexit(this.qLock)
            vector = (Vector)vector.clone();
            this.q.enqueue(mailEvent, vector);
        }
    }
    
    private void terminateQueue() {
        synchronized (this.qLock) {
            if (this.q != null) {
                final Vector vector = new Vector();
                vector.setSize(1);
                this.q.enqueue(new TerminatorEvent(), vector);
                this.q = null;
            }
        }
    }
    
    public void addConnectionListener(final ConnectionListener connectionListener) {
        synchronized (this) {
            if (this.connectionListeners == null) {
                this.connectionListeners = new Vector();
            }
            this.connectionListeners.addElement(connectionListener);
        }
    }
    
    public void addFolderListener(final FolderListener folderListener) {
        synchronized (this) {
            if (this.folderListeners == null) {
                this.folderListeners = new Vector();
            }
            this.folderListeners.addElement(folderListener);
        }
    }
    
    public void addMessageChangedListener(final MessageChangedListener messageChangedListener) {
        synchronized (this) {
            if (this.messageChangedListeners == null) {
                this.messageChangedListeners = new Vector();
            }
            this.messageChangedListeners.addElement(messageChangedListener);
        }
    }
    
    public void addMessageCountListener(final MessageCountListener messageCountListener) {
        synchronized (this) {
            if (this.messageCountListeners == null) {
                this.messageCountListeners = new Vector();
            }
            this.messageCountListeners.addElement(messageCountListener);
        }
    }
    
    public abstract void appendMessages(final Message[] p0) throws MessagingException;
    
    public abstract void close(final boolean p0) throws MessagingException;
    
    public void copyMessages(final Message[] array, final Folder folder) throws MessagingException {
        if (!folder.exists()) {
            throw new FolderNotFoundException(String.valueOf(folder.getFullName()) + " does not exist", folder);
        }
        folder.appendMessages(array);
    }
    
    public abstract boolean create(final int p0) throws MessagingException;
    
    public abstract boolean delete(final boolean p0) throws MessagingException;
    
    public abstract boolean exists() throws MessagingException;
    
    public abstract Message[] expunge() throws MessagingException;
    
    public void fetch(final Message[] array, final FetchProfile fetchProfile) throws MessagingException {
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.terminateQueue();
    }
    
    public int getDeletedMessageCount() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokevirtual   javax/mail/Folder.isOpen:()Z
        //     6: istore          5
        //     8: iload           5
        //    10: ifne            19
        //    13: iconst_m1      
        //    14: istore_3       
        //    15: aload_0        
        //    16: monitorexit    
        //    17: iload_3        
        //    18: ireturn        
        //    19: iconst_0       
        //    20: istore_1       
        //    21: aload_0        
        //    22: invokevirtual   javax/mail/Folder.getMessageCount:()I
        //    25: istore          4
        //    27: iconst_1       
        //    28: istore_2       
        //    29: iload_1        
        //    30: istore_3       
        //    31: iload_2        
        //    32: iload           4
        //    34: if_icmpgt       15
        //    37: aload_0        
        //    38: iload_2        
        //    39: invokevirtual   javax/mail/Folder.getMessage:(I)Ljavax/mail/Message;
        //    42: getstatic       javax/mail/Flags$Flag.DELETED:Ljavax/mail/Flags$Flag;
        //    45: invokevirtual   javax/mail/Message.isSet:(Ljavax/mail/Flags$Flag;)Z
        //    48: istore          5
        //    50: iload_1        
        //    51: istore_3       
        //    52: iload           5
        //    54: ifeq            61
        //    57: iload_1        
        //    58: iconst_1       
        //    59: iadd           
        //    60: istore_3       
        //    61: iload_2        
        //    62: iconst_1       
        //    63: iadd           
        //    64: istore_2       
        //    65: iload_3        
        //    66: istore_1       
        //    67: goto            29
        //    70: astore          6
        //    72: aload_0        
        //    73: monitorexit    
        //    74: aload           6
        //    76: athrow         
        //    77: astore          6
        //    79: iload_1        
        //    80: istore_3       
        //    81: goto            61
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      8      70     77     Any
        //  21     27     70     77     Any
        //  37     50     77     84     Ljavax/mail/MessageRemovedException;
        //  37     50     70     77     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0061:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public abstract Folder getFolder(final String p0) throws MessagingException;
    
    public abstract String getFullName();
    
    public abstract Message getMessage(final int p0) throws MessagingException;
    
    public abstract int getMessageCount() throws MessagingException;
    
    public Message[] getMessages() throws MessagingException {
        synchronized (this) {
            if (!this.isOpen()) {
                throw new IllegalStateException("Folder not open");
            }
        }
        final int messageCount = this.getMessageCount();
        final Message[] array = new Message[messageCount];
        for (int i = 1; i <= messageCount; ++i) {
            array[i - 1] = this.getMessage(i);
        }
        // monitorexit(this)
        return array;
    }
    
    public Message[] getMessages(final int n, final int n2) throws MessagingException {
        synchronized (this) {
            final Message[] array = new Message[n2 - n + 1];
            for (int i = n; i <= n2; ++i) {
                array[i - n] = this.getMessage(i);
            }
            return array;
        }
    }
    
    public Message[] getMessages(final int[] array) throws MessagingException {
        synchronized (this) {
            final int length = array.length;
            final Message[] array2 = new Message[length];
            for (int i = 0; i < length; ++i) {
                array2[i] = this.getMessage(array[i]);
            }
            return array2;
        }
    }
    
    public int getMode() {
        if (!this.isOpen()) {
            throw new IllegalStateException("Folder not open");
        }
        return this.mode;
    }
    
    public abstract String getName();
    
    public int getNewMessageCount() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokevirtual   javax/mail/Folder.isOpen:()Z
        //     6: istore          5
        //     8: iload           5
        //    10: ifne            19
        //    13: iconst_m1      
        //    14: istore_3       
        //    15: aload_0        
        //    16: monitorexit    
        //    17: iload_3        
        //    18: ireturn        
        //    19: iconst_0       
        //    20: istore_1       
        //    21: aload_0        
        //    22: invokevirtual   javax/mail/Folder.getMessageCount:()I
        //    25: istore          4
        //    27: iconst_1       
        //    28: istore_2       
        //    29: iload_1        
        //    30: istore_3       
        //    31: iload_2        
        //    32: iload           4
        //    34: if_icmpgt       15
        //    37: aload_0        
        //    38: iload_2        
        //    39: invokevirtual   javax/mail/Folder.getMessage:(I)Ljavax/mail/Message;
        //    42: getstatic       javax/mail/Flags$Flag.RECENT:Ljavax/mail/Flags$Flag;
        //    45: invokevirtual   javax/mail/Message.isSet:(Ljavax/mail/Flags$Flag;)Z
        //    48: istore          5
        //    50: iload_1        
        //    51: istore_3       
        //    52: iload           5
        //    54: ifeq            61
        //    57: iload_1        
        //    58: iconst_1       
        //    59: iadd           
        //    60: istore_3       
        //    61: iload_2        
        //    62: iconst_1       
        //    63: iadd           
        //    64: istore_2       
        //    65: iload_3        
        //    66: istore_1       
        //    67: goto            29
        //    70: astore          6
        //    72: aload_0        
        //    73: monitorexit    
        //    74: aload           6
        //    76: athrow         
        //    77: astore          6
        //    79: iload_1        
        //    80: istore_3       
        //    81: goto            61
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      8      70     77     Any
        //  21     27     70     77     Any
        //  37     50     77     84     Ljavax/mail/MessageRemovedException;
        //  37     50     70     77     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0061:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public abstract Folder getParent() throws MessagingException;
    
    public abstract Flags getPermanentFlags();
    
    public abstract char getSeparator() throws MessagingException;
    
    public Store getStore() {
        return this.store;
    }
    
    public abstract int getType() throws MessagingException;
    
    public URLName getURLName() throws MessagingException {
        final URLName urlName = this.getStore().getURLName();
        final String fullName = this.getFullName();
        final StringBuffer sb = new StringBuffer();
        this.getSeparator();
        if (fullName != null) {
            sb.append(fullName);
        }
        return new URLName(urlName.getProtocol(), urlName.getHost(), urlName.getPort(), sb.toString(), urlName.getUsername(), null);
    }
    
    public int getUnreadMessageCount() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokevirtual   javax/mail/Folder.isOpen:()Z
        //     6: istore          5
        //     8: iload           5
        //    10: ifne            19
        //    13: iconst_m1      
        //    14: istore_3       
        //    15: aload_0        
        //    16: monitorexit    
        //    17: iload_3        
        //    18: ireturn        
        //    19: iconst_0       
        //    20: istore_1       
        //    21: aload_0        
        //    22: invokevirtual   javax/mail/Folder.getMessageCount:()I
        //    25: istore          4
        //    27: iconst_1       
        //    28: istore_2       
        //    29: iload_1        
        //    30: istore_3       
        //    31: iload_2        
        //    32: iload           4
        //    34: if_icmpgt       15
        //    37: aload_0        
        //    38: iload_2        
        //    39: invokevirtual   javax/mail/Folder.getMessage:(I)Ljavax/mail/Message;
        //    42: getstatic       javax/mail/Flags$Flag.SEEN:Ljavax/mail/Flags$Flag;
        //    45: invokevirtual   javax/mail/Message.isSet:(Ljavax/mail/Flags$Flag;)Z
        //    48: istore          5
        //    50: iload_1        
        //    51: istore_3       
        //    52: iload           5
        //    54: ifne            61
        //    57: iload_1        
        //    58: iconst_1       
        //    59: iadd           
        //    60: istore_3       
        //    61: iload_2        
        //    62: iconst_1       
        //    63: iadd           
        //    64: istore_2       
        //    65: iload_3        
        //    66: istore_1       
        //    67: goto            29
        //    70: astore          6
        //    72: aload_0        
        //    73: monitorexit    
        //    74: aload           6
        //    76: athrow         
        //    77: astore          6
        //    79: iload_1        
        //    80: istore_3       
        //    81: goto            61
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  2      8      70     77     Any
        //  21     27     70     77     Any
        //  37     50     77     84     Ljavax/mail/MessageRemovedException;
        //  37     50     70     77     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0061:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public abstract boolean hasNewMessages() throws MessagingException;
    
    public abstract boolean isOpen();
    
    public boolean isSubscribed() {
        return true;
    }
    
    public Folder[] list() throws MessagingException {
        return this.list("%");
    }
    
    public abstract Folder[] list(final String p0) throws MessagingException;
    
    public Folder[] listSubscribed() throws MessagingException {
        return this.listSubscribed("%");
    }
    
    public Folder[] listSubscribed(final String s) throws MessagingException {
        return this.list(s);
    }
    
    protected void notifyConnectionListeners(final int n) {
        if (this.connectionListeners != null) {
            this.queueEvent(new ConnectionEvent(this, n), this.connectionListeners);
        }
        if (n == 3) {
            this.terminateQueue();
        }
    }
    
    protected void notifyFolderListeners(final int n) {
        if (this.folderListeners != null) {
            this.queueEvent(new FolderEvent(this, this, n), this.folderListeners);
        }
        this.store.notifyFolderListeners(n, this);
    }
    
    protected void notifyFolderRenamedListeners(final Folder folder) {
        if (this.folderListeners != null) {
            this.queueEvent(new FolderEvent(this, this, folder, 3), this.folderListeners);
        }
        this.store.notifyFolderRenamedListeners(this, folder);
    }
    
    protected void notifyMessageAddedListeners(final Message[] array) {
        if (this.messageCountListeners == null) {
            return;
        }
        this.queueEvent(new MessageCountEvent(this, 1, false, array), this.messageCountListeners);
    }
    
    protected void notifyMessageChangedListeners(final int n, final Message message) {
        if (this.messageChangedListeners == null) {
            return;
        }
        this.queueEvent(new MessageChangedEvent(this, n, message), this.messageChangedListeners);
    }
    
    protected void notifyMessageRemovedListeners(final boolean b, final Message[] array) {
        if (this.messageCountListeners == null) {
            return;
        }
        this.queueEvent(new MessageCountEvent(this, 2, b, array), this.messageCountListeners);
    }
    
    public abstract void open(final int p0) throws MessagingException;
    
    public void removeConnectionListener(final ConnectionListener connectionListener) {
        synchronized (this) {
            if (this.connectionListeners != null) {
                this.connectionListeners.removeElement(connectionListener);
            }
        }
    }
    
    public void removeFolderListener(final FolderListener folderListener) {
        synchronized (this) {
            if (this.folderListeners != null) {
                this.folderListeners.removeElement(folderListener);
            }
        }
    }
    
    public void removeMessageChangedListener(final MessageChangedListener messageChangedListener) {
        synchronized (this) {
            if (this.messageChangedListeners != null) {
                this.messageChangedListeners.removeElement(messageChangedListener);
            }
        }
    }
    
    public void removeMessageCountListener(final MessageCountListener messageCountListener) {
        synchronized (this) {
            if (this.messageCountListeners != null) {
                this.messageCountListeners.removeElement(messageCountListener);
            }
        }
    }
    
    public abstract boolean renameTo(final Folder p0) throws MessagingException;
    
    public Message[] search(final SearchTerm searchTerm) throws MessagingException {
        return this.search(searchTerm, this.getMessages());
    }
    
    public Message[] search(final SearchTerm searchTerm, final Message[] array) throws MessagingException {
        final Vector<Message> vector = new Vector<Message>();
        int i = 0;
        while (i < array.length) {
            while (true) {
                try {
                    if (array[i].match(searchTerm)) {
                        vector.addElement(array[i]);
                    }
                    ++i;
                }
                catch (MessageRemovedException ex) {
                    continue;
                }
                break;
            }
        }
        final Message[] array2 = new Message[vector.size()];
        vector.copyInto(array2);
        return array2;
    }
    
    public void setFlags(int i, final int n, final Flags flags, final boolean b) throws MessagingException {
        // monitorenter(this)
        while (i <= n) {
            while (true) {
                try {
                    this.getMessage(i).setFlags(flags, b);
                    ++i;
                }
                catch (MessageRemovedException ex) {
                    continue;
                }
                finally {
                }
                // monitorexit(this)
                break;
            }
        }
    }
    // monitorexit(this)
    
    public void setFlags(final int[] p0, final Flags p1, final boolean p2) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: iconst_0       
        //     3: istore          4
        //     5: aload_1        
        //     6: arraylength    
        //     7: istore          5
        //     9: iload           4
        //    11: iload           5
        //    13: if_icmplt       19
        //    16: aload_0        
        //    17: monitorexit    
        //    18: return         
        //    19: aload_0        
        //    20: aload_1        
        //    21: iload           4
        //    23: iaload         
        //    24: invokevirtual   javax/mail/Folder.getMessage:(I)Ljavax/mail/Message;
        //    27: aload_2        
        //    28: iload_3        
        //    29: invokevirtual   javax/mail/Message.setFlags:(Ljavax/mail/Flags;Z)V
        //    32: iload           4
        //    34: iconst_1       
        //    35: iadd           
        //    36: istore          4
        //    38: goto            5
        //    41: astore_1       
        //    42: aload_0        
        //    43: monitorexit    
        //    44: aload_1        
        //    45: athrow         
        //    46: astore          6
        //    48: goto            32
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  5      9      41     46     Any
        //  19     32     46     51     Ljavax/mail/MessageRemovedException;
        //  19     32     41     46     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0019:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setFlags(final Message[] p0, final Flags p1, final boolean p2) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: iconst_0       
        //     3: istore          4
        //     5: aload_1        
        //     6: arraylength    
        //     7: istore          5
        //     9: iload           4
        //    11: iload           5
        //    13: if_icmplt       19
        //    16: aload_0        
        //    17: monitorexit    
        //    18: return         
        //    19: aload_1        
        //    20: iload           4
        //    22: aaload         
        //    23: aload_2        
        //    24: iload_3        
        //    25: invokevirtual   javax/mail/Message.setFlags:(Ljavax/mail/Flags;Z)V
        //    28: iload           4
        //    30: iconst_1       
        //    31: iadd           
        //    32: istore          4
        //    34: goto            5
        //    37: astore_1       
        //    38: aload_0        
        //    39: monitorexit    
        //    40: aload_1        
        //    41: athrow         
        //    42: astore          6
        //    44: goto            28
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  5      9      37     42     Any
        //  19     28     42     47     Ljavax/mail/MessageRemovedException;
        //  19     28     37     42     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setSubscribed(final boolean b) throws MessagingException {
        throw new MethodNotSupportedException();
    }
    
    @Override
    public String toString() {
        final String fullName = this.getFullName();
        if (fullName != null) {
            return fullName;
        }
        return super.toString();
    }
    
    static class TerminatorEvent extends MailEvent
    {
        private static final long serialVersionUID = 3765761925441296565L;
        
        TerminatorEvent() {
            super(new Object());
        }
        
        @Override
        public void dispatch(final Object o) {
            Thread.currentThread().interrupt();
        }
    }
}
