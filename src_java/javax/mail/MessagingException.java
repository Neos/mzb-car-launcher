package javax.mail;

public class MessagingException extends Exception
{
    private static final long serialVersionUID = -7569192289819959253L;
    private Exception next;
    
    public MessagingException() {
        this.initCause(null);
    }
    
    public MessagingException(final String s) {
        super(s);
        this.initCause(null);
    }
    
    public MessagingException(final String s, final Exception next) {
        super(s);
        this.next = next;
        this.initCause(null);
    }
    
    private final String superToString() {
        return super.toString();
    }
    
    @Override
    public Throwable getCause() {
        synchronized (this) {
            return this.next;
        }
    }
    
    public Exception getNextException() {
        synchronized (this) {
            return this.next;
        }
    }
    
    public boolean setNextException(final Exception next) {
        // monitorenter(this)
        Exception next2 = this;
        try {
            while (next2 instanceof MessagingException && ((MessagingException)next2).next != null) {
                next2 = ((MessagingException)next2).next;
            }
            boolean b;
            if (next2 instanceof MessagingException) {
                ((MessagingException)next2).next = next;
                b = true;
            }
            else {
                b = false;
            }
            return b;
        }
        finally {
        }
        // monitorexit(this)
    }
    
    @Override
    public String toString() {
        synchronized (this) {
            String s = super.toString();
            final Exception next = this.next;
            if (next != null) {
                String s2;
                if ((s2 = s) == null) {
                    s2 = "";
                }
                final StringBuffer sb = new StringBuffer(s2);
                Exception next2 = next;
                while (next2 != null) {
                    sb.append(";\n  nested exception is:\n\t");
                    if (next2 instanceof MessagingException) {
                        final MessagingException ex = (MessagingException)next2;
                        sb.append(ex.superToString());
                        next2 = ex.next;
                    }
                    else {
                        sb.append(next2.toString());
                        next2 = null;
                    }
                }
                s = sb.toString();
            }
            return s;
        }
    }
}
