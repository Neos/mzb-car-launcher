package javax.mail;

import java.util.*;
import javax.mail.event.*;

public abstract class Store extends Service
{
    private volatile Vector folderListeners;
    private volatile Vector storeListeners;
    
    protected Store(final Session session, final URLName urlName) {
        super(session, urlName);
        this.storeListeners = null;
        this.folderListeners = null;
    }
    
    public void addFolderListener(final FolderListener folderListener) {
        synchronized (this) {
            if (this.folderListeners == null) {
                this.folderListeners = new Vector();
            }
            this.folderListeners.addElement(folderListener);
        }
    }
    
    public void addStoreListener(final StoreListener storeListener) {
        synchronized (this) {
            if (this.storeListeners == null) {
                this.storeListeners = new Vector();
            }
            this.storeListeners.addElement(storeListener);
        }
    }
    
    public abstract Folder getDefaultFolder() throws MessagingException;
    
    public abstract Folder getFolder(final String p0) throws MessagingException;
    
    public abstract Folder getFolder(final URLName p0) throws MessagingException;
    
    public Folder[] getPersonalNamespaces() throws MessagingException {
        return new Folder[] { this.getDefaultFolder() };
    }
    
    public Folder[] getSharedNamespaces() throws MessagingException {
        return new Folder[0];
    }
    
    public Folder[] getUserNamespaces(final String s) throws MessagingException {
        return new Folder[0];
    }
    
    protected void notifyFolderListeners(final int n, final Folder folder) {
        if (this.folderListeners == null) {
            return;
        }
        this.queueEvent(new FolderEvent(this, folder, n), this.folderListeners);
    }
    
    protected void notifyFolderRenamedListeners(final Folder folder, final Folder folder2) {
        if (this.folderListeners == null) {
            return;
        }
        this.queueEvent(new FolderEvent(this, folder, folder2, 3), this.folderListeners);
    }
    
    protected void notifyStoreListeners(final int n, final String s) {
        if (this.storeListeners == null) {
            return;
        }
        this.queueEvent(new StoreEvent(this, n, s), this.storeListeners);
    }
    
    public void removeFolderListener(final FolderListener folderListener) {
        synchronized (this) {
            if (this.folderListeners != null) {
                this.folderListeners.removeElement(folderListener);
            }
        }
    }
    
    public void removeStoreListener(final StoreListener storeListener) {
        synchronized (this) {
            if (this.storeListeners != null) {
                this.storeListeners.removeElement(storeListener);
            }
        }
    }
}
