package javax.mail;

public class FolderClosedException extends MessagingException
{
    private static final long serialVersionUID = 1687879213433302315L;
    private transient Folder folder;
    
    public FolderClosedException(final Folder folder) {
        this(folder, null);
    }
    
    public FolderClosedException(final Folder folder, final String s) {
        super(s);
        this.folder = folder;
    }
    
    public Folder getFolder() {
        return this.folder;
    }
}
