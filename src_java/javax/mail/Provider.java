package javax.mail;

public class Provider
{
    private String className;
    private String protocol;
    private Type type;
    private String vendor;
    private String version;
    
    public Provider(final Type type, final String protocol, final String className, final String vendor, final String version) {
        this.type = type;
        this.protocol = protocol;
        this.className = className;
        this.vendor = vendor;
        this.version = version;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public Type getType() {
        return this.type;
    }
    
    public String getVendor() {
        return this.vendor;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    @Override
    public String toString() {
        String s2;
        final String s = s2 = "javax.mail.Provider[" + this.type + "," + this.protocol + "," + this.className;
        if (this.vendor != null) {
            s2 = String.valueOf(s) + "," + this.vendor;
        }
        String string = s2;
        if (this.version != null) {
            string = String.valueOf(s2) + "," + this.version;
        }
        return String.valueOf(string) + "]";
    }
    
    public static class Type
    {
        public static final Type STORE;
        public static final Type TRANSPORT;
        private String type;
        
        static {
            STORE = new Type("STORE");
            TRANSPORT = new Type("TRANSPORT");
        }
        
        private Type(final String type) {
            this.type = type;
        }
        
        @Override
        public String toString() {
            return this.type;
        }
    }
}
