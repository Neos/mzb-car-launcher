package javax.mail;

import java.util.*;
import javax.mail.event.*;

public abstract class Transport extends Service
{
    private Vector transportListeners;
    
    public Transport(final Session session, final URLName urlName) {
        super(session, urlName);
        this.transportListeners = null;
    }
    
    public static void send(final Message message) throws MessagingException {
        message.saveChanges();
        send0(message, message.getAllRecipients());
    }
    
    public static void send(final Message message, final Address[] array) throws MessagingException {
        message.saveChanges();
        send0(message, array);
    }
    
    private static void send0(final Message p0, final Address[] p1) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnull          9
        //     4: aload_1        
        //     5: arraylength    
        //     6: ifne            19
        //     9: new             Ljavax/mail/SendFailedException;
        //    12: dup            
        //    13: ldc             "No recipient addresses"
        //    15: invokespecial   javax/mail/SendFailedException.<init>:(Ljava/lang/String;)V
        //    18: athrow         
        //    19: new             Ljava/util/Hashtable;
        //    22: dup            
        //    23: invokespecial   java/util/Hashtable.<init>:()V
        //    26: astore          6
        //    28: new             Ljava/util/Vector;
        //    31: dup            
        //    32: invokespecial   java/util/Vector.<init>:()V
        //    35: astore          7
        //    37: new             Ljava/util/Vector;
        //    40: dup            
        //    41: invokespecial   java/util/Vector.<init>:()V
        //    44: astore          9
        //    46: new             Ljava/util/Vector;
        //    49: dup            
        //    50: invokespecial   java/util/Vector.<init>:()V
        //    53: astore          8
        //    55: iconst_0       
        //    56: istore_2       
        //    57: iload_2        
        //    58: aload_1        
        //    59: arraylength    
        //    60: if_icmplt       83
        //    63: aload           6
        //    65: invokevirtual   java/util/Hashtable.size:()I
        //    68: istore_2       
        //    69: iload_2        
        //    70: ifne            158
        //    73: new             Ljavax/mail/SendFailedException;
        //    76: dup            
        //    77: ldc             "No recipient addresses"
        //    79: invokespecial   javax/mail/SendFailedException.<init>:(Ljava/lang/String;)V
        //    82: athrow         
        //    83: aload           6
        //    85: aload_1        
        //    86: iload_2        
        //    87: aaload         
        //    88: invokevirtual   javax/mail/Address.getType:()Ljava/lang/String;
        //    91: invokevirtual   java/util/Hashtable.containsKey:(Ljava/lang/Object;)Z
        //    94: ifeq            124
        //    97: aload           6
        //    99: aload_1        
        //   100: iload_2        
        //   101: aaload         
        //   102: invokevirtual   javax/mail/Address.getType:()Ljava/lang/String;
        //   105: invokevirtual   java/util/Hashtable.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   108: checkcast       Ljava/util/Vector;
        //   111: aload_1        
        //   112: iload_2        
        //   113: aaload         
        //   114: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   117: iload_2        
        //   118: iconst_1       
        //   119: iadd           
        //   120: istore_2       
        //   121: goto            57
        //   124: new             Ljava/util/Vector;
        //   127: dup            
        //   128: invokespecial   java/util/Vector.<init>:()V
        //   131: astore          5
        //   133: aload           5
        //   135: aload_1        
        //   136: iload_2        
        //   137: aaload         
        //   138: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   141: aload           6
        //   143: aload_1        
        //   144: iload_2        
        //   145: aaload         
        //   146: invokevirtual   javax/mail/Address.getType:()Ljava/lang/String;
        //   149: aload           5
        //   151: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   154: pop            
        //   155: goto            117
        //   158: aload_0        
        //   159: getfield        javax/mail/Message.session:Ljavax/mail/Session;
        //   162: ifnull          204
        //   165: aload_0        
        //   166: getfield        javax/mail/Message.session:Ljavax/mail/Session;
        //   169: astore          5
        //   171: iload_2        
        //   172: iconst_1       
        //   173: if_icmpne       224
        //   176: aload           5
        //   178: aload_1        
        //   179: iconst_0       
        //   180: aaload         
        //   181: invokevirtual   javax/mail/Session.getTransport:(Ljavax/mail/Address;)Ljavax/mail/Transport;
        //   184: astore          5
        //   186: aload           5
        //   188: invokevirtual   javax/mail/Transport.connect:()V
        //   191: aload           5
        //   193: aload_0        
        //   194: aload_1        
        //   195: invokevirtual   javax/mail/Transport.sendMessage:(Ljavax/mail/Message;[Ljavax/mail/Address;)V
        //   198: aload           5
        //   200: invokevirtual   javax/mail/Transport.close:()V
        //   203: return         
        //   204: invokestatic    java/lang/System.getProperties:()Ljava/util/Properties;
        //   207: aconst_null    
        //   208: invokestatic    javax/mail/Session.getDefaultInstance:(Ljava/util/Properties;Ljavax/mail/Authenticator;)Ljavax/mail/Session;
        //   211: astore          5
        //   213: goto            171
        //   216: astore_0       
        //   217: aload           5
        //   219: invokevirtual   javax/mail/Transport.close:()V
        //   222: aload_0        
        //   223: athrow         
        //   224: aconst_null    
        //   225: astore_1       
        //   226: iconst_0       
        //   227: istore_2       
        //   228: aload           6
        //   230: invokevirtual   java/util/Hashtable.elements:()Ljava/util/Enumeration;
        //   233: astore          10
        //   235: aload           10
        //   237: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //   242: ifne            371
        //   245: iload_2        
        //   246: ifne            265
        //   249: aload           7
        //   251: invokevirtual   java/util/Vector.size:()I
        //   254: ifne            265
        //   257: aload           8
        //   259: invokevirtual   java/util/Vector.size:()I
        //   262: ifeq            203
        //   265: aconst_null    
        //   266: checkcast       [Ljavax/mail/Address;
        //   269: astore_0       
        //   270: aconst_null    
        //   271: checkcast       [Ljavax/mail/Address;
        //   274: astore          5
        //   276: aconst_null    
        //   277: checkcast       [Ljavax/mail/Address;
        //   280: astore          6
        //   282: aload           9
        //   284: invokevirtual   java/util/Vector.size:()I
        //   287: ifle            305
        //   290: aload           9
        //   292: invokevirtual   java/util/Vector.size:()I
        //   295: anewarray       Ljavax/mail/Address;
        //   298: astore_0       
        //   299: aload           9
        //   301: aload_0        
        //   302: invokevirtual   java/util/Vector.copyInto:([Ljava/lang/Object;)V
        //   305: aload           8
        //   307: invokevirtual   java/util/Vector.size:()I
        //   310: ifle            330
        //   313: aload           8
        //   315: invokevirtual   java/util/Vector.size:()I
        //   318: anewarray       Ljavax/mail/Address;
        //   321: astore          5
        //   323: aload           8
        //   325: aload           5
        //   327: invokevirtual   java/util/Vector.copyInto:([Ljava/lang/Object;)V
        //   330: aload           7
        //   332: invokevirtual   java/util/Vector.size:()I
        //   335: ifle            355
        //   338: aload           7
        //   340: invokevirtual   java/util/Vector.size:()I
        //   343: anewarray       Ljavax/mail/Address;
        //   346: astore          6
        //   348: aload           7
        //   350: aload           6
        //   352: invokevirtual   java/util/Vector.copyInto:([Ljava/lang/Object;)V
        //   355: new             Ljavax/mail/SendFailedException;
        //   358: dup            
        //   359: ldc             "Sending failed"
        //   361: aload_1        
        //   362: aload_0        
        //   363: aload           5
        //   365: aload           6
        //   367: invokespecial   javax/mail/SendFailedException.<init>:(Ljava/lang/String;Ljava/lang/Exception;[Ljavax/mail/Address;[Ljavax/mail/Address;[Ljavax/mail/Address;)V
        //   370: athrow         
        //   371: aload           10
        //   373: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //   378: checkcast       Ljava/util/Vector;
        //   381: astore          11
        //   383: aload           11
        //   385: invokevirtual   java/util/Vector.size:()I
        //   388: anewarray       Ljavax/mail/Address;
        //   391: astore          6
        //   393: aload           11
        //   395: aload           6
        //   397: invokevirtual   java/util/Vector.copyInto:([Ljava/lang/Object;)V
        //   400: aload           5
        //   402: aload           6
        //   404: iconst_0       
        //   405: aaload         
        //   406: invokevirtual   javax/mail/Session.getTransport:(Ljavax/mail/Address;)Ljavax/mail/Transport;
        //   409: astore          11
        //   411: aload           11
        //   413: ifnonnull       441
        //   416: iconst_0       
        //   417: istore_3       
        //   418: iload_3        
        //   419: aload           6
        //   421: arraylength    
        //   422: if_icmpge       235
        //   425: aload           7
        //   427: aload           6
        //   429: iload_3        
        //   430: aaload         
        //   431: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   434: iload_3        
        //   435: iconst_1       
        //   436: iadd           
        //   437: istore_3       
        //   438: goto            418
        //   441: aload           11
        //   443: invokevirtual   javax/mail/Transport.connect:()V
        //   446: aload           11
        //   448: aload_0        
        //   449: aload           6
        //   451: invokevirtual   javax/mail/Transport.sendMessage:(Ljavax/mail/Message;[Ljavax/mail/Address;)V
        //   454: aload           11
        //   456: invokevirtual   javax/mail/Transport.close:()V
        //   459: goto            235
        //   462: astore          6
        //   464: iconst_1       
        //   465: istore_3       
        //   466: aload_1        
        //   467: ifnonnull       550
        //   470: aload           6
        //   472: astore_1       
        //   473: aload           6
        //   475: invokevirtual   javax/mail/SendFailedException.getInvalidAddresses:()[Ljavax/mail/Address;
        //   478: astore          12
        //   480: aload           12
        //   482: ifnull          494
        //   485: iconst_0       
        //   486: istore_2       
        //   487: iload_2        
        //   488: aload           12
        //   490: arraylength    
        //   491: if_icmplt       568
        //   494: aload           6
        //   496: invokevirtual   javax/mail/SendFailedException.getValidSentAddresses:()[Ljavax/mail/Address;
        //   499: astore          12
        //   501: aload           12
        //   503: ifnull          515
        //   506: iconst_0       
        //   507: istore_2       
        //   508: iload_2        
        //   509: aload           12
        //   511: arraylength    
        //   512: if_icmplt       584
        //   515: aload           6
        //   517: invokevirtual   javax/mail/SendFailedException.getValidUnsentAddresses:()[Ljavax/mail/Address;
        //   520: astore          6
        //   522: aload           6
        //   524: ifnull          540
        //   527: iconst_0       
        //   528: istore_2       
        //   529: aload           6
        //   531: arraylength    
        //   532: istore          4
        //   534: iload_2        
        //   535: iload           4
        //   537: if_icmplt       600
        //   540: aload           11
        //   542: invokevirtual   javax/mail/Transport.close:()V
        //   545: iload_3        
        //   546: istore_2       
        //   547: goto            235
        //   550: aload_1        
        //   551: aload           6
        //   553: invokevirtual   javax/mail/MessagingException.setNextException:(Ljava/lang/Exception;)Z
        //   556: pop            
        //   557: goto            473
        //   560: astore_0       
        //   561: aload           11
        //   563: invokevirtual   javax/mail/Transport.close:()V
        //   566: aload_0        
        //   567: athrow         
        //   568: aload           7
        //   570: aload           12
        //   572: iload_2        
        //   573: aaload         
        //   574: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   577: iload_2        
        //   578: iconst_1       
        //   579: iadd           
        //   580: istore_2       
        //   581: goto            487
        //   584: aload           9
        //   586: aload           12
        //   588: iload_2        
        //   589: aaload         
        //   590: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   593: iload_2        
        //   594: iconst_1       
        //   595: iadd           
        //   596: istore_2       
        //   597: goto            508
        //   600: aload           8
        //   602: aload           6
        //   604: iload_2        
        //   605: aaload         
        //   606: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
        //   609: iload_2        
        //   610: iconst_1       
        //   611: iadd           
        //   612: istore_2       
        //   613: goto            529
        //   616: astore          6
        //   618: iconst_1       
        //   619: istore_2       
        //   620: aload_1        
        //   621: ifnonnull       635
        //   624: aload           6
        //   626: astore_1       
        //   627: aload           11
        //   629: invokevirtual   javax/mail/Transport.close:()V
        //   632: goto            235
        //   635: aload_1        
        //   636: aload           6
        //   638: invokevirtual   javax/mail/MessagingException.setNextException:(Ljava/lang/Exception;)Z
        //   641: pop            
        //   642: goto            627
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  186    198    216    224    Any
        //  441    454    462    616    Ljavax/mail/SendFailedException;
        //  441    454    616    645    Ljavax/mail/MessagingException;
        //  441    454    560    568    Any
        //  473    480    560    568    Any
        //  487    494    560    568    Any
        //  494    501    560    568    Any
        //  508    515    560    568    Any
        //  515    522    560    568    Any
        //  529    534    560    568    Any
        //  550    557    560    568    Any
        //  568    577    560    568    Any
        //  584    593    560    568    Any
        //  600    609    560    568    Any
        //  635    642    560    568    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void addTransportListener(final TransportListener transportListener) {
        synchronized (this) {
            if (this.transportListeners == null) {
                this.transportListeners = new Vector();
            }
            this.transportListeners.addElement(transportListener);
        }
    }
    
    protected void notifyTransportListeners(final int n, final Address[] array, final Address[] array2, final Address[] array3, final Message message) {
        if (this.transportListeners == null) {
            return;
        }
        this.queueEvent(new TransportEvent(this, n, array, array2, array3, message), this.transportListeners);
    }
    
    public void removeTransportListener(final TransportListener transportListener) {
        synchronized (this) {
            if (this.transportListeners != null) {
                this.transportListeners.removeElement(transportListener);
            }
        }
    }
    
    public abstract void sendMessage(final Message p0, final Address[] p1) throws MessagingException;
}
