package javax.mail.event;

public class ConnectionEvent extends MailEvent
{
    public static final int CLOSED = 3;
    public static final int DISCONNECTED = 2;
    public static final int OPENED = 1;
    private static final long serialVersionUID = -1855480171284792957L;
    protected int type;
    
    public ConnectionEvent(final Object o, final int type) {
        super(o);
        this.type = type;
    }
    
    @Override
    public void dispatch(final Object o) {
        if (this.type == 1) {
            ((ConnectionListener)o).opened(this);
        }
        else {
            if (this.type == 2) {
                ((ConnectionListener)o).disconnected(this);
                return;
            }
            if (this.type == 3) {
                ((ConnectionListener)o).closed(this);
            }
        }
    }
    
    public int getType() {
        return this.type;
    }
}
