package javax.mail.event;

import javax.mail.*;

public class FolderEvent extends MailEvent
{
    public static final int CREATED = 1;
    public static final int DELETED = 2;
    public static final int RENAMED = 3;
    private static final long serialVersionUID = 5278131310563694307L;
    protected transient Folder folder;
    protected transient Folder newFolder;
    protected int type;
    
    public FolderEvent(final Object o, final Folder folder, final int n) {
        this(o, folder, folder, n);
    }
    
    public FolderEvent(final Object o, final Folder folder, final Folder newFolder, final int type) {
        super(o);
        this.folder = folder;
        this.newFolder = newFolder;
        this.type = type;
    }
    
    @Override
    public void dispatch(final Object o) {
        if (this.type == 1) {
            ((FolderListener)o).folderCreated(this);
        }
        else {
            if (this.type == 2) {
                ((FolderListener)o).folderDeleted(this);
                return;
            }
            if (this.type == 3) {
                ((FolderListener)o).folderRenamed(this);
            }
        }
    }
    
    public Folder getFolder() {
        return this.folder;
    }
    
    public Folder getNewFolder() {
        return this.newFolder;
    }
    
    public int getType() {
        return this.type;
    }
}
