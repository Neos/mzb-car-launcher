package javax.mail.event;

import javax.mail.*;

public class StoreEvent extends MailEvent
{
    public static final int ALERT = 1;
    public static final int NOTICE = 2;
    private static final long serialVersionUID = 1938704919992515330L;
    protected String message;
    protected int type;
    
    public StoreEvent(final Store store, final int type, final String message) {
        super(store);
        this.type = type;
        this.message = message;
    }
    
    @Override
    public void dispatch(final Object o) {
        ((StoreListener)o).notification(this);
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public int getMessageType() {
        return this.type;
    }
}
