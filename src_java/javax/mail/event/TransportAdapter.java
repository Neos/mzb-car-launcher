package javax.mail.event;

public abstract class TransportAdapter implements TransportListener
{
    @Override
    public void messageDelivered(final TransportEvent transportEvent) {
    }
    
    @Override
    public void messageNotDelivered(final TransportEvent transportEvent) {
    }
    
    @Override
    public void messagePartiallyDelivered(final TransportEvent transportEvent) {
    }
}
