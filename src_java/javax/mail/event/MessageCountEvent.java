package javax.mail.event;

import javax.mail.*;

public class MessageCountEvent extends MailEvent
{
    public static final int ADDED = 1;
    public static final int REMOVED = 2;
    private static final long serialVersionUID = -7447022340837897369L;
    protected transient Message[] msgs;
    protected boolean removed;
    protected int type;
    
    public MessageCountEvent(final Folder folder, final int type, final boolean removed, final Message[] msgs) {
        super(folder);
        this.type = type;
        this.removed = removed;
        this.msgs = msgs;
    }
    
    @Override
    public void dispatch(final Object o) {
        if (this.type == 1) {
            ((MessageCountListener)o).messagesAdded(this);
            return;
        }
        ((MessageCountListener)o).messagesRemoved(this);
    }
    
    public Message[] getMessages() {
        return this.msgs;
    }
    
    public int getType() {
        return this.type;
    }
    
    public boolean isRemoved() {
        return this.removed;
    }
}
