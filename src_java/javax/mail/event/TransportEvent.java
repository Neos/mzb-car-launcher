package javax.mail.event;

import javax.mail.*;

public class TransportEvent extends MailEvent
{
    public static final int MESSAGE_DELIVERED = 1;
    public static final int MESSAGE_NOT_DELIVERED = 2;
    public static final int MESSAGE_PARTIALLY_DELIVERED = 3;
    private static final long serialVersionUID = -4729852364684273073L;
    protected transient Address[] invalid;
    protected transient Message msg;
    protected int type;
    protected transient Address[] validSent;
    protected transient Address[] validUnsent;
    
    public TransportEvent(final Transport transport, final int type, final Address[] validSent, final Address[] validUnsent, final Address[] invalid, final Message msg) {
        super(transport);
        this.type = type;
        this.validSent = validSent;
        this.validUnsent = validUnsent;
        this.invalid = invalid;
        this.msg = msg;
    }
    
    @Override
    public void dispatch(final Object o) {
        if (this.type == 1) {
            ((TransportListener)o).messageDelivered(this);
            return;
        }
        if (this.type == 2) {
            ((TransportListener)o).messageNotDelivered(this);
            return;
        }
        ((TransportListener)o).messagePartiallyDelivered(this);
    }
    
    public Address[] getInvalidAddresses() {
        return this.invalid;
    }
    
    public Message getMessage() {
        return this.msg;
    }
    
    public int getType() {
        return this.type;
    }
    
    public Address[] getValidSentAddresses() {
        return this.validSent;
    }
    
    public Address[] getValidUnsentAddresses() {
        return this.validUnsent;
    }
}
