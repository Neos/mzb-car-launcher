package javax.mail.event;

import java.util.*;

public interface ConnectionListener extends EventListener
{
    void closed(final ConnectionEvent p0);
    
    void disconnected(final ConnectionEvent p0);
    
    void opened(final ConnectionEvent p0);
}
