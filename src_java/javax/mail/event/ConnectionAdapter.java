package javax.mail.event;

public abstract class ConnectionAdapter implements ConnectionListener
{
    @Override
    public void closed(final ConnectionEvent connectionEvent) {
    }
    
    @Override
    public void disconnected(final ConnectionEvent connectionEvent) {
    }
    
    @Override
    public void opened(final ConnectionEvent connectionEvent) {
    }
}
