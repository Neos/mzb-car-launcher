package javax.mail.event;

public abstract class FolderAdapter implements FolderListener
{
    @Override
    public void folderCreated(final FolderEvent folderEvent) {
    }
    
    @Override
    public void folderDeleted(final FolderEvent folderEvent) {
    }
    
    @Override
    public void folderRenamed(final FolderEvent folderEvent) {
    }
}
