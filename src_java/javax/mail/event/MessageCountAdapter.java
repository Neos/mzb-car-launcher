package javax.mail.event;

public abstract class MessageCountAdapter implements MessageCountListener
{
    @Override
    public void messagesAdded(final MessageCountEvent messageCountEvent) {
    }
    
    @Override
    public void messagesRemoved(final MessageCountEvent messageCountEvent) {
    }
}
