package javax.mail;

public class StoreClosedException extends MessagingException
{
    private static final long serialVersionUID = -3145392336120082655L;
    private transient Store store;
    
    public StoreClosedException(final Store store) {
        this(store, null);
    }
    
    public StoreClosedException(final Store store, final String s) {
        super(s);
        this.store = store;
    }
    
    public Store getStore() {
        return this.store;
    }
}
