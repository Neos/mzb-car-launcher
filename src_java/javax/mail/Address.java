package javax.mail;

import java.io.*;

public abstract class Address implements Serializable
{
    private static final long serialVersionUID = -5822459626751992278L;
    
    @Override
    public abstract boolean equals(final Object p0);
    
    public abstract String getType();
    
    @Override
    public abstract String toString();
}
