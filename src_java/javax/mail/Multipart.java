package javax.mail;

import java.util.*;
import java.io.*;

public abstract class Multipart
{
    protected String contentType;
    protected Part parent;
    protected Vector parts;
    
    protected Multipart() {
        this.parts = new Vector();
        this.contentType = "multipart/mixed";
    }
    
    public void addBodyPart(final BodyPart bodyPart) throws MessagingException {
        synchronized (this) {
            if (this.parts == null) {
                this.parts = new Vector();
            }
            this.parts.addElement(bodyPart);
            bodyPart.setParent(this);
        }
    }
    
    public void addBodyPart(final BodyPart bodyPart, final int n) throws MessagingException {
        synchronized (this) {
            if (this.parts == null) {
                this.parts = new Vector();
            }
            this.parts.insertElementAt(bodyPart, n);
            bodyPart.setParent(this);
        }
    }
    
    public BodyPart getBodyPart(final int n) throws MessagingException {
        synchronized (this) {
            if (this.parts == null) {
                throw new IndexOutOfBoundsException("No such BodyPart");
            }
        }
        // monitorexit(this)
        return this.parts.elementAt(n);
    }
    
    public String getContentType() {
        return this.contentType;
    }
    
    public int getCount() throws MessagingException {
        synchronized (this) {
            int size;
            if (this.parts == null) {
                size = 0;
            }
            else {
                size = this.parts.size();
            }
            return size;
        }
    }
    
    public Part getParent() {
        synchronized (this) {
            return this.parent;
        }
    }
    
    public void removeBodyPart(final int n) throws MessagingException {
        synchronized (this) {
            if (this.parts == null) {
                throw new IndexOutOfBoundsException("No such BodyPart");
            }
        }
        final BodyPart bodyPart = this.parts.elementAt(n);
        this.parts.removeElementAt(n);
        bodyPart.setParent(null);
    }
    // monitorexit(this)
    
    public boolean removeBodyPart(final BodyPart bodyPart) throws MessagingException {
        synchronized (this) {
            if (this.parts == null) {
                throw new MessagingException("No such body part");
            }
        }
        final BodyPart bodyPart2;
        final boolean removeElement = this.parts.removeElement(bodyPart2);
        bodyPart2.setParent(null);
        // monitorexit(this)
        return removeElement;
    }
    
    protected void setMultipartDataSource(final MultipartDataSource multipartDataSource) throws MessagingException {
        synchronized (this) {
            this.contentType = multipartDataSource.getContentType();
            for (int count = multipartDataSource.getCount(), i = 0; i < count; ++i) {
                this.addBodyPart(multipartDataSource.getBodyPart(i));
            }
        }
    }
    
    public void setParent(final Part parent) {
        synchronized (this) {
            this.parent = parent;
        }
    }
    
    public abstract void writeTo(final OutputStream p0) throws IOException, MessagingException;
}
