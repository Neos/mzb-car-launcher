package javax.mail.util;

import javax.mail.internet.*;
import java.io.*;

public class SharedByteArrayInputStream extends ByteArrayInputStream implements SharedInputStream
{
    protected int start;
    
    public SharedByteArrayInputStream(final byte[] array) {
        super(array);
        this.start = 0;
    }
    
    public SharedByteArrayInputStream(final byte[] array, final int start, final int n) {
        super(array, start, n);
        this.start = 0;
        this.start = start;
    }
    
    @Override
    public long getPosition() {
        return this.pos - this.start;
    }
    
    @Override
    public InputStream newStream(final long n, final long n2) {
        if (n < 0L) {
            throw new IllegalArgumentException("start < 0");
        }
        long n3 = n2;
        if (n2 == -1L) {
            n3 = this.count - this.start;
        }
        return new SharedByteArrayInputStream(this.buf, this.start + (int)n, (int)(n3 - n));
    }
}
