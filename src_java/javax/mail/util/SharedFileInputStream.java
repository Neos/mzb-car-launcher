package javax.mail.util;

import javax.mail.internet.*;
import java.io.*;

public class SharedFileInputStream extends BufferedInputStream implements SharedInputStream
{
    private static int defaultBufferSize;
    protected long bufpos;
    protected int bufsize;
    protected long datalen;
    protected RandomAccessFile in;
    private boolean master;
    private SharedFile sf;
    protected long start;
    
    static {
        SharedFileInputStream.defaultBufferSize = 2048;
    }
    
    public SharedFileInputStream(final File file) throws IOException {
        this(file, SharedFileInputStream.defaultBufferSize);
    }
    
    public SharedFileInputStream(final File file, final int n) throws IOException {
        super(null);
        this.start = 0L;
        this.master = true;
        if (n <= 0) {
            throw new IllegalArgumentException("Buffer size <= 0");
        }
        this.init(new SharedFile(file), n);
    }
    
    public SharedFileInputStream(final String s) throws IOException {
        this(s, SharedFileInputStream.defaultBufferSize);
    }
    
    public SharedFileInputStream(final String s, final int n) throws IOException {
        super(null);
        this.start = 0L;
        this.master = true;
        if (n <= 0) {
            throw new IllegalArgumentException("Buffer size <= 0");
        }
        this.init(new SharedFile(s), n);
    }
    
    private SharedFileInputStream(final SharedFile sf, final long n, final long datalen, final int bufsize) {
        super(null);
        this.start = 0L;
        this.master = true;
        this.master = false;
        this.sf = sf;
        this.in = sf.open();
        this.start = n;
        this.bufpos = n;
        this.datalen = datalen;
        this.bufsize = bufsize;
        this.buf = new byte[bufsize];
    }
    
    private void ensureOpen() throws IOException {
        if (this.in == null) {
            throw new IOException("Stream closed");
        }
    }
    
    private void fill() throws IOException {
        if (this.markpos < 0) {
            this.pos = 0;
            this.bufpos += this.count;
        }
        else if (this.pos >= this.buf.length) {
            if (this.markpos > 0) {
                final int pos = this.pos - this.markpos;
                System.arraycopy(this.buf, this.markpos, this.buf, 0, pos);
                this.pos = pos;
                this.bufpos += this.markpos;
                this.markpos = 0;
            }
            else if (this.buf.length >= this.marklimit) {
                this.markpos = -1;
                this.pos = 0;
                this.bufpos += this.count;
            }
            else {
                int marklimit;
                if ((marklimit = this.pos * 2) > this.marklimit) {
                    marklimit = this.marklimit;
                }
                final byte[] buf = new byte[marklimit];
                System.arraycopy(this.buf, 0, buf, 0, this.pos);
                this.buf = buf;
            }
        }
        this.count = this.pos;
        this.in.seek(this.bufpos + this.pos);
        int n;
        if (this.bufpos - this.start + this.pos + (n = this.buf.length - this.pos) > this.datalen) {
            n = (int)(this.datalen - (this.bufpos - this.start + this.pos));
        }
        final int read = this.in.read(this.buf, this.pos, n);
        if (read > 0) {
            this.count = this.pos + read;
        }
    }
    
    private int in_available() throws IOException {
        return (int)(this.start + this.datalen - (this.bufpos + this.count));
    }
    
    private void init(final SharedFile sf, final int bufsize) throws IOException {
        this.sf = sf;
        this.in = sf.open();
        this.start = 0L;
        this.datalen = this.in.length();
        this.bufsize = bufsize;
        this.buf = new byte[bufsize];
    }
    
    private int read1(final byte[] array, final int n, final int n2) throws IOException {
        int n3;
        if ((n3 = this.count - this.pos) <= 0) {
            this.fill();
            if ((n3 = this.count - this.pos) <= 0) {
                return -1;
            }
        }
        if (n3 >= n2) {
            n3 = n2;
        }
        System.arraycopy(this.buf, this.pos, array, n, n3);
        this.pos += n3;
        return n3;
    }
    
    @Override
    public int available() throws IOException {
        synchronized (this) {
            this.ensureOpen();
            return this.count - this.pos + this.in_available();
        }
    }
    
    @Override
    public void close() throws IOException {
        if (this.in == null) {
            return;
        }
        try {
            if (this.master) {
                this.sf.forceClose();
            }
            else {
                this.sf.close();
            }
        }
        finally {
            this.sf = null;
            this.in = null;
            this.buf = null;
        }
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.close();
    }
    
    @Override
    public long getPosition() {
        if (this.in == null) {
            throw new RuntimeException("Stream closed");
        }
        return this.bufpos + this.pos - this.start;
    }
    
    @Override
    public void mark(final int marklimit) {
        synchronized (this) {
            this.marklimit = marklimit;
            this.markpos = this.pos;
        }
    }
    
    @Override
    public boolean markSupported() {
        return true;
    }
    
    @Override
    public InputStream newStream(final long n, final long n2) {
        if (this.in == null) {
            throw new RuntimeException("Stream closed");
        }
        if (n < 0L) {
            throw new IllegalArgumentException("start < 0");
        }
        long datalen = n2;
        if (n2 == -1L) {
            datalen = this.datalen;
        }
        return new SharedFileInputStream(this.sf, this.start + (int)n, (int)(datalen - n), this.bufsize);
    }
    
    @Override
    public int read() throws IOException {
        synchronized (this) {
            this.ensureOpen();
            if (this.pos < this.count) {
                return this.buf[this.pos++] & 0xFF;
            }
            this.fill();
            if (this.pos < this.count) {
                return this.buf[this.pos++] & 0xFF;
            }
            return -1;
            n = (this.buf[this.pos++] & 0xFF);
            return n;
        }
    }
    
    @Override
    public int read(final byte[] array, final int n, final int n2) throws IOException {
        synchronized (this) {
            this.ensureOpen();
            if ((n | n2 | n + n2 | array.length - (n + n2)) < 0) {
                throw new IndexOutOfBoundsException();
            }
        }
        int n3;
        if (n2 == 0) {
            n3 = 0;
        }
        else {
            final byte[] array2;
            int read1 = this.read1(array2, n, n2);
            if ((n3 = read1) > 0) {
                while ((n3 = read1) < n2) {
                    final int read2 = this.read1(array2, n + read1, n2 - read1);
                    n3 = read1;
                    if (read2 <= 0) {
                        break;
                    }
                    read1 += read2;
                }
            }
        }
        // monitorexit(this)
        return n3;
    }
    
    @Override
    public void reset() throws IOException {
        synchronized (this) {
            this.ensureOpen();
            if (this.markpos < 0) {
                throw new IOException("Resetting to invalid mark");
            }
        }
        this.pos = this.markpos;
    }
    // monitorexit(this)
    
    @Override
    public long skip(long n) throws IOException {
    Label_0100_Outer:
        while (true) {
            final long n2 = 0L;
            while (true) {
                while (true) {
                    long n3 = 0L;
                    Label_0103: {
                        synchronized (this) {
                            this.ensureOpen();
                            if (n <= 0L) {
                                n3 = n2;
                            }
                            else {
                                if ((n3 = this.count - this.pos) > 0L) {
                                    break Label_0103;
                                }
                                this.fill();
                                final long n4 = this.count - this.pos;
                                n3 = n2;
                                if (n4 > 0L) {
                                    n3 = n4;
                                    break Label_0103;
                                }
                            }
                            return n3;
                            this.pos += (int)n;
                            n3 = n;
                            return n3;
                        }
                        continue Label_0100_Outer;
                    }
                    if (n3 < n) {
                        n = n3;
                        continue Label_0100_Outer;
                    }
                    break;
                }
                continue;
            }
        }
    }
    
    static class SharedFile
    {
        private int cnt;
        private RandomAccessFile in;
        
        SharedFile(final File file) throws IOException {
            this.in = new RandomAccessFile(file, "r");
        }
        
        SharedFile(final String s) throws IOException {
            this.in = new RandomAccessFile(s, "r");
        }
        
        public void close() throws IOException {
            synchronized (this) {
                if (this.cnt > 0 && --this.cnt <= 0) {
                    this.in.close();
                }
            }
        }
        
        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            this.in.close();
        }
        
        public void forceClose() throws IOException {
            synchronized (this) {
                if (this.cnt > 0) {
                    this.cnt = 0;
                    this.in.close();
                }
                else {
                    try {
                        this.in.close();
                    }
                    catch (IOException ex) {}
                }
            }
        }
        
        public RandomAccessFile open() {
            ++this.cnt;
            return this.in;
        }
    }
}
