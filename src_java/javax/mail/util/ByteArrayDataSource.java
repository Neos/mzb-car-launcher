package javax.mail.util;

import javax.activation.*;
import javax.mail.internet.*;
import java.io.*;

public class ByteArrayDataSource implements DataSource
{
    private byte[] data;
    private int len;
    private String name;
    private String type;
    
    public ByteArrayDataSource(final InputStream inputStream, final String type) throws IOException {
        this.len = -1;
        this.name = "";
        final DSByteArrayOutputStream dsByteArrayOutputStream = new DSByteArrayOutputStream();
        final byte[] array = new byte[8192];
        while (true) {
            final int read = inputStream.read(array);
            if (read <= 0) {
                break;
            }
            dsByteArrayOutputStream.write(array, 0, read);
        }
        this.data = dsByteArrayOutputStream.getBuf();
        this.len = dsByteArrayOutputStream.getCount();
        if (this.data.length - this.len > 262144) {
            this.data = dsByteArrayOutputStream.toByteArray();
            this.len = this.data.length;
        }
        this.type = type;
    }
    
    public ByteArrayDataSource(final String s, final String type) throws IOException {
        this.len = -1;
        this.name = "";
        String parameter = null;
        while (true) {
            try {
                parameter = new ContentType(type).getParameter("charset");
                String defaultJavaCharset = parameter;
                if (parameter == null) {
                    defaultJavaCharset = MimeUtility.getDefaultJavaCharset();
                }
                this.data = s.getBytes(defaultJavaCharset);
                this.type = type;
            }
            catch (ParseException ex) {
                continue;
            }
            break;
        }
    }
    
    public ByteArrayDataSource(final byte[] data, final String type) {
        this.len = -1;
        this.name = "";
        this.data = data;
        this.type = type;
    }
    
    @Override
    public String getContentType() {
        return this.type;
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        if (this.data == null) {
            throw new IOException("no data");
        }
        if (this.len < 0) {
            this.len = this.data.length;
        }
        return new SharedByteArrayInputStream(this.data, 0, this.len);
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new IOException("cannot do this");
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    static class DSByteArrayOutputStream extends ByteArrayOutputStream
    {
        public byte[] getBuf() {
            return this.buf;
        }
        
        public int getCount() {
            return this.count;
        }
    }
}
