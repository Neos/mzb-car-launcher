package javax.mail;

import javax.activation.*;

public interface MultipartDataSource extends DataSource
{
    BodyPart getBodyPart(final int p0) throws MessagingException;
    
    int getCount();
}
