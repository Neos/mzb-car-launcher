package javax.mail;

import java.util.*;

public class FetchProfile
{
    private Vector headers;
    private Vector specials;
    
    public FetchProfile() {
        this.specials = null;
        this.headers = null;
    }
    
    public void add(final String s) {
        if (this.headers == null) {
            this.headers = new Vector();
        }
        this.headers.addElement(s);
    }
    
    public void add(final Item item) {
        if (this.specials == null) {
            this.specials = new Vector();
        }
        this.specials.addElement(item);
    }
    
    public boolean contains(final String s) {
        return this.headers != null && this.headers.contains(s);
    }
    
    public boolean contains(final Item item) {
        return this.specials != null && this.specials.contains(item);
    }
    
    public String[] getHeaderNames() {
        if (this.headers == null) {
            return new String[0];
        }
        final String[] array = new String[this.headers.size()];
        this.headers.copyInto(array);
        return array;
    }
    
    public Item[] getItems() {
        if (this.specials == null) {
            return new Item[0];
        }
        final Item[] array = new Item[this.specials.size()];
        this.specials.copyInto(array);
        return array;
    }
    
    public static class Item
    {
        public static final Item CONTENT_INFO;
        public static final Item ENVELOPE;
        public static final Item FLAGS;
        private String name;
        
        static {
            ENVELOPE = new Item("ENVELOPE");
            CONTENT_INFO = new Item("CONTENT_INFO");
            FLAGS = new Item("FLAGS");
        }
        
        protected Item(final String name) {
            this.name = name;
        }
    }
}
