package javax.mail;

import java.util.*;
import javax.mail.search.*;
import java.io.*;

public abstract class Message implements Part
{
    protected boolean expunged;
    protected Folder folder;
    protected int msgnum;
    protected Session session;
    
    protected Message() {
        this.msgnum = 0;
        this.expunged = false;
        this.folder = null;
        this.session = null;
    }
    
    protected Message(final Folder folder, final int msgnum) {
        this.msgnum = 0;
        this.expunged = false;
        this.folder = null;
        this.session = null;
        this.folder = folder;
        this.msgnum = msgnum;
        this.session = folder.store.session;
    }
    
    protected Message(final Session session) {
        this.msgnum = 0;
        this.expunged = false;
        this.folder = null;
        this.session = null;
        this.session = session;
    }
    
    public abstract void addFrom(final Address[] p0) throws MessagingException;
    
    public void addRecipient(final RecipientType recipientType, final Address address) throws MessagingException {
        this.addRecipients(recipientType, new Address[] { address });
    }
    
    public abstract void addRecipients(final RecipientType p0, final Address[] p1) throws MessagingException;
    
    public Address[] getAllRecipients() throws MessagingException {
        final Address[] recipients = this.getRecipients(RecipientType.TO);
        final Address[] recipients2 = this.getRecipients(RecipientType.CC);
        final Address[] recipients3 = this.getRecipients(RecipientType.BCC);
        if (recipients2 == null && recipients3 == null) {
            return recipients;
        }
        int length;
        if (recipients != null) {
            length = recipients.length;
        }
        else {
            length = 0;
        }
        int length2;
        if (recipients2 != null) {
            length2 = recipients2.length;
        }
        else {
            length2 = 0;
        }
        int length3;
        if (recipients3 != null) {
            length3 = recipients3.length;
        }
        else {
            length3 = 0;
        }
        final Address[] array = new Address[length + length2 + length3];
        int n = 0;
        if (recipients != null) {
            System.arraycopy(recipients, 0, array, 0, recipients.length);
            n = 0 + recipients.length;
        }
        int n2 = n;
        if (recipients2 != null) {
            System.arraycopy(recipients2, 0, array, n, recipients2.length);
            n2 = n + recipients2.length;
        }
        if (recipients3 != null) {
            System.arraycopy(recipients3, 0, array, n2, recipients3.length);
            final int length4 = recipients3.length;
        }
        return array;
    }
    
    public abstract Flags getFlags() throws MessagingException;
    
    public Folder getFolder() {
        return this.folder;
    }
    
    public abstract Address[] getFrom() throws MessagingException;
    
    public int getMessageNumber() {
        return this.msgnum;
    }
    
    public abstract Date getReceivedDate() throws MessagingException;
    
    public abstract Address[] getRecipients(final RecipientType p0) throws MessagingException;
    
    public Address[] getReplyTo() throws MessagingException {
        return this.getFrom();
    }
    
    public abstract Date getSentDate() throws MessagingException;
    
    public abstract String getSubject() throws MessagingException;
    
    public boolean isExpunged() {
        return this.expunged;
    }
    
    public boolean isSet(final Flags.Flag flag) throws MessagingException {
        return this.getFlags().contains(flag);
    }
    
    public boolean match(final SearchTerm searchTerm) throws MessagingException {
        return searchTerm.match(this);
    }
    
    public abstract Message reply(final boolean p0) throws MessagingException;
    
    public abstract void saveChanges() throws MessagingException;
    
    protected void setExpunged(final boolean expunged) {
        this.expunged = expunged;
    }
    
    public void setFlag(final Flags.Flag flag, final boolean b) throws MessagingException {
        this.setFlags(new Flags(flag), b);
    }
    
    public abstract void setFlags(final Flags p0, final boolean p1) throws MessagingException;
    
    public abstract void setFrom() throws MessagingException;
    
    public abstract void setFrom(final Address p0) throws MessagingException;
    
    protected void setMessageNumber(final int msgnum) {
        this.msgnum = msgnum;
    }
    
    public void setRecipient(final RecipientType recipientType, final Address address) throws MessagingException {
        this.setRecipients(recipientType, new Address[] { address });
    }
    
    public abstract void setRecipients(final RecipientType p0, final Address[] p1) throws MessagingException;
    
    public void setReplyTo(final Address[] array) throws MessagingException {
        throw new MethodNotSupportedException("setReplyTo not supported");
    }
    
    public abstract void setSentDate(final Date p0) throws MessagingException;
    
    public abstract void setSubject(final String p0) throws MessagingException;
    
    public static class RecipientType implements Serializable
    {
        public static final RecipientType BCC;
        public static final RecipientType CC;
        public static final RecipientType TO;
        private static final long serialVersionUID = -7479791750606340008L;
        protected String type;
        
        static {
            TO = new RecipientType("To");
            CC = new RecipientType("Cc");
            BCC = new RecipientType("Bcc");
        }
        
        protected RecipientType(final String type) {
            this.type = type;
        }
        
        protected Object readResolve() throws ObjectStreamException {
            if (this.type.equals("To")) {
                return RecipientType.TO;
            }
            if (this.type.equals("Cc")) {
                return RecipientType.CC;
            }
            if (this.type.equals("Bcc")) {
                return RecipientType.BCC;
            }
            throw new InvalidObjectException("Attempt to resolve unknown RecipientType: " + this.type);
        }
        
        @Override
        public String toString() {
            return this.type;
        }
    }
}
