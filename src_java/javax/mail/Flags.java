package javax.mail;

import java.io.*;
import java.util.*;

public class Flags implements Cloneable, Serializable
{
    private static final int ANSWERED_BIT = 1;
    private static final int DELETED_BIT = 2;
    private static final int DRAFT_BIT = 4;
    private static final int FLAGGED_BIT = 8;
    private static final int RECENT_BIT = 16;
    private static final int SEEN_BIT = 32;
    private static final int USER_BIT = Integer.MIN_VALUE;
    private static final long serialVersionUID = 6243590407214169028L;
    private int system_flags;
    private Hashtable user_flags;
    
    public Flags() {
        this.system_flags = 0;
        this.user_flags = null;
    }
    
    public Flags(final String s) {
        this.system_flags = 0;
        this.user_flags = null;
        (this.user_flags = new Hashtable(1)).put(s.toLowerCase(Locale.ENGLISH), s);
    }
    
    public Flags(final Flag flag) {
        this.system_flags = 0;
        this.user_flags = null;
        this.system_flags |= flag.bit;
    }
    
    public Flags(final Flags flags) {
        this.system_flags = 0;
        this.user_flags = null;
        this.system_flags = flags.system_flags;
        if (flags.user_flags != null) {
            this.user_flags = (Hashtable)flags.user_flags.clone();
        }
    }
    
    public void add(final String s) {
        if (this.user_flags == null) {
            this.user_flags = new Hashtable(1);
        }
        this.user_flags.put(s.toLowerCase(Locale.ENGLISH), s);
    }
    
    public void add(final Flag flag) {
        this.system_flags |= flag.bit;
    }
    
    public void add(final Flags flags) {
        this.system_flags |= flags.system_flags;
        if (flags.user_flags != null) {
            if (this.user_flags == null) {
                this.user_flags = new Hashtable(1);
            }
            final Enumeration<String> keys = flags.user_flags.keys();
            while (keys.hasMoreElements()) {
                final String s = keys.nextElement();
                this.user_flags.put(s, flags.user_flags.get(s));
            }
        }
    }
    
    public Object clone() {
        Flags flags = null;
        while (true) {
            try {
                flags = (Flags)super.clone();
                if (this.user_flags != null && flags != null) {
                    flags.user_flags = (Hashtable)this.user_flags.clone();
                }
                return flags;
            }
            catch (CloneNotSupportedException ex) {
                continue;
            }
            break;
        }
    }
    
    public boolean contains(final String s) {
        return this.user_flags != null && this.user_flags.containsKey(s.toLowerCase(Locale.ENGLISH));
    }
    
    public boolean contains(final Flag flag) {
        return (this.system_flags & flag.bit) != 0x0;
    }
    
    public boolean contains(final Flags flags) {
        if ((flags.system_flags & this.system_flags) == flags.system_flags) {
            if (flags.user_flags != null) {
                if (this.user_flags == null) {
                    return false;
                }
                final Enumeration<Object> keys = flags.user_flags.keys();
                while (keys.hasMoreElements()) {
                    if (!this.user_flags.containsKey(keys.nextElement())) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof Flags) {
            final Flags flags = (Flags)o;
            if (flags.system_flags == this.system_flags) {
                if (flags.user_flags == null && this.user_flags == null) {
                    return true;
                }
                if (flags.user_flags != null && this.user_flags != null && flags.user_flags.size() == this.user_flags.size()) {
                    final Enumeration keys = flags.user_flags.keys();
                    while (keys.hasMoreElements()) {
                        if (!this.user_flags.containsKey(keys.nextElement())) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    public Flag[] getSystemFlags() {
        final Vector<Flag> vector = new Vector<Flag>();
        if ((this.system_flags & 0x1) != 0x0) {
            vector.addElement(Flag.ANSWERED);
        }
        if ((this.system_flags & 0x2) != 0x0) {
            vector.addElement(Flag.DELETED);
        }
        if ((this.system_flags & 0x4) != 0x0) {
            vector.addElement(Flag.DRAFT);
        }
        if ((this.system_flags & 0x8) != 0x0) {
            vector.addElement(Flag.FLAGGED);
        }
        if ((this.system_flags & 0x10) != 0x0) {
            vector.addElement(Flag.RECENT);
        }
        if ((this.system_flags & 0x20) != 0x0) {
            vector.addElement(Flag.SEEN);
        }
        if ((this.system_flags & Integer.MIN_VALUE) != 0x0) {
            vector.addElement(Flag.USER);
        }
        final Flag[] array = new Flag[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    public String[] getUserFlags() {
        final Vector<Object> vector = new Vector<Object>();
        if (this.user_flags != null) {
            final Enumeration<Object> elements = this.user_flags.elements();
            while (elements.hasMoreElements()) {
                vector.addElement(elements.nextElement());
            }
        }
        final String[] array = new String[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    @Override
    public int hashCode() {
        int system_flags;
        int n = system_flags = this.system_flags;
        if (this.user_flags != null) {
            final Enumeration<String> keys = this.user_flags.keys();
            while (keys.hasMoreElements()) {
                n += keys.nextElement().hashCode();
            }
            system_flags = n;
        }
        return system_flags;
    }
    
    public void remove(final String s) {
        if (this.user_flags != null) {
            this.user_flags.remove(s.toLowerCase(Locale.ENGLISH));
        }
    }
    
    public void remove(final Flag flag) {
        this.system_flags &= ~flag.bit;
    }
    
    public void remove(final Flags flags) {
        this.system_flags &= ~flags.system_flags;
        if (flags.user_flags != null && this.user_flags != null) {
            final Enumeration<Object> keys = flags.user_flags.keys();
            while (keys.hasMoreElements()) {
                this.user_flags.remove(keys.nextElement());
            }
        }
    }
    
    public static final class Flag
    {
        public static final Flag ANSWERED;
        public static final Flag DELETED;
        public static final Flag DRAFT;
        public static final Flag FLAGGED;
        public static final Flag RECENT;
        public static final Flag SEEN;
        public static final Flag USER;
        private int bit;
        
        static {
            ANSWERED = new Flag(1);
            DELETED = new Flag(2);
            DRAFT = new Flag(4);
            FLAGGED = new Flag(8);
            RECENT = new Flag(16);
            SEEN = new Flag(32);
            USER = new Flag(Integer.MIN_VALUE);
        }
        
        private Flag(final int bit) {
            this.bit = bit;
        }
    }
}
