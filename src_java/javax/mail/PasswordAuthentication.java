package javax.mail;

public final class PasswordAuthentication
{
    private String password;
    private String userName;
    
    public PasswordAuthentication(final String userName, final String password) {
        this.userName = userName;
        this.password = password;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public String getUserName() {
        return this.userName;
    }
}
