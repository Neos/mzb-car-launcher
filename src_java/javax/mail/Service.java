package javax.mail;

import java.util.*;
import javax.mail.event.*;

public abstract class Service
{
    private boolean connected;
    private Vector connectionListeners;
    protected boolean debug;
    private EventQueue q;
    private Object qLock;
    protected Session session;
    protected URLName url;
    
    protected Service(final Session session, final URLName url) {
        this.url = null;
        this.debug = false;
        this.connected = false;
        this.connectionListeners = null;
        this.qLock = new Object();
        this.session = session;
        this.url = url;
        this.debug = session.getDebug();
    }
    
    private void terminateQueue() {
        synchronized (this.qLock) {
            if (this.q != null) {
                final Vector vector = new Vector();
                vector.setSize(1);
                this.q.enqueue(new TerminatorEvent(), vector);
                this.q = null;
            }
        }
    }
    
    public void addConnectionListener(final ConnectionListener connectionListener) {
        synchronized (this) {
            if (this.connectionListeners == null) {
                this.connectionListeners = new Vector();
            }
            this.connectionListeners.addElement(connectionListener);
        }
    }
    
    public void close() throws MessagingException {
        synchronized (this) {
            this.setConnected(false);
            this.notifyConnectionListeners(3);
        }
    }
    
    public void connect() throws MessagingException {
        this.connect(null, null, null);
    }
    
    public void connect(final String p0, final int p1, final String p2, final String p3) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokevirtual   javax/mail/Service.isConnected:()Z
        //     6: ifeq            24
        //     9: new             Ljava/lang/IllegalStateException;
        //    12: dup            
        //    13: ldc             "already connected"
        //    15: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //    18: athrow         
        //    19: astore_1       
        //    20: aload_0        
        //    21: monitorexit    
        //    22: aload_1        
        //    23: athrow         
        //    24: iconst_0       
        //    25: istore          7
        //    27: iconst_0       
        //    28: istore          6
        //    30: aconst_null    
        //    31: astore          12
        //    33: aconst_null    
        //    34: astore          13
        //    36: aload_1        
        //    37: astore          9
        //    39: iload_2        
        //    40: istore          5
        //    42: aload_3        
        //    43: astore          11
        //    45: aload           4
        //    47: astore          10
        //    49: aload_0        
        //    50: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //    53: ifnull          144
        //    56: aload_0        
        //    57: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //    60: invokevirtual   javax/mail/URLName.getProtocol:()Ljava/lang/String;
        //    63: astore          12
        //    65: aload_1        
        //    66: astore          9
        //    68: aload_1        
        //    69: ifnonnull       81
        //    72: aload_0        
        //    73: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //    76: invokevirtual   javax/mail/URLName.getHost:()Ljava/lang/String;
        //    79: astore          9
        //    81: iload_2        
        //    82: istore          5
        //    84: iload_2        
        //    85: iconst_m1      
        //    86: if_icmpne       98
        //    89: aload_0        
        //    90: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //    93: invokevirtual   javax/mail/URLName.getPort:()I
        //    96: istore          5
        //    98: aload_3        
        //    99: ifnonnull       492
        //   102: aload_0        
        //   103: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //   106: invokevirtual   javax/mail/URLName.getUsername:()Ljava/lang/String;
        //   109: astore_3       
        //   110: aload_3        
        //   111: astore          11
        //   113: aload           4
        //   115: astore_1       
        //   116: aload           4
        //   118: ifnonnull       132
        //   121: aload_0        
        //   122: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //   125: invokevirtual   javax/mail/URLName.getPassword:()Ljava/lang/String;
        //   128: astore_1       
        //   129: aload_3        
        //   130: astore          11
        //   132: aload_0        
        //   133: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //   136: invokevirtual   javax/mail/URLName.getFile:()Ljava/lang/String;
        //   139: astore          13
        //   141: aload_1        
        //   142: astore          10
        //   144: aload           9
        //   146: astore          4
        //   148: aload           11
        //   150: astore_1       
        //   151: aload           12
        //   153: ifnull          238
        //   156: aload           9
        //   158: astore_3       
        //   159: aload           9
        //   161: ifnonnull       194
        //   164: aload_0        
        //   165: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   168: new             Ljava/lang/StringBuilder;
        //   171: dup            
        //   172: ldc             "mail."
        //   174: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   177: aload           12
        //   179: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   182: ldc             ".host"
        //   184: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   187: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   190: invokevirtual   javax/mail/Session.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   193: astore_3       
        //   194: aload_3        
        //   195: astore          4
        //   197: aload           11
        //   199: astore_1       
        //   200: aload           11
        //   202: ifnonnull       238
        //   205: aload_0        
        //   206: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   209: new             Ljava/lang/StringBuilder;
        //   212: dup            
        //   213: ldc             "mail."
        //   215: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   218: aload           12
        //   220: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   223: ldc             ".user"
        //   225: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   228: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   231: invokevirtual   javax/mail/Session.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   234: astore_1       
        //   235: aload_3        
        //   236: astore          4
        //   238: aload           4
        //   240: astore          9
        //   242: aload           4
        //   244: ifnonnull       258
        //   247: aload_0        
        //   248: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   251: ldc             "mail.host"
        //   253: invokevirtual   javax/mail/Session.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   256: astore          9
        //   258: aload_1        
        //   259: astore_3       
        //   260: aload_1        
        //   261: ifnonnull       274
        //   264: aload_0        
        //   265: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   268: ldc             "mail.user"
        //   270: invokevirtual   javax/mail/Session.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   273: astore_3       
        //   274: aload_3        
        //   275: astore          4
        //   277: aload_3        
        //   278: ifnonnull       288
        //   281: ldc             "user.name"
        //   283: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   286: astore          4
        //   288: iload           6
        //   290: istore_2       
        //   291: aload           4
        //   293: astore_3       
        //   294: aload           10
        //   296: astore_1       
        //   297: aload           10
        //   299: ifnonnull       378
        //   302: iload           6
        //   304: istore_2       
        //   305: aload           4
        //   307: astore_3       
        //   308: aload           10
        //   310: astore_1       
        //   311: aload_0        
        //   312: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //   315: ifnull          378
        //   318: aload_0        
        //   319: new             Ljavax/mail/URLName;
        //   322: dup            
        //   323: aload           12
        //   325: aload           9
        //   327: iload           5
        //   329: aload           13
        //   331: aload           4
        //   333: aconst_null    
        //   334: invokespecial   javax/mail/URLName.<init>:(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   337: invokevirtual   javax/mail/Service.setURLName:(Ljavax/mail/URLName;)V
        //   340: aload_0        
        //   341: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   344: aload_0        
        //   345: invokevirtual   javax/mail/Service.getURLName:()Ljavax/mail/URLName;
        //   348: invokevirtual   javax/mail/Session.getPasswordAuthentication:(Ljavax/mail/URLName;)Ljavax/mail/PasswordAuthentication;
        //   351: astore          11
        //   353: aload           11
        //   355: ifnull          672
        //   358: aload           4
        //   360: ifnonnull       565
        //   363: aload           11
        //   365: invokevirtual   javax/mail/PasswordAuthentication.getUserName:()Ljava/lang/String;
        //   368: astore_3       
        //   369: aload           11
        //   371: invokevirtual   javax/mail/PasswordAuthentication.getPassword:()Ljava/lang/String;
        //   374: astore_1       
        //   375: iload           6
        //   377: istore_2       
        //   378: aconst_null    
        //   379: astore          4
        //   381: aload_0        
        //   382: aload           9
        //   384: iload           5
        //   386: aload_3        
        //   387: aload_1        
        //   388: invokevirtual   javax/mail/Service.protocolConnect:(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z
        //   391: istore          8
        //   393: iload           8
        //   395: istore          7
        //   397: iload           7
        //   399: istore          8
        //   401: aload_3        
        //   402: astore          11
        //   404: aload_1        
        //   405: astore          10
        //   407: iload           7
        //   409: ifne            479
        //   412: aload           9
        //   414: invokestatic    java/net/InetAddress.getByName:(Ljava/lang/String;)Ljava/net/InetAddress;
        //   417: astore          10
        //   419: aload_0        
        //   420: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   423: aload           10
        //   425: iload           5
        //   427: aload           12
        //   429: aconst_null    
        //   430: aload_3        
        //   431: invokevirtual   javax/mail/Session.requestPasswordAuthentication:(Ljava/net/InetAddress;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljavax/mail/PasswordAuthentication;
        //   434: astore          14
        //   436: iload           7
        //   438: istore          8
        //   440: aload_3        
        //   441: astore          11
        //   443: aload_1        
        //   444: astore          10
        //   446: aload           14
        //   448: ifnull          479
        //   451: aload           14
        //   453: invokevirtual   javax/mail/PasswordAuthentication.getUserName:()Ljava/lang/String;
        //   456: astore          11
        //   458: aload           14
        //   460: invokevirtual   javax/mail/PasswordAuthentication.getPassword:()Ljava/lang/String;
        //   463: astore          10
        //   465: aload_0        
        //   466: aload           9
        //   468: iload           5
        //   470: aload           11
        //   472: aload           10
        //   474: invokevirtual   javax/mail/Service.protocolConnect:(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z
        //   477: istore          8
        //   479: iload           8
        //   481: ifne            610
        //   484: aload           4
        //   486: ifnull          602
        //   489: aload           4
        //   491: athrow         
        //   492: aload_3        
        //   493: astore          11
        //   495: aload           4
        //   497: astore_1       
        //   498: aload           4
        //   500: ifnonnull       132
        //   503: aload_3        
        //   504: astore          11
        //   506: aload           4
        //   508: astore_1       
        //   509: aload_3        
        //   510: aload_0        
        //   511: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //   514: invokevirtual   javax/mail/URLName.getUsername:()Ljava/lang/String;
        //   517: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   520: ifeq            132
        //   523: aload_0        
        //   524: getfield        javax/mail/Service.url:Ljavax/mail/URLName;
        //   527: invokevirtual   javax/mail/URLName.getPassword:()Ljava/lang/String;
        //   530: astore_1       
        //   531: aload_3        
        //   532: astore          11
        //   534: goto            132
        //   537: astore_1       
        //   538: aload_3        
        //   539: astore          4
        //   541: aload_0        
        //   542: getfield        javax/mail/Service.debug:Z
        //   545: ifeq            288
        //   548: aload_1        
        //   549: aload_0        
        //   550: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   553: invokevirtual   javax/mail/Session.getDebugOut:()Ljava/io/PrintStream;
        //   556: invokevirtual   java/lang/SecurityException.printStackTrace:(Ljava/io/PrintStream;)V
        //   559: aload_3        
        //   560: astore          4
        //   562: goto            288
        //   565: iload           6
        //   567: istore_2       
        //   568: aload           4
        //   570: astore_3       
        //   571: aload           10
        //   573: astore_1       
        //   574: aload           4
        //   576: aload           11
        //   578: invokevirtual   javax/mail/PasswordAuthentication.getUserName:()Ljava/lang/String;
        //   581: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   584: ifeq            378
        //   587: aload           11
        //   589: invokevirtual   javax/mail/PasswordAuthentication.getPassword:()Ljava/lang/String;
        //   592: astore_1       
        //   593: iload           6
        //   595: istore_2       
        //   596: aload           4
        //   598: astore_3       
        //   599: goto            378
        //   602: new             Ljavax/mail/AuthenticationFailedException;
        //   605: dup            
        //   606: invokespecial   javax/mail/AuthenticationFailedException.<init>:()V
        //   609: athrow         
        //   610: aload_0        
        //   611: new             Ljavax/mail/URLName;
        //   614: dup            
        //   615: aload           12
        //   617: aload           9
        //   619: iload           5
        //   621: aload           13
        //   623: aload           11
        //   625: aload           10
        //   627: invokespecial   javax/mail/URLName.<init>:(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   630: invokevirtual   javax/mail/Service.setURLName:(Ljavax/mail/URLName;)V
        //   633: iload_2        
        //   634: ifeq            659
        //   637: aload_0        
        //   638: getfield        javax/mail/Service.session:Ljavax/mail/Session;
        //   641: aload_0        
        //   642: invokevirtual   javax/mail/Service.getURLName:()Ljavax/mail/URLName;
        //   645: new             Ljavax/mail/PasswordAuthentication;
        //   648: dup            
        //   649: aload           11
        //   651: aload           10
        //   653: invokespecial   javax/mail/PasswordAuthentication.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   656: invokevirtual   javax/mail/Session.setPasswordAuthentication:(Ljavax/mail/URLName;Ljavax/mail/PasswordAuthentication;)V
        //   659: aload_0        
        //   660: iconst_1       
        //   661: invokevirtual   javax/mail/Service.setConnected:(Z)V
        //   664: aload_0        
        //   665: iconst_1       
        //   666: invokevirtual   javax/mail/Service.notifyConnectionListeners:(I)V
        //   669: aload_0        
        //   670: monitorexit    
        //   671: return         
        //   672: iconst_1       
        //   673: istore_2       
        //   674: aload           4
        //   676: astore_3       
        //   677: aload           10
        //   679: astore_1       
        //   680: goto            378
        //   683: astore          4
        //   685: goto            397
        //   688: astore          10
        //   690: aconst_null    
        //   691: astore          10
        //   693: goto            419
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                      
        //  -----  -----  -----  -----  ------------------------------------------
        //  2      19     19     24     Any
        //  49     65     19     24     Any
        //  72     81     19     24     Any
        //  89     98     19     24     Any
        //  102    110    19     24     Any
        //  121    129    19     24     Any
        //  132    141    19     24     Any
        //  164    194    19     24     Any
        //  205    235    19     24     Any
        //  247    258    19     24     Any
        //  264    274    19     24     Any
        //  281    288    537    565    Ljava/lang/SecurityException;
        //  281    288    19     24     Any
        //  311    353    19     24     Any
        //  363    375    19     24     Any
        //  381    393    683    688    Ljavax/mail/AuthenticationFailedException;
        //  381    393    19     24     Any
        //  412    419    688    696    Ljava/net/UnknownHostException;
        //  412    419    19     24     Any
        //  419    436    19     24     Any
        //  451    479    19     24     Any
        //  489    492    19     24     Any
        //  509    531    19     24     Any
        //  541    559    19     24     Any
        //  574    593    19     24     Any
        //  602    610    19     24     Any
        //  610    633    19     24     Any
        //  637    659    19     24     Any
        //  659    669    19     24     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 357, Size: 357
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void connect(final String s, final String s2) throws MessagingException {
        this.connect(null, s, s2);
    }
    
    public void connect(final String s, final String s2, final String s3) throws MessagingException {
        this.connect(s, -1, s2, s3);
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.terminateQueue();
    }
    
    public URLName getURLName() {
        synchronized (this) {
            URLName url;
            if (this.url != null && (this.url.getPassword() != null || this.url.getFile() != null)) {
                url = new URLName(this.url.getProtocol(), this.url.getHost(), this.url.getPort(), null, this.url.getUsername(), null);
            }
            else {
                url = this.url;
            }
            return url;
        }
    }
    
    public boolean isConnected() {
        synchronized (this) {
            return this.connected;
        }
    }
    
    protected void notifyConnectionListeners(final int n) {
        synchronized (this) {
            if (this.connectionListeners != null) {
                this.queueEvent(new ConnectionEvent(this, n), this.connectionListeners);
            }
            if (n == 3) {
                this.terminateQueue();
            }
        }
    }
    
    protected boolean protocolConnect(final String s, final int n, final String s2, final String s3) throws MessagingException {
        return false;
    }
    
    protected void queueEvent(final MailEvent mailEvent, Vector vector) {
        synchronized (this.qLock) {
            if (this.q == null) {
                this.q = new EventQueue();
            }
            // monitorexit(this.qLock)
            vector = (Vector)vector.clone();
            this.q.enqueue(mailEvent, vector);
        }
    }
    
    public void removeConnectionListener(final ConnectionListener connectionListener) {
        synchronized (this) {
            if (this.connectionListeners != null) {
                this.connectionListeners.removeElement(connectionListener);
            }
        }
    }
    
    protected void setConnected(final boolean connected) {
        synchronized (this) {
            this.connected = connected;
        }
    }
    
    protected void setURLName(final URLName url) {
        synchronized (this) {
            this.url = url;
        }
    }
    
    @Override
    public String toString() {
        final URLName urlName = this.getURLName();
        if (urlName != null) {
            return urlName.toString();
        }
        return super.toString();
    }
    
    static class TerminatorEvent extends MailEvent
    {
        private static final long serialVersionUID = 5542172141759168416L;
        
        TerminatorEvent() {
            super(new Object());
        }
        
        @Override
        public void dispatch(final Object o) {
            Thread.currentThread().interrupt();
        }
    }
}
