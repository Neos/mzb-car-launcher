package javax.mail;

public interface UIDFolder
{
    public static final long LASTUID = -1L;
    
    Message getMessageByUID(final long p0) throws MessagingException;
    
    Message[] getMessagesByUID(final long p0, final long p1) throws MessagingException;
    
    Message[] getMessagesByUID(final long[] p0) throws MessagingException;
    
    long getUID(final Message p0) throws MessagingException;
    
    long getUIDValidity() throws MessagingException;
    
    public static class FetchProfileItem extends Item
    {
        public static final FetchProfileItem UID;
        
        static {
            UID = new FetchProfileItem("UID");
        }
        
        protected FetchProfileItem(final String s) {
            super(s);
        }
    }
}
