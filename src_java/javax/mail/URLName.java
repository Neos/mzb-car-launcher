package javax.mail;

import java.io.*;
import java.net.*;
import java.util.*;

public class URLName
{
    static final int caseDiff = 32;
    private static boolean doEncode;
    static BitSet dontNeedEncoding;
    private String file;
    protected String fullURL;
    private int hashCode;
    private String host;
    private InetAddress hostAddress;
    private boolean hostAddressKnown;
    private String password;
    private int port;
    private String protocol;
    private String ref;
    private String username;
    
    static {
        boolean doEncode = true;
        URLName.doEncode = true;
        while (true) {
            try {
                if (Boolean.getBoolean("mail.URLName.dontencode")) {
                    doEncode = false;
                }
                URLName.doEncode = doEncode;
                URLName.dontNeedEncoding = new BitSet(256);
                for (int i = 97; i <= 122; ++i) {
                    URLName.dontNeedEncoding.set(i);
                }
                for (int j = 65; j <= 90; ++j) {
                    URLName.dontNeedEncoding.set(j);
                }
                for (int k = 48; k <= 57; ++k) {
                    URLName.dontNeedEncoding.set(k);
                }
                URLName.dontNeedEncoding.set(32);
                URLName.dontNeedEncoding.set(45);
                URLName.dontNeedEncoding.set(95);
                URLName.dontNeedEncoding.set(46);
                URLName.dontNeedEncoding.set(42);
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
    
    public URLName(final String s) {
        this.hostAddressKnown = false;
        this.port = -1;
        this.hashCode = 0;
        this.parseString(s);
    }
    
    public URLName(String password, final String host, int index, final String file, final String s, final String s2) {
        this.hostAddressKnown = false;
        this.port = -1;
        this.hashCode = 0;
        this.protocol = password;
        this.host = host;
        this.port = index;
        while (true) {
            Label_0116: {
                if (file == null) {
                    break Label_0116;
                }
                index = file.indexOf(35);
                if (index == -1) {
                    break Label_0116;
                }
                this.file = file.substring(0, index);
                this.ref = file.substring(index + 1);
                password = s;
                if (URLName.doEncode) {
                    password = encode(s);
                }
                this.username = password;
                password = s2;
                if (URLName.doEncode) {
                    password = encode(s2);
                }
                this.password = password;
                return;
            }
            this.file = file;
            this.ref = null;
            continue;
        }
    }
    
    public URLName(final URL url) {
        this(url.toString());
    }
    
    private static String _encode(final String s) {
        final StringBuffer sb = new StringBuffer(s.length());
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(10);
        final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (URLName.dontNeedEncoding.get(char1)) {
                int n;
                if ((n = char1) == 32) {
                    n = 43;
                }
                sb.append((char)n);
            }
            else {
                while (true) {
                    while (true) {
                        byte[] byteArray = null;
                        int n2 = 0;
                        Label_0148: {
                            try {
                                outputStreamWriter.write(char1);
                                outputStreamWriter.flush();
                                byteArray = byteArrayOutputStream.toByteArray();
                                n2 = 0;
                                if (n2 < byteArray.length) {
                                    break Label_0148;
                                }
                                byteArrayOutputStream.reset();
                            }
                            catch (IOException ex) {
                                byteArrayOutputStream.reset();
                            }
                            break;
                        }
                        sb.append('%');
                        char forDigit;
                        final char c = forDigit = Character.forDigit(byteArray[n2] >> 4 & 0xF, 16);
                        if (Character.isLetter(c)) {
                            forDigit = (char)(c - ' ');
                        }
                        sb.append(forDigit);
                        char forDigit2;
                        final char c2 = forDigit2 = Character.forDigit(byteArray[n2] & 0xF, 16);
                        if (Character.isLetter(c2)) {
                            forDigit2 = (char)(c2 - ' ');
                        }
                        sb.append(forDigit2);
                        ++n2;
                        continue;
                    }
                }
            }
        }
        return sb.toString();
    }
    
    static String decode(String string) {
        Object o;
        if (string == null) {
            o = null;
        }
        else {
            o = string;
            if (indexOfAny((String)string, "+%") != -1) {
                final StringBuffer sb = new StringBuffer();
                int n = 0;
            Label_0102_Outer:
                while (true) {
                    Label_0061: {
                        if (n < ((String)string).length()) {
                            break Label_0061;
                        }
                        string = sb.toString();
                        try {
                            string = new String(((String)string).getBytes("8859_1"));
                            return (String)string;
                            while (true) {
                                ++n;
                                continue Label_0102_Outer;
                                Label_0109: {
                                    sb.append(' ');
                                }
                                continue;
                                try {
                                    Label_0119:
                                    sb.append((char)Integer.parseInt(((String)string).substring(n + 1, n + 3), 16));
                                    n += 2;
                                }
                                catch (NumberFormatException string) {
                                    throw new IllegalArgumentException();
                                }
                                final char char1 = ((String)string).charAt(n);
                                Label_0096:
                                sb.append(char1);
                                continue;
                            }
                        }
                        // switch([Lcom.strobel.decompiler.ast.Label;@7b60f4b1, char1)
                        catch (UnsupportedEncodingException ex) {
                            return (String)string;
                        }
                    }
                }
            }
        }
        return (String)o;
    }
    
    static String encode(final String s) {
        if (s != null) {
            int n = 0;
            while (true) {
                final String s2 = s;
                if (n >= s.length()) {
                    return s2;
                }
                final char char1 = s.charAt(n);
                if (char1 == ' ' || !URLName.dontNeedEncoding.get(char1)) {
                    break;
                }
                ++n;
            }
            return _encode(s);
        }
        return null;
    }
    
    private InetAddress getHostAddress() {
        InetAddress inetAddress = null;
        synchronized (this) {
            if (this.hostAddressKnown) {
                inetAddress = this.hostAddress;
            }
            else if (this.host != null) {
                try {
                    this.hostAddress = InetAddress.getByName(this.host);
                    this.hostAddressKnown = true;
                    inetAddress = this.hostAddress;
                }
                catch (UnknownHostException ex) {
                    this.hostAddress = null;
                }
            }
            return inetAddress;
        }
    }
    
    private static int indexOfAny(final String s, final String s2) {
        return indexOfAny(s, s2, 0);
    }
    
    private static int indexOfAny(final String s, final String s2, int i) {
        Block_0: {
            break Block_0;
        Label_0037:
            while (true) {
                int length;
                do {
                    Label_0009: {
                        break Label_0009;
                        try {
                            length = s.length();
                            continue Label_0037;
                            final int index = s2.indexOf(s.charAt(i));
                            final int n = i;
                            // iftrue(Label_0045:, index >= 0)
                            ++i;
                            continue Label_0037;
                        }
                        catch (StringIndexOutOfBoundsException ex) {
                            return -1;
                        }
                    }
                    continue Label_0037;
                    Label_0045: {
                        return;
                    }
                } while (i < length);
                break;
            }
        }
        return -1;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof URLName) {
            final URLName urlName = (URLName)o;
            if (urlName.protocol != null && urlName.protocol.equals(this.protocol)) {
                final InetAddress hostAddress = this.getHostAddress();
                final InetAddress hostAddress2 = urlName.getHostAddress();
                if (hostAddress != null && hostAddress2 != null) {
                    if (!hostAddress.equals(hostAddress2)) {
                        return false;
                    }
                }
                else if (this.host != null && urlName.host != null) {
                    if (!this.host.equalsIgnoreCase(urlName.host)) {
                        return false;
                    }
                }
                else if (this.host != urlName.host) {
                    return false;
                }
                if (this.username == urlName.username || (this.username != null && this.username.equals(urlName.username))) {
                    String file;
                    if (this.file == null) {
                        file = "";
                    }
                    else {
                        file = this.file;
                    }
                    String file2;
                    if (urlName.file == null) {
                        file2 = "";
                    }
                    else {
                        file2 = urlName.file;
                    }
                    if (file.equals(file2) && this.port == urlName.port) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public String getFile() {
        return this.file;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public String getPassword() {
        if (URLName.doEncode) {
            return decode(this.password);
        }
        return this.password;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public String getRef() {
        return this.ref;
    }
    
    public URL getURL() throws MalformedURLException {
        return new URL(this.getProtocol(), this.getHost(), this.getPort(), this.getFile());
    }
    
    public String getUsername() {
        if (URLName.doEncode) {
            return decode(this.username);
        }
        return this.username;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode != 0) {
            return this.hashCode;
        }
        if (this.protocol != null) {
            this.hashCode += this.protocol.hashCode();
        }
        final InetAddress hostAddress = this.getHostAddress();
        if (hostAddress != null) {
            this.hashCode += hostAddress.hashCode();
        }
        else if (this.host != null) {
            this.hashCode += this.host.toLowerCase(Locale.ENGLISH).hashCode();
        }
        if (this.username != null) {
            this.hashCode += this.username.hashCode();
        }
        if (this.file != null) {
            this.hashCode += this.file.hashCode();
        }
        return this.hashCode += this.port;
    }
    
    protected void parseString(String s) {
        this.password = null;
        this.username = null;
        this.host = null;
        this.ref = null;
        this.file = null;
        this.protocol = null;
        this.port = -1;
        int n = s.length();
        final int index = s.indexOf(58);
        if (index != -1) {
            this.protocol = s.substring(0, index);
        }
        while (true) {
            String host = null;
            Label_0320: {
                if (s.regionMatches(index + 1, "//", 0, 2)) {
                    final int index2 = s.indexOf(47, index + 3);
                    if (index2 == -1) {
                        break Label_0320;
                    }
                    host = s.substring(index + 3, index2);
                    if (index2 + 1 >= n) {
                        break Label_0320;
                    }
                    this.file = s.substring(index2 + 1);
                    s = host;
                }
                else {
                    if (index + 1 < n) {
                        this.file = s.substring(index + 1);
                        break Label_0270;
                    }
                    break Label_0270;
                }
            Label_0196_Outer:
                while (true) {
                    n = s.indexOf(64);
                    host = s;
                    Label_0343: {
                        if (n != -1) {
                            final String substring = s.substring(0, n);
                            host = s.substring(n + 1);
                            n = substring.indexOf(58);
                            if (n == -1) {
                                break Label_0343;
                            }
                            this.username = substring.substring(0, n);
                            this.password = substring.substring(n + 1);
                        }
                    Label_0230_Outer:
                        while (true) {
                            Label_0352: {
                                if (host.length() <= 0 || host.charAt(0) != '[') {
                                    break Label_0352;
                                }
                                n = host.indexOf(58, host.indexOf(93));
                            Label_0259_Outer:
                                while (true) {
                                    if (n == -1) {
                                        break Label_0320;
                                    }
                                    s = host.substring(n + 1);
                                    while (true) {
                                        if (s.length() <= 0) {
                                            break Label_0259;
                                        }
                                        try {
                                            this.port = Integer.parseInt(s);
                                            this.host = host.substring(0, n);
                                            if (this.file != null) {
                                                n = this.file.indexOf(35);
                                                if (n != -1) {
                                                    this.ref = this.file.substring(n + 1);
                                                    this.file = this.file.substring(0, n);
                                                }
                                            }
                                            return;
                                            n = host.indexOf(58);
                                            continue Label_0259_Outer;
                                            this.file = "";
                                            s = host;
                                            continue Label_0196_Outer;
                                            final String substring;
                                            this.username = substring;
                                            continue Label_0230_Outer;
                                            s = s.substring(index + 3);
                                            continue Label_0196_Outer;
                                        }
                                        catch (NumberFormatException ex) {
                                            this.port = -1;
                                            continue;
                                        }
                                        break;
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            this.host = host;
            continue;
        }
    }
    
    @Override
    public String toString() {
        if (this.fullURL == null) {
            final StringBuffer sb = new StringBuffer();
            if (this.protocol != null) {
                sb.append(this.protocol);
                sb.append(":");
            }
            if (this.username != null || this.host != null) {
                sb.append("//");
                if (this.username != null) {
                    sb.append(this.username);
                    if (this.password != null) {
                        sb.append(":");
                        sb.append(this.password);
                    }
                    sb.append("@");
                }
                if (this.host != null) {
                    sb.append(this.host);
                }
                if (this.port != -1) {
                    sb.append(":");
                    sb.append(Integer.toString(this.port));
                }
                if (this.file != null) {
                    sb.append("/");
                }
            }
            if (this.file != null) {
                sb.append(this.file);
            }
            if (this.ref != null) {
                sb.append("#");
                sb.append(this.ref);
            }
            this.fullURL = sb.toString();
        }
        return this.fullURL;
    }
}
