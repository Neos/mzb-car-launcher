package javax.mail.internet;

import java.text.*;

class MailDateParser
{
    int index;
    char[] orig;
    
    public MailDateParser(final char[] orig) {
        this.index = 0;
        this.orig = null;
        this.orig = orig;
    }
    
    int getIndex() {
        return this.index;
    }
    
    public int parseAlphaTimeZone() throws ParseException {
        int n;
        int n2;
        while (true) {
            n = 0;
            Label_0421: {
                Label_0412: {
                    Label_0403: {
                        Label_0394: {
                            Label_0307: {
                                Label_0162: {
                                    while (true) {
                                        Label_0500: {
                                            try {
                                                switch (this.orig[this.index++]) {
                                                    case 'U':
                                                    case 'u': {
                                                        break Label_0162;
                                                    }
                                                    case 'G':
                                                    case 'g': {
                                                        break Label_0307;
                                                    }
                                                    case 'E':
                                                    case 'e': {
                                                        break Label_0394;
                                                    }
                                                    case 'C':
                                                    case 'c': {
                                                        break Label_0403;
                                                    }
                                                    case 'M':
                                                    case 'm': {
                                                        break Label_0412;
                                                    }
                                                    case 'P':
                                                    case 'p': {
                                                        break Label_0421;
                                                    }
                                                    default: {
                                                        break Label_0500;
                                                    }
                                                }
                                                throw new ParseException("Bad Alpha TimeZone", this.index);
                                            }
                                            catch (ArrayIndexOutOfBoundsException ex) {
                                                throw new ParseException("Bad Alpha TimeZone", this.index);
                                            }
                                            break Label_0162;
                                        }
                                        continue;
                                    }
                                }
                                final char c = this.orig[this.index++];
                                if (c == 'T' || c == 't') {
                                    n2 = 0;
                                    break;
                                }
                                throw new ParseException("Bad Alpha TimeZone", this.index);
                            }
                            final char c2 = this.orig[this.index++];
                            if (c2 == 'M' || c2 == 'm') {
                                final char c3 = this.orig[this.index++];
                                if (c3 == 'T' || c3 == 't') {
                                    n2 = 0;
                                    break;
                                }
                            }
                            throw new ParseException("Bad Alpha TimeZone", this.index);
                        }
                        n2 = 300;
                        n = 1;
                        break;
                    }
                    n2 = 360;
                    n = 1;
                    break;
                }
                n2 = 420;
                n = 1;
                break;
            }
            n2 = 480;
            n = 1;
            break;
        }
        int n3 = n2;
        if (n != 0) {
            final char c4 = this.orig[this.index++];
            if (c4 == 'S' || c4 == 's') {
                final char c5 = this.orig[this.index++];
                n3 = n2;
                if (c5 != 'T') {
                    n3 = n2;
                    if (c5 != 't') {
                        throw new ParseException("Bad Alpha TimeZone", this.index);
                    }
                }
            }
            else {
                if (c4 != 'D') {
                    n3 = n2;
                    if (c4 != 'd') {
                        return n3;
                    }
                }
                final char c6 = this.orig[this.index++];
                if (c6 != 'T' && c6 == 't') {
                    throw new ParseException("Bad Alpha TimeZone", this.index);
                }
                n3 = n2 - 60;
            }
        }
        return n3;
    }
    
    public int parseMonth() throws ParseException {
    Label_0515_Outer:
        while (true) {
            while (true) {
                while (true) {
                    char c7 = '\0';
                    Label_0873: {
                        char c6 = '\0';
                        Label_0857: {
                            char c2 = '\0';
                            Label_0838: {
                                Label_0831: {
                                    try {
                                        switch (this.orig[this.index++]) {
                                            case 'J':
                                            case 'j': {
                                                switch (this.orig[this.index++]) {
                                                    case 'A':
                                                    case 'a': {
                                                        final char c = this.orig[this.index++];
                                                        if (c != 'N' && c != 'n') {
                                                            break Label_0160;
                                                        }
                                                        return 0;
                                                    }
                                                    case 'U':
                                                    case 'u': {
                                                        c2 = this.orig[this.index++];
                                                        if (c2 != 'N' && c2 != 'n') {
                                                            break Label_0838;
                                                        }
                                                        return 5;
                                                    }
                                                    default: {
                                                        break Label_0831;
                                                    }
                                                }
                                                break;
                                            }
                                            case 'F':
                                            case 'f': {
                                                final char c3 = this.orig[this.index++];
                                                if (c3 != 'E' && c3 != 'e') {
                                                    break;
                                                }
                                                final char c4 = this.orig[this.index++];
                                                if (c4 != 'B' && c4 != 'b') {
                                                    break;
                                                }
                                                return 1;
                                            }
                                            case 'M':
                                            case 'm': {
                                                final char c5 = this.orig[this.index++];
                                                if (c5 != 'A' && c5 != 'a') {
                                                    break;
                                                }
                                                c6 = this.orig[this.index++];
                                                if (c6 != 'R' && c6 != 'r') {
                                                    break Label_0857;
                                                }
                                                return 2;
                                            }
                                            case 'A':
                                            case 'a': {
                                                c7 = this.orig[this.index++];
                                                if (c7 != 'P' && c7 != 'p') {
                                                    break Label_0873;
                                                }
                                                final char c8 = this.orig[this.index++];
                                                if (c8 != 'R' && c8 != 'r') {
                                                    break;
                                                }
                                                return 3;
                                            }
                                            case 'S':
                                            case 's': {
                                                final char c9 = this.orig[this.index++];
                                                if (c9 != 'E' && c9 != 'e') {
                                                    break;
                                                }
                                                final char c10 = this.orig[this.index++];
                                                if (c10 != 'P' && c10 != 'p') {
                                                    break;
                                                }
                                                return 8;
                                            }
                                            case 'O':
                                            case 'o': {
                                                final char c11 = this.orig[this.index++];
                                                if (c11 != 'C' && c11 != 'c') {
                                                    break;
                                                }
                                                final char c12 = this.orig[this.index++];
                                                if (c12 != 'T' && c12 != 't') {
                                                    break;
                                                }
                                                return 9;
                                            }
                                            case 'N':
                                            case 'n': {
                                                final char c13 = this.orig[this.index++];
                                                if (c13 != 'O' && c13 != 'o') {
                                                    break;
                                                }
                                                final char c14 = this.orig[this.index++];
                                                if (c14 != 'V' && c14 != 'v') {
                                                    break;
                                                }
                                                return 10;
                                            }
                                            case 'D':
                                            case 'd': {
                                                final char c15 = this.orig[this.index++];
                                                if (c15 != 'E' && c15 != 'e') {
                                                    break;
                                                }
                                                final char c16 = this.orig[this.index++];
                                                if (c16 == 'C' || c16 == 'c') {
                                                    return 11;
                                                }
                                                break;
                                            }
                                        }
                                        throw new ParseException("Bad Month", this.index);
                                        final char c17 = this.orig[this.index++];
                                        // iftrue(Label_0888:, c17 == 'G' || c17 == 'g')
                                        throw new ParseException("Bad Month", this.index);
                                    }
                                    catch (ArrayIndexOutOfBoundsException ex) {
                                        throw new ParseException("Bad Month", this.index);
                                    }
                                }
                                continue Label_0515_Outer;
                            }
                            if (c2 == 'L' || c2 == 'l') {
                                return 6;
                            }
                            continue Label_0515_Outer;
                        }
                        if (c6 == 'Y' || c6 == 'y') {
                            return 4;
                        }
                        continue Label_0515_Outer;
                    }
                    if (c7 != 'U' && c7 != 'u') {
                        continue Label_0515_Outer;
                    }
                    break;
                }
                continue;
            }
            Label_0888: {
                return 7;
            }
        }
    }
    
    public int parseNumber() throws ParseException {
        final int length = this.orig.length;
        int n = 0;
        int n2 = 0;
        while (this.index < length) {
            switch (this.orig[this.index]) {
                default: {
                    if (n == 0) {
                        throw new ParseException("No Number found", this.index);
                    }
                    return n2;
                }
                case '0': {
                    n2 *= 10;
                    break;
                }
                case '1': {
                    n2 = n2 * 10 + 1;
                    break;
                }
                case '2': {
                    n2 = n2 * 10 + 2;
                    break;
                }
                case '3': {
                    n2 = n2 * 10 + 3;
                    break;
                }
                case '4': {
                    n2 = n2 * 10 + 4;
                    break;
                }
                case '5': {
                    n2 = n2 * 10 + 5;
                    break;
                }
                case '6': {
                    n2 = n2 * 10 + 6;
                    break;
                }
                case '7': {
                    n2 = n2 * 10 + 7;
                    break;
                }
                case '8': {
                    n2 = n2 * 10 + 8;
                    break;
                }
                case '9': {
                    n2 = n2 * 10 + 9;
                    break;
                }
            }
            n = 1;
            ++this.index;
        }
        if (n != 0) {
            return n2;
        }
        throw new ParseException("No Number found", this.index);
    }
    
    public int parseNumericTimeZone() throws ParseException {
        boolean b = false;
        final char c = this.orig[this.index++];
        if (c == '+') {
            b = true;
        }
        else if (c != '-') {
            throw new ParseException("Bad Numeric TimeZone", this.index);
        }
        final int number = this.parseNumber();
        int n = number / 100 * 60 + number % 100;
        if (b) {
            n = -n;
        }
        return n;
    }
    
    public int parseTimeZone() throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        }
        final char c = this.orig[this.index];
        if (c == '+' || c == '-') {
            return this.parseNumericTimeZone();
        }
        return this.parseAlphaTimeZone();
    }
    
    public int peekChar() throws ParseException {
        if (this.index < this.orig.length) {
            return this.orig[this.index];
        }
        throw new ParseException("No more characters", this.index);
    }
    
    public void skipChar(final char c) throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        }
        if (this.orig[this.index] == c) {
            ++this.index;
            return;
        }
        throw new ParseException("Wrong char", this.index);
    }
    
    public boolean skipIfChar(final char c) throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        }
        if (this.orig[this.index] == c) {
            ++this.index;
            return true;
        }
        return false;
    }
    
    public void skipUntilNumber() throws ParseException {
        Label_0092:Label_0064_Outer:
        while (true) {
            while (true) {
                Label_0093: {
                    try {
                        switch (this.orig[this.index]) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9': {
                                break Label_0092;
                            }
                            default: {
                                break Label_0093;
                            }
                        }
                        ++this.index;
                        continue Label_0064_Outer;
                    }
                    catch (ArrayIndexOutOfBoundsException ex) {
                        throw new ParseException("No Number Found", this.index);
                    }
                    break;
                }
                continue;
            }
        }
    }
    
    public void skipWhiteSpace() {
        while (this.index < this.orig.length) {
            switch (this.orig[this.index]) {
                default: {}
                case '\t':
                case '\n':
                case '\r':
                case ' ': {
                    ++this.index;
                    continue;
                }
            }
        }
    }
}
