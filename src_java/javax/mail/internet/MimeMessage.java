package javax.mail.internet;

import javax.mail.util.*;
import javax.activation.*;
import java.util.*;
import java.text.*;
import javax.mail.*;
import com.sun.mail.util.*;
import java.io.*;

public class MimeMessage extends Message implements MimePart
{
    private static final Flags answeredFlag;
    private static MailDateFormat mailDateFormat;
    Object cachedContent;
    protected byte[] content;
    protected InputStream contentStream;
    protected DataHandler dh;
    protected Flags flags;
    protected InternetHeaders headers;
    protected boolean modified;
    protected boolean saved;
    private boolean strict;
    
    static {
        MimeMessage.mailDateFormat = new MailDateFormat();
        answeredFlag = new Flags(Flags.Flag.ANSWERED);
    }
    
    protected MimeMessage(final Folder folder, final int n) {
        super(folder, n);
        this.modified = false;
        this.saved = false;
        this.strict = true;
        this.flags = new Flags();
        this.saved = true;
        this.initStrict();
    }
    
    protected MimeMessage(final Folder folder, final InputStream inputStream, final int n) throws MessagingException {
        this(folder, n);
        this.initStrict();
        this.parse(inputStream);
    }
    
    protected MimeMessage(final Folder folder, final InternetHeaders headers, final byte[] content, final int n) throws MessagingException {
        this(folder, n);
        this.headers = headers;
        this.content = content;
        this.initStrict();
    }
    
    public MimeMessage(final Session session) {
        super(session);
        this.modified = false;
        this.saved = false;
        this.strict = true;
        this.modified = true;
        this.headers = new InternetHeaders();
        this.flags = new Flags();
        this.initStrict();
    }
    
    public MimeMessage(final Session session, final InputStream inputStream) throws MessagingException {
        super(session);
        this.modified = false;
        this.saved = false;
        this.strict = true;
        this.flags = new Flags();
        this.initStrict();
        this.parse(inputStream);
        this.saved = true;
    }
    
    public MimeMessage(final MimeMessage mimeMessage) throws MessagingException {
        super(mimeMessage.session);
        this.modified = false;
        this.saved = false;
        this.strict = true;
        this.flags = mimeMessage.getFlags();
        final int size = mimeMessage.getSize();
        Label_0093: {
            if (size <= 0) {
                break Label_0093;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(size);
            try {
                while (true) {
                    this.strict = mimeMessage.strict;
                    mimeMessage.writeTo(byteArrayOutputStream);
                    byteArrayOutputStream.close();
                    final SharedByteArrayInputStream sharedByteArrayInputStream = new SharedByteArrayInputStream(byteArrayOutputStream.toByteArray());
                    this.parse(sharedByteArrayInputStream);
                    sharedByteArrayInputStream.close();
                    this.saved = true;
                    return;
                    byteArrayOutputStream = new ByteArrayOutputStream();
                    continue;
                }
            }
            catch (IOException ex) {
                throw new MessagingException("IOException while copying message", ex);
            }
        }
    }
    
    private void addAddressHeader(final String s, final Address[] array) throws MessagingException {
        final String string = InternetAddress.toString(array);
        if (string == null) {
            return;
        }
        this.addHeader(s, string);
    }
    
    private Address[] eliminateDuplicates(final Vector vector, final Address[] array) {
        if (array == null) {
            return null;
        }
        int n = 0;
        int i = 0;
    Label_0011:
        while (i < array.length) {
            final boolean b = false;
            while (true) {
                for (int j = 0; j < vector.size(); ++j) {
                    if (vector.elementAt(j).equals(array[i])) {
                        final int n2 = 1;
                        ++n;
                        array[i] = null;
                        if (n2 == 0) {
                            vector.addElement(array[i]);
                        }
                        ++i;
                        continue Label_0011;
                    }
                }
                final int n2 = b ? 1 : 0;
                continue;
            }
        }
        Address[] array2 = array;
        if (n != 0) {
            if (array instanceof InternetAddress[]) {
                array2 = new InternetAddress[array.length - n];
            }
            else {
                array2 = new Address[array.length - n];
            }
            int k = 0;
            int n3 = 0;
            while (k < array.length) {
                int n4 = n3;
                if (array[k] != null) {
                    array2[n3] = array[k];
                    n4 = n3 + 1;
                }
                ++k;
                n3 = n4;
            }
        }
        return array2;
    }
    
    private Address[] getAddressHeader(String header) throws MessagingException {
        header = this.getHeader(header, ",");
        if (header == null) {
            return null;
        }
        return InternetAddress.parseHeader(header, this.strict);
    }
    
    private String getHeaderName(final Message.RecipientType recipientType) throws MessagingException {
        if (recipientType == Message.RecipientType.TO) {
            return "To";
        }
        if (recipientType == Message.RecipientType.CC) {
            return "Cc";
        }
        if (recipientType == Message.RecipientType.BCC) {
            return "Bcc";
        }
        if (recipientType == RecipientType.NEWSGROUPS) {
            return "Newsgroups";
        }
        throw new MessagingException("Invalid Recipient Type");
    }
    
    private void initStrict() {
        if (this.session != null) {
            final String property = this.session.getProperty("mail.mime.address.strict");
            this.strict = (property == null || !property.equalsIgnoreCase("false"));
        }
    }
    
    private void setAddressHeader(final String s, final Address[] array) throws MessagingException {
        final String string = InternetAddress.toString(array);
        if (string == null) {
            this.removeHeader(s);
            return;
        }
        this.setHeader(s, string);
    }
    
    @Override
    public void addFrom(final Address[] array) throws MessagingException {
        this.addAddressHeader("From", array);
    }
    
    @Override
    public void addHeader(final String s, final String s2) throws MessagingException {
        this.headers.addHeader(s, s2);
    }
    
    @Override
    public void addHeaderLine(final String s) throws MessagingException {
        this.headers.addHeaderLine(s);
    }
    
    public void addRecipients(final Message.RecipientType recipientType, final String s) throws MessagingException {
        if (recipientType == RecipientType.NEWSGROUPS) {
            if (s != null && s.length() != 0) {
                this.addHeader("Newsgroups", s);
            }
            return;
        }
        this.addAddressHeader(this.getHeaderName(recipientType), InternetAddress.parse(s));
    }
    
    @Override
    public void addRecipients(final Message.RecipientType recipientType, final Address[] array) throws MessagingException {
        if (recipientType == RecipientType.NEWSGROUPS) {
            final String string = NewsAddress.toString(array);
            if (string != null) {
                this.addHeader("Newsgroups", string);
            }
            return;
        }
        this.addAddressHeader(this.getHeaderName(recipientType), array);
    }
    
    protected InternetHeaders createInternetHeaders(final InputStream inputStream) throws MessagingException {
        return new InternetHeaders(inputStream);
    }
    
    protected MimeMessage createMimeMessage(final Session session) throws MessagingException {
        return new MimeMessage(session);
    }
    
    @Override
    public Enumeration getAllHeaderLines() throws MessagingException {
        return this.headers.getAllHeaderLines();
    }
    
    @Override
    public Enumeration getAllHeaders() throws MessagingException {
        return this.headers.getAllHeaders();
    }
    
    @Override
    public Address[] getAllRecipients() throws MessagingException {
        final Address[] allRecipients = super.getAllRecipients();
        final Address[] recipients = this.getRecipients(RecipientType.NEWSGROUPS);
        if (recipients == null) {
            return allRecipients;
        }
        if (allRecipients == null) {
            return recipients;
        }
        final Address[] array = new Address[allRecipients.length + recipients.length];
        System.arraycopy(allRecipients, 0, array, 0, allRecipients.length);
        System.arraycopy(recipients, 0, array, allRecipients.length, recipients.length);
        return array;
    }
    
    @Override
    public Object getContent() throws IOException, MessagingException {
        Object o;
        if (this.cachedContent != null) {
            o = this.cachedContent;
        }
        else {
            try {
                final Object cachedContent = o = this.getDataHandler().getContent();
                if (MimeBodyPart.cacheMultipart) {
                    if (!(cachedContent instanceof Multipart)) {
                        o = cachedContent;
                        if (!(cachedContent instanceof Message)) {
                            return o;
                        }
                    }
                    if (this.content == null) {
                        o = cachedContent;
                        if (this.contentStream == null) {
                            return o;
                        }
                    }
                    return this.cachedContent = cachedContent;
                }
            }
            catch (FolderClosedIOException ex) {
                throw new FolderClosedException(ex.getFolder(), ex.getMessage());
            }
            catch (MessageRemovedIOException ex2) {
                throw new MessageRemovedException(ex2.getMessage());
            }
        }
        return o;
    }
    
    @Override
    public String getContentID() throws MessagingException {
        return this.getHeader("Content-Id", null);
    }
    
    @Override
    public String[] getContentLanguage() throws MessagingException {
        return MimeBodyPart.getContentLanguage(this);
    }
    
    @Override
    public String getContentMD5() throws MessagingException {
        return this.getHeader("Content-MD5", null);
    }
    
    protected InputStream getContentStream() throws MessagingException {
        if (this.contentStream != null) {
            return ((SharedInputStream)this.contentStream).newStream(0L, -1L);
        }
        if (this.content != null) {
            return new SharedByteArrayInputStream(this.content);
        }
        throw new MessagingException("No content");
    }
    
    @Override
    public String getContentType() throws MessagingException {
        String header;
        if ((header = this.getHeader("Content-Type", null)) == null) {
            header = "text/plain";
        }
        return header;
    }
    
    @Override
    public DataHandler getDataHandler() throws MessagingException {
        synchronized (this) {
            if (this.dh == null) {
                this.dh = new DataHandler(new MimePartDataSource(this));
            }
            return this.dh;
        }
    }
    
    @Override
    public String getDescription() throws MessagingException {
        return MimeBodyPart.getDescription(this);
    }
    
    @Override
    public String getDisposition() throws MessagingException {
        return MimeBodyPart.getDisposition(this);
    }
    
    @Override
    public String getEncoding() throws MessagingException {
        return MimeBodyPart.getEncoding(this);
    }
    
    @Override
    public String getFileName() throws MessagingException {
        return MimeBodyPart.getFileName(this);
    }
    
    @Override
    public Flags getFlags() throws MessagingException {
        synchronized (this) {
            return (Flags)this.flags.clone();
        }
    }
    
    @Override
    public Address[] getFrom() throws MessagingException {
        Address[] array;
        if ((array = this.getAddressHeader("From")) == null) {
            array = this.getAddressHeader("Sender");
        }
        return array;
    }
    
    @Override
    public String getHeader(final String s, final String s2) throws MessagingException {
        return this.headers.getHeader(s, s2);
    }
    
    @Override
    public String[] getHeader(final String s) throws MessagingException {
        return this.headers.getHeader(s);
    }
    
    @Override
    public InputStream getInputStream() throws IOException, MessagingException {
        return this.getDataHandler().getInputStream();
    }
    
    @Override
    public int getLineCount() throws MessagingException {
        return -1;
    }
    
    @Override
    public Enumeration getMatchingHeaderLines(final String[] array) throws MessagingException {
        return this.headers.getMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getMatchingHeaders(final String[] array) throws MessagingException {
        return this.headers.getMatchingHeaders(array);
    }
    
    public String getMessageID() throws MessagingException {
        return this.getHeader("Message-ID", null);
    }
    
    @Override
    public Enumeration getNonMatchingHeaderLines(final String[] array) throws MessagingException {
        return this.headers.getNonMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaders(final String[] array) throws MessagingException {
        return this.headers.getNonMatchingHeaders(array);
    }
    
    public InputStream getRawInputStream() throws MessagingException {
        return this.getContentStream();
    }
    
    @Override
    public Date getReceivedDate() throws MessagingException {
        return null;
    }
    
    @Override
    public Address[] getRecipients(final Message.RecipientType recipientType) throws MessagingException {
        if (recipientType != RecipientType.NEWSGROUPS) {
            return this.getAddressHeader(this.getHeaderName(recipientType));
        }
        final String header = this.getHeader("Newsgroups", ",");
        if (header == null) {
            return null;
        }
        return NewsAddress.parse(header);
    }
    
    @Override
    public Address[] getReplyTo() throws MessagingException {
        Address[] array;
        if ((array = this.getAddressHeader("Reply-To")) == null) {
            array = this.getFrom();
        }
        return array;
    }
    
    public Address getSender() throws MessagingException {
        final Address[] addressHeader = this.getAddressHeader("Sender");
        if (addressHeader == null || addressHeader.length == 0) {
            return null;
        }
        return addressHeader[0];
    }
    
    @Override
    public Date getSentDate() throws MessagingException {
        final String header = this.getHeader("Date", null);
        if (header != null) {
            try {
                synchronized (MimeMessage.mailDateFormat) {
                    return MimeMessage.mailDateFormat.parse(header);
                }
            }
            catch (ParseException ex) {
                return null;
            }
        }
        return null;
    }
    
    @Override
    public int getSize() throws MessagingException {
        int n;
        if (this.content != null) {
            n = this.content.length;
        }
        else {
            if (this.contentStream == null) {
                return -1;
            }
            try {
                if ((n = this.contentStream.available()) <= 0) {
                    return -1;
                }
            }
            catch (IOException ex) {
                return -1;
            }
        }
        return n;
    }
    
    @Override
    public String getSubject() throws MessagingException {
        final String header = this.getHeader("Subject", null);
        if (header == null) {
            return null;
        }
        try {
            return MimeUtility.decodeText(MimeUtility.unfold(header));
        }
        catch (UnsupportedEncodingException ex) {
            return header;
        }
    }
    
    @Override
    public boolean isMimeType(final String s) throws MessagingException {
        return MimeBodyPart.isMimeType(this, s);
    }
    
    @Override
    public boolean isSet(final Flags.Flag flag) throws MessagingException {
        synchronized (this) {
            return this.flags.contains(flag);
        }
    }
    
    protected void parse(final InputStream inputStream) throws MessagingException {
        InputStream inputStream2 = inputStream;
        if (!(inputStream instanceof ByteArrayInputStream)) {
            inputStream2 = inputStream;
            if (!(inputStream instanceof BufferedInputStream)) {
                inputStream2 = inputStream;
                if (!(inputStream instanceof SharedInputStream)) {
                    inputStream2 = new BufferedInputStream(inputStream);
                }
            }
        }
        this.headers = this.createInternetHeaders(inputStream2);
        if (inputStream2 instanceof SharedInputStream) {
            final SharedInputStream sharedInputStream = (SharedInputStream)inputStream2;
            this.contentStream = sharedInputStream.newStream(sharedInputStream.getPosition(), -1L);
        }
        else {
            try {
                this.content = ASCIIUtility.getBytes(inputStream2);
            }
            catch (IOException ex) {
                throw new MessagingException("IOException", ex);
            }
        }
        this.modified = false;
    }
    
    @Override
    public void removeHeader(final String s) throws MessagingException {
        this.headers.removeHeader(s);
    }
    
    @Override
    public Message reply(final boolean b) throws MessagingException {
        final MimeMessage mimeMessage = this.createMimeMessage(this.session);
        final String header = this.getHeader("Subject", null);
        if (header != null) {
            String string = header;
            if (!header.regionMatches(true, 0, "Re: ", 0, 4)) {
                string = "Re: " + header;
            }
            mimeMessage.setHeader("Subject", string);
        }
        final Address[] replyTo = this.getReplyTo();
        mimeMessage.setRecipients(Message.RecipientType.TO, replyTo);
    Label_0235_Outer:
        while (true) {
        Label_0391_Outer:
            while (true) {
                Label_0293: {
                    if (!b) {
                        break Label_0293;
                    }
                    final Vector<InternetAddress> vector = new Vector<InternetAddress>();
                    final InternetAddress localAddress = InternetAddress.getLocalAddress(this.session);
                    if (localAddress != null) {
                        vector.addElement(localAddress);
                    }
                    String property = null;
                    if (this.session != null) {
                        property = this.session.getProperty("mail.alternates");
                    }
                    if (property != null) {
                        this.eliminateDuplicates(vector, InternetAddress.parse(property, false));
                    }
                    String property2 = null;
                    if (this.session != null) {
                        property2 = this.session.getProperty("mail.replyallcc");
                    }
                    if (property2 == null || !property2.equalsIgnoreCase("true")) {
                        break Label_0235_Outer;
                    }
                    final int n = 1;
                    this.eliminateDuplicates(vector, replyTo);
                    final Address[] eliminateDuplicates = this.eliminateDuplicates(vector, this.getRecipients(Message.RecipientType.TO));
                    if (eliminateDuplicates != null && eliminateDuplicates.length > 0) {
                        if (n == 0) {
                            break Label_0391_Outer;
                        }
                        mimeMessage.addRecipients(Message.RecipientType.CC, eliminateDuplicates);
                    }
                    final Address[] eliminateDuplicates2 = this.eliminateDuplicates(vector, this.getRecipients(Message.RecipientType.CC));
                    if (eliminateDuplicates2 != null && eliminateDuplicates2.length > 0) {
                        mimeMessage.addRecipients(Message.RecipientType.CC, eliminateDuplicates2);
                    }
                    final Address[] recipients = this.getRecipients(RecipientType.NEWSGROUPS);
                    if (recipients != null && recipients.length > 0) {
                        mimeMessage.setRecipients(RecipientType.NEWSGROUPS, recipients);
                    }
                }
                final String header2 = this.getHeader("Message-Id", null);
                if (header2 != null) {
                    mimeMessage.setHeader("In-Reply-To", header2);
                }
                String s;
                if ((s = this.getHeader("References", " ")) == null) {
                    s = this.getHeader("In-Reply-To", " ");
                }
                String string2 = s;
                Label_0439: {
                    if (header2 != null) {
                        if (s == null) {
                            break Label_0439;
                        }
                        string2 = String.valueOf(MimeUtility.unfold(s)) + " " + header2;
                    }
                    while (true) {
                        if (string2 != null) {
                            mimeMessage.setHeader("References", MimeUtility.fold(12, string2));
                        }
                        try {
                            this.setFlags(MimeMessage.answeredFlag, true);
                            return mimeMessage;
                            string2 = header2;
                            continue;
                            final Address[] eliminateDuplicates;
                            mimeMessage.addRecipients(Message.RecipientType.TO, eliminateDuplicates);
                            continue Label_0391_Outer;
                            final int n = 0;
                            continue Label_0235_Outer;
                        }
                        catch (MessagingException ex) {
                            return mimeMessage;
                        }
                        break;
                    }
                }
                break;
            }
            break;
        }
    }
    
    @Override
    public void saveChanges() throws MessagingException {
        this.modified = true;
        this.saved = true;
        this.updateHeaders();
    }
    
    @Override
    public void setContent(final Object o, final String s) throws MessagingException {
        if (o instanceof Multipart) {
            this.setContent((Multipart)o);
            return;
        }
        this.setDataHandler(new DataHandler(o, s));
    }
    
    @Override
    public void setContent(final Multipart multipart) throws MessagingException {
        this.setDataHandler(new DataHandler(multipart, multipart.getContentType()));
        multipart.setParent(this);
    }
    
    public void setContentID(final String s) throws MessagingException {
        if (s == null) {
            this.removeHeader("Content-ID");
            return;
        }
        this.setHeader("Content-ID", s);
    }
    
    @Override
    public void setContentLanguage(final String[] array) throws MessagingException {
        MimeBodyPart.setContentLanguage(this, array);
    }
    
    @Override
    public void setContentMD5(final String s) throws MessagingException {
        this.setHeader("Content-MD5", s);
    }
    
    @Override
    public void setDataHandler(final DataHandler dh) throws MessagingException {
        synchronized (this) {
            this.dh = dh;
            this.cachedContent = null;
            MimeBodyPart.invalidateContentHeaders(this);
        }
    }
    
    @Override
    public void setDescription(final String s) throws MessagingException {
        this.setDescription(s, null);
    }
    
    public void setDescription(final String s, final String s2) throws MessagingException {
        MimeBodyPart.setDescription(this, s, s2);
    }
    
    @Override
    public void setDisposition(final String s) throws MessagingException {
        MimeBodyPart.setDisposition(this, s);
    }
    
    @Override
    public void setFileName(final String s) throws MessagingException {
        MimeBodyPart.setFileName(this, s);
    }
    
    @Override
    public void setFlags(final Flags flags, final boolean b) throws MessagingException {
        // monitorenter(this)
        Label_0017: {
            if (!b) {
                break Label_0017;
            }
            try {
                this.flags.add(flags);
                return;
                this.flags.remove(flags);
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    @Override
    public void setFrom() throws MessagingException {
        final InternetAddress localAddress = InternetAddress.getLocalAddress(this.session);
        if (localAddress != null) {
            this.setFrom(localAddress);
            return;
        }
        throw new MessagingException("No From address");
    }
    
    @Override
    public void setFrom(final Address address) throws MessagingException {
        if (address == null) {
            this.removeHeader("From");
            return;
        }
        this.setHeader("From", address.toString());
    }
    
    @Override
    public void setHeader(final String s, final String s2) throws MessagingException {
        this.headers.setHeader(s, s2);
    }
    
    public void setRecipients(final Message.RecipientType recipientType, final String s) throws MessagingException {
        if (recipientType != RecipientType.NEWSGROUPS) {
            this.setAddressHeader(this.getHeaderName(recipientType), InternetAddress.parse(s));
            return;
        }
        if (s == null || s.length() == 0) {
            this.removeHeader("Newsgroups");
            return;
        }
        this.setHeader("Newsgroups", s);
    }
    
    @Override
    public void setRecipients(final Message.RecipientType recipientType, final Address[] array) throws MessagingException {
        if (recipientType != RecipientType.NEWSGROUPS) {
            this.setAddressHeader(this.getHeaderName(recipientType), array);
            return;
        }
        if (array == null || array.length == 0) {
            this.removeHeader("Newsgroups");
            return;
        }
        this.setHeader("Newsgroups", NewsAddress.toString(array));
    }
    
    @Override
    public void setReplyTo(final Address[] array) throws MessagingException {
        this.setAddressHeader("Reply-To", array);
    }
    
    public void setSender(final Address address) throws MessagingException {
        if (address == null) {
            this.removeHeader("Sender");
            return;
        }
        this.setHeader("Sender", address.toString());
    }
    
    @Override
    public void setSentDate(final Date date) throws MessagingException {
        if (date == null) {
            this.removeHeader("Date");
            return;
        }
        synchronized (MimeMessage.mailDateFormat) {
            this.setHeader("Date", MimeMessage.mailDateFormat.format(date));
        }
    }
    
    @Override
    public void setSubject(final String s) throws MessagingException {
        this.setSubject(s, null);
    }
    
    public void setSubject(final String s, final String s2) throws MessagingException {
        if (s == null) {
            this.removeHeader("Subject");
            return;
        }
        try {
            this.setHeader("Subject", MimeUtility.fold(9, MimeUtility.encodeText(s, s2, null)));
        }
        catch (UnsupportedEncodingException ex) {
            throw new MessagingException("Encoding error", ex);
        }
    }
    
    @Override
    public void setText(final String s) throws MessagingException {
        this.setText(s, null);
    }
    
    @Override
    public void setText(final String s, final String s2) throws MessagingException {
        MimeBodyPart.setText(this, s, s2, "plain");
    }
    
    @Override
    public void setText(final String s, final String s2, final String s3) throws MessagingException {
        MimeBodyPart.setText(this, s, s2, s3);
    }
    
    protected void updateHeaders() throws MessagingException {
        MimeBodyPart.updateHeaders(this);
        this.setHeader("MIME-Version", "1.0");
        this.updateMessageID();
        if (this.cachedContent == null) {
            return;
        }
        this.dh = new DataHandler(this.cachedContent, this.getContentType());
        this.cachedContent = null;
        this.content = null;
        while (true) {
            if (this.contentStream == null) {
                break Label_0068;
            }
            try {
                this.contentStream.close();
                this.contentStream = null;
            }
            catch (IOException ex) {
                continue;
            }
            break;
        }
    }
    
    protected void updateMessageID() throws MessagingException {
        this.setHeader("Message-ID", "<" + UniqueValue.getUniqueMessageIDValue(this.session) + ">");
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) throws IOException, MessagingException {
        this.writeTo(outputStream, null);
    }
    
    public void writeTo(final OutputStream outputStream, final String[] array) throws IOException, MessagingException {
        if (!this.saved) {
            this.saveChanges();
        }
        if (this.modified) {
            MimeBodyPart.writeTo(this, outputStream, array);
            return;
        }
        final Enumeration nonMatchingHeaderLines = this.getNonMatchingHeaderLines(array);
        final LineOutputStream lineOutputStream = new LineOutputStream(outputStream);
        while (nonMatchingHeaderLines.hasMoreElements()) {
            lineOutputStream.writeln(nonMatchingHeaderLines.nextElement());
        }
        lineOutputStream.writeln();
        if (this.content == null) {
            final InputStream contentStream = this.getContentStream();
            final byte[] array2 = new byte[8192];
            while (true) {
                final int read = contentStream.read(array2);
                if (read <= 0) {
                    break;
                }
                outputStream.write(array2, 0, read);
            }
            contentStream.close();
            final byte[] array3 = null;
        }
        else {
            outputStream.write(this.content);
        }
        outputStream.flush();
    }
    
    public static class RecipientType extends Message.RecipientType
    {
        public static final RecipientType NEWSGROUPS;
        private static final long serialVersionUID = -5468290701714395543L;
        
        static {
            NEWSGROUPS = new RecipientType("Newsgroups");
        }
        
        protected RecipientType(final String s) {
            super(s);
        }
        
        @Override
        protected Object readResolve() throws ObjectStreamException {
            if (this.type.equals("Newsgroups")) {
                return RecipientType.NEWSGROUPS;
            }
            return super.readResolve();
        }
    }
}
