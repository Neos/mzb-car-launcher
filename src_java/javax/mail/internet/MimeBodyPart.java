package javax.mail.internet;

import java.util.*;
import java.io.*;
import javax.activation.*;
import com.sun.mail.util.*;
import javax.mail.*;

public class MimeBodyPart extends BodyPart implements MimePart
{
    static boolean cacheMultipart;
    private static boolean decodeFileName;
    private static boolean encodeFileName;
    private static boolean setContentTypeFileName;
    private static boolean setDefaultTextCharset;
    private Object cachedContent;
    protected byte[] content;
    protected InputStream contentStream;
    protected DataHandler dh;
    protected InternetHeaders headers;
    
    static {
        final boolean b = false;
        MimeBodyPart.setDefaultTextCharset = true;
        MimeBodyPart.setContentTypeFileName = true;
        MimeBodyPart.encodeFileName = false;
        MimeBodyPart.decodeFileName = false;
        MimeBodyPart.cacheMultipart = true;
        try {
            final String property = System.getProperty("mail.mime.setdefaulttextcharset");
            MimeBodyPart.setDefaultTextCharset = (property == null || !property.equalsIgnoreCase("false"));
            final String property2 = System.getProperty("mail.mime.setcontenttypefilename");
            MimeBodyPart.setContentTypeFileName = (property2 == null || !property2.equalsIgnoreCase("false"));
            final String property3 = System.getProperty("mail.mime.encodefilename");
            MimeBodyPart.encodeFileName = (property3 != null && !property3.equalsIgnoreCase("false"));
            final String property4 = System.getProperty("mail.mime.decodefilename");
            MimeBodyPart.decodeFileName = (property4 != null && !property4.equalsIgnoreCase("false"));
            final String property5 = System.getProperty("mail.mime.cachemultipart");
            MimeBodyPart.cacheMultipart = (property5 == null || !property5.equalsIgnoreCase("false") || b);
        }
        catch (SecurityException ex) {}
    }
    
    public MimeBodyPart() {
        this.headers = new InternetHeaders();
    }
    
    public MimeBodyPart(final InputStream inputStream) throws MessagingException {
        InputStream inputStream2 = inputStream;
        if (!(inputStream instanceof ByteArrayInputStream)) {
            inputStream2 = inputStream;
            if (!(inputStream instanceof BufferedInputStream)) {
                inputStream2 = inputStream;
                if (!(inputStream instanceof SharedInputStream)) {
                    inputStream2 = new BufferedInputStream(inputStream);
                }
            }
        }
        this.headers = new InternetHeaders(inputStream2);
        if (inputStream2 instanceof SharedInputStream) {
            final SharedInputStream sharedInputStream = (SharedInputStream)inputStream2;
            this.contentStream = sharedInputStream.newStream(sharedInputStream.getPosition(), -1L);
            return;
        }
        try {
            this.content = ASCIIUtility.getBytes(inputStream2);
        }
        catch (IOException ex) {
            throw new MessagingException("Error reading input stream", ex);
        }
    }
    
    public MimeBodyPart(final InternetHeaders headers, final byte[] content) throws MessagingException {
        this.headers = headers;
        this.content = content;
    }
    
    static String[] getContentLanguage(final MimePart mimePart) throws MessagingException {
        final String header = mimePart.getHeader("Content-Language", null);
        if (header != null) {
            final HeaderTokenizer headerTokenizer = new HeaderTokenizer(header, "()<>@,;:\\\"\t []/?=");
            final Vector<String> vector = new Vector<String>();
            while (true) {
                final HeaderTokenizer.Token next = headerTokenizer.next();
                final int type = next.getType();
                if (type == -4) {
                    break;
                }
                if (type != -1) {
                    continue;
                }
                vector.addElement(next.getValue());
            }
            if (vector.size() != 0) {
                final String[] array = new String[vector.size()];
                vector.copyInto(array);
                return array;
            }
        }
        return null;
    }
    
    static String getDescription(MimePart header) throws MessagingException {
        header = (MimePart)header.getHeader("Content-Description", null);
        if (header == null) {
            return null;
        }
        try {
            return MimeUtility.decodeText(MimeUtility.unfold((String)header));
        }
        catch (UnsupportedEncodingException ex) {
            return (String)header;
        }
    }
    
    static String getDisposition(final MimePart mimePart) throws MessagingException {
        final String header = mimePart.getHeader("Content-Disposition", null);
        if (header == null) {
            return null;
        }
        return new ContentDisposition(header).getDisposition();
    }
    
    static String getEncoding(final MimePart mimePart) throws MessagingException {
        final String header = mimePart.getHeader("Content-Transfer-Encoding", null);
        if (header == null) {
            return null;
        }
        final String trim = header.trim();
        if (trim.equalsIgnoreCase("7bit") || trim.equalsIgnoreCase("8bit") || trim.equalsIgnoreCase("quoted-printable") || trim.equalsIgnoreCase("binary") || trim.equalsIgnoreCase("base64")) {
            return trim;
        }
        final HeaderTokenizer headerTokenizer = new HeaderTokenizer(trim, "()<>@,;:\\\"\t []/?=");
        int i;
        HeaderTokenizer.Token next;
        do {
            next = headerTokenizer.next();
            i = next.getType();
            if (i == -4) {
                return trim;
            }
        } while (i != -1);
        return next.getValue();
    }
    
    static String getFileName(final MimePart mimePart) throws MessagingException {
        String parameter = null;
        final String header = mimePart.getHeader("Content-Disposition", null);
        if (header != null) {
            parameter = new ContentDisposition(header).getParameter("filename");
        }
        while (true) {
            String parameter2;
            if ((parameter2 = parameter) != null) {
                break Label_0066;
            }
            final String header2 = mimePart.getHeader("Content-Type", null);
            parameter2 = parameter;
            if (header2 == null) {
                break Label_0066;
            }
            try {
                parameter2 = new ContentType(header2).getParameter("name");
                String decodeText = parameter2;
                if (!MimeBodyPart.decodeFileName || (decodeText = parameter2) == null) {
                    return decodeText;
                }
                try {
                    decodeText = MimeUtility.decodeText(parameter2);
                    return decodeText;
                }
                catch (UnsupportedEncodingException ex) {
                    throw new MessagingException("Can't decode filename", ex);
                }
            }
            catch (ParseException ex2) {
                parameter2 = parameter;
                continue;
            }
            break;
        }
    }
    
    static void invalidateContentHeaders(final MimePart mimePart) throws MessagingException {
        mimePart.removeHeader("Content-Type");
        mimePart.removeHeader("Content-Transfer-Encoding");
    }
    
    static boolean isMimeType(final MimePart mimePart, final String s) throws MessagingException {
        try {
            return new ContentType(mimePart.getContentType()).match(s);
        }
        catch (ParseException ex) {
            return mimePart.getContentType().equalsIgnoreCase(s);
        }
    }
    
    static void setContentLanguage(final MimePart mimePart, final String[] array) throws MessagingException {
        final StringBuffer sb = new StringBuffer(array[0]);
        for (int i = 1; i < array.length; ++i) {
            sb.append(',').append(array[i]);
        }
        mimePart.setHeader("Content-Language", sb.toString());
    }
    
    static void setDescription(final MimePart mimePart, final String s, final String s2) throws MessagingException {
        if (s == null) {
            mimePart.removeHeader("Content-Description");
            return;
        }
        try {
            mimePart.setHeader("Content-Description", MimeUtility.fold(21, MimeUtility.encodeText(s, s2, null)));
        }
        catch (UnsupportedEncodingException ex) {
            throw new MessagingException("Encoding error", ex);
        }
    }
    
    static void setDisposition(final MimePart mimePart, final String disposition) throws MessagingException {
        if (disposition == null) {
            mimePart.removeHeader("Content-Disposition");
            return;
        }
        final String header = mimePart.getHeader("Content-Disposition", null);
        String string = disposition;
        if (header != null) {
            final ContentDisposition contentDisposition = new ContentDisposition(header);
            contentDisposition.setDisposition(disposition);
            string = contentDisposition.toString();
        }
        mimePart.setHeader("Content-Disposition", string);
    }
    
    static void setEncoding(final MimePart mimePart, final String s) throws MessagingException {
        mimePart.setHeader("Content-Transfer-Encoding", s);
    }
    
    static void setFileName(final MimePart p0, final String p1) throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: getstatic       javax/mail/internet/MimeBodyPart.encodeFileName:Z
        //     5: ifeq            19
        //     8: aload_1        
        //     9: astore_2       
        //    10: aload_1        
        //    11: ifnull          19
        //    14: aload_1        
        //    15: invokestatic    javax/mail/internet/MimeUtility.encodeText:(Ljava/lang/String;)Ljava/lang/String;
        //    18: astore_2       
        //    19: aload_0        
        //    20: ldc             "Content-Disposition"
        //    22: aconst_null    
        //    23: invokeinterface javax/mail/internet/MimePart.getHeader:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    28: astore_1       
        //    29: aload_1        
        //    30: ifnonnull       127
        //    33: ldc_w           "attachment"
        //    36: astore_1       
        //    37: new             Ljavax/mail/internet/ContentDisposition;
        //    40: dup            
        //    41: aload_1        
        //    42: invokespecial   javax/mail/internet/ContentDisposition.<init>:(Ljava/lang/String;)V
        //    45: astore_1       
        //    46: aload_1        
        //    47: ldc             "filename"
        //    49: aload_2        
        //    50: invokevirtual   javax/mail/internet/ContentDisposition.setParameter:(Ljava/lang/String;Ljava/lang/String;)V
        //    53: aload_0        
        //    54: ldc             "Content-Disposition"
        //    56: aload_1        
        //    57: invokevirtual   javax/mail/internet/ContentDisposition.toString:()Ljava/lang/String;
        //    60: invokeinterface javax/mail/internet/MimePart.setHeader:(Ljava/lang/String;Ljava/lang/String;)V
        //    65: getstatic       javax/mail/internet/MimeBodyPart.setContentTypeFileName:Z
        //    68: ifeq            113
        //    71: aload_0        
        //    72: ldc             "Content-Type"
        //    74: aconst_null    
        //    75: invokeinterface javax/mail/internet/MimePart.getHeader:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    80: astore_1       
        //    81: aload_1        
        //    82: ifnull          113
        //    85: new             Ljavax/mail/internet/ContentType;
        //    88: dup            
        //    89: aload_1        
        //    90: invokespecial   javax/mail/internet/ContentType.<init>:(Ljava/lang/String;)V
        //    93: astore_1       
        //    94: aload_1        
        //    95: ldc             "name"
        //    97: aload_2        
        //    98: invokevirtual   javax/mail/internet/ContentType.setParameter:(Ljava/lang/String;Ljava/lang/String;)V
        //   101: aload_0        
        //   102: ldc             "Content-Type"
        //   104: aload_1        
        //   105: invokevirtual   javax/mail/internet/ContentType.toString:()Ljava/lang/String;
        //   108: invokeinterface javax/mail/internet/MimePart.setHeader:(Ljava/lang/String;Ljava/lang/String;)V
        //   113: return         
        //   114: astore_0       
        //   115: new             Ljavax/mail/MessagingException;
        //   118: dup            
        //   119: ldc_w           "Can't encode filename"
        //   122: aload_0        
        //   123: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   126: athrow         
        //   127: goto            37
        //   130: astore_0       
        //   131: return         
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                  
        //  -----  -----  -----  -----  --------------------------------------
        //  14     19     114    127    Ljava/io/UnsupportedEncodingException;
        //  85     113    130    132    Ljavax/mail/internet/ParseException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0113:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static void setText(final MimePart mimePart, final String s, final String s2, final String s3) throws MessagingException {
        String defaultMIMECharset = s2;
        if (s2 == null) {
            if (MimeUtility.checkAscii(s) != 1) {
                defaultMIMECharset = MimeUtility.getDefaultMIMECharset();
            }
            else {
                defaultMIMECharset = "us-ascii";
            }
        }
        mimePart.setContent(s, "text/" + s3 + "; charset=" + MimeUtility.quote(defaultMIMECharset, "()<>@,;:\\\"\t []/?="));
    }
    
    static void updateHeaders(final MimePart mimePart) throws MessagingException {
        final DataHandler dataHandler = mimePart.getDataHandler();
        if (dataHandler != null) {
            String contentType = null;
            int n;
            int n2;
            ContentType contentType2 = null;
            MimeBodyPart mimeBodyPart;
            Object o = null;
            String string;
            String encoding;
            String defaultMIMECharset;
            String header;
            String string2;
            String parameter;
            MimeMessage mimeMessage;
            Label_0084_Outer:Label_0098_Outer:Label_0193_Outer:
            while (true) {
                while (true) {
                Label_0418:
                    while (true) {
                        Label_0402: {
                            while (true) {
                                Label_0309: {
                                Label_0300:
                                    while (true) {
                                        try {
                                            contentType = dataHandler.getContentType();
                                            n = 0;
                                            if (mimePart.getHeader("Content-Type") == null) {
                                                n2 = 1;
                                                contentType2 = new ContentType(contentType);
                                                if (!contentType2.match("multipart/*")) {
                                                    break Label_0402;
                                                }
                                                n = 1;
                                                if (!(mimePart instanceof MimeBodyPart)) {
                                                    break Label_0309;
                                                }
                                                mimeBodyPart = (MimeBodyPart)mimePart;
                                                if (mimeBodyPart.cachedContent == null) {
                                                    break Label_0300;
                                                }
                                                o = mimeBodyPart.cachedContent;
                                                if (!(o instanceof MimeMultipart)) {
                                                    break;
                                                }
                                                ((MimeMultipart)o).updateHeaders();
                                                string = contentType;
                                                if (n == 0) {
                                                    if (mimePart.getHeader("Content-Transfer-Encoding") == null) {
                                                        setEncoding(mimePart, MimeUtility.getEncoding(dataHandler));
                                                    }
                                                    string = contentType;
                                                    if (n2 != 0) {
                                                        string = contentType;
                                                        if (MimeBodyPart.setDefaultTextCharset) {
                                                            string = contentType;
                                                            if (contentType2.match("text/*")) {
                                                                string = contentType;
                                                                if (contentType2.getParameter("charset") == null) {
                                                                    encoding = mimePart.getEncoding();
                                                                    if (encoding == null || !encoding.equalsIgnoreCase("7bit")) {
                                                                        break Label_0418;
                                                                    }
                                                                    defaultMIMECharset = "us-ascii";
                                                                    contentType2.setParameter("charset", defaultMIMECharset);
                                                                    string = contentType2.toString();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (n2 != 0) {
                                                    header = mimePart.getHeader("Content-Disposition", null);
                                                    string2 = string;
                                                    if (header != null) {
                                                        parameter = new ContentDisposition(header).getParameter("filename");
                                                        string2 = string;
                                                        if (parameter != null) {
                                                            contentType2.setParameter("name", parameter);
                                                            string2 = contentType2.toString();
                                                        }
                                                    }
                                                    mimePart.setHeader("Content-Type", string2);
                                                }
                                                return;
                                            }
                                        }
                                        catch (IOException ex) {
                                            throw new MessagingException("IOException updating headers", ex);
                                        }
                                        n2 = 0;
                                        continue Label_0084_Outer;
                                    }
                                    o = dataHandler.getContent();
                                    continue Label_0098_Outer;
                                }
                                if (mimePart instanceof MimeMessage) {
                                    mimeMessage = (MimeMessage)mimePart;
                                    Label_0425: {
                                        if (mimeMessage.cachedContent != null) {
                                            o = mimeMessage.cachedContent;
                                            break Label_0425;
                                        }
                                        o = dataHandler.getContent();
                                        break Label_0425;
                                    }
                                    continue Label_0098_Outer;
                                }
                                o = dataHandler.getContent();
                                continue Label_0098_Outer;
                            }
                        }
                        if (contentType2.match("message/rfc822")) {
                            n = 1;
                            continue Label_0193_Outer;
                        }
                        continue Label_0193_Outer;
                    }
                    defaultMIMECharset = MimeUtility.getDefaultMIMECharset();
                    continue;
                }
            }
            throw new MessagingException("MIME part of type \"" + contentType + "\" contains object of type " + ((MimeMultipart)o).getClass().getName() + " instead of MimeMultipart");
        }
    }
    
    static void writeTo(final MimePart mimePart, OutputStream encode, final String[] array) throws IOException, MessagingException {
        LineOutputStream lineOutputStream;
        if (encode instanceof LineOutputStream) {
            lineOutputStream = (LineOutputStream)encode;
        }
        else {
            lineOutputStream = new LineOutputStream(encode);
        }
        final Enumeration nonMatchingHeaderLines = mimePart.getNonMatchingHeaderLines(array);
        while (nonMatchingHeaderLines.hasMoreElements()) {
            lineOutputStream.writeln(nonMatchingHeaderLines.nextElement());
        }
        lineOutputStream.writeln();
        encode = MimeUtility.encode(encode, mimePart.getEncoding());
        mimePart.getDataHandler().writeTo(encode);
        encode.flush();
    }
    
    @Override
    public void addHeader(final String s, final String s2) throws MessagingException {
        this.headers.addHeader(s, s2);
    }
    
    @Override
    public void addHeaderLine(final String s) throws MessagingException {
        this.headers.addHeaderLine(s);
    }
    
    public void attachFile(final File file) throws IOException, MessagingException {
        final FileDataSource fileDataSource = new FileDataSource(file);
        this.setDataHandler(new DataHandler(fileDataSource));
        this.setFileName(fileDataSource.getName());
    }
    
    public void attachFile(final String s) throws IOException, MessagingException {
        this.attachFile(new File(s));
    }
    
    @Override
    public Enumeration getAllHeaderLines() throws MessagingException {
        return this.headers.getAllHeaderLines();
    }
    
    @Override
    public Enumeration getAllHeaders() throws MessagingException {
        return this.headers.getAllHeaders();
    }
    
    @Override
    public Object getContent() throws IOException, MessagingException {
        Object o;
        if (this.cachedContent != null) {
            o = this.cachedContent;
        }
        else {
            try {
                final Object cachedContent = o = this.getDataHandler().getContent();
                if (MimeBodyPart.cacheMultipart) {
                    if (!(cachedContent instanceof Multipart)) {
                        o = cachedContent;
                        if (!(cachedContent instanceof Message)) {
                            return o;
                        }
                    }
                    if (this.content == null) {
                        o = cachedContent;
                        if (this.contentStream == null) {
                            return o;
                        }
                    }
                    return this.cachedContent = cachedContent;
                }
            }
            catch (FolderClosedIOException ex) {
                throw new FolderClosedException(ex.getFolder(), ex.getMessage());
            }
            catch (MessageRemovedIOException ex2) {
                throw new MessageRemovedException(ex2.getMessage());
            }
        }
        return o;
    }
    
    @Override
    public String getContentID() throws MessagingException {
        return this.getHeader("Content-Id", null);
    }
    
    @Override
    public String[] getContentLanguage() throws MessagingException {
        return getContentLanguage(this);
    }
    
    @Override
    public String getContentMD5() throws MessagingException {
        return this.getHeader("Content-MD5", null);
    }
    
    protected InputStream getContentStream() throws MessagingException {
        if (this.contentStream != null) {
            return ((SharedInputStream)this.contentStream).newStream(0L, -1L);
        }
        if (this.content != null) {
            return new ByteArrayInputStream(this.content);
        }
        throw new MessagingException("No content");
    }
    
    @Override
    public String getContentType() throws MessagingException {
        String header;
        if ((header = this.getHeader("Content-Type", null)) == null) {
            header = "text/plain";
        }
        return header;
    }
    
    @Override
    public DataHandler getDataHandler() throws MessagingException {
        if (this.dh == null) {
            this.dh = new DataHandler(new MimePartDataSource(this));
        }
        return this.dh;
    }
    
    @Override
    public String getDescription() throws MessagingException {
        return getDescription(this);
    }
    
    @Override
    public String getDisposition() throws MessagingException {
        return getDisposition(this);
    }
    
    @Override
    public String getEncoding() throws MessagingException {
        return getEncoding(this);
    }
    
    @Override
    public String getFileName() throws MessagingException {
        return getFileName(this);
    }
    
    @Override
    public String getHeader(final String s, final String s2) throws MessagingException {
        return this.headers.getHeader(s, s2);
    }
    
    @Override
    public String[] getHeader(final String s) throws MessagingException {
        return this.headers.getHeader(s);
    }
    
    @Override
    public InputStream getInputStream() throws IOException, MessagingException {
        return this.getDataHandler().getInputStream();
    }
    
    @Override
    public int getLineCount() throws MessagingException {
        return -1;
    }
    
    @Override
    public Enumeration getMatchingHeaderLines(final String[] array) throws MessagingException {
        return this.headers.getMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getMatchingHeaders(final String[] array) throws MessagingException {
        return this.headers.getMatchingHeaders(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaderLines(final String[] array) throws MessagingException {
        return this.headers.getNonMatchingHeaderLines(array);
    }
    
    @Override
    public Enumeration getNonMatchingHeaders(final String[] array) throws MessagingException {
        return this.headers.getNonMatchingHeaders(array);
    }
    
    public InputStream getRawInputStream() throws MessagingException {
        return this.getContentStream();
    }
    
    @Override
    public int getSize() throws MessagingException {
        int n;
        if (this.content != null) {
            n = this.content.length;
        }
        else {
            if (this.contentStream == null) {
                return -1;
            }
            try {
                if ((n = this.contentStream.available()) <= 0) {
                    return -1;
                }
            }
            catch (IOException ex) {
                return -1;
            }
        }
        return n;
    }
    
    @Override
    public boolean isMimeType(final String s) throws MessagingException {
        return isMimeType(this, s);
    }
    
    @Override
    public void removeHeader(final String s) throws MessagingException {
        this.headers.removeHeader(s);
    }
    
    public void saveFile(final File p0) throws IOException, MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_3       
        //     2: aconst_null    
        //     3: astore          5
        //     5: aconst_null    
        //     6: astore          6
        //     8: new             Ljava/io/BufferedOutputStream;
        //    11: dup            
        //    12: new             Ljava/io/FileOutputStream;
        //    15: dup            
        //    16: aload_1        
        //    17: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //    20: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;)V
        //    23: astore          4
        //    25: aload           6
        //    27: astore_1       
        //    28: aload_0        
        //    29: invokevirtual   javax/mail/internet/MimeBodyPart.getInputStream:()Ljava/io/InputStream;
        //    32: astore_3       
        //    33: aload_3        
        //    34: astore_1       
        //    35: sipush          8192
        //    38: newarray        B
        //    40: astore          5
        //    42: aload_3        
        //    43: astore_1       
        //    44: aload_3        
        //    45: aload           5
        //    47: invokevirtual   java/io/InputStream.read:([B)I
        //    50: istore_2       
        //    51: iload_2        
        //    52: ifgt            74
        //    55: aload_3        
        //    56: ifnull          63
        //    59: aload_3        
        //    60: invokevirtual   java/io/InputStream.close:()V
        //    63: aload           4
        //    65: ifnull          73
        //    68: aload           4
        //    70: invokevirtual   java/io/OutputStream.close:()V
        //    73: return         
        //    74: aload_3        
        //    75: astore_1       
        //    76: aload           4
        //    78: aload           5
        //    80: iconst_0       
        //    81: iload_2        
        //    82: invokevirtual   java/io/OutputStream.write:([BII)V
        //    85: goto            42
        //    88: astore_3       
        //    89: aload_1        
        //    90: ifnull          97
        //    93: aload_1        
        //    94: invokevirtual   java/io/InputStream.close:()V
        //    97: aload           4
        //    99: ifnull          107
        //   102: aload           4
        //   104: invokevirtual   java/io/OutputStream.close:()V
        //   107: aload_3        
        //   108: athrow         
        //   109: astore_1       
        //   110: goto            97
        //   113: astore_1       
        //   114: goto            107
        //   117: astore_1       
        //   118: goto            63
        //   121: astore_1       
        //   122: return         
        //   123: astore          6
        //   125: aload           5
        //   127: astore_1       
        //   128: aload_3        
        //   129: astore          4
        //   131: aload           6
        //   133: astore_3       
        //   134: goto            89
        //    Exceptions:
        //  throws java.io.IOException
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      25     123    137    Any
        //  28     33     88     89     Any
        //  35     42     88     89     Any
        //  44     51     88     89     Any
        //  59     63     117    121    Ljava/io/IOException;
        //  68     73     121    123    Ljava/io/IOException;
        //  76     85     88     89     Any
        //  93     97     109    113    Ljava/io/IOException;
        //  102    107    113    117    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 76, Size: 76
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void saveFile(final String s) throws IOException, MessagingException {
        this.saveFile(new File(s));
    }
    
    @Override
    public void setContent(final Object o, final String s) throws MessagingException {
        if (o instanceof Multipart) {
            this.setContent((Multipart)o);
            return;
        }
        this.setDataHandler(new DataHandler(o, s));
    }
    
    @Override
    public void setContent(final Multipart multipart) throws MessagingException {
        this.setDataHandler(new DataHandler(multipart, multipart.getContentType()));
        multipart.setParent(this);
    }
    
    public void setContentID(final String s) throws MessagingException {
        if (s == null) {
            this.removeHeader("Content-ID");
            return;
        }
        this.setHeader("Content-ID", s);
    }
    
    @Override
    public void setContentLanguage(final String[] array) throws MessagingException {
        setContentLanguage(this, array);
    }
    
    @Override
    public void setContentMD5(final String s) throws MessagingException {
        this.setHeader("Content-MD5", s);
    }
    
    @Override
    public void setDataHandler(final DataHandler dh) throws MessagingException {
        this.dh = dh;
        this.cachedContent = null;
        invalidateContentHeaders(this);
    }
    
    @Override
    public void setDescription(final String s) throws MessagingException {
        this.setDescription(s, null);
    }
    
    public void setDescription(final String s, final String s2) throws MessagingException {
        setDescription(this, s, s2);
    }
    
    @Override
    public void setDisposition(final String s) throws MessagingException {
        setDisposition(this, s);
    }
    
    @Override
    public void setFileName(final String s) throws MessagingException {
        setFileName(this, s);
    }
    
    @Override
    public void setHeader(final String s, final String s2) throws MessagingException {
        this.headers.setHeader(s, s2);
    }
    
    @Override
    public void setText(final String s) throws MessagingException {
        this.setText(s, null);
    }
    
    @Override
    public void setText(final String s, final String s2) throws MessagingException {
        setText(this, s, s2, "plain");
    }
    
    @Override
    public void setText(final String s, final String s2, final String s3) throws MessagingException {
        setText(this, s, s2, s3);
    }
    
    protected void updateHeaders() throws MessagingException {
        updateHeaders(this);
        if (this.cachedContent == null) {
            return;
        }
        this.dh = new DataHandler(this.cachedContent, this.getContentType());
        this.cachedContent = null;
        this.content = null;
        while (true) {
            if (this.contentStream == null) {
                break Label_0054;
            }
            try {
                this.contentStream.close();
                this.contentStream = null;
            }
            catch (IOException ex) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) throws IOException, MessagingException {
        writeTo(this, outputStream, null);
    }
}
