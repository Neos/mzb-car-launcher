package javax.mail.internet;

public class HeaderTokenizer
{
    private static final Token EOFToken;
    public static final String MIME = "()<>@,;:\\\"\t []/?=";
    public static final String RFC822 = "()<>@,;:\\\"\t .[]";
    private int currentPos;
    private String delimiters;
    private int maxPos;
    private int nextPos;
    private int peekPos;
    private boolean skipComments;
    private String string;
    
    static {
        EOFToken = new Token(-4, null);
    }
    
    public HeaderTokenizer(final String s) {
        this(s, "()<>@,;:\\\"\t .[]");
    }
    
    public HeaderTokenizer(final String s, final String s2) {
        this(s, s2, true);
    }
    
    public HeaderTokenizer(final String s, final String delimiters, final boolean skipComments) {
        String string = s;
        if (s == null) {
            string = "";
        }
        this.string = string;
        this.skipComments = skipComments;
        this.delimiters = delimiters;
        this.peekPos = 0;
        this.nextPos = 0;
        this.currentPos = 0;
        this.maxPos = this.string.length();
    }
    
    private static String filterToken(final String s, int n, final int n2) {
        final StringBuffer sb = new StringBuffer();
        int n3 = 0;
        final int n4 = 0;
        int i = n;
        n = n4;
        while (i < n2) {
            final char char1 = s.charAt(i);
            if (char1 == '\n' && n != 0) {
                n = 0;
            }
            else {
                n = 0;
                if (n3 == 0) {
                    if (char1 == '\\') {
                        n3 = 1;
                    }
                    else if (char1 == '\r') {
                        n = 1;
                    }
                    else {
                        sb.append(char1);
                    }
                }
                else {
                    sb.append(char1);
                    n3 = 0;
                }
            }
            ++i;
        }
        return sb.toString();
    }
    
    private Token getNext() throws ParseException {
        if (this.currentPos >= this.maxPos) {
            return HeaderTokenizer.EOFToken;
        }
        if (this.skipWhiteSpace() == -4) {
            return HeaderTokenizer.EOFToken;
        }
        int n = 0;
        char c;
        int n3;
        for (c = this.string.charAt(this.currentPos); c == '('; c = this.string.charAt(this.currentPos), n = n3) {
            final int currentPos = this.currentPos + 1;
            this.currentPos = currentPos;
            int n2 = 1;
            n3 = n;
            while (n2 > 0 && this.currentPos < this.maxPos) {
                final char char1 = this.string.charAt(this.currentPos);
                int n4;
                int n5;
                if (char1 == '\\') {
                    ++this.currentPos;
                    n4 = 1;
                    n5 = n2;
                }
                else if (char1 == '\r') {
                    n4 = 1;
                    n5 = n2;
                }
                else if (char1 == '(') {
                    n5 = n2 + 1;
                    n4 = n3;
                }
                else {
                    n4 = n3;
                    n5 = n2;
                    if (char1 == ')') {
                        n5 = n2 - 1;
                        n4 = n3;
                    }
                }
                ++this.currentPos;
                n3 = n4;
                n2 = n5;
            }
            if (n2 != 0) {
                throw new ParseException("Unbalanced comments");
            }
            if (!this.skipComments) {
                String s;
                if (n3 != 0) {
                    s = filterToken(this.string, currentPos, this.currentPos - 1);
                }
                else {
                    s = this.string.substring(currentPos, this.currentPos - 1);
                }
                return new Token(-3, s);
            }
            if (this.skipWhiteSpace() == -4) {
                return HeaderTokenizer.EOFToken;
            }
        }
        if (c == '\"') {
            final int currentPos2 = this.currentPos + 1;
            this.currentPos = currentPos2;
            int n6 = n;
            while (this.currentPos < this.maxPos) {
                final char char2 = this.string.charAt(this.currentPos);
                int n7;
                if (char2 == '\\') {
                    ++this.currentPos;
                    n7 = 1;
                }
                else if (char2 == '\r') {
                    n7 = 1;
                }
                else {
                    n7 = n6;
                    if (char2 == '\"') {
                        ++this.currentPos;
                        String s2;
                        if (n6 != 0) {
                            s2 = filterToken(this.string, currentPos2, this.currentPos - 1);
                        }
                        else {
                            s2 = this.string.substring(currentPos2, this.currentPos - 1);
                        }
                        return new Token(-2, s2);
                    }
                }
                ++this.currentPos;
                n6 = n7;
            }
            throw new ParseException("Unbalanced quoted string");
        }
        if (c < ' ' || c >= '\u007f' || this.delimiters.indexOf(c) >= 0) {
            ++this.currentPos;
            return new Token(c, new String(new char[] { c }));
        }
        final int currentPos3 = this.currentPos;
        while (this.currentPos < this.maxPos) {
            final char char3 = this.string.charAt(this.currentPos);
            if (char3 < ' ' || char3 >= '\u007f' || char3 == '(' || char3 == ' ' || char3 == '\"' || this.delimiters.indexOf(char3) >= 0) {
                break;
            }
            ++this.currentPos;
        }
        return new Token(-1, this.string.substring(currentPos3, this.currentPos));
    }
    
    private int skipWhiteSpace() {
        while (this.currentPos < this.maxPos) {
            final char char1 = this.string.charAt(this.currentPos);
            if (char1 != ' ' && char1 != '\t' && char1 != '\r' && char1 != '\n') {
                return this.currentPos;
            }
            ++this.currentPos;
        }
        return -4;
    }
    
    public String getRemainder() {
        return this.string.substring(this.nextPos);
    }
    
    public Token next() throws ParseException {
        this.currentPos = this.nextPos;
        final Token next = this.getNext();
        final int currentPos = this.currentPos;
        this.peekPos = currentPos;
        this.nextPos = currentPos;
        return next;
    }
    
    public Token peek() throws ParseException {
        this.currentPos = this.peekPos;
        final Token next = this.getNext();
        this.peekPos = this.currentPos;
        return next;
    }
    
    public static class Token
    {
        public static final int ATOM = -1;
        public static final int COMMENT = -3;
        public static final int EOF = -4;
        public static final int QUOTEDSTRING = -2;
        private int type;
        private String value;
        
        public Token(final int type, final String value) {
            this.type = type;
            this.value = value;
        }
        
        public int getType() {
            return this.type;
        }
        
        public String getValue() {
            return this.value;
        }
    }
}
