package javax.mail.internet;

import javax.activation.*;
import javax.mail.*;
import java.io.*;
import com.sun.mail.util.*;

public class MimeMultipart extends Multipart
{
    private static boolean bmparse;
    private static boolean ignoreMissingBoundaryParameter;
    private static boolean ignoreMissingEndBoundary;
    private boolean complete;
    protected DataSource ds;
    protected boolean parsed;
    private String preamble;
    
    static {
        final boolean b = false;
        MimeMultipart.ignoreMissingEndBoundary = true;
        MimeMultipart.ignoreMissingBoundaryParameter = true;
        MimeMultipart.bmparse = true;
        try {
            final String property = System.getProperty("mail.mime.multipart.ignoremissingendboundary");
            MimeMultipart.ignoreMissingEndBoundary = (property == null || !property.equalsIgnoreCase("false"));
            final String property2 = System.getProperty("mail.mime.multipart.ignoremissingboundaryparameter");
            MimeMultipart.ignoreMissingBoundaryParameter = (property2 == null || !property2.equalsIgnoreCase("false"));
            final String property3 = System.getProperty("mail.mime.multipart.bmparse");
            MimeMultipart.bmparse = (property3 == null || !property3.equalsIgnoreCase("false") || b);
        }
        catch (SecurityException ex) {}
    }
    
    public MimeMultipart() {
        this("mixed");
    }
    
    public MimeMultipart(final String s) {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        final String uniqueBoundaryValue = UniqueValue.getUniqueBoundaryValue();
        final ContentType contentType = new ContentType("multipart", s, null);
        contentType.setParameter("boundary", uniqueBoundaryValue);
        this.contentType = contentType.toString();
    }
    
    public MimeMultipart(final DataSource ds) throws MessagingException {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        if (ds instanceof MessageAware) {
            this.setParent(((MessageAware)ds).getMessageContext().getPart());
        }
        if (ds instanceof MultipartDataSource) {
            this.setMultipartDataSource((MultipartDataSource)ds);
            return;
        }
        this.parsed = false;
        this.ds = ds;
        this.contentType = ds.getContentType();
    }
    
    private void parsebm() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        javax/mail/internet/MimeMultipart.parsed:Z
        //     6: istore          9
        //     8: iload           9
        //    10: ifeq            16
        //    13: aload_0        
        //    14: monitorexit    
        //    15: return         
        //    16: aconst_null    
        //    17: astore          19
        //    19: lconst_0       
        //    20: lstore          14
        //    22: lconst_0       
        //    23: lstore          10
        //    25: aload_0        
        //    26: getfield        javax/mail/internet/MimeMultipart.ds:Ljavax/activation/DataSource;
        //    29: invokeinterface javax/activation/DataSource.getInputStream:()Ljava/io/InputStream;
        //    34: astore          16
        //    36: aload           16
        //    38: astore          18
        //    40: aload           16
        //    42: instanceof      Ljava/io/ByteArrayInputStream;
        //    45: ifne            83
        //    48: aload           16
        //    50: astore          18
        //    52: aload           16
        //    54: instanceof      Ljava/io/BufferedInputStream;
        //    57: ifne            83
        //    60: aload           16
        //    62: astore          18
        //    64: aload           16
        //    66: instanceof      Ljavax/mail/internet/SharedInputStream;
        //    69: ifne            83
        //    72: new             Ljava/io/BufferedInputStream;
        //    75: dup            
        //    76: aload           16
        //    78: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //    81: astore          18
        //    83: aload           18
        //    85: instanceof      Ljavax/mail/internet/SharedInputStream;
        //    88: ifeq            98
        //    91: aload           18
        //    93: checkcast       Ljavax/mail/internet/SharedInputStream;
        //    96: astore          19
        //    98: new             Ljavax/mail/internet/ContentType;
        //   101: dup            
        //   102: aload_0        
        //   103: getfield        javax/mail/internet/MimeMultipart.contentType:Ljava/lang/String;
        //   106: invokespecial   javax/mail/internet/ContentType.<init>:(Ljava/lang/String;)V
        //   109: astore          17
        //   111: aconst_null    
        //   112: astore          16
        //   114: aload           17
        //   116: ldc             "boundary"
        //   118: invokevirtual   javax/mail/internet/ContentType.getParameter:(Ljava/lang/String;)Ljava/lang/String;
        //   121: astore          17
        //   123: aload           17
        //   125: ifnull          240
        //   128: new             Ljava/lang/StringBuilder;
        //   131: dup            
        //   132: ldc             "--"
        //   134: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   137: aload           17
        //   139: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   142: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   145: astore          16
        //   147: new             Lcom/sun/mail/util/LineInputStream;
        //   150: dup            
        //   151: aload           18
        //   153: invokespecial   com/sun/mail/util/LineInputStream.<init>:(Ljava/io/InputStream;)V
        //   156: astore          24
        //   158: aconst_null    
        //   159: astore          20
        //   161: aconst_null    
        //   162: astore          22
        //   164: aload           24
        //   166: invokevirtual   com/sun/mail/util/LineInputStream.readLine:()Ljava/lang/String;
        //   169: astore          23
        //   171: aload           23
        //   173: ifnonnull       256
        //   176: aload           16
        //   178: astore          17
        //   180: aload           23
        //   182: ifnonnull       421
        //   185: new             Ljavax/mail/MessagingException;
        //   188: dup            
        //   189: ldc             "Missing start boundary"
        //   191: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   194: athrow         
        //   195: astore          16
        //   197: new             Ljavax/mail/MessagingException;
        //   200: dup            
        //   201: ldc             "IO Error"
        //   203: aload           16
        //   205: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   208: athrow         
        //   209: astore          16
        //   211: aload           18
        //   213: invokevirtual   java/io/InputStream.close:()V
        //   216: aload           16
        //   218: athrow         
        //   219: astore          16
        //   221: aload_0        
        //   222: monitorexit    
        //   223: aload           16
        //   225: athrow         
        //   226: astore          16
        //   228: new             Ljavax/mail/MessagingException;
        //   231: dup            
        //   232: ldc             "No inputstream from datasource"
        //   234: aload           16
        //   236: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   239: athrow         
        //   240: getstatic       javax/mail/internet/MimeMultipart.ignoreMissingBoundaryParameter:Z
        //   243: ifne            147
        //   246: new             Ljavax/mail/MessagingException;
        //   249: dup            
        //   250: ldc             "Missing boundary parameter"
        //   252: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   255: athrow         
        //   256: aload           23
        //   258: invokevirtual   java/lang/String.length:()I
        //   261: iconst_1       
        //   262: isub           
        //   263: istore_1       
        //   264: goto            1138
        //   267: aload           23
        //   269: iconst_0       
        //   270: iload_1        
        //   271: iconst_1       
        //   272: iadd           
        //   273: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   276: astore          21
        //   278: aload           16
        //   280: ifnull          400
        //   283: aload           16
        //   285: astore          17
        //   287: aload           21
        //   289: astore          23
        //   291: aload           21
        //   293: aload           16
        //   295: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   298: ifne            180
        //   301: aload           21
        //   303: invokevirtual   java/lang/String.length:()I
        //   306: istore_1       
        //   307: iload_1        
        //   308: ifle            164
        //   311: aload           22
        //   313: astore          17
        //   315: aload           22
        //   317: ifnonnull       329
        //   320: ldc             "line.separator"
        //   322: ldc             "\n"
        //   324: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   327: astore          17
        //   329: aload           20
        //   331: astore          23
        //   333: aload           20
        //   335: ifnonnull       354
        //   338: new             Ljava/lang/StringBuffer;
        //   341: dup            
        //   342: aload           21
        //   344: invokevirtual   java/lang/String.length:()I
        //   347: iconst_2       
        //   348: iadd           
        //   349: invokespecial   java/lang/StringBuffer.<init>:(I)V
        //   352: astore          23
        //   354: aload           23
        //   356: aload           21
        //   358: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   361: aload           17
        //   363: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   366: pop            
        //   367: aload           17
        //   369: astore          22
        //   371: aload           23
        //   373: astore          20
        //   375: goto            164
        //   378: aload           23
        //   380: iload_1        
        //   381: invokevirtual   java/lang/String.charAt:(I)C
        //   384: istore_2       
        //   385: iload_2        
        //   386: bipush          32
        //   388: if_icmpeq       1145
        //   391: iload_2        
        //   392: bipush          9
        //   394: if_icmpne       267
        //   397: goto            1145
        //   400: aload           21
        //   402: ldc             "--"
        //   404: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   407: ifeq            301
        //   410: aload           21
        //   412: astore          17
        //   414: aload           21
        //   416: astore          23
        //   418: goto            180
        //   421: aload           20
        //   423: ifnull          435
        //   426: aload_0        
        //   427: aload           20
        //   429: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   432: putfield        javax/mail/internet/MimeMultipart.preamble:Ljava/lang/String;
        //   435: aload           17
        //   437: invokestatic    com/sun/mail/util/ASCIIUtility.getBytes:(Ljava/lang/String;)[B
        //   440: astore          23
        //   442: aload           23
        //   444: arraylength    
        //   445: istore          7
        //   447: sipush          256
        //   450: newarray        I
        //   452: astore          25
        //   454: iconst_0       
        //   455: istore_1       
        //   456: iload_1        
        //   457: iload           7
        //   459: if_icmplt       502
        //   462: iload           7
        //   464: newarray        I
        //   466: astore          26
        //   468: iload           7
        //   470: istore_2       
        //   471: iload_2        
        //   472: ifgt            1161
        //   475: aload           26
        //   477: iload           7
        //   479: iconst_1       
        //   480: isub           
        //   481: iconst_1       
        //   482: iastore        
        //   483: iconst_0       
        //   484: istore_3       
        //   485: iload_3        
        //   486: ifeq            519
        //   489: aload           18
        //   491: invokevirtual   java/io/InputStream.close:()V
        //   494: aload_0        
        //   495: iconst_1       
        //   496: putfield        javax/mail/internet/MimeMultipart.parsed:Z
        //   499: goto            13
        //   502: aload           25
        //   504: aload           23
        //   506: iload_1        
        //   507: baload         
        //   508: iload_1        
        //   509: iconst_1       
        //   510: iadd           
        //   511: iastore        
        //   512: iload_1        
        //   513: iconst_1       
        //   514: iadd           
        //   515: istore_1       
        //   516: goto            456
        //   519: aconst_null    
        //   520: astore          20
        //   522: aload           19
        //   524: ifnull          585
        //   527: aload           19
        //   529: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   534: lstore          14
        //   536: aload           24
        //   538: invokevirtual   com/sun/mail/util/LineInputStream.readLine:()Ljava/lang/String;
        //   541: astore          16
        //   543: aload           16
        //   545: ifnull          556
        //   548: aload           16
        //   550: invokevirtual   java/lang/String.length:()I
        //   553: ifgt            536
        //   556: aload           16
        //   558: ifnonnull       593
        //   561: getstatic       javax/mail/internet/MimeMultipart.ignoreMissingEndBoundary:Z
        //   564: ifne            577
        //   567: new             Ljavax/mail/MessagingException;
        //   570: dup            
        //   571: ldc             "missing multipart end boundary"
        //   573: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   576: athrow         
        //   577: aload_0        
        //   578: iconst_0       
        //   579: putfield        javax/mail/internet/MimeMultipart.complete:Z
        //   582: goto            489
        //   585: aload_0        
        //   586: aload           18
        //   588: invokevirtual   javax/mail/internet/MimeMultipart.createInternetHeaders:(Ljava/io/InputStream;)Ljavax/mail/internet/InternetHeaders;
        //   591: astore          20
        //   593: aload           18
        //   595: invokevirtual   java/io/InputStream.markSupported:()Z
        //   598: ifne            611
        //   601: new             Ljavax/mail/MessagingException;
        //   604: dup            
        //   605: ldc             "Stream doesn't support mark"
        //   607: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   610: athrow         
        //   611: aconst_null    
        //   612: astore          21
        //   614: aload           19
        //   616: ifnonnull       696
        //   619: new             Ljava/io/ByteArrayOutputStream;
        //   622: dup            
        //   623: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //   626: astore          21
        //   628: iload           7
        //   630: newarray        B
        //   632: astore          16
        //   634: iload           7
        //   636: newarray        B
        //   638: astore          17
        //   640: iconst_0       
        //   641: istore          4
        //   643: iconst_1       
        //   644: istore_2       
        //   645: aload           18
        //   647: iload           7
        //   649: iconst_4       
        //   650: iadd           
        //   651: sipush          1000
        //   654: iadd           
        //   655: invokevirtual   java/io/InputStream.mark:(I)V
        //   658: iconst_0       
        //   659: istore          5
        //   661: aload           18
        //   663: aload           16
        //   665: iconst_0       
        //   666: iload           7
        //   668: invokestatic    javax/mail/internet/MimeMultipart.readFully:(Ljava/io/InputStream;[BII)I
        //   671: istore          8
        //   673: iload           8
        //   675: iload           7
        //   677: if_icmpge       1221
        //   680: getstatic       javax/mail/internet/MimeMultipart.ignoreMissingEndBoundary:Z
        //   683: ifne            708
        //   686: new             Ljavax/mail/MessagingException;
        //   689: dup            
        //   690: ldc             "missing multipart end boundary"
        //   692: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   695: athrow         
        //   696: aload           19
        //   698: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   703: lstore          10
        //   705: goto            628
        //   708: aload           19
        //   710: ifnull          722
        //   713: aload           19
        //   715: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   720: lstore          10
        //   722: aload_0        
        //   723: iconst_0       
        //   724: putfield        javax/mail/internet/MimeMultipart.complete:Z
        //   727: iconst_1       
        //   728: istore_2       
        //   729: lload           10
        //   731: lstore          12
        //   733: aload           19
        //   735: ifnull          1069
        //   738: aload_0        
        //   739: aload           19
        //   741: lload           14
        //   743: lload           12
        //   745: invokeinterface javax/mail/internet/SharedInputStream.newStream:(JJ)Ljava/io/InputStream;
        //   750: invokevirtual   javax/mail/internet/MimeMultipart.createMimeBodyPart:(Ljava/io/InputStream;)Ljavax/mail/internet/MimeBodyPart;
        //   753: astore          16
        //   755: aload_0        
        //   756: aload           16
        //   758: invokespecial   javax/mail/Multipart.addBodyPart:(Ljavax/mail/BodyPart;)V
        //   761: iload_2        
        //   762: istore_3       
        //   763: lload           12
        //   765: lstore          10
        //   767: goto            485
        //   770: aload           19
        //   772: ifnull          791
        //   775: aload           19
        //   777: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   782: iload           7
        //   784: i2l            
        //   785: lsub           
        //   786: iload_1        
        //   787: i2l            
        //   788: lsub           
        //   789: lstore          10
        //   791: aload           18
        //   793: invokevirtual   java/io/InputStream.read:()I
        //   796: istore_2       
        //   797: iload_2        
        //   798: istore          6
        //   800: iload_2        
        //   801: bipush          45
        //   803: if_icmpne       843
        //   806: iload_2        
        //   807: istore          6
        //   809: aload           18
        //   811: invokevirtual   java/io/InputStream.read:()I
        //   814: bipush          45
        //   816: if_icmpne       843
        //   819: aload_0        
        //   820: iconst_1       
        //   821: putfield        javax/mail/internet/MimeMultipart.complete:Z
        //   824: iconst_1       
        //   825: istore_2       
        //   826: iload_1        
        //   827: istore          5
        //   829: lload           10
        //   831: lstore          12
        //   833: goto            733
        //   836: aload           18
        //   838: invokevirtual   java/io/InputStream.read:()I
        //   841: istore          6
        //   843: iload           6
        //   845: bipush          32
        //   847: if_icmpeq       836
        //   850: iload           6
        //   852: bipush          9
        //   854: if_icmpeq       836
        //   857: iload_3        
        //   858: istore_2       
        //   859: iload_1        
        //   860: istore          5
        //   862: lload           10
        //   864: lstore          12
        //   866: iload           6
        //   868: bipush          10
        //   870: if_icmpeq       733
        //   873: lload           10
        //   875: lstore          12
        //   877: iload           6
        //   879: bipush          13
        //   881: if_icmpne       1349
        //   884: aload           18
        //   886: iconst_1       
        //   887: invokevirtual   java/io/InputStream.mark:(I)V
        //   890: iload_3        
        //   891: istore_2       
        //   892: iload_1        
        //   893: istore          5
        //   895: lload           10
        //   897: lstore          12
        //   899: aload           18
        //   901: invokevirtual   java/io/InputStream.read:()I
        //   904: bipush          10
        //   906: if_icmpeq       733
        //   909: aload           18
        //   911: invokevirtual   java/io/InputStream.reset:()V
        //   914: iload_3        
        //   915: istore_2       
        //   916: iload_1        
        //   917: istore          5
        //   919: lload           10
        //   921: lstore          12
        //   923: goto            733
        //   926: iload           5
        //   928: iconst_1       
        //   929: iadd           
        //   930: aload           25
        //   932: aload           16
        //   934: iload           5
        //   936: baload         
        //   937: bipush          127
        //   939: iand           
        //   940: iaload         
        //   941: isub           
        //   942: aload           26
        //   944: iload           5
        //   946: iaload         
        //   947: invokestatic    java/lang/Math.max:(II)I
        //   950: istore_1       
        //   951: iload_1        
        //   952: iconst_2       
        //   953: if_icmpge       1021
        //   956: aload           19
        //   958: ifnonnull       979
        //   961: iload           4
        //   963: iconst_1       
        //   964: if_icmple       979
        //   967: aload           21
        //   969: aload           17
        //   971: iconst_0       
        //   972: iload           4
        //   974: iconst_1       
        //   975: isub           
        //   976: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   979: aload           18
        //   981: invokevirtual   java/io/InputStream.reset:()V
        //   984: aload_0        
        //   985: aload           18
        //   987: lconst_1       
        //   988: invokespecial   javax/mail/internet/MimeMultipart.skipFully:(Ljava/io/InputStream;J)V
        //   991: iload           4
        //   993: iconst_1       
        //   994: if_icmplt       1367
        //   997: aload           17
        //   999: iconst_0       
        //  1000: aload           17
        //  1002: iload           4
        //  1004: iconst_1       
        //  1005: isub           
        //  1006: baload         
        //  1007: bastore        
        //  1008: aload           17
        //  1010: iconst_1       
        //  1011: aload           16
        //  1013: iconst_0       
        //  1014: baload         
        //  1015: bastore        
        //  1016: iconst_2       
        //  1017: istore_1       
        //  1018: goto            1355
        //  1021: iload           4
        //  1023: ifle            1041
        //  1026: aload           19
        //  1028: ifnonnull       1041
        //  1031: aload           21
        //  1033: aload           17
        //  1035: iconst_0       
        //  1036: iload           4
        //  1038: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //  1041: aload           18
        //  1043: invokevirtual   java/io/InputStream.reset:()V
        //  1046: aload_0        
        //  1047: aload           18
        //  1049: iload_1        
        //  1050: i2l            
        //  1051: invokespecial   javax/mail/internet/MimeMultipart.skipFully:(Ljava/io/InputStream;J)V
        //  1054: aload           16
        //  1056: astore          22
        //  1058: aload           17
        //  1060: astore          16
        //  1062: aload           22
        //  1064: astore          17
        //  1066: goto            1355
        //  1069: iload           4
        //  1071: iload           5
        //  1073: isub           
        //  1074: ifle            1090
        //  1077: aload           21
        //  1079: aload           17
        //  1081: iconst_0       
        //  1082: iload           4
        //  1084: iload           5
        //  1086: isub           
        //  1087: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //  1090: aload_0        
        //  1091: getfield        javax/mail/internet/MimeMultipart.complete:Z
        //  1094: ifne            1112
        //  1097: iload           8
        //  1099: ifle            1112
        //  1102: aload           21
        //  1104: aload           16
        //  1106: iconst_0       
        //  1107: iload           8
        //  1109: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //  1112: aload_0        
        //  1113: aload           20
        //  1115: aload           21
        //  1117: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //  1120: invokevirtual   javax/mail/internet/MimeMultipart.createMimeBodyPart:(Ljavax/mail/internet/InternetHeaders;[B)Ljavax/mail/internet/MimeBodyPart;
        //  1123: astore          16
        //  1125: goto            755
        //  1128: astore          17
        //  1130: goto            216
        //  1133: astore          16
        //  1135: goto            494
        //  1138: iload_1        
        //  1139: ifge            378
        //  1142: goto            267
        //  1145: iload_1        
        //  1146: iconst_1       
        //  1147: isub           
        //  1148: istore_1       
        //  1149: goto            1138
        //  1152: astore          17
        //  1154: ldc             "\n"
        //  1156: astore          17
        //  1158: goto            329
        //  1161: iload           7
        //  1163: iconst_1       
        //  1164: isub           
        //  1165: istore_1       
        //  1166: iload_1        
        //  1167: iload_2        
        //  1168: if_icmpge       1182
        //  1171: iload_1        
        //  1172: ifgt            1209
        //  1175: iload_2        
        //  1176: iconst_1       
        //  1177: isub           
        //  1178: istore_2       
        //  1179: goto            471
        //  1182: aload           23
        //  1184: iload_1        
        //  1185: baload         
        //  1186: aload           23
        //  1188: iload_1        
        //  1189: iload_2        
        //  1190: isub           
        //  1191: baload         
        //  1192: if_icmpne       1175
        //  1195: aload           26
        //  1197: iload_1        
        //  1198: iconst_1       
        //  1199: isub           
        //  1200: iload_2        
        //  1201: iastore        
        //  1202: iload_1        
        //  1203: iconst_1       
        //  1204: isub           
        //  1205: istore_1       
        //  1206: goto            1166
        //  1209: iload_1        
        //  1210: iconst_1       
        //  1211: isub           
        //  1212: istore_1       
        //  1213: aload           26
        //  1215: iload_1        
        //  1216: iload_2        
        //  1217: iastore        
        //  1218: goto            1171
        //  1221: iload           7
        //  1223: iconst_1       
        //  1224: isub           
        //  1225: istore_1       
        //  1226: iload_1        
        //  1227: ifge            1331
        //  1230: lload           10
        //  1232: lstore          12
        //  1234: iload_1        
        //  1235: istore          5
        //  1237: iload_1        
        //  1238: ifge            926
        //  1241: iconst_0       
        //  1242: istore          5
        //  1244: iload           5
        //  1246: istore_1       
        //  1247: iload_2        
        //  1248: ifne            1316
        //  1251: aload           17
        //  1253: iload           4
        //  1255: iconst_1       
        //  1256: isub           
        //  1257: baload         
        //  1258: istore          6
        //  1260: iload           6
        //  1262: bipush          13
        //  1264: if_icmpeq       1277
        //  1267: iload           5
        //  1269: istore_1       
        //  1270: iload           6
        //  1272: bipush          10
        //  1274: if_icmpne       1316
        //  1277: iconst_1       
        //  1278: istore          5
        //  1280: iload           5
        //  1282: istore_1       
        //  1283: iload           6
        //  1285: bipush          10
        //  1287: if_icmpne       1316
        //  1290: iload           5
        //  1292: istore_1       
        //  1293: iload           4
        //  1295: iconst_2       
        //  1296: if_icmplt       1316
        //  1299: iload           5
        //  1301: istore_1       
        //  1302: aload           17
        //  1304: iload           4
        //  1306: iconst_2       
        //  1307: isub           
        //  1308: baload         
        //  1309: bipush          13
        //  1311: if_icmpne       1316
        //  1314: iconst_2       
        //  1315: istore_1       
        //  1316: iload_2        
        //  1317: ifne            770
        //  1320: lload           10
        //  1322: lstore          12
        //  1324: iload_1        
        //  1325: ifle            1349
        //  1328: goto            770
        //  1331: aload           16
        //  1333: iload_1        
        //  1334: baload         
        //  1335: aload           23
        //  1337: iload_1        
        //  1338: baload         
        //  1339: if_icmpne       1230
        //  1342: iload_1        
        //  1343: iconst_1       
        //  1344: isub           
        //  1345: istore_1       
        //  1346: goto            1226
        //  1349: iconst_0       
        //  1350: istore          5
        //  1352: goto            926
        //  1355: iconst_0       
        //  1356: istore_2       
        //  1357: lload           12
        //  1359: lstore          10
        //  1361: iload_1        
        //  1362: istore          4
        //  1364: goto            645
        //  1367: aload           17
        //  1369: iconst_0       
        //  1370: aload           16
        //  1372: iconst_0       
        //  1373: baload         
        //  1374: bastore        
        //  1375: iconst_1       
        //  1376: istore_1       
        //  1377: goto            1355
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  2      8      219    226    Any
        //  25     36     226    240    Ljava/lang/Exception;
        //  25     36     219    226    Any
        //  40     48     226    240    Ljava/lang/Exception;
        //  40     48     219    226    Any
        //  52     60     226    240    Ljava/lang/Exception;
        //  52     60     219    226    Any
        //  64     83     226    240    Ljava/lang/Exception;
        //  64     83     219    226    Any
        //  83     98     219    226    Any
        //  98     111    219    226    Any
        //  114    123    219    226    Any
        //  128    147    219    226    Any
        //  147    158    195    209    Ljava/io/IOException;
        //  147    158    209    219    Any
        //  164    171    195    209    Ljava/io/IOException;
        //  164    171    209    219    Any
        //  185    195    195    209    Ljava/io/IOException;
        //  185    195    209    219    Any
        //  197    209    209    219    Any
        //  211    216    1128   1133   Ljava/io/IOException;
        //  211    216    219    226    Any
        //  216    219    219    226    Any
        //  228    240    219    226    Any
        //  240    256    219    226    Any
        //  256    264    195    209    Ljava/io/IOException;
        //  256    264    209    219    Any
        //  267    278    195    209    Ljava/io/IOException;
        //  267    278    209    219    Any
        //  291    301    195    209    Ljava/io/IOException;
        //  291    301    209    219    Any
        //  301    307    195    209    Ljava/io/IOException;
        //  301    307    209    219    Any
        //  320    329    1152   1161   Ljava/lang/SecurityException;
        //  320    329    195    209    Ljava/io/IOException;
        //  320    329    209    219    Any
        //  338    354    195    209    Ljava/io/IOException;
        //  338    354    209    219    Any
        //  354    367    195    209    Ljava/io/IOException;
        //  354    367    209    219    Any
        //  378    385    195    209    Ljava/io/IOException;
        //  378    385    209    219    Any
        //  400    410    195    209    Ljava/io/IOException;
        //  400    410    209    219    Any
        //  426    435    195    209    Ljava/io/IOException;
        //  426    435    209    219    Any
        //  435    454    195    209    Ljava/io/IOException;
        //  435    454    209    219    Any
        //  462    468    195    209    Ljava/io/IOException;
        //  462    468    209    219    Any
        //  489    494    1133   1138   Ljava/io/IOException;
        //  489    494    219    226    Any
        //  494    499    219    226    Any
        //  527    536    195    209    Ljava/io/IOException;
        //  527    536    209    219    Any
        //  536    543    195    209    Ljava/io/IOException;
        //  536    543    209    219    Any
        //  548    556    195    209    Ljava/io/IOException;
        //  548    556    209    219    Any
        //  561    577    195    209    Ljava/io/IOException;
        //  561    577    209    219    Any
        //  577    582    195    209    Ljava/io/IOException;
        //  577    582    209    219    Any
        //  585    593    195    209    Ljava/io/IOException;
        //  585    593    209    219    Any
        //  593    611    195    209    Ljava/io/IOException;
        //  593    611    209    219    Any
        //  619    628    195    209    Ljava/io/IOException;
        //  619    628    209    219    Any
        //  628    640    195    209    Ljava/io/IOException;
        //  628    640    209    219    Any
        //  645    658    195    209    Ljava/io/IOException;
        //  645    658    209    219    Any
        //  661    673    195    209    Ljava/io/IOException;
        //  661    673    209    219    Any
        //  680    696    195    209    Ljava/io/IOException;
        //  680    696    209    219    Any
        //  696    705    195    209    Ljava/io/IOException;
        //  696    705    209    219    Any
        //  713    722    195    209    Ljava/io/IOException;
        //  713    722    209    219    Any
        //  722    727    195    209    Ljava/io/IOException;
        //  722    727    209    219    Any
        //  738    755    195    209    Ljava/io/IOException;
        //  738    755    209    219    Any
        //  755    761    195    209    Ljava/io/IOException;
        //  755    761    209    219    Any
        //  775    791    195    209    Ljava/io/IOException;
        //  775    791    209    219    Any
        //  791    797    195    209    Ljava/io/IOException;
        //  791    797    209    219    Any
        //  809    824    195    209    Ljava/io/IOException;
        //  809    824    209    219    Any
        //  836    843    195    209    Ljava/io/IOException;
        //  836    843    209    219    Any
        //  884    890    195    209    Ljava/io/IOException;
        //  884    890    209    219    Any
        //  899    914    195    209    Ljava/io/IOException;
        //  899    914    209    219    Any
        //  926    951    195    209    Ljava/io/IOException;
        //  926    951    209    219    Any
        //  967    979    195    209    Ljava/io/IOException;
        //  967    979    209    219    Any
        //  979    991    195    209    Ljava/io/IOException;
        //  979    991    209    219    Any
        //  1031   1041   195    209    Ljava/io/IOException;
        //  1031   1041   209    219    Any
        //  1041   1054   195    209    Ljava/io/IOException;
        //  1041   1054   209    219    Any
        //  1077   1090   195    209    Ljava/io/IOException;
        //  1077   1090   209    219    Any
        //  1090   1097   195    209    Ljava/io/IOException;
        //  1090   1097   209    219    Any
        //  1102   1112   195    209    Ljava/io/IOException;
        //  1102   1112   209    219    Any
        //  1112   1125   195    209    Ljava/io/IOException;
        //  1112   1125   209    219    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 713, Size: 713
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static int readFully(final InputStream inputStream, final byte[] array, int n, int i) throws IOException {
        if (i == 0) {
            i = 0;
        }
        else {
            final int n2 = 0;
            int n3 = n;
            n = n2;
            while (i > 0) {
                final int read = inputStream.read(array, n3, i);
                if (read <= 0) {
                    break;
                }
                n3 += read;
                n += read;
                i -= read;
            }
            if ((i = n) <= 0) {
                return -1;
            }
        }
        return i;
    }
    
    private void skipFully(final InputStream inputStream, long n) throws IOException {
        while (n > 0L) {
            final long skip = inputStream.skip(n);
            if (skip <= 0L) {
                throw new EOFException("can't skip");
            }
            n -= skip;
        }
    }
    
    @Override
    public void addBodyPart(final BodyPart bodyPart) throws MessagingException {
        synchronized (this) {
            this.parse();
            super.addBodyPart(bodyPart);
        }
    }
    
    @Override
    public void addBodyPart(final BodyPart bodyPart, final int n) throws MessagingException {
        synchronized (this) {
            this.parse();
            super.addBodyPart(bodyPart, n);
        }
    }
    
    protected InternetHeaders createInternetHeaders(final InputStream inputStream) throws MessagingException {
        return new InternetHeaders(inputStream);
    }
    
    protected MimeBodyPart createMimeBodyPart(final InputStream inputStream) throws MessagingException {
        return new MimeBodyPart(inputStream);
    }
    
    protected MimeBodyPart createMimeBodyPart(final InternetHeaders internetHeaders, final byte[] array) throws MessagingException {
        return new MimeBodyPart(internetHeaders, array);
    }
    
    @Override
    public BodyPart getBodyPart(final int n) throws MessagingException {
        synchronized (this) {
            this.parse();
            return super.getBodyPart(n);
        }
    }
    
    public BodyPart getBodyPart(final String s) throws MessagingException {
        synchronized (this) {
            this.parse();
            for (int count = this.getCount(), i = 0; i < count; ++i) {
                final MimeBodyPart mimeBodyPart = (MimeBodyPart)this.getBodyPart(i);
                final String contentID = mimeBodyPart.getContentID();
                if (contentID != null && contentID.equals(s)) {
                    return mimeBodyPart;
                }
            }
            return null;
        }
    }
    
    @Override
    public int getCount() throws MessagingException {
        synchronized (this) {
            this.parse();
            return super.getCount();
        }
    }
    
    public String getPreamble() throws MessagingException {
        synchronized (this) {
            this.parse();
            return this.preamble;
        }
    }
    
    public boolean isComplete() throws MessagingException {
        synchronized (this) {
            this.parse();
            return this.complete;
        }
    }
    
    protected void parse() throws MessagingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        javax/mail/internet/MimeMultipart.parsed:Z
        //     6: istore          10
        //     8: iload           10
        //    10: ifeq            16
        //    13: aload_0        
        //    14: monitorexit    
        //    15: return         
        //    16: getstatic       javax/mail/internet/MimeMultipart.bmparse:Z
        //    19: ifeq            36
        //    22: aload_0        
        //    23: invokespecial   javax/mail/internet/MimeMultipart.parsebm:()V
        //    26: goto            13
        //    29: astore          17
        //    31: aload_0        
        //    32: monitorexit    
        //    33: aload           17
        //    35: athrow         
        //    36: aconst_null    
        //    37: astore          20
        //    39: lconst_0       
        //    40: lstore          13
        //    42: lconst_0       
        //    43: lstore          11
        //    45: aload_0        
        //    46: getfield        javax/mail/internet/MimeMultipart.ds:Ljavax/activation/DataSource;
        //    49: invokeinterface javax/activation/DataSource.getInputStream:()Ljava/io/InputStream;
        //    54: astore          17
        //    56: aload           17
        //    58: astore          19
        //    60: aload           17
        //    62: instanceof      Ljava/io/ByteArrayInputStream;
        //    65: ifne            103
        //    68: aload           17
        //    70: astore          19
        //    72: aload           17
        //    74: instanceof      Ljava/io/BufferedInputStream;
        //    77: ifne            103
        //    80: aload           17
        //    82: astore          19
        //    84: aload           17
        //    86: instanceof      Ljavax/mail/internet/SharedInputStream;
        //    89: ifne            103
        //    92: new             Ljava/io/BufferedInputStream;
        //    95: dup            
        //    96: aload           17
        //    98: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   101: astore          19
        //   103: aload           19
        //   105: instanceof      Ljavax/mail/internet/SharedInputStream;
        //   108: ifeq            118
        //   111: aload           19
        //   113: checkcast       Ljavax/mail/internet/SharedInputStream;
        //   116: astore          20
        //   118: new             Ljavax/mail/internet/ContentType;
        //   121: dup            
        //   122: aload_0        
        //   123: getfield        javax/mail/internet/MimeMultipart.contentType:Ljava/lang/String;
        //   126: invokespecial   javax/mail/internet/ContentType.<init>:(Ljava/lang/String;)V
        //   129: astore          18
        //   131: aconst_null    
        //   132: astore          17
        //   134: aload           18
        //   136: ldc             "boundary"
        //   138: invokevirtual   javax/mail/internet/ContentType.getParameter:(Ljava/lang/String;)Ljava/lang/String;
        //   141: astore          18
        //   143: aload           18
        //   145: ifnull          253
        //   148: new             Ljava/lang/StringBuilder;
        //   151: dup            
        //   152: ldc             "--"
        //   154: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   157: aload           18
        //   159: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   162: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   165: astore          17
        //   167: new             Lcom/sun/mail/util/LineInputStream;
        //   170: dup            
        //   171: aload           19
        //   173: invokespecial   com/sun/mail/util/LineInputStream.<init>:(Ljava/io/InputStream;)V
        //   176: astore          25
        //   178: aconst_null    
        //   179: astore          21
        //   181: aconst_null    
        //   182: astore          23
        //   184: aload           25
        //   186: invokevirtual   com/sun/mail/util/LineInputStream.readLine:()Ljava/lang/String;
        //   189: astore          24
        //   191: aload           24
        //   193: ifnonnull       269
        //   196: aload           17
        //   198: astore          18
        //   200: aload           24
        //   202: ifnonnull       434
        //   205: new             Ljavax/mail/MessagingException;
        //   208: dup            
        //   209: ldc             "Missing start boundary"
        //   211: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   214: athrow         
        //   215: astore          17
        //   217: new             Ljavax/mail/MessagingException;
        //   220: dup            
        //   221: ldc             "IO Error"
        //   223: aload           17
        //   225: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   228: athrow         
        //   229: astore          17
        //   231: aload           19
        //   233: invokevirtual   java/io/InputStream.close:()V
        //   236: aload           17
        //   238: athrow         
        //   239: astore          17
        //   241: new             Ljavax/mail/MessagingException;
        //   244: dup            
        //   245: ldc             "No inputstream from datasource"
        //   247: aload           17
        //   249: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;Ljava/lang/Exception;)V
        //   252: athrow         
        //   253: getstatic       javax/mail/internet/MimeMultipart.ignoreMissingBoundaryParameter:Z
        //   256: ifne            167
        //   259: new             Ljavax/mail/MessagingException;
        //   262: dup            
        //   263: ldc             "Missing boundary parameter"
        //   265: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   268: athrow         
        //   269: aload           24
        //   271: invokevirtual   java/lang/String.length:()I
        //   274: iconst_1       
        //   275: isub           
        //   276: istore_1       
        //   277: goto            1045
        //   280: aload           24
        //   282: iconst_0       
        //   283: iload_1        
        //   284: iconst_1       
        //   285: iadd           
        //   286: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   289: astore          22
        //   291: aload           17
        //   293: ifnull          413
        //   296: aload           17
        //   298: astore          18
        //   300: aload           22
        //   302: astore          24
        //   304: aload           22
        //   306: aload           17
        //   308: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   311: ifne            200
        //   314: aload           22
        //   316: invokevirtual   java/lang/String.length:()I
        //   319: istore_1       
        //   320: iload_1        
        //   321: ifle            184
        //   324: aload           23
        //   326: astore          18
        //   328: aload           23
        //   330: ifnonnull       342
        //   333: ldc             "line.separator"
        //   335: ldc             "\n"
        //   337: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   340: astore          18
        //   342: aload           21
        //   344: astore          24
        //   346: aload           21
        //   348: ifnonnull       367
        //   351: new             Ljava/lang/StringBuffer;
        //   354: dup            
        //   355: aload           22
        //   357: invokevirtual   java/lang/String.length:()I
        //   360: iconst_2       
        //   361: iadd           
        //   362: invokespecial   java/lang/StringBuffer.<init>:(I)V
        //   365: astore          24
        //   367: aload           24
        //   369: aload           22
        //   371: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   374: aload           18
        //   376: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   379: pop            
        //   380: aload           18
        //   382: astore          23
        //   384: aload           24
        //   386: astore          21
        //   388: goto            184
        //   391: aload           24
        //   393: iload_1        
        //   394: invokevirtual   java/lang/String.charAt:(I)C
        //   397: istore_2       
        //   398: iload_2        
        //   399: bipush          32
        //   401: if_icmpeq       1052
        //   404: iload_2        
        //   405: bipush          9
        //   407: if_icmpne       280
        //   410: goto            1052
        //   413: aload           22
        //   415: ldc             "--"
        //   417: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   420: ifeq            314
        //   423: aload           22
        //   425: astore          18
        //   427: aload           22
        //   429: astore          24
        //   431: goto            200
        //   434: aload           21
        //   436: ifnull          448
        //   439: aload_0        
        //   440: aload           21
        //   442: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   445: putfield        javax/mail/internet/MimeMultipart.preamble:Ljava/lang/String;
        //   448: aload           18
        //   450: invokestatic    com/sun/mail/util/ASCIIUtility.getBytes:(Ljava/lang/String;)[B
        //   453: astore          21
        //   455: aload           21
        //   457: arraylength    
        //   458: istore          9
        //   460: iconst_0       
        //   461: istore          4
        //   463: iload           4
        //   465: ifeq            481
        //   468: aload           19
        //   470: invokevirtual   java/io/InputStream.close:()V
        //   473: aload_0        
        //   474: iconst_1       
        //   475: putfield        javax/mail/internet/MimeMultipart.parsed:Z
        //   478: goto            13
        //   481: aconst_null    
        //   482: astore          17
        //   484: aload           20
        //   486: ifnull          547
        //   489: aload           20
        //   491: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   496: lstore          13
        //   498: aload           25
        //   500: invokevirtual   com/sun/mail/util/LineInputStream.readLine:()Ljava/lang/String;
        //   503: astore          18
        //   505: aload           18
        //   507: ifnull          518
        //   510: aload           18
        //   512: invokevirtual   java/lang/String.length:()I
        //   515: ifgt            498
        //   518: aload           18
        //   520: ifnonnull       555
        //   523: getstatic       javax/mail/internet/MimeMultipart.ignoreMissingEndBoundary:Z
        //   526: ifne            539
        //   529: new             Ljavax/mail/MessagingException;
        //   532: dup            
        //   533: ldc             "missing multipart end boundary"
        //   535: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   538: athrow         
        //   539: aload_0        
        //   540: iconst_0       
        //   541: putfield        javax/mail/internet/MimeMultipart.complete:Z
        //   544: goto            468
        //   547: aload_0        
        //   548: aload           19
        //   550: invokevirtual   javax/mail/internet/MimeMultipart.createInternetHeaders:(Ljava/io/InputStream;)Ljavax/mail/internet/InternetHeaders;
        //   553: astore          17
        //   555: aload           19
        //   557: invokevirtual   java/io/InputStream.markSupported:()Z
        //   560: ifne            573
        //   563: new             Ljavax/mail/MessagingException;
        //   566: dup            
        //   567: ldc             "Stream doesn't support mark"
        //   569: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   572: athrow         
        //   573: aconst_null    
        //   574: astore          18
        //   576: aload           20
        //   578: ifnonnull       696
        //   581: new             Ljava/io/ByteArrayOutputStream;
        //   584: dup            
        //   585: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //   588: astore          18
        //   590: goto            1068
        //   593: iload_1        
        //   594: istore          6
        //   596: iload_2        
        //   597: istore          5
        //   599: iload_3        
        //   600: ifeq            846
        //   603: aload           19
        //   605: iload           9
        //   607: iconst_4       
        //   608: iadd           
        //   609: sipush          1000
        //   612: iadd           
        //   613: invokevirtual   java/io/InputStream.mark:(I)V
        //   616: iconst_0       
        //   617: istore_3       
        //   618: goto            1077
        //   621: iload_3        
        //   622: iload           9
        //   624: if_icmpne       799
        //   627: aload           19
        //   629: invokevirtual   java/io/InputStream.read:()I
        //   632: istore_3       
        //   633: iload_3        
        //   634: istore          5
        //   636: iload_3        
        //   637: bipush          45
        //   639: if_icmpne       738
        //   642: iload_3        
        //   643: istore          5
        //   645: aload           19
        //   647: invokevirtual   java/io/InputStream.read:()I
        //   650: bipush          45
        //   652: if_icmpne       738
        //   655: aload_0        
        //   656: iconst_1       
        //   657: putfield        javax/mail/internet/MimeMultipart.complete:Z
        //   660: iconst_1       
        //   661: istore_3       
        //   662: aload           20
        //   664: ifnull          1019
        //   667: aload_0        
        //   668: aload           20
        //   670: lload           13
        //   672: lload           11
        //   674: invokeinterface javax/mail/internet/SharedInputStream.newStream:(JJ)Ljava/io/InputStream;
        //   679: invokevirtual   javax/mail/internet/MimeMultipart.createMimeBodyPart:(Ljava/io/InputStream;)Ljavax/mail/internet/MimeBodyPart;
        //   682: astore          17
        //   684: aload_0        
        //   685: aload           17
        //   687: invokespecial   javax/mail/Multipart.addBodyPart:(Ljavax/mail/BodyPart;)V
        //   690: iload_3        
        //   691: istore          4
        //   693: goto            463
        //   696: aload           20
        //   698: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   703: lstore          11
        //   705: goto            1068
        //   708: aload           19
        //   710: invokevirtual   java/io/InputStream.read:()I
        //   713: aload           21
        //   715: iload_3        
        //   716: baload         
        //   717: sipush          255
        //   720: iand           
        //   721: if_icmpne       621
        //   724: iload_3        
        //   725: iconst_1       
        //   726: iadd           
        //   727: istore_3       
        //   728: goto            1077
        //   731: aload           19
        //   733: invokevirtual   java/io/InputStream.read:()I
        //   736: istore          5
        //   738: iload           5
        //   740: bipush          32
        //   742: if_icmpeq       731
        //   745: iload           5
        //   747: bipush          9
        //   749: if_icmpeq       731
        //   752: iload           4
        //   754: istore_3       
        //   755: iload           5
        //   757: bipush          10
        //   759: if_icmpeq       662
        //   762: iload           5
        //   764: bipush          13
        //   766: if_icmpne       799
        //   769: aload           19
        //   771: iconst_1       
        //   772: invokevirtual   java/io/InputStream.mark:(I)V
        //   775: iload           4
        //   777: istore_3       
        //   778: aload           19
        //   780: invokevirtual   java/io/InputStream.read:()I
        //   783: bipush          10
        //   785: if_icmpeq       662
        //   788: aload           19
        //   790: invokevirtual   java/io/InputStream.reset:()V
        //   793: iload           4
        //   795: istore_3       
        //   796: goto            662
        //   799: aload           19
        //   801: invokevirtual   java/io/InputStream.reset:()V
        //   804: iload_1        
        //   805: istore          6
        //   807: iload_2        
        //   808: istore          5
        //   810: aload           18
        //   812: ifnull          846
        //   815: iload_1        
        //   816: istore          6
        //   818: iload_2        
        //   819: istore          5
        //   821: iload_1        
        //   822: iconst_m1      
        //   823: if_icmpeq       846
        //   826: aload           18
        //   828: iload_1        
        //   829: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   832: iload_2        
        //   833: iconst_m1      
        //   834: if_icmpeq       1086
        //   837: aload           18
        //   839: iload_2        
        //   840: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   843: goto            1086
        //   846: aload           19
        //   848: invokevirtual   java/io/InputStream.read:()I
        //   851: istore          7
        //   853: iload           7
        //   855: ifge            1095
        //   858: getstatic       javax/mail/internet/MimeMultipart.ignoreMissingEndBoundary:Z
        //   861: ifne            874
        //   864: new             Ljavax/mail/MessagingException;
        //   867: dup            
        //   868: ldc             "missing multipart end boundary"
        //   870: invokespecial   javax/mail/MessagingException.<init>:(Ljava/lang/String;)V
        //   873: athrow         
        //   874: aload_0        
        //   875: iconst_0       
        //   876: putfield        javax/mail/internet/MimeMultipart.complete:Z
        //   879: iconst_1       
        //   880: istore_3       
        //   881: goto            662
        //   884: iconst_1       
        //   885: istore          8
        //   887: lload           11
        //   889: lstore          15
        //   891: aload           20
        //   893: ifnull          907
        //   896: aload           20
        //   898: invokeinterface javax/mail/internet/SharedInputStream.getPosition:()J
        //   903: lconst_1       
        //   904: lsub           
        //   905: lstore          15
        //   907: iload           7
        //   909: istore          6
        //   911: iload           8
        //   913: istore_3       
        //   914: iload           6
        //   916: istore_1       
        //   917: lload           15
        //   919: lstore          11
        //   921: iload           5
        //   923: istore_2       
        //   924: iload           7
        //   926: bipush          13
        //   928: if_icmpne       593
        //   931: aload           19
        //   933: iconst_1       
        //   934: invokevirtual   java/io/InputStream.mark:(I)V
        //   937: aload           19
        //   939: invokevirtual   java/io/InputStream.read:()I
        //   942: istore_2       
        //   943: iload_2        
        //   944: bipush          10
        //   946: if_icmpne       962
        //   949: iload           8
        //   951: istore_3       
        //   952: iload           6
        //   954: istore_1       
        //   955: lload           15
        //   957: lstore          11
        //   959: goto            593
        //   962: aload           19
        //   964: invokevirtual   java/io/InputStream.reset:()V
        //   967: iload           8
        //   969: istore_3       
        //   970: iload           6
        //   972: istore_1       
        //   973: lload           15
        //   975: lstore          11
        //   977: iload           5
        //   979: istore_2       
        //   980: goto            593
        //   983: iconst_0       
        //   984: istore          8
        //   986: iload           8
        //   988: istore_3       
        //   989: iload           6
        //   991: istore_1       
        //   992: iload           5
        //   994: istore_2       
        //   995: aload           18
        //   997: ifnull          593
        //  1000: aload           18
        //  1002: iload           7
        //  1004: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //  1007: iload           8
        //  1009: istore_3       
        //  1010: iload           6
        //  1012: istore_1       
        //  1013: iload           5
        //  1015: istore_2       
        //  1016: goto            593
        //  1019: aload_0        
        //  1020: aload           17
        //  1022: aload           18
        //  1024: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //  1027: invokevirtual   javax/mail/internet/MimeMultipart.createMimeBodyPart:(Ljavax/mail/internet/InternetHeaders;[B)Ljavax/mail/internet/MimeBodyPart;
        //  1030: astore          17
        //  1032: goto            684
        //  1035: astore          18
        //  1037: goto            236
        //  1040: astore          17
        //  1042: goto            473
        //  1045: iload_1        
        //  1046: ifge            391
        //  1049: goto            280
        //  1052: iload_1        
        //  1053: iconst_1       
        //  1054: isub           
        //  1055: istore_1       
        //  1056: goto            1045
        //  1059: astore          18
        //  1061: ldc             "\n"
        //  1063: astore          18
        //  1065: goto            342
        //  1068: iconst_1       
        //  1069: istore_3       
        //  1070: iconst_m1      
        //  1071: istore_1       
        //  1072: iconst_m1      
        //  1073: istore_2       
        //  1074: goto            593
        //  1077: iload_3        
        //  1078: iload           9
        //  1080: if_icmplt       708
        //  1083: goto            621
        //  1086: iconst_m1      
        //  1087: istore          5
        //  1089: iconst_m1      
        //  1090: istore          6
        //  1092: goto            846
        //  1095: iload           7
        //  1097: bipush          13
        //  1099: if_icmpeq       884
        //  1102: iload           7
        //  1104: bipush          10
        //  1106: if_icmpne       983
        //  1109: goto            884
        //    Exceptions:
        //  throws javax.mail.MessagingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  2      8      29     36     Any
        //  16     26     29     36     Any
        //  45     56     239    253    Ljava/lang/Exception;
        //  45     56     29     36     Any
        //  60     68     239    253    Ljava/lang/Exception;
        //  60     68     29     36     Any
        //  72     80     239    253    Ljava/lang/Exception;
        //  72     80     29     36     Any
        //  84     103    239    253    Ljava/lang/Exception;
        //  84     103    29     36     Any
        //  103    118    29     36     Any
        //  118    131    29     36     Any
        //  134    143    29     36     Any
        //  148    167    29     36     Any
        //  167    178    215    229    Ljava/io/IOException;
        //  167    178    229    239    Any
        //  184    191    215    229    Ljava/io/IOException;
        //  184    191    229    239    Any
        //  205    215    215    229    Ljava/io/IOException;
        //  205    215    229    239    Any
        //  217    229    229    239    Any
        //  231    236    1035   1040   Ljava/io/IOException;
        //  231    236    29     36     Any
        //  236    239    29     36     Any
        //  241    253    29     36     Any
        //  253    269    29     36     Any
        //  269    277    215    229    Ljava/io/IOException;
        //  269    277    229    239    Any
        //  280    291    215    229    Ljava/io/IOException;
        //  280    291    229    239    Any
        //  304    314    215    229    Ljava/io/IOException;
        //  304    314    229    239    Any
        //  314    320    215    229    Ljava/io/IOException;
        //  314    320    229    239    Any
        //  333    342    1059   1068   Ljava/lang/SecurityException;
        //  333    342    215    229    Ljava/io/IOException;
        //  333    342    229    239    Any
        //  351    367    215    229    Ljava/io/IOException;
        //  351    367    229    239    Any
        //  367    380    215    229    Ljava/io/IOException;
        //  367    380    229    239    Any
        //  391    398    215    229    Ljava/io/IOException;
        //  391    398    229    239    Any
        //  413    423    215    229    Ljava/io/IOException;
        //  413    423    229    239    Any
        //  439    448    215    229    Ljava/io/IOException;
        //  439    448    229    239    Any
        //  448    460    215    229    Ljava/io/IOException;
        //  448    460    229    239    Any
        //  468    473    1040   1045   Ljava/io/IOException;
        //  468    473    29     36     Any
        //  473    478    29     36     Any
        //  489    498    215    229    Ljava/io/IOException;
        //  489    498    229    239    Any
        //  498    505    215    229    Ljava/io/IOException;
        //  498    505    229    239    Any
        //  510    518    215    229    Ljava/io/IOException;
        //  510    518    229    239    Any
        //  523    539    215    229    Ljava/io/IOException;
        //  523    539    229    239    Any
        //  539    544    215    229    Ljava/io/IOException;
        //  539    544    229    239    Any
        //  547    555    215    229    Ljava/io/IOException;
        //  547    555    229    239    Any
        //  555    573    215    229    Ljava/io/IOException;
        //  555    573    229    239    Any
        //  581    590    215    229    Ljava/io/IOException;
        //  581    590    229    239    Any
        //  603    616    215    229    Ljava/io/IOException;
        //  603    616    229    239    Any
        //  627    633    215    229    Ljava/io/IOException;
        //  627    633    229    239    Any
        //  645    660    215    229    Ljava/io/IOException;
        //  645    660    229    239    Any
        //  667    684    215    229    Ljava/io/IOException;
        //  667    684    229    239    Any
        //  684    690    215    229    Ljava/io/IOException;
        //  684    690    229    239    Any
        //  696    705    215    229    Ljava/io/IOException;
        //  696    705    229    239    Any
        //  708    724    215    229    Ljava/io/IOException;
        //  708    724    229    239    Any
        //  731    738    215    229    Ljava/io/IOException;
        //  731    738    229    239    Any
        //  769    775    215    229    Ljava/io/IOException;
        //  769    775    229    239    Any
        //  778    793    215    229    Ljava/io/IOException;
        //  778    793    229    239    Any
        //  799    804    215    229    Ljava/io/IOException;
        //  799    804    229    239    Any
        //  826    832    215    229    Ljava/io/IOException;
        //  826    832    229    239    Any
        //  837    843    215    229    Ljava/io/IOException;
        //  837    843    229    239    Any
        //  846    853    215    229    Ljava/io/IOException;
        //  846    853    229    239    Any
        //  858    874    215    229    Ljava/io/IOException;
        //  858    874    229    239    Any
        //  874    879    215    229    Ljava/io/IOException;
        //  874    879    229    239    Any
        //  896    907    215    229    Ljava/io/IOException;
        //  896    907    229    239    Any
        //  931    943    215    229    Ljava/io/IOException;
        //  931    943    229    239    Any
        //  962    967    215    229    Ljava/io/IOException;
        //  962    967    229    239    Any
        //  1000   1007   215    229    Ljava/io/IOException;
        //  1000   1007   229    239    Any
        //  1019   1032   215    229    Ljava/io/IOException;
        //  1019   1032   229    239    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0236:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void removeBodyPart(final int n) throws MessagingException {
        this.parse();
        super.removeBodyPart(n);
    }
    
    @Override
    public boolean removeBodyPart(final BodyPart bodyPart) throws MessagingException {
        this.parse();
        return super.removeBodyPart(bodyPart);
    }
    
    public void setPreamble(final String preamble) throws MessagingException {
        synchronized (this) {
            this.preamble = preamble;
        }
    }
    
    public void setSubType(final String subType) throws MessagingException {
        synchronized (this) {
            final ContentType contentType = new ContentType(this.contentType);
            contentType.setSubType(subType);
            this.contentType = contentType.toString();
        }
    }
    
    protected void updateHeaders() throws MessagingException {
        for (int i = 0; i < this.parts.size(); ++i) {
            ((MimeBodyPart)this.parts.elementAt(i)).updateHeaders();
        }
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) throws IOException, MessagingException {
        while (true) {
            while (true) {
                Label_0187: {
                    synchronized (this) {
                        this.parse();
                        final String string = "--" + new ContentType(this.contentType).getParameter("boundary");
                        final LineOutputStream lineOutputStream = new LineOutputStream(outputStream);
                        if (this.preamble == null) {
                            break Label_0187;
                        }
                        final byte[] bytes = ASCIIUtility.getBytes(this.preamble);
                        lineOutputStream.write(bytes);
                        if (bytes.length > 0 && bytes[bytes.length - 1] != 13 && bytes[bytes.length - 1] != 10) {
                            lineOutputStream.writeln();
                        }
                        break Label_0187;
                        // iftrue(Label_0149:, n < this.parts.size())
                        lineOutputStream.writeln(String.valueOf(string) + "--");
                        return;
                        Label_0149: {
                            lineOutputStream.writeln(string);
                        }
                        final int n;
                        final OutputStream outputStream2;
                        ((MimeBodyPart)this.parts.elementAt(n)).writeTo(outputStream2);
                        lineOutputStream.writeln();
                        ++n;
                        continue;
                    }
                }
                int n = 0;
                continue;
            }
        }
    }
}
