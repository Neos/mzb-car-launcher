package javax.mail.internet;

import java.util.*;
import java.text.*;

public class MailDateFormat extends SimpleDateFormat
{
    private static Calendar cal;
    static boolean debug = false;
    private static final long serialVersionUID = -8148227605210628779L;
    private static TimeZone tz;
    
    static {
        MailDateFormat.debug = false;
        MailDateFormat.tz = TimeZone.getTimeZone("GMT");
        MailDateFormat.cal = new GregorianCalendar(MailDateFormat.tz);
    }
    
    public MailDateFormat() {
        super("EEE, d MMM yyyy HH:mm:ss 'XXXXX' (z)", Locale.US);
    }
    
    private static Date ourUTC(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final boolean lenient) {
        synchronized (MailDateFormat.class) {
            MailDateFormat.cal.clear();
            MailDateFormat.cal.setLenient(lenient);
            MailDateFormat.cal.set(1, n);
            MailDateFormat.cal.set(2, n2);
            MailDateFormat.cal.set(5, n3);
            MailDateFormat.cal.set(11, n4);
            MailDateFormat.cal.set(12, n5 + n7);
            MailDateFormat.cal.set(13, n6);
            return MailDateFormat.cal.getTime();
        }
    }
    
    private static Date parseDate(final char[] array, final ParsePosition parsePosition, final boolean b) {
        while (true) {
            int number = 0;
            final boolean b2 = false;
            while (true) {
                int number3;
                try {
                    final MailDateParser mailDateParser = new MailDateParser(array);
                    mailDateParser.skipUntilNumber();
                    final int number2 = mailDateParser.parseNumber();
                    if (!mailDateParser.skipIfChar('-')) {
                        mailDateParser.skipWhiteSpace();
                    }
                    final int month = mailDateParser.parseMonth();
                    if (!mailDateParser.skipIfChar('-')) {
                        mailDateParser.skipWhiteSpace();
                    }
                    number3 = mailDateParser.parseNumber();
                    if (number3 < 50) {
                        final int n = number3 + 2000;
                        mailDateParser.skipWhiteSpace();
                        final int number4 = mailDateParser.parseNumber();
                        mailDateParser.skipChar(':');
                        final int number5 = mailDateParser.parseNumber();
                        if (mailDateParser.skipIfChar(':')) {
                            number = mailDateParser.parseNumber();
                        }
                        while (true) {
                            try {
                                mailDateParser.skipWhiteSpace();
                                final int timeZone = mailDateParser.parseTimeZone();
                                parsePosition.setIndex(mailDateParser.getIndex());
                                return ourUTC(n, month, number2, number4, number5, number, timeZone, b);
                            }
                            catch (ParseException ex2) {
                                int timeZone = b2 ? 1 : 0;
                                if (MailDateFormat.debug) {
                                    System.out.println("No timezone? : '" + new String(array) + "'");
                                    timeZone = (b2 ? 1 : 0);
                                }
                                continue;
                            }
                            break;
                        }
                    }
                }
                catch (Exception ex) {
                    if (MailDateFormat.debug) {
                        System.out.println("Bad date: '" + new String(array) + "'");
                        ex.printStackTrace();
                    }
                    parsePosition.setIndex(1);
                    return null;
                }
                int n;
                if ((n = number3) < 100) {
                    n = number3 + 1900;
                    continue;
                }
                continue;
            }
        }
    }
    
    @Override
    public StringBuffer format(final Date time, final StringBuffer sb, final FieldPosition fieldPosition) {
        final int length = sb.length();
        super.format(time, sb, fieldPosition);
        int n;
        for (n = length + 25; sb.charAt(n) != 'X'; ++n) {}
        this.calendar.clear();
        this.calendar.setTime(time);
        int n2 = this.calendar.get(15) + this.calendar.get(16);
        int n3;
        if (n2 < 0) {
            sb.setCharAt(n, '-');
            n2 = -n2;
            n3 = n + 1;
        }
        else {
            sb.setCharAt(n, '+');
            n3 = n + 1;
        }
        final int n4 = n2 / 60 / 1000;
        final int n5 = n4 / 60;
        final int n6 = n4 % 60;
        final int n7 = n3 + 1;
        sb.setCharAt(n3, Character.forDigit(n5 / 10, 10));
        final int n8 = n7 + 1;
        sb.setCharAt(n7, Character.forDigit(n5 % 10, 10));
        final int n9 = n8 + 1;
        sb.setCharAt(n8, Character.forDigit(n6 / 10, 10));
        sb.setCharAt(n9, Character.forDigit(n6 % 10, 10));
        return sb;
    }
    
    @Override
    public Date parse(final String s, final ParsePosition parsePosition) {
        return parseDate(s.toCharArray(), parsePosition, this.isLenient());
    }
    
    @Override
    public void setCalendar(final Calendar calendar) {
        throw new RuntimeException("Method setCalendar() shouldn't be called");
    }
    
    @Override
    public void setNumberFormat(final NumberFormat numberFormat) {
        throw new RuntimeException("Method setNumberFormat() shouldn't be called");
    }
}
