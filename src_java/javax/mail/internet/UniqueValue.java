package javax.mail.internet;

import javax.mail.*;

class UniqueValue
{
    private static int id;
    
    static {
        UniqueValue.id = 0;
    }
    
    public static String getUniqueBoundaryValue() {
        final StringBuffer sb = new StringBuffer();
        sb.append("----=_Part_").append(getUniqueId()).append("_").append(sb.hashCode()).append('.').append(System.currentTimeMillis());
        return sb.toString();
    }
    
    private static int getUniqueId() {
        synchronized (UniqueValue.class) {
            final int id = UniqueValue.id;
            UniqueValue.id = id + 1;
            return id;
        }
    }
    
    public static String getUniqueMessageIDValue(final Session session) {
        final InternetAddress localAddress = InternetAddress.getLocalAddress(session);
        String address;
        if (localAddress != null) {
            address = localAddress.getAddress();
        }
        else {
            address = "javamailuser@localhost";
        }
        final StringBuffer sb = new StringBuffer();
        sb.append(sb.hashCode()).append('.').append(getUniqueId()).append('.').append(System.currentTimeMillis()).append('.').append("JavaMail.").append(address);
        return sb.toString();
    }
}
