package javax.mail.internet;

import javax.mail.*;
import java.io.*;
import javax.activation.*;
import com.sun.mail.util.*;
import java.util.*;

public class MimeUtility
{
    public static final int ALL = -1;
    static final int ALL_ASCII = 1;
    static final int MOSTLY_ASCII = 2;
    static final int MOSTLY_NONASCII = 3;
    private static boolean decodeStrict;
    private static String defaultJavaCharset;
    private static String defaultMIMECharset;
    private static boolean encodeEolStrict;
    private static boolean foldEncodedWords;
    private static boolean foldText;
    private static Hashtable java2mime;
    private static Hashtable mime2java;
    
    static {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_1       
        //     2: iconst_1       
        //     3: putstatic       javax/mail/internet/MimeUtility.decodeStrict:Z
        //     6: iconst_0       
        //     7: putstatic       javax/mail/internet/MimeUtility.encodeEolStrict:Z
        //    10: iconst_0       
        //    11: putstatic       javax/mail/internet/MimeUtility.foldEncodedWords:Z
        //    14: iconst_1       
        //    15: putstatic       javax/mail/internet/MimeUtility.foldText:Z
        //    18: ldc             "mail.mime.decodetext.strict"
        //    20: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    23: astore_2       
        //    24: aload_2        
        //    25: ifnull          705
        //    28: aload_2        
        //    29: ldc             "false"
        //    31: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //    34: ifeq            705
        //    37: iconst_0       
        //    38: istore_0       
        //    39: iload_0        
        //    40: putstatic       javax/mail/internet/MimeUtility.decodeStrict:Z
        //    43: ldc             "mail.mime.encodeeol.strict"
        //    45: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    48: astore_2       
        //    49: aload_2        
        //    50: ifnull          710
        //    53: aload_2        
        //    54: ldc             "true"
        //    56: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //    59: ifeq            710
        //    62: iconst_1       
        //    63: istore_0       
        //    64: iload_0        
        //    65: putstatic       javax/mail/internet/MimeUtility.encodeEolStrict:Z
        //    68: ldc             "mail.mime.foldencodedwords"
        //    70: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    73: astore_2       
        //    74: aload_2        
        //    75: ifnull          715
        //    78: aload_2        
        //    79: ldc             "true"
        //    81: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //    84: ifeq            715
        //    87: iconst_1       
        //    88: istore_0       
        //    89: iload_0        
        //    90: putstatic       javax/mail/internet/MimeUtility.foldEncodedWords:Z
        //    93: ldc             "mail.mime.foldtext"
        //    95: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    98: astore_2       
        //    99: aload_2        
        //   100: ifnull          720
        //   103: aload_2        
        //   104: ldc             "false"
        //   106: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   109: ifeq            720
        //   112: iload_1        
        //   113: istore_0       
        //   114: iload_0        
        //   115: putstatic       javax/mail/internet/MimeUtility.foldText:Z
        //   118: new             Ljava/util/Hashtable;
        //   121: dup            
        //   122: bipush          40
        //   124: invokespecial   java/util/Hashtable.<init>:(I)V
        //   127: putstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   130: new             Ljava/util/Hashtable;
        //   133: dup            
        //   134: bipush          10
        //   136: invokespecial   java/util/Hashtable.<init>:(I)V
        //   139: putstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   142: ldc             Ljavax/mail/internet/MimeUtility;.class
        //   144: ldc             "/META-INF/javamail.charset.map"
        //   146: invokevirtual   java/lang/Class.getResourceAsStream:(Ljava/lang/String;)Ljava/io/InputStream;
        //   149: astore_2       
        //   150: aload_2        
        //   151: ifnull          191
        //   154: new             Lcom/sun/mail/util/LineInputStream;
        //   157: dup            
        //   158: aload_2        
        //   159: invokespecial   com/sun/mail/util/LineInputStream.<init>:(Ljava/io/InputStream;)V
        //   162: astore          4
        //   164: aload           4
        //   166: checkcast       Lcom/sun/mail/util/LineInputStream;
        //   169: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   172: invokestatic    javax/mail/internet/MimeUtility.loadMappings:(Lcom/sun/mail/util/LineInputStream;Ljava/util/Hashtable;)V
        //   175: aload           4
        //   177: checkcast       Lcom/sun/mail/util/LineInputStream;
        //   180: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   183: invokestatic    javax/mail/internet/MimeUtility.loadMappings:(Lcom/sun/mail/util/LineInputStream;Ljava/util/Hashtable;)V
        //   186: aload           4
        //   188: invokevirtual   java/io/InputStream.close:()V
        //   191: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   194: invokevirtual   java/util/Hashtable.isEmpty:()Z
        //   197: ifeq            585
        //   200: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   203: ldc             "8859_1"
        //   205: ldc             "ISO-8859-1"
        //   207: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   210: pop            
        //   211: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   214: ldc             "iso8859_1"
        //   216: ldc             "ISO-8859-1"
        //   218: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   221: pop            
        //   222: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   225: ldc             "iso8859-1"
        //   227: ldc             "ISO-8859-1"
        //   229: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   232: pop            
        //   233: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   236: ldc             "8859_2"
        //   238: ldc             "ISO-8859-2"
        //   240: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   243: pop            
        //   244: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   247: ldc             "iso8859_2"
        //   249: ldc             "ISO-8859-2"
        //   251: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   254: pop            
        //   255: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   258: ldc             "iso8859-2"
        //   260: ldc             "ISO-8859-2"
        //   262: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   265: pop            
        //   266: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   269: ldc             "8859_3"
        //   271: ldc             "ISO-8859-3"
        //   273: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   276: pop            
        //   277: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   280: ldc             "iso8859_3"
        //   282: ldc             "ISO-8859-3"
        //   284: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   287: pop            
        //   288: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   291: ldc             "iso8859-3"
        //   293: ldc             "ISO-8859-3"
        //   295: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   298: pop            
        //   299: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   302: ldc             "8859_4"
        //   304: ldc             "ISO-8859-4"
        //   306: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   309: pop            
        //   310: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   313: ldc             "iso8859_4"
        //   315: ldc             "ISO-8859-4"
        //   317: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   320: pop            
        //   321: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   324: ldc             "iso8859-4"
        //   326: ldc             "ISO-8859-4"
        //   328: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   331: pop            
        //   332: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   335: ldc             "8859_5"
        //   337: ldc             "ISO-8859-5"
        //   339: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   342: pop            
        //   343: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   346: ldc             "iso8859_5"
        //   348: ldc             "ISO-8859-5"
        //   350: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   353: pop            
        //   354: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   357: ldc             "iso8859-5"
        //   359: ldc             "ISO-8859-5"
        //   361: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   364: pop            
        //   365: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   368: ldc             "8859_6"
        //   370: ldc             "ISO-8859-6"
        //   372: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   375: pop            
        //   376: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   379: ldc             "iso8859_6"
        //   381: ldc             "ISO-8859-6"
        //   383: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   386: pop            
        //   387: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   390: ldc             "iso8859-6"
        //   392: ldc             "ISO-8859-6"
        //   394: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   397: pop            
        //   398: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   401: ldc             "8859_7"
        //   403: ldc             "ISO-8859-7"
        //   405: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   408: pop            
        //   409: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   412: ldc             "iso8859_7"
        //   414: ldc             "ISO-8859-7"
        //   416: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   419: pop            
        //   420: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   423: ldc             "iso8859-7"
        //   425: ldc             "ISO-8859-7"
        //   427: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   430: pop            
        //   431: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   434: ldc             "8859_8"
        //   436: ldc             "ISO-8859-8"
        //   438: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   441: pop            
        //   442: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   445: ldc             "iso8859_8"
        //   447: ldc             "ISO-8859-8"
        //   449: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   452: pop            
        //   453: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   456: ldc             "iso8859-8"
        //   458: ldc             "ISO-8859-8"
        //   460: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   463: pop            
        //   464: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   467: ldc             "8859_9"
        //   469: ldc             "ISO-8859-9"
        //   471: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   474: pop            
        //   475: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   478: ldc             "iso8859_9"
        //   480: ldc             "ISO-8859-9"
        //   482: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   485: pop            
        //   486: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   489: ldc             "iso8859-9"
        //   491: ldc             "ISO-8859-9"
        //   493: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   496: pop            
        //   497: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   500: ldc             "sjis"
        //   502: ldc             "Shift_JIS"
        //   504: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   507: pop            
        //   508: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   511: ldc             "jis"
        //   513: ldc             "ISO-2022-JP"
        //   515: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   518: pop            
        //   519: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   522: ldc             "iso2022jp"
        //   524: ldc             "ISO-2022-JP"
        //   526: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   529: pop            
        //   530: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   533: ldc             "euc_jp"
        //   535: ldc             "euc-jp"
        //   537: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   540: pop            
        //   541: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   544: ldc             "koi8_r"
        //   546: ldc             "koi8-r"
        //   548: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   551: pop            
        //   552: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   555: ldc             "euc_cn"
        //   557: ldc             "euc-cn"
        //   559: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   562: pop            
        //   563: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   566: ldc             "euc_tw"
        //   568: ldc             "euc-tw"
        //   570: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   573: pop            
        //   574: getstatic       javax/mail/internet/MimeUtility.java2mime:Ljava/util/Hashtable;
        //   577: ldc             "euc_kr"
        //   579: ldc             "euc-kr"
        //   581: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   584: pop            
        //   585: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   588: invokevirtual   java/util/Hashtable.isEmpty:()Z
        //   591: ifeq            704
        //   594: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   597: ldc             "iso-2022-cn"
        //   599: ldc             "ISO2022CN"
        //   601: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   604: pop            
        //   605: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   608: ldc             "iso-2022-kr"
        //   610: ldc             "ISO2022KR"
        //   612: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   615: pop            
        //   616: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   619: ldc             "utf-8"
        //   621: ldc             "UTF8"
        //   623: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   626: pop            
        //   627: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   630: ldc             "utf8"
        //   632: ldc             "UTF8"
        //   634: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   637: pop            
        //   638: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   641: ldc             "ja_jp.iso2022-7"
        //   643: ldc             "ISO2022JP"
        //   645: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   648: pop            
        //   649: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   652: ldc             "ja_jp.eucjp"
        //   654: ldc             "EUCJIS"
        //   656: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   659: pop            
        //   660: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   663: ldc             "euc-kr"
        //   665: ldc             "KSC5601"
        //   667: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   670: pop            
        //   671: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   674: ldc             "euckr"
        //   676: ldc             "KSC5601"
        //   678: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   681: pop            
        //   682: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   685: ldc             "us-ascii"
        //   687: ldc             "ISO-8859-1"
        //   689: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   692: pop            
        //   693: getstatic       javax/mail/internet/MimeUtility.mime2java:Ljava/util/Hashtable;
        //   696: ldc             "x-us-ascii"
        //   698: ldc             "ISO-8859-1"
        //   700: invokevirtual   java/util/Hashtable.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   703: pop            
        //   704: return         
        //   705: iconst_1       
        //   706: istore_0       
        //   707: goto            39
        //   710: iconst_0       
        //   711: istore_0       
        //   712: goto            64
        //   715: iconst_0       
        //   716: istore_0       
        //   717: goto            89
        //   720: iconst_1       
        //   721: istore_0       
        //   722: goto            114
        //   725: astore_3       
        //   726: aload_2        
        //   727: invokevirtual   java/io/InputStream.close:()V
        //   730: aload_3        
        //   731: athrow         
        //   732: astore_2       
        //   733: goto            191
        //   736: astore_2       
        //   737: goto            730
        //   740: astore_2       
        //   741: goto            191
        //   744: astore_3       
        //   745: aload           4
        //   747: astore_2       
        //   748: goto            726
        //   751: astore_2       
        //   752: goto            118
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  18     24     751    755    Ljava/lang/SecurityException;
        //  28     37     751    755    Ljava/lang/SecurityException;
        //  39     49     751    755    Ljava/lang/SecurityException;
        //  53     62     751    755    Ljava/lang/SecurityException;
        //  64     74     751    755    Ljava/lang/SecurityException;
        //  78     87     751    755    Ljava/lang/SecurityException;
        //  89     99     751    755    Ljava/lang/SecurityException;
        //  103    112    751    755    Ljava/lang/SecurityException;
        //  114    118    751    755    Ljava/lang/SecurityException;
        //  142    150    732    736    Ljava/lang/Exception;
        //  154    164    725    726    Any
        //  164    186    744    751    Any
        //  186    191    740    744    Ljava/lang/Exception;
        //  726    730    736    740    Ljava/lang/Exception;
        //  730    732    732    736    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0191:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static int checkAscii(final InputStream inputStream, int n, final boolean b) {
        final boolean b2 = false;
        final boolean b3 = false;
        int n2 = 4096;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        final boolean b7 = MimeUtility.encodeEolStrict && b;
        byte[] array = null;
        int n3 = b2 ? 1 : 0;
        int n4 = b6 ? 1 : 0;
        int n5 = b4 ? 1 : 0;
        int n6 = b5 ? 1 : 0;
        int n7 = b3 ? 1 : 0;
        int i = n;
        if (n != 0) {
            int min;
            if (n == -1) {
                min = 4096;
            }
            else {
                min = Math.min(n, 4096);
            }
            array = new byte[min];
            i = n;
            n7 = (b3 ? 1 : 0);
            n6 = (b5 ? 1 : 0);
            n5 = (b4 ? 1 : 0);
            n2 = min;
            n4 = (b6 ? 1 : 0);
            n3 = (b2 ? 1 : 0);
        }
    Label_0127:
        while (true) {
            while (i != 0) {
                int n8 = 0;
                int n9 = 0;
                int n10 = 0;
                int n11 = 0;
                Label_0339: {
                    int read = 0;
                    int n14;
                    while (true) {
                        n8 = n3;
                        n9 = n4;
                        n10 = n6;
                        n11 = n7;
                    Label_0234_Outer:
                        while (true) {
                        Label_0234:
                            while (true) {
                                int n13 = 0;
                                Label_0371: {
                                    try {
                                        read = inputStream.read(array, 0, n2);
                                        n8 = n3;
                                        n9 = n4;
                                        n10 = n6;
                                        n11 = n7;
                                        if (read != -1) {
                                            n8 = 0;
                                            final int n12 = 0;
                                            n = n3;
                                            n11 = n4;
                                            n13 = n12;
                                            n9 = n5;
                                            n10 = n6;
                                            n14 = n7;
                                            break Label_0371;
                                        }
                                        break Label_0127;
                                        int n15;
                                        n11 = (n15 = n9 + 1);
                                        int n16 = n10;
                                        // iftrue(Label_0234:, n11 <= 998)
                                        while (true) {
                                            Block_23: {
                                                break Block_23;
                                                final int n17;
                                                n8 = n17;
                                                ++n13;
                                                final int n18;
                                                n11 = n18;
                                                n9 = n15;
                                                n10 = n16;
                                                break Label_0371;
                                            }
                                            n16 = 1;
                                            n15 = n11;
                                            break Label_0234;
                                            Label_0328: {
                                                ++n;
                                            }
                                            continue Label_0234_Outer;
                                            while (true) {
                                                return 3;
                                                n8 = n;
                                                final int n18;
                                                n9 = n18;
                                                n10 = n16;
                                                n11 = n14;
                                                continue;
                                            }
                                            Label_0298:
                                            ++n14;
                                            continue Label_0234_Outer;
                                        }
                                    }
                                    // iftrue(Label_0298:, !b)
                                    // iftrue(Label_0328:, !nonascii(n17))
                                    catch (IOException ex) {
                                        break Label_0127;
                                    }
                                    break Label_0339;
                                }
                                if (n13 >= read) {
                                    break;
                                }
                                final int n17 = array[n13] & 0xFF;
                                int n18 = n11;
                                Label_0485: {
                                    if (b7) {
                                        if (n8 != 13 || n17 == 10) {
                                            n18 = n11;
                                            if (n8 == 13) {
                                                break Label_0485;
                                            }
                                            n18 = n11;
                                            if (n17 != 10) {
                                                break Label_0485;
                                            }
                                        }
                                        n18 = 1;
                                    }
                                }
                                if (n17 == 13 || n17 == 10) {
                                    final int n15 = 0;
                                    final int n16 = n10;
                                    continue Label_0234;
                                }
                                break;
                            }
                            continue Label_0234_Outer;
                        }
                    }
                    n3 = n;
                    n4 = n11;
                    n5 = n9;
                    n6 = n10;
                    n7 = n14;
                    if (i != -1) {
                        i -= read;
                        n3 = n;
                        n4 = n11;
                        n5 = n9;
                        n6 = n10;
                        n7 = n14;
                        continue;
                    }
                    continue;
                    if (i == 0 && b) {
                        return 3;
                    }
                }
                if (n11 == 0) {
                    if (n9 != 0) {
                        return 3;
                    }
                    if (n10 != 0) {
                        return 2;
                    }
                    return 1;
                }
                else {
                    if (n8 > n11) {
                        return 2;
                    }
                    return 3;
                }
            }
            int n11 = n7;
            int n10 = n6;
            int n9 = n4;
            int n8 = n3;
            continue Label_0127;
        }
    }
    
    static int checkAscii(final String s) {
        int n = 0;
        int n2 = 0;
        for (int length = s.length(), i = 0; i < length; ++i) {
            if (nonascii(s.charAt(i))) {
                ++n2;
            }
            else {
                ++n;
            }
        }
        if (n2 == 0) {
            return 1;
        }
        if (n > n2) {
            return 2;
        }
        return 3;
    }
    
    static int checkAscii(final byte[] array) {
        int n = 0;
        int n2 = 0;
        for (int i = 0; i < array.length; ++i) {
            if (nonascii(array[i] & 0xFF)) {
                ++n2;
            }
            else {
                ++n;
            }
        }
        if (n2 == 0) {
            return 1;
        }
        if (n > n2) {
            return 2;
        }
        return 3;
    }
    
    public static InputStream decode(final InputStream inputStream, final String s) throws MessagingException {
        InputStream inputStream2;
        if (s.equalsIgnoreCase("base64")) {
            inputStream2 = new BASE64DecoderStream(inputStream);
        }
        else {
            if (s.equalsIgnoreCase("quoted-printable")) {
                return new QPDecoderStream(inputStream);
            }
            if (s.equalsIgnoreCase("uuencode") || s.equalsIgnoreCase("x-uuencode") || s.equalsIgnoreCase("x-uue")) {
                return new UUDecoderStream(inputStream);
            }
            inputStream2 = inputStream;
            if (!s.equalsIgnoreCase("binary")) {
                inputStream2 = inputStream;
                if (!s.equalsIgnoreCase("7bit")) {
                    inputStream2 = inputStream;
                    if (!s.equalsIgnoreCase("8bit")) {
                        throw new MessagingException("Unknown encoding: " + s);
                    }
                }
            }
        }
        return inputStream2;
    }
    
    private static String decodeInnerWords(final String s) throws UnsupportedEncodingException {
        int n = 0;
        final StringBuffer sb = new StringBuffer();
    Label_0106_Outer:
        while (true) {
            final int index = s.indexOf("=?", n);
            Label_0121: {
                int n2 = 0;
                Block_4: {
                    if (index >= 0) {
                        sb.append(s.substring(n, index));
                        n2 = s.indexOf(63, index + 2);
                        if (n2 >= 0) {
                            n2 = s.indexOf(63, n2 + 1);
                            if (n2 >= 0) {
                                n2 = s.indexOf("?=", n2 + 1);
                                if (n2 >= 0) {
                                    break Block_4;
                                }
                            }
                        }
                    }
                    if (n == 0) {
                        break;
                    }
                    break Label_0121;
                }
                String s2 = s.substring(index, n2 + 2);
                while (true) {
                    try {
                        s2 = decodeWord(s2);
                        sb.append(s2);
                        n = n2 + 2;
                        continue Label_0106_Outer;
                        // iftrue(Label_0140:, n >= s.length())
                        Block_7: {
                            break Block_7;
                            Label_0140: {
                                return sb.toString();
                            }
                        }
                        sb.append(s.substring(n));
                        return sb.toString();
                    }
                    catch (ParseException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
        return s;
    }
    
    public static String decodeText(String nextToken) throws UnsupportedEncodingException {
        if (nextToken.indexOf("=?") == -1) {
            return nextToken;
        }
        final StringTokenizer stringTokenizer = new StringTokenizer(nextToken, " \t\n\r", true);
        final StringBuffer sb = new StringBuffer();
        final StringBuffer sb2 = new StringBuffer();
        int endsWith = 0;
        while (stringTokenizer.hasMoreTokens()) {
            nextToken = stringTokenizer.nextToken();
            final char char1 = nextToken.charAt(0);
            if (char1 == ' ' || char1 == '\t' || char1 == '\r' || char1 == '\n') {
                sb2.append(char1);
            }
            else {
                while (true) {
                    try {
                        final String decodeWord = decodeWord(nextToken);
                        if (endsWith == 0 && sb2.length() > 0) {
                            sb.append(sb2);
                        }
                        endsWith = 1;
                        nextToken = decodeWord;
                        sb.append(nextToken);
                        sb2.setLength(0);
                    }
                    catch (ParseException ex) {
                        if (MimeUtility.decodeStrict) {
                            if (sb2.length() > 0) {
                                sb.append(sb2);
                            }
                            endsWith = 0;
                            continue;
                        }
                        final String decodeInnerWords = decodeInnerWords(nextToken);
                        if (decodeInnerWords != nextToken) {
                            if ((endsWith == 0 || !nextToken.startsWith("=?")) && sb2.length() > 0) {
                                sb.append(sb2);
                            }
                            endsWith = (nextToken.endsWith("?=") ? 1 : 0);
                            nextToken = decodeInnerWords;
                            continue;
                        }
                        if (sb2.length() > 0) {
                            sb.append(sb2);
                        }
                        endsWith = 0;
                        continue;
                    }
                    break;
                }
            }
        }
        sb.append(sb2);
        return sb.toString();
    }
    
    public static String decodeWord(String s) throws ParseException, UnsupportedEncodingException {
        if (!s.startsWith("=?")) {
            throw new ParseException("encoded word does not start with \"=?\": " + s);
        }
        final int index = s.indexOf(63, 2);
        if (index == -1) {
            throw new ParseException("encoded word does not include charset: " + s);
        }
        final String javaCharset = javaCharset(s.substring(2, index));
        final int n = index + 1;
        final int index2 = s.indexOf(63, n);
        if (index2 == -1) {
            throw new ParseException("encoded word does not include encoding: " + s);
        }
        final String substring = s.substring(n, index2);
        final int n2 = index2 + 1;
        final int index3 = s.indexOf("?=", n2);
        if (index3 == -1) {
            throw new ParseException("encoded word does not end with \"?=\": " + s);
        }
        final String substring2 = s.substring(n2, index3);
        String s2;
        try {
            if (substring2.length() <= 0) {
                goto Label_0388;
            }
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(ASCIIUtility.getBytes(substring2));
            FilterInputStream filterInputStream;
            if (substring.equalsIgnoreCase("B")) {
                filterInputStream = new BASE64DecoderStream(byteArrayInputStream);
            }
            else {
                if (!substring.equalsIgnoreCase("Q")) {
                    throw new UnsupportedEncodingException("unknown encoding: " + substring);
                }
                filterInputStream = new QDecoderStream(byteArrayInputStream);
            }
            final int available = byteArrayInputStream.available();
            if (filterInputStream.read(new byte[available], 0, available) > 0) {
                goto Label_0358;
            }
            s2 = "";
            if (index3 + 2 < s.length()) {
                final String s3 = s = s.substring(index3 + 2);
                if (!MimeUtility.decodeStrict) {
                    s = decodeInnerWords(s3);
                }
                return String.valueOf(s2) + s;
            }
        }
        catch (UnsupportedEncodingException ex) {
            throw ex;
        }
        catch (IOException ex2) {
            throw new ParseException(ex2.toString());
        }
        catch (IllegalArgumentException ex3) {
            throw new UnsupportedEncodingException(javaCharset);
        }
        return s2;
    }
    
    private static void doEncode(final String s, final boolean b, String s2, int i, final String s3, final boolean b2, final boolean b3, final StringBuffer sb) throws UnsupportedEncodingException {
        final byte[] bytes = s.getBytes(s2);
        int n;
        if (b) {
            n = BEncoderStream.encodedLength(bytes);
        }
        else {
            n = QEncoderStream.encodedLength(bytes, b3);
        }
        if (n > i) {
            final int length = s.length();
            if (length > 1) {
                doEncode(s.substring(0, length / 2), b, s2, i, s3, b2, b3, sb);
                doEncode(s.substring(length / 2, length), b, s2, i, s3, false, b3, sb);
                return;
            }
        }
        s2 = (String)new ByteArrayOutputStream();
        Label_0177: {
            if (!b) {
                break Label_0177;
            }
            FilterOutputStream filterOutputStream = new BEncoderStream((OutputStream)s2);
            while (true) {
                try {
                    while (true) {
                        filterOutputStream.write(bytes);
                        filterOutputStream.close();
                        final byte[] byteArray = ((ByteArrayOutputStream)s2).toByteArray();
                        if (!b2) {
                            if (MimeUtility.foldEncodedWords) {
                                sb.append("\r\n ");
                            }
                            else {
                                sb.append(" ");
                            }
                        }
                        sb.append(s3);
                        for (i = 0; i < byteArray.length; ++i) {
                            sb.append((char)byteArray[i]);
                        }
                        sb.append("?=");
                        return;
                        filterOutputStream = new QEncoderStream((OutputStream)s2, b3);
                        continue;
                    }
                }
                catch (IOException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    public static OutputStream encode(final OutputStream outputStream, final String s) throws MessagingException {
        if (s != null) {
            if (s.equalsIgnoreCase("base64")) {
                return new BASE64EncoderStream(outputStream);
            }
            if (s.equalsIgnoreCase("quoted-printable")) {
                return new QPEncoderStream(outputStream);
            }
            if (s.equalsIgnoreCase("uuencode") || s.equalsIgnoreCase("x-uuencode") || s.equalsIgnoreCase("x-uue")) {
                return new UUEncoderStream(outputStream);
            }
            if (!s.equalsIgnoreCase("binary") && !s.equalsIgnoreCase("7bit") && !s.equalsIgnoreCase("8bit")) {
                throw new MessagingException("Unknown encoding: " + s);
            }
        }
        return outputStream;
    }
    
    public static OutputStream encode(final OutputStream outputStream, final String s, final String s2) throws MessagingException {
        if (s != null) {
            if (s.equalsIgnoreCase("base64")) {
                return new BASE64EncoderStream(outputStream);
            }
            if (s.equalsIgnoreCase("quoted-printable")) {
                return new QPEncoderStream(outputStream);
            }
            if (s.equalsIgnoreCase("uuencode") || s.equalsIgnoreCase("x-uuencode") || s.equalsIgnoreCase("x-uue")) {
                return new UUEncoderStream(outputStream, s2);
            }
            if (!s.equalsIgnoreCase("binary") && !s.equalsIgnoreCase("7bit") && !s.equalsIgnoreCase("8bit")) {
                throw new MessagingException("Unknown encoding: " + s);
            }
        }
        return outputStream;
    }
    
    public static String encodeText(final String s) throws UnsupportedEncodingException {
        return encodeText(s, null, null);
    }
    
    public static String encodeText(final String s, final String s2, final String s3) throws UnsupportedEncodingException {
        return encodeWord(s, s2, s3, false);
    }
    
    public static String encodeWord(final String s) throws UnsupportedEncodingException {
        return encodeWord(s, null, null);
    }
    
    public static String encodeWord(final String s, final String s2, final String s3) throws UnsupportedEncodingException {
        return encodeWord(s, s2, s3, true);
    }
    
    private static String encodeWord(final String s, String s2, final String s3, final boolean b) throws UnsupportedEncodingException {
        final int checkAscii = checkAscii(s);
        if (checkAscii == 1) {
            return s;
        }
        String s4;
        String defaultMIMECharset;
        if (s2 == null) {
            s4 = getDefaultJavaCharset();
            defaultMIMECharset = getDefaultMIMECharset();
        }
        else {
            s4 = javaCharset(s2);
            defaultMIMECharset = s2;
        }
        s2 = s3;
        if (s3 == null) {
            if (checkAscii != 3) {
                s2 = "Q";
            }
            else {
                s2 = "B";
            }
        }
        boolean b2;
        if (s2.equalsIgnoreCase("B")) {
            b2 = true;
        }
        else {
            if (!s2.equalsIgnoreCase("Q")) {
                throw new UnsupportedEncodingException("Unknown transfer encoding: " + s2);
            }
            b2 = false;
        }
        final StringBuffer sb = new StringBuffer();
        doEncode(s, b2, s4, 68 - defaultMIMECharset.length(), "=?" + defaultMIMECharset + "?" + s2 + "?", true, b, sb);
        return sb.toString();
    }
    
    public static String fold(int n, final String s) {
        if (!MimeUtility.foldText) {
            return s;
        }
        int i;
        for (i = s.length() - 1; i >= 0; --i) {
            final char char1 = s.charAt(i);
            if (char1 != ' ' && char1 != '\t' && char1 != '\r' && char1 != '\n') {
                break;
            }
        }
        String s2 = s;
        if (i != s.length() - 1) {
            s2 = s.substring(0, i + 1);
        }
        if (s2.length() + n <= 76) {
            return s2;
        }
        final StringBuffer sb = new StringBuffer(s2.length() + 4);
        int n2 = 0;
        int n4;
        char char3;
        for (int n3 = n; s2.length() + n3 > 76; s2 = s2.substring(n4 + 1), n3 = 1, n2 = char3) {
            char char2;
            int n5 = 0;
            for (n4 = -1, n = 0; n < s2.length() && (n4 == -1 || n3 + n <= 76); ++n, n4 = n5) {
                char2 = s2.charAt(n);
                Label_0243: {
                    if (char2 != ' ') {
                        n5 = n4;
                        if (char2 != '\t') {
                            break Label_0243;
                        }
                    }
                    n5 = n4;
                    if (n2 != 32) {
                        n5 = n4;
                        if (n2 != 9) {
                            n5 = n;
                        }
                    }
                }
                n2 = char2;
            }
            if (n4 == -1) {
                sb.append(s2);
                s2 = "";
                break;
            }
            sb.append(s2.substring(0, n4));
            sb.append("\r\n");
            char3 = s2.charAt(n4);
            sb.append(char3);
        }
        sb.append(s2);
        return sb.toString();
    }
    
    public static String getDefaultJavaCharset() {
        Label_0050: {
            if (MimeUtility.defaultJavaCharset != null) {
                break Label_0050;
            }
            String property = null;
            while (true) {
                try {
                    property = System.getProperty("mail.mime.charset");
                    if (property != null && property.length() > 0) {
                        return MimeUtility.defaultJavaCharset = javaCharset(property);
                    }
                    try {
                        MimeUtility.defaultJavaCharset = System.getProperty("file.encoding", "8859_1");
                        return MimeUtility.defaultJavaCharset;
                    }
                    catch (SecurityException property) {
                        MimeUtility.defaultJavaCharset = new InputStreamReader(new NullInputStream()).getEncoding();
                        if (MimeUtility.defaultJavaCharset == null) {
                            MimeUtility.defaultJavaCharset = "8859_1";
                            return MimeUtility.defaultJavaCharset;
                        }
                        return MimeUtility.defaultJavaCharset;
                    }
                }
                catch (SecurityException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    static String getDefaultMIMECharset() {
        while (true) {
            if (MimeUtility.defaultMIMECharset != null) {
                break Label_0015;
            }
            try {
                MimeUtility.defaultMIMECharset = System.getProperty("mail.mime.charset");
                if (MimeUtility.defaultMIMECharset == null) {
                    MimeUtility.defaultMIMECharset = mimeCharset(getDefaultJavaCharset());
                }
                return MimeUtility.defaultMIMECharset;
            }
            catch (SecurityException ex) {
                continue;
            }
            break;
        }
    }
    
    public static String getEncoding(final DataHandler p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   javax/activation/DataHandler.getName:()Ljava/lang/String;
        //     4: ifnull          15
        //     7: aload_0        
        //     8: invokevirtual   javax/activation/DataHandler.getDataSource:()Ljavax/activation/DataSource;
        //    11: invokestatic    javax/mail/internet/MimeUtility.getEncoding:(Ljavax/activation/DataSource;)Ljava/lang/String;
        //    14: areturn        
        //    15: new             Ljavax/mail/internet/ContentType;
        //    18: dup            
        //    19: aload_0        
        //    20: invokevirtual   javax/activation/DataHandler.getContentType:()Ljava/lang/String;
        //    23: invokespecial   javax/mail/internet/ContentType.<init>:(Ljava/lang/String;)V
        //    26: astore_1       
        //    27: aload_1        
        //    28: ldc_w           "text/*"
        //    31: invokevirtual   javax/mail/internet/ContentType.match:(Ljava/lang/String;)Z
        //    34: ifeq            105
        //    37: new             Ljavax/mail/internet/AsciiOutputStream;
        //    40: dup            
        //    41: iconst_0       
        //    42: iconst_0       
        //    43: invokespecial   javax/mail/internet/AsciiOutputStream.<init>:(ZZ)V
        //    46: astore_1       
        //    47: aload_0        
        //    48: aload_1        
        //    49: invokevirtual   javax/activation/DataHandler.writeTo:(Ljava/io/OutputStream;)V
        //    52: aload_1        
        //    53: invokevirtual   javax/mail/internet/AsciiOutputStream.getAscii:()I
        //    56: tableswitch {
        //                2: 91
        //                3: 98
        //          default: 80
        //        }
        //    80: ldc_w           "base64"
        //    83: astore_0       
        //    84: aload_0        
        //    85: areturn        
        //    86: astore_0       
        //    87: ldc_w           "base64"
        //    90: areturn        
        //    91: ldc_w           "7bit"
        //    94: astore_0       
        //    95: goto            84
        //    98: ldc_w           "quoted-printable"
        //   101: astore_0       
        //   102: goto            84
        //   105: new             Ljavax/mail/internet/AsciiOutputStream;
        //   108: dup            
        //   109: iconst_1       
        //   110: getstatic       javax/mail/internet/MimeUtility.encodeEolStrict:Z
        //   113: invokespecial   javax/mail/internet/AsciiOutputStream.<init>:(ZZ)V
        //   116: astore_1       
        //   117: aload_0        
        //   118: aload_1        
        //   119: invokevirtual   javax/activation/DataHandler.writeTo:(Ljava/io/OutputStream;)V
        //   122: aload_1        
        //   123: invokevirtual   javax/mail/internet/AsciiOutputStream.getAscii:()I
        //   126: iconst_1       
        //   127: if_icmpne       137
        //   130: ldc_w           "7bit"
        //   133: astore_0       
        //   134: goto            84
        //   137: ldc_w           "base64"
        //   140: astore_0       
        //   141: goto            84
        //   144: astore_0       
        //   145: goto            52
        //   148: astore_0       
        //   149: goto            122
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  15     27     86     91     Ljava/lang/Exception;
        //  47     52     144    148    Ljava/io/IOException;
        //  117    122    148    152    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0052:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String getEncoding(final DataSource p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_0        
        //     5: invokeinterface javax/activation/DataSource.getContentType:()Ljava/lang/String;
        //    10: invokespecial   javax/mail/internet/ContentType.<init>:(Ljava/lang/String;)V
        //    13: astore_3       
        //    14: aload_0        
        //    15: invokeinterface javax/activation/DataSource.getInputStream:()Ljava/io/InputStream;
        //    20: astore_2       
        //    21: aload_3        
        //    22: ldc_w           "text/*"
        //    25: invokevirtual   javax/mail/internet/ContentType.match:(Ljava/lang/String;)Z
        //    28: ifeq            75
        //    31: iconst_0       
        //    32: istore_1       
        //    33: aload_2        
        //    34: iconst_m1      
        //    35: iload_1        
        //    36: invokestatic    javax/mail/internet/MimeUtility.checkAscii:(Ljava/io/InputStream;IZ)I
        //    39: tableswitch {
        //                2: 80
        //                3: 87
        //          default: 60
        //        }
        //    60: ldc_w           "base64"
        //    63: astore_0       
        //    64: aload_2        
        //    65: invokevirtual   java/io/InputStream.close:()V
        //    68: aload_0        
        //    69: areturn        
        //    70: astore_0       
        //    71: ldc_w           "base64"
        //    74: areturn        
        //    75: iconst_1       
        //    76: istore_1       
        //    77: goto            33
        //    80: ldc_w           "7bit"
        //    83: astore_0       
        //    84: goto            64
        //    87: ldc_w           "quoted-printable"
        //    90: astore_0       
        //    91: goto            64
        //    94: astore_2       
        //    95: goto            68
        //    98: astore_0       
        //    99: goto            71
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      14     70     71     Ljava/lang/Exception;
        //  14     21     98     102    Ljava/lang/Exception;
        //  64     68     94     98     Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0033:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static int indexOfAny(final String s, final String s2) {
        return indexOfAny(s, s2, 0);
    }
    
    private static int indexOfAny(final String s, final String s2, int i) {
        Block_0: {
            break Block_0;
        Label_0037:
            while (true) {
                int length;
                do {
                    Label_0009: {
                        break Label_0009;
                        try {
                            length = s.length();
                            continue Label_0037;
                            while (true) {
                                ++i;
                                continue Label_0037;
                                final int index = s2.indexOf(s.charAt(i));
                                final int n = i;
                                continue;
                            }
                        }
                        // iftrue(Label_0045:, index >= 0)
                        catch (StringIndexOutOfBoundsException ex) {
                            return -1;
                        }
                    }
                    continue Label_0037;
                    Label_0045: {
                        return;
                    }
                } while (i < length);
                break;
            }
        }
        return -1;
    }
    
    public static String javaCharset(final String s) {
        if (MimeUtility.mime2java != null && s != null) {
            final String s2 = MimeUtility.mime2java.get(s.toLowerCase(Locale.ENGLISH));
            if (s2 != null) {
                return s2;
            }
        }
        return s;
    }
    
    private static void loadMappings(final LineInputStream lineInputStream, final Hashtable hashtable) {
        while (true) {
            String line;
            try {
                line = lineInputStream.readLine();
                if (line == null) {
                    return;
                }
            }
            catch (IOException ex) {
                return;
            }
            if (line.startsWith("--") && line.endsWith("--")) {
                return;
            }
            if (line.trim().length() == 0 || line.startsWith("#")) {
                continue;
            }
            final StringTokenizer stringTokenizer = new StringTokenizer(line, " \t");
            try {
                hashtable.put(stringTokenizer.nextToken().toLowerCase(Locale.ENGLISH), stringTokenizer.nextToken());
            }
            catch (NoSuchElementException ex2) {}
        }
    }
    
    public static String mimeCharset(final String s) {
        if (MimeUtility.java2mime != null && s != null) {
            final String s2 = MimeUtility.java2mime.get(s.toLowerCase(Locale.ENGLISH));
            if (s2 != null) {
                return s2;
            }
        }
        return s;
    }
    
    static final boolean nonascii(final int n) {
        return n >= 127 || (n < 32 && n != 13 && n != 10 && n != 9);
    }
    
    public static String quote(final String s, String string) {
        final int length = s.length();
        int n = 0;
        for (int i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\"' || char1 == '\\' || char1 == '\r' || char1 == '\n') {
                final StringBuffer sb = new StringBuffer(length + 3);
                sb.append('\"');
                sb.append(s.substring(0, i));
                int n2 = 0;
                while (i < length) {
                    final char char2 = s.charAt(i);
                    if ((char2 == '\"' || char2 == '\\' || char2 == '\r' || char2 == '\n') && (char2 != '\n' || n2 != 13)) {
                        sb.append('\\');
                    }
                    sb.append(char2);
                    n2 = char2;
                    ++i;
                }
                sb.append('\"');
                return sb.toString();
            }
            if (char1 < ' ' || char1 >= '\u007f' || string.indexOf(char1) >= 0) {
                n = 1;
            }
        }
        string = s;
        if (n != 0) {
            final StringBuffer sb2 = new StringBuffer(length + 2);
            sb2.append('\"').append(s).append('\"');
            string = sb2.toString();
        }
        return string;
    }
    
    public static String unfold(String s) {
        String s2;
        if (!MimeUtility.foldText) {
            s2 = s;
        }
        else {
            StringBuffer sb = null;
            while (true) {
                final int indexOfAny = indexOfAny(s, "\r\n");
                if (indexOfAny < 0) {
                    break;
                }
                final int length = s.length();
                final int n = indexOfAny + 1;
                int n2;
                if ((n2 = n) < length) {
                    n2 = n;
                    if (s.charAt(n - 1) == '\r') {
                        n2 = n;
                        if (s.charAt(n) == '\n') {
                            n2 = n + 1;
                        }
                    }
                }
                if (indexOfAny == 0 || s.charAt(indexOfAny - 1) != '\\') {
                    if (n2 < length) {
                        final char char1 = s.charAt(n2);
                        if (char1 == ' ' || char1 == '\t') {
                            int i;
                            for (i = n2 + 1; i < length; ++i) {
                                final char char2 = s.charAt(i);
                                if (char2 != ' ' && char2 != '\t') {
                                    break;
                                }
                            }
                            StringBuffer sb2;
                            if ((sb2 = sb) == null) {
                                sb2 = new StringBuffer(s.length());
                            }
                            if (indexOfAny != 0) {
                                sb2.append(s.substring(0, indexOfAny));
                                sb2.append(' ');
                            }
                            s = s.substring(i);
                            sb = sb2;
                            continue;
                        }
                    }
                    StringBuffer sb3;
                    if ((sb3 = sb) == null) {
                        sb3 = new StringBuffer(s.length());
                    }
                    sb3.append(s.substring(0, n2));
                    s = s.substring(n2);
                    sb = sb3;
                }
                else {
                    StringBuffer sb4;
                    if ((sb4 = sb) == null) {
                        sb4 = new StringBuffer(s.length());
                    }
                    sb4.append(s.substring(0, indexOfAny - 1));
                    sb4.append(s.substring(indexOfAny, n2));
                    s = s.substring(n2);
                    sb = sb4;
                }
            }
            s2 = s;
            if (sb != null) {
                sb.append(s);
                return sb.toString();
            }
        }
        return s2;
    }
    
    class NullInputStream extends InputStream
    {
        @Override
        public int read() {
            return 0;
        }
    }
}
