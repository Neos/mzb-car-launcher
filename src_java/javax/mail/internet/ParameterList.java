package javax.mail.internet;

import java.io.*;
import java.util.*;

public class ParameterList
{
    private static boolean applehack;
    private static boolean decodeParameters;
    private static boolean decodeParametersStrict;
    private static boolean encodeParameters;
    private static final char[] hex;
    private String lastName;
    private Map list;
    private Set multisegmentNames;
    private Map slist;
    
    static {
        final boolean b = true;
        ParameterList.encodeParameters = false;
        ParameterList.decodeParameters = false;
        ParameterList.decodeParametersStrict = false;
        ParameterList.applehack = false;
        while (true) {
            try {
                final String property = System.getProperty("mail.mime.encodeparameters");
                ParameterList.encodeParameters = (property != null && property.equalsIgnoreCase("true"));
                final String property2 = System.getProperty("mail.mime.decodeparameters");
                ParameterList.decodeParameters = (property2 != null && property2.equalsIgnoreCase("true"));
                final String property3 = System.getProperty("mail.mime.decodeparameters.strict");
                ParameterList.decodeParametersStrict = (property3 != null && property3.equalsIgnoreCase("true"));
                final String property4 = System.getProperty("mail.mime.applefilenames");
                ParameterList.applehack = (property4 != null && property4.equalsIgnoreCase("true") && b);
                hex = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            }
            catch (SecurityException ex) {
                continue;
            }
            break;
        }
    }
    
    public ParameterList() {
        this.list = new LinkedHashMap();
        this.lastName = null;
        if (ParameterList.decodeParameters) {
            this.multisegmentNames = new HashSet();
            this.slist = new HashMap();
        }
    }
    
    public ParameterList(final String s) throws ParseException {
        this();
        final HeaderTokenizer headerTokenizer = new HeaderTokenizer(s, "()<>@,;:\\\"\t []/?=");
        while (true) {
            final HeaderTokenizer.Token next = headerTokenizer.next();
            final int type = next.getType();
            if (type == -4) {
                break;
            }
            if ((char)type == ';') {
                final HeaderTokenizer.Token next2 = headerTokenizer.next();
                if (next2.getType() == -4) {
                    break;
                }
                if (next2.getType() != -1) {
                    throw new ParseException("Expected parameter name, got \"" + next2.getValue() + "\"");
                }
                final String lowerCase = next2.getValue().toLowerCase(Locale.ENGLISH);
                final HeaderTokenizer.Token next3 = headerTokenizer.next();
                if ((char)next3.getType() != '=') {
                    throw new ParseException("Expected '=', got \"" + next3.getValue() + "\"");
                }
                final HeaderTokenizer.Token next4 = headerTokenizer.next();
                final int type2 = next4.getType();
                if (type2 != -1 && type2 != -2) {
                    throw new ParseException("Expected parameter value, got \"" + next4.getValue() + "\"");
                }
                final String value = next4.getValue();
                this.lastName = lowerCase;
                if (ParameterList.decodeParameters) {
                    this.putEncodedName(lowerCase, value);
                }
                else {
                    this.list.put(lowerCase, value);
                }
            }
            else {
                if (!ParameterList.applehack || type != -1 || this.lastName == null || (!this.lastName.equals("name") && !this.lastName.equals("filename"))) {
                    throw new ParseException("Expected ';', got \"" + next.getValue() + "\"");
                }
                this.list.put(this.lastName, String.valueOf(this.list.get(this.lastName)) + " " + next.getValue());
            }
        }
        if (ParameterList.decodeParameters) {
            this.combineMultisegmentNames(false);
        }
    }
    
    private void combineMultisegmentNames(final boolean p0) throws ParseException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        javax/mail/internet/ParameterList.multisegmentNames:Ljava/util/Set;
        //     4: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //     9: astore          11
        //    11: aload           11
        //    13: invokeinterface java/util/Iterator.hasNext:()Z
        //    18: istore_3       
        //    19: iload_3        
        //    20: ifne            101
        //    23: iload_1        
        //    24: ifne            31
        //    27: iconst_1       
        //    28: ifeq            100
        //    31: aload_0        
        //    32: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //    35: invokeinterface java/util/Map.size:()I
        //    40: ifle            82
        //    43: aload_0        
        //    44: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //    47: invokeinterface java/util/Map.values:()Ljava/util/Collection;
        //    52: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //    57: astore          4
        //    59: aload           4
        //    61: invokeinterface java/util/Iterator.hasNext:()Z
        //    66: ifne            735
        //    69: aload_0        
        //    70: getfield        javax/mail/internet/ParameterList.list:Ljava/util/Map;
        //    73: aload_0        
        //    74: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //    77: invokeinterface java/util/Map.putAll:(Ljava/util/Map;)V
        //    82: aload_0        
        //    83: getfield        javax/mail/internet/ParameterList.multisegmentNames:Ljava/util/Set;
        //    86: invokeinterface java/util/Set.clear:()V
        //    91: aload_0        
        //    92: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //    95: invokeinterface java/util/Map.clear:()V
        //   100: return         
        //   101: aload           11
        //   103: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   108: checkcast       Ljava/lang/String;
        //   111: astore          12
        //   113: new             Ljava/lang/StringBuffer;
        //   116: dup            
        //   117: invokespecial   java/lang/StringBuffer.<init>:()V
        //   120: astore          13
        //   122: new             Ljavax/mail/internet/ParameterList$MultiValue;
        //   125: dup            
        //   126: aconst_null    
        //   127: invokespecial   javax/mail/internet/ParameterList$MultiValue.<init>:(Ljavax/mail/internet/ParameterList$MultiValue;)V
        //   130: astore          14
        //   132: iconst_0       
        //   133: istore_2       
        //   134: aconst_null    
        //   135: astore          5
        //   137: new             Ljava/lang/StringBuilder;
        //   140: dup            
        //   141: aload           12
        //   143: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   146: invokespecial   java/lang/StringBuilder.<init>:(Ljava/lang/String;)V
        //   149: ldc             "*"
        //   151: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   154: iload_2        
        //   155: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   158: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   161: astore          15
        //   163: aload_0        
        //   164: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //   167: aload           15
        //   169: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   174: astore          16
        //   176: aload           16
        //   178: ifnonnull       282
        //   181: iload_2        
        //   182: ifne            651
        //   185: aload_0        
        //   186: getfield        javax/mail/internet/ParameterList.list:Ljava/util/Map;
        //   189: aload           12
        //   191: invokeinterface java/util/Map.remove:(Ljava/lang/Object;)Ljava/lang/Object;
        //   196: pop            
        //   197: goto            11
        //   200: astore          4
        //   202: iload_1        
        //   203: ifne            210
        //   206: iconst_0       
        //   207: ifeq            279
        //   210: aload_0        
        //   211: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //   214: invokeinterface java/util/Map.size:()I
        //   219: ifle            261
        //   222: aload_0        
        //   223: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //   226: invokeinterface java/util/Map.values:()Ljava/util/Collection;
        //   231: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //   236: astore          5
        //   238: aload           5
        //   240: invokeinterface java/util/Iterator.hasNext:()Z
        //   245: ifne            678
        //   248: aload_0        
        //   249: getfield        javax/mail/internet/ParameterList.list:Ljava/util/Map;
        //   252: aload_0        
        //   253: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //   256: invokeinterface java/util/Map.putAll:(Ljava/util/Map;)V
        //   261: aload_0        
        //   262: getfield        javax/mail/internet/ParameterList.multisegmentNames:Ljava/util/Set;
        //   265: invokeinterface java/util/Set.clear:()V
        //   270: aload_0        
        //   271: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //   274: invokeinterface java/util/Map.clear:()V
        //   279: aload           4
        //   281: athrow         
        //   282: aload           14
        //   284: aload           16
        //   286: invokevirtual   javax/mail/internet/ParameterList$MultiValue.add:(Ljava/lang/Object;)Z
        //   289: pop            
        //   290: aconst_null    
        //   291: astore          6
        //   293: aconst_null    
        //   294: astore          10
        //   296: aconst_null    
        //   297: astore          4
        //   299: aload           16
        //   301: instanceof      Ljavax/mail/internet/ParameterList$Value;
        //   304: istore_3       
        //   305: iload_3        
        //   306: ifeq            633
        //   309: aload           4
        //   311: astore          8
        //   313: aload           6
        //   315: astore          9
        //   317: aload           10
        //   319: astore          7
        //   321: aload           16
        //   323: checkcast       Ljavax/mail/internet/ParameterList$Value;
        //   326: astore          16
        //   328: aload           4
        //   330: astore          8
        //   332: aload           6
        //   334: astore          9
        //   336: aload           10
        //   338: astore          7
        //   340: aload           16
        //   342: getfield        javax/mail/internet/ParameterList$Value.encodedValue:Ljava/lang/String;
        //   345: astore          6
        //   347: aload           6
        //   349: astore          4
        //   351: iload_2        
        //   352: ifne            461
        //   355: aload           4
        //   357: astore          8
        //   359: aload           4
        //   361: astore          9
        //   363: aload           4
        //   365: astore          7
        //   367: aload           6
        //   369: invokestatic    javax/mail/internet/ParameterList.decodeValue:(Ljava/lang/String;)Ljavax/mail/internet/ParameterList$Value;
        //   372: astore          10
        //   374: aload           4
        //   376: astore          8
        //   378: aload           4
        //   380: astore          9
        //   382: aload           4
        //   384: astore          7
        //   386: aload           10
        //   388: getfield        javax/mail/internet/ParameterList$Value.charset:Ljava/lang/String;
        //   391: astore          6
        //   393: aload           4
        //   395: astore          8
        //   397: aload           4
        //   399: astore          9
        //   401: aload           4
        //   403: astore          7
        //   405: aload           16
        //   407: aload           6
        //   409: putfield        javax/mail/internet/ParameterList$Value.charset:Ljava/lang/String;
        //   412: aload           10
        //   414: getfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   417: astore          5
        //   419: aload           16
        //   421: aload           5
        //   423: putfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   426: aload           6
        //   428: astore          4
        //   430: aload           13
        //   432: aload           5
        //   434: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   437: pop            
        //   438: aload_0        
        //   439: getfield        javax/mail/internet/ParameterList.slist:Ljava/util/Map;
        //   442: aload           15
        //   444: invokeinterface java/util/Map.remove:(Ljava/lang/Object;)Ljava/lang/Object;
        //   449: pop            
        //   450: iload_2        
        //   451: iconst_1       
        //   452: iadd           
        //   453: istore_2       
        //   454: aload           4
        //   456: astore          5
        //   458: goto            137
        //   461: aload           5
        //   463: ifnonnull       526
        //   466: aload           4
        //   468: astore          8
        //   470: aload           4
        //   472: astore          9
        //   474: aload           4
        //   476: astore          7
        //   478: aload_0        
        //   479: getfield        javax/mail/internet/ParameterList.multisegmentNames:Ljava/util/Set;
        //   482: aload           12
        //   484: invokeinterface java/util/Set.remove:(Ljava/lang/Object;)Z
        //   489: pop            
        //   490: goto            181
        //   493: astore          7
        //   495: aload           5
        //   497: astore          6
        //   499: aload           8
        //   501: astore          5
        //   503: aload           6
        //   505: astore          4
        //   507: getstatic       javax/mail/internet/ParameterList.decodeParametersStrict:Z
        //   510: ifeq            430
        //   513: new             Ljavax/mail/internet/ParseException;
        //   516: dup            
        //   517: aload           7
        //   519: invokevirtual   java/lang/NumberFormatException.toString:()Ljava/lang/String;
        //   522: invokespecial   javax/mail/internet/ParseException.<init>:(Ljava/lang/String;)V
        //   525: athrow         
        //   526: aload           4
        //   528: astore          8
        //   530: aload           4
        //   532: astore          9
        //   534: aload           4
        //   536: astore          7
        //   538: aload           6
        //   540: aload           5
        //   542: invokestatic    javax/mail/internet/ParameterList.decodeBytes:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   545: astore          6
        //   547: aload           4
        //   549: astore          8
        //   551: aload           4
        //   553: astore          9
        //   555: aload           4
        //   557: astore          7
        //   559: aload           16
        //   561: aload           6
        //   563: putfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   566: aload           5
        //   568: astore          4
        //   570: aload           6
        //   572: astore          5
        //   574: goto            430
        //   577: astore          7
        //   579: aload           5
        //   581: astore          6
        //   583: aload           9
        //   585: astore          5
        //   587: aload           6
        //   589: astore          4
        //   591: getstatic       javax/mail/internet/ParameterList.decodeParametersStrict:Z
        //   594: ifeq            430
        //   597: new             Ljavax/mail/internet/ParseException;
        //   600: dup            
        //   601: aload           7
        //   603: invokevirtual   java/io/UnsupportedEncodingException.toString:()Ljava/lang/String;
        //   606: invokespecial   javax/mail/internet/ParseException.<init>:(Ljava/lang/String;)V
        //   609: athrow         
        //   610: aload           6
        //   612: astore          4
        //   614: getstatic       javax/mail/internet/ParameterList.decodeParametersStrict:Z
        //   617: ifeq            430
        //   620: new             Ljavax/mail/internet/ParseException;
        //   623: dup            
        //   624: aload           8
        //   626: invokevirtual   java/lang/StringIndexOutOfBoundsException.toString:()Ljava/lang/String;
        //   629: invokespecial   javax/mail/internet/ParseException.<init>:(Ljava/lang/String;)V
        //   632: athrow         
        //   633: aload           16
        //   635: checkcast       Ljava/lang/String;
        //   638: astore          6
        //   640: aload           5
        //   642: astore          4
        //   644: aload           6
        //   646: astore          5
        //   648: goto            430
        //   651: aload           14
        //   653: aload           13
        //   655: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   658: putfield        javax/mail/internet/ParameterList$MultiValue.value:Ljava/lang/String;
        //   661: aload_0        
        //   662: getfield        javax/mail/internet/ParameterList.list:Ljava/util/Map;
        //   665: aload           12
        //   667: aload           14
        //   669: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   674: pop            
        //   675: goto            11
        //   678: aload           5
        //   680: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   685: astore          6
        //   687: aload           6
        //   689: instanceof      Ljavax/mail/internet/ParameterList$Value;
        //   692: ifeq            238
        //   695: aload           6
        //   697: checkcast       Ljavax/mail/internet/ParameterList$Value;
        //   700: astore          6
        //   702: aload           6
        //   704: getfield        javax/mail/internet/ParameterList$Value.encodedValue:Ljava/lang/String;
        //   707: invokestatic    javax/mail/internet/ParameterList.decodeValue:(Ljava/lang/String;)Ljavax/mail/internet/ParameterList$Value;
        //   710: astore          7
        //   712: aload           6
        //   714: aload           7
        //   716: getfield        javax/mail/internet/ParameterList$Value.charset:Ljava/lang/String;
        //   719: putfield        javax/mail/internet/ParameterList$Value.charset:Ljava/lang/String;
        //   722: aload           6
        //   724: aload           7
        //   726: getfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   729: putfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   732: goto            238
        //   735: aload           4
        //   737: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   742: astore          5
        //   744: aload           5
        //   746: instanceof      Ljavax/mail/internet/ParameterList$Value;
        //   749: ifeq            59
        //   752: aload           5
        //   754: checkcast       Ljavax/mail/internet/ParameterList$Value;
        //   757: astore          5
        //   759: aload           5
        //   761: getfield        javax/mail/internet/ParameterList$Value.encodedValue:Ljava/lang/String;
        //   764: invokestatic    javax/mail/internet/ParameterList.decodeValue:(Ljava/lang/String;)Ljavax/mail/internet/ParameterList$Value;
        //   767: astore          6
        //   769: aload           5
        //   771: aload           6
        //   773: getfield        javax/mail/internet/ParameterList$Value.charset:Ljava/lang/String;
        //   776: putfield        javax/mail/internet/ParameterList$Value.charset:Ljava/lang/String;
        //   779: aload           5
        //   781: aload           6
        //   783: getfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   786: putfield        javax/mail/internet/ParameterList$Value.value:Ljava/lang/String;
        //   789: goto            59
        //   792: astore          8
        //   794: aload           4
        //   796: astore          5
        //   798: goto            610
        //   801: astore          7
        //   803: aload           4
        //   805: astore          5
        //   807: goto            587
        //   810: astore          7
        //   812: aload           4
        //   814: astore          5
        //   816: goto            503
        //   819: astore          8
        //   821: aload           5
        //   823: astore          6
        //   825: aload           7
        //   827: astore          5
        //   829: goto            610
        //    Exceptions:
        //  throws javax.mail.internet.ParseException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                       
        //  -----  -----  -----  -----  -------------------------------------------
        //  0      11     200    735    Any
        //  11     19     200    735    Any
        //  101    132    200    735    Any
        //  137    176    200    735    Any
        //  185    197    200    735    Any
        //  282    290    200    735    Any
        //  299    305    200    735    Any
        //  321    328    493    503    Ljava/lang/NumberFormatException;
        //  321    328    577    587    Ljava/io/UnsupportedEncodingException;
        //  321    328    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  321    328    200    735    Any
        //  340    347    493    503    Ljava/lang/NumberFormatException;
        //  340    347    577    587    Ljava/io/UnsupportedEncodingException;
        //  340    347    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  340    347    200    735    Any
        //  367    374    493    503    Ljava/lang/NumberFormatException;
        //  367    374    577    587    Ljava/io/UnsupportedEncodingException;
        //  367    374    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  367    374    200    735    Any
        //  386    393    493    503    Ljava/lang/NumberFormatException;
        //  386    393    577    587    Ljava/io/UnsupportedEncodingException;
        //  386    393    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  386    393    200    735    Any
        //  405    412    493    503    Ljava/lang/NumberFormatException;
        //  405    412    577    587    Ljava/io/UnsupportedEncodingException;
        //  405    412    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  405    412    200    735    Any
        //  412    426    810    819    Ljava/lang/NumberFormatException;
        //  412    426    801    810    Ljava/io/UnsupportedEncodingException;
        //  412    426    792    801    Ljava/lang/StringIndexOutOfBoundsException;
        //  412    426    200    735    Any
        //  430    450    200    735    Any
        //  478    490    493    503    Ljava/lang/NumberFormatException;
        //  478    490    577    587    Ljava/io/UnsupportedEncodingException;
        //  478    490    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  478    490    200    735    Any
        //  507    526    200    735    Any
        //  538    547    493    503    Ljava/lang/NumberFormatException;
        //  538    547    577    587    Ljava/io/UnsupportedEncodingException;
        //  538    547    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  538    547    200    735    Any
        //  559    566    493    503    Ljava/lang/NumberFormatException;
        //  559    566    577    587    Ljava/io/UnsupportedEncodingException;
        //  559    566    819    832    Ljava/lang/StringIndexOutOfBoundsException;
        //  559    566    200    735    Any
        //  591    610    200    735    Any
        //  614    633    200    735    Any
        //  633    640    200    735    Any
        //  651    675    200    735    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0430:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static String decodeBytes(final String s, final String s2) throws UnsupportedEncodingException {
        final byte[] array = new byte[s.length()];
        int i;
        int n;
        int n2;
        for (i = 0, n = 0; i < s.length(); i = n2 + 1, ++n) {
            char char1 = s.charAt(i);
            n2 = i;
            if (char1 == '%') {
                char1 = (char)Integer.parseInt(s.substring(i + 1, i + 3), 16);
                n2 = i + 2;
            }
            array[n] = (byte)char1;
        }
        return new String(array, 0, n, MimeUtility.javaCharset(s2));
    }
    
    private static Value decodeValue(final String s) throws ParseException {
        final Value value = new Value(null);
        value.encodedValue = s;
        value.value = s;
        try {
            if (s.indexOf(39) <= 0 && ParameterList.decodeParametersStrict) {
                throw new ParseException("Missing charset in encoded value: " + s);
            }
            goto Label_0080;
        }
        catch (NumberFormatException ex) {
            if (ParameterList.decodeParametersStrict) {
                throw new ParseException(ex.toString());
            }
            goto Label_0185;
        }
        catch (UnsupportedEncodingException ex2) {
            if (ParameterList.decodeParametersStrict) {
                throw new ParseException(ex2.toString());
            }
            goto Label_0185;
        }
        catch (StringIndexOutOfBoundsException ex3) {
            if (ParameterList.decodeParametersStrict) {
                throw new ParseException(ex3.toString());
            }
            goto Label_0185;
        }
    }
    
    private static Value encodeValue(final String value, final String charset) {
        if (MimeUtility.checkAscii(value) == 1) {
            return null;
        }
        while (true) {
            while (true) {
                byte[] bytes;
                StringBuffer sb;
                int n;
                try {
                    bytes = value.getBytes(MimeUtility.javaCharset(charset));
                    sb = new StringBuffer(bytes.length + charset.length() + 2);
                    sb.append(charset).append("''");
                    n = 0;
                    if (n >= bytes.length) {
                        final Value value2 = new Value(null);
                        value2.charset = charset;
                        value2.value = value;
                        value2.encodedValue = sb.toString();
                        return value2;
                    }
                }
                catch (UnsupportedEncodingException ex) {
                    return null;
                }
                final char c = (char)(bytes[n] & 0xFF);
                if (c <= ' ' || c >= '\u007f' || c == '*' || c == '\'' || c == '%' || "()<>@,;:\\\"\t []/?=".indexOf(c) >= 0) {
                    sb.append('%').append(ParameterList.hex[c >> 4]).append(ParameterList.hex[c & '\u000f']);
                }
                else {
                    sb.append(c);
                }
                ++n;
                continue;
            }
        }
    }
    
    private void putEncodedName(String s, String s2) throws ParseException {
        final int index = s.indexOf(42);
        if (index < 0) {
            this.list.put(s, s2);
            return;
        }
        if (index == s.length() - 1) {
            s = s.substring(0, index);
            this.list.put(s, decodeValue(s2));
            return;
        }
        final String substring = s.substring(0, index);
        this.multisegmentNames.add(substring);
        this.list.put(substring, "");
        if (s.endsWith("*")) {
            final Object o = new Value(null);
            ((Value)o).encodedValue = s2;
            ((Value)o).value = s2;
            s = s.substring(0, s.length() - 1);
            s2 = (String)o;
        }
        this.slist.put(s, s2);
    }
    
    private static String quote(final String s) {
        return MimeUtility.quote(s, "()<>@,;:\\\"\t []/?=");
    }
    
    public String get(final String s) {
        final String value = this.list.get(s.trim().toLowerCase(Locale.ENGLISH));
        if (value instanceof MultiValue) {
            return ((MultiValue)value).value;
        }
        if (value instanceof Value) {
            return ((Value)value).value;
        }
        return value;
    }
    
    public Enumeration getNames() {
        return new ParamEnum(this.list.keySet().iterator());
    }
    
    public void remove(final String s) {
        this.list.remove(s.trim().toLowerCase(Locale.ENGLISH));
    }
    
    public void set(String lowerCase, final String s) {
        Label_0042: {
            if (lowerCase != null || s == null || !s.equals("DONE")) {
                break Label_0042;
            }
            if (!ParameterList.decodeParameters || this.multisegmentNames.size() <= 0) {
                return;
            }
            try {
                this.combineMultisegmentNames(true);
                return;
                lowerCase = lowerCase.trim().toLowerCase(Locale.ENGLISH);
                // iftrue(Label_0080:, !ParameterList.decodeParameters)
                while (true) {
                    Block_7: {
                        break Block_7;
                        this.list.put(lowerCase, s);
                        return;
                    }
                    try {
                        this.putEncodedName(lowerCase, s);
                        return;
                    }
                    catch (ParseException ex) {
                        this.list.put(lowerCase, s);
                        return;
                    }
                    continue;
                }
            }
            catch (ParseException ex2) {}
        }
    }
    
    public void set(final String s, final String s2, final String s3) {
        if (!ParameterList.encodeParameters) {
            this.set(s, s2);
            return;
        }
        final Value encodeValue = encodeValue(s2, s3);
        if (encodeValue != null) {
            this.list.put(s.trim().toLowerCase(Locale.ENGLISH), encodeValue);
            return;
        }
        this.set(s, s2);
    }
    
    public int size() {
        return this.list.size();
    }
    
    @Override
    public String toString() {
        return this.toString(0);
    }
    
    public String toString(int i) {
        final ToStringBuffer toStringBuffer = new ToStringBuffer(i);
        for (final String s : this.list.keySet()) {
            final Object value = this.list.get(s);
            if (value instanceof MultiValue) {
                final MultiValue multiValue = (MultiValue)value;
                final String string = String.valueOf(s) + "*";
                Value value2;
                for (i = 0; i < multiValue.size(); ++i) {
                    value2 = multiValue.get(i);
                    if (value2 instanceof Value) {
                        toStringBuffer.addNV(String.valueOf(string) + i + "*", value2.encodedValue);
                    }
                    else {
                        toStringBuffer.addNV(String.valueOf(string) + i, (String)value2);
                    }
                }
            }
            else if (value instanceof Value) {
                toStringBuffer.addNV(String.valueOf(s) + "*", ((Value)value).encodedValue);
            }
            else {
                toStringBuffer.addNV(s, (String)value);
            }
        }
        return toStringBuffer.toString();
    }
    
    private static class MultiValue extends ArrayList
    {
        String value;
    }
    
    private static class ParamEnum implements Enumeration
    {
        private Iterator it;
        
        ParamEnum(final Iterator it) {
            this.it = it;
        }
        
        @Override
        public boolean hasMoreElements() {
            return this.it.hasNext();
        }
        
        @Override
        public Object nextElement() {
            return this.it.next();
        }
    }
    
    private static class ToStringBuffer
    {
        private StringBuffer sb;
        private int used;
        
        public ToStringBuffer(final int used) {
            this.sb = new StringBuffer();
            this.used = used;
        }
        
        public void addNV(String fold, String access$0) {
            access$0 = quote(access$0);
            this.sb.append("; ");
            this.used += 2;
            if (this.used + (fold.length() + access$0.length() + 1) > 76) {
                this.sb.append("\r\n\t");
                this.used = 8;
            }
            this.sb.append(fold).append('=');
            this.used += fold.length() + 1;
            if (this.used + access$0.length() <= 76) {
                this.sb.append(access$0);
                this.used += access$0.length();
                return;
            }
            fold = MimeUtility.fold(this.used, access$0);
            this.sb.append(fold);
            final int lastIndex = fold.lastIndexOf(10);
            if (lastIndex >= 0) {
                this.used += fold.length() - lastIndex - 1;
                return;
            }
            this.used += fold.length();
        }
        
        @Override
        public String toString() {
            return this.sb.toString();
        }
    }
    
    private static class Value
    {
        String charset;
        String encodedValue;
        String value;
    }
}
