package javax.mail.internet;

import javax.mail.*;
import java.util.*;

public class NewsAddress extends Address
{
    private static final long serialVersionUID = -4203797299824684143L;
    protected String host;
    protected String newsgroup;
    
    public NewsAddress() {
    }
    
    public NewsAddress(final String s) {
        this(s, null);
    }
    
    public NewsAddress(final String newsgroup, final String host) {
        this.newsgroup = newsgroup;
        this.host = host;
    }
    
    public static NewsAddress[] parse(final String s) throws AddressException {
        final StringTokenizer stringTokenizer = new StringTokenizer(s, ",");
        final Vector<NewsAddress> vector = new Vector<NewsAddress>();
        while (stringTokenizer.hasMoreTokens()) {
            vector.addElement(new NewsAddress(stringTokenizer.nextToken()));
        }
        final int size = vector.size();
        final NewsAddress[] array = new NewsAddress[size];
        if (size > 0) {
            vector.copyInto(array);
        }
        return array;
    }
    
    public static String toString(final Address[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        final StringBuffer sb = new StringBuffer(((NewsAddress)array[0]).toString());
        for (int i = 1; i < array.length; ++i) {
            sb.append(",").append(((NewsAddress)array[i]).toString());
        }
        return sb.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof NewsAddress) {
            final NewsAddress newsAddress = (NewsAddress)o;
            if (this.newsgroup.equals(newsAddress.newsgroup) && ((this.host == null && newsAddress.host == null) || (this.host != null && newsAddress.host != null && this.host.equalsIgnoreCase(newsAddress.host)))) {
                return true;
            }
        }
        return false;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public String getNewsgroup() {
        return this.newsgroup;
    }
    
    @Override
    public String getType() {
        return "news";
    }
    
    @Override
    public int hashCode() {
        int n = 0;
        if (this.newsgroup != null) {
            n = 0 + this.newsgroup.hashCode();
        }
        int n2 = n;
        if (this.host != null) {
            n2 = n + this.host.toLowerCase(Locale.ENGLISH).hashCode();
        }
        return n2;
    }
    
    public void setHost(final String host) {
        this.host = host;
    }
    
    public void setNewsgroup(final String newsgroup) {
        this.newsgroup = newsgroup;
    }
    
    @Override
    public String toString() {
        return this.newsgroup;
    }
}
