package javax.mail.internet;

public class AddressException extends ParseException
{
    private static final long serialVersionUID = 9134583443539323120L;
    protected int pos;
    protected String ref;
    
    public AddressException() {
        this.ref = null;
        this.pos = -1;
    }
    
    public AddressException(final String s) {
        super(s);
        this.ref = null;
        this.pos = -1;
    }
    
    public AddressException(final String s, final String ref) {
        super(s);
        this.ref = null;
        this.pos = -1;
        this.ref = ref;
    }
    
    public AddressException(final String s, final String ref, final int pos) {
        super(s);
        this.ref = null;
        this.pos = -1;
        this.ref = ref;
        this.pos = pos;
    }
    
    public int getPos() {
        return this.pos;
    }
    
    public String getRef() {
        return this.ref;
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        if (this.ref == null) {
            return string;
        }
        final String string2 = String.valueOf(string) + " in string ``" + this.ref + "''";
        if (this.pos < 0) {
            return string2;
        }
        return String.valueOf(string2) + " at position " + this.pos;
    }
}
