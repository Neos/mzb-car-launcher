package javax.mail.internet;

import java.io.*;

class AsciiOutputStream extends OutputStream
{
    private int ascii;
    private boolean badEOL;
    private boolean breakOnNonAscii;
    private boolean checkEOL;
    private int lastb;
    private int linelen;
    private boolean longLine;
    private int non_ascii;
    private int ret;
    
    public AsciiOutputStream(final boolean breakOnNonAscii, final boolean b) {
        final boolean b2 = false;
        this.ascii = 0;
        this.non_ascii = 0;
        this.linelen = 0;
        this.longLine = false;
        this.badEOL = false;
        this.checkEOL = false;
        this.lastb = 0;
        this.ret = 0;
        this.breakOnNonAscii = breakOnNonAscii;
        boolean checkEOL = b2;
        if (b) {
            checkEOL = b2;
            if (breakOnNonAscii) {
                checkEOL = true;
            }
        }
        this.checkEOL = checkEOL;
    }
    
    private final void check(int lastb) throws IOException {
        lastb &= 0xFF;
        if (this.checkEOL && ((this.lastb == 13 && lastb != 10) || (this.lastb != 13 && lastb == 10))) {
            this.badEOL = true;
        }
        if (lastb == 13 || lastb == 10) {
            this.linelen = 0;
        }
        else {
            ++this.linelen;
            if (this.linelen > 998) {
                this.longLine = true;
            }
        }
        if (MimeUtility.nonascii(lastb)) {
            ++this.non_ascii;
            if (this.breakOnNonAscii) {
                this.ret = 3;
                throw new EOFException();
            }
        }
        else {
            ++this.ascii;
        }
        this.lastb = lastb;
    }
    
    public int getAscii() {
        final int n = 3;
        int ret;
        if (this.ret != 0) {
            ret = this.ret;
        }
        else {
            ret = n;
            if (!this.badEOL) {
                if (this.non_ascii == 0) {
                    if (this.longLine) {
                        return 2;
                    }
                    return 1;
                }
                else {
                    ret = n;
                    if (this.ascii > this.non_ascii) {
                        return 2;
                    }
                }
            }
        }
        return ret;
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.check(n);
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, final int n, final int n2) throws IOException {
        for (int i = n; i < n2 + n; ++i) {
            this.check(array[i]);
        }
    }
}
