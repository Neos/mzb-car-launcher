package javax.mail.internet;

import javax.activation.*;
import javax.mail.*;
import java.io.*;
import java.net.*;

public class MimePartDataSource implements DataSource, MessageAware
{
    private static boolean ignoreMultipartEncoding;
    private MessageContext context;
    protected MimePart part;
    
    static {
        final boolean b = true;
        MimePartDataSource.ignoreMultipartEncoding = true;
        try {
            final String property = System.getProperty("mail.mime.ignoremultipartencoding");
            boolean ignoreMultipartEncoding = b;
            if (property != null) {
                ignoreMultipartEncoding = b;
                if (property.equalsIgnoreCase("false")) {
                    ignoreMultipartEncoding = false;
                }
            }
            MimePartDataSource.ignoreMultipartEncoding = ignoreMultipartEncoding;
        }
        catch (SecurityException ex) {}
    }
    
    public MimePartDataSource(final MimePart part) {
        this.part = part;
    }
    
    private static String restrictEncoding(final String s, final MimePart mimePart) throws MessagingException {
        if (MimePartDataSource.ignoreMultipartEncoding && s != null && !s.equalsIgnoreCase("7bit") && !s.equalsIgnoreCase("8bit") && !s.equalsIgnoreCase("binary")) {
            final String contentType = mimePart.getContentType();
            if (contentType != null) {
                try {
                    final ContentType contentType2 = new ContentType(contentType);
                    if (contentType2.match("multipart/*") || contentType2.match("message/*")) {
                        return null;
                    }
                }
                catch (ParseException ex) {
                    return s;
                }
            }
        }
        return s;
    }
    
    @Override
    public String getContentType() {
        try {
            return this.part.getContentType();
        }
        catch (MessagingException ex) {
            return "application/octet-stream";
        }
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        InputStream inputStream;
        try {
            if (this.part instanceof MimeBodyPart) {
                inputStream = ((MimeBodyPart)this.part).getContentStream();
            }
            else {
                if (!(this.part instanceof MimeMessage)) {
                    throw new MessagingException("Unknown part");
                }
                inputStream = ((MimeMessage)this.part).getContentStream();
            }
            final String restrictEncoding = restrictEncoding(this.part.getEncoding(), this.part);
            if (restrictEncoding != null) {
                return MimeUtility.decode(inputStream, restrictEncoding);
            }
        }
        catch (MessagingException ex) {
            throw new IOException(ex.getMessage());
        }
        return inputStream;
    }
    
    @Override
    public MessageContext getMessageContext() {
        synchronized (this) {
            if (this.context == null) {
                this.context = new MessageContext(this.part);
            }
            return this.context;
        }
    }
    
    @Override
    public String getName() {
        try {
            if (this.part instanceof MimeBodyPart) {
                return ((MimeBodyPart)this.part).getFileName();
            }
        }
        catch (MessagingException ex) {}
        return "";
    }
    
    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new UnknownServiceException();
    }
}
