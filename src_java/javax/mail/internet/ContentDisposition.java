package javax.mail.internet;

public class ContentDisposition
{
    private String disposition;
    private ParameterList list;
    
    public ContentDisposition() {
    }
    
    public ContentDisposition(String remainder) throws ParseException {
        final HeaderTokenizer headerTokenizer = new HeaderTokenizer(remainder, "()<>@,;:\\\"\t []/?=");
        final HeaderTokenizer.Token next = headerTokenizer.next();
        if (next.getType() != -1) {
            throw new ParseException();
        }
        this.disposition = next.getValue();
        remainder = headerTokenizer.getRemainder();
        if (remainder != null) {
            this.list = new ParameterList(remainder);
        }
    }
    
    public ContentDisposition(final String disposition, final ParameterList list) {
        this.disposition = disposition;
        this.list = list;
    }
    
    public String getDisposition() {
        return this.disposition;
    }
    
    public String getParameter(final String s) {
        if (this.list == null) {
            return null;
        }
        return this.list.get(s);
    }
    
    public ParameterList getParameterList() {
        return this.list;
    }
    
    public void setDisposition(final String disposition) {
        this.disposition = disposition;
    }
    
    public void setParameter(final String s, final String s2) {
        if (this.list == null) {
            this.list = new ParameterList();
        }
        this.list.set(s, s2);
    }
    
    public void setParameterList(final ParameterList list) {
        this.list = list;
    }
    
    @Override
    public String toString() {
        if (this.disposition == null) {
            return null;
        }
        if (this.list == null) {
            return this.disposition;
        }
        final StringBuffer sb = new StringBuffer(this.disposition);
        sb.append(this.list.toString(sb.length() + 21));
        return sb.toString();
    }
}
