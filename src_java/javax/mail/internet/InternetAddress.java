package javax.mail.internet;

import java.io.*;
import javax.mail.*;
import java.net.*;
import java.util.*;

public class InternetAddress extends Address implements Cloneable
{
    private static final String rfc822phrase;
    private static final long serialVersionUID = -7507595530758302903L;
    private static final String specialsNoDot = "()<>,;:\\\"[]@";
    private static final String specialsNoDotNoAt = "()<>,;:\\\"[]";
    protected String address;
    protected String encodedPersonal;
    protected String personal;
    
    static {
        rfc822phrase = "()<>@,;:\\\"\t .[]".replace(' ', '\0').replace('\t', '\0');
    }
    
    public InternetAddress() {
    }
    
    public InternetAddress(final String s) throws AddressException {
        final InternetAddress[] parse = parse(s, true);
        if (parse.length != 1) {
            throw new AddressException("Illegal address", s);
        }
        this.address = parse[0].address;
        this.personal = parse[0].personal;
        this.encodedPersonal = parse[0].encodedPersonal;
    }
    
    public InternetAddress(final String s, final String s2) throws UnsupportedEncodingException {
        this(s, s2, null);
    }
    
    public InternetAddress(final String address, final String s, final String s2) throws UnsupportedEncodingException {
        this.address = address;
        this.setPersonal(s, s2);
    }
    
    public InternetAddress(final String s, final boolean b) throws AddressException {
        this(s);
        if (b) {
            checkAddress(this.address, true, true);
        }
    }
    
    private static void checkAddress(final String s, final boolean b, final boolean b2) throws AddressException {
        int n = 0;
        if (s.indexOf(34) < 0) {
            Label_0032: {
                if (b) {
                    n = 0;
                    int indexOfAny = 0;
                    Block_5: {
                        while (true) {
                            indexOfAny = indexOfAny(s, ",:", n);
                            if (indexOfAny < 0) {
                                break Label_0032;
                            }
                            if (s.charAt(n) != '@') {
                                break;
                            }
                            if (s.charAt(indexOfAny) == ':') {
                                break Block_5;
                            }
                            n = indexOfAny + 1;
                        }
                        throw new AddressException("Illegal route-addr", s);
                    }
                    n = indexOfAny + 1;
                }
            }
            final int index = s.indexOf(64, n);
            String substring;
            String substring2;
            if (index >= 0) {
                if (index == n) {
                    throw new AddressException("Missing local name", s);
                }
                if (index == s.length() - 1) {
                    throw new AddressException("Missing domain", s);
                }
                substring = s.substring(n, index);
                substring2 = s.substring(index + 1);
            }
            else {
                if (b2) {
                    throw new AddressException("Missing final '@domain'", s);
                }
                substring = s;
                substring2 = null;
            }
            if (indexOfAny(s, " \t\n\r") >= 0) {
                throw new AddressException("Illegal whitespace in address", s);
            }
            if (indexOfAny(substring, "()<>,;:\\\"[]@") >= 0) {
                throw new AddressException("Illegal character in local name", s);
            }
            if (substring2 != null && substring2.indexOf(91) < 0 && indexOfAny(substring2, "()<>,;:\\\"[]@") >= 0) {
                throw new AddressException("Illegal character in domain", s);
            }
        }
    }
    
    public static InternetAddress getLocalAddress(final Session session) {
        String property = null;
        String s = null;
        String s2 = null;
        Label_0096: {
            if (session != null) {
                break Label_0096;
            }
            try {
                property = System.getProperty("user.name");
                s = InetAddress.getLocalHost().getHostName();
                // iftrue(Label_0132:, property2 == null)
                // iftrue(Label_0186:, property3 == null)
                // iftrue(Label_0023:, property3.length() != 0)
                // iftrue(Label_0023:, s2 = property4 != null)
                // iftrue(Label_0139:, property2.length() != 0)
                // iftrue(Label_0160:, property6.length() != 0)
                // iftrue(Label_0023:, localHost == null)
                while (true) {
                Label_0153_Outer:
                    while (true) {
                        String property3 = null;
                        String property4 = null;
                        String property5 = null;
                    Label_0132_Outer:
                        while (true) {
                            String property6;
                            while (true) {
                                String string;
                                String property2 = null;
                                final InetAddress localHost;
                                Block_10:Label_0160_Outer:
                                while (true) {
                                    while (true) {
                                    Block_12:
                                        while (true) {
                                            if ((string = s2) == null) {
                                                string = s2;
                                                if (property != null) {
                                                    string = s2;
                                                    if (property.length() != 0) {
                                                        string = s2;
                                                        if (s != null) {
                                                            string = s2;
                                                            if (s.length() != 0) {
                                                                string = String.valueOf(property) + "@" + s;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (string != null) {
                                                return new InternetAddress(string);
                                            }
                                            goto Label_0220;
                                            property2 = session.getProperty("mail.user");
                                            break Block_10;
                                            property3 = session.getProperty("mail.host");
                                            break Block_12;
                                            s = localHost.getHostName();
                                            s2 = property4;
                                            property = property5;
                                            continue Label_0160_Outer;
                                        }
                                        s2 = property4;
                                        s = property3;
                                        property = property5;
                                        break Label_0132_Outer;
                                        property5 = System.getProperty("user.name");
                                        continue Label_0153_Outer;
                                    }
                                    property6 = session.getProperty("user.name");
                                    break Label_0153_Outer;
                                    property4 = session.getProperty("mail.from");
                                    continue Label_0160_Outer;
                                }
                                property6 = property2;
                                continue;
                            }
                            property5 = property6;
                            continue Label_0132_Outer;
                        }
                        final InetAddress localHost = InetAddress.getLocalHost();
                        s2 = property4;
                        s = property3;
                        property = property5;
                        continue;
                    }
                    continue;
                }
            }
            // iftrue(Label_0153:, property6 == null)
            catch (UnknownHostException ex) {}
            catch (AddressException ex2) {
                goto Label_0220;
            }
            catch (SecurityException ex3) {
                goto Label_0220;
            }
        }
    }
    
    private static int indexOfAny(final String s, final String s2) {
        return indexOfAny(s, s2, 0);
    }
    
    private static int indexOfAny(final String s, final String s2, int i) {
        Block_0: {
            break Block_0;
        Label_0037:
            while (true) {
                int length;
                do {
                    Label_0009: {
                        break Label_0009;
                        try {
                            length = s.length();
                            continue Label_0037;
                            while (true) {
                                ++i;
                                continue Label_0037;
                                final int index = s2.indexOf(s.charAt(i));
                                final int n = i;
                                continue;
                            }
                        }
                        // iftrue(Label_0045:, index >= 0)
                        catch (StringIndexOutOfBoundsException ex) {
                            return -1;
                        }
                    }
                    continue Label_0037;
                    Label_0045: {
                        return;
                    }
                } while (i < length);
                break;
            }
        }
        return -1;
    }
    
    private boolean isSimple() {
        return this.address == null || indexOfAny(this.address, "()<>,;:\\\"[]") < 0;
    }
    
    private static int lengthOfFirstSegment(final String s) {
        final int index = s.indexOf("\r\n");
        if (index != -1) {
            return index;
        }
        return s.length();
    }
    
    private static int lengthOfLastSegment(final String s, final int n) {
        final int lastIndex = s.lastIndexOf("\r\n");
        if (lastIndex != -1) {
            return s.length() - lastIndex - 2;
        }
        return s.length() + n;
    }
    
    public static InternetAddress[] parse(final String s) throws AddressException {
        return parse(s, true);
    }
    
    public static InternetAddress[] parse(final String s, final boolean b) throws AddressException {
        return parse(s, b, false);
    }
    
    private static InternetAddress[] parse(final String s, final boolean b, final boolean b2) throws AddressException {
        int n = -1;
        int n2 = -1;
        final int length = s.length();
        int n3 = 0;
        boolean b3 = false;
        int n4 = 0;
        final Vector<InternetAddress> vector = new Vector<InternetAddress>();
        int n5 = -1;
        int n6 = -1;
        int i = 0;
    Label_0387_Outer:
        while (i < length) {
            int n7 = n5;
            int n8 = n2;
            int n9 = n3;
            int j = i;
            int n10 = n4;
            boolean b4 = b3;
            int n11 = n6;
            int n12 = n;
            while (true) {
                switch (s.charAt(i)) {
                    default: {
                        n7 = n5;
                        n8 = n2;
                        n9 = n3;
                        j = i;
                        n10 = n4;
                        b4 = b3;
                        n11 = n6;
                        n12 = n;
                        if (n6 == -1) {
                            n11 = i;
                            n12 = n;
                            b4 = b3;
                            n10 = n4;
                            j = i;
                            n9 = n3;
                            n8 = n2;
                            n7 = n5;
                        }
                        break Label_0387;
                    }
                    case '\t':
                    case '\n':
                    case '\r':
                    case ' ': {
                        i = j + 1;
                        n5 = n7;
                        n2 = n8;
                        n3 = n9;
                        n4 = n10;
                        b3 = b4;
                        n6 = n11;
                        n = n12;
                        continue Label_0387_Outer;
                    }
                    case '(': {
                        final boolean b5 = true;
                        int n13 = n5;
                        if (n6 >= 0 && (n13 = n5) == -1) {
                            n13 = i;
                        }
                        int n14;
                        if ((n14 = n) == -1) {
                            n14 = i + 1;
                        }
                        int n15;
                        int n16;
                        for (n15 = i + 1, n16 = 1; n15 < length && n16 > 0; ++n15) {
                            switch (s.charAt(n15)) {
                                case '\\': {
                                    ++n15;
                                    break;
                                }
                                case '(': {
                                    ++n16;
                                    break;
                                }
                                case ')': {
                                    --n16;
                                    break;
                                }
                            }
                        }
                        if (n16 > 0) {
                            throw new AddressException("Missing ')'", s, n15);
                        }
                        final int n17 = n15 - 1;
                        n7 = n13;
                        n8 = n2;
                        n9 = n3;
                        j = n17;
                        n10 = (b5 ? 1 : 0);
                        b4 = b3;
                        n11 = n6;
                        n12 = n14;
                        if (n2 == -1) {
                            n8 = n17;
                            n7 = n13;
                            n9 = n3;
                            j = n17;
                            n10 = (b5 ? 1 : 0);
                            b4 = b3;
                            n11 = n6;
                            n12 = n14;
                        }
                        continue;
                    }
                    case ')': {
                        throw new AddressException("Missing '('", s, i);
                    }
                    case '<': {
                        n10 = 1;
                        if (b3) {
                            throw new AddressException("Extra route-addr", s, i);
                        }
                        n8 = n2;
                        n11 = n6;
                        if (n3 == 0) {
                            n = n6;
                            if (n >= 0) {
                                n2 = i;
                            }
                            n11 = i + 1;
                            n8 = n2;
                        }
                        final boolean b6 = false;
                        j = i + 1;
                        int n18 = b6 ? 1 : 0;
                    Label_0726:
                        while (j < length) {
                            switch (s.charAt(j)) {
                                case '\\': {
                                    ++j;
                                    break;
                                }
                                case '\"': {
                                    if (n18 != 0) {
                                        n18 = 0;
                                    }
                                    else {
                                        n18 = 1;
                                    }
                                    break;
                                }
                                case '>': {
                                    if (n18 != 0) {
                                        break;
                                    }
                                    break Label_0726;
                                }
                            }
                            ++j;
                        }
                        if (j < length) {
                            b4 = true;
                            n7 = j;
                            n9 = n3;
                            n12 = n;
                            continue;
                        }
                        if (n18 != 0) {
                            throw new AddressException("Missing '\"'", s, j);
                        }
                        throw new AddressException("Missing '>'", s, j);
                    }
                    case '>': {
                        throw new AddressException("Missing '<'", s, i);
                    }
                    case '\"': {
                        n10 = 1;
                        n11 = n6;
                        if (n6 == -1) {
                            n11 = i;
                        }
                        int k = 0;
                    Label_0900:
                        for (k = i + 1; k < length; ++k) {
                            switch (s.charAt(k)) {
                                case '\"': {
                                    break Label_0900;
                                }
                                case '\\': {
                                    ++k;
                                    break;
                                }
                            }
                        }
                        n7 = n5;
                        n8 = n2;
                        n9 = n3;
                        j = k;
                        b4 = b3;
                        n12 = n;
                        if (k >= length) {
                            throw new AddressException("Missing '\"'", s, k);
                        }
                        continue;
                    }
                    case '[': {
                        n10 = 1;
                        int l = 0;
                    Label_0999:
                        for (l = i + 1; l < length; ++l) {
                            switch (s.charAt(l)) {
                                case ']': {
                                    break Label_0999;
                                }
                                case '\\': {
                                    ++l;
                                    break;
                                }
                            }
                        }
                        n7 = n5;
                        n8 = n2;
                        n9 = n3;
                        j = l;
                        b4 = b3;
                        n11 = n6;
                        n12 = n;
                        if (l >= length) {
                            throw new AddressException("Missing ']'", s, l);
                        }
                        continue;
                    }
                    case ',': {
                        if (n6 == -1) {
                            b4 = false;
                            n10 = 0;
                            n7 = -1;
                            n11 = -1;
                            n8 = n2;
                            n9 = n3;
                            j = i;
                            n12 = n;
                            continue;
                        }
                        if (n3 != 0) {
                            b4 = false;
                            n7 = n5;
                            n8 = n2;
                            n9 = n3;
                            j = i;
                            n10 = n4;
                            n11 = n6;
                            n12 = n;
                            continue;
                        }
                        int n19;
                        if ((n19 = n5) == -1) {
                            n19 = i;
                        }
                        final String trim = s.substring(n6, n19).trim();
                        if (n4 != 0 || b || b2) {
                            if (b || !b2) {
                                checkAddress(trim, b3, false);
                            }
                            final InternetAddress internetAddress = new InternetAddress();
                            internetAddress.setAddress(trim);
                            n8 = n2;
                            if ((n12 = n) >= 0) {
                                internetAddress.encodedPersonal = unquote(s.substring(n, n2).trim());
                                n8 = -1;
                                n12 = -1;
                            }
                            vector.addElement(internetAddress);
                        }
                        else {
                            final StringTokenizer stringTokenizer = new StringTokenizer(trim);
                            while (true) {
                                n8 = n2;
                                n12 = n;
                                if (!stringTokenizer.hasMoreTokens()) {
                                    break;
                                }
                                final String nextToken = stringTokenizer.nextToken();
                                checkAddress(nextToken, false, false);
                                final InternetAddress internetAddress2 = new InternetAddress();
                                internetAddress2.setAddress(nextToken);
                                vector.addElement(internetAddress2);
                            }
                        }
                        b4 = false;
                        n10 = 0;
                        n7 = -1;
                        n11 = -1;
                        n9 = n3;
                        j = i;
                        continue;
                    }
                    case ':': {
                        final boolean b7 = true;
                        if (n3 != 0) {
                            throw new AddressException("Nested group", s, i);
                        }
                        final boolean b8 = true;
                        n7 = n5;
                        n8 = n2;
                        n9 = (b8 ? 1 : 0);
                        j = i;
                        n10 = (b7 ? 1 : 0);
                        b4 = b3;
                        n11 = n6;
                        n12 = n;
                        if (n6 == -1) {
                            n11 = i;
                            n7 = n5;
                            n8 = n2;
                            n9 = (b8 ? 1 : 0);
                            j = i;
                            n10 = (b7 ? 1 : 0);
                            b4 = b3;
                            n12 = n;
                        }
                        continue;
                    }
                    case ';': {
                        int n20 = n6;
                        if (n6 == -1) {
                            n20 = i;
                        }
                        if (n3 == 0) {
                            throw new AddressException("Illegal semicolon, not in group", s, i);
                        }
                        n9 = 0;
                        int n21;
                        if ((n21 = n20) == -1) {
                            n21 = i;
                        }
                        final InternetAddress internetAddress3 = new InternetAddress();
                        internetAddress3.setAddress(s.substring(n21, i + 1).trim());
                        vector.addElement(internetAddress3);
                        b4 = false;
                        n7 = -1;
                        n11 = -1;
                        n8 = n2;
                        j = i;
                        n10 = n4;
                        n12 = n;
                        continue;
                    }
                }
                break;
            }
        }
        if (n6 >= 0) {
            int n22;
            if ((n22 = n5) == -1) {
                n22 = i;
            }
            final String trim2 = s.substring(n6, n22).trim();
            if (n4 != 0 || b || b2) {
                if (b || !b2) {
                    checkAddress(trim2, b3, false);
                }
                final InternetAddress internetAddress4 = new InternetAddress();
                internetAddress4.setAddress(trim2);
                if (n >= 0) {
                    internetAddress4.encodedPersonal = unquote(s.substring(n, n2).trim());
                }
                vector.addElement(internetAddress4);
            }
            else {
                final StringTokenizer stringTokenizer2 = new StringTokenizer(trim2);
                while (stringTokenizer2.hasMoreTokens()) {
                    final String nextToken2 = stringTokenizer2.nextToken();
                    checkAddress(nextToken2, false, false);
                    final InternetAddress internetAddress5 = new InternetAddress();
                    internetAddress5.setAddress(nextToken2);
                    vector.addElement(internetAddress5);
                }
            }
        }
        final InternetAddress[] array = new InternetAddress[vector.size()];
        vector.copyInto(array);
        return array;
    }
    
    public static InternetAddress[] parseHeader(final String s, final boolean b) throws AddressException {
        return parse(s, b, true);
    }
    
    private static String quotePhrase(final String s) {
        final int length = s.length();
        int n = 0;
        for (int i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\"' || char1 == '\\') {
                final StringBuffer sb = new StringBuffer(length + 3);
                sb.append('\"');
                for (int j = 0; j < length; ++j) {
                    final char char2 = s.charAt(j);
                    if (char2 == '\"' || char2 == '\\') {
                        sb.append('\\');
                    }
                    sb.append(char2);
                }
                sb.append('\"');
                return sb.toString();
            }
            if ((char1 < ' ' && char1 != '\r' && char1 != '\n' && char1 != '\t') || char1 >= '\u007f' || InternetAddress.rfc822phrase.indexOf(char1) >= 0) {
                n = 1;
            }
        }
        String string = s;
        if (n != 0) {
            final StringBuffer sb2 = new StringBuffer(length + 2);
            sb2.append('\"').append(s).append('\"');
            string = sb2.toString();
        }
        return string;
    }
    
    public static String toString(final Address[] array) {
        return toString(array, 0);
    }
    
    public static String toString(final Address[] array, int lengthOfLastSegment) {
        if (array == null || array.length == 0) {
            return null;
        }
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            int n = lengthOfLastSegment;
            if (i != 0) {
                sb.append(", ");
                n = lengthOfLastSegment + 2;
            }
            final String string = array[i].toString();
            lengthOfLastSegment = n;
            if (n + lengthOfFirstSegment(string) > 76) {
                sb.append("\r\n\t");
                lengthOfLastSegment = 8;
            }
            sb.append(string);
            lengthOfLastSegment = lengthOfLastSegment(string, lengthOfLastSegment);
        }
        return sb.toString();
    }
    
    private static String unquote(String s) {
        String s2 = s;
        if (s.startsWith("\"")) {
            s2 = s;
            if (s.endsWith("\"")) {
                s = (s2 = s.substring(1, s.length() - 1));
                if (s.indexOf(92) >= 0) {
                    final StringBuffer sb = new StringBuffer(s.length());
                    int n;
                    for (int i = 0; i < s.length(); i = n + 1) {
                        char c2;
                        final char c = c2 = s.charAt(i);
                        n = i;
                        if (c == '\\') {
                            c2 = c;
                            if ((n = i) < s.length() - 1) {
                                n = i + 1;
                                c2 = s.charAt(n);
                            }
                        }
                        sb.append(c2);
                    }
                    s2 = sb.toString();
                }
            }
        }
        return s2;
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException ex) {
            return null;
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof InternetAddress) {
            final String address = ((InternetAddress)o).getAddress();
            if (address == this.address) {
                return true;
            }
            if (this.address != null && this.address.equalsIgnoreCase(address)) {
                return true;
            }
        }
        return false;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public InternetAddress[] getGroup(final boolean b) throws AddressException {
        final String address = this.getAddress();
        if (address.endsWith(";")) {
            final int index = address.indexOf(58);
            if (index >= 0) {
                return parseHeader(address.substring(index + 1, address.length() - 1), b);
            }
        }
        return null;
    }
    
    public String getPersonal() {
        if (this.personal != null) {
            return this.personal;
        }
        if (this.encodedPersonal != null) {
            try {
                return this.personal = MimeUtility.decodeText(this.encodedPersonal);
            }
            catch (Exception ex) {
                return this.encodedPersonal;
            }
        }
        return null;
    }
    
    @Override
    public String getType() {
        return "rfc822";
    }
    
    @Override
    public int hashCode() {
        if (this.address == null) {
            return 0;
        }
        return this.address.toLowerCase(Locale.ENGLISH).hashCode();
    }
    
    public boolean isGroup() {
        return this.address != null && this.address.endsWith(";") && this.address.indexOf(58) > 0;
    }
    
    public void setAddress(final String address) {
        this.address = address;
    }
    
    public void setPersonal(final String personal) throws UnsupportedEncodingException {
        this.personal = personal;
        if (personal != null) {
            this.encodedPersonal = MimeUtility.encodeWord(personal);
            return;
        }
        this.encodedPersonal = null;
    }
    
    public void setPersonal(final String personal, final String s) throws UnsupportedEncodingException {
        this.personal = personal;
        if (personal != null) {
            this.encodedPersonal = MimeUtility.encodeWord(personal, s, null);
            return;
        }
        this.encodedPersonal = null;
    }
    
    @Override
    public String toString() {
        while (true) {
            if (this.encodedPersonal != null || this.personal == null) {
                break Label_0025;
            }
            try {
                this.encodedPersonal = MimeUtility.encodeWord(this.personal);
                if (this.encodedPersonal != null) {
                    return String.valueOf(quotePhrase(this.encodedPersonal)) + " <" + this.address + ">";
                }
                if (this.isGroup() || this.isSimple()) {
                    return this.address;
                }
                return "<" + this.address + ">";
            }
            catch (UnsupportedEncodingException ex) {
                continue;
            }
            break;
        }
    }
    
    public String toUnicodeString() {
        final String personal = this.getPersonal();
        if (personal != null) {
            return String.valueOf(quotePhrase(personal)) + " <" + this.address + ">";
        }
        if (this.isGroup() || this.isSimple()) {
            return this.address;
        }
        return "<" + this.address + ">";
    }
    
    public void validate() throws AddressException {
        checkAddress(this.getAddress(), true, true);
    }
}
