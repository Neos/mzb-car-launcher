package javax.mail.internet;

import java.util.*;
import com.sun.mail.util.*;
import java.io.*;
import javax.mail.*;

public class InternetHeaders
{
    protected List headers;
    
    public InternetHeaders() {
        (this.headers = new ArrayList(40)).add(new InternetHeader("Return-Path", null));
        this.headers.add(new InternetHeader("Received", null));
        this.headers.add(new InternetHeader("Resent-Date", null));
        this.headers.add(new InternetHeader("Resent-From", null));
        this.headers.add(new InternetHeader("Resent-Sender", null));
        this.headers.add(new InternetHeader("Resent-To", null));
        this.headers.add(new InternetHeader("Resent-Cc", null));
        this.headers.add(new InternetHeader("Resent-Bcc", null));
        this.headers.add(new InternetHeader("Resent-Message-Id", null));
        this.headers.add(new InternetHeader("Date", null));
        this.headers.add(new InternetHeader("From", null));
        this.headers.add(new InternetHeader("Sender", null));
        this.headers.add(new InternetHeader("Reply-To", null));
        this.headers.add(new InternetHeader("To", null));
        this.headers.add(new InternetHeader("Cc", null));
        this.headers.add(new InternetHeader("Bcc", null));
        this.headers.add(new InternetHeader("Message-Id", null));
        this.headers.add(new InternetHeader("In-Reply-To", null));
        this.headers.add(new InternetHeader("References", null));
        this.headers.add(new InternetHeader("Subject", null));
        this.headers.add(new InternetHeader("Comments", null));
        this.headers.add(new InternetHeader("Keywords", null));
        this.headers.add(new InternetHeader("Errors-To", null));
        this.headers.add(new InternetHeader("MIME-Version", null));
        this.headers.add(new InternetHeader("Content-Type", null));
        this.headers.add(new InternetHeader("Content-Transfer-Encoding", null));
        this.headers.add(new InternetHeader("Content-MD5", null));
        this.headers.add(new InternetHeader(":", null));
        this.headers.add(new InternetHeader("Content-Length", null));
        this.headers.add(new InternetHeader("Status", null));
    }
    
    public InternetHeaders(final InputStream inputStream) throws MessagingException {
        this.headers = new ArrayList(40);
        this.load(inputStream);
    }
    
    public void addHeader(final String s, final String s2) {
        int size = this.headers.size();
        boolean b;
        if (!s.equalsIgnoreCase("Received") && !s.equalsIgnoreCase("Return-Path")) {
            b = false;
        }
        else {
            b = true;
        }
        if (b) {
            size = 0;
        }
        for (int i = this.headers.size() - 1; i >= 0; --i) {
            final InternetHeader internetHeader = this.headers.get(i);
            if (s.equalsIgnoreCase(internetHeader.getName())) {
                if (!b) {
                    this.headers.add(i + 1, new InternetHeader(s, s2));
                    return;
                }
                size = i;
            }
            if (internetHeader.getName().equals(":")) {
                size = i;
            }
        }
        this.headers.add(size, new InternetHeader(s, s2));
    }
    
    public void addHeaderLine(final String s) {
        try {
            final char char1 = s.charAt(0);
            if (char1 == ' ' || char1 == '\t') {
                final InternetHeader internetHeader = this.headers.get(this.headers.size() - 1);
                internetHeader.line = String.valueOf(internetHeader.line) + "\r\n" + s;
                return;
            }
            this.headers.add(new InternetHeader(s));
        }
        catch (StringIndexOutOfBoundsException ex) {}
        catch (NoSuchElementException ex2) {}
    }
    
    public Enumeration getAllHeaderLines() {
        return this.getNonMatchingHeaderLines(null);
    }
    
    public Enumeration getAllHeaders() {
        return new matchEnum(this.headers, null, false, false);
    }
    
    public String getHeader(final String s, final String s2) {
        final String[] header = this.getHeader(s);
        if (header == null) {
            return null;
        }
        if (header.length == 1 || s2 == null) {
            return header[0];
        }
        final StringBuffer sb = new StringBuffer(header[0]);
        for (int i = 1; i < header.length; ++i) {
            sb.append(s2);
            sb.append(header[i]);
        }
        return sb.toString();
    }
    
    public String[] getHeader(final String s) {
        final Iterator<InternetHeader> iterator = this.headers.iterator();
        final ArrayList<String> list = new ArrayList<String>();
        while (iterator.hasNext()) {
            final InternetHeader internetHeader = iterator.next();
            if (s.equalsIgnoreCase(internetHeader.getName()) && internetHeader.line != null) {
                list.add(internetHeader.getValue());
            }
        }
        if (list.size() == 0) {
            return null;
        }
        return list.toArray(new String[list.size()]);
    }
    
    public Enumeration getMatchingHeaderLines(final String[] array) {
        return new matchEnum(this.headers, array, true, true);
    }
    
    public Enumeration getMatchingHeaders(final String[] array) {
        return new matchEnum(this.headers, array, true, false);
    }
    
    public Enumeration getNonMatchingHeaderLines(final String[] array) {
        return new matchEnum(this.headers, array, false, true);
    }
    
    public Enumeration getNonMatchingHeaders(final String[] array) {
        return new matchEnum(this.headers, array, false, false);
    }
    
    public void load(final InputStream inputStream) throws MessagingException {
        final LineInputStream lineInputStream = new LineInputStream(inputStream);
        String s = null;
        final StringBuffer sb = new StringBuffer();
    Label_0081_Outer:
        while (true) {
            while (true) {
                String line = null;
                Label_0144: {
                    try {
                        do {
                            line = lineInputStream.readLine();
                            if (line != null && (line.startsWith(" ") || line.startsWith("\t"))) {
                                String s2;
                                if ((s2 = s) != null) {
                                    sb.append(s);
                                    s2 = null;
                                }
                                sb.append("\r\n");
                                sb.append(line);
                                s = s2;
                                if (line != null) {
                                    continue Label_0081_Outer;
                                }
                                break Label_0081_Outer;
                            }
                            else {
                                if (s != null) {
                                    this.addHeaderLine(s);
                                    break Label_0144;
                                }
                                if (sb.length() > 0) {
                                    this.addHeaderLine(sb.toString());
                                    sb.setLength(0);
                                }
                                break Label_0144;
                            }
                        } while (line.length() > 0);
                        return;
                    }
                    catch (IOException ex) {
                        throw new MessagingException("Error in input stream", ex);
                    }
                    break;
                }
                s = line;
                continue;
            }
        }
    }
    
    public void removeHeader(final String s) {
        for (int i = 0; i < this.headers.size(); ++i) {
            final InternetHeader internetHeader = this.headers.get(i);
            if (s.equalsIgnoreCase(internetHeader.getName())) {
                internetHeader.line = null;
            }
        }
    }
    
    public void setHeader(final String s, final String s2) {
        int n = 0;
        int n2;
        int n3;
    Label_0140:
        for (int i = 0; i < this.headers.size(); i = n3 + 1, n = n2) {
            final InternetHeader internetHeader = this.headers.get(i);
            n2 = n;
            n3 = i;
            if (s.equalsIgnoreCase(internetHeader.getName())) {
                if (n == 0) {
                    while (true) {
                        Label_0152: {
                            if (internetHeader.line == null) {
                                break Label_0152;
                            }
                            final int index = internetHeader.line.indexOf(58);
                            if (index < 0) {
                                break Label_0152;
                            }
                            internetHeader.line = String.valueOf(internetHeader.line.substring(0, index + 1)) + " " + s2;
                            n2 = 1;
                            n3 = i;
                            continue Label_0140;
                        }
                        internetHeader.line = String.valueOf(s) + ": " + s2;
                        continue;
                    }
                }
                this.headers.remove(i);
                n3 = i - 1;
                n2 = n;
            }
        }
        if (n == 0) {
            this.addHeader(s, s2);
        }
    }
    
    protected static final class InternetHeader extends Header
    {
        String line;
        
        public InternetHeader(final String line) {
            super("", "");
            final int index = line.indexOf(58);
            if (index < 0) {
                this.name = line.trim();
            }
            else {
                this.name = line.substring(0, index).trim();
            }
            this.line = line;
        }
        
        public InternetHeader(final String s, final String s2) {
            super(s, "");
            if (s2 != null) {
                this.line = String.valueOf(s) + ": " + s2;
                return;
            }
            this.line = null;
        }
        
        @Override
        public String getValue() {
            final int index = this.line.indexOf(58);
            if (index < 0) {
                return this.line;
            }
            int i;
            for (i = index + 1; i < this.line.length(); ++i) {
                final char char1 = this.line.charAt(i);
                if (char1 != ' ' && char1 != '\t' && char1 != '\r' && char1 != '\n') {
                    break;
                }
            }
            return this.line.substring(i);
        }
    }
    
    static class matchEnum implements Enumeration
    {
        private Iterator e;
        private boolean match;
        private String[] names;
        private InternetHeader next_header;
        private boolean want_line;
        
        matchEnum(final List list, final String[] names, final boolean match, final boolean want_line) {
            this.e = list.iterator();
            this.names = names;
            this.match = match;
            this.want_line = want_line;
            this.next_header = null;
        }
        
        private InternetHeader nextMatch() {
        Label_0000:
            while (this.e.hasNext()) {
                final InternetHeader internetHeader = this.e.next();
                if (internetHeader.line != null) {
                    if (this.names == null) {
                        if (this.match) {
                            return null;
                        }
                        return internetHeader;
                    }
                    else {
                        int i = 0;
                        while (i < this.names.length) {
                            if (this.names[i].equalsIgnoreCase(internetHeader.getName())) {
                                if (this.match) {
                                    return internetHeader;
                                }
                                continue Label_0000;
                            }
                            else {
                                ++i;
                            }
                        }
                        if (!this.match) {
                            return internetHeader;
                        }
                        continue;
                    }
                }
            }
            return null;
        }
        
        @Override
        public boolean hasMoreElements() {
            if (this.next_header == null) {
                this.next_header = this.nextMatch();
            }
            return this.next_header != null;
        }
        
        @Override
        public Object nextElement() {
            if (this.next_header == null) {
                this.next_header = this.nextMatch();
            }
            if (this.next_header == null) {
                throw new NoSuchElementException("No more headers");
            }
            final InternetHeader next_header = this.next_header;
            this.next_header = null;
            if (this.want_line) {
                return next_header.line;
            }
            return new Header(next_header.getName(), next_header.getValue());
        }
    }
}
