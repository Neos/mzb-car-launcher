package javax.mail.internet;

public class ContentType
{
    private ParameterList list;
    private String primaryType;
    private String subType;
    
    public ContentType() {
    }
    
    public ContentType(String remainder) throws ParseException {
        final HeaderTokenizer headerTokenizer = new HeaderTokenizer(remainder, "()<>@,;:\\\"\t []/?=");
        final HeaderTokenizer.Token next = headerTokenizer.next();
        if (next.getType() != -1) {
            throw new ParseException();
        }
        this.primaryType = next.getValue();
        if ((char)headerTokenizer.next().getType() != '/') {
            throw new ParseException();
        }
        final HeaderTokenizer.Token next2 = headerTokenizer.next();
        if (next2.getType() != -1) {
            throw new ParseException();
        }
        this.subType = next2.getValue();
        remainder = headerTokenizer.getRemainder();
        if (remainder != null) {
            this.list = new ParameterList(remainder);
        }
    }
    
    public ContentType(final String primaryType, final String subType, final ParameterList list) {
        this.primaryType = primaryType;
        this.subType = subType;
        this.list = list;
    }
    
    public String getBaseType() {
        return String.valueOf(this.primaryType) + '/' + this.subType;
    }
    
    public String getParameter(final String s) {
        if (this.list == null) {
            return null;
        }
        return this.list.get(s);
    }
    
    public ParameterList getParameterList() {
        return this.list;
    }
    
    public String getPrimaryType() {
        return this.primaryType;
    }
    
    public String getSubType() {
        return this.subType;
    }
    
    public boolean match(final String s) {
        try {
            return this.match(new ContentType(s));
        }
        catch (ParseException ex) {
            return false;
        }
    }
    
    public boolean match(final ContentType contentType) {
        if (this.primaryType.equalsIgnoreCase(contentType.getPrimaryType())) {
            final String subType = contentType.getSubType();
            if (this.subType.charAt(0) == '*' || subType.charAt(0) == '*') {
                return true;
            }
            if (this.subType.equalsIgnoreCase(subType)) {
                return true;
            }
        }
        return false;
    }
    
    public void setParameter(final String s, final String s2) {
        if (this.list == null) {
            this.list = new ParameterList();
        }
        this.list.set(s, s2);
    }
    
    public void setParameterList(final ParameterList list) {
        this.list = list;
    }
    
    public void setPrimaryType(final String primaryType) {
        this.primaryType = primaryType;
    }
    
    public void setSubType(final String subType) {
        this.subType = subType;
    }
    
    @Override
    public String toString() {
        if (this.primaryType == null || this.subType == null) {
            return null;
        }
        final StringBuffer sb = new StringBuffer();
        sb.append(this.primaryType).append('/').append(this.subType);
        if (this.list != null) {
            sb.append(this.list.toString(sb.length() + 14));
        }
        return sb.toString();
    }
}
