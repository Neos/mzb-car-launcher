package javax.mail.internet;

import javax.mail.*;
import com.sun.mail.util.*;
import java.util.*;
import java.io.*;

public class PreencodedMimeBodyPart extends MimeBodyPart
{
    private String encoding;
    
    public PreencodedMimeBodyPart(final String encoding) {
        this.encoding = encoding;
    }
    
    @Override
    public String getEncoding() throws MessagingException {
        return this.encoding;
    }
    
    @Override
    protected void updateHeaders() throws MessagingException {
        super.updateHeaders();
        MimeBodyPart.setEncoding(this, this.encoding);
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) throws IOException, MessagingException {
        LineOutputStream lineOutputStream;
        if (outputStream instanceof LineOutputStream) {
            lineOutputStream = (LineOutputStream)outputStream;
        }
        else {
            lineOutputStream = new LineOutputStream(outputStream);
        }
        final Enumeration allHeaderLines = this.getAllHeaderLines();
        while (allHeaderLines.hasMoreElements()) {
            lineOutputStream.writeln(allHeaderLines.nextElement());
        }
        lineOutputStream.writeln();
        this.getDataHandler().writeTo(outputStream);
        outputStream.flush();
    }
}
