package javax.mail.search;

import javax.mail.*;

public final class MessageNumberTerm extends IntegerComparisonTerm
{
    private static final long serialVersionUID = -5379625829658623812L;
    
    public MessageNumberTerm(final int n) {
        super(3, n);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof MessageNumberTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
        try {
            return super.match(message.getMessageNumber());
        }
        catch (Exception ex) {
            return false;
        }
    }
}
