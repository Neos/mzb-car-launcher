package javax.mail.search;

import javax.mail.*;

public final class BodyTerm extends StringTerm
{
    private static final long serialVersionUID = -4888862527916911385L;
    
    public BodyTerm(final String s) {
        super(s);
    }
    
    private boolean matchPart(final Part part) {
        while (true) {
        Label_0064_Outer:
            while (true) {
                int n = 0;
                while (true) {
                    int count;
                    try {
                        if (part.isMimeType("text/*")) {
                            final String s = (String)part.getContent();
                            return s != null && super.match(s);
                        }
                        if (part.isMimeType("multipart/*")) {
                            final Multipart multipart = (Multipart)part.getContent();
                            count = multipart.getCount();
                            n = 0;
                            break Label_0111;
                        }
                        if (part.isMimeType("message/rfc822")) {
                            return this.matchPart((Part)part.getContent());
                        }
                        break;
                        // iftrue(Label_0118:, !this.matchPart((Part)multipart.getBodyPart(n)))
                        return true;
                    }
                    catch (Exception ex) {
                        break;
                    }
                    if (n >= count) {
                        break;
                    }
                    continue;
                }
                Label_0118: {
                    ++n;
                }
                continue Label_0064_Outer;
            }
        }
        return false;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof BodyTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
        return this.matchPart(message);
    }
}
