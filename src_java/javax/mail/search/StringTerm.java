package javax.mail.search;

public abstract class StringTerm extends SearchTerm
{
    private static final long serialVersionUID = 1274042129007696269L;
    protected boolean ignoreCase;
    protected String pattern;
    
    protected StringTerm(final String pattern) {
        this.pattern = pattern;
        this.ignoreCase = true;
    }
    
    protected StringTerm(final String pattern, final boolean ignoreCase) {
        this.pattern = pattern;
        this.ignoreCase = ignoreCase;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof StringTerm) {
            final StringTerm stringTerm = (StringTerm)o;
            if (this.ignoreCase) {
                if (stringTerm.pattern.equalsIgnoreCase(this.pattern) && stringTerm.ignoreCase == this.ignoreCase) {
                    return true;
                }
            }
            else if (stringTerm.pattern.equals(this.pattern) && stringTerm.ignoreCase == this.ignoreCase) {
                return true;
            }
        }
        return false;
    }
    
    public boolean getIgnoreCase() {
        return this.ignoreCase;
    }
    
    public String getPattern() {
        return this.pattern;
    }
    
    @Override
    public int hashCode() {
        if (this.ignoreCase) {
            return this.pattern.hashCode();
        }
        return ~this.pattern.hashCode();
    }
    
    protected boolean match(final String s) {
        for (int length = s.length(), length2 = this.pattern.length(), i = 0; i <= length - length2; ++i) {
            if (s.regionMatches(this.ignoreCase, i, this.pattern, 0, this.pattern.length())) {
                return true;
            }
        }
        return false;
    }
}
