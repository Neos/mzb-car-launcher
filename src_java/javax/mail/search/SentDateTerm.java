package javax.mail.search;

import java.util.*;
import javax.mail.*;

public final class SentDateTerm extends DateTerm
{
    private static final long serialVersionUID = 5647755030530907263L;
    
    public SentDateTerm(final int n, final Date date) {
        super(n, date);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SentDateTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
        Date sentDate;
        try {
            sentDate = message.getSentDate();
            if (sentDate == null) {
                return false;
            }
        }
        catch (Exception ex) {
            return false;
        }
        return super.match(sentDate);
    }
}
