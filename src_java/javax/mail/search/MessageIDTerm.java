package javax.mail.search;

import javax.mail.*;

public final class MessageIDTerm extends StringTerm
{
    private static final long serialVersionUID = -2121096296454691963L;
    
    public MessageIDTerm(final String s) {
        super(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof MessageIDTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
    Block_2:
        while (true) {
            String[] header;
            try {
                header = message.getHeader("Message-ID");
                if (header == null) {
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            for (int i = 0; i < header.length; ++i) {
                if (super.match(header[i])) {
                    break Block_2;
                }
            }
            return false;
        }
        return true;
    }
}
