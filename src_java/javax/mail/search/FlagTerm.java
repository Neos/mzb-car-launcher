package javax.mail.search;

import javax.mail.*;

public final class FlagTerm extends SearchTerm
{
    private static final long serialVersionUID = -142991500302030647L;
    protected Flags flags;
    protected boolean set;
    
    public FlagTerm(final Flags flags, final boolean set) {
        this.flags = flags;
        this.set = set;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof FlagTerm) {
            final FlagTerm flagTerm = (FlagTerm)o;
            if (flagTerm.set == this.set && flagTerm.flags.equals(this.flags)) {
                return true;
            }
        }
        return false;
    }
    
    public Flags getFlags() {
        return (Flags)this.flags.clone();
    }
    
    public boolean getTestSet() {
        return this.set;
    }
    
    @Override
    public int hashCode() {
        if (this.set) {
            return this.flags.hashCode();
        }
        return ~this.flags.hashCode();
    }
    
    @Override
    public boolean match(final Message message) {
        final boolean b = true;
        boolean b2;
        try {
            final Flags flags = message.getFlags();
            if (!this.set) {
                final Flags.Flag[] systemFlags = this.flags.getSystemFlags();
                for (int i = 0; i < systemFlags.length; ++i) {
                    if (flags.contains(systemFlags[i])) {
                        return false;
                    }
                }
                final String[] userFlags = this.flags.getUserFlags();
                int n = 0;
                while (true) {
                    b2 = b;
                    if (n >= userFlags.length) {
                        return b2;
                    }
                    if (flags.contains(userFlags[n])) {
                        break;
                    }
                    ++n;
                }
                return false;
            }
            if (flags.contains(this.flags)) {
                return true;
            }
            return false;
        }
        catch (Exception ex) {
            b2 = false;
        }
        return b2;
    }
}
