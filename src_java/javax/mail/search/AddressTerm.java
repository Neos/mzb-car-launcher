package javax.mail.search;

import javax.mail.*;

public abstract class AddressTerm extends SearchTerm
{
    private static final long serialVersionUID = 2005405551929769980L;
    protected Address address;
    
    protected AddressTerm(final Address address) {
        this.address = address;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AddressTerm && ((AddressTerm)o).address.equals(this.address);
    }
    
    public Address getAddress() {
        return this.address;
    }
    
    @Override
    public int hashCode() {
        return this.address.hashCode();
    }
    
    protected boolean match(final Address address) {
        return address.equals(this.address);
    }
}
