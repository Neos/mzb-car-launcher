package javax.mail.search;

import javax.mail.*;

public final class RecipientStringTerm extends AddressStringTerm
{
    private static final long serialVersionUID = -8293562089611618849L;
    private Message.RecipientType type;
    
    public RecipientStringTerm(final Message.RecipientType type, final String s) {
        super(s);
        this.type = type;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof RecipientStringTerm && ((RecipientStringTerm)o).type.equals(this.type) && super.equals(o);
    }
    
    public Message.RecipientType getRecipientType() {
        return this.type;
    }
    
    @Override
    public int hashCode() {
        return this.type.hashCode() + super.hashCode();
    }
    
    @Override
    public boolean match(final Message message) {
    Block_2:
        while (true) {
            Address[] recipients;
            try {
                recipients = message.getRecipients(this.type);
                if (recipients == null) {
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            for (int i = 0; i < recipients.length; ++i) {
                if (super.match(recipients[i])) {
                    break Block_2;
                }
            }
            return false;
        }
        return true;
    }
}
