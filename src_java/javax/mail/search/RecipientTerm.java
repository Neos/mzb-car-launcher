package javax.mail.search;

import javax.mail.*;

public final class RecipientTerm extends AddressTerm
{
    private static final long serialVersionUID = 6548700653122680468L;
    protected Message.RecipientType type;
    
    public RecipientTerm(final Message.RecipientType type, final Address address) {
        super(address);
        this.type = type;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof RecipientTerm && ((RecipientTerm)o).type.equals(this.type) && super.equals(o);
    }
    
    public Message.RecipientType getRecipientType() {
        return this.type;
    }
    
    @Override
    public int hashCode() {
        return this.type.hashCode() + super.hashCode();
    }
    
    @Override
    public boolean match(final Message message) {
    Block_2:
        while (true) {
            Address[] recipients;
            try {
                recipients = message.getRecipients(this.type);
                if (recipients == null) {
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            for (int i = 0; i < recipients.length; ++i) {
                if (super.match(recipients[i])) {
                    break Block_2;
                }
            }
            return false;
        }
        return true;
    }
}
