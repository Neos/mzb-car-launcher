package javax.mail.search;

import javax.mail.*;

public final class AndTerm extends SearchTerm
{
    private static final long serialVersionUID = -3583274505380989582L;
    protected SearchTerm[] terms;
    
    public AndTerm(final SearchTerm searchTerm, final SearchTerm searchTerm2) {
        (this.terms = new SearchTerm[2])[0] = searchTerm;
        this.terms[1] = searchTerm2;
    }
    
    public AndTerm(final SearchTerm[] array) {
        this.terms = new SearchTerm[array.length];
        for (int i = 0; i < array.length; ++i) {
            this.terms[i] = array[i];
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof AndTerm) {
            final AndTerm andTerm = (AndTerm)o;
            if (andTerm.terms.length == this.terms.length) {
                for (int i = 0; i < this.terms.length; ++i) {
                    if (!this.terms[i].equals(andTerm.terms[i])) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    public SearchTerm[] getTerms() {
        return this.terms.clone();
    }
    
    @Override
    public int hashCode() {
        int n = 0;
        for (int i = 0; i < this.terms.length; ++i) {
            n += this.terms[i].hashCode();
        }
        return n;
    }
    
    @Override
    public boolean match(final Message message) {
        for (int i = 0; i < this.terms.length; ++i) {
            if (!this.terms[i].match(message)) {
                return false;
            }
        }
        return true;
    }
}
