package javax.mail.search;

import java.util.*;
import javax.mail.*;

public final class ReceivedDateTerm extends DateTerm
{
    private static final long serialVersionUID = -2756695246195503170L;
    
    public ReceivedDateTerm(final int n, final Date date) {
        super(n, date);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ReceivedDateTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
        Date receivedDate;
        try {
            receivedDate = message.getReceivedDate();
            if (receivedDate == null) {
                return false;
            }
        }
        catch (Exception ex) {
            return false;
        }
        return super.match(receivedDate);
    }
}
