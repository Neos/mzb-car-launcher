package javax.mail.search;

import java.util.*;
import javax.mail.*;

public final class HeaderTerm extends StringTerm
{
    private static final long serialVersionUID = 8342514650333389122L;
    protected String headerName;
    
    public HeaderTerm(final String headerName, final String s) {
        super(s);
        this.headerName = headerName;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof HeaderTerm) {
            final HeaderTerm headerTerm = (HeaderTerm)o;
            if (headerTerm.headerName.equalsIgnoreCase(this.headerName) && super.equals(headerTerm)) {
                return true;
            }
        }
        return false;
    }
    
    public String getHeaderName() {
        return this.headerName;
    }
    
    @Override
    public int hashCode() {
        return this.headerName.toLowerCase(Locale.ENGLISH).hashCode() + super.hashCode();
    }
    
    @Override
    public boolean match(final Message message) {
    Block_2:
        while (true) {
            String[] header;
            try {
                header = message.getHeader(this.headerName);
                if (header == null) {
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            for (int i = 0; i < header.length; ++i) {
                if (super.match(header[i])) {
                    break Block_2;
                }
            }
            return false;
        }
        return true;
    }
}
