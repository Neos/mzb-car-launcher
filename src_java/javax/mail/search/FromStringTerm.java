package javax.mail.search;

import javax.mail.*;

public final class FromStringTerm extends AddressStringTerm
{
    private static final long serialVersionUID = 5801127523826772788L;
    
    public FromStringTerm(final String s) {
        super(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof FromStringTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
    Block_2:
        while (true) {
            Address[] from;
            try {
                from = message.getFrom();
                if (from == null) {
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            for (int i = 0; i < from.length; ++i) {
                if (super.match(from[i])) {
                    break Block_2;
                }
            }
            return false;
        }
        return true;
    }
}
