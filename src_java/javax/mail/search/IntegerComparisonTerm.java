package javax.mail.search;

public abstract class IntegerComparisonTerm extends ComparisonTerm
{
    private static final long serialVersionUID = -6963571240154302484L;
    protected int number;
    
    protected IntegerComparisonTerm(final int comparison, final int number) {
        this.comparison = comparison;
        this.number = number;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof IntegerComparisonTerm && ((IntegerComparisonTerm)o).number == this.number && super.equals(o);
    }
    
    public int getComparison() {
        return this.comparison;
    }
    
    public int getNumber() {
        return this.number;
    }
    
    @Override
    public int hashCode() {
        return this.number + super.hashCode();
    }
    
    protected boolean match(final int n) {
        boolean b = true;
        switch (this.comparison) {
            default: {
                b = false;
                break;
            }
            case 1: {
                if (n > this.number) {
                    return false;
                }
                break;
            }
            case 2: {
                if (n >= this.number) {
                    return false;
                }
                break;
            }
            case 3: {
                if (n != this.number) {
                    return false;
                }
                break;
            }
            case 4: {
                if (n == this.number) {
                    return false;
                }
                break;
            }
            case 5: {
                if (n <= this.number) {
                    return false;
                }
                break;
            }
            case 6: {
                if (n < this.number) {
                    return false;
                }
                break;
            }
        }
        return b;
    }
}
