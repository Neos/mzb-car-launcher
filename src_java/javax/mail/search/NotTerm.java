package javax.mail.search;

import javax.mail.*;

public final class NotTerm extends SearchTerm
{
    private static final long serialVersionUID = 7152293214217310216L;
    protected SearchTerm term;
    
    public NotTerm(final SearchTerm term) {
        this.term = term;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof NotTerm && ((NotTerm)o).term.equals(this.term);
    }
    
    public SearchTerm getTerm() {
        return this.term;
    }
    
    @Override
    public int hashCode() {
        return this.term.hashCode() << 1;
    }
    
    @Override
    public boolean match(final Message message) {
        return !this.term.match(message);
    }
}
