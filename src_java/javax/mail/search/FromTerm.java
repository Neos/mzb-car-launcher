package javax.mail.search;

import javax.mail.*;

public final class FromTerm extends AddressTerm
{
    private static final long serialVersionUID = 5214730291502658665L;
    
    public FromTerm(final Address address) {
        super(address);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof FromTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
    Block_2:
        while (true) {
            Address[] from;
            try {
                from = message.getFrom();
                if (from == null) {
                    return false;
                }
            }
            catch (Exception ex) {
                return false;
            }
            for (int i = 0; i < from.length; ++i) {
                if (super.match(from[i])) {
                    break Block_2;
                }
            }
            return false;
        }
        return true;
    }
}
