package javax.mail.search;

import javax.mail.*;
import javax.mail.internet.*;

public abstract class AddressStringTerm extends StringTerm
{
    private static final long serialVersionUID = 3086821234204980368L;
    
    protected AddressStringTerm(final String s) {
        super(s, true);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AddressStringTerm && super.equals(o);
    }
    
    protected boolean match(final Address address) {
        if (address instanceof InternetAddress) {
            return super.match(((InternetAddress)address).toUnicodeString());
        }
        return super.match(address.toString());
    }
}
