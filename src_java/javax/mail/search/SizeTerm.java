package javax.mail.search;

import javax.mail.*;

public final class SizeTerm extends IntegerComparisonTerm
{
    private static final long serialVersionUID = -2556219451005103709L;
    
    public SizeTerm(final int n, final int n2) {
        super(n, n2);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SizeTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
        int size;
        try {
            size = message.getSize();
            if (size == -1) {
                return false;
            }
        }
        catch (Exception ex) {
            return false;
        }
        return super.match(size);
    }
}
