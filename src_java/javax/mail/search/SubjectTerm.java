package javax.mail.search;

import javax.mail.*;

public final class SubjectTerm extends StringTerm
{
    private static final long serialVersionUID = 7481568618055573432L;
    
    public SubjectTerm(final String s) {
        super(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SubjectTerm && super.equals(o);
    }
    
    @Override
    public boolean match(final Message message) {
        String subject;
        try {
            subject = message.getSubject();
            if (subject == null) {
                return false;
            }
        }
        catch (Exception ex) {
            return false;
        }
        return super.match(subject);
    }
}
