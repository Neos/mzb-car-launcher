package javax.mail.search;

import java.util.*;

public abstract class DateTerm extends ComparisonTerm
{
    private static final long serialVersionUID = 4818873430063720043L;
    protected Date date;
    
    protected DateTerm(final int comparison, final Date date) {
        this.comparison = comparison;
        this.date = date;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof DateTerm && ((DateTerm)o).date.equals(this.date) && super.equals(o);
    }
    
    public int getComparison() {
        return this.comparison;
    }
    
    public Date getDate() {
        return new Date(this.date.getTime());
    }
    
    @Override
    public int hashCode() {
        return this.date.hashCode() + super.hashCode();
    }
    
    protected boolean match(final Date date) {
        switch (this.comparison) {
            case 1: {
                if (date.before(this.date) || date.equals(this.date)) {
                    return true;
                }
                break;
            }
            case 2: {
                return date.before(this.date);
            }
            case 3: {
                return date.equals(this.date);
            }
            case 4: {
                if (!date.equals(this.date)) {
                    return true;
                }
                break;
            }
            case 5: {
                return date.after(this.date);
            }
            case 6: {
                if (date.after(this.date) || date.equals(this.date)) {
                    return true;
                }
                break;
            }
        }
        return false;
    }
}
