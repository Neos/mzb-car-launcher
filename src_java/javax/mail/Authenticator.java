package javax.mail;

import java.net.*;

public abstract class Authenticator
{
    private int requestingPort;
    private String requestingPrompt;
    private String requestingProtocol;
    private InetAddress requestingSite;
    private String requestingUserName;
    
    private void reset() {
        this.requestingSite = null;
        this.requestingPort = -1;
        this.requestingProtocol = null;
        this.requestingPrompt = null;
        this.requestingUserName = null;
    }
    
    protected final String getDefaultUserName() {
        return this.requestingUserName;
    }
    
    protected PasswordAuthentication getPasswordAuthentication() {
        return null;
    }
    
    protected final int getRequestingPort() {
        return this.requestingPort;
    }
    
    protected final String getRequestingPrompt() {
        return this.requestingPrompt;
    }
    
    protected final String getRequestingProtocol() {
        return this.requestingProtocol;
    }
    
    protected final InetAddress getRequestingSite() {
        return this.requestingSite;
    }
    
    final PasswordAuthentication requestPasswordAuthentication(final InetAddress requestingSite, final int requestingPort, final String requestingProtocol, final String requestingPrompt, final String requestingUserName) {
        this.reset();
        this.requestingSite = requestingSite;
        this.requestingPort = requestingPort;
        this.requestingProtocol = requestingProtocol;
        this.requestingPrompt = requestingPrompt;
        this.requestingUserName = requestingUserName;
        return this.getPasswordAuthentication();
    }
}
