package javax.mail;

public class MessageContext
{
    private Part part;
    
    public MessageContext(final Part part) {
        this.part = part;
    }
    
    private static Message getMessage(Part parent) throws MessagingException {
        while (parent != null) {
            if (parent instanceof Message) {
                return (Message)parent;
            }
            final Multipart parent2 = ((BodyPart)parent).getParent();
            if (parent2 == null) {
                return null;
            }
            parent = parent2.getParent();
        }
        return null;
    }
    
    public Message getMessage() {
        try {
            return getMessage(this.part);
        }
        catch (MessagingException ex) {
            return null;
        }
    }
    
    public Part getPart() {
        return this.part;
    }
    
    public Session getSession() {
        final Message message = this.getMessage();
        if (message != null) {
            return message.session;
        }
        return null;
    }
}
