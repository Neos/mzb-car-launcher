package javax.mail;

public class SendFailedException extends MessagingException
{
    private static final long serialVersionUID = -6457531621682372913L;
    protected transient Address[] invalid;
    protected transient Address[] validSent;
    protected transient Address[] validUnsent;
    
    public SendFailedException() {
    }
    
    public SendFailedException(final String s) {
        super(s);
    }
    
    public SendFailedException(final String s, final Exception ex) {
        super(s, ex);
    }
    
    public SendFailedException(final String s, final Exception ex, final Address[] validSent, final Address[] validUnsent, final Address[] invalid) {
        super(s, ex);
        this.validSent = validSent;
        this.validUnsent = validUnsent;
        this.invalid = invalid;
    }
    
    public Address[] getInvalidAddresses() {
        return this.invalid;
    }
    
    public Address[] getValidSentAddresses() {
        return this.validSent;
    }
    
    public Address[] getValidUnsentAddresses() {
        return this.validUnsent;
    }
}
