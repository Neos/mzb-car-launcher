package de.mindpipe.android.logging.log4j;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import android.util.*;

public class LogCatAppender extends AppenderSkeleton
{
    protected Layout tagLayout;
    
    public LogCatAppender() {
        this(new PatternLayout("%m%n"));
    }
    
    public LogCatAppender(final Layout layout) {
        this(layout, new PatternLayout("%c"));
    }
    
    public LogCatAppender(final Layout layout, final Layout tagLayout) {
        this.tagLayout = tagLayout;
        this.setLayout(layout);
    }
    
    @Override
    protected void append(final LoggingEvent loggingEvent) {
        switch (loggingEvent.getLevel().toInt()) {
            default: {}
            case 5000: {
                if (loggingEvent.getThrowableInformation() != null) {
                    Log.v(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent), loggingEvent.getThrowableInformation().getThrowable());
                    return;
                }
                Log.v(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent));
            }
            case 10000: {
                if (loggingEvent.getThrowableInformation() != null) {
                    Log.d(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent), loggingEvent.getThrowableInformation().getThrowable());
                    return;
                }
                Log.d(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent));
            }
            case 20000: {
                if (loggingEvent.getThrowableInformation() != null) {
                    Log.i(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent), loggingEvent.getThrowableInformation().getThrowable());
                    return;
                }
                Log.i(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent));
            }
            case 30000: {
                if (loggingEvent.getThrowableInformation() != null) {
                    Log.w(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent), loggingEvent.getThrowableInformation().getThrowable());
                    return;
                }
                Log.w(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent));
            }
            case 40000: {
                if (loggingEvent.getThrowableInformation() != null) {
                    Log.e(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent), loggingEvent.getThrowableInformation().getThrowable());
                    return;
                }
                Log.e(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent));
            }
            case 50000: {
                if (loggingEvent.getThrowableInformation() != null) {
                    Log.wtf(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent), loggingEvent.getThrowableInformation().getThrowable());
                    return;
                }
                Log.wtf(this.getTagLayout().format(loggingEvent), this.getLayout().format(loggingEvent));
            }
        }
    }
    
    @Override
    public void close() {
    }
    
    public Layout getTagLayout() {
        return this.tagLayout;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    public void setTagLayout(final Layout tagLayout) {
        this.tagLayout = tagLayout;
    }
}
