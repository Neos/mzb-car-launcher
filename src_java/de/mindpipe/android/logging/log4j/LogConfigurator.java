package de.mindpipe.android.logging.log4j;

import java.io.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;

public class LogConfigurator
{
    private String fileName;
    private String filePattern;
    private boolean immediateFlush;
    private boolean internalDebugging;
    private String logCatPattern;
    private int maxBackupSize;
    private long maxFileSize;
    private boolean resetConfiguration;
    private Level rootLevel;
    private boolean useFileAppender;
    private boolean useLogCatAppender;
    
    public LogConfigurator() {
        this.rootLevel = Level.DEBUG;
        this.filePattern = "%d - [%p::%c::%C] - %m%n";
        this.logCatPattern = "%m%n";
        this.fileName = "android-log4j.log";
        this.maxBackupSize = 5;
        this.maxFileSize = 524288L;
        this.immediateFlush = true;
        this.useLogCatAppender = true;
        this.useFileAppender = true;
        this.resetConfiguration = true;
        this.internalDebugging = false;
    }
    
    public LogConfigurator(final String fileName) {
        this.rootLevel = Level.DEBUG;
        this.filePattern = "%d - [%p::%c::%C] - %m%n";
        this.logCatPattern = "%m%n";
        this.fileName = "android-log4j.log";
        this.maxBackupSize = 5;
        this.maxFileSize = 524288L;
        this.immediateFlush = true;
        this.useLogCatAppender = true;
        this.useFileAppender = true;
        this.resetConfiguration = true;
        this.internalDebugging = false;
        this.setFileName(fileName);
    }
    
    public LogConfigurator(final String s, final int maxBackupSize, final long maxFileSize, final String s2, final Level level) {
        this(s, level, s2);
        this.setMaxBackupSize(maxBackupSize);
        this.setMaxFileSize(maxFileSize);
    }
    
    public LogConfigurator(final String s, final Level rootLevel) {
        this(s);
        this.setRootLevel(rootLevel);
    }
    
    public LogConfigurator(final String s, final Level rootLevel, final String filePattern) {
        this(s);
        this.setRootLevel(rootLevel);
        this.setFilePattern(filePattern);
    }
    
    private void configureFileAppender() {
        final Logger rootLogger = Logger.getRootLogger();
        final PatternLayout patternLayout = new PatternLayout(this.getFilePattern());
        try {
            final RollingFileAppender rollingFileAppender = new RollingFileAppender(patternLayout, this.getFileName());
            rollingFileAppender.setMaxBackupIndex(this.getMaxBackupSize());
            rollingFileAppender.setMaximumFileSize(this.getMaxFileSize());
            rollingFileAppender.setImmediateFlush(this.isImmediateFlush());
            rootLogger.addAppender(rollingFileAppender);
        }
        catch (IOException ex) {
            throw new RuntimeException("Exception configuring log system", ex);
        }
    }
    
    private void configureLogCatAppender() {
        Logger.getRootLogger().addAppender(new LogCatAppender(new PatternLayout(this.getLogCatPattern())));
    }
    
    public void configure() {
        final Logger rootLogger = Logger.getRootLogger();
        if (this.isResetConfiguration()) {
            LogManager.getLoggerRepository().resetConfiguration();
        }
        LogLog.setInternalDebugging(this.isInternalDebugging());
        if (this.isUseFileAppender()) {
            this.configureFileAppender();
        }
        if (this.isUseLogCatAppender()) {
            this.configureLogCatAppender();
        }
        rootLogger.setLevel(this.getRootLevel());
    }
    
    public String getFileName() {
        return this.fileName;
    }
    
    public String getFilePattern() {
        return this.filePattern;
    }
    
    public String getLogCatPattern() {
        return this.logCatPattern;
    }
    
    public int getMaxBackupSize() {
        return this.maxBackupSize;
    }
    
    public long getMaxFileSize() {
        return this.maxFileSize;
    }
    
    public Level getRootLevel() {
        return this.rootLevel;
    }
    
    public boolean isImmediateFlush() {
        return this.immediateFlush;
    }
    
    public boolean isInternalDebugging() {
        return this.internalDebugging;
    }
    
    public boolean isResetConfiguration() {
        return this.resetConfiguration;
    }
    
    public boolean isUseFileAppender() {
        return this.useFileAppender;
    }
    
    public boolean isUseLogCatAppender() {
        return this.useLogCatAppender;
    }
    
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
    
    public void setFilePattern(final String filePattern) {
        this.filePattern = filePattern;
    }
    
    public void setImmediateFlush(final boolean immediateFlush) {
        this.immediateFlush = immediateFlush;
    }
    
    public void setInternalDebugging(final boolean internalDebugging) {
        this.internalDebugging = internalDebugging;
    }
    
    public void setLevel(final String s, final Level level) {
        Logger.getLogger(s).setLevel(level);
    }
    
    public void setLogCatPattern(final String logCatPattern) {
        this.logCatPattern = logCatPattern;
    }
    
    public void setMaxBackupSize(final int maxBackupSize) {
        this.maxBackupSize = maxBackupSize;
    }
    
    public void setMaxFileSize(final long maxFileSize) {
        this.maxFileSize = maxFileSize;
    }
    
    public void setResetConfiguration(final boolean resetConfiguration) {
        this.resetConfiguration = resetConfiguration;
    }
    
    public void setRootLevel(final Level rootLevel) {
        this.rootLevel = rootLevel;
    }
    
    public void setUseFileAppender(final boolean useFileAppender) {
        this.useFileAppender = useFileAppender;
    }
    
    public void setUseLogCatAppender(final boolean useLogCatAppender) {
        this.useLogCatAppender = useLogCatAppender;
    }
}
