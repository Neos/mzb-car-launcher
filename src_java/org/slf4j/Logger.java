package org.slf4j;

public interface Logger
{
    public static final String ROOT_LOGGER_NAME = "ROOT";
    
    void debug(final String p0);
    
    void debug(final String p0, final Object p1);
    
    void debug(final String p0, final Object p1, final Object p2);
    
    void debug(final String p0, final Throwable p1);
    
    void debug(final String p0, final Object... p1);
    
    void debug(final Marker p0, final String p1);
    
    void debug(final Marker p0, final String p1, final Object p2);
    
    void debug(final Marker p0, final String p1, final Object p2, final Object p3);
    
    void debug(final Marker p0, final String p1, final Throwable p2);
    
    void debug(final Marker p0, final String p1, final Object... p2);
    
    void error(final String p0);
    
    void error(final String p0, final Object p1);
    
    void error(final String p0, final Object p1, final Object p2);
    
    void error(final String p0, final Throwable p1);
    
    void error(final String p0, final Object... p1);
    
    void error(final Marker p0, final String p1);
    
    void error(final Marker p0, final String p1, final Object p2);
    
    void error(final Marker p0, final String p1, final Object p2, final Object p3);
    
    void error(final Marker p0, final String p1, final Throwable p2);
    
    void error(final Marker p0, final String p1, final Object... p2);
    
    String getName();
    
    void info(final String p0);
    
    void info(final String p0, final Object p1);
    
    void info(final String p0, final Object p1, final Object p2);
    
    void info(final String p0, final Throwable p1);
    
    void info(final String p0, final Object... p1);
    
    void info(final Marker p0, final String p1);
    
    void info(final Marker p0, final String p1, final Object p2);
    
    void info(final Marker p0, final String p1, final Object p2, final Object p3);
    
    void info(final Marker p0, final String p1, final Throwable p2);
    
    void info(final Marker p0, final String p1, final Object... p2);
    
    boolean isDebugEnabled();
    
    boolean isDebugEnabled(final Marker p0);
    
    boolean isErrorEnabled();
    
    boolean isErrorEnabled(final Marker p0);
    
    boolean isInfoEnabled();
    
    boolean isInfoEnabled(final Marker p0);
    
    boolean isTraceEnabled();
    
    boolean isTraceEnabled(final Marker p0);
    
    boolean isWarnEnabled();
    
    boolean isWarnEnabled(final Marker p0);
    
    void trace(final String p0);
    
    void trace(final String p0, final Object p1);
    
    void trace(final String p0, final Object p1, final Object p2);
    
    void trace(final String p0, final Throwable p1);
    
    void trace(final String p0, final Object... p1);
    
    void trace(final Marker p0, final String p1);
    
    void trace(final Marker p0, final String p1, final Object p2);
    
    void trace(final Marker p0, final String p1, final Object p2, final Object p3);
    
    void trace(final Marker p0, final String p1, final Throwable p2);
    
    void trace(final Marker p0, final String p1, final Object... p2);
    
    void warn(final String p0);
    
    void warn(final String p0, final Object p1);
    
    void warn(final String p0, final Object p1, final Object p2);
    
    void warn(final String p0, final Throwable p1);
    
    void warn(final String p0, final Object... p1);
    
    void warn(final Marker p0, final String p1);
    
    void warn(final Marker p0, final String p1, final Object p2);
    
    void warn(final Marker p0, final String p1, final Object p2, final Object p3);
    
    void warn(final Marker p0, final String p1, final Throwable p2);
    
    void warn(final Marker p0, final String p1, final Object... p2);
}
