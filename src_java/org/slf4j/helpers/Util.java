package org.slf4j.helpers;

public class Util
{
    public static final void report(final String s) {
        System.err.println("SLF4J: " + s);
    }
    
    public static final void report(final String s, final Throwable t) {
        System.err.println(s);
        System.err.println("Reported exception:");
        t.printStackTrace();
    }
}
