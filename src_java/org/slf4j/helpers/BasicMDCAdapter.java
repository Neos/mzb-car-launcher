package org.slf4j.helpers;

import org.slf4j.spi.*;
import java.util.*;

public class BasicMDCAdapter implements MDCAdapter
{
    static boolean IS_JDK14;
    private InheritableThreadLocal inheritableThreadLocal;
    
    static {
        BasicMDCAdapter.IS_JDK14 = isJDK14();
    }
    
    public BasicMDCAdapter() {
        this.inheritableThreadLocal = new InheritableThreadLocal();
    }
    
    static boolean isJDK14() {
        try {
            return System.getProperty("java.version").startsWith("1.4");
        }
        catch (SecurityException ex) {
            return false;
        }
    }
    
    @Override
    public void clear() {
        final Map map = (Map)this.inheritableThreadLocal.get();
        if (map != null) {
            map.clear();
            if (!isJDK14()) {
                this.inheritableThreadLocal.remove();
                return;
            }
            this.inheritableThreadLocal.set(null);
        }
    }
    
    @Override
    public String get(final String s) {
        final Map map = (Map)this.inheritableThreadLocal.get();
        if (map != null && s != null) {
            return map.get(s);
        }
        return null;
    }
    
    @Override
    public Map getCopyOfContextMap() {
        final Map map = (Map)this.inheritableThreadLocal.get();
        if (map != null) {
            final Map<Object, Object> synchronizedMap = Collections.synchronizedMap(new HashMap<Object, Object>());
            synchronized (map) {
                synchronizedMap.putAll(map);
                return synchronizedMap;
            }
        }
        return null;
    }
    
    public Set getKeys() {
        final Map map = (Map)this.inheritableThreadLocal.get();
        if (map != null) {
            return map.keySet();
        }
        return null;
    }
    
    @Override
    public void put(final String s, final String s2) {
        if (s == null) {
            throw new IllegalArgumentException("key cannot be null");
        }
        Map<String, String> synchronizedMap;
        if ((synchronizedMap = (Map<String, String>)this.inheritableThreadLocal.get()) == null) {
            synchronizedMap = Collections.synchronizedMap(new HashMap<String, String>());
            this.inheritableThreadLocal.set(synchronizedMap);
        }
        synchronizedMap.put(s, s2);
    }
    
    @Override
    public void remove(final String s) {
        final Map map = (Map)this.inheritableThreadLocal.get();
        if (map != null) {
            map.remove(s);
        }
    }
    
    @Override
    public void setContextMap(Map synchronizedMap) {
        synchronizedMap = Collections.synchronizedMap(new HashMap<Object, Object>(synchronizedMap));
        this.inheritableThreadLocal.set(synchronizedMap);
    }
}
