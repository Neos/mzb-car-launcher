package org.slf4j.helpers;

import org.slf4j.spi.*;
import java.util.*;

public class NOPMDCAdapter implements MDCAdapter
{
    @Override
    public void clear() {
    }
    
    @Override
    public String get(final String s) {
        return null;
    }
    
    @Override
    public Map getCopyOfContextMap() {
        return null;
    }
    
    @Override
    public void put(final String s, final String s2) {
    }
    
    @Override
    public void remove(final String s) {
    }
    
    @Override
    public void setContextMap(final Map map) {
    }
}
