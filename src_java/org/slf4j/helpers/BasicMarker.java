package org.slf4j.helpers;

import org.slf4j.*;
import java.util.*;

public class BasicMarker implements Marker
{
    private static String CLOSE;
    private static String OPEN;
    private static String SEP;
    private static final long serialVersionUID = 1803952589649545191L;
    private final String name;
    private List refereceList;
    
    static {
        BasicMarker.OPEN = "[ ";
        BasicMarker.CLOSE = " ]";
        BasicMarker.SEP = ", ";
    }
    
    BasicMarker(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("A marker name cannot be null");
        }
        this.name = name;
    }
    
    @Override
    public void add(final Marker marker) {
        // monitorenter(this)
        if (marker == null) {
            try {
                throw new IllegalArgumentException("A null value cannot be added to a Marker as reference.");
            }
            finally {
            }
            // monitorexit(this)
        }
        if (!this.contains(marker) && !marker.contains(this)) {
            if (this.refereceList == null) {
                this.refereceList = new Vector();
            }
            this.refereceList.add(marker);
        }
    }
    // monitorexit(this)
    
    @Override
    public boolean contains(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("Other cannot be null");
        }
        if (!this.name.equals(s)) {
            if (this.hasReferences()) {
                for (int i = 0; i < this.refereceList.size(); ++i) {
                    if (((Marker)this.refereceList.get(i)).contains(s)) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }
    
    @Override
    public boolean contains(final Marker marker) {
        if (marker == null) {
            throw new IllegalArgumentException("Other cannot be null");
        }
        if (!this.equals(marker)) {
            if (this.hasReferences()) {
                for (int i = 0; i < this.refereceList.size(); ++i) {
                    if (((Marker)this.refereceList.get(i)).contains(marker)) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        boolean b2;
        if (this == o) {
            b2 = true;
        }
        else {
            b2 = b;
            if (o != null) {
                b2 = b;
                if (o instanceof Marker) {
                    return this.name.equals(((Marker)o).getName());
                }
            }
        }
        return b2;
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    @Override
    public boolean hasChildren() {
        return this.hasReferences();
    }
    
    @Override
    public boolean hasReferences() {
        synchronized (this) {
            return this.refereceList != null && this.refereceList.size() > 0;
        }
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public Iterator iterator() {
        synchronized (this) {
            Iterator iterator;
            if (this.refereceList != null) {
                iterator = this.refereceList.iterator();
            }
            else {
                iterator = Collections.EMPTY_LIST.iterator();
            }
            return iterator;
        }
    }
    
    @Override
    public boolean remove(final Marker marker) {
        final boolean b = false;
        synchronized (this) {
            boolean b2;
            if (this.refereceList == null) {
                b2 = b;
            }
            else {
                final int size = this.refereceList.size();
                int n = 0;
                while (true) {
                    b2 = b;
                    if (n >= size) {
                        return b2;
                    }
                    if (marker.equals(this.refereceList.get(n))) {
                        break;
                    }
                    ++n;
                }
                this.refereceList.remove(n);
                b2 = true;
            }
            return b2;
        }
    }
    
    @Override
    public String toString() {
        if (!this.hasReferences()) {
            return this.getName();
        }
        final Iterator iterator = this.iterator();
        final StringBuffer sb = new StringBuffer(this.getName());
        sb.append(' ').append(BasicMarker.OPEN);
        while (iterator.hasNext()) {
            sb.append(iterator.next().getName());
            if (iterator.hasNext()) {
                sb.append(BasicMarker.SEP);
            }
        }
        sb.append(BasicMarker.CLOSE);
        return sb.toString();
    }
}
