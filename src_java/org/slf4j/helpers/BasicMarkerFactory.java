package org.slf4j.helpers;

import org.slf4j.*;
import java.util.concurrent.*;

public class BasicMarkerFactory implements IMarkerFactory
{
    private final ConcurrentMap<String, Marker> markerMap;
    
    public BasicMarkerFactory() {
        this.markerMap = new ConcurrentHashMap<String, Marker>();
    }
    
    @Override
    public boolean detachMarker(final String s) {
        return s != null && this.markerMap.remove(s) != null;
    }
    
    @Override
    public boolean exists(final String s) {
        return s != null && this.markerMap.containsKey(s);
    }
    
    @Override
    public Marker getDetachedMarker(final String s) {
        return new BasicMarker(s);
    }
    
    @Override
    public Marker getMarker(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("Marker name cannot be null");
        }
        Marker marker;
        if ((marker = this.markerMap.get(s)) == null) {
            marker = new BasicMarker(s);
            final Marker marker2 = this.markerMap.putIfAbsent(s, marker);
            if (marker2 != null) {
                marker = marker2;
            }
        }
        return marker;
    }
}
