package org.slf4j.helpers;

public class FormattingTuple
{
    public static FormattingTuple NULL;
    private Object[] argArray;
    private String message;
    private Throwable throwable;
    
    static {
        FormattingTuple.NULL = new FormattingTuple(null);
    }
    
    public FormattingTuple(final String s) {
        this(s, null, null);
    }
    
    public FormattingTuple(final String message, final Object[] argArray, final Throwable throwable) {
        this.message = message;
        this.throwable = throwable;
        if (throwable == null) {
            this.argArray = argArray;
            return;
        }
        this.argArray = trimmedCopy(argArray);
    }
    
    static Object[] trimmedCopy(final Object[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalStateException("non-sensical empty or null argument array");
        }
        final int n = array.length - 1;
        final Object[] array2 = new Object[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    public Object[] getArgArray() {
        return this.argArray;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public Throwable getThrowable() {
        return this.throwable;
    }
}
