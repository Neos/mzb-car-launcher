package org.slf4j.helpers;

import java.util.*;

public final class MessageFormatter
{
    static final char DELIM_START = '{';
    static final char DELIM_STOP = '}';
    static final String DELIM_STR = "{}";
    private static final char ESCAPE_CHAR = '\\';
    
    public static final FormattingTuple arrayFormat(final String s, final Object[] array) {
        final Throwable throwableCandidate = getThrowableCandidate(array);
        if (s == null) {
            return new FormattingTuple(null, array, throwableCandidate);
        }
        if (array == null) {
            return new FormattingTuple(s);
        }
        int n = 0;
        final StringBuilder sb = new StringBuilder(s.length() + 50);
        int i = 0;
        while (i < array.length) {
            final int index = s.indexOf("{}", n);
            if (index == -1) {
                if (n == 0) {
                    return new FormattingTuple(s, array, throwableCandidate);
                }
                sb.append(s.substring(n, s.length()));
                return new FormattingTuple(sb.toString(), array, throwableCandidate);
            }
            else {
                if (isEscapedDelimeter(s, index)) {
                    if (!isDoubleEscaped(s, index)) {
                        --i;
                        sb.append(s.substring(n, index - 1));
                        sb.append('{');
                        n = index + 1;
                    }
                    else {
                        sb.append(s.substring(n, index - 1));
                        deeplyAppendParameter(sb, array[i], new HashMap());
                        n = index + 2;
                    }
                }
                else {
                    sb.append(s.substring(n, index));
                    deeplyAppendParameter(sb, array[i], new HashMap());
                    n = index + 2;
                }
                ++i;
            }
        }
        sb.append(s.substring(n, s.length()));
        if (i < array.length - 1) {
            return new FormattingTuple(sb.toString(), array, throwableCandidate);
        }
        return new FormattingTuple(sb.toString(), array, null);
    }
    
    private static void booleanArrayAppend(final StringBuilder sb, final boolean[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    private static void byteArrayAppend(final StringBuilder sb, final byte[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    private static void charArrayAppend(final StringBuilder sb, final char[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    private static void deeplyAppendParameter(final StringBuilder sb, final Object o, final Map map) {
        if (o == null) {
            sb.append("null");
            return;
        }
        if (!o.getClass().isArray()) {
            safeObjectAppend(sb, o);
            return;
        }
        if (o instanceof boolean[]) {
            booleanArrayAppend(sb, (boolean[])o);
            return;
        }
        if (o instanceof byte[]) {
            byteArrayAppend(sb, (byte[])o);
            return;
        }
        if (o instanceof char[]) {
            charArrayAppend(sb, (char[])o);
            return;
        }
        if (o instanceof short[]) {
            shortArrayAppend(sb, (short[])o);
            return;
        }
        if (o instanceof int[]) {
            intArrayAppend(sb, (int[])o);
            return;
        }
        if (o instanceof long[]) {
            longArrayAppend(sb, (long[])o);
            return;
        }
        if (o instanceof float[]) {
            floatArrayAppend(sb, (float[])o);
            return;
        }
        if (o instanceof double[]) {
            doubleArrayAppend(sb, (double[])o);
            return;
        }
        objectArrayAppend(sb, (Object[])o, map);
    }
    
    private static void doubleArrayAppend(final StringBuilder sb, final double[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    private static void floatArrayAppend(final StringBuilder sb, final float[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    public static final FormattingTuple format(final String s, final Object o) {
        return arrayFormat(s, new Object[] { o });
    }
    
    public static final FormattingTuple format(final String s, final Object o, final Object o2) {
        return arrayFormat(s, new Object[] { o, o2 });
    }
    
    static final Throwable getThrowableCandidate(final Object[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        final Object o = array[array.length - 1];
        if (o instanceof Throwable) {
            return (Throwable)o;
        }
        return null;
    }
    
    private static void intArrayAppend(final StringBuilder sb, final int[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    static final boolean isDoubleEscaped(final String s, final int n) {
        return n >= 2 && s.charAt(n - 2) == '\\';
    }
    
    static final boolean isEscapedDelimeter(final String s, final int n) {
        return n != 0 && s.charAt(n - 1) == '\\';
    }
    
    private static void longArrayAppend(final StringBuilder sb, final long[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
    
    private static void objectArrayAppend(final StringBuilder sb, final Object[] array, final Map map) {
        sb.append('[');
        if (!map.containsKey(array)) {
            map.put(array, null);
            for (int length = array.length, i = 0; i < length; ++i) {
                deeplyAppendParameter(sb, array[i], map);
                if (i != length - 1) {
                    sb.append(", ");
                }
            }
            map.remove(array);
        }
        else {
            sb.append("...");
        }
        sb.append(']');
    }
    
    private static void safeObjectAppend(final StringBuilder sb, final Object o) {
        try {
            sb.append(o.toString());
        }
        catch (Throwable t) {
            System.err.println("SLF4J: Failed toString() invocation on an object of type [" + o.getClass().getName() + "]");
            t.printStackTrace();
            sb.append("[FAILED toString()]");
        }
    }
    
    private static void shortArrayAppend(final StringBuilder sb, final short[] array) {
        sb.append('[');
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i]);
            if (i != length - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
    }
}
