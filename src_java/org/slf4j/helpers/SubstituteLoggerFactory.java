package org.slf4j.helpers;

import java.util.concurrent.*;
import org.slf4j.*;
import java.util.*;

public class SubstituteLoggerFactory implements ILoggerFactory
{
    final ConcurrentMap<String, SubstituteLogger> loggers;
    
    public SubstituteLoggerFactory() {
        this.loggers = new ConcurrentHashMap<String, SubstituteLogger>();
    }
    
    public void clear() {
        this.loggers.clear();
    }
    
    @Override
    public Logger getLogger(final String s) {
        SubstituteLogger substituteLogger;
        if ((substituteLogger = this.loggers.get(s)) == null) {
            substituteLogger = new SubstituteLogger(s);
            final SubstituteLogger substituteLogger2 = this.loggers.putIfAbsent(s, substituteLogger);
            if (substituteLogger2 != null) {
                substituteLogger = substituteLogger2;
            }
        }
        return substituteLogger;
    }
    
    public List getLoggerNames() {
        return new ArrayList(this.loggers.keySet());
    }
    
    public List<SubstituteLogger> getLoggers() {
        return new ArrayList<SubstituteLogger>(this.loggers.values());
    }
}
