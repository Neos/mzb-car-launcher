package org.slf4j.helpers;

import org.slf4j.*;

public class SubstituteLogger implements Logger
{
    private volatile Logger _delegate;
    private final String name;
    
    public SubstituteLogger(final String name) {
        this.name = name;
    }
    
    @Override
    public void debug(final String s) {
        this.delegate().debug(s);
    }
    
    @Override
    public void debug(final String s, final Object o) {
        this.delegate().debug(s, o);
    }
    
    @Override
    public void debug(final String s, final Object o, final Object o2) {
        this.delegate().debug(s, o, o2);
    }
    
    @Override
    public void debug(final String s, final Throwable t) {
        this.delegate().debug(s, t);
    }
    
    @Override
    public void debug(final String s, final Object... array) {
        this.delegate().debug(s, array);
    }
    
    @Override
    public void debug(final Marker marker, final String s) {
        this.delegate().debug(marker, s);
    }
    
    @Override
    public void debug(final Marker marker, final String s, final Object o) {
        this.delegate().debug(marker, s, o);
    }
    
    @Override
    public void debug(final Marker marker, final String s, final Object o, final Object o2) {
        this.delegate().debug(marker, s, o, o2);
    }
    
    @Override
    public void debug(final Marker marker, final String s, final Throwable t) {
        this.delegate().debug(marker, s, t);
    }
    
    @Override
    public void debug(final Marker marker, final String s, final Object... array) {
        this.delegate().debug(marker, s, array);
    }
    
    Logger delegate() {
        if (this._delegate != null) {
            return this._delegate;
        }
        return NOPLogger.NOP_LOGGER;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this != o) {
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            if (!this.name.equals(((SubstituteLogger)o).name)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void error(final String s) {
        this.delegate().error(s);
    }
    
    @Override
    public void error(final String s, final Object o) {
        this.delegate().error(s, o);
    }
    
    @Override
    public void error(final String s, final Object o, final Object o2) {
        this.delegate().error(s, o, o2);
    }
    
    @Override
    public void error(final String s, final Throwable t) {
        this.delegate().error(s, t);
    }
    
    @Override
    public void error(final String s, final Object... array) {
        this.delegate().error(s, array);
    }
    
    @Override
    public void error(final Marker marker, final String s) {
        this.delegate().error(marker, s);
    }
    
    @Override
    public void error(final Marker marker, final String s, final Object o) {
        this.delegate().error(marker, s, o);
    }
    
    @Override
    public void error(final Marker marker, final String s, final Object o, final Object o2) {
        this.delegate().error(marker, s, o, o2);
    }
    
    @Override
    public void error(final Marker marker, final String s, final Throwable t) {
        this.delegate().error(marker, s, t);
    }
    
    @Override
    public void error(final Marker marker, final String s, final Object... array) {
        this.delegate().error(marker, s, array);
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public void info(final String s) {
        this.delegate().info(s);
    }
    
    @Override
    public void info(final String s, final Object o) {
        this.delegate().info(s, o);
    }
    
    @Override
    public void info(final String s, final Object o, final Object o2) {
        this.delegate().info(s, o, o2);
    }
    
    @Override
    public void info(final String s, final Throwable t) {
        this.delegate().info(s, t);
    }
    
    @Override
    public void info(final String s, final Object... array) {
        this.delegate().info(s, array);
    }
    
    @Override
    public void info(final Marker marker, final String s) {
        this.delegate().info(marker, s);
    }
    
    @Override
    public void info(final Marker marker, final String s, final Object o) {
        this.delegate().info(marker, s, o);
    }
    
    @Override
    public void info(final Marker marker, final String s, final Object o, final Object o2) {
        this.delegate().info(marker, s, o, o2);
    }
    
    @Override
    public void info(final Marker marker, final String s, final Throwable t) {
        this.delegate().info(marker, s, t);
    }
    
    @Override
    public void info(final Marker marker, final String s, final Object... array) {
        this.delegate().info(marker, s, array);
    }
    
    @Override
    public boolean isDebugEnabled() {
        return this.delegate().isDebugEnabled();
    }
    
    @Override
    public boolean isDebugEnabled(final Marker marker) {
        return this.delegate().isDebugEnabled(marker);
    }
    
    @Override
    public boolean isErrorEnabled() {
        return this.delegate().isErrorEnabled();
    }
    
    @Override
    public boolean isErrorEnabled(final Marker marker) {
        return this.delegate().isErrorEnabled(marker);
    }
    
    @Override
    public boolean isInfoEnabled() {
        return this.delegate().isInfoEnabled();
    }
    
    @Override
    public boolean isInfoEnabled(final Marker marker) {
        return this.delegate().isInfoEnabled(marker);
    }
    
    @Override
    public boolean isTraceEnabled() {
        return this.delegate().isTraceEnabled();
    }
    
    @Override
    public boolean isTraceEnabled(final Marker marker) {
        return this.delegate().isTraceEnabled(marker);
    }
    
    @Override
    public boolean isWarnEnabled() {
        return this.delegate().isWarnEnabled();
    }
    
    @Override
    public boolean isWarnEnabled(final Marker marker) {
        return this.delegate().isWarnEnabled(marker);
    }
    
    public void setDelegate(final Logger delegate) {
        this._delegate = delegate;
    }
    
    @Override
    public void trace(final String s) {
        this.delegate().trace(s);
    }
    
    @Override
    public void trace(final String s, final Object o) {
        this.delegate().trace(s, o);
    }
    
    @Override
    public void trace(final String s, final Object o, final Object o2) {
        this.delegate().trace(s, o, o2);
    }
    
    @Override
    public void trace(final String s, final Throwable t) {
        this.delegate().trace(s, t);
    }
    
    @Override
    public void trace(final String s, final Object... array) {
        this.delegate().trace(s, array);
    }
    
    @Override
    public void trace(final Marker marker, final String s) {
        this.delegate().trace(marker, s);
    }
    
    @Override
    public void trace(final Marker marker, final String s, final Object o) {
        this.delegate().trace(marker, s, o);
    }
    
    @Override
    public void trace(final Marker marker, final String s, final Object o, final Object o2) {
        this.delegate().trace(marker, s, o, o2);
    }
    
    @Override
    public void trace(final Marker marker, final String s, final Throwable t) {
        this.delegate().trace(marker, s, t);
    }
    
    @Override
    public void trace(final Marker marker, final String s, final Object... array) {
        this.delegate().trace(marker, s, array);
    }
    
    @Override
    public void warn(final String s) {
        this.delegate().warn(s);
    }
    
    @Override
    public void warn(final String s, final Object o) {
        this.delegate().warn(s, o);
    }
    
    @Override
    public void warn(final String s, final Object o, final Object o2) {
        this.delegate().warn(s, o, o2);
    }
    
    @Override
    public void warn(final String s, final Throwable t) {
        this.delegate().warn(s, t);
    }
    
    @Override
    public void warn(final String s, final Object... array) {
        this.delegate().warn(s, array);
    }
    
    @Override
    public void warn(final Marker marker, final String s) {
        this.delegate().warn(marker, s);
    }
    
    @Override
    public void warn(final Marker marker, final String s, final Object o) {
        this.delegate().warn(marker, s, o);
    }
    
    @Override
    public void warn(final Marker marker, final String s, final Object o, final Object o2) {
        this.delegate().warn(marker, s, o, o2);
    }
    
    @Override
    public void warn(final Marker marker, final String s, final Throwable t) {
        this.delegate().warn(marker, s, t);
    }
    
    @Override
    public void warn(final Marker marker, final String s, final Object... array) {
        this.delegate().warn(marker, s, array);
    }
}
