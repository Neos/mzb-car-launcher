package org.slf4j.helpers;

import org.slf4j.*;

public class NOPLoggerFactory implements ILoggerFactory
{
    @Override
    public Logger getLogger(final String s) {
        return NOPLogger.NOP_LOGGER;
    }
}
