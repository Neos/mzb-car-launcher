package org.slf4j;

public interface IMarkerFactory
{
    boolean detachMarker(final String p0);
    
    boolean exists(final String p0);
    
    Marker getDetachedMarker(final String p0);
    
    Marker getMarker(final String p0);
}
