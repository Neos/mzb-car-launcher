package org.slf4j;

import org.slf4j.impl.*;
import java.net.*;
import java.io.*;
import org.slf4j.helpers.*;
import java.util.*;

public final class LoggerFactory
{
    private static final String[] API_COMPATIBILITY_LIST;
    static final String CODES_PREFIX = "http://www.slf4j.org/codes.html";
    static final int FAILED_INITIALIZATION = 2;
    static int INITIALIZATION_STATE = 0;
    static final String MULTIPLE_BINDINGS_URL = "http://www.slf4j.org/codes.html#multiple_bindings";
    static NOPLoggerFactory NOP_FALLBACK_FACTORY;
    static final int NOP_FALLBACK_INITIALIZATION = 4;
    static final String NO_STATICLOGGERBINDER_URL = "http://www.slf4j.org/codes.html#StaticLoggerBinder";
    static final String NULL_LF_URL = "http://www.slf4j.org/codes.html#null_LF";
    static final int ONGOING_INITIALIZATION = 1;
    private static String STATIC_LOGGER_BINDER_PATH;
    static final String SUBSTITUTE_LOGGER_URL = "http://www.slf4j.org/codes.html#substituteLogger";
    static final int SUCCESSFUL_INITIALIZATION = 3;
    static SubstituteLoggerFactory TEMP_FACTORY;
    static final int UNINITIALIZED = 0;
    static final String UNSUCCESSFUL_INIT_MSG = "org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit";
    static final String UNSUCCESSFUL_INIT_URL = "http://www.slf4j.org/codes.html#unsuccessfulInit";
    static final String VERSION_MISMATCH = "http://www.slf4j.org/codes.html#version_mismatch";
    
    static {
        LoggerFactory.INITIALIZATION_STATE = 0;
        LoggerFactory.TEMP_FACTORY = new SubstituteLoggerFactory();
        LoggerFactory.NOP_FALLBACK_FACTORY = new NOPLoggerFactory();
        API_COMPATIBILITY_LIST = new String[] { "1.6", "1.7" };
        LoggerFactory.STATIC_LOGGER_BINDER_PATH = "org/slf4j/impl/StaticLoggerBinder.class";
    }
    
    private static final void bind() {
        try {
            final Set possibleStaticLoggerBinderPathSet = findPossibleStaticLoggerBinderPathSet();
            reportMultipleBindingAmbiguity(possibleStaticLoggerBinderPathSet);
            StaticLoggerBinder.getSingleton();
            LoggerFactory.INITIALIZATION_STATE = 3;
            reportActualBinding(possibleStaticLoggerBinderPathSet);
            fixSubstitutedLoggers();
        }
        catch (NoClassDefFoundError noClassDefFoundError) {
            if (messageContainsOrgSlf4jImplStaticLoggerBinder(noClassDefFoundError.getMessage())) {
                LoggerFactory.INITIALIZATION_STATE = 4;
                Util.report("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                Util.report("Defaulting to no-operation (NOP) logger implementation");
                Util.report("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            failedBinding(noClassDefFoundError);
            throw noClassDefFoundError;
        }
        catch (NoSuchMethodError noSuchMethodError) {
            final String message = noSuchMethodError.getMessage();
            if (message != null && message.indexOf("org.slf4j.impl.StaticLoggerBinder.getSingleton()") != -1) {
                LoggerFactory.INITIALIZATION_STATE = 2;
                Util.report("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                Util.report("Your binding is version 1.5.5 or earlier.");
                Util.report("Upgrade your binding to version 1.6.x.");
            }
            throw noSuchMethodError;
        }
        catch (Exception ex) {
            failedBinding(ex);
            throw new IllegalStateException("Unexpected initialization failure", ex);
        }
    }
    
    static void failedBinding(final Throwable t) {
        LoggerFactory.INITIALIZATION_STATE = 2;
        Util.report("Failed to instantiate SLF4J LoggerFactory", t);
    }
    
    private static Set findPossibleStaticLoggerBinderPathSet() {
        LinkedHashSet<URL> set;
        while (true) {
            set = new LinkedHashSet<URL>();
            while (true) {
                ClassLoader classLoader = null;
                Label_0062: {
                    try {
                        classLoader = LoggerFactory.class.getClassLoader();
                        if (classLoader != null) {
                            break Label_0062;
                        }
                        final Enumeration<URL> enumeration = ClassLoader.getSystemResources(LoggerFactory.STATIC_LOGGER_BINDER_PATH);
                        while (enumeration.hasMoreElements()) {
                            set.add(enumeration.nextElement());
                        }
                    }
                    catch (IOException ex) {
                        Util.report("Error getting resources from path", ex);
                    }
                    break;
                }
                final Enumeration<URL> enumeration = classLoader.getResources(LoggerFactory.STATIC_LOGGER_BINDER_PATH);
                continue;
            }
        }
        return set;
    }
    
    private static final void fixSubstitutedLoggers() {
        final List<SubstituteLogger> loggers = LoggerFactory.TEMP_FACTORY.getLoggers();
        if (loggers.isEmpty()) {
            return;
        }
        Util.report("The following set of substitute loggers may have been accessed");
        Util.report("during the initialization phase. Logging calls during this");
        Util.report("phase were not honored. However, subsequent logging calls to these");
        Util.report("loggers will work as normally expected.");
        Util.report("See also http://www.slf4j.org/codes.html#substituteLogger");
        for (final SubstituteLogger substituteLogger : loggers) {
            substituteLogger.setDelegate(getLogger(substituteLogger.getName()));
            Util.report(substituteLogger.getName());
        }
        LoggerFactory.TEMP_FACTORY.clear();
    }
    
    public static ILoggerFactory getILoggerFactory() {
        if (LoggerFactory.INITIALIZATION_STATE == 0) {
            LoggerFactory.INITIALIZATION_STATE = 1;
            performInitialization();
        }
        switch (LoggerFactory.INITIALIZATION_STATE) {
            default: {
                throw new IllegalStateException("Unreachable code");
            }
            case 3: {
                return StaticLoggerBinder.getSingleton().getLoggerFactory();
            }
            case 4: {
                return LoggerFactory.NOP_FALLBACK_FACTORY;
            }
            case 2: {
                throw new IllegalStateException("org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
            }
            case 1: {
                return LoggerFactory.TEMP_FACTORY;
            }
        }
    }
    
    public static Logger getLogger(final Class clazz) {
        return getLogger(clazz.getName());
    }
    
    public static Logger getLogger(final String s) {
        return getILoggerFactory().getLogger(s);
    }
    
    private static boolean isAmbiguousStaticLoggerBinderPathSet(final Set set) {
        return set.size() > 1;
    }
    
    private static boolean messageContainsOrgSlf4jImplStaticLoggerBinder(final String s) {
        if (s != null) {
            if (s.indexOf("org/slf4j/impl/StaticLoggerBinder") != -1) {
                return true;
            }
            if (s.indexOf("org.slf4j.impl.StaticLoggerBinder") != -1) {
                return true;
            }
        }
        return false;
    }
    
    private static final void performInitialization() {
        bind();
        if (LoggerFactory.INITIALIZATION_STATE == 3) {
            versionSanityCheck();
        }
    }
    
    private static void reportActualBinding(final Set set) {
        if (isAmbiguousStaticLoggerBinderPathSet(set)) {
            Util.report("Actual binding is of type [" + StaticLoggerBinder.getSingleton().getLoggerFactoryClassStr() + "]");
        }
    }
    
    private static void reportMultipleBindingAmbiguity(final Set set) {
        if (isAmbiguousStaticLoggerBinderPathSet(set)) {
            Util.report("Class path contains multiple SLF4J bindings.");
            final Iterator<URL> iterator = set.iterator();
            while (iterator.hasNext()) {
                Util.report("Found binding in [" + iterator.next() + "]");
            }
            Util.report("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }
    
    static void reset() {
        LoggerFactory.INITIALIZATION_STATE = 0;
        LoggerFactory.TEMP_FACTORY = new SubstituteLoggerFactory();
    }
    
    private static final void versionSanityCheck() {
        while (true) {
            while (true) {
                int n;
                try {
                    final String requested_API_VERSION = StaticLoggerBinder.REQUESTED_API_VERSION;
                    boolean b = false;
                    n = 0;
                    if (n >= LoggerFactory.API_COMPATIBILITY_LIST.length) {
                        if (!b) {
                            Util.report("The requested version " + requested_API_VERSION + " by your slf4j binding is not compatible with " + Arrays.asList(LoggerFactory.API_COMPATIBILITY_LIST).toString());
                            Util.report("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
                        }
                        return;
                    }
                    if (requested_API_VERSION.startsWith(LoggerFactory.API_COMPATIBILITY_LIST[n])) {
                        b = true;
                    }
                }
                catch (Throwable t) {
                    Util.report("Unexpected problem occured during version sanity check", t);
                    return;
                }
                catch (NoSuchFieldError noSuchFieldError) {
                    return;
                }
                ++n;
                continue;
            }
        }
    }
}
