package org.slf4j.impl;

import org.slf4j.*;
import java.util.concurrent.*;
import org.apache.log4j.*;

public class Log4jLoggerFactory implements ILoggerFactory
{
    ConcurrentMap<String, Logger> loggerMap;
    
    public Log4jLoggerFactory() {
        this.loggerMap = new ConcurrentHashMap<String, Logger>();
    }
    
    @Override
    public Logger getLogger(final String s) {
        final Logger logger = this.loggerMap.get(s);
        if (logger != null) {
            return logger;
        }
        org.apache.log4j.Logger logger2;
        if (s.equalsIgnoreCase("ROOT")) {
            logger2 = LogManager.getRootLogger();
        }
        else {
            logger2 = LogManager.getLogger(s);
        }
        final Log4jLoggerAdapter log4jLoggerAdapter = new Log4jLoggerAdapter(logger2);
        Log4jLoggerAdapter log4jLoggerAdapter2 = this.loggerMap.putIfAbsent(s, log4jLoggerAdapter);
        if (log4jLoggerAdapter2 == null) {
            log4jLoggerAdapter2 = log4jLoggerAdapter;
        }
        return log4jLoggerAdapter2;
    }
}
