package org.slf4j.impl;

import org.slf4j.spi.*;
import java.io.*;
import org.apache.log4j.*;
import org.slf4j.helpers.*;
import org.slf4j.*;

public final class Log4jLoggerAdapter extends MarkerIgnoringBase implements LocationAwareLogger, Serializable
{
    static final String FQCN;
    private static final long serialVersionUID = 6182834493563598289L;
    final transient org.apache.log4j.Logger logger;
    final boolean traceCapable;
    
    static {
        FQCN = Log4jLoggerAdapter.class.getName();
    }
    
    Log4jLoggerAdapter(final org.apache.log4j.Logger logger) {
        this.logger = logger;
        this.name = logger.getName();
        this.traceCapable = this.isTraceCapable();
    }
    
    private boolean isTraceCapable() {
        try {
            this.logger.isTraceEnabled();
            return true;
        }
        catch (NoSuchMethodError noSuchMethodError) {
            return false;
        }
    }
    
    @Override
    public void debug(final String s) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, s, null);
    }
    
    @Override
    public void debug(final String s, final Object o) {
        if (this.logger.isDebugEnabled()) {
            final FormattingTuple format = MessageFormatter.format(s, o);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void debug(final String s, final Object o, final Object o2) {
        if (this.logger.isDebugEnabled()) {
            final FormattingTuple format = MessageFormatter.format(s, o, o2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void debug(final String s, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, s, t);
    }
    
    @Override
    public void debug(final String s, final Object... array) {
        if (this.logger.isDebugEnabled()) {
            final FormattingTuple arrayFormat = MessageFormatter.arrayFormat(s, array);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.DEBUG, arrayFormat.getMessage(), arrayFormat.getThrowable());
        }
    }
    
    @Override
    public void error(final String s) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, s, null);
    }
    
    @Override
    public void error(final String s, final Object o) {
        if (this.logger.isEnabledFor(Level.ERROR)) {
            final FormattingTuple format = MessageFormatter.format(s, o);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void error(final String s, final Object o, final Object o2) {
        if (this.logger.isEnabledFor(Level.ERROR)) {
            final FormattingTuple format = MessageFormatter.format(s, o, o2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void error(final String s, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, s, t);
    }
    
    @Override
    public void error(final String s, final Object... array) {
        if (this.logger.isEnabledFor(Level.ERROR)) {
            final FormattingTuple arrayFormat = MessageFormatter.arrayFormat(s, array);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.ERROR, arrayFormat.getMessage(), arrayFormat.getThrowable());
        }
    }
    
    @Override
    public void info(final String s) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, s, null);
    }
    
    @Override
    public void info(final String s, final Object o) {
        if (this.logger.isInfoEnabled()) {
            final FormattingTuple format = MessageFormatter.format(s, o);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void info(final String s, final Object o, final Object o2) {
        if (this.logger.isInfoEnabled()) {
            final FormattingTuple format = MessageFormatter.format(s, o, o2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void info(final String s, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, s, t);
    }
    
    @Override
    public void info(final String s, final Object... array) {
        if (this.logger.isInfoEnabled()) {
            final FormattingTuple arrayFormat = MessageFormatter.arrayFormat(s, array);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.INFO, arrayFormat.getMessage(), arrayFormat.getThrowable());
        }
    }
    
    @Override
    public boolean isDebugEnabled() {
        return this.logger.isDebugEnabled();
    }
    
    @Override
    public boolean isErrorEnabled() {
        return this.logger.isEnabledFor(Level.ERROR);
    }
    
    @Override
    public boolean isInfoEnabled() {
        return this.logger.isInfoEnabled();
    }
    
    @Override
    public boolean isTraceEnabled() {
        if (this.traceCapable) {
            return this.logger.isTraceEnabled();
        }
        return this.logger.isDebugEnabled();
    }
    
    @Override
    public boolean isWarnEnabled() {
        return this.logger.isEnabledFor(Level.WARN);
    }
    
    @Override
    public void log(final Marker marker, final String s, final int n, final String s2, final Object[] array, final Throwable t) {
        Level level = null;
        switch (n) {
            default: {
                throw new IllegalStateException("Level number " + n + " is not recognized.");
            }
            case 0: {
                if (this.traceCapable) {
                    level = Level.TRACE;
                    break;
                }
                level = Level.DEBUG;
                break;
            }
            case 10: {
                level = Level.DEBUG;
                break;
            }
            case 20: {
                level = Level.INFO;
                break;
            }
            case 30: {
                level = Level.WARN;
                break;
            }
            case 40: {
                level = Level.ERROR;
                break;
            }
        }
        this.logger.log(s, level, s2, t);
    }
    
    @Override
    public void trace(final String s) {
        final org.apache.log4j.Logger logger = this.logger;
        final String fqcn = Log4jLoggerAdapter.FQCN;
        Level level;
        if (this.traceCapable) {
            level = Level.TRACE;
        }
        else {
            level = Level.DEBUG;
        }
        logger.log(fqcn, level, s, null);
    }
    
    @Override
    public void trace(final String s, final Object o) {
        if (this.isTraceEnabled()) {
            final FormattingTuple format = MessageFormatter.format(s, o);
            final org.apache.log4j.Logger logger = this.logger;
            final String fqcn = Log4jLoggerAdapter.FQCN;
            Level level;
            if (this.traceCapable) {
                level = Level.TRACE;
            }
            else {
                level = Level.DEBUG;
            }
            logger.log(fqcn, level, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void trace(final String s, final Object o, final Object o2) {
        if (this.isTraceEnabled()) {
            final FormattingTuple format = MessageFormatter.format(s, o, o2);
            final org.apache.log4j.Logger logger = this.logger;
            final String fqcn = Log4jLoggerAdapter.FQCN;
            Level level;
            if (this.traceCapable) {
                level = Level.TRACE;
            }
            else {
                level = Level.DEBUG;
            }
            logger.log(fqcn, level, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void trace(final String s, final Throwable t) {
        final org.apache.log4j.Logger logger = this.logger;
        final String fqcn = Log4jLoggerAdapter.FQCN;
        Level level;
        if (this.traceCapable) {
            level = Level.TRACE;
        }
        else {
            level = Level.DEBUG;
        }
        logger.log(fqcn, level, s, t);
    }
    
    @Override
    public void trace(final String s, final Object... array) {
        if (this.isTraceEnabled()) {
            final FormattingTuple arrayFormat = MessageFormatter.arrayFormat(s, array);
            final org.apache.log4j.Logger logger = this.logger;
            final String fqcn = Log4jLoggerAdapter.FQCN;
            Level level;
            if (this.traceCapable) {
                level = Level.TRACE;
            }
            else {
                level = Level.DEBUG;
            }
            logger.log(fqcn, level, arrayFormat.getMessage(), arrayFormat.getThrowable());
        }
    }
    
    @Override
    public void warn(final String s) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, s, null);
    }
    
    @Override
    public void warn(final String s, final Object o) {
        if (this.logger.isEnabledFor(Level.WARN)) {
            final FormattingTuple format = MessageFormatter.format(s, o);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void warn(final String s, final Object o, final Object o2) {
        if (this.logger.isEnabledFor(Level.WARN)) {
            final FormattingTuple format = MessageFormatter.format(s, o, o2);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, format.getMessage(), format.getThrowable());
        }
    }
    
    @Override
    public void warn(final String s, final Throwable t) {
        this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, s, t);
    }
    
    @Override
    public void warn(final String s, final Object... array) {
        if (this.logger.isEnabledFor(Level.WARN)) {
            final FormattingTuple arrayFormat = MessageFormatter.arrayFormat(s, array);
            this.logger.log(Log4jLoggerAdapter.FQCN, Level.WARN, arrayFormat.getMessage(), arrayFormat.getThrowable());
        }
    }
}
