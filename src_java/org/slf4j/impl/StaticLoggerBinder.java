package org.slf4j.impl;

import org.slf4j.spi.*;
import org.slf4j.*;
import org.apache.log4j.*;
import org.slf4j.helpers.*;

public class StaticLoggerBinder implements LoggerFactoryBinder
{
    public static String REQUESTED_API_VERSION;
    private static final StaticLoggerBinder SINGLETON;
    private static final String loggerFactoryClassStr;
    private final ILoggerFactory loggerFactory;
    
    static {
        SINGLETON = new StaticLoggerBinder();
        StaticLoggerBinder.REQUESTED_API_VERSION = "1.6.99";
        loggerFactoryClassStr = Log4jLoggerFactory.class.getName();
    }
    
    private StaticLoggerBinder() {
        this.loggerFactory = new Log4jLoggerFactory();
        try {
            final Level trace = Level.TRACE;
        }
        catch (NoSuchFieldError noSuchFieldError) {
            Util.report("This version of SLF4J requires log4j version 1.2.12 or later. See also http://www.slf4j.org/codes.html#log4j_version");
        }
    }
    
    public static final StaticLoggerBinder getSingleton() {
        return StaticLoggerBinder.SINGLETON;
    }
    
    @Override
    public ILoggerFactory getLoggerFactory() {
        return this.loggerFactory;
    }
    
    @Override
    public String getLoggerFactoryClassStr() {
        return StaticLoggerBinder.loggerFactoryClassStr;
    }
}
