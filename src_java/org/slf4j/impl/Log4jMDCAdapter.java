package org.slf4j.impl;

import org.slf4j.spi.*;
import org.apache.log4j.*;
import java.util.*;

public class Log4jMDCAdapter implements MDCAdapter
{
    @Override
    public void clear() {
        final Hashtable context = MDC.getContext();
        if (context != null) {
            context.clear();
        }
    }
    
    @Override
    public String get(final String s) {
        return (String)MDC.get(s);
    }
    
    @Override
    public Map getCopyOfContextMap() {
        final Hashtable context = MDC.getContext();
        if (context != null) {
            return new HashMap(context);
        }
        return null;
    }
    
    @Override
    public void put(final String s, final String s2) {
        MDC.put(s, s2);
    }
    
    @Override
    public void remove(final String s) {
        MDC.remove(s);
    }
    
    @Override
    public void setContextMap(final Map map) {
        final Hashtable context = MDC.getContext();
        if (context == null) {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                MDC.put(entry.getKey(), entry.getValue());
            }
        }
        else {
            context.clear();
            context.putAll(map);
        }
    }
}
