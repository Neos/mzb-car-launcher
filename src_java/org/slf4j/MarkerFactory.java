package org.slf4j;

import org.slf4j.impl.*;
import org.slf4j.helpers.*;

public class MarkerFactory
{
    static IMarkerFactory markerFactory;
    
    static {
        try {
            MarkerFactory.markerFactory = StaticMarkerBinder.SINGLETON.getMarkerFactory();
        }
        catch (NoClassDefFoundError noClassDefFoundError) {
            MarkerFactory.markerFactory = new BasicMarkerFactory();
        }
        catch (Exception ex) {
            Util.report("Unexpected failure while binding MarkerFactory", ex);
        }
    }
    
    public static Marker getDetachedMarker(final String s) {
        return MarkerFactory.markerFactory.getDetachedMarker(s);
    }
    
    public static IMarkerFactory getIMarkerFactory() {
        return MarkerFactory.markerFactory;
    }
    
    public static Marker getMarker(final String s) {
        return MarkerFactory.markerFactory.getMarker(s);
    }
}
