package org.slf4j;

import java.io.*;
import java.util.*;

public interface Marker extends Serializable
{
    public static final String ANY_MARKER = "*";
    public static final String ANY_NON_NULL_MARKER = "+";
    
    void add(final Marker p0);
    
    boolean contains(final String p0);
    
    boolean contains(final Marker p0);
    
    boolean equals(final Object p0);
    
    String getName();
    
    boolean hasChildren();
    
    boolean hasReferences();
    
    int hashCode();
    
    Iterator iterator();
    
    boolean remove(final Marker p0);
}
