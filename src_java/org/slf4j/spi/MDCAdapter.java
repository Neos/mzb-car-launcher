package org.slf4j.spi;

import java.util.*;

public interface MDCAdapter
{
    void clear();
    
    String get(final String p0);
    
    Map getCopyOfContextMap();
    
    void put(final String p0, final String p1);
    
    void remove(final String p0);
    
    void setContextMap(final Map p0);
}
