package org.apache.log4j.net;

import java.util.*;
import java.net.*;
import java.io.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.*;

public class SocketServer
{
    static String CONFIG_FILE_EXT;
    static String GENERIC;
    static Logger cat;
    static Class class$org$apache$log4j$net$SocketServer;
    static int port;
    static SocketServer server;
    File dir;
    LoggerRepository genericHierarchy;
    Hashtable hierarchyMap;
    
    static {
        SocketServer.GENERIC = "generic";
        SocketServer.CONFIG_FILE_EXT = ".lcf";
        Class class$org$apache$log4j$net$SocketServer;
        if (SocketServer.class$org$apache$log4j$net$SocketServer == null) {
            class$org$apache$log4j$net$SocketServer = (SocketServer.class$org$apache$log4j$net$SocketServer = class$("org.apache.log4j.net.SocketServer"));
        }
        else {
            class$org$apache$log4j$net$SocketServer = SocketServer.class$org$apache$log4j$net$SocketServer;
        }
        SocketServer.cat = Logger.getLogger(class$org$apache$log4j$net$SocketServer);
    }
    
    public SocketServer(final File dir) {
        this.dir = dir;
        this.hierarchyMap = new Hashtable(11);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    static void init(String s, final String s2, final String s3) {
        while (true) {
            try {
                SocketServer.port = Integer.parseInt(s);
                PropertyConfigurator.configure(s2);
                s = (String)new File(s3);
                if (!((File)s).isDirectory()) {
                    usage(new StringBuffer().append("[").append(s3).append("] is not a directory.").toString());
                }
                SocketServer.server = new SocketServer((File)s);
            }
            catch (NumberFormatException ex) {
                ex.printStackTrace();
                usage(new StringBuffer().append("Could not interpret port number [").append(s).append("].").toString());
                continue;
            }
            break;
        }
    }
    
    public static void main(final String[] array) {
        while (true) {
            Label_0167: {
                if (array.length != 3) {
                    break Label_0167;
                }
                init(array[0], array[1], array[2]);
                try {
                    SocketServer.cat.info(new StringBuffer().append("Listening on port ").append(SocketServer.port).toString());
                    final ServerSocket serverSocket = new ServerSocket(SocketServer.port);
                    while (true) {
                        SocketServer.cat.info("Waiting to accept a new client.");
                        final Socket accept = serverSocket.accept();
                        final InetAddress inetAddress = accept.getInetAddress();
                        SocketServer.cat.info(new StringBuffer().append("Connected to client at ").append(inetAddress).toString());
                        LoggerRepository configureHierarchy;
                        if ((configureHierarchy = SocketServer.server.hierarchyMap.get(inetAddress)) == null) {
                            configureHierarchy = SocketServer.server.configureHierarchy(inetAddress);
                        }
                        SocketServer.cat.info("Starting new socket node.");
                        new Thread(new SocketNode(accept, configureHierarchy)).start();
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    return;
                }
            }
            usage("Wrong number of arguments.");
            continue;
        }
    }
    
    static void usage(final String s) {
        System.err.println(s);
        final PrintStream err = System.err;
        final StringBuffer append = new StringBuffer().append("Usage: java ");
        Class class$org$apache$log4j$net$SocketServer;
        if (SocketServer.class$org$apache$log4j$net$SocketServer == null) {
            class$org$apache$log4j$net$SocketServer = (SocketServer.class$org$apache$log4j$net$SocketServer = class$("org.apache.log4j.net.SocketServer"));
        }
        else {
            class$org$apache$log4j$net$SocketServer = SocketServer.class$org$apache$log4j$net$SocketServer;
        }
        err.println(append.append(class$org$apache$log4j$net$SocketServer.getName()).append(" port configFile directory").toString());
        System.exit(1);
    }
    
    LoggerRepository configureHierarchy(final InetAddress inetAddress) {
        SocketServer.cat.info(new StringBuffer().append("Locating configuration file for ").append(inetAddress).toString());
        final String string = inetAddress.toString();
        final int index = string.indexOf("/");
        if (index == -1) {
            SocketServer.cat.warn(new StringBuffer().append("Could not parse the inetAddress [").append(inetAddress).append("]. Using default hierarchy.").toString());
            return this.genericHierarchy();
        }
        final File file = new File(this.dir, new StringBuffer().append(string.substring(0, index)).append(SocketServer.CONFIG_FILE_EXT).toString());
        if (file.exists()) {
            final Hierarchy hierarchy = new Hierarchy(new RootLogger(Level.DEBUG));
            this.hierarchyMap.put(inetAddress, hierarchy);
            new PropertyConfigurator().doConfigure(file.getAbsolutePath(), hierarchy);
            return hierarchy;
        }
        SocketServer.cat.warn(new StringBuffer().append("Could not find config file [").append(file).append("].").toString());
        return this.genericHierarchy();
    }
    
    LoggerRepository genericHierarchy() {
        if (this.genericHierarchy == null) {
            final File file = new File(this.dir, new StringBuffer().append(SocketServer.GENERIC).append(SocketServer.CONFIG_FILE_EXT).toString());
            if (file.exists()) {
                this.genericHierarchy = new Hierarchy(new RootLogger(Level.DEBUG));
                new PropertyConfigurator().doConfigure(file.getAbsolutePath(), this.genericHierarchy);
            }
            else {
                SocketServer.cat.warn(new StringBuffer().append("Could not find config file [").append(file).append("]. Will use the default hierarchy.").toString());
                this.genericHierarchy = LogManager.getLoggerRepository();
            }
        }
        return this.genericHierarchy;
    }
}
