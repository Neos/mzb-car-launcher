package org.apache.log4j.net;

import org.apache.log4j.helpers.*;
import java.util.*;
import java.lang.reflect.*;

public class ZeroConfSupport
{
    static Class class$java$lang$String;
    static Class class$java$util$Hashtable;
    static Class class$java$util$Map;
    private static Object jmDNS;
    private static Class jmDNSClass;
    private static Class serviceInfoClass;
    Object serviceInfo;
    
    static {
        ZeroConfSupport.jmDNS = initializeJMDNS();
    }
    
    public ZeroConfSupport(final String s, final int n, final String s2) {
        this(s, n, s2, new HashMap());
    }
    
    public ZeroConfSupport(final String s, final int n, final String s2, final Map map) {
        boolean b = false;
        while (true) {
            try {
                ZeroConfSupport.jmDNSClass.getMethod("create", (Class[])null);
                b = true;
                if (b) {
                    LogLog.debug("using JmDNS version 3 to construct serviceInfo instance");
                    this.serviceInfo = this.buildServiceInfoVersion3(s, n, s2, map);
                    return;
                }
                LogLog.debug("using JmDNS version 1.0 to construct serviceInfo instance");
                this.serviceInfo = this.buildServiceInfoVersion1(s, n, s2, map);
            }
            catch (NoSuchMethodException ex) {
                continue;
            }
            break;
        }
    }
    
    private Object buildServiceInfoVersion1(final String s, final int n, final String s2, final Map map) {
        final Hashtable hashtable = new Hashtable(map);
        try {
            Class class$java$lang$String;
            if (ZeroConfSupport.class$java$lang$String == null) {
                class$java$lang$String = (ZeroConfSupport.class$java$lang$String = class$("java.lang.String"));
            }
            else {
                class$java$lang$String = ZeroConfSupport.class$java$lang$String;
            }
            Class class$java$lang$String2;
            if (ZeroConfSupport.class$java$lang$String == null) {
                class$java$lang$String2 = (ZeroConfSupport.class$java$lang$String = class$("java.lang.String"));
            }
            else {
                class$java$lang$String2 = ZeroConfSupport.class$java$lang$String;
            }
            final Class<Integer> type = Integer.TYPE;
            final Class<Integer> type2 = Integer.TYPE;
            final Class<Integer> type3 = Integer.TYPE;
            Class class$java$util$Hashtable;
            if (ZeroConfSupport.class$java$util$Hashtable == null) {
                class$java$util$Hashtable = (ZeroConfSupport.class$java$util$Hashtable = class$("java.util.Hashtable"));
            }
            else {
                class$java$util$Hashtable = ZeroConfSupport.class$java$util$Hashtable;
            }
            final Object instance = ZeroConfSupport.serviceInfoClass.getConstructor(class$java$lang$String, class$java$lang$String2, type, type2, type3, class$java$util$Hashtable).newInstance(s, s2, new Integer(n), new Integer(0), new Integer(0), hashtable);
            LogLog.debug(new StringBuffer().append("created serviceinfo: ").append(instance).toString());
            return instance;
        }
        catch (IllegalAccessException ex) {
            LogLog.warn("Unable to construct ServiceInfo instance", ex);
        }
        catch (NoSuchMethodException ex2) {
            LogLog.warn("Unable to get ServiceInfo constructor", ex2);
            goto Label_0231;
        }
        catch (InstantiationException ex3) {
            LogLog.warn("Unable to construct ServiceInfo instance", ex3);
            goto Label_0231;
        }
        catch (InvocationTargetException ex4) {
            LogLog.warn("Unable to construct ServiceInfo instance", ex4);
            goto Label_0231;
        }
    }
    
    private Object buildServiceInfoVersion3(final String s, final int n, final String s2, final Map map) {
        try {
            Class class$java$lang$String;
            if (ZeroConfSupport.class$java$lang$String == null) {
                class$java$lang$String = (ZeroConfSupport.class$java$lang$String = class$("java.lang.String"));
            }
            else {
                class$java$lang$String = ZeroConfSupport.class$java$lang$String;
            }
            Class class$java$lang$String2;
            if (ZeroConfSupport.class$java$lang$String == null) {
                class$java$lang$String2 = (ZeroConfSupport.class$java$lang$String = class$("java.lang.String"));
            }
            else {
                class$java$lang$String2 = ZeroConfSupport.class$java$lang$String;
            }
            final Class<Integer> type = Integer.TYPE;
            final Class<Integer> type2 = Integer.TYPE;
            final Class<Integer> type3 = Integer.TYPE;
            Class class$java$util$Map;
            if (ZeroConfSupport.class$java$util$Map == null) {
                class$java$util$Map = (ZeroConfSupport.class$java$util$Map = class$("java.util.Map"));
            }
            else {
                class$java$util$Map = ZeroConfSupport.class$java$util$Map;
            }
            final Object invoke = ZeroConfSupport.serviceInfoClass.getMethod("create", class$java$lang$String, class$java$lang$String2, type, type2, type3, class$java$util$Map).invoke(null, s, s2, new Integer(n), new Integer(0), new Integer(0), map);
            LogLog.debug(new StringBuffer().append("created serviceinfo: ").append(invoke).toString());
            return invoke;
        }
        catch (IllegalAccessException ex) {
            LogLog.warn("Unable to invoke create method", ex);
        }
        catch (NoSuchMethodException ex2) {
            LogLog.warn("Unable to find create method", ex2);
            goto Label_0223;
        }
        catch (InvocationTargetException ex3) {
            LogLog.warn("Unable to invoke create method", ex3);
            goto Label_0223;
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private static Object createJmDNSVersion1() {
        try {
            return ZeroConfSupport.jmDNSClass.newInstance();
        }
        catch (InstantiationException ex) {
            LogLog.warn("Unable to instantiate JMDNS", ex);
        }
        catch (IllegalAccessException ex2) {
            LogLog.warn("Unable to instantiate JMDNS", ex2);
            goto Label_0016;
        }
    }
    
    private static Object createJmDNSVersion3() {
        try {
            return ZeroConfSupport.jmDNSClass.getMethod("create", (Class[])null).invoke(null, (Object[])null);
        }
        catch (IllegalAccessException ex) {
            LogLog.warn("Unable to instantiate jmdns class", ex);
            return null;
        }
        catch (NoSuchMethodException ex2) {
            LogLog.warn("Unable to access constructor", ex2);
            return null;
        }
        catch (InvocationTargetException ex3) {
            LogLog.warn("Unable to call constructor", ex3);
            return null;
        }
    }
    
    public static Object getJMDNSInstance() {
        return ZeroConfSupport.jmDNS;
    }
    
    private static Object initializeJMDNS() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //     5: putstatic       org/apache/log4j/net/ZeroConfSupport.jmDNSClass:Ljava/lang/Class;
        //     8: ldc             "javax.jmdns.ServiceInfo"
        //    10: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    13: putstatic       org/apache/log4j/net/ZeroConfSupport.serviceInfoClass:Ljava/lang/Class;
        //    16: iconst_0       
        //    17: istore_0       
        //    18: getstatic       org/apache/log4j/net/ZeroConfSupport.jmDNSClass:Ljava/lang/Class;
        //    21: ldc             "create"
        //    23: aconst_null    
        //    24: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    27: pop            
        //    28: iconst_1       
        //    29: istore_0       
        //    30: iload_0        
        //    31: ifeq            48
        //    34: invokestatic    org/apache/log4j/net/ZeroConfSupport.createJmDNSVersion3:()Ljava/lang/Object;
        //    37: areturn        
        //    38: astore_1       
        //    39: ldc             "JmDNS or serviceInfo class not found"
        //    41: aload_1        
        //    42: invokestatic    org/apache/log4j/helpers/LogLog.warn:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    45: goto            16
        //    48: invokestatic    org/apache/log4j/net/ZeroConfSupport.createJmDNSVersion1:()Ljava/lang/Object;
        //    51: areturn        
        //    52: astore_1       
        //    53: goto            30
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  0      16     38     48     Ljava/lang/ClassNotFoundException;
        //  18     28     52     56     Ljava/lang/NoSuchMethodException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0030:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void advertise() {
        try {
            ZeroConfSupport.jmDNSClass.getMethod("registerService", ZeroConfSupport.serviceInfoClass).invoke(ZeroConfSupport.jmDNS, this.serviceInfo);
            LogLog.debug(new StringBuffer().append("registered serviceInfo: ").append(this.serviceInfo).toString());
        }
        catch (IllegalAccessException ex) {
            LogLog.warn("Unable to invoke registerService method", ex);
        }
        catch (NoSuchMethodException ex2) {
            LogLog.warn("No registerService method", ex2);
        }
        catch (InvocationTargetException ex3) {
            LogLog.warn("Unable to invoke registerService method", ex3);
        }
    }
    
    public void unadvertise() {
        try {
            ZeroConfSupport.jmDNSClass.getMethod("unregisterService", ZeroConfSupport.serviceInfoClass).invoke(ZeroConfSupport.jmDNS, this.serviceInfo);
            LogLog.debug(new StringBuffer().append("unregistered serviceInfo: ").append(this.serviceInfo).toString());
        }
        catch (IllegalAccessException ex) {
            LogLog.warn("Unable to invoke unregisterService method", ex);
        }
        catch (NoSuchMethodException ex2) {
            LogLog.warn("No unregisterService method", ex2);
        }
        catch (InvocationTargetException ex3) {
            LogLog.warn("Unable to invoke unregisterService method", ex3);
        }
    }
}
