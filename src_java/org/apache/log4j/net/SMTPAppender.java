package org.apache.log4j.net;

import org.apache.log4j.spi.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import org.apache.log4j.xml.*;
import java.io.*;
import javax.mail.internet.*;
import javax.mail.*;
import org.apache.log4j.helpers.*;
import java.util.*;

public class SMTPAppender extends AppenderSkeleton implements UnrecognizedElementHandler
{
    static Class class$org$apache$log4j$spi$TriggeringEventEvaluator;
    private String bcc;
    private int bufferSize;
    protected CyclicBuffer cb;
    private String cc;
    protected TriggeringEventEvaluator evaluator;
    private String from;
    private boolean locationInfo;
    protected Message msg;
    private String replyTo;
    private boolean sendOnClose;
    private boolean smtpDebug;
    private String smtpHost;
    private String smtpPassword;
    private int smtpPort;
    private String smtpProtocol;
    private String smtpUsername;
    private String subject;
    private String to;
    
    public SMTPAppender() {
        this(new DefaultEvaluator());
    }
    
    public SMTPAppender(final TriggeringEventEvaluator evaluator) {
        this.smtpPort = -1;
        this.smtpDebug = false;
        this.bufferSize = 512;
        this.locationInfo = false;
        this.sendOnClose = false;
        this.cb = new CyclicBuffer(this.bufferSize);
        this.evaluator = evaluator;
    }
    
    static String access$000(final SMTPAppender smtpAppender) {
        return smtpAppender.smtpUsername;
    }
    
    static String access$100(final SMTPAppender smtpAppender) {
        return smtpAppender.smtpPassword;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void activateOptions() {
        this.msg = new MimeMessage(this.createSession());
        while (true) {
            try {
                this.addressMessage(this.msg);
                if (this.subject == null) {
                    break Label_0049;
                }
                try {
                    this.msg.setSubject(MimeUtility.encodeText(this.subject, "UTF-8", null));
                    if (this.evaluator instanceof OptionHandler) {
                        ((OptionHandler)this.evaluator).activateOptions();
                    }
                }
                catch (UnsupportedEncodingException ex) {
                    LogLog.error("Unable to encode SMTP subject", ex);
                }
            }
            catch (MessagingException ex2) {
                LogLog.error("Could not activate SMTPAppender options.", ex2);
                continue;
            }
            break;
        }
    }
    
    protected void addressMessage(final Message message) throws MessagingException {
        if (this.from != null) {
            message.setFrom(this.getAddress(this.from));
        }
        else {
            message.setFrom();
        }
        if (this.replyTo != null && this.replyTo.length() > 0) {
            message.setReplyTo(this.parseAddress(this.replyTo));
        }
        if (this.to != null && this.to.length() > 0) {
            message.setRecipients(Message.RecipientType.TO, this.parseAddress(this.to));
        }
        if (this.cc != null && this.cc.length() > 0) {
            message.setRecipients(Message.RecipientType.CC, this.parseAddress(this.cc));
        }
        if (this.bcc != null && this.bcc.length() > 0) {
            message.setRecipients(Message.RecipientType.BCC, this.parseAddress(this.bcc));
        }
    }
    
    public void append(final LoggingEvent loggingEvent) {
        if (this.checkEntryConditions()) {
            loggingEvent.getThreadName();
            loggingEvent.getNDC();
            loggingEvent.getMDCCopy();
            if (this.locationInfo) {
                loggingEvent.getLocationInformation();
            }
            loggingEvent.getRenderedMessage();
            loggingEvent.getThrowableStrRep();
            this.cb.add(loggingEvent);
            if (this.evaluator.isTriggeringEvent(loggingEvent)) {
                this.sendBuffer();
            }
        }
    }
    
    protected boolean checkEntryConditions() {
        if (this.msg == null) {
            this.errorHandler.error("Message object not configured.");
            return false;
        }
        if (this.evaluator == null) {
            this.errorHandler.error(new StringBuffer().append("No TriggeringEventEvaluator is set for appender [").append(this.name).append("].").toString());
            return false;
        }
        if (this.layout == null) {
            this.errorHandler.error(new StringBuffer().append("No layout set for appender named [").append(this.name).append("].").toString());
            return false;
        }
        return true;
    }
    
    @Override
    public void close() {
        synchronized (this) {
            this.closed = true;
            if (this.sendOnClose && this.cb.length() > 0) {
                this.sendBuffer();
            }
        }
    }
    
    protected Session createSession() {
        while (true) {
            try {
                final Properties properties = new Properties(System.getProperties());
                String string = "mail.smtp";
                if (this.smtpProtocol != null) {
                    ((Hashtable<String, String>)properties).put("mail.transport.protocol", this.smtpProtocol);
                    string = new StringBuffer().append("mail.").append(this.smtpProtocol).toString();
                }
                if (this.smtpHost != null) {
                    ((Hashtable<String, String>)properties).put(new StringBuffer().append(string).append(".host").toString(), this.smtpHost);
                }
                if (this.smtpPort > 0) {
                    ((Hashtable<String, String>)properties).put(new StringBuffer().append(string).append(".port").toString(), String.valueOf(this.smtpPort));
                }
                Authenticator authenticator = null;
                if (this.smtpPassword != null) {
                    authenticator = authenticator;
                    if (this.smtpUsername != null) {
                        ((Hashtable<String, String>)properties).put(new StringBuffer().append(string).append(".auth").toString(), "true");
                        authenticator = new SMTPAppender$1(this);
                    }
                }
                final Session instance = Session.getInstance(properties, authenticator);
                if (this.smtpProtocol != null) {
                    instance.setProtocolForAddress("rfc822", this.smtpProtocol);
                }
                if (this.smtpDebug) {
                    instance.setDebug(this.smtpDebug);
                }
                return instance;
            }
            catch (SecurityException ex) {
                final Properties properties = new Properties();
                continue;
            }
            break;
        }
    }
    
    protected String formatBody() {
        final StringBuffer sb = new StringBuffer();
        final String header = this.layout.getHeader();
        if (header != null) {
            sb.append(header);
        }
        for (int length = this.cb.length(), i = 0; i < length; ++i) {
            final LoggingEvent value = this.cb.get();
            sb.append(this.layout.format(value));
            if (this.layout.ignoresThrowable()) {
                final String[] throwableStrRep = value.getThrowableStrRep();
                if (throwableStrRep != null) {
                    for (int j = 0; j < throwableStrRep.length; ++j) {
                        sb.append(throwableStrRep[j]);
                        sb.append(Layout.LINE_SEP);
                    }
                }
            }
        }
        final String footer = this.layout.getFooter();
        if (footer != null) {
            sb.append(footer);
        }
        return sb.toString();
    }
    
    InternetAddress getAddress(final String s) {
        try {
            return new InternetAddress(s);
        }
        catch (AddressException ex) {
            this.errorHandler.error(new StringBuffer().append("Could not parse address [").append(s).append("].").toString(), ex, 6);
            return null;
        }
    }
    
    public String getBcc() {
        return this.bcc;
    }
    
    public int getBufferSize() {
        return this.bufferSize;
    }
    
    public String getCc() {
        return this.cc;
    }
    
    public final TriggeringEventEvaluator getEvaluator() {
        return this.evaluator;
    }
    
    public String getEvaluatorClass() {
        if (this.evaluator == null) {
            return null;
        }
        return this.evaluator.getClass().getName();
    }
    
    public String getFrom() {
        return this.from;
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    public String getReplyTo() {
        return this.replyTo;
    }
    
    public boolean getSMTPDebug() {
        return this.smtpDebug;
    }
    
    public String getSMTPHost() {
        return this.smtpHost;
    }
    
    public String getSMTPPassword() {
        return this.smtpPassword;
    }
    
    public final int getSMTPPort() {
        return this.smtpPort;
    }
    
    public final String getSMTPProtocol() {
        return this.smtpProtocol;
    }
    
    public String getSMTPUsername() {
        return this.smtpUsername;
    }
    
    public final boolean getSendOnClose() {
        return this.sendOnClose;
    }
    
    public String getSubject() {
        return this.subject;
    }
    
    public String getTo() {
        return this.to;
    }
    
    InternetAddress[] parseAddress(final String s) {
        try {
            return InternetAddress.parse(s, true);
        }
        catch (AddressException ex) {
            this.errorHandler.error(new StringBuffer().append("Could not parse address [").append(s).append("].").toString(), ex, 6);
            return null;
        }
    }
    
    @Override
    public boolean parseUnrecognizedElement(final Element element, final Properties properties) throws Exception {
        if ("triggeringPolicy".equals(element.getNodeName())) {
            Class class$org$apache$log4j$spi$TriggeringEventEvaluator;
            if (SMTPAppender.class$org$apache$log4j$spi$TriggeringEventEvaluator == null) {
                class$org$apache$log4j$spi$TriggeringEventEvaluator = (SMTPAppender.class$org$apache$log4j$spi$TriggeringEventEvaluator = class$("org.apache.log4j.spi.TriggeringEventEvaluator"));
            }
            else {
                class$org$apache$log4j$spi$TriggeringEventEvaluator = SMTPAppender.class$org$apache$log4j$spi$TriggeringEventEvaluator;
            }
            final Object element2 = DOMConfigurator.parseElement(element, properties, class$org$apache$log4j$spi$TriggeringEventEvaluator);
            if (element2 instanceof TriggeringEventEvaluator) {
                this.setEvaluator((TriggeringEventEvaluator)element2);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    protected void sendBuffer() {
    Label_0233_Outer:
        while (true) {
            while (true) {
                int n3 = 0;
                Label_0322: {
                    while (true) {
                    Label_0317:
                        while (true) {
                            int n2;
                            try {
                                final String formatBody = this.formatBody();
                                int n = 1;
                                n2 = 0;
                                if (n2 >= formatBody.length() || n == 0) {
                                    MimeBodyPart mimeBodyPart;
                                    if (n != 0) {
                                        mimeBodyPart = new MimeBodyPart();
                                        mimeBodyPart.setContent(formatBody, this.layout.getContentType());
                                    }
                                    else {
                                        try {
                                            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(MimeUtility.encode(byteArrayOutputStream, "quoted-printable"), "UTF-8");
                                            outputStreamWriter.write(formatBody);
                                            outputStreamWriter.close();
                                            final InternetHeaders internetHeaders = new InternetHeaders();
                                            internetHeaders.setHeader("Content-Type", new StringBuffer().append(this.layout.getContentType()).append("; charset=UTF-8").toString());
                                            internetHeaders.setHeader("Content-Transfer-Encoding", "quoted-printable");
                                            mimeBodyPart = new MimeBodyPart(internetHeaders, byteArrayOutputStream.toByteArray());
                                        }
                                        catch (Exception ex3) {
                                            final StringBuffer sb = new StringBuffer(formatBody);
                                            n3 = 0;
                                            if (n3 < sb.length()) {
                                                if (sb.charAt(n3) >= '\u0080') {
                                                    sb.setCharAt(n3, '?');
                                                }
                                                break Label_0322;
                                            }
                                            else {
                                                mimeBodyPart = new MimeBodyPart();
                                                mimeBodyPart.setContent(sb.toString(), this.layout.getContentType());
                                            }
                                        }
                                    }
                                    final MimeMultipart content = new MimeMultipart();
                                    content.addBodyPart(mimeBodyPart);
                                    this.msg.setContent(content);
                                    this.msg.setSentDate(new Date());
                                    Transport.send(this.msg);
                                    return;
                                }
                                if (formatBody.charAt(n2) > '\u007f') {
                                    break Label_0317;
                                }
                                n = 1;
                            }
                            catch (MessagingException ex) {
                                LogLog.error("Error occured while sending e-mail notification.", ex);
                                return;
                            }
                            catch (RuntimeException ex2) {
                                LogLog.error("Error occured while sending e-mail notification.", ex2);
                                return;
                            }
                            ++n2;
                            continue Label_0233_Outer;
                        }
                        int n = 0;
                        continue;
                    }
                }
                ++n3;
                continue;
            }
        }
    }
    
    public void setBcc(final String bcc) {
        this.bcc = bcc;
    }
    
    public void setBufferSize(final int bufferSize) {
        this.bufferSize = bufferSize;
        this.cb.resize(bufferSize);
    }
    
    public void setCc(final String cc) {
        this.cc = cc;
    }
    
    public final void setEvaluator(final TriggeringEventEvaluator evaluator) {
        if (evaluator == null) {
            throw new NullPointerException("trigger");
        }
        this.evaluator = evaluator;
    }
    
    public void setEvaluatorClass(final String s) {
        Class class$org$apache$log4j$spi$TriggeringEventEvaluator;
        if (SMTPAppender.class$org$apache$log4j$spi$TriggeringEventEvaluator == null) {
            class$org$apache$log4j$spi$TriggeringEventEvaluator = (SMTPAppender.class$org$apache$log4j$spi$TriggeringEventEvaluator = class$("org.apache.log4j.spi.TriggeringEventEvaluator"));
        }
        else {
            class$org$apache$log4j$spi$TriggeringEventEvaluator = SMTPAppender.class$org$apache$log4j$spi$TriggeringEventEvaluator;
        }
        this.evaluator = (TriggeringEventEvaluator)OptionConverter.instantiateByClassName(s, class$org$apache$log4j$spi$TriggeringEventEvaluator, this.evaluator);
    }
    
    public void setFrom(final String from) {
        this.from = from;
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setReplyTo(final String replyTo) {
        this.replyTo = replyTo;
    }
    
    public void setSMTPDebug(final boolean smtpDebug) {
        this.smtpDebug = smtpDebug;
    }
    
    public void setSMTPHost(final String smtpHost) {
        this.smtpHost = smtpHost;
    }
    
    public void setSMTPPassword(final String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }
    
    public final void setSMTPPort(final int smtpPort) {
        this.smtpPort = smtpPort;
    }
    
    public final void setSMTPProtocol(final String smtpProtocol) {
        this.smtpProtocol = smtpProtocol;
    }
    
    public void setSMTPUsername(final String smtpUsername) {
        this.smtpUsername = smtpUsername;
    }
    
    public final void setSendOnClose(final boolean sendOnClose) {
        this.sendOnClose = sendOnClose;
    }
    
    public void setSubject(final String subject) {
        this.subject = subject;
    }
    
    public void setTo(final String to) {
        this.to = to;
    }
}
