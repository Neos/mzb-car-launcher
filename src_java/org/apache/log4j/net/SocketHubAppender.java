package org.apache.log4j.net;

import org.apache.log4j.*;
import java.util.*;
import java.net.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;
import java.io.*;

public class SocketHubAppender extends AppenderSkeleton
{
    static final int DEFAULT_PORT = 4560;
    public static final String ZONE = "_log4j_obj_tcpaccept_appender.local.";
    private boolean advertiseViaMulticastDNS;
    private String application;
    private CyclicBuffer buffer;
    private boolean locationInfo;
    private Vector oosList;
    private int port;
    private ServerMonitor serverMonitor;
    private ServerSocket serverSocket;
    private ZeroConfSupport zeroConf;
    
    public SocketHubAppender() {
        this.port = 4560;
        this.oosList = new Vector();
        this.serverMonitor = null;
        this.locationInfo = false;
        this.buffer = null;
    }
    
    public SocketHubAppender(final int port) {
        this.port = 4560;
        this.oosList = new Vector();
        this.serverMonitor = null;
        this.locationInfo = false;
        this.buffer = null;
        this.port = port;
        this.startServer();
    }
    
    static ServerSocket access$000(final SocketHubAppender socketHubAppender) {
        return socketHubAppender.serverSocket;
    }
    
    static ServerSocket access$002(final SocketHubAppender socketHubAppender, final ServerSocket serverSocket) {
        return socketHubAppender.serverSocket = serverSocket;
    }
    
    static CyclicBuffer access$100(final SocketHubAppender socketHubAppender) {
        return socketHubAppender.buffer;
    }
    
    private void startServer() {
        this.serverMonitor = new ServerMonitor(this.port, this.oosList);
    }
    
    @Override
    public void activateOptions() {
        if (this.advertiseViaMulticastDNS) {
            (this.zeroConf = new ZeroConfSupport("_log4j_obj_tcpaccept_appender.local.", this.port, this.getName())).advertise();
        }
        this.startServer();
    }
    
    public void append(final LoggingEvent loggingEvent) {
        if (loggingEvent != null) {
            if (this.locationInfo) {
                loggingEvent.getLocationInformation();
            }
            if (this.application != null) {
                loggingEvent.setProperty("application", this.application);
            }
            loggingEvent.getNDC();
            loggingEvent.getThreadName();
            loggingEvent.getMDCCopy();
            loggingEvent.getRenderedMessage();
            loggingEvent.getThrowableStrRep();
            if (this.buffer != null) {
                this.buffer.add(loggingEvent);
            }
        }
        if (loggingEvent != null && this.oosList.size() != 0) {
            int i = 0;
            while (i < this.oosList.size()) {
                ObjectOutputStream objectOutputStream = null;
                while (true) {
                    try {
                        objectOutputStream = this.oosList.elementAt(i);
                        if (objectOutputStream == null) {
                            break;
                        }
                        try {
                            objectOutputStream.writeObject(loggingEvent);
                            objectOutputStream.flush();
                            objectOutputStream.reset();
                            ++i;
                        }
                        catch (IOException objectOutputStream) {
                            if (objectOutputStream instanceof InterruptedIOException) {
                                Thread.currentThread().interrupt();
                            }
                            this.oosList.removeElementAt(i);
                            LogLog.debug("dropped connection");
                            --i;
                        }
                    }
                    catch (ArrayIndexOutOfBoundsException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    public void cleanUp() {
        LogLog.debug("stopping ServerSocket");
        this.serverMonitor.stopMonitor();
        this.serverMonitor = null;
        LogLog.debug("closing client connections");
    Label_0052_Outer:
        while (this.oosList.size() != 0) {
            final ObjectOutputStream objectOutputStream = this.oosList.elementAt(0);
            if (objectOutputStream != null) {
                while (true) {
                    try {
                        objectOutputStream.close();
                        this.oosList.removeElementAt(0);
                        continue Label_0052_Outer;
                    }
                    catch (InterruptedIOException ex) {
                        Thread.currentThread().interrupt();
                        LogLog.error("could not close oos.", ex);
                        continue;
                    }
                    catch (IOException ex2) {
                        LogLog.error("could not close oos.", ex2);
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this) {
            if (!this.closed) {
                LogLog.debug(new StringBuffer().append("closing SocketHubAppender ").append(this.getName()).toString());
                this.closed = true;
                if (this.advertiseViaMulticastDNS) {
                    this.zeroConf.unadvertise();
                }
                this.cleanUp();
                LogLog.debug(new StringBuffer().append("SocketHubAppender ").append(this.getName()).append(" closed").toString());
            }
        }
    }
    
    protected ServerSocket createServerSocket(final int n) throws IOException {
        return new ServerSocket(n);
    }
    
    public String getApplication() {
        return this.application;
    }
    
    public int getBufferSize() {
        if (this.buffer == null) {
            return 0;
        }
        return this.buffer.getMaxSize();
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public boolean isAdvertiseViaMulticastDNS() {
        return this.advertiseViaMulticastDNS;
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
    
    public void setAdvertiseViaMulticastDNS(final boolean advertiseViaMulticastDNS) {
        this.advertiseViaMulticastDNS = advertiseViaMulticastDNS;
    }
    
    public void setApplication(final String application) {
        this.application = application;
    }
    
    public void setBufferSize(final int n) {
        this.buffer = new CyclicBuffer(n);
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setPort(final int port) {
        this.port = port;
    }
    
    private class ServerMonitor implements Runnable
    {
        private boolean keepRunning;
        private Thread monitorThread;
        private Vector oosList;
        private int port;
        private final SocketHubAppender this$0;
        
        public ServerMonitor(final SocketHubAppender this$0, final int port, final Vector oosList) {
            this.this$0 = this$0;
            this.port = port;
            this.oosList = oosList;
            this.keepRunning = true;
            (this.monitorThread = new Thread(this)).setDaemon(true);
            this.monitorThread.setName(new StringBuffer().append("SocketHubAppender-Monitor-").append(this.port).toString());
            this.monitorThread.start();
        }
        
        private void sendCachedEvents(final ObjectOutputStream objectOutputStream) throws IOException {
            if (SocketHubAppender.access$100(this.this$0) != null) {
                for (int i = 0; i < SocketHubAppender.access$100(this.this$0).length(); ++i) {
                    objectOutputStream.writeObject(SocketHubAppender.access$100(this.this$0).get(i));
                }
                objectOutputStream.flush();
                objectOutputStream.reset();
            }
        }
        
        @Override
        public void run() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //     4: aconst_null    
            //     5: invokestatic    org/apache/log4j/net/SocketHubAppender.access$002:(Lorg/apache/log4j/net/SocketHubAppender;Ljava/net/ServerSocket;)Ljava/net/ServerSocket;
            //     8: pop            
            //     9: aload_0        
            //    10: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    13: aload_0        
            //    14: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    17: aload_0        
            //    18: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.port:I
            //    21: invokevirtual   org/apache/log4j/net/SocketHubAppender.createServerSocket:(I)Ljava/net/ServerSocket;
            //    24: invokestatic    org/apache/log4j/net/SocketHubAppender.access$002:(Lorg/apache/log4j/net/SocketHubAppender;Ljava/net/ServerSocket;)Ljava/net/ServerSocket;
            //    27: pop            
            //    28: aload_0        
            //    29: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    32: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //    35: sipush          1000
            //    38: invokevirtual   java/net/ServerSocket.setSoTimeout:(I)V
            //    41: aload_0        
            //    42: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    45: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //    48: sipush          1000
            //    51: invokevirtual   java/net/ServerSocket.setSoTimeout:(I)V
            //    54: aload_0        
            //    55: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.keepRunning:Z
            //    58: istore_1       
            //    59: iload_1        
            //    60: ifeq            300
            //    63: aconst_null    
            //    64: astore_2       
            //    65: aload_0        
            //    66: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    69: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //    72: invokevirtual   java/net/ServerSocket.accept:()Ljava/net/Socket;
            //    75: astore_3       
            //    76: aload_3        
            //    77: astore_2       
            //    78: aload_2        
            //    79: ifnull          54
            //    82: aload_2        
            //    83: invokevirtual   java/net/Socket.getInetAddress:()Ljava/net/InetAddress;
            //    86: astore_3       
            //    87: new             Ljava/lang/StringBuffer;
            //    90: dup            
            //    91: invokespecial   java/lang/StringBuffer.<init>:()V
            //    94: ldc             "accepting connection from "
            //    96: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
            //    99: aload_3        
            //   100: invokevirtual   java/net/InetAddress.getHostName:()Ljava/lang/String;
            //   103: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
            //   106: ldc             " ("
            //   108: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
            //   111: aload_3        
            //   112: invokevirtual   java/net/InetAddress.getHostAddress:()Ljava/lang/String;
            //   115: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
            //   118: ldc             ")"
            //   120: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
            //   123: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
            //   126: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
            //   129: new             Ljava/io/ObjectOutputStream;
            //   132: dup            
            //   133: aload_2        
            //   134: invokevirtual   java/net/Socket.getOutputStream:()Ljava/io/OutputStream;
            //   137: invokespecial   java/io/ObjectOutputStream.<init>:(Ljava/io/OutputStream;)V
            //   140: astore_2       
            //   141: aload_0        
            //   142: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //   145: invokestatic    org/apache/log4j/net/SocketHubAppender.access$100:(Lorg/apache/log4j/net/SocketHubAppender;)Lorg/apache/log4j/helpers/CyclicBuffer;
            //   148: ifnull          169
            //   151: aload_0        
            //   152: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //   155: invokestatic    org/apache/log4j/net/SocketHubAppender.access$100:(Lorg/apache/log4j/net/SocketHubAppender;)Lorg/apache/log4j/helpers/CyclicBuffer;
            //   158: invokevirtual   org/apache/log4j/helpers/CyclicBuffer.length:()I
            //   161: ifle            169
            //   164: aload_0        
            //   165: aload_2        
            //   166: invokespecial   org/apache/log4j/net/SocketHubAppender$ServerMonitor.sendCachedEvents:(Ljava/io/ObjectOutputStream;)V
            //   169: aload_0        
            //   170: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.oosList:Ljava/util/Vector;
            //   173: aload_2        
            //   174: invokevirtual   java/util/Vector.addElement:(Ljava/lang/Object;)V
            //   177: goto            54
            //   180: astore_2       
            //   181: aload_2        
            //   182: instanceof      Ljava/io/InterruptedIOException;
            //   185: ifeq            194
            //   188: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //   191: invokevirtual   java/lang/Thread.interrupt:()V
            //   194: ldc             "exception creating output stream on socket."
            //   196: aload_2        
            //   197: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   200: goto            54
            //   203: astore_2       
            //   204: aload_0        
            //   205: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //   208: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //   211: invokevirtual   java/net/ServerSocket.close:()V
            //   214: aload_2        
            //   215: athrow         
            //   216: astore_2       
            //   217: aload_2        
            //   218: instanceof      Ljava/io/InterruptedIOException;
            //   221: ifne            231
            //   224: aload_2        
            //   225: instanceof      Ljava/lang/InterruptedException;
            //   228: ifeq            237
            //   231: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //   234: invokevirtual   java/lang/Thread.interrupt:()V
            //   237: ldc             "exception setting timeout, shutting down server socket."
            //   239: aload_2        
            //   240: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   243: aload_0        
            //   244: iconst_0       
            //   245: putfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.keepRunning:Z
            //   248: return         
            //   249: astore_2       
            //   250: ldc             "exception setting timeout, shutting down server socket."
            //   252: aload_2        
            //   253: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   256: aload_0        
            //   257: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //   260: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //   263: invokevirtual   java/net/ServerSocket.close:()V
            //   266: return         
            //   267: astore_2       
            //   268: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //   271: invokevirtual   java/lang/Thread.interrupt:()V
            //   274: return         
            //   275: astore_3       
            //   276: ldc             "exception accepting socket, shutting down server socket."
            //   278: aload_3        
            //   279: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   282: aload_0        
            //   283: iconst_0       
            //   284: putfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.keepRunning:Z
            //   287: goto            78
            //   290: astore_3       
            //   291: ldc             "exception accepting socket."
            //   293: aload_3        
            //   294: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   297: goto            78
            //   300: aload_0        
            //   301: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //   304: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //   307: invokevirtual   java/net/ServerSocket.close:()V
            //   310: return         
            //   311: astore_2       
            //   312: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //   315: invokevirtual   java/lang/Thread.interrupt:()V
            //   318: return         
            //   319: astore_3       
            //   320: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //   323: invokevirtual   java/lang/Thread.interrupt:()V
            //   326: goto            214
            //   329: astore_3       
            //   330: goto            214
            //   333: astore_2       
            //   334: return         
            //   335: astore_3       
            //   336: goto            78
            //   339: astore_2       
            //   340: return         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                            
            //  -----  -----  -----  -----  --------------------------------
            //  9      41     216    249    Ljava/lang/Exception;
            //  41     54     249    275    Ljava/net/SocketException;
            //  41     54     203    333    Any
            //  54     59     203    333    Any
            //  65     76     335    339    Ljava/io/InterruptedIOException;
            //  65     76     275    290    Ljava/net/SocketException;
            //  65     76     290    300    Ljava/io/IOException;
            //  65     76     203    333    Any
            //  82     169    180    203    Ljava/io/IOException;
            //  82     169    203    333    Any
            //  169    177    180    203    Ljava/io/IOException;
            //  169    177    203    333    Any
            //  181    194    203    333    Any
            //  194    200    203    333    Any
            //  204    214    319    329    Ljava/io/InterruptedIOException;
            //  204    214    329    333    Ljava/io/IOException;
            //  250    256    203    333    Any
            //  256    266    267    275    Ljava/io/InterruptedIOException;
            //  256    266    339    341    Ljava/io/IOException;
            //  276    287    203    333    Any
            //  291    297    203    333    Any
            //  300    310    311    319    Ljava/io/InterruptedIOException;
            //  300    310    333    335    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index: 163, Size: 163
            //     at java.util.ArrayList.rangeCheck(Unknown Source)
            //     at java.util.ArrayList.get(Unknown Source)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3569)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public void stopMonitor() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: monitorenter   
            //     2: aload_0        
            //     3: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.keepRunning:Z
            //     6: ifeq            65
            //     9: ldc             "server monitor thread shutting down"
            //    11: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
            //    14: aload_0        
            //    15: iconst_0       
            //    16: putfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.keepRunning:Z
            //    19: aload_0        
            //    20: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    23: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //    26: ifnull          48
            //    29: aload_0        
            //    30: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    33: invokestatic    org/apache/log4j/net/SocketHubAppender.access$000:(Lorg/apache/log4j/net/SocketHubAppender;)Ljava/net/ServerSocket;
            //    36: invokevirtual   java/net/ServerSocket.close:()V
            //    39: aload_0        
            //    40: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.this$0:Lorg/apache/log4j/net/SocketHubAppender;
            //    43: aconst_null    
            //    44: invokestatic    org/apache/log4j/net/SocketHubAppender.access$002:(Lorg/apache/log4j/net/SocketHubAppender;Ljava/net/ServerSocket;)Ljava/net/ServerSocket;
            //    47: pop            
            //    48: aload_0        
            //    49: getfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.monitorThread:Ljava/lang/Thread;
            //    52: invokevirtual   java/lang/Thread.join:()V
            //    55: aload_0        
            //    56: aconst_null    
            //    57: putfield        org/apache/log4j/net/SocketHubAppender$ServerMonitor.monitorThread:Ljava/lang/Thread;
            //    60: ldc             "server monitor thread shut down"
            //    62: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
            //    65: aload_0        
            //    66: monitorexit    
            //    67: return         
            //    68: astore_1       
            //    69: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //    72: invokevirtual   java/lang/Thread.interrupt:()V
            //    75: goto            55
            //    78: astore_1       
            //    79: aload_0        
            //    80: monitorexit    
            //    81: aload_1        
            //    82: athrow         
            //    83: astore_1       
            //    84: goto            48
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                            
            //  -----  -----  -----  -----  --------------------------------
            //  2      19     78     83     Any
            //  19     48     83     87     Ljava/io/IOException;
            //  19     48     78     83     Any
            //  48     55     68     78     Ljava/lang/InterruptedException;
            //  48     55     78     83     Any
            //  55     65     78     83     Any
            //  69     75     78     83     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0048:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}
