package org.apache.log4j.net;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import java.io.*;
import java.net.*;

public class SocketAppender extends AppenderSkeleton
{
    public static final int DEFAULT_PORT = 4560;
    static final int DEFAULT_RECONNECTION_DELAY = 30000;
    private static final int RESET_FREQUENCY = 1;
    public static final String ZONE = "_log4j_obj_tcpconnect_appender.local.";
    InetAddress address;
    private boolean advertiseViaMulticastDNS;
    private String application;
    private Connector connector;
    int counter;
    boolean locationInfo;
    ObjectOutputStream oos;
    int port;
    int reconnectionDelay;
    String remoteHost;
    private ZeroConfSupport zeroConf;
    
    public SocketAppender() {
        this.port = 4560;
        this.reconnectionDelay = 30000;
        this.locationInfo = false;
        this.counter = 0;
    }
    
    public SocketAppender(final String remoteHost, final int port) {
        this.port = 4560;
        this.reconnectionDelay = 30000;
        this.locationInfo = false;
        this.counter = 0;
        this.port = port;
        this.address = getAddressByName(remoteHost);
        this.remoteHost = remoteHost;
        this.connect(this.address, port);
    }
    
    public SocketAppender(final InetAddress address, final int port) {
        this.port = 4560;
        this.reconnectionDelay = 30000;
        this.locationInfo = false;
        this.counter = 0;
        this.address = address;
        this.remoteHost = address.getHostName();
        this.connect(address, this.port = port);
    }
    
    static Connector access$002(final SocketAppender socketAppender, final Connector connector) {
        return socketAppender.connector = connector;
    }
    
    static InetAddress getAddressByName(final String s) {
        try {
            return InetAddress.getByName(s);
        }
        catch (Exception ex) {
            if (ex instanceof InterruptedIOException || ex instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            LogLog.error(new StringBuffer().append("Could not find address of [").append(s).append("].").toString(), ex);
            return null;
        }
    }
    
    @Override
    public void activateOptions() {
        if (this.advertiseViaMulticastDNS) {
            (this.zeroConf = new ZeroConfSupport("_log4j_obj_tcpconnect_appender.local.", this.port, this.getName())).advertise();
        }
        this.connect(this.address, this.port);
    }
    
    public void append(final LoggingEvent loggingEvent) {
        if (loggingEvent != null) {
            if (this.address == null) {
                this.errorHandler.error(new StringBuffer().append("No remote host is set for SocketAppender named \"").append(this.name).append("\".").toString());
                return;
            }
            if (this.oos != null) {
                try {
                    if (this.locationInfo) {
                        loggingEvent.getLocationInformation();
                    }
                    if (this.application != null) {
                        loggingEvent.setProperty("application", this.application);
                    }
                    loggingEvent.getNDC();
                    loggingEvent.getThreadName();
                    loggingEvent.getMDCCopy();
                    loggingEvent.getRenderedMessage();
                    loggingEvent.getThrowableStrRep();
                    this.oos.writeObject(loggingEvent);
                    this.oos.flush();
                    if (++this.counter >= 1) {
                        this.counter = 0;
                        this.oos.reset();
                    }
                }
                catch (IOException ex) {
                    if (ex instanceof InterruptedIOException) {
                        Thread.currentThread().interrupt();
                    }
                    this.oos = null;
                    LogLog.warn(new StringBuffer().append("Detected problem with connection: ").append(ex).toString());
                    if (this.reconnectionDelay > 0) {
                        this.fireConnector();
                        return;
                    }
                    this.errorHandler.error("Detected problem with connection, not reconnecting.", ex, 0);
                }
            }
        }
    }
    
    public void cleanUp() {
        Label_0019: {
            if (this.oos == null) {
                break Label_0019;
            }
            while (true) {
                try {
                    this.oos.close();
                    this.oos = null;
                    if (this.connector != null) {
                        this.connector.interrupted = true;
                        this.connector = null;
                    }
                }
                catch (IOException ex) {
                    if (ex instanceof InterruptedIOException) {
                        Thread.currentThread().interrupt();
                    }
                    LogLog.error("Could not close oos.", ex);
                    continue;
                }
                break;
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this) {
            if (!this.closed) {
                this.closed = true;
                if (this.advertiseViaMulticastDNS) {
                    this.zeroConf.unadvertise();
                }
                this.cleanUp();
            }
        }
    }
    
    void connect(final InetAddress inetAddress, final int n) {
        if (this.address == null) {
            return;
        }
        try {
            this.cleanUp();
            this.oos = new ObjectOutputStream(new Socket(inetAddress, n).getOutputStream());
        }
        catch (IOException ex) {
            if (ex instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            final String string = new StringBuffer().append("Could not connect to remote log4j server at [").append(inetAddress.getHostName()).append("].").toString();
            String s;
            if (this.reconnectionDelay > 0) {
                s = new StringBuffer().append(string).append(" We will try again later.").toString();
                this.fireConnector();
            }
            else {
                s = new StringBuffer().append(string).append(" We are not retrying.").toString();
                this.errorHandler.error(s, ex, 0);
            }
            LogLog.error(s);
        }
    }
    
    void fireConnector() {
        if (this.connector == null) {
            LogLog.debug("Starting a new connector thread.");
            (this.connector = new Connector()).setDaemon(true);
            this.connector.setPriority(1);
            this.connector.start();
        }
    }
    
    public String getApplication() {
        return this.application;
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public int getReconnectionDelay() {
        return this.reconnectionDelay;
    }
    
    public String getRemoteHost() {
        return this.remoteHost;
    }
    
    public boolean isAdvertiseViaMulticastDNS() {
        return this.advertiseViaMulticastDNS;
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
    
    public void setAdvertiseViaMulticastDNS(final boolean advertiseViaMulticastDNS) {
        this.advertiseViaMulticastDNS = advertiseViaMulticastDNS;
    }
    
    public void setApplication(final String application) {
        this.application = application;
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setPort(final int port) {
        this.port = port;
    }
    
    public void setReconnectionDelay(final int reconnectionDelay) {
        this.reconnectionDelay = reconnectionDelay;
    }
    
    public void setRemoteHost(final String remoteHost) {
        this.address = getAddressByName(remoteHost);
        this.remoteHost = remoteHost;
    }
    
    class Connector extends Thread
    {
        boolean interrupted;
        private final SocketAppender this$0;
        
        Connector(final SocketAppender this$0) {
            this.this$0 = this$0;
            this.interrupted = false;
        }
        
        @Override
        public void run() {
            while (!this.interrupted) {
                try {
                    Thread.sleep(this.this$0.reconnectionDelay);
                    LogLog.debug(new StringBuffer().append("Attempting connection to ").append(this.this$0.address.getHostName()).toString());
                    final Socket socket = new Socket(this.this$0.address, this.this$0.port);
                    synchronized (this) {
                        this.this$0.oos = new ObjectOutputStream(socket.getOutputStream());
                        SocketAppender.access$002(this.this$0, null);
                        LogLog.debug("Connection established. Exiting connector thread.");
                        return;
                    }
                }
                catch (InterruptedException ex2) {
                    LogLog.debug("Connector interrupted. Leaving loop.");
                    return;
                }
                catch (ConnectException ex3) {
                    LogLog.debug(new StringBuffer().append("Remote host ").append(this.this$0.address.getHostName()).append(" refused connection.").toString());
                    continue;
                }
                catch (IOException ex) {
                    if (ex instanceof InterruptedIOException) {
                        Thread.currentThread().interrupt();
                    }
                    LogLog.debug(new StringBuffer().append("Could not connect to ").append(this.this$0.address.getHostName()).append(". Exception is ").append(ex).toString());
                    continue;
                }
                break;
            }
        }
    }
}
