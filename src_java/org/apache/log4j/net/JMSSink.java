package org.apache.log4j.net;

import javax.naming.*;
import org.apache.log4j.xml.*;
import org.apache.log4j.*;
import java.io.*;
import javax.jms.*;
import org.apache.log4j.spi.*;

public class JMSSink implements MessageListener
{
    static Class class$org$apache$log4j$net$JMSSink;
    static Logger logger;
    
    static {
        Class class$org$apache$log4j$net$JMSSink;
        if (JMSSink.class$org$apache$log4j$net$JMSSink == null) {
            class$org$apache$log4j$net$JMSSink = (JMSSink.class$org$apache$log4j$net$JMSSink = class$("org.apache.log4j.net.JMSSink"));
        }
        else {
            class$org$apache$log4j$net$JMSSink = JMSSink.class$org$apache$log4j$net$JMSSink;
        }
        JMSSink.logger = Logger.getLogger(class$org$apache$log4j$net$JMSSink);
    }
    
    public JMSSink(final String s, final String s2, final String s3, final String s4) {
        try {
            final InitialContext initialContext = new InitialContext();
            final TopicConnection topicConnection = ((TopicConnectionFactory)lookup(initialContext, s)).createTopicConnection(s3, s4);
            topicConnection.start();
            topicConnection.createTopicSession(false, 1).createSubscriber((Topic)initialContext.lookup(s2)).setMessageListener((MessageListener)this);
        }
        catch (JMSException ex) {
            JMSSink.logger.error("Could not read JMS message.", (Throwable)ex);
        }
        catch (NamingException ex2) {
            JMSSink.logger.error("Could not read JMS message.", ex2);
        }
        catch (RuntimeException ex3) {
            JMSSink.logger.error("Could not read JMS message.", ex3);
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    protected static Object lookup(final Context context, final String s) throws NamingException {
        try {
            return context.lookup(s);
        }
        catch (NameNotFoundException ex) {
            JMSSink.logger.error(new StringBuffer().append("Could not find name [").append(s).append("].").toString());
            throw ex;
        }
    }
    
    public static void main(final String[] array) throws Exception {
        if (array.length != 5) {
            usage("Wrong number of arguments.");
        }
        final String s = array[0];
        final String s2 = array[1];
        final String s3 = array[2];
        final String s4 = array[3];
        final String s5 = array[4];
        if (s5.endsWith(".xml")) {
            DOMConfigurator.configure(s5);
        }
        else {
            PropertyConfigurator.configure(s5);
        }
        new JMSSink(s, s2, s3, s4);
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Type \"exit\" to quit JMSSink.");
        while (!bufferedReader.readLine().equalsIgnoreCase("exit")) {}
        System.out.println("Exiting. Kill the application if it does not exit due to daemon threads.");
    }
    
    static void usage(final String s) {
        System.err.println(s);
        final PrintStream err = System.err;
        final StringBuffer append = new StringBuffer().append("Usage: java ");
        Class class$org$apache$log4j$net$JMSSink;
        if (JMSSink.class$org$apache$log4j$net$JMSSink == null) {
            class$org$apache$log4j$net$JMSSink = (JMSSink.class$org$apache$log4j$net$JMSSink = class$("org.apache.log4j.net.JMSSink"));
        }
        else {
            class$org$apache$log4j$net$JMSSink = JMSSink.class$org$apache$log4j$net$JMSSink;
        }
        err.println(append.append(class$org$apache$log4j$net$JMSSink.getName()).append(" TopicConnectionFactoryBindingName TopicBindingName username password configFile").toString());
        System.exit(1);
    }
    
    public void onMessage(final Message message) {
        try {
            if (message instanceof ObjectMessage) {
                final LoggingEvent loggingEvent = (LoggingEvent)((ObjectMessage)message).getObject();
                Logger.getLogger(loggingEvent.getLoggerName()).callAppenders(loggingEvent);
                return;
            }
            JMSSink.logger.warn(new StringBuffer().append("Received message is of type ").append(message.getJMSType()).append(", was expecting ObjectMessage.").toString());
        }
        catch (JMSException ex) {
            JMSSink.logger.error("Exception thrown while processing incoming message.", (Throwable)ex);
        }
    }
}
