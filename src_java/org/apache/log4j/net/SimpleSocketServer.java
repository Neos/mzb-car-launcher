package org.apache.log4j.net;

import org.apache.log4j.xml.*;
import org.apache.log4j.*;
import java.net.*;
import java.io.*;

public class SimpleSocketServer
{
    static Logger cat;
    static Class class$org$apache$log4j$net$SimpleSocketServer;
    static int port;
    
    static {
        Class class$org$apache$log4j$net$SimpleSocketServer;
        if (SimpleSocketServer.class$org$apache$log4j$net$SimpleSocketServer == null) {
            class$org$apache$log4j$net$SimpleSocketServer = (SimpleSocketServer.class$org$apache$log4j$net$SimpleSocketServer = class$("org.apache.log4j.net.SimpleSocketServer"));
        }
        else {
            class$org$apache$log4j$net$SimpleSocketServer = SimpleSocketServer.class$org$apache$log4j$net$SimpleSocketServer;
        }
        SimpleSocketServer.cat = Logger.getLogger(class$org$apache$log4j$net$SimpleSocketServer);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    static void init(final String s, final String s2) {
        while (true) {
            try {
                SimpleSocketServer.port = Integer.parseInt(s);
                if (s2.endsWith(".xml")) {
                    DOMConfigurator.configure(s2);
                    return;
                }
            }
            catch (NumberFormatException ex) {
                ex.printStackTrace();
                usage(new StringBuffer().append("Could not interpret port number [").append(s).append("].").toString());
                continue;
            }
            break;
        }
        PropertyConfigurator.configure(s2);
    }
    
    public static void main(final String[] array) {
        while (true) {
            Label_0153: {
                if (array.length != 2) {
                    break Label_0153;
                }
                init(array[0], array[1]);
                try {
                    SimpleSocketServer.cat.info(new StringBuffer().append("Listening on port ").append(SimpleSocketServer.port).toString());
                    final ServerSocket serverSocket = new ServerSocket(SimpleSocketServer.port);
                    while (true) {
                        SimpleSocketServer.cat.info("Waiting to accept a new client.");
                        final Socket accept = serverSocket.accept();
                        SimpleSocketServer.cat.info(new StringBuffer().append("Connected to client at ").append(accept.getInetAddress()).toString());
                        SimpleSocketServer.cat.info("Starting new socket node.");
                        new Thread(new SocketNode(accept, LogManager.getLoggerRepository()), new StringBuffer().append("SimpleSocketServer-").append(SimpleSocketServer.port).toString()).start();
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    return;
                }
            }
            usage("Wrong number of arguments.");
            continue;
        }
    }
    
    static void usage(final String s) {
        System.err.println(s);
        final PrintStream err = System.err;
        final StringBuffer append = new StringBuffer().append("Usage: java ");
        Class class$org$apache$log4j$net$SimpleSocketServer;
        if (SimpleSocketServer.class$org$apache$log4j$net$SimpleSocketServer == null) {
            class$org$apache$log4j$net$SimpleSocketServer = (SimpleSocketServer.class$org$apache$log4j$net$SimpleSocketServer = class$("org.apache.log4j.net.SimpleSocketServer"));
        }
        else {
            class$org$apache$log4j$net$SimpleSocketServer = SimpleSocketServer.class$org$apache$log4j$net$SimpleSocketServer;
        }
        err.println(append.append(class$org$apache$log4j$net$SimpleSocketServer.getName()).append(" port configFile").toString());
        System.exit(1);
    }
}
