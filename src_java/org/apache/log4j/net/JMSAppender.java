package org.apache.log4j.net;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import java.util.*;
import org.apache.log4j.spi.*;
import java.io.*;
import javax.jms.*;
import javax.naming.*;

public class JMSAppender extends AppenderSkeleton
{
    String initialContextFactoryName;
    boolean locationInfo;
    String password;
    String providerURL;
    String securityCredentials;
    String securityPrincipalName;
    String tcfBindingName;
    String topicBindingName;
    TopicConnection topicConnection;
    TopicPublisher topicPublisher;
    TopicSession topicSession;
    String urlPkgPrefixes;
    String userName;
    
    @Override
    public void activateOptions() {
        try {
            LogLog.debug("Getting initial context.");
            if (this.initialContextFactoryName == null) {
                goto Label_0384;
            }
            final Properties properties = new Properties();
            ((Hashtable<String, String>)properties).put("java.naming.factory.initial", this.initialContextFactoryName);
            if (this.providerURL != null) {
                ((Hashtable<String, String>)properties).put("java.naming.provider.url", this.providerURL);
            }
            else {
                LogLog.warn("You have set InitialContextFactoryName option but not the ProviderURL. This is likely to cause problems.");
            }
            if (this.urlPkgPrefixes != null) {
                ((Hashtable<String, String>)properties).put("java.naming.factory.url.pkgs", this.urlPkgPrefixes);
            }
            if (this.securityPrincipalName != null) {
                ((Hashtable<String, String>)properties).put("java.naming.security.principal", this.securityPrincipalName);
                if (this.securityCredentials == null) {
                    goto Label_0336;
                }
                ((Hashtable<String, String>)properties).put("java.naming.security.credentials", this.securityCredentials);
            }
            final InitialContext initialContext = new InitialContext(properties);
            LogLog.debug(new StringBuffer().append("Looking up [").append(this.tcfBindingName).append("]").toString());
            final TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)this.lookup(initialContext, this.tcfBindingName);
            LogLog.debug("About to create TopicConnection.");
            if (this.userName != null) {
                this.topicConnection = topicConnectionFactory.createTopicConnection(this.userName, this.password);
                LogLog.debug("Creating TopicSession, non-transactional, in AUTO_ACKNOWLEDGE mode.");
                this.topicSession = this.topicConnection.createTopicSession(false, 1);
                LogLog.debug(new StringBuffer().append("Looking up topic name [").append(this.topicBindingName).append("].").toString());
                final Topic topic = (Topic)this.lookup(initialContext, this.topicBindingName);
                LogLog.debug("Creating TopicPublisher.");
                this.topicPublisher = this.topicSession.createPublisher(topic);
                LogLog.debug("Starting TopicConnection.");
                this.topicConnection.start();
                initialContext.close();
                return;
            }
            goto Label_0395;
        }
        catch (JMSException ex) {
            this.errorHandler.error(new StringBuffer().append("Error while activating options for appender named [").append(this.name).append("].").toString(), (Exception)ex, 0);
        }
        catch (NamingException ex2) {
            this.errorHandler.error(new StringBuffer().append("Error while activating options for appender named [").append(this.name).append("].").toString(), ex2, 0);
        }
        catch (RuntimeException ex3) {
            this.errorHandler.error(new StringBuffer().append("Error while activating options for appender named [").append(this.name).append("].").toString(), ex3, 0);
        }
    }
    
    public void append(final LoggingEvent object) {
        if (!this.checkEntryConditions()) {
            return;
        }
        try {
            final ObjectMessage objectMessage = this.topicSession.createObjectMessage();
            if (this.locationInfo) {
                object.getLocationInformation();
            }
            objectMessage.setObject((Serializable)object);
            this.topicPublisher.publish((Message)objectMessage);
        }
        catch (JMSException ex) {
            this.errorHandler.error(new StringBuffer().append("Could not publish message in JMSAppender [").append(this.name).append("].").toString(), (Exception)ex, 0);
        }
        catch (RuntimeException ex2) {
            this.errorHandler.error(new StringBuffer().append("Could not publish message in JMSAppender [").append(this.name).append("].").toString(), ex2, 0);
        }
    }
    
    protected boolean checkEntryConditions() {
        String s = null;
        if (this.topicConnection == null) {
            s = "No TopicConnection";
        }
        else if (this.topicSession == null) {
            s = "No TopicSession";
        }
        else if (this.topicPublisher == null) {
            s = "No TopicPublisher";
        }
        if (s != null) {
            this.errorHandler.error(new StringBuffer().append(s).append(" for JMSAppender named [").append(this.name).append("].").toString());
            return false;
        }
        return true;
    }
    
    @Override
    public void close() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        org/apache/log4j/net/JMSAppender.closed:Z
        //     6: istore_1       
        //     7: iload_1        
        //     8: ifeq            14
        //    11: aload_0        
        //    12: monitorexit    
        //    13: return         
        //    14: new             Ljava/lang/StringBuffer;
        //    17: dup            
        //    18: invokespecial   java/lang/StringBuffer.<init>:()V
        //    21: ldc             "Closing appender ["
        //    23: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    26: aload_0        
        //    27: getfield        org/apache/log4j/net/JMSAppender.name:Ljava/lang/String;
        //    30: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    33: ldc             "]."
        //    35: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    38: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    41: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //    44: aload_0        
        //    45: iconst_1       
        //    46: putfield        org/apache/log4j/net/JMSAppender.closed:Z
        //    49: aload_0        
        //    50: getfield        org/apache/log4j/net/JMSAppender.topicSession:Ljavax/jms/TopicSession;
        //    53: ifnull          65
        //    56: aload_0        
        //    57: getfield        org/apache/log4j/net/JMSAppender.topicSession:Ljavax/jms/TopicSession;
        //    60: invokeinterface javax/jms/TopicSession.close:()V
        //    65: aload_0        
        //    66: getfield        org/apache/log4j/net/JMSAppender.topicConnection:Ljavax/jms/TopicConnection;
        //    69: ifnull          81
        //    72: aload_0        
        //    73: getfield        org/apache/log4j/net/JMSAppender.topicConnection:Ljavax/jms/TopicConnection;
        //    76: invokeinterface javax/jms/TopicConnection.close:()V
        //    81: aload_0        
        //    82: aconst_null    
        //    83: putfield        org/apache/log4j/net/JMSAppender.topicPublisher:Ljavax/jms/TopicPublisher;
        //    86: aload_0        
        //    87: aconst_null    
        //    88: putfield        org/apache/log4j/net/JMSAppender.topicSession:Ljavax/jms/TopicSession;
        //    91: aload_0        
        //    92: aconst_null    
        //    93: putfield        org/apache/log4j/net/JMSAppender.topicConnection:Ljavax/jms/TopicConnection;
        //    96: goto            11
        //    99: astore_2       
        //   100: aload_0        
        //   101: monitorexit    
        //   102: aload_2        
        //   103: athrow         
        //   104: astore_2       
        //   105: new             Ljava/lang/StringBuffer;
        //   108: dup            
        //   109: invokespecial   java/lang/StringBuffer.<init>:()V
        //   112: ldc             "Error while closing JMSAppender ["
        //   114: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   117: aload_0        
        //   118: getfield        org/apache/log4j/net/JMSAppender.name:Ljava/lang/String;
        //   121: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   124: ldc             "]."
        //   126: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   129: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   132: aload_2        
        //   133: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   136: goto            81
        //   139: astore_2       
        //   140: new             Ljava/lang/StringBuffer;
        //   143: dup            
        //   144: invokespecial   java/lang/StringBuffer.<init>:()V
        //   147: ldc             "Error while closing JMSAppender ["
        //   149: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   152: aload_0        
        //   153: getfield        org/apache/log4j/net/JMSAppender.name:Ljava/lang/String;
        //   156: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   159: ldc             "]."
        //   161: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   164: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   167: aload_2        
        //   168: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   171: goto            81
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  2      7      99     104    Any
        //  14     49     99     104    Any
        //  49     65     104    139    Ljavax/jms/JMSException;
        //  49     65     139    174    Ljava/lang/RuntimeException;
        //  49     65     99     104    Any
        //  65     81     104    139    Ljavax/jms/JMSException;
        //  65     81     139    174    Ljava/lang/RuntimeException;
        //  65     81     99     104    Any
        //  81     96     99     104    Any
        //  105    136    99     104    Any
        //  140    171    99     104    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0065:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public String getInitialContextFactoryName() {
        return this.initialContextFactoryName;
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public String getProviderURL() {
        return this.providerURL;
    }
    
    public String getSecurityCredentials() {
        return this.securityCredentials;
    }
    
    public String getSecurityPrincipalName() {
        return this.securityPrincipalName;
    }
    
    public String getTopicBindingName() {
        return this.topicBindingName;
    }
    
    protected TopicConnection getTopicConnection() {
        return this.topicConnection;
    }
    
    public String getTopicConnectionFactoryBindingName() {
        return this.tcfBindingName;
    }
    
    protected TopicPublisher getTopicPublisher() {
        return this.topicPublisher;
    }
    
    protected TopicSession getTopicSession() {
        return this.topicSession;
    }
    
    String getURLPkgPrefixes() {
        return this.urlPkgPrefixes;
    }
    
    public String getUserName() {
        return this.userName;
    }
    
    protected Object lookup(final Context context, final String s) throws NamingException {
        try {
            return context.lookup(s);
        }
        catch (NameNotFoundException ex) {
            LogLog.error(new StringBuffer().append("Could not find name [").append(s).append("].").toString());
            throw ex;
        }
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
    
    public void setInitialContextFactoryName(final String initialContextFactoryName) {
        this.initialContextFactoryName = initialContextFactoryName;
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    public void setProviderURL(final String providerURL) {
        this.providerURL = providerURL;
    }
    
    public void setSecurityCredentials(final String securityCredentials) {
        this.securityCredentials = securityCredentials;
    }
    
    public void setSecurityPrincipalName(final String securityPrincipalName) {
        this.securityPrincipalName = securityPrincipalName;
    }
    
    public void setTopicBindingName(final String topicBindingName) {
        this.topicBindingName = topicBindingName;
    }
    
    public void setTopicConnectionFactoryBindingName(final String tcfBindingName) {
        this.tcfBindingName = tcfBindingName;
    }
    
    public void setURLPkgPrefixes(final String urlPkgPrefixes) {
        this.urlPkgPrefixes = urlPkgPrefixes;
    }
    
    public void setUserName(final String userName) {
        this.userName = userName;
    }
}
