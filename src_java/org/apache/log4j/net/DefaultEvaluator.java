package org.apache.log4j.net;

import org.apache.log4j.spi.*;
import org.apache.log4j.*;

class DefaultEvaluator implements TriggeringEventEvaluator
{
    @Override
    public boolean isTriggeringEvent(final LoggingEvent loggingEvent) {
        return loggingEvent.getLevel().isGreaterOrEqual(Level.ERROR);
    }
}
