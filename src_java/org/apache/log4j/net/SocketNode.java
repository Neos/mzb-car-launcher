package org.apache.log4j.net;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import java.net.*;
import java.io.*;

public class SocketNode implements Runnable
{
    static Class class$org$apache$log4j$net$SocketNode;
    static Logger logger;
    LoggerRepository hierarchy;
    ObjectInputStream ois;
    Socket socket;
    
    static {
        Class class$org$apache$log4j$net$SocketNode;
        if (SocketNode.class$org$apache$log4j$net$SocketNode == null) {
            class$org$apache$log4j$net$SocketNode = (SocketNode.class$org$apache$log4j$net$SocketNode = class$("org.apache.log4j.net.SocketNode"));
        }
        else {
            class$org$apache$log4j$net$SocketNode = SocketNode.class$org$apache$log4j$net$SocketNode;
        }
        SocketNode.logger = Logger.getLogger(class$org$apache$log4j$net$SocketNode);
    }
    
    public SocketNode(final Socket socket, final LoggerRepository hierarchy) {
        this.socket = socket;
        this.hierarchy = hierarchy;
        try {
            this.ois = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        }
        catch (InterruptedIOException ex) {
            Thread.currentThread().interrupt();
            SocketNode.logger.error(new StringBuffer().append("Could not open ObjectInputStream to ").append(socket).toString(), ex);
        }
        catch (IOException ex2) {
            SocketNode.logger.error(new StringBuffer().append("Could not open ObjectInputStream to ").append(socket).toString(), ex2);
        }
        catch (RuntimeException ex3) {
            SocketNode.logger.error(new StringBuffer().append("Could not open ObjectInputStream to ").append(socket).toString(), ex3);
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void run() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //     4: ifnull          92
        //     7: aload_0        
        //     8: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //    11: invokevirtual   java/io/ObjectInputStream.readObject:()Ljava/lang/Object;
        //    14: checkcast       Lorg/apache/log4j/spi/LoggingEvent;
        //    17: astore_1       
        //    18: aload_0        
        //    19: getfield        org/apache/log4j/net/SocketNode.hierarchy:Lorg/apache/log4j/spi/LoggerRepository;
        //    22: aload_1        
        //    23: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLoggerName:()Ljava/lang/String;
        //    26: invokeinterface org/apache/log4j/spi/LoggerRepository.getLogger:(Ljava/lang/String;)Lorg/apache/log4j/Logger;
        //    31: astore_2       
        //    32: aload_1        
        //    33: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLevel:()Lorg/apache/log4j/Level;
        //    36: aload_2        
        //    37: invokevirtual   org/apache/log4j/Logger.getEffectiveLevel:()Lorg/apache/log4j/Level;
        //    40: invokevirtual   org/apache/log4j/Level.isGreaterOrEqual:(Lorg/apache/log4j/Priority;)Z
        //    43: ifeq            7
        //    46: aload_2        
        //    47: aload_1        
        //    48: invokevirtual   org/apache/log4j/Logger.callAppenders:(Lorg/apache/log4j/spi/LoggingEvent;)V
        //    51: goto            7
        //    54: astore_1       
        //    55: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //    58: ldc             "Caught java.io.EOFException closing conneciton."
        //    60: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;)V
        //    63: aload_0        
        //    64: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //    67: ifnull          77
        //    70: aload_0        
        //    71: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //    74: invokevirtual   java/io/ObjectInputStream.close:()V
        //    77: aload_0        
        //    78: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //    81: ifnull          91
        //    84: aload_0        
        //    85: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //    88: invokevirtual   java/net/Socket.close:()V
        //    91: return         
        //    92: aload_0        
        //    93: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //    96: ifnull          106
        //    99: aload_0        
        //   100: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   103: invokevirtual   java/io/ObjectInputStream.close:()V
        //   106: aload_0        
        //   107: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   110: ifnull          91
        //   113: aload_0        
        //   114: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   117: invokevirtual   java/net/Socket.close:()V
        //   120: return         
        //   121: astore_1       
        //   122: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   125: invokevirtual   java/lang/Thread.interrupt:()V
        //   128: return         
        //   129: astore_1       
        //   130: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   133: ldc             "Could not close connection."
        //   135: aload_1        
        //   136: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   139: goto            106
        //   142: astore_1       
        //   143: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   146: ldc             "Could not close connection."
        //   148: aload_1        
        //   149: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   152: goto            77
        //   155: astore_1       
        //   156: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   159: invokevirtual   java/lang/Thread.interrupt:()V
        //   162: return         
        //   163: astore_1       
        //   164: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   167: ldc             "Caught java.net.SocketException closing conneciton."
        //   169: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;)V
        //   172: aload_0        
        //   173: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   176: ifnull          186
        //   179: aload_0        
        //   180: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   183: invokevirtual   java/io/ObjectInputStream.close:()V
        //   186: aload_0        
        //   187: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   190: ifnull          91
        //   193: aload_0        
        //   194: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   197: invokevirtual   java/net/Socket.close:()V
        //   200: return         
        //   201: astore_1       
        //   202: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   205: invokevirtual   java/lang/Thread.interrupt:()V
        //   208: return         
        //   209: astore_1       
        //   210: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   213: ldc             "Could not close connection."
        //   215: aload_1        
        //   216: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   219: goto            186
        //   222: astore_1       
        //   223: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   226: invokevirtual   java/lang/Thread.interrupt:()V
        //   229: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   232: new             Ljava/lang/StringBuffer;
        //   235: dup            
        //   236: invokespecial   java/lang/StringBuffer.<init>:()V
        //   239: ldc             "Caught java.io.InterruptedIOException: "
        //   241: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   244: aload_1        
        //   245: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   248: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   251: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;)V
        //   254: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   257: ldc             "Closing connection."
        //   259: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;)V
        //   262: aload_0        
        //   263: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   266: ifnull          276
        //   269: aload_0        
        //   270: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   273: invokevirtual   java/io/ObjectInputStream.close:()V
        //   276: aload_0        
        //   277: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   280: ifnull          91
        //   283: aload_0        
        //   284: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   287: invokevirtual   java/net/Socket.close:()V
        //   290: return         
        //   291: astore_1       
        //   292: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   295: invokevirtual   java/lang/Thread.interrupt:()V
        //   298: return         
        //   299: astore_1       
        //   300: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   303: ldc             "Could not close connection."
        //   305: aload_1        
        //   306: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   309: goto            276
        //   312: astore_1       
        //   313: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   316: new             Ljava/lang/StringBuffer;
        //   319: dup            
        //   320: invokespecial   java/lang/StringBuffer.<init>:()V
        //   323: ldc             "Caught java.io.IOException: "
        //   325: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   328: aload_1        
        //   329: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   332: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   335: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;)V
        //   338: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   341: ldc             "Closing connection."
        //   343: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;)V
        //   346: aload_0        
        //   347: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   350: ifnull          360
        //   353: aload_0        
        //   354: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   357: invokevirtual   java/io/ObjectInputStream.close:()V
        //   360: aload_0        
        //   361: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   364: ifnull          91
        //   367: aload_0        
        //   368: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   371: invokevirtual   java/net/Socket.close:()V
        //   374: return         
        //   375: astore_1       
        //   376: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   379: invokevirtual   java/lang/Thread.interrupt:()V
        //   382: return         
        //   383: astore_1       
        //   384: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   387: ldc             "Could not close connection."
        //   389: aload_1        
        //   390: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   393: goto            360
        //   396: astore_1       
        //   397: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   400: ldc             "Unexpected exception. Closing conneciton."
        //   402: aload_1        
        //   403: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   406: aload_0        
        //   407: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   410: ifnull          420
        //   413: aload_0        
        //   414: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   417: invokevirtual   java/io/ObjectInputStream.close:()V
        //   420: aload_0        
        //   421: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   424: ifnull          91
        //   427: aload_0        
        //   428: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   431: invokevirtual   java/net/Socket.close:()V
        //   434: return         
        //   435: astore_1       
        //   436: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   439: invokevirtual   java/lang/Thread.interrupt:()V
        //   442: return         
        //   443: astore_1       
        //   444: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   447: ldc             "Could not close connection."
        //   449: aload_1        
        //   450: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   453: goto            420
        //   456: astore_1       
        //   457: aload_0        
        //   458: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   461: ifnull          471
        //   464: aload_0        
        //   465: getfield        org/apache/log4j/net/SocketNode.ois:Ljava/io/ObjectInputStream;
        //   468: invokevirtual   java/io/ObjectInputStream.close:()V
        //   471: aload_0        
        //   472: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   475: ifnull          485
        //   478: aload_0        
        //   479: getfield        org/apache/log4j/net/SocketNode.socket:Ljava/net/Socket;
        //   482: invokevirtual   java/net/Socket.close:()V
        //   485: aload_1        
        //   486: athrow         
        //   487: astore_2       
        //   488: getstatic       org/apache/log4j/net/SocketNode.logger:Lorg/apache/log4j/Logger;
        //   491: ldc             "Could not close connection."
        //   493: aload_2        
        //   494: invokevirtual   org/apache/log4j/Logger.info:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   497: goto            471
        //   500: astore_2       
        //   501: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   504: invokevirtual   java/lang/Thread.interrupt:()V
        //   507: goto            485
        //   510: astore_2       
        //   511: goto            485
        //   514: astore_1       
        //   515: return         
        //   516: astore_1       
        //   517: return         
        //   518: astore_1       
        //   519: return         
        //   520: astore_1       
        //   521: return         
        //   522: astore_1       
        //   523: return         
        //   524: astore_1       
        //   525: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  0      7      54     163    Ljava/io/EOFException;
        //  0      7      163    222    Ljava/net/SocketException;
        //  0      7      222    312    Ljava/io/InterruptedIOException;
        //  0      7      312    396    Ljava/io/IOException;
        //  0      7      396    456    Ljava/lang/Exception;
        //  0      7      456    514    Any
        //  7      51     54     163    Ljava/io/EOFException;
        //  7      51     163    222    Ljava/net/SocketException;
        //  7      51     222    312    Ljava/io/InterruptedIOException;
        //  7      51     312    396    Ljava/io/IOException;
        //  7      51     396    456    Ljava/lang/Exception;
        //  7      51     456    514    Any
        //  55     63     456    514    Any
        //  70     77     142    155    Ljava/lang/Exception;
        //  84     91     155    163    Ljava/io/InterruptedIOException;
        //  84     91     522    524    Ljava/io/IOException;
        //  99     106    129    142    Ljava/lang/Exception;
        //  113    120    121    129    Ljava/io/InterruptedIOException;
        //  113    120    524    526    Ljava/io/IOException;
        //  164    172    456    514    Any
        //  179    186    209    222    Ljava/lang/Exception;
        //  193    200    201    209    Ljava/io/InterruptedIOException;
        //  193    200    520    522    Ljava/io/IOException;
        //  223    262    456    514    Any
        //  269    276    299    312    Ljava/lang/Exception;
        //  283    290    291    299    Ljava/io/InterruptedIOException;
        //  283    290    518    520    Ljava/io/IOException;
        //  313    346    456    514    Any
        //  353    360    383    396    Ljava/lang/Exception;
        //  367    374    375    383    Ljava/io/InterruptedIOException;
        //  367    374    516    518    Ljava/io/IOException;
        //  397    406    456    514    Any
        //  413    420    443    456    Ljava/lang/Exception;
        //  427    434    435    443    Ljava/io/InterruptedIOException;
        //  427    434    514    516    Ljava/io/IOException;
        //  464    471    487    500    Ljava/lang/Exception;
        //  478    485    500    510    Ljava/io/InterruptedIOException;
        //  478    485    510    514    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 244, Size: 244
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3569)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
