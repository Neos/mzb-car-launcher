package org.apache.log4j.net;

import java.text.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import java.io.*;

public class SyslogAppender extends AppenderSkeleton
{
    protected static final int FACILITY_OI = 1;
    public static final int LOG_AUTH = 32;
    public static final int LOG_AUTHPRIV = 80;
    public static final int LOG_CRON = 72;
    public static final int LOG_DAEMON = 24;
    public static final int LOG_FTP = 88;
    public static final int LOG_KERN = 0;
    public static final int LOG_LOCAL0 = 128;
    public static final int LOG_LOCAL1 = 136;
    public static final int LOG_LOCAL2 = 144;
    public static final int LOG_LOCAL3 = 152;
    public static final int LOG_LOCAL4 = 160;
    public static final int LOG_LOCAL5 = 168;
    public static final int LOG_LOCAL6 = 176;
    public static final int LOG_LOCAL7 = 184;
    public static final int LOG_LPR = 48;
    public static final int LOG_MAIL = 16;
    public static final int LOG_NEWS = 56;
    public static final int LOG_SYSLOG = 40;
    public static final int LOG_USER = 8;
    public static final int LOG_UUCP = 64;
    protected static final int SYSLOG_HOST_OI = 0;
    static final String TAB = "    ";
    private final SimpleDateFormat dateFormat;
    boolean facilityPrinting;
    String facilityStr;
    private boolean header;
    private boolean layoutHeaderChecked;
    private String localHostname;
    SyslogQuietWriter sqw;
    int syslogFacility;
    String syslogHost;
    
    public SyslogAppender() {
        this.syslogFacility = 8;
        this.facilityPrinting = false;
        this.header = false;
        this.dateFormat = new SimpleDateFormat("MMM dd HH:mm:ss ", Locale.ENGLISH);
        this.layoutHeaderChecked = false;
        this.initSyslogFacilityStr();
    }
    
    public SyslogAppender(final Layout layout, final int syslogFacility) {
        this.syslogFacility = 8;
        this.facilityPrinting = false;
        this.header = false;
        this.dateFormat = new SimpleDateFormat("MMM dd HH:mm:ss ", Locale.ENGLISH);
        this.layoutHeaderChecked = false;
        this.layout = layout;
        this.syslogFacility = syslogFacility;
        this.initSyslogFacilityStr();
    }
    
    public SyslogAppender(final Layout layout, final String syslogHost, final int n) {
        this(layout, n);
        this.setSyslogHost(syslogHost);
    }
    
    public static int getFacility(final String s) {
        String trim = s;
        if (s != null) {
            trim = s.trim();
        }
        if ("KERN".equalsIgnoreCase(trim)) {
            return 0;
        }
        if ("USER".equalsIgnoreCase(trim)) {
            return 8;
        }
        if ("MAIL".equalsIgnoreCase(trim)) {
            return 16;
        }
        if ("DAEMON".equalsIgnoreCase(trim)) {
            return 24;
        }
        if ("AUTH".equalsIgnoreCase(trim)) {
            return 32;
        }
        if ("SYSLOG".equalsIgnoreCase(trim)) {
            return 40;
        }
        if ("LPR".equalsIgnoreCase(trim)) {
            return 48;
        }
        if ("NEWS".equalsIgnoreCase(trim)) {
            return 56;
        }
        if ("UUCP".equalsIgnoreCase(trim)) {
            return 64;
        }
        if ("CRON".equalsIgnoreCase(trim)) {
            return 72;
        }
        if ("AUTHPRIV".equalsIgnoreCase(trim)) {
            return 80;
        }
        if ("FTP".equalsIgnoreCase(trim)) {
            return 88;
        }
        if ("LOCAL0".equalsIgnoreCase(trim)) {
            return 128;
        }
        if ("LOCAL1".equalsIgnoreCase(trim)) {
            return 136;
        }
        if ("LOCAL2".equalsIgnoreCase(trim)) {
            return 144;
        }
        if ("LOCAL3".equalsIgnoreCase(trim)) {
            return 152;
        }
        if ("LOCAL4".equalsIgnoreCase(trim)) {
            return 160;
        }
        if ("LOCAL5".equalsIgnoreCase(trim)) {
            return 168;
        }
        if ("LOCAL6".equalsIgnoreCase(trim)) {
            return 176;
        }
        if ("LOCAL7".equalsIgnoreCase(trim)) {
            return 184;
        }
        return -1;
    }
    
    public static String getFacilityString(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 0: {
                return "kern";
            }
            case 8: {
                return "user";
            }
            case 16: {
                return "mail";
            }
            case 24: {
                return "daemon";
            }
            case 32: {
                return "auth";
            }
            case 40: {
                return "syslog";
            }
            case 48: {
                return "lpr";
            }
            case 56: {
                return "news";
            }
            case 64: {
                return "uucp";
            }
            case 72: {
                return "cron";
            }
            case 80: {
                return "authpriv";
            }
            case 88: {
                return "ftp";
            }
            case 128: {
                return "local0";
            }
            case 136: {
                return "local1";
            }
            case 144: {
                return "local2";
            }
            case 152: {
                return "local3";
            }
            case 160: {
                return "local4";
            }
            case 168: {
                return "local5";
            }
            case 176: {
                return "local6";
            }
            case 184: {
                return "local7";
            }
        }
    }
    
    private String getLocalHostname() {
        Label_0017: {
            if (this.localHostname != null) {
                break Label_0017;
            }
            try {
                this.localHostname = InetAddress.getLocalHost().getHostName();
                return this.localHostname;
            }
            catch (UnknownHostException ex) {
                this.localHostname = "UNKNOWN_HOST";
                return this.localHostname;
            }
        }
    }
    
    private String getPacketHeader(final long n) {
        if (this.header) {
            final StringBuffer sb = new StringBuffer(this.dateFormat.format(new Date(n)));
            if (sb.charAt(4) == '0') {
                sb.setCharAt(4, ' ');
            }
            sb.append(this.getLocalHostname());
            sb.append(' ');
            return sb.toString();
        }
        return "";
    }
    
    private void initSyslogFacilityStr() {
        this.facilityStr = getFacilityString(this.syslogFacility);
        if (this.facilityStr == null) {
            System.err.println(new StringBuffer().append("\"").append(this.syslogFacility).append("\" is an unknown syslog facility. Defaulting to \"USER\".").toString());
            this.syslogFacility = 8;
            this.facilityStr = "user:";
            return;
        }
        this.facilityStr = new StringBuffer().append(this.facilityStr).append(":").toString();
    }
    
    private void sendLayoutMessage(final String s) {
        if (this.sqw != null) {
            String string = s;
            final String packetHeader = this.getPacketHeader(new Date().getTime());
            if (this.facilityPrinting || packetHeader.length() > 0) {
                final StringBuffer sb = new StringBuffer(packetHeader);
                if (this.facilityPrinting) {
                    sb.append(this.facilityStr);
                }
                sb.append(s);
                string = sb.toString();
            }
            this.sqw.setLevel(6);
            this.sqw.write(string);
        }
    }
    
    private void splitPacket(final String s, final String s2) {
        if (s2.getBytes().length <= 1019) {
            this.sqw.write(s2);
            return;
        }
        final int n = s.length() + (s2.length() - s.length()) / 2;
        this.splitPacket(s, new StringBuffer().append(s2.substring(0, n)).append("...").toString());
        this.splitPacket(s, new StringBuffer().append(s).append("...").append(s2.substring(n)).toString());
    }
    
    @Override
    public void activateOptions() {
        if (this.header) {
            this.getLocalHostname();
        }
        if (this.layout != null && this.layout.getHeader() != null) {
            this.sendLayoutMessage(this.layout.getHeader());
        }
        this.layoutHeaderChecked = true;
    }
    
    public void append(final LoggingEvent loggingEvent) {
        if (this.isAsSevereAsThreshold(loggingEvent.getLevel())) {
            if (this.sqw == null) {
                this.errorHandler.error(new StringBuffer().append("No syslog host is set for SyslogAppedender named \"").append(this.name).append("\".").toString());
                return;
            }
            if (!this.layoutHeaderChecked) {
                if (this.layout != null && this.layout.getHeader() != null) {
                    this.sendLayoutMessage(this.layout.getHeader());
                }
                this.layoutHeaderChecked = true;
            }
            final String packetHeader = this.getPacketHeader(loggingEvent.timeStamp);
            String s;
            if (this.layout == null) {
                s = String.valueOf(loggingEvent.getMessage());
            }
            else {
                s = this.layout.format(loggingEvent);
            }
            String string = null;
            Label_0183: {
                if (!this.facilityPrinting) {
                    string = s;
                    if (packetHeader.length() <= 0) {
                        break Label_0183;
                    }
                }
                final StringBuffer sb = new StringBuffer(packetHeader);
                if (this.facilityPrinting) {
                    sb.append(this.facilityStr);
                }
                sb.append(s);
                string = sb.toString();
            }
            this.sqw.setLevel(loggingEvent.getLevel().getSyslogEquivalent());
            if (string.length() > 256) {
                this.splitPacket(packetHeader, string);
            }
            else {
                this.sqw.write(string);
            }
            if (this.layout == null || this.layout.ignoresThrowable()) {
                final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
                if (throwableStrRep != null) {
                    for (int i = 0; i < throwableStrRep.length; ++i) {
                        if (throwableStrRep[i].startsWith("\t")) {
                            this.sqw.write(new StringBuffer().append(packetHeader).append("    ").append(throwableStrRep[i].substring(1)).toString());
                        }
                        else {
                            this.sqw.write(new StringBuffer().append(packetHeader).append(throwableStrRep[i]).toString());
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void close() {
        // monitorenter(this)
        try {
            this.closed = true;
            if (this.sqw == null) {
                return;
            }
            try {
                if (this.layoutHeaderChecked && this.layout != null && this.layout.getFooter() != null) {
                    this.sendLayoutMessage(this.layout.getFooter());
                }
                this.sqw.close();
                this.sqw = null;
            }
            catch (InterruptedIOException ex) {
                Thread.currentThread().interrupt();
                this.sqw = null;
            }
            catch (IOException ex2) {
                this.sqw = null;
            }
        }
        finally {}
    }
    
    public String getFacility() {
        return getFacilityString(this.syslogFacility);
    }
    
    public boolean getFacilityPrinting() {
        return this.facilityPrinting;
    }
    
    public final boolean getHeader() {
        return this.header;
    }
    
    public String getSyslogHost() {
        return this.syslogHost;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    public void setFacility(final String s) {
        if (s != null) {
            this.syslogFacility = getFacility(s);
            if (this.syslogFacility == -1) {
                System.err.println(new StringBuffer().append("[").append(s).append("] is an unknown syslog facility. Defaulting to [USER].").toString());
                this.syslogFacility = 8;
            }
            this.initSyslogFacilityStr();
            if (this.sqw != null) {
                this.sqw.setSyslogFacility(this.syslogFacility);
            }
        }
    }
    
    public void setFacilityPrinting(final boolean facilityPrinting) {
        this.facilityPrinting = facilityPrinting;
    }
    
    public final void setHeader(final boolean header) {
        this.header = header;
    }
    
    public void setSyslogHost(final String syslogHost) {
        this.sqw = new SyslogQuietWriter(new SyslogWriter(syslogHost), this.syslogFacility, this.errorHandler);
        this.syslogHost = syslogHost;
    }
}
