package org.apache.log4j.net;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import java.net.*;
import java.io.*;
import org.apache.log4j.helpers.*;
import java.util.*;

public class TelnetAppender extends AppenderSkeleton
{
    private int port;
    private SocketHandler sh;
    
    public TelnetAppender() {
        this.port = 23;
    }
    
    @Override
    public void activateOptions() {
        while (true) {
            try {
                (this.sh = new SocketHandler(this.port)).start();
                super.activateOptions();
            }
            catch (InterruptedIOException ex) {
                Thread.currentThread().interrupt();
                ex.printStackTrace();
                continue;
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
                continue;
            }
            catch (RuntimeException ex3) {
                ex3.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    @Override
    protected void append(final LoggingEvent loggingEvent) {
        if (this.sh != null) {
            this.sh.send(this.layout.format(loggingEvent));
            if (this.layout.ignoresThrowable()) {
                final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
                if (throwableStrRep != null) {
                    final StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < throwableStrRep.length; ++i) {
                        sb.append(throwableStrRep[i]);
                        sb.append("\r\n");
                    }
                    this.sh.send(sb.toString());
                }
            }
        }
    }
    
    @Override
    public void close() {
        if (this.sh == null) {
            return;
        }
        this.sh.close();
        try {
            this.sh.join();
        }
        catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
    
    public int getPort() {
        return this.port;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    public void setPort(final int port) {
        this.port = port;
    }
    
    protected class SocketHandler extends Thread
    {
        private int MAX_CONNECTIONS;
        private Vector connections;
        private ServerSocket serverSocket;
        private final TelnetAppender this$0;
        private Vector writers;
        
        public SocketHandler(final TelnetAppender this$0, final int n) throws IOException {
            this.this$0 = this$0;
            this.writers = new Vector();
            this.connections = new Vector();
            this.MAX_CONNECTIONS = 20;
            this.serverSocket = new ServerSocket(n);
            this.setName(new StringBuffer().append("TelnetAppender-").append(this.getName()).append("-").append(n).toString());
        }
        
        public void close() {
            // monitorenter(this)
            try {
                final Enumeration<Socket> elements = this.connections.elements();
                while (elements.hasMoreElements()) {
                    try {
                        elements.nextElement().close();
                    }
                    catch (InterruptedIOException ex) {
                        Thread.currentThread().interrupt();
                    }
                    catch (RuntimeException ex2) {}
                    catch (IOException ex3) {}
                }
                goto Label_0051;
            }
            finally {}
            try {
                this.serverSocket.close();
            }
            catch (InterruptedIOException ex4) {}
            catch (RuntimeException ex5) {}
            catch (IOException ex6) {}
        }
        
        public void finalize() {
            this.close();
        }
        
        @Override
        public void run() {
            while (true) {
                Label_0155: {
                    if (!this.serverSocket.isClosed()) {
                        try {
                            final Socket accept = this.serverSocket.accept();
                            final PrintWriter printWriter = new PrintWriter(accept.getOutputStream());
                            if (this.connections.size() >= this.MAX_CONNECTIONS) {
                                break Label_0155;
                            }
                            synchronized (this) {
                                this.connections.addElement(accept);
                                this.writers.addElement(printWriter);
                                printWriter.print(new StringBuffer().append("TelnetAppender v1.0 (").append(this.connections.size()).append(" active connections)\r\n\r\n").toString());
                                printWriter.flush();
                            }
                        }
                        catch (Exception ex) {
                            if (ex instanceof InterruptedIOException || ex instanceof InterruptedException) {
                                Thread.currentThread().interrupt();
                            }
                            if (!this.serverSocket.isClosed()) {
                                LogLog.error("Encountered error while in SocketHandler loop.", ex);
                            }
                        }
                    }
                    try {
                        this.serverSocket.close();
                        return;
                        final PrintWriter printWriter;
                        printWriter.print("Too many connections.\r\n");
                        printWriter.flush();
                        final Socket accept;
                        accept.close();
                    }
                    catch (InterruptedIOException ex2) {
                        Thread.currentThread().interrupt();
                    }
                    catch (IOException ex3) {}
                }
            }
        }
        
        public void send(final String s) {
            synchronized (this) {
                final Iterator iterator = this.connections.iterator();
                final Iterator<PrintWriter> iterator2 = (Iterator<PrintWriter>)this.writers.iterator();
                while (iterator2.hasNext()) {
                    iterator.next();
                    final PrintWriter printWriter = iterator2.next();
                    printWriter.print(s);
                    if (printWriter.checkError()) {
                        iterator.remove();
                        iterator2.remove();
                    }
                }
            }
        }
        // monitorexit(this)
    }
}
