package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.io.*;
import org.apache.log4j.spi.*;

public class RollingFileAppender extends FileAppender
{
    protected int maxBackupIndex;
    protected long maxFileSize;
    private long nextRollover;
    
    public RollingFileAppender() {
        this.maxFileSize = 10485760L;
        this.maxBackupIndex = 1;
        this.nextRollover = 0L;
    }
    
    public RollingFileAppender(final Layout layout, final String s) throws IOException {
        super(layout, s);
        this.maxFileSize = 10485760L;
        this.maxBackupIndex = 1;
        this.nextRollover = 0L;
    }
    
    public RollingFileAppender(final Layout layout, final String s, final boolean b) throws IOException {
        super(layout, s, b);
        this.maxFileSize = 10485760L;
        this.maxBackupIndex = 1;
        this.nextRollover = 0L;
    }
    
    public int getMaxBackupIndex() {
        return this.maxBackupIndex;
    }
    
    public long getMaximumFileSize() {
        return this.maxFileSize;
    }
    
    public void rollOver() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/apache/log4j/RollingFileAppender.qw:Lorg/apache/log4j/helpers/QuietWriter;
        //     4: ifnull          50
        //     7: aload_0        
        //     8: getfield        org/apache/log4j/RollingFileAppender.qw:Lorg/apache/log4j/helpers/QuietWriter;
        //    11: checkcast       Lorg/apache/log4j/helpers/CountingQuietWriter;
        //    14: invokevirtual   org/apache/log4j/helpers/CountingQuietWriter.getCount:()J
        //    17: lstore_2       
        //    18: new             Ljava/lang/StringBuffer;
        //    21: dup            
        //    22: invokespecial   java/lang/StringBuffer.<init>:()V
        //    25: ldc             "rolling over count="
        //    27: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    30: lload_2        
        //    31: invokevirtual   java/lang/StringBuffer.append:(J)Ljava/lang/StringBuffer;
        //    34: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    37: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //    40: aload_0        
        //    41: aload_0        
        //    42: getfield        org/apache/log4j/RollingFileAppender.maxFileSize:J
        //    45: lload_2        
        //    46: ladd           
        //    47: putfield        org/apache/log4j/RollingFileAppender.nextRollover:J
        //    50: new             Ljava/lang/StringBuffer;
        //    53: dup            
        //    54: invokespecial   java/lang/StringBuffer.<init>:()V
        //    57: ldc             "maxBackupIndex="
        //    59: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    62: aload_0        
        //    63: getfield        org/apache/log4j/RollingFileAppender.maxBackupIndex:I
        //    66: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //    69: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    72: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //    75: iconst_1       
        //    76: istore          4
        //    78: iconst_1       
        //    79: istore          5
        //    81: aload_0        
        //    82: getfield        org/apache/log4j/RollingFileAppender.maxBackupIndex:I
        //    85: ifle            420
        //    88: new             Ljava/io/File;
        //    91: dup            
        //    92: new             Ljava/lang/StringBuffer;
        //    95: dup            
        //    96: invokespecial   java/lang/StringBuffer.<init>:()V
        //    99: aload_0        
        //   100: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   103: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   106: bipush          46
        //   108: invokevirtual   java/lang/StringBuffer.append:(C)Ljava/lang/StringBuffer;
        //   111: aload_0        
        //   112: getfield        org/apache/log4j/RollingFileAppender.maxBackupIndex:I
        //   115: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   118: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   121: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   124: astore          6
        //   126: aload           6
        //   128: invokevirtual   java/io/File.exists:()Z
        //   131: ifeq            141
        //   134: aload           6
        //   136: invokevirtual   java/io/File.delete:()Z
        //   139: istore          5
        //   141: aload_0        
        //   142: getfield        org/apache/log4j/RollingFileAppender.maxBackupIndex:I
        //   145: iconst_1       
        //   146: isub           
        //   147: istore_1       
        //   148: iload_1        
        //   149: iconst_1       
        //   150: if_icmplt       287
        //   153: iload           5
        //   155: ifeq            287
        //   158: new             Ljava/io/File;
        //   161: dup            
        //   162: new             Ljava/lang/StringBuffer;
        //   165: dup            
        //   166: invokespecial   java/lang/StringBuffer.<init>:()V
        //   169: aload_0        
        //   170: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   173: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   176: ldc             "."
        //   178: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   181: iload_1        
        //   182: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   185: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   188: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   191: astore          6
        //   193: aload           6
        //   195: invokevirtual   java/io/File.exists:()Z
        //   198: ifeq            280
        //   201: new             Ljava/io/File;
        //   204: dup            
        //   205: new             Ljava/lang/StringBuffer;
        //   208: dup            
        //   209: invokespecial   java/lang/StringBuffer.<init>:()V
        //   212: aload_0        
        //   213: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   216: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   219: bipush          46
        //   221: invokevirtual   java/lang/StringBuffer.append:(C)Ljava/lang/StringBuffer;
        //   224: iload_1        
        //   225: iconst_1       
        //   226: iadd           
        //   227: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   230: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   233: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   236: astore          7
        //   238: new             Ljava/lang/StringBuffer;
        //   241: dup            
        //   242: invokespecial   java/lang/StringBuffer.<init>:()V
        //   245: ldc             "Renaming file "
        //   247: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   250: aload           6
        //   252: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   255: ldc             " to "
        //   257: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   260: aload           7
        //   262: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   265: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   268: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //   271: aload           6
        //   273: aload           7
        //   275: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   278: istore          5
        //   280: iload_1        
        //   281: iconst_1       
        //   282: isub           
        //   283: istore_1       
        //   284: goto            148
        //   287: iload           5
        //   289: istore          4
        //   291: iload           5
        //   293: ifeq            420
        //   296: new             Ljava/io/File;
        //   299: dup            
        //   300: new             Ljava/lang/StringBuffer;
        //   303: dup            
        //   304: invokespecial   java/lang/StringBuffer.<init>:()V
        //   307: aload_0        
        //   308: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   311: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   314: ldc             "."
        //   316: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   319: iconst_1       
        //   320: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   323: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   326: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   329: astore          6
        //   331: aload_0        
        //   332: invokevirtual   org/apache/log4j/RollingFileAppender.closeFile:()V
        //   335: new             Ljava/io/File;
        //   338: dup            
        //   339: aload_0        
        //   340: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   343: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   346: astore          7
        //   348: new             Ljava/lang/StringBuffer;
        //   351: dup            
        //   352: invokespecial   java/lang/StringBuffer.<init>:()V
        //   355: ldc             "Renaming file "
        //   357: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   360: aload           7
        //   362: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   365: ldc             " to "
        //   367: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   370: aload           6
        //   372: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   375: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   378: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //   381: aload           7
        //   383: aload           6
        //   385: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   388: istore          5
        //   390: iload           5
        //   392: istore          4
        //   394: iload           5
        //   396: ifne            420
        //   399: aload_0        
        //   400: aload_0        
        //   401: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   404: iconst_1       
        //   405: aload_0        
        //   406: getfield        org/apache/log4j/RollingFileAppender.bufferedIO:Z
        //   409: aload_0        
        //   410: getfield        org/apache/log4j/RollingFileAppender.bufferSize:I
        //   413: invokevirtual   org/apache/log4j/RollingFileAppender.setFile:(Ljava/lang/String;ZZI)V
        //   416: iload           5
        //   418: istore          4
        //   420: iload           4
        //   422: ifeq            447
        //   425: aload_0        
        //   426: aload_0        
        //   427: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   430: iconst_0       
        //   431: aload_0        
        //   432: getfield        org/apache/log4j/RollingFileAppender.bufferedIO:Z
        //   435: aload_0        
        //   436: getfield        org/apache/log4j/RollingFileAppender.bufferSize:I
        //   439: invokevirtual   org/apache/log4j/RollingFileAppender.setFile:(Ljava/lang/String;ZZI)V
        //   442: aload_0        
        //   443: lconst_0       
        //   444: putfield        org/apache/log4j/RollingFileAppender.nextRollover:J
        //   447: return         
        //   448: astore          6
        //   450: aload           6
        //   452: instanceof      Ljava/io/InterruptedIOException;
        //   455: ifeq            464
        //   458: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   461: invokevirtual   java/lang/Thread.interrupt:()V
        //   464: new             Ljava/lang/StringBuffer;
        //   467: dup            
        //   468: invokespecial   java/lang/StringBuffer.<init>:()V
        //   471: ldc             "setFile("
        //   473: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   476: aload_0        
        //   477: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   480: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   483: ldc             ", true) call failed."
        //   485: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   488: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   491: aload           6
        //   493: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   496: iload           5
        //   498: istore          4
        //   500: goto            420
        //   503: astore          6
        //   505: aload           6
        //   507: instanceof      Ljava/io/InterruptedIOException;
        //   510: ifeq            519
        //   513: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   516: invokevirtual   java/lang/Thread.interrupt:()V
        //   519: new             Ljava/lang/StringBuffer;
        //   522: dup            
        //   523: invokespecial   java/lang/StringBuffer.<init>:()V
        //   526: ldc             "setFile("
        //   528: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   531: aload_0        
        //   532: getfield        org/apache/log4j/RollingFileAppender.fileName:Ljava/lang/String;
        //   535: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   538: ldc             ", false) call failed."
        //   540: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   543: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   546: aload           6
        //   548: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   551: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  399    416    448    503    Ljava/io/IOException;
        //  425    447    503    552    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0447:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void setFile(final String s, final boolean b, final boolean b2, final int n) throws IOException {
        synchronized (this) {
            super.setFile(s, b, this.bufferedIO, this.bufferSize);
            if (b) {
                ((CountingQuietWriter)this.qw).setCount(new File(s).length());
            }
        }
    }
    
    public void setMaxBackupIndex(final int maxBackupIndex) {
        this.maxBackupIndex = maxBackupIndex;
    }
    
    public void setMaxFileSize(final String s) {
        this.maxFileSize = OptionConverter.toFileSize(s, this.maxFileSize + 1L);
    }
    
    public void setMaximumFileSize(final long maxFileSize) {
        this.maxFileSize = maxFileSize;
    }
    
    @Override
    protected void setQWForFiles(final Writer writer) {
        this.qw = new CountingQuietWriter(writer, this.errorHandler);
    }
    
    @Override
    protected void subAppend(final LoggingEvent loggingEvent) {
        super.subAppend(loggingEvent);
        if (this.fileName != null && this.qw != null) {
            final long count = ((CountingQuietWriter)this.qw).getCount();
            if (count >= this.maxFileSize && count >= this.nextRollover) {
                this.rollOver();
            }
        }
    }
}
