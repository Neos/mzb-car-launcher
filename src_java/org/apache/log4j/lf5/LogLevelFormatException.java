package org.apache.log4j.lf5;

public class LogLevelFormatException extends Exception
{
    public LogLevelFormatException(final String s) {
        super(s);
    }
}
