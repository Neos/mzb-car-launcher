package org.apache.log4j.lf5.viewer.configure;

import java.net.*;
import java.util.*;
import java.io.*;

public class MRUFileManager
{
    private static final String CONFIG_FILE_NAME = "mru_file_manager";
    private static final int DEFAULT_MAX_SIZE = 3;
    private int _maxSize;
    private LinkedList _mruFileList;
    
    public MRUFileManager() {
        this._maxSize = 0;
        this.load();
        this.setMaxSize(3);
    }
    
    public MRUFileManager(final int maxSize) {
        this._maxSize = 0;
        this.load();
        this.setMaxSize(maxSize);
    }
    
    public static void createConfigurationDirectory() {
        final File file = new File(new StringBuffer().append(System.getProperty("user.home")).append(System.getProperty("file.separator")).append("lf5").toString());
        if (file.exists()) {
            return;
        }
        try {
            file.mkdir();
        }
        catch (SecurityException ex) {
            ex.printStackTrace();
        }
    }
    
    public Object getFile(final int n) {
        if (n < this.size()) {
            return this._mruFileList.get(n);
        }
        return null;
    }
    
    protected String getFilename() {
        final String property = System.getProperty("user.home");
        final String property2 = System.getProperty("file.separator");
        return new StringBuffer().append(property).append(property2).append("lf5").append(property2).append("mru_file_manager").toString();
    }
    
    public InputStream getInputStream(final int n) throws IOException, FileNotFoundException {
        if (n >= this.size()) {
            return null;
        }
        final Object file = this.getFile(n);
        if (file instanceof File) {
            return this.getInputStream((File)file);
        }
        return this.getInputStream((URL)file);
    }
    
    protected InputStream getInputStream(final File file) throws IOException, FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(file));
    }
    
    protected InputStream getInputStream(final URL url) throws IOException {
        return url.openStream();
    }
    
    public String[] getMRUFileList() {
        String[] array;
        if (this.size() == 0) {
            array = null;
        }
        else {
            final String[] array2 = new String[this.size()];
            int n = 0;
            while (true) {
                array = array2;
                if (n >= this.size()) {
                    break;
                }
                final Object file = this.getFile(n);
                if (file instanceof File) {
                    array2[n] = ((File)file).getAbsolutePath();
                }
                else {
                    array2[n] = file.toString();
                }
                ++n;
            }
        }
        return array;
    }
    
    protected void load() {
        createConfigurationDirectory();
        final File file = new File(this.getFilename());
        if (file.exists()) {
            try {
                final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
                this._mruFileList = (LinkedList)objectInputStream.readObject();
                objectInputStream.close();
                final Iterator iterator = this._mruFileList.iterator();
                while (iterator.hasNext()) {
                    final Object next = iterator.next();
                    if (!(next instanceof File) && !(next instanceof URL)) {
                        iterator.remove();
                    }
                }
            }
            catch (Exception ex) {
                this._mruFileList = new LinkedList();
            }
            return;
        }
        this._mruFileList = new LinkedList();
    }
    
    public void moveToTop(final int n) {
        this._mruFileList.add(0, this._mruFileList.remove(n));
    }
    
    public void save() {
        final File file = new File(this.getFilename());
        try {
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
            objectOutputStream.writeObject(this._mruFileList);
            objectOutputStream.flush();
            objectOutputStream.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void set(final File mru) {
        this.setMRU(mru);
    }
    
    public void set(final URL mru) {
        this.setMRU(mru);
    }
    
    protected void setMRU(final Object o) {
        final int index = this._mruFileList.indexOf(o);
        if (index == -1) {
            this._mruFileList.add(0, o);
            this.setMaxSize(this._maxSize);
            return;
        }
        this.moveToTop(index);
    }
    
    protected void setMaxSize(final int maxSize) {
        if (maxSize < this._mruFileList.size()) {
            for (int i = 0; i < this._mruFileList.size() - maxSize; ++i) {
                this._mruFileList.removeLast();
            }
        }
        this._maxSize = maxSize;
    }
    
    public int size() {
        return this._mruFileList.size();
    }
}
