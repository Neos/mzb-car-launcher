package org.apache.log4j.lf5.viewer.configure;

import java.awt.*;
import javax.swing.tree.*;
import javax.swing.*;
import javax.xml.parsers.*;
import org.apache.log4j.lf5.viewer.categoryexplorer.*;
import org.apache.log4j.lf5.*;
import org.w3c.dom.*;
import java.util.*;
import org.apache.log4j.lf5.viewer.*;
import java.io.*;

public class ConfigurationManager
{
    private static final String BLUE = "blue";
    private static final String CATEGORY = "category";
    private static final String COLORLEVEL = "colorlevel";
    private static final String COLUMN = "column";
    private static final String CONFIG_FILE_NAME = "lf5_configuration.xml";
    private static final String EXPANDED = "expanded";
    private static final String FIRST_CATEGORY_NAME = "Categories";
    private static final String GREEN = "green";
    private static final String LEVEL = "level";
    private static final String NAME = "name";
    private static final String NDCTEXTFILTER = "searchtext";
    private static final String PATH = "path";
    private static final String RED = "red";
    private static final String SELECTED = "selected";
    private LogBrokerMonitor _monitor;
    private LogTable _table;
    
    public ConfigurationManager(final LogBrokerMonitor monitor, final LogTable table) {
        this._monitor = null;
        this._table = null;
        this._monitor = monitor;
        this._table = table;
        this.load();
    }
    
    private void closeConfigurationXML(final StringBuffer sb) {
        sb.append("</configuration>\r\n");
    }
    
    private void exportLogLevelColorXMLElement(final String s, final Color color, final StringBuffer sb) {
        sb.append("\t\t<").append("colorlevel").append(" ").append("name");
        sb.append("=\"").append(s).append("\" ");
        sb.append("red").append("=\"").append(color.getRed()).append("\" ");
        sb.append("green").append("=\"").append(color.getGreen()).append("\" ");
        sb.append("blue").append("=\"").append(color.getBlue());
        sb.append("\"/>\r\n");
    }
    
    private void exportLogLevelXMLElement(final String s, final boolean b, final StringBuffer sb) {
        sb.append("\t\t<").append("level").append(" ").append("name");
        sb.append("=\"").append(s).append("\" ");
        sb.append("selected").append("=\"").append(b);
        sb.append("\"/>\r\n");
    }
    
    private void exportLogTableColumnXMLElement(final String s, final boolean b, final StringBuffer sb) {
        sb.append("\t\t<").append("column").append(" ").append("name");
        sb.append("=\"").append(s).append("\" ");
        sb.append("selected").append("=\"").append(b);
        sb.append("\"/>\r\n");
    }
    
    private void exportXMLElement(final CategoryNode categoryNode, final TreePath treePath, final StringBuffer sb) {
        final CategoryExplorerTree categoryExplorerTree = this._monitor.getCategoryExplorerTree();
        sb.append("\t<").append("category").append(" ");
        sb.append("name").append("=\"").append(categoryNode.getTitle()).append("\" ");
        sb.append("path").append("=\"").append(treePathToString(treePath)).append("\" ");
        sb.append("expanded").append("=\"").append(categoryExplorerTree.isExpanded(treePath)).append("\" ");
        sb.append("selected").append("=\"").append(categoryNode.isSelected()).append("\"/>\r\n");
    }
    
    private void openConfigurationXML(final StringBuffer sb) {
        sb.append("<configuration>\r\n");
    }
    
    private void openXMLDocument(final StringBuffer sb) {
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n");
    }
    
    private void processConfigurationNode(final CategoryNode categoryNode, final StringBuffer sb) {
        final CategoryExplorerModel explorerModel = this._monitor.getCategoryExplorerTree().getExplorerModel();
        final Enumeration breadthFirstEnumeration = categoryNode.breadthFirstEnumeration();
        while (breadthFirstEnumeration.hasMoreElements()) {
            final CategoryNode categoryNode2 = breadthFirstEnumeration.nextElement();
            this.exportXMLElement(categoryNode2, explorerModel.getTreePathToRoot(categoryNode2), sb);
        }
    }
    
    private void processLogLevelColors(final Map map, final Map map2, final StringBuffer sb) {
        sb.append("\t<loglevelcolors>\r\n");
        for (final LogLevel logLevel : map.keySet()) {
            this.exportLogLevelColorXMLElement(logLevel.getLabel(), map2.get(logLevel), sb);
        }
        sb.append("\t</loglevelcolors>\r\n");
    }
    
    private void processLogLevels(final Map map, final StringBuffer sb) {
        sb.append("\t<loglevels>\r\n");
        for (final LogLevel logLevel : map.keySet()) {
            this.exportLogLevelXMLElement(logLevel.getLabel(), ((JCheckBoxMenuItem)map.get(logLevel)).isSelected(), sb);
        }
        sb.append("\t</loglevels>\r\n");
    }
    
    private void processLogRecordFilter(final String s, final StringBuffer sb) {
        sb.append("\t<").append("searchtext").append(" ");
        sb.append("name").append("=\"").append(s).append("\"");
        sb.append("/>\r\n");
    }
    
    private void processLogTableColumns(final List list, final StringBuffer sb) {
        sb.append("\t<logtablecolumns>\r\n");
        for (final LogTableColumn logTableColumn : list) {
            this.exportLogTableColumnXMLElement(logTableColumn.getLabel(), this._monitor.getTableColumnMenuItem(logTableColumn).isSelected(), sb);
        }
        sb.append("\t</logtablecolumns>\r\n");
    }
    
    public static String treePathToString(final TreePath treePath) {
        final StringBuffer sb = new StringBuffer();
        final Object[] path = treePath.getPath();
        for (int i = 1; i < path.length; ++i) {
            final CategoryNode categoryNode = (CategoryNode)path[i];
            if (i > 1) {
                sb.append(".");
            }
            sb.append(categoryNode.getTitle());
        }
        return sb.toString();
    }
    
    protected void collapseTree() {
        final CategoryExplorerTree categoryExplorerTree = this._monitor.getCategoryExplorerTree();
        for (int i = categoryExplorerTree.getRowCount() - 1; i > 0; --i) {
            categoryExplorerTree.collapseRow(i);
        }
    }
    
    protected void deleteConfigurationFile() {
        try {
            final File file = new File(this.getFilename());
            if (file.exists()) {
                file.delete();
            }
        }
        catch (SecurityException ex) {
            System.err.println(new StringBuffer().append("Cannot delete ").append(this.getFilename()).append(" because a security violation occured.").toString());
        }
    }
    
    protected String getFilename() {
        final String property = System.getProperty("user.home");
        final String property2 = System.getProperty("file.separator");
        return new StringBuffer().append(property).append(property2).append("lf5").append(property2).append("lf5_configuration.xml").toString();
    }
    
    protected String getValue(final NamedNodeMap namedNodeMap, final String s) {
        return namedNodeMap.getNamedItem(s).getNodeValue();
    }
    
    protected void load() {
        final File file = new File(this.getFilename());
        if (!file.exists()) {
            return;
        }
        try {
            final Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
            this.processRecordFilter(parse);
            this.processCategories(parse);
            this.processLogLevels(parse);
            this.processLogLevelColors(parse);
            this.processLogTableColumns(parse);
        }
        catch (Exception ex) {
            System.err.println(new StringBuffer().append("Unable process configuration file at ").append(this.getFilename()).append(". Error Message=").append(ex.getMessage()).toString());
        }
    }
    
    protected void processCategories(final Document document) {
        final CategoryExplorerTree categoryExplorerTree = this._monitor.getCategoryExplorerTree();
        final CategoryExplorerModel explorerModel = categoryExplorerTree.getExplorerModel();
        final NodeList elementsByTagName = document.getElementsByTagName("category");
        int n;
        if (this.getValue(elementsByTagName.item(0).getAttributes(), "name").equalsIgnoreCase("Categories")) {
            n = 1;
        }
        else {
            n = 0;
        }
        for (int i = elementsByTagName.getLength() - 1; i >= n; --i) {
            final NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
            final CategoryNode addCategory = explorerModel.addCategory(new CategoryPath(this.getValue(attributes, "path")));
            addCategory.setSelected(this.getValue(attributes, "selected").equalsIgnoreCase("true"));
            if (this.getValue(attributes, "expanded").equalsIgnoreCase("true")) {}
            categoryExplorerTree.expandPath(explorerModel.getTreePathToRoot(addCategory));
        }
    }
    
    protected void processLogLevelColors(Document elementsByTagName) {
        elementsByTagName = (Document)elementsByTagName.getElementsByTagName("colorlevel");
        LogLevel.getLogLevelColorMap();
        int i = 0;
        while (i < ((NodeList)elementsByTagName).getLength()) {
            final Node item = ((NodeList)elementsByTagName).item(i);
            if (item == null) {
                break;
            }
            final NamedNodeMap attributes = item.getAttributes();
            final String value = this.getValue(attributes, "name");
            while (true) {
                try {
                    final LogLevel value2 = LogLevel.valueOf(value);
                    final Color color = new Color(Integer.parseInt(this.getValue(attributes, "red")), Integer.parseInt(this.getValue(attributes, "green")), Integer.parseInt(this.getValue(attributes, "blue")));
                    if (value2 != null) {
                        value2.setLogLevelColorMap(value2, color);
                    }
                    ++i;
                }
                catch (LogLevelFormatException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    protected void processLogLevels(Document elementsByTagName) {
        elementsByTagName = (Document)elementsByTagName.getElementsByTagName("level");
        final Map logLevelMenuItems = this._monitor.getLogLevelMenuItems();
        int n = 0;
    Label_0084_Outer:
        while (true) {
            if (n >= ((NodeList)elementsByTagName).getLength()) {
                return;
            }
            final NamedNodeMap attributes = ((NodeList)elementsByTagName).item(n).getAttributes();
            final String value = this.getValue(attributes, "name");
            while (true) {
                try {
                    logLevelMenuItems.get(LogLevel.valueOf(value)).setSelected(this.getValue(attributes, "selected").equalsIgnoreCase("true"));
                    ++n;
                    continue Label_0084_Outer;
                }
                catch (LogLevelFormatException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    protected void processLogTableColumns(Document elementsByTagName) {
        elementsByTagName = (Document)elementsByTagName.getElementsByTagName("column");
        final Map logTableColumnMenuItems = this._monitor.getLogTableColumnMenuItems();
        final ArrayList<LogTableColumn> view = new ArrayList<LogTableColumn>();
        int i = 0;
        while (i < ((NodeList)elementsByTagName).getLength()) {
            final Node item = ((NodeList)elementsByTagName).item(i);
            if (item == null) {
                break;
            }
            final NamedNodeMap attributes = item.getAttributes();
            final String value = this.getValue(attributes, "name");
            while (true) {
                try {
                    final LogTableColumn value2 = LogTableColumn.valueOf(value);
                    final JCheckBoxMenuItem checkBoxMenuItem = logTableColumnMenuItems.get(value2);
                    checkBoxMenuItem.setSelected(this.getValue(attributes, "selected").equalsIgnoreCase("true"));
                    if (checkBoxMenuItem.isSelected()) {
                        view.add(value2);
                    }
                    if (view.isEmpty()) {
                        this._table.setDetailedView();
                    }
                    else {
                        this._table.setView(view);
                    }
                    ++i;
                }
                catch (LogTableColumnFormatException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    protected void processRecordFilter(final Document document) {
        final Node item = document.getElementsByTagName("searchtext").item(0);
        if (item != null) {
            final String value = this.getValue(item.getAttributes(), "name");
            if (value != null && !value.equals("")) {
                this._monitor.setNDCLogRecordFilter(value);
            }
        }
    }
    
    public void reset() {
        this.deleteConfigurationFile();
        this.collapseTree();
        this.selectAllNodes();
    }
    
    public void save() {
        final CategoryNode rootCategoryNode = this._monitor.getCategoryExplorerTree().getExplorerModel().getRootCategoryNode();
        final StringBuffer sb = new StringBuffer(2048);
        this.openXMLDocument(sb);
        this.openConfigurationXML(sb);
        this.processLogRecordFilter(this._monitor.getNDCTextFilter(), sb);
        this.processLogLevels(this._monitor.getLogLevelMenuItems(), sb);
        this.processLogLevelColors(this._monitor.getLogLevelMenuItems(), LogLevel.getLogLevelColorMap(), sb);
        this.processLogTableColumns(LogTableColumn.getLogTableColumns(), sb);
        this.processConfigurationNode(rootCategoryNode, sb);
        this.closeConfigurationXML(sb);
        this.store(sb.toString());
    }
    
    protected void selectAllNodes() {
        final Enumeration breadthFirstEnumeration = this._monitor.getCategoryExplorerTree().getExplorerModel().getRootCategoryNode().breadthFirstEnumeration();
        while (breadthFirstEnumeration.hasMoreElements()) {
            breadthFirstEnumeration.nextElement().setSelected(true);
        }
    }
    
    protected void store(final String s) {
        try {
            final PrintWriter printWriter = new PrintWriter(new FileWriter(this.getFilename()));
            printWriter.print(s);
            printWriter.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
