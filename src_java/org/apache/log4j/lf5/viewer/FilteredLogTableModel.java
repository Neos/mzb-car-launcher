package org.apache.log4j.lf5.viewer;

import javax.swing.table.*;
import org.apache.log4j.lf5.*;
import java.util.*;

public class FilteredLogTableModel extends AbstractTableModel
{
    protected List _allRecords;
    protected String[] _colNames;
    protected LogRecordFilter _filter;
    protected List _filteredRecords;
    protected int _maxNumberOfLogRecords;
    
    public FilteredLogTableModel() {
        this._filter = new PassingLogRecordFilter();
        this._allRecords = new ArrayList();
        this._maxNumberOfLogRecords = 5000;
        this._colNames = new String[] { "Date", "Thread", "Message #", "Level", "NDC", "Category", "Message", "Location", "Thrown" };
    }
    
    private int numberOfRecordsToTrim() {
        return this._allRecords.size() - this._maxNumberOfLogRecords;
    }
    
    public boolean addLogRecord(final LogRecord logRecord) {
        synchronized (this) {
            this._allRecords.add(logRecord);
            boolean b;
            if (!this._filter.passes(logRecord)) {
                b = false;
            }
            else {
                this.getFilteredRecords().add(logRecord);
                this.fireTableRowsInserted(this.getRowCount(), this.getRowCount());
                this.trimRecords();
                b = true;
            }
            return b;
        }
    }
    
    public void clear() {
        synchronized (this) {
            this._allRecords.clear();
            this._filteredRecords.clear();
            this.fireTableDataChanged();
        }
    }
    
    protected List createFilteredRecordsList() {
        final ArrayList<LogRecord> list = new ArrayList<LogRecord>();
        for (final LogRecord logRecord : this._allRecords) {
            if (this._filter.passes(logRecord)) {
                list.add(logRecord);
            }
        }
        return list;
    }
    
    public void fastRefresh() {
        synchronized (this) {
            this._filteredRecords.remove(0);
            this.fireTableRowsDeleted(0, 0);
        }
    }
    
    protected Object getColumn(final int n, final LogRecord logRecord) {
        if (logRecord == null) {
            return "NULL Column";
        }
        final String string = new Date(logRecord.getMillis()).toString();
        switch (n) {
            default: {
                throw new IllegalArgumentException(new StringBuffer().append("The column number ").append(n).append("must be between 0 and 8").toString());
            }
            case 0: {
                return new StringBuffer().append(string).append(" (").append(logRecord.getMillis()).append(")").toString();
            }
            case 1: {
                return logRecord.getThreadDescription();
            }
            case 2: {
                return new Long(logRecord.getSequenceNumber());
            }
            case 3: {
                return logRecord.getLevel();
            }
            case 4: {
                return logRecord.getNDC();
            }
            case 5: {
                return logRecord.getCategory();
            }
            case 6: {
                return logRecord.getMessage();
            }
            case 7: {
                return logRecord.getLocation();
            }
            case 8: {
                return logRecord.getThrownStackTrace();
            }
        }
    }
    
    @Override
    public int getColumnCount() {
        return this._colNames.length;
    }
    
    @Override
    public String getColumnName(final int n) {
        return this._colNames[n];
    }
    
    protected LogRecord getFilteredRecord(final int n) {
        final List filteredRecords = this.getFilteredRecords();
        final int size = filteredRecords.size();
        if (n < size) {
            return filteredRecords.get(n);
        }
        return filteredRecords.get(size - 1);
    }
    
    protected List getFilteredRecords() {
        if (this._filteredRecords == null) {
            this.refresh();
        }
        return this._filteredRecords;
    }
    
    public LogRecordFilter getLogRecordFilter() {
        return this._filter;
    }
    
    @Override
    public int getRowCount() {
        return this.getFilteredRecords().size();
    }
    
    public int getTotalRowCount() {
        return this._allRecords.size();
    }
    
    @Override
    public Object getValueAt(final int n, final int n2) {
        return this.getColumn(n2, this.getFilteredRecord(n));
    }
    
    protected boolean needsTrimming() {
        return this._allRecords.size() > this._maxNumberOfLogRecords;
    }
    
    public void refresh() {
        synchronized (this) {
            this._filteredRecords = this.createFilteredRecordsList();
            this.fireTableDataChanged();
        }
    }
    
    public void setLogRecordFilter(final LogRecordFilter filter) {
        this._filter = filter;
    }
    
    public void setMaxNumberOfLogRecords(final int maxNumberOfLogRecords) {
        if (maxNumberOfLogRecords > 0) {
            this._maxNumberOfLogRecords = maxNumberOfLogRecords;
        }
    }
    
    protected void trimOldestRecords() {
        synchronized (this._allRecords) {
            final int numberOfRecordsToTrim = this.numberOfRecordsToTrim();
            if (numberOfRecordsToTrim > 1) {
                this._allRecords.subList(0, numberOfRecordsToTrim).clear();
                this.refresh();
            }
            else {
                this._allRecords.remove(0);
                this.fastRefresh();
            }
        }
    }
    
    protected void trimRecords() {
        if (this.needsTrimming()) {
            this.trimOldestRecords();
        }
    }
}
