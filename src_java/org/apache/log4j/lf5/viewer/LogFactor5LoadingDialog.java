package org.apache.log4j.lf5.viewer;

import javax.swing.*;
import java.awt.*;

public class LogFactor5LoadingDialog extends LogFactor5Dialog
{
    public LogFactor5LoadingDialog(final JFrame frame, final String s) {
        super(frame, "LogFactor5", false);
        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        this.wrapStringOnPanel(s, panel2);
        this.getContentPane().add(panel2, "Center");
        this.getContentPane().add(panel, "South");
        this.show();
    }
}
