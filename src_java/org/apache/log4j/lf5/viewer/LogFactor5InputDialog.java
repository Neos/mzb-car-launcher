package org.apache.log4j.lf5.viewer;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class LogFactor5InputDialog extends LogFactor5Dialog
{
    public static final int SIZE = 30;
    private JTextField _textField;
    
    public LogFactor5InputDialog(final JFrame frame, final String s, final String s2) {
        this(frame, s, s2, 30);
    }
    
    public LogFactor5InputDialog(final JFrame frame, final String s, final String s2, final int n) {
        super(frame, s, true);
        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout());
        panel2.add(new JLabel(s2));
        panel2.add(this._textField = new JTextField(n));
        this.addKeyListener(new LogFactor5InputDialog$1(this));
        final JButton button = new JButton("Ok");
        button.addActionListener(new LogFactor5InputDialog$2(this));
        final JButton button2 = new JButton("Cancel");
        button2.addActionListener(new LogFactor5InputDialog$3(this));
        panel.add(button);
        panel.add(button2);
        this.getContentPane().add(panel2, "Center");
        this.getContentPane().add(panel, "South");
        this.pack();
        this.centerWindow(this);
        this.show();
    }
    
    static JTextField access$000(final LogFactor5InputDialog logFactor5InputDialog) {
        return logFactor5InputDialog._textField;
    }
    
    public String getText() {
        String text;
        final String s = text = this._textField.getText();
        if (s != null) {
            text = s;
            if (s.trim().length() == 0) {
                text = null;
            }
        }
        return text;
    }
}
