package org.apache.log4j.lf5.viewer.categoryexplorer;

import java.awt.event.*;
import java.util.*;
import java.awt.*;
import javax.swing.tree.*;
import javax.swing.*;

public class CategoryNodeEditor extends CategoryAbstractCellEditor
{
    protected CategoryExplorerModel _categoryModel;
    protected JCheckBox _checkBox;
    protected CategoryNode _lastEditedNode;
    protected CategoryNodeEditorRenderer _renderer;
    protected JTree _tree;
    
    public CategoryNodeEditor(final CategoryExplorerModel categoryModel) {
        this._renderer = new CategoryNodeEditorRenderer();
        this._checkBox = this._renderer.getCheckBox();
        this._categoryModel = categoryModel;
        this._checkBox.addActionListener(new CategoryNodeEditor$1(this));
        this._renderer.addMouseListener(new CategoryNodeEditor$2(this));
    }
    
    protected void collapse(final CategoryNode categoryNode) {
        this._tree.collapsePath(this.getTreePath(categoryNode));
    }
    
    protected void collapseDescendants(final CategoryNode categoryNode) {
        final Enumeration depthFirstEnumeration = categoryNode.depthFirstEnumeration();
        while (depthFirstEnumeration.hasMoreElements()) {
            this.collapse(depthFirstEnumeration.nextElement());
        }
    }
    
    protected JMenuItem createCollapseMenuItem(final CategoryNode categoryNode) {
        final JMenuItem menuItem = new JMenuItem("Collapse All Descendant Categories");
        menuItem.addActionListener(new CategoryNodeEditor$7(this, categoryNode));
        return menuItem;
    }
    
    protected JMenuItem createExpandMenuItem(final CategoryNode categoryNode) {
        final JMenuItem menuItem = new JMenuItem("Expand All Descendant Categories");
        menuItem.addActionListener(new CategoryNodeEditor$6(this, categoryNode));
        return menuItem;
    }
    
    protected JMenuItem createPropertiesMenuItem(final CategoryNode categoryNode) {
        final JMenuItem menuItem = new JMenuItem("Properties");
        menuItem.addActionListener(new CategoryNodeEditor$3(this, categoryNode));
        return menuItem;
    }
    
    protected JMenuItem createRemoveMenuItem() {
        final JMenuItem menuItem = new JMenuItem("Remove All Empty Categories");
        menuItem.addActionListener(new CategoryNodeEditor$8(this));
        return menuItem;
    }
    
    protected JMenuItem createSelectDescendantsMenuItem(final CategoryNode categoryNode) {
        final JMenuItem menuItem = new JMenuItem("Select All Descendant Categories");
        menuItem.addActionListener(new CategoryNodeEditor$4(this, categoryNode));
        return menuItem;
    }
    
    protected JMenuItem createUnselectDescendantsMenuItem(final CategoryNode categoryNode) {
        final JMenuItem menuItem = new JMenuItem("Deselect All Descendant Categories");
        menuItem.addActionListener(new CategoryNodeEditor$5(this, categoryNode));
        return menuItem;
    }
    
    protected void expand(final CategoryNode categoryNode) {
        this._tree.expandPath(this.getTreePath(categoryNode));
    }
    
    protected void expandDescendants(final CategoryNode categoryNode) {
        final Enumeration depthFirstEnumeration = categoryNode.depthFirstEnumeration();
        while (depthFirstEnumeration.hasMoreElements()) {
            this.expand(depthFirstEnumeration.nextElement());
        }
    }
    
    @Override
    public Object getCellEditorValue() {
        return this._lastEditedNode.getUserObject();
    }
    
    protected Object getDisplayedProperties(final CategoryNode categoryNode) {
        final ArrayList<String> list = new ArrayList<String>();
        list.add(new StringBuffer().append("Category: ").append(categoryNode.getTitle()).toString());
        if (categoryNode.hasFatalRecords()) {
            list.add("Contains at least one fatal LogRecord.");
        }
        if (categoryNode.hasFatalChildren()) {
            list.add("Contains descendants with a fatal LogRecord.");
        }
        list.add(new StringBuffer().append("LogRecords in this category alone: ").append(categoryNode.getNumberOfContainedRecords()).toString());
        list.add(new StringBuffer().append("LogRecords in descendant categories: ").append(categoryNode.getNumberOfRecordsFromChildren()).toString());
        list.add(new StringBuffer().append("LogRecords in this category including descendants: ").append(categoryNode.getTotalNumberOfRecords()).toString());
        return list.toArray();
    }
    
    @Override
    public Component getTreeCellEditorComponent(final JTree tree, final Object o, final boolean b, final boolean b2, final boolean b3, final int n) {
        this._lastEditedNode = (CategoryNode)o;
        this._tree = tree;
        return this._renderer.getTreeCellRendererComponent(tree, o, b, b2, b3, n, true);
    }
    
    protected TreePath getTreePath(final CategoryNode categoryNode) {
        return new TreePath(categoryNode.getPath());
    }
    
    protected int removeUnusedNodes() {
        int n = 0;
        final Enumeration depthFirstEnumeration = this._categoryModel.getRootCategoryNode().depthFirstEnumeration();
        while (depthFirstEnumeration.hasMoreElements()) {
            final CategoryNode categoryNode = depthFirstEnumeration.nextElement();
            if (categoryNode.isLeaf() && categoryNode.getNumberOfContainedRecords() == 0 && categoryNode.getParent() != null) {
                this._categoryModel.removeNodeFromParent(categoryNode);
                ++n;
            }
        }
        return n;
    }
    
    protected void showPopup(final CategoryNode categoryNode, final int n, final int n2) {
        final JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.setSize(150, 400);
        if (categoryNode.getParent() == null) {
            popupMenu.add(this.createRemoveMenuItem());
            popupMenu.addSeparator();
        }
        popupMenu.add(this.createSelectDescendantsMenuItem(categoryNode));
        popupMenu.add(this.createUnselectDescendantsMenuItem(categoryNode));
        popupMenu.addSeparator();
        popupMenu.add(this.createExpandMenuItem(categoryNode));
        popupMenu.add(this.createCollapseMenuItem(categoryNode));
        popupMenu.addSeparator();
        popupMenu.add(this.createPropertiesMenuItem(categoryNode));
        popupMenu.show(this._renderer, n, n2);
    }
    
    protected void showPropertiesDialog(final CategoryNode categoryNode) {
        JOptionPane.showMessageDialog(this._tree, this.getDisplayedProperties(categoryNode), new StringBuffer().append("Category Properties: ").append(categoryNode.getTitle()).toString(), -1);
    }
}
