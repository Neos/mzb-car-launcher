package org.apache.log4j.lf5.viewer.categoryexplorer;

import java.awt.event.*;
import java.awt.*;
import java.util.*;
import org.apache.log4j.lf5.*;
import javax.swing.tree.*;
import javax.swing.*;

public class CategoryExplorerModel extends DefaultTreeModel
{
    private static final long serialVersionUID = -3413887384316015901L;
    protected ActionEvent _event;
    protected ActionListener _listener;
    protected boolean _renderFatal;
    
    public CategoryExplorerModel(final CategoryNode categoryNode) {
        super(categoryNode);
        this._renderFatal = true;
        this._listener = null;
        this._event = new ActionEvent(this, 1001, "Nodes Selection changed");
    }
    
    public void addActionListener(final ActionListener actionListener) {
        synchronized (this) {
            this._listener = AWTEventMulticaster.add(this._listener, actionListener);
        }
    }
    
    public CategoryNode addCategory(final CategoryPath categoryPath) {
        CategoryNode categoryNode = (CategoryNode)this.getRoot();
        int i = 0;
    Label_0011:
        while (i < categoryPath.size()) {
            final CategoryElement categoryElement = categoryPath.categoryElementAt(i);
            final Enumeration children = categoryNode.children();
            final boolean b = false;
            while (true) {
                CategoryNode categoryNode2;
                do {
                    final boolean b2 = b;
                    categoryNode2 = categoryNode;
                    if (!children.hasMoreElements()) {
                        categoryNode = categoryNode2;
                        if (!b2) {
                            categoryNode = new CategoryNode(categoryElement.getTitle());
                            this.insertNodeInto(categoryNode, categoryNode2, categoryNode2.getChildCount());
                            this.refresh(categoryNode);
                        }
                        ++i;
                        continue Label_0011;
                    }
                    categoryNode2 = children.nextElement();
                } while (!categoryNode2.getTitle().toLowerCase().equals(categoryElement.getTitle().toLowerCase()));
                final boolean b2 = true;
                continue;
            }
        }
        return categoryNode;
    }
    
    public void addLogRecord(final LogRecord logRecord) {
        final CategoryPath categoryPath = new CategoryPath(logRecord.getCategory());
        this.addCategory(categoryPath);
        final CategoryNode categoryNode = this.getCategoryNode(categoryPath);
        categoryNode.addRecord();
        if (this._renderFatal && logRecord.isFatal()) {
            final TreeNode[] pathToRoot = this.getPathToRoot(categoryNode);
            for (int length = pathToRoot.length, i = 1; i < length - 1; ++i) {
                final CategoryNode categoryNode2 = (CategoryNode)pathToRoot[i];
                categoryNode2.setHasFatalChildren(true);
                this.nodeChanged(categoryNode2);
            }
            categoryNode.setHasFatalRecords(true);
            this.nodeChanged(categoryNode);
        }
    }
    
    public CategoryNode getCategoryNode(final String s) {
        return this.getCategoryNode(new CategoryPath(s));
    }
    
    public CategoryNode getCategoryNode(final CategoryPath categoryPath) {
        CategoryNode categoryNode = (CategoryNode)this.getRoot();
        int i = 0;
    Label_0011:
        while (i < categoryPath.size()) {
            final CategoryElement categoryElement = categoryPath.categoryElementAt(i);
            final Enumeration children = categoryNode.children();
            final boolean b = false;
            while (true) {
                CategoryNode categoryNode2;
                do {
                    final boolean b2 = b;
                    categoryNode2 = categoryNode;
                    if (children.hasMoreElements()) {
                        categoryNode2 = children.nextElement();
                    }
                    else {
                        if (!b2) {
                            return null;
                        }
                        ++i;
                        categoryNode = categoryNode2;
                        continue Label_0011;
                    }
                } while (!categoryNode2.getTitle().toLowerCase().equals(categoryElement.getTitle().toLowerCase()));
                final boolean b2 = true;
                continue;
            }
        }
        return categoryNode;
    }
    
    public CategoryNode getRootCategoryNode() {
        return (CategoryNode)this.getRoot();
    }
    
    public TreePath getTreePathToRoot(final CategoryNode categoryNode) {
        if (categoryNode == null) {
            return null;
        }
        return new TreePath(this.getPathToRoot(categoryNode));
    }
    
    public boolean isCategoryPathActive(final CategoryPath categoryPath) {
        CategoryNode categoryNode = (CategoryNode)this.getRoot();
        boolean b = false;
        int i = 0;
    Label_0014:
        while (i < categoryPath.size()) {
            final CategoryElement categoryElement = categoryPath.categoryElementAt(i);
            final Enumeration children = categoryNode.children();
            final boolean b2 = false;
            final boolean b3 = false;
            while (true) {
                CategoryNode categoryNode2;
                do {
                    b = b3;
                    final boolean b4 = b2;
                    final CategoryNode categoryNode3 = categoryNode;
                    if (children.hasMoreElements()) {
                        categoryNode2 = children.nextElement();
                    }
                    else {
                        if (!b || !b4) {
                            return false;
                        }
                        ++i;
                        categoryNode = categoryNode3;
                        continue Label_0014;
                    }
                } while (!categoryNode2.getTitle().toLowerCase().equals(categoryElement.getTitle().toLowerCase()));
                final boolean b5 = true;
                final CategoryNode categoryNode4 = categoryNode2;
                b = b3;
                boolean b4 = b5;
                CategoryNode categoryNode3 = categoryNode4;
                if (categoryNode4.isSelected()) {
                    b = true;
                    categoryNode3 = categoryNode4;
                    b4 = b5;
                }
                continue;
            }
        }
        return b;
    }
    
    protected void notifyActionListeners() {
        if (this._listener != null) {
            this._listener.actionPerformed(this._event);
        }
    }
    
    protected void refresh(final CategoryNode categoryNode) {
        SwingUtilities.invokeLater(new CategoryExplorerModel$1(this, categoryNode));
    }
    
    public void removeActionListener(final ActionListener actionListener) {
        synchronized (this) {
            this._listener = AWTEventMulticaster.remove(this._listener, actionListener);
        }
    }
    
    public void resetAllNodeCounts() {
        final Enumeration depthFirstEnumeration = this.getRootCategoryNode().depthFirstEnumeration();
        while (depthFirstEnumeration.hasMoreElements()) {
            final CategoryNode categoryNode = depthFirstEnumeration.nextElement();
            categoryNode.resetNumberOfContainedRecords();
            this.nodeChanged(categoryNode);
        }
    }
    
    public void setDescendantSelection(final CategoryNode categoryNode, final boolean selected) {
        final Enumeration depthFirstEnumeration = categoryNode.depthFirstEnumeration();
        while (depthFirstEnumeration.hasMoreElements()) {
            final CategoryNode categoryNode2 = depthFirstEnumeration.nextElement();
            if (categoryNode2.isSelected() != selected) {
                categoryNode2.setSelected(selected);
                this.nodeChanged(categoryNode2);
            }
        }
        this.notifyActionListeners();
    }
    
    public void setParentSelection(final CategoryNode categoryNode, final boolean selected) {
        final TreeNode[] pathToRoot = this.getPathToRoot(categoryNode);
        for (int length = pathToRoot.length, i = 1; i < length; ++i) {
            final CategoryNode categoryNode2 = (CategoryNode)pathToRoot[i];
            if (categoryNode2.isSelected() != selected) {
                categoryNode2.setSelected(selected);
                this.nodeChanged(categoryNode2);
            }
        }
        this.notifyActionListeners();
    }
    
    public void update(final CategoryNode categoryNode, final boolean b) {
        if (categoryNode.isSelected() == b) {
            return;
        }
        if (b) {
            this.setParentSelection(categoryNode, true);
            return;
        }
        this.setDescendantSelection(categoryNode, false);
    }
}
