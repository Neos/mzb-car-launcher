package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.*;
import java.awt.*;

public class CategoryNodeEditorRenderer extends CategoryNodeRenderer
{
    private static final long serialVersionUID = -6094804684259929574L;
    
    public JCheckBox getCheckBox() {
        return this._checkBox;
    }
    
    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object o, final boolean b, final boolean b2, final boolean b3, final int n, final boolean b4) {
        return super.getTreeCellRendererComponent(tree, o, b, b2, b3, n, b4);
    }
}
