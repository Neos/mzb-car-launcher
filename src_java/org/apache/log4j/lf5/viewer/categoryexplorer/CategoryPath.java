package org.apache.log4j.lf5.viewer.categoryexplorer;

import java.util.*;

public class CategoryPath
{
    protected LinkedList _categoryElements;
    
    public CategoryPath() {
        this._categoryElements = new LinkedList();
    }
    
    public CategoryPath(final String s) {
        this._categoryElements = new LinkedList();
        String s2 = s;
        if (s == null) {
            s2 = "Debug";
        }
        final StringTokenizer stringTokenizer = new StringTokenizer(s2.replace('/', '.').replace('\\', '.'), ".");
        while (stringTokenizer.hasMoreTokens()) {
            this.addCategoryElement(new CategoryElement(stringTokenizer.nextToken()));
        }
    }
    
    public void addCategoryElement(final CategoryElement categoryElement) {
        this._categoryElements.addLast(categoryElement);
    }
    
    public CategoryElement categoryElementAt(final int n) {
        return this._categoryElements.get(n);
    }
    
    public boolean isEmpty() {
        boolean b = false;
        if (this._categoryElements.size() == 0) {
            b = true;
        }
        return b;
    }
    
    public void removeAllCategoryElements() {
        this._categoryElements.clear();
    }
    
    public int size() {
        return this._categoryElements.size();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer(100);
        sb.append("\n");
        sb.append("===========================\n");
        sb.append("CategoryPath:                   \n");
        sb.append("---------------------------\n");
        sb.append("\nCategoryPath:\n\t");
        if (this.size() > 0) {
            for (int i = 0; i < this.size(); ++i) {
                sb.append(this.categoryElementAt(i).toString());
                sb.append("\n\t");
            }
        }
        else {
            sb.append("<<NONE>>");
        }
        sb.append("\n");
        sb.append("===========================\n");
        return sb.toString();
    }
}
