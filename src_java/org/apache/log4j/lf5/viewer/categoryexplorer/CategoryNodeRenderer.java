package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.tree.*;
import java.awt.*;
import javax.swing.*;

public class CategoryNodeRenderer extends DefaultTreeCellRenderer
{
    public static final Color FATAL_CHILDREN;
    protected static ImageIcon _sat;
    private static final long serialVersionUID = -6046702673278595048L;
    protected JCheckBox _checkBox;
    protected JPanel _panel;
    
    static {
        FATAL_CHILDREN = new Color(189, 113, 0);
        CategoryNodeRenderer._sat = null;
    }
    
    public CategoryNodeRenderer() {
        this._checkBox = new JCheckBox();
        (this._panel = new JPanel()).setBackground(UIManager.getColor("Tree.textBackground"));
        if (CategoryNodeRenderer._sat == null) {
            CategoryNodeRenderer._sat = new ImageIcon(this.getClass().getResource("/org/apache/log4j/lf5/viewer/images/channelexplorer_satellite.gif"));
        }
        this.setOpaque(false);
        this._checkBox.setOpaque(false);
        this._panel.setOpaque(false);
        this._panel.setLayout(new FlowLayout(0, 0, 0));
        this._panel.add(this._checkBox);
        this._panel.add(this);
        this.setOpenIcon(CategoryNodeRenderer._sat);
        this.setClosedIcon(CategoryNodeRenderer._sat);
        this.setLeafIcon(CategoryNodeRenderer._sat);
    }
    
    protected String buildToolTip(final CategoryNode categoryNode) {
        final StringBuffer sb = new StringBuffer();
        sb.append(categoryNode.getTitle()).append(" contains a total of ");
        sb.append(categoryNode.getTotalNumberOfRecords());
        sb.append(" LogRecords.");
        sb.append(" Right-click for more info.");
        return sb.toString();
    }
    
    public Dimension getCheckBoxOffset() {
        return new Dimension(0, 0);
    }
    
    @Override
    public Component getTreeCellRendererComponent(final JTree tree, final Object o, final boolean b, final boolean b2, final boolean b3, final int n, final boolean b4) {
        final CategoryNode categoryNode = (CategoryNode)o;
        super.getTreeCellRendererComponent(tree, o, b, b2, b3, n, b4);
        if (n == 0) {
            this._checkBox.setVisible(false);
        }
        else {
            this._checkBox.setVisible(true);
            this._checkBox.setSelected(categoryNode.isSelected());
        }
        this._panel.setToolTipText(this.buildToolTip(categoryNode));
        if (categoryNode.hasFatalChildren()) {
            this.setForeground(CategoryNodeRenderer.FATAL_CHILDREN);
        }
        if (categoryNode.hasFatalRecords()) {
            this.setForeground(Color.red);
        }
        return this._panel;
    }
}
