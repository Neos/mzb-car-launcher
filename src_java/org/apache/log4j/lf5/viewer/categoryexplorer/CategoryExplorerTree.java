package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.tree.*;

public class CategoryExplorerTree extends JTree
{
    private static final long serialVersionUID = 8066257446951323576L;
    protected CategoryExplorerModel _model;
    protected boolean _rootAlreadyExpanded;
    
    public CategoryExplorerTree() {
        this._rootAlreadyExpanded = false;
        this.setModel(this._model = new CategoryExplorerModel(new CategoryNode("Categories")));
        this.init();
    }
    
    public CategoryExplorerTree(final CategoryExplorerModel model) {
        super(model);
        this._rootAlreadyExpanded = false;
        this._model = model;
        this.init();
    }
    
    protected void ensureRootExpansion() {
        this._model.addTreeModelListener(new CategoryExplorerTree$1(this));
    }
    
    protected void expandRootNode() {
        if (this._rootAlreadyExpanded) {
            return;
        }
        this._rootAlreadyExpanded = true;
        this.expandPath(new TreePath(this._model.getRootCategoryNode().getPath()));
    }
    
    public CategoryExplorerModel getExplorerModel() {
        return this._model;
    }
    
    @Override
    public String getToolTipText(final MouseEvent mouseEvent) {
        try {
            return super.getToolTipText(mouseEvent);
        }
        catch (Exception ex) {
            return "";
        }
    }
    
    protected void init() {
        this.putClientProperty("JTree.lineStyle", "Angled");
        final CategoryNodeRenderer cellRenderer = new CategoryNodeRenderer();
        this.setEditable(true);
        this.setCellRenderer(cellRenderer);
        this.setCellEditor(new CategoryImmediateEditor(this, new CategoryNodeRenderer(), new CategoryNodeEditor(this._model)));
        this.setShowsRootHandles(true);
        this.setToolTipText("");
        this.ensureRootExpansion();
    }
}
