package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.tree.*;
import java.awt.*;

public class CategoryImmediateEditor extends DefaultTreeCellEditor
{
    protected Icon editingIcon;
    private CategoryNodeRenderer renderer;
    
    public CategoryImmediateEditor(final JTree tree, final CategoryNodeRenderer renderer, final CategoryNodeEditor categoryNodeEditor) {
        super(tree, renderer, categoryNodeEditor);
        this.editingIcon = null;
        (this.renderer = renderer).setIcon(null);
        renderer.setLeafIcon(null);
        renderer.setOpenIcon(null);
        renderer.setClosedIcon(null);
        super.editingIcon = null;
    }
    
    @Override
    protected boolean canEditImmediately(final EventObject eventObject) {
        boolean inCheckBoxHitRegion = false;
        if (eventObject instanceof MouseEvent) {
            inCheckBoxHitRegion = this.inCheckBoxHitRegion((MouseEvent)eventObject);
        }
        return inCheckBoxHitRegion;
    }
    
    @Override
    protected void determineOffset(final JTree tree, final Object o, final boolean b, final boolean b2, final boolean b3, final int n) {
        this.offset = 0;
    }
    
    public boolean inCheckBoxHitRegion(final MouseEvent mouseEvent) {
        final TreePath pathForLocation = this.tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY());
        if (pathForLocation == null) {
            return false;
        }
        final CategoryNode categoryNode = (CategoryNode)pathForLocation.getLastPathComponent();
        final Rectangle rowBounds = this.tree.getRowBounds(this.lastRow);
        final Dimension checkBoxOffset = this.renderer.getCheckBoxOffset();
        rowBounds.translate(this.offset + checkBoxOffset.width, checkBoxOffset.height);
        rowBounds.contains(mouseEvent.getPoint());
        return true;
    }
    
    @Override
    public boolean shouldSelectCell(final EventObject eventObject) {
        boolean leaf = false;
        if (eventObject instanceof MouseEvent) {
            final MouseEvent mouseEvent = (MouseEvent)eventObject;
            leaf = ((CategoryNode)this.tree.getPathForLocation(mouseEvent.getX(), mouseEvent.getY()).getLastPathComponent()).isLeaf();
        }
        return leaf;
    }
}
