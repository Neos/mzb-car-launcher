package org.apache.log4j.lf5.viewer.categoryexplorer;

import org.apache.log4j.lf5.*;
import javax.swing.tree.*;
import java.util.*;

public class CategoryExplorerLogRecordFilter implements LogRecordFilter
{
    protected CategoryExplorerModel _model;
    
    public CategoryExplorerLogRecordFilter(final CategoryExplorerModel model) {
        this._model = model;
    }
    
    @Override
    public boolean passes(final LogRecord logRecord) {
        return this._model.isCategoryPathActive(new CategoryPath(logRecord.getCategory()));
    }
    
    public void reset() {
        this.resetAllNodes();
    }
    
    protected void resetAllNodes() {
        final Enumeration depthFirstEnumeration = this._model.getRootCategoryNode().depthFirstEnumeration();
        while (depthFirstEnumeration.hasMoreElements()) {
            final CategoryNode categoryNode = depthFirstEnumeration.nextElement();
            categoryNode.resetNumberOfContainedRecords();
            this._model.nodeChanged(categoryNode);
        }
    }
}
