package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.event.*;

public class TreeModelAdapter implements TreeModelListener
{
    @Override
    public void treeNodesChanged(final TreeModelEvent treeModelEvent) {
    }
    
    @Override
    public void treeNodesInserted(final TreeModelEvent treeModelEvent) {
    }
    
    @Override
    public void treeNodesRemoved(final TreeModelEvent treeModelEvent) {
    }
    
    @Override
    public void treeStructureChanged(final TreeModelEvent treeModelEvent) {
    }
}
