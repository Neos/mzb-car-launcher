package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.tree.*;
import java.util.*;

public class CategoryNode extends DefaultMutableTreeNode
{
    private static final long serialVersionUID = 5958994817693177319L;
    protected boolean _hasFatalChildren;
    protected boolean _hasFatalRecords;
    protected int _numberOfContainedRecords;
    protected int _numberOfRecordsFromChildren;
    protected boolean _selected;
    
    public CategoryNode(final String userObject) {
        this._selected = true;
        this._numberOfContainedRecords = 0;
        this._numberOfRecordsFromChildren = 0;
        this._hasFatalChildren = false;
        this._hasFatalRecords = false;
        this.setUserObject(userObject);
    }
    
    public void addRecord() {
        ++this._numberOfContainedRecords;
        this.addRecordToParent();
    }
    
    protected void addRecordFromChild() {
        ++this._numberOfRecordsFromChildren;
        this.addRecordToParent();
    }
    
    protected void addRecordToParent() {
        final TreeNode parent = this.getParent();
        if (parent == null) {
            return;
        }
        ((CategoryNode)parent).addRecordFromChild();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CategoryNode && this.getTitle().toLowerCase().equals(((CategoryNode)o).getTitle().toLowerCase());
    }
    
    public int getNumberOfContainedRecords() {
        return this._numberOfContainedRecords;
    }
    
    protected int getNumberOfRecordsFromChildren() {
        return this._numberOfRecordsFromChildren;
    }
    
    public String getTitle() {
        return (String)this.getUserObject();
    }
    
    protected int getTotalNumberOfRecords() {
        return this.getNumberOfRecordsFromChildren() + this.getNumberOfContainedRecords();
    }
    
    public boolean hasFatalChildren() {
        return this._hasFatalChildren;
    }
    
    public boolean hasFatalRecords() {
        return this._hasFatalRecords;
    }
    
    @Override
    public int hashCode() {
        return this.getTitle().hashCode();
    }
    
    public boolean isSelected() {
        return this._selected;
    }
    
    public void resetNumberOfContainedRecords() {
        this._numberOfContainedRecords = 0;
        this._numberOfRecordsFromChildren = 0;
        this._hasFatalRecords = false;
        this._hasFatalChildren = false;
    }
    
    public void setAllDescendantsDeSelected() {
        final Enumeration children = this.children();
        while (children.hasMoreElements()) {
            final CategoryNode categoryNode = children.nextElement();
            categoryNode.setSelected(false);
            categoryNode.setAllDescendantsDeSelected();
        }
    }
    
    public void setAllDescendantsSelected() {
        final Enumeration children = this.children();
        while (children.hasMoreElements()) {
            final CategoryNode categoryNode = children.nextElement();
            categoryNode.setSelected(true);
            categoryNode.setAllDescendantsSelected();
        }
    }
    
    public void setHasFatalChildren(final boolean hasFatalChildren) {
        this._hasFatalChildren = hasFatalChildren;
    }
    
    public void setHasFatalRecords(final boolean hasFatalRecords) {
        this._hasFatalRecords = hasFatalRecords;
    }
    
    public void setSelected(final boolean selected) {
        if (selected != this._selected) {
            this._selected = selected;
        }
    }
    
    @Override
    public String toString() {
        return this.getTitle();
    }
}
