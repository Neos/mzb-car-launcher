package org.apache.log4j.lf5.viewer.categoryexplorer;

import javax.swing.table.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class CategoryAbstractCellEditor implements TableCellEditor, TreeCellEditor
{
    static Class class$javax$swing$event$CellEditorListener;
    protected ChangeEvent _changeEvent;
    protected int _clickCountToStart;
    protected EventListenerList _listenerList;
    protected Object _value;
    
    public CategoryAbstractCellEditor() {
        this._listenerList = new EventListenerList();
        this._changeEvent = null;
        this._clickCountToStart = 1;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void addCellEditorListener(final CellEditorListener cellEditorListener) {
        final EventListenerList listenerList = this._listenerList;
        Class class$javax$swing$event$CellEditorListener;
        if (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener == null) {
            class$javax$swing$event$CellEditorListener = (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener = class$("javax.swing.event.CellEditorListener"));
        }
        else {
            class$javax$swing$event$CellEditorListener = CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener;
        }
        listenerList.add(class$javax$swing$event$CellEditorListener, cellEditorListener);
    }
    
    @Override
    public void cancelCellEditing() {
        this.fireEditingCanceled();
    }
    
    protected void fireEditingCanceled() {
        final Object[] listenerList = this._listenerList.getListenerList();
        for (int i = listenerList.length - 2; i >= 0; i -= 2) {
            final Object o = listenerList[i];
            Class class$javax$swing$event$CellEditorListener;
            if (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener == null) {
                class$javax$swing$event$CellEditorListener = (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener = class$("javax.swing.event.CellEditorListener"));
            }
            else {
                class$javax$swing$event$CellEditorListener = CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener;
            }
            if (o == class$javax$swing$event$CellEditorListener) {
                if (this._changeEvent == null) {
                    this._changeEvent = new ChangeEvent(this);
                }
                ((CellEditorListener)listenerList[i + 1]).editingCanceled(this._changeEvent);
            }
        }
    }
    
    protected void fireEditingStopped() {
        final Object[] listenerList = this._listenerList.getListenerList();
        for (int i = listenerList.length - 2; i >= 0; i -= 2) {
            final Object o = listenerList[i];
            Class class$javax$swing$event$CellEditorListener;
            if (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener == null) {
                class$javax$swing$event$CellEditorListener = (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener = class$("javax.swing.event.CellEditorListener"));
            }
            else {
                class$javax$swing$event$CellEditorListener = CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener;
            }
            if (o == class$javax$swing$event$CellEditorListener) {
                if (this._changeEvent == null) {
                    this._changeEvent = new ChangeEvent(this);
                }
                ((CellEditorListener)listenerList[i + 1]).editingStopped(this._changeEvent);
            }
        }
    }
    
    @Override
    public Object getCellEditorValue() {
        return this._value;
    }
    
    public int getClickCountToStart() {
        return this._clickCountToStart;
    }
    
    @Override
    public Component getTableCellEditorComponent(final JTable table, final Object o, final boolean b, final int n, final int n2) {
        return null;
    }
    
    @Override
    public Component getTreeCellEditorComponent(final JTree tree, final Object o, final boolean b, final boolean b2, final boolean b3, final int n) {
        return null;
    }
    
    @Override
    public boolean isCellEditable(final EventObject eventObject) {
        return !(eventObject instanceof MouseEvent) || ((MouseEvent)eventObject).getClickCount() >= this._clickCountToStart;
    }
    
    @Override
    public void removeCellEditorListener(final CellEditorListener cellEditorListener) {
        final EventListenerList listenerList = this._listenerList;
        Class class$javax$swing$event$CellEditorListener;
        if (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener == null) {
            class$javax$swing$event$CellEditorListener = (CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener = class$("javax.swing.event.CellEditorListener"));
        }
        else {
            class$javax$swing$event$CellEditorListener = CategoryAbstractCellEditor.class$javax$swing$event$CellEditorListener;
        }
        listenerList.remove(class$javax$swing$event$CellEditorListener, cellEditorListener);
    }
    
    public void setCellEditorValue(final Object value) {
        this._value = value;
    }
    
    public void setClickCountToStart(final int clickCountToStart) {
        this._clickCountToStart = clickCountToStart;
    }
    
    @Override
    public boolean shouldSelectCell(final EventObject eventObject) {
        return this.isCellEditable(eventObject) && (eventObject == null || ((MouseEvent)eventObject).getClickCount() >= this._clickCountToStart);
    }
    
    @Override
    public boolean stopCellEditing() {
        this.fireEditingStopped();
        return true;
    }
}
