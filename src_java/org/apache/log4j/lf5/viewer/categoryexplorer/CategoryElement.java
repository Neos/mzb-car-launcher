package org.apache.log4j.lf5.viewer.categoryexplorer;

public class CategoryElement
{
    protected String _categoryTitle;
    
    public CategoryElement() {
    }
    
    public CategoryElement(final String categoryTitle) {
        this._categoryTitle = categoryTitle;
    }
    
    public String getTitle() {
        return this._categoryTitle;
    }
    
    public void setTitle(final String categoryTitle) {
        this._categoryTitle = categoryTitle;
    }
}
