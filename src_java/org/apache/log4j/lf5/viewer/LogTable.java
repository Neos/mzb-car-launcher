package org.apache.log4j.lf5.viewer;

import org.apache.log4j.lf5.util.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;
import javax.swing.*;

public class LogTable extends JTable
{
    private static final long serialVersionUID = 4867085140195148458L;
    protected int _colCategory;
    protected int _colDate;
    protected int _colLevel;
    protected int _colLocation;
    protected int _colMessage;
    protected int _colMessageNum;
    protected int _colNDC;
    protected LogTableColumn[] _colNames;
    protected int _colThread;
    protected int _colThrown;
    protected int[] _colWidths;
    protected DateFormatManager _dateFormatManager;
    protected JTextArea _detailTextArea;
    protected int _numCols;
    protected int _rowHeight;
    protected TableColumn[] _tableColumns;
    
    public LogTable(final JTextArea detailTextArea) {
        this._rowHeight = 30;
        this._numCols = 9;
        this._tableColumns = new TableColumn[this._numCols];
        this._colWidths = new int[] { 40, 40, 40, 70, 70, 360, 440, 200, 60 };
        this._colNames = LogTableColumn.getLogTableColumnArray();
        this._colDate = 0;
        this._colThread = 1;
        this._colMessageNum = 2;
        this._colLevel = 3;
        this._colNDC = 4;
        this._colCategory = 5;
        this._colMessage = 6;
        this._colLocation = 7;
        this._colThrown = 8;
        this._dateFormatManager = null;
        this.init();
        this._detailTextArea = detailTextArea;
        this.setModel(new FilteredLogTableModel());
        final Enumeration<TableColumn> columns = this.getColumnModel().getColumns();
        int n = 0;
        while (columns.hasMoreElements()) {
            final TableColumn tableColumn = columns.nextElement();
            tableColumn.setCellRenderer(new LogTableRowRenderer());
            tableColumn.setPreferredWidth(this._colWidths[n]);
            this._tableColumns[n] = tableColumn;
            ++n;
        }
        this.getSelectionModel().addListSelectionListener(new LogTableListSelectionListener(this));
    }
    
    public void clearLogRecords() {
        synchronized (this) {
            this.getFilteredLogTableModel().clear();
        }
    }
    
    protected Vector getColumnNameAndNumber() {
        final Vector<LogTableColumn> vector = new Vector<LogTableColumn>();
        for (int i = 0; i < this._colNames.length; ++i) {
            vector.add(i, this._colNames[i]);
        }
        return vector;
    }
    
    public DateFormatManager getDateFormatManager() {
        return this._dateFormatManager;
    }
    
    public FilteredLogTableModel getFilteredLogTableModel() {
        return (FilteredLogTableModel)this.getModel();
    }
    
    protected void init() {
        this.setRowHeight(this._rowHeight);
        this.setSelectionMode(0);
    }
    
    public void setDateFormatManager(final DateFormatManager dateFormatManager) {
        this._dateFormatManager = dateFormatManager;
    }
    
    public void setDetailedView() {
        final TableColumnModel columnModel = this.getColumnModel();
        for (int i = 0; i < this._numCols; ++i) {
            columnModel.removeColumn(this._tableColumns[i]);
        }
        for (int j = 0; j < this._numCols; ++j) {
            columnModel.addColumn(this._tableColumns[j]);
        }
        this.sizeColumnsToFit(-1);
    }
    
    @Override
    public void setFont(final Font font) {
        super.setFont(font);
        final Graphics graphics = this.getGraphics();
        if (graphics != null) {
            final int height = graphics.getFontMetrics(font).getHeight();
            this.setRowHeight(this._rowHeight = height / 3 + height);
        }
    }
    
    public void setView(final List list) {
        final TableColumnModel columnModel = this.getColumnModel();
        for (int i = 0; i < this._numCols; ++i) {
            columnModel.removeColumn(this._tableColumns[i]);
        }
        final Iterator<Object> iterator = list.iterator();
        final Vector columnNameAndNumber = this.getColumnNameAndNumber();
        while (iterator.hasNext()) {
            columnModel.addColumn(this._tableColumns[columnNameAndNumber.indexOf(iterator.next())]);
        }
        this.sizeColumnsToFit(-1);
    }
    
    class LogTableListSelectionListener implements ListSelectionListener
    {
        protected JTable _table;
        private final LogTable this$0;
        
        public LogTableListSelectionListener(final LogTable this$0, final JTable table) {
            this.this$0 = this$0;
            this._table = table;
        }
        
        @Override
        public void valueChanged(final ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                final ListSelectionModel listSelectionModel = (ListSelectionModel)listSelectionEvent.getSource();
                if (!listSelectionModel.isSelectionEmpty()) {
                    final StringBuffer sb = new StringBuffer();
                    final int minSelectionIndex = listSelectionModel.getMinSelectionIndex();
                    for (int i = 0; i < this.this$0._numCols - 1; ++i) {
                        String string = "";
                        final Object value = this._table.getModel().getValueAt(minSelectionIndex, i);
                        if (value != null) {
                            string = value.toString();
                        }
                        sb.append(new StringBuffer().append(this.this$0._colNames[i]).append(":").toString());
                        sb.append("\t");
                        if (i == this.this$0._colThread || i == this.this$0._colMessage || i == this.this$0._colLevel) {
                            sb.append("\t");
                        }
                        if (i == this.this$0._colDate || i == this.this$0._colNDC) {
                            sb.append("\t\t");
                        }
                        sb.append(string);
                        sb.append("\n");
                    }
                    sb.append(new StringBuffer().append(this.this$0._colNames[this.this$0._numCols - 1]).append(":\n").toString());
                    final Object value2 = this._table.getModel().getValueAt(minSelectionIndex, this.this$0._numCols - 1);
                    if (value2 != null) {
                        sb.append(value2.toString());
                    }
                    this.this$0._detailTextArea.setText(sb.toString());
                }
            }
        }
    }
}
