package org.apache.log4j.lf5.viewer;

import javax.swing.table.*;

public class LogTableModel extends DefaultTableModel
{
    private static final long serialVersionUID = 3593300685868700894L;
    
    public LogTableModel(final Object[] array, final int n) {
        super(array, n);
    }
    
    @Override
    public boolean isCellEditable(final int n, final int n2) {
        return false;
    }
}
