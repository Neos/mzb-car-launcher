package org.apache.log4j.lf5.viewer;

import java.awt.event.*;
import java.awt.*;

public class TrackingAdjustmentListener implements AdjustmentListener
{
    protected int _lastMaximum;
    
    public TrackingAdjustmentListener() {
        this._lastMaximum = -1;
    }
    
    @Override
    public void adjustmentValueChanged(final AdjustmentEvent adjustmentEvent) {
        final Adjustable adjustable = adjustmentEvent.getAdjustable();
        final int maximum = adjustable.getMaximum();
        if (adjustable.getMaximum() == this._lastMaximum) {
            return;
        }
        if (adjustable.getUnitIncrement() + (adjustable.getValue() + adjustable.getVisibleAmount()) >= this._lastMaximum) {
            adjustable.setValue(adjustable.getMaximum());
        }
        this._lastMaximum = maximum;
    }
}
