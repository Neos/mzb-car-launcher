package org.apache.log4j.lf5.viewer;

import org.apache.log4j.lf5.viewer.categoryexplorer.*;
import org.apache.log4j.lf5.viewer.configure.*;
import org.apache.log4j.lf5.*;
import org.apache.log4j.lf5.util.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;

public class LogBrokerMonitor
{
    public static final String DETAILED_VIEW = "Detailed";
    protected String _NDCTextFilter;
    protected boolean _callSystemExitOnClose;
    protected CategoryExplorerTree _categoryExplorerTree;
    protected List _columns;
    protected ConfigurationManager _configurationManager;
    protected String _currentView;
    protected List _displayedLogBrokerProperties;
    protected File _fileLocation;
    protected String _fontName;
    protected int _fontSize;
    protected JComboBox _fontSizeCombo;
    protected boolean _isDisposed;
    protected Dimension _lastTableViewportSize;
    protected LogLevel _leastSevereDisplayedLogLevel;
    protected List _levels;
    protected boolean _loadSystemFonts;
    protected Object _lock;
    protected Map _logLevelMenuItems;
    protected JFrame _logMonitorFrame;
    protected int _logMonitorFrameHeight;
    protected int _logMonitorFrameWidth;
    protected Map _logTableColumnMenuItems;
    protected JScrollPane _logTableScrollPane;
    protected MRUFileManager _mruFileManager;
    protected String _searchText;
    protected JLabel _statusLabel;
    protected LogTable _table;
    protected boolean _trackTableScrollPane;
    
    public LogBrokerMonitor(final List levels) {
        this._logMonitorFrameWidth = 550;
        this._logMonitorFrameHeight = 500;
        this._NDCTextFilter = "";
        this._leastSevereDisplayedLogLevel = LogLevel.DEBUG;
        this._lock = new Object();
        this._fontSize = 10;
        this._fontName = "Dialog";
        this._currentView = "Detailed";
        this._loadSystemFonts = false;
        this._trackTableScrollPane = true;
        this._callSystemExitOnClose = false;
        this._displayedLogBrokerProperties = new Vector();
        this._logLevelMenuItems = new HashMap();
        this._logTableColumnMenuItems = new HashMap();
        this._levels = null;
        this._columns = null;
        this._isDisposed = false;
        this._configurationManager = null;
        this._mruFileManager = null;
        this._fileLocation = null;
        this._levels = levels;
        this._columns = LogTableColumn.getLogTableColumns();
        String property;
        if ((property = System.getProperty("monitor.exit")) == null) {
            property = "false";
        }
        if (property.trim().toLowerCase().equals("true")) {
            this._callSystemExitOnClose = true;
        }
        this.initComponents();
        this._logMonitorFrame.addWindowListener(new LogBrokerMonitorWindowAdaptor(this));
    }
    
    public void addDisplayedProperty(final Object o) {
        this._displayedLogBrokerProperties.add(o);
    }
    
    public void addMessage(final LogRecord logRecord) {
        if (this._isDisposed) {
            return;
        }
        SwingUtilities.invokeLater(new LogBrokerMonitor$2(this, logRecord));
    }
    
    protected void addTableModelProperties() {
        final FilteredLogTableModel filteredLogTableModel = this._table.getFilteredLogTableModel();
        this.addDisplayedProperty(new LogBrokerMonitor$5(this));
        this.addDisplayedProperty(new LogBrokerMonitor$6(this, filteredLogTableModel));
    }
    
    protected void centerFrame(final JFrame frame) {
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final Dimension size = frame.getSize();
        frame.setLocation((screenSize.width - size.width) / 2, (screenSize.height - size.height) / 2);
    }
    
    protected int changeFontSizeCombo(final JComboBox comboBox, final int n) {
        final int itemCount = comboBox.getItemCount();
        Object item = comboBox.getItemAt(0);
        int int1 = Integer.parseInt(String.valueOf(item));
        Object o;
        int n2;
        for (int i = 0; i < itemCount; ++i, item = o, int1 = n2) {
            final Object item2 = comboBox.getItemAt(i);
            final int int2 = Integer.parseInt(String.valueOf(item2));
            o = item;
            if ((n2 = int1) < int2) {
                o = item;
                n2 = int1;
                if (int2 <= n) {
                    n2 = int2;
                    o = item2;
                }
            }
        }
        comboBox.setSelectedItem(item);
        return int1;
    }
    
    protected void clearDetailTextArea() {
        this._table._detailTextArea.setText("");
    }
    
    protected void closeAfterConfirm() {
        final StringBuffer sb = new StringBuffer();
        if (!this._callSystemExitOnClose) {
            sb.append("Are you sure you want to close the logging ");
            sb.append("console?\n");
            sb.append("(Note: This will not shut down the Virtual Machine,\n");
            sb.append("or the Swing event thread.)");
        }
        else {
            sb.append("Are you sure you want to exit?\n");
            sb.append("This will shut down the Virtual Machine.\n");
        }
        String s = "Are you sure you want to dispose of the Logging Console?";
        if (this._callSystemExitOnClose) {
            s = "Are you sure you want to exit?";
        }
        if (JOptionPane.showConfirmDialog(this._logMonitorFrame, sb.toString(), s, 2, 3, null) == 0) {
            this.dispose();
        }
    }
    
    protected JMenuItem createAllLogLevelsMenuItem() {
        final JMenuItem menuItem = new JMenuItem("Show all LogLevels");
        menuItem.setMnemonic('s');
        menuItem.addActionListener(new LogBrokerMonitor$8(this));
        return menuItem;
    }
    
    protected JMenuItem createAllLogTableColumnsMenuItem() {
        final JMenuItem menuItem = new JMenuItem("Show all Columns");
        menuItem.setMnemonic('s');
        menuItem.addActionListener(new LogBrokerMonitor$14(this));
        return menuItem;
    }
    
    protected JMenuItem createCloseMI() {
        final JMenuItem menuItem = new JMenuItem("Close");
        menuItem.setMnemonic('c');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("control Q"));
        menuItem.addActionListener(new LogBrokerMonitor$18(this));
        return menuItem;
    }
    
    protected JMenuItem createConfigureMaxRecords() {
        final JMenuItem menuItem = new JMenuItem("Set Max Number of Records");
        menuItem.setMnemonic('m');
        menuItem.addActionListener(new LogBrokerMonitor$23(this));
        return menuItem;
    }
    
    protected JMenu createConfigureMenu() {
        final JMenu menu = new JMenu("Configure");
        menu.setMnemonic('c');
        menu.add(this.createConfigureSave());
        menu.add(this.createConfigureReset());
        menu.add(this.createConfigureMaxRecords());
        return menu;
    }
    
    protected JMenuItem createConfigureReset() {
        final JMenuItem menuItem = new JMenuItem("Reset");
        menuItem.setMnemonic('r');
        menuItem.addActionListener(new LogBrokerMonitor$22(this));
        return menuItem;
    }
    
    protected JMenuItem createConfigureSave() {
        final JMenuItem menuItem = new JMenuItem("Save");
        menuItem.setMnemonic('s');
        menuItem.addActionListener(new LogBrokerMonitor$21(this));
        return menuItem;
    }
    
    protected JTextArea createDetailTextArea() {
        final JTextArea textArea = new JTextArea();
        textArea.setFont(new Font("Monospaced", 0, 14));
        textArea.setTabSize(3);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(false);
        return textArea;
    }
    
    protected JMenuItem createEditFindMI() {
        final JMenuItem menuItem = new JMenuItem("Find");
        menuItem.setMnemonic('f');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("control F"));
        menuItem.addActionListener(new LogBrokerMonitor$26(this));
        return menuItem;
    }
    
    protected JMenuItem createEditFindNextMI() {
        final JMenuItem menuItem = new JMenuItem("Find Next");
        menuItem.setMnemonic('n');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("F3"));
        menuItem.addActionListener(new LogBrokerMonitor$25(this));
        return menuItem;
    }
    
    protected JMenu createEditMenu() {
        final JMenu menu = new JMenu("Edit");
        menu.setMnemonic('e');
        menu.add(this.createEditFindMI());
        menu.add(this.createEditFindNextMI());
        menu.addSeparator();
        menu.add(this.createEditSortNDCMI());
        menu.add(this.createEditRestoreAllNDCMI());
        return menu;
    }
    
    protected JMenuItem createEditRestoreAllNDCMI() {
        final JMenuItem menuItem = new JMenuItem("Restore all NDCs");
        menuItem.setMnemonic('r');
        menuItem.addActionListener(new LogBrokerMonitor$28(this));
        return menuItem;
    }
    
    protected JMenuItem createEditSortNDCMI() {
        final JMenuItem menuItem = new JMenuItem("Sort by NDC");
        menuItem.setMnemonic('s');
        menuItem.addActionListener(new LogBrokerMonitor$27(this));
        return menuItem;
    }
    
    protected JMenuItem createExitMI() {
        final JMenuItem menuItem = new JMenuItem("Exit");
        menuItem.setMnemonic('x');
        menuItem.addActionListener(new LogBrokerMonitor$20(this));
        return menuItem;
    }
    
    protected JMenu createFileMenu() {
        final JMenu menu = new JMenu("File");
        menu.setMnemonic('f');
        menu.add(this.createOpenMI());
        menu.add(this.createOpenURLMI());
        menu.addSeparator();
        menu.add(this.createCloseMI());
        this.createMRUFileListMI(menu);
        menu.addSeparator();
        menu.add(this.createExitMI());
        return menu;
    }
    
    protected JMenu createHelpMenu() {
        final JMenu menu = new JMenu("Help");
        menu.setMnemonic('h');
        menu.add(this.createHelpProperties());
        return menu;
    }
    
    protected JMenuItem createHelpProperties() {
        final JMenuItem menuItem = new JMenuItem("LogFactor5 Properties");
        menuItem.setMnemonic('l');
        menuItem.addActionListener(new LogBrokerMonitor$24(this));
        return menuItem;
    }
    
    protected JMenu createLogLevelColorMenu() {
        final JMenu menu = new JMenu("Configure LogLevel Colors");
        menu.setMnemonic('c');
        final Iterator logLevels = this.getLogLevels();
        while (logLevels.hasNext()) {
            menu.add(this.createSubMenuItem(logLevels.next()));
        }
        return menu;
    }
    
    protected JComboBox createLogLevelCombo() {
        final JComboBox<Object> comboBox = new JComboBox<Object>();
        final Iterator logLevels = this.getLogLevels();
        while (logLevels.hasNext()) {
            comboBox.addItem(logLevels.next());
        }
        comboBox.setSelectedItem(this._leastSevereDisplayedLogLevel);
        comboBox.addActionListener(new LogBrokerMonitor$32(this));
        comboBox.setMaximumSize(comboBox.getPreferredSize());
        return comboBox;
    }
    
    protected JMenu createLogLevelMenu() {
        final JMenu menu = new JMenu("Log Level");
        menu.setMnemonic('l');
        final Iterator logLevels = this.getLogLevels();
        while (logLevels.hasNext()) {
            menu.add(this.getMenuItem(logLevels.next()));
        }
        menu.addSeparator();
        menu.add(this.createAllLogLevelsMenuItem());
        menu.add(this.createNoLogLevelsMenuItem());
        menu.addSeparator();
        menu.add(this.createLogLevelColorMenu());
        menu.add(this.createResetLogLevelColorMenuItem());
        return menu;
    }
    
    protected LogRecordFilter createLogRecordFilter() {
        return new LogBrokerMonitor$3(this);
    }
    
    protected JCheckBoxMenuItem createLogTableColumnMenuItem(final LogTableColumn logTableColumn) {
        final JCheckBoxMenuItem checkBoxMenuItem = new JCheckBoxMenuItem(logTableColumn.toString());
        checkBoxMenuItem.setSelected(true);
        checkBoxMenuItem.setMnemonic(logTableColumn.toString().charAt(0));
        checkBoxMenuItem.addActionListener(new LogBrokerMonitor$13(this));
        return checkBoxMenuItem;
    }
    
    protected void createMRUFileListMI(final JMenu menu) {
        final String[] mruFileList = this._mruFileManager.getMRUFileList();
        if (mruFileList != null) {
            menu.addSeparator();
            for (int i = 0; i < mruFileList.length; ++i) {
                final JMenuItem menuItem = new JMenuItem(new StringBuffer().append(i + 1).append(" ").append(mruFileList[i]).toString());
                menuItem.setMnemonic(i + 1);
                menuItem.addActionListener(new LogBrokerMonitor$19(this));
                menu.add(menuItem);
            }
        }
    }
    
    protected JMenuBar createMenuBar() {
        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(this.createFileMenu());
        menuBar.add(this.createEditMenu());
        menuBar.add(this.createLogLevelMenu());
        menuBar.add(this.createViewMenu());
        menuBar.add(this.createConfigureMenu());
        menuBar.add(this.createHelpMenu());
        return menuBar;
    }
    
    protected JCheckBoxMenuItem createMenuItem(final LogLevel logLevel) {
        final JCheckBoxMenuItem checkBoxMenuItem = new JCheckBoxMenuItem(logLevel.toString());
        checkBoxMenuItem.setSelected(true);
        checkBoxMenuItem.setMnemonic(logLevel.toString().charAt(0));
        checkBoxMenuItem.addActionListener(new LogBrokerMonitor$12(this));
        return checkBoxMenuItem;
    }
    
    protected LogRecordFilter createNDCLogRecordFilter(final String ndcTextFilter) {
        this._NDCTextFilter = ndcTextFilter;
        return new LogBrokerMonitor$4(this);
    }
    
    protected JMenuItem createNoLogLevelsMenuItem() {
        final JMenuItem menuItem = new JMenuItem("Hide all LogLevels");
        menuItem.setMnemonic('h');
        menuItem.addActionListener(new LogBrokerMonitor$9(this));
        return menuItem;
    }
    
    protected JMenuItem createNoLogTableColumnsMenuItem() {
        final JMenuItem menuItem = new JMenuItem("Hide all Columns");
        menuItem.setMnemonic('h');
        menuItem.addActionListener(new LogBrokerMonitor$15(this));
        return menuItem;
    }
    
    protected JMenuItem createOpenMI() {
        final JMenuItem menuItem = new JMenuItem("Open...");
        menuItem.setMnemonic('o');
        menuItem.addActionListener(new LogBrokerMonitor$16(this));
        return menuItem;
    }
    
    protected JMenuItem createOpenURLMI() {
        final JMenuItem menuItem = new JMenuItem("Open URL...");
        menuItem.setMnemonic('u');
        menuItem.addActionListener(new LogBrokerMonitor$17(this));
        return menuItem;
    }
    
    protected JMenuItem createResetLogLevelColorMenuItem() {
        final JMenuItem menuItem = new JMenuItem("Reset LogLevel Colors");
        menuItem.setMnemonic('r');
        menuItem.addActionListener(new LogBrokerMonitor$10(this));
        return menuItem;
    }
    
    protected JPanel createStatusArea() {
        final JPanel panel = new JPanel();
        final JLabel statusLabel = new JLabel("No log records to display.");
        (this._statusLabel = statusLabel).setHorizontalAlignment(2);
        panel.setBorder(BorderFactory.createEtchedBorder());
        panel.setLayout(new FlowLayout(0, 0, 0));
        panel.add(statusLabel);
        return panel;
    }
    
    protected JMenuItem createSubMenuItem(final LogLevel logLevel) {
        final JMenuItem menuItem = new JMenuItem(logLevel.toString());
        menuItem.setMnemonic(logLevel.toString().charAt(0));
        menuItem.addActionListener(new LogBrokerMonitor$11(this, menuItem, logLevel));
        return menuItem;
    }
    
    protected JToolBar createToolBar() {
        final JToolBar toolBar = new JToolBar();
        toolBar.putClientProperty("JToolBar.isRollover", Boolean.TRUE);
        final JComboBox<String> comboBox = new JComboBox<String>();
        final JComboBox<String> fontSizeCombo = new JComboBox<String>();
        this._fontSizeCombo = fontSizeCombo;
        ClassLoader classLoader;
        if ((classLoader = this.getClass().getClassLoader()) == null) {
            classLoader = ClassLoader.getSystemClassLoader();
        }
        final URL resource = classLoader.getResource("org/apache/log4j/lf5/viewer/images/channelexplorer_new.gif");
        Icon icon = null;
        if (resource != null) {
            icon = new ImageIcon(resource);
        }
        final JButton button = new JButton("Clear Log Table");
        if (icon != null) {
            button.setIcon(icon);
        }
        button.setToolTipText("Clear Log Table.");
        button.addActionListener(new LogBrokerMonitor$29(this));
        final Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
        String[] array;
        if (this._loadSystemFonts) {
            array = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        }
        else {
            array = defaultToolkit.getFontList();
        }
        for (int i = 0; i < array.length; ++i) {
            comboBox.addItem(array[i]);
        }
        comboBox.setSelectedItem(this._fontName);
        comboBox.addActionListener(new LogBrokerMonitor$30(this));
        fontSizeCombo.addItem("8");
        fontSizeCombo.addItem("9");
        fontSizeCombo.addItem("10");
        fontSizeCombo.addItem("12");
        fontSizeCombo.addItem("14");
        fontSizeCombo.addItem("16");
        fontSizeCombo.addItem("18");
        fontSizeCombo.addItem("24");
        fontSizeCombo.setSelectedItem(String.valueOf(this._fontSize));
        fontSizeCombo.addActionListener(new LogBrokerMonitor$31(this));
        toolBar.add(new JLabel(" Font: "));
        toolBar.add(comboBox);
        toolBar.add(fontSizeCombo);
        toolBar.addSeparator();
        toolBar.addSeparator();
        toolBar.add(button);
        button.setAlignmentY(0.5f);
        button.setAlignmentX(0.5f);
        comboBox.setMaximumSize(comboBox.getPreferredSize());
        fontSizeCombo.setMaximumSize(fontSizeCombo.getPreferredSize());
        return toolBar;
    }
    
    protected JMenu createViewMenu() {
        final JMenu menu = new JMenu("View");
        menu.setMnemonic('v');
        final Iterator logTableColumns = this.getLogTableColumns();
        while (logTableColumns.hasNext()) {
            menu.add(this.getLogTableColumnMenuItem(logTableColumns.next()));
        }
        menu.addSeparator();
        menu.add(this.createAllLogTableColumnsMenuItem());
        menu.add(this.createNoLogTableColumnsMenuItem());
        return menu;
    }
    
    public void dispose() {
        this._logMonitorFrame.dispose();
        this._isDisposed = true;
        if (this._callSystemExitOnClose) {
            System.exit(0);
        }
    }
    
    protected int findRecord(int n, final String s, final List list) {
        if (n < 0) {
            n = 0;
        }
        else {
            ++n;
        }
        for (int size = list.size(), i = n; i < size; ++i) {
            if (this.matches(list.get(i), s)) {
                return i;
            }
        }
        for (int j = 0; j < n; ++j) {
            if (this.matches(list.get(j), s)) {
                return j;
            }
        }
        return -1;
    }
    
    protected void findSearchText() {
        final String searchText = this._searchText;
        if (searchText == null || searchText.length() == 0) {
            return;
        }
        this.selectRow(this.findRecord(this.getFirstSelectedRow(), searchText, this._table.getFilteredLogTableModel().getFilteredRecords()));
    }
    
    public JFrame getBaseFrame() {
        return this._logMonitorFrame;
    }
    
    public boolean getCallSystemExitOnClose() {
        return this._callSystemExitOnClose;
    }
    
    public CategoryExplorerTree getCategoryExplorerTree() {
        return this._categoryExplorerTree;
    }
    
    public DateFormatManager getDateFormatManager() {
        return this._table.getDateFormatManager();
    }
    
    protected int getFirstSelectedRow() {
        return this._table.getSelectionModel().getMinSelectionIndex();
    }
    
    public Map getLogLevelMenuItems() {
        return this._logLevelMenuItems;
    }
    
    protected Iterator getLogLevels() {
        return this._levels.iterator();
    }
    
    protected JCheckBoxMenuItem getLogTableColumnMenuItem(final LogTableColumn logTableColumn) {
        JCheckBoxMenuItem logTableColumnMenuItem;
        if ((logTableColumnMenuItem = this._logTableColumnMenuItems.get(logTableColumn)) == null) {
            logTableColumnMenuItem = this.createLogTableColumnMenuItem(logTableColumn);
            this._logTableColumnMenuItems.put(logTableColumn, logTableColumnMenuItem);
        }
        return logTableColumnMenuItem;
    }
    
    public Map getLogTableColumnMenuItems() {
        return this._logTableColumnMenuItems;
    }
    
    protected Iterator getLogTableColumns() {
        return this._columns.iterator();
    }
    
    protected JCheckBoxMenuItem getMenuItem(final LogLevel logLevel) {
        JCheckBoxMenuItem menuItem;
        if ((menuItem = this._logLevelMenuItems.get(logLevel)) == null) {
            menuItem = this.createMenuItem(logLevel);
            this._logLevelMenuItems.put(logLevel, menuItem);
        }
        return menuItem;
    }
    
    public String getNDCTextFilter() {
        return this._NDCTextFilter;
    }
    
    protected String getRecordsDisplayedMessage() {
        final FilteredLogTableModel filteredLogTableModel = this._table.getFilteredLogTableModel();
        return this.getStatusText(filteredLogTableModel.getRowCount(), filteredLogTableModel.getTotalRowCount());
    }
    
    protected String getStatusText(final int n, final int n2) {
        final StringBuffer sb = new StringBuffer();
        sb.append("Displaying: ");
        sb.append(n);
        sb.append(" records out of a total of: ");
        sb.append(n2);
        sb.append(" records.");
        return sb.toString();
    }
    
    public JCheckBoxMenuItem getTableColumnMenuItem(final LogTableColumn logTableColumn) {
        return this.getLogTableColumnMenuItem(logTableColumn);
    }
    
    public void hide() {
        this._logMonitorFrame.setVisible(false);
    }
    
    protected void initComponents() {
        (this._logMonitorFrame = new JFrame("LogFactor5")).setDefaultCloseOperation(0);
        final URL resource = this.getClass().getResource("/org/apache/log4j/lf5/viewer/images/lf5_small_icon.gif");
        if (resource != null) {
            this._logMonitorFrame.setIconImage(new ImageIcon(resource).getImage());
        }
        this.updateFrameSize();
        final JTextArea detailTextArea = this.createDetailTextArea();
        final JScrollPane rightComponent = new JScrollPane(detailTextArea);
        this._table = new LogTable(detailTextArea);
        this.setView(this._currentView, this._table);
        this._table.setFont(new Font(this._fontName, 0, this._fontSize));
        this._logTableScrollPane = new JScrollPane(this._table);
        if (this._trackTableScrollPane) {
            this._logTableScrollPane.getVerticalScrollBar().addAdjustmentListener(new TrackingAdjustmentListener());
        }
        final JSplitPane rightComponent2 = new JSplitPane();
        rightComponent2.setOneTouchExpandable(true);
        rightComponent2.setOrientation(0);
        rightComponent2.setLeftComponent(this._logTableScrollPane);
        rightComponent2.setRightComponent(rightComponent);
        rightComponent2.setDividerLocation(350);
        this._categoryExplorerTree = new CategoryExplorerTree();
        this._table.getFilteredLogTableModel().setLogRecordFilter(this.createLogRecordFilter());
        final JScrollPane leftComponent = new JScrollPane(this._categoryExplorerTree);
        leftComponent.setPreferredSize(new Dimension(130, 400));
        this._mruFileManager = new MRUFileManager();
        final JSplitPane splitPane = new JSplitPane();
        splitPane.setOneTouchExpandable(true);
        splitPane.setRightComponent(rightComponent2);
        splitPane.setLeftComponent(leftComponent);
        splitPane.setDividerLocation(130);
        this._logMonitorFrame.getRootPane().setJMenuBar(this.createMenuBar());
        this._logMonitorFrame.getContentPane().add(splitPane, "Center");
        this._logMonitorFrame.getContentPane().add(this.createToolBar(), "North");
        this._logMonitorFrame.getContentPane().add(this.createStatusArea(), "South");
        this.makeLogTableListenToCategoryExplorer();
        this.addTableModelProperties();
        this._configurationManager = new ConfigurationManager(this, this._table);
    }
    
    protected boolean loadLogFile(final File file) {
        try {
            new LogFileParser(file).parse(this);
            return true;
        }
        catch (IOException ex) {
            final LogFactor5ErrorDialog logFactor5ErrorDialog = new LogFactor5ErrorDialog(this.getBaseFrame(), new StringBuffer().append("Error reading ").append(file.getName()).toString());
            return false;
        }
    }
    
    protected boolean loadLogFile(final URL url) {
        try {
            new LogFileParser(url.openStream()).parse(this);
            return true;
        }
        catch (IOException ex) {
            final LogFactor5ErrorDialog logFactor5ErrorDialog = new LogFactor5ErrorDialog(this.getBaseFrame(), new StringBuffer().append("Error reading URL:").append(url.getFile()).toString());
            return false;
        }
    }
    
    protected void makeLogTableListenToCategoryExplorer() {
        this._categoryExplorerTree.getExplorerModel().addActionListener(new LogBrokerMonitor$7(this));
    }
    
    protected boolean matches(final LogRecord logRecord, final String s) {
        final String message = logRecord.getMessage();
        final String ndc = logRecord.getNDC();
        return (message != null || ndc != null) && s != null && (message.toLowerCase().indexOf(s.toLowerCase()) != -1 || ndc.toLowerCase().indexOf(s.toLowerCase()) != -1);
    }
    
    protected void pause(final int n) {
        final long n2 = n;
        try {
            Thread.sleep(n2);
        }
        catch (InterruptedException ex) {}
    }
    
    protected void refresh(final JTextArea textArea) {
        final String text = textArea.getText();
        textArea.setText("");
        textArea.setText(text);
    }
    
    protected void refreshDetailTextArea() {
        this.refresh(this._table._detailTextArea);
    }
    
    protected void requestClose() {
        this.setCallSystemExitOnClose(false);
        this.closeAfterConfirm();
    }
    
    protected void requestExit() {
        this._mruFileManager.save();
        this.setCallSystemExitOnClose(true);
        this.closeAfterConfirm();
    }
    
    protected void requestOpen() {
        JFileChooser fileChooser;
        if (this._fileLocation == null) {
            fileChooser = new JFileChooser();
        }
        else {
            fileChooser = new JFileChooser(this._fileLocation);
        }
        if (fileChooser.showOpenDialog(this._logMonitorFrame) == 0) {
            final File selectedFile = fileChooser.getSelectedFile();
            if (this.loadLogFile(selectedFile)) {
                this._fileLocation = fileChooser.getSelectedFile();
                this._mruFileManager.set(selectedFile);
                this.updateMRUList();
            }
        }
    }
    
    protected void requestOpenMRU(final ActionEvent actionEvent) {
        final StringTokenizer stringTokenizer = new StringTokenizer(actionEvent.getActionCommand());
        final String trim = stringTokenizer.nextToken().trim();
        final String nextToken = stringTokenizer.nextToken("\n");
        try {
            final int n = Integer.parseInt(trim) - 1;
            new LogFileParser(this._mruFileManager.getInputStream(n)).parse(this);
            this._mruFileManager.moveToTop(n);
            this.updateMRUList();
        }
        catch (Exception ex) {
            final LogFactor5ErrorDialog logFactor5ErrorDialog = new LogFactor5ErrorDialog(this.getBaseFrame(), new StringBuffer().append("Unable to load file ").append(nextToken).toString());
        }
    }
    
    protected void requestOpenURL() {
        final String text = new LogFactor5InputDialog(this.getBaseFrame(), "Open URL", "URL:").getText();
        if (text == null) {
            return;
        }
        String string = text;
        if (text.indexOf("://") == -1) {
            string = new StringBuffer().append("http://").append(text).toString();
        }
        try {
            final URL url = new URL(string);
            if (this.loadLogFile(url)) {
                this._mruFileManager.set(url);
                this.updateMRUList();
            }
        }
        catch (MalformedURLException ex) {
            new LogFactor5ErrorDialog(this.getBaseFrame(), "Error reading URL.");
        }
    }
    
    protected void resetConfiguration() {
        this._configurationManager.reset();
    }
    
    protected void saveConfiguration() {
        this._configurationManager.save();
    }
    
    protected void selectAllLogLevels(final boolean selected) {
        final Iterator logLevels = this.getLogLevels();
        while (logLevels.hasNext()) {
            this.getMenuItem(logLevels.next()).setSelected(selected);
        }
    }
    
    protected void selectAllLogTableColumns(final boolean selected) {
        final Iterator logTableColumns = this.getLogTableColumns();
        while (logTableColumns.hasNext()) {
            this.getLogTableColumnMenuItem(logTableColumns.next()).setSelected(selected);
        }
    }
    
    protected void selectRow(final int n) {
        if (n == -1) {
            JOptionPane.showMessageDialog(this._logMonitorFrame, new StringBuffer().append(this._searchText).append(" not found.").toString(), "Text not found", 1);
            return;
        }
        LF5SwingUtils.selectRow(n, this._table, this._logTableScrollPane);
    }
    
    public void setCallSystemExitOnClose(final boolean callSystemExitOnClose) {
        this._callSystemExitOnClose = callSystemExitOnClose;
    }
    
    public void setDateFormatManager(final DateFormatManager dateFormatManager) {
        this._table.setDateFormatManager(dateFormatManager);
    }
    
    public void setFontSize(final int n) {
        this.changeFontSizeCombo(this._fontSizeCombo, n);
    }
    
    protected void setFontSize(final Component component, final int n) {
        final Font font = component.getFont();
        component.setFont(new Font(font.getFontName(), font.getStyle(), n));
    }
    
    protected void setFontSizeSilently(final int fontSize) {
        this._fontSize = fontSize;
        this.setFontSize(this._table._detailTextArea, fontSize);
        this.selectRow(0);
        this.setFontSize(this._table, fontSize);
    }
    
    public void setFrameSize(final int logMonitorFrameWidth, final int logMonitorFrameHeight) {
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (logMonitorFrameWidth > 0 && logMonitorFrameWidth < screenSize.width) {
            this._logMonitorFrameWidth = logMonitorFrameWidth;
        }
        if (logMonitorFrameHeight > 0 && logMonitorFrameHeight < screenSize.height) {
            this._logMonitorFrameHeight = logMonitorFrameHeight;
        }
        this.updateFrameSize();
    }
    
    protected void setLeastSevereDisplayedLogLevel(final LogLevel leastSevereDisplayedLogLevel) {
        if (leastSevereDisplayedLogLevel == null || this._leastSevereDisplayedLogLevel == leastSevereDisplayedLogLevel) {
            return;
        }
        this._leastSevereDisplayedLogLevel = leastSevereDisplayedLogLevel;
        this._table.getFilteredLogTableModel().refresh();
        this.updateStatusLabel();
    }
    
    public void setMaxNumberOfLogRecords(final int maxNumberOfLogRecords) {
        this._table.getFilteredLogTableModel().setMaxNumberOfLogRecords(maxNumberOfLogRecords);
    }
    
    protected void setMaxRecordConfiguration() {
        final String text = new LogFactor5InputDialog(this.getBaseFrame(), "Set Max Number of Records", "", 10).getText();
        if (text == null) {
            return;
        }
        try {
            this.setMaxNumberOfLogRecords(Integer.parseInt(text));
        }
        catch (NumberFormatException ex) {
            final LogFactor5ErrorDialog logFactor5ErrorDialog = new LogFactor5ErrorDialog(this.getBaseFrame(), new StringBuffer().append("'").append(text).append("' is an invalid parameter.\nPlease try again.").toString());
            this.setMaxRecordConfiguration();
        }
    }
    
    public void setNDCLogRecordFilter(final String s) {
        this._table.getFilteredLogTableModel().setLogRecordFilter(this.createNDCLogRecordFilter(s));
    }
    
    protected void setNDCTextFilter(final String ndcTextFilter) {
        if (ndcTextFilter == null) {
            this._NDCTextFilter = "";
            return;
        }
        this._NDCTextFilter = ndcTextFilter;
    }
    
    protected void setSearchText(final String searchText) {
        this._searchText = searchText;
    }
    
    public void setTitle(final String s) {
        this._logMonitorFrame.setTitle(new StringBuffer().append(s).append(" - LogFactor5").toString());
    }
    
    protected void setView(final String currentView, final LogTable logTable) {
        if ("Detailed".equals(currentView)) {
            logTable.setDetailedView();
            this._currentView = currentView;
            return;
        }
        throw new IllegalArgumentException(new StringBuffer().append(currentView).append("does not match a supported view.").toString());
    }
    
    public void show() {
        this.show(0);
    }
    
    public void show(final int n) {
        if (this._logMonitorFrame.isVisible()) {
            return;
        }
        SwingUtilities.invokeLater(new LogBrokerMonitor$1(this, n));
    }
    
    protected void showLogLevelColorChangeDialog(final JMenuItem menuItem, final LogLevel logLevel) {
        final Color showDialog = JColorChooser.showDialog(this._logMonitorFrame, "Choose LogLevel Color", menuItem.getForeground());
        if (showDialog != null) {
            logLevel.setLogLevelColorMap(logLevel, showDialog);
            this._table.getFilteredLogTableModel().refresh();
        }
    }
    
    protected void showPropertiesDialog(final String s) {
        JOptionPane.showMessageDialog(this._logMonitorFrame, this._displayedLogBrokerProperties.toArray(), s, -1);
    }
    
    protected void sortByNDC() {
        final String ndcTextFilter = this._NDCTextFilter;
        if (ndcTextFilter == null || ndcTextFilter.length() == 0) {
            return;
        }
        this._table.getFilteredLogTableModel().setLogRecordFilter(this.createNDCLogRecordFilter(ndcTextFilter));
    }
    
    protected void trackTableScrollPane() {
    }
    
    protected void updateFrameSize() {
        this._logMonitorFrame.setSize(this._logMonitorFrameWidth, this._logMonitorFrameHeight);
        this.centerFrame(this._logMonitorFrame);
    }
    
    protected void updateMRUList() {
        final JMenu menu = this._logMonitorFrame.getJMenuBar().getMenu(0);
        menu.removeAll();
        menu.add(this.createOpenMI());
        menu.add(this.createOpenURLMI());
        menu.addSeparator();
        menu.add(this.createCloseMI());
        this.createMRUFileListMI(menu);
        menu.addSeparator();
        menu.add(this.createExitMI());
    }
    
    protected void updateStatusLabel() {
        this._statusLabel.setText(this.getRecordsDisplayedMessage());
    }
    
    protected List updateView() {
        final ArrayList<LogTableColumn> list = new ArrayList<LogTableColumn>();
        for (final LogTableColumn logTableColumn : this._columns) {
            if (this.getLogTableColumnMenuItem(logTableColumn).isSelected()) {
                list.add(logTableColumn);
            }
        }
        return list;
    }
    
    class LogBrokerMonitorWindowAdaptor extends WindowAdapter
    {
        protected LogBrokerMonitor _monitor;
        private final LogBrokerMonitor this$0;
        
        public LogBrokerMonitorWindowAdaptor(final LogBrokerMonitor this$0, final LogBrokerMonitor monitor) {
            this.this$0 = this$0;
            this._monitor = monitor;
        }
        
        @Override
        public void windowClosing(final WindowEvent windowEvent) {
            this._monitor.requestClose();
        }
    }
}
