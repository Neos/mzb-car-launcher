package org.apache.log4j.lf5.viewer;

import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LF5SwingUtils
{
    protected static boolean contains(final int n, final TableModel tableModel) {
        return tableModel != null && n >= 0 && n < tableModel.getRowCount();
    }
    
    public static void makeScrollBarTrack(final Adjustable adjustable) {
        if (adjustable == null) {
            return;
        }
        adjustable.addAdjustmentListener(new TrackingAdjustmentListener());
    }
    
    public static void makeVerticalScrollBarTrack(final JScrollPane scrollPane) {
        if (scrollPane == null) {
            return;
        }
        makeScrollBarTrack(scrollPane.getVerticalScrollBar());
    }
    
    protected static void moveAdjustable(final int value, final Adjustable adjustable) {
        if (adjustable == null) {
            return;
        }
        adjustable.setValue(value);
    }
    
    protected static void repaintLater(final JComponent component) {
        SwingUtilities.invokeLater(new LF5SwingUtils$1(component));
    }
    
    public static void selectRow(final int n, final JTable table, final JScrollPane scrollPane) {
        if (table != null && scrollPane != null && contains(n, table.getModel())) {
            moveAdjustable(table.getRowHeight() * n, scrollPane.getVerticalScrollBar());
            selectRow(n, table.getSelectionModel());
            repaintLater(table);
        }
    }
    
    protected static void selectRow(final int n, final ListSelectionModel listSelectionModel) {
        if (listSelectionModel == null) {
            return;
        }
        listSelectionModel.setSelectionInterval(n, n);
    }
}
