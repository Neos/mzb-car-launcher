package org.apache.log4j.lf5.viewer;

import javax.swing.*;
import java.awt.*;

public abstract class LogFactor5Dialog extends JDialog
{
    protected static final Font DISPLAY_FONT;
    
    static {
        DISPLAY_FONT = new Font("Arial", 1, 12);
    }
    
    protected LogFactor5Dialog(final JFrame frame, final String s, final boolean b) {
        super(frame, s, b);
    }
    
    protected void centerWindow(final Window window) {
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (screenSize.width < window.getSize().width) {
            window.setSize(screenSize.width, window.getSize().height);
        }
        if (screenSize.height < window.getSize().height) {
            window.setSize(window.getSize().width, screenSize.height);
        }
        window.setLocation((screenSize.width - window.getSize().width) / 2, (screenSize.height - window.getSize().height) / 2);
    }
    
    protected GridBagConstraints getDefaultConstraints() {
        final GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.insets = new Insets(4, 4, 4, 4);
        gridBagConstraints.fill = 0;
        gridBagConstraints.anchor = 17;
        return gridBagConstraints;
    }
    
    protected void minimumSizeDialog(final Component component, final int n, final int n2) {
        if (component.getSize().width < n) {
            component.setSize(n, component.getSize().height);
        }
        if (component.getSize().height < n2) {
            component.setSize(component.getSize().width, n2);
        }
    }
    
    @Override
    public void show() {
        this.pack();
        this.minimumSizeDialog(this, 200, 100);
        this.centerWindow(this);
        super.show();
    }
    
    protected void wrapStringOnPanel(String substring, final Container container) {
        final GridBagConstraints defaultConstraints = this.getDefaultConstraints();
        defaultConstraints.gridwidth = 0;
        defaultConstraints.insets = new Insets(0, 0, 0, 0);
        final GridBagLayout gridBagLayout = (GridBagLayout)container.getLayout();
        while (substring.length() > 0) {
            final int index = substring.indexOf(10);
            String substring2;
            if (index >= 0) {
                substring2 = substring.substring(0, index);
                substring = substring.substring(index + 1);
            }
            else {
                final String s = "";
                substring2 = substring;
                substring = s;
            }
            final Label label = new Label(substring2);
            label.setFont(LogFactor5Dialog.DISPLAY_FONT);
            gridBagLayout.setConstraints(label, defaultConstraints);
            container.add(label);
        }
    }
}
