package org.apache.log4j.lf5.viewer;

import javax.swing.table.*;
import org.apache.log4j.lf5.*;
import javax.swing.*;
import java.awt.*;

public class LogTableRowRenderer extends DefaultTableCellRenderer
{
    private static final long serialVersionUID = -3951639953706443213L;
    protected Color _color;
    protected boolean _highlightFatal;
    
    public LogTableRowRenderer() {
        this._highlightFatal = true;
        this._color = new Color(230, 230, 230);
    }
    
    protected Color getLogLevelColor(final LogLevel logLevel) {
        return LogLevel.getLogLevelColorMap().get(logLevel);
    }
    
    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object o, final boolean b, final boolean b2, final int n, final int n2) {
        if (n % 2 == 0) {
            this.setBackground(this._color);
        }
        else {
            this.setBackground(Color.white);
        }
        this.setForeground(this.getLogLevelColor(((FilteredLogTableModel)table.getModel()).getFilteredRecord(n).getLevel()));
        return super.getTableCellRendererComponent(table, o, b, b2, n, n2);
    }
}
