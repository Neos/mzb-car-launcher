package org.apache.log4j.lf5.viewer;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class LogFactor5ErrorDialog extends LogFactor5Dialog
{
    public LogFactor5ErrorDialog(final JFrame frame, final String s) {
        super(frame, "Error", true);
        final JButton button = new JButton("Ok");
        button.addActionListener(new LogFactor5ErrorDialog$1(this));
        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(button);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        this.wrapStringOnPanel(s, panel2);
        this.getContentPane().add(panel2, "Center");
        this.getContentPane().add(panel, "South");
        this.show();
    }
}
