package org.apache.log4j.lf5.viewer;

import java.io.*;
import java.util.*;

public class LogTableColumn implements Serializable
{
    public static final LogTableColumn CATEGORY;
    public static final LogTableColumn DATE;
    public static final LogTableColumn LEVEL;
    public static final LogTableColumn LOCATION;
    public static final LogTableColumn MESSAGE;
    public static final LogTableColumn MESSAGE_NUM;
    public static final LogTableColumn NDC;
    public static final LogTableColumn THREAD;
    public static final LogTableColumn THROWN;
    private static LogTableColumn[] _log4JColumns;
    private static Map _logTableColumnMap;
    private static final long serialVersionUID = -4275827753626456547L;
    protected String _label;
    
    static {
        DATE = new LogTableColumn("Date");
        THREAD = new LogTableColumn("Thread");
        MESSAGE_NUM = new LogTableColumn("Message #");
        LEVEL = new LogTableColumn("Level");
        NDC = new LogTableColumn("NDC");
        CATEGORY = new LogTableColumn("Category");
        MESSAGE = new LogTableColumn("Message");
        LOCATION = new LogTableColumn("Location");
        THROWN = new LogTableColumn("Thrown");
        LogTableColumn._log4JColumns = new LogTableColumn[] { LogTableColumn.DATE, LogTableColumn.THREAD, LogTableColumn.MESSAGE_NUM, LogTableColumn.LEVEL, LogTableColumn.NDC, LogTableColumn.CATEGORY, LogTableColumn.MESSAGE, LogTableColumn.LOCATION, LogTableColumn.THROWN };
        LogTableColumn._logTableColumnMap = new HashMap();
        for (int i = 0; i < LogTableColumn._log4JColumns.length; ++i) {
            LogTableColumn._logTableColumnMap.put(LogTableColumn._log4JColumns[i].getLabel(), LogTableColumn._log4JColumns[i]);
        }
    }
    
    public LogTableColumn(final String label) {
        this._label = label;
    }
    
    public static LogTableColumn[] getLogTableColumnArray() {
        return LogTableColumn._log4JColumns;
    }
    
    public static List getLogTableColumns() {
        return Arrays.asList(LogTableColumn._log4JColumns);
    }
    
    public static LogTableColumn valueOf(final String s) throws LogTableColumnFormatException {
        LogTableColumn logTableColumn = null;
        String trim = s;
        if (s != null) {
            trim = s.trim();
            logTableColumn = (LogTableColumn)LogTableColumn._logTableColumnMap.get(trim);
        }
        if (logTableColumn == null) {
            final StringBuffer sb = new StringBuffer();
            sb.append(new StringBuffer().append("Error while trying to parse (").append(trim).append(") into").toString());
            sb.append(" a LogTableColumn.");
            throw new LogTableColumnFormatException(sb.toString());
        }
        return logTableColumn;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = false;
        if (o instanceof LogTableColumn) {
            b = b;
            if (this.getLabel() == ((LogTableColumn)o).getLabel()) {
                b = true;
            }
        }
        return b;
    }
    
    public String getLabel() {
        return this._label;
    }
    
    @Override
    public int hashCode() {
        return this._label.hashCode();
    }
    
    @Override
    public String toString() {
        return this._label;
    }
}
