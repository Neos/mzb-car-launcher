package org.apache.log4j.lf5;

import org.apache.log4j.spi.*;

public class Log4JLogRecord extends LogRecord
{
    @Override
    public boolean isSevereLevel() {
        boolean b = false;
        if (LogLevel.ERROR.equals(this.getLevel()) || LogLevel.FATAL.equals(this.getLevel())) {
            b = true;
        }
        return b;
    }
    
    public void setThrownStackTrace(final ThrowableInformation throwableInformation) {
        final String[] throwableStrRep = throwableInformation.getThrowableStrRep();
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < throwableStrRep.length; ++i) {
            sb.append(new StringBuffer().append(throwableStrRep[i]).append("\n").toString());
        }
        this._thrownStackTrace = sb.toString();
    }
}
