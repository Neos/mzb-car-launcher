package org.apache.log4j.lf5;

import org.apache.log4j.*;
import java.net.*;
import java.io.*;
import org.apache.log4j.spi.*;

public class DefaultLF5Configurator implements Configurator
{
    static Class class$org$apache$log4j$lf5$DefaultLF5Configurator;
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void configure() throws IOException {
        Class class$org$apache$log4j$lf5$DefaultLF5Configurator;
        if (DefaultLF5Configurator.class$org$apache$log4j$lf5$DefaultLF5Configurator == null) {
            class$org$apache$log4j$lf5$DefaultLF5Configurator = (DefaultLF5Configurator.class$org$apache$log4j$lf5$DefaultLF5Configurator = class$("org.apache.log4j.lf5.DefaultLF5Configurator"));
        }
        else {
            class$org$apache$log4j$lf5$DefaultLF5Configurator = DefaultLF5Configurator.class$org$apache$log4j$lf5$DefaultLF5Configurator;
        }
        final URL resource = class$org$apache$log4j$lf5$DefaultLF5Configurator.getResource("/org/apache/log4j/lf5/config/defaultconfig.properties");
        if (resource != null) {
            PropertyConfigurator.configure(resource);
            return;
        }
        throw new IOException(new StringBuffer().append("Error: Unable to open the resource").append("/org/apache/log4j/lf5/config/defaultconfig.properties").toString());
    }
    
    @Override
    public void doConfigure(final InputStream inputStream, final LoggerRepository loggerRepository) {
        throw new IllegalStateException("This class should NOT be instantiated!");
    }
    
    @Override
    public void doConfigure(final URL url, final LoggerRepository loggerRepository) {
        throw new IllegalStateException("This class should NOT be instantiated!");
    }
}
