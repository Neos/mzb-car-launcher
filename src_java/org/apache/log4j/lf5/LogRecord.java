package org.apache.log4j.lf5;

import java.io.*;

public abstract class LogRecord implements Serializable
{
    protected static long _seqCount;
    protected String _category;
    protected LogLevel _level;
    protected String _location;
    protected String _message;
    protected long _millis;
    protected String _ndc;
    protected long _sequenceNumber;
    protected String _thread;
    protected Throwable _thrown;
    protected String _thrownStackTrace;
    
    static {
        LogRecord._seqCount = 0L;
    }
    
    public LogRecord() {
        this._millis = System.currentTimeMillis();
        this._category = "Debug";
        this._message = "";
        this._level = LogLevel.INFO;
        this._sequenceNumber = getNextId();
        this._thread = Thread.currentThread().toString();
        this._ndc = "";
        this._location = "";
    }
    
    protected static long getNextId() {
        synchronized (LogRecord.class) {
            return ++LogRecord._seqCount;
        }
    }
    
    public static void resetSequenceNumber() {
        synchronized (LogRecord.class) {
            LogRecord._seqCount = 0L;
        }
    }
    
    public String getCategory() {
        return this._category;
    }
    
    public LogLevel getLevel() {
        return this._level;
    }
    
    public String getLocation() {
        return this._location;
    }
    
    public String getMessage() {
        return this._message;
    }
    
    public long getMillis() {
        return this._millis;
    }
    
    public String getNDC() {
        return this._ndc;
    }
    
    public long getSequenceNumber() {
        return this._sequenceNumber;
    }
    
    public String getThreadDescription() {
        return this._thread;
    }
    
    public Throwable getThrown() {
        return this._thrown;
    }
    
    public String getThrownStackTrace() {
        return this._thrownStackTrace;
    }
    
    public boolean hasThrown() {
        final Throwable thrown = this.getThrown();
        if (thrown != null) {
            final String string = thrown.toString();
            if (string != null && string.trim().length() != 0) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isFatal() {
        return this.isSevereLevel() || this.hasThrown();
    }
    
    public abstract boolean isSevereLevel();
    
    public void setCategory(final String category) {
        this._category = category;
    }
    
    public void setLevel(final LogLevel level) {
        this._level = level;
    }
    
    public void setLocation(final String location) {
        this._location = location;
    }
    
    public void setMessage(final String message) {
        this._message = message;
    }
    
    public void setMillis(final long millis) {
        this._millis = millis;
    }
    
    public void setNDC(final String ndc) {
        this._ndc = ndc;
    }
    
    public void setSequenceNumber(final long sequenceNumber) {
        this._sequenceNumber = sequenceNumber;
    }
    
    public void setThreadDescription(final String thread) {
        this._thread = thread;
    }
    
    public void setThrown(final Throwable thrown) {
        if (thrown == null) {
            return;
        }
        this._thrown = thrown;
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        thrown.printStackTrace(printWriter);
        printWriter.flush();
        this._thrownStackTrace = stringWriter.toString();
        try {
            printWriter.close();
            stringWriter.close();
        }
        catch (IOException ex) {}
    }
    
    public void setThrownStackTrace(final String thrownStackTrace) {
        this._thrownStackTrace = thrownStackTrace;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(new StringBuffer().append("LogRecord: [").append(this._level).append(", ").append(this._message).append("]").toString());
        return sb.toString();
    }
}
