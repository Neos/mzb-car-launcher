package org.apache.log4j.lf5.util;

import java.util.*;
import java.text.*;

public class DateFormatManager
{
    private DateFormat _dateFormat;
    private Locale _locale;
    private String _pattern;
    private TimeZone _timeZone;
    
    public DateFormatManager() {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this.configure();
    }
    
    public DateFormatManager(final String pattern) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._pattern = pattern;
        this.configure();
    }
    
    public DateFormatManager(final Locale locale) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._locale = locale;
        this.configure();
    }
    
    public DateFormatManager(final Locale locale, final String pattern) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._locale = locale;
        this._pattern = pattern;
        this.configure();
    }
    
    public DateFormatManager(final TimeZone timeZone) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._timeZone = timeZone;
        this.configure();
    }
    
    public DateFormatManager(final TimeZone timeZone, final String pattern) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._timeZone = timeZone;
        this._pattern = pattern;
        this.configure();
    }
    
    public DateFormatManager(final TimeZone timeZone, final Locale locale) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._timeZone = timeZone;
        this._locale = locale;
        this.configure();
    }
    
    public DateFormatManager(final TimeZone timeZone, final Locale locale, final String pattern) {
        this._timeZone = null;
        this._locale = null;
        this._pattern = null;
        this._dateFormat = null;
        this._timeZone = timeZone;
        this._locale = locale;
        this._pattern = pattern;
        this.configure();
    }
    
    private void configure() {
        synchronized (this) {
            (this._dateFormat = DateFormat.getDateTimeInstance(0, 0, this.getLocale())).setTimeZone(this.getTimeZone());
            if (this._pattern != null) {
                ((SimpleDateFormat)this._dateFormat).applyPattern(this._pattern);
            }
        }
    }
    
    public String format(final Date date) {
        return this.getDateFormatInstance().format(date);
    }
    
    public String format(final Date date, final String s) {
        DateFormat dateFormatInstance;
        final DateFormat dateFormat = dateFormatInstance = this.getDateFormatInstance();
        if (dateFormat instanceof SimpleDateFormat) {
            dateFormatInstance = (SimpleDateFormat)dateFormat.clone();
            ((SimpleDateFormat)dateFormatInstance).applyPattern(s);
        }
        return dateFormatInstance.format(date);
    }
    
    public DateFormat getDateFormatInstance() {
        synchronized (this) {
            return this._dateFormat;
        }
    }
    
    public Locale getLocale() {
        synchronized (this) {
            Locale locale;
            if (this._locale == null) {
                locale = Locale.getDefault();
            }
            else {
                locale = this._locale;
            }
            return locale;
        }
    }
    
    public String getOutputFormat() {
        synchronized (this) {
            return this._pattern;
        }
    }
    
    public String getPattern() {
        synchronized (this) {
            return this._pattern;
        }
    }
    
    public TimeZone getTimeZone() {
        synchronized (this) {
            TimeZone timeZone;
            if (this._timeZone == null) {
                timeZone = TimeZone.getDefault();
            }
            else {
                timeZone = this._timeZone;
            }
            return timeZone;
        }
    }
    
    public Date parse(final String s) throws ParseException {
        return this.getDateFormatInstance().parse(s);
    }
    
    public Date parse(final String s, final String s2) throws ParseException {
        DateFormat dateFormatInstance;
        final DateFormat dateFormat = dateFormatInstance = this.getDateFormatInstance();
        if (dateFormat instanceof SimpleDateFormat) {
            dateFormatInstance = (SimpleDateFormat)dateFormat.clone();
            ((SimpleDateFormat)dateFormatInstance).applyPattern(s2);
        }
        return dateFormatInstance.parse(s);
    }
    
    public void setDateFormatInstance(final DateFormat dateFormat) {
        synchronized (this) {
            this._dateFormat = dateFormat;
        }
    }
    
    public void setLocale(final Locale locale) {
        synchronized (this) {
            this._locale = locale;
            this.configure();
        }
    }
    
    public void setOutputFormat(final String pattern) {
        synchronized (this) {
            this._pattern = pattern;
            this.configure();
        }
    }
    
    public void setPattern(final String pattern) {
        synchronized (this) {
            this._pattern = pattern;
            this.configure();
        }
    }
    
    public void setTimeZone(final TimeZone timeZone) {
        synchronized (this) {
            this._timeZone = timeZone;
            this.configure();
        }
    }
}
