package org.apache.log4j.lf5.util;

import java.io.*;
import java.net.*;

public class Resource
{
    protected String _name;
    
    public Resource() {
    }
    
    public Resource(final String name) {
        this._name = name;
    }
    
    public InputStream getInputStream() {
        return ResourceUtils.getResourceAsStream(this, this);
    }
    
    public InputStreamReader getInputStreamReader() {
        final InputStream resourceAsStream = ResourceUtils.getResourceAsStream(this, this);
        if (resourceAsStream == null) {
            return null;
        }
        return new InputStreamReader(resourceAsStream);
    }
    
    public String getName() {
        return this._name;
    }
    
    public URL getURL() {
        return ResourceUtils.getResourceAsURL(this, this);
    }
    
    public void setName(final String name) {
        this._name = name;
    }
}
