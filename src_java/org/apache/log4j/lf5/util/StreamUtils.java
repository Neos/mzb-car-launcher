package org.apache.log4j.lf5.util;

import java.io.*;

public abstract class StreamUtils
{
    public static final int DEFAULT_BUFFER_SIZE = 2048;
    
    public static void copy(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        copy(inputStream, outputStream, 2048);
    }
    
    public static void copy(final InputStream inputStream, final OutputStream outputStream, int i) throws IOException {
        byte[] array;
        for (array = new byte[i], i = inputStream.read(array); i != -1; i = inputStream.read(array)) {
            outputStream.write(array, 0, i);
        }
        outputStream.flush();
    }
    
    public static void copyThenClose(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        copy(inputStream, outputStream);
        inputStream.close();
        outputStream.close();
    }
    
    public static byte[] getBytes(final InputStream inputStream) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        copy(inputStream, byteArrayOutputStream);
        byteArrayOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }
}
