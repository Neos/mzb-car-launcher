package org.apache.log4j.lf5.util;

import org.apache.log4j.lf5.viewer.*;
import java.awt.*;
import java.util.*;
import org.apache.log4j.lf5.*;

public class LogMonitorAdapter
{
    public static final int JDK14_LOG_LEVELS = 1;
    public static final int LOG4J_LOG_LEVELS = 0;
    private LogLevel _defaultLevel;
    private LogBrokerMonitor _logMonitor;
    
    private LogMonitorAdapter(final List list) {
        this._defaultLevel = null;
        this._defaultLevel = list.get(0);
        (this._logMonitor = new LogBrokerMonitor(list)).setFrameSize(getDefaultMonitorWidth(), getDefaultMonitorHeight());
        this._logMonitor.setFontSize(12);
        this._logMonitor.show();
    }
    
    protected static int getDefaultMonitorHeight() {
        return getScreenHeight() * 3 / 4;
    }
    
    protected static int getDefaultMonitorWidth() {
        return getScreenWidth() * 3 / 4;
    }
    
    protected static int getScreenHeight() {
        try {
            return Toolkit.getDefaultToolkit().getScreenSize().height;
        }
        catch (Throwable t) {
            return 600;
        }
    }
    
    protected static int getScreenWidth() {
        try {
            return Toolkit.getDefaultToolkit().getScreenSize().width;
        }
        catch (Throwable t) {
            return 800;
        }
    }
    
    public static LogMonitorAdapter newInstance(final int n) {
        if (n == 1) {
            final LogMonitorAdapter instance = newInstance(LogLevel.getJdk14Levels());
            instance.setDefaultLevel(LogLevel.FINEST);
            instance.setSevereLevel(LogLevel.SEVERE);
            return instance;
        }
        final LogMonitorAdapter instance2 = newInstance(LogLevel.getLog4JLevels());
        instance2.setDefaultLevel(LogLevel.DEBUG);
        instance2.setSevereLevel(LogLevel.FATAL);
        return instance2;
    }
    
    public static LogMonitorAdapter newInstance(final List list) {
        return new LogMonitorAdapter(list);
    }
    
    public static LogMonitorAdapter newInstance(final LogLevel[] array) {
        if (array == null) {
            return null;
        }
        return newInstance(Arrays.asList(array));
    }
    
    public void addMessage(final LogRecord logRecord) {
        this._logMonitor.addMessage(logRecord);
    }
    
    public LogLevel getDefaultLevel() {
        return this._defaultLevel;
    }
    
    public LogLevel getSevereLevel() {
        return AdapterLogRecord.getSevereLevel();
    }
    
    public void log(final String s, final String s2) {
        this.log(s, null, s2);
    }
    
    public void log(final String s, final LogLevel logLevel, final String s2) {
        this.log(s, logLevel, s2, null, null);
    }
    
    public void log(final String s, final LogLevel logLevel, final String s2, final String s3) {
        this.log(s, logLevel, s2, null, s3);
    }
    
    public void log(final String s, final LogLevel logLevel, final String s2, final Throwable t) {
        this.log(s, logLevel, s2, t, null);
    }
    
    public void log(final String category, final LogLevel level, final String message, final Throwable thrown, final String ndc) {
        final AdapterLogRecord adapterLogRecord = new AdapterLogRecord();
        adapterLogRecord.setCategory(category);
        adapterLogRecord.setMessage(message);
        adapterLogRecord.setNDC(ndc);
        adapterLogRecord.setThrown(thrown);
        if (level == null) {
            adapterLogRecord.setLevel(this.getDefaultLevel());
        }
        else {
            adapterLogRecord.setLevel(level);
        }
        this.addMessage(adapterLogRecord);
    }
    
    public void setDefaultLevel(final LogLevel defaultLevel) {
        this._defaultLevel = defaultLevel;
    }
    
    public void setMaxNumberOfRecords(final int maxNumberOfLogRecords) {
        this._logMonitor.setMaxNumberOfLogRecords(maxNumberOfLogRecords);
    }
    
    public void setSevereLevel(final LogLevel severeLevel) {
        AdapterLogRecord.setSevereLevel(severeLevel);
    }
}
