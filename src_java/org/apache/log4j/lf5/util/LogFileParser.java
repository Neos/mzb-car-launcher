package org.apache.log4j.lf5.util;

import java.io.*;
import java.text.*;
import org.apache.log4j.lf5.*;
import org.apache.log4j.lf5.viewer.*;

public class LogFileParser implements Runnable
{
    public static final String ATTRIBUTE_DELIMITER = "[slf5s.";
    public static final String CATEGORY_DELIMITER = "[slf5s.CATEGORY]";
    public static final String DATE_DELIMITER = "[slf5s.DATE]";
    public static final String LOCATION_DELIMITER = "[slf5s.LOCATION]";
    public static final String MESSAGE_DELIMITER = "[slf5s.MESSAGE]";
    public static final String NDC_DELIMITER = "[slf5s.NDC]";
    public static final String PRIORITY_DELIMITER = "[slf5s.PRIORITY]";
    public static final String RECORD_DELIMITER = "[slf5s.start]";
    public static final String THREAD_DELIMITER = "[slf5s.THREAD]";
    private static SimpleDateFormat _sdf;
    private InputStream _in;
    LogFactor5LoadingDialog _loadDialog;
    private LogBrokerMonitor _monitor;
    
    static {
        LogFileParser._sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss,S");
    }
    
    public LogFileParser(final File file) throws IOException, FileNotFoundException {
        this(new FileInputStream(file));
    }
    
    public LogFileParser(final InputStream in) throws IOException {
        this._in = null;
        this._in = in;
    }
    
    static void access$000(final LogFileParser logFileParser) {
        logFileParser.destroyDialog();
    }
    
    private LogRecord createLogRecord(final String s) {
        if (s == null || s.trim().length() == 0) {
            return null;
        }
        final Log4JLogRecord log4JLogRecord = new Log4JLogRecord();
        log4JLogRecord.setMillis(this.parseDate(s));
        log4JLogRecord.setLevel(this.parsePriority(s));
        log4JLogRecord.setCategory(this.parseCategory(s));
        log4JLogRecord.setLocation(this.parseLocation(s));
        log4JLogRecord.setThreadDescription(this.parseThread(s));
        log4JLogRecord.setNDC(this.parseNDC(s));
        log4JLogRecord.setMessage(this.parseMessage(s));
        log4JLogRecord.setThrownStackTrace(this.parseThrowable(s));
        return log4JLogRecord;
    }
    
    private void destroyDialog() {
        this._loadDialog.hide();
        this._loadDialog.dispose();
    }
    
    private String getAttribute(final int n, final String s) {
        final int lastIndex = s.lastIndexOf("[slf5s.", n - 1);
        if (lastIndex == -1) {
            return s.substring(0, n);
        }
        return s.substring(s.indexOf("]", lastIndex) + 1, n).trim();
    }
    
    private String loadLogFile(final InputStream inputStream) throws IOException {
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        final int available = bufferedInputStream.available();
        StringBuffer sb;
        if (available > 0) {
            sb = new StringBuffer(available);
        }
        else {
            sb = new StringBuffer(1024);
        }
        while (true) {
            final int read = bufferedInputStream.read();
            if (read == -1) {
                break;
            }
            sb.append((char)read);
        }
        bufferedInputStream.close();
        return sb.toString();
    }
    
    private String parseAttribute(final String s, final String s2) {
        final int index = s2.indexOf(s);
        if (index == -1) {
            return null;
        }
        return this.getAttribute(index, s2);
    }
    
    private String parseCategory(final String s) {
        return this.parseAttribute("[slf5s.CATEGORY]", s);
    }
    
    private long parseDate(String attribute) {
        try {
            attribute = this.parseAttribute("[slf5s.DATE]", attribute);
            if (attribute == null) {
                return 0L;
            }
            return LogFileParser._sdf.parse(attribute).getTime();
        }
        catch (ParseException ex) {
            return 0L;
        }
    }
    
    private String parseLocation(final String s) {
        return this.parseAttribute("[slf5s.LOCATION]", s);
    }
    
    private String parseMessage(final String s) {
        return this.parseAttribute("[slf5s.MESSAGE]", s);
    }
    
    private String parseNDC(final String s) {
        return this.parseAttribute("[slf5s.NDC]", s);
    }
    
    private LogLevel parsePriority(String attribute) {
        attribute = this.parseAttribute("[slf5s.PRIORITY]", attribute);
        if (attribute != null) {
            try {
                return LogLevel.valueOf(attribute);
            }
            catch (LogLevelFormatException ex) {
                return LogLevel.DEBUG;
            }
        }
        return LogLevel.DEBUG;
    }
    
    private String parseThread(final String s) {
        return this.parseAttribute("[slf5s.THREAD]", s);
    }
    
    private String parseThrowable(final String s) {
        return this.getAttribute(s.length(), s);
    }
    
    protected void displayError(final String s) {
        new LogFactor5ErrorDialog(this._monitor.getBaseFrame(), s);
    }
    
    public void parse(final LogBrokerMonitor monitor) throws RuntimeException {
        this._monitor = monitor;
        new Thread(this).start();
    }
    
    @Override
    public void run() {
        int n = 0;
        boolean b = false;
        this._loadDialog = new LogFactor5LoadingDialog(this._monitor.getBaseFrame(), "Loading file...");
        try {
            final String loadLogFile = this.loadLogFile(this._in);
            while (true) {
                final int index = loadLogFile.indexOf("[slf5s.start]", n);
                if (index == -1) {
                    break;
                }
                final LogRecord logRecord = this.createLogRecord(loadLogFile.substring(n, index));
                b = true;
                if (logRecord != null) {
                    this._monitor.addMessage(logRecord);
                }
                n = index + "[slf5s.start]".length();
            }
            if (n < loadLogFile.length() && b) {
                final LogRecord logRecord2 = this.createLogRecord(loadLogFile.substring(n));
                if (logRecord2 != null) {
                    this._monitor.addMessage(logRecord2);
                }
            }
            if (!b) {
                throw new RuntimeException("Invalid log file format");
            }
            goto Label_0162;
        }
        catch (RuntimeException ex) {
            this.destroyDialog();
            this.displayError("Error - Invalid log file format.\nPlease see documentation on how to load log files.");
        }
        catch (IOException ex2) {
            this.destroyDialog();
            this.displayError("Error - Unable to load log file!");
            goto Label_0156;
        }
    }
}
