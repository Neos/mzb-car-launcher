package org.apache.log4j.lf5.util;

import java.io.*;
import java.net.*;

public class ResourceUtils
{
    public static InputStream getResourceAsStream(final Object o, final Resource resource) {
        final ClassLoader classLoader = o.getClass().getClassLoader();
        if (classLoader != null) {
            return classLoader.getResourceAsStream(resource.getName());
        }
        return ClassLoader.getSystemResourceAsStream(resource.getName());
    }
    
    public static URL getResourceAsURL(final Object o, final Resource resource) {
        final ClassLoader classLoader = o.getClass().getClassLoader();
        if (classLoader != null) {
            return classLoader.getResource(resource.getName());
        }
        return ClassLoader.getSystemResource(resource.getName());
    }
}
