package org.apache.log4j.lf5.util;

import org.apache.log4j.lf5.*;
import java.io.*;

public class AdapterLogRecord extends LogRecord
{
    private static PrintWriter pw;
    private static LogLevel severeLevel;
    private static StringWriter sw;
    
    static {
        AdapterLogRecord.severeLevel = null;
        AdapterLogRecord.sw = new StringWriter();
        AdapterLogRecord.pw = new PrintWriter(AdapterLogRecord.sw);
    }
    
    public static LogLevel getSevereLevel() {
        return AdapterLogRecord.severeLevel;
    }
    
    public static void setSevereLevel(final LogLevel severeLevel) {
        AdapterLogRecord.severeLevel = severeLevel;
    }
    
    protected String getLocationInfo(final String s) {
        return this.parseLine(this.stackTraceToString(new Throwable()), s);
    }
    
    @Override
    public boolean isSevereLevel() {
        return AdapterLogRecord.severeLevel != null && AdapterLogRecord.severeLevel.equals(this.getLevel());
    }
    
    protected String parseLine(String substring, final String s) {
        final int index = substring.indexOf(s);
        if (index == -1) {
            return null;
        }
        substring = substring.substring(index);
        return substring.substring(0, substring.indexOf(")") + 1);
    }
    
    @Override
    public void setCategory(final String category) {
        super.setCategory(category);
        super.setLocation(this.getLocationInfo(category));
    }
    
    protected String stackTraceToString(final Throwable t) {
        synchronized (AdapterLogRecord.sw) {
            t.printStackTrace(AdapterLogRecord.pw);
            final String string = AdapterLogRecord.sw.toString();
            AdapterLogRecord.sw.getBuffer().setLength(0);
            return string;
        }
    }
}
