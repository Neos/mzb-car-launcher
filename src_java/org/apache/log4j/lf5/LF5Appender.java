package org.apache.log4j.lf5;

import org.apache.log4j.*;
import org.apache.log4j.lf5.viewer.*;
import java.awt.*;
import org.apache.log4j.spi.*;

public class LF5Appender extends AppenderSkeleton
{
    protected static LogBrokerMonitor _defaultLogMonitor;
    protected static AppenderFinalizer _finalizer;
    protected LogBrokerMonitor _logMonitor;
    
    public LF5Appender() {
        this(getDefaultInstance());
    }
    
    public LF5Appender(final LogBrokerMonitor logMonitor) {
        if (logMonitor != null) {
            this._logMonitor = logMonitor;
        }
    }
    
    protected static LogBrokerMonitor getDefaultInstance() {
        synchronized (LF5Appender.class) {
            Label_0063: {
                if (LF5Appender._defaultLogMonitor != null) {
                    break Label_0063;
                }
                try {
                    LF5Appender._defaultLogMonitor = new LogBrokerMonitor(LogLevel.getLog4JLevels());
                    LF5Appender._finalizer = new AppenderFinalizer(LF5Appender._defaultLogMonitor);
                    LF5Appender._defaultLogMonitor.setFrameSize(getDefaultMonitorWidth(), getDefaultMonitorHeight());
                    LF5Appender._defaultLogMonitor.setFontSize(12);
                    LF5Appender._defaultLogMonitor.show();
                    return LF5Appender._defaultLogMonitor;
                }
                catch (SecurityException ex) {
                    LF5Appender._defaultLogMonitor = null;
                }
            }
        }
    }
    
    protected static int getDefaultMonitorHeight() {
        return getScreenHeight() * 3 / 4;
    }
    
    protected static int getDefaultMonitorWidth() {
        return getScreenWidth() * 3 / 4;
    }
    
    protected static int getScreenHeight() {
        try {
            return Toolkit.getDefaultToolkit().getScreenSize().height;
        }
        catch (Throwable t) {
            return 600;
        }
    }
    
    protected static int getScreenWidth() {
        try {
            return Toolkit.getDefaultToolkit().getScreenSize().width;
        }
        catch (Throwable t) {
            return 800;
        }
    }
    
    public static void main(final String[] array) {
        new LF5Appender();
    }
    
    public void append(final LoggingEvent loggingEvent) {
        final String loggerName = loggingEvent.getLoggerName();
        final String renderedMessage = loggingEvent.getRenderedMessage();
        final String ndc = loggingEvent.getNDC();
        final String threadName = loggingEvent.getThreadName();
        final String string = loggingEvent.getLevel().toString();
        final long timeStamp = loggingEvent.timeStamp;
        final LocationInfo locationInformation = loggingEvent.getLocationInformation();
        final Log4JLogRecord log4JLogRecord = new Log4JLogRecord();
        log4JLogRecord.setCategory(loggerName);
        log4JLogRecord.setMessage(renderedMessage);
        log4JLogRecord.setLocation(locationInformation.fullInfo);
        log4JLogRecord.setMillis(timeStamp);
        log4JLogRecord.setThreadDescription(threadName);
        Label_0145: {
            if (ndc == null) {
                break Label_0145;
            }
            log4JLogRecord.setNDC(ndc);
        Label_0128_Outer:
            while (true) {
                if (loggingEvent.getThrowableInformation() != null) {
                    log4JLogRecord.setThrownStackTrace(loggingEvent.getThrowableInformation());
                }
                while (true) {
                    try {
                        log4JLogRecord.setLevel(LogLevel.valueOf(string));
                        if (this._logMonitor != null) {
                            this._logMonitor.addMessage(log4JLogRecord);
                        }
                        return;
                        log4JLogRecord.setNDC("");
                        continue Label_0128_Outer;
                    }
                    catch (LogLevelFormatException ex) {
                        log4JLogRecord.setLevel(LogLevel.WARN);
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    @Override
    public void close() {
    }
    
    public boolean equals(final LF5Appender lf5Appender) {
        return this._logMonitor == lf5Appender.getLogBrokerMonitor();
    }
    
    public LogBrokerMonitor getLogBrokerMonitor() {
        return this._logMonitor;
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
    
    public void setCallSystemExitOnClose(final boolean callSystemExitOnClose) {
        this._logMonitor.setCallSystemExitOnClose(callSystemExitOnClose);
    }
    
    public void setMaxNumberOfRecords(final int maxNumberOfLogRecords) {
        LF5Appender._defaultLogMonitor.setMaxNumberOfLogRecords(maxNumberOfLogRecords);
    }
}
