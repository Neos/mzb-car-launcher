package org.apache.log4j.lf5;

import java.io.*;
import java.awt.*;
import java.util.*;

public class LogLevel implements Serializable
{
    public static final LogLevel CONFIG;
    public static final LogLevel DEBUG;
    public static final LogLevel ERROR;
    public static final LogLevel FATAL;
    public static final LogLevel FINE;
    public static final LogLevel FINER;
    public static final LogLevel FINEST;
    public static final LogLevel INFO;
    public static final LogLevel SEVERE;
    public static final LogLevel WARN;
    public static final LogLevel WARNING;
    private static LogLevel[] _allDefaultLevels;
    private static LogLevel[] _jdk14Levels;
    private static LogLevel[] _log4JLevels;
    private static Map _logLevelColorMap;
    private static Map _logLevelMap;
    private static Map _registeredLogLevelMap;
    protected String _label;
    protected int _precedence;
    
    static {
        FATAL = new LogLevel("FATAL", 0);
        ERROR = new LogLevel("ERROR", 1);
        WARN = new LogLevel("WARN", 2);
        INFO = new LogLevel("INFO", 3);
        DEBUG = new LogLevel("DEBUG", 4);
        SEVERE = new LogLevel("SEVERE", 1);
        WARNING = new LogLevel("WARNING", 2);
        CONFIG = new LogLevel("CONFIG", 4);
        FINE = new LogLevel("FINE", 5);
        FINER = new LogLevel("FINER", 6);
        FINEST = new LogLevel("FINEST", 7);
        LogLevel._registeredLogLevelMap = new HashMap();
        LogLevel._log4JLevels = new LogLevel[] { LogLevel.FATAL, LogLevel.ERROR, LogLevel.WARN, LogLevel.INFO, LogLevel.DEBUG };
        LogLevel._jdk14Levels = new LogLevel[] { LogLevel.SEVERE, LogLevel.WARNING, LogLevel.INFO, LogLevel.CONFIG, LogLevel.FINE, LogLevel.FINER, LogLevel.FINEST };
        LogLevel._allDefaultLevels = new LogLevel[] { LogLevel.FATAL, LogLevel.ERROR, LogLevel.WARN, LogLevel.INFO, LogLevel.DEBUG, LogLevel.SEVERE, LogLevel.WARNING, LogLevel.CONFIG, LogLevel.FINE, LogLevel.FINER, LogLevel.FINEST };
        LogLevel._logLevelMap = new HashMap();
        for (int i = 0; i < LogLevel._allDefaultLevels.length; ++i) {
            LogLevel._logLevelMap.put(LogLevel._allDefaultLevels[i].getLabel(), LogLevel._allDefaultLevels[i]);
        }
        LogLevel._logLevelColorMap = new HashMap();
        for (int j = 0; j < LogLevel._allDefaultLevels.length; ++j) {
            LogLevel._logLevelColorMap.put(LogLevel._allDefaultLevels[j], Color.black);
        }
    }
    
    public LogLevel(final String label, final int precedence) {
        this._label = label;
        this._precedence = precedence;
    }
    
    public static List getAllDefaultLevels() {
        return Arrays.asList(LogLevel._allDefaultLevels);
    }
    
    public static List getJdk14Levels() {
        return Arrays.asList(LogLevel._jdk14Levels);
    }
    
    public static List getLog4JLevels() {
        return Arrays.asList(LogLevel._log4JLevels);
    }
    
    public static Map getLogLevelColorMap() {
        return LogLevel._logLevelColorMap;
    }
    
    public static LogLevel register(final LogLevel logLevel) {
        if (logLevel != null && LogLevel._logLevelMap.get(logLevel.getLabel()) == null) {
            return LogLevel._registeredLogLevelMap.put(logLevel.getLabel(), logLevel);
        }
        return null;
    }
    
    public static void register(final List list) {
        if (list != null) {
            final Iterator<LogLevel> iterator = list.iterator();
            while (iterator.hasNext()) {
                register(iterator.next());
            }
        }
    }
    
    public static void register(final LogLevel[] array) {
        if (array != null) {
            for (int i = 0; i < array.length; ++i) {
                register(array[i]);
            }
        }
    }
    
    public static void resetLogLevelColorMap() {
        LogLevel._logLevelColorMap.clear();
        for (int i = 0; i < LogLevel._allDefaultLevels.length; ++i) {
            LogLevel._logLevelColorMap.put(LogLevel._allDefaultLevels[i], Color.black);
        }
    }
    
    public static LogLevel valueOf(final String s) throws LogLevelFormatException {
        LogLevel logLevel = null;
        String upperCase = s;
        if (s != null) {
            upperCase = s.trim().toUpperCase();
            logLevel = (LogLevel)LogLevel._logLevelMap.get(upperCase);
        }
        LogLevel logLevel2;
        if ((logLevel2 = logLevel) == null) {
            logLevel2 = logLevel;
            if (LogLevel._registeredLogLevelMap.size() > 0) {
                logLevel2 = LogLevel._registeredLogLevelMap.get(upperCase);
            }
        }
        if (logLevel2 == null) {
            final StringBuffer sb = new StringBuffer();
            sb.append(new StringBuffer().append("Error while trying to parse (").append(upperCase).append(") into").toString());
            sb.append(" a LogLevel.");
            throw new LogLevelFormatException(sb.toString());
        }
        return logLevel2;
    }
    
    public boolean encompasses(final LogLevel logLevel) {
        return logLevel.getPrecedence() <= this.getPrecedence();
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = false;
        if (o instanceof LogLevel) {
            b = b;
            if (this.getPrecedence() == ((LogLevel)o).getPrecedence()) {
                b = true;
            }
        }
        return b;
    }
    
    public String getLabel() {
        return this._label;
    }
    
    protected int getPrecedence() {
        return this._precedence;
    }
    
    @Override
    public int hashCode() {
        return this._label.hashCode();
    }
    
    public void setLogLevelColorMap(final LogLevel logLevel, final Color color) {
        LogLevel._logLevelColorMap.remove(logLevel);
        Color black = color;
        if (color == null) {
            black = Color.black;
        }
        LogLevel._logLevelColorMap.put(logLevel, black);
    }
    
    @Override
    public String toString() {
        return this._label;
    }
}
