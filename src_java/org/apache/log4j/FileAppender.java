package org.apache.log4j;

import java.io.*;
import org.apache.log4j.helpers.*;

public class FileAppender extends WriterAppender
{
    protected int bufferSize;
    protected boolean bufferedIO;
    protected boolean fileAppend;
    protected String fileName;
    
    public FileAppender() {
        this.fileAppend = true;
        this.fileName = null;
        this.bufferedIO = false;
        this.bufferSize = 8192;
    }
    
    public FileAppender(final Layout layout, final String s) throws IOException {
        this(layout, s, true);
    }
    
    public FileAppender(final Layout layout, final String s, final boolean b) throws IOException {
        this.fileAppend = true;
        this.fileName = null;
        this.bufferedIO = false;
        this.bufferSize = 8192;
        this.layout = layout;
        this.setFile(s, b, false, this.bufferSize);
    }
    
    public FileAppender(final Layout layout, final String s, final boolean b, final boolean b2, final int n) throws IOException {
        this.fileAppend = true;
        this.fileName = null;
        this.bufferedIO = false;
        this.bufferSize = 8192;
        this.layout = layout;
        this.setFile(s, b, b2, n);
    }
    
    @Override
    public void activateOptions() {
        if (this.fileName != null) {
            try {
                this.setFile(this.fileName, this.fileAppend, this.bufferedIO, this.bufferSize);
                return;
            }
            catch (IOException ex) {
                this.errorHandler.error(new StringBuffer().append("setFile(").append(this.fileName).append(",").append(this.fileAppend).append(") call failed.").toString(), ex, 4);
                return;
            }
        }
        LogLog.warn(new StringBuffer().append("File option not set for appender [").append(this.name).append("].").toString());
        LogLog.warn("Are you using FileAppender instead of ConsoleAppender?");
    }
    
    protected void closeFile() {
        if (this.qw == null) {
            return;
        }
        try {
            this.qw.close();
        }
        catch (IOException ex) {
            if (ex instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            LogLog.error(new StringBuffer().append("Could not close ").append(this.qw).toString(), ex);
        }
    }
    
    public boolean getAppend() {
        return this.fileAppend;
    }
    
    public int getBufferSize() {
        return this.bufferSize;
    }
    
    public boolean getBufferedIO() {
        return this.bufferedIO;
    }
    
    public String getFile() {
        return this.fileName;
    }
    
    @Override
    protected void reset() {
        this.closeFile();
        this.fileName = null;
        super.reset();
    }
    
    public void setAppend(final boolean fileAppend) {
        this.fileAppend = fileAppend;
    }
    
    public void setBufferSize(final int bufferSize) {
        this.bufferSize = bufferSize;
    }
    
    public void setBufferedIO(final boolean bufferedIO) {
        this.bufferedIO = bufferedIO;
        if (bufferedIO) {
            this.immediateFlush = false;
        }
    }
    
    public void setFile(final String s) {
        this.fileName = s.trim();
    }
    
    public void setFile(final String fileName, final boolean fileAppend, final boolean bufferedIO, final int bufferSize) throws IOException {
        FileOutputStream fileOutputStream = null;
        synchronized (this) {
            LogLog.debug(new StringBuffer().append("setFile called: ").append(fileName).append(", ").append(fileAppend).toString());
            if (bufferedIO) {
                this.setImmediateFlush(false);
            }
            this.reset();
            while (true) {
                try {
                    fileOutputStream = new FileOutputStream(fileName, fileAppend);
                    Writer writer;
                    final OutputStreamWriter outputStreamWriter = (OutputStreamWriter)(writer = this.createWriter(fileOutputStream));
                    if (bufferedIO) {
                        writer = new BufferedWriter(outputStreamWriter, bufferSize);
                    }
                    this.setQWForFiles(writer);
                    this.fileName = fileName;
                    this.fileAppend = fileAppend;
                    this.bufferedIO = bufferedIO;
                    this.bufferSize = bufferSize;
                    this.writeHeader();
                    LogLog.debug("setFile ended");
                    return;
                }
                catch (FileNotFoundException fileOutputStream) {
                    final String parent = new File(fileName).getParent();
                    if (parent == null) {
                        throw fileOutputStream;
                    }
                    final File file = new File(parent);
                    if (!file.exists() && file.mkdirs()) {
                        fileOutputStream = new FileOutputStream(fileName, fileAppend);
                        continue;
                    }
                    throw fileOutputStream;
                }
                break;
            }
        }
        throw fileOutputStream;
    }
    
    protected void setQWForFiles(final Writer writer) {
        this.qw = new QuietWriter(writer, this.errorHandler);
    }
}
