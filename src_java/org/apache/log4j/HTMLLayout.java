package org.apache.log4j;

import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import java.util.*;

public class HTMLLayout extends Layout
{
    public static final String LOCATION_INFO_OPTION = "LocationInfo";
    public static final String TITLE_OPTION = "Title";
    static String TRACE_PREFIX;
    protected final int BUF_SIZE;
    protected final int MAX_CAPACITY;
    boolean locationInfo;
    private StringBuffer sbuf;
    String title;
    
    static {
        HTMLLayout.TRACE_PREFIX = "<br>&nbsp;&nbsp;&nbsp;&nbsp;";
    }
    
    public HTMLLayout() {
        this.BUF_SIZE = 256;
        this.MAX_CAPACITY = 1024;
        this.sbuf = new StringBuffer(256);
        this.locationInfo = false;
        this.title = "Log4J Log Messages";
    }
    
    @Override
    public void activateOptions() {
    }
    
    void appendThrowableAsHTML(final String[] array, final StringBuffer sb) {
        if (array != null) {
            final int length = array.length;
            if (length != 0) {
                sb.append(Transform.escapeTags(array[0]));
                sb.append(Layout.LINE_SEP);
                for (int i = 1; i < length; ++i) {
                    sb.append(HTMLLayout.TRACE_PREFIX);
                    sb.append(Transform.escapeTags(array[i]));
                    sb.append(Layout.LINE_SEP);
                }
            }
        }
    }
    
    @Override
    public String format(final LoggingEvent loggingEvent) {
        if (this.sbuf.capacity() > 1024) {
            this.sbuf = new StringBuffer(256);
        }
        else {
            this.sbuf.setLength(0);
        }
        this.sbuf.append(new StringBuffer().append(Layout.LINE_SEP).append("<tr>").append(Layout.LINE_SEP).toString());
        this.sbuf.append("<td>");
        this.sbuf.append(loggingEvent.timeStamp - LoggingEvent.getStartTime());
        this.sbuf.append(new StringBuffer().append("</td>").append(Layout.LINE_SEP).toString());
        final String escapeTags = Transform.escapeTags(loggingEvent.getThreadName());
        this.sbuf.append(new StringBuffer().append("<td title=\"").append(escapeTags).append(" thread\">").toString());
        this.sbuf.append(escapeTags);
        this.sbuf.append(new StringBuffer().append("</td>").append(Layout.LINE_SEP).toString());
        this.sbuf.append("<td title=\"Level\">");
        if (loggingEvent.getLevel().equals(Level.DEBUG)) {
            this.sbuf.append("<font color=\"#339933\">");
            this.sbuf.append(Transform.escapeTags(String.valueOf(loggingEvent.getLevel())));
            this.sbuf.append("</font>");
        }
        else if (loggingEvent.getLevel().isGreaterOrEqual(Level.WARN)) {
            this.sbuf.append("<font color=\"#993300\"><strong>");
            this.sbuf.append(Transform.escapeTags(String.valueOf(loggingEvent.getLevel())));
            this.sbuf.append("</strong></font>");
        }
        else {
            this.sbuf.append(Transform.escapeTags(String.valueOf(loggingEvent.getLevel())));
        }
        this.sbuf.append(new StringBuffer().append("</td>").append(Layout.LINE_SEP).toString());
        final String escapeTags2 = Transform.escapeTags(loggingEvent.getLoggerName());
        this.sbuf.append(new StringBuffer().append("<td title=\"").append(escapeTags2).append(" category\">").toString());
        this.sbuf.append(escapeTags2);
        this.sbuf.append(new StringBuffer().append("</td>").append(Layout.LINE_SEP).toString());
        if (this.locationInfo) {
            final LocationInfo locationInformation = loggingEvent.getLocationInformation();
            this.sbuf.append("<td>");
            this.sbuf.append(Transform.escapeTags(locationInformation.getFileName()));
            this.sbuf.append(':');
            this.sbuf.append(locationInformation.getLineNumber());
            this.sbuf.append(new StringBuffer().append("</td>").append(Layout.LINE_SEP).toString());
        }
        this.sbuf.append("<td title=\"Message\">");
        this.sbuf.append(Transform.escapeTags(loggingEvent.getRenderedMessage()));
        this.sbuf.append(new StringBuffer().append("</td>").append(Layout.LINE_SEP).toString());
        this.sbuf.append(new StringBuffer().append("</tr>").append(Layout.LINE_SEP).toString());
        if (loggingEvent.getNDC() != null) {
            this.sbuf.append("<tr><td bgcolor=\"#EEEEEE\" style=\"font-size : xx-small;\" colspan=\"6\" title=\"Nested Diagnostic Context\">");
            this.sbuf.append(new StringBuffer().append("NDC: ").append(Transform.escapeTags(loggingEvent.getNDC())).toString());
            this.sbuf.append(new StringBuffer().append("</td></tr>").append(Layout.LINE_SEP).toString());
        }
        final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
        if (throwableStrRep != null) {
            this.sbuf.append("<tr><td bgcolor=\"#993300\" style=\"color:White; font-size : xx-small;\" colspan=\"6\">");
            this.appendThrowableAsHTML(throwableStrRep, this.sbuf);
            this.sbuf.append(new StringBuffer().append("</td></tr>").append(Layout.LINE_SEP).toString());
        }
        return this.sbuf.toString();
    }
    
    @Override
    public String getContentType() {
        return "text/html";
    }
    
    @Override
    public String getFooter() {
        final StringBuffer sb = new StringBuffer();
        sb.append(new StringBuffer().append("</table>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<br>").append(Layout.LINE_SEP).toString());
        sb.append("</body></html>");
        return sb.toString();
    }
    
    @Override
    public String getHeader() {
        final StringBuffer sb = new StringBuffer();
        sb.append(new StringBuffer().append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<html>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<head>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<title>").append(this.title).append("</title>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<style type=\"text/css\">").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<!--").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("body, table {font-family: arial,sans-serif; font-size: x-small;}").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("th {background: #336699; color: #FFFFFF; text-align: left;}").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("-->").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("</style>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("</head>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<body bgcolor=\"#FFFFFF\" topmargin=\"6\" leftmargin=\"6\">").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<hr size=\"1\" noshade>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("Log session start time ").append(new Date()).append("<br>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<br>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<table cellspacing=\"0\" cellpadding=\"4\" border=\"1\" bordercolor=\"#224466\" width=\"100%\">").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<tr>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<th>Time</th>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<th>Thread</th>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<th>Level</th>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("<th>Category</th>").append(Layout.LINE_SEP).toString());
        if (this.locationInfo) {
            sb.append(new StringBuffer().append("<th>File:Line</th>").append(Layout.LINE_SEP).toString());
        }
        sb.append(new StringBuffer().append("<th>Message</th>").append(Layout.LINE_SEP).toString());
        sb.append(new StringBuffer().append("</tr>").append(Layout.LINE_SEP).toString());
        return sb.toString();
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    @Override
    public boolean ignoresThrowable() {
        return false;
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
}
