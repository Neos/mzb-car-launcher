package org.apache.log4j;

import org.apache.log4j.spi.*;

public interface Appender
{
    void addFilter(final Filter p0);
    
    void clearFilters();
    
    void close();
    
    void doAppend(final LoggingEvent p0);
    
    ErrorHandler getErrorHandler();
    
    Filter getFilter();
    
    Layout getLayout();
    
    String getName();
    
    boolean requiresLayout();
    
    void setErrorHandler(final ErrorHandler p0);
    
    void setLayout(final Layout p0);
    
    void setName(final String p0);
}
