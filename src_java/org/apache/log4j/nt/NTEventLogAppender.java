package org.apache.log4j.nt;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;

public class NTEventLogAppender extends AppenderSkeleton
{
    private int _handle;
    private String server;
    private String source;
    
    static {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: anewarray       Ljava/lang/String;
        //     4: astore_2       
        //     5: aload_2        
        //     6: iconst_0       
        //     7: ldc             "os.arch"
        //     9: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    12: aastore        
        //    13: iconst_0       
        //    14: istore_0       
        //    15: iconst_0       
        //    16: istore_1       
        //    17: iload_0        
        //    18: aload_2        
        //    19: arraylength    
        //    20: if_icmpge       49
        //    23: new             Ljava/lang/StringBuffer;
        //    26: dup            
        //    27: invokespecial   java/lang/StringBuffer.<init>:()V
        //    30: ldc             "NTEventLogAppender."
        //    32: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    35: aload_2        
        //    36: iload_0        
        //    37: aaload         
        //    38: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    41: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    44: invokestatic    java/lang/System.loadLibrary:(Ljava/lang/String;)V
        //    47: iconst_1       
        //    48: istore_1       
        //    49: iload_1        
        //    50: ifne            58
        //    53: ldc             "NTEventLogAppender"
        //    55: invokestatic    java/lang/System.loadLibrary:(Ljava/lang/String;)V
        //    58: return         
        //    59: astore_2       
        //    60: iconst_3       
        //    61: anewarray       Ljava/lang/String;
        //    64: astore_2       
        //    65: aload_2        
        //    66: iconst_0       
        //    67: ldc             "amd64"
        //    69: aastore        
        //    70: aload_2        
        //    71: iconst_1       
        //    72: ldc             "ia64"
        //    74: aastore        
        //    75: aload_2        
        //    76: iconst_2       
        //    77: ldc             "x86"
        //    79: aastore        
        //    80: goto            13
        //    83: astore_3       
        //    84: iload_0        
        //    85: iconst_1       
        //    86: iadd           
        //    87: istore_0       
        //    88: goto            15
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  0      13     59     83     Ljava/lang/SecurityException;
        //  23     47     83     91     Ljava/lang/UnsatisfiedLinkError;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public NTEventLogAppender() {
        this(null, null, null);
    }
    
    public NTEventLogAppender(final String s) {
        this(null, s, null);
    }
    
    public NTEventLogAppender(final String s, final String s2) {
        this(s, s2, null);
    }
    
    public NTEventLogAppender(final String s, final String s2, final Layout layout) {
        this._handle = 0;
        this.source = null;
        this.server = null;
        String s3 = s2;
        if (s2 == null) {
            s3 = "Log4j";
        }
        Label_0057: {
            if (layout != null) {
                break Label_0057;
            }
            this.layout = new TTCCLayout();
            try {
                while (true) {
                    this._handle = this.registerEventSource(s, s3);
                    return;
                    this.layout = layout;
                    continue;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
                this._handle = 0;
            }
        }
    }
    
    public NTEventLogAppender(final String s, final Layout layout) {
        this(null, s, layout);
    }
    
    public NTEventLogAppender(final Layout layout) {
        this(null, null, layout);
    }
    
    private native void deregisterEventSource(final int p0);
    
    private native int registerEventSource(final String p0, final String p1);
    
    private native void reportEvent(final int p0, final String p1, final int p2);
    
    @Override
    public void activateOptions() {
        if (this.source == null) {
            return;
        }
        try {
            this._handle = this.registerEventSource(this.server, this.source);
        }
        catch (Exception ex) {
            LogLog.error("Could not register event source.", ex);
            this._handle = 0;
        }
    }
    
    public void append(final LoggingEvent loggingEvent) {
        final StringBuffer sb = new StringBuffer();
        sb.append(this.layout.format(loggingEvent));
        if (this.layout.ignoresThrowable()) {
            final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
            if (throwableStrRep != null) {
                for (int length = throwableStrRep.length, i = 0; i < length; ++i) {
                    sb.append(throwableStrRep[i]);
                }
            }
        }
        this.reportEvent(this._handle, sb.toString(), loggingEvent.getLevel().toInt());
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public void finalize() {
        this.deregisterEventSource(this._handle);
        this._handle = 0;
    }
    
    public String getSource() {
        return this.source;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    public void setSource(final String s) {
        this.source = s.trim();
    }
}
