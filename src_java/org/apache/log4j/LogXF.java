package org.apache.log4j;

import org.apache.log4j.spi.*;

public abstract class LogXF
{
    private static final String FQCN;
    protected static final Level TRACE;
    static Class class$org$apache$log4j$LogXF;
    
    static {
        TRACE = new Level(5000, "TRACE", 7);
        Class class$org$apache$log4j$LogXF;
        if (LogXF.class$org$apache$log4j$LogXF == null) {
            class$org$apache$log4j$LogXF = (LogXF.class$org$apache$log4j$LogXF = class$("org.apache.log4j.LogXF"));
        }
        else {
            class$org$apache$log4j$LogXF = LogXF.class$org$apache$log4j$LogXF;
        }
        FQCN = class$org$apache$log4j$LogXF.getName();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void entering(final Logger logger, final String s, final String s2) {
        if (logger.isDebugEnabled()) {
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, new StringBuffer().append(s).append(".").append(s2).append(" ENTRY").toString(), null));
        }
    }
    
    public static void entering(final Logger logger, String s, String string, final Object o) {
        if (logger.isDebugEnabled()) {
            string = new StringBuffer().append(s).append(".").append(string).append(" ENTRY ").toString();
            if (o == null) {
                s = new StringBuffer().append(string).append("null").toString();
            }
            else {
                try {
                    s = new StringBuffer().append(string).append(o).toString();
                }
                catch (Throwable t) {
                    s = new StringBuffer().append(string).append("?").toString();
                }
            }
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, s, null));
        }
    }
    
    public static void entering(final Logger logger, String string, final String s, final String s2) {
        if (logger.isDebugEnabled()) {
            string = new StringBuffer().append(string).append(".").append(s).append(" ENTRY ").append(s2).toString();
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, string, null));
        }
    }
    
    public static void entering(final Logger logger, String s, String s2, final Object[] array) {
        if (logger.isDebugEnabled()) {
            s = new StringBuffer().append(s).append(".").append(s2).append(" ENTRY ").toString();
            if (array != null && array.length > 0) {
                s2 = "{";
                int i = 0;
            Label_0088_Outer:
                while (i < array.length) {
                    while (true) {
                        try {
                            s = new StringBuffer().append(s).append(s2).append(array[i]).toString();
                            s2 = ",";
                            ++i;
                            continue Label_0088_Outer;
                        }
                        catch (Throwable t) {
                            s = new StringBuffer().append(s).append(s2).append("?").toString();
                            continue;
                        }
                        break;
                    }
                    break;
                }
                s = new StringBuffer().append(s).append("}").toString();
            }
            else {
                s = new StringBuffer().append(s).append("{}").toString();
            }
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, s, null));
        }
    }
    
    public static void exiting(final Logger logger, final String s, final String s2) {
        if (logger.isDebugEnabled()) {
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, new StringBuffer().append(s).append(".").append(s2).append(" RETURN").toString(), null));
        }
    }
    
    public static void exiting(final Logger logger, String s, String string, final Object o) {
        if (logger.isDebugEnabled()) {
            string = new StringBuffer().append(s).append(".").append(string).append(" RETURN ").toString();
            if (o == null) {
                s = new StringBuffer().append(string).append("null").toString();
            }
            else {
                try {
                    s = new StringBuffer().append(string).append(o).toString();
                }
                catch (Throwable t) {
                    s = new StringBuffer().append(string).append("?").toString();
                }
            }
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, s, null));
        }
    }
    
    public static void exiting(final Logger logger, final String s, final String s2, final String s3) {
        if (logger.isDebugEnabled()) {
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, new StringBuffer().append(s).append(".").append(s2).append(" RETURN ").append(s3).toString(), null));
        }
    }
    
    public static void throwing(final Logger logger, final String s, final String s2, final Throwable t) {
        if (logger.isDebugEnabled()) {
            logger.callAppenders(new LoggingEvent(LogXF.FQCN, logger, Level.DEBUG, new StringBuffer().append(s).append(".").append(s2).append(" THROW").toString(), t));
        }
    }
    
    protected static Object[] toArray(final Object o) {
        return new Object[] { o };
    }
    
    protected static Object[] toArray(final Object o, final Object o2) {
        return new Object[] { o, o2 };
    }
    
    protected static Object[] toArray(final Object o, final Object o2, final Object o3) {
        return new Object[] { o, o2, o3 };
    }
    
    protected static Object[] toArray(final Object o, final Object o2, final Object o3, final Object o4) {
        return new Object[] { o, o2, o3, o4 };
    }
    
    protected static Boolean valueOf(final boolean b) {
        if (b) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    
    protected static Byte valueOf(final byte b) {
        return new Byte(b);
    }
    
    protected static Character valueOf(final char c) {
        return new Character(c);
    }
    
    protected static Double valueOf(final double n) {
        return new Double(n);
    }
    
    protected static Float valueOf(final float n) {
        return new Float(n);
    }
    
    protected static Integer valueOf(final int n) {
        return new Integer(n);
    }
    
    protected static Long valueOf(final long n) {
        return new Long(n);
    }
    
    protected static Short valueOf(final short n) {
        return new Short(n);
    }
}
