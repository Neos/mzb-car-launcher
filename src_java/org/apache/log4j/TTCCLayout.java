package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.util.*;
import org.apache.log4j.spi.*;

public class TTCCLayout extends DateLayout
{
    protected final StringBuffer buf;
    private boolean categoryPrefixing;
    private boolean contextPrinting;
    private boolean threadPrinting;
    
    public TTCCLayout() {
        this.threadPrinting = true;
        this.categoryPrefixing = true;
        this.contextPrinting = true;
        this.buf = new StringBuffer(256);
        this.setDateFormat("RELATIVE", null);
    }
    
    public TTCCLayout(final String dateFormat) {
        this.threadPrinting = true;
        this.categoryPrefixing = true;
        this.contextPrinting = true;
        this.buf = new StringBuffer(256);
        this.setDateFormat(dateFormat);
    }
    
    @Override
    public String format(final LoggingEvent loggingEvent) {
        this.buf.setLength(0);
        this.dateFormat(this.buf, loggingEvent);
        if (this.threadPrinting) {
            this.buf.append('[');
            this.buf.append(loggingEvent.getThreadName());
            this.buf.append("] ");
        }
        this.buf.append(loggingEvent.getLevel().toString());
        this.buf.append(' ');
        if (this.categoryPrefixing) {
            this.buf.append(loggingEvent.getLoggerName());
            this.buf.append(' ');
        }
        if (this.contextPrinting) {
            final String ndc = loggingEvent.getNDC();
            if (ndc != null) {
                this.buf.append(ndc);
                this.buf.append(' ');
            }
        }
        this.buf.append("- ");
        this.buf.append(loggingEvent.getRenderedMessage());
        this.buf.append(TTCCLayout.LINE_SEP);
        return this.buf.toString();
    }
    
    public boolean getCategoryPrefixing() {
        return this.categoryPrefixing;
    }
    
    public boolean getContextPrinting() {
        return this.contextPrinting;
    }
    
    public boolean getThreadPrinting() {
        return this.threadPrinting;
    }
    
    @Override
    public boolean ignoresThrowable() {
        return true;
    }
    
    public void setCategoryPrefixing(final boolean categoryPrefixing) {
        this.categoryPrefixing = categoryPrefixing;
    }
    
    public void setContextPrinting(final boolean contextPrinting) {
        this.contextPrinting = contextPrinting;
    }
    
    public void setThreadPrinting(final boolean threadPrinting) {
        this.threadPrinting = threadPrinting;
    }
}
