package org.apache.log4j;

import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.or.*;
import java.util.*;

public class Hierarchy implements LoggerRepository, RendererSupport, ThrowableRendererSupport
{
    private LoggerFactory defaultFactory;
    boolean emittedNoAppenderWarning;
    boolean emittedNoResourceBundleWarning;
    Hashtable ht;
    private Vector listeners;
    RendererMap rendererMap;
    Logger root;
    Level threshold;
    int thresholdInt;
    private ThrowableRenderer throwableRenderer;
    
    public Hierarchy(final Logger root) {
        this.emittedNoAppenderWarning = false;
        this.emittedNoResourceBundleWarning = false;
        this.throwableRenderer = null;
        this.ht = new Hashtable();
        this.listeners = new Vector(1);
        this.root = root;
        this.setThreshold(Level.ALL);
        this.root.setHierarchy(this);
        this.rendererMap = new RendererMap();
        this.defaultFactory = new DefaultCategoryFactory();
    }
    
    private final void updateChildren(final ProvisionNode provisionNode, final Logger parent) {
        for (int size = provisionNode.size(), i = 0; i < size; ++i) {
            final Logger logger = provisionNode.elementAt(i);
            if (!logger.parent.name.startsWith(parent.name)) {
                parent.parent = logger.parent;
                logger.parent = parent;
            }
        }
    }
    
    private final void updateParents(final Logger logger) {
        final String name = logger.name;
        final int length = name.length();
        final boolean b = false;
        int n = name.lastIndexOf(46, length - 1);
        boolean b2;
        while (true) {
            b2 = b;
            if (n < 0) {
                break;
            }
            final CategoryKey categoryKey = new CategoryKey(name.substring(0, n));
            final Object value = this.ht.get(categoryKey);
            if (value == null) {
                this.ht.put(categoryKey, new ProvisionNode(logger));
            }
            else {
                if (value instanceof Category) {
                    b2 = true;
                    logger.parent = (Category)value;
                    break;
                }
                if (value instanceof ProvisionNode) {
                    ((ProvisionNode)value).addElement(logger);
                }
                else {
                    new IllegalStateException(new StringBuffer().append("unexpected object type ").append(((ProvisionNode)value).getClass()).append(" in ht.").toString()).printStackTrace();
                }
            }
            n = name.lastIndexOf(46, n - 1);
        }
        if (!b2) {
            logger.parent = this.root;
        }
    }
    
    @Override
    public void addHierarchyEventListener(final HierarchyEventListener hierarchyEventListener) {
        if (this.listeners.contains(hierarchyEventListener)) {
            LogLog.warn("Ignoring attempt to add an existent listener.");
            return;
        }
        this.listeners.addElement(hierarchyEventListener);
    }
    
    public void addRenderer(final Class clazz, final ObjectRenderer objectRenderer) {
        this.rendererMap.put(clazz, objectRenderer);
    }
    
    public void clear() {
        this.ht.clear();
    }
    
    @Override
    public void emitNoAppenderWarning(final Category category) {
        if (!this.emittedNoAppenderWarning) {
            LogLog.warn(new StringBuffer().append("No appenders could be found for logger (").append(category.getName()).append(").").toString());
            LogLog.warn("Please initialize the log4j system properly.");
            LogLog.warn("See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.");
            this.emittedNoAppenderWarning = true;
        }
    }
    
    @Override
    public Logger exists(final String s) {
        final Logger value = this.ht.get(new CategoryKey(s));
        if (value instanceof Logger) {
            return value;
        }
        return null;
    }
    
    @Override
    public void fireAddAppenderEvent(final Category category, final Appender appender) {
        if (this.listeners != null) {
            for (int size = this.listeners.size(), i = 0; i < size; ++i) {
                ((HierarchyEventListener)this.listeners.elementAt(i)).addAppenderEvent(category, appender);
            }
        }
    }
    
    void fireRemoveAppenderEvent(final Category category, final Appender appender) {
        if (this.listeners != null) {
            for (int size = this.listeners.size(), i = 0; i < size; ++i) {
                ((HierarchyEventListener)this.listeners.elementAt(i)).removeAppenderEvent(category, appender);
            }
        }
    }
    
    @Override
    public Enumeration getCurrentCategories() {
        return this.getCurrentLoggers();
    }
    
    @Override
    public Enumeration getCurrentLoggers() {
        final Vector<Object> vector = new Vector<Object>(this.ht.size());
        final Enumeration<Object> elements = this.ht.elements();
        while (elements.hasMoreElements()) {
            final Object nextElement = elements.nextElement();
            if (nextElement instanceof Logger) {
                vector.addElement(nextElement);
            }
        }
        return vector.elements();
    }
    
    @Override
    public Logger getLogger(final String s) {
        return this.getLogger(s, this.defaultFactory);
    }
    
    @Override
    public Logger getLogger(final String s, final LoggerFactory loggerFactory) {
        final CategoryKey categoryKey = new CategoryKey(s);
        synchronized (this.ht) {
            final Object value = this.ht.get(categoryKey);
            if (value == null) {
                final Logger newLoggerInstance = loggerFactory.makeNewLoggerInstance(s);
                newLoggerInstance.setHierarchy(this);
                this.ht.put(categoryKey, newLoggerInstance);
                this.updateParents(newLoggerInstance);
                return newLoggerInstance;
            }
            if (value instanceof Logger) {
                return (Logger)value;
            }
            if (value instanceof ProvisionNode) {
                final Logger newLoggerInstance2 = loggerFactory.makeNewLoggerInstance(s);
                newLoggerInstance2.setHierarchy(this);
                this.ht.put(categoryKey, newLoggerInstance2);
                this.updateChildren((ProvisionNode)value, newLoggerInstance2);
                this.updateParents(newLoggerInstance2);
                return newLoggerInstance2;
            }
        }
        // monitorexit(hashtable)
        return null;
    }
    
    @Override
    public RendererMap getRendererMap() {
        return this.rendererMap;
    }
    
    @Override
    public Logger getRootLogger() {
        return this.root;
    }
    
    @Override
    public Level getThreshold() {
        return this.threshold;
    }
    
    @Override
    public ThrowableRenderer getThrowableRenderer() {
        return this.throwableRenderer;
    }
    
    @Override
    public boolean isDisabled(final int n) {
        return this.thresholdInt > n;
    }
    
    public void overrideAsNeeded(final String s) {
        LogLog.warn("The Hiearchy.overrideAsNeeded method has been deprecated.");
    }
    
    @Override
    public void resetConfiguration() {
        this.getRootLogger().setLevel(Level.DEBUG);
        this.root.setResourceBundle(null);
        this.setThreshold(Level.ALL);
        synchronized (this.ht) {
            this.shutdown();
            final Enumeration currentLoggers = this.getCurrentLoggers();
            while (currentLoggers.hasMoreElements()) {
                final Logger logger = currentLoggers.nextElement();
                logger.setLevel(null);
                logger.setAdditivity(true);
                logger.setResourceBundle(null);
            }
        }
        // monitorexit(hashtable)
        this.rendererMap.clear();
        this.throwableRenderer = null;
    }
    
    public void setDisableOverride(final String s) {
        LogLog.warn("The Hiearchy.setDisableOverride method has been deprecated.");
    }
    
    @Override
    public void setRenderer(final Class clazz, final ObjectRenderer objectRenderer) {
        this.rendererMap.put(clazz, objectRenderer);
    }
    
    @Override
    public void setThreshold(final String s) {
        final Level level = Level.toLevel(s, null);
        if (level != null) {
            this.setThreshold(level);
            return;
        }
        LogLog.warn(new StringBuffer().append("Could not convert [").append(s).append("] to Level.").toString());
    }
    
    @Override
    public void setThreshold(final Level threshold) {
        if (threshold != null) {
            this.thresholdInt = threshold.level;
            this.threshold = threshold;
        }
    }
    
    @Override
    public void setThrowableRenderer(final ThrowableRenderer throwableRenderer) {
        this.throwableRenderer = throwableRenderer;
    }
    
    @Override
    public void shutdown() {
        this.getRootLogger().closeNestedAppenders();
        synchronized (this.ht) {
            final Enumeration currentLoggers = this.getCurrentLoggers();
            while (currentLoggers.hasMoreElements()) {
                currentLoggers.nextElement().closeNestedAppenders();
            }
        }
        final Category category;
        category.removeAllAppenders();
        final Enumeration currentLoggers2 = this.getCurrentLoggers();
        while (currentLoggers2.hasMoreElements()) {
            currentLoggers2.nextElement().removeAllAppenders();
        }
    }
    // monitorexit(hashtable)
}
