package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.util.*;

public class NDC
{
    static final int REAP_THRESHOLD = 5;
    static Hashtable ht;
    static int pushCounter;
    
    static {
        NDC.ht = new Hashtable();
        NDC.pushCounter = 0;
    }
    
    public static void clear() {
        final Stack currentStack = getCurrentStack();
        if (currentStack != null) {
            currentStack.setSize(0);
        }
    }
    
    public static Stack cloneStack() {
        final Stack currentStack = getCurrentStack();
        if (currentStack == null) {
            return null;
        }
        return (Stack)currentStack.clone();
    }
    
    public static String get() {
        final Stack currentStack = getCurrentStack();
        if (currentStack != null && !currentStack.isEmpty()) {
            return currentStack.peek().fullMessage;
        }
        return null;
    }
    
    private static Stack getCurrentStack() {
        if (NDC.ht != null) {
            return NDC.ht.get(Thread.currentThread());
        }
        return null;
    }
    
    public static int getDepth() {
        final Stack currentStack = getCurrentStack();
        if (currentStack == null) {
            return 0;
        }
        return currentStack.size();
    }
    
    public static void inherit(final Stack stack) {
        if (stack != null) {
            NDC.ht.put(Thread.currentThread(), stack);
        }
    }
    
    private static void lazyRemove() {
        if (NDC.ht != null) {
            synchronized (NDC.ht) {
                if (++NDC.pushCounter <= 5) {
                    return;
                }
            }
            NDC.pushCounter = 0;
            int n = 0;
            final Vector<Thread> vector = new Vector<Thread>();
            final Enumeration keys = NDC.ht.keys();
            while (keys.hasMoreElements() && n <= 4) {
                final Thread thread = keys.nextElement();
                if (thread.isAlive()) {
                    ++n;
                }
                else {
                    n = 0;
                    vector.addElement(thread);
                }
            }
            // monitorexit(hashtable)
            for (int size = vector.size(), i = 0; i < size; ++i) {
                final Thread thread2 = vector.elementAt(i);
                LogLog.debug(new StringBuffer().append("Lazy NDC removal for thread [").append(thread2.getName()).append("] (").append(NDC.ht.size()).append(").").toString());
                NDC.ht.remove(thread2);
            }
        }
    }
    
    public static String peek() {
        final Stack currentStack = getCurrentStack();
        if (currentStack != null && !currentStack.isEmpty()) {
            return currentStack.peek().message;
        }
        return "";
    }
    
    public static String pop() {
        final Stack currentStack = getCurrentStack();
        if (currentStack != null && !currentStack.isEmpty()) {
            return currentStack.pop().message;
        }
        return "";
    }
    
    public static void push(final String s) {
        final Stack currentStack = getCurrentStack();
        if (currentStack == null) {
            final DiagnosticContext diagnosticContext = new DiagnosticContext(s, null);
            final Stack<DiagnosticContext> stack = new Stack<DiagnosticContext>();
            NDC.ht.put(Thread.currentThread(), stack);
            stack.push(diagnosticContext);
            return;
        }
        if (currentStack.isEmpty()) {
            currentStack.push(new DiagnosticContext(s, null));
            return;
        }
        currentStack.push(new DiagnosticContext(s, currentStack.peek()));
    }
    
    public static void remove() {
        if (NDC.ht != null) {
            NDC.ht.remove(Thread.currentThread());
            lazyRemove();
        }
    }
    
    public static void setMaxDepth(final int size) {
        final Stack currentStack = getCurrentStack();
        if (currentStack != null && size < currentStack.size()) {
            currentStack.setSize(size);
        }
    }
    
    private static class DiagnosticContext
    {
        String fullMessage;
        String message;
        
        DiagnosticContext(final String s, final DiagnosticContext diagnosticContext) {
            this.message = s;
            if (diagnosticContext != null) {
                this.fullMessage = new StringBuffer().append(diagnosticContext.fullMessage).append(' ').append(s).toString();
                return;
            }
            this.fullMessage = s;
        }
    }
}
