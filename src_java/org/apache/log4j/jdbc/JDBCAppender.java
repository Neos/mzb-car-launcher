package org.apache.log4j.jdbc;

import org.apache.log4j.spi.*;
import java.util.*;
import java.sql.*;
import org.apache.log4j.*;

public class JDBCAppender extends AppenderSkeleton implements Appender
{
    protected ArrayList buffer;
    protected int bufferSize;
    protected Connection connection;
    protected String databasePassword;
    protected String databaseURL;
    protected String databaseUser;
    private boolean locationInfo;
    protected ArrayList removes;
    protected String sqlStatement;
    
    public JDBCAppender() {
        this.databaseURL = "jdbc:odbc:myDB";
        this.databaseUser = "me";
        this.databasePassword = "mypassword";
        this.connection = null;
        this.sqlStatement = "";
        this.bufferSize = 1;
        this.locationInfo = false;
        this.buffer = new ArrayList(this.bufferSize);
        this.removes = new ArrayList(this.bufferSize);
    }
    
    public void append(final LoggingEvent loggingEvent) {
        loggingEvent.getNDC();
        loggingEvent.getThreadName();
        loggingEvent.getMDCCopy();
        if (this.locationInfo) {
            loggingEvent.getLocationInformation();
        }
        loggingEvent.getRenderedMessage();
        loggingEvent.getThrowableStrRep();
        this.buffer.add(loggingEvent);
        if (this.buffer.size() >= this.bufferSize) {
            this.flushBuffer();
        }
    }
    
    @Override
    public void close() {
        this.flushBuffer();
        while (true) {
            try {
                if (this.connection != null && !this.connection.isClosed()) {
                    this.connection.close();
                }
                this.closed = true;
            }
            catch (SQLException ex) {
                this.errorHandler.error("Error closing connection", ex, 0);
                continue;
            }
            break;
        }
    }
    
    protected void closeConnection(final Connection connection) {
    }
    
    protected void execute(final String s) throws SQLException {
        Connection connection = null;
        Statement statement2;
        final Statement statement = statement2 = null;
        try {
            final Connection connection2 = connection = this.getConnection();
            statement2 = statement;
            final Statement statement3 = connection2.createStatement();
            connection = connection2;
            statement2 = statement3;
            statement3.executeUpdate(s);
        }
        finally {
            if (statement2 != null) {
                statement2.close();
            }
            this.closeConnection(connection);
        }
    }
    
    @Override
    public void finalize() {
        this.close();
    }
    
    public void flushBuffer() {
        this.removes.ensureCapacity(this.buffer.size());
        for (final LoggingEvent loggingEvent : this.buffer) {
            try {
                this.execute(this.getLogStatement(loggingEvent));
                continue;
            }
            catch (SQLException ex) {
                this.errorHandler.error("Failed to excute sql", ex, 2);
                continue;
            }
            finally {
                this.removes.add(loggingEvent);
            }
            break;
        }
        this.buffer.removeAll(this.removes);
        this.removes.clear();
    }
    
    public int getBufferSize() {
        return this.bufferSize;
    }
    
    protected Connection getConnection() throws SQLException {
        if (!DriverManager.getDrivers().hasMoreElements()) {
            this.setDriver("sun.jdbc.odbc.JdbcOdbcDriver");
        }
        if (this.connection == null) {
            this.connection = DriverManager.getConnection(this.databaseURL, this.databaseUser, this.databasePassword);
        }
        return this.connection;
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    protected String getLogStatement(final LoggingEvent loggingEvent) {
        return this.getLayout().format(loggingEvent);
    }
    
    public String getPassword() {
        return this.databasePassword;
    }
    
    public String getSql() {
        return this.sqlStatement;
    }
    
    public String getURL() {
        return this.databaseURL;
    }
    
    public String getUser() {
        return this.databaseUser;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    public void setBufferSize(final int bufferSize) {
        this.bufferSize = bufferSize;
        this.buffer.ensureCapacity(this.bufferSize);
        this.removes.ensureCapacity(this.bufferSize);
    }
    
    public void setDriver(final String s) {
        try {
            Class.forName(s);
        }
        catch (Exception ex) {
            this.errorHandler.error("Failed to load driver", ex, 0);
        }
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setPassword(final String databasePassword) {
        this.databasePassword = databasePassword;
    }
    
    public void setSql(final String s) {
        this.sqlStatement = s;
        if (this.getLayout() == null) {
            this.setLayout(new PatternLayout(s));
            return;
        }
        ((PatternLayout)this.getLayout()).setConversionPattern(s);
    }
    
    public void setURL(final String databaseURL) {
        this.databaseURL = databaseURL;
    }
    
    public void setUser(final String databaseUser) {
        this.databaseUser = databaseUser;
    }
}
