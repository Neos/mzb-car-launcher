package org.apache.log4j;

import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;

public abstract class AppenderSkeleton implements Appender, OptionHandler
{
    protected boolean closed;
    protected ErrorHandler errorHandler;
    protected Filter headFilter;
    protected Layout layout;
    protected String name;
    protected Filter tailFilter;
    protected Priority threshold;
    
    public AppenderSkeleton() {
        this.errorHandler = new OnlyOnceErrorHandler();
        this.closed = false;
    }
    
    protected AppenderSkeleton(final boolean b) {
        this.errorHandler = new OnlyOnceErrorHandler();
        this.closed = false;
    }
    
    @Override
    public void activateOptions() {
    }
    
    @Override
    public void addFilter(final Filter filter) {
        if (this.headFilter == null) {
            this.tailFilter = filter;
            this.headFilter = filter;
            return;
        }
        this.tailFilter.setNext(filter);
        this.tailFilter = filter;
    }
    
    protected abstract void append(final LoggingEvent p0);
    
    @Override
    public void clearFilters() {
        this.tailFilter = null;
        this.headFilter = null;
    }
    
    @Override
    public void doAppend(final LoggingEvent loggingEvent) {
    Label_0058_Outer:
        while (true) {
            while (true) {
                Label_0113: {
                    synchronized (this) {
                        if (this.closed) {
                            LogLog.error(new StringBuffer().append("Attempted to append to closed appender named [").append(this.name).append("].").toString());
                        }
                        else if (this.isAsSevereAsThreshold(loggingEvent.getLevel())) {
                            Filter filter = this.headFilter;
                        Label_0100:
                            while (filter != null) {
                                switch (filter.decide(loggingEvent)) {
                                    case -1: {
                                        return;
                                    }
                                    case 0: {
                                        filter = filter.getNext();
                                        continue Label_0058_Outer;
                                    }
                                    case 1: {
                                        break Label_0100;
                                    }
                                    default: {
                                        break Label_0113;
                                    }
                                }
                            }
                            this.append(loggingEvent);
                        }
                        return;
                    }
                }
                continue;
            }
        }
    }
    
    public void finalize() {
        if (this.closed) {
            return;
        }
        LogLog.debug(new StringBuffer().append("Finalizing appender named [").append(this.name).append("].").toString());
        this.close();
    }
    
    @Override
    public ErrorHandler getErrorHandler() {
        return this.errorHandler;
    }
    
    @Override
    public Filter getFilter() {
        return this.headFilter;
    }
    
    public final Filter getFirstFilter() {
        return this.headFilter;
    }
    
    @Override
    public Layout getLayout() {
        return this.layout;
    }
    
    @Override
    public final String getName() {
        return this.name;
    }
    
    public Priority getThreshold() {
        return this.threshold;
    }
    
    public boolean isAsSevereAsThreshold(final Priority priority) {
        return this.threshold == null || priority.isGreaterOrEqual(this.threshold);
    }
    
    @Override
    public void setErrorHandler(final ErrorHandler errorHandler) {
        // monitorenter(this)
        Label_0014: {
            if (errorHandler != null) {
                break Label_0014;
            }
            try {
                LogLog.warn("You have tried to set a null error-handler.");
                return;
                this.errorHandler = errorHandler;
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    @Override
    public void setLayout(final Layout layout) {
        this.layout = layout;
    }
    
    @Override
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setThreshold(final Priority threshold) {
        this.threshold = threshold;
    }
}
