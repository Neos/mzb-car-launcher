package org.apache.log4j;

class NameValue
{
    String key;
    String value;
    
    public NameValue(final String key, final String value) {
        this.key = key;
        this.value = value;
    }
    
    @Override
    public String toString() {
        return new StringBuffer().append(this.key).append("=").append(this.value).toString();
    }
}
