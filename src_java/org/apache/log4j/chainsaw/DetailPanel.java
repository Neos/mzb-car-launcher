package org.apache.log4j.chainsaw;

import java.text.*;
import org.apache.log4j.*;
import javax.swing.border.*;
import java.awt.*;
import javax.swing.event.*;
import javax.swing.*;
import java.util.*;

class DetailPanel extends JPanel implements ListSelectionListener
{
    private static final MessageFormat FORMATTER;
    private static final Logger LOG;
    static Class class$org$apache$log4j$chainsaw$DetailPanel;
    private final JEditorPane mDetails;
    private final MyTableModel mModel;
    
    static {
        Class class$org$apache$log4j$chainsaw$DetailPanel;
        if (DetailPanel.class$org$apache$log4j$chainsaw$DetailPanel == null) {
            class$org$apache$log4j$chainsaw$DetailPanel = (DetailPanel.class$org$apache$log4j$chainsaw$DetailPanel = class$("org.apache.log4j.chainsaw.DetailPanel"));
        }
        else {
            class$org$apache$log4j$chainsaw$DetailPanel = DetailPanel.class$org$apache$log4j$chainsaw$DetailPanel;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$DetailPanel);
        FORMATTER = new MessageFormat("<b>Time:</b> <code>{0,time,medium}</code>&nbsp;&nbsp;<b>Priority:</b> <code>{1}</code>&nbsp;&nbsp;<b>Thread:</b> <code>{2}</code>&nbsp;&nbsp;<b>NDC:</b> <code>{3}</code><br><b>Logger:</b> <code>{4}</code><br><b>Location:</b> <code>{5}</code><br><b>Message:</b><pre>{6}</pre><b>Throwable:</b><pre>{7}</pre>");
    }
    
    DetailPanel(final JTable table, final MyTableModel mModel) {
        this.mModel = mModel;
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createTitledBorder("Details: "));
        (this.mDetails = new JEditorPane()).setEditable(false);
        this.mDetails.setContentType("text/html");
        this.add(new JScrollPane(this.mDetails), "Center");
        table.getSelectionModel().addListSelectionListener(this);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private String escape(final String s) {
        if (s == null) {
            return null;
        }
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            switch (char1) {
                default: {
                    sb.append(char1);
                    break;
                }
                case 60: {
                    sb.append("&lt;");
                    break;
                }
                case 62: {
                    sb.append("&gt;");
                    break;
                }
                case 34: {
                    sb.append("&quot;");
                    break;
                }
                case 38: {
                    sb.append("&amp;");
                    break;
                }
            }
        }
        return sb.toString();
    }
    
    private static String getThrowableStrRep(final EventDetails eventDetails) {
        final String[] throwableStrRep = eventDetails.getThrowableStrRep();
        if (throwableStrRep == null) {
            return null;
        }
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < throwableStrRep.length; ++i) {
            sb.append(throwableStrRep[i]).append("\n");
        }
        return sb.toString();
    }
    
    @Override
    public void valueChanged(final ListSelectionEvent listSelectionEvent) {
        if (listSelectionEvent.getValueIsAdjusting()) {
            return;
        }
        final ListSelectionModel listSelectionModel = (ListSelectionModel)listSelectionEvent.getSource();
        if (listSelectionModel.isSelectionEmpty()) {
            this.mDetails.setText("Nothing selected");
            return;
        }
        final EventDetails eventDetails = this.mModel.getEventDetails(listSelectionModel.getMinSelectionIndex());
        this.mDetails.setText(DetailPanel.FORMATTER.format(new Object[] { new Date(eventDetails.getTimeStamp()), eventDetails.getPriority(), this.escape(eventDetails.getThreadName()), this.escape(eventDetails.getNDC()), this.escape(eventDetails.getCategoryName()), this.escape(eventDetails.getLocationDetails()), this.escape(eventDetails.getMessage()), this.escape(getThrowableStrRep(eventDetails)) }));
        this.mDetails.setCaretPosition(0);
    }
}
