package org.apache.log4j.chainsaw;

import org.xml.sax.helpers.*;
import org.apache.log4j.*;
import java.util.*;
import org.xml.sax.*;

class XMLFileHandler extends DefaultHandler
{
    private static final String TAG_EVENT = "log4j:event";
    private static final String TAG_LOCATION_INFO = "log4j:locationInfo";
    private static final String TAG_MESSAGE = "log4j:message";
    private static final String TAG_NDC = "log4j:NDC";
    private static final String TAG_THROWABLE = "log4j:throwable";
    private final StringBuffer mBuf;
    private String mCategoryName;
    private Level mLevel;
    private String mLocationDetails;
    private String mMessage;
    private final MyTableModel mModel;
    private String mNDC;
    private int mNumEvents;
    private String mThreadName;
    private String[] mThrowableStrRep;
    private long mTimeStamp;
    
    XMLFileHandler(final MyTableModel mModel) {
        this.mBuf = new StringBuffer();
        this.mModel = mModel;
    }
    
    private void addEvent() {
        this.mModel.addEvent(new EventDetails(this.mTimeStamp, this.mLevel, this.mCategoryName, this.mNDC, this.mThreadName, this.mMessage, this.mThrowableStrRep, this.mLocationDetails));
        ++this.mNumEvents;
    }
    
    private void resetData() {
        this.mTimeStamp = 0L;
        this.mLevel = null;
        this.mCategoryName = null;
        this.mNDC = null;
        this.mThreadName = null;
        this.mMessage = null;
        this.mThrowableStrRep = null;
        this.mLocationDetails = null;
    }
    
    @Override
    public void characters(final char[] array, final int n, final int n2) {
        this.mBuf.append(String.valueOf(array, n, n2));
    }
    
    @Override
    public void endElement(final String s, final String s2, final String s3) {
        if ("log4j:event".equals(s3)) {
            this.addEvent();
            this.resetData();
        }
        else {
            if ("log4j:NDC".equals(s3)) {
                this.mNDC = this.mBuf.toString();
                return;
            }
            if ("log4j:message".equals(s3)) {
                this.mMessage = this.mBuf.toString();
                return;
            }
            if ("log4j:throwable".equals(s3)) {
                final StringTokenizer stringTokenizer = new StringTokenizer(this.mBuf.toString(), "\n\t");
                this.mThrowableStrRep = new String[stringTokenizer.countTokens()];
                if (this.mThrowableStrRep.length > 0) {
                    this.mThrowableStrRep[0] = stringTokenizer.nextToken();
                    for (int i = 1; i < this.mThrowableStrRep.length; ++i) {
                        this.mThrowableStrRep[i] = new StringBuffer().append("\t").append(stringTokenizer.nextToken()).toString();
                    }
                }
            }
        }
    }
    
    int getNumEvents() {
        return this.mNumEvents;
    }
    
    @Override
    public void startDocument() throws SAXException {
        this.mNumEvents = 0;
    }
    
    @Override
    public void startElement(final String s, final String s2, final String s3, final Attributes attributes) {
        this.mBuf.setLength(0);
        if ("log4j:event".equals(s3)) {
            this.mThreadName = attributes.getValue("thread");
            this.mTimeStamp = Long.parseLong(attributes.getValue("timestamp"));
            this.mCategoryName = attributes.getValue("logger");
            this.mLevel = Level.toLevel(attributes.getValue("level"));
        }
        else if ("log4j:locationInfo".equals(s3)) {
            this.mLocationDetails = new StringBuffer().append(attributes.getValue("class")).append(".").append(attributes.getValue("method")).append("(").append(attributes.getValue("file")).append(":").append(attributes.getValue("line")).append(")").toString();
        }
    }
}
