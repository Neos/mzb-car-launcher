package org.apache.log4j.chainsaw;

import javax.swing.border.*;
import java.awt.*;
import org.apache.log4j.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.*;

class ControlPanel extends JPanel
{
    private static final Logger LOG;
    static Class class$org$apache$log4j$chainsaw$ControlPanel;
    
    static {
        Class class$org$apache$log4j$chainsaw$ControlPanel;
        if (ControlPanel.class$org$apache$log4j$chainsaw$ControlPanel == null) {
            class$org$apache$log4j$chainsaw$ControlPanel = (ControlPanel.class$org$apache$log4j$chainsaw$ControlPanel = class$("org.apache.log4j.chainsaw.ControlPanel"));
        }
        else {
            class$org$apache$log4j$chainsaw$ControlPanel = ControlPanel.class$org$apache$log4j$chainsaw$ControlPanel;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$ControlPanel);
    }
    
    ControlPanel(final MyTableModel myTableModel) {
        this.setBorder(BorderFactory.createTitledBorder("Controls: "));
        final GridBagLayout layout = new GridBagLayout();
        final GridBagConstraints gridBagConstraints = new GridBagConstraints();
        this.setLayout(layout);
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.gridy = 0;
        final JLabel label = new JLabel("Filter Level:");
        layout.setConstraints(label, gridBagConstraints);
        this.add(label);
        ++gridBagConstraints.gridy;
        final JLabel label2 = new JLabel("Filter Thread:");
        layout.setConstraints(label2, gridBagConstraints);
        this.add(label2);
        ++gridBagConstraints.gridy;
        final JLabel label3 = new JLabel("Filter Logger:");
        layout.setConstraints(label3, gridBagConstraints);
        this.add(label3);
        ++gridBagConstraints.gridy;
        final JLabel label4 = new JLabel("Filter NDC:");
        layout.setConstraints(label4, gridBagConstraints);
        this.add(label4);
        ++gridBagConstraints.gridy;
        final JLabel label5 = new JLabel("Filter Message:");
        layout.setConstraints(label5, gridBagConstraints);
        this.add(label5);
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.gridy = 0;
        final Level[] array = { Level.FATAL, Level.ERROR, Level.WARN, Level.INFO, Level.DEBUG, Level.TRACE };
        final JComboBox comboBox = new JComboBox(array);
        final Level level = array[array.length - 1];
        comboBox.setSelectedItem(level);
        myTableModel.setPriorityFilter(level);
        layout.setConstraints(comboBox, gridBagConstraints);
        this.add(comboBox);
        comboBox.setEditable(false);
        comboBox.addActionListener(new ControlPanel$1(this, myTableModel, comboBox));
        gridBagConstraints.fill = 2;
        ++gridBagConstraints.gridy;
        final JTextField textField = new JTextField("");
        textField.getDocument().addDocumentListener(new ControlPanel$2(this, myTableModel, textField));
        layout.setConstraints(textField, gridBagConstraints);
        this.add(textField);
        ++gridBagConstraints.gridy;
        final JTextField textField2 = new JTextField("");
        textField2.getDocument().addDocumentListener(new ControlPanel$3(this, myTableModel, textField2));
        layout.setConstraints(textField2, gridBagConstraints);
        this.add(textField2);
        ++gridBagConstraints.gridy;
        final JTextField textField3 = new JTextField("");
        textField3.getDocument().addDocumentListener(new ControlPanel$4(this, myTableModel, textField3));
        layout.setConstraints(textField3, gridBagConstraints);
        this.add(textField3);
        ++gridBagConstraints.gridy;
        final JTextField textField4 = new JTextField("");
        textField4.getDocument().addDocumentListener(new ControlPanel$5(this, myTableModel, textField4));
        layout.setConstraints(textField4, gridBagConstraints);
        this.add(textField4);
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        final JButton button = new JButton("Exit");
        button.setMnemonic('x');
        button.addActionListener(ExitAction.INSTANCE);
        layout.setConstraints(button, gridBagConstraints);
        this.add(button);
        ++gridBagConstraints.gridy;
        final JButton button2 = new JButton("Clear");
        button2.setMnemonic('c');
        button2.addActionListener(new ControlPanel$6(this, myTableModel));
        layout.setConstraints(button2, gridBagConstraints);
        this.add(button2);
        ++gridBagConstraints.gridy;
        final JButton button3 = new JButton("Pause");
        button3.setMnemonic('p');
        button3.addActionListener(new ControlPanel$7(this, myTableModel, button3));
        layout.setConstraints(button3, gridBagConstraints);
        this.add(button3);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
}
