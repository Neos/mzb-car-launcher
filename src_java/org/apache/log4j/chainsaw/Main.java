package org.apache.log4j.chainsaw;

import javax.swing.table.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import javax.accessibility.*;
import java.util.*;
import org.apache.log4j.*;
import javax.swing.*;

public class Main extends JFrame
{
    private static final int DEFAULT_PORT = 4445;
    private static final Logger LOG;
    public static final String PORT_PROP_NAME = "chainsaw.port";
    static Class class$org$apache$log4j$chainsaw$Main;
    
    static {
        Class class$org$apache$log4j$chainsaw$Main;
        if (Main.class$org$apache$log4j$chainsaw$Main == null) {
            class$org$apache$log4j$chainsaw$Main = (Main.class$org$apache$log4j$chainsaw$Main = class$("org.apache.log4j.chainsaw.Main"));
        }
        else {
            class$org$apache$log4j$chainsaw$Main = Main.class$org$apache$log4j$chainsaw$Main;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$Main);
    }
    
    private Main() {
        super("CHAINSAW - Log4J Log Viewer");
        final MyTableModel myTableModel = new MyTableModel();
        final JMenuBar jMenuBar = new JMenuBar();
        this.setJMenuBar(jMenuBar);
        Accessible accessible = new JMenu("File");
        jMenuBar.add((JMenu)accessible);
        while (true) {
            try {
                final LoadXMLAction loadXMLAction = new LoadXMLAction(this, myTableModel);
                final JMenuItem menuItem = new JMenuItem("Load file...");
                ((JMenu)accessible).add(menuItem);
                menuItem.addActionListener(loadXMLAction);
                final JMenuItem menuItem2 = new JMenuItem("Exit");
                ((JMenu)accessible).add(menuItem2);
                menuItem2.addActionListener(ExitAction.INSTANCE);
                accessible = new ControlPanel(myTableModel);
                this.getContentPane().add((Component)accessible, "North");
                final JTable table = new JTable(myTableModel);
                table.setSelectionMode(0);
                accessible = new JScrollPane(table);
                ((JComponent)accessible).setBorder(BorderFactory.createTitledBorder("Events: "));
                ((JComponent)accessible).setPreferredSize(new Dimension(900, 300));
                final DetailPanel detailPanel = new DetailPanel(table, myTableModel);
                detailPanel.setPreferredSize(new Dimension(900, 300));
                accessible = new JSplitPane(0, (Component)accessible, detailPanel);
                this.getContentPane().add((Component)accessible, "Center");
                this.addWindowListener(new Main$1(this));
                this.pack();
                this.setVisible(true);
                this.setupReceiver(myTableModel);
            }
            catch (NoClassDefFoundError noClassDefFoundError) {
                Main.LOG.info("Missing classes for XML parser", noClassDefFoundError);
                JOptionPane.showMessageDialog(this, "XML parser not in classpath - unable to load XML events.", "CHAINSAW", 0);
                continue;
            }
            catch (Exception ex) {
                Main.LOG.info("Unable to create the action to load XML files", ex);
                JOptionPane.showMessageDialog(this, "Unable to create a XML parser - unable to load XML events.", "CHAINSAW", 0);
                continue;
            }
            break;
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private static void initLog4J() {
        final Properties properties = new Properties();
        properties.setProperty("log4j.rootLogger", "DEBUG, A1");
        properties.setProperty("log4j.appender.A1", "org.apache.log4j.ConsoleAppender");
        properties.setProperty("log4j.appender.A1.layout", "org.apache.log4j.TTCCLayout");
        PropertyConfigurator.configure(properties);
    }
    
    public static void main(final String[] array) {
        initLog4J();
        new Main();
    }
    
    private void setupReceiver(final MyTableModel p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: istore_3       
        //     4: ldc             "chainsaw.port"
        //     6: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //     9: astore          4
        //    11: iload_3        
        //    12: istore_2       
        //    13: aload           4
        //    15: ifnull          24
        //    18: aload           4
        //    20: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    23: istore_2       
        //    24: new             Lorg/apache/log4j/chainsaw/LoggingReceiver;
        //    27: dup            
        //    28: aload_1        
        //    29: iload_2        
        //    30: invokespecial   org/apache/log4j/chainsaw/LoggingReceiver.<init>:(Lorg/apache/log4j/chainsaw/MyTableModel;I)V
        //    33: invokevirtual   org/apache/log4j/chainsaw/LoggingReceiver.start:()V
        //    36: return         
        //    37: astore          5
        //    39: getstatic       org/apache/log4j/chainsaw/Main.LOG:Lorg/apache/log4j/Logger;
        //    42: new             Ljava/lang/StringBuffer;
        //    45: dup            
        //    46: invokespecial   java/lang/StringBuffer.<init>:()V
        //    49: ldc_w           "Unable to parse chainsaw.port property with value "
        //    52: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    55: aload           4
        //    57: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    60: ldc_w           "."
        //    63: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    66: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    69: invokevirtual   org/apache/log4j/Logger.fatal:(Ljava/lang/Object;)V
        //    72: aload_0        
        //    73: new             Ljava/lang/StringBuffer;
        //    76: dup            
        //    77: invokespecial   java/lang/StringBuffer.<init>:()V
        //    80: ldc_w           "Unable to parse port number from '"
        //    83: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    86: aload           4
        //    88: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    91: ldc_w           "', quitting."
        //    94: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    97: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   100: ldc             "CHAINSAW"
        //   102: iconst_0       
        //   103: invokestatic    javax/swing/JOptionPane.showMessageDialog:(Ljava/awt/Component;Ljava/lang/Object;Ljava/lang/String;I)V
        //   106: iconst_1       
        //   107: invokestatic    java/lang/System.exit:(I)V
        //   110: iload_3        
        //   111: istore_2       
        //   112: goto            24
        //   115: astore_1       
        //   116: getstatic       org/apache/log4j/chainsaw/Main.LOG:Lorg/apache/log4j/Logger;
        //   119: ldc_w           "Unable to connect to socket server, quiting"
        //   122: aload_1        
        //   123: invokevirtual   org/apache/log4j/Logger.fatal:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   126: aload_0        
        //   127: new             Ljava/lang/StringBuffer;
        //   130: dup            
        //   131: invokespecial   java/lang/StringBuffer.<init>:()V
        //   134: ldc_w           "Unable to create socket on port "
        //   137: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   140: iload_2        
        //   141: invokevirtual   java/lang/StringBuffer.append:(I)Ljava/lang/StringBuffer;
        //   144: ldc_w           ", quitting."
        //   147: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   150: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   153: ldc             "CHAINSAW"
        //   155: iconst_0       
        //   156: invokestatic    javax/swing/JOptionPane.showMessageDialog:(Ljava/awt/Component;Ljava/lang/Object;Ljava/lang/String;I)V
        //   159: iconst_1       
        //   160: invokestatic    java/lang/System.exit:(I)V
        //   163: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  18     24     37     115    Ljava/lang/NumberFormatException;
        //  24     36     115    164    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0024:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
