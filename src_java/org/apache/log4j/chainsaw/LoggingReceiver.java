package org.apache.log4j.chainsaw;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import java.io.*;
import java.net.*;

class LoggingReceiver extends Thread
{
    private static final Logger LOG;
    static Class class$org$apache$log4j$chainsaw$LoggingReceiver;
    private MyTableModel mModel;
    private ServerSocket mSvrSock;
    
    static {
        Class class$org$apache$log4j$chainsaw$LoggingReceiver;
        if (LoggingReceiver.class$org$apache$log4j$chainsaw$LoggingReceiver == null) {
            class$org$apache$log4j$chainsaw$LoggingReceiver = (LoggingReceiver.class$org$apache$log4j$chainsaw$LoggingReceiver = class$("org.apache.log4j.chainsaw.LoggingReceiver"));
        }
        else {
            class$org$apache$log4j$chainsaw$LoggingReceiver = LoggingReceiver.class$org$apache$log4j$chainsaw$LoggingReceiver;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$LoggingReceiver);
    }
    
    LoggingReceiver(final MyTableModel mModel, final int n) throws IOException {
        this.setDaemon(true);
        this.mModel = mModel;
        this.mSvrSock = new ServerSocket(n);
    }
    
    static Logger access$000() {
        return LoggingReceiver.LOG;
    }
    
    static MyTableModel access$100(final LoggingReceiver loggingReceiver) {
        return loggingReceiver.mModel;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void run() {
        LoggingReceiver.LOG.info("Thread started");
        try {
            while (true) {
                LoggingReceiver.LOG.debug("Waiting for a connection");
                final Socket accept = this.mSvrSock.accept();
                LoggingReceiver.LOG.debug(new StringBuffer().append("Got a connection from ").append(accept.getInetAddress().getHostName()).toString());
                final Thread thread = new Thread(new Slurper(accept));
                thread.setDaemon(true);
                thread.start();
            }
        }
        catch (IOException ex) {
            LoggingReceiver.LOG.error("Error in accepting connections, stopping.", ex);
        }
    }
    
    private class Slurper implements Runnable
    {
        private final Socket mClient;
        private final LoggingReceiver this$0;
        
        Slurper(final LoggingReceiver this$0, final Socket mClient) {
            this.this$0 = this$0;
            this.mClient = mClient;
        }
        
        @Override
        public void run() {
            LoggingReceiver.access$000().debug("Starting to get data");
            try {
                final ObjectInputStream objectInputStream = new ObjectInputStream(this.mClient.getInputStream());
                while (true) {
                    LoggingReceiver.access$100(this.this$0).addEvent(new EventDetails((LoggingEvent)objectInputStream.readObject()));
                }
            }
            catch (EOFException ex4) {
                LoggingReceiver.access$000().info("Reached EOF, closing connection");
            }
            catch (SocketException ex5) {
                LoggingReceiver.access$000().info("Caught SocketException, closing connection");
            }
            catch (IOException ex) {
                LoggingReceiver.access$000().warn("Got IOException, closing connection", ex);
            }
            catch (ClassNotFoundException ex2) {
                LoggingReceiver.access$000().warn("Got ClassNotFoundException, closing connection", ex2);
            }
            try {
                this.mClient.close();
            }
            catch (IOException ex3) {
                LoggingReceiver.access$000().warn("Error closing connection", ex3);
            }
        }
    }
}
