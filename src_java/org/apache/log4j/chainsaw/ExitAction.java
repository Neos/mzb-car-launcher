package org.apache.log4j.chainsaw;

import javax.swing.*;
import org.apache.log4j.*;
import java.awt.event.*;

class ExitAction extends AbstractAction
{
    public static final ExitAction INSTANCE;
    private static final Logger LOG;
    static Class class$org$apache$log4j$chainsaw$ExitAction;
    
    static {
        Class class$org$apache$log4j$chainsaw$ExitAction;
        if (ExitAction.class$org$apache$log4j$chainsaw$ExitAction == null) {
            class$org$apache$log4j$chainsaw$ExitAction = (ExitAction.class$org$apache$log4j$chainsaw$ExitAction = class$("org.apache.log4j.chainsaw.ExitAction"));
        }
        else {
            class$org$apache$log4j$chainsaw$ExitAction = ExitAction.class$org$apache$log4j$chainsaw$ExitAction;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$ExitAction);
        INSTANCE = new ExitAction();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        ExitAction.LOG.info("shutting down");
        System.exit(0);
    }
}
