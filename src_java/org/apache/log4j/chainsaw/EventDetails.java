package org.apache.log4j.chainsaw;

import org.apache.log4j.spi.*;
import org.apache.log4j.*;

class EventDetails
{
    private final String mCategoryName;
    private final String mLocationDetails;
    private final String mMessage;
    private final String mNDC;
    private final Priority mPriority;
    private final String mThreadName;
    private final String[] mThrowableStrRep;
    private final long mTimeStamp;
    
    EventDetails(final long mTimeStamp, final Priority mPriority, final String mCategoryName, final String mndc, final String mThreadName, final String mMessage, final String[] mThrowableStrRep, final String mLocationDetails) {
        this.mTimeStamp = mTimeStamp;
        this.mPriority = mPriority;
        this.mCategoryName = mCategoryName;
        this.mNDC = mndc;
        this.mThreadName = mThreadName;
        this.mMessage = mMessage;
        this.mThrowableStrRep = mThrowableStrRep;
        this.mLocationDetails = mLocationDetails;
    }
    
    EventDetails(final LoggingEvent loggingEvent) {
        final long timeStamp = loggingEvent.timeStamp;
        final Level level = loggingEvent.getLevel();
        final String loggerName = loggingEvent.getLoggerName();
        final String ndc = loggingEvent.getNDC();
        final String threadName = loggingEvent.getThreadName();
        final String renderedMessage = loggingEvent.getRenderedMessage();
        final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
        String fullInfo;
        if (loggingEvent.getLocationInformation() == null) {
            fullInfo = null;
        }
        else {
            fullInfo = loggingEvent.getLocationInformation().fullInfo;
        }
        this(timeStamp, level, loggerName, ndc, threadName, renderedMessage, throwableStrRep, fullInfo);
    }
    
    String getCategoryName() {
        return this.mCategoryName;
    }
    
    String getLocationDetails() {
        return this.mLocationDetails;
    }
    
    String getMessage() {
        return this.mMessage;
    }
    
    String getNDC() {
        return this.mNDC;
    }
    
    Priority getPriority() {
        return this.mPriority;
    }
    
    String getThreadName() {
        return this.mThreadName;
    }
    
    String[] getThrowableStrRep() {
        return this.mThrowableStrRep;
    }
    
    long getTimeStamp() {
        return this.mTimeStamp;
    }
}
