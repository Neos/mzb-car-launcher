package org.apache.log4j.chainsaw;

import javax.swing.table.*;
import java.text.*;
import org.apache.log4j.*;
import java.util.*;

class MyTableModel extends AbstractTableModel
{
    private static final String[] COL_NAMES;
    private static final DateFormat DATE_FORMATTER;
    private static final EventDetails[] EMPTY_LIST;
    private static final Logger LOG;
    private static final Comparator MY_COMP;
    static Class class$java$lang$Boolean;
    static Class class$java$lang$Object;
    static Class class$org$apache$log4j$chainsaw$MyTableModel;
    private final SortedSet mAllEvents;
    private String mCategoryFilter;
    private EventDetails[] mFilteredEvents;
    private final Object mLock;
    private String mMessageFilter;
    private String mNDCFilter;
    private boolean mPaused;
    private final List mPendingEvents;
    private Priority mPriorityFilter;
    private String mThreadFilter;
    
    static {
        Class class$org$apache$log4j$chainsaw$MyTableModel;
        if (MyTableModel.class$org$apache$log4j$chainsaw$MyTableModel == null) {
            class$org$apache$log4j$chainsaw$MyTableModel = (MyTableModel.class$org$apache$log4j$chainsaw$MyTableModel = class$("org.apache.log4j.chainsaw.MyTableModel"));
        }
        else {
            class$org$apache$log4j$chainsaw$MyTableModel = MyTableModel.class$org$apache$log4j$chainsaw$MyTableModel;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$MyTableModel);
        MY_COMP = new MyTableModel$1();
        COL_NAMES = new String[] { "Time", "Priority", "Trace", "Category", "NDC", "Message" };
        EMPTY_LIST = new EventDetails[0];
        DATE_FORMATTER = DateFormat.getDateTimeInstance(3, 2);
    }
    
    MyTableModel() {
        this.mLock = new Object();
        this.mAllEvents = new TreeSet(MyTableModel.MY_COMP);
        this.mFilteredEvents = MyTableModel.EMPTY_LIST;
        this.mPendingEvents = new ArrayList();
        this.mPaused = false;
        this.mThreadFilter = "";
        this.mMessageFilter = "";
        this.mNDCFilter = "";
        this.mCategoryFilter = "";
        this.mPriorityFilter = Priority.DEBUG;
        final Thread thread = new Thread(new Processor(null));
        thread.setDaemon(true);
        thread.start();
    }
    
    static Object access$000(final MyTableModel myTableModel) {
        return myTableModel.mLock;
    }
    
    static boolean access$100(final MyTableModel myTableModel) {
        return myTableModel.mPaused;
    }
    
    static List access$200(final MyTableModel myTableModel) {
        return myTableModel.mPendingEvents;
    }
    
    static SortedSet access$300(final MyTableModel myTableModel) {
        return myTableModel.mAllEvents;
    }
    
    static boolean access$400(final MyTableModel myTableModel, final EventDetails eventDetails) {
        return myTableModel.matchFilter(eventDetails);
    }
    
    static void access$500(final MyTableModel myTableModel, final boolean b) {
        myTableModel.updateFilteredEvents(b);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private boolean matchFilter(final EventDetails eventDetails) {
        if (eventDetails.getPriority().isGreaterOrEqual(this.mPriorityFilter) && eventDetails.getThreadName().indexOf(this.mThreadFilter) >= 0 && eventDetails.getCategoryName().indexOf(this.mCategoryFilter) >= 0 && (this.mNDCFilter.length() == 0 || (eventDetails.getNDC() != null && eventDetails.getNDC().indexOf(this.mNDCFilter) >= 0))) {
            final String message = eventDetails.getMessage();
            if (message == null) {
                if (this.mMessageFilter.length() != 0) {
                    return false;
                }
            }
            else if (message.indexOf(this.mMessageFilter) < 0) {
                return false;
            }
            return true;
        }
        return false;
    }
    
    private void updateFilteredEvents(final boolean b) {
        final long currentTimeMillis = System.currentTimeMillis();
        final ArrayList<EventDetails> list = new ArrayList<EventDetails>();
        final int size = this.mAllEvents.size();
        for (final EventDetails eventDetails : this.mAllEvents) {
            if (this.matchFilter(eventDetails)) {
                list.add(eventDetails);
            }
        }
        Object o;
        if (this.mFilteredEvents.length == 0) {
            o = null;
        }
        else {
            o = this.mFilteredEvents[0];
        }
        this.mFilteredEvents = list.toArray(MyTableModel.EMPTY_LIST);
        if (b && o != null) {
            final int index = list.indexOf(o);
            if (index < 1) {
                MyTableModel.LOG.warn("In strange state");
                this.fireTableDataChanged();
            }
            else {
                this.fireTableRowsInserted(0, index - 1);
            }
        }
        else {
            this.fireTableDataChanged();
        }
        MyTableModel.LOG.debug(new StringBuffer().append("Total time [ms]: ").append(System.currentTimeMillis() - currentTimeMillis).append(" in update, size: ").append(size).toString());
    }
    
    public void addEvent(final EventDetails eventDetails) {
        synchronized (this.mLock) {
            this.mPendingEvents.add(eventDetails);
        }
    }
    
    public void clear() {
        synchronized (this.mLock) {
            this.mAllEvents.clear();
            this.mFilteredEvents = new EventDetails[0];
            this.mPendingEvents.clear();
            this.fireTableDataChanged();
        }
    }
    
    @Override
    public Class getColumnClass(final int n) {
        if (n == 2) {
            if (MyTableModel.class$java$lang$Boolean == null) {
                return MyTableModel.class$java$lang$Boolean = class$("java.lang.Boolean");
            }
            return MyTableModel.class$java$lang$Boolean;
        }
        else {
            if (MyTableModel.class$java$lang$Object == null) {
                return MyTableModel.class$java$lang$Object = class$("java.lang.Object");
            }
            return MyTableModel.class$java$lang$Object;
        }
    }
    
    @Override
    public int getColumnCount() {
        return MyTableModel.COL_NAMES.length;
    }
    
    @Override
    public String getColumnName(final int n) {
        return MyTableModel.COL_NAMES[n];
    }
    
    public EventDetails getEventDetails(final int n) {
        synchronized (this.mLock) {
            return this.mFilteredEvents[n];
        }
    }
    
    @Override
    public int getRowCount() {
        synchronized (this.mLock) {
            return this.mFilteredEvents.length;
        }
    }
    
    @Override
    public Object getValueAt(final int n, final int n2) {
        synchronized (this.mLock) {
            final EventDetails eventDetails = this.mFilteredEvents[n];
            if (n2 == 0) {
                return MyTableModel.DATE_FORMATTER.format(new Date(eventDetails.getTimeStamp()));
            }
            if (n2 == 1) {
                return eventDetails.getPriority();
            }
        }
        final EventDetails eventDetails2;
        if (n2 == 2) {
            Boolean b;
            if (eventDetails2.getThrowableStrRep() == null) {
                b = Boolean.FALSE;
            }
            else {
                b = Boolean.TRUE;
            }
            // monitorexit(o)
            return b;
        }
        if (n2 == 3) {
            // monitorexit(o)
            return eventDetails2.getCategoryName();
        }
        if (n2 == 4) {
            // monitorexit(o)
            return eventDetails2.getNDC();
        }
        // monitorexit(o)
        return eventDetails2.getMessage();
    }
    
    public boolean isPaused() {
        synchronized (this.mLock) {
            return this.mPaused;
        }
    }
    
    public void setCategoryFilter(final String s) {
        synchronized (this.mLock) {
            this.mCategoryFilter = s.trim();
            this.updateFilteredEvents(false);
        }
    }
    
    public void setMessageFilter(final String s) {
        synchronized (this.mLock) {
            this.mMessageFilter = s.trim();
            this.updateFilteredEvents(false);
        }
    }
    
    public void setNDCFilter(final String s) {
        synchronized (this.mLock) {
            this.mNDCFilter = s.trim();
            this.updateFilteredEvents(false);
        }
    }
    
    public void setPriorityFilter(final Priority mPriorityFilter) {
        synchronized (this.mLock) {
            this.mPriorityFilter = mPriorityFilter;
            this.updateFilteredEvents(false);
        }
    }
    
    public void setThreadFilter(final String s) {
        synchronized (this.mLock) {
            this.mThreadFilter = s.trim();
            this.updateFilteredEvents(false);
        }
    }
    
    public void toggle() {
        while (true) {
            while (true) {
                synchronized (this.mLock) {
                    if (!this.mPaused) {
                        final boolean mPaused = true;
                        this.mPaused = mPaused;
                        return;
                    }
                }
                final boolean mPaused = false;
                continue;
            }
        }
    }
    
    private class Processor implements Runnable
    {
        private final MyTableModel this$0;
        
        private Processor(final MyTableModel this$0) {
            this.this$0 = this$0;
        }
        
        Processor(final MyTableModel myTableModel, final MyTableModel$1 myTableModel$1) {
            this(myTableModel);
        }
        
        @Override
        public void run() {
            boolean b;
            int n;
            Iterator<EventDetails> iterator;
            EventDetails eventDetails;
            Label_0006_Outer:Label_0056_Outer:Label_0116_Outer:Label_0170_Outer:
            while (true) {
                while (true) {
                Label_0178:
                    while (true) {
                    Label_0173:
                        while (true) {
                            while (true) {
                                try {
                                    while (true) {
                                        Thread.sleep(1000L);
                                        synchronized (MyTableModel.access$000(this.this$0)) {
                                            if (MyTableModel.access$100(this.this$0)) {
                                                continue Label_0006_Outer;
                                            }
                                        }
                                        b = true;
                                        n = 0;
                                        iterator = MyTableModel.access$200(this.this$0).iterator();
                                        if (iterator.hasNext()) {
                                            break;
                                        }
                                        MyTableModel.access$200(this.this$0).clear();
                                        if (n != 0) {
                                            MyTableModel.access$500(this.this$0, b);
                                        }
                                    }
                                    // monitorexit(o)
                                    eventDetails = iterator.next();
                                    MyTableModel.access$300(this.this$0).add(eventDetails);
                                    if (!b || eventDetails != MyTableModel.access$300(this.this$0).first()) {
                                        break Label_0173;
                                    }
                                    b = true;
                                    if (n == 0) {
                                        if (!MyTableModel.access$400(this.this$0, eventDetails)) {
                                            break Label_0178;
                                        }
                                    }
                                }
                                catch (InterruptedException ex) {
                                    continue Label_0056_Outer;
                                }
                                break;
                            }
                            n = 1;
                            continue Label_0116_Outer;
                        }
                        b = false;
                        continue Label_0170_Outer;
                    }
                    n = 0;
                    continue;
                }
            }
        }
    }
}
