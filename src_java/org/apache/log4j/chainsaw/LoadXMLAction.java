package org.apache.log4j.chainsaw;

import org.apache.log4j.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import java.io.*;

class LoadXMLAction extends AbstractAction
{
    private static final Logger LOG;
    static Class class$org$apache$log4j$chainsaw$LoadXMLAction;
    private final JFileChooser mChooser;
    private final XMLFileHandler mHandler;
    private final JFrame mParent;
    private final XMLReader mParser;
    
    static {
        Class class$org$apache$log4j$chainsaw$LoadXMLAction;
        if (LoadXMLAction.class$org$apache$log4j$chainsaw$LoadXMLAction == null) {
            class$org$apache$log4j$chainsaw$LoadXMLAction = (LoadXMLAction.class$org$apache$log4j$chainsaw$LoadXMLAction = class$("org.apache.log4j.chainsaw.LoadXMLAction"));
        }
        else {
            class$org$apache$log4j$chainsaw$LoadXMLAction = LoadXMLAction.class$org$apache$log4j$chainsaw$LoadXMLAction;
        }
        LOG = Logger.getLogger(class$org$apache$log4j$chainsaw$LoadXMLAction);
    }
    
    LoadXMLAction(final JFrame mParent, final MyTableModel myTableModel) throws SAXException, ParserConfigurationException {
        (this.mChooser = new JFileChooser()).setMultiSelectionEnabled(false);
        this.mChooser.setFileSelectionMode(0);
        this.mParent = mParent;
        this.mHandler = new XMLFileHandler(myTableModel);
        (this.mParser = SAXParserFactory.newInstance().newSAXParser().getXMLReader()).setContentHandler(this.mHandler);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private int loadFile(final String s) throws SAXException, IOException {
        synchronized (this.mParser) {
            final StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" standalone=\"yes\"?>\n");
            sb.append("<!DOCTYPE log4j:eventSet ");
            sb.append("[<!ENTITY data SYSTEM \"file:///");
            sb.append(s);
            sb.append("\">]>\n");
            sb.append("<log4j:eventSet xmlns:log4j=\"Claira\">\n");
            sb.append("&data;\n");
            sb.append("</log4j:eventSet>\n");
            this.mParser.parse(new InputSource(new StringReader(sb.toString())));
            return this.mHandler.getNumEvents();
        }
    }
    
    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        LoadXMLAction.LOG.info("load file called");
        if (this.mChooser.showOpenDialog(this.mParent) != 0) {
            return;
        }
        LoadXMLAction.LOG.info("Need to load a file");
        final File selectedFile = this.mChooser.getSelectedFile();
        LoadXMLAction.LOG.info(new StringBuffer().append("loading the contents of ").append(selectedFile.getAbsolutePath()).toString());
        try {
            JOptionPane.showMessageDialog(this.mParent, new StringBuffer().append("Loaded ").append(this.loadFile(selectedFile.getAbsolutePath())).append(" events.").toString(), "CHAINSAW", 1);
        }
        catch (Exception ex) {
            LoadXMLAction.LOG.warn("caught an exception loading the file", ex);
            JOptionPane.showMessageDialog(this.mParent, new StringBuffer().append("Error parsing file - ").append(ex.getMessage()).toString(), "CHAINSAW", 0);
        }
    }
}
