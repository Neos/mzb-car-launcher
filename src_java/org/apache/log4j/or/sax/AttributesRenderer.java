package org.apache.log4j.or.sax;

import org.apache.log4j.or.*;
import org.xml.sax.*;

public class AttributesRenderer implements ObjectRenderer
{
    @Override
    public String doRender(final Object o) {
        if (o instanceof Attributes) {
            final StringBuffer sb = new StringBuffer();
            final Attributes attributes = (Attributes)o;
            final int length = attributes.getLength();
            int n = 1;
            for (int i = 0; i < length; ++i) {
                if (n != 0) {
                    n = 0;
                }
                else {
                    sb.append(", ");
                }
                sb.append(attributes.getQName(i));
                sb.append('=');
                sb.append(attributes.getValue(i));
            }
            return sb.toString();
        }
        try {
            return o.toString();
        }
        catch (Exception ex) {
            return ex.toString();
        }
    }
}
