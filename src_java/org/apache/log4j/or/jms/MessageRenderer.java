package org.apache.log4j.or.jms;

import org.apache.log4j.or.*;
import org.apache.log4j.helpers.*;
import javax.jms.*;

public class MessageRenderer implements ObjectRenderer
{
    @Override
    public String doRender(final Object o) {
        if (!(o instanceof Message)) {
            return o.toString();
        }
    Label_0056_Outer:
        while (true) {
            final StringBuffer sb = new StringBuffer();
            final Message message = (Message)o;
            while (true) {
            Label_0265:
                while (true) {
                    Label_0250: {
                        try {
                            sb.append("DeliveryMode=");
                            switch (message.getJMSDeliveryMode()) {
                                case 1: {
                                    sb.append("NON_PERSISTENT");
                                    sb.append(", CorrelationID=");
                                    sb.append(message.getJMSCorrelationID());
                                    sb.append(", Destination=");
                                    sb.append(message.getJMSDestination());
                                    sb.append(", Expiration=");
                                    sb.append(message.getJMSExpiration());
                                    sb.append(", MessageID=");
                                    sb.append(message.getJMSMessageID());
                                    sb.append(", Priority=");
                                    sb.append(message.getJMSPriority());
                                    sb.append(", Redelivered=");
                                    sb.append(message.getJMSRedelivered());
                                    sb.append(", ReplyTo=");
                                    sb.append(message.getJMSReplyTo());
                                    sb.append(", Timestamp=");
                                    sb.append(message.getJMSTimestamp());
                                    sb.append(", Type=");
                                    sb.append(message.getJMSType());
                                    return sb.toString();
                                }
                                case 2: {
                                    break Label_0250;
                                }
                                default: {
                                    break Label_0265;
                                }
                            }
                            sb.append("UNKNOWN");
                            continue Label_0056_Outer;
                        }
                        catch (JMSException ex) {
                            LogLog.error("Could not parse Message.", (Throwable)ex);
                            return sb.toString();
                        }
                    }
                    sb.append("PERSISTENT");
                    continue Label_0056_Outer;
                }
                continue;
            }
        }
    }
}
