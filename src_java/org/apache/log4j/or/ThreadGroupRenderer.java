package org.apache.log4j.or;

import org.apache.log4j.*;

public class ThreadGroupRenderer implements ObjectRenderer
{
    @Override
    public String doRender(final Object o) {
        if (o instanceof ThreadGroup) {
            final StringBuffer sb = new StringBuffer();
            final ThreadGroup threadGroup = (ThreadGroup)o;
            sb.append("java.lang.ThreadGroup[name=");
            sb.append(threadGroup.getName());
            sb.append(", maxpri=");
            sb.append(threadGroup.getMaxPriority());
            sb.append("]");
            final Thread[] array = new Thread[threadGroup.activeCount()];
            threadGroup.enumerate(array);
            for (int i = 0; i < array.length; ++i) {
                sb.append(Layout.LINE_SEP);
                sb.append("   Thread=[");
                sb.append(array[i].getName());
                sb.append(",");
                sb.append(array[i].getPriority());
                sb.append(",");
                sb.append(array[i].isDaemon());
                sb.append("]");
            }
            return sb.toString();
        }
        try {
            return o.toString();
        }
        catch (Exception ex) {
            return ex.toString();
        }
    }
}
