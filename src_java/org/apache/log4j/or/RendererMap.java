package org.apache.log4j.or;

import java.util.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;

public class RendererMap
{
    static Class class$org$apache$log4j$or$ObjectRenderer;
    static ObjectRenderer defaultRenderer;
    Hashtable map;
    
    static {
        RendererMap.defaultRenderer = new DefaultRenderer();
    }
    
    public RendererMap() {
        this.map = new Hashtable();
    }
    
    public static void addRenderer(final RendererSupport rendererSupport, final String s, final String s2) {
        LogLog.debug(new StringBuffer().append("Rendering class: [").append(s2).append("], Rendered class: [").append(s).append("].").toString());
        Class class$org$apache$log4j$or$ObjectRenderer;
        if (RendererMap.class$org$apache$log4j$or$ObjectRenderer == null) {
            class$org$apache$log4j$or$ObjectRenderer = (RendererMap.class$org$apache$log4j$or$ObjectRenderer = class$("org.apache.log4j.or.ObjectRenderer"));
        }
        else {
            class$org$apache$log4j$or$ObjectRenderer = RendererMap.class$org$apache$log4j$or$ObjectRenderer;
        }
        final ObjectRenderer objectRenderer = (ObjectRenderer)OptionConverter.instantiateByClassName(s2, class$org$apache$log4j$or$ObjectRenderer, null);
        if (objectRenderer == null) {
            LogLog.error(new StringBuffer().append("Could not instantiate renderer [").append(s2).append("].").toString());
            return;
        }
        try {
            rendererSupport.setRenderer(Loader.loadClass(s), objectRenderer);
        }
        catch (ClassNotFoundException ex) {
            LogLog.error(new StringBuffer().append("Could not find class [").append(s).append("].").toString(), ex);
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public void clear() {
        this.map.clear();
    }
    
    public String findAndRender(final Object o) {
        if (o == null) {
            return null;
        }
        return this.get(o.getClass()).doRender(o);
    }
    
    public ObjectRenderer get(Class superclass) {
        while (superclass != null) {
            final ObjectRenderer objectRenderer = this.map.get(superclass);
            if (objectRenderer != null) {
                return objectRenderer;
            }
            final ObjectRenderer searchInterfaces = this.searchInterfaces(superclass);
            if (searchInterfaces != null) {
                return searchInterfaces;
            }
            superclass = superclass.getSuperclass();
        }
        return RendererMap.defaultRenderer;
    }
    
    public ObjectRenderer get(final Object o) {
        if (o == null) {
            return null;
        }
        return this.get(o.getClass());
    }
    
    public ObjectRenderer getDefaultRenderer() {
        return RendererMap.defaultRenderer;
    }
    
    public void put(final Class clazz, final ObjectRenderer objectRenderer) {
        this.map.put(clazz, objectRenderer);
    }
    
    ObjectRenderer searchInterfaces(final Class clazz) {
        final ObjectRenderer objectRenderer = this.map.get(clazz);
        if (objectRenderer != null) {
            return objectRenderer;
        }
        final Class[] interfaces = clazz.getInterfaces();
        for (int i = 0; i < interfaces.length; ++i) {
            final ObjectRenderer searchInterfaces = this.searchInterfaces(interfaces[i]);
            if (searchInterfaces != null) {
                return searchInterfaces;
            }
        }
        return null;
    }
}
