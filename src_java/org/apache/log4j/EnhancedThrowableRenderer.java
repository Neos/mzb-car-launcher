package org.apache.log4j;

import org.apache.log4j.spi.*;
import java.lang.reflect.*;
import java.util.*;

public final class EnhancedThrowableRenderer implements ThrowableRenderer
{
    static Class class$java$lang$Throwable;
    private Method getClassNameMethod;
    private Method getStackTraceMethod;
    
    public EnhancedThrowableRenderer() {
        try {
            Class class$java$lang$Throwable;
            if (EnhancedThrowableRenderer.class$java$lang$Throwable == null) {
                class$java$lang$Throwable = (EnhancedThrowableRenderer.class$java$lang$Throwable = class$("java.lang.Throwable"));
            }
            else {
                class$java$lang$Throwable = EnhancedThrowableRenderer.class$java$lang$Throwable;
            }
            this.getStackTraceMethod = class$java$lang$Throwable.getMethod("getStackTrace", (Class[])null);
            this.getClassNameMethod = Class.forName("java.lang.StackTraceElement").getMethod("getClassName", (Class<?>[])null);
        }
        catch (Exception ex) {}
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private Class findClass(final String s) throws ClassNotFoundException {
        try {
            return Thread.currentThread().getContextClassLoader().loadClass(s);
        }
        catch (ClassNotFoundException ex) {
            try {
                return Class.forName(s);
            }
            catch (ClassNotFoundException ex2) {
                return this.getClass().getClassLoader().loadClass(s);
            }
        }
    }
    
    private String formatElement(final Object p0, final Map p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: ldc             "\tat "
        //     6: invokespecial   java/lang/StringBuffer.<init>:(Ljava/lang/String;)V
        //     9: astore          7
        //    11: aload           7
        //    13: aload_1        
        //    14: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //    17: pop            
        //    18: aload_0        
        //    19: getfield        org/apache/log4j/EnhancedThrowableRenderer.getClassNameMethod:Ljava/lang/reflect/Method;
        //    22: aload_1        
        //    23: aconst_null    
        //    24: checkcast       [Ljava/lang/Object;
        //    27: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    30: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //    33: astore_1       
        //    34: aload_2        
        //    35: aload_1        
        //    36: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    41: astore          8
        //    43: aload           8
        //    45: ifnull          62
        //    48: aload           7
        //    50: aload           8
        //    52: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //    55: pop            
        //    56: aload           7
        //    58: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    61: areturn        
        //    62: aload_0        
        //    63: aload_1        
        //    64: invokespecial   org/apache/log4j/EnhancedThrowableRenderer.findClass:(Ljava/lang/String;)Ljava/lang/Class;
        //    67: astore          8
        //    69: aload           7
        //    71: invokevirtual   java/lang/StringBuffer.length:()I
        //    74: istore          6
        //    76: aload           7
        //    78: bipush          91
        //    80: invokevirtual   java/lang/StringBuffer.append:(C)Ljava/lang/StringBuffer;
        //    83: pop            
        //    84: aload           8
        //    86: invokevirtual   java/lang/Class.getProtectionDomain:()Ljava/security/ProtectionDomain;
        //    89: invokevirtual   java/security/ProtectionDomain.getCodeSource:()Ljava/security/CodeSource;
        //    92: astore          9
        //    94: aload           9
        //    96: ifnull          191
        //    99: aload           9
        //   101: invokevirtual   java/security/CodeSource.getLocation:()Ljava/net/URL;
        //   104: astore          9
        //   106: aload           9
        //   108: ifnull          191
        //   111: ldc             "file"
        //   113: aload           9
        //   115: invokevirtual   java/net/URL.getProtocol:()Ljava/lang/String;
        //   118: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   121: ifeq            278
        //   124: aload           9
        //   126: invokevirtual   java/net/URL.getPath:()Ljava/lang/String;
        //   129: astore          10
        //   131: aload           10
        //   133: ifnull          191
        //   136: aload           10
        //   138: bipush          47
        //   140: invokevirtual   java/lang/String.lastIndexOf:(I)I
        //   143: istore          4
        //   145: aload           10
        //   147: getstatic       java/io/File.separatorChar:C
        //   150: invokevirtual   java/lang/String.lastIndexOf:(I)I
        //   153: istore          5
        //   155: iload           4
        //   157: istore_3       
        //   158: iload           5
        //   160: iload           4
        //   162: if_icmple       168
        //   165: iload           5
        //   167: istore_3       
        //   168: iload_3        
        //   169: ifle            183
        //   172: iload_3        
        //   173: aload           10
        //   175: invokevirtual   java/lang/String.length:()I
        //   178: iconst_1       
        //   179: isub           
        //   180: if_icmpne       261
        //   183: aload           7
        //   185: aload           9
        //   187: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   190: pop            
        //   191: aload           7
        //   193: bipush          58
        //   195: invokevirtual   java/lang/StringBuffer.append:(C)Ljava/lang/StringBuffer;
        //   198: pop            
        //   199: aload           8
        //   201: invokevirtual   java/lang/Class.getPackage:()Ljava/lang/Package;
        //   204: astore          8
        //   206: aload           8
        //   208: ifnull          231
        //   211: aload           8
        //   213: invokevirtual   java/lang/Package.getImplementationVersion:()Ljava/lang/String;
        //   216: astore          8
        //   218: aload           8
        //   220: ifnull          231
        //   223: aload           7
        //   225: aload           8
        //   227: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   230: pop            
        //   231: aload           7
        //   233: bipush          93
        //   235: invokevirtual   java/lang/StringBuffer.append:(C)Ljava/lang/StringBuffer;
        //   238: pop            
        //   239: aload_2        
        //   240: aload_1        
        //   241: aload           7
        //   243: iload           6
        //   245: invokevirtual   java/lang/StringBuffer.substring:(I)Ljava/lang/String;
        //   248: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   253: pop            
        //   254: goto            56
        //   257: astore_1       
        //   258: goto            56
        //   261: aload           7
        //   263: aload           10
        //   265: iload_3        
        //   266: iconst_1       
        //   267: iadd           
        //   268: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   271: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   274: pop            
        //   275: goto            191
        //   278: aload           7
        //   280: aload           9
        //   282: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   285: pop            
        //   286: goto            191
        //   289: astore          9
        //   291: goto            191
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  18     43     257    261    Ljava/lang/Exception;
        //  48     56     257    261    Ljava/lang/Exception;
        //  62     84     257    261    Ljava/lang/Exception;
        //  84     94     289    294    Ljava/lang/SecurityException;
        //  84     94     257    261    Ljava/lang/Exception;
        //  99     106    289    294    Ljava/lang/SecurityException;
        //  99     106    257    261    Ljava/lang/Exception;
        //  111    131    289    294    Ljava/lang/SecurityException;
        //  111    131    257    261    Ljava/lang/Exception;
        //  136    155    289    294    Ljava/lang/SecurityException;
        //  136    155    257    261    Ljava/lang/Exception;
        //  172    183    289    294    Ljava/lang/SecurityException;
        //  172    183    257    261    Ljava/lang/Exception;
        //  183    191    289    294    Ljava/lang/SecurityException;
        //  183    191    257    261    Ljava/lang/Exception;
        //  191    206    257    261    Ljava/lang/Exception;
        //  211    218    257    261    Ljava/lang/Exception;
        //  223    231    257    261    Ljava/lang/Exception;
        //  231    254    257    261    Ljava/lang/Exception;
        //  261    275    289    294    Ljava/lang/SecurityException;
        //  261    275    257    261    Ljava/lang/Exception;
        //  278    286    289    294    Ljava/lang/SecurityException;
        //  278    286    257    261    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0168:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public String[] doRender(final Throwable t) {
        if (this.getStackTraceMethod != null) {
            try {
                final Object[] array = (Object[])this.getStackTraceMethod.invoke(t, (Object[])null);
                final String[] array2 = new String[array.length + 1];
                array2[0] = t.toString();
                final HashMap hashMap = new HashMap();
                int n = 0;
                while (true) {
                    final String[] render = array2;
                    if (n >= array.length) {
                        return render;
                    }
                    array2[n + 1] = this.formatElement(array[n], hashMap);
                    ++n;
                }
            }
            catch (Exception ex) {}
        }
        return DefaultThrowableRenderer.render(t);
    }
}
