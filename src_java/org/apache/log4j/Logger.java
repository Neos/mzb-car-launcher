package org.apache.log4j;

import org.apache.log4j.spi.*;

public class Logger extends Category
{
    private static final String FQCN;
    static Class class$org$apache$log4j$Logger;
    
    static {
        Class class$org$apache$log4j$Logger;
        if (Logger.class$org$apache$log4j$Logger == null) {
            class$org$apache$log4j$Logger = (Logger.class$org$apache$log4j$Logger = class$("org.apache.log4j.Logger"));
        }
        else {
            class$org$apache$log4j$Logger = Logger.class$org$apache$log4j$Logger;
        }
        FQCN = class$org$apache$log4j$Logger.getName();
    }
    
    protected Logger(final String s) {
        super(s);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static Logger getLogger(final Class clazz) {
        return LogManager.getLogger(clazz.getName());
    }
    
    public static Logger getLogger(final String s) {
        return LogManager.getLogger(s);
    }
    
    public static Logger getLogger(final String s, final LoggerFactory loggerFactory) {
        return LogManager.getLogger(s, loggerFactory);
    }
    
    public static Logger getRootLogger() {
        return LogManager.getRootLogger();
    }
    
    public boolean isTraceEnabled() {
        return !this.repository.isDisabled(5000) && Level.TRACE.isGreaterOrEqual(this.getEffectiveLevel());
    }
    
    public void trace(final Object o) {
        if (!this.repository.isDisabled(5000) && Level.TRACE.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Logger.FQCN, Level.TRACE, o, null);
        }
    }
    
    public void trace(final Object o, final Throwable t) {
        if (!this.repository.isDisabled(5000) && Level.TRACE.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Logger.FQCN, Level.TRACE, o, t);
        }
    }
}
