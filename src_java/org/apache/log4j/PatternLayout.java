package org.apache.log4j;

import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;

public class PatternLayout extends Layout
{
    public static final String DEFAULT_CONVERSION_PATTERN = "%m%n";
    public static final String TTCC_CONVERSION_PATTERN = "%r [%t] %p %c %x - %m%n";
    protected final int BUF_SIZE;
    protected final int MAX_CAPACITY;
    private PatternConverter head;
    private String pattern;
    private StringBuffer sbuf;
    
    public PatternLayout() {
        this("%m%n");
    }
    
    public PatternLayout(final String pattern) {
        this.BUF_SIZE = 256;
        this.MAX_CAPACITY = 1024;
        this.sbuf = new StringBuffer(256);
        this.pattern = pattern;
        String s = pattern;
        if (pattern == null) {
            s = "%m%n";
        }
        this.head = this.createPatternParser(s).parse();
    }
    
    @Override
    public void activateOptions() {
    }
    
    protected PatternParser createPatternParser(final String s) {
        return new PatternParser(s);
    }
    
    @Override
    public String format(final LoggingEvent loggingEvent) {
        if (this.sbuf.capacity() > 1024) {
            this.sbuf = new StringBuffer(256);
        }
        else {
            this.sbuf.setLength(0);
        }
        for (PatternConverter patternConverter = this.head; patternConverter != null; patternConverter = patternConverter.next) {
            patternConverter.format(this.sbuf, loggingEvent);
        }
        return this.sbuf.toString();
    }
    
    public String getConversionPattern() {
        return this.pattern;
    }
    
    @Override
    public boolean ignoresThrowable() {
        return true;
    }
    
    public void setConversionPattern(final String pattern) {
        this.pattern = pattern;
        this.head = this.createPatternParser(pattern).parse();
    }
}
