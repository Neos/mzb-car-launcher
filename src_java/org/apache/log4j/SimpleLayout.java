package org.apache.log4j;

import org.apache.log4j.spi.*;

public class SimpleLayout extends Layout
{
    StringBuffer sbuf;
    
    public SimpleLayout() {
        this.sbuf = new StringBuffer(128);
    }
    
    @Override
    public void activateOptions() {
    }
    
    @Override
    public String format(final LoggingEvent loggingEvent) {
        this.sbuf.setLength(0);
        this.sbuf.append(loggingEvent.getLevel().toString());
        this.sbuf.append(" - ");
        this.sbuf.append(loggingEvent.getRenderedMessage());
        this.sbuf.append(SimpleLayout.LINE_SEP);
        return this.sbuf.toString();
    }
    
    @Override
    public boolean ignoresThrowable() {
        return true;
    }
}
