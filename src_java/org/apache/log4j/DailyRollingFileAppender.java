package org.apache.log4j;

import java.text.*;
import org.apache.log4j.helpers.*;
import java.util.*;
import org.apache.log4j.spi.*;
import java.io.*;

public class DailyRollingFileAppender extends FileAppender
{
    static final int HALF_DAY = 2;
    static final int TOP_OF_DAY = 3;
    static final int TOP_OF_HOUR = 1;
    static final int TOP_OF_MINUTE = 0;
    static final int TOP_OF_MONTH = 5;
    static final int TOP_OF_TROUBLE = -1;
    static final int TOP_OF_WEEK = 4;
    static final TimeZone gmtTimeZone;
    int checkPeriod;
    private String datePattern;
    private long nextCheck;
    Date now;
    RollingCalendar rc;
    private String scheduledFilename;
    SimpleDateFormat sdf;
    
    static {
        gmtTimeZone = TimeZone.getTimeZone("GMT");
    }
    
    public DailyRollingFileAppender() {
        this.datePattern = "'.'yyyy-MM-dd";
        this.nextCheck = System.currentTimeMillis() - 1L;
        this.now = new Date();
        this.rc = new RollingCalendar();
        this.checkPeriod = -1;
    }
    
    public DailyRollingFileAppender(final Layout layout, final String s, final String datePattern) throws IOException {
        super(layout, s, true);
        this.datePattern = "'.'yyyy-MM-dd";
        this.nextCheck = System.currentTimeMillis() - 1L;
        this.now = new Date();
        this.rc = new RollingCalendar();
        this.checkPeriod = -1;
        this.datePattern = datePattern;
        this.activateOptions();
    }
    
    @Override
    public void activateOptions() {
        super.activateOptions();
        if (this.datePattern != null && this.fileName != null) {
            this.now.setTime(System.currentTimeMillis());
            this.sdf = new SimpleDateFormat(this.datePattern);
            final int computeCheckPeriod = this.computeCheckPeriod();
            this.printPeriodicity(computeCheckPeriod);
            this.rc.setType(computeCheckPeriod);
            this.scheduledFilename = new StringBuffer().append(this.fileName).append(this.sdf.format(new Date(new File(this.fileName).lastModified()))).toString();
            return;
        }
        LogLog.error(new StringBuffer().append("Either File or DatePattern options are not set for appender [").append(this.name).append("].").toString());
    }
    
    int computeCheckPeriod() {
        final RollingCalendar rollingCalendar = new RollingCalendar(DailyRollingFileAppender.gmtTimeZone, Locale.getDefault());
        final Date date = new Date(0L);
        if (this.datePattern != null) {
            for (int i = 0; i <= 5; ++i) {
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(this.datePattern);
                simpleDateFormat.setTimeZone(DailyRollingFileAppender.gmtTimeZone);
                final String format = simpleDateFormat.format(date);
                rollingCalendar.setType(i);
                final String format2 = simpleDateFormat.format(new Date(rollingCalendar.getNextCheckMillis(date)));
                if (format != null && format2 != null && !format.equals(format2)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    public String getDatePattern() {
        return this.datePattern;
    }
    
    void printPeriodicity(final int n) {
        switch (n) {
            default: {
                LogLog.warn(new StringBuffer().append("Unknown periodicity for appender [").append(this.name).append("].").toString());
            }
            case 0: {
                LogLog.debug(new StringBuffer().append("Appender [").append(this.name).append("] to be rolled every minute.").toString());
            }
            case 1: {
                LogLog.debug(new StringBuffer().append("Appender [").append(this.name).append("] to be rolled on top of every hour.").toString());
            }
            case 2: {
                LogLog.debug(new StringBuffer().append("Appender [").append(this.name).append("] to be rolled at midday and midnight.").toString());
            }
            case 3: {
                LogLog.debug(new StringBuffer().append("Appender [").append(this.name).append("] to be rolled at midnight.").toString());
            }
            case 4: {
                LogLog.debug(new StringBuffer().append("Appender [").append(this.name).append("] to be rolled at start of week.").toString());
            }
            case 5: {
                LogLog.debug(new StringBuffer().append("Appender [").append(this.name).append("] to be rolled at start of every month.").toString());
            }
        }
    }
    
    void rollOver() throws IOException {
        if (this.datePattern == null) {
            this.errorHandler.error("Missing DatePattern option in rollOver().");
        }
        else {
            final String string = new StringBuffer().append(this.fileName).append(this.sdf.format(this.now)).toString();
            if (!this.scheduledFilename.equals(string)) {
                this.closeFile();
                final File file = new File(this.scheduledFilename);
                if (file.exists()) {
                    file.delete();
                }
                Label_0163: {
                    if (!new File(this.fileName).renameTo(file)) {
                        break Label_0163;
                    }
                    LogLog.debug(new StringBuffer().append(this.fileName).append(" -> ").append(this.scheduledFilename).toString());
                    while (true) {
                        try {
                            while (true) {
                                this.setFile(this.fileName, true, this.bufferedIO, this.bufferSize);
                                this.scheduledFilename = string;
                                return;
                                LogLog.error(new StringBuffer().append("Failed to rename [").append(this.fileName).append("] to [").append(this.scheduledFilename).append("].").toString());
                                continue;
                            }
                        }
                        catch (IOException ex) {
                            this.errorHandler.error(new StringBuffer().append("setFile(").append(this.fileName).append(", true) call failed.").toString());
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    public void setDatePattern(final String datePattern) {
        this.datePattern = datePattern;
    }
    
    @Override
    protected void subAppend(final LoggingEvent loggingEvent) {
        final long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            if (currentTimeMillis < this.nextCheck) {
                break Label_0040;
            }
            this.now.setTime(currentTimeMillis);
            this.nextCheck = this.rc.getNextCheckMillis(this.now);
            try {
                this.rollOver();
                super.subAppend(loggingEvent);
            }
            catch (IOException ex) {
                if (ex instanceof InterruptedIOException) {
                    Thread.currentThread().interrupt();
                }
                LogLog.error("rollOver() failed.", ex);
                continue;
            }
            break;
        }
    }
}
