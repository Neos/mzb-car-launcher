package org.apache.log4j.varia;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;

public class LevelMatchFilter extends Filter
{
    boolean acceptOnMatch;
    Level levelToMatch;
    
    public LevelMatchFilter() {
        this.acceptOnMatch = true;
    }
    
    @Override
    public int decide(final LoggingEvent loggingEvent) {
        if (this.levelToMatch != null) {
            boolean b = false;
            if (this.levelToMatch.equals(loggingEvent.getLevel())) {
                b = true;
            }
            if (b) {
                if (this.acceptOnMatch) {
                    return 1;
                }
                return -1;
            }
        }
        return 0;
    }
    
    public boolean getAcceptOnMatch() {
        return this.acceptOnMatch;
    }
    
    public String getLevelToMatch() {
        if (this.levelToMatch == null) {
            return null;
        }
        return this.levelToMatch.toString();
    }
    
    public void setAcceptOnMatch(final boolean acceptOnMatch) {
        this.acceptOnMatch = acceptOnMatch;
    }
    
    public void setLevelToMatch(final String s) {
        this.levelToMatch = OptionConverter.toLevel(s, null);
    }
}
