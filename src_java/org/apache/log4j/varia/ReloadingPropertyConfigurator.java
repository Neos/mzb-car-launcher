package org.apache.log4j.varia;

import org.apache.log4j.*;
import java.io.*;
import org.apache.log4j.spi.*;
import java.net.*;

public class ReloadingPropertyConfigurator implements Configurator
{
    PropertyConfigurator delegate;
    
    public ReloadingPropertyConfigurator() {
        this.delegate = new PropertyConfigurator();
    }
    
    @Override
    public void doConfigure(final InputStream inputStream, final LoggerRepository loggerRepository) {
    }
    
    @Override
    public void doConfigure(final URL url, final LoggerRepository loggerRepository) {
    }
}
