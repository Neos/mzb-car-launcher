package org.apache.log4j.varia;

import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;

public class StringMatchFilter extends Filter
{
    public static final String ACCEPT_ON_MATCH_OPTION = "AcceptOnMatch";
    public static final String STRING_TO_MATCH_OPTION = "StringToMatch";
    boolean acceptOnMatch;
    String stringToMatch;
    
    public StringMatchFilter() {
        this.acceptOnMatch = true;
    }
    
    @Override
    public int decide(final LoggingEvent loggingEvent) {
        final String renderedMessage = loggingEvent.getRenderedMessage();
        if (renderedMessage == null || this.stringToMatch == null || renderedMessage.indexOf(this.stringToMatch) == -1) {
            return 0;
        }
        if (this.acceptOnMatch) {
            return 1;
        }
        return -1;
    }
    
    public boolean getAcceptOnMatch() {
        return this.acceptOnMatch;
    }
    
    public String[] getOptionStrings() {
        return new String[] { "StringToMatch", "AcceptOnMatch" };
    }
    
    public String getStringToMatch() {
        return this.stringToMatch;
    }
    
    public void setAcceptOnMatch(final boolean acceptOnMatch) {
        this.acceptOnMatch = acceptOnMatch;
    }
    
    public void setOption(final String s, final String stringToMatch) {
        if (s.equalsIgnoreCase("StringToMatch")) {
            this.stringToMatch = stringToMatch;
        }
        else if (s.equalsIgnoreCase("AcceptOnMatch")) {
            this.acceptOnMatch = OptionConverter.toBoolean(stringToMatch, this.acceptOnMatch);
        }
    }
    
    public void setStringToMatch(final String stringToMatch) {
        this.stringToMatch = stringToMatch;
    }
}
