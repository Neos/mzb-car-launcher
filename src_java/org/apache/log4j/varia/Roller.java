package org.apache.log4j.varia;

import org.apache.log4j.*;
import java.net.*;
import java.io.*;

public class Roller
{
    static Logger cat;
    static Class class$org$apache$log4j$varia$Roller;
    static String host;
    static int port;
    
    static {
        Class class$org$apache$log4j$varia$Roller;
        if (Roller.class$org$apache$log4j$varia$Roller == null) {
            class$org$apache$log4j$varia$Roller = (Roller.class$org$apache$log4j$varia$Roller = class$("org.apache.log4j.varia.Roller"));
        }
        else {
            class$org$apache$log4j$varia$Roller = Roller.class$org$apache$log4j$varia$Roller;
        }
        Roller.cat = Logger.getLogger(class$org$apache$log4j$varia$Roller);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    static void init(final String host, final String s) {
        Roller.host = host;
        try {
            Roller.port = Integer.parseInt(s);
        }
        catch (NumberFormatException ex) {
            usage(new StringBuffer().append("Second argument ").append(s).append(" is not a valid integer.").toString());
        }
    }
    
    public static void main(final String[] array) {
        BasicConfigurator.configure();
        if (array.length == 2) {
            init(array[0], array[1]);
        }
        else {
            usage("Wrong number of arguments.");
        }
        roll();
    }
    
    static void roll() {
        while (true) {
            try {
                final Socket socket = new Socket(Roller.host, Roller.port);
                final DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                final DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                dataOutputStream.writeUTF("RollOver");
                final String utf = dataInputStream.readUTF();
                if ("OK".equals(utf)) {
                    Roller.cat.info("Roll over signal acknowledged by remote appender.");
                }
                else {
                    Roller.cat.warn(new StringBuffer().append("Unexpected return code ").append(utf).append(" from remote entity.").toString());
                    System.exit(2);
                }
                System.exit(0);
            }
            catch (IOException ex) {
                Roller.cat.error(new StringBuffer().append("Could not send roll signal on host ").append(Roller.host).append(" port ").append(Roller.port).append(" .").toString(), ex);
                System.exit(2);
                continue;
            }
            break;
        }
    }
    
    static void usage(final String s) {
        System.err.println(s);
        final PrintStream err = System.err;
        final StringBuffer append = new StringBuffer().append("Usage: java ");
        Class class$org$apache$log4j$varia$Roller;
        if (Roller.class$org$apache$log4j$varia$Roller == null) {
            class$org$apache$log4j$varia$Roller = (Roller.class$org$apache$log4j$varia$Roller = class$("org.apache.log4j.varia.Roller"));
        }
        else {
            class$org$apache$log4j$varia$Roller = Roller.class$org$apache$log4j$varia$Roller;
        }
        err.println(append.append(class$org$apache$log4j$varia$Roller.getName()).append("host_name port_number").toString());
        System.exit(1);
    }
}
