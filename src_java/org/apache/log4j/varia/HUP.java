package org.apache.log4j.varia;

import org.apache.log4j.helpers.*;
import java.io.*;
import java.net.*;

class HUP extends Thread
{
    ExternallyRolledFileAppender er;
    int port;
    
    HUP(final ExternallyRolledFileAppender er, final int port) {
        this.er = er;
        this.port = port;
    }
    
    @Override
    public void run() {
        while (!this.isInterrupted()) {
            try {
                final ServerSocket serverSocket = new ServerSocket(this.port);
                while (true) {
                    final Socket accept = serverSocket.accept();
                    LogLog.debug(new StringBuffer().append("Connected to client at ").append(accept.getInetAddress()).toString());
                    new Thread(new HUPNode(accept, this.er), "ExternallyRolledFileAppender-HUP").start();
                }
            }
            catch (InterruptedIOException ex) {
                Thread.currentThread().interrupt();
                ex.printStackTrace();
                continue;
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
                continue;
            }
            catch (RuntimeException ex3) {
                ex3.printStackTrace();
                continue;
            }
            break;
        }
    }
}
