package org.apache.log4j.varia;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;

public class NullAppender extends AppenderSkeleton
{
    private static NullAppender instance;
    
    static {
        NullAppender.instance = new NullAppender();
    }
    
    public static NullAppender getNullAppender() {
        return NullAppender.instance;
    }
    
    @Override
    public void activateOptions() {
    }
    
    @Override
    protected void append(final LoggingEvent loggingEvent) {
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public void doAppend(final LoggingEvent loggingEvent) {
    }
    
    public NullAppender getInstance() {
        return NullAppender.instance;
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
}
