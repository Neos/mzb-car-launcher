package org.apache.log4j.varia;

import org.apache.log4j.*;

public class ExternallyRolledFileAppender extends RollingFileAppender
{
    public static final String OK = "OK";
    public static final String ROLL_OVER = "RollOver";
    HUP hup;
    int port;
    
    public ExternallyRolledFileAppender() {
        this.port = 0;
    }
    
    @Override
    public void activateOptions() {
        super.activateOptions();
        if (this.port != 0) {
            if (this.hup != null) {
                this.hup.interrupt();
            }
            (this.hup = new HUP(this, this.port)).setDaemon(true);
            this.hup.start();
        }
    }
    
    public int getPort() {
        return this.port;
    }
    
    public void setPort(final int port) {
        this.port = port;
    }
}
