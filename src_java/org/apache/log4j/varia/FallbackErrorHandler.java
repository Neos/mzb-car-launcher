package org.apache.log4j.varia;

import java.util.*;
import org.apache.log4j.spi.*;
import java.io.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.*;

public class FallbackErrorHandler implements ErrorHandler
{
    Appender backup;
    Vector loggers;
    Appender primary;
    
    @Override
    public void activateOptions() {
    }
    
    @Override
    public void error(final String s) {
    }
    
    @Override
    public void error(final String s, final Exception ex, final int n) {
        this.error(s, ex, n, null);
    }
    
    @Override
    public void error(final String s, final Exception ex, int i, final LoggingEvent loggingEvent) {
        if (ex instanceof InterruptedIOException) {
            Thread.currentThread().interrupt();
        }
        LogLog.debug(new StringBuffer().append("FB: The following error reported: ").append(s).toString(), ex);
        LogLog.debug("FB: INITIATING FALLBACK PROCEDURE.");
        if (this.loggers != null) {
            Logger logger;
            for (i = 0; i < this.loggers.size(); ++i) {
                logger = this.loggers.elementAt(i);
                LogLog.debug(new StringBuffer().append("FB: Searching for [").append(this.primary.getName()).append("] in logger [").append(logger.getName()).append("].").toString());
                LogLog.debug(new StringBuffer().append("FB: Replacing [").append(this.primary.getName()).append("] by [").append(this.backup.getName()).append("] in logger [").append(logger.getName()).append("].").toString());
                logger.removeAppender(this.primary);
                LogLog.debug(new StringBuffer().append("FB: Adding appender [").append(this.backup.getName()).append("] to logger ").append(logger.getName()).toString());
                logger.addAppender(this.backup);
            }
        }
    }
    
    @Override
    public void setAppender(final Appender primary) {
        LogLog.debug(new StringBuffer().append("FB: Setting primary appender to [").append(primary.getName()).append("].").toString());
        this.primary = primary;
    }
    
    @Override
    public void setBackupAppender(final Appender backup) {
        LogLog.debug(new StringBuffer().append("FB: Setting backup appender to [").append(backup.getName()).append("].").toString());
        this.backup = backup;
    }
    
    @Override
    public void setLogger(final Logger logger) {
        LogLog.debug(new StringBuffer().append("FB: Adding logger [").append(logger.getName()).append("].").toString());
        if (this.loggers == null) {
            this.loggers = new Vector();
        }
        this.loggers.addElement(logger);
    }
}
