package org.apache.log4j.varia;

import org.apache.log4j.spi.*;

public class DenyAllFilter extends Filter
{
    @Override
    public int decide(final LoggingEvent loggingEvent) {
        return -1;
    }
    
    public String[] getOptionStrings() {
        return null;
    }
    
    public void setOption(final String s, final String s2) {
    }
}
