package org.apache.log4j.varia;

import java.net.*;
import java.io.*;
import org.apache.log4j.helpers.*;

class HUPNode implements Runnable
{
    DataInputStream dis;
    DataOutputStream dos;
    ExternallyRolledFileAppender er;
    Socket socket;
    
    public HUPNode(final Socket socket, final ExternallyRolledFileAppender er) {
        this.socket = socket;
        this.er = er;
        try {
            this.dis = new DataInputStream(socket.getInputStream());
            this.dos = new DataOutputStream(socket.getOutputStream());
        }
        catch (InterruptedIOException ex) {
            Thread.currentThread().interrupt();
            ex.printStackTrace();
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
        }
        catch (RuntimeException ex3) {
            ex3.printStackTrace();
        }
    }
    
    @Override
    public void run() {
        try {
            final String utf = this.dis.readUTF();
            LogLog.debug("Got external roll over signal.");
            if (!"RollOver".equals(utf)) {
                goto Label_0074;
            }
            synchronized (this.er) {
                this.er.rollOver();
                // monitorexit(this.er)
                this.dos.writeUTF("OK");
                this.dos.close();
            }
        }
        catch (InterruptedIOException ex) {
            Thread.currentThread().interrupt();
            LogLog.error("Unexpected exception. Exiting HUPNode.", ex);
        }
        catch (IOException ex2) {
            LogLog.error("Unexpected exception. Exiting HUPNode.", ex2);
        }
        catch (RuntimeException ex3) {
            LogLog.error("Unexpected exception. Exiting HUPNode.", ex3);
        }
    }
}
