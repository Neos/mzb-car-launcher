package org.apache.log4j.varia;

import org.apache.log4j.spi.*;
import org.apache.log4j.*;

public class LevelRangeFilter extends Filter
{
    boolean acceptOnMatch;
    Level levelMax;
    Level levelMin;
    
    public LevelRangeFilter() {
        this.acceptOnMatch = false;
    }
    
    @Override
    public int decide(final LoggingEvent loggingEvent) {
        if ((this.levelMin != null && !loggingEvent.getLevel().isGreaterOrEqual(this.levelMin)) || (this.levelMax != null && loggingEvent.getLevel().toInt() > this.levelMax.toInt())) {
            return -1;
        }
        if (this.acceptOnMatch) {
            return 1;
        }
        return 0;
    }
    
    public boolean getAcceptOnMatch() {
        return this.acceptOnMatch;
    }
    
    public Level getLevelMax() {
        return this.levelMax;
    }
    
    public Level getLevelMin() {
        return this.levelMin;
    }
    
    public void setAcceptOnMatch(final boolean acceptOnMatch) {
        this.acceptOnMatch = acceptOnMatch;
    }
    
    public void setLevelMax(final Level levelMax) {
        this.levelMax = levelMax;
    }
    
    public void setLevelMin(final Level levelMin) {
        this.levelMin = levelMin;
    }
}
