package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class FullLocationPatternConverter extends LoggingEventPatternConverter
{
    private static final FullLocationPatternConverter INSTANCE;
    
    static {
        INSTANCE = new FullLocationPatternConverter();
    }
    
    private FullLocationPatternConverter() {
        super("Full Location", "fullLocation");
    }
    
    public static FullLocationPatternConverter newInstance(final String[] array) {
        return FullLocationPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final LocationInfo locationInformation = loggingEvent.getLocationInformation();
        if (locationInformation != null) {
            sb.append(locationInformation.fullInfo);
        }
    }
}
