package org.apache.log4j.pattern;

public final class FormattingInfo
{
    private static final FormattingInfo DEFAULT;
    private static final char[] SPACES;
    private final boolean leftAlign;
    private final int maxLength;
    private final int minLength;
    
    static {
        SPACES = new char[] { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };
        DEFAULT = new FormattingInfo(false, 0, Integer.MAX_VALUE);
    }
    
    public FormattingInfo(final boolean leftAlign, final int minLength, final int maxLength) {
        this.leftAlign = leftAlign;
        this.minLength = minLength;
        this.maxLength = maxLength;
    }
    
    public static FormattingInfo getDefault() {
        return FormattingInfo.DEFAULT;
    }
    
    public void format(int i, final StringBuffer sb) {
        final int n = sb.length() - i;
        if (n > this.maxLength) {
            sb.delete(i, sb.length() - this.maxLength);
        }
        else if (n < this.minLength) {
            if (!this.leftAlign) {
                int j;
                for (j = this.minLength - n; j > 8; j -= 8) {
                    sb.insert(i, FormattingInfo.SPACES);
                }
                sb.insert(i, FormattingInfo.SPACES, 0, j);
                return;
            }
            final int length = sb.length();
            sb.setLength(this.minLength + i);
            for (i = length; i < sb.length(); ++i) {
                sb.setCharAt(i, ' ');
            }
        }
    }
    
    public int getMaxLength() {
        return this.maxLength;
    }
    
    public int getMinLength() {
        return this.minLength;
    }
    
    public boolean isLeftAligned() {
        return this.leftAlign;
    }
}
