package org.apache.log4j.pattern;

import org.apache.log4j.helpers.*;
import java.util.*;
import org.apache.log4j.spi.*;

public final class BridgePatternConverter extends PatternConverter
{
    private boolean handlesExceptions;
    private LoggingEventPatternConverter[] patternConverters;
    private FormattingInfo[] patternFields;
    
    public BridgePatternConverter(final String s) {
        this.next = null;
        this.handlesExceptions = false;
        final ArrayList<LoggingEventPatternConverter> list = new ArrayList<LoggingEventPatternConverter>();
        final ArrayList<FormattingInfo> list2 = new ArrayList<FormattingInfo>();
        PatternParser.parse(s, list, list2, null, PatternParser.getPatternLayoutRules());
        this.patternConverters = new LoggingEventPatternConverter[list.size()];
        this.patternFields = new FormattingInfo[list.size()];
        int n = 0;
        final Iterator<Object> iterator = list.iterator();
        final Iterator<Object> iterator2 = list2.iterator();
        while (iterator.hasNext()) {
            final LoggingEventPatternConverter next = iterator.next();
            if (next instanceof LoggingEventPatternConverter) {
                this.patternConverters[n] = next;
                this.handlesExceptions |= this.patternConverters[n].handlesThrowable();
            }
            else {
                this.patternConverters[n] = new LiteralPatternConverter("");
            }
            if (iterator2.hasNext()) {
                this.patternFields[n] = iterator2.next();
            }
            else {
                this.patternFields[n] = FormattingInfo.getDefault();
            }
            ++n;
        }
    }
    
    @Override
    protected String convert(final LoggingEvent loggingEvent) {
        final StringBuffer sb = new StringBuffer();
        this.format(sb, loggingEvent);
        return sb.toString();
    }
    
    @Override
    public void format(final StringBuffer sb, final LoggingEvent loggingEvent) {
        for (int i = 0; i < this.patternConverters.length; ++i) {
            final int length = sb.length();
            this.patternConverters[i].format(loggingEvent, sb);
            this.patternFields[i].format(length, sb);
        }
    }
    
    public boolean ignoresThrowable() {
        return !this.handlesExceptions;
    }
}
