package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;
import java.util.*;
import java.text.*;

public final class DatePatternConverter extends LoggingEventPatternConverter
{
    private static final String ABSOLUTE_FORMAT = "ABSOLUTE";
    private static final String ABSOLUTE_TIME_PATTERN = "HH:mm:ss,SSS";
    private static final String DATE_AND_TIME_FORMAT = "DATE";
    private static final String DATE_AND_TIME_PATTERN = "dd MMM yyyy HH:mm:ss,SSS";
    private static final String ISO8601_FORMAT = "ISO8601";
    private static final String ISO8601_PATTERN = "yyyy-MM-dd HH:mm:ss,SSS";
    private final CachedDateFormat df;
    
    private DatePatternConverter(final String[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ldc             "Date"
        //     3: ldc             "date"
        //     5: invokespecial   org/apache/log4j/pattern/LoggingEventPatternConverter.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //     8: aload_1        
        //     9: ifnull          17
        //    12: aload_1        
        //    13: arraylength    
        //    14: ifne            102
        //    17: aconst_null    
        //    18: astore          5
        //    20: aload           5
        //    22: ifnull          35
        //    25: aload           5
        //    27: ldc             "ISO8601"
        //    29: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //    32: ifeq            110
        //    35: ldc             "yyyy-MM-dd HH:mm:ss,SSS"
        //    37: astore          4
        //    39: sipush          1000
        //    42: istore_2       
        //    43: new             Ljava/text/SimpleDateFormat;
        //    46: dup            
        //    47: aload           4
        //    49: invokespecial   java/text/SimpleDateFormat.<init>:(Ljava/lang/String;)V
        //    52: astore          6
        //    54: aload           4
        //    56: invokestatic    org/apache/log4j/pattern/CachedDateFormat.getMaximumCacheValidity:(Ljava/lang/String;)I
        //    59: istore_3       
        //    60: iload_3        
        //    61: istore_2       
        //    62: aload           6
        //    64: astore          4
        //    66: aload_1        
        //    67: ifnull          192
        //    70: aload_1        
        //    71: arraylength    
        //    72: iconst_1       
        //    73: if_icmple       192
        //    76: aload           4
        //    78: aload_1        
        //    79: iconst_1       
        //    80: aaload         
        //    81: invokestatic    java/util/TimeZone.getTimeZone:(Ljava/lang/String;)Ljava/util/TimeZone;
        //    84: invokevirtual   java/text/DateFormat.setTimeZone:(Ljava/util/TimeZone;)V
        //    87: aload_0        
        //    88: new             Lorg/apache/log4j/pattern/CachedDateFormat;
        //    91: dup            
        //    92: aload           4
        //    94: iload_2        
        //    95: invokespecial   org/apache/log4j/pattern/CachedDateFormat.<init>:(Ljava/text/DateFormat;I)V
        //    98: putfield        org/apache/log4j/pattern/DatePatternConverter.df:Lorg/apache/log4j/pattern/CachedDateFormat;
        //   101: return         
        //   102: aload_1        
        //   103: iconst_0       
        //   104: aaload         
        //   105: astore          5
        //   107: goto            20
        //   110: aload           5
        //   112: ldc             "ABSOLUTE"
        //   114: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   117: ifeq            127
        //   120: ldc             "HH:mm:ss,SSS"
        //   122: astore          4
        //   124: goto            39
        //   127: aload           5
        //   129: ldc             "DATE"
        //   131: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   134: ifeq            144
        //   137: ldc             "dd MMM yyyy HH:mm:ss,SSS"
        //   139: astore          4
        //   141: goto            39
        //   144: aload           5
        //   146: astore          4
        //   148: goto            39
        //   151: astore          4
        //   153: new             Ljava/lang/StringBuffer;
        //   156: dup            
        //   157: invokespecial   java/lang/StringBuffer.<init>:()V
        //   160: ldc             "Could not instantiate SimpleDateFormat with pattern "
        //   162: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   165: aload           5
        //   167: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   170: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   173: aload           4
        //   175: invokestatic    org/apache/log4j/helpers/LogLog.warn:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   178: new             Ljava/text/SimpleDateFormat;
        //   181: dup            
        //   182: ldc             "yyyy-MM-dd HH:mm:ss,SSS"
        //   184: invokespecial   java/text/SimpleDateFormat.<init>:(Ljava/lang/String;)V
        //   187: astore          4
        //   189: goto            66
        //   192: new             Lorg/apache/log4j/pattern/DatePatternConverter$DefaultZoneDateFormat;
        //   195: dup            
        //   196: aload           4
        //   198: invokespecial   org/apache/log4j/pattern/DatePatternConverter$DefaultZoneDateFormat.<init>:(Ljava/text/DateFormat;)V
        //   201: astore          4
        //   203: goto            87
        //   206: astore          4
        //   208: goto            153
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  43     54     151    153    Ljava/lang/IllegalArgumentException;
        //  54     60     206    211    Ljava/lang/IllegalArgumentException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0066:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static DatePatternConverter newInstance(final String[] array) {
        return new DatePatternConverter(array);
    }
    
    @Override
    public void format(final Object o, final StringBuffer sb) {
        if (o instanceof Date) {
            this.format((Date)o, sb);
        }
        super.format(o, sb);
    }
    
    public void format(final Date date, final StringBuffer sb) {
        synchronized (this) {
            this.df.format(date.getTime(), sb);
        }
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        synchronized (this) {
            this.df.format(loggingEvent.timeStamp, sb);
        }
    }
    
    private static class DefaultZoneDateFormat extends DateFormat
    {
        private static final long serialVersionUID = 1L;
        private final DateFormat dateFormat;
        
        public DefaultZoneDateFormat(final DateFormat dateFormat) {
            this.dateFormat = dateFormat;
        }
        
        @Override
        public StringBuffer format(final Date date, final StringBuffer sb, final FieldPosition fieldPosition) {
            this.dateFormat.setTimeZone(TimeZone.getDefault());
            return this.dateFormat.format(date, sb, fieldPosition);
        }
        
        @Override
        public Date parse(final String s, final ParsePosition parsePosition) {
            this.dateFormat.setTimeZone(TimeZone.getDefault());
            return this.dateFormat.parse(s, parsePosition);
        }
    }
}
