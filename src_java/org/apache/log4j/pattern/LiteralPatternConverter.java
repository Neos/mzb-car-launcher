package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class LiteralPatternConverter extends LoggingEventPatternConverter
{
    private final String literal;
    
    public LiteralPatternConverter(final String literal) {
        super("Literal", "literal");
        this.literal = literal;
    }
    
    @Override
    public void format(final Object o, final StringBuffer sb) {
        sb.append(this.literal);
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append(this.literal);
    }
}
