package org.apache.log4j.pattern;

import java.lang.reflect.*;
import org.apache.log4j.helpers.*;
import java.io.*;
import org.apache.log4j.*;
import java.util.*;
import org.apache.log4j.spi.*;

public class LogEvent implements Serializable
{
    static final Integer[] PARAM_ARRAY;
    static final String TO_LEVEL = "toLevel";
    static final Class[] TO_LEVEL_PARAMS;
    static Class class$org$apache$log4j$Level;
    static final Hashtable methodCache;
    static final long serialVersionUID = -868428216207166145L;
    private static long startTime;
    public final String categoryName;
    public final transient String fqnOfCategoryClass;
    public transient Priority level;
    private LocationInfo locationInfo;
    private transient Category logger;
    private Hashtable mdcCopy;
    private boolean mdcCopyLookupRequired;
    private transient Object message;
    private String ndc;
    private boolean ndcLookupRequired;
    private String renderedMessage;
    private String threadName;
    private ThrowableInformation throwableInfo;
    public final long timeStamp;
    
    static {
        LogEvent.startTime = System.currentTimeMillis();
        PARAM_ARRAY = new Integer[1];
        TO_LEVEL_PARAMS = new Class[] { Integer.TYPE };
        methodCache = new Hashtable(3);
    }
    
    public LogEvent(final String fqnOfCategoryClass, final Category logger, final long timeStamp, final Priority level, final Object message, final Throwable t) {
        this.ndcLookupRequired = true;
        this.mdcCopyLookupRequired = true;
        this.fqnOfCategoryClass = fqnOfCategoryClass;
        this.logger = logger;
        this.categoryName = logger.getName();
        this.level = level;
        this.message = message;
        if (t != null) {
            this.throwableInfo = new ThrowableInformation(t);
        }
        this.timeStamp = timeStamp;
    }
    
    public LogEvent(final String fqnOfCategoryClass, final Category logger, final Priority level, final Object message, final Throwable t) {
        this.ndcLookupRequired = true;
        this.mdcCopyLookupRequired = true;
        this.fqnOfCategoryClass = fqnOfCategoryClass;
        this.logger = logger;
        this.categoryName = logger.getName();
        this.level = level;
        this.message = message;
        if (t != null) {
            this.throwableInfo = new ThrowableInformation(t);
        }
        this.timeStamp = System.currentTimeMillis();
    }
    
    public LogEvent(final String fqnOfCategoryClass, final Logger logger, final long timeStamp, final Level level, final Object message, final String threadName, final ThrowableInformation throwableInfo, final String ndc, final LocationInfo locationInfo, final Map map) {
        this.ndcLookupRequired = true;
        this.mdcCopyLookupRequired = true;
        this.fqnOfCategoryClass = fqnOfCategoryClass;
        this.logger = logger;
        if (logger != null) {
            this.categoryName = logger.getName();
        }
        else {
            this.categoryName = null;
        }
        this.level = level;
        this.message = message;
        if (throwableInfo != null) {
            this.throwableInfo = throwableInfo;
        }
        this.timeStamp = timeStamp;
        this.threadName = threadName;
        this.ndcLookupRequired = false;
        this.ndc = ndc;
        this.locationInfo = locationInfo;
        this.mdcCopyLookupRequired = false;
        if (map != null) {
            this.mdcCopy = new Hashtable(map);
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static long getStartTime() {
        return LogEvent.startTime;
    }
    
    private void readLevel(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        final int int1 = objectInputStream.readInt();
        try {
            final String s = (String)objectInputStream.readObject();
            if (s == null) {
                this.level = Level.toLevel(int1);
                return;
            }
            Method declaredMethod;
            if ((declaredMethod = LogEvent.methodCache.get(s)) == null) {
                declaredMethod = Loader.loadClass(s).getDeclaredMethod("toLevel", (Class[])LogEvent.TO_LEVEL_PARAMS);
                LogEvent.methodCache.put(s, declaredMethod);
            }
            LogEvent.PARAM_ARRAY[0] = new Integer(int1);
            this.level = (Level)declaredMethod.invoke(null, (Object[])LogEvent.PARAM_ARRAY);
        }
        catch (Exception ex) {
            LogLog.warn("Level deserialization failed, reverting to default.", ex);
            this.level = Level.toLevel(int1);
        }
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.readLevel(objectInputStream);
        if (this.locationInfo == null) {
            this.locationInfo = new LocationInfo(null, null);
        }
    }
    
    private void writeLevel(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.level.toInt());
        final Class<? extends Priority> class1 = this.level.getClass();
        Class class$org$apache$log4j$Level;
        if (LogEvent.class$org$apache$log4j$Level == null) {
            class$org$apache$log4j$Level = (LogEvent.class$org$apache$log4j$Level = class$("org.apache.log4j.Level"));
        }
        else {
            class$org$apache$log4j$Level = LogEvent.class$org$apache$log4j$Level;
        }
        if (class1 == class$org$apache$log4j$Level) {
            objectOutputStream.writeObject(null);
            return;
        }
        objectOutputStream.writeObject(class1.getName());
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        this.getThreadName();
        this.getRenderedMessage();
        this.getNDC();
        this.getMDCCopy();
        this.getThrowableStrRep();
        objectOutputStream.defaultWriteObject();
        this.writeLevel(objectOutputStream);
    }
    
    public String getFQNOfLoggerClass() {
        return this.fqnOfCategoryClass;
    }
    
    public Level getLevel() {
        return (Level)this.level;
    }
    
    public LocationInfo getLocationInformation() {
        if (this.locationInfo == null) {
            this.locationInfo = new LocationInfo(new Throwable(), this.fqnOfCategoryClass);
        }
        return this.locationInfo;
    }
    
    public String getLoggerName() {
        return this.categoryName;
    }
    
    public Object getMDC(final String s) {
        if (this.mdcCopy != null) {
            final Object value = this.mdcCopy.get(s);
            if (value != null) {
                return value;
            }
        }
        return MDC.get(s);
    }
    
    public void getMDCCopy() {
        if (this.mdcCopyLookupRequired) {
            this.mdcCopyLookupRequired = false;
            final Hashtable context = MDC.getContext();
            if (context != null) {
                this.mdcCopy = (Hashtable)context.clone();
            }
        }
    }
    
    public Object getMessage() {
        if (this.message != null) {
            return this.message;
        }
        return this.getRenderedMessage();
    }
    
    public String getNDC() {
        if (this.ndcLookupRequired) {
            this.ndcLookupRequired = false;
            this.ndc = NDC.get();
        }
        return this.ndc;
    }
    
    public Map getProperties() {
        this.getMDCCopy();
        Cloneable mdcCopy;
        if (this.mdcCopy == null) {
            mdcCopy = new HashMap<Object, Object>();
        }
        else {
            mdcCopy = this.mdcCopy;
        }
        return Collections.unmodifiableMap((Map<?, ?>)mdcCopy);
    }
    
    public final String getProperty(String string) {
        final Object mdc = this.getMDC(string);
        string = null;
        if (mdc != null) {
            string = mdc.toString();
        }
        return string;
    }
    
    public Set getPropertyKeySet() {
        return this.getProperties().keySet();
    }
    
    public String getRenderedMessage() {
        if (this.renderedMessage == null && this.message != null) {
            if (this.message instanceof String) {
                this.renderedMessage = (String)this.message;
            }
            else {
                final LoggerRepository loggerRepository = this.logger.getLoggerRepository();
                if (loggerRepository instanceof RendererSupport) {
                    this.renderedMessage = ((RendererSupport)loggerRepository).getRendererMap().findAndRender(this.message);
                }
                else {
                    this.renderedMessage = this.message.toString();
                }
            }
        }
        return this.renderedMessage;
    }
    
    public String getThreadName() {
        if (this.threadName == null) {
            this.threadName = Thread.currentThread().getName();
        }
        return this.threadName;
    }
    
    public ThrowableInformation getThrowableInformation() {
        return this.throwableInfo;
    }
    
    public String[] getThrowableStrRep() {
        if (this.throwableInfo == null) {
            return null;
        }
        return this.throwableInfo.getThrowableStrRep();
    }
    
    public final long getTimeStamp() {
        return this.timeStamp;
    }
    
    public final boolean locationInformationExists() {
        return this.locationInfo != null;
    }
    
    public final void setProperty(final String s, final String s2) {
        if (this.mdcCopy == null) {
            this.getMDCCopy();
        }
        if (this.mdcCopy == null) {
            this.mdcCopy = new Hashtable();
        }
        this.mdcCopy.put(s, s2);
    }
}
