package org.apache.log4j.pattern;

import java.util.*;

public abstract class NameAbbreviator
{
    private static final NameAbbreviator DEFAULT;
    
    static {
        DEFAULT = new NOPAbbreviator();
    }
    
    public static NameAbbreviator getAbbreviator(String trim) {
        if (trim.length() <= 0) {
            return NameAbbreviator.DEFAULT;
        }
        trim = trim.trim();
        if (trim.length() == 0) {
            return NameAbbreviator.DEFAULT;
        }
        int n = 0;
        int n2 = 0;
        if (trim.length() > 0) {
            if (trim.charAt(0) == '-') {
                n2 = 0 + 1;
            }
            while ((n = n2) < trim.length()) {
                n = n2;
                if (trim.charAt(n2) < '0') {
                    break;
                }
                n = n2;
                if (trim.charAt(n2) > '9') {
                    break;
                }
                ++n2;
            }
        }
        if (n != trim.length()) {
            final ArrayList<PatternAbbreviatorFragment> list = new ArrayList<PatternAbbreviatorFragment>(5);
            int index;
            for (int n3 = 0; n3 < trim.length() && n3 >= 0; n3 = index + 1) {
                int n4 = n3;
                int n5;
                if (trim.charAt(n3) == '*') {
                    n5 = Integer.MAX_VALUE;
                    ++n4;
                }
                else if (trim.charAt(n3) >= '0' && trim.charAt(n3) <= '9') {
                    n5 = trim.charAt(n3) - '0';
                    ++n4;
                }
                else {
                    n5 = 0;
                }
                char char1 = '\0';
                if (n4 < trim.length() && (char1 = trim.charAt(n4)) == '.') {
                    char1 = '\0';
                }
                list.add(new PatternAbbreviatorFragment(n5, char1));
                index = trim.indexOf(".", n3);
                if (index == -1) {
                    break;
                }
            }
            return new PatternAbbreviator(list);
        }
        final int int1 = Integer.parseInt(trim);
        if (int1 >= 0) {
            return new MaxElementAbbreviator(int1);
        }
        return new DropElementAbbreviator(-int1);
    }
    
    public static NameAbbreviator getDefaultAbbreviator() {
        return NameAbbreviator.DEFAULT;
    }
    
    public abstract void abbreviate(final int p0, final StringBuffer p1);
    
    private static class DropElementAbbreviator extends NameAbbreviator
    {
        private final int count;
        
        public DropElementAbbreviator(final int count) {
            this.count = count;
        }
        
        @Override
        public void abbreviate(final int n, final StringBuffer sb) {
            int count = this.count;
            for (int i = sb.indexOf(".", n); i != -1; i = sb.indexOf(".", i + 1)) {
                --count;
                if (count == 0) {
                    sb.delete(n, i + 1);
                    break;
                }
            }
        }
    }
    
    private static class MaxElementAbbreviator extends NameAbbreviator
    {
        private final int count;
        
        public MaxElementAbbreviator(final int count) {
            this.count = count;
        }
        
        @Override
        public void abbreviate(final int n, final StringBuffer sb) {
            int lastIndex = sb.length() - 1;
            final String string = sb.toString();
            for (int i = this.count; i > 0; --i) {
                lastIndex = string.lastIndexOf(".", lastIndex - 1);
                if (lastIndex == -1 || lastIndex < n) {
                    return;
                }
            }
            sb.delete(n, lastIndex + 1);
        }
    }
    
    private static class NOPAbbreviator extends NameAbbreviator
    {
        @Override
        public void abbreviate(final int n, final StringBuffer sb) {
        }
    }
    
    private static class PatternAbbreviator extends NameAbbreviator
    {
        private final PatternAbbreviatorFragment[] fragments;
        
        public PatternAbbreviator(final List list) {
            if (list.size() == 0) {
                throw new IllegalArgumentException("fragments must have at least one element");
            }
            list.toArray(this.fragments = new PatternAbbreviatorFragment[list.size()]);
        }
        
        @Override
        public void abbreviate(int n, final StringBuffer sb) {
            for (int n2 = 0; n2 < this.fragments.length - 1 && n < sb.length(); n = this.fragments[n2].abbreviate(sb, n), ++n2) {}
            for (PatternAbbreviatorFragment patternAbbreviatorFragment = this.fragments[this.fragments.length - 1]; n < sb.length() && n >= 0; n = patternAbbreviatorFragment.abbreviate(sb, n)) {}
        }
    }
    
    private static class PatternAbbreviatorFragment
    {
        private final int charCount;
        private final char ellipsis;
        
        public PatternAbbreviatorFragment(final int charCount, final char ellipsis) {
            this.charCount = charCount;
            this.ellipsis = ellipsis;
        }
        
        public int abbreviate(final StringBuffer sb, int n) {
            int index;
            final int n2 = index = sb.toString().indexOf(".", n);
            if (n2 != -1) {
                int n3 = n2;
                if (n2 - n > this.charCount) {
                    sb.delete(this.charCount + n, n2);
                    n = (n3 = n + this.charCount);
                    if (this.ellipsis != '\0') {
                        sb.insert(n, this.ellipsis);
                        n3 = n + 1;
                    }
                }
                index = n3 + 1;
            }
            return index;
        }
    }
}
