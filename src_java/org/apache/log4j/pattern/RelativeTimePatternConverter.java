package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public class RelativeTimePatternConverter extends LoggingEventPatternConverter
{
    private CachedTimestamp lastTimestamp;
    
    public RelativeTimePatternConverter() {
        super("Time", "time");
        this.lastTimestamp = new CachedTimestamp(0L, "");
    }
    
    public static RelativeTimePatternConverter newInstance(final String[] array) {
        return new RelativeTimePatternConverter();
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final long timeStamp = loggingEvent.timeStamp;
        if (!this.lastTimestamp.format(timeStamp, sb)) {
            final String string = Long.toString(timeStamp - LoggingEvent.getStartTime());
            sb.append(string);
            this.lastTimestamp = new CachedTimestamp(timeStamp, string);
        }
    }
    
    private static final class CachedTimestamp
    {
        private final String formatted;
        private final long timestamp;
        
        public CachedTimestamp(final long timestamp, final String formatted) {
            this.timestamp = timestamp;
            this.formatted = formatted;
        }
        
        public boolean format(final long n, final StringBuffer sb) {
            if (n == this.timestamp) {
                sb.append(this.formatted);
                return true;
            }
            return false;
        }
    }
}
