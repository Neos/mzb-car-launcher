package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public class SequenceNumberPatternConverter extends LoggingEventPatternConverter
{
    private static final SequenceNumberPatternConverter INSTANCE;
    
    static {
        INSTANCE = new SequenceNumberPatternConverter();
    }
    
    private SequenceNumberPatternConverter() {
        super("Sequence Number", "sn");
    }
    
    public static SequenceNumberPatternConverter newInstance(final String[] array) {
        return SequenceNumberPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append("0");
    }
}
