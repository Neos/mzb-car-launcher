package org.apache.log4j.pattern;

public final class FileDatePatternConverter
{
    public static PatternConverter newInstance(final String[] array) {
        if (array == null || array.length == 0) {
            return DatePatternConverter.newInstance(new String[] { "yyyy-MM-dd" });
        }
        return DatePatternConverter.newInstance(array);
    }
}
