package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;
import java.util.*;

public final class PropertiesPatternConverter extends LoggingEventPatternConverter
{
    private final String option;
    
    private PropertiesPatternConverter(final String[] array) {
        String string;
        if (array != null && array.length > 0) {
            string = new StringBuffer().append("Property{").append(array[0]).append("}").toString();
        }
        else {
            string = "Properties";
        }
        super(string, "property");
        if (array != null && array.length > 0) {
            this.option = array[0];
            return;
        }
        this.option = null;
    }
    
    public static PropertiesPatternConverter newInstance(final String[] array) {
        return new PropertiesPatternConverter(array);
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        if (this.option == null) {
            sb.append("{");
            try {
                final Set propertyKeySet = MDCKeySetExtractor.INSTANCE.getPropertyKeySet(loggingEvent);
                if (propertyKeySet != null) {
                    for (final Object next : propertyKeySet) {
                        sb.append("{").append(next).append(",").append(loggingEvent.getMDC(next.toString())).append("}");
                    }
                }
            }
            catch (Exception ex) {
                LogLog.error("Unexpected exception while extracting MDC keys", ex);
            }
            sb.append("}");
        }
        else {
            final Object mdc = loggingEvent.getMDC(this.option);
            if (mdc != null) {
                sb.append(mdc);
            }
        }
    }
}
