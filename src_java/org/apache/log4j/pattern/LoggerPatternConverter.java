package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class LoggerPatternConverter extends NamePatternConverter
{
    private static final LoggerPatternConverter INSTANCE;
    
    static {
        INSTANCE = new LoggerPatternConverter(null);
    }
    
    private LoggerPatternConverter(final String[] array) {
        super("Logger", "logger", array);
    }
    
    public static LoggerPatternConverter newInstance(final String[] array) {
        if (array == null || array.length == 0) {
            return LoggerPatternConverter.INSTANCE;
        }
        return new LoggerPatternConverter(array);
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final int length = sb.length();
        sb.append(loggingEvent.getLoggerName());
        this.abbreviate(length, sb);
    }
}
