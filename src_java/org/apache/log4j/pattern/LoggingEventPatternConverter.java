package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public abstract class LoggingEventPatternConverter extends PatternConverter
{
    protected LoggingEventPatternConverter(final String s, final String s2) {
        super(s, s2);
    }
    
    @Override
    public void format(final Object o, final StringBuffer sb) {
        if (o instanceof LoggingEvent) {
            this.format((LoggingEvent)o, sb);
        }
    }
    
    public abstract void format(final LoggingEvent p0, final StringBuffer p1);
    
    public boolean handlesThrowable() {
        return false;
    }
}
