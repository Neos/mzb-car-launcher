package org.apache.log4j.pattern;

public abstract class NamePatternConverter extends LoggingEventPatternConverter
{
    private final NameAbbreviator abbreviator;
    
    protected NamePatternConverter(final String s, final String s2, final String[] array) {
        super(s, s2);
        if (array != null && array.length > 0) {
            this.abbreviator = NameAbbreviator.getAbbreviator(array[0]);
            return;
        }
        this.abbreviator = NameAbbreviator.getDefaultAbbreviator();
    }
    
    protected final void abbreviate(final int n, final StringBuffer sb) {
        this.abbreviator.abbreviate(n, sb);
    }
}
