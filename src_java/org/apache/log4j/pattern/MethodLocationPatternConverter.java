package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class MethodLocationPatternConverter extends LoggingEventPatternConverter
{
    private static final MethodLocationPatternConverter INSTANCE;
    
    static {
        INSTANCE = new MethodLocationPatternConverter();
    }
    
    private MethodLocationPatternConverter() {
        super("Method", "method");
    }
    
    public static MethodLocationPatternConverter newInstance(final String[] array) {
        return MethodLocationPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final LocationInfo locationInformation = loggingEvent.getLocationInformation();
        if (locationInformation != null) {
            sb.append(locationInformation.getMethodName());
        }
    }
}
