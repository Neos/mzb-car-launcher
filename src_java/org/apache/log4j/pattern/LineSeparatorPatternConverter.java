package org.apache.log4j.pattern;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;

public final class LineSeparatorPatternConverter extends LoggingEventPatternConverter
{
    private static final LineSeparatorPatternConverter INSTANCE;
    private final String lineSep;
    
    static {
        INSTANCE = new LineSeparatorPatternConverter();
    }
    
    private LineSeparatorPatternConverter() {
        super("Line Sep", "lineSep");
        this.lineSep = Layout.LINE_SEP;
    }
    
    public static LineSeparatorPatternConverter newInstance(final String[] array) {
        return LineSeparatorPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final Object o, final StringBuffer sb) {
        sb.append(this.lineSep);
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append(this.lineSep);
    }
}
