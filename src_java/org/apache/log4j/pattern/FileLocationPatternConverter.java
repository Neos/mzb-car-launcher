package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class FileLocationPatternConverter extends LoggingEventPatternConverter
{
    private static final FileLocationPatternConverter INSTANCE;
    
    static {
        INSTANCE = new FileLocationPatternConverter();
    }
    
    private FileLocationPatternConverter() {
        super("File Location", "file");
    }
    
    public static FileLocationPatternConverter newInstance(final String[] array) {
        return FileLocationPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final LocationInfo locationInformation = loggingEvent.getLocationInformation();
        if (locationInformation != null) {
            sb.append(locationInformation.getFileName());
        }
    }
}
