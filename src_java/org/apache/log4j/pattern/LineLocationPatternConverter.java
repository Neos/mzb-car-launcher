package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class LineLocationPatternConverter extends LoggingEventPatternConverter
{
    private static final LineLocationPatternConverter INSTANCE;
    
    static {
        INSTANCE = new LineLocationPatternConverter();
    }
    
    private LineLocationPatternConverter() {
        super("Line", "line");
    }
    
    public static LineLocationPatternConverter newInstance(final String[] array) {
        return LineLocationPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final LocationInfo locationInformation = loggingEvent.getLocationInformation();
        if (locationInformation != null) {
            sb.append(locationInformation.getLineNumber());
        }
    }
}
