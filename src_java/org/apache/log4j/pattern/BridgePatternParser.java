package org.apache.log4j.pattern;

import org.apache.log4j.helpers.*;

public final class BridgePatternParser extends PatternParser
{
    public BridgePatternParser(final String s) {
        super(s);
    }
    
    @Override
    public PatternConverter parse() {
        return new BridgePatternConverter(this.pattern);
    }
}
