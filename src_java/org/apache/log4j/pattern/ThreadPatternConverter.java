package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public class ThreadPatternConverter extends LoggingEventPatternConverter
{
    private static final ThreadPatternConverter INSTANCE;
    
    static {
        INSTANCE = new ThreadPatternConverter();
    }
    
    private ThreadPatternConverter() {
        super("Thread", "thread");
    }
    
    public static ThreadPatternConverter newInstance(final String[] array) {
        return ThreadPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append(loggingEvent.getThreadName());
    }
}
