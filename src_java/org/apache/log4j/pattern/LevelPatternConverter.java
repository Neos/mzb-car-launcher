package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class LevelPatternConverter extends LoggingEventPatternConverter
{
    private static final LevelPatternConverter INSTANCE;
    private static final int TRACE_INT = 5000;
    
    static {
        INSTANCE = new LevelPatternConverter();
    }
    
    private LevelPatternConverter() {
        super("Level", "level");
    }
    
    public static LevelPatternConverter newInstance(final String[] array) {
        return LevelPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append(loggingEvent.getLevel().toString());
    }
    
    @Override
    public String getStyleClass(final Object o) {
        if (!(o instanceof LoggingEvent)) {
            return "level";
        }
        switch (((LoggingEvent)o).getLevel().toInt()) {
            default: {
                return new StringBuffer().append("level ").append(((LoggingEvent)o).getLevel().toString()).toString();
            }
            case 5000: {
                return "level trace";
            }
            case 10000: {
                return "level debug";
            }
            case 20000: {
                return "level info";
            }
            case 30000: {
                return "level warn";
            }
            case 40000: {
                return "level error";
            }
            case 50000: {
                return "level fatal";
            }
        }
    }
}
