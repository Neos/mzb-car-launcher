package org.apache.log4j.pattern;

import java.text.*;
import java.util.*;

public final class CachedDateFormat extends DateFormat
{
    private static final String DIGITS = "0123456789";
    private static final int MAGIC1 = 654;
    private static final int MAGIC2 = 987;
    private static final String MAGICSTRING1 = "654";
    private static final String MAGICSTRING2 = "987";
    public static final int NO_MILLISECONDS = -2;
    public static final int UNRECOGNIZED_MILLISECONDS = -1;
    private static final String ZERO_STRING = "000";
    private static final long serialVersionUID = 1L;
    private StringBuffer cache;
    private final int expiration;
    private final DateFormat formatter;
    private int millisecondStart;
    private long previousTime;
    private long slotBegin;
    private final Date tmpDate;
    
    public CachedDateFormat(final DateFormat formatter, final int expiration) {
        this.cache = new StringBuffer(50);
        this.tmpDate = new Date(0L);
        if (formatter == null) {
            throw new IllegalArgumentException("dateFormat cannot be null");
        }
        if (expiration < 0) {
            throw new IllegalArgumentException("expiration must be non-negative");
        }
        this.formatter = formatter;
        this.expiration = expiration;
        this.millisecondStart = 0;
        this.previousTime = Long.MIN_VALUE;
        this.slotBegin = Long.MIN_VALUE;
    }
    
    public static int findMillisecondStart(final long n, final String s, final DateFormat dateFormat) {
        long n3;
        final long n2 = n3 = n / 1000L * 1000L;
        if (n2 > n) {
            n3 = n2 - 1000L;
        }
        final int n4 = (int)(n - n3);
        int n5 = 654;
        String s2 = "654";
        if (n4 == 654) {
            n5 = 987;
            s2 = "987";
        }
        final String format = dateFormat.format(new Date(n5 + n3));
        if (format.length() == s.length()) {
            for (int i = 0; i < s.length(); ++i) {
                if (s.charAt(i) != format.charAt(i)) {
                    final StringBuffer sb = new StringBuffer("ABC");
                    millisecondFormat(n4, sb, 0);
                    final String format2 = dateFormat.format(new Date(n3));
                    if (format2.length() == s.length() && s2.regionMatches(0, format, i, s2.length()) && sb.toString().regionMatches(0, s, i, s2.length())) {
                        final int n6 = i;
                        if ("000".regionMatches(0, format2, i, "000".length())) {
                            return n6;
                        }
                    }
                    return -1;
                }
            }
            return -2;
        }
        return -1;
    }
    
    public static int getMaximumCacheValidity(final String s) {
        final int index = s.indexOf(83);
        if (index >= 0 && index != s.lastIndexOf("SSS")) {
            return 1;
        }
        return 1000;
    }
    
    private static void millisecondFormat(final int n, final StringBuffer sb, final int n2) {
        sb.setCharAt(n2, "0123456789".charAt(n / 100));
        sb.setCharAt(n2 + 1, "0123456789".charAt(n / 10 % 10));
        sb.setCharAt(n2 + 2, "0123456789".charAt(n % 10));
    }
    
    public StringBuffer format(final long previousTime, final StringBuffer sb) {
        if (previousTime == this.previousTime) {
            sb.append(this.cache);
        }
        else {
            if (this.millisecondStart != -1 && previousTime < this.slotBegin + this.expiration && previousTime >= this.slotBegin && previousTime < this.slotBegin + 1000L) {
                if (this.millisecondStart >= 0) {
                    millisecondFormat((int)(previousTime - this.slotBegin), this.cache, this.millisecondStart);
                }
                this.previousTime = previousTime;
                sb.append(this.cache);
                return sb;
            }
            this.cache.setLength(0);
            this.tmpDate.setTime(previousTime);
            this.cache.append(this.formatter.format(this.tmpDate));
            sb.append(this.cache);
            this.previousTime = previousTime;
            this.slotBegin = this.previousTime / 1000L * 1000L;
            if (this.slotBegin > this.previousTime) {
                this.slotBegin -= 1000L;
            }
            if (this.millisecondStart >= 0) {
                this.millisecondStart = findMillisecondStart(previousTime, this.cache.toString(), this.formatter);
                return sb;
            }
        }
        return sb;
    }
    
    @Override
    public StringBuffer format(final Date date, final StringBuffer sb, final FieldPosition fieldPosition) {
        this.format(date.getTime(), sb);
        return sb;
    }
    
    @Override
    public NumberFormat getNumberFormat() {
        return this.formatter.getNumberFormat();
    }
    
    @Override
    public Date parse(final String s, final ParsePosition parsePosition) {
        return this.formatter.parse(s, parsePosition);
    }
    
    @Override
    public void setTimeZone(final TimeZone timeZone) {
        this.formatter.setTimeZone(timeZone);
        this.previousTime = Long.MIN_VALUE;
        this.slotBegin = Long.MIN_VALUE;
    }
}
