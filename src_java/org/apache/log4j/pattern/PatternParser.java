package org.apache.log4j.pattern;

import org.apache.log4j.helpers.*;
import java.io.*;
import java.util.*;

public final class PatternParser
{
    private static final int CONVERTER_STATE = 1;
    private static final int DOT_STATE = 3;
    private static final char ESCAPE_CHAR = '%';
    private static final Map FILENAME_PATTERN_RULES;
    private static final int LITERAL_STATE = 0;
    private static final int MAX_STATE = 5;
    private static final int MIN_STATE = 4;
    private static final Map PATTERN_LAYOUT_RULES;
    static Class class$org$apache$log4j$pattern$ClassNamePatternConverter;
    static Class class$org$apache$log4j$pattern$DatePatternConverter;
    static Class class$org$apache$log4j$pattern$FileDatePatternConverter;
    static Class class$org$apache$log4j$pattern$FileLocationPatternConverter;
    static Class class$org$apache$log4j$pattern$FullLocationPatternConverter;
    static Class class$org$apache$log4j$pattern$IntegerPatternConverter;
    static Class class$org$apache$log4j$pattern$LevelPatternConverter;
    static Class class$org$apache$log4j$pattern$LineLocationPatternConverter;
    static Class class$org$apache$log4j$pattern$LineSeparatorPatternConverter;
    static Class class$org$apache$log4j$pattern$LoggerPatternConverter;
    static Class class$org$apache$log4j$pattern$MessagePatternConverter;
    static Class class$org$apache$log4j$pattern$MethodLocationPatternConverter;
    static Class class$org$apache$log4j$pattern$NDCPatternConverter;
    static Class class$org$apache$log4j$pattern$PropertiesPatternConverter;
    static Class class$org$apache$log4j$pattern$RelativeTimePatternConverter;
    static Class class$org$apache$log4j$pattern$SequenceNumberPatternConverter;
    static Class class$org$apache$log4j$pattern$ThreadPatternConverter;
    static Class class$org$apache$log4j$pattern$ThrowableInformationPatternConverter;
    
    static {
        final HashMap<String, Class> hashMap = new HashMap<String, Class>(17);
        Class class$org$apache$log4j$pattern$LoggerPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$LoggerPatternConverter == null) {
            class$org$apache$log4j$pattern$LoggerPatternConverter = (PatternParser.class$org$apache$log4j$pattern$LoggerPatternConverter = class$("org.apache.log4j.pattern.LoggerPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LoggerPatternConverter = PatternParser.class$org$apache$log4j$pattern$LoggerPatternConverter;
        }
        hashMap.put("c", class$org$apache$log4j$pattern$LoggerPatternConverter);
        Class class$org$apache$log4j$pattern$LoggerPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$LoggerPatternConverter == null) {
            class$org$apache$log4j$pattern$LoggerPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$LoggerPatternConverter = class$("org.apache.log4j.pattern.LoggerPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LoggerPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$LoggerPatternConverter;
        }
        hashMap.put("logger", class$org$apache$log4j$pattern$LoggerPatternConverter2);
        Class class$org$apache$log4j$pattern$ClassNamePatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$ClassNamePatternConverter == null) {
            class$org$apache$log4j$pattern$ClassNamePatternConverter = (PatternParser.class$org$apache$log4j$pattern$ClassNamePatternConverter = class$("org.apache.log4j.pattern.ClassNamePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$ClassNamePatternConverter = PatternParser.class$org$apache$log4j$pattern$ClassNamePatternConverter;
        }
        hashMap.put("C", class$org$apache$log4j$pattern$ClassNamePatternConverter);
        Class class$org$apache$log4j$pattern$ClassNamePatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$ClassNamePatternConverter == null) {
            class$org$apache$log4j$pattern$ClassNamePatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$ClassNamePatternConverter = class$("org.apache.log4j.pattern.ClassNamePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$ClassNamePatternConverter2 = PatternParser.class$org$apache$log4j$pattern$ClassNamePatternConverter;
        }
        hashMap.put("class", class$org$apache$log4j$pattern$ClassNamePatternConverter2);
        Class class$org$apache$log4j$pattern$DatePatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$DatePatternConverter == null) {
            class$org$apache$log4j$pattern$DatePatternConverter = (PatternParser.class$org$apache$log4j$pattern$DatePatternConverter = class$("org.apache.log4j.pattern.DatePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$DatePatternConverter = PatternParser.class$org$apache$log4j$pattern$DatePatternConverter;
        }
        hashMap.put("d", class$org$apache$log4j$pattern$DatePatternConverter);
        Class class$org$apache$log4j$pattern$DatePatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$DatePatternConverter == null) {
            class$org$apache$log4j$pattern$DatePatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$DatePatternConverter = class$("org.apache.log4j.pattern.DatePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$DatePatternConverter2 = PatternParser.class$org$apache$log4j$pattern$DatePatternConverter;
        }
        hashMap.put("date", class$org$apache$log4j$pattern$DatePatternConverter2);
        Class class$org$apache$log4j$pattern$FileLocationPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$FileLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$FileLocationPatternConverter = (PatternParser.class$org$apache$log4j$pattern$FileLocationPatternConverter = class$("org.apache.log4j.pattern.FileLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$FileLocationPatternConverter = PatternParser.class$org$apache$log4j$pattern$FileLocationPatternConverter;
        }
        hashMap.put("F", class$org$apache$log4j$pattern$FileLocationPatternConverter);
        Class class$org$apache$log4j$pattern$FileLocationPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$FileLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$FileLocationPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$FileLocationPatternConverter = class$("org.apache.log4j.pattern.FileLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$FileLocationPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$FileLocationPatternConverter;
        }
        hashMap.put("file", class$org$apache$log4j$pattern$FileLocationPatternConverter2);
        Class class$org$apache$log4j$pattern$FullLocationPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$FullLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$FullLocationPatternConverter = (PatternParser.class$org$apache$log4j$pattern$FullLocationPatternConverter = class$("org.apache.log4j.pattern.FullLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$FullLocationPatternConverter = PatternParser.class$org$apache$log4j$pattern$FullLocationPatternConverter;
        }
        hashMap.put("l", class$org$apache$log4j$pattern$FullLocationPatternConverter);
        Class class$org$apache$log4j$pattern$LineLocationPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$LineLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$LineLocationPatternConverter = (PatternParser.class$org$apache$log4j$pattern$LineLocationPatternConverter = class$("org.apache.log4j.pattern.LineLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LineLocationPatternConverter = PatternParser.class$org$apache$log4j$pattern$LineLocationPatternConverter;
        }
        hashMap.put("L", class$org$apache$log4j$pattern$LineLocationPatternConverter);
        Class class$org$apache$log4j$pattern$LineLocationPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$LineLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$LineLocationPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$LineLocationPatternConverter = class$("org.apache.log4j.pattern.LineLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LineLocationPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$LineLocationPatternConverter;
        }
        hashMap.put("line", class$org$apache$log4j$pattern$LineLocationPatternConverter2);
        Class class$org$apache$log4j$pattern$MessagePatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$MessagePatternConverter == null) {
            class$org$apache$log4j$pattern$MessagePatternConverter = (PatternParser.class$org$apache$log4j$pattern$MessagePatternConverter = class$("org.apache.log4j.pattern.MessagePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$MessagePatternConverter = PatternParser.class$org$apache$log4j$pattern$MessagePatternConverter;
        }
        hashMap.put("m", class$org$apache$log4j$pattern$MessagePatternConverter);
        Class class$org$apache$log4j$pattern$MessagePatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$MessagePatternConverter == null) {
            class$org$apache$log4j$pattern$MessagePatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$MessagePatternConverter = class$("org.apache.log4j.pattern.MessagePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$MessagePatternConverter2 = PatternParser.class$org$apache$log4j$pattern$MessagePatternConverter;
        }
        hashMap.put("message", class$org$apache$log4j$pattern$MessagePatternConverter2);
        Class class$org$apache$log4j$pattern$LineSeparatorPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$LineSeparatorPatternConverter == null) {
            class$org$apache$log4j$pattern$LineSeparatorPatternConverter = (PatternParser.class$org$apache$log4j$pattern$LineSeparatorPatternConverter = class$("org.apache.log4j.pattern.LineSeparatorPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LineSeparatorPatternConverter = PatternParser.class$org$apache$log4j$pattern$LineSeparatorPatternConverter;
        }
        hashMap.put("n", class$org$apache$log4j$pattern$LineSeparatorPatternConverter);
        Class class$org$apache$log4j$pattern$MethodLocationPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$MethodLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$MethodLocationPatternConverter = (PatternParser.class$org$apache$log4j$pattern$MethodLocationPatternConverter = class$("org.apache.log4j.pattern.MethodLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$MethodLocationPatternConverter = PatternParser.class$org$apache$log4j$pattern$MethodLocationPatternConverter;
        }
        hashMap.put("M", class$org$apache$log4j$pattern$MethodLocationPatternConverter);
        Class class$org$apache$log4j$pattern$MethodLocationPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$MethodLocationPatternConverter == null) {
            class$org$apache$log4j$pattern$MethodLocationPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$MethodLocationPatternConverter = class$("org.apache.log4j.pattern.MethodLocationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$MethodLocationPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$MethodLocationPatternConverter;
        }
        hashMap.put("method", class$org$apache$log4j$pattern$MethodLocationPatternConverter2);
        Class class$org$apache$log4j$pattern$LevelPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$LevelPatternConverter == null) {
            class$org$apache$log4j$pattern$LevelPatternConverter = (PatternParser.class$org$apache$log4j$pattern$LevelPatternConverter = class$("org.apache.log4j.pattern.LevelPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LevelPatternConverter = PatternParser.class$org$apache$log4j$pattern$LevelPatternConverter;
        }
        hashMap.put("p", class$org$apache$log4j$pattern$LevelPatternConverter);
        Class class$org$apache$log4j$pattern$LevelPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$LevelPatternConverter == null) {
            class$org$apache$log4j$pattern$LevelPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$LevelPatternConverter = class$("org.apache.log4j.pattern.LevelPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$LevelPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$LevelPatternConverter;
        }
        hashMap.put("level", class$org$apache$log4j$pattern$LevelPatternConverter2);
        Class class$org$apache$log4j$pattern$RelativeTimePatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$RelativeTimePatternConverter == null) {
            class$org$apache$log4j$pattern$RelativeTimePatternConverter = (PatternParser.class$org$apache$log4j$pattern$RelativeTimePatternConverter = class$("org.apache.log4j.pattern.RelativeTimePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$RelativeTimePatternConverter = PatternParser.class$org$apache$log4j$pattern$RelativeTimePatternConverter;
        }
        hashMap.put("r", class$org$apache$log4j$pattern$RelativeTimePatternConverter);
        Class class$org$apache$log4j$pattern$RelativeTimePatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$RelativeTimePatternConverter == null) {
            class$org$apache$log4j$pattern$RelativeTimePatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$RelativeTimePatternConverter = class$("org.apache.log4j.pattern.RelativeTimePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$RelativeTimePatternConverter2 = PatternParser.class$org$apache$log4j$pattern$RelativeTimePatternConverter;
        }
        hashMap.put("relative", class$org$apache$log4j$pattern$RelativeTimePatternConverter2);
        Class class$org$apache$log4j$pattern$ThreadPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$ThreadPatternConverter == null) {
            class$org$apache$log4j$pattern$ThreadPatternConverter = (PatternParser.class$org$apache$log4j$pattern$ThreadPatternConverter = class$("org.apache.log4j.pattern.ThreadPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$ThreadPatternConverter = PatternParser.class$org$apache$log4j$pattern$ThreadPatternConverter;
        }
        hashMap.put("t", class$org$apache$log4j$pattern$ThreadPatternConverter);
        Class class$org$apache$log4j$pattern$ThreadPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$ThreadPatternConverter == null) {
            class$org$apache$log4j$pattern$ThreadPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$ThreadPatternConverter = class$("org.apache.log4j.pattern.ThreadPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$ThreadPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$ThreadPatternConverter;
        }
        hashMap.put("thread", class$org$apache$log4j$pattern$ThreadPatternConverter2);
        Class class$org$apache$log4j$pattern$NDCPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$NDCPatternConverter == null) {
            class$org$apache$log4j$pattern$NDCPatternConverter = (PatternParser.class$org$apache$log4j$pattern$NDCPatternConverter = class$("org.apache.log4j.pattern.NDCPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$NDCPatternConverter = PatternParser.class$org$apache$log4j$pattern$NDCPatternConverter;
        }
        hashMap.put("x", class$org$apache$log4j$pattern$NDCPatternConverter);
        Class class$org$apache$log4j$pattern$NDCPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$NDCPatternConverter == null) {
            class$org$apache$log4j$pattern$NDCPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$NDCPatternConverter = class$("org.apache.log4j.pattern.NDCPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$NDCPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$NDCPatternConverter;
        }
        hashMap.put("ndc", class$org$apache$log4j$pattern$NDCPatternConverter2);
        Class class$org$apache$log4j$pattern$PropertiesPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$PropertiesPatternConverter == null) {
            class$org$apache$log4j$pattern$PropertiesPatternConverter = (PatternParser.class$org$apache$log4j$pattern$PropertiesPatternConverter = class$("org.apache.log4j.pattern.PropertiesPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$PropertiesPatternConverter = PatternParser.class$org$apache$log4j$pattern$PropertiesPatternConverter;
        }
        hashMap.put("X", class$org$apache$log4j$pattern$PropertiesPatternConverter);
        Class class$org$apache$log4j$pattern$PropertiesPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$PropertiesPatternConverter == null) {
            class$org$apache$log4j$pattern$PropertiesPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$PropertiesPatternConverter = class$("org.apache.log4j.pattern.PropertiesPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$PropertiesPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$PropertiesPatternConverter;
        }
        hashMap.put("properties", class$org$apache$log4j$pattern$PropertiesPatternConverter2);
        Class class$org$apache$log4j$pattern$SequenceNumberPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$SequenceNumberPatternConverter == null) {
            class$org$apache$log4j$pattern$SequenceNumberPatternConverter = (PatternParser.class$org$apache$log4j$pattern$SequenceNumberPatternConverter = class$("org.apache.log4j.pattern.SequenceNumberPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$SequenceNumberPatternConverter = PatternParser.class$org$apache$log4j$pattern$SequenceNumberPatternConverter;
        }
        hashMap.put("sn", class$org$apache$log4j$pattern$SequenceNumberPatternConverter);
        Class class$org$apache$log4j$pattern$SequenceNumberPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$SequenceNumberPatternConverter == null) {
            class$org$apache$log4j$pattern$SequenceNumberPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$SequenceNumberPatternConverter = class$("org.apache.log4j.pattern.SequenceNumberPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$SequenceNumberPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$SequenceNumberPatternConverter;
        }
        hashMap.put("sequenceNumber", class$org$apache$log4j$pattern$SequenceNumberPatternConverter2);
        Class class$org$apache$log4j$pattern$ThrowableInformationPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$ThrowableInformationPatternConverter == null) {
            class$org$apache$log4j$pattern$ThrowableInformationPatternConverter = (PatternParser.class$org$apache$log4j$pattern$ThrowableInformationPatternConverter = class$("org.apache.log4j.pattern.ThrowableInformationPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$ThrowableInformationPatternConverter = PatternParser.class$org$apache$log4j$pattern$ThrowableInformationPatternConverter;
        }
        hashMap.put("throwable", class$org$apache$log4j$pattern$ThrowableInformationPatternConverter);
        PATTERN_LAYOUT_RULES = new ReadOnlyMap(hashMap);
        final HashMap<String, Class> hashMap2 = new HashMap<String, Class>(4);
        Class class$org$apache$log4j$pattern$FileDatePatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$FileDatePatternConverter == null) {
            class$org$apache$log4j$pattern$FileDatePatternConverter = (PatternParser.class$org$apache$log4j$pattern$FileDatePatternConverter = class$("org.apache.log4j.pattern.FileDatePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$FileDatePatternConverter = PatternParser.class$org$apache$log4j$pattern$FileDatePatternConverter;
        }
        hashMap2.put("d", class$org$apache$log4j$pattern$FileDatePatternConverter);
        Class class$org$apache$log4j$pattern$FileDatePatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$FileDatePatternConverter == null) {
            class$org$apache$log4j$pattern$FileDatePatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$FileDatePatternConverter = class$("org.apache.log4j.pattern.FileDatePatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$FileDatePatternConverter2 = PatternParser.class$org$apache$log4j$pattern$FileDatePatternConverter;
        }
        hashMap2.put("date", class$org$apache$log4j$pattern$FileDatePatternConverter2);
        Class class$org$apache$log4j$pattern$IntegerPatternConverter;
        if (PatternParser.class$org$apache$log4j$pattern$IntegerPatternConverter == null) {
            class$org$apache$log4j$pattern$IntegerPatternConverter = (PatternParser.class$org$apache$log4j$pattern$IntegerPatternConverter = class$("org.apache.log4j.pattern.IntegerPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$IntegerPatternConverter = PatternParser.class$org$apache$log4j$pattern$IntegerPatternConverter;
        }
        hashMap2.put("i", class$org$apache$log4j$pattern$IntegerPatternConverter);
        Class class$org$apache$log4j$pattern$IntegerPatternConverter2;
        if (PatternParser.class$org$apache$log4j$pattern$IntegerPatternConverter == null) {
            class$org$apache$log4j$pattern$IntegerPatternConverter2 = (PatternParser.class$org$apache$log4j$pattern$IntegerPatternConverter = class$("org.apache.log4j.pattern.IntegerPatternConverter"));
        }
        else {
            class$org$apache$log4j$pattern$IntegerPatternConverter2 = PatternParser.class$org$apache$log4j$pattern$IntegerPatternConverter;
        }
        hashMap2.put("index", class$org$apache$log4j$pattern$IntegerPatternConverter2);
        FILENAME_PATTERN_RULES = new ReadOnlyMap(hashMap2);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private static PatternConverter createConverter(final String ex, final StringBuffer ex2, Map loadClass, final Map map, final List list) {
        Serializable s = ex;
        int length = ((String)ex).length();
        Serializable value = null;
        String substring = null;
        Object invoke;
        Serializable value2;
        Label_0282_Outer:Block_10_Outer:Label_0046_Outer:
        while (true) {
            if (length > 0 && value == null) {
                substring = ((String)s).substring(0, length);
                if (loadClass != null) {
                    value = ((Map<K, String>)loadClass).get(substring);
                }
            }
            else {
                if (value == null) {
                    break;
                }
                Label_0228: {
                    if (!(value instanceof Class)) {
                        break Label_0228;
                    }
                    loadClass = value;
                    try {
                        // iftrue(Label_0282:, !value instanceof String)
                        while (true) {
                            while (true) {
                                while (true) {
                                    invoke = ((Class)loadClass).getMethod("newInstance", Class.forName("[Ljava.lang.String;")).invoke(null, list.toArray(new String[list.size()]));
                                    if (invoke instanceof PatternConverter) {
                                        ((StringBuffer)ex2).delete(0, ((StringBuffer)ex2).length() - (((String)ex).length() - ((String)s).length()));
                                        return (PatternConverter)invoke;
                                    }
                                    LogLog.warn(new StringBuffer().append("Class ").append(((Class)loadClass).getName()).append(" does not extend PatternConverter.").toString());
                                    return null;
                                    LogLog.warn(new StringBuffer().append("Bad map entry for conversion pattern %").append((String)s).append(".").toString());
                                    return null;
                                    try {
                                        loadClass = Loader.loadClass((String)value);
                                        continue Label_0282_Outer;
                                    }
                                    catch (ClassNotFoundException ex) {
                                        LogLog.warn(new StringBuffer().append("Class for conversion pattern %").append((String)s).append(" not found").toString(), ex);
                                        return null;
                                    }
                                    break;
                                }
                                continue Block_10_Outer;
                            }
                            continue Label_0046_Outer;
                        }
                    }
                    catch (Exception ex3) {
                        LogLog.error(new StringBuffer().append("Error creating converter for ").append((String)ex).toString(), ex3);
                        try {
                            loadClass = ((Class<PatternConverter>)loadClass).newInstance();
                            ((StringBuffer)ex2).delete(0, ((StringBuffer)ex2).length() - (((String)ex).length() - ((String)s).length()));
                            return (PatternConverter)loadClass;
                        }
                        catch (Exception ex2) {
                            LogLog.error(new StringBuffer().append("Error creating converter for ").append((String)ex).toString(), ex2);
                        }
                    }
                }
            }
            while (true) {
                value2 = value;
                if (value == null) {
                    value2 = value;
                    if (map != null) {
                        value2 = map.get(substring);
                    }
                }
                --length;
                value = value2;
                s = substring;
                continue Label_0046_Outer;
                continue;
            }
        }
        LogLog.error(new StringBuffer().append("Unrecognized format specifier [").append((String)ex).append("]").toString());
        return null;
    }
    
    private static int extractConverter(final char c, final String s, int n, final StringBuffer sb, final StringBuffer sb2) {
        sb.setLength(0);
        if (!Character.isUnicodeIdentifierStart(c)) {
            return n;
        }
        sb.append(c);
        while (n < s.length() && Character.isUnicodeIdentifierPart(s.charAt(n))) {
            sb.append(s.charAt(n));
            sb2.append(s.charAt(n));
            ++n;
        }
        return n;
    }
    
    private static int extractOptions(final String s, int n, final List list) {
        while (n < s.length() && s.charAt(n) == '{') {
            final int index = s.indexOf(125, n);
            if (index == -1) {
                break;
            }
            list.add(s.substring(n + 1, index));
            n = index + 1;
        }
        return n;
    }
    
    private static int finalizeConverter(final char c, final String s, int n, final StringBuffer sb, final FormattingInfo formattingInfo, final Map map, final Map map2, final List list, final List list2) {
        final StringBuffer sb2 = new StringBuffer();
        n = extractConverter(c, s, n, sb2, sb);
        final String string = sb2.toString();
        final ArrayList list3 = new ArrayList();
        n = extractOptions(s, n, list3);
        final PatternConverter converter = createConverter(string, sb, map, map2, list3);
        if (converter == null) {
            StringBuffer sb3;
            if (string == null || string.length() == 0) {
                sb3 = new StringBuffer("Empty conversion specifier starting at position ");
            }
            else {
                sb3 = new StringBuffer("Unrecognized conversion specifier [");
                sb3.append(string);
                sb3.append("] starting at position ");
            }
            sb3.append(Integer.toString(n));
            sb3.append(" in conversion pattern.");
            LogLog.error(sb3.toString());
            list.add(new LiteralPatternConverter(sb.toString()));
            list2.add(FormattingInfo.getDefault());
        }
        else {
            list.add(converter);
            list2.add(formattingInfo);
            if (sb.length() > 0) {
                list.add(new LiteralPatternConverter(sb.toString()));
                list2.add(FormattingInfo.getDefault());
            }
        }
        sb.setLength(0);
        return n;
    }
    
    public static Map getFileNamePatternRules() {
        return PatternParser.FILENAME_PATTERN_RULES;
    }
    
    public static Map getPatternLayoutRules() {
        return PatternParser.PATTERN_LAYOUT_RULES;
    }
    
    public static void parse(final String s, final List list, final List list2, final Map map, final Map map2) {
        if (s == null) {
            throw new NullPointerException("pattern");
        }
        final StringBuffer sb = new StringBuffer(32);
        final int length = s.length();
        int n = 0;
        FormattingInfo default1 = FormattingInfo.getDefault();
        int i = 0;
    Label_0128_Outer:
        while (i < length) {
            final int n2 = i + 1;
            final char char1 = s.charAt(i);
            i = n2;
            FormattingInfo formattingInfo = default1;
            int n3 = n;
            while (true) {
                switch (n) {
                    default: {
                        n3 = n;
                        formattingInfo = default1;
                        i = n2;
                    }
                    case 2: {
                        default1 = formattingInfo;
                        n = n3;
                        continue Label_0128_Outer;
                    }
                    case 0: {
                        if (n2 == length) {
                            sb.append(char1);
                            i = n2;
                            continue Label_0128_Outer;
                        }
                        if (char1 != '%') {
                            sb.append(char1);
                            i = n2;
                            formattingInfo = default1;
                            n3 = n;
                            continue;
                        }
                        switch (s.charAt(n2)) {
                            default: {
                                if (sb.length() != 0) {
                                    list.add(new LiteralPatternConverter(sb.toString()));
                                    list2.add(FormattingInfo.getDefault());
                                }
                                sb.setLength(0);
                                sb.append(char1);
                                n3 = 1;
                                formattingInfo = FormattingInfo.getDefault();
                                i = n2;
                                continue;
                            }
                            case '%': {
                                sb.append(char1);
                                i = n2 + 1;
                                formattingInfo = default1;
                                n3 = n;
                                continue;
                            }
                        }
                        break;
                    }
                    case 1: {
                        sb.append(char1);
                        switch (char1) {
                            default: {
                                if (char1 >= '0' && char1 <= '9') {
                                    formattingInfo = new FormattingInfo(default1.isLeftAligned(), char1 - '0', default1.getMaxLength());
                                    n3 = 4;
                                    i = n2;
                                    continue;
                                }
                                i = finalizeConverter(char1, s, n2, sb, default1, map, map2, list, list2);
                                n3 = 0;
                                formattingInfo = FormattingInfo.getDefault();
                                sb.setLength(0);
                                continue;
                            }
                            case '-': {
                                formattingInfo = new FormattingInfo(true, default1.getMinLength(), default1.getMaxLength());
                                i = n2;
                                n3 = n;
                                continue;
                            }
                            case '.': {
                                n3 = 3;
                                i = n2;
                                formattingInfo = default1;
                                continue;
                            }
                        }
                        break;
                    }
                    case 4: {
                        sb.append(char1);
                        if (char1 >= '0' && char1 <= '9') {
                            formattingInfo = new FormattingInfo(default1.isLeftAligned(), default1.getMinLength() * 10 + (char1 - '0'), default1.getMaxLength());
                            i = n2;
                            n3 = n;
                            continue;
                        }
                        if (char1 == '.') {
                            n3 = 3;
                            i = n2;
                            formattingInfo = default1;
                            continue;
                        }
                        i = finalizeConverter(char1, s, n2, sb, default1, map, map2, list, list2);
                        n3 = 0;
                        formattingInfo = FormattingInfo.getDefault();
                        sb.setLength(0);
                        continue;
                    }
                    case 3: {
                        sb.append(char1);
                        if (char1 >= '0' && char1 <= '9') {
                            formattingInfo = new FormattingInfo(default1.isLeftAligned(), default1.getMinLength(), char1 - '0');
                            n3 = 5;
                            i = n2;
                            continue;
                        }
                        LogLog.error(new StringBuffer().append("Error occured in position ").append(n2).append(".\n Was expecting digit, instead got char \"").append(char1).append("\".").toString());
                        n3 = 0;
                        i = n2;
                        formattingInfo = default1;
                        continue;
                    }
                    case 5: {
                        sb.append(char1);
                        if (char1 >= '0' && char1 <= '9') {
                            formattingInfo = new FormattingInfo(default1.isLeftAligned(), default1.getMinLength(), default1.getMaxLength() * 10 + (char1 - '0'));
                            i = n2;
                            n3 = n;
                            continue;
                        }
                        i = finalizeConverter(char1, s, n2, sb, default1, map, map2, list, list2);
                        n3 = 0;
                        formattingInfo = FormattingInfo.getDefault();
                        sb.setLength(0);
                        continue;
                    }
                }
                break;
            }
        }
        if (sb.length() != 0) {
            list.add(new LiteralPatternConverter(sb.toString()));
            list2.add(FormattingInfo.getDefault());
        }
    }
    
    private static class ReadOnlyMap implements Map
    {
        private final Map map;
        
        public ReadOnlyMap(final Map map) {
            this.map = map;
        }
        
        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.map.containsKey(o);
        }
        
        @Override
        public boolean containsValue(final Object o) {
            return this.map.containsValue(o);
        }
        
        @Override
        public Set entrySet() {
            return this.map.entrySet();
        }
        
        @Override
        public Object get(final Object o) {
            return this.map.get(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.map.isEmpty();
        }
        
        @Override
        public Set keySet() {
            return this.map.keySet();
        }
        
        @Override
        public Object put(final Object o, final Object o2) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void putAll(final Map map) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Object remove(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int size() {
            return this.map.size();
        }
        
        @Override
        public Collection values() {
            return this.map.values();
        }
    }
}
