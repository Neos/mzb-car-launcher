package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class MessagePatternConverter extends LoggingEventPatternConverter
{
    private static final MessagePatternConverter INSTANCE;
    
    static {
        INSTANCE = new MessagePatternConverter();
    }
    
    private MessagePatternConverter() {
        super("Message", "message");
    }
    
    public static MessagePatternConverter newInstance(final String[] array) {
        return MessagePatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append(loggingEvent.getRenderedMessage());
    }
}
