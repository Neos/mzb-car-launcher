package org.apache.log4j.pattern;

import java.util.*;

public final class IntegerPatternConverter extends PatternConverter
{
    private static final IntegerPatternConverter INSTANCE;
    
    static {
        INSTANCE = new IntegerPatternConverter();
    }
    
    private IntegerPatternConverter() {
        super("Integer", "integer");
    }
    
    public static IntegerPatternConverter newInstance(final String[] array) {
        return IntegerPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final Object o, final StringBuffer sb) {
        if (o instanceof Integer) {
            sb.append(o.toString());
        }
        if (o instanceof Date) {
            sb.append(Long.toString(((Date)o).getTime()));
        }
    }
}
