package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class ClassNamePatternConverter extends NamePatternConverter
{
    private ClassNamePatternConverter(final String[] array) {
        super("Class Name", "class name", array);
    }
    
    public static ClassNamePatternConverter newInstance(final String[] array) {
        return new ClassNamePatternConverter(array);
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        final int length = sb.length();
        final LocationInfo locationInformation = loggingEvent.getLocationInformation();
        if (locationInformation == null) {
            sb.append("?");
        }
        else {
            sb.append(locationInformation.getClassName());
        }
        this.abbreviate(length, sb);
    }
}
