package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public class ThrowableInformationPatternConverter extends LoggingEventPatternConverter
{
    private int maxLines;
    
    private ThrowableInformationPatternConverter(final String[] array) {
        super("Throwable", "throwable");
        this.maxLines = Integer.MAX_VALUE;
        if (array != null && array.length > 0) {
            if ("none".equals(array[0])) {
                this.maxLines = 0;
            }
            else {
                if ("short".equals(array[0])) {
                    this.maxLines = 1;
                    return;
                }
                try {
                    this.maxLines = Integer.parseInt(array[0]);
                }
                catch (NumberFormatException ex) {}
            }
        }
    }
    
    public static ThrowableInformationPatternConverter newInstance(final String[] array) {
        return new ThrowableInformationPatternConverter(array);
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        if (this.maxLines != 0) {
            final ThrowableInformation throwableInformation = loggingEvent.getThrowableInformation();
            if (throwableInformation != null) {
                final String[] throwableStrRep = throwableInformation.getThrowableStrRep();
                final int length = throwableStrRep.length;
                int maxLines;
                if (this.maxLines < 0) {
                    maxLines = length + this.maxLines;
                }
                else if ((maxLines = length) > this.maxLines) {
                    maxLines = this.maxLines;
                }
                for (int i = 0; i < maxLines; ++i) {
                    sb.append(throwableStrRep[i]).append("\n");
                }
            }
        }
    }
    
    @Override
    public boolean handlesThrowable() {
        return true;
    }
}
