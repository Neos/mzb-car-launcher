package org.apache.log4j.pattern;

import org.apache.log4j.spi.*;

public final class NDCPatternConverter extends LoggingEventPatternConverter
{
    private static final NDCPatternConverter INSTANCE;
    
    static {
        INSTANCE = new NDCPatternConverter();
    }
    
    private NDCPatternConverter() {
        super("NDC", "ndc");
    }
    
    public static NDCPatternConverter newInstance(final String[] array) {
        return NDCPatternConverter.INSTANCE;
    }
    
    @Override
    public void format(final LoggingEvent loggingEvent, final StringBuffer sb) {
        sb.append(loggingEvent.getNDC());
    }
}
