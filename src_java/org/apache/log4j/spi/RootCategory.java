package org.apache.log4j.spi;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;

public final class RootCategory extends Logger
{
    public RootCategory(final Level level) {
        super("root");
        this.setLevel(level);
    }
    
    public final Level getChainedLevel() {
        return this.level;
    }
    
    @Override
    public final void setLevel(final Level level) {
        if (level == null) {
            LogLog.error("You have tried to set a null level to root.", new Throwable());
            return;
        }
        this.level = level;
    }
    
    public final void setPriority(final Level level) {
        this.setLevel(level);
    }
}
