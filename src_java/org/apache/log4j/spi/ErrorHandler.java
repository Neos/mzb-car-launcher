package org.apache.log4j.spi;

import org.apache.log4j.*;

public interface ErrorHandler extends OptionHandler
{
    void error(final String p0);
    
    void error(final String p0, final Exception p1, final int p2);
    
    void error(final String p0, final Exception p1, final int p2, final LoggingEvent p3);
    
    void setAppender(final Appender p0);
    
    void setBackupAppender(final Appender p0);
    
    void setLogger(final Logger p0);
}
