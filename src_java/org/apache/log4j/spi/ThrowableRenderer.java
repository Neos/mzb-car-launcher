package org.apache.log4j.spi;

public interface ThrowableRenderer
{
    String[] doRender(final Throwable p0);
}
