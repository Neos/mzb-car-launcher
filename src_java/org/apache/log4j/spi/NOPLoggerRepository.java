package org.apache.log4j.spi;

import java.util.*;
import org.apache.log4j.*;

public final class NOPLoggerRepository implements LoggerRepository
{
    @Override
    public void addHierarchyEventListener(final HierarchyEventListener hierarchyEventListener) {
    }
    
    @Override
    public void emitNoAppenderWarning(final Category category) {
    }
    
    @Override
    public Logger exists(final String s) {
        return null;
    }
    
    @Override
    public void fireAddAppenderEvent(final Category category, final Appender appender) {
    }
    
    @Override
    public Enumeration getCurrentCategories() {
        return this.getCurrentLoggers();
    }
    
    @Override
    public Enumeration getCurrentLoggers() {
        return new Vector().elements();
    }
    
    @Override
    public Logger getLogger(final String s) {
        return new NOPLogger(this, s);
    }
    
    @Override
    public Logger getLogger(final String s, final LoggerFactory loggerFactory) {
        return new NOPLogger(this, s);
    }
    
    @Override
    public Logger getRootLogger() {
        return new NOPLogger(this, "root");
    }
    
    @Override
    public Level getThreshold() {
        return Level.OFF;
    }
    
    @Override
    public boolean isDisabled(final int n) {
        return true;
    }
    
    @Override
    public void resetConfiguration() {
    }
    
    @Override
    public void setThreshold(final String s) {
    }
    
    @Override
    public void setThreshold(final Level level) {
    }
    
    @Override
    public void shutdown() {
    }
}
