package org.apache.log4j.spi;

public interface TriggeringEventEvaluator
{
    boolean isTriggeringEvent(final LoggingEvent p0);
}
