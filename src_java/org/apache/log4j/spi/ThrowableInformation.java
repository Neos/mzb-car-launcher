package org.apache.log4j.spi;

import java.io.*;
import org.apache.log4j.*;

public class ThrowableInformation implements Serializable
{
    static final long serialVersionUID = -4748765566864322735L;
    private transient Category category;
    private String[] rep;
    private transient Throwable throwable;
    
    public ThrowableInformation(final Throwable throwable) {
        this.throwable = throwable;
    }
    
    public ThrowableInformation(final Throwable throwable, final Category category) {
        this.throwable = throwable;
        this.category = category;
    }
    
    public ThrowableInformation(final String[] array) {
        if (array != null) {
            this.rep = array.clone();
        }
    }
    
    public Throwable getThrowable() {
        return this.throwable;
    }
    
    public String[] getThrowableStrRep() {
        synchronized (this) {
            if (this.rep == null) {
                ThrowableRenderer throwableRenderer2;
                final ThrowableRenderer throwableRenderer = throwableRenderer2 = null;
                if (this.category != null) {
                    final LoggerRepository loggerRepository = this.category.getLoggerRepository();
                    throwableRenderer2 = throwableRenderer;
                    if (loggerRepository instanceof ThrowableRendererSupport) {
                        throwableRenderer2 = ((ThrowableRendererSupport)loggerRepository).getThrowableRenderer();
                    }
                }
                if (throwableRenderer2 == null) {
                    this.rep = DefaultThrowableRenderer.render(this.throwable);
                }
                else {
                    this.rep = throwableRenderer2.doRender(this.throwable);
                }
            }
            return this.rep.clone();
        }
    }
}
