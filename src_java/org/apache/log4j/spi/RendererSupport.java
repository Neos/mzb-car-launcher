package org.apache.log4j.spi;

import org.apache.log4j.or.*;

public interface RendererSupport
{
    RendererMap getRendererMap();
    
    void setRenderer(final Class p0, final ObjectRenderer p1);
}
