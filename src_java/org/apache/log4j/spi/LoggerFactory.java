package org.apache.log4j.spi;

import org.apache.log4j.*;

public interface LoggerFactory
{
    Logger makeNewLoggerInstance(final String p0);
}
