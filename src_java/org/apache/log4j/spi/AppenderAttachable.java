package org.apache.log4j.spi;

import org.apache.log4j.*;
import java.util.*;

public interface AppenderAttachable
{
    void addAppender(final Appender p0);
    
    Enumeration getAllAppenders();
    
    Appender getAppender(final String p0);
    
    boolean isAttached(final Appender p0);
    
    void removeAllAppenders();
    
    void removeAppender(final String p0);
    
    void removeAppender(final Appender p0);
}
