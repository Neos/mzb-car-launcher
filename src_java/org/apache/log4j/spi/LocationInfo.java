package org.apache.log4j.spi;

import java.lang.reflect.*;
import java.io.*;
import org.apache.log4j.helpers.*;

public class LocationInfo implements Serializable
{
    public static final String NA = "?";
    public static final LocationInfo NA_LOCATION_INFO;
    static Class class$java$lang$Throwable;
    private static Method getClassNameMethod;
    private static Method getFileNameMethod;
    private static Method getLineNumberMethod;
    private static Method getMethodNameMethod;
    private static Method getStackTraceMethod;
    static boolean inVisualAge = false;
    private static PrintWriter pw;
    static final long serialVersionUID = -1325822038990805636L;
    private static StringWriter sw;
    transient String className;
    transient String fileName;
    public String fullInfo;
    transient String lineNumber;
    transient String methodName;
    
    static {
        boolean inVisualAge = false;
        LocationInfo.sw = new StringWriter();
        LocationInfo.pw = new PrintWriter(LocationInfo.sw);
        NA_LOCATION_INFO = new LocationInfo("?", "?", "?", "?");
        LocationInfo.inVisualAge = false;
        while (true) {
            try {
                if (Class.forName("com.ibm.uvm.tools.DebugSupport") != null) {
                    inVisualAge = true;
                }
                LocationInfo.inVisualAge = inVisualAge;
                LogLog.debug("Detected IBM VisualAge environment.");
                try {
                    Class class$java$lang$Throwable;
                    if (LocationInfo.class$java$lang$Throwable == null) {
                        class$java$lang$Throwable = (LocationInfo.class$java$lang$Throwable = class$("java.lang.Throwable"));
                    }
                    else {
                        class$java$lang$Throwable = LocationInfo.class$java$lang$Throwable;
                    }
                    LocationInfo.getStackTraceMethod = class$java$lang$Throwable.getMethod("getStackTrace", (Class[])null);
                    final Class<?> forName = Class.forName("java.lang.StackTraceElement");
                    LocationInfo.getClassNameMethod = forName.getMethod("getClassName", (Class<?>[])null);
                    LocationInfo.getMethodNameMethod = forName.getMethod("getMethodName", (Class<?>[])null);
                    LocationInfo.getFileNameMethod = forName.getMethod("getFileName", (Class<?>[])null);
                    LocationInfo.getLineNumberMethod = forName.getMethod("getLineNumber", (Class<?>[])null);
                }
                catch (ClassNotFoundException ex) {
                    LogLog.debug("LocationInfo will use pre-JDK 1.4 methods to determine location.");
                }
                catch (NoSuchMethodException ex2) {
                    LogLog.debug("LocationInfo will use pre-JDK 1.4 methods to determine location.");
                }
            }
            catch (Throwable t) {
                continue;
            }
            break;
        }
    }
    
    public LocationInfo(final String fileName, final String className, final String methodName, final String lineNumber) {
        this.fileName = fileName;
        this.className = className;
        this.methodName = methodName;
        this.lineNumber = lineNumber;
        final StringBuffer sb = new StringBuffer();
        appendFragment(sb, className);
        sb.append(".");
        appendFragment(sb, methodName);
        sb.append("(");
        appendFragment(sb, fileName);
        sb.append(":");
        appendFragment(sb, lineNumber);
        sb.append(")");
        this.fullInfo = sb.toString();
    }
    
    public LocationInfo(final Throwable p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/lang/Object.<init>:()V
        //     4: aload_1        
        //     5: ifnull          12
        //     8: aload_2        
        //     9: ifnonnull       13
        //    12: return         
        //    13: getstatic       org/apache/log4j/spi/LocationInfo.getLineNumberMethod:Ljava/lang/reflect/Method;
        //    16: ifnull          268
        //    19: getstatic       org/apache/log4j/spi/LocationInfo.getStackTraceMethod:Ljava/lang/reflect/Method;
        //    22: aload_1        
        //    23: aconst_null    
        //    24: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    27: checkcast       [Ljava/lang/Object;
        //    30: checkcast       [Ljava/lang/Object;
        //    33: astore          8
        //    35: ldc             "?"
        //    37: astore          6
        //    39: aload           8
        //    41: arraylength    
        //    42: iconst_1       
        //    43: isub           
        //    44: istore_3       
        //    45: iload_3        
        //    46: iflt            12
        //    49: getstatic       org/apache/log4j/spi/LocationInfo.getClassNameMethod:Ljava/lang/reflect/Method;
        //    52: aload           8
        //    54: iload_3        
        //    55: aaload         
        //    56: aconst_null    
        //    57: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    60: checkcast       Ljava/lang/String;
        //    63: astore          7
        //    65: aload_2        
        //    66: aload           7
        //    68: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    71: ifeq            512
        //    74: iload_3        
        //    75: iconst_1       
        //    76: iadd           
        //    77: istore_3       
        //    78: iload_3        
        //    79: aload           8
        //    81: arraylength    
        //    82: if_icmpge       12
        //    85: aload_0        
        //    86: aload           6
        //    88: putfield        org/apache/log4j/spi/LocationInfo.className:Ljava/lang/String;
        //    91: aload_0        
        //    92: getstatic       org/apache/log4j/spi/LocationInfo.getMethodNameMethod:Ljava/lang/reflect/Method;
        //    95: aload           8
        //    97: iload_3        
        //    98: aaload         
        //    99: aconst_null    
        //   100: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   103: checkcast       Ljava/lang/String;
        //   106: putfield        org/apache/log4j/spi/LocationInfo.methodName:Ljava/lang/String;
        //   109: aload_0        
        //   110: getstatic       org/apache/log4j/spi/LocationInfo.getFileNameMethod:Ljava/lang/reflect/Method;
        //   113: aload           8
        //   115: iload_3        
        //   116: aaload         
        //   117: aconst_null    
        //   118: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   121: checkcast       Ljava/lang/String;
        //   124: putfield        org/apache/log4j/spi/LocationInfo.fileName:Ljava/lang/String;
        //   127: aload_0        
        //   128: getfield        org/apache/log4j/spi/LocationInfo.fileName:Ljava/lang/String;
        //   131: ifnonnull       140
        //   134: aload_0        
        //   135: ldc             "?"
        //   137: putfield        org/apache/log4j/spi/LocationInfo.fileName:Ljava/lang/String;
        //   140: getstatic       org/apache/log4j/spi/LocationInfo.getLineNumberMethod:Ljava/lang/reflect/Method;
        //   143: aload           8
        //   145: iload_3        
        //   146: aaload         
        //   147: aconst_null    
        //   148: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   151: checkcast       Ljava/lang/Integer;
        //   154: invokevirtual   java/lang/Integer.intValue:()I
        //   157: istore_3       
        //   158: iload_3        
        //   159: ifge            461
        //   162: aload_0        
        //   163: ldc             "?"
        //   165: putfield        org/apache/log4j/spi/LocationInfo.lineNumber:Ljava/lang/String;
        //   168: new             Ljava/lang/StringBuffer;
        //   171: dup            
        //   172: invokespecial   java/lang/StringBuffer.<init>:()V
        //   175: astore          6
        //   177: aload           6
        //   179: aload_0        
        //   180: getfield        org/apache/log4j/spi/LocationInfo.className:Ljava/lang/String;
        //   183: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   186: pop            
        //   187: aload           6
        //   189: ldc             "."
        //   191: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   194: pop            
        //   195: aload           6
        //   197: aload_0        
        //   198: getfield        org/apache/log4j/spi/LocationInfo.methodName:Ljava/lang/String;
        //   201: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   204: pop            
        //   205: aload           6
        //   207: ldc             "("
        //   209: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   212: pop            
        //   213: aload           6
        //   215: aload_0        
        //   216: getfield        org/apache/log4j/spi/LocationInfo.fileName:Ljava/lang/String;
        //   219: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   222: pop            
        //   223: aload           6
        //   225: ldc             ":"
        //   227: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   230: pop            
        //   231: aload           6
        //   233: aload_0        
        //   234: getfield        org/apache/log4j/spi/LocationInfo.lineNumber:Ljava/lang/String;
        //   237: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   240: pop            
        //   241: aload           6
        //   243: ldc             ")"
        //   245: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   248: pop            
        //   249: aload_0        
        //   250: aload           6
        //   252: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   255: putfield        org/apache/log4j/spi/LocationInfo.fullInfo:Ljava/lang/String;
        //   258: return         
        //   259: astore          6
        //   261: ldc             "LocationInfo failed using JDK 1.4 methods"
        //   263: aload           6
        //   265: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   268: getstatic       org/apache/log4j/spi/LocationInfo.sw:Ljava/io/StringWriter;
        //   271: astore          6
        //   273: aload           6
        //   275: monitorenter   
        //   276: aload_1        
        //   277: getstatic       org/apache/log4j/spi/LocationInfo.pw:Ljava/io/PrintWriter;
        //   280: invokevirtual   java/lang/Throwable.printStackTrace:(Ljava/io/PrintWriter;)V
        //   283: getstatic       org/apache/log4j/spi/LocationInfo.sw:Ljava/io/StringWriter;
        //   286: invokevirtual   java/io/StringWriter.toString:()Ljava/lang/String;
        //   289: astore_1       
        //   290: getstatic       org/apache/log4j/spi/LocationInfo.sw:Ljava/io/StringWriter;
        //   293: invokevirtual   java/io/StringWriter.getBuffer:()Ljava/lang/StringBuffer;
        //   296: iconst_0       
        //   297: invokevirtual   java/lang/StringBuffer.setLength:(I)V
        //   300: aload           6
        //   302: monitorexit    
        //   303: aload_1        
        //   304: aload_2        
        //   305: invokevirtual   java/lang/String.lastIndexOf:(Ljava/lang/String;)I
        //   308: istore          4
        //   310: iload           4
        //   312: iconst_m1      
        //   313: if_icmpeq       12
        //   316: iload           4
        //   318: istore_3       
        //   319: aload_2        
        //   320: invokevirtual   java/lang/String.length:()I
        //   323: iload           4
        //   325: iadd           
        //   326: aload_1        
        //   327: invokevirtual   java/lang/String.length:()I
        //   330: if_icmpge       389
        //   333: iload           4
        //   335: istore_3       
        //   336: aload_1        
        //   337: aload_2        
        //   338: invokevirtual   java/lang/String.length:()I
        //   341: iload           4
        //   343: iadd           
        //   344: invokevirtual   java/lang/String.charAt:(I)C
        //   347: bipush          46
        //   349: if_icmpeq       389
        //   352: aload_1        
        //   353: new             Ljava/lang/StringBuffer;
        //   356: dup            
        //   357: invokespecial   java/lang/StringBuffer.<init>:()V
        //   360: aload_2        
        //   361: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   364: ldc             "."
        //   366: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   369: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   372: invokevirtual   java/lang/String.lastIndexOf:(Ljava/lang/String;)I
        //   375: istore          5
        //   377: iload           4
        //   379: istore_3       
        //   380: iload           5
        //   382: iconst_m1      
        //   383: if_icmpeq       389
        //   386: iload           5
        //   388: istore_3       
        //   389: aload_1        
        //   390: getstatic       org/apache/log4j/Layout.LINE_SEP:Ljava/lang/String;
        //   393: iload_3        
        //   394: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;I)I
        //   397: istore_3       
        //   398: iload_3        
        //   399: iconst_m1      
        //   400: if_icmpeq       12
        //   403: iload_3        
        //   404: getstatic       org/apache/log4j/Layout.LINE_SEP_LEN:I
        //   407: iadd           
        //   408: istore_3       
        //   409: aload_1        
        //   410: getstatic       org/apache/log4j/Layout.LINE_SEP:Ljava/lang/String;
        //   413: iload_3        
        //   414: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;I)I
        //   417: istore          4
        //   419: iload           4
        //   421: iconst_m1      
        //   422: if_icmpeq       12
        //   425: getstatic       org/apache/log4j/spi/LocationInfo.inVisualAge:Z
        //   428: ifne            449
        //   431: aload_1        
        //   432: ldc             "at "
        //   434: iload           4
        //   436: invokevirtual   java/lang/String.lastIndexOf:(Ljava/lang/String;I)I
        //   439: istore_3       
        //   440: iload_3        
        //   441: iconst_m1      
        //   442: if_icmpeq       12
        //   445: iload_3        
        //   446: iconst_3       
        //   447: iadd           
        //   448: istore_3       
        //   449: aload_0        
        //   450: aload_1        
        //   451: iload_3        
        //   452: iload           4
        //   454: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   457: putfield        org/apache/log4j/spi/LocationInfo.fullInfo:Ljava/lang/String;
        //   460: return         
        //   461: aload_0        
        //   462: iload_3        
        //   463: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //   466: putfield        org/apache/log4j/spi/LocationInfo.lineNumber:Ljava/lang/String;
        //   469: goto            168
        //   472: astore          6
        //   474: aload           6
        //   476: invokevirtual   java/lang/reflect/InvocationTargetException.getTargetException:()Ljava/lang/Throwable;
        //   479: instanceof      Ljava/lang/InterruptedException;
        //   482: ifne            496
        //   485: aload           6
        //   487: invokevirtual   java/lang/reflect/InvocationTargetException.getTargetException:()Ljava/lang/Throwable;
        //   490: instanceof      Ljava/io/InterruptedIOException;
        //   493: ifeq            502
        //   496: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   499: invokevirtual   java/lang/Thread.interrupt:()V
        //   502: ldc             "LocationInfo failed using JDK 1.4 methods"
        //   504: aload           6
        //   506: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   509: goto            268
        //   512: aload           7
        //   514: astore          6
        //   516: iload_3        
        //   517: iconst_1       
        //   518: isub           
        //   519: istore_3       
        //   520: goto            45
        //   523: astore          6
        //   525: ldc             "LocationInfo failed using JDK 1.4 methods"
        //   527: aload           6
        //   529: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   532: goto            268
        //   535: astore_1       
        //   536: aload           6
        //   538: monitorexit    
        //   539: aload_1        
        //   540: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  19     35     259    268    Ljava/lang/IllegalAccessException;
        //  19     35     472    512    Ljava/lang/reflect/InvocationTargetException;
        //  19     35     523    535    Ljava/lang/RuntimeException;
        //  39     45     259    268    Ljava/lang/IllegalAccessException;
        //  39     45     472    512    Ljava/lang/reflect/InvocationTargetException;
        //  39     45     523    535    Ljava/lang/RuntimeException;
        //  49     74     259    268    Ljava/lang/IllegalAccessException;
        //  49     74     472    512    Ljava/lang/reflect/InvocationTargetException;
        //  49     74     523    535    Ljava/lang/RuntimeException;
        //  78     140    259    268    Ljava/lang/IllegalAccessException;
        //  78     140    472    512    Ljava/lang/reflect/InvocationTargetException;
        //  78     140    523    535    Ljava/lang/RuntimeException;
        //  140    158    259    268    Ljava/lang/IllegalAccessException;
        //  140    158    472    512    Ljava/lang/reflect/InvocationTargetException;
        //  140    158    523    535    Ljava/lang/RuntimeException;
        //  162    168    259    268    Ljava/lang/IllegalAccessException;
        //  162    168    472    512    Ljava/lang/reflect/InvocationTargetException;
        //  162    168    523    535    Ljava/lang/RuntimeException;
        //  168    258    259    268    Ljava/lang/IllegalAccessException;
        //  168    258    472    512    Ljava/lang/reflect/InvocationTargetException;
        //  168    258    523    535    Ljava/lang/RuntimeException;
        //  276    303    535    541    Any
        //  461    469    259    268    Ljava/lang/IllegalAccessException;
        //  461    469    472    512    Ljava/lang/reflect/InvocationTargetException;
        //  461    469    523    535    Ljava/lang/RuntimeException;
        //  536    539    535    541    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0496:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static final void appendFragment(final StringBuffer sb, final String s) {
        if (s == null) {
            sb.append("?");
            return;
        }
        sb.append(s);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public String getClassName() {
        if (this.fullInfo == null) {
            return "?";
        }
        if (this.className == null) {
            final int lastIndex = this.fullInfo.lastIndexOf(40);
            if (lastIndex == -1) {
                this.className = "?";
            }
            else {
                final int lastIndex2 = this.fullInfo.lastIndexOf(46, lastIndex);
                int n = 0;
                if (LocationInfo.inVisualAge) {
                    n = this.fullInfo.lastIndexOf(32, lastIndex2) + 1;
                }
                if (lastIndex2 == -1) {
                    this.className = "?";
                }
                else {
                    this.className = this.fullInfo.substring(n, lastIndex2);
                }
            }
        }
        return this.className;
    }
    
    public String getFileName() {
        if (this.fullInfo == null) {
            return "?";
        }
        if (this.fileName == null) {
            final int lastIndex = this.fullInfo.lastIndexOf(58);
            if (lastIndex == -1) {
                this.fileName = "?";
            }
            else {
                this.fileName = this.fullInfo.substring(this.fullInfo.lastIndexOf(40, lastIndex - 1) + 1, lastIndex);
            }
        }
        return this.fileName;
    }
    
    public String getLineNumber() {
        if (this.fullInfo == null) {
            return "?";
        }
        if (this.lineNumber == null) {
            final int lastIndex = this.fullInfo.lastIndexOf(41);
            final int lastIndex2 = this.fullInfo.lastIndexOf(58, lastIndex - 1);
            if (lastIndex2 == -1) {
                this.lineNumber = "?";
            }
            else {
                this.lineNumber = this.fullInfo.substring(lastIndex2 + 1, lastIndex);
            }
        }
        return this.lineNumber;
    }
    
    public String getMethodName() {
        if (this.fullInfo == null) {
            return "?";
        }
        if (this.methodName == null) {
            final int lastIndex = this.fullInfo.lastIndexOf(40);
            final int lastIndex2 = this.fullInfo.lastIndexOf(46, lastIndex);
            if (lastIndex2 == -1) {
                this.methodName = "?";
            }
            else {
                this.methodName = this.fullInfo.substring(lastIndex2 + 1, lastIndex);
            }
        }
        return this.methodName;
    }
}
