package org.apache.log4j.spi;

import java.io.*;

class NullWriter extends Writer
{
    @Override
    public void close() {
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public void write(final char[] array, final int n, final int n2) {
    }
}
