package org.apache.log4j.spi;

import java.util.*;
import org.apache.log4j.*;

public interface LoggerRepository
{
    void addHierarchyEventListener(final HierarchyEventListener p0);
    
    void emitNoAppenderWarning(final Category p0);
    
    Logger exists(final String p0);
    
    void fireAddAppenderEvent(final Category p0, final Appender p1);
    
    Enumeration getCurrentCategories();
    
    Enumeration getCurrentLoggers();
    
    Logger getLogger(final String p0);
    
    Logger getLogger(final String p0, final LoggerFactory p1);
    
    Logger getRootLogger();
    
    Level getThreshold();
    
    boolean isDisabled(final int p0);
    
    void resetConfiguration();
    
    void setThreshold(final String p0);
    
    void setThreshold(final Level p0);
    
    void shutdown();
}
