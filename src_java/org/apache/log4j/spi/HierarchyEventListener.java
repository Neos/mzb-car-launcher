package org.apache.log4j.spi;

import org.apache.log4j.*;

public interface HierarchyEventListener
{
    void addAppenderEvent(final Category p0, final Appender p1);
    
    void removeAppenderEvent(final Category p0, final Appender p1);
}
