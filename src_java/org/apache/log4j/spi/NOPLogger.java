package org.apache.log4j.spi;

import org.apache.log4j.*;
import java.util.*;

public final class NOPLogger extends Logger
{
    public NOPLogger(final NOPLoggerRepository repository, final String s) {
        super(s);
        this.repository = repository;
        this.level = Level.OFF;
        this.parent = this;
    }
    
    @Override
    public void addAppender(final Appender appender) {
    }
    
    @Override
    public void assertLog(final boolean b, final String s) {
    }
    
    @Override
    public void callAppenders(final LoggingEvent loggingEvent) {
    }
    
    @Override
    void closeNestedAppenders() {
    }
    
    @Override
    public void debug(final Object o) {
    }
    
    @Override
    public void debug(final Object o, final Throwable t) {
    }
    
    @Override
    public void error(final Object o) {
    }
    
    @Override
    public void error(final Object o, final Throwable t) {
    }
    
    @Override
    public void fatal(final Object o) {
    }
    
    @Override
    public void fatal(final Object o, final Throwable t) {
    }
    
    @Override
    public Enumeration getAllAppenders() {
        return new Vector().elements();
    }
    
    @Override
    public Appender getAppender(final String s) {
        return null;
    }
    
    @Override
    public Priority getChainedPriority() {
        return this.getEffectiveLevel();
    }
    
    @Override
    public Level getEffectiveLevel() {
        return Level.OFF;
    }
    
    @Override
    public ResourceBundle getResourceBundle() {
        return null;
    }
    
    @Override
    public void info(final Object o) {
    }
    
    @Override
    public void info(final Object o, final Throwable t) {
    }
    
    @Override
    public boolean isAttached(final Appender appender) {
        return false;
    }
    
    @Override
    public boolean isDebugEnabled() {
        return false;
    }
    
    @Override
    public boolean isEnabledFor(final Priority priority) {
        return false;
    }
    
    @Override
    public boolean isInfoEnabled() {
        return false;
    }
    
    @Override
    public boolean isTraceEnabled() {
        return false;
    }
    
    @Override
    public void l7dlog(final Priority priority, final String s, final Throwable t) {
    }
    
    @Override
    public void l7dlog(final Priority priority, final String s, final Object[] array, final Throwable t) {
    }
    
    @Override
    public void log(final String s, final Priority priority, final Object o, final Throwable t) {
    }
    
    @Override
    public void log(final Priority priority, final Object o) {
    }
    
    @Override
    public void log(final Priority priority, final Object o, final Throwable t) {
    }
    
    @Override
    public void removeAllAppenders() {
    }
    
    @Override
    public void removeAppender(final String s) {
    }
    
    @Override
    public void removeAppender(final Appender appender) {
    }
    
    @Override
    public void setLevel(final Level level) {
    }
    
    @Override
    public void setPriority(final Priority priority) {
    }
    
    @Override
    public void setResourceBundle(final ResourceBundle resourceBundle) {
    }
    
    @Override
    public void trace(final Object o) {
    }
    
    @Override
    public void trace(final Object o, final Throwable t) {
    }
    
    @Override
    public void warn(final Object o) {
    }
    
    @Override
    public void warn(final Object o, final Throwable t) {
    }
}
