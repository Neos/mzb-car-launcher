package org.apache.log4j;

import org.apache.log4j.spi.*;
import java.util.*;

public final class LogSF extends LogXF
{
    private static final String FQCN;
    static Class class$org$apache$log4j$LogSF;
    
    static {
        Class class$org$apache$log4j$LogSF;
        if (LogSF.class$org$apache$log4j$LogSF == null) {
            class$org$apache$log4j$LogSF = (LogSF.class$org$apache$log4j$LogSF = class$("org.apache.log4j.LogSF"));
        }
        else {
            class$org$apache$log4j$LogSF = LogSF.class$org$apache$log4j$LogSF;
        }
        FQCN = class$org$apache$log4j$LogSF.getName();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void debug(final Logger logger, final String s, final byte b) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final char c) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final double n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final float n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final int n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final long n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, o));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final short n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final boolean b) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object[] array) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, array));
        }
    }
    
    public static void debug(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, array), t);
        }
    }
    
    public static void error(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.ERROR)) {
            forcedLog(logger, Level.ERROR, format(s, array));
        }
    }
    
    public static void error(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.ERROR)) {
            forcedLog(logger, Level.ERROR, format(s, array), t);
        }
    }
    
    public static void fatal(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.FATAL)) {
            forcedLog(logger, Level.FATAL, format(s, array));
        }
    }
    
    public static void fatal(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.FATAL)) {
            forcedLog(logger, Level.FATAL, format(s, array), t);
        }
    }
    
    private static void forcedLog(final Logger logger, final Level level, final String s) {
        logger.callAppenders(new LoggingEvent(LogSF.FQCN, logger, level, s, null));
    }
    
    private static void forcedLog(final Logger logger, final Level level, final String s, final Throwable t) {
        logger.callAppenders(new LoggingEvent(LogSF.FQCN, logger, level, s, t));
    }
    
    private static String format(final String s, final Object o) {
        String format = s;
        if (s != null) {
            if (s.indexOf("\\{") >= 0) {
                format = format(s, new Object[] { o });
            }
            else {
                final int index = s.indexOf("{}");
                format = s;
                if (index >= 0) {
                    return new StringBuffer().append(s.substring(0, index)).append(o).append(s.substring(index + 2)).toString();
                }
            }
        }
        return format;
    }
    
    private static String format(String string, final String s, final Object[] array) {
        while (true) {
            Label_0025: {
                if (string == null) {
                    break Label_0025;
                }
                try {
                    string = ResourceBundle.getBundle(string).getString(s);
                    return format(string, array);
                }
                catch (Exception ex) {
                    string = s;
                    return format(string, array);
                }
            }
            string = s;
            continue;
        }
    }
    
    private static String format(final String s, final Object[] array) {
        if (s != null) {
            String s2 = "";
            int n = 0;
            int i = s.indexOf("{");
            int n2 = 0;
            while (i >= 0) {
                if (i == 0 || s.charAt(i - 1) != '\\') {
                    final String string = new StringBuffer().append(s2).append(s.substring(n, i)).toString();
                    if (i + 1 < s.length() && s.charAt(i + 1) == '}') {
                        if (array != null && n2 < array.length) {
                            final StringBuffer append = new StringBuffer().append(string);
                            final int n3 = n2 + 1;
                            s2 = append.append(array[n2]).toString();
                            n2 = n3;
                        }
                        else {
                            s2 = new StringBuffer().append(string).append("{}").toString();
                        }
                        n = i + 2;
                    }
                    else {
                        s2 = new StringBuffer().append(string).append("{").toString();
                        n = i + 1;
                    }
                }
                else {
                    s2 = new StringBuffer().append(s2).append(s.substring(n, i - 1)).append("{").toString();
                    n = i + 1;
                }
                i = s.indexOf("{", n);
            }
            return new StringBuffer().append(s2).append(s.substring(n)).toString();
        }
        return null;
    }
    
    public static void info(final Logger logger, final String s, final byte b) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void info(final Logger logger, final String s, final char c) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void info(final Logger logger, final String s, final double n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final float n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final int n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final long n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, o));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void info(final Logger logger, final String s, final short n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final boolean b) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object[] array) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, array));
        }
    }
    
    public static void info(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, array), t);
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final byte b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final char c) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(c))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final double n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final float n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final int n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final long n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o, final Object o2) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final short n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final boolean b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, array));
        }
    }
    
    public static void log(final Logger logger, final Level level, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, array), t);
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final byte b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final char c) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(c))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final double n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final float n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final int n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final long n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o, final Object o2) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o, o2)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final short n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final boolean b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, array));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final Throwable t, final String s, final String s2, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, array), t);
        }
    }
    
    public static void trace(final Logger logger, final String s, final byte b) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final char c) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final double n) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final float n) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final int n) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final long n) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, o));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final short n) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final boolean b) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, array));
        }
    }
    
    public static void trace(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(LogSF.TRACE)) {
            forcedLog(logger, LogSF.TRACE, format(s, array), t);
        }
    }
    
    public static void warn(final Logger logger, final String s, final byte b) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final char c) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final double n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final float n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final int n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final long n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, o));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final short n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final boolean b) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, array));
        }
    }
    
    public static void warn(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, array), t);
        }
    }
}
