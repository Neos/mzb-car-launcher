package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.util.*;
import java.lang.reflect.*;

public class MDC
{
    static final int HT_SIZE = 7;
    static Class class$java$lang$ThreadLocal;
    static final MDC mdc;
    boolean java1;
    private Method removeMethod;
    Object tlm;
    
    static {
        mdc = new MDC();
    }
    
    private MDC() {
        if (!(this.java1 = Loader.isJava1())) {
            this.tlm = new ThreadLocalMap();
        }
        try {
            Class class$java$lang$ThreadLocal;
            if (MDC.class$java$lang$ThreadLocal == null) {
                class$java$lang$ThreadLocal = (MDC.class$java$lang$ThreadLocal = class$("java.lang.ThreadLocal"));
            }
            else {
                class$java$lang$ThreadLocal = MDC.class$java$lang$ThreadLocal;
            }
            this.removeMethod = class$java$lang$ThreadLocal.getMethod("remove", (Class[])null);
        }
        catch (NoSuchMethodException ex) {}
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void clear() {
        if (MDC.mdc != null) {
            MDC.mdc.clear0();
        }
    }
    
    private void clear0() {
        if (this.java1 || this.tlm == null) {
            return;
        }
        final Hashtable hashtable = ((ThreadLocalMap)this.tlm).get();
        if (hashtable != null) {
            hashtable.clear();
        }
        if (this.removeMethod == null) {
            return;
        }
        try {
            this.removeMethod.invoke(this.tlm, (Object[])null);
        }
        catch (InvocationTargetException ex) {}
        catch (IllegalAccessException ex2) {}
    }
    
    public static Object get(final String s) {
        if (MDC.mdc != null) {
            return MDC.mdc.get0(s);
        }
        return null;
    }
    
    private Object get0(final String s) {
        if (this.java1 || this.tlm == null) {
            return null;
        }
        final Hashtable<K, Object> hashtable = ((ThreadLocalMap)this.tlm).get();
        if (hashtable != null && s != null) {
            return hashtable.get(s);
        }
        return null;
    }
    
    public static Hashtable getContext() {
        if (MDC.mdc != null) {
            return MDC.mdc.getContext0();
        }
        return null;
    }
    
    private Hashtable getContext0() {
        if (this.java1 || this.tlm == null) {
            return null;
        }
        return ((ThreadLocalMap)this.tlm).get();
    }
    
    public static void put(final String s, final Object o) {
        if (MDC.mdc != null) {
            MDC.mdc.put0(s, o);
        }
    }
    
    private void put0(final String s, final Object o) {
        if (this.java1 || this.tlm == null) {
            return;
        }
        Hashtable<String, Object> hashtable;
        if ((hashtable = ((ThreadLocalMap)this.tlm).get()) == null) {
            hashtable = new Hashtable<String, Object>(7);
            ((ThreadLocalMap)this.tlm).set(hashtable);
        }
        hashtable.put(s, o);
    }
    
    public static void remove(final String s) {
        if (MDC.mdc != null) {
            MDC.mdc.remove0(s);
        }
    }
    
    private void remove0(final String s) {
        if (!this.java1 && this.tlm != null) {
            final Hashtable hashtable = ((ThreadLocalMap)this.tlm).get();
            if (hashtable != null) {
                hashtable.remove(s);
                if (hashtable.isEmpty()) {
                    this.clear0();
                }
            }
        }
    }
}
