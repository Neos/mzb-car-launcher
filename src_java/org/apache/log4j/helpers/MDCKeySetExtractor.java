package org.apache.log4j.helpers;

import java.lang.reflect.*;
import org.apache.log4j.spi.*;
import java.util.*;
import java.io.*;
import org.apache.log4j.pattern.*;

public final class MDCKeySetExtractor
{
    public static final MDCKeySetExtractor INSTANCE;
    static Class class$org$apache$log4j$pattern$LogEvent;
    static Class class$org$apache$log4j$spi$LoggingEvent;
    private final Method getKeySetMethod;
    
    static {
        INSTANCE = new MDCKeySetExtractor();
    }
    
    private MDCKeySetExtractor() {
        while (true) {
            try {
                Class class$org$apache$log4j$spi$LoggingEvent;
                if (MDCKeySetExtractor.class$org$apache$log4j$spi$LoggingEvent == null) {
                    class$org$apache$log4j$spi$LoggingEvent = (MDCKeySetExtractor.class$org$apache$log4j$spi$LoggingEvent = class$("org.apache.log4j.spi.LoggingEvent"));
                }
                else {
                    class$org$apache$log4j$spi$LoggingEvent = MDCKeySetExtractor.class$org$apache$log4j$spi$LoggingEvent;
                }
                final Method method = class$org$apache$log4j$spi$LoggingEvent.getMethod("getPropertyKeySet", (Class[])null);
                this.getKeySetMethod = method;
            }
            catch (Exception ex) {
                final Method method = null;
                continue;
            }
            break;
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public Set getPropertyKeySet(final LoggingEvent loggingEvent) throws Exception {
        final Set set = null;
        final Set set2 = null;
        if (this.getKeySetMethod == null) {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(loggingEvent);
            objectOutputStream.close();
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            Class class$org$apache$log4j$pattern$LogEvent;
            if (MDCKeySetExtractor.class$org$apache$log4j$pattern$LogEvent == null) {
                class$org$apache$log4j$pattern$LogEvent = (MDCKeySetExtractor.class$org$apache$log4j$pattern$LogEvent = class$("org.apache.log4j.pattern.LogEvent"));
            }
            else {
                class$org$apache$log4j$pattern$LogEvent = MDCKeySetExtractor.class$org$apache$log4j$pattern$LogEvent;
            }
            final String name = class$org$apache$log4j$pattern$LogEvent.getName();
            if (byteArray[6] != 0) {
                final Set set3 = set2;
                if (byteArray[7] != name.length()) {
                    return set3;
                }
            }
            for (int i = 0; i < name.length(); ++i) {
                byteArray[i + 8] = (byte)name.charAt(i);
            }
            final ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(byteArray));
            final Object object = objectInputStream.readObject();
            Set propertyKeySet = set;
            if (object instanceof LogEvent) {
                propertyKeySet = ((LogEvent)object).getPropertyKeySet();
            }
            objectInputStream.close();
            return propertyKeySet;
        }
        return (Set)this.getKeySetMethod.invoke(loggingEvent, (Object[])null);
    }
}
