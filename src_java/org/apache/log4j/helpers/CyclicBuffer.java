package org.apache.log4j.helpers;

import org.apache.log4j.spi.*;

public class CyclicBuffer
{
    LoggingEvent[] ea;
    int first;
    int last;
    int maxSize;
    int numElems;
    
    public CyclicBuffer(final int maxSize) throws IllegalArgumentException {
        if (maxSize < 1) {
            throw new IllegalArgumentException(new StringBuffer().append("The maxSize argument (").append(maxSize).append(") is not a positive integer.").toString());
        }
        this.maxSize = maxSize;
        this.ea = new LoggingEvent[maxSize];
        this.first = 0;
        this.last = 0;
        this.numElems = 0;
    }
    
    public void add(final LoggingEvent loggingEvent) {
        this.ea[this.last] = loggingEvent;
        final int last = this.last + 1;
        this.last = last;
        if (last == this.maxSize) {
            this.last = 0;
        }
        if (this.numElems < this.maxSize) {
            ++this.numElems;
        }
        else if (++this.first == this.maxSize) {
            this.first = 0;
        }
    }
    
    public LoggingEvent get() {
        LoggingEvent loggingEvent = null;
        if (this.numElems > 0) {
            --this.numElems;
            final LoggingEvent loggingEvent2 = this.ea[this.first];
            this.ea[this.first] = null;
            final int first = this.first + 1;
            this.first = first;
            loggingEvent = loggingEvent2;
            if (first == this.maxSize) {
                this.first = 0;
                loggingEvent = loggingEvent2;
            }
        }
        return loggingEvent;
    }
    
    public LoggingEvent get(final int n) {
        if (n < 0 || n >= this.numElems) {
            return null;
        }
        return this.ea[(this.first + n) % this.maxSize];
    }
    
    public int getMaxSize() {
        return this.maxSize;
    }
    
    public int length() {
        return this.numElems;
    }
    
    public void resize(final int maxSize) {
        if (maxSize < 0) {
            throw new IllegalArgumentException(new StringBuffer().append("Negative array size [").append(maxSize).append("] not allowed.").toString());
        }
        if (maxSize == this.numElems) {
            return;
        }
        final LoggingEvent[] ea = new LoggingEvent[maxSize];
        int numElems;
        if (maxSize < this.numElems) {
            numElems = maxSize;
        }
        else {
            numElems = this.numElems;
        }
        for (int i = 0; i < numElems; ++i) {
            ea[i] = this.ea[this.first];
            this.ea[this.first] = null;
            if (++this.first == this.numElems) {
                this.first = 0;
            }
        }
        this.ea = ea;
        this.first = 0;
        if ((this.numElems = numElems) == (this.maxSize = maxSize)) {
            this.last = 0;
            return;
        }
        this.last = numElems;
    }
}
