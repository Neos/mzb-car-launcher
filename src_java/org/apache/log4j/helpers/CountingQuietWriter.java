package org.apache.log4j.helpers;

import org.apache.log4j.spi.*;
import java.io.*;

public class CountingQuietWriter extends QuietWriter
{
    protected long count;
    
    public CountingQuietWriter(final Writer writer, final ErrorHandler errorHandler) {
        super(writer, errorHandler);
    }
    
    public long getCount() {
        return this.count;
    }
    
    public void setCount(final long count) {
        this.count = count;
    }
    
    @Override
    public void write(final String s) {
        try {
            this.out.write(s);
            this.count += s.length();
        }
        catch (IOException ex) {
            this.errorHandler.error("Write failure.", ex, 1);
        }
    }
}
