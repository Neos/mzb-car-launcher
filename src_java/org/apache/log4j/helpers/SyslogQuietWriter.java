package org.apache.log4j.helpers;

import java.io.*;
import org.apache.log4j.spi.*;

public class SyslogQuietWriter extends QuietWriter
{
    int level;
    int syslogFacility;
    
    public SyslogQuietWriter(final Writer writer, final int syslogFacility, final ErrorHandler errorHandler) {
        super(writer, errorHandler);
        this.syslogFacility = syslogFacility;
    }
    
    public void setLevel(final int level) {
        this.level = level;
    }
    
    public void setSyslogFacility(final int syslogFacility) {
        this.syslogFacility = syslogFacility;
    }
    
    @Override
    public void write(final String s) {
        super.write(new StringBuffer().append("<").append(this.syslogFacility | this.level).append(">").append(s).toString());
    }
}
