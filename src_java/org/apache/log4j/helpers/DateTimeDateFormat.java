package org.apache.log4j.helpers;

import java.util.*;
import java.text.*;

public class DateTimeDateFormat extends AbsoluteTimeDateFormat
{
    private static final long serialVersionUID = 5547637772208514971L;
    String[] shortMonths;
    
    public DateTimeDateFormat() {
        this.shortMonths = new DateFormatSymbols().getShortMonths();
    }
    
    public DateTimeDateFormat(final TimeZone timeZone) {
        this();
        this.setCalendar(Calendar.getInstance(timeZone));
    }
    
    @Override
    public StringBuffer format(final Date time, final StringBuffer sb, final FieldPosition fieldPosition) {
        this.calendar.setTime(time);
        final int value = this.calendar.get(5);
        if (value < 10) {
            sb.append('0');
        }
        sb.append(value);
        sb.append(' ');
        sb.append(this.shortMonths[this.calendar.get(2)]);
        sb.append(' ');
        sb.append(this.calendar.get(1));
        sb.append(' ');
        return super.format(time, sb, fieldPosition);
    }
    
    @Override
    public Date parse(final String s, final ParsePosition parsePosition) {
        return null;
    }
}
