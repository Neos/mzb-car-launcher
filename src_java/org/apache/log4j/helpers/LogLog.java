package org.apache.log4j.helpers;

public class LogLog
{
    public static final String CONFIG_DEBUG_KEY = "log4j.configDebug";
    public static final String DEBUG_KEY = "log4j.debug";
    private static final String ERR_PREFIX = "log4j:ERROR ";
    private static final String PREFIX = "log4j: ";
    private static final String WARN_PREFIX = "log4j:WARN ";
    protected static boolean debugEnabled;
    private static boolean quietMode;
    
    static {
        LogLog.debugEnabled = false;
        LogLog.quietMode = false;
        String s;
        if ((s = OptionConverter.getSystemProperty("log4j.debug", null)) == null) {
            s = OptionConverter.getSystemProperty("log4j.configDebug", null);
        }
        if (s != null) {
            LogLog.debugEnabled = OptionConverter.toBoolean(s, true);
        }
    }
    
    public static void debug(final String s) {
        if (LogLog.debugEnabled && !LogLog.quietMode) {
            System.out.println(new StringBuffer().append("log4j: ").append(s).toString());
        }
    }
    
    public static void debug(final String s, final Throwable t) {
        if (LogLog.debugEnabled && !LogLog.quietMode) {
            System.out.println(new StringBuffer().append("log4j: ").append(s).toString());
            if (t != null) {
                t.printStackTrace(System.out);
            }
        }
    }
    
    public static void error(final String s) {
        if (LogLog.quietMode) {
            return;
        }
        System.err.println(new StringBuffer().append("log4j:ERROR ").append(s).toString());
    }
    
    public static void error(final String s, final Throwable t) {
        if (!LogLog.quietMode) {
            System.err.println(new StringBuffer().append("log4j:ERROR ").append(s).toString());
            if (t != null) {
                t.printStackTrace();
            }
        }
    }
    
    public static void setInternalDebugging(final boolean debugEnabled) {
        LogLog.debugEnabled = debugEnabled;
    }
    
    public static void setQuietMode(final boolean quietMode) {
        LogLog.quietMode = quietMode;
    }
    
    public static void warn(final String s) {
        if (LogLog.quietMode) {
            return;
        }
        System.err.println(new StringBuffer().append("log4j:WARN ").append(s).toString());
    }
    
    public static void warn(final String s, final Throwable t) {
        if (!LogLog.quietMode) {
            System.err.println(new StringBuffer().append("log4j:WARN ").append(s).toString());
            if (t != null) {
                t.printStackTrace();
            }
        }
    }
}
