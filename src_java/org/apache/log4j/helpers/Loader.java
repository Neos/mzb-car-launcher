package org.apache.log4j.helpers;

import java.net.*;
import java.io.*;
import java.lang.reflect.*;

public class Loader
{
    static final String TSTR = "Caught Exception while in Loader.getResource. This may be innocuous.";
    static Class class$java$lang$Thread;
    static Class class$org$apache$log4j$helpers$Loader;
    private static boolean ignoreTCL;
    private static boolean java1;
    
    static {
        Loader.java1 = true;
        Loader.ignoreTCL = false;
        final String systemProperty = OptionConverter.getSystemProperty("java.version", null);
        if (systemProperty != null) {
            final int index = systemProperty.indexOf(46);
            if (index != -1 && systemProperty.charAt(index + 1) != '1') {
                Loader.java1 = false;
            }
        }
        final String systemProperty2 = OptionConverter.getSystemProperty("log4j.ignoreTCL", null);
        if (systemProperty2 != null) {
            Loader.ignoreTCL = OptionConverter.toBoolean(systemProperty2, true);
        }
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static URL getResource(final String s) {
        try {
            if (!Loader.java1 && !Loader.ignoreTCL) {
                final ClassLoader tcl = getTCL();
                if (tcl != null) {
                    LogLog.debug(new StringBuffer().append("Trying to find [").append(s).append("] using context classloader ").append(tcl).append(".").toString());
                    final URL resource = tcl.getResource(s);
                    if (resource != null) {
                        return resource;
                    }
                }
            }
            Class class$org$apache$log4j$helpers$Loader;
            if (Loader.class$org$apache$log4j$helpers$Loader == null) {
                class$org$apache$log4j$helpers$Loader = (Loader.class$org$apache$log4j$helpers$Loader = class$("org.apache.log4j.helpers.Loader"));
            }
            else {
                class$org$apache$log4j$helpers$Loader = Loader.class$org$apache$log4j$helpers$Loader;
            }
            final ClassLoader classLoader = class$org$apache$log4j$helpers$Loader.getClassLoader();
            if (classLoader == null) {
                goto Label_0155;
            }
            LogLog.debug(new StringBuffer().append("Trying to find [").append(s).append("] using ").append(classLoader).append(" class loader.").toString());
            final URL resource2 = classLoader.getResource(s);
            if (resource2 != null) {
                return resource2;
            }
            goto Label_0155;
        }
        catch (IllegalAccessException ex) {
            LogLog.warn("Caught Exception while in Loader.getResource. This may be innocuous.", ex);
        }
        catch (InvocationTargetException ex2) {
            if (ex2.getTargetException() instanceof InterruptedException || ex2.getTargetException() instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            LogLog.warn("Caught Exception while in Loader.getResource. This may be innocuous.", ex2);
            goto Label_0155;
        }
        catch (Throwable t) {
            LogLog.warn("Caught Exception while in Loader.getResource. This may be innocuous.", t);
            goto Label_0155;
        }
    }
    
    public static URL getResource(final String s, final Class clazz) {
        return getResource(s);
    }
    
    private static ClassLoader getTCL() throws IllegalAccessException, InvocationTargetException {
        try {
            Class class$java$lang$Thread;
            if (Loader.class$java$lang$Thread == null) {
                class$java$lang$Thread = (Loader.class$java$lang$Thread = class$("java.lang.Thread"));
            }
            else {
                class$java$lang$Thread = Loader.class$java$lang$Thread;
            }
            return (ClassLoader)class$java$lang$Thread.getMethod("getContextClassLoader", (Class[])null).invoke(Thread.currentThread(), (Object[])null);
        }
        catch (NoSuchMethodException ex) {
            return null;
        }
    }
    
    public static boolean isJava1() {
        return Loader.java1;
    }
    
    public static Class loadClass(final String s) throws ClassNotFoundException {
        if (Loader.java1 || Loader.ignoreTCL) {
            return Class.forName(s);
        }
        try {
            return getTCL().loadClass(s);
        }
        catch (InvocationTargetException ex) {
            if (!(ex.getTargetException() instanceof InterruptedException) && !(ex.getTargetException() instanceof InterruptedIOException)) {
                goto Label_0054;
            }
            Thread.currentThread().interrupt();
        }
        catch (Throwable t) {
            goto Label_0054;
        }
    }
}
