package org.apache.log4j.helpers;

import java.io.*;

public abstract class FileWatchdog extends Thread
{
    public static final long DEFAULT_DELAY = 60000L;
    protected long delay;
    File file;
    protected String filename;
    boolean interrupted;
    long lastModif;
    boolean warnedAlready;
    
    protected FileWatchdog(final String filename) {
        super("FileWatchdog");
        this.delay = 60000L;
        this.lastModif = 0L;
        this.warnedAlready = false;
        this.interrupted = false;
        this.filename = filename;
        this.file = new File(filename);
        this.setDaemon(true);
        this.checkAndConfigure();
    }
    
    protected void checkAndConfigure() {
        while (true) {
            try {
                if (this.file.exists()) {
                    final long lastModified = this.file.lastModified();
                    if (lastModified > this.lastModif) {
                        this.lastModif = lastModified;
                        this.doOnChange();
                        this.warnedAlready = false;
                    }
                    return;
                }
            }
            catch (SecurityException ex) {
                LogLog.warn(new StringBuffer().append("Was not allowed to read check file existance, file:[").append(this.filename).append("].").toString());
                this.interrupted = true;
                return;
            }
            if (!this.warnedAlready) {
                break;
            }
            return;
        }
        LogLog.debug(new StringBuffer().append("[").append(this.filename).append("] does not exist.").toString());
        this.warnedAlready = true;
    }
    
    protected abstract void doOnChange();
    
    @Override
    public void run() {
    Label_0014_Outer:
        while (true) {
            if (this.interrupted) {
                return;
            }
            while (true) {
                try {
                    Thread.sleep(this.delay);
                    this.checkAndConfigure();
                    continue Label_0014_Outer;
                }
                catch (InterruptedException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    public void setDelay(final long delay) {
        this.delay = delay;
    }
}
