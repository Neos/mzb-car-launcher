package org.apache.log4j.helpers;

import org.apache.log4j.spi.*;

public abstract class PatternConverter
{
    static String[] SPACES;
    boolean leftAlign;
    int max;
    int min;
    public PatternConverter next;
    
    static {
        PatternConverter.SPACES = new String[] { " ", "  ", "    ", "        ", "                ", "                                " };
    }
    
    protected PatternConverter() {
        this.min = -1;
        this.max = Integer.MAX_VALUE;
        this.leftAlign = false;
    }
    
    protected PatternConverter(final FormattingInfo formattingInfo) {
        this.min = -1;
        this.max = Integer.MAX_VALUE;
        this.leftAlign = false;
        this.min = formattingInfo.min;
        this.max = formattingInfo.max;
        this.leftAlign = formattingInfo.leftAlign;
    }
    
    protected abstract String convert(final LoggingEvent p0);
    
    public void format(final StringBuffer sb, final LoggingEvent loggingEvent) {
        final String convert = this.convert(loggingEvent);
        if (convert == null) {
            if (this.min > 0) {
                this.spacePad(sb, this.min);
            }
            return;
        }
        final int length = convert.length();
        if (length > this.max) {
            sb.append(convert.substring(length - this.max));
            return;
        }
        if (length >= this.min) {
            sb.append(convert);
            return;
        }
        if (this.leftAlign) {
            sb.append(convert);
            this.spacePad(sb, this.min - length);
            return;
        }
        this.spacePad(sb, this.min - length);
        sb.append(convert);
    }
    
    public void spacePad(final StringBuffer sb, int i) {
        while (i >= 32) {
            sb.append(PatternConverter.SPACES[5]);
            i -= 32;
        }
        for (int j = 4; j >= 0; --j) {
            if ((1 << j & i) != 0x0) {
                sb.append(PatternConverter.SPACES[j]);
            }
        }
    }
}
