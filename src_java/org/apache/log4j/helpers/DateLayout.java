package org.apache.log4j.helpers;

import org.apache.log4j.*;
import java.util.*;
import org.apache.log4j.spi.*;
import java.text.*;

public abstract class DateLayout extends Layout
{
    public static final String DATE_FORMAT_OPTION = "DateFormat";
    public static final String NULL_DATE_FORMAT = "NULL";
    public static final String RELATIVE_TIME_DATE_FORMAT = "RELATIVE";
    public static final String TIMEZONE_OPTION = "TimeZone";
    protected Date date;
    protected DateFormat dateFormat;
    private String dateFormatOption;
    protected FieldPosition pos;
    private String timeZoneID;
    
    public DateLayout() {
        this.pos = new FieldPosition(0);
        this.date = new Date();
    }
    
    @Override
    public void activateOptions() {
        this.setDateFormat(this.dateFormatOption);
        if (this.timeZoneID != null && this.dateFormat != null) {
            this.dateFormat.setTimeZone(TimeZone.getTimeZone(this.timeZoneID));
        }
    }
    
    public void dateFormat(final StringBuffer sb, final LoggingEvent loggingEvent) {
        if (this.dateFormat != null) {
            this.date.setTime(loggingEvent.timeStamp);
            this.dateFormat.format(this.date, sb, this.pos);
            sb.append(' ');
        }
    }
    
    public String getDateFormat() {
        return this.dateFormatOption;
    }
    
    public String[] getOptionStrings() {
        return new String[] { "DateFormat", "TimeZone" };
    }
    
    public String getTimeZone() {
        return this.timeZoneID;
    }
    
    public void setDateFormat(final String dateFormatOption) {
        if (dateFormatOption != null) {
            this.dateFormatOption = dateFormatOption;
        }
        this.setDateFormat(this.dateFormatOption, TimeZone.getDefault());
    }
    
    public void setDateFormat(final String s, final TimeZone timeZone) {
        if (s == null) {
            this.dateFormat = null;
            return;
        }
        if (s.equalsIgnoreCase("NULL")) {
            this.dateFormat = null;
            return;
        }
        if (s.equalsIgnoreCase("RELATIVE")) {
            this.dateFormat = new RelativeTimeDateFormat();
            return;
        }
        if (s.equalsIgnoreCase("ABSOLUTE")) {
            this.dateFormat = new AbsoluteTimeDateFormat(timeZone);
            return;
        }
        if (s.equalsIgnoreCase("DATE")) {
            this.dateFormat = new DateTimeDateFormat(timeZone);
            return;
        }
        if (s.equalsIgnoreCase("ISO8601")) {
            this.dateFormat = new ISO8601DateFormat(timeZone);
            return;
        }
        (this.dateFormat = new SimpleDateFormat(s)).setTimeZone(timeZone);
    }
    
    public void setDateFormat(final DateFormat dateFormat, final TimeZone timeZone) {
        (this.dateFormat = dateFormat).setTimeZone(timeZone);
    }
    
    public void setOption(final String s, final String timeZoneID) {
        if (s.equalsIgnoreCase("DateFormat")) {
            this.dateFormatOption = timeZoneID.toUpperCase();
        }
        else if (s.equalsIgnoreCase("TimeZone")) {
            this.timeZoneID = timeZoneID;
        }
    }
    
    public void setTimeZone(final String timeZoneID) {
        this.timeZoneID = timeZoneID;
    }
}
