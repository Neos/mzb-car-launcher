package org.apache.log4j.helpers;

import java.net.*;
import java.io.*;

public class SyslogWriter extends Writer
{
    static String syslogHost;
    final int SYSLOG_PORT;
    private InetAddress address;
    private DatagramSocket ds;
    private final int port;
    
    public SyslogWriter(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/io/Writer.<init>:()V
        //     4: aload_0        
        //     5: sipush          514
        //     8: putfield        org/apache/log4j/helpers/SyslogWriter.SYSLOG_PORT:I
        //    11: aload_1        
        //    12: putstatic       org/apache/log4j/helpers/SyslogWriter.syslogHost:Ljava/lang/String;
        //    15: aload_1        
        //    16: ifnonnull       29
        //    19: new             Ljava/lang/NullPointerException;
        //    22: dup            
        //    23: ldc             "syslogHost"
        //    25: invokespecial   java/lang/NullPointerException.<init>:(Ljava/lang/String;)V
        //    28: athrow         
        //    29: aload_1        
        //    30: astore          5
        //    32: iconst_m1      
        //    33: istore_3       
        //    34: aload           5
        //    36: ldc             "["
        //    38: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //    41: iconst_m1      
        //    42: if_icmpne       68
        //    45: aload           5
        //    47: astore          4
        //    49: iload_3        
        //    50: istore_2       
        //    51: aload           5
        //    53: bipush          58
        //    55: invokevirtual   java/lang/String.indexOf:(I)I
        //    58: aload           5
        //    60: bipush          58
        //    62: invokevirtual   java/lang/String.lastIndexOf:(I)I
        //    65: if_icmpne       195
        //    68: aload           5
        //    70: astore_1       
        //    71: new             Ljava/net/URL;
        //    74: dup            
        //    75: new             Ljava/lang/StringBuffer;
        //    78: dup            
        //    79: invokespecial   java/lang/StringBuffer.<init>:()V
        //    82: ldc             "http://"
        //    84: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    87: aload           5
        //    89: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    92: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    95: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
        //    98: astore          6
        //   100: aload           5
        //   102: astore          4
        //   104: iload_3        
        //   105: istore_2       
        //   106: aload           5
        //   108: astore_1       
        //   109: aload           6
        //   111: invokevirtual   java/net/URL.getHost:()Ljava/lang/String;
        //   114: ifnull          195
        //   117: aload           5
        //   119: astore_1       
        //   120: aload           6
        //   122: invokevirtual   java/net/URL.getHost:()Ljava/lang/String;
        //   125: astore          5
        //   127: aload           5
        //   129: astore          4
        //   131: aload           5
        //   133: astore_1       
        //   134: aload           5
        //   136: ldc             "["
        //   138: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   141: ifeq            186
        //   144: aload           5
        //   146: astore          4
        //   148: aload           5
        //   150: astore_1       
        //   151: aload           5
        //   153: aload           5
        //   155: invokevirtual   java/lang/String.length:()I
        //   158: iconst_1       
        //   159: isub           
        //   160: invokevirtual   java/lang/String.charAt:(I)C
        //   163: bipush          93
        //   165: if_icmpne       186
        //   168: aload           5
        //   170: astore_1       
        //   171: aload           5
        //   173: iconst_1       
        //   174: aload           5
        //   176: invokevirtual   java/lang/String.length:()I
        //   179: iconst_1       
        //   180: isub           
        //   181: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   184: astore          4
        //   186: aload           4
        //   188: astore_1       
        //   189: aload           6
        //   191: invokevirtual   java/net/URL.getPort:()I
        //   194: istore_2       
        //   195: iload_2        
        //   196: istore_3       
        //   197: iload_2        
        //   198: iconst_m1      
        //   199: if_icmpne       206
        //   202: sipush          514
        //   205: istore_3       
        //   206: aload_0        
        //   207: iload_3        
        //   208: putfield        org/apache/log4j/helpers/SyslogWriter.port:I
        //   211: aload_0        
        //   212: aload           4
        //   214: invokestatic    java/net/InetAddress.getByName:(Ljava/lang/String;)Ljava/net/InetAddress;
        //   217: putfield        org/apache/log4j/helpers/SyslogWriter.address:Ljava/net/InetAddress;
        //   220: aload_0        
        //   221: new             Ljava/net/DatagramSocket;
        //   224: dup            
        //   225: invokespecial   java/net/DatagramSocket.<init>:()V
        //   228: putfield        org/apache/log4j/helpers/SyslogWriter.ds:Ljava/net/DatagramSocket;
        //   231: return         
        //   232: astore          4
        //   234: ldc             "Malformed URL: will attempt to interpret as InetAddress."
        //   236: aload           4
        //   238: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   241: aload_1        
        //   242: astore          4
        //   244: iload_3        
        //   245: istore_2       
        //   246: goto            195
        //   249: astore_1       
        //   250: new             Ljava/lang/StringBuffer;
        //   253: dup            
        //   254: invokespecial   java/lang/StringBuffer.<init>:()V
        //   257: ldc             "Could not find "
        //   259: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   262: aload           4
        //   264: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   267: ldc             ". All logging will FAIL."
        //   269: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   272: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   275: aload_1        
        //   276: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   279: goto            220
        //   282: astore_1       
        //   283: aload_1        
        //   284: invokevirtual   java/net/SocketException.printStackTrace:()V
        //   287: new             Ljava/lang/StringBuffer;
        //   290: dup            
        //   291: invokespecial   java/lang/StringBuffer.<init>:()V
        //   294: ldc             "Could not instantiate DatagramSocket to "
        //   296: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   299: aload           4
        //   301: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   304: ldc             ". All logging will FAIL."
        //   306: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   309: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   312: aload_1        
        //   313: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   316: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  71     100    232    249    Ljava/net/MalformedURLException;
        //  109    117    232    249    Ljava/net/MalformedURLException;
        //  120    127    232    249    Ljava/net/MalformedURLException;
        //  134    144    232    249    Ljava/net/MalformedURLException;
        //  151    168    232    249    Ljava/net/MalformedURLException;
        //  171    186    232    249    Ljava/net/MalformedURLException;
        //  189    195    232    249    Ljava/net/MalformedURLException;
        //  211    220    249    282    Ljava/net/UnknownHostException;
        //  220    231    282    317    Ljava/net/SocketException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 156, Size: 156
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void close() {
        if (this.ds != null) {
            this.ds.close();
        }
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public void write(final String s) throws IOException {
        if (this.ds != null && this.address != null) {
            final byte[] bytes = s.getBytes();
            int length;
            if ((length = bytes.length) >= 1024) {
                length = 1024;
            }
            this.ds.send(new DatagramPacket(bytes, length, this.address, this.port));
        }
    }
    
    @Override
    public void write(final char[] array, final int n, final int n2) throws IOException {
        this.write(new String(array, n, n2));
    }
}
