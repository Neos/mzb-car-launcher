package org.apache.log4j.helpers;

import org.apache.log4j.spi.*;
import java.io.*;

public class QuietWriter extends FilterWriter
{
    protected ErrorHandler errorHandler;
    
    public QuietWriter(final Writer writer, final ErrorHandler errorHandler) {
        super(writer);
        this.setErrorHandler(errorHandler);
    }
    
    @Override
    public void flush() {
        try {
            this.out.flush();
        }
        catch (Exception ex) {
            this.errorHandler.error("Failed to flush writer,", ex, 2);
        }
    }
    
    public void setErrorHandler(final ErrorHandler errorHandler) {
        if (errorHandler == null) {
            throw new IllegalArgumentException("Attempted to set null ErrorHandler.");
        }
        this.errorHandler = errorHandler;
    }
    
    @Override
    public void write(final String s) {
        if (s == null) {
            return;
        }
        try {
            this.out.write(s);
        }
        catch (Exception ex) {
            this.errorHandler.error(new StringBuffer().append("Failed to write [").append(s).append("].").toString(), ex, 1);
        }
    }
}
