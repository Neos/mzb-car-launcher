package org.apache.log4j.helpers;

import org.apache.log4j.spi.*;
import java.io.*;
import org.apache.log4j.*;

public class OnlyOnceErrorHandler implements ErrorHandler
{
    final String ERROR_PREFIX;
    final String WARN_PREFIX;
    boolean firstTime;
    
    public OnlyOnceErrorHandler() {
        this.WARN_PREFIX = "log4j warning: ";
        this.ERROR_PREFIX = "log4j error: ";
        this.firstTime = true;
    }
    
    @Override
    public void activateOptions() {
    }
    
    @Override
    public void error(final String s) {
        if (this.firstTime) {
            LogLog.error(s);
            this.firstTime = false;
        }
    }
    
    @Override
    public void error(final String s, final Exception ex, final int n) {
        this.error(s, ex, n, null);
    }
    
    @Override
    public void error(final String s, final Exception ex, final int n, final LoggingEvent loggingEvent) {
        if (ex instanceof InterruptedIOException || ex instanceof InterruptedException) {
            Thread.currentThread().interrupt();
        }
        if (this.firstTime) {
            LogLog.error(s, ex);
            this.firstTime = false;
        }
    }
    
    @Override
    public void setAppender(final Appender appender) {
    }
    
    @Override
    public void setBackupAppender(final Appender appender) {
    }
    
    @Override
    public void setLogger(final Logger logger) {
    }
}
