package org.apache.log4j.helpers;

import org.apache.log4j.spi.*;

public class BoundedFIFO
{
    LoggingEvent[] buf;
    int first;
    int maxSize;
    int next;
    int numElements;
    
    public BoundedFIFO(final int maxSize) {
        this.numElements = 0;
        this.first = 0;
        this.next = 0;
        if (maxSize < 1) {
            throw new IllegalArgumentException(new StringBuffer().append("The maxSize argument (").append(maxSize).append(") is not a positive integer.").toString());
        }
        this.maxSize = maxSize;
        this.buf = new LoggingEvent[maxSize];
    }
    
    public LoggingEvent get() {
        if (this.numElements == 0) {
            return null;
        }
        final LoggingEvent loggingEvent = this.buf[this.first];
        this.buf[this.first] = null;
        if (++this.first == this.maxSize) {
            this.first = 0;
        }
        --this.numElements;
        return loggingEvent;
    }
    
    public int getMaxSize() {
        return this.maxSize;
    }
    
    public boolean isFull() {
        return this.numElements == this.maxSize;
    }
    
    public int length() {
        return this.numElements;
    }
    
    int min(final int n, final int n2) {
        if (n < n2) {
            return n;
        }
        return n2;
    }
    
    public void put(final LoggingEvent loggingEvent) {
        if (this.numElements != this.maxSize) {
            this.buf[this.next] = loggingEvent;
            if (++this.next == this.maxSize) {
                this.next = 0;
            }
            ++this.numElements;
        }
    }
    
    public void resize(final int maxSize) {
        synchronized (this) {
            if (maxSize != this.maxSize) {
                final LoggingEvent[] buf = new LoggingEvent[maxSize];
                final int min = this.min(this.min(this.maxSize - this.first, maxSize), this.numElements);
                System.arraycopy(this.buf, this.first, buf, 0, min);
                int min2;
                final int n = min2 = 0;
                if (min < this.numElements) {
                    min2 = n;
                    if (min < maxSize) {
                        min2 = this.min(this.numElements - min, maxSize - min);
                        System.arraycopy(this.buf, 0, buf, min, min2);
                    }
                }
                this.buf = buf;
                this.maxSize = maxSize;
                this.first = 0;
                this.numElements = min + min2;
                this.next = this.numElements;
                if (this.next == this.maxSize) {
                    this.next = 0;
                }
            }
        }
    }
    
    public boolean wasEmpty() {
        return this.numElements == 1;
    }
    
    public boolean wasFull() {
        return this.numElements + 1 == this.maxSize;
    }
}
