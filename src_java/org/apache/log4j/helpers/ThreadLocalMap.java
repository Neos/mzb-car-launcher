package org.apache.log4j.helpers;

import java.util.*;

public final class ThreadLocalMap extends InheritableThreadLocal
{
    public final Object childValue(final Object o) {
        final Hashtable hashtable = (Hashtable)o;
        if (hashtable != null) {
            return hashtable.clone();
        }
        return null;
    }
}
