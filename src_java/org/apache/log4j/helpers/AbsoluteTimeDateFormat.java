package org.apache.log4j.helpers;

import java.util.*;
import java.text.*;

public class AbsoluteTimeDateFormat extends DateFormat
{
    public static final String ABS_TIME_DATE_FORMAT = "ABSOLUTE";
    public static final String DATE_AND_TIME_DATE_FORMAT = "DATE";
    public static final String ISO8601_DATE_FORMAT = "ISO8601";
    private static long previousTime = 0L;
    private static char[] previousTimeWithoutMillis;
    private static final long serialVersionUID = -388856345976723342L;
    
    static {
        AbsoluteTimeDateFormat.previousTimeWithoutMillis = new char[9];
    }
    
    public AbsoluteTimeDateFormat() {
        this.setCalendar(Calendar.getInstance());
    }
    
    public AbsoluteTimeDateFormat(final TimeZone timeZone) {
        this.setCalendar(Calendar.getInstance(timeZone));
    }
    
    @Override
    public StringBuffer format(final Date time, final StringBuffer sb, final FieldPosition fieldPosition) {
        final long time2 = time.getTime();
        final int n = (int)(time2 % 1000L);
        if (time2 - n != AbsoluteTimeDateFormat.previousTime || AbsoluteTimeDateFormat.previousTimeWithoutMillis[0] == '\0') {
            this.calendar.setTime(time);
            final int length = sb.length();
            final int value = this.calendar.get(11);
            if (value < 10) {
                sb.append('0');
            }
            sb.append(value);
            sb.append(':');
            final int value2 = this.calendar.get(12);
            if (value2 < 10) {
                sb.append('0');
            }
            sb.append(value2);
            sb.append(':');
            final int value3 = this.calendar.get(13);
            if (value3 < 10) {
                sb.append('0');
            }
            sb.append(value3);
            sb.append(',');
            sb.getChars(length, sb.length(), AbsoluteTimeDateFormat.previousTimeWithoutMillis, 0);
            AbsoluteTimeDateFormat.previousTime = time2 - n;
        }
        else {
            sb.append(AbsoluteTimeDateFormat.previousTimeWithoutMillis);
        }
        if (n < 100) {
            sb.append('0');
        }
        if (n < 10) {
            sb.append('0');
        }
        sb.append(n);
        return sb;
    }
    
    @Override
    public Date parse(final String s, final ParsePosition parsePosition) {
        return null;
    }
}
