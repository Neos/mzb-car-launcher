package org.apache.log4j.helpers;

import java.text.*;
import java.io.*;
import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import java.util.*;

public class PatternParser
{
    static final int CLASS_LOCATION_CONVERTER = 1002;
    private static final int CONVERTER_STATE = 1;
    private static final int DOT_STATE = 3;
    private static final char ESCAPE_CHAR = '%';
    static final int FILE_LOCATION_CONVERTER = 1004;
    static final int FULL_LOCATION_CONVERTER = 1000;
    static final int LEVEL_CONVERTER = 2002;
    static final int LINE_LOCATION_CONVERTER = 1003;
    private static final int LITERAL_STATE = 0;
    private static final int MAX_STATE = 5;
    static final int MESSAGE_CONVERTER = 2004;
    static final int METHOD_LOCATION_CONVERTER = 1001;
    private static final int MIN_STATE = 4;
    static final int NDC_CONVERTER = 2003;
    static final int RELATIVE_TIME_CONVERTER = 2000;
    static final int THREAD_CONVERTER = 2001;
    static Class class$java$text$DateFormat;
    protected StringBuffer currentLiteral;
    protected FormattingInfo formattingInfo;
    PatternConverter head;
    protected int i;
    protected String pattern;
    protected int patternLength;
    int state;
    PatternConverter tail;
    
    public PatternParser(final String pattern) {
        this.currentLiteral = new StringBuffer(32);
        this.formattingInfo = new FormattingInfo();
        this.pattern = pattern;
        this.patternLength = pattern.length();
        this.state = 0;
    }
    
    private void addToList(final PatternConverter patternConverter) {
        if (this.head == null) {
            this.tail = patternConverter;
            this.head = patternConverter;
            return;
        }
        this.tail.next = patternConverter;
        this.tail = patternConverter;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    protected void addConverter(final PatternConverter patternConverter) {
        this.currentLiteral.setLength(0);
        this.addToList(patternConverter);
        this.state = 0;
        this.formattingInfo.reset();
    }
    
    protected String extractOption() {
        if (this.i < this.patternLength && this.pattern.charAt(this.i) == '{') {
            final int index = this.pattern.indexOf(125, this.i);
            if (index > this.i) {
                final String substring = this.pattern.substring(this.i + 1, index);
                this.i = index + 1;
                return substring;
            }
        }
        return null;
    }
    
    protected int extractPrecisionOption() {
        final String option = this.extractOption();
        final boolean b = false;
        int int1 = 0;
        if (option == null) {
            return int1;
        }
        int1 = (b ? 1 : 0);
        try {
            final int n = int1 = Integer.parseInt(option);
            if (n <= 0) {
                int1 = n;
                LogLog.error(new StringBuffer().append("Precision option (").append(option).append(") isn't a positive integer.").toString());
                int1 = 0;
            }
            return int1;
        }
        catch (NumberFormatException ex) {
            LogLog.error(new StringBuffer().append("Category option \"").append(option).append("\" not a decimal integer.").toString(), ex);
            return int1;
        }
    }
    
    protected void finalizeConverter(final char c) {
        PatternConverter patternConverter = null;
        Label_0410: {
            switch (c) {
                default: {
                    LogLog.error(new StringBuffer().append("Unexpected char [").append(c).append("] at position ").append(this.i).append(" in conversion patterrn.").toString());
                    patternConverter = new LiteralPatternConverter(this.currentLiteral.toString());
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'c': {
                    patternConverter = new CategoryPatternConverter(this.formattingInfo, this.extractPrecisionOption());
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'C': {
                    patternConverter = new ClassNamePatternConverter(this.formattingInfo, this.extractPrecisionOption());
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'd': {
                    Serializable s = "ISO8601";
                    final String option = this.extractOption();
                    if (option != null) {
                        s = option;
                    }
                    Label_0271: {
                        if (((String)s).equalsIgnoreCase("ISO8601")) {
                            s = new ISO8601DateFormat();
                        }
                        else if (((String)s).equalsIgnoreCase("ABSOLUTE")) {
                            s = new AbsoluteTimeDateFormat();
                        }
                        else {
                            if (!((String)s).equalsIgnoreCase("DATE")) {
                                try {
                                    s = new SimpleDateFormat((String)s);
                                    break Label_0271;
                                }
                                catch (IllegalArgumentException ex) {
                                    LogLog.error(new StringBuffer().append("Could not instantiate SimpleDateFormat with ").append((String)s).toString(), ex);
                                    Class class$java$text$DateFormat;
                                    if (PatternParser.class$java$text$DateFormat == null) {
                                        class$java$text$DateFormat = (PatternParser.class$java$text$DateFormat = class$("java.text.DateFormat"));
                                    }
                                    else {
                                        class$java$text$DateFormat = PatternParser.class$java$text$DateFormat;
                                    }
                                    s = (DateFormat)OptionConverter.instantiateByClassName("org.apache.log4j.helpers.ISO8601DateFormat", class$java$text$DateFormat, null);
                                    break Label_0271;
                                }
                                break Label_0410;
                            }
                            s = new DateTimeDateFormat();
                        }
                    }
                    patternConverter = new DatePatternConverter(this.formattingInfo, (DateFormat)s);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'F': {
                    patternConverter = new LocationPatternConverter(this.formattingInfo, 1004);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'l': {
                    patternConverter = new LocationPatternConverter(this.formattingInfo, 1000);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'L': {
                    patternConverter = new LocationPatternConverter(this.formattingInfo, 1003);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'm': {
                    patternConverter = new BasicPatternConverter(this.formattingInfo, 2004);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'M': {
                    patternConverter = new LocationPatternConverter(this.formattingInfo, 1001);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'p': {
                    patternConverter = new BasicPatternConverter(this.formattingInfo, 2002);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'r': {
                    patternConverter = new BasicPatternConverter(this.formattingInfo, 2000);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 't': {
                    patternConverter = new BasicPatternConverter(this.formattingInfo, 2001);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'x': {
                    patternConverter = new BasicPatternConverter(this.formattingInfo, 2003);
                    this.currentLiteral.setLength(0);
                    break;
                }
                case 'X': {
                    patternConverter = new MDCPatternConverter(this.formattingInfo, this.extractOption());
                    this.currentLiteral.setLength(0);
                    break;
                }
            }
        }
        this.addConverter(patternConverter);
    }
    
    public PatternConverter parse() {
        this.i = 0;
        while (this.i < this.patternLength) {
            final char char1 = this.pattern.charAt(this.i++);
            switch (this.state) {
                default: {
                    continue;
                }
                case 0: {
                    if (this.i == this.patternLength) {
                        this.currentLiteral.append(char1);
                        continue;
                    }
                    if (char1 != '%') {
                        this.currentLiteral.append(char1);
                        continue;
                    }
                    switch (this.pattern.charAt(this.i)) {
                        default: {
                            if (this.currentLiteral.length() != 0) {
                                this.addToList(new LiteralPatternConverter(this.currentLiteral.toString()));
                            }
                            this.currentLiteral.setLength(0);
                            this.currentLiteral.append(char1);
                            this.state = 1;
                            this.formattingInfo.reset();
                            continue;
                        }
                        case '%': {
                            this.currentLiteral.append(char1);
                            ++this.i;
                            continue;
                        }
                        case 'n': {
                            this.currentLiteral.append(Layout.LINE_SEP);
                            ++this.i;
                            continue;
                        }
                    }
                    break;
                }
                case 1: {
                    this.currentLiteral.append(char1);
                    switch (char1) {
                        default: {
                            if (char1 >= '0' && char1 <= '9') {
                                this.formattingInfo.min = char1 - '0';
                                this.state = 4;
                                continue;
                            }
                            this.finalizeConverter(char1);
                            continue;
                        }
                        case '-': {
                            this.formattingInfo.leftAlign = true;
                            continue;
                        }
                        case '.': {
                            this.state = 3;
                            continue;
                        }
                    }
                    break;
                }
                case 4: {
                    this.currentLiteral.append(char1);
                    if (char1 >= '0' && char1 <= '9') {
                        this.formattingInfo.min = this.formattingInfo.min * 10 + (char1 - '0');
                        continue;
                    }
                    if (char1 == '.') {
                        this.state = 3;
                        continue;
                    }
                    this.finalizeConverter(char1);
                    continue;
                }
                case 3: {
                    this.currentLiteral.append(char1);
                    if (char1 >= '0' && char1 <= '9') {
                        this.formattingInfo.max = char1 - '0';
                        this.state = 5;
                        continue;
                    }
                    LogLog.error(new StringBuffer().append("Error occured in position ").append(this.i).append(".\n Was expecting digit, instead got char \"").append(char1).append("\".").toString());
                    this.state = 0;
                    continue;
                }
                case 5: {
                    this.currentLiteral.append(char1);
                    if (char1 >= '0' && char1 <= '9') {
                        this.formattingInfo.max = this.formattingInfo.max * 10 + (char1 - '0');
                        continue;
                    }
                    this.finalizeConverter(char1);
                    this.state = 0;
                    continue;
                }
            }
        }
        if (this.currentLiteral.length() != 0) {
            this.addToList(new LiteralPatternConverter(this.currentLiteral.toString()));
        }
        return this.head;
    }
    
    private static class BasicPatternConverter extends PatternConverter
    {
        int type;
        
        BasicPatternConverter(final FormattingInfo formattingInfo, final int type) {
            super(formattingInfo);
            this.type = type;
        }
        
        public String convert(final LoggingEvent loggingEvent) {
            switch (this.type) {
                default: {
                    return null;
                }
                case 2000: {
                    return Long.toString(loggingEvent.timeStamp - LoggingEvent.getStartTime());
                }
                case 2001: {
                    return loggingEvent.getThreadName();
                }
                case 2002: {
                    return loggingEvent.getLevel().toString();
                }
                case 2003: {
                    return loggingEvent.getNDC();
                }
                case 2004: {
                    return loggingEvent.getRenderedMessage();
                }
            }
        }
    }
    
    private class CategoryPatternConverter extends NamedPatternConverter
    {
        private final PatternParser this$0;
        
        CategoryPatternConverter(final PatternParser this$0, final FormattingInfo formattingInfo, final int n) {
            this.this$0 = this$0;
            super(formattingInfo, n);
        }
        
        @Override
        String getFullyQualifiedName(final LoggingEvent loggingEvent) {
            return loggingEvent.getLoggerName();
        }
    }
    
    private class ClassNamePatternConverter extends NamedPatternConverter
    {
        private final PatternParser this$0;
        
        ClassNamePatternConverter(final PatternParser this$0, final FormattingInfo formattingInfo, final int n) {
            this.this$0 = this$0;
            super(formattingInfo, n);
        }
        
        @Override
        String getFullyQualifiedName(final LoggingEvent loggingEvent) {
            return loggingEvent.getLocationInformation().getClassName();
        }
    }
    
    private static class DatePatternConverter extends PatternConverter
    {
        private Date date;
        private DateFormat df;
        
        DatePatternConverter(final FormattingInfo formattingInfo, final DateFormat df) {
            super(formattingInfo);
            this.date = new Date();
            this.df = df;
        }
        
        public String convert(final LoggingEvent loggingEvent) {
            this.date.setTime(loggingEvent.timeStamp);
            try {
                return this.df.format(this.date);
            }
            catch (Exception ex) {
                LogLog.error("Error occured while converting date.", ex);
                return null;
            }
        }
    }
    
    private static class LiteralPatternConverter extends PatternConverter
    {
        private String literal;
        
        LiteralPatternConverter(final String literal) {
            this.literal = literal;
        }
        
        public String convert(final LoggingEvent loggingEvent) {
            return this.literal;
        }
        
        @Override
        public final void format(final StringBuffer sb, final LoggingEvent loggingEvent) {
            sb.append(this.literal);
        }
    }
    
    private class LocationPatternConverter extends PatternConverter
    {
        private final PatternParser this$0;
        int type;
        
        LocationPatternConverter(final PatternParser this$0, final FormattingInfo formattingInfo, final int type) {
            this.this$0 = this$0;
            super(formattingInfo);
            this.type = type;
        }
        
        public String convert(final LoggingEvent loggingEvent) {
            final LocationInfo locationInformation = loggingEvent.getLocationInformation();
            switch (this.type) {
                default: {
                    return null;
                }
                case 1000: {
                    return locationInformation.fullInfo;
                }
                case 1001: {
                    return locationInformation.getMethodName();
                }
                case 1003: {
                    return locationInformation.getLineNumber();
                }
                case 1004: {
                    return locationInformation.getFileName();
                }
            }
        }
    }
    
    private static class MDCPatternConverter extends PatternConverter
    {
        private String key;
        
        MDCPatternConverter(final FormattingInfo formattingInfo, final String key) {
            super(formattingInfo);
            this.key = key;
        }
        
        public String convert(final LoggingEvent loggingEvent) {
            if (this.key == null) {
                final StringBuffer sb = new StringBuffer("{");
                final Map properties = loggingEvent.getProperties();
                if (properties.size() > 0) {
                    final Object[] array = properties.keySet().toArray();
                    Arrays.sort(array);
                    for (int i = 0; i < array.length; ++i) {
                        sb.append('{');
                        sb.append(array[i]);
                        sb.append(',');
                        sb.append(properties.get(array[i]));
                        sb.append('}');
                    }
                }
                sb.append('}');
                return sb.toString();
            }
            final Object mdc = loggingEvent.getMDC(this.key);
            if (mdc == null) {
                return null;
            }
            return mdc.toString();
        }
    }
    
    private abstract static class NamedPatternConverter extends PatternConverter
    {
        int precision;
        
        NamedPatternConverter(final FormattingInfo formattingInfo, final int precision) {
            super(formattingInfo);
            this.precision = precision;
        }
        
        public String convert(final LoggingEvent loggingEvent) {
            final String fullyQualifiedName = this.getFullyQualifiedName(loggingEvent);
            if (this.precision > 0) {
                final int length = fullyQualifiedName.length();
                int lastIndex = length - 1;
                for (int i = this.precision; i > 0; --i) {
                    lastIndex = fullyQualifiedName.lastIndexOf(46, lastIndex - 1);
                    if (lastIndex == -1) {
                        return fullyQualifiedName;
                    }
                }
                return fullyQualifiedName.substring(lastIndex + 1, length);
            }
            return fullyQualifiedName;
        }
        
        abstract String getFullyQualifiedName(final LoggingEvent p0);
    }
}
