package org.apache.log4j.helpers;

public class Transform
{
    private static final String CDATA_EMBEDED_END = "]]>]]&gt;<![CDATA[";
    private static final String CDATA_END = "]]>";
    private static final int CDATA_END_LEN;
    private static final String CDATA_PSEUDO_END = "]]&gt;";
    private static final String CDATA_START = "<![CDATA[";
    
    static {
        CDATA_END_LEN = "]]>".length();
    }
    
    public static void appendEscapingCDATA(final StringBuffer sb, final String s) {
        if (s != null) {
            int i = s.indexOf("]]>");
            if (i >= 0) {
                int n;
                for (n = 0; i > -1; i = s.indexOf("]]>", n)) {
                    sb.append(s.substring(n, i));
                    sb.append("]]>]]&gt;<![CDATA[");
                    n = i + Transform.CDATA_END_LEN;
                    if (n >= s.length()) {
                        return;
                    }
                }
                sb.append(s.substring(n));
                return;
            }
            sb.append(s);
        }
    }
    
    public static String escapeTags(final String s) {
        if (s == null || s.length() == 0 || (s.indexOf(34) == -1 && s.indexOf(38) == -1 && s.indexOf(60) == -1 && s.indexOf(62) == -1)) {
            return s;
        }
        final StringBuffer sb = new StringBuffer(s.length() + 6);
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 > '>') {
                sb.append(char1);
            }
            else if (char1 == '<') {
                sb.append("&lt;");
            }
            else if (char1 == '>') {
                sb.append("&gt;");
            }
            else if (char1 == '&') {
                sb.append("&amp;");
            }
            else if (char1 == '\"') {
                sb.append("&quot;");
            }
            else {
                sb.append(char1);
            }
        }
        return sb.toString();
    }
}
