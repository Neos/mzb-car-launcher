package org.apache.log4j.helpers;

import org.apache.log4j.*;
import org.apache.log4j.spi.*;
import java.util.*;

public class AppenderAttachableImpl implements AppenderAttachable
{
    protected Vector appenderList;
    
    @Override
    public void addAppender(final Appender appender) {
        if (appender != null) {
            if (this.appenderList == null) {
                this.appenderList = new Vector(1);
            }
            if (!this.appenderList.contains(appender)) {
                this.appenderList.addElement(appender);
            }
        }
    }
    
    public int appendLoopOnAppenders(final LoggingEvent loggingEvent) {
        int n = 0;
        if (this.appenderList != null) {
            final int size = this.appenderList.size();
            int n2 = 0;
            while (true) {
                n = size;
                if (n2 >= size) {
                    break;
                }
                ((Appender)this.appenderList.elementAt(n2)).doAppend(loggingEvent);
                ++n2;
            }
        }
        return n;
    }
    
    @Override
    public Enumeration getAllAppenders() {
        if (this.appenderList == null) {
            return null;
        }
        return this.appenderList.elements();
    }
    
    @Override
    public Appender getAppender(final String s) {
        if (this.appenderList != null && s != null) {
            for (int size = this.appenderList.size(), i = 0; i < size; ++i) {
                final Appender appender;
                if (s.equals((appender = this.appenderList.elementAt(i)).getName())) {
                    return appender;
                }
            }
            return null;
        }
        return null;
    }
    
    @Override
    public boolean isAttached(final Appender appender) {
        if (this.appenderList != null && appender != null) {
            for (int size = this.appenderList.size(), i = 0; i < size; ++i) {
                if (this.appenderList.elementAt(i) == appender) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public void removeAllAppenders() {
        if (this.appenderList != null) {
            for (int size = this.appenderList.size(), i = 0; i < size; ++i) {
                ((Appender)this.appenderList.elementAt(i)).close();
            }
            this.appenderList.removeAllElements();
            this.appenderList = null;
        }
    }
    
    @Override
    public void removeAppender(final String s) {
        if (s != null && this.appenderList != null) {
            for (int size = this.appenderList.size(), i = 0; i < size; ++i) {
                if (s.equals(((Appender)this.appenderList.elementAt(i)).getName())) {
                    this.appenderList.removeElementAt(i);
                    return;
                }
            }
        }
    }
    
    @Override
    public void removeAppender(final Appender appender) {
        if (appender == null || this.appenderList == null) {
            return;
        }
        this.appenderList.removeElement(appender);
    }
}
