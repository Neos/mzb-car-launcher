package org.apache.log4j.helpers;

import java.util.*;
import org.apache.log4j.spi.*;
import java.net.*;
import org.apache.log4j.*;
import java.io.*;
import java.lang.reflect.*;

public class OptionConverter
{
    static String DELIM_START;
    static int DELIM_START_LEN;
    static char DELIM_STOP;
    static int DELIM_STOP_LEN;
    static Class class$java$lang$String;
    static Class class$org$apache$log4j$Level;
    static Class class$org$apache$log4j$spi$Configurator;
    
    static {
        OptionConverter.DELIM_START = "${";
        OptionConverter.DELIM_STOP = '}';
        OptionConverter.DELIM_START_LEN = 2;
        OptionConverter.DELIM_STOP_LEN = 1;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static String[] concatanateArrays(final String[] array, final String[] array2) {
        final String[] array3 = new String[array.length + array2.length];
        System.arraycopy(array, 0, array3, 0, array.length);
        System.arraycopy(array2, 0, array3, array.length, array2.length);
        return array3;
    }
    
    public static String convertSpecialChars(final String s) {
        final int length = s.length();
        final StringBuffer sb = new StringBuffer(length);
        int i = 0;
        while (i < length) {
            final int n = i + 1;
            char c2;
            final char c = c2 = s.charAt(i);
            i = n;
            if (c == '\\') {
                i = n + 1;
                c2 = s.charAt(n);
                if (c2 == 'n') {
                    c2 = '\n';
                }
                else if (c2 == 'r') {
                    c2 = '\r';
                }
                else if (c2 == 't') {
                    c2 = '\t';
                }
                else if (c2 == 'f') {
                    c2 = '\f';
                }
                else if (c2 == '\b') {
                    c2 = '\b';
                }
                else if (c2 == '\"') {
                    c2 = '\"';
                }
                else if (c2 == '\'') {
                    c2 = '\'';
                }
                else if (c2 == '\\') {
                    c2 = '\\';
                }
            }
            sb.append(c2);
        }
        return sb.toString();
    }
    
    public static String findAndSubst(String property, final Properties properties) {
        property = properties.getProperty(property);
        if (property == null) {
            return null;
        }
        try {
            return substVars(property, properties);
        }
        catch (IllegalArgumentException ex) {
            LogLog.error(new StringBuffer().append("Bad option value [").append(property).append("].").toString(), ex);
            return property;
        }
    }
    
    public static String getSystemProperty(final String s, final String s2) {
        try {
            return System.getProperty(s, s2);
        }
        catch (Throwable t) {
            LogLog.debug(new StringBuffer().append("Was not allowed to read system property \"").append(s).append("\".").toString());
            return s2;
        }
    }
    
    public static Object instantiateByClassName(final String s, final Class clazz, final Object o) {
        if (s != null) {
            try {
                final Class loadClass = Loader.loadClass(s);
                if (!clazz.isAssignableFrom(loadClass)) {
                    LogLog.error(new StringBuffer().append("A \"").append(s).append("\" object is not assignable to a \"").append(clazz.getName()).append("\" variable.").toString());
                    LogLog.error(new StringBuffer().append("The class \"").append(clazz.getName()).append("\" was loaded by ").toString());
                    LogLog.error(new StringBuffer().append("[").append(clazz.getClassLoader()).append("] whereas object of type ").toString());
                    LogLog.error(new StringBuffer().append("\"").append(loadClass.getName()).append("\" was loaded by [").append(loadClass.getClassLoader()).append("].").toString());
                    return o;
                }
                return loadClass.newInstance();
            }
            catch (ClassNotFoundException ex) {
                LogLog.error(new StringBuffer().append("Could not instantiate class [").append(s).append("].").toString(), ex);
                return o;
            }
            catch (IllegalAccessException ex2) {
                LogLog.error(new StringBuffer().append("Could not instantiate class [").append(s).append("].").toString(), ex2);
                return o;
            }
            catch (InstantiationException ex3) {
                LogLog.error(new StringBuffer().append("Could not instantiate class [").append(s).append("].").toString(), ex3);
                return o;
            }
            catch (RuntimeException ex4) {
                LogLog.error(new StringBuffer().append("Could not instantiate class [").append(s).append("].").toString(), ex4);
            }
        }
        return o;
    }
    
    public static Object instantiateByKey(final Properties properties, final String s, final Class clazz, final Object o) {
        final String andSubst = findAndSubst(s, properties);
        if (andSubst == null) {
            LogLog.error(new StringBuffer().append("Could not find value for key ").append(s).toString());
            return o;
        }
        return instantiateByClassName(andSubst.trim(), clazz, o);
    }
    
    public static void selectAndConfigure(final InputStream inputStream, final String s, final LoggerRepository loggerRepository) {
        Configurator configurator;
        if (s != null) {
            LogLog.debug(new StringBuffer().append("Preferred configurator class: ").append(s).toString());
            Class class$org$apache$log4j$spi$Configurator;
            if (OptionConverter.class$org$apache$log4j$spi$Configurator == null) {
                class$org$apache$log4j$spi$Configurator = (OptionConverter.class$org$apache$log4j$spi$Configurator = class$("org.apache.log4j.spi.Configurator"));
            }
            else {
                class$org$apache$log4j$spi$Configurator = OptionConverter.class$org$apache$log4j$spi$Configurator;
            }
            if ((configurator = (Configurator)instantiateByClassName(s, class$org$apache$log4j$spi$Configurator, null)) == null) {
                LogLog.error(new StringBuffer().append("Could not instantiate configurator [").append(s).append("].").toString());
                return;
            }
        }
        else {
            configurator = new PropertyConfigurator();
        }
        configurator.doConfigure(inputStream, loggerRepository);
    }
    
    public static void selectAndConfigure(final URL url, final String s, final LoggerRepository loggerRepository) {
        final String file = url.getFile();
        String s2 = s;
        if (s == null) {
            s2 = s;
            if (file != null) {
                s2 = s;
                if (file.endsWith(".xml")) {
                    s2 = "org.apache.log4j.xml.DOMConfigurator";
                }
            }
        }
        Configurator configurator;
        if (s2 != null) {
            LogLog.debug(new StringBuffer().append("Preferred configurator class: ").append(s2).toString());
            Class class$org$apache$log4j$spi$Configurator;
            if (OptionConverter.class$org$apache$log4j$spi$Configurator == null) {
                class$org$apache$log4j$spi$Configurator = (OptionConverter.class$org$apache$log4j$spi$Configurator = class$("org.apache.log4j.spi.Configurator"));
            }
            else {
                class$org$apache$log4j$spi$Configurator = OptionConverter.class$org$apache$log4j$spi$Configurator;
            }
            if ((configurator = (Configurator)instantiateByClassName(s2, class$org$apache$log4j$spi$Configurator, null)) == null) {
                LogLog.error(new StringBuffer().append("Could not instantiate configurator [").append(s2).append("].").toString());
                return;
            }
        }
        else {
            configurator = new PropertyConfigurator();
        }
        configurator.doConfigure(url, loggerRepository);
    }
    
    public static String substVars(final String s, final Properties properties) throws IllegalArgumentException {
        final StringBuffer sb = new StringBuffer();
        int n = 0;
        while (true) {
            final int index = s.indexOf(OptionConverter.DELIM_START, n);
            if (index == -1) {
                if (n == 0) {
                    return s;
                }
                sb.append(s.substring(n, s.length()));
                return sb.toString();
            }
            else {
                sb.append(s.substring(n, index));
                final int index2 = s.indexOf(OptionConverter.DELIM_STOP, index);
                if (index2 == -1) {
                    throw new IllegalArgumentException(new StringBuffer().append('\"').append(s).append("\" has no closing brace. Opening brace at position ").append(index).append('.').toString());
                }
                final String substring = s.substring(index + OptionConverter.DELIM_START_LEN, index2);
                final String systemProperty = getSystemProperty(substring, null);
                String property;
                if ((property = systemProperty) == null) {
                    property = systemProperty;
                    if (properties != null) {
                        property = properties.getProperty(substring);
                    }
                }
                if (property != null) {
                    sb.append(substVars(property, properties));
                }
                n = index2 + OptionConverter.DELIM_STOP_LEN;
            }
        }
    }
    
    public static boolean toBoolean(String trim, final boolean b) {
        if (trim != null) {
            trim = trim.trim();
            if ("true".equalsIgnoreCase(trim)) {
                return true;
            }
            if ("false".equalsIgnoreCase(trim)) {
                return false;
            }
        }
        return b;
    }
    
    public static long toFileSize(final String s, final long n) {
        if (s != null) {
            final String upperCase = s.trim().toUpperCase();
            long n2 = 1L;
            final int index = upperCase.indexOf("KB");
            Label_0067: {
                if (index == -1) {
                    break Label_0067;
                }
                n2 = 1024L;
                String s2 = upperCase.substring(0, index);
                while (true) {
                    if (s2 == null) {
                        return n;
                    }
                    try {
                        return Long.valueOf(s2) * n2;
                        final int index2 = upperCase.indexOf("MB");
                        // iftrue(Label_0098:, index2 == -1)
                        n2 = 1048576L;
                        s2 = upperCase.substring(0, index2);
                        continue;
                        final int index3;
                        Label_0098: {
                            index3 = upperCase.indexOf("GB");
                        }
                        s2 = upperCase;
                        // iftrue(Label_0046:, index3 == -1)
                        n2 = 1073741824L;
                        s2 = upperCase.substring(0, index3);
                        continue;
                    }
                    catch (NumberFormatException ex) {
                        LogLog.error(new StringBuffer().append("[").append(s2).append("] is not in proper int form.").toString());
                        LogLog.error(new StringBuffer().append("[").append(s).append("] not in expected format.").toString(), ex);
                        return n;
                    }
                    break;
                }
            }
        }
        return n;
    }
    
    public static int toInt(String trim, final int n) {
        int intValue = n;
        if (trim == null) {
            return intValue;
        }
        trim = trim.trim();
        try {
            intValue = Integer.valueOf(trim);
            return intValue;
        }
        catch (NumberFormatException ex) {
            LogLog.error(new StringBuffer().append("[").append(trim).append("] is not in proper int form.").toString());
            ex.printStackTrace();
            return n;
        }
    }
    
    public static Level toLevel(String s, Level level) {
        if (s == null) {
            return level;
        }
        final String trim = s.trim();
        final int index = trim.indexOf(35);
        if (index == -1) {
            if ("NULL".equalsIgnoreCase(trim)) {
                return null;
            }
            return Level.toLevel(trim, level);
        }
        else {
            s = (String)level;
            final String substring = trim.substring(index + 1);
            final String substring2 = trim.substring(0, index);
            if ("NULL".equalsIgnoreCase(substring2)) {
                return null;
            }
            LogLog.debug(new StringBuffer().append("toLevel:class=[").append(substring).append("]").append(":pri=[").append(substring2).append("]").toString());
            try {
                final Class loadClass = Loader.loadClass(substring);
                Class class$java$lang$String;
                if (OptionConverter.class$java$lang$String == null) {
                    class$java$lang$String = (OptionConverter.class$java$lang$String = class$("java.lang.String"));
                }
                else {
                    class$java$lang$String = OptionConverter.class$java$lang$String;
                }
                Class class$org$apache$log4j$Level;
                if (OptionConverter.class$org$apache$log4j$Level == null) {
                    class$org$apache$log4j$Level = (OptionConverter.class$org$apache$log4j$Level = class$("org.apache.log4j.Level"));
                }
                else {
                    class$org$apache$log4j$Level = OptionConverter.class$org$apache$log4j$Level;
                }
                level = (Level)(s = (String)loadClass.getMethod("toLevel", class$java$lang$String, class$org$apache$log4j$Level).invoke(null, substring2, level));
            }
            catch (ClassNotFoundException ex6) {
                LogLog.warn(new StringBuffer().append("custom level class [").append(substring).append("] not found.").toString());
            }
            catch (NoSuchMethodException ex) {
                LogLog.warn(new StringBuffer().append("custom level class [").append(substring).append("]").append(" does not have a class function toLevel(String, Level)").toString(), ex);
            }
            catch (InvocationTargetException ex2) {
                if (ex2.getTargetException() instanceof InterruptedException || ex2.getTargetException() instanceof InterruptedIOException) {
                    Thread.currentThread().interrupt();
                }
                LogLog.warn(new StringBuffer().append("custom level class [").append(substring).append("]").append(" could not be instantiated").toString(), ex2);
            }
            catch (ClassCastException ex3) {
                LogLog.warn(new StringBuffer().append("class [").append(substring).append("] is not a subclass of org.apache.log4j.Level").toString(), ex3);
            }
            catch (IllegalAccessException ex4) {
                LogLog.warn(new StringBuffer().append("class [").append(substring).append("] cannot be instantiated due to access restrictions").toString(), ex4);
            }
            catch (RuntimeException ex5) {
                LogLog.warn(new StringBuffer().append("class [").append(substring).append("], level [").append(substring2).append("] conversion failed.").toString(), ex5);
            }
            return (Level)s;
        }
    }
}
