package org.apache.log4j.helpers;

import java.util.*;

public class NullEnumeration implements Enumeration
{
    private static final NullEnumeration instance;
    
    static {
        instance = new NullEnumeration();
    }
    
    public static NullEnumeration getInstance() {
        return NullEnumeration.instance;
    }
    
    @Override
    public boolean hasMoreElements() {
        return false;
    }
    
    @Override
    public Object nextElement() {
        throw new NoSuchElementException();
    }
}
