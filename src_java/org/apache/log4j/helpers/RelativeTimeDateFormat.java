package org.apache.log4j.helpers;

import java.util.*;
import java.text.*;

public class RelativeTimeDateFormat extends DateFormat
{
    private static final long serialVersionUID = 7055751607085611984L;
    protected final long startTime;
    
    public RelativeTimeDateFormat() {
        this.startTime = System.currentTimeMillis();
    }
    
    @Override
    public StringBuffer format(final Date date, final StringBuffer sb, final FieldPosition fieldPosition) {
        return sb.append(date.getTime() - this.startTime);
    }
    
    @Override
    public Date parse(final String s, final ParsePosition parsePosition) {
        return null;
    }
}
