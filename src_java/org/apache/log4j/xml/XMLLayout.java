package org.apache.log4j.xml;

import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import java.util.*;

public class XMLLayout extends Layout
{
    private final int DEFAULT_SIZE;
    private final int UPPER_LIMIT;
    private StringBuffer buf;
    private boolean locationInfo;
    private boolean properties;
    
    public XMLLayout() {
        this.DEFAULT_SIZE = 256;
        this.UPPER_LIMIT = 2048;
        this.buf = new StringBuffer(256);
        this.locationInfo = false;
        this.properties = false;
    }
    
    @Override
    public void activateOptions() {
    }
    
    @Override
    public String format(final LoggingEvent loggingEvent) {
        if (this.buf.capacity() > 2048) {
            this.buf = new StringBuffer(256);
        }
        else {
            this.buf.setLength(0);
        }
        this.buf.append("<log4j:event logger=\"");
        this.buf.append(Transform.escapeTags(loggingEvent.getLoggerName()));
        this.buf.append("\" timestamp=\"");
        this.buf.append(loggingEvent.timeStamp);
        this.buf.append("\" level=\"");
        this.buf.append(Transform.escapeTags(String.valueOf(loggingEvent.getLevel())));
        this.buf.append("\" thread=\"");
        this.buf.append(Transform.escapeTags(loggingEvent.getThreadName()));
        this.buf.append("\">\r\n");
        this.buf.append("<log4j:message><![CDATA[");
        Transform.appendEscapingCDATA(this.buf, loggingEvent.getRenderedMessage());
        this.buf.append("]]></log4j:message>\r\n");
        final String ndc = loggingEvent.getNDC();
        if (ndc != null) {
            this.buf.append("<log4j:NDC><![CDATA[");
            Transform.appendEscapingCDATA(this.buf, ndc);
            this.buf.append("]]></log4j:NDC>\r\n");
        }
        final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
        if (throwableStrRep != null) {
            this.buf.append("<log4j:throwable><![CDATA[");
            for (int i = 0; i < throwableStrRep.length; ++i) {
                Transform.appendEscapingCDATA(this.buf, throwableStrRep[i]);
                this.buf.append("\r\n");
            }
            this.buf.append("]]></log4j:throwable>\r\n");
        }
        if (this.locationInfo) {
            final LocationInfo locationInformation = loggingEvent.getLocationInformation();
            this.buf.append("<log4j:locationInfo class=\"");
            this.buf.append(Transform.escapeTags(locationInformation.getClassName()));
            this.buf.append("\" method=\"");
            this.buf.append(Transform.escapeTags(locationInformation.getMethodName()));
            this.buf.append("\" file=\"");
            this.buf.append(Transform.escapeTags(locationInformation.getFileName()));
            this.buf.append("\" line=\"");
            this.buf.append(locationInformation.getLineNumber());
            this.buf.append("\"/>\r\n");
        }
        if (this.properties) {
            final Set propertyKeySet = loggingEvent.getPropertyKeySet();
            if (propertyKeySet.size() > 0) {
                this.buf.append("<log4j:properties>\r\n");
                final Object[] array = propertyKeySet.toArray();
                Arrays.sort(array);
                for (int j = 0; j < array.length; ++j) {
                    final String string = array[j].toString();
                    final Object mdc = loggingEvent.getMDC(string);
                    if (mdc != null) {
                        this.buf.append("<log4j:data name=\"");
                        this.buf.append(Transform.escapeTags(string));
                        this.buf.append("\" value=\"");
                        this.buf.append(Transform.escapeTags(String.valueOf(mdc)));
                        this.buf.append("\"/>\r\n");
                    }
                }
                this.buf.append("</log4j:properties>\r\n");
            }
        }
        this.buf.append("</log4j:event>\r\n\r\n");
        return this.buf.toString();
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    public boolean getProperties() {
        return this.properties;
    }
    
    @Override
    public boolean ignoresThrowable() {
        return false;
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    public void setProperties(final boolean properties) {
        this.properties = properties;
    }
}
