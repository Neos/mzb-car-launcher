package org.apache.log4j.xml;

import org.w3c.dom.*;
import java.util.*;

public interface UnrecognizedElementHandler
{
    boolean parseUnrecognizedElement(final Element p0, final Properties p1) throws Exception;
}
