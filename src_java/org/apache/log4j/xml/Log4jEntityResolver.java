package org.apache.log4j.xml;

import org.xml.sax.*;
import org.apache.log4j.helpers.*;
import java.io.*;

public class Log4jEntityResolver implements EntityResolver
{
    private static final String PUBLIC_ID = "-//APACHE//DTD LOG4J 1.2//EN";
    
    @Override
    public InputSource resolveEntity(final String s, final String s2) {
        if (s2.endsWith("log4j.dtd") || "-//APACHE//DTD LOG4J 1.2//EN".equals(s)) {
            final Class<? extends Log4jEntityResolver> class1 = this.getClass();
            InputStream resourceAsStream;
            if ((resourceAsStream = class1.getResourceAsStream("/org/apache/log4j/xml/log4j.dtd")) == null) {
                LogLog.warn(new StringBuffer().append("Could not find [log4j.dtd] using [").append(class1.getClassLoader()).append("] class loader, parsed without DTD.").toString());
                resourceAsStream = new ByteArrayInputStream(new byte[0]);
            }
            return new InputSource(resourceAsStream);
        }
        return null;
    }
}
