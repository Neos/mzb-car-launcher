package org.apache.log4j.xml;

import org.apache.log4j.helpers.*;
import org.apache.log4j.*;

class XMLWatchdog extends FileWatchdog
{
    XMLWatchdog(final String s) {
        super(s);
    }
    
    public void doOnChange() {
        new DOMConfigurator().doConfigure(this.filename, LogManager.getLoggerRepository());
    }
}
