package org.apache.log4j.xml;

import java.util.*;
import java.net.*;
import javax.xml.parsers.*;
import org.apache.log4j.config.*;
import org.w3c.dom.*;
import org.apache.log4j.helpers.*;
import java.lang.reflect.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.or.*;
import org.xml.sax.*;
import java.io.*;
import org.apache.log4j.*;

public class DOMConfigurator implements Configurator
{
    static final String ADDITIVITY_ATTR = "additivity";
    static final String APPENDER_REF_TAG = "appender-ref";
    static final String APPENDER_TAG = "appender";
    static final String CATEGORY = "category";
    static final String CATEGORY_FACTORY_TAG = "categoryFactory";
    static final String CLASS_ATTR = "class";
    static final String CONFIGURATION_TAG = "log4j:configuration";
    static final String CONFIG_DEBUG_ATTR = "configDebug";
    static final String EMPTY_STR = "";
    static final String ERROR_HANDLER_TAG = "errorHandler";
    static final String FILTER_TAG = "filter";
    static final String INTERNAL_DEBUG_ATTR = "debug";
    static final String LAYOUT_TAG = "layout";
    static final String LEVEL_TAG = "level";
    static final String LOGGER = "logger";
    static final String LOGGER_FACTORY_TAG = "loggerFactory";
    static final String LOGGER_REF = "logger-ref";
    static final String NAME_ATTR = "name";
    static final String OLD_CONFIGURATION_TAG = "configuration";
    static final Class[] ONE_STRING_PARAM;
    static final String PARAM_TAG = "param";
    static final String PRIORITY_TAG = "priority";
    static final String REF_ATTR = "ref";
    static final String RENDERED_CLASS_ATTR = "renderedClass";
    static final String RENDERER_TAG = "renderer";
    static final String RENDERING_CLASS_ATTR = "renderingClass";
    private static final String RESET_ATTR = "reset";
    static final String ROOT_REF = "root-ref";
    static final String ROOT_TAG = "root";
    static final String THRESHOLD_ATTR = "threshold";
    private static final String THROWABLE_RENDERER_TAG = "throwableRenderer";
    static final String VALUE_ATTR = "value";
    static Class class$java$lang$String;
    static Class class$org$apache$log4j$spi$ErrorHandler;
    static Class class$org$apache$log4j$spi$Filter;
    static Class class$org$apache$log4j$spi$LoggerFactory;
    static final String dbfKey = "javax.xml.parsers.DocumentBuilderFactory";
    Hashtable appenderBag;
    protected LoggerFactory catFactory;
    Properties props;
    LoggerRepository repository;
    
    static {
        Class class$java$lang$String;
        if (DOMConfigurator.class$java$lang$String == null) {
            class$java$lang$String = (DOMConfigurator.class$java$lang$String = class$("java.lang.String"));
        }
        else {
            class$java$lang$String = DOMConfigurator.class$java$lang$String;
        }
        ONE_STRING_PARAM = new Class[] { class$java$lang$String };
    }
    
    public DOMConfigurator() {
        this.catFactory = null;
        this.appenderBag = new Hashtable();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void configure(final String s) throws FactoryConfigurationError {
        new DOMConfigurator().doConfigure(s, LogManager.getLoggerRepository());
    }
    
    public static void configure(final URL url) throws FactoryConfigurationError {
        new DOMConfigurator().doConfigure(url, LogManager.getLoggerRepository());
    }
    
    public static void configure(final Element element) {
        new DOMConfigurator().doConfigure(element, LogManager.getLoggerRepository());
    }
    
    public static void configureAndWatch(final String s) {
        configureAndWatch(s, 60000L);
    }
    
    public static void configureAndWatch(final String s, final long delay) {
        final XMLWatchdog xmlWatchdog = new XMLWatchdog(s);
        xmlWatchdog.setDelay(delay);
        xmlWatchdog.start();
    }
    
    private final void doConfigure(final ParseAction factoryConfigurationError, final LoggerRepository repository) throws FactoryConfigurationError {
        this.repository = repository;
        DocumentBuilderFactory instance;
        try {
            LogLog.debug(new StringBuffer().append("System property is :").append(OptionConverter.getSystemProperty("javax.xml.parsers.DocumentBuilderFactory", null)).toString());
            instance = DocumentBuilderFactory.newInstance();
            LogLog.debug("Standard DocumentBuilderFactory search succeded.");
            LogLog.debug(new StringBuffer().append("DocumentBuilderFactory is: ").append(instance.getClass().getName()).toString());
            final DocumentBuilderFactory documentBuilderFactory = instance;
            final boolean b = true;
            documentBuilderFactory.setValidating(b);
            final DocumentBuilderFactory documentBuilderFactory2 = instance;
            final DocumentBuilder documentBuilder = documentBuilderFactory2.newDocumentBuilder();
            final DocumentBuilder documentBuilder3;
            final DocumentBuilder documentBuilder2 = documentBuilder3 = documentBuilder;
            final SAXErrorHandler saxErrorHandler = new SAXErrorHandler();
            documentBuilder3.setErrorHandler(saxErrorHandler);
            final DocumentBuilder documentBuilder4 = documentBuilder2;
            final Log4jEntityResolver log4jEntityResolver = new Log4jEntityResolver();
            documentBuilder4.setEntityResolver(log4jEntityResolver);
            final DOMConfigurator domConfigurator = this;
            final FactoryConfigurationError factoryConfigurationError2 = factoryConfigurationError;
            final DocumentBuilder documentBuilder5 = documentBuilder2;
            final Document document = ((ParseAction)factoryConfigurationError2).parse(documentBuilder5);
            final Element element = document.getDocumentElement();
            domConfigurator.parse(element);
            return;
        }
        catch (FactoryConfigurationError factoryConfigurationError) {
            LogLog.debug("Could not instantiate a DocumentBuilderFactory.", factoryConfigurationError.getException());
            throw factoryConfigurationError;
        }
        try {
            final DocumentBuilderFactory documentBuilderFactory = instance;
            final boolean b = true;
            documentBuilderFactory.setValidating(b);
            final DocumentBuilderFactory documentBuilderFactory2 = instance;
            final DocumentBuilder documentBuilder = documentBuilderFactory2.newDocumentBuilder();
            final DocumentBuilder documentBuilder3;
            final DocumentBuilder documentBuilder2 = documentBuilder3 = documentBuilder;
            final SAXErrorHandler saxErrorHandler = new SAXErrorHandler();
            documentBuilder3.setErrorHandler(saxErrorHandler);
            final DocumentBuilder documentBuilder4 = documentBuilder2;
            final Log4jEntityResolver log4jEntityResolver = new Log4jEntityResolver();
            documentBuilder4.setEntityResolver(log4jEntityResolver);
            final DOMConfigurator domConfigurator = this;
            final FactoryConfigurationError factoryConfigurationError2 = factoryConfigurationError;
            final DocumentBuilder documentBuilder5 = documentBuilder2;
            final Document document = ((ParseAction)factoryConfigurationError2).parse(documentBuilder5);
            final Element element = document.getDocumentElement();
            domConfigurator.parse(element);
        }
        catch (Exception ex) {
            if (ex instanceof InterruptedException || ex instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            LogLog.error(new StringBuffer().append("Could not parse ").append(factoryConfigurationError.toString()).append(".").toString(), ex);
        }
    }
    
    public static Object parseElement(Element element, final Properties properties, final Class clazz) throws Exception {
        final Object instantiateByClassName = OptionConverter.instantiateByClassName(subst(element.getAttribute("class"), properties), clazz, null);
        Object o;
        if (instantiateByClassName != null) {
            final PropertySetter propertySetter = new PropertySetter(instantiateByClassName);
            final NodeList childNodes = element.getChildNodes();
            final int length = childNodes.getLength();
            int n = 0;
            while (true) {
                o = instantiateByClassName;
                if (n >= length) {
                    break;
                }
                final Node item = childNodes.item(n);
                if (item.getNodeType() == 1) {
                    element = (Element)item;
                    if (element.getTagName().equals("param")) {
                        setParameter(element, propertySetter, properties);
                    }
                    else {
                        parseUnrecognizedElement(instantiateByClassName, element, properties);
                    }
                }
                ++n;
            }
        }
        else {
            o = null;
        }
        return o;
    }
    
    private static void parseUnrecognizedElement(final Object o, final Element element, final Properties properties) throws Exception {
        boolean unrecognizedElement = false;
        if (o instanceof UnrecognizedElementHandler) {
            unrecognizedElement = ((UnrecognizedElementHandler)o).parseUnrecognizedElement(element, properties);
        }
        if (!unrecognizedElement) {
            LogLog.warn(new StringBuffer().append("Unrecognized element ").append(element.getNodeName()).toString());
        }
    }
    
    private static void quietParseUnrecognizedElement(final Object o, final Element element, final Properties properties) {
        try {
            parseUnrecognizedElement(o, element, properties);
        }
        catch (Exception ex) {
            if (ex instanceof InterruptedException || ex instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            LogLog.error("Error in extension content: ", ex);
        }
    }
    
    public static void setParameter(final Element element, final PropertySetter propertySetter, final Properties properties) {
        propertySetter.setProperty(subst(element.getAttribute("name"), properties), subst(OptionConverter.convertSpecialChars(element.getAttribute("value")), properties));
    }
    
    public static String subst(final String s, final Properties properties) {
        try {
            return OptionConverter.substVars(s, properties);
        }
        catch (IllegalArgumentException ex) {
            LogLog.warn("Could not perform variable substitution.", ex);
            return s;
        }
    }
    
    @Override
    public void doConfigure(final InputStream inputStream, final LoggerRepository loggerRepository) throws FactoryConfigurationError {
        this.doConfigure((ParseAction)new DOMConfigurator$3(this, inputStream), loggerRepository);
    }
    
    public void doConfigure(final Reader reader, final LoggerRepository loggerRepository) throws FactoryConfigurationError {
        this.doConfigure((ParseAction)new DOMConfigurator$4(this, reader), loggerRepository);
    }
    
    public void doConfigure(final String s, final LoggerRepository loggerRepository) {
        this.doConfigure((ParseAction)new DOMConfigurator$1(this, s), loggerRepository);
    }
    
    @Override
    public void doConfigure(final URL url, final LoggerRepository loggerRepository) {
        this.doConfigure((ParseAction)new DOMConfigurator$2(this, url), loggerRepository);
    }
    
    public void doConfigure(final Element element, final LoggerRepository repository) {
        this.repository = repository;
        this.parse(element);
    }
    
    protected void doConfigure(final InputSource inputSource, final LoggerRepository loggerRepository) throws FactoryConfigurationError {
        if (inputSource.getSystemId() == null) {
            inputSource.setSystemId("dummy://log4j.dtd");
        }
        this.doConfigure((ParseAction)new DOMConfigurator$5(this, inputSource), loggerRepository);
    }
    
    protected Appender findAppenderByName(final Document document, final String s) {
        final Appender appender = this.appenderBag.get(s);
        if (appender != null) {
            return appender;
        }
        final Element element = null;
        final NodeList elementsByTagName = document.getElementsByTagName("appender");
        int n = 0;
        Element element2;
        while (true) {
            element2 = element;
            if (n >= elementsByTagName.getLength()) {
                break;
            }
            final Node item = elementsByTagName.item(n);
            if (s.equals(item.getAttributes().getNamedItem("name").getNodeValue())) {
                element2 = (Element)item;
                break;
            }
            ++n;
        }
        if (element2 == null) {
            LogLog.error(new StringBuffer().append("No appender named [").append(s).append("] could be found.").toString());
            return null;
        }
        final Appender appender2 = this.parseAppender(element2);
        if (appender2 != null) {
            this.appenderBag.put(s, appender2);
        }
        return appender2;
    }
    
    protected Appender findAppenderByReference(final Element element) {
        return this.findAppenderByName(element.getOwnerDocument(), this.subst(element.getAttribute("ref")));
    }
    
    protected void parse(final Element element) {
        final String tagName = element.getTagName();
        if (!tagName.equals("log4j:configuration")) {
            if (!tagName.equals("configuration")) {
                LogLog.error("DOM element is - not a <log4j:configuration> element.");
                return;
            }
            LogLog.warn("The <configuration> element has been deprecated.");
            LogLog.warn("Use the <log4j:configuration> element instead.");
        }
        final String subst = this.subst(element.getAttribute("debug"));
        LogLog.debug(new StringBuffer().append("debug attribute= \"").append(subst).append("\".").toString());
        if (!subst.equals("") && !subst.equals("null")) {
            LogLog.setInternalDebugging(OptionConverter.toBoolean(subst, true));
        }
        else {
            LogLog.debug("Ignoring debug attribute.");
        }
        final String subst2 = this.subst(element.getAttribute("reset"));
        LogLog.debug(new StringBuffer().append("reset attribute= \"").append(subst2).append("\".").toString());
        if (!"".equals(subst2) && OptionConverter.toBoolean(subst2, false)) {
            this.repository.resetConfiguration();
        }
        final String subst3 = this.subst(element.getAttribute("configDebug"));
        if (!subst3.equals("") && !subst3.equals("null")) {
            LogLog.warn("The \"configDebug\" attribute is deprecated.");
            LogLog.warn("Use the \"debug\" attribute instead.");
            LogLog.setInternalDebugging(OptionConverter.toBoolean(subst3, true));
        }
        final String subst4 = this.subst(element.getAttribute("threshold"));
        LogLog.debug(new StringBuffer().append("Threshold =\"").append(subst4).append("\".").toString());
        if (!"".equals(subst4) && !"null".equals(subst4)) {
            this.repository.setThreshold(subst4);
        }
        final NodeList childNodes = element.getChildNodes();
        final int length = childNodes.getLength();
        for (int i = 0; i < length; ++i) {
            final Node item = childNodes.item(i);
            if (item.getNodeType() == 1) {
                final Element element2 = (Element)item;
                final String tagName2 = element2.getTagName();
                if (tagName2.equals("categoryFactory") || tagName2.equals("loggerFactory")) {
                    this.parseCategoryFactory(element2);
                }
            }
        }
        for (int j = 0; j < length; ++j) {
            final Node item2 = childNodes.item(j);
            if (item2.getNodeType() == 1) {
                final Element element3 = (Element)item2;
                final String tagName3 = element3.getTagName();
                if (tagName3.equals("category") || tagName3.equals("logger")) {
                    this.parseCategory(element3);
                }
                else if (tagName3.equals("root")) {
                    this.parseRoot(element3);
                }
                else if (tagName3.equals("renderer")) {
                    this.parseRenderer(element3);
                }
                else if (tagName3.equals("throwableRenderer")) {
                    if (this.repository instanceof ThrowableRendererSupport) {
                        final ThrowableRenderer throwableRenderer = this.parseThrowableRenderer(element3);
                        if (throwableRenderer != null) {
                            ((ThrowableRendererSupport)this.repository).setThrowableRenderer(throwableRenderer);
                        }
                    }
                }
                else if (!tagName3.equals("appender") && !tagName3.equals("categoryFactory") && !tagName3.equals("loggerFactory")) {
                    quietParseUnrecognizedElement(this.repository, element3, this.props);
                }
            }
        }
    }
    
    protected Appender parseAppender(final Element element) {
        Appender appender;
        PropertySetter propertySetter;
        while (true) {
            final String subst = this.subst(element.getAttribute("class"));
            LogLog.debug(new StringBuffer().append("Class name: [").append(subst).append(']').toString());
            while (true) {
                int n = 0;
                Label_0455: {
                    Object instance;
                    Element element2;
                    try {
                        instance = Loader.loadClass(subst).newInstance();
                        appender = (Appender)instance;
                        propertySetter = new PropertySetter(appender);
                        appender.setName(this.subst(element.getAttribute("name")));
                        final NodeList childNodes = element.getChildNodes();
                        final int length = childNodes.getLength();
                        n = 0;
                        if (n >= length) {
                            break;
                        }
                        final Node item = childNodes.item(n);
                        if (item.getNodeType() != 1) {
                            break Label_0455;
                        }
                        element2 = (Element)item;
                        if (element2.getTagName().equals("param")) {
                            this.setParameter(element2, propertySetter);
                            break Label_0455;
                        }
                        if (element2.getTagName().equals("layout")) {
                            appender.setLayout(this.parseLayout(element2));
                            break Label_0455;
                        }
                    }
                    catch (Exception ex) {
                        if (ex instanceof InterruptedException || ex instanceof InterruptedIOException) {
                            Thread.currentThread().interrupt();
                        }
                        LogLog.error("Could not create an Appender. Reported error follows.", ex);
                        return null;
                    }
                    if (element2.getTagName().equals("filter")) {
                        this.parseFilters(element2, appender);
                    }
                    else if (element2.getTagName().equals("errorHandler")) {
                        this.parseErrorHandler(element2, appender);
                    }
                    else if (element2.getTagName().equals("appender-ref")) {
                        final String subst2 = this.subst(element2.getAttribute("ref"));
                        if (appender instanceof AppenderAttachable) {
                            final AppenderAttachable appenderAttachable = (AppenderAttachable)appender;
                            LogLog.debug(new StringBuffer().append("Attaching appender named [").append(subst2).append("] to appender named [").append(appender.getName()).append("].").toString());
                            appenderAttachable.addAppender(this.findAppenderByReference(element2));
                        }
                        else {
                            LogLog.error(new StringBuffer().append("Requesting attachment of appender named [").append(subst2).append("] to appender named [").append(appender.getName()).append("] which does not implement org.apache.log4j.spi.AppenderAttachable.").toString());
                        }
                    }
                    else {
                        parseUnrecognizedElement(instance, element2, this.props);
                    }
                }
                ++n;
                continue;
            }
        }
        propertySetter.activate();
        return appender;
    }
    
    protected void parseCategory(final Element element) {
        final String subst = this.subst(element.getAttribute("name"));
        Object o = this.subst(element.getAttribute("class"));
        Label_0156: {
            if (!"".equals(o)) {
                break Label_0156;
            }
            LogLog.debug("Retreiving an instance of org.apache.log4j.Logger.");
            Label_0137: {
                if (this.catFactory != null) {
                    break Label_0137;
                }
                o = this.repository.getLogger(subst);
                while (true) {
                    synchronized (o) {
                        final boolean boolean1 = OptionConverter.toBoolean(this.subst(element.getAttribute("additivity")), true);
                        LogLog.debug(new StringBuffer().append("Setting [").append(((Category)o).getName()).append("] additivity to [").append(boolean1).append("].").toString());
                        ((Category)o).setAdditivity(boolean1);
                        this.parseChildrenOfLoggerElement(element, (Logger)o, false);
                        return;
                        LogLog.debug(new StringBuffer().append("Desired logger sub-class: [").append((String)o).append(']').toString());
                        try {
                            o = Loader.loadClass((String)o).getMethod("getLogger", (Class[])DOMConfigurator.ONE_STRING_PARAM).invoke(null, subst);
                        }
                        catch (InvocationTargetException ex) {
                            if (ex.getTargetException() instanceof InterruptedException || ex.getTargetException() instanceof InterruptedIOException) {
                                Thread.currentThread().interrupt();
                            }
                            LogLog.error(new StringBuffer().append("Could not retrieve category [").append(subst).append("]. Reported error follows.").toString(), ex);
                            return;
                        }
                        catch (Exception ex2) {
                            LogLog.error(new StringBuffer().append("Could not retrieve category [").append(subst).append("]. Reported error follows.").toString(), ex2);
                            return;
                        }
                        o = this.repository.getLogger(subst, this.catFactory);
                    }
                }
            }
        }
    }
    
    protected void parseCategoryFactory(final Element element) {
        final String subst = this.subst(element.getAttribute("class"));
        if ("".equals(subst)) {
            LogLog.error("Category Factory tag class attribute not found.");
            LogLog.debug("No Category Factory configured.");
        }
        else {
            LogLog.debug(new StringBuffer().append("Desired category factory: [").append(subst).append(']').toString());
            Class class$org$apache$log4j$spi$LoggerFactory;
            if (DOMConfigurator.class$org$apache$log4j$spi$LoggerFactory == null) {
                class$org$apache$log4j$spi$LoggerFactory = (DOMConfigurator.class$org$apache$log4j$spi$LoggerFactory = class$("org.apache.log4j.spi.LoggerFactory"));
            }
            else {
                class$org$apache$log4j$spi$LoggerFactory = DOMConfigurator.class$org$apache$log4j$spi$LoggerFactory;
            }
            final Object instantiateByClassName = OptionConverter.instantiateByClassName(subst, class$org$apache$log4j$spi$LoggerFactory, null);
            if (instantiateByClassName instanceof LoggerFactory) {
                this.catFactory = (LoggerFactory)instantiateByClassName;
            }
            else {
                LogLog.error(new StringBuffer().append("Category Factory class ").append(subst).append(" does not implement org.apache.log4j.LoggerFactory").toString());
            }
            final PropertySetter propertySetter = new PropertySetter(instantiateByClassName);
            final NodeList childNodes = element.getChildNodes();
            for (int length = childNodes.getLength(), i = 0; i < length; ++i) {
                final Node item = childNodes.item(i);
                if (item.getNodeType() == 1) {
                    final Element element2 = (Element)item;
                    if (element2.getTagName().equals("param")) {
                        this.setParameter(element2, propertySetter);
                    }
                    else {
                        quietParseUnrecognizedElement(instantiateByClassName, element2, this.props);
                    }
                }
            }
        }
    }
    
    protected void parseChildrenOfLoggerElement(final Element element, final Logger logger, final boolean b) {
        final PropertySetter propertySetter = new PropertySetter(logger);
        logger.removeAllAppenders();
        final NodeList childNodes = element.getChildNodes();
        for (int length = childNodes.getLength(), i = 0; i < length; ++i) {
            final Node item = childNodes.item(i);
            if (item.getNodeType() == 1) {
                final Element element2 = (Element)item;
                final String tagName = element2.getTagName();
                if (tagName.equals("appender-ref")) {
                    final Element element3 = (Element)item;
                    final Appender appenderByReference = this.findAppenderByReference(element3);
                    final String subst = this.subst(element3.getAttribute("ref"));
                    if (appenderByReference != null) {
                        LogLog.debug(new StringBuffer().append("Adding appender named [").append(subst).append("] to category [").append(logger.getName()).append("].").toString());
                    }
                    else {
                        LogLog.debug(new StringBuffer().append("Appender named [").append(subst).append("] not found.").toString());
                    }
                    logger.addAppender(appenderByReference);
                }
                else if (tagName.equals("level")) {
                    this.parseLevel(element2, logger, b);
                }
                else if (tagName.equals("priority")) {
                    this.parseLevel(element2, logger, b);
                }
                else if (tagName.equals("param")) {
                    this.setParameter(element2, propertySetter);
                }
                else {
                    quietParseUnrecognizedElement(logger, element2, this.props);
                }
            }
        }
        propertySetter.activate();
    }
    
    protected void parseErrorHandler(Element element, final Appender appender) {
        final String subst = this.subst(element.getAttribute("class"));
        Class class$org$apache$log4j$spi$ErrorHandler;
        if (DOMConfigurator.class$org$apache$log4j$spi$ErrorHandler == null) {
            class$org$apache$log4j$spi$ErrorHandler = (DOMConfigurator.class$org$apache$log4j$spi$ErrorHandler = class$("org.apache.log4j.spi.ErrorHandler"));
        }
        else {
            class$org$apache$log4j$spi$ErrorHandler = DOMConfigurator.class$org$apache$log4j$spi$ErrorHandler;
        }
        final org.apache.log4j.spi.ErrorHandler errorHandler = (org.apache.log4j.spi.ErrorHandler)OptionConverter.instantiateByClassName(subst, class$org$apache$log4j$spi$ErrorHandler, null);
        if (errorHandler != null) {
            errorHandler.setAppender(appender);
            final PropertySetter propertySetter = new PropertySetter(errorHandler);
            final NodeList childNodes = element.getChildNodes();
            for (int length = childNodes.getLength(), i = 0; i < length; ++i) {
                final Node item = childNodes.item(i);
                if (item.getNodeType() == 1) {
                    element = (Element)item;
                    final String tagName = element.getTagName();
                    if (tagName.equals("param")) {
                        this.setParameter(element, propertySetter);
                    }
                    else if (tagName.equals("appender-ref")) {
                        errorHandler.setBackupAppender(this.findAppenderByReference(element));
                    }
                    else if (tagName.equals("logger-ref")) {
                        final String attribute = element.getAttribute("ref");
                        Logger logger;
                        if (this.catFactory == null) {
                            logger = this.repository.getLogger(attribute);
                        }
                        else {
                            logger = this.repository.getLogger(attribute, this.catFactory);
                        }
                        errorHandler.setLogger(logger);
                    }
                    else if (tagName.equals("root-ref")) {
                        errorHandler.setLogger(this.repository.getRootLogger());
                    }
                    else {
                        quietParseUnrecognizedElement(errorHandler, element, this.props);
                    }
                }
            }
            propertySetter.activate();
            appender.setErrorHandler(errorHandler);
        }
    }
    
    protected void parseFilters(final Element element, final Appender appender) {
        final String subst = this.subst(element.getAttribute("class"));
        Class class$org$apache$log4j$spi$Filter;
        if (DOMConfigurator.class$org$apache$log4j$spi$Filter == null) {
            class$org$apache$log4j$spi$Filter = (DOMConfigurator.class$org$apache$log4j$spi$Filter = class$("org.apache.log4j.spi.Filter"));
        }
        else {
            class$org$apache$log4j$spi$Filter = DOMConfigurator.class$org$apache$log4j$spi$Filter;
        }
        final Filter filter = (Filter)OptionConverter.instantiateByClassName(subst, class$org$apache$log4j$spi$Filter, null);
        if (filter != null) {
            final PropertySetter propertySetter = new PropertySetter(filter);
            final NodeList childNodes = element.getChildNodes();
            for (int length = childNodes.getLength(), i = 0; i < length; ++i) {
                final Node item = childNodes.item(i);
                if (item.getNodeType() == 1) {
                    final Element element2 = (Element)item;
                    if (element2.getTagName().equals("param")) {
                        this.setParameter(element2, propertySetter);
                    }
                    else {
                        quietParseUnrecognizedElement(filter, element2, this.props);
                    }
                }
            }
            propertySetter.activate();
            LogLog.debug(new StringBuffer().append("Adding filter of type [").append(filter.getClass()).append("] to appender named [").append(appender.getName()).append("].").toString());
            appender.addFilter(filter);
        }
    }
    
    protected Layout parseLayout(final Element element) {
        Layout layout;
        PropertySetter propertySetter;
        while (true) {
            final String subst = this.subst(element.getAttribute("class"));
            LogLog.debug(new StringBuffer().append("Parsing layout of class: \"").append(subst).append("\"").toString());
            while (true) {
                int n = 0;
                Label_0198: {
                    try {
                        final Object instance = Loader.loadClass(subst).newInstance();
                        layout = (Layout)instance;
                        propertySetter = new PropertySetter(layout);
                        final NodeList childNodes = element.getChildNodes();
                        final int length = childNodes.getLength();
                        n = 0;
                        if (n < length) {
                            final Node item = childNodes.item(n);
                            if (item.getNodeType() != 1) {
                                break Label_0198;
                            }
                            final Element element2 = (Element)item;
                            if (element2.getTagName().equals("param")) {
                                this.setParameter(element2, propertySetter);
                                break Label_0198;
                            }
                            parseUnrecognizedElement(instance, element2, this.props);
                            break Label_0198;
                        }
                    }
                    catch (Exception ex) {
                        if (ex instanceof InterruptedException || ex instanceof InterruptedIOException) {
                            Thread.currentThread().interrupt();
                        }
                        LogLog.error("Could not create the Layout. Reported error follows.", ex);
                        return null;
                    }
                    break;
                }
                ++n;
                continue;
            }
        }
        propertySetter.activate();
        return layout;
    }
    
    protected void parseLevel(final Element element, final Logger logger, final boolean b) {
        String name = logger.getName();
        if (b) {
            name = "root";
        }
        final String subst = this.subst(element.getAttribute("value"));
        LogLog.debug(new StringBuffer().append("Level value for ").append(name).append(" is  [").append(subst).append("].").toString());
        if ("inherited".equalsIgnoreCase(subst) || "null".equalsIgnoreCase(subst)) {
            if (b) {
                LogLog.error("Root level cannot be inherited. Ignoring directive.");
            }
            else {
                logger.setLevel(null);
            }
        }
        else {
            final String subst2 = this.subst(element.getAttribute("class"));
            if ("".equals(subst2)) {
                logger.setLevel(OptionConverter.toLevel(subst, Level.DEBUG));
            }
            else {
                LogLog.debug(new StringBuffer().append("Desired Level sub-class: [").append(subst2).append(']').toString());
                try {
                    logger.setLevel((Level)Loader.loadClass(subst2).getMethod("toLevel", (Class[])DOMConfigurator.ONE_STRING_PARAM).invoke(null, subst));
                }
                catch (Exception ex) {
                    if (ex instanceof InterruptedException || ex instanceof InterruptedIOException) {
                        Thread.currentThread().interrupt();
                    }
                    LogLog.error(new StringBuffer().append("Could not create level [").append(subst).append("]. Reported error follows.").toString(), ex);
                    return;
                }
            }
        }
        LogLog.debug(new StringBuffer().append(name).append(" level set to ").append(logger.getLevel()).toString());
    }
    
    protected void parseRenderer(final Element element) {
        final String subst = this.subst(element.getAttribute("renderingClass"));
        final String subst2 = this.subst(element.getAttribute("renderedClass"));
        if (this.repository instanceof RendererSupport) {
            RendererMap.addRenderer((RendererSupport)this.repository, subst2, subst);
        }
    }
    
    protected void parseRoot(final Element element) {
        final Logger rootLogger = this.repository.getRootLogger();
        synchronized (rootLogger) {
            this.parseChildrenOfLoggerElement(element, rootLogger, true);
        }
    }
    
    protected ThrowableRenderer parseThrowableRenderer(final Element element) {
        ThrowableRenderer throwableRenderer;
        PropertySetter propertySetter;
        while (true) {
            final String subst = this.subst(element.getAttribute("class"));
            LogLog.debug(new StringBuffer().append("Parsing throwableRenderer of class: \"").append(subst).append("\"").toString());
            while (true) {
                int n = 0;
                Label_0198: {
                    try {
                        final Object instance = Loader.loadClass(subst).newInstance();
                        throwableRenderer = (ThrowableRenderer)instance;
                        propertySetter = new PropertySetter(throwableRenderer);
                        final NodeList childNodes = element.getChildNodes();
                        final int length = childNodes.getLength();
                        n = 0;
                        if (n < length) {
                            final Node item = childNodes.item(n);
                            if (item.getNodeType() != 1) {
                                break Label_0198;
                            }
                            final Element element2 = (Element)item;
                            if (element2.getTagName().equals("param")) {
                                this.setParameter(element2, propertySetter);
                                break Label_0198;
                            }
                            parseUnrecognizedElement(instance, element2, this.props);
                            break Label_0198;
                        }
                    }
                    catch (Exception ex) {
                        if (ex instanceof InterruptedException || ex instanceof InterruptedIOException) {
                            Thread.currentThread().interrupt();
                        }
                        LogLog.error("Could not create the ThrowableRenderer. Reported error follows.", ex);
                        return null;
                    }
                    break;
                }
                ++n;
                continue;
            }
        }
        propertySetter.activate();
        return throwableRenderer;
    }
    
    protected void setParameter(final Element element, final PropertySetter propertySetter) {
        propertySetter.setProperty(this.subst(element.getAttribute("name")), this.subst(OptionConverter.convertSpecialChars(element.getAttribute("value"))));
    }
    
    protected String subst(final String s) {
        return subst(s, this.props);
    }
    
    private interface ParseAction
    {
        Document parse(final DocumentBuilder p0) throws SAXException, IOException;
    }
}
