package org.apache.log4j.xml;

import org.xml.sax.*;
import org.apache.log4j.helpers.*;

public class SAXErrorHandler implements ErrorHandler
{
    private static void emitMessage(final String s, final SAXParseException ex) {
        LogLog.warn(new StringBuffer().append(s).append(ex.getLineNumber()).append(" and column ").append(ex.getColumnNumber()).toString());
        LogLog.warn(ex.getMessage(), ex.getException());
    }
    
    @Override
    public void error(final SAXParseException ex) {
        emitMessage("Continuable parsing error ", ex);
    }
    
    @Override
    public void fatalError(final SAXParseException ex) {
        emitMessage("Fatal parsing error ", ex);
    }
    
    @Override
    public void warning(final SAXParseException ex) {
        emitMessage("Parsing warning ", ex);
    }
}
