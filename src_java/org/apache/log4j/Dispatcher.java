package org.apache.log4j;

import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;

class Dispatcher extends Thread
{
    private AppenderAttachableImpl aai;
    private BoundedFIFO bf;
    AsyncAppender container;
    private boolean interrupted;
    
    Dispatcher(final BoundedFIFO bf, final AsyncAppender container) {
        this.interrupted = false;
        this.bf = bf;
        this.container = container;
        this.aai = container.aai;
        this.setDaemon(true);
        this.setPriority(1);
        this.setName(new StringBuffer().append("Dispatcher-").append(this.getName()).toString());
    }
    
    void close() {
        synchronized (this.bf) {
            this.interrupted = true;
            if (this.bf.length() == 0) {
                this.bf.notify();
            }
        }
    }
    
    @Override
    public void run() {
        while (true) {
            synchronized (this.bf) {
                // monitorexit(this.bf)
                Label_0041: {
                    if (this.bf.length() != 0) {
                        break Label_0041;
                    }
                    Label_0034: {
                        if (!this.interrupted) {
                            break Label_0034;
                        }
                        this.aai.removeAllAppenders();
                        return;
                        try {
                            this.bf.wait();
                            final LoggingEvent value = this.bf.get();
                            if (this.bf.wasFull()) {
                                this.bf.notify();
                            }
                            // monitorexit(this.bf)
                            final AppenderAttachableImpl aai = this.container.aai;
                            synchronized (this.bf) {
                                if (this.aai != null && value != null) {
                                    this.aai.appendLoopOnAppenders(value);
                                }
                            }
                        }
                        catch (InterruptedException ex) {}
                    }
                }
            }
        }
    }
}
