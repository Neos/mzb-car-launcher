package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.io.*;

public class ConsoleAppender extends WriterAppender
{
    public static final String SYSTEM_ERR = "System.err";
    public static final String SYSTEM_OUT = "System.out";
    private boolean follow;
    protected String target;
    
    public ConsoleAppender() {
        this.target = "System.out";
        this.follow = false;
    }
    
    public ConsoleAppender(final Layout layout) {
        this(layout, "System.out");
    }
    
    public ConsoleAppender(final Layout layout, final String target) {
        this.target = "System.out";
        this.follow = false;
        this.setLayout(layout);
        this.setTarget(target);
        this.activateOptions();
    }
    
    @Override
    public void activateOptions() {
        if (this.follow) {
            if (this.target.equals("System.err")) {
                this.setWriter(this.createWriter(new SystemErrStream()));
            }
            else {
                this.setWriter(this.createWriter(new SystemOutStream()));
            }
        }
        else if (this.target.equals("System.err")) {
            this.setWriter(this.createWriter(System.err));
        }
        else {
            this.setWriter(this.createWriter(System.out));
        }
        super.activateOptions();
    }
    
    @Override
    protected final void closeWriter() {
        if (this.follow) {
            super.closeWriter();
        }
    }
    
    public final boolean getFollow() {
        return this.follow;
    }
    
    public String getTarget() {
        return this.target;
    }
    
    public final void setFollow(final boolean follow) {
        this.follow = follow;
    }
    
    public void setTarget(final String s) {
        final String trim = s.trim();
        if ("System.out".equalsIgnoreCase(trim)) {
            this.target = "System.out";
            return;
        }
        if ("System.err".equalsIgnoreCase(trim)) {
            this.target = "System.err";
            return;
        }
        this.targetWarn(s);
    }
    
    void targetWarn(final String s) {
        LogLog.warn(new StringBuffer().append("[").append(s).append("] should be System.out or System.err.").toString());
        LogLog.warn("Using previously set target, System.out by default.");
    }
    
    private static class SystemErrStream extends OutputStream
    {
        @Override
        public void close() {
        }
        
        @Override
        public void flush() {
            System.err.flush();
        }
        
        @Override
        public void write(final int n) throws IOException {
            System.err.write(n);
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            System.err.write(array);
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            System.err.write(array, n, n2);
        }
    }
    
    private static class SystemOutStream extends OutputStream
    {
        @Override
        public void close() {
        }
        
        @Override
        public void flush() {
            System.out.flush();
        }
        
        @Override
        public void write(final int n) throws IOException {
            System.out.write(n);
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            System.out.write(array);
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            System.out.write(array, n, n2);
        }
    }
}
