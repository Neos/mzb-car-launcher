package org.apache.log4j;

import org.apache.log4j.pattern.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;

public class EnhancedPatternLayout extends Layout
{
    public static final String DEFAULT_CONVERSION_PATTERN = "%m%n";
    public static final String PATTERN_RULE_REGISTRY = "PATTERN_RULE_REGISTRY";
    public static final String TTCC_CONVERSION_PATTERN = "%r [%t] %p %c %x - %m%n";
    protected final int BUF_SIZE;
    protected final int MAX_CAPACITY;
    private String conversionPattern;
    private boolean handlesExceptions;
    private PatternConverter head;
    
    public EnhancedPatternLayout() {
        this("%m%n");
    }
    
    public EnhancedPatternLayout(final String conversionPattern) {
        this.BUF_SIZE = 256;
        this.MAX_CAPACITY = 1024;
        this.conversionPattern = conversionPattern;
        String s = conversionPattern;
        if (conversionPattern == null) {
            s = "%m%n";
        }
        this.head = this.createPatternParser(s).parse();
        if (this.head instanceof BridgePatternConverter) {
            this.handlesExceptions = !((BridgePatternConverter)this.head).ignoresThrowable();
            return;
        }
        this.handlesExceptions = false;
    }
    
    @Override
    public void activateOptions() {
    }
    
    protected PatternParser createPatternParser(final String s) {
        return new BridgePatternParser(s);
    }
    
    @Override
    public String format(final LoggingEvent loggingEvent) {
        final StringBuffer sb = new StringBuffer();
        for (PatternConverter patternConverter = this.head; patternConverter != null; patternConverter = patternConverter.next) {
            patternConverter.format(sb, loggingEvent);
        }
        return sb.toString();
    }
    
    public String getConversionPattern() {
        return this.conversionPattern;
    }
    
    @Override
    public boolean ignoresThrowable() {
        return !this.handlesExceptions;
    }
    
    public void setConversionPattern(final String s) {
        this.conversionPattern = OptionConverter.convertSpecialChars(s);
        this.head = this.createPatternParser(this.conversionPattern).parse();
        if (this.head instanceof BridgePatternConverter) {
            this.handlesExceptions = !((BridgePatternConverter)this.head).ignoresThrowable();
            return;
        }
        this.handlesExceptions = false;
    }
}
