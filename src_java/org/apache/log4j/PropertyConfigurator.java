package org.apache.log4j;

import java.net.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.config.*;
import java.io.*;
import java.util.*;
import org.apache.log4j.or.*;
import org.apache.log4j.spi.*;

public class PropertyConfigurator implements Configurator
{
    static final String ADDITIVITY_PREFIX = "log4j.additivity.";
    static final String APPENDER_PREFIX = "log4j.appender.";
    private static final String APPENDER_REF_TAG = "appender-ref";
    static final String CATEGORY_PREFIX = "log4j.category.";
    static final String FACTORY_PREFIX = "log4j.factory";
    private static final String INTERNAL_ROOT_NAME = "root";
    public static final String LOGGER_FACTORY_KEY = "log4j.loggerFactory";
    static final String LOGGER_PREFIX = "log4j.logger.";
    private static final String LOGGER_REF = "logger-ref";
    static final String RENDERER_PREFIX = "log4j.renderer.";
    private static final String RESET_KEY = "log4j.reset";
    static final String ROOT_CATEGORY_PREFIX = "log4j.rootCategory";
    static final String ROOT_LOGGER_PREFIX = "log4j.rootLogger";
    private static final String ROOT_REF = "root-ref";
    static final String THRESHOLD_PREFIX = "log4j.threshold";
    private static final String THROWABLE_RENDERER_PREFIX = "log4j.throwableRenderer";
    static Class class$org$apache$log4j$Appender;
    static Class class$org$apache$log4j$Layout;
    static Class class$org$apache$log4j$spi$ErrorHandler;
    static Class class$org$apache$log4j$spi$Filter;
    static Class class$org$apache$log4j$spi$LoggerFactory;
    static Class class$org$apache$log4j$spi$ThrowableRenderer;
    protected LoggerFactory loggerFactory;
    protected Hashtable registry;
    private LoggerRepository repository;
    
    public PropertyConfigurator() {
        this.registry = new Hashtable(11);
        this.loggerFactory = new DefaultCategoryFactory();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void configure(final InputStream inputStream) {
        new PropertyConfigurator().doConfigure(inputStream, LogManager.getLoggerRepository());
    }
    
    public static void configure(final String s) {
        new PropertyConfigurator().doConfigure(s, LogManager.getLoggerRepository());
    }
    
    public static void configure(final URL url) {
        new PropertyConfigurator().doConfigure(url, LogManager.getLoggerRepository());
    }
    
    public static void configure(final Properties properties) {
        new PropertyConfigurator().doConfigure(properties, LogManager.getLoggerRepository());
    }
    
    public static void configureAndWatch(final String s) {
        configureAndWatch(s, 60000L);
    }
    
    public static void configureAndWatch(final String s, final long delay) {
        final PropertyWatchdog propertyWatchdog = new PropertyWatchdog(s);
        propertyWatchdog.setDelay(delay);
        propertyWatchdog.start();
    }
    
    private void parseErrorHandler(final ErrorHandler errorHandler, String andSubst, final Properties properties, final LoggerRepository loggerRepository) {
        if (OptionConverter.toBoolean(OptionConverter.findAndSubst(new StringBuffer().append(andSubst).append("root-ref").toString(), properties), false)) {
            errorHandler.setLogger(loggerRepository.getRootLogger());
        }
        final String andSubst2 = OptionConverter.findAndSubst(new StringBuffer().append(andSubst).append("logger-ref").toString(), properties);
        if (andSubst2 != null) {
            Logger logger;
            if (this.loggerFactory == null) {
                logger = loggerRepository.getLogger(andSubst2);
            }
            else {
                logger = loggerRepository.getLogger(andSubst2, this.loggerFactory);
            }
            errorHandler.setLogger(logger);
        }
        andSubst = OptionConverter.findAndSubst(new StringBuffer().append(andSubst).append("appender-ref").toString(), properties);
        if (andSubst != null) {
            final Appender appender = this.parseAppender(properties, andSubst);
            if (appender != null) {
                errorHandler.setBackupAppender(appender);
            }
        }
    }
    
    protected void configureLoggerFactory(final Properties properties) {
        final String andSubst = OptionConverter.findAndSubst("log4j.loggerFactory", properties);
        if (andSubst != null) {
            LogLog.debug(new StringBuffer().append("Setting category factory to [").append(andSubst).append("].").toString());
            Class class$org$apache$log4j$spi$LoggerFactory;
            if (PropertyConfigurator.class$org$apache$log4j$spi$LoggerFactory == null) {
                class$org$apache$log4j$spi$LoggerFactory = (PropertyConfigurator.class$org$apache$log4j$spi$LoggerFactory = class$("org.apache.log4j.spi.LoggerFactory"));
            }
            else {
                class$org$apache$log4j$spi$LoggerFactory = PropertyConfigurator.class$org$apache$log4j$spi$LoggerFactory;
            }
            PropertySetter.setProperties(this.loggerFactory = (LoggerFactory)OptionConverter.instantiateByClassName(andSubst, class$org$apache$log4j$spi$LoggerFactory, this.loggerFactory), properties, "log4j.factory.");
        }
    }
    
    void configureRootCategory(final Properties properties, LoggerRepository rootLogger) {
        String s = "log4j.rootLogger";
        String s2;
        if ((s2 = OptionConverter.findAndSubst("log4j.rootLogger", properties)) == null) {
            s2 = OptionConverter.findAndSubst("log4j.rootCategory", properties);
            s = "log4j.rootCategory";
        }
        if (s2 == null) {
            LogLog.debug("Could not find root logger information. Is this OK?");
            return;
        }
        rootLogger = (LoggerRepository)rootLogger.getRootLogger();
        synchronized (rootLogger) {
            this.parseCategory(properties, (Logger)rootLogger, s, "root", s2);
        }
    }
    
    @Override
    public void doConfigure(final InputStream inputStream, final LoggerRepository loggerRepository) {
        final Properties properties = new Properties();
        try {
            properties.load(inputStream);
            this.doConfigure(properties, loggerRepository);
        }
        catch (IOException ex) {
            if (ex instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            LogLog.error(new StringBuffer().append("Could not read configuration file from InputStream [").append(inputStream).append("].").toString(), ex);
            LogLog.error(new StringBuffer().append("Ignoring configuration InputStream [").append(inputStream).append("].").toString());
        }
    }
    
    public void doConfigure(final String p0, final LoggerRepository p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/util/Properties.<init>:()V
        //     7: astore          6
        //     9: aconst_null    
        //    10: astore_3       
        //    11: aconst_null    
        //    12: astore          5
        //    14: new             Ljava/io/FileInputStream;
        //    17: dup            
        //    18: aload_1        
        //    19: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //    22: astore          4
        //    24: aload           6
        //    26: aload           4
        //    28: invokevirtual   java/util/Properties.load:(Ljava/io/InputStream;)V
        //    31: aload           4
        //    33: invokevirtual   java/io/FileInputStream.close:()V
        //    36: aload           4
        //    38: ifnull          46
        //    41: aload           4
        //    43: invokevirtual   java/io/FileInputStream.close:()V
        //    46: aload_0        
        //    47: aload           6
        //    49: aload_2        
        //    50: invokevirtual   org/apache/log4j/PropertyConfigurator.doConfigure:(Ljava/util/Properties;Lorg/apache/log4j/spi/LoggerRepository;)V
        //    53: return         
        //    54: astore_1       
        //    55: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //    58: invokevirtual   java/lang/Thread.interrupt:()V
        //    61: goto            46
        //    64: astore          4
        //    66: aload           5
        //    68: astore_2       
        //    69: aload_2        
        //    70: astore_3       
        //    71: aload           4
        //    73: instanceof      Ljava/io/InterruptedIOException;
        //    76: ifne            89
        //    79: aload_2        
        //    80: astore_3       
        //    81: aload           4
        //    83: instanceof      Ljava/lang/InterruptedException;
        //    86: ifeq            97
        //    89: aload_2        
        //    90: astore_3       
        //    91: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //    94: invokevirtual   java/lang/Thread.interrupt:()V
        //    97: aload_2        
        //    98: astore_3       
        //    99: new             Ljava/lang/StringBuffer;
        //   102: dup            
        //   103: invokespecial   java/lang/StringBuffer.<init>:()V
        //   106: ldc_w           "Could not read configuration file ["
        //   109: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   112: aload_1        
        //   113: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   116: ldc             "]."
        //   118: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   121: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   124: aload           4
        //   126: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   129: aload_2        
        //   130: astore_3       
        //   131: new             Ljava/lang/StringBuffer;
        //   134: dup            
        //   135: invokespecial   java/lang/StringBuffer.<init>:()V
        //   138: ldc_w           "Ignoring configuration file ["
        //   141: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   144: aload_1        
        //   145: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   148: ldc             "]."
        //   150: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   153: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   156: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;)V
        //   159: aload_2        
        //   160: ifnull          53
        //   163: aload_2        
        //   164: invokevirtual   java/io/FileInputStream.close:()V
        //   167: return         
        //   168: astore_1       
        //   169: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   172: invokevirtual   java/lang/Thread.interrupt:()V
        //   175: return         
        //   176: astore_1       
        //   177: aload_3        
        //   178: ifnull          185
        //   181: aload_3        
        //   182: invokevirtual   java/io/FileInputStream.close:()V
        //   185: aload_1        
        //   186: athrow         
        //   187: astore_2       
        //   188: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   191: invokevirtual   java/lang/Thread.interrupt:()V
        //   194: goto            185
        //   197: astore_1       
        //   198: goto            46
        //   201: astore_1       
        //   202: return         
        //   203: astore_2       
        //   204: goto            185
        //   207: astore_1       
        //   208: aload           4
        //   210: astore_3       
        //   211: goto            177
        //   214: astore_3       
        //   215: aload           4
        //   217: astore_2       
        //   218: aload_3        
        //   219: astore          4
        //   221: goto            69
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  14     24     64     69     Ljava/lang/Exception;
        //  14     24     176    177    Any
        //  24     36     214    224    Ljava/lang/Exception;
        //  24     36     207    214    Any
        //  41     46     54     64     Ljava/io/InterruptedIOException;
        //  41     46     197    201    Ljava/lang/Throwable;
        //  71     79     176    177    Any
        //  81     89     176    177    Any
        //  91     97     176    177    Any
        //  99     129    176    177    Any
        //  131    159    176    177    Any
        //  163    167    168    176    Ljava/io/InterruptedIOException;
        //  163    167    201    203    Ljava/lang/Throwable;
        //  181    185    187    197    Ljava/io/InterruptedIOException;
        //  181    185    203    207    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0046:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void doConfigure(final URL p0, final LoggerRepository p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/util/Properties.<init>:()V
        //     7: astore          7
        //     9: new             Ljava/lang/StringBuffer;
        //    12: dup            
        //    13: invokespecial   java/lang/StringBuffer.<init>:()V
        //    16: ldc_w           "Reading configuration from URL "
        //    19: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    22: aload_1        
        //    23: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //    26: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    29: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //    32: aconst_null    
        //    33: astore          6
        //    35: aconst_null    
        //    36: astore          5
        //    38: aload           5
        //    40: astore          4
        //    42: aload           6
        //    44: astore_3       
        //    45: aload_1        
        //    46: invokevirtual   java/net/URL.openConnection:()Ljava/net/URLConnection;
        //    49: astore          8
        //    51: aload           5
        //    53: astore          4
        //    55: aload           6
        //    57: astore_3       
        //    58: aload           8
        //    60: iconst_0       
        //    61: invokevirtual   java/net/URLConnection.setUseCaches:(Z)V
        //    64: aload           5
        //    66: astore          4
        //    68: aload           6
        //    70: astore_3       
        //    71: aload           8
        //    73: invokevirtual   java/net/URLConnection.getInputStream:()Ljava/io/InputStream;
        //    76: astore          5
        //    78: aload           5
        //    80: astore          4
        //    82: aload           5
        //    84: astore_3       
        //    85: aload           7
        //    87: aload           5
        //    89: invokevirtual   java/util/Properties.load:(Ljava/io/InputStream;)V
        //    92: aload           5
        //    94: ifnull          102
        //    97: aload           5
        //    99: invokevirtual   java/io/InputStream.close:()V
        //   102: aload_0        
        //   103: aload           7
        //   105: aload_2        
        //   106: invokevirtual   org/apache/log4j/PropertyConfigurator.doConfigure:(Ljava/util/Properties;Lorg/apache/log4j/spi/LoggerRepository;)V
        //   109: return         
        //   110: astore_1       
        //   111: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   114: invokevirtual   java/lang/Thread.interrupt:()V
        //   117: goto            102
        //   120: astore_2       
        //   121: aload           4
        //   123: astore_3       
        //   124: aload_2        
        //   125: instanceof      Ljava/io/InterruptedIOException;
        //   128: ifne            141
        //   131: aload           4
        //   133: astore_3       
        //   134: aload_2        
        //   135: instanceof      Ljava/lang/InterruptedException;
        //   138: ifeq            150
        //   141: aload           4
        //   143: astore_3       
        //   144: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   147: invokevirtual   java/lang/Thread.interrupt:()V
        //   150: aload           4
        //   152: astore_3       
        //   153: new             Ljava/lang/StringBuffer;
        //   156: dup            
        //   157: invokespecial   java/lang/StringBuffer.<init>:()V
        //   160: ldc_w           "Could not read configuration file from URL ["
        //   163: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   166: aload_1        
        //   167: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   170: ldc             "]."
        //   172: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   175: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   178: aload_2        
        //   179: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   182: aload           4
        //   184: astore_3       
        //   185: new             Ljava/lang/StringBuffer;
        //   188: dup            
        //   189: invokespecial   java/lang/StringBuffer.<init>:()V
        //   192: ldc_w           "Ignoring configuration file ["
        //   195: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   198: aload_1        
        //   199: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   202: ldc             "]."
        //   204: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   207: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   210: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;)V
        //   213: aload           4
        //   215: ifnull          109
        //   218: aload           4
        //   220: invokevirtual   java/io/InputStream.close:()V
        //   223: return         
        //   224: astore_1       
        //   225: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   228: invokevirtual   java/lang/Thread.interrupt:()V
        //   231: return         
        //   232: astore_1       
        //   233: aload_3        
        //   234: ifnull          241
        //   237: aload_3        
        //   238: invokevirtual   java/io/InputStream.close:()V
        //   241: aload_1        
        //   242: athrow         
        //   243: astore_2       
        //   244: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   247: invokevirtual   java/lang/Thread.interrupt:()V
        //   250: goto            241
        //   253: astore_1       
        //   254: goto            102
        //   257: astore_1       
        //   258: goto            102
        //   261: astore_1       
        //   262: return         
        //   263: astore_1       
        //   264: return         
        //   265: astore_2       
        //   266: goto            241
        //   269: astore_2       
        //   270: goto            241
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  45     51     120    265    Ljava/lang/Exception;
        //  45     51     232    273    Any
        //  58     64     120    265    Ljava/lang/Exception;
        //  58     64     232    273    Any
        //  71     78     120    265    Ljava/lang/Exception;
        //  71     78     232    273    Any
        //  85     92     120    265    Ljava/lang/Exception;
        //  85     92     232    273    Any
        //  97     102    110    120    Ljava/io/InterruptedIOException;
        //  97     102    253    257    Ljava/io/IOException;
        //  97     102    257    261    Ljava/lang/RuntimeException;
        //  124    131    232    273    Any
        //  134    141    232    273    Any
        //  144    150    232    273    Any
        //  153    182    232    273    Any
        //  185    213    232    273    Any
        //  218    223    224    232    Ljava/io/InterruptedIOException;
        //  218    223    261    263    Ljava/io/IOException;
        //  218    223    263    265    Ljava/lang/RuntimeException;
        //  237    241    243    253    Ljava/io/InterruptedIOException;
        //  237    241    265    269    Ljava/io/IOException;
        //  237    241    269    273    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 132, Size: 132
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3569)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void doConfigure(final Properties properties, final LoggerRepository repository) {
        this.repository = repository;
        String property;
        if ((property = properties.getProperty("log4j.debug")) == null) {
            final String property2 = properties.getProperty("log4j.configDebug");
            if ((property = property2) != null) {
                LogLog.warn("[log4j.configDebug] is deprecated. Use [log4j.debug] instead.");
                property = property2;
            }
        }
        if (property != null) {
            LogLog.setInternalDebugging(OptionConverter.toBoolean(property, true));
        }
        final String property3 = properties.getProperty("log4j.reset");
        if (property3 != null && OptionConverter.toBoolean(property3, false)) {
            repository.resetConfiguration();
        }
        final String andSubst = OptionConverter.findAndSubst("log4j.threshold", properties);
        if (andSubst != null) {
            repository.setThreshold(OptionConverter.toLevel(andSubst, Level.ALL));
            LogLog.debug(new StringBuffer().append("Hierarchy threshold set to [").append(repository.getThreshold()).append("].").toString());
        }
        this.configureRootCategory(properties, repository);
        this.configureLoggerFactory(properties);
        this.parseCatsAndRenderers(properties, repository);
        LogLog.debug("Finished configuring.");
        this.registry.clear();
    }
    
    void parseAdditivityForLogger(final Properties properties, final Logger logger, final String s) {
        final String andSubst = OptionConverter.findAndSubst(new StringBuffer().append("log4j.additivity.").append(s).toString(), properties);
        LogLog.debug(new StringBuffer().append("Handling log4j.additivity.").append(s).append("=[").append(andSubst).append("]").toString());
        if (andSubst != null && !andSubst.equals("")) {
            final boolean boolean1 = OptionConverter.toBoolean(andSubst, true);
            LogLog.debug(new StringBuffer().append("Setting additivity for \"").append(s).append("\" to ").append(boolean1).toString());
            logger.setAdditivity(boolean1);
        }
    }
    
    Appender parseAppender(final Properties properties, final String name) {
        final Appender registryGet = this.registryGet(name);
        if (registryGet != null) {
            LogLog.debug(new StringBuffer().append("Appender \"").append(name).append("\" was already parsed.").toString());
            return registryGet;
        }
        final String string = new StringBuffer().append("log4j.appender.").append(name).toString();
        final String string2 = new StringBuffer().append(string).append(".layout").toString();
        Class class$org$apache$log4j$Appender;
        if (PropertyConfigurator.class$org$apache$log4j$Appender == null) {
            class$org$apache$log4j$Appender = (PropertyConfigurator.class$org$apache$log4j$Appender = class$("org.apache.log4j.Appender"));
        }
        else {
            class$org$apache$log4j$Appender = PropertyConfigurator.class$org$apache$log4j$Appender;
        }
        final Appender appender = (Appender)OptionConverter.instantiateByKey(properties, string, class$org$apache$log4j$Appender, null);
        if (appender == null) {
            LogLog.error(new StringBuffer().append("Could not instantiate appender named \"").append(name).append("\".").toString());
            return null;
        }
        appender.setName(name);
        if (appender instanceof OptionHandler) {
            if (appender.requiresLayout()) {
                Class class$org$apache$log4j$Layout;
                if (PropertyConfigurator.class$org$apache$log4j$Layout == null) {
                    class$org$apache$log4j$Layout = (PropertyConfigurator.class$org$apache$log4j$Layout = class$("org.apache.log4j.Layout"));
                }
                else {
                    class$org$apache$log4j$Layout = PropertyConfigurator.class$org$apache$log4j$Layout;
                }
                final Layout layout = (Layout)OptionConverter.instantiateByKey(properties, string2, class$org$apache$log4j$Layout, null);
                if (layout != null) {
                    appender.setLayout(layout);
                    LogLog.debug(new StringBuffer().append("Parsing layout options for \"").append(name).append("\".").toString());
                    PropertySetter.setProperties(layout, properties, new StringBuffer().append(string2).append(".").toString());
                    LogLog.debug(new StringBuffer().append("End of parsing for \"").append(name).append("\".").toString());
                }
            }
            final String string3 = new StringBuffer().append(string).append(".errorhandler").toString();
            if (OptionConverter.findAndSubst(string3, properties) != null) {
                Class class$org$apache$log4j$spi$ErrorHandler;
                if (PropertyConfigurator.class$org$apache$log4j$spi$ErrorHandler == null) {
                    class$org$apache$log4j$spi$ErrorHandler = (PropertyConfigurator.class$org$apache$log4j$spi$ErrorHandler = class$("org.apache.log4j.spi.ErrorHandler"));
                }
                else {
                    class$org$apache$log4j$spi$ErrorHandler = PropertyConfigurator.class$org$apache$log4j$spi$ErrorHandler;
                }
                final ErrorHandler errorHandler = (ErrorHandler)OptionConverter.instantiateByKey(properties, string3, class$org$apache$log4j$spi$ErrorHandler, null);
                if (errorHandler != null) {
                    appender.setErrorHandler(errorHandler);
                    LogLog.debug(new StringBuffer().append("Parsing errorhandler options for \"").append(name).append("\".").toString());
                    this.parseErrorHandler(errorHandler, string3, properties, this.repository);
                    final Properties properties2 = new Properties();
                    final String[] array = { new StringBuffer().append(string3).append(".").append("root-ref").toString(), new StringBuffer().append(string3).append(".").append("logger-ref").toString(), new StringBuffer().append(string3).append(".").append("appender-ref").toString() };
                    for (final Map.Entry<Object, Object> entry : properties.entrySet()) {
                        int n;
                        for (n = 0; n < array.length && !array[n].equals(entry.getKey()); ++n) {}
                        if (n == array.length) {
                            ((Hashtable<Object, V>)properties2).put(entry.getKey(), entry.getValue());
                        }
                    }
                    PropertySetter.setProperties(errorHandler, properties2, new StringBuffer().append(string3).append(".").toString());
                    LogLog.debug(new StringBuffer().append("End of errorhandler parsing for \"").append(name).append("\".").toString());
                }
            }
            PropertySetter.setProperties(appender, properties, new StringBuffer().append(string).append(".").toString());
            LogLog.debug(new StringBuffer().append("Parsed \"").append(name).append("\" options.").toString());
        }
        this.parseAppenderFilters(properties, name, appender);
        this.registryPut(appender);
        return appender;
    }
    
    void parseAppenderFilters(final Properties properties, String substring, final Appender appender) {
        final String string = new StringBuffer().append("log4j.appender.").append(substring).append(".filter.").toString();
        final int length = string.length();
        final Hashtable<Object, Vector<NameValue>> hashtable = new Hashtable<Object, Vector<NameValue>>();
        final Enumeration<String> keys = (Enumeration<String>)properties.keys();
        String s = "";
        while (keys.hasMoreElements()) {
            final String s2 = keys.nextElement();
            if (s2.startsWith(string)) {
                final int index = s2.indexOf(46, length);
                String substring2 = s2;
                substring = s;
                if (index != -1) {
                    substring2 = s2.substring(0, index);
                    substring = s2.substring(index + 1);
                }
                Vector<NameValue> vector;
                if ((vector = hashtable.get(substring2)) == null) {
                    vector = new Vector<NameValue>();
                    hashtable.put(substring2, vector);
                }
                s = substring;
                if (index == -1) {
                    continue;
                }
                vector.add(new NameValue(substring, OptionConverter.findAndSubst(s2, properties)));
                s = substring;
            }
        }
        final SortedKeyEnumeration sortedKeyEnumeration = new SortedKeyEnumeration(hashtable);
        while (sortedKeyEnumeration.hasMoreElements()) {
            final String s3 = sortedKeyEnumeration.nextElement();
            final String property = properties.getProperty(s3);
            if (property != null) {
                LogLog.debug(new StringBuffer().append("Filter key: [").append(s3).append("] class: [").append(properties.getProperty(s3)).append("] props: ").append(hashtable.get(s3)).toString());
                Class class$org$apache$log4j$spi$Filter;
                if (PropertyConfigurator.class$org$apache$log4j$spi$Filter == null) {
                    class$org$apache$log4j$spi$Filter = (PropertyConfigurator.class$org$apache$log4j$spi$Filter = class$("org.apache.log4j.spi.Filter"));
                }
                else {
                    class$org$apache$log4j$spi$Filter = PropertyConfigurator.class$org$apache$log4j$spi$Filter;
                }
                final Filter filter = (Filter)OptionConverter.instantiateByClassName(property, class$org$apache$log4j$spi$Filter, null);
                if (filter == null) {
                    continue;
                }
                final PropertySetter propertySetter = new PropertySetter(filter);
                final Enumeration<NameValue> elements = hashtable.get(s3).elements();
                while (elements.hasMoreElements()) {
                    final NameValue nameValue = elements.nextElement();
                    propertySetter.setProperty(nameValue.key, nameValue.value);
                }
                propertySetter.activate();
                LogLog.debug(new StringBuffer().append("Adding filter of type [").append(filter.getClass()).append("] to appender named [").append(appender.getName()).append("].").toString());
                appender.addFilter(filter);
            }
            else {
                LogLog.warn(new StringBuffer().append("Missing class definition for filter: [").append(s3).append("]").toString());
            }
        }
    }
    
    void parseCategory(final Properties properties, final Logger logger, final String s, String trim, String nextToken) {
        LogLog.debug(new StringBuffer().append("Parsing for [").append(trim).append("] with value=[").append(nextToken).append("].").toString());
        final StringTokenizer stringTokenizer = new StringTokenizer(nextToken, ",");
        Label_0193: {
            if (nextToken.startsWith(",") || nextToken.equals("")) {
                break Label_0193;
            }
            if (stringTokenizer.hasMoreTokens()) {
                nextToken = stringTokenizer.nextToken();
                LogLog.debug(new StringBuffer().append("Level token is [").append(nextToken).append("].").toString());
                if ("inherited".equalsIgnoreCase(nextToken) || "null".equalsIgnoreCase(nextToken)) {
                    if (trim.equals("root")) {
                        LogLog.warn("The root logger cannot be set to null.");
                    }
                    else {
                        logger.setLevel(null);
                    }
                }
                else {
                    logger.setLevel(OptionConverter.toLevel(nextToken, Level.DEBUG));
                }
                LogLog.debug(new StringBuffer().append("Category ").append(trim).append(" set to ").append(logger.getLevel()).toString());
                break Label_0193;
            }
            return;
        }
        logger.removeAllAppenders();
        while (stringTokenizer.hasMoreTokens()) {
            trim = stringTokenizer.nextToken().trim();
            if (trim != null && !trim.equals(",")) {
                LogLog.debug(new StringBuffer().append("Parsing appender named \"").append(trim).append("\".").toString());
                final Appender appender = this.parseAppender(properties, trim);
                if (appender == null) {
                    continue;
                }
                logger.addAppender(appender);
            }
        }
    }
    
    protected void parseCatsAndRenderers(final Properties properties, final LoggerRepository loggerRepository) {
        final Enumeration<?> propertyNames = properties.propertyNames();
    Label_0071_Outer:
        while (propertyNames.hasMoreElements()) {
            final String s = (String)propertyNames.nextElement();
            if (s.startsWith("log4j.category.") || s.startsWith("log4j.logger.")) {
                String s2 = null;
                while (true) {
                    Label_0127: {
                        if (!s.startsWith("log4j.category.")) {
                            break Label_0127;
                        }
                        s2 = s.substring("log4j.category.".length());
                        final String andSubst = OptionConverter.findAndSubst(s, properties);
                        final Logger logger = loggerRepository.getLogger(s2, this.loggerFactory);
                        synchronized (logger) {
                            this.parseCategory(properties, logger, s, s2, andSubst);
                            this.parseAdditivityForLogger(properties, logger, s2);
                            continue Label_0071_Outer;
                        }
                    }
                    if (s.startsWith("log4j.logger.")) {
                        s2 = s.substring("log4j.logger.".length());
                    }
                    continue;
                }
            }
            else if (s.startsWith("log4j.renderer.")) {
                final String substring = s.substring("log4j.renderer.".length());
                final String andSubst2 = OptionConverter.findAndSubst(s, properties);
                if (!(loggerRepository instanceof RendererSupport)) {
                    continue;
                }
                RendererMap.addRenderer((RendererSupport)loggerRepository, substring, andSubst2);
            }
            else {
                if (!s.equals("log4j.throwableRenderer") || !(loggerRepository instanceof ThrowableRendererSupport)) {
                    continue;
                }
                Class class$org$apache$log4j$spi$ThrowableRenderer;
                if (PropertyConfigurator.class$org$apache$log4j$spi$ThrowableRenderer == null) {
                    class$org$apache$log4j$spi$ThrowableRenderer = (PropertyConfigurator.class$org$apache$log4j$spi$ThrowableRenderer = class$("org.apache.log4j.spi.ThrowableRenderer"));
                }
                else {
                    class$org$apache$log4j$spi$ThrowableRenderer = PropertyConfigurator.class$org$apache$log4j$spi$ThrowableRenderer;
                }
                final ThrowableRenderer throwableRenderer = (ThrowableRenderer)OptionConverter.instantiateByKey(properties, "log4j.throwableRenderer", class$org$apache$log4j$spi$ThrowableRenderer, null);
                if (throwableRenderer == null) {
                    LogLog.error("Could not instantiate throwableRenderer.");
                }
                else {
                    new PropertySetter(throwableRenderer).setProperties(properties, "log4j.throwableRenderer.");
                    ((ThrowableRendererSupport)loggerRepository).setThrowableRenderer(throwableRenderer);
                }
            }
        }
    }
    
    Appender registryGet(final String s) {
        return this.registry.get(s);
    }
    
    void registryPut(final Appender appender) {
        this.registry.put(appender.getName(), appender);
    }
}
