package org.apache.log4j.config;

import java.io.*;
import java.util.*;
import org.apache.log4j.*;

public class PropertyPrinter implements PropertyCallback
{
    protected Hashtable appenderNames;
    protected boolean doCapitalize;
    protected Hashtable layoutNames;
    protected int numAppenders;
    protected PrintWriter out;
    
    public PropertyPrinter(final PrintWriter printWriter) {
        this(printWriter, false);
    }
    
    public PropertyPrinter(final PrintWriter out, final boolean doCapitalize) {
        this.numAppenders = 0;
        this.appenderNames = new Hashtable();
        this.layoutNames = new Hashtable();
        this.out = out;
        this.doCapitalize = doCapitalize;
        this.print(out);
        out.flush();
    }
    
    public static String capitalize(final String s) {
        String string = s;
        if (Character.isLowerCase(s.charAt(0))) {
            if (s.length() != 1) {
                string = s;
                if (!Character.isLowerCase(s.charAt(1))) {
                    return string;
                }
            }
            final StringBuffer sb = new StringBuffer(s);
            sb.setCharAt(0, Character.toUpperCase(s.charAt(0)));
            string = sb.toString();
        }
        return string;
    }
    
    public static void main(final String[] array) {
        new PropertyPrinter(new PrintWriter(System.out));
    }
    
    @Override
    public void foundProperty(final Object o, final String s, final String s2, final Object o2) {
        if (o instanceof Appender && "name".equals(s2)) {
            return;
        }
        String capitalize = s2;
        if (this.doCapitalize) {
            capitalize = capitalize(s2);
        }
        this.out.println(new StringBuffer().append(s).append(capitalize).append("=").append(o2.toString()).toString());
    }
    
    protected String genAppName() {
        return new StringBuffer().append("A").append(this.numAppenders++).toString();
    }
    
    protected boolean isGenAppName(final String s) {
        if (s.length() >= 2 && s.charAt(0) == 'A') {
            for (int i = 0; i < s.length(); ++i) {
                if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public void print(final PrintWriter printWriter) {
        this.printOptions(printWriter, Logger.getRootLogger());
        final Enumeration currentLoggers = LogManager.getCurrentLoggers();
        while (currentLoggers.hasMoreElements()) {
            this.printOptions(printWriter, currentLoggers.nextElement());
        }
    }
    
    protected void printOptions(final PrintWriter printWriter, final Object o, final String s) {
        printWriter.println(new StringBuffer().append(s).append("=").append(o.getClass().getName()).toString());
        PropertyGetter.getProperties(o, (PropertyGetter.PropertyCallback)this, new StringBuffer().append(s).append(".").toString());
    }
    
    protected void printOptions(final PrintWriter printWriter, final Category category) {
        final Enumeration allAppenders = category.getAllAppenders();
        final Level level = category.getLevel();
        String s;
        if (level == null) {
            s = "";
        }
        else {
            s = level.toString();
        }
        while (allAppenders.hasMoreElements()) {
            final Appender appender = allAppenders.nextElement();
            String s2;
            if ((s2 = this.appenderNames.get(appender)) == null) {
                final String name = appender.getName();
                String genAppName = null;
                Label_0096: {
                    if (name != null) {
                        genAppName = name;
                        if (!this.isGenAppName(name)) {
                            break Label_0096;
                        }
                    }
                    genAppName = this.genAppName();
                }
                this.appenderNames.put(appender, genAppName);
                this.printOptions(printWriter, appender, new StringBuffer().append("log4j.appender.").append(genAppName).toString());
                s2 = genAppName;
                if (appender.getLayout() != null) {
                    this.printOptions(printWriter, appender.getLayout(), new StringBuffer().append("log4j.appender.").append(genAppName).append(".layout").toString());
                    s2 = genAppName;
                }
            }
            s = new StringBuffer().append(s).append(", ").append(s2).toString();
        }
        String string;
        if (category == Logger.getRootLogger()) {
            string = "log4j.rootLogger";
        }
        else {
            string = new StringBuffer().append("log4j.logger.").append(category.getName()).toString();
        }
        if (s != "") {
            printWriter.println(new StringBuffer().append(string).append("=").append(s).toString());
        }
        if (!category.getAdditivity() && category != Logger.getRootLogger()) {
            printWriter.println(new StringBuffer().append("log4j.additivity.").append(category.getName()).append("=false").toString());
        }
    }
    
    protected void printOptions(final PrintWriter printWriter, final Logger logger) {
        this.printOptions(printWriter, (Category)logger);
    }
}
