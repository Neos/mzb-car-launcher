package org.apache.log4j.config;

import java.beans.*;
import org.apache.log4j.helpers.*;
import java.io.*;
import java.lang.reflect.*;

public class PropertyGetter
{
    protected static final Object[] NULL_ARG;
    static Class class$java$lang$String;
    static Class class$org$apache$log4j$Priority;
    protected Object obj;
    protected PropertyDescriptor[] props;
    
    static {
        NULL_ARG = new Object[0];
    }
    
    public PropertyGetter(final Object obj) throws IntrospectionException {
        this.props = Introspector.getBeanInfo(obj.getClass()).getPropertyDescriptors();
        this.obj = obj;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void getProperties(final Object o, final PropertyCallback propertyCallback, final String s) {
        try {
            new PropertyGetter(o).getProperties(propertyCallback, s);
        }
        catch (IntrospectionException ex) {
            LogLog.error(new StringBuffer().append("Failed to introspect object ").append(o).toString(), ex);
        }
    }
    
    public void getProperties(final PropertyCallback propertyCallback, final String s) {
        for (int i = 0; i < this.props.length; ++i) {
            final Method readMethod = this.props[i].getReadMethod();
            if (readMethod != null && this.isHandledType(readMethod.getReturnType())) {
                final String name = this.props[i].getName();
                try {
                    final Object invoke = readMethod.invoke(this.obj, PropertyGetter.NULL_ARG);
                    if (invoke != null) {
                        propertyCallback.foundProperty(this.obj, s, name, invoke);
                    }
                }
                catch (IllegalAccessException ex2) {
                    LogLog.warn(new StringBuffer().append("Failed to get value of property ").append(name).toString());
                }
                catch (InvocationTargetException ex) {
                    if (ex.getTargetException() instanceof InterruptedException || ex.getTargetException() instanceof InterruptedIOException) {
                        Thread.currentThread().interrupt();
                    }
                    LogLog.warn(new StringBuffer().append("Failed to get value of property ").append(name).toString());
                }
                catch (RuntimeException ex3) {
                    LogLog.warn(new StringBuffer().append("Failed to get value of property ").append(name).toString());
                }
            }
        }
    }
    
    protected boolean isHandledType(final Class clazz) {
        Class class$java$lang$String;
        if (PropertyGetter.class$java$lang$String == null) {
            class$java$lang$String = (PropertyGetter.class$java$lang$String = class$("java.lang.String"));
        }
        else {
            class$java$lang$String = PropertyGetter.class$java$lang$String;
        }
        if (!class$java$lang$String.isAssignableFrom(clazz) && !Integer.TYPE.isAssignableFrom(clazz) && !Long.TYPE.isAssignableFrom(clazz) && !Boolean.TYPE.isAssignableFrom(clazz)) {
            Class class$org$apache$log4j$Priority;
            if (PropertyGetter.class$org$apache$log4j$Priority == null) {
                class$org$apache$log4j$Priority = (PropertyGetter.class$org$apache$log4j$Priority = class$("org.apache.log4j.Priority"));
            }
            else {
                class$org$apache$log4j$Priority = PropertyGetter.class$org$apache$log4j$Priority;
            }
            if (!class$org$apache$log4j$Priority.isAssignableFrom(clazz)) {
                return false;
            }
        }
        return true;
    }
    
    public interface PropertyCallback
    {
        void foundProperty(final Object p0, final String p1, final String p2, final Object p3);
    }
}
