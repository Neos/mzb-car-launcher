package org.apache.log4j.config;

public class PropertySetterException extends Exception
{
    private static final long serialVersionUID = -1352613734254235861L;
    protected Throwable rootCause;
    
    public PropertySetterException(final String s) {
        super(s);
    }
    
    public PropertySetterException(final Throwable rootCause) {
        this.rootCause = rootCause;
    }
    
    @Override
    public String getMessage() {
        String s2;
        final String s = s2 = super.getMessage();
        if (s == null) {
            s2 = s;
            if (this.rootCause != null) {
                s2 = this.rootCause.getMessage();
            }
        }
        return s2;
    }
}
