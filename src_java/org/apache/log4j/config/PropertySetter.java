package org.apache.log4j.config;

import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;
import java.beans.*;
import org.apache.log4j.*;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;

public class PropertySetter
{
    static Class class$java$lang$String;
    static Class class$org$apache$log4j$Priority;
    static Class class$org$apache$log4j$spi$ErrorHandler;
    static Class class$org$apache$log4j$spi$OptionHandler;
    protected Object obj;
    protected PropertyDescriptor[] props;
    
    public PropertySetter(final Object obj) {
        this.obj = obj;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void setProperties(final Object o, final Properties properties, final String s) {
        new PropertySetter(o).setProperties(properties, s);
    }
    
    public void activate() {
        if (this.obj instanceof OptionHandler) {
            ((OptionHandler)this.obj).activateOptions();
        }
    }
    
    protected Object convertArg(String s, final Class clazz) {
        if (s == null) {
            s = null;
        }
        else {
            final String trim = s.trim();
            Class class$java$lang$String;
            if (PropertySetter.class$java$lang$String == null) {
                class$java$lang$String = (PropertySetter.class$java$lang$String = class$("java.lang.String"));
            }
            else {
                class$java$lang$String = PropertySetter.class$java$lang$String;
            }
            if (!class$java$lang$String.isAssignableFrom(clazz)) {
                if (Integer.TYPE.isAssignableFrom(clazz)) {
                    return new Integer(trim);
                }
                if (Long.TYPE.isAssignableFrom(clazz)) {
                    return new Long(trim);
                }
                if (Boolean.TYPE.isAssignableFrom(clazz)) {
                    if ("true".equalsIgnoreCase(trim)) {
                        return Boolean.TRUE;
                    }
                    if ("false".equalsIgnoreCase(trim)) {
                        return Boolean.FALSE;
                    }
                }
                else {
                    Class class$org$apache$log4j$Priority;
                    if (PropertySetter.class$org$apache$log4j$Priority == null) {
                        class$org$apache$log4j$Priority = (PropertySetter.class$org$apache$log4j$Priority = class$("org.apache.log4j.Priority"));
                    }
                    else {
                        class$org$apache$log4j$Priority = PropertySetter.class$org$apache$log4j$Priority;
                    }
                    if (class$org$apache$log4j$Priority.isAssignableFrom(clazz)) {
                        return OptionConverter.toLevel(trim, Level.DEBUG);
                    }
                    Class class$org$apache$log4j$spi$ErrorHandler;
                    if (PropertySetter.class$org$apache$log4j$spi$ErrorHandler == null) {
                        class$org$apache$log4j$spi$ErrorHandler = (PropertySetter.class$org$apache$log4j$spi$ErrorHandler = class$("org.apache.log4j.spi.ErrorHandler"));
                    }
                    else {
                        class$org$apache$log4j$spi$ErrorHandler = PropertySetter.class$org$apache$log4j$spi$ErrorHandler;
                    }
                    if (class$org$apache$log4j$spi$ErrorHandler.isAssignableFrom(clazz)) {
                        Class class$org$apache$log4j$spi$ErrorHandler2;
                        if (PropertySetter.class$org$apache$log4j$spi$ErrorHandler == null) {
                            class$org$apache$log4j$spi$ErrorHandler2 = (PropertySetter.class$org$apache$log4j$spi$ErrorHandler = class$("org.apache.log4j.spi.ErrorHandler"));
                        }
                        else {
                            class$org$apache$log4j$spi$ErrorHandler2 = PropertySetter.class$org$apache$log4j$spi$ErrorHandler;
                        }
                        return OptionConverter.instantiateByClassName(trim, class$org$apache$log4j$spi$ErrorHandler2, null);
                    }
                }
                return null;
            }
        }
        return s;
    }
    
    protected PropertyDescriptor getPropertyDescriptor(final String s) {
        if (this.props == null) {
            this.introspect();
        }
        for (int i = 0; i < this.props.length; ++i) {
            if (s.equals(this.props[i].getName())) {
                return this.props[i];
            }
        }
        return null;
    }
    
    protected void introspect() {
        try {
            this.props = Introspector.getBeanInfo(this.obj.getClass()).getPropertyDescriptors();
        }
        catch (IntrospectionException ex) {
            LogLog.error(new StringBuffer().append("Failed to introspect ").append(this.obj).append(": ").append(ex.getMessage()).toString());
            this.props = new PropertyDescriptor[0];
        }
    }
    
    public void setProperties(final Properties properties, final String s) {
        final int length = s.length();
        final Enumeration<?> propertyNames = properties.propertyNames();
        while (propertyNames.hasMoreElements()) {
            final String s2 = (String)propertyNames.nextElement();
            if (s2.startsWith(s) && s2.indexOf(46, length + 1) <= 0) {
                final String andSubst = OptionConverter.findAndSubst(s2, properties);
                final String substring = s2.substring(length);
                if (("layout".equals(substring) || "errorhandler".equals(substring)) && this.obj instanceof Appender) {
                    continue;
                }
                final PropertyDescriptor propertyDescriptor = this.getPropertyDescriptor(Introspector.decapitalize(substring));
                if (propertyDescriptor != null) {
                    if (PropertySetter.class$org$apache$log4j$spi$OptionHandler != null) {
                        goto Label_0300;
                    }
                    if ((PropertySetter.class$org$apache$log4j$spi$OptionHandler = class$("org.apache.log4j.spi.OptionHandler")).isAssignableFrom(propertyDescriptor.getPropertyType()) && propertyDescriptor.getWriteMethod() != null) {
                        final OptionHandler optionHandler = (OptionHandler)OptionConverter.instantiateByKey(properties, new StringBuffer().append(s).append(substring).toString(), propertyDescriptor.getPropertyType(), null);
                        new PropertySetter(optionHandler).setProperties(properties, new StringBuffer().append(s).append(substring).append(".").toString());
                        try {
                            propertyDescriptor.getWriteMethod().invoke(this.obj, optionHandler);
                        }
                        catch (IllegalAccessException ex) {
                            LogLog.warn(new StringBuffer().append("Failed to set property [").append(substring).append("] to value \"").append(andSubst).append("\". ").toString(), ex);
                        }
                        catch (InvocationTargetException ex2) {
                            if (ex2.getTargetException() instanceof InterruptedException || ex2.getTargetException() instanceof InterruptedIOException) {
                                Thread.currentThread().interrupt();
                            }
                            LogLog.warn(new StringBuffer().append("Failed to set property [").append(substring).append("] to value \"").append(andSubst).append("\". ").toString(), ex2);
                        }
                        catch (RuntimeException ex3) {
                            LogLog.warn(new StringBuffer().append("Failed to set property [").append(substring).append("] to value \"").append(andSubst).append("\". ").toString(), ex3);
                        }
                        continue;
                    }
                }
                this.setProperty(substring, andSubst);
            }
        }
        this.activate();
    }
    
    public void setProperty(final PropertyDescriptor p0, final String p1, final String p2) throws PropertySetterException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   java/beans/PropertyDescriptor.getWriteMethod:()Ljava/lang/reflect/Method;
        //     4: astore          4
        //     6: aload           4
        //     8: ifnonnull       45
        //    11: new             Lorg/apache/log4j/config/PropertySetterException;
        //    14: dup            
        //    15: new             Ljava/lang/StringBuffer;
        //    18: dup            
        //    19: invokespecial   java/lang/StringBuffer.<init>:()V
        //    22: ldc_w           "No setter for property ["
        //    25: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    28: aload_2        
        //    29: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    32: ldc_w           "]."
        //    35: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    38: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    41: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/String;)V
        //    44: athrow         
        //    45: aload           4
        //    47: invokevirtual   java/lang/reflect/Method.getParameterTypes:()[Ljava/lang/Class;
        //    50: astore_1       
        //    51: aload_1        
        //    52: arraylength    
        //    53: iconst_1       
        //    54: if_icmpeq       68
        //    57: new             Lorg/apache/log4j/config/PropertySetterException;
        //    60: dup            
        //    61: ldc_w           "#params for setter != 1"
        //    64: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/String;)V
        //    67: athrow         
        //    68: aload_0        
        //    69: aload_3        
        //    70: aload_1        
        //    71: iconst_0       
        //    72: aaload         
        //    73: invokevirtual   org/apache/log4j/config/PropertySetter.convertArg:(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
        //    76: astore_3       
        //    77: aload_3        
        //    78: ifnonnull       158
        //    81: new             Lorg/apache/log4j/config/PropertySetterException;
        //    84: dup            
        //    85: new             Ljava/lang/StringBuffer;
        //    88: dup            
        //    89: invokespecial   java/lang/StringBuffer.<init>:()V
        //    92: ldc_w           "Conversion to type ["
        //    95: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    98: aload_1        
        //    99: iconst_0       
        //   100: aaload         
        //   101: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   104: ldc_w           "] failed."
        //   107: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   110: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   113: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/String;)V
        //   116: athrow         
        //   117: astore_2       
        //   118: new             Lorg/apache/log4j/config/PropertySetterException;
        //   121: dup            
        //   122: new             Ljava/lang/StringBuffer;
        //   125: dup            
        //   126: invokespecial   java/lang/StringBuffer.<init>:()V
        //   129: ldc_w           "Conversion to type ["
        //   132: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   135: aload_1        
        //   136: iconst_0       
        //   137: aaload         
        //   138: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   141: ldc_w           "] failed. Reason: "
        //   144: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   147: aload_2        
        //   148: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   151: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   154: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/String;)V
        //   157: athrow         
        //   158: new             Ljava/lang/StringBuffer;
        //   161: dup            
        //   162: invokespecial   java/lang/StringBuffer.<init>:()V
        //   165: ldc_w           "Setting property ["
        //   168: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   171: aload_2        
        //   172: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   175: ldc_w           "] to ["
        //   178: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   181: aload_3        
        //   182: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/Object;)Ljava/lang/StringBuffer;
        //   185: ldc_w           "]."
        //   188: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   191: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   194: invokestatic    org/apache/log4j/helpers/LogLog.debug:(Ljava/lang/String;)V
        //   197: aload           4
        //   199: aload_0        
        //   200: getfield        org/apache/log4j/config/PropertySetter.obj:Ljava/lang/Object;
        //   203: iconst_1       
        //   204: anewarray       Ljava/lang/Object;
        //   207: dup            
        //   208: iconst_0       
        //   209: aload_3        
        //   210: aastore        
        //   211: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   214: pop            
        //   215: return         
        //   216: astore_1       
        //   217: new             Lorg/apache/log4j/config/PropertySetterException;
        //   220: dup            
        //   221: aload_1        
        //   222: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/Throwable;)V
        //   225: athrow         
        //   226: astore_1       
        //   227: aload_1        
        //   228: invokevirtual   java/lang/reflect/InvocationTargetException.getTargetException:()Ljava/lang/Throwable;
        //   231: instanceof      Ljava/lang/InterruptedException;
        //   234: ifne            247
        //   237: aload_1        
        //   238: invokevirtual   java/lang/reflect/InvocationTargetException.getTargetException:()Ljava/lang/Throwable;
        //   241: instanceof      Ljava/io/InterruptedIOException;
        //   244: ifeq            253
        //   247: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   250: invokevirtual   java/lang/Thread.interrupt:()V
        //   253: new             Lorg/apache/log4j/config/PropertySetterException;
        //   256: dup            
        //   257: aload_1        
        //   258: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/Throwable;)V
        //   261: athrow         
        //   262: astore_1       
        //   263: new             Lorg/apache/log4j/config/PropertySetterException;
        //   266: dup            
        //   267: aload_1        
        //   268: invokespecial   org/apache/log4j/config/PropertySetterException.<init>:(Ljava/lang/Throwable;)V
        //   271: athrow         
        //    Exceptions:
        //  throws org.apache.log4j.config.PropertySetterException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  68     77     117    158    Ljava/lang/Throwable;
        //  197    215    216    226    Ljava/lang/IllegalAccessException;
        //  197    215    226    262    Ljava/lang/reflect/InvocationTargetException;
        //  197    215    262    272    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setProperty(String decapitalize, final String s) {
        if (s == null) {
            return;
        }
        decapitalize = Introspector.decapitalize(decapitalize);
        final PropertyDescriptor propertyDescriptor = this.getPropertyDescriptor(decapitalize);
        if (propertyDescriptor == null) {
            LogLog.warn(new StringBuffer().append("No such property [").append(decapitalize).append("] in ").append(this.obj.getClass().getName()).append(".").toString());
            return;
        }
        try {
            this.setProperty(propertyDescriptor, decapitalize, s);
        }
        catch (PropertySetterException ex) {
            LogLog.warn(new StringBuffer().append("Failed to set property [").append(decapitalize).append("] to value \"").append(s).append("\". ").toString(), ex.rootCause);
        }
    }
}
