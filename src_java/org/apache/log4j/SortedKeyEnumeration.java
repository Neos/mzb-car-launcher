package org.apache.log4j;

import java.util.*;

class SortedKeyEnumeration implements Enumeration
{
    private Enumeration e;
    
    public SortedKeyEnumeration(final Hashtable hashtable) {
        final Enumeration<String> keys = hashtable.keys();
        final Vector<String> vector = new Vector<String>(hashtable.size());
        int n = 0;
        while (keys.hasMoreElements()) {
            String s;
            int n2;
            for (s = keys.nextElement(), n2 = 0; n2 < n && s.compareTo((String)vector.get(n2)) > 0; ++n2) {}
            vector.add(n2, s);
            ++n;
        }
        this.e = vector.elements();
    }
    
    @Override
    public boolean hasMoreElements() {
        return this.e.hasMoreElements();
    }
    
    @Override
    public Object nextElement() {
        return this.e.nextElement();
    }
}
