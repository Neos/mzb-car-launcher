package org.apache.log4j;

import org.apache.log4j.spi.*;
import java.text.*;
import java.util.*;

public final class LogMF extends LogXF
{
    private static final String FQCN;
    static Class class$org$apache$log4j$LogMF;
    private static DateFormat dateFormat;
    private static Locale dateLocale;
    private static NumberFormat numberFormat;
    private static Locale numberLocale;
    
    static {
        LogMF.numberFormat = null;
        LogMF.numberLocale = null;
        LogMF.dateFormat = null;
        LogMF.dateLocale = null;
        Class class$org$apache$log4j$LogMF;
        if (LogMF.class$org$apache$log4j$LogMF == null) {
            class$org$apache$log4j$LogMF = (LogMF.class$org$apache$log4j$LogMF = class$("org.apache.log4j.LogMF"));
        }
        else {
            class$org$apache$log4j$LogMF = LogMF.class$org$apache$log4j$LogMF;
        }
        FQCN = class$org$apache$log4j$LogMF.getName();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static void debug(final Logger logger, final String s, final byte b) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final char c) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final double n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final float n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final int n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final long n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, o));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final short n) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final boolean b) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void debug(final Logger logger, final String s, final Object[] array) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, array));
        }
    }
    
    public static void debug(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isDebugEnabled()) {
            forcedLog(logger, Level.DEBUG, format(s, array), t);
        }
    }
    
    public static void error(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.ERROR)) {
            forcedLog(logger, Level.ERROR, format(s, array));
        }
    }
    
    public static void error(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.ERROR)) {
            forcedLog(logger, Level.ERROR, format(s, array), t);
        }
    }
    
    public static void fatal(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.FATAL)) {
            forcedLog(logger, Level.FATAL, format(s, array));
        }
    }
    
    public static void fatal(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.FATAL)) {
            forcedLog(logger, Level.FATAL, format(s, array), t);
        }
    }
    
    private static void forcedLog(final Logger logger, final Level level, final String s) {
        logger.callAppenders(new LoggingEvent(LogMF.FQCN, logger, level, s, null));
    }
    
    private static void forcedLog(final Logger logger, final Level level, final String s, final Throwable t) {
        logger.callAppenders(new LoggingEvent(LogMF.FQCN, logger, level, s, t));
    }
    
    private static String format(final String s, final Object o) {
        if (s == null) {
            return null;
        }
        if (isSimple(s)) {
            String s2 = null;
            int n = 0;
            String string = "";
            int i = s.indexOf(123);
            while (i >= 0) {
                if (i + 2 < s.length() && s.charAt(i + 2) == '}' && s.charAt(i + 1) >= '0' && s.charAt(i + 1) <= '9') {
                    final char char1 = s.charAt(i + 1);
                    final String string2 = new StringBuffer().append(string).append(s.substring(n, i)).toString();
                    if (char1 - '0' != '\0') {
                        string = new StringBuffer().append(string2).append(s.substring(i, i + 3)).toString();
                    }
                    else {
                        String formatObject;
                        if ((formatObject = s2) == null) {
                            formatObject = formatObject(o);
                        }
                        final String string3 = new StringBuffer().append(string2).append(formatObject).toString();
                        s2 = formatObject;
                        string = string3;
                    }
                    n = i + 3;
                    i = s.indexOf(123, n);
                }
                else {
                    i = s.indexOf(123, i + 1);
                }
            }
            return new StringBuffer().append(string).append(s.substring(n)).toString();
        }
        try {
            return MessageFormat.format(s, o);
        }
        catch (IllegalArgumentException ex) {
            return s;
        }
    }
    
    private static String format(String string, final String s, final Object[] array) {
        while (true) {
            Label_0025: {
                if (string == null) {
                    break Label_0025;
                }
                try {
                    string = ResourceBundle.getBundle(string).getString(s);
                    return format(string, array);
                }
                catch (Exception ex) {
                    string = s;
                    return format(string, array);
                }
            }
            string = s;
            continue;
        }
    }
    
    private static String format(final String s, final Object[] array) {
        if (s == null) {
            return null;
        }
        if (isSimple(s)) {
            final String[] array2 = new String[10];
            int n = 0;
            String string = "";
            int i = s.indexOf(123);
            while (i >= 0) {
                if (i + 2 < s.length() && s.charAt(i + 2) == '}' && s.charAt(i + 1) >= '0' && s.charAt(i + 1) <= '9') {
                    final char c = (char)(s.charAt(i + 1) - '0');
                    final String string2 = new StringBuffer().append(string).append(s.substring(n, i)).toString();
                    if (array2[c] == null) {
                        if (array == null || c >= array.length) {
                            array2[c] = s.substring(i, i + 3);
                        }
                        else {
                            array2[c] = formatObject(array[c]);
                        }
                    }
                    string = new StringBuffer().append(string2).append(array2[c]).toString();
                    n = i + 3;
                    i = s.indexOf(123, n);
                }
                else {
                    i = s.indexOf(123, i + 1);
                }
            }
            return new StringBuffer().append(string).append(s.substring(n)).toString();
        }
        try {
            return MessageFormat.format(s, array);
        }
        catch (IllegalArgumentException ex) {
            return s;
        }
    }
    
    private static String formatDate(final Object o) {
        synchronized (LogMF.class) {
            final Locale default1 = Locale.getDefault();
            if (default1 != LogMF.dateLocale || LogMF.dateFormat == null) {
                LogMF.dateLocale = default1;
                LogMF.dateFormat = DateFormat.getDateTimeInstance(3, 3, default1);
            }
            return LogMF.dateFormat.format(o);
        }
    }
    
    private static String formatNumber(final Object o) {
        synchronized (LogMF.class) {
            final Locale default1 = Locale.getDefault();
            if (default1 != LogMF.numberLocale || LogMF.numberFormat == null) {
                LogMF.numberLocale = default1;
                LogMF.numberFormat = NumberFormat.getInstance(default1);
            }
            return LogMF.numberFormat.format(o);
        }
    }
    
    private static String formatObject(final Object o) {
        if (o instanceof String) {
            return o.toString();
        }
        if (o instanceof Double || o instanceof Float) {
            return formatNumber(o);
        }
        if (o instanceof Date) {
            return formatDate(o);
        }
        return String.valueOf(o);
    }
    
    public static void info(final Logger logger, final String s, final byte b) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void info(final Logger logger, final String s, final char c) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void info(final Logger logger, final String s, final double n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final float n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final int n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final long n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, o));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void info(final Logger logger, final String s, final short n) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void info(final Logger logger, final String s, final boolean b) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void info(final Logger logger, final String s, final Object[] array) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, array));
        }
    }
    
    public static void info(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isInfoEnabled()) {
            forcedLog(logger, Level.INFO, format(s, array), t);
        }
    }
    
    private static boolean isSimple(final String s) {
        if (s.indexOf(39) == -1) {
            for (int i = s.indexOf(123); i != -1; i = s.indexOf(123, i + 1)) {
                if (i + 2 >= s.length() || s.charAt(i + 2) != '}' || s.charAt(i + 1) < '0' || s.charAt(i + 1) > '9') {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public static void log(final Logger logger, final Level level, final String s, final byte b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final char c) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(c))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final double n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final float n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final int n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final long n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o, final Object o2) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final short n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final boolean b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void log(final Logger logger, final Level level, final String s, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, array));
        }
    }
    
    public static void log(final Logger logger, final Level level, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, array), t);
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final byte b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final char c) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(c))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final double n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final float n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final int n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final long n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o, final Object o2) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o, o2)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final short n) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(n))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final boolean b) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, LogXF.toArray(LogXF.valueOf(b))));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final String s, final String s2, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, array));
        }
    }
    
    public static void logrb(final Logger logger, final Level level, final Throwable t, final String s, final String s2, final Object[] array) {
        if (logger.isEnabledFor(level)) {
            forcedLog(logger, level, format(s, s2, array), t);
        }
    }
    
    public static void trace(final Logger logger, final String s, final byte b) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final char c) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final double n) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final float n) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final int n) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final long n) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, o));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final short n) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final boolean b) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void trace(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, array));
        }
    }
    
    public static void trace(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(LogMF.TRACE)) {
            forcedLog(logger, LogMF.TRACE, format(s, array), t);
        }
    }
    
    public static void warn(final Logger logger, final String s, final byte b) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final char c) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(c)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final double n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final float n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final int n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final long n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, o));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o, final Object o2) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.toArray(o, o2)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o, final Object o2, final Object o3) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.toArray(o, o2, o3)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object o, final Object o2, final Object o3, final Object o4) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.toArray(o, o2, o3, o4)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final short n) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(n)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final boolean b) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, LogXF.valueOf(b)));
        }
    }
    
    public static void warn(final Logger logger, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, array));
        }
    }
    
    public static void warn(final Logger logger, final Throwable t, final String s, final Object[] array) {
        if (logger.isEnabledFor(Level.WARN)) {
            forcedLog(logger, Level.WARN, format(s, array), t);
        }
    }
}
