package org.apache.log4j.jmx;

import java.util.*;
import java.beans.*;
import java.io.*;
import java.lang.reflect.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;
import org.apache.log4j.*;
import javax.management.*;

public class AppenderDynamicMBean extends AbstractDynamicMBean
{
    private static Logger cat;
    static Class class$java$lang$String;
    static Class class$org$apache$log4j$Layout;
    static Class class$org$apache$log4j$Priority;
    static Class class$org$apache$log4j$jmx$AppenderDynamicMBean;
    private Appender appender;
    private Vector dAttributes;
    private String dClassName;
    private MBeanConstructorInfo[] dConstructors;
    private String dDescription;
    private MBeanOperationInfo[] dOperations;
    private Hashtable dynamicProps;
    
    static {
        Class class$org$apache$log4j$jmx$AppenderDynamicMBean;
        if (AppenderDynamicMBean.class$org$apache$log4j$jmx$AppenderDynamicMBean == null) {
            class$org$apache$log4j$jmx$AppenderDynamicMBean = (AppenderDynamicMBean.class$org$apache$log4j$jmx$AppenderDynamicMBean = class$("org.apache.log4j.jmx.AppenderDynamicMBean"));
        }
        else {
            class$org$apache$log4j$jmx$AppenderDynamicMBean = AppenderDynamicMBean.class$org$apache$log4j$jmx$AppenderDynamicMBean;
        }
        AppenderDynamicMBean.cat = Logger.getLogger(class$org$apache$log4j$jmx$AppenderDynamicMBean);
    }
    
    public AppenderDynamicMBean(final Appender appender) throws IntrospectionException {
        this.dConstructors = new MBeanConstructorInfo[1];
        this.dAttributes = new Vector();
        this.dClassName = this.getClass().getName();
        this.dynamicProps = new Hashtable(5);
        this.dOperations = new MBeanOperationInfo[2];
        this.dDescription = "This MBean acts as a management facade for log4j appenders.";
        this.appender = appender;
        this.buildDynamicMBeanInfo();
    }
    
    private void buildDynamicMBeanInfo() throws IntrospectionException {
        this.dConstructors[0] = new MBeanConstructorInfo("AppenderDynamicMBean(): Constructs a AppenderDynamicMBean instance", this.getClass().getConstructors()[0]);
        final PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(this.appender.getClass()).getPropertyDescriptors();
        for (int length = propertyDescriptors.length, i = 0; i < length; ++i) {
            final String name = propertyDescriptors[i].getName();
            final Method readMethod = propertyDescriptors[i].getReadMethod();
            final Method writeMethod = propertyDescriptors[i].getWriteMethod();
            if (readMethod != null) {
                final Class<?> returnType = readMethod.getReturnType();
                if (this.isSupportedType(returnType)) {
                    Class class$org$apache$log4j$Priority;
                    if (AppenderDynamicMBean.class$org$apache$log4j$Priority == null) {
                        class$org$apache$log4j$Priority = (AppenderDynamicMBean.class$org$apache$log4j$Priority = class$("org.apache.log4j.Priority"));
                    }
                    else {
                        class$org$apache$log4j$Priority = AppenderDynamicMBean.class$org$apache$log4j$Priority;
                    }
                    String name2;
                    if (returnType.isAssignableFrom(class$org$apache$log4j$Priority)) {
                        name2 = "java.lang.String";
                    }
                    else {
                        name2 = returnType.getName();
                    }
                    this.dAttributes.add(new MBeanAttributeInfo(name, name2, "Dynamic", true, writeMethod != null, false));
                    this.dynamicProps.put(name, new MethodUnion(readMethod, writeMethod));
                }
            }
        }
        this.dOperations[0] = new MBeanOperationInfo("activateOptions", "activateOptions(): add an appender", new MBeanParameterInfo[0], "void", 1);
        this.dOperations[1] = new MBeanOperationInfo("setLayout", "setLayout(): add a layout", new MBeanParameterInfo[] { new MBeanParameterInfo("layout class", "java.lang.String", "layout class") }, "void", 1);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private boolean isSupportedType(final Class clazz) {
        if (clazz.isPrimitive()) {
            return true;
        }
        Class class$java$lang$String;
        if (AppenderDynamicMBean.class$java$lang$String == null) {
            class$java$lang$String = (AppenderDynamicMBean.class$java$lang$String = class$("java.lang.String"));
        }
        else {
            class$java$lang$String = AppenderDynamicMBean.class$java$lang$String;
        }
        if (clazz == class$java$lang$String) {
            return true;
        }
        Class class$org$apache$log4j$Priority;
        if (AppenderDynamicMBean.class$org$apache$log4j$Priority == null) {
            class$org$apache$log4j$Priority = (AppenderDynamicMBean.class$org$apache$log4j$Priority = class$("org.apache.log4j.Priority"));
        }
        else {
            class$org$apache$log4j$Priority = AppenderDynamicMBean.class$org$apache$log4j$Priority;
        }
        return clazz.isAssignableFrom(class$org$apache$log4j$Priority);
    }
    
    @Override
    public Object getAttribute(final String s) throws AttributeNotFoundException, MBeanException, ReflectionException {
        if (s == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke a getter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        AppenderDynamicMBean.cat.debug(new StringBuffer().append("getAttribute called with [").append(s).append("].").toString());
        if (s.startsWith(new StringBuffer().append("appender=").append(this.appender.getName()).append(",layout").toString())) {
            try {
                return new ObjectName(new StringBuffer().append("log4j:").append(s).toString());
            }
            catch (MalformedObjectNameException ex) {
                AppenderDynamicMBean.cat.error("attributeName", ex);
            }
            catch (RuntimeException ex2) {
                AppenderDynamicMBean.cat.error("attributeName", ex2);
                goto Label_0159;
            }
            try {
                final MethodUnion methodUnion;
                return methodUnion.readMethod.invoke(this.appender, (Object[])null);
            }
            catch (IllegalAccessException ex4) {
                return null;
            }
            catch (InvocationTargetException ex3) {
                if (ex3.getTargetException() instanceof InterruptedException || ex3.getTargetException() instanceof InterruptedIOException) {
                    Thread.currentThread().interrupt();
                }
                return null;
            }
            catch (RuntimeException ex5) {
                return null;
            }
            throw new AttributeNotFoundException(new StringBuffer().append("Cannot find ").append(s).append(" attribute in ").append(this.dClassName).toString());
        }
        goto Label_0159;
    }
    
    @Override
    protected Logger getLogger() {
        return AppenderDynamicMBean.cat;
    }
    
    @Override
    public MBeanInfo getMBeanInfo() {
        AppenderDynamicMBean.cat.debug("getMBeanInfo called.");
        final MBeanAttributeInfo[] array = new MBeanAttributeInfo[this.dAttributes.size()];
        this.dAttributes.toArray(array);
        return new MBeanInfo(this.dClassName, this.dDescription, array, this.dConstructors, this.dOperations, new MBeanNotificationInfo[0]);
    }
    
    @Override
    public Object invoke(final String s, final Object[] array, final String[] array2) throws MBeanException, ReflectionException {
        if (s.equals("activateOptions") && this.appender instanceof OptionHandler) {
            ((OptionHandler)this.appender).activateOptions();
            return "Options activated.";
        }
        if (s.equals("setLayout")) {
            final String s2 = (String)array[0];
            Class class$org$apache$log4j$Layout;
            if (AppenderDynamicMBean.class$org$apache$log4j$Layout == null) {
                class$org$apache$log4j$Layout = (AppenderDynamicMBean.class$org$apache$log4j$Layout = class$("org.apache.log4j.Layout"));
            }
            else {
                class$org$apache$log4j$Layout = AppenderDynamicMBean.class$org$apache$log4j$Layout;
            }
            final Layout layout = (Layout)OptionConverter.instantiateByClassName(s2, class$org$apache$log4j$Layout, null);
            this.appender.setLayout(layout);
            this.registerLayoutMBean(layout);
        }
        return null;
    }
    
    @Override
    public ObjectName preRegister(final MBeanServer server, final ObjectName objectName) {
        AppenderDynamicMBean.cat.debug(new StringBuffer().append("preRegister called. Server=").append(server).append(", name=").append(objectName).toString());
        this.server = server;
        this.registerLayoutMBean(this.appender.getLayout());
        return objectName;
    }
    
    void registerLayoutMBean(final Layout p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: return         
        //     5: new             Ljava/lang/StringBuffer;
        //     8: dup            
        //     9: invokespecial   java/lang/StringBuffer.<init>:()V
        //    12: aload_0        
        //    13: getfield        org/apache/log4j/jmx/AppenderDynamicMBean.appender:Lorg/apache/log4j/Appender;
        //    16: invokestatic    org/apache/log4j/jmx/AppenderDynamicMBean.getAppenderName:(Lorg/apache/log4j/Appender;)Ljava/lang/String;
        //    19: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    22: ldc_w           ",layout="
        //    25: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    28: aload_1        
        //    29: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //    32: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //    35: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    38: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    41: astore_2       
        //    42: getstatic       org/apache/log4j/jmx/AppenderDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //    45: new             Ljava/lang/StringBuffer;
        //    48: dup            
        //    49: invokespecial   java/lang/StringBuffer.<init>:()V
        //    52: ldc_w           "Adding LayoutMBean:"
        //    55: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    58: aload_2        
        //    59: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    62: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    65: invokevirtual   org/apache/log4j/Logger.debug:(Ljava/lang/Object;)V
        //    68: new             Lorg/apache/log4j/jmx/LayoutDynamicMBean;
        //    71: dup            
        //    72: aload_1        
        //    73: invokespecial   org/apache/log4j/jmx/LayoutDynamicMBean.<init>:(Lorg/apache/log4j/Layout;)V
        //    76: astore_1       
        //    77: new             Ljavax/management/ObjectName;
        //    80: dup            
        //    81: new             Ljava/lang/StringBuffer;
        //    84: dup            
        //    85: invokespecial   java/lang/StringBuffer.<init>:()V
        //    88: ldc_w           "log4j:appender="
        //    91: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    94: aload_2        
        //    95: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    98: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   101: invokespecial   javax/management/ObjectName.<init>:(Ljava/lang/String;)V
        //   104: astore_3       
        //   105: aload_0        
        //   106: getfield        org/apache/log4j/jmx/AppenderDynamicMBean.server:Ljavax/management/MBeanServer;
        //   109: aload_3        
        //   110: invokeinterface javax/management/MBeanServer.isRegistered:(Ljavax/management/ObjectName;)Z
        //   115: ifne            190
        //   118: aload_0        
        //   119: aload_1        
        //   120: aload_3        
        //   121: invokevirtual   org/apache/log4j/jmx/AppenderDynamicMBean.registerMBean:(Ljava/lang/Object;Ljavax/management/ObjectName;)V
        //   124: aload_0        
        //   125: getfield        org/apache/log4j/jmx/AppenderDynamicMBean.dAttributes:Ljava/util/Vector;
        //   128: new             Ljavax/management/MBeanAttributeInfo;
        //   131: dup            
        //   132: new             Ljava/lang/StringBuffer;
        //   135: dup            
        //   136: invokespecial   java/lang/StringBuffer.<init>:()V
        //   139: ldc             "appender="
        //   141: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   144: aload_2        
        //   145: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   148: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   151: ldc_w           "javax.management.ObjectName"
        //   154: new             Ljava/lang/StringBuffer;
        //   157: dup            
        //   158: invokespecial   java/lang/StringBuffer.<init>:()V
        //   161: ldc_w           "The "
        //   164: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   167: aload_2        
        //   168: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   171: ldc_w           " layout."
        //   174: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   177: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   180: iconst_1       
        //   181: iconst_1       
        //   182: iconst_0       
        //   183: invokespecial   javax/management/MBeanAttributeInfo.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V
        //   186: invokevirtual   java/util/Vector.add:(Ljava/lang/Object;)Z
        //   189: pop            
        //   190: return         
        //   191: astore_1       
        //   192: getstatic       org/apache/log4j/jmx/AppenderDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //   195: new             Ljava/lang/StringBuffer;
        //   198: dup            
        //   199: invokespecial   java/lang/StringBuffer.<init>:()V
        //   202: ldc_w           "Could not add DynamicLayoutMBean for ["
        //   205: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   208: aload_2        
        //   209: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   212: ldc             "]."
        //   214: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   217: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   220: aload_1        
        //   221: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   224: return         
        //   225: astore_1       
        //   226: getstatic       org/apache/log4j/jmx/AppenderDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //   229: new             Ljava/lang/StringBuffer;
        //   232: dup            
        //   233: invokespecial   java/lang/StringBuffer.<init>:()V
        //   236: ldc_w           "Could not add DynamicLayoutMBean for ["
        //   239: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   242: aload_2        
        //   243: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   246: ldc             "]."
        //   248: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   251: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   254: aload_1        
        //   255: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   258: return         
        //   259: astore_1       
        //   260: getstatic       org/apache/log4j/jmx/AppenderDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //   263: new             Ljava/lang/StringBuffer;
        //   266: dup            
        //   267: invokespecial   java/lang/StringBuffer.<init>:()V
        //   270: ldc_w           "Could not add DynamicLayoutMBean for ["
        //   273: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   276: aload_2        
        //   277: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   280: ldc             "]."
        //   282: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   285: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   288: aload_1        
        //   289: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   292: return         
        //   293: astore_1       
        //   294: goto            260
        //   297: astore_1       
        //   298: goto            226
        //   301: astore_1       
        //   302: goto            192
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                               
        //  -----  -----  -----  -----  -----------------------------------
        //  68     105    191    192    Ljavax/management/JMException;
        //  68     105    225    226    Ljava/beans/IntrospectionException;
        //  68     105    259    260    Ljava/lang/RuntimeException;
        //  105    190    301    305    Ljavax/management/JMException;
        //  105    190    297    301    Ljava/beans/IntrospectionException;
        //  105    190    293    297    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0190:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void setAttribute(final Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        if (attribute == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute cannot be null"), new StringBuffer().append("Cannot invoke a setter of ").append(this.dClassName).append(" with null attribute").toString());
        }
        final String name = attribute.getName();
        final Object value = attribute.getValue();
        if (name == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke the setter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        final MethodUnion methodUnion = this.dynamicProps.get(name);
        Label_0283: {
            if (methodUnion == null || methodUnion.writeMethod == null) {
                break Label_0283;
            }
            final Class<?> clazz = methodUnion.writeMethod.getParameterTypes()[0];
            Label_0214: {
                if (AppenderDynamicMBean.class$org$apache$log4j$Priority != null) {
                    break Label_0214;
                }
                Class class$org$apache$log4j$Priority = AppenderDynamicMBean.class$org$apache$log4j$Priority = class$("org.apache.log4j.Priority");
                while (true) {
                    Object level = value;
                    if (clazz == class$org$apache$log4j$Priority) {
                        level = OptionConverter.toLevel((String)value, (Level)this.getAttribute(name));
                    }
                    try {
                        methodUnion.writeMethod.invoke(this.appender, level);
                        return;
                        class$org$apache$log4j$Priority = AppenderDynamicMBean.class$org$apache$log4j$Priority;
                        continue;
                    }
                    catch (InvocationTargetException ex) {
                        if (ex.getTargetException() instanceof InterruptedException || ex.getTargetException() instanceof InterruptedIOException) {
                            Thread.currentThread().interrupt();
                        }
                        AppenderDynamicMBean.cat.error("FIXME", ex);
                        return;
                    }
                    catch (IllegalAccessException ex2) {
                        AppenderDynamicMBean.cat.error("FIXME", ex2);
                        return;
                    }
                    catch (RuntimeException ex3) {
                        AppenderDynamicMBean.cat.error("FIXME", ex3);
                        return;
                    }
                    break;
                }
            }
        }
        if (!name.endsWith(".layout")) {
            throw new AttributeNotFoundException(new StringBuffer().append("Attribute ").append(name).append(" not found in ").append(this.getClass().getName()).toString());
        }
    }
}
