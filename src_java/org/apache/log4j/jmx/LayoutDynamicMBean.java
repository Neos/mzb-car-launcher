package org.apache.log4j.jmx;

import java.util.*;
import java.beans.*;
import java.io.*;
import java.lang.reflect.*;
import org.apache.log4j.spi.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import javax.management.*;

public class LayoutDynamicMBean extends AbstractDynamicMBean
{
    private static Logger cat;
    static Class class$java$lang$String;
    static Class class$org$apache$log4j$Level;
    static Class class$org$apache$log4j$Priority;
    static Class class$org$apache$log4j$jmx$LayoutDynamicMBean;
    private Vector dAttributes;
    private String dClassName;
    private MBeanConstructorInfo[] dConstructors;
    private String dDescription;
    private MBeanOperationInfo[] dOperations;
    private Hashtable dynamicProps;
    private Layout layout;
    
    static {
        Class class$org$apache$log4j$jmx$LayoutDynamicMBean;
        if (LayoutDynamicMBean.class$org$apache$log4j$jmx$LayoutDynamicMBean == null) {
            class$org$apache$log4j$jmx$LayoutDynamicMBean = (LayoutDynamicMBean.class$org$apache$log4j$jmx$LayoutDynamicMBean = class$("org.apache.log4j.jmx.LayoutDynamicMBean"));
        }
        else {
            class$org$apache$log4j$jmx$LayoutDynamicMBean = LayoutDynamicMBean.class$org$apache$log4j$jmx$LayoutDynamicMBean;
        }
        LayoutDynamicMBean.cat = Logger.getLogger(class$org$apache$log4j$jmx$LayoutDynamicMBean);
    }
    
    public LayoutDynamicMBean(final Layout layout) throws IntrospectionException {
        this.dConstructors = new MBeanConstructorInfo[1];
        this.dAttributes = new Vector();
        this.dClassName = this.getClass().getName();
        this.dynamicProps = new Hashtable(5);
        this.dOperations = new MBeanOperationInfo[1];
        this.dDescription = "This MBean acts as a management facade for log4j layouts.";
        this.layout = layout;
        this.buildDynamicMBeanInfo();
    }
    
    private void buildDynamicMBeanInfo() throws IntrospectionException {
        this.dConstructors[0] = new MBeanConstructorInfo("LayoutDynamicMBean(): Constructs a LayoutDynamicMBean instance", this.getClass().getConstructors()[0]);
        final PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(this.layout.getClass()).getPropertyDescriptors();
        for (int length = propertyDescriptors.length, i = 0; i < length; ++i) {
            final String name = propertyDescriptors[i].getName();
            final Method readMethod = propertyDescriptors[i].getReadMethod();
            final Method writeMethod = propertyDescriptors[i].getWriteMethod();
            if (readMethod != null) {
                final Class<?> returnType = readMethod.getReturnType();
                if (this.isSupportedType(returnType)) {
                    Class class$org$apache$log4j$Level;
                    if (LayoutDynamicMBean.class$org$apache$log4j$Level == null) {
                        class$org$apache$log4j$Level = (LayoutDynamicMBean.class$org$apache$log4j$Level = class$("org.apache.log4j.Level"));
                    }
                    else {
                        class$org$apache$log4j$Level = LayoutDynamicMBean.class$org$apache$log4j$Level;
                    }
                    String name2;
                    if (returnType.isAssignableFrom(class$org$apache$log4j$Level)) {
                        name2 = "java.lang.String";
                    }
                    else {
                        name2 = returnType.getName();
                    }
                    this.dAttributes.add(new MBeanAttributeInfo(name, name2, "Dynamic", true, writeMethod != null, false));
                    this.dynamicProps.put(name, new MethodUnion(readMethod, writeMethod));
                }
            }
        }
        this.dOperations[0] = new MBeanOperationInfo("activateOptions", "activateOptions(): add an layout", new MBeanParameterInfo[0], "void", 1);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private boolean isSupportedType(final Class clazz) {
        if (clazz.isPrimitive()) {
            return true;
        }
        Class class$java$lang$String;
        if (LayoutDynamicMBean.class$java$lang$String == null) {
            class$java$lang$String = (LayoutDynamicMBean.class$java$lang$String = class$("java.lang.String"));
        }
        else {
            class$java$lang$String = LayoutDynamicMBean.class$java$lang$String;
        }
        if (clazz == class$java$lang$String) {
            return true;
        }
        Class class$org$apache$log4j$Level;
        if (LayoutDynamicMBean.class$org$apache$log4j$Level == null) {
            class$org$apache$log4j$Level = (LayoutDynamicMBean.class$org$apache$log4j$Level = class$("org.apache.log4j.Level"));
        }
        else {
            class$org$apache$log4j$Level = LayoutDynamicMBean.class$org$apache$log4j$Level;
        }
        return clazz.isAssignableFrom(class$org$apache$log4j$Level);
    }
    
    @Override
    public Object getAttribute(final String s) throws AttributeNotFoundException, MBeanException, ReflectionException {
        final Object o = null;
        if (s == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke a getter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        final MethodUnion methodUnion = this.dynamicProps.get(s);
        LayoutDynamicMBean.cat.debug(new StringBuffer().append("----name=").append(s).append(", mu=").append(methodUnion).toString());
        if (methodUnion != null && methodUnion.readMethod != null) {
            try {
                return methodUnion.readMethod.invoke(this.layout, (Object[])null);
            }
            catch (InvocationTargetException ex) {
                if (!(ex.getTargetException() instanceof InterruptedException)) {
                    final Object invoke = o;
                    if (!(ex.getTargetException() instanceof InterruptedIOException)) {
                        return invoke;
                    }
                }
                Thread.currentThread().interrupt();
                return null;
            }
            catch (IllegalAccessException ex2) {
                return null;
            }
            catch (RuntimeException ex3) {
                return null;
            }
        }
        throw new AttributeNotFoundException(new StringBuffer().append("Cannot find ").append(s).append(" attribute in ").append(this.dClassName).toString());
    }
    
    @Override
    protected Logger getLogger() {
        return LayoutDynamicMBean.cat;
    }
    
    @Override
    public MBeanInfo getMBeanInfo() {
        LayoutDynamicMBean.cat.debug("getMBeanInfo called.");
        final MBeanAttributeInfo[] array = new MBeanAttributeInfo[this.dAttributes.size()];
        this.dAttributes.toArray(array);
        return new MBeanInfo(this.dClassName, this.dDescription, array, this.dConstructors, this.dOperations, new MBeanNotificationInfo[0]);
    }
    
    @Override
    public Object invoke(final String s, final Object[] array, final String[] array2) throws MBeanException, ReflectionException {
        if (s.equals("activateOptions") && this.layout instanceof OptionHandler) {
            this.layout.activateOptions();
            return "Options activated.";
        }
        return null;
    }
    
    @Override
    public void setAttribute(final Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        if (attribute == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute cannot be null"), new StringBuffer().append("Cannot invoke a setter of ").append(this.dClassName).append(" with null attribute").toString());
        }
        final String name = attribute.getName();
        final Object value = attribute.getValue();
        if (name == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke the setter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        final MethodUnion methodUnion = this.dynamicProps.get(name);
        if (methodUnion != null && methodUnion.writeMethod != null) {
            final Class<?> clazz = methodUnion.writeMethod.getParameterTypes()[0];
            Label_0215: {
                if (LayoutDynamicMBean.class$org$apache$log4j$Priority != null) {
                    break Label_0215;
                }
                Class class$org$apache$log4j$Priority = LayoutDynamicMBean.class$org$apache$log4j$Priority = class$("org.apache.log4j.Priority");
                while (true) {
                    Object level = value;
                    if (clazz == class$org$apache$log4j$Priority) {
                        level = OptionConverter.toLevel((String)value, (Level)this.getAttribute(name));
                    }
                    try {
                        methodUnion.writeMethod.invoke(this.layout, level);
                        return;
                        class$org$apache$log4j$Priority = LayoutDynamicMBean.class$org$apache$log4j$Priority;
                        continue;
                    }
                    catch (InvocationTargetException ex) {
                        if (ex.getTargetException() instanceof InterruptedException || ex.getTargetException() instanceof InterruptedIOException) {
                            Thread.currentThread().interrupt();
                        }
                        LayoutDynamicMBean.cat.error("FIXME", ex);
                        return;
                    }
                    catch (IllegalAccessException ex2) {
                        LayoutDynamicMBean.cat.error("FIXME", ex2);
                        return;
                    }
                    catch (RuntimeException ex3) {
                        LayoutDynamicMBean.cat.error("FIXME", ex3);
                        return;
                    }
                    break;
                }
            }
        }
        throw new AttributeNotFoundException(new StringBuffer().append("Attribute ").append(name).append(" not found in ").append(this.getClass().getName()).toString());
    }
}
