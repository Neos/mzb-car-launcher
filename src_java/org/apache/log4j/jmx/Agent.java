package org.apache.log4j.jmx;

import org.apache.log4j.*;
import java.io.*;
import java.lang.reflect.*;
import javax.management.*;

public class Agent
{
    static Class class$org$apache$log4j$jmx$Agent;
    static Logger log;
    
    static {
        Class class$org$apache$log4j$jmx$Agent;
        if (Agent.class$org$apache$log4j$jmx$Agent == null) {
            class$org$apache$log4j$jmx$Agent = (Agent.class$org$apache$log4j$jmx$Agent = class$("org.apache.log4j.jmx.Agent"));
        }
        else {
            class$org$apache$log4j$jmx$Agent = Agent.class$org$apache$log4j$jmx$Agent;
        }
        Agent.log = Logger.getLogger(class$org$apache$log4j$jmx$Agent);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private static Object createServer() {
        try {
            return Class.forName("com.sun.jdmk.comm.HtmlAdapterServer").newInstance();
        }
        catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex.toString());
        }
        catch (InstantiationException ex2) {
            throw new RuntimeException(ex2.toString());
        }
        catch (IllegalAccessException ex3) {
            throw new RuntimeException(ex3.toString());
        }
    }
    
    private static void startServer(final Object o) {
        try {
            o.getClass().getMethod("start", (Class<?>[])new Class[0]).invoke(o, new Object[0]);
        }
        catch (InvocationTargetException ex) {
            final Throwable targetException = ex.getTargetException();
            if (targetException instanceof RuntimeException) {
                throw (RuntimeException)targetException;
            }
            if (targetException != null) {
                if (targetException instanceof InterruptedException || targetException instanceof InterruptedIOException) {
                    Thread.currentThread().interrupt();
                }
                throw new RuntimeException(targetException.toString());
            }
            throw new RuntimeException();
        }
        catch (NoSuchMethodException ex2) {
            throw new RuntimeException(ex2.toString());
        }
        catch (IllegalAccessException ex3) {
            throw new RuntimeException(ex3.toString());
        }
    }
    
    public void start() {
        final MBeanServer mBeanServer = MBeanServerFactory.createMBeanServer();
        final Object server = createServer();
        try {
            Agent.log.info("Registering HtmlAdaptorServer instance.");
            mBeanServer.registerMBean(server, new ObjectName("Adaptor:name=html,port=8082"));
            Agent.log.info("Registering HierarchyDynamicMBean instance.");
            mBeanServer.registerMBean(new HierarchyDynamicMBean(), new ObjectName("log4j:hiearchy=default"));
            startServer(server);
        }
        catch (JMException ex) {
            Agent.log.error("Problem while registering MBeans instances.", ex);
        }
        catch (RuntimeException ex2) {
            Agent.log.error("Problem while registering MBeans instances.", ex2);
        }
    }
}
