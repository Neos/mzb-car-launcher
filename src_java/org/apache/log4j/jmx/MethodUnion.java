package org.apache.log4j.jmx;

import java.lang.reflect.*;

class MethodUnion
{
    Method readMethod;
    Method writeMethod;
    
    MethodUnion(final Method readMethod, final Method writeMethod) {
        this.readMethod = readMethod;
        this.writeMethod = writeMethod;
    }
}
