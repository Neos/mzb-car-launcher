package org.apache.log4j.jmx;

import org.apache.log4j.spi.*;
import java.util.*;
import org.apache.log4j.*;
import org.apache.log4j.helpers.*;
import javax.management.*;

public class HierarchyDynamicMBean extends AbstractDynamicMBean implements HierarchyEventListener, NotificationBroadcaster
{
    static final String ADD_APPENDER = "addAppender.";
    static final String THRESHOLD = "threshold";
    static Class class$org$apache$log4j$jmx$HierarchyDynamicMBean;
    private static Logger log;
    private String dClassName;
    private MBeanConstructorInfo[] dConstructors;
    private String dDescription;
    private MBeanOperationInfo[] dOperations;
    private LoggerRepository hierarchy;
    private NotificationBroadcasterSupport nbs;
    private Vector vAttributes;
    
    static {
        Class class$org$apache$log4j$jmx$HierarchyDynamicMBean;
        if (HierarchyDynamicMBean.class$org$apache$log4j$jmx$HierarchyDynamicMBean == null) {
            class$org$apache$log4j$jmx$HierarchyDynamicMBean = (HierarchyDynamicMBean.class$org$apache$log4j$jmx$HierarchyDynamicMBean = class$("org.apache.log4j.jmx.HierarchyDynamicMBean"));
        }
        else {
            class$org$apache$log4j$jmx$HierarchyDynamicMBean = HierarchyDynamicMBean.class$org$apache$log4j$jmx$HierarchyDynamicMBean;
        }
        HierarchyDynamicMBean.log = Logger.getLogger(class$org$apache$log4j$jmx$HierarchyDynamicMBean);
    }
    
    public HierarchyDynamicMBean() {
        this.dConstructors = new MBeanConstructorInfo[1];
        this.dOperations = new MBeanOperationInfo[1];
        this.vAttributes = new Vector();
        this.dClassName = this.getClass().getName();
        this.dDescription = "This MBean acts as a management facade for org.apache.log4j.Hierarchy.";
        this.nbs = new NotificationBroadcasterSupport();
        this.hierarchy = LogManager.getLoggerRepository();
        this.buildDynamicMBeanInfo();
    }
    
    private void buildDynamicMBeanInfo() {
        this.dConstructors[0] = new MBeanConstructorInfo("HierarchyDynamicMBean(): Constructs a HierarchyDynamicMBean instance", this.getClass().getConstructors()[0]);
        this.vAttributes.add(new MBeanAttributeInfo("threshold", "java.lang.String", "The \"threshold\" state of the hiearchy.", true, true, false));
        this.dOperations[0] = new MBeanOperationInfo("addLoggerMBean", "addLoggerMBean(): add a loggerMBean", new MBeanParameterInfo[] { new MBeanParameterInfo("name", "java.lang.String", "Create a logger MBean") }, "javax.management.ObjectName", 1);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void addAppenderEvent(final Category category, final Appender userData) {
        HierarchyDynamicMBean.log.debug(new StringBuffer().append("addAppenderEvent called: logger=").append(category.getName()).append(", appender=").append(userData.getName()).toString());
        final Notification notification = new Notification(new StringBuffer().append("addAppender.").append(category.getName()).toString(), this, 0L);
        notification.setUserData(userData);
        HierarchyDynamicMBean.log.debug("sending notification.");
        this.nbs.sendNotification(notification);
    }
    
    public ObjectName addLoggerMBean(final String s) {
        final Logger exists = LogManager.exists(s);
        if (exists != null) {
            return this.addLoggerMBean(exists);
        }
        return null;
    }
    
    ObjectName addLoggerMBean(final Logger p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: astore          5
        //     6: aconst_null    
        //     7: astore          4
        //     9: aconst_null    
        //    10: astore_3       
        //    11: new             Lorg/apache/log4j/jmx/LoggerDynamicMBean;
        //    14: dup            
        //    15: aload_1        
        //    16: invokespecial   org/apache/log4j/jmx/LoggerDynamicMBean.<init>:(Lorg/apache/log4j/Logger;)V
        //    19: astore          6
        //    21: new             Ljavax/management/ObjectName;
        //    24: dup            
        //    25: ldc             "log4j"
        //    27: ldc             "logger"
        //    29: aload           5
        //    31: invokespecial   javax/management/ObjectName.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //    34: astore_2       
        //    35: aload_0        
        //    36: getfield        org/apache/log4j/jmx/HierarchyDynamicMBean.server:Ljavax/management/MBeanServer;
        //    39: aload_2        
        //    40: invokeinterface javax/management/MBeanServer.isRegistered:(Ljavax/management/ObjectName;)Z
        //    45: ifne            196
        //    48: aload_0        
        //    49: aload           6
        //    51: aload_2        
        //    52: invokevirtual   org/apache/log4j/jmx/HierarchyDynamicMBean.registerMBean:(Ljava/lang/Object;Ljavax/management/ObjectName;)V
        //    55: new             Ljavax/management/NotificationFilterSupport;
        //    58: dup            
        //    59: invokespecial   javax/management/NotificationFilterSupport.<init>:()V
        //    62: astore_3       
        //    63: aload_3        
        //    64: new             Ljava/lang/StringBuffer;
        //    67: dup            
        //    68: invokespecial   java/lang/StringBuffer.<init>:()V
        //    71: ldc             "addAppender."
        //    73: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    76: aload_1        
        //    77: invokevirtual   org/apache/log4j/Logger.getName:()Ljava/lang/String;
        //    80: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    83: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    86: invokevirtual   javax/management/NotificationFilterSupport.enableType:(Ljava/lang/String;)V
        //    89: getstatic       org/apache/log4j/jmx/HierarchyDynamicMBean.log:Lorg/apache/log4j/Logger;
        //    92: new             Ljava/lang/StringBuffer;
        //    95: dup            
        //    96: invokespecial   java/lang/StringBuffer.<init>:()V
        //    99: ldc             "---Adding logger ["
        //   101: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   104: aload           5
        //   106: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   109: ldc             "] as listener."
        //   111: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   114: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   117: invokevirtual   org/apache/log4j/Logger.debug:(Ljava/lang/Object;)V
        //   120: aload_0        
        //   121: getfield        org/apache/log4j/jmx/HierarchyDynamicMBean.nbs:Ljavax/management/NotificationBroadcasterSupport;
        //   124: aload           6
        //   126: aload_3        
        //   127: aconst_null    
        //   128: invokevirtual   javax/management/NotificationBroadcasterSupport.addNotificationListener:(Ljavax/management/NotificationListener;Ljavax/management/NotificationFilter;Ljava/lang/Object;)V
        //   131: aload_0        
        //   132: getfield        org/apache/log4j/jmx/HierarchyDynamicMBean.vAttributes:Ljava/util/Vector;
        //   135: new             Ljavax/management/MBeanAttributeInfo;
        //   138: dup            
        //   139: new             Ljava/lang/StringBuffer;
        //   142: dup            
        //   143: invokespecial   java/lang/StringBuffer.<init>:()V
        //   146: ldc             "logger="
        //   148: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   151: aload           5
        //   153: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   156: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   159: ldc             "javax.management.ObjectName"
        //   161: new             Ljava/lang/StringBuffer;
        //   164: dup            
        //   165: invokespecial   java/lang/StringBuffer.<init>:()V
        //   168: ldc             "The "
        //   170: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   173: aload           5
        //   175: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   178: ldc             " logger."
        //   180: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   183: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   186: iconst_1       
        //   187: iconst_1       
        //   188: iconst_0       
        //   189: invokespecial   javax/management/MBeanAttributeInfo.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V
        //   192: invokevirtual   java/util/Vector.add:(Ljava/lang/Object;)Z
        //   195: pop            
        //   196: aload_2        
        //   197: areturn        
        //   198: astore_1       
        //   199: aload_3        
        //   200: astore_2       
        //   201: getstatic       org/apache/log4j/jmx/HierarchyDynamicMBean.log:Lorg/apache/log4j/Logger;
        //   204: new             Ljava/lang/StringBuffer;
        //   207: dup            
        //   208: invokespecial   java/lang/StringBuffer.<init>:()V
        //   211: ldc             "Could not add loggerMBean for ["
        //   213: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   216: aload           5
        //   218: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   221: ldc_w           "]."
        //   224: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   227: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   230: aload_1        
        //   231: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   234: aload_2        
        //   235: areturn        
        //   236: astore_1       
        //   237: aload           4
        //   239: astore_2       
        //   240: getstatic       org/apache/log4j/jmx/HierarchyDynamicMBean.log:Lorg/apache/log4j/Logger;
        //   243: new             Ljava/lang/StringBuffer;
        //   246: dup            
        //   247: invokespecial   java/lang/StringBuffer.<init>:()V
        //   250: ldc             "Could not add loggerMBean for ["
        //   252: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   255: aload           5
        //   257: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   260: ldc_w           "]."
        //   263: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   266: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   269: aload_1        
        //   270: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   273: aload_2        
        //   274: areturn        
        //   275: astore_1       
        //   276: goto            240
        //   279: astore_1       
        //   280: goto            201
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                          
        //  -----  -----  -----  -----  ------------------------------
        //  11     35     198    201    Ljavax/management/JMException;
        //  11     35     236    240    Ljava/lang/RuntimeException;
        //  35     196    279    283    Ljavax/management/JMException;
        //  35     196    275    279    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0196:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void addNotificationListener(final NotificationListener notificationListener, final NotificationFilter notificationFilter, final Object o) {
        this.nbs.addNotificationListener(notificationListener, notificationFilter, o);
    }
    
    @Override
    public Object getAttribute(final String s) throws AttributeNotFoundException, MBeanException, ReflectionException {
        if (s == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke a getter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        HierarchyDynamicMBean.log.debug(new StringBuffer().append("Called getAttribute with [").append(s).append("].").toString());
        if (s.equals("threshold")) {
            return this.hierarchy.getThreshold();
        }
        if (!s.startsWith("logger")) {
            goto Label_0220;
        }
        final int index = s.indexOf("%3D");
        String string = s;
        if (index > 0) {
            string = new StringBuffer().append(s.substring(0, index)).append('=').append(s.substring(index + 3)).toString();
        }
        try {
            return new ObjectName(new StringBuffer().append("log4j:").append(string).toString());
        }
        catch (JMException ex) {
            HierarchyDynamicMBean.log.error(new StringBuffer().append("Could not create ObjectName").append(string).toString());
        }
        catch (RuntimeException ex2) {
            HierarchyDynamicMBean.log.error(new StringBuffer().append("Could not create ObjectName").append(string).toString());
            goto Label_0220;
        }
    }
    
    @Override
    protected Logger getLogger() {
        return HierarchyDynamicMBean.log;
    }
    
    @Override
    public MBeanInfo getMBeanInfo() {
        final MBeanAttributeInfo[] array = new MBeanAttributeInfo[this.vAttributes.size()];
        this.vAttributes.toArray(array);
        return new MBeanInfo(this.dClassName, this.dDescription, array, this.dConstructors, this.dOperations, new MBeanNotificationInfo[0]);
    }
    
    @Override
    public MBeanNotificationInfo[] getNotificationInfo() {
        return this.nbs.getNotificationInfo();
    }
    
    @Override
    public Object invoke(final String s, final Object[] array, final String[] array2) throws MBeanException, ReflectionException {
        if (s == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Operation name cannot be null"), new StringBuffer().append("Cannot invoke a null operation in ").append(this.dClassName).toString());
        }
        if (s.equals("addLoggerMBean")) {
            return this.addLoggerMBean((String)array[0]);
        }
        throw new ReflectionException(new NoSuchMethodException(s), new StringBuffer().append("Cannot find the operation ").append(s).append(" in ").append(this.dClassName).toString());
    }
    
    @Override
    public void postRegister(final Boolean b) {
        HierarchyDynamicMBean.log.debug("postRegister is called.");
        this.hierarchy.addHierarchyEventListener(this);
        this.addLoggerMBean(this.hierarchy.getRootLogger());
    }
    
    @Override
    public void removeAppenderEvent(final Category category, final Appender appender) {
        HierarchyDynamicMBean.log.debug(new StringBuffer().append("removeAppenderCalled: logger=").append(category.getName()).append(", appender=").append(appender.getName()).toString());
    }
    
    @Override
    public void removeNotificationListener(final NotificationListener notificationListener) throws ListenerNotFoundException {
        this.nbs.removeNotificationListener(notificationListener);
    }
    
    @Override
    public void setAttribute(final Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        if (attribute == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute cannot be null"), new StringBuffer().append("Cannot invoke a setter of ").append(this.dClassName).append(" with null attribute").toString());
        }
        final String name = attribute.getName();
        final Object value = attribute.getValue();
        if (name == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke the setter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        if (name.equals("threshold")) {
            this.hierarchy.setThreshold(OptionConverter.toLevel((String)value, this.hierarchy.getThreshold()));
        }
    }
}
