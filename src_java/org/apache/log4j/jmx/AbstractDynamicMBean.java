package org.apache.log4j.jmx;

import org.apache.log4j.*;
import javax.management.*;
import java.util.*;

public abstract class AbstractDynamicMBean implements DynamicMBean, MBeanRegistration
{
    String dClassName;
    private final Vector mbeanList;
    MBeanServer server;
    
    public AbstractDynamicMBean() {
        this.mbeanList = new Vector();
    }
    
    protected static String getAppenderName(final Appender appender) {
        final String name = appender.getName();
        if (name != null) {
            final String string = name;
            if (name.trim().length() != 0) {
                return string;
            }
        }
        return appender.toString();
    }
    
    @Override
    public AttributeList getAttributes(final String[] array) {
        if (array == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("attributeNames[] cannot be null"), new StringBuffer().append("Cannot invoke a getter of ").append(this.dClassName).toString());
        }
        final AttributeList list = new AttributeList();
        if (array.length != 0) {
            int i = 0;
            while (i < array.length) {
                while (true) {
                    try {
                        list.add(new Attribute(array[i], this.getAttribute(array[i])));
                        ++i;
                    }
                    catch (JMException ex) {
                        ex.printStackTrace();
                        continue;
                    }
                    catch (RuntimeException ex2) {
                        ex2.printStackTrace();
                        continue;
                    }
                    break;
                }
            }
        }
        return list;
    }
    
    protected abstract Logger getLogger();
    
    @Override
    public void postDeregister() {
        this.getLogger().debug("postDeregister is called.");
    }
    
    @Override
    public void postRegister(final Boolean b) {
    }
    
    @Override
    public void preDeregister() {
        this.getLogger().debug("preDeregister called.");
        final Enumeration<ObjectName> elements = this.mbeanList.elements();
        while (elements.hasMoreElements()) {
            final ObjectName objectName = elements.nextElement();
            try {
                this.server.unregisterMBean(objectName);
            }
            catch (InstanceNotFoundException ex) {
                this.getLogger().warn(new StringBuffer().append("Missing MBean ").append(objectName.getCanonicalName()).toString());
            }
            catch (MBeanRegistrationException ex2) {
                this.getLogger().warn(new StringBuffer().append("Failed unregistering ").append(objectName.getCanonicalName()).toString());
            }
        }
    }
    
    @Override
    public ObjectName preRegister(final MBeanServer server, final ObjectName objectName) {
        this.getLogger().debug(new StringBuffer().append("preRegister called. Server=").append(server).append(", name=").append(objectName).toString());
        this.server = server;
        return objectName;
    }
    
    protected void registerMBean(final Object o, final ObjectName objectName) throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
        this.server.registerMBean(o, objectName);
        this.mbeanList.add(objectName);
    }
    
    @Override
    public AttributeList setAttributes(AttributeList iterator) {
        if (iterator == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("AttributeList attributes cannot be null"), new StringBuffer().append("Cannot invoke a setter of ").append(this.dClassName).toString());
        }
        final AttributeList list = new AttributeList();
        if (!iterator.isEmpty()) {
            iterator = (AttributeList)iterator.iterator();
            while (((Iterator)iterator).hasNext()) {
                final Attribute attribute = ((Iterator<Attribute>)iterator).next();
                try {
                    this.setAttribute(attribute);
                    final String name = attribute.getName();
                    list.add(new Attribute(name, this.getAttribute(name)));
                }
                catch (JMException ex) {
                    ex.printStackTrace();
                }
                catch (RuntimeException ex2) {
                    ex2.printStackTrace();
                }
            }
        }
        return list;
    }
}
