package org.apache.log4j.jmx;

import org.apache.log4j.helpers.*;
import java.util.*;
import org.apache.log4j.*;
import javax.management.*;

public class LoggerDynamicMBean extends AbstractDynamicMBean implements NotificationListener
{
    private static Logger cat;
    static Class class$org$apache$log4j$Appender;
    static Class class$org$apache$log4j$jmx$LoggerDynamicMBean;
    private Vector dAttributes;
    private String dClassName;
    private MBeanConstructorInfo[] dConstructors;
    private String dDescription;
    private MBeanOperationInfo[] dOperations;
    private Logger logger;
    
    static {
        Class class$org$apache$log4j$jmx$LoggerDynamicMBean;
        if (LoggerDynamicMBean.class$org$apache$log4j$jmx$LoggerDynamicMBean == null) {
            class$org$apache$log4j$jmx$LoggerDynamicMBean = (LoggerDynamicMBean.class$org$apache$log4j$jmx$LoggerDynamicMBean = class$("org.apache.log4j.jmx.LoggerDynamicMBean"));
        }
        else {
            class$org$apache$log4j$jmx$LoggerDynamicMBean = LoggerDynamicMBean.class$org$apache$log4j$jmx$LoggerDynamicMBean;
        }
        LoggerDynamicMBean.cat = Logger.getLogger(class$org$apache$log4j$jmx$LoggerDynamicMBean);
    }
    
    public LoggerDynamicMBean(final Logger logger) {
        this.dConstructors = new MBeanConstructorInfo[1];
        this.dOperations = new MBeanOperationInfo[1];
        this.dAttributes = new Vector();
        this.dClassName = this.getClass().getName();
        this.dDescription = "This MBean acts as a management facade for a org.apache.log4j.Logger instance.";
        this.logger = logger;
        this.buildDynamicMBeanInfo();
    }
    
    private void buildDynamicMBeanInfo() {
        this.dConstructors[0] = new MBeanConstructorInfo("HierarchyDynamicMBean(): Constructs a HierarchyDynamicMBean instance", this.getClass().getConstructors()[0]);
        this.dAttributes.add(new MBeanAttributeInfo("name", "java.lang.String", "The name of this Logger.", true, false, false));
        this.dAttributes.add(new MBeanAttributeInfo("priority", "java.lang.String", "The priority of this logger.", true, true, false));
        this.dOperations[0] = new MBeanOperationInfo("addAppender", "addAppender(): add an appender", new MBeanParameterInfo[] { new MBeanParameterInfo("class name", "java.lang.String", "add an appender to this logger"), new MBeanParameterInfo("appender name", "java.lang.String", "name of the appender") }, "void", 1);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    void addAppender(final String s, final String name) {
        LoggerDynamicMBean.cat.debug(new StringBuffer().append("addAppender called with ").append(s).append(", ").append(name).toString());
        Class class$org$apache$log4j$Appender;
        if (LoggerDynamicMBean.class$org$apache$log4j$Appender == null) {
            class$org$apache$log4j$Appender = (LoggerDynamicMBean.class$org$apache$log4j$Appender = class$("org.apache.log4j.Appender"));
        }
        else {
            class$org$apache$log4j$Appender = LoggerDynamicMBean.class$org$apache$log4j$Appender;
        }
        final Appender appender = (Appender)OptionConverter.instantiateByClassName(s, class$org$apache$log4j$Appender, null);
        appender.setName(name);
        this.logger.addAppender(appender);
    }
    
    void appenderMBeanRegistration() {
        final Enumeration allAppenders = this.logger.getAllAppenders();
        while (allAppenders.hasMoreElements()) {
            this.registerAppenderMBean(allAppenders.nextElement());
        }
    }
    
    @Override
    public Object getAttribute(final String s) throws AttributeNotFoundException, MBeanException, ReflectionException {
        if (s == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke a getter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        if (s.equals("name")) {
            return this.logger.getName();
        }
        if (s.equals("priority")) {
            final Level level = this.logger.getLevel();
            if (level == null) {
                return null;
            }
            return level.toString();
        }
        else {
            if (!s.startsWith("appender=")) {
                goto Label_0157;
            }
            try {
                return new ObjectName(new StringBuffer().append("log4j:").append(s).toString());
            }
            catch (MalformedObjectNameException ex) {
                LoggerDynamicMBean.cat.error(new StringBuffer().append("Could not create ObjectName").append(s).toString());
            }
            catch (RuntimeException ex2) {
                LoggerDynamicMBean.cat.error(new StringBuffer().append("Could not create ObjectName").append(s).toString());
                goto Label_0157;
            }
        }
    }
    
    @Override
    protected Logger getLogger() {
        return this.logger;
    }
    
    @Override
    public MBeanInfo getMBeanInfo() {
        final MBeanAttributeInfo[] array = new MBeanAttributeInfo[this.dAttributes.size()];
        this.dAttributes.toArray(array);
        return new MBeanInfo(this.dClassName, this.dDescription, array, this.dConstructors, this.dOperations, new MBeanNotificationInfo[0]);
    }
    
    @Override
    public void handleNotification(final Notification notification, final Object o) {
        LoggerDynamicMBean.cat.debug(new StringBuffer().append("Received notification: ").append(notification.getType()).toString());
        this.registerAppenderMBean((Appender)notification.getUserData());
    }
    
    @Override
    public Object invoke(final String s, final Object[] array, final String[] array2) throws MBeanException, ReflectionException {
        if (s.equals("addAppender")) {
            this.addAppender((String)array[0], (String)array[1]);
            return "Hello world.";
        }
        return null;
    }
    
    @Override
    public void postRegister(final Boolean b) {
        this.appenderMBeanRegistration();
    }
    
    void registerAppenderMBean(final Appender p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: astore_2       
        //     5: getstatic       org/apache/log4j/jmx/LoggerDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //     8: new             Ljava/lang/StringBuffer;
        //    11: dup            
        //    12: invokespecial   java/lang/StringBuffer.<init>:()V
        //    15: ldc_w           "Adding AppenderMBean for appender named "
        //    18: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    21: aload_2        
        //    22: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    25: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //    28: invokevirtual   org/apache/log4j/Logger.debug:(Ljava/lang/Object;)V
        //    31: new             Lorg/apache/log4j/jmx/AppenderDynamicMBean;
        //    34: dup            
        //    35: aload_1        
        //    36: invokespecial   org/apache/log4j/jmx/AppenderDynamicMBean.<init>:(Lorg/apache/log4j/Appender;)V
        //    39: astore_1       
        //    40: new             Ljavax/management/ObjectName;
        //    43: dup            
        //    44: ldc_w           "log4j"
        //    47: ldc_w           "appender"
        //    50: aload_2        
        //    51: invokespecial   javax/management/ObjectName.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //    54: astore_3       
        //    55: aload_0        
        //    56: getfield        org/apache/log4j/jmx/LoggerDynamicMBean.server:Ljavax/management/MBeanServer;
        //    59: aload_3        
        //    60: invokeinterface javax/management/MBeanServer.isRegistered:(Ljavax/management/ObjectName;)Z
        //    65: ifne            140
        //    68: aload_0        
        //    69: aload_1        
        //    70: aload_3        
        //    71: invokevirtual   org/apache/log4j/jmx/LoggerDynamicMBean.registerMBean:(Ljava/lang/Object;Ljavax/management/ObjectName;)V
        //    74: aload_0        
        //    75: getfield        org/apache/log4j/jmx/LoggerDynamicMBean.dAttributes:Ljava/util/Vector;
        //    78: new             Ljavax/management/MBeanAttributeInfo;
        //    81: dup            
        //    82: new             Ljava/lang/StringBuffer;
        //    85: dup            
        //    86: invokespecial   java/lang/StringBuffer.<init>:()V
        //    89: ldc             "appender="
        //    91: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    94: aload_2        
        //    95: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //    98: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   101: ldc_w           "javax.management.ObjectName"
        //   104: new             Ljava/lang/StringBuffer;
        //   107: dup            
        //   108: invokespecial   java/lang/StringBuffer.<init>:()V
        //   111: ldc_w           "The "
        //   114: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   117: aload_2        
        //   118: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   121: ldc_w           " appender."
        //   124: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   127: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   130: iconst_1       
        //   131: iconst_1       
        //   132: iconst_0       
        //   133: invokespecial   javax/management/MBeanAttributeInfo.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V
        //   136: invokevirtual   java/util/Vector.add:(Ljava/lang/Object;)Z
        //   139: pop            
        //   140: return         
        //   141: astore_1       
        //   142: getstatic       org/apache/log4j/jmx/LoggerDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //   145: new             Ljava/lang/StringBuffer;
        //   148: dup            
        //   149: invokespecial   java/lang/StringBuffer.<init>:()V
        //   152: ldc_w           "Could not add appenderMBean for ["
        //   155: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   158: aload_2        
        //   159: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   162: ldc_w           "]."
        //   165: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   168: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   171: aload_1        
        //   172: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   175: return         
        //   176: astore_1       
        //   177: getstatic       org/apache/log4j/jmx/LoggerDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //   180: new             Ljava/lang/StringBuffer;
        //   183: dup            
        //   184: invokespecial   java/lang/StringBuffer.<init>:()V
        //   187: ldc_w           "Could not add appenderMBean for ["
        //   190: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   193: aload_2        
        //   194: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   197: ldc_w           "]."
        //   200: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   203: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   206: aload_1        
        //   207: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   210: return         
        //   211: astore_1       
        //   212: getstatic       org/apache/log4j/jmx/LoggerDynamicMBean.cat:Lorg/apache/log4j/Logger;
        //   215: new             Ljava/lang/StringBuffer;
        //   218: dup            
        //   219: invokespecial   java/lang/StringBuffer.<init>:()V
        //   222: ldc_w           "Could not add appenderMBean for ["
        //   225: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   228: aload_2        
        //   229: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   232: ldc_w           "]."
        //   235: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   238: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   241: aload_1        
        //   242: invokevirtual   org/apache/log4j/Logger.error:(Ljava/lang/Object;Ljava/lang/Throwable;)V
        //   245: return         
        //   246: astore_1       
        //   247: goto            212
        //   250: astore_1       
        //   251: goto            177
        //   254: astore_1       
        //   255: goto            142
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                               
        //  -----  -----  -----  -----  -----------------------------------
        //  31     55     141    142    Ljavax/management/JMException;
        //  31     55     176    177    Ljava/beans/IntrospectionException;
        //  31     55     211    212    Ljava/lang/RuntimeException;
        //  55     140    254    258    Ljavax/management/JMException;
        //  55     140    250    254    Ljava/beans/IntrospectionException;
        //  55     140    246    250    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0140:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void setAttribute(final Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        if (attribute == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute cannot be null"), new StringBuffer().append("Cannot invoke a setter of ").append(this.dClassName).append(" with null attribute").toString());
        }
        final String name = attribute.getName();
        final Object value = attribute.getValue();
        if (name == null) {
            throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), new StringBuffer().append("Cannot invoke the setter of ").append(this.dClassName).append(" with null attribute name").toString());
        }
        if (name.equals("priority")) {
            if (value instanceof String) {
                final String s = (String)value;
                final Level level = this.logger.getLevel();
                Level level2;
                if (s.equalsIgnoreCase("NULL")) {
                    level2 = null;
                }
                else {
                    level2 = OptionConverter.toLevel(s, level);
                }
                this.logger.setLevel(level2);
            }
            return;
        }
        throw new AttributeNotFoundException(new StringBuffer().append("Attribute ").append(name).append(" not found in ").append(this.getClass().getName()).toString());
    }
}
