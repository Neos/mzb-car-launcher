package org.apache.log4j;

import org.apache.log4j.helpers.*;
import org.apache.log4j.spi.*;
import java.util.*;
import java.text.*;

public class AsyncAppender extends AppenderSkeleton implements AppenderAttachable
{
    public static final int DEFAULT_BUFFER_SIZE = 128;
    AppenderAttachableImpl aai;
    private final AppenderAttachableImpl appenders;
    private boolean blocking;
    private final List buffer;
    private int bufferSize;
    private final Map discardMap;
    private final Thread dispatcher;
    private boolean locationInfo;
    
    public AsyncAppender() {
        this.buffer = new ArrayList();
        this.discardMap = new HashMap();
        this.bufferSize = 128;
        this.locationInfo = false;
        this.blocking = true;
        this.appenders = new AppenderAttachableImpl();
        this.aai = this.appenders;
        (this.dispatcher = new Thread(new Dispatcher(this, this.buffer, this.discardMap, this.appenders))).setDaemon(true);
        this.dispatcher.setName(new StringBuffer().append("AsyncAppender-Dispatcher-").append(this.dispatcher.getName()).toString());
        this.dispatcher.start();
    }
    
    @Override
    public void addAppender(final Appender appender) {
        synchronized (this.appenders) {
            this.appenders.addAppender(appender);
        }
    }
    
    public void append(LoggingEvent loggingEvent) {
        if (this.dispatcher == null || !this.dispatcher.isAlive() || this.bufferSize <= 0) {
            synchronized (this.appenders) {
                this.appenders.appendLoopOnAppenders(loggingEvent);
                return;
            }
        }
        loggingEvent.getNDC();
        loggingEvent.getThreadName();
        loggingEvent.getMDCCopy();
        if (this.locationInfo) {
            loggingEvent.getLocationInformation();
        }
        loggingEvent.getRenderedMessage();
        loggingEvent.getThrowableStrRep();
        // monitorenter(this.buffer)
    Label_0195_Outer:
        while (true) {
            try {
                final int size = this.buffer.size();
                if (size < this.bufferSize) {
                    this.buffer.add(loggingEvent);
                    if (size == 0) {
                        this.buffer.notifyAll();
                    }
                    return;
                }
            }
            finally {
            }
            // monitorexit(this.buffer)
            boolean b2;
            final boolean b = b2 = true;
            DiscardSummary discardSummary;
            while (true) {
                if (!this.blocking) {
                    break Label_0195;
                }
                b2 = b;
                if (Thread.interrupted()) {
                    break Label_0195;
                }
                final Thread currentThread = Thread.currentThread();
                final Thread dispatcher = this.dispatcher;
                b2 = b;
                if (currentThread == dispatcher) {
                    break Label_0195;
                }
                try {
                    this.buffer.wait();
                    b2 = false;
                    if (!b2) {
                        continue Label_0195_Outer;
                    }
                    final String loggerName = loggingEvent.getLoggerName();
                    discardSummary = this.discardMap.get(loggerName);
                    if (discardSummary == null) {
                        loggingEvent = (LoggingEvent)new DiscardSummary(loggingEvent);
                        this.discardMap.put(loggerName, loggingEvent);
                        return;
                    }
                }
                catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    b2 = b;
                    continue;
                }
                break;
            }
            discardSummary.add(loggingEvent);
        }
    }
    
    @Override
    public void close() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/apache/log4j/AsyncAppender.buffer:Ljava/util/List;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: iconst_1       
        //     9: putfield        org/apache/log4j/AsyncAppender.closed:Z
        //    12: aload_0        
        //    13: getfield        org/apache/log4j/AsyncAppender.buffer:Ljava/util/List;
        //    16: invokevirtual   java/lang/Object.notifyAll:()V
        //    19: aload_1        
        //    20: monitorexit    
        //    21: aload_0        
        //    22: getfield        org/apache/log4j/AsyncAppender.dispatcher:Ljava/lang/Thread;
        //    25: invokevirtual   java/lang/Thread.join:()V
        //    28: aload_0        
        //    29: getfield        org/apache/log4j/AsyncAppender.appenders:Lorg/apache/log4j/helpers/AppenderAttachableImpl;
        //    32: astore_1       
        //    33: aload_1        
        //    34: monitorenter   
        //    35: aload_0        
        //    36: getfield        org/apache/log4j/AsyncAppender.appenders:Lorg/apache/log4j/helpers/AppenderAttachableImpl;
        //    39: invokevirtual   org/apache/log4j/helpers/AppenderAttachableImpl.getAllAppenders:()Ljava/util/Enumeration;
        //    42: astore_2       
        //    43: aload_2        
        //    44: ifnull          108
        //    47: aload_2        
        //    48: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //    53: ifeq            108
        //    56: aload_2        
        //    57: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //    62: astore_3       
        //    63: aload_3        
        //    64: instanceof      Lorg/apache/log4j/Appender;
        //    67: ifeq            47
        //    70: aload_3        
        //    71: checkcast       Lorg/apache/log4j/Appender;
        //    74: invokeinterface org/apache/log4j/Appender.close:()V
        //    79: goto            47
        //    82: astore_2       
        //    83: aload_1        
        //    84: monitorexit    
        //    85: aload_2        
        //    86: athrow         
        //    87: astore_2       
        //    88: aload_1        
        //    89: monitorexit    
        //    90: aload_2        
        //    91: athrow         
        //    92: astore_1       
        //    93: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //    96: invokevirtual   java/lang/Thread.interrupt:()V
        //    99: ldc             "Got an InterruptedException while waiting for the dispatcher to finish."
        //   101: aload_1        
        //   102: invokestatic    org/apache/log4j/helpers/LogLog.error:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   105: goto            28
        //   108: aload_1        
        //   109: monitorexit    
        //   110: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  7      21     87     92     Any
        //  21     28     92     108    Ljava/lang/InterruptedException;
        //  35     43     82     87     Any
        //  47     79     82     87     Any
        //  83     85     82     87     Any
        //  88     90     87     92     Any
        //  108    110    82     87     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0028:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Enumeration getAllAppenders() {
        synchronized (this.appenders) {
            return this.appenders.getAllAppenders();
        }
    }
    
    @Override
    public Appender getAppender(final String s) {
        synchronized (this.appenders) {
            return this.appenders.getAppender(s);
        }
    }
    
    public boolean getBlocking() {
        return this.blocking;
    }
    
    public int getBufferSize() {
        return this.bufferSize;
    }
    
    public boolean getLocationInfo() {
        return this.locationInfo;
    }
    
    @Override
    public boolean isAttached(final Appender appender) {
        synchronized (this.appenders) {
            return this.appenders.isAttached(appender);
        }
    }
    
    @Override
    public void removeAllAppenders() {
        synchronized (this.appenders) {
            this.appenders.removeAllAppenders();
        }
    }
    
    @Override
    public void removeAppender(final String s) {
        synchronized (this.appenders) {
            this.appenders.removeAppender(s);
        }
    }
    
    @Override
    public void removeAppender(final Appender appender) {
        synchronized (this.appenders) {
            this.appenders.removeAppender(appender);
        }
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
    
    public void setBlocking(final boolean blocking) {
        synchronized (this.buffer) {
            this.blocking = blocking;
            this.buffer.notifyAll();
        }
    }
    
    public void setBufferSize(final int n) {
        if (n < 0) {
            throw new NegativeArraySizeException("size");
        }
        final List buffer = this.buffer;
        // monitorenter(buffer)
        int bufferSize;
        if ((bufferSize = n) < 1) {
            bufferSize = 1;
        }
        try {
            this.bufferSize = bufferSize;
            this.buffer.notifyAll();
        }
        finally {
        }
        // monitorexit(buffer)
    }
    
    public void setLocationInfo(final boolean locationInfo) {
        this.locationInfo = locationInfo;
    }
    
    private static final class DiscardSummary
    {
        private int count;
        private LoggingEvent maxEvent;
        
        public DiscardSummary(final LoggingEvent maxEvent) {
            this.maxEvent = maxEvent;
            this.count = 1;
        }
        
        public void add(final LoggingEvent maxEvent) {
            if (maxEvent.getLevel().toInt() > this.maxEvent.getLevel().toInt()) {
                this.maxEvent = maxEvent;
            }
            ++this.count;
        }
        
        public LoggingEvent createEvent() {
            return new LoggingEvent("org.apache.log4j.AsyncAppender.DONT_REPORT_LOCATION", Logger.getLogger(this.maxEvent.getLoggerName()), this.maxEvent.getLevel(), MessageFormat.format("Discarded {0} messages due to full event buffer including: {1}", new Integer(this.count), this.maxEvent.getMessage()), null);
        }
    }
    
    private static class Dispatcher implements Runnable
    {
        private final AppenderAttachableImpl appenders;
        private final List buffer;
        private final Map discardMap;
        private final AsyncAppender parent;
        
        public Dispatcher(final AsyncAppender parent, final List buffer, final Map discardMap, final AppenderAttachableImpl appenders) {
            this.parent = parent;
            this.buffer = buffer;
            this.appenders = appenders;
            this.discardMap = discardMap;
        }
        
        @Override
        public void run() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: istore_2       
            //     2: iload_2        
            //     3: ifeq            255
            //     6: aconst_null    
            //     7: astore          4
            //     9: aload_0        
            //    10: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //    13: astore          5
            //    15: aload           5
            //    17: monitorenter   
            //    18: aload_0        
            //    19: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //    22: invokeinterface java/util/List.size:()I
            //    27: istore_2       
            //    28: aload_0        
            //    29: getfield        org/apache/log4j/AsyncAppender$Dispatcher.parent:Lorg/apache/log4j/AsyncAppender;
            //    32: getfield        org/apache/log4j/AsyncAppender.closed:Z
            //    35: ifne            267
            //    38: iconst_1       
            //    39: istore_1       
            //    40: iload_2        
            //    41: ifne            80
            //    44: iload_1        
            //    45: ifeq            80
            //    48: aload_0        
            //    49: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //    52: invokevirtual   java/lang/Object.wait:()V
            //    55: aload_0        
            //    56: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //    59: invokeinterface java/util/List.size:()I
            //    64: istore_2       
            //    65: aload_0        
            //    66: getfield        org/apache/log4j/AsyncAppender$Dispatcher.parent:Lorg/apache/log4j/AsyncAppender;
            //    69: getfield        org/apache/log4j/AsyncAppender.closed:Z
            //    72: ifne            272
            //    75: iconst_1       
            //    76: istore_1       
            //    77: goto            264
            //    80: iload_2        
            //    81: ifle            187
            //    84: aload_0        
            //    85: getfield        org/apache/log4j/AsyncAppender$Dispatcher.discardMap:Ljava/util/Map;
            //    88: invokeinterface java/util/Map.size:()I
            //    93: iload_2        
            //    94: iadd           
            //    95: anewarray       Lorg/apache/log4j/spi/LoggingEvent;
            //    98: astore          4
            //   100: aload_0        
            //   101: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //   104: aload           4
            //   106: invokeinterface java/util/List.toArray:([Ljava/lang/Object;)[Ljava/lang/Object;
            //   111: pop            
            //   112: aload_0        
            //   113: getfield        org/apache/log4j/AsyncAppender$Dispatcher.discardMap:Ljava/util/Map;
            //   116: invokeinterface java/util/Map.values:()Ljava/util/Collection;
            //   121: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
            //   126: astore          6
            //   128: aload           6
            //   130: invokeinterface java/util/Iterator.hasNext:()Z
            //   135: ifeq            162
            //   138: aload           4
            //   140: iload_2        
            //   141: aload           6
            //   143: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
            //   148: checkcast       Lorg/apache/log4j/AsyncAppender$DiscardSummary;
            //   151: invokevirtual   org/apache/log4j/AsyncAppender$DiscardSummary.createEvent:()Lorg/apache/log4j/spi/LoggingEvent;
            //   154: aastore        
            //   155: iload_2        
            //   156: iconst_1       
            //   157: iadd           
            //   158: istore_2       
            //   159: goto            128
            //   162: aload_0        
            //   163: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //   166: invokeinterface java/util/List.clear:()V
            //   171: aload_0        
            //   172: getfield        org/apache/log4j/AsyncAppender$Dispatcher.discardMap:Ljava/util/Map;
            //   175: invokeinterface java/util/Map.clear:()V
            //   180: aload_0        
            //   181: getfield        org/apache/log4j/AsyncAppender$Dispatcher.buffer:Ljava/util/List;
            //   184: invokevirtual   java/lang/Object.notifyAll:()V
            //   187: aload           5
            //   189: monitorexit    
            //   190: iload_1        
            //   191: istore_2       
            //   192: aload           4
            //   194: ifnull          2
            //   197: iconst_0       
            //   198: istore_3       
            //   199: iload_1        
            //   200: istore_2       
            //   201: iload_3        
            //   202: aload           4
            //   204: arraylength    
            //   205: if_icmpge       2
            //   208: aload_0        
            //   209: getfield        org/apache/log4j/AsyncAppender$Dispatcher.appenders:Lorg/apache/log4j/helpers/AppenderAttachableImpl;
            //   212: astore          5
            //   214: aload           5
            //   216: monitorenter   
            //   217: aload_0        
            //   218: getfield        org/apache/log4j/AsyncAppender$Dispatcher.appenders:Lorg/apache/log4j/helpers/AppenderAttachableImpl;
            //   221: aload           4
            //   223: iload_3        
            //   224: aaload         
            //   225: invokevirtual   org/apache/log4j/helpers/AppenderAttachableImpl.appendLoopOnAppenders:(Lorg/apache/log4j/spi/LoggingEvent;)I
            //   228: pop            
            //   229: aload           5
            //   231: monitorexit    
            //   232: iload_3        
            //   233: iconst_1       
            //   234: iadd           
            //   235: istore_3       
            //   236: goto            199
            //   239: astore          4
            //   241: aload           5
            //   243: monitorexit    
            //   244: aload           4
            //   246: athrow         
            //   247: astore          4
            //   249: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
            //   252: invokevirtual   java/lang/Thread.interrupt:()V
            //   255: return         
            //   256: astore          4
            //   258: aload           5
            //   260: monitorexit    
            //   261: aload           4
            //   263: athrow         
            //   264: goto            40
            //   267: iconst_0       
            //   268: istore_1       
            //   269: goto            40
            //   272: iconst_0       
            //   273: istore_1       
            //   274: goto            264
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                            
            //  -----  -----  -----  -----  --------------------------------
            //  9      18     247    255    Ljava/lang/InterruptedException;
            //  18     38     239    247    Any
            //  48     75     239    247    Any
            //  84     128    239    247    Any
            //  128    155    239    247    Any
            //  162    187    239    247    Any
            //  187    190    239    247    Any
            //  201    217    247    255    Ljava/lang/InterruptedException;
            //  217    232    256    264    Any
            //  241    244    239    247    Any
            //  244    247    247    255    Ljava/lang/InterruptedException;
            //  258    261    256    264    Any
            //  261    264    247    255    Ljava/lang/InterruptedException;
            // 
            // The error that occurred was:
            // 
            // java.lang.NullPointerException
            //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
            //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
            //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
            //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}
