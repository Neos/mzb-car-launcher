package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.spi.*;
import java.io.*;

public class LogManager
{
    public static final String CONFIGURATOR_CLASS_KEY = "log4j.configuratorClass";
    public static final String DEFAULT_CONFIGURATION_FILE = "log4j.properties";
    public static final String DEFAULT_CONFIGURATION_KEY = "log4j.configuration";
    public static final String DEFAULT_INIT_OVERRIDE_KEY = "log4j.defaultInitOverride";
    static final String DEFAULT_XML_CONFIGURATION_FILE = "log4j.xml";
    private static Object guard;
    private static RepositorySelector repositorySelector;
    
    static {
        LogManager.guard = null;
        LogManager.repositorySelector = new DefaultRepositorySelector(new Hierarchy(new RootLogger(Level.DEBUG)));
        final String systemProperty = OptionConverter.getSystemProperty("log4j.defaultInitOverride", null);
        if (systemProperty == null || "false".equalsIgnoreCase(systemProperty)) {
            final String systemProperty2 = OptionConverter.getSystemProperty("log4j.configuration", null);
            final String systemProperty3 = OptionConverter.getSystemProperty("log4j.configuratorClass", null);
            Label_0127: {
                if (systemProperty2 != null) {
                    break Label_0127;
                }
                URL url;
                if ((url = Loader.getResource("log4j.xml")) == null) {
                    url = Loader.getResource("log4j.properties");
                }
                while (true) {
                    if (url == null) {
                        break Label_0127;
                    }
                    LogLog.debug(new StringBuffer().append("Using URL [").append(url).append("] for automatic log4j configuration.").toString());
                    try {
                        OptionConverter.selectAndConfigure(url, systemProperty3, getLoggerRepository());
                        return;
                        try {
                            url = new URL(systemProperty2);
                        }
                        catch (MalformedURLException ex) {
                            url = Loader.getResource(systemProperty2);
                        }
                        continue;
                    }
                    catch (NoClassDefFoundError noClassDefFoundError) {
                        LogLog.warn("Error during default initialization", noClassDefFoundError);
                        return;
                    }
                    break;
                }
            }
            LogLog.debug(new StringBuffer().append("Could not find resource: [").append(systemProperty2).append("].").toString());
            return;
        }
        LogLog.debug("Default initialization of overridden by log4j.defaultInitOverrideproperty.");
    }
    
    public static Logger exists(final String s) {
        return getLoggerRepository().exists(s);
    }
    
    public static Enumeration getCurrentLoggers() {
        return getLoggerRepository().getCurrentLoggers();
    }
    
    public static Logger getLogger(final Class clazz) {
        return getLoggerRepository().getLogger(clazz.getName());
    }
    
    public static Logger getLogger(final String s) {
        return getLoggerRepository().getLogger(s);
    }
    
    public static Logger getLogger(final String s, final LoggerFactory loggerFactory) {
        return getLoggerRepository().getLogger(s, loggerFactory);
    }
    
    public static LoggerRepository getLoggerRepository() {
        if (LogManager.repositorySelector == null) {
            LogManager.repositorySelector = new DefaultRepositorySelector(new NOPLoggerRepository());
            LogManager.guard = null;
            final IllegalStateException ex = new IllegalStateException("Class invariant violation");
            if (isLikelySafeScenario(ex)) {
                LogLog.debug("log4j called after unloading, see http://logging.apache.org/log4j/1.2/faq.html#unload.", ex);
            }
            else {
                LogLog.error("log4j called after unloading, see http://logging.apache.org/log4j/1.2/faq.html#unload.", ex);
            }
        }
        return LogManager.repositorySelector.getLoggerRepository();
    }
    
    public static Logger getRootLogger() {
        return getLoggerRepository().getRootLogger();
    }
    
    private static boolean isLikelySafeScenario(final Exception ex) {
        final StringWriter stringWriter = new StringWriter();
        ex.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString().indexOf("org.apache.catalina.loader.WebappClassLoader.stop") != -1;
    }
    
    public static void resetConfiguration() {
        getLoggerRepository().resetConfiguration();
    }
    
    public static void setRepositorySelector(final RepositorySelector repositorySelector, final Object guard) throws IllegalArgumentException {
        if (LogManager.guard != null && LogManager.guard != guard) {
            throw new IllegalArgumentException("Attempted to reset the LoggerFactory without possessing the guard.");
        }
        if (repositorySelector == null) {
            throw new IllegalArgumentException("RepositorySelector must be non-null.");
        }
        LogManager.guard = guard;
        LogManager.repositorySelector = repositorySelector;
    }
    
    public static void shutdown() {
        getLoggerRepository().shutdown();
    }
}
