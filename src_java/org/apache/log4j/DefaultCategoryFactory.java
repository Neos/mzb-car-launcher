package org.apache.log4j;

import org.apache.log4j.spi.*;

class DefaultCategoryFactory implements LoggerFactory
{
    @Override
    public Logger makeNewLoggerInstance(final String s) {
        return new Logger(s);
    }
}
