package org.apache.log4j;

import org.apache.log4j.helpers.*;

class PropertyWatchdog extends FileWatchdog
{
    PropertyWatchdog(final String s) {
        super(s);
    }
    
    public void doOnChange() {
        new PropertyConfigurator().doConfigure(this.filename, LogManager.getLoggerRepository());
    }
}
