package org.apache.log4j;

import java.util.*;

class ProvisionNode extends Vector
{
    private static final long serialVersionUID = -4479121426311014469L;
    
    ProvisionNode(final Logger logger) {
        this.addElement(logger);
    }
}
