package org.apache.log4j;

import java.io.*;

public class Level extends Priority implements Serializable
{
    public static final Level ALL;
    public static final Level DEBUG;
    public static final Level ERROR;
    public static final Level FATAL;
    public static final Level INFO;
    public static final Level OFF;
    public static final Level TRACE;
    public static final int TRACE_INT = 5000;
    public static final Level WARN;
    static Class class$org$apache$log4j$Level;
    static final long serialVersionUID = 3491141966387921974L;
    
    static {
        OFF = new Level(Integer.MAX_VALUE, "OFF", 0);
        FATAL = new Level(50000, "FATAL", 0);
        ERROR = new Level(40000, "ERROR", 3);
        WARN = new Level(30000, "WARN", 4);
        INFO = new Level(20000, "INFO", 6);
        DEBUG = new Level(10000, "DEBUG", 7);
        TRACE = new Level(5000, "TRACE", 7);
        ALL = new Level(Integer.MIN_VALUE, "ALL", 7);
    }
    
    protected Level(final int n, final String s, final int n2) {
        super(n, s, n2);
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.level = objectInputStream.readInt();
        this.syslogEquivalent = objectInputStream.readInt();
        this.levelStr = objectInputStream.readUTF();
        if (this.levelStr == null) {
            this.levelStr = "";
        }
    }
    
    private Object readResolve() throws ObjectStreamException {
        final Class<? extends Level> class1 = this.getClass();
        Class class$org$apache$log4j$Level;
        if (Level.class$org$apache$log4j$Level == null) {
            class$org$apache$log4j$Level = (Level.class$org$apache$log4j$Level = class$("org.apache.log4j.Level"));
        }
        else {
            class$org$apache$log4j$Level = Level.class$org$apache$log4j$Level;
        }
        Level level = this;
        if (class1 == class$org$apache$log4j$Level) {
            level = toLevel(this.level);
        }
        return level;
    }
    
    public static Level toLevel(final int n) {
        return toLevel(n, Level.DEBUG);
    }
    
    public static Level toLevel(final int n, final Level level) {
        switch (n) {
            default: {
                return level;
            }
            case Integer.MIN_VALUE: {
                return Level.ALL;
            }
            case 10000: {
                return Level.DEBUG;
            }
            case 20000: {
                return Level.INFO;
            }
            case 30000: {
                return Level.WARN;
            }
            case 40000: {
                return Level.ERROR;
            }
            case 50000: {
                return Level.FATAL;
            }
            case Integer.MAX_VALUE: {
                return Level.OFF;
            }
            case 5000: {
                return Level.TRACE;
            }
        }
    }
    
    public static Level toLevel(final String s) {
        return toLevel(s, Level.DEBUG);
    }
    
    public static Level toLevel(String upperCase, final Level level) {
        if (upperCase != null) {
            upperCase = upperCase.toUpperCase();
            if (upperCase.equals("ALL")) {
                return Level.ALL;
            }
            if (upperCase.equals("DEBUG")) {
                return Level.DEBUG;
            }
            if (upperCase.equals("INFO")) {
                return Level.INFO;
            }
            if (upperCase.equals("WARN")) {
                return Level.WARN;
            }
            if (upperCase.equals("ERROR")) {
                return Level.ERROR;
            }
            if (upperCase.equals("FATAL")) {
                return Level.FATAL;
            }
            if (upperCase.equals("OFF")) {
                return Level.OFF;
            }
            if (upperCase.equals("TRACE")) {
                return Level.TRACE;
            }
            if (upperCase.equals("\u0130NFO")) {
                return Level.INFO;
            }
        }
        return level;
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.level);
        objectOutputStream.writeInt(this.syslogEquivalent);
        objectOutputStream.writeUTF(this.levelStr);
    }
}
