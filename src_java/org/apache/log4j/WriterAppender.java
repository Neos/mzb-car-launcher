package org.apache.log4j;

import org.apache.log4j.helpers.*;
import java.io.*;
import org.apache.log4j.spi.*;

public class WriterAppender extends AppenderSkeleton
{
    protected String encoding;
    protected boolean immediateFlush;
    protected QuietWriter qw;
    
    public WriterAppender() {
        this.immediateFlush = true;
    }
    
    public WriterAppender(final Layout layout, final OutputStream outputStream) {
        this(layout, new OutputStreamWriter(outputStream));
    }
    
    public WriterAppender(final Layout layout, final Writer writer) {
        this.immediateFlush = true;
        this.layout = layout;
        this.setWriter(writer);
    }
    
    @Override
    public void activateOptions() {
    }
    
    public void append(final LoggingEvent loggingEvent) {
        if (!this.checkEntryConditions()) {
            return;
        }
        this.subAppend(loggingEvent);
    }
    
    protected boolean checkEntryConditions() {
        if (this.closed) {
            LogLog.warn("Not allowed to write to a closed appender.");
            return false;
        }
        if (this.qw == null) {
            this.errorHandler.error(new StringBuffer().append("No output stream or file set for the appender named [").append(this.name).append("].").toString());
            return false;
        }
        if (this.layout == null) {
            this.errorHandler.error(new StringBuffer().append("No layout set for the appender named [").append(this.name).append("].").toString());
            return false;
        }
        return true;
    }
    
    @Override
    public void close() {
        synchronized (this) {
            if (!this.closed) {
                this.closed = true;
                this.writeFooter();
                this.reset();
            }
        }
    }
    
    protected void closeWriter() {
        if (this.qw == null) {
            return;
        }
        try {
            this.qw.close();
        }
        catch (IOException ex) {
            if (ex instanceof InterruptedIOException) {
                Thread.currentThread().interrupt();
            }
            LogLog.error(new StringBuffer().append("Could not close ").append(this.qw).toString(), ex);
        }
    }
    
    protected OutputStreamWriter createWriter(final OutputStream outputStream) {
        OutputStreamWriter outputStreamWriter = null;
        final String encoding = this.getEncoding();
        OutputStreamWriter outputStreamWriter2 = outputStreamWriter;
        while (true) {
            if (encoding == null) {
                break Label_0026;
            }
            try {
                outputStreamWriter2 = new OutputStreamWriter(outputStream, encoding);
                if ((outputStreamWriter = outputStreamWriter2) == null) {
                    outputStreamWriter = new OutputStreamWriter(outputStream);
                }
                return outputStreamWriter;
            }
            catch (IOException ex) {
                if (ex instanceof InterruptedIOException) {
                    Thread.currentThread().interrupt();
                }
                LogLog.warn("Error initializing output writer.");
                LogLog.warn("Unsupported encoding?");
                outputStreamWriter2 = outputStreamWriter;
                continue;
            }
            break;
        }
    }
    
    public String getEncoding() {
        return this.encoding;
    }
    
    public boolean getImmediateFlush() {
        return this.immediateFlush;
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    protected void reset() {
        this.closeWriter();
        this.qw = null;
    }
    
    public void setEncoding(final String encoding) {
        this.encoding = encoding;
    }
    
    @Override
    public void setErrorHandler(final ErrorHandler errorHandler) {
        // monitorenter(this)
        Label_0014: {
            if (errorHandler != null) {
                break Label_0014;
            }
            try {
                LogLog.warn("You have tried to set a null error-handler.");
                Label_0011: {
                    return;
                }
                while (true) {
                    this.qw.setErrorHandler(errorHandler);
                    return;
                    this.errorHandler = errorHandler;
                    continue;
                }
            }
            // iftrue(Label_0011:, this.qw == null)
            finally {
            }
            // monitorexit(this)
        }
    }
    
    public void setImmediateFlush(final boolean immediateFlush) {
        this.immediateFlush = immediateFlush;
    }
    
    public void setWriter(final Writer writer) {
        synchronized (this) {
            this.reset();
            this.qw = new QuietWriter(writer, this.errorHandler);
            this.writeHeader();
        }
    }
    
    protected boolean shouldFlush(final LoggingEvent loggingEvent) {
        return this.immediateFlush;
    }
    
    protected void subAppend(final LoggingEvent loggingEvent) {
        this.qw.write(this.layout.format(loggingEvent));
        if (this.layout.ignoresThrowable()) {
            final String[] throwableStrRep = loggingEvent.getThrowableStrRep();
            if (throwableStrRep != null) {
                for (int length = throwableStrRep.length, i = 0; i < length; ++i) {
                    this.qw.write(throwableStrRep[i]);
                    this.qw.write(Layout.LINE_SEP);
                }
            }
        }
        if (this.shouldFlush(loggingEvent)) {
            this.qw.flush();
        }
    }
    
    protected void writeFooter() {
        if (this.layout != null) {
            final String footer = this.layout.getFooter();
            if (footer != null && this.qw != null) {
                this.qw.write(footer);
                this.qw.flush();
            }
        }
    }
    
    protected void writeHeader() {
        if (this.layout != null) {
            final String header = this.layout.getHeader();
            if (header != null && this.qw != null) {
                this.qw.write(header);
            }
        }
    }
}
