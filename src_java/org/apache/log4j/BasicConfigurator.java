package org.apache.log4j;

public class BasicConfigurator
{
    public static void configure() {
        Logger.getRootLogger().addAppender(new ConsoleAppender(new PatternLayout("%r [%t] %p %c %x - %m%n")));
    }
    
    public static void configure(final Appender appender) {
        Logger.getRootLogger().addAppender(appender);
    }
    
    public static void resetConfiguration() {
        LogManager.resetConfiguration();
    }
}
