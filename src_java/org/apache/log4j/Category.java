package org.apache.log4j;

import org.apache.log4j.spi.*;
import org.apache.log4j.helpers.*;
import java.text.*;
import java.util.*;

public class Category implements AppenderAttachable
{
    private static final String FQCN;
    static Class class$org$apache$log4j$Category;
    AppenderAttachableImpl aai;
    protected boolean additive;
    protected volatile Level level;
    protected String name;
    protected volatile Category parent;
    protected LoggerRepository repository;
    protected ResourceBundle resourceBundle;
    
    static {
        Class class$org$apache$log4j$Category;
        if (Category.class$org$apache$log4j$Category == null) {
            class$org$apache$log4j$Category = (Category.class$org$apache$log4j$Category = class$("org.apache.log4j.Category"));
        }
        else {
            class$org$apache$log4j$Category = Category.class$org$apache$log4j$Category;
        }
        FQCN = class$org$apache$log4j$Category.getName();
    }
    
    protected Category(final String name) {
        this.additive = true;
        this.name = name;
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    public static Logger exists(final String s) {
        return LogManager.exists(s);
    }
    
    private void fireRemoveAppenderEvent(final Appender appender) {
        if (appender != null) {
            if (this.repository instanceof Hierarchy) {
                ((Hierarchy)this.repository).fireRemoveAppenderEvent(this, appender);
            }
            else if (this.repository instanceof HierarchyEventListener) {
                ((HierarchyEventListener)this.repository).removeAppenderEvent(this, appender);
            }
        }
    }
    
    public static Enumeration getCurrentCategories() {
        return LogManager.getCurrentLoggers();
    }
    
    public static LoggerRepository getDefaultHierarchy() {
        return LogManager.getLoggerRepository();
    }
    
    public static Category getInstance(final Class clazz) {
        return LogManager.getLogger(clazz);
    }
    
    public static Category getInstance(final String s) {
        return LogManager.getLogger(s);
    }
    
    public static final Category getRoot() {
        return LogManager.getRootLogger();
    }
    
    public static void shutdown() {
        LogManager.shutdown();
    }
    
    @Override
    public void addAppender(final Appender appender) {
        synchronized (this) {
            if (this.aai == null) {
                this.aai = new AppenderAttachableImpl();
            }
            this.aai.addAppender(appender);
            this.repository.fireAddAppenderEvent(this, appender);
        }
    }
    
    public void assertLog(final boolean b, final String s) {
        if (!b) {
            this.error(s);
        }
    }
    
    public void callAppenders(final LoggingEvent loggingEvent) {
        int n = 0;
        Category parent = this;
        while (true) {
            int n2 = n;
            Label_0048: {
                if (parent == null) {
                    break Label_0048;
                }
                n2 = n;
                try {
                    if (parent.aai != null) {
                        n2 = n + parent.aai.appendLoopOnAppenders(loggingEvent);
                    }
                    if (!parent.additive) {
                        // monitorexit(parent)
                        if (n2 == 0) {
                            this.repository.emitNoAppenderWarning(this);
                        }
                        return;
                    }
                    // monitorexit(parent)
                    parent = parent.parent;
                    n = n2;
                }
                finally {
                }
                // monitorexit(parent)
            }
        }
        // monitorenter(parent)
    }
    
    void closeNestedAppenders() {
        synchronized (this) {
            final Enumeration allAppenders = this.getAllAppenders();
            if (allAppenders != null) {
                while (allAppenders.hasMoreElements()) {
                    final Appender appender = allAppenders.nextElement();
                    if (appender instanceof AppenderAttachable) {
                        appender.close();
                    }
                }
            }
        }
    }
    // monitorexit(this)
    
    public void debug(final Object o) {
        if (!this.repository.isDisabled(10000) && Level.DEBUG.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.DEBUG, o, null);
        }
    }
    
    public void debug(final Object o, final Throwable t) {
        if (!this.repository.isDisabled(10000) && Level.DEBUG.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.DEBUG, o, t);
        }
    }
    
    public void error(final Object o) {
        if (!this.repository.isDisabled(40000) && Level.ERROR.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.ERROR, o, null);
        }
    }
    
    public void error(final Object o, final Throwable t) {
        if (!this.repository.isDisabled(40000) && Level.ERROR.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.ERROR, o, t);
        }
    }
    
    public void fatal(final Object o) {
        if (!this.repository.isDisabled(50000) && Level.FATAL.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.FATAL, o, null);
        }
    }
    
    public void fatal(final Object o, final Throwable t) {
        if (!this.repository.isDisabled(50000) && Level.FATAL.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.FATAL, o, t);
        }
    }
    
    protected void forcedLog(final String s, final Priority priority, final Object o, final Throwable t) {
        this.callAppenders(new LoggingEvent(s, this, priority, o, t));
    }
    
    public boolean getAdditivity() {
        return this.additive;
    }
    
    @Override
    public Enumeration getAllAppenders() {
        synchronized (this) {
            Enumeration enumeration;
            if (this.aai == null) {
                enumeration = NullEnumeration.getInstance();
            }
            else {
                enumeration = this.aai.getAllAppenders();
            }
            return enumeration;
        }
    }
    
    @Override
    public Appender getAppender(final String s) {
        synchronized (this) {
            Appender appender;
            if (this.aai == null || s == null) {
                appender = null;
            }
            else {
                appender = this.aai.getAppender(s);
            }
            return appender;
        }
    }
    
    public Priority getChainedPriority() {
        for (Category parent = this; parent != null; parent = parent.parent) {
            if (parent.level != null) {
                return parent.level;
            }
        }
        return null;
    }
    
    public Level getEffectiveLevel() {
        for (Category parent = this; parent != null; parent = parent.parent) {
            if (parent.level != null) {
                return parent.level;
            }
        }
        return null;
    }
    
    public LoggerRepository getHierarchy() {
        return this.repository;
    }
    
    public final Level getLevel() {
        return this.level;
    }
    
    public LoggerRepository getLoggerRepository() {
        return this.repository;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final Category getParent() {
        return this.parent;
    }
    
    public final Level getPriority() {
        return this.level;
    }
    
    public ResourceBundle getResourceBundle() {
        for (Category parent = this; parent != null; parent = parent.parent) {
            if (parent.resourceBundle != null) {
                return parent.resourceBundle;
            }
        }
        return null;
    }
    
    protected String getResourceBundleString(final String s) {
        final ResourceBundle resourceBundle = this.getResourceBundle();
        if (resourceBundle == null) {
            return null;
        }
        try {
            return resourceBundle.getString(s);
        }
        catch (MissingResourceException ex) {
            this.error(new StringBuffer().append("No resource is associated with key \"").append(s).append("\".").toString());
            return null;
        }
    }
    
    public void info(final Object o) {
        if (!this.repository.isDisabled(20000) && Level.INFO.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.INFO, o, null);
        }
    }
    
    public void info(final Object o, final Throwable t) {
        if (!this.repository.isDisabled(20000) && Level.INFO.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.INFO, o, t);
        }
    }
    
    @Override
    public boolean isAttached(final Appender appender) {
        return appender != null && this.aai != null && this.aai.isAttached(appender);
    }
    
    public boolean isDebugEnabled() {
        return !this.repository.isDisabled(10000) && Level.DEBUG.isGreaterOrEqual(this.getEffectiveLevel());
    }
    
    public boolean isEnabledFor(final Priority priority) {
        return !this.repository.isDisabled(priority.level) && priority.isGreaterOrEqual(this.getEffectiveLevel());
    }
    
    public boolean isInfoEnabled() {
        return !this.repository.isDisabled(20000) && Level.INFO.isGreaterOrEqual(this.getEffectiveLevel());
    }
    
    public void l7dlog(final Priority priority, final String s, final Throwable t) {
        if (!this.repository.isDisabled(priority.level) && priority.isGreaterOrEqual(this.getEffectiveLevel())) {
            String resourceBundleString;
            if ((resourceBundleString = this.getResourceBundleString(s)) == null) {
                resourceBundleString = s;
            }
            this.forcedLog(Category.FQCN, priority, resourceBundleString, t);
        }
    }
    
    public void l7dlog(final Priority priority, String format, final Object[] array, final Throwable t) {
        if (!this.repository.isDisabled(priority.level) && priority.isGreaterOrEqual(this.getEffectiveLevel())) {
            final String resourceBundleString = this.getResourceBundleString(format);
            if (resourceBundleString != null) {
                format = MessageFormat.format(resourceBundleString, array);
            }
            this.forcedLog(Category.FQCN, priority, format, t);
        }
    }
    
    public void log(final String s, final Priority priority, final Object o, final Throwable t) {
        if (!this.repository.isDisabled(priority.level) && priority.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(s, priority, o, t);
        }
    }
    
    public void log(final Priority priority, final Object o) {
        if (!this.repository.isDisabled(priority.level) && priority.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, priority, o, null);
        }
    }
    
    public void log(final Priority priority, final Object o, final Throwable t) {
        if (!this.repository.isDisabled(priority.level) && priority.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, priority, o, t);
        }
    }
    
    @Override
    public void removeAllAppenders() {
        Label_0099: {
            synchronized (this) {
                if (this.aai == null) {
                    break Label_0099;
                }
                final Vector<Object> vector = new Vector<Object>();
                final Enumeration allAppenders = this.aai.getAllAppenders();
                while (allAppenders != null && allAppenders.hasMoreElements()) {
                    vector.add(allAppenders.nextElement());
                }
            }
            this.aai.removeAllAppenders();
            final Vector<Appender> vector2;
            final Enumeration<Appender> elements = vector2.elements();
            while (elements.hasMoreElements()) {
                this.fireRemoveAppenderEvent(elements.nextElement());
            }
            this.aai = null;
        }
    }
    // monitorexit(this)
    
    @Override
    public void removeAppender(final String s) {
        // monitorenter(this)
        if (s == null) {
            return;
        }
        try {
            if (this.aai != null) {
                final Appender appender = this.aai.getAppender(s);
                this.aai.removeAppender(s);
                if (appender != null) {
                    this.fireRemoveAppenderEvent(appender);
                }
            }
        }
        finally {
        }
        // monitorexit(this)
    }
    
    @Override
    public void removeAppender(final Appender appender) {
        // monitorenter(this)
        if (appender == null) {
            return;
        }
        try {
            if (this.aai != null) {
                final boolean attached = this.aai.isAttached(appender);
                this.aai.removeAppender(appender);
                if (attached) {
                    this.fireRemoveAppenderEvent(appender);
                }
            }
        }
        finally {
        }
        // monitorexit(this)
    }
    
    public void setAdditivity(final boolean additive) {
        this.additive = additive;
    }
    
    final void setHierarchy(final LoggerRepository repository) {
        this.repository = repository;
    }
    
    public void setLevel(final Level level) {
        this.level = level;
    }
    
    public void setPriority(final Priority priority) {
        this.level = (Level)priority;
    }
    
    public void setResourceBundle(final ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }
    
    public void warn(final Object o) {
        if (!this.repository.isDisabled(30000) && Level.WARN.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.WARN, o, null);
        }
    }
    
    public void warn(final Object o, final Throwable t) {
        if (!this.repository.isDisabled(30000) && Level.WARN.isGreaterOrEqual(this.getEffectiveLevel())) {
            this.forcedLog(Category.FQCN, Level.WARN, o, t);
        }
    }
}
