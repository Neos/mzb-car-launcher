package org.apache.log4j;

class CategoryKey
{
    static Class class$org$apache$log4j$CategoryKey;
    int hashCache;
    String name;
    
    CategoryKey(final String name) {
        this.name = name;
        this.hashCache = name.hashCode();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null) {
            Class class$org$apache$log4j$CategoryKey;
            if (CategoryKey.class$org$apache$log4j$CategoryKey == null) {
                class$org$apache$log4j$CategoryKey = (CategoryKey.class$org$apache$log4j$CategoryKey = class$("org.apache.log4j.CategoryKey"));
            }
            else {
                class$org$apache$log4j$CategoryKey = CategoryKey.class$org$apache$log4j$CategoryKey;
            }
            if (class$org$apache$log4j$CategoryKey == o.getClass()) {
                return this.name.equals(((CategoryKey)o).name);
            }
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        return this.hashCache;
    }
}
