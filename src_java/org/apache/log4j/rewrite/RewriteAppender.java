package org.apache.log4j.rewrite;

import org.apache.log4j.helpers.*;
import org.apache.log4j.*;
import org.w3c.dom.*;
import java.util.*;
import org.apache.log4j.xml.*;
import org.apache.log4j.spi.*;

public class RewriteAppender extends AppenderSkeleton implements AppenderAttachable, UnrecognizedElementHandler
{
    static Class class$org$apache$log4j$rewrite$RewritePolicy;
    private final AppenderAttachableImpl appenders;
    private RewritePolicy policy;
    
    public RewriteAppender() {
        this.appenders = new AppenderAttachableImpl();
    }
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public void addAppender(final Appender appender) {
        synchronized (this.appenders) {
            this.appenders.addAppender(appender);
        }
    }
    
    @Override
    protected void append(final LoggingEvent loggingEvent) {
        LoggingEvent rewrite = loggingEvent;
        if (this.policy != null) {
            rewrite = this.policy.rewrite(loggingEvent);
        }
        if (rewrite != null) {
            synchronized (this.appenders) {
                this.appenders.appendLoopOnAppenders(rewrite);
            }
        }
    }
    
    @Override
    public void close() {
        this.closed = true;
        synchronized (this.appenders) {
            final Enumeration allAppenders = this.appenders.getAllAppenders();
            if (allAppenders != null) {
                while (allAppenders.hasMoreElements()) {
                    final Appender nextElement = allAppenders.nextElement();
                    if (nextElement instanceof Appender) {
                        nextElement.close();
                    }
                }
            }
        }
    }
    // monitorexit(appenderAttachableImpl)
    
    @Override
    public Enumeration getAllAppenders() {
        synchronized (this.appenders) {
            return this.appenders.getAllAppenders();
        }
    }
    
    @Override
    public Appender getAppender(final String s) {
        synchronized (this.appenders) {
            return this.appenders.getAppender(s);
        }
    }
    
    @Override
    public boolean isAttached(final Appender appender) {
        synchronized (this.appenders) {
            return this.appenders.isAttached(appender);
        }
    }
    
    @Override
    public boolean parseUnrecognizedElement(final Element element, final Properties properties) throws Exception {
        if ("rewritePolicy".equals(element.getNodeName())) {
            Class class$org$apache$log4j$rewrite$RewritePolicy;
            if (RewriteAppender.class$org$apache$log4j$rewrite$RewritePolicy == null) {
                class$org$apache$log4j$rewrite$RewritePolicy = (RewriteAppender.class$org$apache$log4j$rewrite$RewritePolicy = class$("org.apache.log4j.rewrite.RewritePolicy"));
            }
            else {
                class$org$apache$log4j$rewrite$RewritePolicy = RewriteAppender.class$org$apache$log4j$rewrite$RewritePolicy;
            }
            final Object element2 = DOMConfigurator.parseElement(element, properties, class$org$apache$log4j$rewrite$RewritePolicy);
            if (element2 != null) {
                if (element2 instanceof OptionHandler) {
                    ((OptionHandler)element2).activateOptions();
                }
                this.setRewritePolicy((RewritePolicy)element2);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public void removeAllAppenders() {
        synchronized (this.appenders) {
            this.appenders.removeAllAppenders();
        }
    }
    
    @Override
    public void removeAppender(final String s) {
        synchronized (this.appenders) {
            this.appenders.removeAppender(s);
        }
    }
    
    @Override
    public void removeAppender(final Appender appender) {
        synchronized (this.appenders) {
            this.appenders.removeAppender(appender);
        }
    }
    
    @Override
    public boolean requiresLayout() {
        return false;
    }
    
    public void setRewritePolicy(final RewritePolicy policy) {
        this.policy = policy;
    }
}
