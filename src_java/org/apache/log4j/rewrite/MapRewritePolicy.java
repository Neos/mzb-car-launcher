package org.apache.log4j.rewrite;

import org.apache.log4j.spi.*;
import java.util.*;
import org.apache.log4j.*;

public class MapRewritePolicy implements RewritePolicy
{
    @Override
    public LoggingEvent rewrite(final LoggingEvent loggingEvent) {
        final Object message = loggingEvent.getMessage();
        if (message instanceof Map) {
            final HashMap<Object, Object> hashMap = new HashMap<Object, Object>(loggingEvent.getProperties());
            final Map<K, Object> map = (Map<K, Object>)message;
            Object value;
            if ((value = map.get("message")) == null) {
                value = message;
            }
            for (final Map.Entry<K, Object> entry : map.entrySet()) {
                if (!"message".equals(entry.getKey())) {
                    hashMap.put(entry.getKey(), entry.getValue());
                }
            }
            final String fqnOfLoggerClass = loggingEvent.getFQNOfLoggerClass();
            Category category;
            if (loggingEvent.getLogger() != null) {
                category = loggingEvent.getLogger();
            }
            else {
                category = Logger.getLogger(loggingEvent.getLoggerName());
            }
            return new LoggingEvent(fqnOfLoggerClass, category, loggingEvent.getTimeStamp(), loggingEvent.getLevel(), value, loggingEvent.getThreadName(), loggingEvent.getThrowableInformation(), loggingEvent.getNDC(), loggingEvent.getLocationInformation(), hashMap);
        }
        return loggingEvent;
    }
}
