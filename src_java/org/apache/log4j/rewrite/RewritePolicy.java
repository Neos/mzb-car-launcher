package org.apache.log4j.rewrite;

import org.apache.log4j.spi.*;

public interface RewritePolicy
{
    LoggingEvent rewrite(final LoggingEvent p0);
}
