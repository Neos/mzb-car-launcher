package org.apache.log4j.rewrite;

import org.apache.log4j.spi.*;

public class ReflectionRewritePolicy implements RewritePolicy
{
    static Class class$java$lang$Object;
    
    static Class class$(final String s) {
        try {
            return Class.forName(s);
        }
        catch (ClassNotFoundException ex) {
            throw new NoClassDefFoundError().initCause(ex);
        }
    }
    
    @Override
    public LoggingEvent rewrite(final LoggingEvent p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   org/apache/log4j/spi/LoggingEvent.getMessage:()Ljava/lang/Object;
        //     4: astore          7
        //     6: aload           7
        //     8: instanceof      Ljava/lang/String;
        //    11: ifne            207
        //    14: aload           7
        //    16: astore          5
        //    18: new             Ljava/util/HashMap;
        //    21: dup            
        //    22: aload_1        
        //    23: invokevirtual   org/apache/log4j/spi/LoggingEvent.getProperties:()Ljava/util/Map;
        //    26: invokespecial   java/util/HashMap.<init>:(Ljava/util/Map;)V
        //    29: astore          8
        //    31: aload           7
        //    33: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //    36: astore          9
        //    38: getstatic       org/apache/log4j/rewrite/ReflectionRewritePolicy.class$java$lang$Object:Ljava/lang/Class;
        //    41: ifnonnull       135
        //    44: ldc             "java.lang.Object"
        //    46: invokestatic    org/apache/log4j/rewrite/ReflectionRewritePolicy.class$:(Ljava/lang/String;)Ljava/lang/Class;
        //    49: astore          6
        //    51: aload           6
        //    53: putstatic       org/apache/log4j/rewrite/ReflectionRewritePolicy.class$java$lang$Object:Ljava/lang/Class;
        //    56: aload           9
        //    58: aload           6
        //    60: invokestatic    java/beans/Introspector.getBeanInfo:(Ljava/lang/Class;Ljava/lang/Class;)Ljava/beans/BeanInfo;
        //    63: invokeinterface java/beans/BeanInfo.getPropertyDescriptors:()[Ljava/beans/PropertyDescriptor;
        //    68: astore          9
        //    70: aload           9
        //    72: arraylength    
        //    73: ifle            207
        //    76: iconst_0       
        //    77: istore_2       
        //    78: aload           9
        //    80: arraylength    
        //    81: istore_3       
        //    82: iload_2        
        //    83: iload_3        
        //    84: if_icmpge       209
        //    87: aload           9
        //    89: iload_2        
        //    90: aaload         
        //    91: invokevirtual   java/beans/PropertyDescriptor.getReadMethod:()Ljava/lang/reflect/Method;
        //    94: aload           7
        //    96: aconst_null    
        //    97: checkcast       [Ljava/lang/Object;
        //   100: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   103: astore          6
        //   105: ldc             "message"
        //   107: aload           9
        //   109: iload_2        
        //   110: aaload         
        //   111: invokevirtual   java/beans/PropertyDescriptor.getName:()Ljava/lang/String;
        //   114: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   117: istore          4
        //   119: iload           4
        //   121: ifeq            143
        //   124: aload           6
        //   126: astore          5
        //   128: iload_2        
        //   129: iconst_1       
        //   130: iadd           
        //   131: istore_2       
        //   132: goto            78
        //   135: getstatic       org/apache/log4j/rewrite/ReflectionRewritePolicy.class$java$lang$Object:Ljava/lang/Class;
        //   138: astore          6
        //   140: goto            56
        //   143: aload           8
        //   145: aload           9
        //   147: iload_2        
        //   148: aaload         
        //   149: invokevirtual   java/beans/PropertyDescriptor.getName:()Ljava/lang/String;
        //   152: aload           6
        //   154: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   159: pop            
        //   160: goto            128
        //   163: astore          6
        //   165: new             Ljava/lang/StringBuffer;
        //   168: dup            
        //   169: invokespecial   java/lang/StringBuffer.<init>:()V
        //   172: ldc             "Unable to evaluate property "
        //   174: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   177: aload           9
        //   179: iload_2        
        //   180: aaload         
        //   181: invokevirtual   java/beans/PropertyDescriptor.getName:()Ljava/lang/String;
        //   184: invokevirtual   java/lang/StringBuffer.append:(Ljava/lang/String;)Ljava/lang/StringBuffer;
        //   187: invokevirtual   java/lang/StringBuffer.toString:()Ljava/lang/String;
        //   190: aload           6
        //   192: invokestatic    org/apache/log4j/helpers/LogLog.warn:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   195: goto            128
        //   198: astore          5
        //   200: ldc             "Unable to get property descriptors"
        //   202: aload           5
        //   204: invokestatic    org/apache/log4j/helpers/LogLog.warn:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   207: aload_1        
        //   208: areturn        
        //   209: aload_1        
        //   210: invokevirtual   org/apache/log4j/spi/LoggingEvent.getFQNOfLoggerClass:()Ljava/lang/String;
        //   213: astore          7
        //   215: aload_1        
        //   216: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLogger:()Lorg/apache/log4j/Category;
        //   219: ifnull          268
        //   222: aload_1        
        //   223: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLogger:()Lorg/apache/log4j/Category;
        //   226: astore          6
        //   228: new             Lorg/apache/log4j/spi/LoggingEvent;
        //   231: dup            
        //   232: aload           7
        //   234: aload           6
        //   236: aload_1        
        //   237: invokevirtual   org/apache/log4j/spi/LoggingEvent.getTimeStamp:()J
        //   240: aload_1        
        //   241: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLevel:()Lorg/apache/log4j/Level;
        //   244: aload           5
        //   246: aload_1        
        //   247: invokevirtual   org/apache/log4j/spi/LoggingEvent.getThreadName:()Ljava/lang/String;
        //   250: aload_1        
        //   251: invokevirtual   org/apache/log4j/spi/LoggingEvent.getThrowableInformation:()Lorg/apache/log4j/spi/ThrowableInformation;
        //   254: aload_1        
        //   255: invokevirtual   org/apache/log4j/spi/LoggingEvent.getNDC:()Ljava/lang/String;
        //   258: aload_1        
        //   259: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLocationInformation:()Lorg/apache/log4j/spi/LocationInfo;
        //   262: aload           8
        //   264: invokespecial   org/apache/log4j/spi/LoggingEvent.<init>:(Ljava/lang/String;Lorg/apache/log4j/Category;JLorg/apache/log4j/Level;Ljava/lang/Object;Ljava/lang/String;Lorg/apache/log4j/spi/ThrowableInformation;Ljava/lang/String;Lorg/apache/log4j/spi/LocationInfo;Ljava/util/Map;)V
        //   267: areturn        
        //   268: aload_1        
        //   269: invokevirtual   org/apache/log4j/spi/LoggingEvent.getLoggerName:()Ljava/lang/String;
        //   272: invokestatic    org/apache/log4j/Logger.getLogger:(Ljava/lang/String;)Lorg/apache/log4j/Logger;
        //   275: astore          6
        //   277: goto            228
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  31     56     198    207    Ljava/lang/Exception;
        //  56     76     198    207    Ljava/lang/Exception;
        //  78     82     198    207    Ljava/lang/Exception;
        //  87     119    163    198    Ljava/lang/Exception;
        //  135    140    198    207    Ljava/lang/Exception;
        //  143    160    163    198    Ljava/lang/Exception;
        //  165    195    198    207    Ljava/lang/Exception;
        //  209    228    198    207    Ljava/lang/Exception;
        //  228    268    198    207    Ljava/lang/Exception;
        //  268    277    198    207    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
