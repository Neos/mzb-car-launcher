package org.apache.log4j.rewrite;

import org.apache.log4j.spi.*;
import org.apache.log4j.*;
import java.util.*;

public class PropertyRewritePolicy implements RewritePolicy
{
    private Map properties;
    
    public PropertyRewritePolicy() {
        this.properties = Collections.EMPTY_MAP;
    }
    
    @Override
    public LoggingEvent rewrite(final LoggingEvent loggingEvent) {
        if (!this.properties.isEmpty()) {
            final HashMap<Object, Object> hashMap = new HashMap<Object, Object>(loggingEvent.getProperties());
            for (final Map.Entry<Object, V> entry : this.properties.entrySet()) {
                if (!hashMap.containsKey(entry.getKey())) {
                    hashMap.put(entry.getKey(), entry.getValue());
                }
            }
            final String fqnOfLoggerClass = loggingEvent.getFQNOfLoggerClass();
            Category category;
            if (loggingEvent.getLogger() != null) {
                category = loggingEvent.getLogger();
            }
            else {
                category = Logger.getLogger(loggingEvent.getLoggerName());
            }
            return new LoggingEvent(fqnOfLoggerClass, category, loggingEvent.getTimeStamp(), loggingEvent.getLevel(), loggingEvent.getMessage(), loggingEvent.getThreadName(), loggingEvent.getThrowableInformation(), loggingEvent.getNDC(), loggingEvent.getLocationInformation(), hashMap);
        }
        return loggingEvent;
    }
    
    public void setProperties(final String s) {
        final HashMap<String, String> properties = new HashMap<String, String>();
        final StringTokenizer stringTokenizer = new StringTokenizer(s, ",");
        while (stringTokenizer.hasMoreTokens()) {
            final StringTokenizer stringTokenizer2 = new StringTokenizer(stringTokenizer.nextToken(), "=");
            properties.put(stringTokenizer2.nextElement().toString().trim(), stringTokenizer2.nextElement().toString().trim());
        }
        synchronized (this) {
            this.properties = properties;
        }
    }
}
