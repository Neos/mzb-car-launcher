package org.apache.log4j;

import org.apache.log4j.spi.*;
import java.util.*;
import java.io.*;

public final class DefaultThrowableRenderer implements ThrowableRenderer
{
    public static String[] render(final Throwable t) {
        Object o = new StringWriter();
        Closeable closeable = new PrintWriter((Writer)o);
        while (true) {
            try {
                t.printStackTrace((PrintWriter)closeable);
                ((PrintWriter)closeable).flush();
                closeable = new LineNumberReader(new StringReader(((StringWriter)o).toString()));
                o = new ArrayList();
                try {
                    for (String s = ((LineNumberReader)closeable).readLine(); s != null; s = ((LineNumberReader)closeable).readLine()) {
                        ((ArrayList<String>)o).add(s);
                    }
                }
                catch (IOException ex) {
                    if (ex instanceof InterruptedIOException) {
                        Thread.currentThread().interrupt();
                    }
                    ((ArrayList<String>)o).add(ex.toString());
                }
                final String[] array = new String[((ArrayList)o).size()];
                ((ArrayList<String>)o).toArray(array);
                return array;
            }
            catch (RuntimeException ex2) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public String[] doRender(final Throwable t) {
        return render(t);
    }
}
