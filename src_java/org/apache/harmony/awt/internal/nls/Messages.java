package org.apache.harmony.awt.internal.nls;

import java.util.*;
import java.security.*;

public class Messages
{
    private static ResourceBundle bundle;
    
    static {
        Messages.bundle = null;
        try {
            Messages.bundle = setLocale(Locale.getDefault(), "org.apache.harmony.awt.internal.nls.messages");
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public static String format(final String s, final Object[] array) {
        final StringBuilder sb = new StringBuilder(s.length() + array.length * 20);
        final String[] array2 = new String[array.length];
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == null) {
                array2[i] = "<null>";
            }
            else {
                array2[i] = array[i].toString();
            }
        }
        int length = 0;
        for (int j = s.indexOf(123, 0); j >= 0; j = s.indexOf(123, length)) {
            if (j != 0 && s.charAt(j - 1) == '\\') {
                if (j != 1) {
                    sb.append(s.substring(length, j - 1));
                }
                sb.append('{');
                length = j + 1;
            }
            else if (j > s.length() - 3) {
                sb.append(s.substring(length, s.length()));
                length = s.length();
            }
            else {
                final byte b = (byte)Character.digit(s.charAt(j + 1), 10);
                if (b < 0 || s.charAt(j + 2) != '}') {
                    sb.append(s.substring(length, j + 1));
                    length = j + 1;
                }
                else {
                    sb.append(s.substring(length, j));
                    if (b >= array2.length) {
                        sb.append("<missing argument>");
                    }
                    else {
                        sb.append(array2[b]);
                    }
                    length = j + 3;
                }
            }
        }
        if (length < s.length()) {
            sb.append(s.substring(length, s.length()));
        }
        return sb.toString();
    }
    
    public static String getString(final String s) {
        if (Messages.bundle == null) {
            return s;
        }
        try {
            return Messages.bundle.getString(s);
        }
        catch (MissingResourceException ex) {
            return "Missing message: " + s;
        }
    }
    
    public static String getString(final String s, final char c) {
        return getString(s, new Object[] { String.valueOf(c) });
    }
    
    public static String getString(final String s, final int n) {
        return getString(s, new Object[] { Integer.toString(n) });
    }
    
    public static String getString(final String s, final Object o) {
        return getString(s, new Object[] { o });
    }
    
    public static String getString(final String s, final Object o, final Object o2) {
        return getString(s, new Object[] { o, o2 });
    }
    
    public static String getString(final String s, final Object[] array) {
        String string = s;
        Label_0018: {
            if (Messages.bundle == null) {
                break Label_0018;
            }
            try {
                string = Messages.bundle.getString(s);
                return format(string, array);
            }
            catch (MissingResourceException ex) {
                string = s;
                return format(string, array);
            }
        }
    }
    
    public static ResourceBundle setLocale(final Locale locale, final String s) {
        try {
            return AccessController.doPrivileged((PrivilegedAction<ResourceBundle>)new PrivilegedAction<Object>() {
                @Override
                public Object run() {
                    final String val$resource = s;
                    final Locale val$locale = locale;
                    ClassLoader classLoader;
                    if (null != null) {
                        classLoader = null;
                    }
                    else {
                        classLoader = ClassLoader.getSystemClassLoader();
                    }
                    return ResourceBundle.getBundle(val$resource, val$locale, classLoader);
                }
            });
        }
        catch (MissingResourceException ex) {
            return null;
        }
    }
}
