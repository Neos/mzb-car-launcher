package org.apache.harmony.awt;

import org.apache.harmony.awt.datatransfer.*;
import java.awt.*;

public final class ContextStorage
{
    private static final ContextStorage globalContext;
    private final Object contextLock;
    private DTK dtk;
    private GraphicsEnvironment graphicsEnvironment;
    private volatile boolean shutdownPending;
    private Toolkit toolkit;
    
    static {
        globalContext = new ContextStorage();
    }
    
    public ContextStorage() {
        this.shutdownPending = false;
        this.contextLock = new ContextLock((ContextLock)null);
    }
    
    public static Object getContextLock() {
        return getCurrentContext().contextLock;
    }
    
    private static ContextStorage getCurrentContext() {
        return ContextStorage.globalContext;
    }
    
    public static DTK getDTK() {
        return getCurrentContext().dtk;
    }
    
    public static Toolkit getDefaultToolkit() {
        return getCurrentContext().toolkit;
    }
    
    public static GraphicsEnvironment getGraphicsEnvironment() {
        return getCurrentContext().graphicsEnvironment;
    }
    
    public static void setDTK(final DTK dtk) {
        getCurrentContext().dtk = dtk;
    }
    
    public static void setDefaultToolkit(final Toolkit toolkit) {
        getCurrentContext().toolkit = toolkit;
    }
    
    public static void setGraphicsEnvironment(final GraphicsEnvironment graphicsEnvironment) {
        getCurrentContext().graphicsEnvironment = graphicsEnvironment;
    }
    
    public static boolean shutdownPending() {
        return getCurrentContext().shutdownPending;
    }
    
    void shutdown() {
    }
    
    private class ContextLock
    {
    }
}
