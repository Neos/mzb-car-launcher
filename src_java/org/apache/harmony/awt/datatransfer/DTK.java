package org.apache.harmony.awt.datatransfer;

import org.apache.harmony.misc.*;
import org.apache.harmony.awt.internal.nls.*;
import org.apache.harmony.awt.*;
import java.awt.datatransfer.*;
import java.nio.charset.*;
import java.awt.dnd.*;
import java.awt.dnd.peer.*;

public abstract class DTK
{
    protected final DataTransferThread dataTransferThread;
    private NativeClipboard nativeClipboard;
    private NativeClipboard nativeSelection;
    protected SystemFlavorMap systemFlavorMap;
    
    protected DTK() {
        this.nativeClipboard = null;
        this.nativeSelection = null;
        (this.dataTransferThread = new DataTransferThread(this)).start();
    }
    
    private static DTK createDTK() {
        switch (SystemUtils.getOS()) {
            default: {
                throw new RuntimeException(Messages.getString("awt.4E"));
            }
            case 1: {
                final String s = "org.apache.harmony.awt.datatransfer.windows.WinDTK";
            }
            case 2: {
                Label_0053: {
                    break Label_0053;
                    try {
                        final String s;
                        return (DTK)Class.forName(s).newInstance();
                        s = "org.apache.harmony.awt.datatransfer.linux.LinuxDTK";
                        return (DTK)Class.forName(s).newInstance();
                    }
                    catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }
                break;
            }
        }
    }
    
    public static DTK getDTK() {
        synchronized (ContextStorage.getContextLock()) {
            if (ContextStorage.shutdownPending()) {
                return null;
            }
            DTK dtk;
            if ((dtk = ContextStorage.getDTK()) == null) {
                dtk = createDTK();
                ContextStorage.setDTK(dtk);
            }
            return dtk;
        }
    }
    
    protected void appendSystemFlavorMap(final SystemFlavorMap systemFlavorMap, final DataFlavor dataFlavor, final String s) {
        systemFlavorMap.addFlavorForUnencodedNative(s, dataFlavor);
        systemFlavorMap.addUnencodedNativeForFlavor(dataFlavor, s);
    }
    
    protected void appendSystemFlavorMap(final SystemFlavorMap systemFlavorMap, final String[] array, final String s, final String s2) {
        TextFlavor.addUnicodeClasses(systemFlavorMap, s2, s);
        for (int i = 0; i < array.length; ++i) {
            if (array[i] != null && Charset.isSupported(array[i])) {
                TextFlavor.addCharsetClasses(systemFlavorMap, s2, s, array[i]);
            }
        }
    }
    
    public abstract DragSourceContextPeer createDragSourceContextPeer(final DragGestureEvent p0);
    
    public abstract DropTargetContextPeer createDropTargetContextPeer(final DropTargetContext p0);
    
    protected String[] getCharsets() {
        return new String[] { "UTF-16", "UTF-8", "unicode", "ISO-8859-1", "US-ASCII" };
    }
    
    public String getDefaultCharset() {
        return "unicode";
    }
    
    public NativeClipboard getNativeClipboard() {
        if (this.nativeClipboard == null) {
            this.nativeClipboard = this.newNativeClipboard();
        }
        return this.nativeClipboard;
    }
    
    public NativeClipboard getNativeSelection() {
        if (this.nativeSelection == null) {
            this.nativeSelection = this.newNativeSelection();
        }
        return this.nativeSelection;
    }
    
    public SystemFlavorMap getSystemFlavorMap() {
        synchronized (this) {
            return this.systemFlavorMap;
        }
    }
    
    public abstract void initDragAndDrop();
    
    public void initSystemFlavorMap(final SystemFlavorMap systemFlavorMap) {
        final String[] charsets = this.getCharsets();
        this.appendSystemFlavorMap(systemFlavorMap, DataFlavor.stringFlavor, "text/plain");
        this.appendSystemFlavorMap(systemFlavorMap, charsets, "plain", "text/plain");
        this.appendSystemFlavorMap(systemFlavorMap, charsets, "html", "text/html");
        this.appendSystemFlavorMap(systemFlavorMap, DataProvider.urlFlavor, "application/x-java-url");
        this.appendSystemFlavorMap(systemFlavorMap, charsets, "uri-list", "application/x-java-url");
        this.appendSystemFlavorMap(systemFlavorMap, DataFlavor.javaFileListFlavor, "application/x-java-file-list");
        this.appendSystemFlavorMap(systemFlavorMap, DataFlavor.imageFlavor, "image/x-java-image");
    }
    
    protected abstract NativeClipboard newNativeClipboard();
    
    protected abstract NativeClipboard newNativeSelection();
    
    public abstract void runEventLoop();
    
    public void setSystemFlavorMap(final SystemFlavorMap systemFlavorMap) {
        synchronized (this) {
            this.systemFlavorMap = systemFlavorMap;
        }
    }
}
