package org.apache.harmony.awt.datatransfer;

public class DataTransferThread extends Thread
{
    private final DTK dtk;
    
    public DataTransferThread(final DTK dtk) {
        super("AWT-DataTransferThread");
        this.setDaemon(true);
        this.dtk = dtk;
    }
    
    @Override
    public void run() {
        // monitorenter(this)
        try {
            this.dtk.initDragAndDrop();
            final DataTransferThread dataTransferThread = this;
            dataTransferThread.notifyAll();
            final DataTransferThread dataTransferThread2 = this;
            // monitorexit(dataTransferThread2)
            final DataTransferThread dataTransferThread3 = this;
            final DTK dtk = dataTransferThread3.dtk;
            dtk.runEventLoop();
            return;
        }
        finally {
            this.notifyAll();
        }
        try {
            final DataTransferThread dataTransferThread = this;
            dataTransferThread.notifyAll();
            final DataTransferThread dataTransferThread2 = this;
            // monitorexit(dataTransferThread2)
            final DataTransferThread dataTransferThread3 = this;
            final DTK dtk = dataTransferThread3.dtk;
            dtk.runEventLoop();
        }
        finally {
        }
        // monitorexit(this)
    }
    
    @Override
    public void start() {
        synchronized (this) {
            super.start();
            try {
                this.wait();
            }
            catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
