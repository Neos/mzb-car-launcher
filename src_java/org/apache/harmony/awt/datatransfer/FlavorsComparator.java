package org.apache.harmony.awt.datatransfer;

import java.util.*;
import java.awt.datatransfer.*;

public class FlavorsComparator implements Comparator<DataFlavor>
{
    @Override
    public int compare(final DataFlavor dataFlavor, final DataFlavor dataFlavor2) {
        final int n = -1;
        int n2;
        if (!dataFlavor.isFlavorTextType() && !dataFlavor2.isFlavorTextType()) {
            n2 = 0;
        }
        else {
            if (!dataFlavor.isFlavorTextType()) {
                n2 = n;
                if (dataFlavor2.isFlavorTextType()) {
                    return n2;
                }
            }
            if (dataFlavor.isFlavorTextType() && !dataFlavor2.isFlavorTextType()) {
                return 1;
            }
            n2 = n;
            if (DataFlavor.selectBestTextFlavor(new DataFlavor[] { dataFlavor, dataFlavor2 }) != dataFlavor) {
                return 1;
            }
        }
        return n2;
    }
}
