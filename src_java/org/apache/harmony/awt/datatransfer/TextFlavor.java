package org.apache.harmony.awt.datatransfer;

import java.io.*;
import java.nio.*;
import java.awt.datatransfer.*;

public class TextFlavor
{
    public static final Class[] charsetTextClasses;
    public static final Class[] unicodeTextClasses;
    
    static {
        unicodeTextClasses = new Class[] { String.class, Reader.class, CharBuffer.class, char[].class };
        charsetTextClasses = new Class[] { InputStream.class, ByteBuffer.class, byte[].class };
    }
    
    public static void addCharsetClasses(final SystemFlavorMap systemFlavorMap, final String s, final String s2, final String s3) {
        for (int i = 0; i < TextFlavor.charsetTextClasses.length; ++i) {
            final String string = "text/" + s2;
            final DataFlavor dataFlavor = new DataFlavor(String.valueOf(string) + (";class=\"" + TextFlavor.charsetTextClasses[i].getName() + "\"" + ";charset=\"" + s3 + "\""), string);
            systemFlavorMap.addFlavorForUnencodedNative(s, dataFlavor);
            systemFlavorMap.addUnencodedNativeForFlavor(dataFlavor, s);
        }
    }
    
    public static void addUnicodeClasses(final SystemFlavorMap systemFlavorMap, final String s, final String s2) {
        for (int i = 0; i < TextFlavor.unicodeTextClasses.length; ++i) {
            final String string = "text/" + s2;
            final DataFlavor dataFlavor = new DataFlavor(String.valueOf(string) + (";class=\"" + TextFlavor.unicodeTextClasses[i].getName() + "\""), string);
            systemFlavorMap.addFlavorForUnencodedNative(s, dataFlavor);
            systemFlavorMap.addUnencodedNativeForFlavor(dataFlavor, s);
        }
    }
}
