package org.apache.harmony.awt.datatransfer;

import java.awt.datatransfer.*;

public abstract class NativeClipboard extends Clipboard
{
    protected static final int OPS_TIMEOUT = 10000;
    
    public NativeClipboard(final String s) {
        super(s);
    }
    
    public void onRestart() {
    }
    
    public void onShutdown() {
    }
}
