package org.apache.harmony.awt.datatransfer;

import java.util.*;
import java.io.*;
import java.awt.datatransfer.*;

public class DataSnapshot implements DataProvider
{
    private final String[] fileList;
    private final String html;
    private final String[] nativeFormats;
    private final RawBitmap rawBitmap;
    private final Map<Class<?>, byte[]> serializedObjects;
    private final String text;
    private final String url;
    
    public DataSnapshot(final DataProvider dataProvider) {
        this.nativeFormats = dataProvider.getNativeFormats();
        this.text = dataProvider.getText();
        this.fileList = dataProvider.getFileList();
        this.url = dataProvider.getURL();
        this.html = dataProvider.getHTML();
        this.rawBitmap = dataProvider.getRawBitmap();
        this.serializedObjects = Collections.synchronizedMap(new HashMap<Class<?>, byte[]>());
        int i = 0;
        while (i < this.nativeFormats.length) {
            Serializable s = null;
            while (true) {
                try {
                    s = SystemFlavorMap.decodeDataFlavor(this.nativeFormats[i]);
                    if (s != null) {
                        s = ((DataFlavor)s).getRepresentationClass();
                        final byte[] serializedObject = dataProvider.getSerializedObject((Class<?>)s);
                        if (serializedObject != null) {
                            this.serializedObjects.put((Class<?>)s, serializedObject);
                        }
                    }
                    ++i;
                }
                catch (ClassNotFoundException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    @Override
    public String[] getFileList() {
        return this.fileList;
    }
    
    @Override
    public String getHTML() {
        return this.html;
    }
    
    @Override
    public String[] getNativeFormats() {
        return this.nativeFormats;
    }
    
    @Override
    public RawBitmap getRawBitmap() {
        return this.rawBitmap;
    }
    
    public short[] getRawBitmapBuffer16() {
        if (this.rawBitmap != null && this.rawBitmap.buffer instanceof short[]) {
            return (short[])this.rawBitmap.buffer;
        }
        return null;
    }
    
    public int[] getRawBitmapBuffer32() {
        if (this.rawBitmap != null && this.rawBitmap.buffer instanceof int[]) {
            return (int[])this.rawBitmap.buffer;
        }
        return null;
    }
    
    public byte[] getRawBitmapBuffer8() {
        if (this.rawBitmap != null && this.rawBitmap.buffer instanceof byte[]) {
            return (byte[])this.rawBitmap.buffer;
        }
        return null;
    }
    
    public int[] getRawBitmapHeader() {
        if (this.rawBitmap != null) {
            return this.rawBitmap.getHeader();
        }
        return null;
    }
    
    @Override
    public byte[] getSerializedObject(final Class<?> clazz) {
        return this.serializedObjects.get(clazz);
    }
    
    public byte[] getSerializedObject(final String s) {
        try {
            return this.getSerializedObject(SystemFlavorMap.decodeDataFlavor(s).getRepresentationClass());
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    @Override
    public String getText() {
        return this.text;
    }
    
    @Override
    public String getURL() {
        return this.url;
    }
    
    @Override
    public boolean isNativeFormatAvailable(final String s) {
        if (s != null) {
            if (s.equals("text/plain")) {
                if (this.text != null) {
                    return true;
                }
            }
            else if (s.equals("application/x-java-file-list")) {
                if (this.fileList != null) {
                    return true;
                }
            }
            else if (s.equals("application/x-java-url")) {
                if (this.url != null) {
                    return true;
                }
            }
            else if (s.equals("text/html")) {
                if (this.html != null) {
                    return true;
                }
            }
            else if (s.equals("image/x-java-image")) {
                if (this.rawBitmap != null) {
                    return true;
                }
            }
            else {
                try {
                    return this.serializedObjects.containsKey(SystemFlavorMap.decodeDataFlavor(s).getRepresentationClass());
                }
                catch (Exception ex) {
                    return false;
                }
            }
        }
        return false;
    }
}
