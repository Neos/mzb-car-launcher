package org.apache.harmony.awt.datatransfer;

public final class RawBitmap
{
    public final int bMask;
    public final int bits;
    public final Object buffer;
    public final int gMask;
    public final int height;
    public final int rMask;
    public final int stride;
    public final int width;
    
    public RawBitmap(final int width, final int height, final int stride, final int bits, final int rMask, final int gMask, final int bMask, final Object buffer) {
        this.width = width;
        this.height = height;
        this.stride = stride;
        this.bits = bits;
        this.rMask = rMask;
        this.gMask = gMask;
        this.bMask = bMask;
        this.buffer = buffer;
    }
    
    public RawBitmap(final int[] array, final Object buffer) {
        this.width = array[0];
        this.height = array[1];
        this.stride = array[2];
        this.bits = array[3];
        this.rMask = array[4];
        this.gMask = array[5];
        this.bMask = array[6];
        this.buffer = buffer;
    }
    
    public int[] getHeader() {
        return new int[] { this.width, this.height, this.stride, this.bits, this.rMask, this.gMask, this.bMask };
    }
}
