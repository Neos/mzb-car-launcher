package org.apache.harmony.awt.datatransfer;

import java.awt.*;
import java.awt.dnd.*;

public class DragSourceEventProxy implements Runnable
{
    public static final int DRAG_ACTION_CHANGED = 3;
    public static final int DRAG_DROP_END = 6;
    public static final int DRAG_ENTER = 1;
    public static final int DRAG_EXIT = 5;
    public static final int DRAG_MOUSE_MOVED = 4;
    public static final int DRAG_OVER = 2;
    private final DragSourceContext context;
    private final int modifiers;
    private final boolean success;
    private final int targetActions;
    private final int type;
    private final int userAction;
    private final int x;
    private final int y;
    
    public DragSourceEventProxy(final DragSourceContext context, final int type, final int userAction, final int targetActions, final Point point, final int modifiers) {
        this.context = context;
        this.type = type;
        this.userAction = userAction;
        this.targetActions = targetActions;
        this.x = point.x;
        this.y = point.y;
        this.modifiers = modifiers;
        this.success = false;
    }
    
    public DragSourceEventProxy(final DragSourceContext context, final int type, final int n, final boolean success, final Point point, final int modifiers) {
        this.context = context;
        this.type = type;
        this.userAction = n;
        this.targetActions = n;
        this.x = point.x;
        this.y = point.y;
        this.modifiers = modifiers;
        this.success = success;
    }
    
    private DragSourceDragEvent newDragSourceDragEvent() {
        return new DragSourceDragEvent(this.context, this.userAction, this.targetActions, this.modifiers, this.x, this.y);
    }
    
    @Override
    public void run() {
        switch (this.type) {
            default: {}
            case 1: {
                this.context.dragEnter(this.newDragSourceDragEvent());
            }
            case 2: {
                this.context.dragOver(this.newDragSourceDragEvent());
            }
            case 3: {
                this.context.dropActionChanged(this.newDragSourceDragEvent());
            }
            case 4: {
                this.context.dragMouseMoved(this.newDragSourceDragEvent());
            }
            case 5: {
                this.context.dragExit(new DragSourceEvent(this.context, this.x, this.y));
            }
            case 6: {
                this.context.dragExit(new DragSourceDropEvent(this.context, this.userAction, this.success, this.x, this.y));
            }
        }
    }
}
