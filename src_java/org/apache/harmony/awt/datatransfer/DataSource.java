package org.apache.harmony.awt.datatransfer;

import java.awt.*;
import java.awt.image.*;
import java.awt.datatransfer.*;
import java.util.*;
import java.io.*;
import java.net.*;

public class DataSource implements DataProvider
{
    protected final Transferable contents;
    private DataFlavor[] flavors;
    private List<String> nativeFormats;
    
    public DataSource(final Transferable contents) {
        this.contents = contents;
    }
    
    private RawBitmap getImageBitmap(final Image image) {
        final RawBitmap rawBitmap = null;
        Label_0036: {
            if (!(image instanceof BufferedImage)) {
                break Label_0036;
            }
            final BufferedImage bufferedImage = (BufferedImage)image;
            if (bufferedImage.getType() != 1) {
                break Label_0036;
            }
            return this.getImageBitmap32(bufferedImage);
        }
        final int width = image.getWidth(null);
        final int height = image.getHeight(null);
        RawBitmap imageBitmap32 = rawBitmap;
        if (width <= 0) {
            return imageBitmap32;
        }
        imageBitmap32 = rawBitmap;
        if (height > 0) {
            final BufferedImage bufferedImage2 = new BufferedImage(width, height, 1);
            final Graphics graphics = bufferedImage2.getGraphics();
            graphics.drawImage(image, 0, 0, null);
            graphics.dispose();
            return this.getImageBitmap32(bufferedImage2);
        }
        return imageBitmap32;
    }
    
    private RawBitmap getImageBitmap32(final BufferedImage bufferedImage) {
        final int[] array = new int[bufferedImage.getWidth() * bufferedImage.getHeight()];
        final DataBufferInt dataBufferInt = (DataBufferInt)bufferedImage.getRaster().getDataBuffer();
        int n = 0;
        final int numBanks = dataBufferInt.getNumBanks();
        final int[] offsets = dataBufferInt.getOffsets();
        for (int i = 0; i < numBanks; ++i) {
            final int[] data = dataBufferInt.getData(i);
            System.arraycopy(data, offsets[i], array, n, data.length - offsets[i]);
            n += data.length - offsets[i];
        }
        return new RawBitmap(bufferedImage.getWidth(), bufferedImage.getHeight(), bufferedImage.getWidth(), 32, 16711680, 65280, 255, array);
    }
    
    private static List<String> getNativesForFlavors(final DataFlavor[] array) {
        final ArrayList<String> list = new ArrayList<String>();
        final SystemFlavorMap systemFlavorMap = (SystemFlavorMap)SystemFlavorMap.getDefaultFlavorMap();
        for (int i = 0; i < array.length; ++i) {
            for (final String s : systemFlavorMap.getNativesForFlavor(array[i])) {
                if (!list.contains(s)) {
                    list.add(s);
                }
            }
        }
        return list;
    }
    
    private String getText(final boolean b) {
        final DataFlavor[] transferDataFlavors = this.contents.getTransferDataFlavors();
        for (int i = 0; i < transferDataFlavors.length; ++i) {
            final DataFlavor dataFlavor = transferDataFlavors[i];
            if (dataFlavor.isFlavorTextType()) {
                if (b) {
                    if (!this.isHtmlFlavor(dataFlavor)) {
                        continue;
                    }
                }
                try {
                    if (String.class.isAssignableFrom(dataFlavor.getRepresentationClass())) {
                        return (String)this.contents.getTransferData(dataFlavor);
                    }
                    return this.getTextFromReader(dataFlavor.getReaderForText(this.contents));
                }
                catch (Exception ex) {}
            }
        }
        return null;
    }
    
    private String getTextFromReader(final Reader reader) throws IOException {
        final StringBuilder sb = new StringBuilder();
        final char[] array = new char[1024];
        while (true) {
            final int read = reader.read(array);
            if (read <= 0) {
                break;
            }
            sb.append(array, 0, read);
        }
        return sb.toString();
    }
    
    private boolean isHtmlFlavor(final DataFlavor dataFlavor) {
        return "html".equalsIgnoreCase(dataFlavor.getSubType());
    }
    
    protected DataFlavor[] getDataFlavors() {
        if (this.flavors == null) {
            this.flavors = this.contents.getTransferDataFlavors();
        }
        return this.flavors;
    }
    
    @Override
    public String[] getFileList() {
        try {
            final List list = (List)this.contents.getTransferData(DataFlavor.javaFileListFlavor);
            return list.toArray(new String[list.size()]);
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    @Override
    public String getHTML() {
        return this.getText(true);
    }
    
    @Override
    public String[] getNativeFormats() {
        return this.getNativeFormatsList().toArray(new String[0]);
    }
    
    public List<String> getNativeFormatsList() {
        if (this.nativeFormats == null) {
            this.nativeFormats = getNativesForFlavors(this.getDataFlavors());
        }
        return this.nativeFormats;
    }
    
    @Override
    public RawBitmap getRawBitmap() {
        final DataFlavor[] transferDataFlavors = this.contents.getTransferDataFlavors();
        for (int i = 0; i < transferDataFlavors.length; ++i) {
            final DataFlavor dataFlavor = transferDataFlavors[i];
            final Class<?> representationClass = dataFlavor.getRepresentationClass();
            if (representationClass != null && Image.class.isAssignableFrom(representationClass)) {
                if (!dataFlavor.isMimeTypeEqual(DataFlavor.imageFlavor)) {
                    if (!dataFlavor.isFlavorSerializedObjectType()) {
                        continue;
                    }
                }
                try {
                    return this.getImageBitmap((Image)this.contents.getTransferData(dataFlavor));
                }
                catch (Throwable t) {}
            }
        }
        return null;
    }
    
    @Override
    public byte[] getSerializedObject(final Class<?> clazz) {
        try {
            final Serializable s = (Serializable)this.contents.getTransferData(new DataFlavor(clazz, null));
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            new ObjectOutputStream(byteArrayOutputStream).writeObject(s);
            return byteArrayOutputStream.toByteArray();
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    @Override
    public String getText() {
        return this.getText(false);
    }
    
    @Override
    public String getURL() {
        try {
            return ((URL)this.contents.getTransferData(DataSource.urlFlavor)).toString();
        }
        catch (Exception ex) {
            try {
                return ((URL)this.contents.getTransferData(DataSource.uriFlavor)).toString();
            }
            catch (Exception ex2) {
                try {
                    return new URL(this.getText()).toString();
                }
                catch (Exception ex3) {
                    return null;
                }
            }
        }
    }
    
    @Override
    public boolean isNativeFormatAvailable(final String s) {
        return this.getNativeFormatsList().contains(s);
    }
}
