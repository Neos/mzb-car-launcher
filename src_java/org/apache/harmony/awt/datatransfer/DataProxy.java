package org.apache.harmony.awt.datatransfer;

import java.nio.*;
import java.awt.color.*;
import java.awt.image.*;
import java.awt.datatransfer.*;
import org.apache.harmony.awt.internal.nls.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;

public final class DataProxy implements Transferable
{
    public static final Class[] charsetTextClasses;
    public static final Class[] unicodeTextClasses;
    private final DataProvider data;
    private final SystemFlavorMap flavorMap;
    
    static {
        unicodeTextClasses = new Class[] { String.class, Reader.class, CharBuffer.class, char[].class };
        charsetTextClasses = new Class[] { byte[].class, ByteBuffer.class, InputStream.class };
    }
    
    public DataProxy(final DataProvider data) {
        this.data = data;
        this.flavorMap = (SystemFlavorMap)SystemFlavorMap.getDefaultFlavorMap();
    }
    
    private BufferedImage createBufferedImage(final RawBitmap rawBitmap) {
        if (rawBitmap == null || rawBitmap.buffer == null || rawBitmap.width <= 0 || rawBitmap.height <= 0) {
            return null;
        }
        final DirectColorModel directColorModel = null;
        final WritableRaster writableRaster = null;
        DirectColorModel directColorModel2 = null;
        WritableRaster writableRaster2 = null;
        Label_0164: {
            if (rawBitmap.bits == 32 && rawBitmap.buffer instanceof int[]) {
                if (!this.isRGB(rawBitmap) && !this.isBGR(rawBitmap)) {
                    return null;
                }
                final int rMask = rawBitmap.rMask;
                final int gMask = rawBitmap.gMask;
                final int bMask = rawBitmap.bMask;
                final int[] array = (int[])rawBitmap.buffer;
                directColorModel2 = new DirectColorModel(24, rawBitmap.rMask, rawBitmap.gMask, rawBitmap.bMask);
                writableRaster2 = Raster.createPackedRaster(new DataBufferInt(array, array.length), rawBitmap.width, rawBitmap.height, rawBitmap.stride, new int[] { rMask, gMask, bMask }, null);
            }
            else if (rawBitmap.bits == 24 && rawBitmap.buffer instanceof byte[]) {
                int[] array2;
                if (this.isRGB(rawBitmap)) {
                    array2 = new int[] { 0, 1, 2 };
                }
                else {
                    if (!this.isBGR(rawBitmap)) {
                        return null;
                    }
                    array2 = new int[] { 2, 1, 0 };
                }
                final byte[] array3 = (byte[])rawBitmap.buffer;
                final ComponentColorModel componentColorModel = new ComponentColorModel(ColorSpace.getInstance(1000), new int[] { 8, 8, 8 }, false, false, 1, 0);
                final WritableRaster interleavedRaster = Raster.createInterleavedRaster(new DataBufferByte(array3, array3.length), rawBitmap.width, rawBitmap.height, rawBitmap.stride, 3, array2, null);
                directColorModel2 = (DirectColorModel)componentColorModel;
                writableRaster2 = interleavedRaster;
            }
            else {
                if (rawBitmap.bits != 16) {
                    directColorModel2 = directColorModel;
                    writableRaster2 = writableRaster;
                    if (rawBitmap.bits != 15) {
                        break Label_0164;
                    }
                }
                directColorModel2 = directColorModel;
                writableRaster2 = writableRaster;
                if (rawBitmap.buffer instanceof short[]) {
                    final int rMask2 = rawBitmap.rMask;
                    final int gMask2 = rawBitmap.gMask;
                    final int bMask2 = rawBitmap.bMask;
                    final short[] array4 = (short[])rawBitmap.buffer;
                    directColorModel2 = new DirectColorModel(rawBitmap.bits, rawBitmap.rMask, rawBitmap.gMask, rawBitmap.bMask);
                    writableRaster2 = Raster.createPackedRaster(new DataBufferUShort(array4, array4.length), rawBitmap.width, rawBitmap.height, rawBitmap.stride, new int[] { rMask2, gMask2, bMask2 }, null);
                }
            }
        }
        if (directColorModel2 == null || writableRaster2 == null) {
            return null;
        }
        return new BufferedImage(directColorModel2, writableRaster2, false, null);
    }
    
    private String getCharset(final DataFlavor dataFlavor) {
        return dataFlavor.getParameter("charset");
    }
    
    private Object getFileList(final DataFlavor dataFlavor) throws IOException, UnsupportedFlavorException {
        if (!this.data.isNativeFormatAvailable("application/x-java-file-list")) {
            throw new UnsupportedFlavorException(dataFlavor);
        }
        final String[] fileList = this.data.getFileList();
        if (fileList == null) {
            throw new IOException(Messages.getString("awt.4F"));
        }
        return Arrays.asList(fileList);
    }
    
    private Object getHTML(final DataFlavor dataFlavor) throws IOException, UnsupportedFlavorException {
        if (!this.data.isNativeFormatAvailable("text/html")) {
            throw new UnsupportedFlavorException(dataFlavor);
        }
        final String html = this.data.getHTML();
        if (html == null) {
            throw new IOException(Messages.getString("awt.4F"));
        }
        return this.getTextRepresentation(html, dataFlavor);
    }
    
    private Image getImage(final DataFlavor dataFlavor) throws IOException, UnsupportedFlavorException {
        if (!this.data.isNativeFormatAvailable("image/x-java-image")) {
            throw new UnsupportedFlavorException(dataFlavor);
        }
        final RawBitmap rawBitmap = this.data.getRawBitmap();
        if (rawBitmap == null) {
            throw new IOException(Messages.getString("awt.4F"));
        }
        return this.createBufferedImage(rawBitmap);
    }
    
    private Object getPlainText(final DataFlavor dataFlavor) throws IOException, UnsupportedFlavorException {
        if (!this.data.isNativeFormatAvailable("text/plain")) {
            throw new UnsupportedFlavorException(dataFlavor);
        }
        final String text = this.data.getText();
        if (text == null) {
            throw new IOException(Messages.getString("awt.4F"));
        }
        return this.getTextRepresentation(text, dataFlavor);
    }
    
    private Object getSerializedObject(final DataFlavor dataFlavor) throws IOException, UnsupportedFlavorException {
        final String encodeDataFlavor = SystemFlavorMap.encodeDataFlavor(dataFlavor);
        if (encodeDataFlavor == null || !this.data.isNativeFormatAvailable(encodeDataFlavor)) {
            throw new UnsupportedFlavorException(dataFlavor);
        }
        final byte[] serializedObject = this.data.getSerializedObject(dataFlavor.getRepresentationClass());
        if (serializedObject == null) {
            throw new IOException(Messages.getString("awt.4F"));
        }
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(serializedObject);
        try {
            return new ObjectInputStream(byteArrayInputStream).readObject();
        }
        catch (ClassNotFoundException ex) {
            throw new IOException(ex.getMessage());
        }
    }
    
    private Object getTextRepresentation(final String s, final DataFlavor dataFlavor) throws UnsupportedFlavorException, IOException {
        if (dataFlavor.getRepresentationClass() == String.class) {
            return s;
        }
        if (dataFlavor.isRepresentationClassReader()) {
            return new StringReader(s);
        }
        if (dataFlavor.isRepresentationClassCharBuffer()) {
            return CharBuffer.wrap(s);
        }
        if (dataFlavor.getRepresentationClass() == char[].class) {
            final char[] array = new char[s.length()];
            s.getChars(0, s.length(), array, 0);
            return array;
        }
        final String charset = this.getCharset(dataFlavor);
        if (dataFlavor.getRepresentationClass() == byte[].class) {
            return s.getBytes(charset);
        }
        if (dataFlavor.isRepresentationClassByteBuffer()) {
            return ByteBuffer.wrap(s.getBytes(charset));
        }
        if (dataFlavor.isRepresentationClassInputStream()) {
            return new ByteArrayInputStream(s.getBytes(charset));
        }
        throw new UnsupportedFlavorException(dataFlavor);
    }
    
    private Object getURL(final DataFlavor dataFlavor) throws IOException, UnsupportedFlavorException {
        if (!this.data.isNativeFormatAvailable("application/x-java-url")) {
            throw new UnsupportedFlavorException(dataFlavor);
        }
        final String url = this.data.getURL();
        if (url == null) {
            throw new IOException(Messages.getString("awt.4F"));
        }
        final URL url2 = new URL(url);
        if (dataFlavor.getRepresentationClass().isAssignableFrom(URL.class)) {
            return url2;
        }
        if (dataFlavor.isFlavorTextType()) {
            return this.getTextRepresentation(url2.toString(), dataFlavor);
        }
        throw new UnsupportedFlavorException(dataFlavor);
    }
    
    private boolean isBGR(final RawBitmap rawBitmap) {
        return rawBitmap.rMask == 255 && rawBitmap.gMask == 65280 && rawBitmap.bMask == 16711680;
    }
    
    private boolean isRGB(final RawBitmap rawBitmap) {
        return rawBitmap.rMask == 16711680 && rawBitmap.gMask == 65280 && rawBitmap.bMask == 255;
    }
    
    public DataProvider getDataProvider() {
        return this.data;
    }
    
    @Override
    public Object getTransferData(final DataFlavor dataFlavor) throws UnsupportedFlavorException, IOException {
        final String string = String.valueOf(dataFlavor.getPrimaryType()) + "/" + dataFlavor.getSubType();
        if (dataFlavor.isFlavorTextType()) {
            if (string.equalsIgnoreCase("text/html")) {
                return this.getHTML(dataFlavor);
            }
            if (string.equalsIgnoreCase("text/uri-list")) {
                return this.getURL(dataFlavor);
            }
            return this.getPlainText(dataFlavor);
        }
        else {
            if (dataFlavor.isFlavorJavaFileListType()) {
                return this.getFileList(dataFlavor);
            }
            if (dataFlavor.isFlavorSerializedObjectType()) {
                return this.getSerializedObject(dataFlavor);
            }
            if (dataFlavor.equals(DataProvider.urlFlavor)) {
                return this.getURL(dataFlavor);
            }
            if (string.equalsIgnoreCase("image/x-java-image") && Image.class.isAssignableFrom(dataFlavor.getRepresentationClass())) {
                return this.getImage(dataFlavor);
            }
            throw new UnsupportedFlavorException(dataFlavor);
        }
    }
    
    @Override
    public DataFlavor[] getTransferDataFlavors() {
        final ArrayList<DataFlavor> list = new ArrayList<DataFlavor>();
        final String[] nativeFormats = this.data.getNativeFormats();
        for (int i = 0; i < nativeFormats.length; ++i) {
            for (final DataFlavor dataFlavor : this.flavorMap.getFlavorsForNative(nativeFormats[i])) {
                if (!list.contains(dataFlavor)) {
                    list.add(dataFlavor);
                }
            }
        }
        return list.toArray(new DataFlavor[list.size()]);
    }
    
    @Override
    public boolean isDataFlavorSupported(final DataFlavor dataFlavor) {
        final DataFlavor[] transferDataFlavors = this.getTransferDataFlavors();
        for (int i = 0; i < transferDataFlavors.length; ++i) {
            if (transferDataFlavors[i].equals(dataFlavor)) {
                return true;
            }
        }
        return false;
    }
}
