import java.io.*;
import a_vcard.android.syncml.pim.vcard.*;

public class WriteExample
{
    public static void main(final String[] array) throws Exception {
        final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream("example.vcard"), "UTF-8");
        final VCardComposer vCardComposer = new VCardComposer();
        final ContactStruct contactStruct = new ContactStruct();
        contactStruct.name = "Neo";
        contactStruct.company = "The Company";
        contactStruct.addPhone(2, "+123456789", null, true);
        outputStreamWriter.write(vCardComposer.createVCard(contactStruct, 2));
        outputStreamWriter.write("\n");
        outputStreamWriter.close();
    }
}
