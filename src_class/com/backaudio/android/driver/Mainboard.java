package com.backaudio.android.driver;

import android.os.*;
import com.backaudio.android.driver.bluetooth.*;
import com.backaudio.android.driver.beans.*;
import java.nio.charset.*;
import java.nio.*;
import java.util.*;
import android.util.*;
import com.touchus.publicutils.sysconst.*;
import android.text.*;
import java.io.*;

public class Mainboard
{
    private static Mainboard instance;
    String CANBOX;
    String MCU;
    private CarBaseInfo baseInfo;
    private IBluetoothProtocolAnalyzer bluetoothProtocolAnalyzer;
    private boolean iAirMenu;
    private boolean isRecvThreadStarted;
    private IMainboardEventLisenner mainboardEventLisenner;
    private SerialPortMainboardIO mainboardIO;
    private SerialPortMcuIO mcuIO;
    private ProtocolAnalyzer mcuProtocolAnalyzer;
    private Thread recvThread;
    private CarRunInfo runInfo;
    
    static /* synthetic */ int[] $SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye() {
        final int[] $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye = Mainboard.$SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye;
        if ($switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye != null) {
            return $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye;
        }
        final int[] $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2 = new int[BenzModel$EBenzTpye.values().length];
        while (true) {
            try {
                $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.A.ordinal()] = 1;
                try {
                    $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.B.ordinal()] = 2;
                    try {
                        $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.C.ordinal()] = 3;
                        try {
                            $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.E_10.ordinal()] = 4;
                            try {
                                $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.E_11.ordinal()] = 5;
                                try {
                                    $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.E_13.ordinal()] = 6;
                                    try {
                                        $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.E_15.ordinal()] = 7;
                                        try {
                                            $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.GLA.ordinal()] = 8;
                                            try {
                                                $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.GLC.ordinal()] = 9;
                                                try {
                                                    $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.GLE.ordinal()] = 10;
                                                    try {
                                                        $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.GLK.ordinal()] = 11;
                                                        try {
                                                            $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2[BenzModel$EBenzTpye.ML.ordinal()] = 12;
                                                            return Mainboard.$SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye = $switch_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye2;
                                                        }
                                                        catch (NoSuchFieldError noSuchFieldError) {}
                                                    }
                                                    catch (NoSuchFieldError noSuchFieldError2) {}
                                                }
                                                catch (NoSuchFieldError noSuchFieldError3) {}
                                            }
                                            catch (NoSuchFieldError noSuchFieldError4) {}
                                        }
                                        catch (NoSuchFieldError noSuchFieldError5) {}
                                    }
                                    catch (NoSuchFieldError noSuchFieldError6) {}
                                }
                                catch (NoSuchFieldError noSuchFieldError7) {}
                            }
                            catch (NoSuchFieldError noSuchFieldError8) {}
                        }
                        catch (NoSuchFieldError noSuchFieldError9) {}
                    }
                    catch (NoSuchFieldError noSuchFieldError10) {}
                }
                catch (NoSuchFieldError noSuchFieldError11) {}
            }
            catch (NoSuchFieldError noSuchFieldError12) {
                continue;
            }
            break;
        }
    }
    
    static {
        Mainboard.instance = null;
    }
    
    private Mainboard() {
        this.mainboardIO = null;
        this.mcuIO = null;
        this.isRecvThreadStarted = false;
        this.recvThread = null;
        this.mcuProtocolAnalyzer = null;
        this.mainboardEventLisenner = null;
        this.runInfo = new CarRunInfo();
        this.baseInfo = new CarBaseInfo();
        this.iAirMenu = false;
        this.CANBOX = "listenlogwrite";
        this.MCU = "listenlogwrite";
        this.mainboardIO = SerialPortMainboardIO.getInstance();
        this.bluetoothProtocolAnalyzer = new BluetoothProtocolAnalyzer2();
        this.isRecvThreadStarted = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(-19);
                final byte[] array = new byte[16];
            Label_0010:
                while (Mainboard.this.isRecvThreadStarted) {
                    while (true) {
                        while (true) {
                            FileInputStream fileInputStream;
                            try {
                                final File file = new File("/dev/input/event2");
                                if (!file.exists() || !file.canRead()) {
                                    break;
                                }
                                fileInputStream = new FileInputStream(file);
                                if (!Mainboard.this.isRecvThreadStarted) {
                                    fileInputStream.close();
                                    continue Label_0010;
                                }
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                                try {
                                    Thread.sleep(2000L);
                                }
                                catch (InterruptedException ex2) {
                                    ex2.printStackTrace();
                                }
                                continue Label_0010;
                            }
                            try {
                                Mainboard.this.mcuProtocolAnalyzer.pushEvent(array, 0, fileInputStream.read(array, 0, array.length));
                                continue;
                            }
                            catch (IOException ex3) {
                                ex3.printStackTrace();
                                try {
                                    Thread.sleep(2000L);
                                    continue;
                                }
                                catch (InterruptedException ex4) {
                                    ex4.printStackTrace();
                                    continue;
                                }
                                continue;
                            }
                            break;
                        }
                        break;
                    }
                    try {
                        Thread.sleep(3000L);
                    }
                    catch (InterruptedException ex5) {
                        ex5.printStackTrace();
                    }
                }
            }
        }, "event2Thread").start();
        this.recvThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(-19);
                final byte[] array = new byte[200];
                while (Mainboard.this.isRecvThreadStarted) {
                    Label_0064: {
                        try {
                            final int read = Mainboard.this.mainboardIO.read(array);
                            if (read <= 0) {
                                break Label_0064;
                            }
                            Mainboard.this.bluetoothProtocolAnalyzer.push(array, 0, read);
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        continue;
                    }
                    Thread.sleep(2L);
                }
            }
        }, "btThread");
        this.isRecvThreadStarted = true;
        this.recvThread.start();
        this.mcuIO = SerialPortMcuIO.getInstance();
        this.mcuProtocolAnalyzer = new ProtocolAnalyzer();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(-19);
                final byte[] array = new byte[200];
                while (Mainboard.this.isRecvThreadStarted) {
                    Label_0062: {
                        try {
                            final int read = Mainboard.this.mcuIO.read(array);
                            if (read <= 0) {
                                break Label_0062;
                            }
                            Mainboard.this.mcuProtocolAnalyzer.pushMcu(array, 0, read);
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        continue;
                    }
                    Thread.sleep(2L);
                }
            }
        }, "mcuThread").start();
    }
    
    private byte getCheckCanboxByte(final byte[] array) {
        final int length = array.length;
        byte b = array[1];
        for (int i = 2; i < length - 1; ++i) {
            b += array[i];
        }
        return (byte)(b ^ 0xFF);
    }
    
    private byte getCheckMcuByte(final byte[] array) {
        byte b = array[2];
        for (int i = 3; i < array.length - 1; ++i) {
            b += array[i];
        }
        return b;
    }
    
    public static Mainboard getInstance() {
        Label_0028: {
            if (Mainboard.instance != null) {
                break Label_0028;
            }
            synchronized (Mainboard.class) {
                if (Mainboard.instance == null) {
                    Mainboard.instance = new Mainboard();
                }
                return Mainboard.instance;
            }
        }
    }
    
    public void addEventHandler(final IBluetoothEventHandler eventHandler) {
        if (eventHandler == null) {
            return;
        }
        this.bluetoothProtocolAnalyzer.setEventHandler(eventHandler);
    }
    
    public void analyzeAUXStatus(final byte[] array) {
        final byte b = array[3];
        Enum<EAUXStutas> activating = EAUXStutas.ACTIVATING;
        final EAUXStutas[] values = EAUXStutas.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            final EAUXStutas eauxStutas = values[i];
            if (eauxStutas.getCode() == b) {
                activating = eauxStutas;
                break;
            }
        }
        this.mainboardEventLisenner.onAUXActivateStutas((EAUXStutas)activating);
    }
    
    public void analyzeAirInfo(final byte[] array) {
        final byte b = array[3];
        final AirInfo airInfo = new AirInfo();
        airInfo.setiAirOpen((b >> 7 & 0x1) == 0x1);
        airInfo.setiACOpen((b >> 6 & 0x1) == 0x1);
        airInfo.setiInnerLoop((b >> 5 & 0x1) == 0x0);
        airInfo.setiAuto2((b >> 4 & 0x1) == 0x1);
        airInfo.setiAuto1((b >> 3 & 0x1) == 0x1);
        airInfo.setiDual((b >> 2 & 0x1) == 0x1);
        airInfo.setiMaxFrontWind((b >> 1 & 0x1) == 0x1);
        airInfo.setiRear((b & 0x1) == 0x1);
        final byte b2 = array[4];
        airInfo.setiFrontWind((b2 >> 7 & 0x1) == 0x1);
        airInfo.setiFlatWind((b2 >> 6 & 0x1) == 0x1);
        airInfo.setiDownWind((b2 >> 5 & 0x1) == 0x1);
        double n;
        if ((b2 & 0x8) == 0x8) {
            n = 0.5;
        }
        else {
            n = 0.0;
        }
        airInfo.setLevel((b2 & 0x7) + n);
        airInfo.setLeftTemp(15.0 + array[5] * 0.5);
        int n2;
        if (array[5] == 255) {
            n2 = -1;
        }
        else {
            n2 = array[6];
        }
        airInfo.setRightTemp(15.0 + n2 * 0.5);
        if (this.iAirMenu != ((array[7] >> 1 & 0x1) == 0x1)) {
            this.mainboardEventLisenner.onHandleIdriver(EIdriverEnum.HOME, EBtnStateEnum.BTN_DOWN);
        }
        this.mainboardEventLisenner.onAirInfo(airInfo);
    }
    
    public void analyzeBenzSize(final byte[] array) {
        this.mainboardEventLisenner.obtainBenzSize(array[5]);
    }
    
    public void analyzeBenzTpye(final byte[] array) {
        final BenzModel$EBenzTpye c = BenzModel$EBenzTpye.C;
        final byte b = array[5];
        final BenzModel$EBenzTpye[] values = BenzModel$EBenzTpye.values();
        while (true) {
            for (int length = values.length, i = 0; i < length; ++i) {
                final BenzModel$EBenzTpye benzModel$EBenzTpye = values[i];
                if (b == benzModel$EBenzTpye.getCode()) {
                    this.mainboardEventLisenner.obtainBenzType(benzModel$EBenzTpye);
                    return;
                }
            }
            final BenzModel$EBenzTpye benzModel$EBenzTpye = c;
            continue;
        }
    }
    
    public void analyzeBrightnessValue(final byte[] array) {
        this.mainboardEventLisenner.obtainBrightness(array[5]);
    }
    
    public void analyzeCanboxPro(byte[] copyOfRange) {
        final byte b = copyOfRange[2];
        copyOfRange = Arrays.copyOfRange(copyOfRange, 3, b + 3);
        final Charset forName = Charset.forName("US-ASCII");
        final ByteBuffer allocate = ByteBuffer.allocate(b);
        allocate.put(copyOfRange);
        allocate.flip();
        this.mainboardEventLisenner.onCanboxInfo(new String(forName.decode(allocate).array()));
    }
    
    public void analyzeCarBasePro(final byte[] array) {
        boolean b = true;
        final byte b2 = array[3];
        final boolean b3 = (b2 >> 4 & 0x1) == 0x1;
        final boolean b4 = (b2 >> 3 & 0x1) == 0x1;
        final boolean b5 = (b2 >> 2 & 0x1) == 0x1;
        final boolean b6 = (b2 >> 1 & 0x1) == 0x1;
        if ((b2 & 0x1) != 0x1) {
            b = false;
        }
        this.baseInfo.setiPowerOn(b3);
        this.baseInfo.setiInP(b4);
        this.baseInfo.setiInRevering(b5);
        this.baseInfo.setiNearLightOpen(b6);
        this.baseInfo.setiAccOpen(b);
        this.runInfo.setCurSpeed(array[7] & 0xFF);
        this.baseInfo.setiFlash(false);
        this.mainboardEventLisenner.onCarBaseInfo(this.baseInfo);
        this.mainboardEventLisenner.onCarRunningInfo(this.runInfo);
    }
    
    public void analyzeCarBasePro1(final byte[] array) {
        boolean b = false;
        final byte b2 = array[3];
        final boolean b3 = (b2 >> 7 & 0x1) == 0x1;
        final boolean b4 = (b2 >> 6 & 0x1) == 0x1;
        final boolean b5 = (b2 >> 5 & 0x1) == 0x1;
        final boolean b6 = (b2 >> 4 & 0x1) == 0x1;
        final boolean b7 = (b2 >> 3 & 0x1) == 0x1;
        final boolean b8 = (b2 >> 2 & 0x1) == 0x1;
        if ((b2 >> 1 & 0x1) == 0x1) {
            b = true;
        }
        this.baseInfo.setiRightFrontOpen(b3);
        this.baseInfo.setiLeftFrontOpen(b4);
        this.baseInfo.setiRightBackOpen(b5);
        this.baseInfo.setiLeftBackOpen(b6);
        this.baseInfo.setiBack(b7);
        this.baseInfo.setiFront(b8);
        this.baseInfo.setiSafetyBelt(b);
        this.baseInfo.setiFlash(true);
        this.mainboardEventLisenner.onCarBaseInfo(this.baseInfo);
    }
    
    public void analyzeCarRunningInfoPro(final byte[] array) {
        final byte b = array[3];
        final boolean foglight = (b & 0x1) == 0x1;
        final boolean b2 = (b >> 1 & 0x1) == 0x1;
        final boolean b3 = (b >> 2 & 0x1) == 0x1;
        this.baseInfo.ichange = false;
        this.baseInfo.setFoglight(foglight);
        this.baseInfo.setiNearLightOpen(b2);
        this.baseInfo.setiDistantLightOpen(b3);
        this.baseInfo.setiBrake((array[4] & 0x1) == 0x1);
        this.baseInfo.setiFootBrake((array[4] >> 1 & 0x1) == 0x1);
        final byte b4 = array[5];
        final byte b5 = array[6];
        final byte b6 = array[7];
        final byte b7 = array[8];
        this.runInfo.setMileage(((b4 & 0xFF) << 8) + (b5 & 0xFF));
        this.runInfo.setRevolutions(((b6 & 0xFF) << 8) + (b7 & 0xFF));
        final byte b8 = array[9];
        final long totalMileage = ((array[10] & 0xFF) << 24) + ((array[11] & 0xFF) << 16) + ((array[12] & 0xFF) << 8) + (array[13] & 0xFF);
        this.runInfo.setOutsideTemp((b8 & 0xFF) / 2.0f - 40.0);
        this.runInfo.setTotalMileage(totalMileage);
        this.baseInfo.setiFlash(false);
        if (this.baseInfo.ichange) {
            this.mainboardEventLisenner.onCarBaseInfo(this.baseInfo);
        }
        this.mainboardEventLisenner.onCarRunningInfo(this.runInfo);
    }
    
    public void analyzeCarRunningInfoPro1(final byte[] array) {
        this.runInfo.setCurSpeed(array[3] & 0xFF);
        this.runInfo.setRevolutions(((array[4] & 0xFF) << 8) + (array[5] & 0xFF));
        this.mainboardEventLisenner.onCarRunningInfo(this.runInfo);
    }
    
    public void analyzeCarRunningInfoPro2(final byte[] array) {
        this.runInfo.setCurSpeed(array[5] & 0xFF);
        this.runInfo.setRevolutions(((array[3] & 0xFF) << 8) + (array[4] & 0xFF));
        this.mainboardEventLisenner.onCarRunningInfo(this.runInfo);
    }
    
    public void analyzeHornSoundValue(final byte[] array) {
        final byte b = array[4];
        this.mainboardEventLisenner.onHornSoundValue(array[5], array[6], array[7], array[8]);
    }
    
    public void analyzeIdriverPro(final byte[] array) {
        if (array.length < 6) {
            return;
        }
        Enum<EIdriverEnum> none = EIdriverEnum.NONE;
        final byte b = array[3];
        final EIdriverEnum[] values = EIdriverEnum.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            final EIdriverEnum eIdriverEnum = values[i];
            if (b == eIdriverEnum.getCode()) {
                none = eIdriverEnum;
                break;
            }
        }
        EBtnStateEnum eBtnStateEnum = EBtnStateEnum.BTN_DOWN;
        if (array[4] == 0) {
            eBtnStateEnum = EBtnStateEnum.BTN_UP;
        }
        else if (array[4] == 2) {
            eBtnStateEnum = EBtnStateEnum.BTN_LONG_PRESS;
        }
        this.mainboardEventLisenner.onHandleIdriver((EIdriverEnum)none, eBtnStateEnum);
    }
    
    public void analyzeLanguageMediaSet(final byte[] array) {
        final ELanguage simplified_CHINESE = ELanguage.SIMPLIFIED_CHINESE;
        Enum<ELanguage> enum1 = null;
        switch (array[5]) {
            default: {
                enum1 = simplified_CHINESE;
                break;
            }
            case 0: {
                enum1 = ELanguage.SIMPLIFIED_CHINESE;
                break;
            }
            case 1: {
                enum1 = ELanguage.TRADITIONAL_CHINESE;
                break;
            }
            case 2: {
                enum1 = ELanguage.US;
                break;
            }
        }
        this.mainboardEventLisenner.obtainLanguageMediaSet((ELanguage)enum1);
    }
    
    public void analyzeMcuEnterAccOrWakeup(final byte[] array) {
        if (array[5] == 1) {
            this.mainboardEventLisenner.onEnterStandbyMode();
        }
        else if (array[5] == 2) {
            final ECarLayer android = ECarLayer.ANDROID;
            final byte b = array[6];
            final ECarLayer[] values = ECarLayer.values();
            while (true) {
                for (int length = values.length, i = 0; i < length; ++i) {
                    final Enum<ECarLayer> enum1 = values[i];
                    if (b == ((ECarLayer)enum1).getCode()) {
                        this.mainboardEventLisenner.onWakeUp((ECarLayer)enum1);
                        return;
                    }
                }
                final Enum<ECarLayer> enum1 = android;
                continue;
            }
        }
    }
    
    public void analyzeMcuInfoPro(final byte[] array) {
        this.mainboardEventLisenner.obtainVersionNumber(new String(Arrays.copyOfRange(array, 5, array[2] - 2 + 5), Charset.forName("utf-8")));
    }
    
    public void analyzeMcuUpgradeDateByindexPro(final byte[] array) {
        this.mainboardEventLisenner.onMcuUpgradeForGetDataByIndex(((array[4] & 0xFF) << 8) + (array[5] & 0xFF));
    }
    
    public void analyzeMcuUpgradeStatePro(final byte[] array) {
        final byte b = array[4];
        Enum<EUpgrade> error = EUpgrade.ERROR;
        final EUpgrade[] values = EUpgrade.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            final EUpgrade eUpgrade = values[i];
            if (eUpgrade.getCode() == b) {
                error = eUpgrade;
                break;
            }
        }
        this.mainboardEventLisenner.onMcuUpgradeState((EUpgrade)error);
    }
    
    public void analyzeOriginalCarPro(final byte[] array) {
        boolean b = true;
        final byte b2 = array[4];
        switch (array[3]) {
            case 1: {
                b = false;
                break;
            }
            case 2: {
                b = true;
                break;
            }
        }
        Enum<EControlSource> radio = EControlSource.RADIO;
        final EControlSource[] values = EControlSource.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            final EControlSource eControlSource = values[i];
            if (eControlSource.getCode() == b2) {
                radio = eControlSource;
                break;
            }
        }
        this.mainboardEventLisenner.onOriginalCarView((EControlSource)radio, b);
    }
    
    public void analyzeReverseMediaSet(final byte[] array) {
        final EReverserMediaSet nomal = EReverserMediaSet.NOMAL;
        Enum<EReverserMediaSet> enum1 = null;
        switch (array[5]) {
            default: {
                enum1 = nomal;
                break;
            }
            case 0: {
                enum1 = EReverserMediaSet.MUTE;
                break;
            }
            case 1: {
                enum1 = EReverserMediaSet.FLAT;
                break;
            }
            case 2: {
                enum1 = EReverserMediaSet.NOMAL;
                break;
            }
        }
        this.mainboardEventLisenner.obtainReverseMediaSet((EReverserMediaSet)enum1);
    }
    
    public void analyzeReverseTpye(final byte[] array) {
        final EReverseTpye original_REVERSE = EReverseTpye.ORIGINAL_REVERSE;
        Enum<EReverseTpye> enum1 = null;
        switch (array[5]) {
            default: {
                enum1 = original_REVERSE;
                break;
            }
            case 0: {
                enum1 = EReverseTpye.ADD_REVERSE;
                break;
            }
            case 1: {
                enum1 = EReverseTpye.ORIGINAL_REVERSE;
                break;
            }
            case 2: {
                enum1 = EReverseTpye.REVERSE_360;
                break;
            }
        }
        this.mainboardEventLisenner.obtainReverseType((EReverseTpye)enum1);
    }
    
    public void analyzeShowOrHideCarLayer(final byte[] array) {
        final ECarLayer android = ECarLayer.ANDROID;
        final byte b = array[5];
        final ECarLayer[] values = ECarLayer.values();
        while (true) {
            for (int length = values.length, i = 0; i < length; ++i) {
                final Enum<ECarLayer> enum1 = values[i];
                if (b == ((ECarLayer)enum1).getCode()) {
                    this.mainboardEventLisenner.onShowOrHideCarLayer((ECarLayer)enum1);
                    return;
                }
            }
            final Enum<ECarLayer> enum1 = android;
            continue;
        }
    }
    
    public void analyzeStoreDateFromMCU(final byte[] array) {
        final ArrayList<Byte> list = new ArrayList<Byte>();
        for (int i = 5; i < array.length - 1; ++i) {
            list.add(array[i]);
        }
        this.mainboardEventLisenner.obtainStoreData(list);
    }
    
    public void analyzeTimePro(final byte[] array) {
        final byte b = array[3];
        final byte b2 = array[4];
        final byte b3 = array[5];
        int n;
        if ((array[6] >> 7 & 0x1) == 0x1) {
            n = 12;
        }
        else {
            n = 24;
        }
        this.mainboardEventLisenner.onTime(b + 2000, b2, b3, n, array[6] & 0x7F, array[7], 0);
    }
    
    public void analyzeUpgradeDateByindexPro(final byte[] array) {
        this.mainboardEventLisenner.onCanboxUpgradeForGetDataByIndex(((array[3] & 0xFF) << 8) + (array[4] & 0xFF));
    }
    
    public void analyzeUpgradeStatePro(final byte[] array) {
        final byte b = array[3];
        Enum<ECanboxUpgrade> error = ECanboxUpgrade.ERROR;
        final ECanboxUpgrade[] values = ECanboxUpgrade.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            final ECanboxUpgrade eCanboxUpgrade = values[i];
            if (eCanboxUpgrade.getCode() == b) {
                error = eCanboxUpgrade;
                break;
            }
        }
        this.mainboardEventLisenner.onCanboxUpgradeState((ECanboxUpgrade)error);
    }
    
    public void closeOrOpenScreen(final boolean b) {
        if (b) {
            this.writeToMcu(new byte[] { -86, 85, 3, 1, 18, 5, 27 });
        }
        else {
            this.writeToMcu(new byte[] { -86, 85, 3, 1, 18, 4, 26 });
        }
        Log.e(this.MCU, ">>>\tcloseOrOpenScreen:\t" + b);
    }
    
    public void connectOrDisConnectCanbox(final boolean b) {
        final byte[] array = { 46, -127, 1, 0, 0 };
        if (b) {
            array[3] = 1;
        }
        else {
            array[3] = 0;
        }
        array[4] = this.getCheckCanboxByte(array);
        this.writeCanbox(array);
        Log.e(this.CANBOX, ">>>\tconnectOrDisConnectCanbox:\t" + b);
    }
    
    public void enterIntoAcc() {
        final byte[] array = { -86, 85, 2, 1, 1, 0 };
        array[5] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
    }
    
    public void enterOrOutMute(final boolean b) {
        byte b2 = 1;
        final byte[] array = { -86, 85, 3, 1, 65, 1, 0 };
        if (!b) {
            b2 = 0;
        }
        array[5] = b2;
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tenterOrOutMute:\t" + b);
    }
    
    public void forcePress() {
        final byte[] array = { 46, -90, 2, 1, 0, 0 };
        array[5] = this.getCheckCanboxByte(array);
        this.writeCanbox(array);
        Log.e(this.CANBOX, ">>>\tforcePress-- ");
    }
    
    public void forceRequestBTAudio(final boolean b, final BenzModel$EBenzTpye benzModel$EBenzTpye, final boolean b2, final boolean b3, final boolean b4, final int n) {
        final boolean b5 = false;
        byte[] array;
        if (b) {
            array = new byte[] { 46, -91, 2, 2, 0, 0 };
        }
        else {
            array = new byte[] { 46, -91, 2, 1, 0, 0 };
        }
        switch ($SWITCH_TABLE$com$touchus$publicutils$sysconst$BenzModel$EBenzTpye()[benzModel$EBenzTpye.ordinal()]) {
            case 6: {
                if (BenzModel.benzCan == BenzModel$EBenzCAN.ZHTD) {
                    array[4] = 0;
                    break;
                }
                break;
            }
            case 2:
            case 11: {
                if (BenzModel.benzCan == BenzModel$EBenzCAN.ZHTD) {
                    array[4] = 1;
                    break;
                }
                break;
            }
            case 5: {
                if (BenzModel.benzCan == BenzModel$EBenzCAN.ZHTD) {
                    array[4] = 2;
                    break;
                }
                break;
            }
        }
        if (BenzModel.benzCan == BenzModel$EBenzCAN.ZMYT) {
            if (b2) {
                array[4] = 64;
                if (n == 1) {
                    array[4] = 65;
                }
                else if (n == 2) {
                    array[4] = 66;
                }
            }
            else {
                array[4] = -128;
                if (!b3 && b4) {
                    array[4] = -127;
                }
                else if (b3 && b4) {
                    array[4] = -126;
                }
                else if (!b3 && !b4) {
                    array[4] = -125;
                }
                else if (b3 && !b4) {
                    array[4] = -124;
                }
            }
        }
        array[5] = this.getCheckCanboxByte(array);
        this.writeCanbox(array);
        Log.e(this.CANBOX, ">>>\tforceRequestBTAudio-- " + b + ",eBenzTpye = " + benzModel$EBenzTpye + ",RadioType isSquare = " + b2 + ",hasNavi = " + b3 + ",isBT = " + (!b4 || b5) + ",isAUX = " + b4 + ",usbNum = " + n);
    }
    
    public void getAllHornSoundValue() {
        final byte[] array = { -86, 85, 3, 1, 61, 1, 0 };
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
    }
    
    public void getBenzSize() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 105, -1, 108 });
        Log.e(this.MCU, ">>>\tgetBenzSize ");
    }
    
    public void getBenzType() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 100, 0, 104 });
        Log.e(this.MCU, ">>>\tgetBenzType ");
    }
    
    public void getBrightnessValue() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 2, -1, 5 });
        Log.e(this.MCU, ">>>\tgetBrightnessValue");
    }
    
    public void getControlInfo(final boolean b, final EControlSource eControlSource) {
        final byte[] array = new byte[13];
        array[0] = 46;
        array[1] = -126;
        array[2] = 9;
        if (b) {
            array[3] = 2;
        }
        else {
            array[3] = 1;
        }
        array[4] = EControlSource.RADIO.getCode();
        array[12] = this.getCheckCanboxByte(array);
        Log.e(this.CANBOX, ">>>\tgetControlInfo:isOriginal = " + b + "\tEControlSource--" + eControlSource.toString());
    }
    
    public void getLanguageSetFromMcu() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 110, -1, 113 });
        Log.e(this.MCU, ">>>\tgetLanguageSetFromMcu ");
    }
    
    public void getMCUVersionDate() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 16, 1, 21 });
        Log.e(this.MCU, ">>>\tgetMCUVersionDate");
    }
    
    public void getMCUVersionNumber() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 17, 1, 22 });
        Log.e(this.MCU, ">>>\tgetMCUVersionNumber");
    }
    
    public void getModeInfo(final EModeInfo eModeInfo) {
        final byte[] array = { 46, -1, 1, eModeInfo.getCode(), 0 };
        array[4] = this.getCheckCanboxByte(array);
        this.writeCanbox(array);
        Log.e(this.CANBOX, ">>>\tgetModeInfo:EModeInfo--" + eModeInfo.toString());
    }
    
    public void getReverseMediaSetFromMcu() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 103, -1, 106 });
        Log.e(this.MCU, ">>>\tgetReverseMediaSetFromMcu ");
    }
    
    public void getReverseSetFromMcu() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 91, 0, 95 });
        Log.e(this.MCU, ">>>\tgetReverseSetFromMcu ");
    }
    
    public void getStoreDataFromMcu() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 96, 1, 101 });
        Log.e(this.MCU, ">>>\tgetStoreDataFromMcu  ");
    }
    
    public void logcatCanbox(final String s) {
        this.mainboardEventLisenner.logcatCanbox(s);
    }
    
    public void openOrCloseRelay(final boolean b) {
        byte b2 = 1;
        final byte[] array = { -86, 85, 3, 1, 64, 1, 0 };
        if (!b) {
            b2 = 2;
        }
        array[5] = b2;
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\topenOrCloseRelay:\t" + b);
    }
    
    public void powerOffAudio() throws Exception {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 14, 7, 25 });
        Log.e(this.MCU, ">>>\tpowerOffAudio");
    }
    
    public void powerOnAudio() throws Exception {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 14, 8, 26 });
        Log.e(this.MCU, ">>>\tpowerOnAudio");
    }
    
    public void readyToStandby() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 0, 3, 7 });
        Log.e(this.MCU, ">>>\treadyToStandby");
    }
    
    public void readyToWork() {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 0, 4, 8 });
        Log.e(this.MCU, ">>>\treadyToWork");
    }
    
    public void requestAUXOperate(final EAUXOperate eauxOperate) {
        final byte[] array = { 46, -89, 4, 89, 84, eauxOperate.getCode(), 0, 0 };
        array[7] = this.getCheckCanboxByte(array);
        this.writeCanbox(array);
        Log.e(this.CANBOX, ">>>\trequestAUXOperate --" + eauxOperate);
    }
    
    public void requestUpgradeCanbox(final int n) {
        byte b = 0;
        final byte[] array = { 46, -31, 2, 0, 0, 0 };
        final String hexString = Integer.toHexString(n);
        String substring = "";
        String substring2 = hexString;
        if (hexString.length() > 2) {
            substring = hexString.substring(0, hexString.length() - 2);
            substring2 = hexString.substring(hexString.length() - 2);
        }
        if (!TextUtils.isEmpty((CharSequence)substring)) {
            b = (byte)Integer.parseInt(substring, 16);
        }
        array[3] = b;
        array[4] = (byte)Integer.parseInt(substring2, 16);
        array[5] = this.getCheckCanboxByte(array);
        this.writeCanbox(array);
        Log.e(this.CANBOX, ">>>\trequestUpgradeCanbox");
    }
    
    public void requestUpgradeMcu(final byte[] array) {
        final byte[] array2 = { -86, 85, 5, -31, array[0], array[1], array[2], array[3], 0 };
        array2[8] = this.getCheckMcuByte(array2);
        this.writeToMcu(array2);
        Log.e(this.CANBOX, ">>>\trequestUpgradeMCU");
    }
    
    public void sendCoordinateToMcu(final int n, final int n2, final int n3) {
        final String hexString = Integer.toHexString(n);
        String substring = "";
        String substring2 = hexString;
        if (hexString.length() > 2) {
            substring = hexString.substring(0, hexString.length() - 2);
            substring2 = hexString.substring(hexString.length() - 2);
        }
        byte b;
        if (TextUtils.isEmpty((CharSequence)substring)) {
            b = 0;
        }
        else {
            b = (byte)Integer.parseInt(substring, 16);
        }
        final byte b2 = (byte)Integer.parseInt(substring2, 16);
        final String hexString2 = Integer.toHexString(n2);
        String substring3 = "";
        String substring4 = hexString2;
        if (hexString2.length() > 2) {
            substring3 = hexString2.substring(0, hexString2.length() - 2);
            substring4 = hexString2.substring(hexString2.length() - 2);
        }
        byte b3;
        if (TextUtils.isEmpty((CharSequence)substring3)) {
            b3 = 0;
        }
        else {
            b3 = (byte)Integer.parseInt(substring3, 16);
        }
        final byte b4 = (byte)Integer.parseInt(substring4, 16);
        this.writeToMcu(new byte[] { -86, 85, 7, 1, 73, b, b2, b3, b4, (byte)n3, (byte)(b + 81 + b2 + b3 + b4 + n3) });
        Log.e(this.MCU, ">>>\tsendCoordinateToMcu x = " + n + ",y = " + n2 + ",data0 = " + b + ",data1 = " + b2 + ",data2 = " + b3 + ",data3 = " + b4 + ",data4 = " + n3);
    }
    
    public void sendLanguageSetToMcu(final int n) {
        int i = 0;
        final byte[] array = { -86, 85, 3, 1, 110, 0, 0 };
        ELanguage us = ELanguage.US;
        for (ELanguage[] values = ELanguage.values(); i < values.length; ++i) {
            final ELanguage eLanguage = values[i];
            if (eLanguage.getCode() == n) {
                us = eLanguage;
                break;
            }
        }
        array[5] = us.getCode();
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsendLanguageSetToMcu type = " + us);
    }
    
    public void sendRecordToMcu() {
        this.writeToMcu(new byte[] { -86, 85, 2, 1, 84, 87 });
    }
    
    public void sendReverseMediaSetToMcu(final int n) {
        int i = 0;
        final byte[] array = { -86, 85, 3, 1, 103, 0, 0 };
        EReverserMediaSet nomal = EReverserMediaSet.NOMAL;
        for (EReverserMediaSet[] values = EReverserMediaSet.values(); i < values.length; ++i) {
            final EReverserMediaSet set = values[i];
            if (set.getCode() == n) {
                nomal = set;
                break;
            }
        }
        array[5] = nomal.getCode();
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsendReverseMediaSetToMcu type = " + nomal);
    }
    
    public void sendReverseSetToMcu(final int n) {
        switch (n) {
            case 0: {
                this.writeToMcu(new byte[] { -86, 85, 4, 1, 91, 1, 1, 98 });
                break;
            }
            case 1: {
                this.writeToMcu(new byte[] { -86, 85, 4, 1, 91, 1, 0, 97 });
                break;
            }
            case 2: {
                this.writeToMcu(new byte[] { -86, 85, 4, 1, 91, 1, 2, 99 });
                break;
            }
        }
        Log.e(this.MCU, ">>>\tsendReverseSetToMcu type = " + n);
    }
    
    public void sendStoreDataToMcu(byte[] byteArray) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(new byte[] { -86, 85, -126, 1, 97 }, 0, 5);
        if (byteArray.length < 128) {
            byteArrayOutputStream.write(byteArray, 0, byteArray.length);
            byteArray = new byte[128 - byteArray.length];
            byteArrayOutputStream.write(byteArray, 0, byteArray.length);
        }
        else {
            byteArrayOutputStream.write(byteArray, 0, 128);
        }
        byteArrayOutputStream.write(new byte[1], 0, 1);
        final byte checkMcuByte = this.getCheckMcuByte(byteArrayOutputStream.toByteArray());
        byteArray = byteArrayOutputStream.toByteArray();
        byteArray[byteArray.length - 1] = checkMcuByte;
        this.writeToMcu(byteArray);
        Log.e(this.MCU, ">>>\tsendStoreDataToMcu data = " + Utils.byteArrayToHexString(byteArray));
    }
    
    public void sendUpdateCanboxInfoByIndex(final int n, byte[] byteArray) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = { 46, -29, -126, 0, 0 };
        if (n >= 0) {
            final String hexString = Integer.toHexString(n);
            String substring = "";
            String substring2 = hexString;
            if (hexString.length() > 2) {
                substring = hexString.substring(0, hexString.length() - 2);
                substring2 = hexString.substring(hexString.length() - 2);
            }
            byte b;
            if (TextUtils.isEmpty((CharSequence)substring)) {
                b = 0;
            }
            else {
                b = (byte)Integer.parseInt(substring, 16);
            }
            array[3] = b;
            array[4] = (byte)Integer.parseInt(substring2, 16);
        }
        else {
            array[4] = (array[3] = 31);
        }
        byteArrayOutputStream.write(array, 0, 5);
        if (byteArray.length < 128) {
            byteArrayOutputStream.write(byteArray, 0, byteArray.length);
            byteArray = new byte[128 - byteArray.length];
            byteArrayOutputStream.write(byteArray, 0, byteArray.length);
        }
        else {
            byteArrayOutputStream.write(byteArray, 0, 128);
        }
        byteArrayOutputStream.write(new byte[1], 0, 1);
        final byte checkCanboxByte = this.getCheckCanboxByte(byteArrayOutputStream.toByteArray());
        byteArray = byteArrayOutputStream.toByteArray();
        byteArray[byteArray.length - 1] = checkCanboxByte;
        this.writeCanbox(byteArray);
        Log.e(this.CANBOX, ">>>\tsendUpdateCanboxInfoByIndex;\tindex--" + n);
    }
    
    public void sendUpdateMcuInfoByIndex(final int n, byte[] byteArray) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = { -86, 85, -125, -29, 0, 0 };
        if (n >= 0) {
            final String hexString = Integer.toHexString(n);
            String substring = "";
            String substring2 = hexString;
            if (hexString.length() > 2) {
                substring = hexString.substring(0, hexString.length() - 2);
                substring2 = hexString.substring(hexString.length() - 2);
            }
            byte b;
            if (TextUtils.isEmpty((CharSequence)substring)) {
                b = 0;
            }
            else {
                b = (byte)Integer.parseInt(substring, 16);
            }
            array[4] = b;
            array[5] = (byte)Integer.parseInt(substring2, 16);
        }
        else {
            array[5] = (array[4] = 31);
        }
        byteArrayOutputStream.write(array, 0, 6);
        if (byteArray.length < 128) {
            byteArrayOutputStream.write(byteArray, 0, byteArray.length);
            byteArray = new byte[128 - byteArray.length];
            byteArrayOutputStream.write(byteArray, 0, byteArray.length);
        }
        else {
            byteArrayOutputStream.write(byteArray, 0, 128);
        }
        byteArrayOutputStream.write(new byte[1], 0, 1);
        final byte checkMcuByte = this.getCheckMcuByte(byteArrayOutputStream.toByteArray());
        byteArray = byteArrayOutputStream.toByteArray();
        byteArray[byteArray.length - 1] = checkMcuByte;
        this.writeToMcu(byteArray);
        Log.e(this.CANBOX, ">>>\tsendUpdateMcuInfoByIndex;\tindex-- " + n);
    }
    
    public void sendUpgradeMcuHeaderInfo(final int n) {
        byte b = 0;
        final byte[] array = { -86, 85, 3, -27, 0, 0, 0 };
        final String hexString = Integer.toHexString(n);
        String substring = "";
        String substring2 = hexString;
        if (hexString.length() > 2) {
            substring = hexString.substring(0, hexString.length() - 2);
            substring2 = hexString.substring(hexString.length() - 2);
        }
        if (!TextUtils.isEmpty((CharSequence)substring)) {
            b = (byte)Integer.parseInt(substring, 16);
        }
        array[4] = b;
        array[5] = (byte)Integer.parseInt(substring2, 16);
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.CANBOX, ">>>\tsendUpgradeMcuHeaderInfo");
    }
    
    public void setAllHornSoundValue(final int n, final int n2, final int n3, final int n4, final int n5) {
        final byte[] array = { -86, 85, 7, 1, 60, (byte)n, (byte)n2, (byte)n3, (byte)n4, (byte)n5, 0 };
        array[10] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsetAllHornSoundValue:\tmain = " + n + ",fLeft = " + n2 + ",fRight = " + n3 + ",rLeft = " + n4 + ",rRight = " + n5);
    }
    
    public void setBenzSize(final int n) {
        final byte[] array = { -86, 85, 3, 1, 105, (byte)n, 0 };
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsetBenzSize type = " + (n + 1));
    }
    
    public void setBenzType(final BenzModel$EBenzTpye benzModel$EBenzTpye) {
        final byte[] array = { -86, 85, 3, 1, 101, benzModel$EBenzTpye.getCode(), 0 };
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsetBenzType type = " + benzModel$EBenzTpye);
    }
    
    public void setBrightnessToMcu(final int n) {
        final byte[] array = { -86, 85, 3, 1, 2, (byte)n, 0 };
        array[6] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsetBrightnessToMcu value = " + n);
    }
    
    public void setDVSoundValue(final int n, final int n2, final int n3, final int n4, final int n5) {
        final byte[] array = { -86, 85, 7, 1, 99, (byte)n, (byte)n2, (byte)n3, (byte)n4, (byte)n5, 0 };
        array[10] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsetDVSoundValue:\tDV main = " + n + ",fLeft = " + n2 + ",fRight = " + n3 + ",rLeft = " + n4 + ",rRight = " + n5);
    }
    
    public void setMainboardEventLisenner(final IMainboardEventLisenner mainboardEventLisenner) {
        this.mainboardEventLisenner = mainboardEventLisenner;
    }
    
    public void setOldCBTAudio() {
        if (BenzModel.benzCan == BenzModel$EBenzCAN.ZMYT) {
            final byte[] array = { 46, -91, 2, 2, -128, 0 };
            array[5] = this.getCheckCanboxByte(array);
            this.writeCanbox(array);
            Log.e(this.CANBOX, ">>>\tsetOldCBTAudio-- ");
        }
    }
    
    public void setSoundTypeValue(final int n, final int n2, final int n3) {
        final byte[] array = { -86, 85, 4, 1, 92, 1, 0, 0, 0 };
        array[5] = (byte)n;
        array[6] = (byte)n2;
        array[7] = (byte)n3;
        array[8] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
        Log.e(this.MCU, ">>>\tsetSoundTypeValue:\ttrebleValue = " + n + ",midtonesValue = " + n2 + ",bassValue = " + n3);
    }
    
    public void showCarLayer(final ECarLayer eCarLayer) {
        this.writeToMcu(new byte[] { -86, 85, 3, 1, 74, eCarLayer.getCode(), (byte)(eCarLayer.getCode() + 78) });
        Log.e(this.MCU, ">>>\tshowCarLayer == " + eCarLayer);
    }
    
    public void wakeupMcu() {
        final byte[] array = { -86, 85, 2, 1, 2, 0 };
        array[5] = this.getCheckMcuByte(array);
        this.writeToMcu(array);
    }
    
    protected byte[] wrapCanbox(final byte[] array) {
        if (array == null) {
            return null;
        }
        final byte[] array2 = new byte[array.length + 6];
        array2[0] = -86;
        array2[1] = 85;
        array2[2] = (byte)(array.length + 2);
        array2[3] = 1;
        array2[4] = 80;
        byte b = (byte)(array2[2] + array2[3] + array2[4]);
        for (int i = 0; i < array.length; ++i) {
            array2[i + 5] = array[i];
            b += array[i];
        }
        array2[array.length + 5] = b;
        return array2;
    }
    
    public void writeBlueTooth(final byte[] array) {
        synchronized (this) {
            this.mainboardIO.write(array);
        }
    }
    
    public void writeCanbox(byte[] wrapCanbox) {
        synchronized (this) {
            wrapCanbox = this.wrapCanbox(wrapCanbox);
            if (this.mainboardIO != null && wrapCanbox != null) {
                Log.d("driverlog", "canboxProtocol write = " + Utils.byteArrayToHexString(wrapCanbox));
                this.mcuIO.write(wrapCanbox);
            }
        }
    }
    
    public void writeToMcu(final byte[] array) {
        this.mcuIO.write(array);
    }
    
    public enum EAUXOperate
    {
        ACTIVATE("ACTIVATE", 0, 1), 
        DEACTIVATE("DEACTIVATE", 1, 2);
        
        private int code;
        
        private EAUXOperate(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EAUXStutas
    {
        ACTIVATING("ACTIVATING", 0, 0), 
        FAILED("FAILED", 2, 2), 
        SUCCEED("SUCCEED", 1, 1);
        
        private int code;
        
        private EAUXStutas(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EBtnStateEnum
    {
        BTN_DOWN("BTN_DOWN", 1, 1), 
        BTN_LONG_PRESS("BTN_LONG_PRESS", 2, 2), 
        BTN_UP("BTN_UP", 0, 0);
        
        private int code;
        
        private EBtnStateEnum(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum ECanboxUpgrade
    {
        ERROR("ERROR", 0, 0), 
        FINISH("FINISH", 2, 2), 
        READY_UPGRADING("READY_UPGRADING", 1, 1);
        
        private int code;
        
        private ECanboxUpgrade(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum ECarLayer
    {
        ANDROID("ANDROID", 0, 16), 
        BT_CONNECT("BT_CONNECT", 5, 65), 
        DV("DV", 2, 33), 
        ORIGINAL("ORIGINAL", 4, 64), 
        ORIGINAL_REVERSE("ORIGINAL_REVERSE", 6, 66), 
        QUERY("QUERY", 9, 255), 
        RECORDER("RECORDER", 1, 32), 
        REVERSE("REVERSE", 8, 80), 
        REVERSE_360("REVERSE_360", 3, 48), 
        SCREEN_OFF("SCREEN_OFF", 7, 96);
        
        private int code;
        
        private ECarLayer(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EControlSource
    {
        AUX("AUX", 7, 7), 
        BT("BT", 5, 5), 
        CAMERA("CAMERA", 12, 12), 
        DISC("DISC", 2, 2), 
        DVR("DVR", 10, 10), 
        GPS("GPS", 4, 4), 
        MENU("MENU", 11, 11), 
        MOBLIE_WEB("MOBLIE_WEB", 14, 14), 
        POWER_OFF("POWER_OFF", 0, 0), 
        RADIO("RADIO", 1, 1), 
        SD("SD", 9, 9), 
        TPMS("TPMS", 13, 13), 
        TV("TV", 3, 3), 
        USB("USB", 8, 8), 
        iPod("iPod", 6, 6);
        
        private int code;
        
        private EControlSource(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EEqualizer
    {
        BASS("BASS", 2, 0), 
        MIDTONES("MIDTONES", 1, 1), 
        TREBLE("TREBLE", 0, 2);
        
        private int code;
        
        private EEqualizer(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EIdriverEnum
    {
        BACK("BACK", 16, 16), 
        BT("BT", 7, 7), 
        CALL_FIX("CALL_FIX", 29, 36), 
        CALL_HELP("CALL_HELP", 27, 34), 
        CALL_SOS("CALL_SOS", 28, 35), 
        CARSET("CARSET", 26, 33), 
        DOWN("DOWN", 11, 11), 
        ESC("ESC", 22, 26), 
        HANG_UP("HANG_UP", 9, 9), 
        HOME("HOME", 12, 12), 
        K_HOME("K_HOME", 23, 27), 
        LEFT("LEFT", 15, 15), 
        MEDIA("MEDIA", 19, 23), 
        MODE("MODE", 5, 5), 
        MUTE("MUTE", 6, 6), 
        NAVI("NAVI", 18, 22), 
        NEXT("NEXT", 3, 3), 
        NONE("NONE", 0, 0), 
        PICK_UP("PICK_UP", 8, 8), 
        POWER_OFF("POWER_OFF", 20, 24), 
        PRESS("PRESS", 13, 13), 
        PREV("PREV", 4, 4), 
        RADIO("RADIO", 17, 21), 
        RIGHT("RIGHT", 14, 14), 
        SPEAKOFF("SPEAKOFF", 24, 28), 
        SPEECH("SPEECH", 21, 25), 
        STAR_BTN("STAR_BTN", 25, 32), 
        TURN_LEFT("TURN_LEFT", 31, 65), 
        TURN_RIGHT("TURN_RIGHT", 30, 64), 
        UP("UP", 10, 10), 
        VOL_ADD("VOL_ADD", 1, 1), 
        VOL_DES("VOL_DES", 2, 2);
        
        private int code;
        
        private EIdriverEnum(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum ELanguage
    {
        SIMPLIFIED_CHINESE("SIMPLIFIED_CHINESE", 0, 0), 
        TRADITIONAL_CHINESE("TRADITIONAL_CHINESE", 1, 1), 
        US("US", 2, 2);
        
        private int code;
        
        private ELanguage(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EModeInfo
    {
        AIR_CONDITION("AIR_CONDITION", 1, 3), 
        BASE_INFO("BASE_INFO", 0, 1), 
        CANBOX_INFO("CANBOX_INFO", 9, 127), 
        CAR_DOOR("CAR_DOOR", 4, 6), 
        DASHBOARD_INFO("DASHBOARD_INFO", 8, 53), 
        FRONT_RADAR("FRONT_RADAR", 3, 5), 
        ORIGINAL_SOURCE("ORIGINAL_SOURCE", 7, 18), 
        ORIGINAL_TIME("ORIGINAL_TIME", 5, 8), 
        REAR_RADAR("REAR_RADAR", 2, 4), 
        WHEEL_ANGLE("WHEEL_ANGLE", 6, 10);
        
        private int code;
        
        private EModeInfo(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EReverseTpye
    {
        ADD_REVERSE("ADD_REVERSE", 0, 0), 
        ORIGINAL_REVERSE("ORIGINAL_REVERSE", 1, 1), 
        REVERSE_360("REVERSE_360", 2, 2);
        
        private int code;
        
        private EReverseTpye(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EReverserMediaSet
    {
        FLAT("FLAT", 1, 1), 
        MUTE("MUTE", 0, 0), 
        NOMAL("NOMAL", 2, 2), 
        QUERY("QUERY", 3, 255);
        
        private int code;
        
        private EReverserMediaSet(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
    
    public enum EUpgrade
    {
        ERROR("ERROR", 0, 0), 
        FINISH("FINISH", 3, 3), 
        GET_HERDER("GET_HERDER", 1, 1), 
        UPGRADING("UPGRADING", 2, 2);
        
        private int code;
        
        private EUpgrade(final String s, final int n, final int code) {
            this.code = code;
        }
        
        public byte getCode() {
            return (byte)this.code;
        }
    }
}
